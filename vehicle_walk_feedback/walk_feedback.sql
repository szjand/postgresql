﻿python: luigi.WalkFeedbackEmails.py

delete from ads.ext_vehicle_walk_feedback where walk_date = current_date

select * from ads.ext_vehicle_walk_feedback where walk_date = current_date

update ads.ext_vehicle_walk_feedback 
set sent = false
where walk_date = current_date;

update ads.ext_vehicle_walk_feedback 
set sent = true
where stock_number = 'G45722X';

truncate ads.ext_vehicle_walk_feedback

delete from ads.ext_vehicle_walk 
where walk_date = current_date;

select a.stock_number, (select * from ads.get_walk_feedback(a.stock_number))
from ads.ext_vehicle_walk_feedback a
where not sent
and walk_date = current_date;
                    
                    
drop function ads.get_walk_feedback(citext);

alter table ads.ext_vehicle_walk_Feedback
add column copied_to_pg boolean;

create or replace function ads.get_walk_feedback(_stock_number citext)
returns text as
-- returns table (stock_number citext, feedback text) as
$BODY$
/*
select * from ads.get_walk_feedback('G44375RA')
*/


select 
		'****** for reasons yet unknown, it appears that some feedback emails are going to to the SPAM folder ******'
		|| E'\r\n' || E'\r\n' ||
    'Walked on ' || walk_date
    || E'\r\n' || E'\r\n' ||
    'Evaluator: ' || evaluator  || '  ' || 'Walker: ' || walker
    || E'\r\n' || E'\r\n' ||
    stock_number || ' : ' || vin || '  ' || 'Prior Owner: ' || prior_owner
    || E'\r\n' || E'\r\n' ||
    model_year || ' ' || make || ' ' || model || ' ' || coalesce(trim_level, '') || ' ' || 
        coalesce(body_style, '') 
    || E'\r\n' || E'\r\n' || 
    'Miles: ' || miles
    || E'\r\n' || E'\r\n' ||
    'Evaluator Notes: ' || evaluator_notes
    || E'\r\n' || E'\r\n' ||
    'Feedback: ' || notes
from ads.ext_vehicle_walk_feedback
where stock_number = _stock_number;
$BODY$
language sql;

update ads.ext_vehicle_walk_feedback set copied_to_pg = true

select * from ads.ext_Vehicle_walk_feedback order by stock_number

10/24/22
requests for prior owner name & evaluator notes
select * from ads.ext_Vehicle_evaluations where vehicleevaluationts::date > current_date - 7

select * from ads.ext_vehicle_walk_feedback where stock_number = 'G45804HA'

select * from ads.ext_Vehicle_walk_feedback where walk_date = current_date


delete from ads.ext_vehicle_walk_feedback where walk_date > '10/23/2022'

                  select a.stock_number, (select * from ads.get_walk_feedback(a.stock_number))
                  from ads.ext_vehicle_walk_feedback a
                  where not sent
                    and walk_date = current_date
                    and not sent;

select * from ads.ext_vehicle_walk_feedback where not sent and walk_date = current_date

update ads.ext_vehicle_walk_feedback
set sent = false
where walk_date = current_date


-- advantage feedback today
SELECT * 
-- SELECT COUNT(*)
FROM VehicleWalks a
WHERE CAST(VehicleWalkts AS sql_date) = curdate()
  AND EXISTS (
	  SELECT 1
		FROM VehicleInventoryItemNotes
		WHERE subcategory = 'VehicleWalk_EvaluatorFeedback'
		  AND VehicleInventoryItemID = a.VehicleInventoryItemID)
ORDER BY VehicleWalkts			
			
SELECT * FROM vehicle_walk_feedback WHERE walk_date = curdate()			

SELECT COUNT(*) FROM vehicle_walk_feedback WHERE walk_date = curdate()


