﻿select a.seq, a.year_month, a.store, a.department, a.area, 
  b.currently, b.pace,
  c.currently, c.pace, 
  d.currently, d.pace,
  e.currently, e.pace,
  f.currently, f.pace,
  g.currently, g.pace,
  h.currently, h.pace
from (
  select seq, year_month, the_date, store, department, area
  from fp.flightplan
  where the_date = '09/17/2018'
  order by the_date, seq) a
left join (  
  select seq, currently, pace
  from fp.flightplan
  where the_date = '09/17/2018') b on a.seq = b.seq
left join (  
  select seq, currently, pace
  from fp.flightplan
  where the_date = '09/18/2018') c on a.seq = c.seq  
left join (  
  select seq, currently, pace
  from fp.flightplan
  where the_date = '09/19/2018') d on a.seq = d.seq
left join (  
  select seq, currently, pace
  from fp.flightplan
  where the_date = '09/20/2018') e on a.seq = e.seq
left join (  
  select seq, currently, pace
  from fp.flightplan
  where the_date = '09/21/2018') f on a.seq = f.seq    
left join (  
  select seq, currently, pace
  from fp.flightplan
  where the_date = '09/22/2018') g on a.seq = g.seq
left join (  
  select seq, currently, pace
  from fp.flightplan
  where the_date = '09/23/2018') h on a.seq = h.seq   
order by a.the_date, a.seq

