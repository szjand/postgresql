﻿
--------------------------------------------------------------------------
-- fixed dept gross/net
-- needs to include ros in cashier status (like deals boarded but not in accounting
--------------------------------------------------------------------------      
gross
  main shop/pdq/detail/carwash
  P4L2 (dept) <- P6L34
  dept: SD, QL, RE, CW

  parts 
  P6L51, 54-55

  body shop
  P4L2 <- P6L43

net
   P4L61 (dept)


select *
from fp.gl_Accounts
where page = 16
  and store = 'RY1'
order by line, col




select *
from fin.fact_Fs a
join fin.dim_Fs b on a.fs_key = b.fs_key
  and b.year_month = 201809     

select * from arkona.ext_eisglobal_sypffxmst limit 10


select *
from fp.gl_accounts
where store = 'RY1'
  and page = 4



-- delete 
-- from fp.acct_sales_gross_details
-- where year_month = _year_month;
-- 
-- insert into fp.acct_sales_gross_details
drop table if exists sept;
create temp table sept as
select b.year_month, b.the_date, a.control, d.store, d.department, d.page, d.line, d.col,
  d.account, e.journal_code, sum(-a.amount)::integer as amount
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201809
join fin.dim_account c on a.account_key = c.account_key
join fp.gl_accounts d on c.account = d.account
  and d.page = 16 and d.line between 21 and 63
join fin.dim_journal e on a.journal_key  = e.journal_key    
where a.post_status = 'Y'
group by b.year_month, b.the_date, a.control, d.store, d.department, d.page, d.line, d.col,
  d.account, e.journal_code;         

select col, sum(amount) 
from sept  
where store = 'ry1'
  and line between 21 and 33
group by col
order by line, col

-- well, initially, this is not close enug
-- lets look at the fs, and of course, it is right on
select b.line, b.col, sum(a.amount)
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
where b.year_month = 201809
  and page = 16
  and line between 21 and 33
  and c.store = 'ry1'
group by b.line, b.col
order by b.line, b.col



-- but this is spotty
select line, col, sum(amount) 
from sept  
where store = 'ry1'
  and line between 21 and 33
group by line, col
order by line, col


-- do the accounts match?
-- looks like a bunch of "extra" accounts in fp
-- except when i grouped and added amount, they are real close
select *, abs(aa.amount) - abs(bb.amount)
from (
select 'fs', d.gl_Account, b.line, b.col, sum(amount) as amount
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
join fin.dim_fs_Account d on a.fs_account_key = d.fs_account_key
where b.year_month = 201809
  and page = 16
  and line between 21 and 33
  and c.store = 'ry1'
group by d.gl_Account, b.line, b.col) aa
full outer join (
select 'fp', account, line, col, sum(amount) as amount
from sept -- fp.gl_accounts
where page = 16 
  and line between 21 and 33
  and store = 'ry1'
group by account, line, col) bb on aa.gl_account = bb.account
order by aa.line, aa.col, aa.gl_account

-- compare totals:  338 diff  close enuf
select 'fs', sum(amount) as amount
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
join fin.dim_fs_Account d on a.fs_account_key = d.fs_account_key
where b.year_month = 201809
  and page = 16
  and line between 21 and 33
  and c.store = 'ry1'

select 'fp',sum(amount) as amount
from sept -- fp.gl_accounts
where page = 16 
  and line between 21 and 33
  and store = 'ry1'

-- compare totals for whole page: not so good, 24117 diff
select 'fs', sum(amount) as amount
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
join fin.dim_fs_Account d on a.fs_account_key = d.fs_account_key
where b.year_month = 201809
  and page = 16
  and line between 21 and 61
  and c.store = 'ry1'

select 'fp',sum(amount) as amount
from sept -- fp.gl_accounts
where page = 16 
  and line between 21 and 61
  and store = 'ry1'

--narrow it down
-- bs
select 'fs', sum(amount) as amount ---206384
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
join fin.dim_fs_Account d on a.fs_account_key = d.fs_account_key
where b.year_month = 201809
  and page = 16
  and line between 35 and 43
  and c.store = 'ry1'

select 'fp',sum(amount) as amount  --206371
from sept -- fp.gl_accounts
where page = 16 
  and line between 35 and 43
  and store = 'ry1'

-- parts  here's the diff
line 49
select *, abs(aa.amount) - abs(bb.amount)
from (
select 'fs', line, sum(amount) as amount ---434757
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
join fin.dim_fs_Account d on a.fs_account_key = d.fs_account_key
where b.year_month = 201809
  and page = 16
  and line between 45 and 61
  and c.store = 'ry1'
group by line) aa
full outer join (
select 'fp', line, sum(amount) as amount  --410315
from sept -- fp.gl_accounts
where page = 16 
  and line between 45 and 61
  and store = 'ry1'
group by line) bb on aa.line = bb.line


select 'fs', a.amount, d.gl_account
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
join fin.dim_fs_Account d on a.fs_account_key = d.fs_account_key
where b.year_month = 201809
  and page = 16
  and line = 49
  and c.store = 'ry1'

-- aha, does not include account 147701/167701
select 'fp', account, sum(amount)
from sept -- fp.gl_accounts
where page = 16 
  and line = 49
  and store = 'ry1'
group by account

select * from fp.gl_Accounts where account in ('147701','167701')

select distinct case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store,
  c.department, a.fxmpge as page, a.fxmlne::integer as line, a.fxmcol as col, 
  b.g_l_acct_number as gl_account
from arkona.ext_Eisglobal_sypffxmst a 
inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
join fin.dim_account c on b.g_l_acct_number = c.account
  and c.current_row = true    
where ( -- use routing from this and the prev year
  a.fxmcyy = (select the_year from dds.dim_date where the_date = current_date)
  or
  a.fxmcyy = (select the_year - 1 from dds.dim_date where the_date = current_date))
  and case when b.g_l_acct_number = '146424' then a.fxmlne <> 27 else 1 = 1 end
  and not exists (
    select 1
    from fp.gl_accounts
    where account = b.g_l_acct_number);


select *
from arkona.ext_ffpxrefdta
where factory_financial_year = 2018
  and g_l_acct_number in ('147701','167701')
-- ahh, they are routed to page 4 line 59
select *
from fp.gl_accounts
where account in ('147701','167701')




select *
from fp.gl_accounts
where page = 4 and line = 59

thinking do parts separately?
just need to be careful, and stick to her definition of gross and net

-- -- est gross from ros in cashier status
-- -- one row per day
-- insert into fp.fixed_est_gross_from_cashier_status
-- select 201810, current_date - 1,
--   coalesce((sum(est_labor_gross) filter (where service_type in ('MN','MR','AM','EM','OT')))::integer, 0) as service, 
--   coalesce((sum(est_labor_gross) filter (where service_type in ('QS', 'QL')))::integer, 0) as pdq, 
--   coalesce((sum(est_labor_gross) filter (where service_type = 'RE'))::integer, 0) as detail,
--   coalesce((sum(est_labor_gross) filter (where service_type = 'CW'))::integer, 0) as carwash, 
--   coalesce((sum(est_labor_gross) filter (where service_type = 'BS'))::integer, 0) as bodyshop, 
--   coalesce((sum(est_parts_gross))::integer, 0) as parts
-- --   (sum(est_parts_gross) filter (where service_type in ('MN','MR','AM','EM','OT')))::integer as service_est_parts_gross
-- --   (sum(est_parts_gross) filter (where service_type in ('QS', 'QL')))::integer as pdq_est_parts_gross, 
-- --   (sum(est_parts_gross) filter (where service_type = 'CW'))::integer as carwash_est_parts_gross, 
-- --   (sum(est_parts_gross) filter (where service_type = 'BS'))::integer as bodyshop_est_parts_gross, 
-- --   (sum(est_parts_gross) filter (where service_type = 'RE'))::integer as detail_est_parts_gross
-- from jeri.open_ros
-- where the_date = current_date - 1
--   and status = 'cashier'
--   and store_code = 'RY1';
-- 
-- create table fp.fixed_est_gross_from_cashier_status (
--   year_month integer not null,
--   the_date date primary key,
--   service integer not null,
--   pdq integer not null,
--   detail integer not null,
--   carwash integer not null,
--   bodyshop integer not null,
--   parts integer not null);
  
-- main shop gross/net
-- jeri is defining net as P4L61 which means it includes parts gross
-- may break out parts split in fp.seq
-- just need to be careful, and stick to her definition of gross and net
-- use sept as a simulation for fp.fixed_gross_details (have not created it yet)


drop table if exists sept;
create temp table sept as
select b.year_month, b.the_date, a.control, d.store, d.department, d.page, d.line, d.col,
  d.account, e.journal_code, sum(-a.amount)::integer as amount
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201809
join fin.dim_account c on a.account_key = c.account_key
join fp.gl_accounts d on c.account = d.account
  and d.page = 16 and d.line between 21 and 63
join fin.dim_journal e on a.journal_key  = e.journal_key    
where a.post_status = 'Y'
group by b.year_month, b.the_date, a.control, d.store, d.department, d.page, d.line, d.col,
  d.account, e.journal_code;         


drop table if exists oct;
create temp table oct as
select b.year_month, b.the_date, a.control, d.store, d.department, d.page, d.line, d.col,
  d.account, e.journal_code, sum(-a.amount)::integer as amount
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201810
join fin.dim_account c on a.account_key = c.account_key
join fp.gl_accounts d on c.account = d.account
  and d.page = 16 and d.line between 21 and 63
join fin.dim_journal e on a.journal_key  = e.journal_key    
where a.post_status = 'Y'
group by b.year_month, b.the_date, a.control, d.store, d.department, d.page, d.line, d.col,
  d.account, e.journal_code;    



select * 
from fin.parts_split a
left join fp.gl_Accounts b on a.gl_Account = b.account

select * from sept limit 100

-- not perfect compared to the statement, but good enuf
select a.*, 
  sum(amount) over (partition by line)
from (  
  select department, line, sum(amount) as amount
  from sept 
  where line between 21 and 63
    and store = 'RY1'
  group by department, line) a
order by line, department

-- GROSS service, pdq, detail, carwash, bodyshop 
-- P4L2
select 
  sum(amount) filter (where line between 21 and 33 and department in('service','quick lane','detail','car wash')) as all_service,
  sum(amount) filter (where line between 21 and 33 and department = 'service') as service,
  sum(amount) filter (where line between 21 and 33 and department = 'quick lane') as pdq,
  sum(amount) filter (where line between 21 and 33 and department = 'detail') as detail,
  sum(amount) filter (where line between 21 and 33 and department = 'car wash') as carwash,
  sum(amount) filter (where line between 35 and 43 and department = 'body shop') as bodyshop,
  sum(amount) filter (where line between 45 and 59 and department = 'parts') as parts
from sept
where store = 'RY1'

select 
  sum(amount) filter (where line between 21 and 33 and department in('service','quick lane','detail','car wash')) as all_service,
  sum(amount) filter (where line between 21 and 33 and department = 'service') as service,
  sum(amount) filter (where line between 21 and 33 and department = 'quick lane') as pdq,
  sum(amount) filter (where line between 21 and 33 and department = 'detail') as detail,
  sum(amount) filter (where line between 21 and 33 and department = 'car wash') as carwash,
  sum(amount) filter (where line between 35 and 43 and department = 'body shop') as bodyshop,
  sum(amount) filter (where line between 45 and 59 and department = 'parts') as parts
from oct
where store = 'RY1'


select 46478 + 312702 + 66726 + 132576 -- = 558482 vs fin st 566959

select a.amount, b.line, b.col, d.gm_account, d.gl_account, c.department, c.sub_department,
  sum(amount) over (partition by sub_department),
  sum(amount) over ()
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201809
  and b.page = 16
  and b.line between 21 and 34
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
  and c.store = 'RY1'  
join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key  

so, main shop is 9k low in doc vs fs * sept

select *, abs(x.amount) - abs(y.amount)
from (
select a.amount, b.line, b.col, d.gm_account, d.gl_account, c.department, c.sub_department,
  sum(amount) over (partition by sub_department),
  sum(amount) over ()
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201809
  and b.page = 16
  and b.line between 21 and 34
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
  and c.store = 'RY1'  
  and c.sub_department = 'mechanical'
join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key) x
full outer join (
select z.*, sum(amount) over ()
from (
select account, sum(amount) as amount
from sept 
where page = 16
  and line between 21 and 34
  and store = 'ry1'
  and department = 'service'
group by account) z) y on x.gl_account = y.account
order by gl_account

select * from sept limit 10

accounts missing in doc:
146000D: 6834  MAIN SHOP LABOR DISCOUNTS
146900: -37589 C/S ROAD HAZARD
146901: -2438  SLS - ROAD HAZARD
166900: 23526 AFTERMARKET GROSS
166901: 1191 AFTERMARKET GROSS

select  6834 -37589 -2438 + 23526 + 1191

select * from fin.dim_account where account in ('146000d','146900','146901','166900','166901')

----------------------------------------------------------------------------------------
-- 10/19 all of which leads me to the conclusion that the doc is of no use, fs is good enuf
----------------------------------------------------------------------------------------

select * from fp.fixed_est_gross_from_cashier_status limit 100

select * from oct limit 100


select the_Date, control, account, journal_code
from (
select * from sept
union all
select * from oct) a
group by the_Date, control, account, journal_code
having count(*) > 1

select line, col, account from fp.fixed_gross_details where department = 'parts' and store = 'ry1' group by line,col,account order by line
  
create table fp.fixed_gross_details (
  year_month integer not null,
  the_date date not null,
  store citext not null,
  control citext not null,
  department citext not null,
  page integer not null,
  line integer not null,
  col integer not null,
  account citext not null,
  journal_code citext not null,
  amount numeric(8,2) not null,
  primary key (the_date,control,account,journal_code));  
  
insert into fp.fixed_gross_details
select _year_month, _the_date, d.store, a.control, d.department,
  d.page, d.line, c.col, d.account, e.journal_code,
  sum(-a.amount) as amount
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = _year_month
join fin.dim_account c on a.account_key = c.account_key
join fp.gl_accounts d on c.account = d.account
  and d.page = 16 and d.line between 21 and 61
join fin.dim_journal e on a.journal_key  = e.journal_key    
where a.post_status = 'Y'
group by b.year_month, b.the_date, a.control, d.store, d.department, d.page, d.line, d.col,
  d.account, e.journal_code;   

select *
from (
select department, sum(amount)
from fp.fixed_gross_details
where line not in (54,55)
  and store = 'RY1'
group by department) a 
cross join (
select * 
from fp.fixed_est_gross_from_cashier_status
where the_date = '10/19/2018') b

select 
  sum(amount) filter (where line between 21 and 33 and department in('service','quick lane','detail','car wash')) as all_service,
  sum(amount) filter (where line between 21 and 33 and department = 'service') as service,
  sum(amount) filter (where line between 21 and 33 and department = 'quick lane') as pdq,
  sum(amount) filter (where line between 21 and 33 and department = 'detail') as detail,
  sum(amount) filter (where line between 21 and 33 and department = 'car wash') as carwash,
  sum(amount) filter (where line between 35 and 43 and department = 'body shop') as bodyshop,
  sum(amount) filter (where line between 45 and 59 and department = 'parts') as parts
from sept
where store = 'RY1'

select * from fp.fixed_est_gross_from_cashier_status

with wd as (
  select *
  from fp.get_working_days (_the_date, 'sales'))
sum(buick) as buick_count, (sum(buick) * (select month_factor from wd))::integer as buick_pace

select line, sum(amount)
from sept
where line between 45 and 61
  and store = 'RY1'
group by line  

select department from fp.fixed_gross_details group by department
select * from fp.fixed_gross_details limit 100

-- service gross
update fp.flightplan x
set currently = (
  select a.amount + (
    select service
    from fp.fixed_est_gross_from_cashier_status
    where the_date = current_date - 2)
  from (  
    select sum(amount)::integer as amount
    from fp.fixed_gross_details
    where store = 'RY1'
      and department = 'service'
      and line between 21 and 33
      and year_month = 201810) a)
where seq = 23
  and the_date = _the_date; 

select distinct department from dds.working_days
   
-- service net, need parts split

select account, sum(amount)
from sept a
inner join fin.parts_split b on a.account = b.gl_account
where store = 'RY1'
group by a.account 

-- fucking finally, this is parts split amounts
select service, pdq, (bs_gm + bs_non_gm) as bs, (service + pdq + bs_gm + bs_non_gm) as parts
from (
  select 
    (sum(amount * .5) filter 
      (where account in ('146700', '166700', '146800', '166800','146700D')))::integer as service,
    (sum(amount * .5) filter (where account in ('147800','167800')))::integer as pdq,
    (sum(amount * .5) filter (where account in ('147700','167700')))::integer as bs_gm,
    (sum(amount) filter (where account in ('147701','167701')))::integer as bs_non_gm
  from sept
  where store = 'RY1') a

select service, pdq, (bs_gm + bs_non_gm) as bs, -(service + pdq + bs_gm + bs_non_gm) as parts
from (
  select 
    coalesce((sum(amount * .5) filter 
      (where account in ('146700', '166700', '146800', '166800','146700D')))::integer, 0) as service,
    coalesce((sum(amount * .5) filter (where account in ('147800','167800')))::integer, 0) as pdq,
    coalesce((sum(amount * .5) filter (where account in ('147700','167700')))::integer, 0) as bs_gm,
    coalesce((sum(amount) filter (where account in ('147701','167701')))::integer, 0) as bs_non_gm
  from fp.fixed_gross_details
  where store = 'RY1'
    and year_month = 201811) a

drop table if exists fp.parts_split;
create table fp.parts_split (
  year_month integer primary key,
  store citext not null,
  service integer not null,
  pdq integer not null,
  bs integer not null,
  parts integer not null);

insert into fp.parts_split
select 201810, 'RY1',
  service, pdq, (bs_gm + bs_non_gm) as bs, -(service + pdq + bs_gm + bs_non_gm) as parts
from (
  select 
    coalesce((sum(amount * .5) filter 
      (where account in ('146700', '166700', '146800', '166800','146700D')))::integer, 0) as service,
    coalesce((sum(amount * .5) filter (where account in ('147800','167800')))::integer, 0) as pdq,
    coalesce((sum(amount * .5) filter (where account in ('147700','167700')))::integer, 0) as bs_gm,
    coalesce((sum(amount) filter (where account in ('147701','167701')))::integer, 0) as bs_non_gm
  from fp.fixed_gross_details
  where store = 'RY1'
    and year_month =201810) a;

    

select 
  sum(currently) filter (where seq in (23, 24, 25, 26) and the_Date = current_date - 2)
  +
  (select service from fp.parts_split where year_month = 201810)
from fp.flightplan
       
--------------------------------------------------------------------
-- parts split

dont forget that i did this to handle P6
update fp.gl_accounts
set department = 'Parts',
    page = 16,
    line = 49,
    col = 2
where account in ('147701','167701');    
---------------------------------------------------------------------
select a.*, b.department, c.*
from fin.parts_split a
join fin.dim_Account b on a.gl_Account = b.account
  and b.store_code = 'ry1'
join fp.gl_accounts c on a.gl_Account = c.account  

pdq: 478s 678s

service: 146700

'467S','667S', '468S','668S'/ 146700, 166700, 146800, 166800 then e.sub_department = 'mechanical'
'478s','678s' / 147800, 167800 then e.sub_department = 'pdq'
'477s','677s' / 147701, 167701, 147700, 167700 then e.department = 'body shop'
'146700D' then e.sub_department = 'mechanical'

all P4L59  

147700: gm parts 50/50  
147701: non gm parts all bs

---------------------------------------------------------------------
-- parts split
---------------------------------------------------------------------

select * from fp.parts_split

select * from fp.fixed_est_gross_from_cashier_status limit 100

select distinct department from fp.fixed_gross_details limit 100

select distinct department from dds.working_days limit 100


select a.*, sum(amount) over (partition by line)
from (
select line, account, sum(amount) as amount from fp.fixed_gross_details where department = 'quick lane'
and store = 'ry1'
group by line,  account) a
order by line

select 35721 - 43561 - 13833 +827
