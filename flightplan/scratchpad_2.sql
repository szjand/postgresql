﻿select * from fp.sales_gross


427

select *
from fin.dim_account
where account in ('164700','164701','164702','165100','165101','165102')


select *
from fin.dim_account
where description like '%recon%'
  and account_type = 'cogs'
  and store_code = 
order by account


select * 
from sls.months
order by year_month

-- 3 previous months
select previous_year_month
from sls.months
where year_month = (
  select previous_year_month
  from sls.months
  where year_month = (
    select previous_year_month
    from sls.months
    where year_month = 201810)) 



select * from fp.fixed_expenses_details    


  select sum(amount)::integer 
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201810
  join fin.dim_account c on a.account_key = c.account_key
  join fp.gl_accounts d on c.account = d.account
    and d.page in (3,4)
    and d.line between 4 and 57
    and d.col = 1
  join fin.dim_journal e on a.journal_key  = e.journal_key    
  where a.post_status = 'Y'
    and d.store = 'RY2'

-- 
-- insert into fp.fixed_expenses_details
select store, year_month, page, line, sum(amount)
from (
  select b.year_month, d.store, d.department, d.page, d.line, 
    d.account, e.journal_code, a.control, sum(-a.amount)::integer as amount
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201807 and 201809
  join fin.dim_account c on a.account_key = c.account_key
  join fp.gl_accounts d on c.account = d.account
    and d.page between 3 and 4
    and d.line between 4 and 57
  join fin.dim_journal e on a.journal_key  = e.journal_key    
  where a.post_status = 'Y'
  group by b.year_month, a.control, d.store, d.department, d.page, d.line, 
    d.account, e.journal_code) x
group by store, year_month, page, line
order by store, year_month, page, line


select year_month, store, page, sum(amount)
from (
  select b.year_month, d.store, d.department, d.page, d.line, 
    d.account, e.journal_code, a.control, sum(-a.amount)::integer as amount
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201807 and 201809
  join fin.dim_account c on a.account_key = c.account_key
  join fp.gl_accounts d on c.account = d.account
    and d.page between 3 and 4
    and d.line between 4 and 57
  join fin.dim_journal e on a.journal_key  = e.journal_key    
  where a.post_status = 'Y'
  group by b.year_month, a.control, d.store, d.department, d.page, d.line, 
    d.account, e.journal_code) x
group by year_month, store, page    

drop table if exists wtf;
create temp table wtf as
  select b.year_month, d.store, d.department, d.page, d.line, 
    d.account, e.journal_code, a.control, sum(-a.amount)::integer as amount
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201807 and 201809
  join fin.dim_account c on a.account_key = c.account_key
  join fp.gl_accounts d on c.account = d.account
    and d.page between 3 and 4
    and d.line between 4 and 57
  join fin.dim_journal e on a.journal_key  = e.journal_key    
  where a.post_status = 'Y'
  group by b.year_month, a.control, d.store, d.department, d.page, d.line, 
    d.account, e.journal_code

select count(*) from wtf limit 100

select year_month, control, account, journal_code
from wtf
group by year_month, control, account, journal_code
having count(*) > 1


select * from wtf where year_month = 201808 and control = '1110660' and account = '12304H'

select max(amount) from wtf

-- need columns to separate new and used
drop table if exists fp.expense_details_history cascade;
create table fp.expense_details_history (
  year_month integer not null,
  store citext not null,
  department citext not null,
  page integer not null,
  line integer not null, 
  col integer not null,
  account citext not null,
  journal citext not null,
  control citext not null,
  amount numeric(9,2) not null,
  primary key(year_month,control, account,journal));

insert into fp.expense_details_history (year_month,store,department,
  page,line,col,account,journal,control,amount)
select b.year_month, d.store, d.department, d.page, d.line, d.col,
  d.account, e.journal_code, a.control, sum(-a.amount) as amount
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month between (
    select previous_year_month
    from sls.months
    where year_month = (
      select previous_year_month
      from sls.months
      where year_month = (
        select previous_year_month
        from sls.months
        where year_month = 201811)))    
    and (
          select previous_year_month
          from sls.months
          where year_month = 201811)   
join fin.dim_account c on a.account_key = c.account_key
join fp.gl_accounts d on c.account = d.account
  and d.page between 3 and 4
  and d.line between 4 and 57
join fin.dim_journal e on a.journal_key  = e.journal_key    
where a.post_status = 'Y'
group by b.year_month, a.control, d.store, d.department, d.page, d.line, d.col,
  d.account, e.journal_code;


select max(year_month) from fp.expense_details_history

select count(*) from fp.expense_details_history

delete from fp.expense_details_history where year_month = 201809;

select (sum(amount)/3)::integer
from fp.expense_details_history
where store = 'RY1'
  and page = 3 
  and line between 8 and 16



  and year_month between (
    select previous_year_month
    from sls.months
    where year_month = (
      select previous_year_month
      from sls.months
      where year_month = (
        select previous_year_month
        from sls.months
        where year_month = _year_month)))    
    and (
          select previous_year_month
          from sls.months
          where year_month = _year_month)  

do
$$
declare 
  the_range int4range := '[3, 7)';
begin
  drop table if exists wtf;
  create temp table wtf as
  select 7 <@ the_range;
end
$$;
select * from wtf;

-- construct a range
select int4range((
  select previous_year_month
  from sls.months
  where year_month = (
    select previous_year_month
    from sls.months
    where year_month = (
      select previous_year_month
      from sls.months
      where year_month = 201810))), 201810);    

do
$$
declare
  prev_3_months int4range := (-- construct a range 
    select int4range((
      select previous_year_month
      from sls.months
      where year_month = (
        select previous_year_month
        from sls.months
        where year_month = (
          select previous_year_month
          from sls.months
          where year_month = 201810))), 201810));      

begin 
  drop table if exists wtf;
  create temp table wtf as
  with
    all_months as (
      select generate_series (201801, 201812, 1) as the_months)
    select *
    from all_months a
    where a.the_months <@ prev_3_months;
end
$$;
select * from wtf;    

drop table if exists wtf;
create temp table wtf as
select int4range((
  select previous_year_month
  from sls.months
  where year_month = (
    select previous_year_month
    from sls.months
    where year_month = (
      select previous_year_month
      from sls.months
      where year_month = 201810))), 201810) as prev

select * 
from wtf      

select distinct year_month
from dds.dim_Date
where year_month <@ (select prev from wtf)

select sum(distinct a.wd_in_month)
from dds.working_days a
join dds.dim_date b on a.the_date = b.the_date
  and b.year_month between 201807 and 201809
where a.department = 'service'  

need to generate a personnel exp per day over the previous 3 months

-- total working days in previous 3 months
select sum(distinct a.wd_in_month)
from dds.working_days a
join dds.dim_date b on a.the_date = b.the_date
  and b.year_month between 201807 and 201809
where a.department = 'service'  


select ( -- working days in current month
  select wd_in_month 
    from dds.working_days 
    where the_date = current_date
      and department = 'service') 
* 
(
select  ( -- prev 3 month personnel exp per working day
  (-- personnel exp prev 3 months
    select sum(-amount)
    from fp.expense_details_history
    where store = 'RY1'
      and page = 3 
      and line between 8 and 16
      and year_month between 201807 and 201809)
  /
  (-- total working days in previous 3 months
    select sum(distinct a.wd_in_month)
    from dds.working_days a
    join dds.dim_date b on a.the_date = b.the_date
      and b.year_month between 201807 and 201809
    where a.department = 'service'))::integer)

select * from dds.working_days where the_date = current_date and department = 'service'

select 23/19.0  -- 1.21

  select sum(-amount)  -- 144309
  from fp.fixed_expenses_details
  where store = 'RY1'
    and department = 'service'
    and line between 8 and 16
    and year_month = 201810

select 144309 * 1.21  -- 174613

select * from fp.fixed_expenses_details limit 100

select store, department from fp.fixed_expenses_details group by store, department order by store, department

select store, department from fp.expense_details_history group by store, department order by store, department
           
-- actually, i don't like this approach, comparing _year_month -1 to max year_month in fp.expense_details_history
-- until the month is closed, there are entries made for the open month
-- due to the fact that pacing is now based on previous 3 month avg, the whole script
-- should not be run until the previous month is closed
-- maybe just skip the pacing parts until the month is closed
-- either way, looks like i need to be able to ascertain the DMS open month (GLPOPYR)

if
  (select previous_year_month from sls.months where year_month = _year_month)
  <>
  (select max(year_month) from fp.expense_details_history) then 
    insert into fp.expense_details_history (year_month,store,department,
      page,line,account,journal,control,amount)
    select b.year_month, d.store, d.department, d.page, d.line, 
      d.account, e.journal_code, a.control, sum(-a.amount) as amount
    from fin.fact_gl a
    join dds.dim_date b on a.date_key = b.date_key
      and b.year_month = (
        select previous_year_month
        from sls.months
        where year_month = _year_month)
    join fin.dim_account c on a.account_key = c.account_key
    join fp.gl_accounts d on c.account = d.account
      and d.page between 3 and 4
      and d.line between 4 and 57
    join fin.dim_journal e on a.journal_key  = e.journal_key    
    where a.post_status = 'Y'
    group by b.year_month, a.control, d.store, d.department, d.page, d.line, 
      d.account, e.journal_code;
end if; 






-- this is what i have decided to go with for used inventory, both stores
select sum(amount)
from (    
  select control, sum(a.amount):: integer as amount 
  from fin.fact_gl a
  join fin.dim_Account b on a.account_key = b.account_key
    and b.store_code = 'RY1'
    and b.department_Code = 'uc'
  join fin.dim_fs_account c on b.account = c.gl_account
    and c.gm_account in ('240','241')    
  where a.post_status = 'Y'
  group by control
  having sum(amount) > 0) x


select * from fp.fixed_est_gross_from_cashier_status  

alter table fp.fixed_est_gross_from_cashier_status  
add column store citext;
update fp.fixed_est_gross_from_cashier_status  
set store = 'RY1';
alter table fp.fixed_est_gross_from_cashier_status alter column store set not null;

select * from fp.fixed_Gross_Details

select * from fin.parts_split where gl_Account like '2%' and gm_account in ('478s','678s')

select * from fp.parts_split


select * 
from fp.fixed_gross_details a
join fin.parts_split b on a.account = b.gl_account
  and b.gl_account like '2%'
order by gm_account


  select a.amount --292488
    + (
        select service
        from fp.fixed_est_gross_from_cashier_status
        where the_date = current_date - 2)
    + (
        select service 
        from fp.parts_split 
        where year_month = 201810
          and store = 'RY1')
  from (  
    select sum(amount)::integer as amount
    from fp.fixed_gross_details
    where store = 'RY1'
      and department = 'service'
      and line between 21 and 33
      and year_month = 201810) a  


select *
from dds.working_days
limit 100      


select department, 1.0 * wd_in_month/wd_of_month_elapsed
from dds.working_days
where the_date = '10/25/2018'

select *
from fp.get_working_Days('10/25/2018','service')

select max(the_date) from fp.flightplan


select * 
from fp.fixed_est_Gross_from_cashier_status

alter table fp.fixed_est_Gross_from_cashier_status 
drop constraint fixed_est_gross_from_cashier_status_pkey;

alter table fp.fixed_est_Gross_from_cashier_status 
add primary key (store, the_date);

alter table fp.parts_split 
drop constraint parts_split_pkey;

alter table fp.parts_split 
add primary key (store, year_month);






-- 10/27 after first run (with honda) there are some issues

GM parts fixed pace is null 1355



select *
from fp.expense_details_history
where store = 'RY1' 
  and department = 'quick lane'
  and line between 41 and 54


do
$$
declare
  _the_date date := current_date - 2;
  _year_month integer := (
    select year_month
    from dds.dim_date
    where the_date = _the_date);  
  _first_of_month date := (
    select first_of_month
    from sls.months
    where year_month = _year_month);  
  _prev_3_months int4range := (
    select int4range((
      select previous_year_month
      from sls.months
      where year_month = (
        select previous_year_month
        from sls.months
        where year_month = (
          select previous_year_month
          from sls.months
          where year_month = _year_month))), _year_month));             
begin
drop table if exists wtf;
create temp table wtf as
  select sum(-amount)
  from fp.fixed_expenses_details
  where store = 'RY1'
    and department = 'parts'
    and line between 8 and 16
    and year_month = _year_month;
end
$$;
select* from wtf;    
    



select line, sum(amount) 
from fp.fixed_expenses_details
where store = 'RY2'
  and department = 'service'
  and line between 8 and 16
  and year_month = 201810
group by line

select sum(amount) 
from fp.fixed_expenses_details
where store = 'RY2'
  and department = 'service'
  and line between 8 and 16
  and year_month = 201810


select * from fp.flightplan where seq = 32 order by the_Date


delete from fp.flightplan where the_date = '10/28/2018';


select *
from fp.parts_split


select *
from fp.fixed_Gross_Details
limit 100

select account, sum(amount)::integer as amount
from fp.fixed_gross_details
where store = 'RY2'
  and department = 'parts'
  and line in (45,46,47,48,49,50,51,52,53,57,58,59)
group by account
order by account




select sum(amount)::integer as amount
from fp.fixed_gross_details
where store = 'RY2'
  and department = 'parts'
  and line in (45,46,47,48,49,50,51,52,53,57,58,59)
group by account
order by account

-- gm sales pace semi fixed

select year_month, sum(amount)
from fp.expense_details_history
where page = 3
  and store = 'RY1'
and line between 18 and 39
group by year_month



select year_month, sum(amount)
-- select *
from fp.expense_details_history
where page = 3
  and store = 'RY1'
and line = 34
group by year_month

