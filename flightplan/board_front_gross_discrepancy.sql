﻿select *
from fp.gl_accounts
where store = 'ry2'
  and department = 'new vehicle'
  and page in (7,14)

drop table if exists acct;
create temp table acct as
select control, sum(-a.amount)::integer as amount
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201809
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account in (
    select account
    from fp.gl_accounts
    where store = 'ry2'
--       and department = 'new vehicle'
      and page in (7,14))
group by a.control  


drop table if exists board;
create temp table board as
  select aa.control, (sum(-aa.amount))::integer as the_sum,
    coalesce((sum(-aa.amount) filter (where c.the_page < 17))::integer, 0) as front_gross,
    coalesce((sum(-aa.amount) filter (where c.the_page = 17))::integer, 0) as back_gross
    from fin.fact_gl aa
    inner join fin.dim_account b on aa.account_key = b.account_key
    inner join (
      select distinct case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code,
      a.fxmpge as the_page, a.fxmlne as line, trim(b.g_l_acct_number) as gl_account
      from arkona.ext_eisglobal_sypffxmst a
      inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
        and a.fxmcyy = b.factory_financial_year
      where a.fxmcyy = 2018
        and coalesce(b. consolidation_grp, '1') <> '3'
        and b.g_l_acct_number <> ''
        and (
          (a.fxmpge between 5 and 15) or
          (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
          (a.fxmpge = 17 and a.fxmlne between 1 and 19))) c on b.account = c.gl_account
    where aa.post_status = 'Y' 
    and c.store_code = 'RY2'
    and c.the_page in (7,14)
    and aa.control in (
      select b.stock_number
      from board.sales_board b  
      where boarded_date between '09/01/2018' and '09/30/2018'
      and is_deleted = false )
    group by aa.control

select sum(front_gross) from board

select *
from board a
full outer join acct b on a.control = b.control
where a.control is null

select sum(amount) from acct


select *
from board a
full outer join acct b on a.control = b.control
where coalesce(a.front_gross, 0) <> coalesce(b.amount, 0)



select *
from fp.flightplan
where the_date = '09/29/2018'


select *
from fp.sales_counts
order by the_Date desc 

select *
from fp.sales_gross
order by the_date desc 


select page, sum(amount)
from fp.acct_sales_Gross_Details
where store = 'ry2'
  and page in (7,14)
group by page  

drop table if exists doc;
create temp table doc as 
select stock_number, sum(amount)
from fp.acct_sales_Gross_Details
where store = 'ry2'
  and page in (7,14)
group by stock_number  


select *
from board a
full outer join doc b on a.control = b.stock_number

/*
sent the doc vs board to jeri, she discovered that the vast majority of differences
were board included 240 for pulse and daily doc did not
so now the question is, what is the accounting differences between board and doc (fp)
*/

-- board accounts, includes 244502
      select distinct  trim(b.g_l_acct_number) as gl_account
      from arkona.ext_eisglobal_sypffxmst a
      inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
        and a.fxmcyy = b.factory_financial_year
      where a.fxmcyy = 2018
        and coalesce(b. consolidation_grp, '1') <> '3'
        and b.g_l_acct_number <> ''
        and (
          (a.fxmpge between 5 and 15) or
          (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
          (a.fxmpge = 17 and a.fxmlne between 1 and 19))
and b.consolidation_grp = '2'
and a.fxmpge in (7,14)          


-- fp, also includes 244502
select *
from fp.gl_accounts
where store = 'RY2'
  and page in (7,14)
order by account  

-- hmmm, both have 143 accounts


-- 10/02
the difference is that fp does not include accessories, new car pages limited to 
lines 1-19 & 25-40, board does include accessories
-- board query
  select aa.control, (sum(-aa.amount))::integer as the_sum,
    coalesce((sum(-aa.amount) filter (where c.the_page < 17))::integer, 0) as front_gross,
    coalesce((sum(-aa.amount) filter (where c.the_page = 17))::integer, 0) as back_gross
select b.account, aa.*, c.*  
    from fin.fact_gl aa
    inner join fin.dim_account b on aa.account_key = b.account_key
    inner join (
      select distinct case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code,
      a.fxmpge as the_page, a.fxmlne as line, trim(b.g_l_acct_number) as gl_account
      from arkona.ext_eisglobal_sypffxmst a
      inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
        and a.fxmcyy = b.factory_financial_year
      where a.fxmcyy = 2018
        and coalesce(b. consolidation_grp, '1') <> '3'
        and b.g_l_acct_number <> ''
        and (
          (a.fxmpge between 5 and 15) or
          (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
          (a.fxmpge = 17 and a.fxmlne between 1 and 19))) c on b.account = c.gl_account
    where aa.post_status = 'Y' and aa.control = 'H10896'
--     and aa.control in (
--       select distinct  b.stock_number
--       from board.sales_board b  
--       where boarded_date between date_trunc('MONTH',coalesce(_date,current_date))::DATE and coalesce(_date,current_date)
--       and is_deleted = false
--         )
    group by aa.control

select *
from fp.acct_sales_Gross_Details
where stock_number = 'H10896'

select b.year_month, b.the_date, a.control, d.store, d.page, d.line, 
  d.account, e.journal_code, sum(-a.amount)::integer as amount
select a.*, d.*  
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
--   and b.year_month = (select year_month from sls.months where open_closed = 'open')
join fin.dim_account c on a.account_key = c.account_key
join fp.gl_accounts d on c.account = d.account
  and (
    (d.page in (5,7,8,9,10,14) and (d.line between 1 and 19 or d.line between 25 and 40))  or
    (d.page = 16 and d.line between 1 and 6) or
    (d.page = 17 and d.line between 1 and 19))
join fin.dim_journal e on a.journal_key  = e.journal_key    
where a.control = 'H10896'



-- 11/01
/*
jeri needs the same thing for RY1 for October
and it is not working out
the issue is comparing the new car front gross for october from the gross board: -43,110
with what accounting shows for october

so here is the afton function called when the number under the gross is clicked and pops up a modal with per unit detail
(modified slightly to return a real dataset, not fucking json)
*/

-- Function: board.get_gross_board_stats(integer, citext, citext, citext, date)
-- DROP FUNCTION board.get_gross_board_stats(integer, citext, citext, citext, date);
-- CREATE OR REPLACE FUNCTION board.get_gross_board_stats(
--     _store integer,
--     _n_u_type citext,
--     _est_or_actual citext,
--     _front_fi_total citext,
--     _date date)
--   RETURNS SETOF json AS
-- $BODY$
do
$$
declare
--     _store integer := 39; -- ry1
    _store integer := 40; -- ry2
    _n_u_type citext := 'N';
    _est_or_actual citext := 'actual';
    _front_fi_total citext := 'Front';
    _date date := '11/30/2018';
begin   

drop table if exists wtf;
create temp table wtf as 

with accounting_ids as (
  select aa.control, (sum(-aa.amount))::integer as the_sum,
    coalesce((sum(-aa.amount) filter (where c.the_page < 17))::integer, 0) as front_gross,
    coalesce((sum(-aa.amount) filter (where c.the_page = 17))::integer, 0) as back_gross
    from fin.fact_gl aa
    inner join fin.dim_account b on aa.account_key = b.account_key
    inner join (
      select distinct case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code,
      a.fxmpge as the_page, a.fxmlne as line, trim(b.g_l_acct_number) as gl_account
      from arkona.ext_eisglobal_sypffxmst a
      inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
        and a.fxmcyy = b.factory_financial_year
      where a.fxmcyy = 2018
        and coalesce(b. consolidation_grp, '1') <> '3'
        and b.g_l_acct_number <> ''
        and (
          (a.fxmpge between 5 and 15) or
          (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
          (a.fxmpge = 17 and a.fxmlne between 1 and 19))) c on b.account = c.gl_account
    where aa.post_status = 'Y' 
    and aa.control in (
      select distinct  b.stock_number
      from board.sales_board b  
      where boarded_date between date_trunc('MONTH',coalesce(_date,current_date))::DATE and coalesce(_date,current_date)
      and is_deleted = false
        )
    group by aa.control) 
select case when board_sub_type = 'N/A' then -1 else 1 end as counter,  stock_number, boarded_date, 
  case when board_sub_type = 'N/A' then board_type else board_sub_type end as board, 
  case 
    when _est_or_actual = 'est' and _front_fi_total = 'front' and front_gross is null and board_type <> 'Back-on' and is_backed_on = false then deal_front 
    when _est_or_actual = 'est' and _front_fi_total = 'fi' and front_gross is null and board_type <> 'Back-on' and is_backed_on = false then deal_fi
    when _est_or_actual = 'est' and _front_fi_total = 'total' and front_gross is null and board_type <> 'Back-on' and is_backed_on = false then deal_fi + deal_front
    when _est_or_actual = 'actual' and _front_fi_total = 'front' then front_gross
    when _est_or_actual = 'actual' and _front_fi_total = 'fi' then back_gross
    when _est_or_actual = 'actual' and _front_fi_total = 'total' then front_gross + back_gross
  end as value
from board.sales_board b
left join accounting_ids a on stock_number = a.control
inner join board.board_types c on c.board_type_key = b.board_type_key  and( board_sub_type in ('Retail','Fleet') or board_type = 'Back-on')
inner join (
  select board_id, vehicle_type,sale_code
  from board.daily_board
  group by  board_id, vehicle_type,sale_code) dd on b.board_id = dd.board_id
left join (
  select board_id ,amount as deal_fi
  from board.deal_gross d
  where amount_type = 'fi_gross' 
  and current_row = true) d on b.board_id = d.board_id 
left join (
  select board_id ,amount as deal_front
  from board.deal_gross d
  where amount_type = 'front_end_gross'
  and current_row = true ) e on b.board_id = e.board_id 
where boarded_date between date_trunc('MONTH',coalesce(_date,current_date))::DATE and coalesce(_date,current_date)
 and is_deleted = false
 and case when _n_u_type = 'both' then 1 = 1 else vehicle_type = _n_u_type end
 and store_key = _store;
end
$$;

select * from wtf;

select sum(value) from wtf

then for the "doc" piece (assuming fp.acct_sales_gross_details from daily flightplan is current)

drop table if exists doc;
create temp table doc as 
select stock_number, sum(amount) as doc_gross
from fp.acct_sales_gross_details 
where year_month = 201811
  and store = 'ry2'
  and page < 16
group by stock_number  

and the resulting spreadsheet for jeri

select *--, sum(coalesce(board_gross, 0)) over (), sum(coalesce(doc_gross, 0)) over (), abs(coalesce(doc_gross, 0)) - abs(coalesce(board_gross, 0)) as diff
-- select sum(board_gross), sum(doc_gross)
from (
select stock_number, board, sum(value) as board_gross
from wtf
group by stock_number, board) a
full outer join doc b on a.stock_number = b.stock_number


-- 11/9 
G35405 Buick Fleet deal, shows up in board, not in doc

need some spec-ing from jeri, currently, doc is from flightplan, retail only
select *
from sls.exT_bopmast_partial
where stock_number = 'g35405'

select *
from fin.fact_gl a
join fin.dim_Account b on a.account_key = b.account_key
where a.control = 'G35405'


G35222
  board: 1709
  doc:   1490

select * from fp.acct_sales_gross_details where stock_number = 'G35222'
select 63861 - 62371 : 1490

-- the difference is that the board is adding the gross from pulse (p14 $219)
select * from arkona.xfm_inpmast where inpmast_stock_number = 'g35222'

-- this is what generates gross for the board

  select aa.control, (sum(-aa.amount))::integer as the_sum,
    coalesce((sum(-aa.amount) filter (where c.the_page < 17))::integer, 0) as front_gross,
    coalesce((sum(-aa.amount) filter (where c.the_page = 17))::integer, 0) as back_gross
    from fin.fact_gl aa
    inner join fin.dim_account b on aa.account_key = b.account_key
    inner join (
      select distinct case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code,
      a.fxmpge as the_page, a.fxmlne as line, trim(b.g_l_acct_number) as gl_account
      from arkona.ext_eisglobal_sypffxmst a
      inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
        and a.fxmcyy = b.factory_financial_year
      where a.fxmcyy = 2018
        and coalesce(b. consolidation_grp, '1') <> '3'
        and b.g_l_acct_number <> ''
        and (
          (a.fxmpge between 5 and 15) or
          (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
          (a.fxmpge = 17 and a.fxmlne between 1 and 19))) c on b.account = c.gl_account
    where aa.post_status = 'Y' 
    and aa.control = 'G35222'
group by aa.control    

-- ungroup it get some detail, add journal
select c.the_page, c.line, b.account, aa.amount, b.description, bb.journal_code
  from fin.fact_gl aa
  inner join fin.dim_account b on aa.account_key = b.account_key
  Inner join fin.dim_journal bb on aa.journal_key = bb.journal_key  
  inner join (
    select distinct case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code,
    a.fxmpge as the_page, a.fxmlne as line, trim(b.g_l_acct_number) as gl_account
    from arkona.ext_eisglobal_sypffxmst a
    inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
      and a.fxmcyy = b.factory_financial_year
    where a.fxmcyy = 2018
      and coalesce(b. consolidation_grp, '1') <> '3'
      and b.g_l_acct_number <> ''
      and (
        (a.fxmpge between 5 and 15) or
        (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
        (a.fxmpge = 17 and a.fxmlne between 1 and 19))) c on b.account = c.gl_account
  where aa.post_status = 'Y' 
  and aa.control = 'G35222'
  