﻿/*
Amounts posted to these accounts from journals other than VSN or VSU 
should identify recon posted after deal is closed.  
It may also be helpful to identify entries going to the 
cost of sale accounts after the vehicle deal is posted (journals SVI, potentially POT, PCA)

GM Store:	
164700,164701,164702,165100,165101,165102	

HGF	
264700,264701,264702,264710,264720,265100	
265101,265102,265110,265120	
*/
select b.the_date, a.*, d.journal_code, c.account, c.description, e.description
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201808
join fin.dim_account c on a.account_key = c.account_key  
  and c.account in ('164700','164701','164702','165100','165101','165102',
    '264700','264701','264702','264710','264720','265100',	
    '265101','265102','265110','265120')
join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code not in ('VSN','VSU')
join fin.dim_gl_description e on a.gl_Description_key = e.gl_description_key  
where a.post_status = 'Y'