﻿-- inventory accounts

select *
from (
  select a.control, sum(a.amount)
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  inner join (
    SELECT distinct gl_account
    FROM fin.dim_fs_account
    where gm_account like '23%') c on b.account = c.gl_account
  where a.post_status = 'Y'
    and left(a.control, 2) not in ('HG','RG')
  group by a.control
  having sum(a.amount) > 1000) d
left join arkona.xfm_inpmast e on d.control = e.inpmast_stock_number
  and e.current_row = true



-- trying to match jeri's RYDELL NEW INVENTORY DOC in executrac


select c.account, c.description, sum(c.amount)::integer, count(*)
from (
  select b.account, b.description, a.control, sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  inner join (
    select account
    from fin.dim_account
    where account_type = 'asset'
      and department_code = 'nc'
      and typical_balance = 'debit') c on b.account = c.account
  where a.post_status = 'Y'
  group by b.account, b.description, a.control
  having sum(a.amount) > 0) c
group by c.account, c.description


select c.account, c.description, sum(c.amount)::integer, count(*)
from (
  select b.account, b.description, a.control, sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
    and b.account_type = 'asset'
    and b.department_code = 'nc'
    and b.typical_balance = 'debit'
    and b.current_row
    and b.account <> '126104'
  where a.post_status = 'Y'
  group by b.account, b.description, a.control
  having sum(a.amount) > 0) c
group by c.account, c.description

create index on fin.dim_account(account_type);
create index on fin.dim_account(department_code);
create index on fin.dim_account(typical_balance);
create index on fin.dim_account(description);

explain (format json)
  select b.account, b.description, a.control, sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
    and b.account_type = 'asset'
    and b.department_code = 'nc'
    and b.typical_balance = 'debit'
    and b.current_row
    and b.account <> '126104'
  where a.post_status = 'Y'
  group by b.account, b.description, a.control
  having sum(a.amount) > 0


create index on fin.fact_gl(amount)

explain analyze
  select a.control
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
    and b.account_type = 'asset'
    and b.department_code = 'nc'
    and b.typical_balance = 'debit'
    and b.current_row
    and b.account <> '126104'
  where a.post_status = 'Y'
  group by a.control
  having sum(a.amount) > 0


 select *
 from fin.dim_account b
 where b.account_type = 'asset'
    and b.department_code = 'nc'
    and b.typical_balance = 'debit'
    and b.current_row
    and b.account <> '126104'

explain analyze
select a.control, d.the_date  --2.7s
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
  and b.account_type = 'asset'
  and b.department_code = 'nc'
  and b.typical_balance = 'debit'
  and b.current_row
  and b.account <> '126104'
inner join (  
  select inpmast_stock_number
  from arkona.xfm_inpmast
  where current_row
    and status = 'I' 
    and type_n_u = 'N') c on a.control = c.inpmast_stock_number
inner join dds.dim_date d on a.date_key = d.date_key    

select a.control, d.the_date  --2.6s
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
  and b.account_type = 'asset'
  and b.department_code = 'nc'
  and b.typical_balance = 'debit'
  and b.current_row
  and b.account <> '126104'
inner join dds.dim_date d on a.date_key = d.date_key  
where exists ( 
  select inpmast_stock_number
  from arkona.xfm_inpmast
  where current_row
    and status = 'I' 
    and type_n_u = 'N'
    and inpmast_stock_number = a.control)


select a.control, d.the_date  --2.6s
from fin.fact_gl a
inner join dds.dim_date d on a.date_key = d.date_key  
where exists ( 
  select inpmast_stock_number
  from arkona.xfm_inpmast
  where current_row
    and status = 'I' 
    and type_n_u = 'N'
    and inpmast_stock_number = a.control)
and exists (
  select account_key
  from fin.dim_account
  where account_type = 'asset'
    and department_code = 'nc'
    and typical_balance = 'debit'
    and current_row
    and account <> '126104' 
    and account_key = a.account_key)   



-- 9/24

explain analyze
  select a.control
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
    and b.account_type = 'asset'
    and b.department_code = 'nc'
    and b.typical_balance = 'debit'
    and b.current_row
    and b.account <> '126104'
  where a.post_status = 'Y'
  group by a.control
  having sum(a.amount) > 0  

explain ANALYZE 
select *
from fin.fact_gl 
where post_status = 'Y'
  and account_key in (
    select account_key
    from fin.dim_account 
    where account_type = 'asset'
      and department_code = 'nc'
      and typical_balance = 'debit'
      and current_row
      and account <> '126104')
      

explain ANALYZE 
select *
from fin.fact_gl a
inner join (
    select account_key
    from fin.dim_account 
    where account_type = 'asset'
      and department_code = 'nc'
      and typical_balance = 'debit'
      and current_row
      and account <> '126104') b on a.account_key = b.account_key
where post_status = 'Y'

how do i narrow down the fact table 


explain ANALYZE 
select a.*
from fin.fact_gl a
inner join dds.dim_date aa on a.date_key = aa.date_key
  and aa.the_date between current_date - 750 and current_date  
inner join fin.dim_account b on a.account_key = b.account_key
    and b.account_type = 'asset'
      and  b.department_code = 'nc'
      and b.typical_balance = 'debit'
      and b.current_row
      and b.account <> '126104'
inner join fin.dim_journal c on a.journal_key = c.journal_key
  and c.journal_code = 'AFM'
  or c.journal_code = 'CRC'    
where a.post_status = 'Y'
  and a.amount > 500


-- 10/02 this has branched
1. inventory accounts
2. current inventory in terms of balance in inventory accounts

-- this should be all the new vehicle inventory accounts
select *
from fin.dim_account b 
where b.account_type = 'asset'
  and b.department_code = 'nc'
  and b.typical_balance = 'debit'
  and b.current_row
  and b.account <> '126104'
order by account  



-- here are all the 261 (factory receivable) accounts, the only one with dept = NC is 126104
select *
from fin.dim_account
where account like '1261%'
  or account like '2261%'
order by account


-- why does this query take 2.6 seconds
-- down to 1.3 sec with date and having
-- 575 rows
select a.control, min(d.the_date) as the_date, sum(a.amount)  --2.6s
from fin.fact_gl a
inner join dds.dim_date d on a.date_key = d.date_key  
and d.the_date between current_Date - 900 and current_date--'06/22/2018'
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account in ( -- all new vehicle inventory account
    select b.account
    from fin.dim_account b 
    where b.account_type = 'asset'
      and b.department_code = 'nc'
      and b.typical_balance = 'debit'
      and b.current_row
      and b.account <> '126104')  
where a.post_status = 'Y'
-- where exists ( 
--   select inpmast_stock_number
--   from arkona.xfm_inpmast
--   where current_row
--     and status = 'I' 
--     and type_n_u = 'N'
--     and inpmast_stock_number = a.control)
-- and exists (
--   select account_key
--   from fin.dim_account
--   where account_type = 'asset'
--     and department_code = 'nc'
--     and typical_balance = 'debit'
--     and current_row
--     and account <> '126104' 
--     and account_key = a.account_key)   
group by a.control 
having sum(a.amount) > 10000
order by the_date
-- and this one takes 434 msec
-- 582 rows
drop table if exists acct;
create temp table acct as
select a.control, max(b.the_date) as the_date, sum(a.amount) as amount
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_date between current_Date - 900 and current_date--'06/22/2018'
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account in ( -- all new vehicle inventory account
    select b.account
    from fin.dim_account b 
    where b.account_type = 'asset'
      and b.department_code = 'nc'
      and b.typical_balance = 'debit'
      and b.current_row
      and b.account <> '126104')    
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
group by a.control
having sum(a.amount) > 10000
order by control

  select a.control
  from fin.fact_gl a
inner join dds.dim_date bb on a.date_key = bb.date_key
  and bb.the_date between current_Date - 900 and current_date
  inner join fin.dim_account b on a.account_key = b.account_key
    and b.account_type = 'asset'
    and b.department_code = 'nc'
    and b.typical_balance = 'debit'
    and b.current_row
    and b.account <> '126104'
  where a.post_status = 'Y'
  group by a.control
having sum(a.amount) > 10000  




  
select * 
from fin.fact_gl a
inner join fin.dim_Account b on a.account_key = b.account_key
--   and b.account = '223100'
where a.post_Status = 'Y'
  and a.control = 'H11810'

select * from acct order by amount

  
-- 586
drop table if exists inpmast;
create temp table inpmast as
select inpmast_stock_number
from arkona.xfm_inpmast
where status = 'I'
  and type_n_u = 'N'
  and current_row


select *
from inpmast a
full outer join acct b on a.inpmast_Stock_number = b.control
where a.inpmast_stock_number is null
 or b.control is null

select *
from nc.vehicle_sales

select *
from fin.fact_gl a
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account in ( -- all new vehicle inventory account
    select b.account
    from fin.dim_account b 
    where b.account_type = 'asset'
      and b.department_code = 'nc'
      and b.typical_balance = 'debit'
      and b.current_row
      and b.account <> '126104')    
where a.post_status = 'Y' 
  and a.control = 'G34463' 


-- 10/4 this is driving me nuts
-- for now i have to conclude that the complex join is less efficient than the where clause
-- 571 msec
explain analyze
select a.control, max(b.the_date) as the_date, sum(a.amount) as amount
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_date between current_Date - 900 and current_date--'06/22/2018'
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account in ( -- all new vehicle inventory account
    select b.account
    from fin.dim_account b 
    where b.account_type = 'asset'
      and b.department_code = 'nc'
      and b.typical_balance = 'debit'
      and b.current_row
      and b.account <> '126104')    
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
group by a.control
having sum(a.amount) > 10000

--9 fucking seconds
select a.control, max(b.the_date) as the_date, sum(a.amount) as amount
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_date between current_Date - 900 and current_date--'06/22/2018'
inner join (
    select b.account_key
    from fin.dim_account b 
    where b.account_type = 'asset'
      and b.department_code = 'nc'
      and b.typical_balance = 'debit'
      and b.current_row
      and b.account <> '126104') c on a.account_key = c.account_key   
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
group by a.control
having sum(a.amount) > 10000

-- wtf 571 msec
-- without the date, 1.5 sec
select a.control-- , sum(a.amount) as amount
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_date between current_Date - 900 and current_date--'06/22/2018'
inner join fin.dim_account c on a.account_key = c.account_key 
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and c.account in ( -- all new vehicle inventory account
    select b.account
    from fin.dim_account b 
    where b.account_type = 'asset'
      and b.department_code = 'nc'
      and b.typical_balance = 'debit'
      and b.current_row
      and b.account <> '126104')   
--   and b.the_date between current_Date - 600 and current_date -- this has no effect
group by a.control
having sum(a.amount) > 10000

-- 9.5 sec
explain analyze
  select a.control
  from fin.fact_gl a
inner join dds.dim_date bb on a.date_key = bb.date_key
  and bb.the_date between current_Date - 900 and current_date
  inner join fin.dim_account b on a.account_key = b.account_key
    and b.account_type = 'asset'
    and b.department_code = 'nc'
    and b.typical_balance = 'debit'
    and b.current_row
    and b.account <> '126104'
  where a.post_status = 'Y'
  group by a.control
having sum(a.amount) > 10000    


-- 10/07 
-- 611 rows 8.8 sec
-- explain analyze
select a.control
from fin.fact_gl a
inner join nc.inventory_accounts b on a.account_key = b.account_key
inner join dds.dim_date bb on a.date_key = bb.date_key
  and bb.the_date between current_Date - 900 and current_date
inner join arkona.xfm_inpmast c on a.control = c.inpmast_stock_number  
where a.post_status = 'Y'  
group by a.control
having sum(a.amount) > 10000


-- wtf 571 msec
-- without the date, 1.5 sec
select a.control-- , sum(a.amount) as amount
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_date between current_Date - 900 and current_date--'06/22/2018'
inner join fin.dim_account c on a.account_key = c.account_key 
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and c.account in ( -- all new vehicle inventory account
    select b.account
    from fin.dim_account b 
    where b.account_type = 'asset'
      and b.department_code = 'nc'
      and b.typical_balance = 'debit'
      and b.current_row
      and b.account <> '126104')   
--   and b.the_date between current_Date - 600 and current_date -- this has no effect
group by a.control
having sum(a.amount) > 10000


ok, even though i do not understand the performance characteristics, 
this is the query for current inventory from accounting
eg, a "significant" balance in the inventory account
goes back 2 1/2 years
-- 560 msec
-- explain analyze
select a.control
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_date between current_Date - 900 and current_date--'06/22/2018'
inner join fin.dim_account c on a.account_key = c.account_key 
inner join nc.inventory_accounts cc on c.account = cc.account
where a.post_status = 'Y'
group by a.control
having sum(a.amount) > 10000


-- test for new inventory account  
select *
from (
  select b.account
  from fin.dim_account b 
  where b.account_type = 'asset'
    and b.department_code = 'nc'
    and b.typical_balance = 'debit'
    and b.current_row
    and b.account <> '126104') c
where not exists (
  select 1
  from nc.inventory_accounts
  where account = c.account)  




select *
from (
  select a.control
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_date between current_Date - 900 and current_date--'06/22/2018'
  inner join fin.dim_account c on a.account_key = c.account_key 
  inner join nc.inventory_accounts cc on c.account = cc.account
  where a.post_status = 'Y'
  group by a.control
  having sum(a.amount) > 10000) c
left join (
  select *
  from (
    select stock_number, vehicle, tags, lookup_date, row_number() over (partition by stock_number order by lookup_date desc)
    from vauto.mike_corp_report) a
  where row_number = 1) d on c.control = d.stock_number


-- 10/17/18 flightplan inventory numbers
-- comparing to jeri new inventory executrak

select c.store_code, a.control, c.account, c.description, sum(amount),
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_date between current_Date - 900 and current_date--'06/22/2018'
inner join fin.dim_account c on a.account_key = c.account_key 
inner join nc.inventory_accounts cc on c.account = cc.account
where a.post_status = 'Y'
group by c.store_code, a.control, c.account, c.description
having sum(a.amount) > 10000
order by account

-- the amounts are good, the counts aren't, which makes no sense
select x.*, sum(amount) over (partition by account), count(control) over (partition by account)
select sum(amount)
from (
  select c.store_code, c.account, a.control, c.description, sum(amount) as amount  
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_date between current_Date - 900 and current_date--'06/22/2018'
  inner join fin.dim_account c on a.account_key = c.account_key 
  inner join nc.inventory_accounts cc on c.account = cc.account
  where a.post_status = 'Y'
    and c.store_code = 'RY1'
  group by c.store_code, c.account, a.control, c.description
  having sum(a.amount) > 10000) x
order by account, control


-- lets start with chevy cars, 47 vs my 50
-- exept when i print out from dt, there are 50, so the count on the executrak is goofy
-- and since i match the amounts i believe i am ok
select *
from (
  select c.store_code, c.account, a.control, c.description, sum(amount) as amount  
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_date between current_Date - 900 and current_date--'06/22/2018'
  inner join fin.dim_account c on a.account_key = c.account_key 
  inner join nc.inventory_accounts cc on c.account = cc.account
  where a.post_status = 'Y'
    and c.store_code = 'RY1'
    and c.account = '123100'
  group by c.store_code, c.account, a.control, c.description
  having sum(a.amount) > 10000) x
order by amount, control  




-- used cars YUCK
select account, sum(amount)
from (
select c.store_code, c.account, a.control, c.description, sum(amount) as amount  
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_date between current_Date - 900 and current_date--'06/22/2018'
inner join fin.dim_account c on a.account_key = c.account_key 
  and c.account in ('124000','124001','124100','124101')
where a.post_status = 'Y'
  and c.store_code = 'RY1'
group by c.store_code, c.account, a.control, c.description
having sum(a.amount) > 100) x
group by account

-- probably close enough for now
-- but it is a trade off relying on inpmast
select c.account, c.description, sum(amount) as amount  
from fin.fact_gl a
join (
  select inpmast_stock_number
  from arkona.xfm_inpmast
  where current_row
    and status = 'I'
    and type_n_u = 'U'
    and inventory_account in ('124000','124001','124100','124101')) b on a.control = b.inpmast_stock_number
inner join fin.dim_account c on a.account_key = c.account_key 
  and c.account in ('124000','124001','124100','124101')
where a.post_status = 'Y'
  and c.store_code = 'RY1'  
group by c.account, c.description  


-- so, this will do for now for flightplan
-- new
select sum(amount)::integer
from (
  select c.store_code, c.account, a.control, c.description, sum(amount) as amount  
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_date between current_Date - 900 and current_date--'06/22/2018'
  inner join fin.dim_account c on a.account_key = c.account_key 
  inner join nc.inventory_accounts cc on c.account = cc.account
  where a.post_status = 'Y'
    and c.store_code = 'RY1'
  group by c.store_code, c.account, a.control, c.description
  having sum(a.amount) > 10000) x


                                                                                                                                                             
-- used
select sum(amount)::integer as amount  
from fin.fact_gl a
join (
  select inpmast_stock_number
  from arkona.xfm_inpmast
  where current_row
    and status = 'I'
    and type_n_u = 'U'
    and inventory_account in ('124000','124001','124100','124101')) b on a.control = b.inpmast_stock_number
inner join fin.dim_account c on a.account_key = c.account_key 
  and c.account in ('124000','124001','124100','124101')
where a.post_status = 'Y'
  and c.store_code = 'RY1'  






