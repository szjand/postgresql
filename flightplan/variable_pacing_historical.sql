﻿----------------------------------------------------------------------------------------------------------
--< Historical
-- 8/27 include current month
----------------------------------------------------------------------------------------------------------
-- count based on transactions to sale accounts
drop table if exists step_2;
create temp table step_2 as
-- include stocknumber on each row
select year_month, the_date, store, page, line, line_label, control, sum(unit_count) as unit_count, sum(amount) as amount
from (
  select b.year_month, b.the_date, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201401 and (select previous_year_month from sls.months where open_closed = 'open')
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select b.year_month, f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month between 201401 and (select previous_year_month from sls.months where open_closed = 'open')
      and (
        (b.page in (5,7,8,9,10,14) and (b.line between 1 and 19 or b.line between 25 and 40)) 
        or
        (b.page = 16 and b.line between 1 and 6)) -- used cars
--         or
--         (b.page = 17 and b.line between 1 and 20))-- f/i
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account and b.year_month = d.year_month
  where a.post_status = 'Y') h
group by year_month, the_date, store, page, line, line_label, control;

-- actual sales for the month, 
-- in reality what this gives me is monthly totals that match the financial statement
drop table if exists step_3; --21996
create temp table step_3 as
select a.year_month, a.the_date, a.store, a.page, a.line::integer, a.line_label, a.control, sum(unit_count) as unit_count,
  case
    when store = 'ry1' and page = 5 then 'Chevrolet'
    when store = 'ry1' and page = 8 then 'Buick'
    when store = 'ry1' and page = 9 then 'Cadillac'
    when store = 'ry1' and page = 10 then 'GMC'
    when store = 'ry1' and page = 14 then 'MV-1'
    when page = 16 and line in (1,4) then 'Certified'
    when page = 16 and line in (2,5) then 'Other'
    when store = 'ry2' and page = 7 then 'Honda'
    when store = 'ry2' and page = 14 then 'Nissan'
  end as make,
  case 
    when page < 16 and line < 20 then 'Car'
    when page < 16 and line > 20 then 'Truck'
    when page = 16 and line < 3 then 'Car'
    when page = 16 and line > 3 then 'Truck'
  end as shape,
  extract(day from the_date)::integer as day_of_month, extract(doy from the_date)::integer as day_of_year
from step_2 a
inner join (
  select year_month, control
  from step_2
  group by year_month, control
  -- includes out of month unwinds
  having sum(unit_count) <> 0) b on a.control = b.control and a.year_month = b.year_month
group by a.year_month, a.the_date, a.store, a.page, a.line, a.line_label, a.control;

-- gross include journal in gross (GLI: sfe, WTD: writedown, gje: pack etc)
drop table if exists step_4;
create temp table step_4 as
select b.the_date, a.control, -a.amount as amount, d.*, e.journal_code, f.description
-- select d.store, line, sum(amount) 
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join (
  select b.year_month, f.store, d.gl_account, b.page, b.line::integer
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month between 201401 and (select previous_year_month from sls.months where open_closed = 'open')
    and (
      (b.page in (5,7,8,9,10,14) and (b.line between 1 and 19 or b.line between 25 and 40)) 
      or
      (b.page = 16 and b.line between 1 and 6))
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on b.year_month = d.year_month
    and c.account = d.gl_account
inner join fin.dim_journal e on a.journal_key  = e.journal_key    
left join fin.dim_gl_description f on a.gl_description_key = f.gl_description_key
  and b.the_date between f.row_from_date and f.row_thru_date
where a.post_status = 'Y'; 

-- car / truck /used totals: count & gross
select *
from (
select a.year_month, a.store, a.page, a.new_car_ct, b.new_car_gr,
  a.new_truck_ct, b.new_truck_gr, a.used_ct, b.used_gr
from ( -- count
  select year_month, store, page, 
    sum(unit_count) filter (where page < 16 and line between 1 and 19) as new_car_ct,
    sum(unit_count) filter (where page < 16 and line between 25 and 40) as new_truck_ct,
    sum(unit_count) filter (where page = 16) as used_ct 
  from step_3  
  group by year_month, store, page) a
join ( -- gross
  select year_month, store, page, 
    (sum(gross) filter (where page < 16 and line between 1 and 19))::integer as new_car_gr,
    (sum(gross) filter (where page < 16 and line between 25 and 40))::integer as new_truck_gr,
    (sum(gross) filter (where page = 16))::integer as used_gr
  from step_4 
  group by year_month, store, page) b on a.store = b.store and a.year_month = b.year_month and a.page = b.page
order by a.year_month, a.store, a.page) x
where right(year_month::text, 2) = '01'

-- monthly quarters
select aa.year_month, aa.quarter, aa.store, aa.sales, bb.gross, (bb.gross/aa.sales)::integer as pvr
from (
  select year_month, quarter, store, sum(unit_count) as sales
  from (
    select a.*,
      case 
        when day_of_month between 1 and 7 then 1
        when day_of_month between 8 and 14 then 2
        when day_of_month between 15 and 21 then 3
        when day_of_month between 22 and 31 then 4
      end as quarter
    from step_3 a
    where page < 16) b
  group by year_month, quarter, store
  order by store, year_month, quarter) aa
  left join (
  select year_month, quarter, store, sum(amount)::integer as gross
  from (
    select a.*,
      case 
        when extract(day from the_date)::integer between 1 and 7 then 1
        when extract(day from the_date)::integer between 8 and 14 then 2
        when extract(day from the_date)::integer between 15 and 21 then 3
        when extract(day from the_date)::integer between 22 and 31 then 4
      end as quarter
    from step_4 a
    where page < 16) b
  group by year_month, quarter, store
  order by store, year_month, quarter) bb on aa.year_month = bb.year_month 
    and aa.quarter = bb.quarter 
    and aa.store = bb.store


-- monthly at page (make)level
-- for ben/greg/jeri to peruse
-- /docs/variable_pacing_history_V1.xlsx
drop table if exists pacing;
create temp table pacing as
select a.year_month, b.quarter, b.store, b.make, coalesce(d.sales, 0) as sales,
  coalesce(f.gross, 0) as gross,
  coalesce(f.writedown, 0) as writedown, 
  coalesce(f.pack, 0) as pack,
  coalesce(f.sfe, 0) as sfe
from ( -- year_month
  select distinct year_month
  from dds.dim_date
  where year_month between 201401 and 201807) a 
join ( -- store, make, quartewr
  select distinct store, 
    case 
      when page < 16 then make
      else  'Used'
    end as make, 
     case 
      when extract(day from the_date)::integer between 1 and 7 then 1
      when extract(day from the_date)::integer between 8 and 14 then 2
      when extract(day from the_date)::integer between 15 and 21 then 3
      when extract(day from the_date)::integer between 22 and 31 then 4
    end as quarter
  from step_3) b on 1 =1 
left join ( -- sales
  select year_month, store, make, quarter, sum(unit_count) as sales
  from (
    select year_month, store, unit_count,
      case 
        when page < 16 then make
        else  'Used'
      end as make,
      case 
        when extract(day from the_date)::integer between 1 and 7 then 1
        when extract(day from the_date)::integer between 8 and 14 then 2
        when extract(day from the_date)::integer between 15 and 21 then 3
        when extract(day from the_date)::integer between 22 and 31 then 4
      end as quarter
    from step_3) c
  group by year_month, store, make, quarter) d on a.year_month = d.year_month
    and b.store = d.store
    and b.quarter = d.quarter
    and b.make = d.make
left join (-- gross
  select year_month, quarter, store, make, 
    (sum(amount) filter (where journal_code not in ('GLI','GJE','WTD')))::integer as gross,
    (sum(amount) filter (where journal_code = 'WTD'))::integer as writedown,
    (sum(amount) filter (where journal_code = 'GLI'))::integer as SFE,
    (sum(amount) filter (where journal_code = 'GJE'))::integer as pack
  from (
    select year_month, store, 
      case 
        when store = 'ry1' and page = 5 then 'Chevrolet'
        when store = 'ry1' and page = 8 then 'Buick'
        when store = 'ry1' and page = 9 then 'Cadillac'
        when store = 'ry1' and page = 10 then 'GMC'
        when store = 'ry2' and page = 7 then 'Honda'
        when store = 'ry2' and page = 14 then 'Nissan'
        when page = 16 then  'Used'
      end as make, journal_code, amount, 
      case 
        when extract(day from the_date)::integer between 1 and 7 then 1
        when extract(day from the_date)::integer between 8 and 14 then 2
        when extract(day from the_date)::integer between 15 and 21 then 3
        when extract(day from the_date)::integer between 22 and 31 then 4
      end as quarter
    from step_4 a) e
  group by store, year_month, quarter, store, make) f on a.year_month = f.year_month
    and b.store = f.store
    and b.quarter = f.quarter
    and b.make = f.make  
order by b.store, a.year_month, b.make, b.quarter

select * from pacing




select *
from pacing a
where year_month = 201807
  
select sum(sales) as sales, sum(gross) as gross, sum(writedown) as wd, 
  sum(pack) as pack, sum(sfe) as sfe,
  sum(gross + writedown + pack + sfe)
from pacing a
where year_month = 201807
  and make = 'GMC'

select *
from step_4
where year_month = 201807
  and page = 10

select a.*, case b.last_day_of_month when true then 'YES' else 'NO' end
from step_4 a
left join dds.dim_date b on a.the_date = b.the_date
where a.year_month > 201712
--   and a.page = 10
  and a.journal_code in ('GJE', 'GLI')
order by b.last_day_of_month, a.the_date

select line, journal_code, sum(amount)
from step_4
where year_month = 201807
  and page = 10
group by line, journal_code
order by line  
    
select page, line, sum(amount)
from step_4
where year_month = 201807
  and page = 10
group by page, line
order by line  

select *
from step_4
where journal_code = 'gje'
  and description is not null

-- the vast majority of GJE, GLI occur on the last day of the month, eg accounting adjustments, not operational gross
-- at least that's how i am seeing it

-- /docs/variable_pacing_history_V2.xlsx
-- add percent of monthly total, exclude sfe
-- excluding sfe, should i also exclude ad pack? and i think i want to display them anyway
-- gross excludes GLI & GJE, includes writedowns, eg, gross excludes all month end adjustments: pack adj, ad pack, sfe, etc

select a.year_month, a.store, a.make, a.q1_sales, a.q2_sales, a.q3_sales, a.q4_sales, 
  a.sales as total_sales, b.gross, 
  case a.sales
    when 0 then 0
    else (round(b.gross/a.sales, 3))::integer 
  end as pvr, b.gje, b.gli
from ( -- sales
  select year_month, store, make, sum(sales) as sales,
    case sum(sales) filter (where quarter = 1) when 0 then 0 else round(sum(sales) filter (where quarter = 1)/sum(sales), 3) end as q1_sales,
    case sum(sales) filter (where quarter = 2) when 0 then 0 else round(sum(sales) filter (where quarter = 2)/sum(sales), 3) end as q2_sales,
    case sum(sales) filter (where quarter = 3) when 0 then 0 else round(sum(sales) filter (where quarter = 3)/sum(sales), 3) end as q3_sales,
    case sum(sales) filter (where quarter = 4) when 0 then 0 else round(sum(sales) filter (where quarter = 4)/sum(sales), 3) end as q4_sales
  from pacing
  where make <> 'MV-1'
  group by year_month, store, make) a
left join (-- gross
  select year_month, store, make, sum(gross + writedown) as gross, sum(pack) as gje, sum(sfe) as gli
  from pacing
  where make <> 'MV-1'
  group by year_month, store, make) b on a.year_month = b.year_month
    and a.store = b.store 
    and a.make = b.make
order by a.store, a.make, a.year_month
---------------------------------------------------------------------------
--/> Historical
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
