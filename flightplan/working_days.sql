﻿Variable/PDQ: Monday - Saturday, 
Fixed: Monday - Friday
Car Wash: Monday - Sunday

select year_month, wd_in_month
select *
from dds.dim_date
where year_month = 201808
where the_year = 2018
group by year_month, wd_in_month
order by year_month

-- holidays, per Ben C, Variable should not have any holidays counted as working days
-- holidays: New Years Day, Memorial Day, 4th of July, Labor Day, Thanksgiving, Christmas
-- car wash only closed on New Years Day, Thanksgiving, Christmas
select *
from dds.dim_Date
where the_year = 2018
  and holiday
order by the_date  

fixed, both main shop and body shop have crews working on Saturdays
but it is not a full crew.

Generate some FTE clock hours for techs on saturdays

-- shit pymast only goes back to 04/15/2018
select min(row_from_date) from arkona.xfm_pymast


select *
from ads.ext_edw_employee_dim
limit 100

drop table if exists saturday_clock_hours;
create temp table saturday_clock_hours as
select b.year_month, a.store_code, c.pydept, a.employee_number, a.the_date, a.clock_hours, c.name, c.distcode
from arkona.xfm_pypclockin a
inner join dds.dim_date b on a.the_date = b.the_date
  and b.the_year in (2017, 2018)
  and b.day_of_week = 7
inner join ads.ext_edw_employee_dim c on a.employee_number = c.employeenumber  
  and a.the_date between c.employeekeyfromdate and c.employeekeythrudate
  and c.activecode <> 'T'
left join arkona.ext_pypclkctl d on c.pydeptcode = d.department_code
  and c.store = d.company_number
where a.clock_hours <> 0; 


select year_month, store_code, pydept as dept, sum(clock_hours) as clock_hours
from saturday_clock_hours
where pydept in ('parts','service recon','service team leaders','body shop','bdc service','service','service tech', 'misc service')
group by year_month, store_code, pydept
order by store_code, year_month



select year_month, store_code, 
  (sum(clock_hours) filter (where pydept like 'BDC%'))::integer as bdc,
  (sum(clock_hours) filter (where pydept = 'body shop'))::integer as body_shop,
  (sum(clock_hours) filter (where pydept like 'Service%' or pydept like 'Misc%'))::integer as service,
  (sum(clock_hours) filter (where pydept = 'parts'))::integer as parts
from saturday_clock_hours
where pydept in ('parts','service recon','service team leaders','body shop','bdc service','service','service tech', 'misc service')
group by year_month, store_code
order by store_code, year_month


-- non saturday clock hours
drop table if exists non_saturday_clock_hours;
create temp table non_saturday_clock_hours as
select b.year_month, a.store_code, c.pydept, a.employee_number, a.the_date, a.clock_hours, c.name, c.distcode
from arkona.xfm_pypclockin a
inner join dds.dim_date b on a.the_date = b.the_date
  and b.the_year in (2017, 2018)
  and b.day_of_week <> 7
inner join ads.ext_edw_employee_dim c on a.employee_number = c.employeenumber  
  and a.the_date between c.employeekeyfromdate and c.employeekeythrudate
  and c.activecode <> 'T'
left join arkona.ext_pypclkctl d on c.pydeptcode = d.department_code
  and c.store = d.company_number
where a.clock_hours <> 0; 


select year_month, store_code, 
  (sum(clock_hours) filter (where pydept like 'BDC%'))::integer as bdc,
  (sum(clock_hours) filter (where pydept = 'body shop'))::integer as body_shop,
  (sum(clock_hours) filter (where pydept like 'Service%' or pydept like 'Misc%'))::integer as service,
  (sum(clock_hours) filter (where pydept = 'parts'))::integer as parts
from non_saturday_clock_hours
where pydept in ('parts','service recon','service team leaders','body shop','bdc service','service','service tech', 'misc service')
group by year_month, store_code
order by store_code, year_month