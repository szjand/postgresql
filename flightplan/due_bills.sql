﻿select CAST(d.soldTS AS sql_date) AS sale_date, c.VehicleInventoryItemID,
  sum(c.totalPartsAmount + c.LaborAmount) AS recon_amount
-- SELECT COUNT(*) -- 346
FROM vehicleduebillauthorizations a
inner JOIN vehicleDueBillAuthorizedItems b on a.VehicleDuebillAuthorizationID = b.VehicleDuebillAuthorizationID
inner JOIN VehicleReconItems c on b.vehicleReconItemID = c.vehicleReconItemID
inner JOIN vehiclesales d on a.vehicleSaleID = d.VehicleSaleID
WHERE CAST(fromts AS sql_date) > '12/31/2017'
GROUP BY CAST(d.soldTS AS sql_date), c.VehicleInventoryItemID


select count(*) from ads.ext_vehicle_due_bill_authorized_items


select the_date, count(*)
from ops.ads_extract
where complete = true
and the_date between '09/01/2018' and current_date 
group by the_date


drop table if exists due_bills;
create temp table due_bills as
select d.soldTS::date AS sale_date, e.stocknumber,
  sum(c.totalPartsAmount + c.LaborAmount) AS recon_amount
-- SELECT COUNT(*) -- 346
FROM ads.ext_vehicle_due_bill_authorizations a
JOIN ads.ext_vehicle_Due_Bill_Authorized_Items b on a.VehicleDuebillAuthorizationID = b.VehicleDuebillAuthorizationID
JOIN ads.ext_Vehicle_Recon_Items c on b.vehicleReconItemID = c.vehicleReconItemID
JOIN ads.ext_vehicle_sales d on a.vehicleSaleID = d.VehicleSaleID
inner join ads.ext_vehicle_inventory_items e on d.VehicleInventoryItemID = e.VehicleInventoryItemID
WHERE a.fromts::date > '12/31/2017'
GROUP BY d.soldTS::date, e.stocknumber;

drop table if exists recon;
create temp table recon as
select b.the_date, 
  a.*, --a.control, a.amount, 
  d.journal_code, c.account, c.description as acct_desc, 
  e.description as trans_desc
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201808
join fin.dim_account c on a.account_key = c.account_key  
  and c.account in ('164700','164701','164702','165100','165101','165102',
    '264700','264701','264702','264710','264720','265100',	
    '265101','265102','265110','265120')
join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code not in ('VSN','VSU')
join fin.dim_gl_description e on a.gl_Description_key = e.gl_description_key
where a.post_status = 'Y';

select *
from due_bills a
left join (
  select control, journal_code, account, sum(amount)
  from recon
  group by control, journal_code, account
  having sum(amount) <> 0) b on a.stocknumber = b.control


select *
from recon 
where control = 'g34123a'

-- delivered 8/10
select * from sls.deals where stock_number = 'g34123a'

select *
from ads.ext_fact_repair_order a
inner join ads.ext_dim_vehicle b on a.vehiclekey = b.vehiclekey
  and b.vin = '3GCUKSEC0FG533027'

ROS
16320680 8/13 internal  


    