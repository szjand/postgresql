﻿/*
10/15/18
see if i can update fp.flightplan for a given date
based on ...flightplan/sql/nightly_data_generation.sql

select * from fp.flightplan where the_date = '10/10/2018'

in an attempt to deal with the beginning of the month issues

does it even make sense to do a flightplan at the end/beginning of the month
the primary issue that comes to mind is the accounting entries that are made at month
end prior to closing, those take place in the new month but are applied to the
closing (previous)month
so it seems that flight plan numbers on make sense thru the day before the end of the month

which is ok, and possible helps clarify the issues i was having with determining the month
all transactions are based on the date rather than the month

flightplan (yearly pacing) data for the last day of the month comes from the fin statement


10/16 this isn't making any sense to me now, the notion of some random date
this has to be done sequentially on a daily basis or it won't work
i still like setting the date variables at the beginning
with _the_date = current_date -1 

this will never be run on the 1st of the month, 
but on the second of the month _the_Date becomes 10/1 and wd_elapsed = 0 therefor divide by 0 error
so, don't run it until the 3rd of the month? sounds goofy, but may be the best solution
*/

do
$$
declare
  _the_date date := current_date - 1;
  _year_month integer := (
    select year_month
    from dds.dim_date
    where the_date = _the_date);  
  _first_of_month date := (
    select first_of_month
    from sls.months
    where year_month = _year_month);      
begin
-- base row
insert into fp.flightplan (seq,year_month,the_date,store,department,area) values
(1, _year_month, _the_date,'RY1','GM Sales','New Chevy'), 
(2, _year_month, _the_date,'RY1','GM Sales','New GMC'), 
(3, _year_month, _the_date,'RY1','GM Sales','New Buick'), 
(4, _year_month, _the_date,'RY1','GM Sales','New Cadillac'), 
(5, _year_month, _the_date,'RY1','GM Sales','Used'), 
(6, _year_month, _the_date,'RY1','GM Sales','New Gross'), 
(7, _year_month, _the_date,'RY1','GM Sales','New PVR'), 
(8, _year_month, _the_date,'RY1','GM Sales','Used Gross'), 
(9, _year_month, _the_date,'RY1','GM Sales','Used PVR'), 
(10, _year_month, _the_date,'RY1','GM Sales','F&I Gross before Comp/Chgbk'), 
(11, _year_month, _the_date,'RY1','GM Sales','F&I PVR'), 
(12, _year_month, _the_date,'RY1','GM Sales','F&I Comp'), 
(12.1, _year_month, _the_date,'RY1','GM Sales','F&I Chargebacks'), 
(13, _year_month, _the_date,'RY1','GM Sales','Sales Comp PVR'), 
(14, _year_month, _the_date,'RY1','GM Sales','Avg recon/unit sold'), 
(15, _year_month, _the_date,'RY1','GM Sales','New Policy'), 
(16, _year_month, _the_date,'RY1','GM Sales','Used Policy'), 
(17, _year_month, _the_date,'RY1','GM Sales','Delivery Expense'), 
(18, _year_month, _the_date,'RY1','GM Sales','Personnel'), 
(19, _year_month, _the_date,'RY1','GM Sales','Semi Fixed'), 
(19.1, _year_month, _the_date,'RY1','GM Sales','Fixed'), 
(20, _year_month, _the_date,'RY1','GM Sales','Net'), 
(21, _year_month, _the_date,'RY1','GM Sales','Inventory $ - New'), 
(22, _year_month, _the_date,'RY1','GM Sales','Inventory $ - Used'), 
(23, _year_month, _the_date,'RY1','GM Service','Gross'), 
(24, _year_month, _the_date,'RY1','GM Service','Personnel'), 
(25, _year_month, _the_date,'RY1','GM Service','Semi Fixed'), 
(25.1, _year_month, _the_date,'RY1','GM Service','Fixed'), 
(26, _year_month, _the_date,'RY1','GM Service','Net'),  
(27, _year_month, _the_date,'RY1','GM PDQ','Gross'), 
(28, _year_month, _the_date,'RY1','GM PDQ','Personnel'),
(29, _year_month, _the_date,'RY1','GM PDQ','Semi Fixed'),
(29.1, _year_month, _the_date,'RY1','GM PDQ','Fixed'),
(30, _year_month, _the_date,'RY1','GM PDQ','Net'),
(31, _year_month, _the_date,'RY1','GM Parts','Gross'),
(32, _year_month, _the_date,'RY1','GM Parts','Personnel'),
(33, _year_month, _the_date,'RY1','GM Parts','Semi Fixed'),
(33.1, _year_month, _the_date,'RY1','GM Parts','Fixed'),
(34, _year_month, _the_date,'RY1','GM Parts','Net'),
(35, _year_month, _the_date,'RY1','GM Parts','Factory $/SA'),
(36, _year_month, _the_date,'RY1','GM Parts','Inventory Adjustments'),
(37, _year_month, _the_date,'RY1','GM Parts','Overtime'),
(38, _year_month, _the_date,'RY1','GM Parts','Wholesale'),
(39, _year_month, _the_date,'RY1','Body Shop','Gross'),
(40, _year_month, _the_date,'RY1','Body Shop','Personnel'),
(41, _year_month, _the_date,'RY1','Body Shop','Semi Fixed'),
(41.1, _year_month, _the_date,'RY1','Body Shop','Fixed'),
(42, _year_month, _the_date,'RY1','Body Shop','Net'),
(43, _year_month, _the_date,'RY1','Detail','Gross'),
(44, _year_month, _the_date,'RY1','Detail','Personnel'),
(45, _year_month, _the_date,'RY1','Detail','Semi Fixed'),
(45.1, _year_month, _the_date,'RY1','Detail','Fixed'),
(46, _year_month, _the_date,'RY1','Detail','Net'),
(47, _year_month, _the_date,'RY1','Carwash','Gross'),
(48, _year_month, _the_date,'RY1','Carwash','Personnel'),
(49, _year_month, _the_date,'RY1','Carwash','Semi Fixed'),
(49.1, _year_month, _the_date,'RY1','Carwash','Fixed'),
(50, _year_month, _the_date,'RY1','Carwash','Net');

--------------------------------------------------------------------------
-- flightplan accounts
--------------------------------------------------------------------------
insert into fp.gl_accounts (store,department,page,line,col,account)
select distinct case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store,
  c.department, a.fxmpge as page, a.fxmlne::integer as line, a.fxmcol as col, 
  b.g_l_acct_number as gl_account
from arkona.ext_Eisglobal_sypffxmst a 
inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
join fin.dim_account c on b.g_l_acct_number = c.account
  and c.current_row = true    
where ( -- use routing from this and the prev year
  a.fxmcyy = (select the_year from dds.dim_date where the_date = current_date)
  or
  a.fxmcyy = (select the_year - 1 from dds.dim_date where the_date = current_date))
  and case when b.g_l_acct_number = '146424' then a.fxmlne <> 27 else 1 = 1 end
  and not exists (
    select 1
    from fp.gl_accounts
    where account = b.g_l_acct_number);
-- parts split issues, 2 parts, first is page 16, accounts 147701/167701
-- nees to route to parts line 49 to correctly generate gross
-- orig routed to body shop page 4 line 59
update fp.gl_accounts
set department = 'Parts',
    page = 16,
    line = 49,
    col = 2
where account in ('147701','167701');    
--------------------------------------------------------------------------
-- deals from accounting
/*

*/
--------------------------------------------------------------------------    
delete 
from fp.acct_sales_counts_details
where year_month = _year_month;

insert into fp.acct_sales_counts_details
select year_month, the_date, store::citext, page, line, control, sum(unit_count) as unit_count
from (
  select b.year_month, b.the_date, d.store, d.page, d.line, a.control, 
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = _year_month
  join fin.dim_account c on a.account_key = c.account_key
    and c.account_type_code = '4'
  join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  join fp.gl_accounts d on c.account = d.account
    and (
      (d.page in (5,7,8,9,10,14) and (d.line between 1 and 19 or d.line between 25 and 40))  or
      (d.page = 16 and d.line between 1 and 6))
  where a.post_status = 'Y') h
group by year_month, the_date, store, page, line, control;

--------------------------------------------------------------------------
-- deal counts from accounting
-- delete from fp.acct_sales_counts where year_month = 201810
--------------------------------------------------------------------------
insert into fp.acct_sales_counts
select _the_date, _year_month,
  coalesce(sum(unit_count) filter (where store = 'RY1' and page = 5), 0) as chev,
  coalesce(sum(unit_count) filter (where store = 'RY1' and page = 8), 0) as buick,
  coalesce(sum(unit_count) filter (where store = 'RY1' and page = 9), 0) as cad,
  coalesce(sum(unit_count) filter (where store = 'RY1' and page = 10), 0) as gmc,
  coalesce(sum(unit_count) filter (where store = 'RY1' and page <> 16), 0) as gm_new,
  coalesce(sum(unit_count) filter (where store = 'RY1' and page = 16), 0) as gm_used,
  coalesce(sum(unit_count) filter (where store = 'RY2' and page = 7), 0) as honda,
  coalesce(sum(unit_count) filter (where store = 'RY2' and page = 14), 0) as nissan,
  coalesce(sum(unit_count) filter (where store = 'RY2' and page <> 16), 0) as hn_new,
  coalesce(sum(unit_count) filter (where store = 'RY2' and page = 16), 0) as hn_used  
from fp.acct_sales_counts_details
where year_month = _year_month;

--------------------------------------------------------------------------
-- deals from sold board
-- select * from fp.boarded_deals_Details
--------------------------------------------------------------------------
delete 
from fp.boarded_deals_details
where year_month = _year_month;

insert into fp.boarded_deals_details
select year_month, boarded_date, store, stock_number, vehicle_type,
  vehicle_make, front_gross, fi_gross
from (
  select (select year_month from dds.dim_date where the_date = current_date),
    boarded_ts ::date as boarded_date,
    case
      when d.store like '%outlet' then 'outlet'::citext
      when d.store like '%Chev%' then 'RY1':: citext
      when d.store like '%Honda%' then 'RY2'::citext
    end as store, a.stock_number, 
    e.vehicle_type, e.vehicle_make,
    case
      when g.amount = 'NaN' then 0
      else coalesce(g.amount, 0)::integer 
    end as front_gross,
     case
      when h.amount = 'NaN' then 0
      else coalesce(h.amount, 0)::integer 
    end as fi_gross
  from board.sales_board a
  inner join board.board_types b on a.board_type_key = b.board_type_key
  inner join dds.dim_date c on a.boarded_ts::date = c.the_date
  inner join onedc.stores d on a.store_key = d.store_key
  inner join board.daily_board e on a.board_id = e.board_id
  left join board.deal_gross g on a.board_id = g.board_id
    and g.current_row = true
    and g.amount_type =  'front_end_gross'
  left join board.deal_gross h on a.board_id = h.board_id
    and h.current_row = true
    and h.amount_type = 'fi_gross'
  where not a.is_backed_on
    and  b.board_type in ('Deal')
    and b.board_sub_type in ('retail','lease')
    and c.year_month = _year_month
    and not a.is_deleted) x 
group by year_month, boarded_date, store, stock_number, vehicle_type,
  vehicle_make, front_gross, fi_gross;


--------------------------------------------------------------------------
-- deal counts from sold board of boarded deals not yet in accounting
--------------------------------------------------------------------------  
-- no delete, this is the accumulated total each day

insert into fp.boarded_deals_counts
select _the_date, _year_month,
  coalesce(count(stock_number) filter (where store = 'RY1' and vehicle_type = 'N' and vehicle_make = 'chevrolet'), 0) as chev,
  coalesce(count(stock_number) filter (where store = 'RY1' and vehicle_type = 'N' and vehicle_make = 'buick'), 0) as buick,
  coalesce(count(stock_number) filter (where store = 'RY1' and vehicle_type = 'N' and vehicle_make = 'cadillac'), 0) as cad,
  coalesce(count(stock_number) filter (where store = 'RY1' and vehicle_type = 'N' and vehicle_make = 'gmc'), 0) as gmc,
  coalesce(count(stock_number) filter (where store = 'RY1' and vehicle_type = 'N'), 0) as gm_new,
  coalesce(count(stock_number) filter (where store = 'RY1' and vehicle_type = 'U'), 0) as gm_used,
  coalesce(count(stock_number) filter (where store = 'RY2' and vehicle_type = 'N' and vehicle_make = 'honda'), 0) as honda,
  coalesce(count(stock_number) filter (where store = 'RY2' and vehicle_type = 'N' and vehicle_make = 'nissan'), 0) as nissan,
  coalesce(count(stock_number) filter (where store = 'RY2' and vehicle_type = 'N'), 0) as hn_new,
  coalesce(count(stock_number) filter (where store = 'RY2' and vehicle_type = 'U'), 0) as hn_used 
from fp.boarded_deals_details a
where year_month = _year_month
  AND not exists ( -- only boarded deals not in accounting
      select stock_number 
      from fp.acct_sales_counts_details
      where stock_number = a.stock_number
        and year_month = _year_month); 

--------------------------------------------------------------------------
-- update flightplan with deal counts
-- date_issue, ok with working days being the day after, year_month: sls.months vs dim_date
-- this will never be run on the 1st of the month, 
-- but on the second of the month _the_Date becomes 10/1 and wd_elapsed = 0 therefor divide by 0 error
-- so, don't run it until the 3rd of the month? sounds goofy, but may be the best solution
--------------------------------------------------------------------------        
insert into fp.sales_counts
with wd as (
  select *
  from fp.get_working_days (_the_date, 'sales'))
select _year_month, _the_date,
  sum(chev) as chev_count, (sum(chev) * (select month_factor from wd))::integer as chev_pace,
  sum(buick) as buick_count, (sum(buick) * (select month_factor from wd))::integer as buick_pace,
  sum(gmc) as gmc_count, (sum(gmc) * (select month_factor from wd))::integer as gmc_pace,
  sum(cad) as cad_count, (sum(cad) * (select month_factor from wd))::integer as cad_pace,
  sum(gm_used) as gm_used_count, (sum(gm_used) * (select month_factor from wd))::integer as gm_used_pace,
  sum(honda) as honda_count, (sum(honda) * (select month_factor from wd))::integer as honda_pace,
  sum(nissan) as nissan_count, (sum(nissan) * (select month_factor from wd))::integer as nissan_pace,
  sum(hn_used) as hn_used_count, (sum(hn_used) * (select month_factor from wd))::integer as hn_used_pace,
  sum(gm_new) as gm_new_count, sum(hn_new) as hn_new_count
from (    
  select *
  from fp.acct_sales_counts
  where the_date = _the_date
  union all
  select *
  from fp.boarded_deals_counts
  where the_date = _the_date) a;

update fp.flightplan
set currently = (select chev_count from fp.sales_counts where the_date = _the_date),
    pace = (select chev_pace from fp.sales_counts where the_date = _the_date)
where the_date = _the_date
  and seq = 1;   
update fp.flightplan  
set currently = (select gmc_count from fp.sales_counts where the_date = _the_date),
    pace = (select gmc_pace from fp.sales_counts where the_date = _the_date)
where the_date = _the_date
  and seq = 2;     
update fp.flightplan  
set currently = (select buick_count from fp.sales_counts where the_date = _the_date),
    pace = (select buick_pace from fp.sales_counts where the_date = _the_date)
where the_date = _the_date
  and seq = 3;   
update fp.flightplan  
set currently = (select cad_count from fp.sales_counts where the_date = _the_date),
    pace = (select cad_pace from fp.sales_counts where the_date = _the_date)
where the_date = _the_date
  and seq = 4; 
update fp.flightplan    
set currently = (select gm_used_count from fp.sales_counts where the_date = _the_date),
    pace = (select gm_used_pace from fp.sales_counts where the_date = _the_date)
where the_date = _the_date
  and seq = 5;      

--------------------------------------------------------------------------
-- sales gross details from accounting
--------------------------------------------------------------------------
delete 
from fp.acct_sales_gross_details
where year_month = _year_month;

insert into fp.acct_sales_gross_details
select b.year_month, b.the_date, a.control, d.store, d.page, d.line, 
  d.account, e.journal_code, sum(-a.amount)::integer as amount
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = _year_month
join fin.dim_account c on a.account_key = c.account_key
join fp.gl_accounts d on c.account = d.account
  and (
    (d.page in (5,7,8,9,10,14) and (d.line between 1 and 19 or d.line between 25 and 40))  or
    (d.page = 16 and d.line between 1 and 6) or
    (d.page = 17 and d.line between 1 and 19))
join fin.dim_journal e on a.journal_key  = e.journal_key    
where a.post_status = 'Y'
group by b.year_month, b.the_date, a.control, d.store, d.page, d.line, 
  d.account, e.journal_code;         

--------------------------------------------------------------------------
-- sales gross daily snapshot
--------------------------------------------------------------------------  
insert into fp.sales_gross
select (select year_month from sls.months where open_closed = 'open'), current_date - 1, 
  sum(gm_new_front_gross) as gm_new_front_gross, 
  sum(hn_new_front_gross) as hn_new_front_gross, 
  sum(gm_used_front_gross) as gm_used_front_gross, 
  sum(hn_used_front_gross) as hn_used_front_gross,
  sum(gm_fi_before) as gm_fi_before,
  sum(hn_fi_before) as hn_fi_before,
  sum(gm_fi_comp) as gm_fi_comp,
  sum(hn_fi_comp) as hn_fi_comp,
  sum(gm_fi_chgbk) as gm_fi_chgbk,
  sum(hn_fi_chgbk) as hn_fi_chgbk
from (
  select 
    sum(amount) filter (where page < 16 and store = 'RY1') as gm_new_front_gross,
    sum(amount) filter (where page < 16 and store = 'RY2') as hn_new_front_gross,
    sum(amount) filter (where page = 16 and store = 'RY1') as gm_used_front_gross,
    sum(amount) filter (where page = 16 and store = 'RY2') as hn_used_front_gross,
    sum(amount) filter (where page = 17 and store = 'RY1' and line in (1,2,5,6,7,11,12,15,16,17)) as gm_fi_before,
    sum(amount) filter (where page = 17 and store = 'RY2' and line in (1,2,5,6,7,11,12,15,16,17)) as hn_fi_before,
    sum(amount) filter (where page = 17 and store = 'RY1' and line in (9, 19)) as gm_fi_comp,
    sum(amount) filter (where page = 17 and store = 'RY2' and line in (9, 19)) as hn_fi_comp,
    sum(amount) filter (where page = 17 and store = 'RY1' and line in (3, 13)) as gm_fi_chgbk,
    sum(amount) filter (where page = 17 and store = 'RY2' and line in (3, 13)) as hn_fi_chgbk
  from fp.acct_sales_gross_details
  where year_month = _year_month
  union all
  select  
    coalesce(sum(front_gross) filter (where vehicle_type = 'N' and store = 'RY1'), 0) as gm_new_front_gross,
    coalesce(sum(front_gross) filter (where vehicle_type = 'N' and store = 'RY2'), 0) as hn_new_front_gross,
    coalesce(sum(front_gross) filter (where vehicle_type = 'U' and store = 'RY1'), 0) as gm_used_front_gross,
    coalesce(sum(front_gross) filter (where vehicle_type = 'U' and store = 'RY2'), 0) as hn_used_front_gross,
    coalesce(sum(fi_gross) filter (where store = 'RY1'), 0) as gm_fi_before,
    coalesce(sum(fi_gross) filter (where store = 'RY2'), 0) as hn_fi_before,
    0 as gm_fi_comp, 0 as hn_fi_comp, 0 as gm_fi_chgbk, 0 as hn_fi_chgbk       
  from fp.boarded_deals_details a
  where year_month = _year_month
    AND not exists (
        select stock_number 
        from fp.acct_sales_gross_details
        where stock_number = a.stock_number
          and year_month = (select year_month from sls.months where open_closed = 'open'))) c
group by _year_month, _the_date;


--------------------------------------------------------------------------
-- update flightplan with sales gross & pvr
--------------------------------------------------------------------------    
-- new gross
update fp.flightplan x
set currently = y.gm_new_front_gross,
    pace = y.gm_new_pace
from (
  select a.gm_new_front_gross, (a.gm_new_front_gross * b.month_factor)::integer as gm_new_pace
  from fp.sales_gross a
  join (select * from fp.get_working_days (_the_date, 'sales')) b on 1 = 1
  where a.the_date = _the_date) y
where x.seq = 6
  and x.the_date = _the_date;

-- used gross 
update fp.flightplan x
set currently = y.gm_used_front_gross,
    pace = y.gm_used_pace
from (
  select a.gm_used_front_gross, (a.gm_used_front_gross * b.month_factor)::integer as gm_used_pace
  from fp.sales_gross a
  join (select * from fp.get_working_days (_the_date, 'sales')) b on 1 = 1
  where a.the_date = _the_date) y
where x.seq = 8
  and x.the_date = _the_date; 

-- new pvr
update fp.flightplan x
set currently = y.gm_new_pvr,
    pace = y.gm_new_pvr
from (
  select 
    case
      when gm_new_count = 0 then 0 
      else gm_new_front_gross/gm_new_count
    end as gm_new_pvr
  from fp.sales_gross a  
  inner join fp.sales_counts b on a.the_date = b.the_date
  where a.the_date = _the_date) y
where x.seq = 7
  and x.the_date = _the_date;  

-- used pvr
update fp.flightplan x
set currently = y.gm_used_pvr,
    pace = y.gm_used_pvr
from (
  select
    case 
      when gm_used_count = 0 then 0
      else gm_used_front_gross/gm_used_count
    end as gm_used_pvr
  from fp.sales_gross a  
  inner join fp.sales_counts b on a.the_date = b.the_date
  where a.the_date = _the_date) y
where x.seq = 9
  and x.the_date = _the_date;    

-- f&i before
update fp.flightplan
set currently = (
  select gm_fi_before
  from fp.sales_gross
  where the_date = _the_date)
where seq = 10
  and the_date = _the_date;  

update fp.flightplan
set pace = (
  select a.gm_fi_before * b.month_factor
  from fp.sales_gross a
  join (select * from fp.get_working_days (_the_date, 'sales')) b on 1 = 1
  where a.the_date = _the_date) 
where seq = 10
  and the_date = _the_date; 

-- f&i pvr  
update fp.flightplan x
set currently = y.pvr,
    pace = y.pvr
from (    
  select a.gm_fi_before/(b.gm_new_count + b.gm_used_count) as pvr
  from fp.sales_gross a
  join fp.sales_counts b on a.the_date = b.the_date
  where a.the_date = _the_date) y
where seq = 11
  and the_date = _the_date;  

-- f&i comp
update fp.flightplan
set currently = (
  select gm_fi_comp
  from fp.sales_gross
  where the_date = _the_date)
where seq = 12
  and the_date = _the_date;  

-- f&i chargeback
update fp.flightplan
set currently = (
  select gm_fi_chgbk
  from fp.sales_gross
  where the_date = _the_date)
where seq = 12.1
  and the_date = _the_date;    

--------------------------------------------------------------------------
-- update flightplan with sales comp pvr
--------------------------------------------------------------------------      
update fp.flightplan
set currently = (
  select 
    case 
      when g.gm_new_count + g.gm_used_count = 0 then 0
      else (f.gm_sales_comp/(g.gm_new_count + g.gm_used_count))::integer
    end as gm_sales_comp_pvr
  --   case 
  --     when g.hn_new_count + g.hn_used_count = 0 then 0
  --     else (f.hn_sales_comp/(g.hn_new_count + g.hn_used_count))::integer
  --   end as hn_sales_comp_pvr  
  from (
    select sum(a.amount) filter (where d.store = 'RY1') as gm_sales_comp,
      sum(a.amount) filter (where d.store = 'RY2') as hn_sales_comp
    from fin.fact_gl a
    join dds.dim_date b on a.date_key = b.date_key
      and b.year_month = _year_month
    join fin.dim_account c on a.account_key = c.account_key
    join fp.gl_accounts d on c.account = d.account
      and d.page = 3
      and d.line = 4
    join fin.dim_journal e on a.journal_key  = e.journal_key    
    where a.post_status = 'Y') f
  join fp.sales_counts g on 1 = 1
    and g.the_date = _the_date)
where seq = 13
  and the_date = _the_date;    

--------------------------------------------------------------------------
-- update flightplan with avg recon per unit sold
--------------------------------------------------------------------------    
update fp.flightplan
set currently = (
  select 
    case
      when g.gm_used_count = 0 then 0
      else f.recon/g.gm_used_count
    end as recon_per_unit_sold
  from (  
    select sum(a.amount)::integer as recon
    from fin.fact_gl a
    join dds.dim_date b on a.date_key = b.date_key
      and b.year_month = _year_month
    join fin.dim_account c on a.account_key = c.account_key
      and c.account in ('164700','164701','164702','165100','165101','165102')
    join fin.dim_journal e on a.journal_key  = e.journal_key   
    where a.post_status = 'Y') f
  join fp.sales_counts g on 1 = 1
    and g.the_date = _the_date),
pace = null    
where seq = 14
  and the_date = _the_date;

--------------------------------------------------------------------------
-- update flightplan with GM Sales Expenses & Net   
-------------------------------------------------------------------------- 
-- new policy
update fp.flightplan 
set currently = (
  select sum(amount)::integer 
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = _year_month
  join fin.dim_account c on a.account_key = c.account_key
  join fp.gl_accounts d on c.account = d.account
    and d.page = 3
    and d.line = 6
    and d.col = 1
  join fin.dim_journal e on a.journal_key  = e.journal_key    
  where a.post_status = 'Y'
    and d.store = 'RY1'), pace = null
where seq = 15 
  and the_date = _the_date;      
-- used policy
update fp.flightplan 
set currently = (
  select sum(amount)::integer 
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = _year_month
  join fin.dim_account c on a.account_key = c.account_key
  join fp.gl_accounts d on c.account = d.account
    and d.page = 3
    and d.line = 6
    and d.col = 5
  join fin.dim_journal e on a.journal_key  = e.journal_key    
  where a.post_status = 'Y'
    and d.store = 'RY1'), pace = null
where seq = 16 
  and the_date = _the_date;   
-- delivery expense
update fp.flightplan 
set currently = (
  select sum(amount)::integer 
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = _year_month
  join fin.dim_account c on a.account_key = c.account_key
  join fp.gl_accounts d on c.account = d.account
    and d.page = 3
    and d.line = 5
  join fin.dim_journal e on a.journal_key  = e.journal_key    
  where a.post_status = 'Y'
    and d.store = 'RY1'), pace = null
where seq = 17 
  and the_date = _the_date;  
-- personnel
update fp.flightplan 
set currently = (
  select sum(amount)::integer 
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = _year_month
  join fin.dim_account c on a.account_key = c.account_key
  join fp.gl_accounts d on c.account = d.account
    and d.page = 3
    and d.line between 8 and 16
  join fin.dim_journal e on a.journal_key  = e.journal_key    
  where a.post_status = 'Y'
    and d.store = 'RY1'), pace = null
where seq = 18
  and the_date = _the_date;  
-- semi-fixed
update fp.flightplan 
set currently = (
  select sum(amount)::integer 
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = _year_month
  join fin.dim_account c on a.account_key = c.account_key
  join fp.gl_accounts d on c.account = d.account
    and d.page = 3
    and d.line between 18 and 39
  join fin.dim_journal e on a.journal_key  = e.journal_key    
  where a.post_status = 'Y'
    and d.store = 'RY1'), pace = null
where seq = 19
  and the_date = _the_date; 
-- fixed
update fp.flightplan 
set currently = (
  select sum(amount)::integer 
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = _year_month
  join fin.dim_account c on a.account_key = c.account_key
  join fp.gl_accounts d on c.account = d.account
    and d.page = 3
    and d.line between 41 and 54
  join fin.dim_journal e on a.journal_key  = e.journal_key    
  where a.post_status = 'Y'
    and d.store = 'RY1'), pace = null
where seq = 19.1
  and the_date = _the_date; 
--------------------------------------------------------------------------------
-- net note: this is NOT RETAIL ONLY
--------------------------------------------------------------------------------
update fp.flightplan 
set currently = (
  select sum(-amount)::integer 
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = _year_month
  join fin.dim_account c on a.account_key = c.account_key
  join fp.gl_accounts d on c.account = d.account
    and (( page between 5 and 10) or -- new
      (page = 16 and line between 1 and 14) or  -- used
      (page = 3 and line between 1 and 57) or  --expenses
      (page = 17 and line between 1 and 20))  -- f&i
  join fin.dim_journal e on a.journal_key  = e.journal_key    
  where a.post_status = 'Y'
    and d.store = 'RY1'), pace = null
where seq = 20
  and the_date = _the_date;  
--------------------------------------------------------------------------------
-- new inventory
--------------------------------------------------------------------------------
update fp.flightplan
set currently = (
  select sum(amount)::integer
  from (
    select c.store_code, c.account, a.control, c.description, sum(amount) as amount  
    from fin.fact_gl a
    inner join dds.dim_date b on a.date_key = b.date_key
      and b.the_date between current_Date - 900 and current_date--'06/22/2018'
    inner join fin.dim_account c on a.account_key = c.account_key 
    inner join nc.inventory_accounts cc on c.account = cc.account
    where a.post_status = 'Y'
      and c.store_code = 'RY1'
    group by c.store_code, c.account, a.control, c.description
    having sum(a.amount) > 10000) x)
where seq = 21
  and the_date = _the_date;    
--------------------------------------------------------------------------------
-- used inventory
--------------------------------------------------------------------------------
update fp.flightplan
set currently = (
  select sum(amount)::integer as amount  
  from fin.fact_gl a
  join (
    select inpmast_stock_number
    from arkona.xfm_inpmast
    where current_row
      and status = 'I'
      and type_n_u = 'U'
      and inventory_account in ('124000','124001','124100','124101')) b on a.control = b.inpmast_stock_number
  inner join fin.dim_account c on a.account_key = c.account_key 
    and c.account in ('124000','124001','124100','124101')
  where a.post_status = 'Y'
    and c.store_code = 'RY1') 
where seq = 22
  and the_date = _the_date;   


--------------------------------------------------------------------------
-- fixed dept expenses details
--------------------------------------------------------------------------      
delete
from fp.fixed_expenses_details
where year_month = _year_month;
insert into fp.fixed_expenses_details
select b.year_month, b.the_date, d.store, d.department, d.line, 
  d.account, e.journal_code, a.control, sum(-a.amount)::integer as amount
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = _year_month
join fin.dim_account c on a.account_key = c.account_key
join fp.gl_accounts d on c.account = d.account
  and d.page = 4
  and d.line between 8 and 55
join fin.dim_journal e on a.journal_key  = e.journal_key    
where a.post_status = 'Y'
group by b.year_month, b.the_date, a.control, d.store, d.department, d.line, 
  d.account, e.journal_code;  

--------------------------------------------------------------------------
-- update fp.flightplan fixed dept expenses
--------------------------------------------------------------------------  
-- main shop personnel
update fp.flightplan x
set currently = (
  select sum(-amount)
  from fp.fixed_expenses_details
  where store = 'RY1'
    and department = 'service'
    and line between 8 and 16
    and year_month = _year_month)
where seq = 24
  and the_date = _the_date;
-- main shop semi-fixed
update fp.flightplan x
set currently = (
  select sum(-amount)
  from fp.fixed_expenses_details
  where store = 'RY1'
    and department = 'service'
    and line between 18 and 39
    and year_month = _year_month)
where seq = 25
  and the_date = _the_date;
-- main shop fixed
update fp.flightplan x
set currently = (
  select sum(-amount)
  from fp.fixed_expenses_details
  where store = 'RY1'
    and department = 'service'
    and line between 41 and 54
    and year_month = _year_month)
where seq = 25.1
  and the_date = _the_date;
-- pdq personnel 
update fp.flightplan x
set currently = (
  select sum(-amount)
  from fp.fixed_expenses_details
  where store = 'RY1'
    and department = 'quick lane'
    and line between 8 and 16
    and year_month = _year_month)
where seq = 28
  and the_date = _the_date;
-- pdq semi-fixed 
update fp.flightplan x
set currently = (
  select sum(-amount)
  from fp.fixed_expenses_details
  where store = 'RY1'
    and department = 'quick lane'
    and line between 18 and 39
    and year_month = _year_month)
where seq = 29
  and the_date = _the_date;  
-- pdq fixed 
update fp.flightplan x
set currently = (
  select sum(-amount)
  from fp.fixed_expenses_details
  where store = 'RY1'
    and department = 'quick lane'
    and line between 41 and 54
    and year_month = _year_month)
where seq = 29.1
  and the_date = _the_date;  
-- parts personnel  
update fp.flightplan x
set currently = (
  select sum(-amount)
  from fp.fixed_expenses_details
  where store = 'RY1'
    and department = 'parts'
    and line between 8 and 16
    and year_month = _year_month)
where seq = 32
  and the_date = _the_date;
-- parts semi-fixed
update fp.flightplan x
set currently = (
  select sum(-amount)
  from fp.fixed_expenses_details
  where store = 'RY1'
    and department = 'parts'
    and line between 18 and 39
    and year_month = _year_month)
where seq = 33
  and the_date = _the_date;  
-- parts fixed
update fp.flightplan x
set currently = (
  select sum(-amount)
  from fp.fixed_expenses_details
  where store = 'RY1'
    and department = 'parts'
    and line between 41 and 54
    and year_month = _year_month)
where seq = 33.1
  and the_date = _the_date; 
-- body shop personnel
update fp.flightplan x
set currently = (
  select sum(-amount)
  from fp.fixed_expenses_details
  where store = 'RY1'
    and department = 'body shop'
    and line between 8 and 16
    and year_month = _year_month)
where seq = 40
  and the_date = _the_date;
-- body shop semi-fixed
update fp.flightplan x
set currently = (
  select sum(-amount)
  from fp.fixed_expenses_details
  where store = 'RY1'
    and department = 'body shop'
    and line between 18 and 39
    and year_month = _year_month)
where seq = 41
  and the_date = _the_date;  
-- body shop fixed
update fp.flightplan x
set currently = (
  select sum(-amount)
  from fp.fixed_expenses_details
  where store = 'RY1'
    and department = 'body shop'
    and line between 41 and 54
    and year_month = _year_month)
where seq = 41.1
  and the_date = _the_date;  
-- detail personnel  
update fp.flightplan x
set currently = (
  select sum(-amount)
  from fp.fixed_expenses_details
  where store = 'RY1'
    and department = 'detail'
    and line between 8 and 16
    and year_month = _year_month)
where seq = 44
  and the_date = _the_date;
-- detail semi-fixed 
update fp.flightplan x
set currently = (
  select sum(-amount)
  from fp.fixed_expenses_details
  where store = 'RY1'
    and department = 'detail'
    and line between 18 and 39
    and year_month = _year_month)
where seq = 45
  and the_date = _the_date;  
-- detail fixed 
update fp.flightplan x
set currently = (
  select sum(-amount)
  from fp.fixed_expenses_details
  where store = 'RY1'
    and department = 'detail'
    and line between 41 and 54
    and year_month = _year_month)
where seq = 45.1
  and the_date = _the_date;  
-- car wash personnel  
update fp.flightplan x
set currently = (
  select sum(-amount)
  from fp.fixed_expenses_details
  where store = 'RY1'
    and department = 'car wash'
    and line between 8 and 16
    and year_month = _year_month)
where seq = 48
  and the_date = _the_date;
-- car wash semi-fixed
update fp.flightplan x
set currently = (
  select sum(-amount)
  from fp.fixed_expenses_details
  where store = 'RY1'
    and department = 'car wash'
    and line between 18 and 39
    and year_month = _year_month)
where seq = 49
  and the_date = _the_date;  
-- car wash fixed
update fp.flightplan x
set currently = (
  select sum(-amount)
  from fp.fixed_expenses_details
  where store = 'RY1'
    and department = 'car wash'
    and line between 441 and 54
    and year_month = _year_month)
where seq = 49.1
  and the_date = _the_date;   

--------------------------------------------------------------------------
-- fixed: gross / net
--------------------------------------------------------------------------  

--------------------------------------------------------------------------
-- fixed: estimated gross from ros in cashier status
-------------------------------------------------------------------------- 
insert into fp.fixed_est_gross_from_cashier_status
select _year_month, _the_date,
  coalesce((sum(est_labor_gross) filter (where service_type in ('MN','MR','AM','EM','OT')))::integer, 0) as service, 
  coalesce((sum(est_labor_gross) filter (where service_type in ('QS', 'QL')))::integer, 0) as pdq, 
  coalesce((sum(est_labor_gross) filter (where service_type = 'RE'))::integer, 0) as detail,
  coalesce((sum(est_labor_gross) filter (where service_type = 'CW'))::integer, 0) as carwash, 
  coalesce((sum(est_labor_gross) filter (where service_type = 'BS'))::integer, 0) as bodyshop, 
  coalesce((sum(est_parts_gross))::integer, 0) as parts
from jeri.open_ros
where the_date = _the_date
  and status = 'cashier'
  and store_code = 'RY1'; 

--------------------------------------------------------------------------
-- fixed:  gross details from general ledger
--------------------------------------------------------------------------  
delete 
from fp.fixed_gross_details
where year_month = _year_month;

insert into fp.fixed_gross_details
select _year_month,  b.the_date, d.store, a.control, d.department,
  d.page, d.line, d.col, d.account, e.journal_code,
  sum(-a.amount) as amount
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = _year_month
join fin.dim_account c on a.account_key = c.account_key
join fp.gl_accounts d on c.account = d.account
  and d.page = 16 and d.line between 21 and 61
join fin.dim_journal e on a.journal_key  = e.journal_key    
where a.post_status = 'Y'
group by b.year_month, b.the_date, a.control, d.store, d.department, d.page, d.line, d.col,
  d.account, e.journal_code;    

--------------------------------------------------------------------------
-- fixed:  parts split
--------------------------------------------------------------------------  
delete 
from fp.parts_split 
where year_month = _year_month;

insert into fp.parts_split
select _year_month, 'RY1',
  service, pdq, (bs_gm + bs_non_gm) as bs, -(service + pdq + bs_gm + bs_non_gm) as parts
from (
  select 
    coalesce((sum(amount * .5) filter 
      (where account in ('146700', '166700', '146800', '166800','146700D')))::integer, 0) as service,
    coalesce((sum(amount * .5) filter (where account in ('147800','167800')))::integer, 0) as pdq,
    coalesce((sum(amount * .5) filter (where account in ('147700','167700')))::integer, 0) as bs_gm,
    coalesce((sum(amount) filter (where account in ('147701','167701')))::integer, 0) as bs_non_gm
  from fp.fixed_gross_details
  where store = 'RY1'
    and year_month = _year_month) a;
    
--------------------------------------------------------------------------
-- update fp.flightplan fixed gross / net
--------------------------------------------------------------------------  
-- service gross
update fp.flightplan x
set currently = (
  select a.amount + (
    select service
    from fp.fixed_est_gross_from_cashier_status
    where the_date = _the_date)
  from (  
    select sum(amount)::integer as amount
    from fp.fixed_gross_details
    where store = 'RY1'
      and department = 'service'
      and line between 21 and 33
      and year_month = _year_month) a)
where seq = 23
  and the_date = _the_date; 
  
-- service pace
with wd as (
  select *
  from fp.get_working_days (_the_date, 'service'))
update fp.flightplan x
set pace = (
  select (currently * (select month_factor from wd))::integer
  from fp.flightplan
  where seq = 23
    and the_date = _the_date)
where seq = 23
  and the_date = _the_date;

-- service net
update fp.flightplan x
set currently = (
  select 
    sum(currently) filter (where seq in (23) and the_date = _the_date)
    -
    sum(currently) filter (where seq in (24, 25, 25.1) and the_date = _the_date)
    +
    (select service from fp.parts_split where year_month = _year_month)
  from fp.flightplan)  
where seq = 26
  and the_date = _the_date; 

-- pdq gross
update fp.flightplan x
set currently = (
  select a.amount + (
    select pdq
    from fp.fixed_est_gross_from_cashier_status
    where the_date = _the_date)
  from (  
    select sum(amount)::integer as amount
    from fp.fixed_gross_details
    where store = 'RY1'
      and department = 'quick lane'
      and line between 21 and 33
      and year_month = _year_month) a)
where seq = 27
  and the_date = _the_date; 
  
-- pdq pace
with wd as (
  select *
  from fp.get_working_days (_the_date, 'pdq'))
update fp.flightplan x
set pace = (
  select (currently * (select month_factor from wd))::integer
  from fp.flightplan
  where seq = 27
    and the_date = _the_date)
where seq = 27
  and the_date = _the_date;

-- pdq net
update fp.flightplan x
set currently = (
  select 
    sum(currently) filter (where seq in (27) and the_date = _the_date)
    -
    sum(currently) filter (where seq in (28, 29, 29.1) and the_date = _the_date)
    +
    (select pdq from fp.parts_split where year_month = _year_month)
  from fp.flightplan)  
where seq = 30
  and the_date = _the_date;    

-- bs gross
update fp.flightplan x
set currently = (
  select a.amount + (
    select bodyshop
    from fp.fixed_est_gross_from_cashier_status
    where the_date = _the_date)
  from (  
    select sum(amount)::integer as amount
    from fp.fixed_gross_details
    where store = 'RY1'
      and department = 'body shop'
      and line between 35 and 42
      and year_month = _year_month) a)
where seq = 39
  and the_date = _the_date;   

-- bs pace
with wd as (
  select *
  from fp.get_working_days (_the_date, 'body shop'))
update fp.flightplan x
set pace = (
  select (currently * (select month_factor from wd))::integer
  from fp.flightplan
  where seq = 39
    and the_date = _the_date)
where seq = 39
  and the_date = _the_date;  

-- bs net
update fp.flightplan x
set currently = (
  select 
    sum(currently) filter (where seq in (39) and the_date = _the_date)
    -
    sum(currently) filter (where seq in (40, 41, 41.1) and the_date = _the_date)
    +
    (select bs from fp.parts_split where year_month = _year_month)
  from fp.flightplan)  
where seq = 42
  and the_date = _the_date;     


-- detail gross
update fp.flightplan x
set currently = (
  select a.amount + (
    select detail
    from fp.fixed_est_gross_from_cashier_status
    where the_date = _the_date)
  from (  
    select sum(amount)::integer as amount
    from fp.fixed_gross_details
    where store = 'RY1'
      and department = 'detail'
      and line between 21 and 33
      and year_month = _year_month) a)
where seq = 43
  and the_date = _the_date;   

-- detail pace
with wd as (
  select *
  from fp.get_working_days (_the_date, 'detail'))
update fp.flightplan x
set pace = (
  select (currently * (select month_factor from wd))::integer
  from fp.flightplan
  where seq = 43
    and the_date = _the_date)
where seq = 43
  and the_date = _the_date;  

-- detail net
update fp.flightplan x
set currently = (
  select 
    sum(currently) filter (where seq in (43) and the_date = _the_date)
    -
    sum(currently) filter (where seq in (44, 45, 45.1) and the_date = _the_date)
  from fp.flightplan)  
where seq = 46
  and the_date = _the_date;     

-- carwash gross
update fp.flightplan x
set currently = (
  select a.amount + (
    select carwash
    from fp.fixed_est_gross_from_cashier_status
    where the_date = _the_date)
  from (  
    select sum(amount)::integer as amount
    from fp.fixed_gross_details
    where store = 'RY1'
      and department = 'car wash'
      and line between 21 and 33
      and year_month = _year_month) a)
where seq = 47
  and the_date = _the_date;   

-- carwash pace
with wd as (
  select *
  from fp.get_working_days (_the_date, 'carwash'))
update fp.flightplan x
set pace = (
  select (currently * (select month_factor from wd))::integer
  from fp.flightplan
  where seq = 47
    and the_date = _the_date)
where seq = 47
  and the_date = _the_date;  

-- carwash net
update fp.flightplan x
set currently = (
  select 
    sum(currently) filter (where seq in (47) and the_date = _the_date)
    -
    sum(currently) filter (where seq in (48, 49, 49.1) and the_date = _the_date)
  from fp.flightplan)  
where seq = 50
  and the_date = _the_date;  

-- parts gross      
update fp.flightplan x
set currently = (
  select a.amount + (
    select parts
    from fp.fixed_est_gross_from_cashier_status
    where the_date = _the_date)
  from (  
    select sum(amount)::integer as amount
    from fp.fixed_gross_details
    where store = 'RY1'
      and department = 'parts'
      and line in (45,46,47,48,49,50,515,52,53,57,58,59)) a)
where seq = 31
  and the_date = _the_date; 
  
-- parts pace
with wd as (
  select *
  from fp.get_working_days (_the_date, 'parts'))
update fp.flightplan x
set pace = (
  select (currently * (select month_factor from wd))::integer
  from fp.flightplan
  where seq = 31
    and the_date = _the_date)
where seq = 31
  and the_date = _the_date;

-- parts line 54 factory $
update fp.flightplan x
set currently = (
    select sum(amount)::integer as amount
    from fp.fixed_gross_details
    where store = 'RY1'
      and department = 'parts'
      and line = 54)
where seq = 35
  and the_date = _the_date; 

-- parts line 55 inv adj
update fp.flightplan x
set currently = (
    select sum(-amount)::integer as amount
    from fp.fixed_gross_details
    where store = 'RY1'
      and department = 'parts'
      and line = 55)
where seq = 36
  and the_date = _the_date; 
  
-- parts net 
update fp.flightplan x
set currently = (
  select 
    sum(currently) filter (where seq in (31, 35, 36) and the_date = _the_date)
    -
    sum(currently) filter (where seq in (32, 33, 33.1) and the_date = _the_date)
    +
    (select service from fp.parts_split where year_month = _year_month)
  from fp.flightplan)  
where seq = 34
  and the_date = _the_date; 

end
$$;   




-- select * from fp.flightplan where the_date = current_date - 1  and seq not in (37,38) order by the_date, seq
