﻿-- -- -- ----------------------------------------------------------------------------------------------------------
-- -- -- --< Current Month
-- -- -- ----------------------------------------------------------------------------------------------------------
-- -- -- -- bit by the no sale in previous month bug god dammit, so buick cars don't show up FUUUUUUCK
-- -- -- -- that is, i can not use a single months's financial statement to determine all relevant accounts for
-- -- -- -- vehicle sales in a different month, maybe i didn't sell any lacrosses in that month 
-- -- -- 
-- -- -- -- current month count is not about matching the financial statement, there is not financial statement for the open month
-- -- -- -- mind fucking a bit on putting accounting together with board data
-- -- -- -- initially, problem seems to be in my count for the month, where i exclude unwinds, counting by
-- -- -- -- stock number instead of bopmast_id
-- -- -- 
-- -- -- -- count based on transactions to sale accounts
-- -- -- drop table if exists step_5;
-- -- -- create temp table step_5 as
-- -- -- select year_month, the_date, store::citext, page, line, control, sum(unit_count) as unit_count
-- -- -- from (
-- -- --   select b.year_month, b.the_date, d.store, d.page, d.line, a.control, 
-- -- --     case when a.amount < 0 then 1 else -1 end as unit_count
-- -- --   from fin.fact_gl a
-- -- --   inner join dds.dim_date b on a.date_key = b.date_key
-- -- --     and b.year_month = (select year_month from sls.months where open_closed = 'open')
-- -- --   inner join fin.dim_account c on a.account_key = c.account_key
-- -- --     and c.account_type_code = '4'
-- -- -- -- add journal
-- -- --   inner join fin.dim_journal aa on a.journal_key = aa.journal_key
-- -- --     and aa.journal_code in ('VSN','VSU')
-- -- --   inner join ( -- d: fs gm_account page/line/acct 
-- -- --     select distinct case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store,
-- -- --       a.fxmpge as page, a.fxmlne::integer as line, b.g_l_acct_number as gl_account
-- -- --     from arkona.ext_Eisglobal_sypffxmst a 
-- -- --     inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
-- -- --       and a.fxmcyy = b.factory_financial_year
-- -- --     where ( -- use routing from this and the prev year
-- -- --       a.fxmcyy = (select the_year from dds.dim_date where the_date = current_date)
-- -- --       or
-- -- --       a.fxmcyy = (select the_year - 1 from dds.dim_date where the_date = current_date))
-- -- --       and (
-- -- --         (a.fxmpge in (5,7,8,9,10,14) and (a.fxmlne between 1 and 19 or a.fxmlne between 25 and 40))  or
-- -- --         (a.fxmpge = 16 and a.fxmlne between 1 and 6))) d on c.account = d.gl_account 
-- -- --   where a.post_status = 'Y') h
-- -- -- group by year_month, the_date, store, page, line, control;
-- -- -- 
-- -- -- -- was thinking a table: variable_accounts or something like that
-- -- -- -- fs based accounts required for flight plan, this was last friday, a bad day overall, could not 
-- -- -- -- visualize what i really wanted
-- -- -- create table variable
-- -- --     select distinct case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store,
-- -- --       a.fxmpge as page, a.fxmlne::integer as line, b.g_l_acct_number as gl_account, b.factory_account
-- -- --     from arkona.ext_Eisglobal_sypffxmst a 
-- -- --     inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
-- -- --       and a.fxmcyy = b.factory_financial_year
-- -- --     where ( -- use routing from this and the prev year
-- -- --       a.fxmcyy = (select the_year from dds.dim_date where the_date = current_date)
-- -- --       or
-- -- --       a.fxmcyy = (select the_year - 1 from dds.dim_date where the_date = current_date))
-- -- --       and (
-- -- --         (a.fxmpge in (5,7,8,9,10,14) )  
-- -- --         or
-- -- --         (a.fxmpge = 16)
-- -- --         or
-- -- --         (a.fxmpge = 17))
-- -- -- 
-- -- -- 
-- -- --         
-- -- -- select * from arkona.ext_ffpxrefdta limit 10
-- -- -- select * from arkona.ext_Eisglobal_sypffxmst limit 10
-- -- -- 
-- -- -- select d.store, a.*, b.page, b.line, b.col, c.gm_account, c.gl_account
-- -- -- from fin.fact_fs a
-- -- -- inner join fin.dim_fs b on a.fs_key = b.fs_key
-- -- -- inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
-- -- -- inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
-- -- -- where b.year_month = 201808
-- -- --   and b.page = 1
-- -- -- --   and b.line between 21 and 26
-- -- -- order by d.store, b.page, b.line, b.col
-- -- -- 
-- -- -- -- actual sales for the month, 
-- -- -- drop table if exists step_6; 
-- -- -- create temp table step_6 as
-- -- -- select a.year_month, a.the_date, a.store, a.page, a.line::integer, a.control, sum(unit_count) as unit_count, gl_account,
-- -- --   case
-- -- --     when store = 'ry1' and page = 5 then 'Chevrolet'
-- -- --     when store = 'ry1' and page = 8 then 'Buick'
-- -- --     when store = 'ry1' and page = 9 then 'Cadillac'
-- -- --     when store = 'ry1' and page = 10 then 'GMC'
-- -- --     when store = 'ry1' and page = 14 then 'MV-1'
-- -- --     when page = 16 and line in (1,4) then 'Certified'
-- -- --     when page = 16 and line in (2,5) then 'Other'
-- -- --     when store = 'ry2' and page = 7 then 'Honda'
-- -- --     when store = 'ry2' and page = 14 then 'Nissan'
-- -- --   end as make,
-- -- --   case 
-- -- --     when page < 16 and line < 20 then 'Car'
-- -- --     when page < 16 and line > 20 then 'Truck'
-- -- --     when page = 16 and line < 3 then 'Car'
-- -- --     when page = 16 and line > 3 then 'Truck'
-- -- --   end as shape,
-- -- --   extract(day from the_date)::integer as day_of_month, extract(doy from the_date)::integer as day_of_year
-- -- -- from step_5 a
-- -- -- group by a.year_month, a.the_date, a.store, a.page, a.line, a.control, gl_account;
-- -- -- 
-- -- -- -- agrees with ry1 doc
-- -- -- -- select store, page, sum(unit_count) from step_6 group by store, page order by store, page
-- -- -- 
-- -- -- -- gross include journal in gross (GLI: sfe, WTD: writedown, etc)
-- -- -- -- include dim_gl_description in gross
-- -- -- 
-- -- -- drop table if exists step_7;
-- -- -- create temp table step_7 as
-- -- -- select b.year_month, b.the_date, a.control, -a.amount as amount, d.*, e.journal_code, f.description
-- -- -- from fin.fact_gl a
-- -- -- inner join dds.dim_date b on a.date_key = b.date_key
-- -- --   and b.year_month = (select year_month from sls.months where open_closed = 'open')
-- -- -- inner join fin.dim_account c on a.account_key = c.account_key
-- -- -- inner join (
-- -- --     select distinct case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store,
-- -- --       a.fxmpge as page, a.fxmlne::integer as line, b.g_l_acct_number as gl_account
-- -- --     from arkona.ext_Eisglobal_sypffxmst a 
-- -- --     inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
-- -- --       and a.fxmcyy = b.factory_financial_year
-- -- --     where (
-- -- --       a.fxmcyy = (select the_year from dds.dim_date where the_date = current_date)
-- -- --       or
-- -- --       a.fxmcyy = (select the_year - 1 from dds.dim_date where the_date = current_date))
-- -- --       and (
-- -- --         (a.fxmpge in (5,7,8,9,10,14) and (a.fxmlne between 1 and 19 or a.fxmlne between 25 and 40))  or
-- -- --         (a.fxmpge = 16 and a.fxmlne between 1 and 6))) d on c.account = d.gl_account --b.year_month = d.year_month
-- -- -- inner join fin.dim_journal e on a.journal_key  = e.journal_key    
-- -- -- left join fin.dim_gl_description f on a.gl_description_key = f.gl_description_key
-- -- --   and b.the_date between f.row_from_date and f.row_thru_date
-- -- -- where a.post_status = 'Y' order by control; 
-- -- -- 
-- -- -- 
-- -- -- select store, page, sum(amount)::integer
-- -- -- from step_7
-- -- -- group by store, page
-- -- -- order by store, page
-- -- -- 
-- -- -- 
-- -- -- -- car/truck/used totals: count & gross
-- -- -- -- gross off for both stores, need to look at it
-- -- -- select *
-- -- -- from (
-- -- -- select a.year_month, a.store, a.page, a.new_car_ct, b.new_car_gr,
-- -- --   a.new_truck_ct, b.new_truck_gr, a.used_ct, b.used_gr
-- -- -- from ( -- count
-- -- --   select year_month, store, page, 
-- -- --     sum(unit_count) filter (where page < 16 and line between 1 and 19) as new_car_ct,
-- -- --     sum(unit_count) filter (where page < 16 and line between 25 and 40) as new_truck_ct,
-- -- --     sum(unit_count) filter (where page = 16) as used_ct 
-- -- --   from step_6  
-- -- --   group by year_month, store, page) a
-- -- -- join ( -- gross
-- -- --   select year_month, store, page, 
-- -- --     (sum(amount) filter (where page < 16 and line between 1 and 19))::integer as new_car_gr,
-- -- --     (sum(amount) filter (where page < 16 and line between 25 and 40))::integer as new_truck_gr,
-- -- --     (sum(amount) filter (where page = 16))::integer as used_gr
-- -- --   from step_7 
-- -- --   group by year_month, store, page) b on a.store = b.store and a.year_month = b.year_month and a.page = b.page
-- -- -- order by a.year_month, a.store, a.page) x
-- -- -- 
-- -- -- 
-- -- -- -- boarded deals
-- -- -- -- don't need f/i gross for this part of flightplan
-- -- -- -- table board.deal_gross
-- -- -- -- populated by opentrack call to DealLookup
-- -- -- -- front_gross: commissionableGross + holdback + incentive
-- -- -- -- which in bopmast should be comm_gross, hold_back, incentive
-- -- -- -- fi_gross: TotalFandIReserve
-- -- -- -- rydell@betarydellvision:~/new-vision/lib/plugins/sales-board$ sales-boards.js
-- -- -- drop table if exists boarded_deals;
-- -- -- create temp table boarded_deals as
-- -- -- select 
-- -- --   case
-- -- --     when d.store like '%outlet' then 'outlet'::citext
-- -- --     when d.store like '%Chev%' then 'RY1':: citext
-- -- --     when d.store like '%Honda%' then 'RY2'::citext
-- -- --   end as store, boarded_ts ::date as board_date, a.is_backed_on, a.stock_number, a.deal_number, 
-- -- --   e.vehicle_type, e.vehicle_make,
-- -- --   e.sale_code, -- f.inpmast_sale_account as sale_acct, 
-- -- --   g.amount::integer as front_gross, b.board_type, b.board_sub_type
-- -- -- -- select *  
-- -- -- from board.sales_board a
-- -- -- inner join board.board_types b on a.board_type_key = b.board_type_key
-- -- -- inner join dds.dim_date c on a.boarded_ts::date = c.the_date
-- -- -- inner join onedc.stores d on a.store_key = d.store_key
-- -- -- inner join board.daily_board e on a.board_id = e.board_id
-- -- -- -- inner join arkona.xfm_inpmast f on a.vin = f.inpmast_vin
-- -- -- --   and f.current_row = true
-- -- -- left join board.deal_gross g on a.board_id = g.board_id
-- -- --   and g.current_row = true
-- -- --   and g.amount_type =  'front_end_gross'
-- -- -- left join board.deal_gross h on a.board_id = h.board_id
-- -- --   and h.current_row = true
-- -- --   and h.amount_type <>  'front_end_gross'  
-- -- -- where b.board_type in ('Deal','Back-on') -- = 'Deal' 
-- -- -- --   and b.board_sub_type in ('Retail', 'Lease')
-- -- --   and c.year_month = (select year_month from sls.months where open_closed = 'open')
-- -- --   and not a.is_deleted
-- -- -- order by store, board_date;
-- -- -- select * from boarded_deals;
-- -- -- 
-- -- -- what i am trying to accomplish is current month sales from accounting plus unposted deals from board
-- -- -- 
-- -- -- the perspective is that of the financial statement
-- -- -- stores are separate statements
-- -- -- makes are separate pages
-- -- -- consultants do no matter
-- -- -- only retail
-- -- -- 
-- -- -- -- on 9/4: no honda deals posted yet
-- -- -- -- ok, slowly getting there
-- -- -- -- sale account in inpmast for 31518D is wrong because car started out at ry1, inpmast not updated
-- -- -- -- when vehicle moved to outlet, in inpmast as 144601 s/b 144602
-- -- -- -- select * from fin.dim_account where account in ('144601','144602')
-- -- -- -- so, leave sale account out of boarded deals
-- -- -- select *
-- -- -- from step_6 a
-- -- -- full outer join boarded_deals b on a.control = b.stock_number
-- -- -- where b.board_sub_type not in ('dealer trade','intercompany wholesale')
-- -- -- order by b.store, b.vehicle_type, b.vehicle_make
-- -- -- 
-- -- --   
-- -- -- -- need the combination of step_6 (for count) and step_7 (for gross)
-- -- -- -- 
-- -- -- -- this looks good (at least today)
-- -- -- select  *
-- -- -- from (
-- -- -- select a.store, a.page, a.control, a.unit_count, b.gross
-- -- -- from step_6 a
-- -- -- join (
-- -- --   select control, sum(amount)::integer as gross
-- -- --   from step_7
-- -- --   group by control) b on a.control = b.control) c
-- -- -- full outer join boarded_deals d on c.control = d.stock_number 
-- -- -- where d.board_sub_type not in ('dealer trade','intercompany wholesale','wholesale', 'CTP')
-- -- --   and board_date < current_date
-- -- -- order by d.store, d.vehicle_type, d.vehicle_make
-- -- -- 
-- -- -- -- unit count, deals from accounting:  
-- -- -- drop table if exists unit_count;
-- -- -- create temp table unit_count as
-- -- -- select store, page, line, control, sum(unit_count) as unit_count
-- -- -- from step_5
-- -- -- group by store, page, line, control
-- -- -- order by store, page;
-- -- -- 
-- -- -- select store, page, sum(unit_count)
-- -- -- from unit_count
-- -- -- group by store, page
-- -- -- order by store, page
-- -- -- 
-- -- -- select store, 
-- -- --   case page
-- -- --     when 16 then 'Used'
-- -- --     else make
-- -- --   end as make, sum(unit_count) as unit_count 
-- -- -- from step_6
-- -- -- group by store, 
-- -- --   case page
-- -- --     when 16 then 'Used'
-- -- --     else make
-- -- --   end
-- -- -- order by store, make  
-- -- -- 
-- -- --   
-- -- -- -- estimated, ie, boarded but not in accounting
-- -- -- select b.*
-- -- -- from step_5 a
-- -- -- full outer join boarded_deals b on a.control = b.stock_number
-- -- -- where b.board_sub_type in ('retail','lease')
-- -- --   and not b.is_backed_on
-- -- --   and a.control is null
-- -- -- 
-- -- -- drop table if exists gross;
-- -- -- create temp table gross as
-- -- -- select store, page, control, sum(amount) as amount
-- -- -- from step_7
-- -- -- group by store, page, control
-- -- -- order by store, page, control;
-- -- -- 
-- -- -- 
-- -- -- -- G34059R has gross on page 5 and 16 ro 16322993 after the sale Detail PRO
-- -- -- select *
-- -- -- from unit_count a
-- -- -- full outer join gross b on a.control = b.control
-- -- -- 
-- -- -- -- non deal gross
-- -- -- select b.store, b.page, sum(b.amount)
-- -- -- from unit_count a
-- -- -- full outer join gross b on a.control = b.control
-- -- -- where a.control is null
-- -- -- group by b.store, b.page
-- -- -- 
-- -- -- select * from step_6 order by store, page, line
-- -- -- 
-- -- -- select store, page, sum(amount)::integer
-- -- -- from gross 
-- -- -- group by store, page
-- -- -- order by store, page

drop table if exists fp.flightplan cascade;
create table fp.flightplan (
  seq numeric(5,2) not null,
  year_month integer not null,
  the_date date not null,
  store citext not null,
  department citext not null,
  area citext not null,
  currently integer ,
  pace integer,
  primary key (the_date,store,department,area));
comment on table fp.flightplan is 'table to accumulate system generated flightplan data';
create unique index on fp.flightplan(seq, the_date);


-- each day a new base row
insert into fp.flightplan (seq,year_month,the_date,store,department,area) values
(1, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Sales','New Chevy'), 
(2, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Sales','New GMC'), 
(3, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Sales','New Buick'), 
(4, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Sales','New Cadillac'), 
(5, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Sales','Used'), 
(6, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Sales','New Gross'), 
(7, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Sales','New PVR'), 
(8, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Sales','Used Gross'), 
(9, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Sales','Used PVR'), 
(10, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Sales','F&I Gross before Comp/Chgbk'), 
(11, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Sales','F&I PVR'), 
(12, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Sales','F&I Comp'), 
(13, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Sales','Sales Comp PVR'), 
(14, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Sales','Avg recon/unit sold'), 
(15, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Sales','New Policy'), 
(16, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Sales','Used Policy'), 
(17, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Sales','Delivery Expense'), 
(18, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Sales','Personnel'), 
(19, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Sales','Semi Fixed'), 
(20, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Sales','Net'), 
(21, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Sales','Inventory $ - New'), 
(22, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Sales','Inventory $ - Used'), 
(23, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Service','Gross'), 
(24, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Service','Personnel'), 
(25, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Service','Semi Fixed'), 
(26, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Service','Net'),  
(27, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM PDQ','Gross'), 
(28, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM PDQ','Personnel'),
(29, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM PDQ','Semi Fixed'),
(30, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM PDQ','Net'),
(31, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Parts','Gross'),
(32, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Parts','Personnel'),
(33, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Parts','Semi Fixed'),
(34, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Parts','Net'),
(35, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Parts','Factory $/SA'),
(36, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Parts','Inventory Adjustments'),
(37, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Parts','Overtime'),
(38, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','GM Parts','Wholesale'),
(39, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','Body Shop','Gross'),
(40, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','Body Shop','Personnel'),
(41, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','Body Shop','Semi Fixed'),
(42, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','Body Shop','Net'),
(43, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','Detail','Gross'),
(44, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','Detail','Personnel'),
(45, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','Detail','Semi Fixed'),
(46, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','Detail','Net'),
(47, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','Carwash','Gross'),
(48, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','Carwash','Personnel'),
(49, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','Carwash','Semi Fixed'),
(50, (select year_month from dds.dim_date where the_date = current_date), current_date,'RY1','Carwash','Net');


--------------------------------------------------------------------------
-- working days
--------------------------------------------------------------------------
create or replace function fp.get_working_days (_the_date date, _dept citext)
returns table (wd_in_month integer, wd_of_month_elapsed integer, month_factor numeric) as
$BODY$
    select wd_in_month, wd_of_month_elapsed, round((wd_in_month*1.0/wd_of_month_elapsed), 2)
    from dds.working_days 
    where the_date = _the_date
      and department = _dept; 
$BODY$
language sql;  


--------------------------------------------------------------------------
-- flightplan accounts
-- 9/21 add departments, columns
--------------------------------------------------------------------------

drop table if exists fp.gl_accounts cascade;
create table fp.gl_accounts (
  store citext not null,
  department citext not null,
  page integer not null,
  line integer not null,
  col integer not null,
  account citext primary key);
comment on table fp.gl_accounts is 'gl accounts from financial statement routing, any account used
  in the current of previous year.  146424 excluded because it shows up in 2 places, in previous
  years it was routed to P16L27, currently routed to P16K29 only. 
  Refreshed nightly. department added to discriminate between
  service sub-departments: pdq, bs, detail, carwash, mainshop';
create index on fp.gl_accounts(page);
create index on fp.gl_accounts(line);
create index on fp.gl_accounts(store);
create index on fp.gl_accounts(department);
create index on fp.gl_accounts(col);
-- initial load
insert into fp.gl_accounts
select distinct case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store,
  c.department, a.fxmpge as page, a.fxmlne::integer as line, a.fxmcol as col, 
  b.g_l_acct_number as gl_account
from arkona.ext_Eisglobal_sypffxmst a 
inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
join fin.dim_account c on b.g_l_acct_number = c.account
  and c.current_row = true    
where ( -- use routing from this and the prev year
  a.fxmcyy = (select the_year from dds.dim_date where the_date = current_date)
  or
  a.fxmcyy = (select the_year - 1 from dds.dim_date where the_date = current_date))
  and case when b.g_l_acct_number = '146424' then a.fxmlne <> 27 else 1 = 1 end;
--nightly
insert into fp.gl_accounts (store,department,page,line,account)
select distinct case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store,
  c.department, a.fxmpge as page, a.fxmlne::integer as line, a.fxmcol as col, 
  b.g_l_acct_number as gl_account
from arkona.ext_Eisglobal_sypffxmst a 
inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
join fin.dim_account c on b.g_l_acct_number = c.account
  and c.current_row = true    
where ( -- use routing from this and the prev year
  a.fxmcyy = (select the_year from dds.dim_date where the_date = current_date)
  or
  a.fxmcyy = (select the_year - 1 from dds.dim_date where the_date = current_date))
  and case when b.g_l_acct_number = '146424' then a.fxmlne <> 27 else 1 = 1 end
  and not exists (
    select 1
    from fp.gl_accounts
    where account = b.g_l_acct_number);

select *
from  arkona.ext_ffpxrefdta
where g_l_acct_number = '146424'

select *
from arkona.ext_Eisglobal_sypffxmst
where fxmact in ('464','462')

select *
from fp.gl_accounts
where account = '146424'
--------------------------------------------------------------------------
-- deals from accounting
--------------------------------------------------------------------------
drop table if exists fp.acct_sales_counts_details;
create table fp.acct_sales_counts_details (
  year_month integer not null,
  the_date date not null,
  store citext not null,
  page integer not null,
  line integer not null,
  stock_number citext not null,
  unit_count integer not null,
  primary key (the_date, stock_number));
comment on table fp.acct_sales_counts_details is 'daily sales determined by general ledger transactions to sale accounts,
  current month refreshed daily';
-- initial load  
insert into fp.acct_sales_counts_details
select year_month, the_date, store::citext, page, line, control, sum(unit_count) as unit_count
from (
  select b.year_month, b.the_date, d.store, d.page, d.line, a.control, 
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = (select year_month from sls.months where open_closed = 'open')
  join fin.dim_account c on a.account_key = c.account_key
    and c.account_type_code = '4'
  join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  join fp.gl_accounts d on c.account = d.account
    and (
      (d.page in (5,7,8,9,10,14) and (d.line between 1 and 19 or d.line between 25 and 40))  or
      (d.page = 16 and d.line between 1 and 6))
  where a.post_status = 'Y') h
group by year_month, the_date, store, page, line, control;

-- nightly
delete 
from fp.acct_sales_counts_details
where year_month = (select year_month from dds.dim_date where the_date = current_date);

insert into fp.acct_sales_counts_details
select year_month, the_date, store::citext, page, line, control, sum(unit_count) as unit_count
from (
  select b.year_month, b.the_date, d.store, d.page, d.line, a.control, 
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = (select year_month from sls.months where open_closed = 'open')
  join fin.dim_account c on a.account_key = c.account_key
    and c.account_type_code = '4'
  join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  join fp.gl_accounts d on c.account = d.account
    and (
      (d.page in (5,7,8,9,10,14) and (d.line between 1 and 19 or d.line between 25 and 40))  or
      (d.page = 16 and d.line between 1 and 6))
  where a.post_status = 'Y') h
group by year_month, the_date, store, page, line, control;

--------------------------------------------------------------------------
-- deal counts from accounting
--------------------------------------------------------------------------
drop table if exists fp.acct_sales_counts;
create table fp.acct_sales_counts (
  the_date date primary key,
  year_month integer not null,
  chev integer not null,
  buick integer not null,
  cad integer not null,
  gmc integer not null,
  gm_new integer not null,
  gm_used integer not null,
  honda integer not null,
  nissan integer not null,
  hn_new integer not null,
  hn_used integer not null);
comment on table fp.acct_sales_counts is 'aggregated count of sales from fp.acct_sales_counts_details,
  snapshot daily';  
insert into fp.acct_sales_counts
select current_date, (select year_month from dds.dim_date where the_date = current_date),
  coalesce(sum(unit_count) filter (where store = 'RY1' and page = 5), 0) as chev,
  coalesce(sum(unit_count) filter (where store = 'RY1' and page = 8), 0) as buick,
  coalesce(sum(unit_count) filter (where store = 'RY1' and page = 9), 0) as cad,
  coalesce(sum(unit_count) filter (where store = 'RY1' and page = 10), 0) as gmc,
  coalesce(sum(unit_count) filter (where store = 'RY1' and page <> 16), 0) as gm_new,
  coalesce(sum(unit_count) filter (where store = 'RY1' and page = 16), 0) as gm_used,
  coalesce(sum(unit_count) filter (where store = 'RY2' and page = 7), 0) as honda,
  coalesce(sum(unit_count) filter (where store = 'RY2' and page = 14), 0) as nissan,
  coalesce(sum(unit_count) filter (where store = 'RY2' and page <> 16), 0) as hn_new,
  coalesce(sum(unit_count) filter (where store = 'RY2' and page = 16), 0) as hn_used  
from fp.acct_sales_counts_details;

--------------------------------------------------------------------------
-- deals from sold board
--------------------------------------------------------------------------
drop table if exists fp.boarded_deals_details;
create table fp.boarded_deals_details (
  year_month integer not null,
  boarded_date date not null,
  store citext not null,
  stock_number citext not null,
  vehicle_type citext not null,
  vehicle_make citext not null,
  front_gross integer not null,
  fi_gross integer not null,
  primary key (boarded_date, stock_number));
comment on table fp.boarded_deals_details is 'data from board.sales_board, board.daily_board, board.deal_gross.
   board_type is Deal, sub_type in retail, lease. adds to pacing those deals that are boarded but not yet
   posted in accounting';
-- initial load   
insert into fp.boarded_deals_details
select year_month, boarded_date, store, stock_number, vehicle_type,
  vehicle_make, front_gross, fi_gross
from (
  select (select year_month from dds.dim_date where the_date = current_date),
    boarded_ts ::date as boarded_date,
    case
      when d.store like '%outlet' then 'outlet'::citext
      when d.store like '%Chev%' then 'RY1':: citext
      when d.store like '%Honda%' then 'RY2'::citext
    end as store, a.stock_number, 
    e.vehicle_type, e.vehicle_make,
    case
      when g.amount = 'NaN' then 0
      else coalesce(g.amount, 0)::integer 
    end as front_gross,
     case
      when h.amount = 'NaN' then 0
      else coalesce(h.amount, 0)::integer 
    end as fi_gross
  from board.sales_board a
  inner join board.board_types b on a.board_type_key = b.board_type_key
  inner join dds.dim_date c on a.boarded_ts::date = c.the_date
  inner join onedc.stores d on a.store_key = d.store_key
  inner join board.daily_board e on a.board_id = e.board_id
  left join board.deal_gross g on a.board_id = g.board_id
    and g.current_row = true
    and g.amount_type =  'front_end_gross'
  left join board.deal_gross h on a.board_id = h.board_id
    and h.current_row = true
    and h.amount_type = 'fi_gross'
  where not a.is_backed_on
    and  b.board_type in ('Deal')
    and b.board_sub_type in ('retail','lease')
    and c.year_month = (select year_month from sls.months where open_closed = 'open')
    and not a.is_deleted) x 
group by year_month, boarded_date, store, stock_number, vehicle_type,
  vehicle_make, front_gross, fi_gross;
-- nightly
delete 
from fp.boarded_deals_details
where year_month =  (select year_month from dds.dim_date where the_date = current_date);
insert into fp.boarded_deals_details
select year_month, boarded_date, store, stock_number, vehicle_type,
  vehicle_make, front_gross, fi_gross
from (
  select (select year_month from dds.dim_date where the_date = current_date),
    boarded_ts ::date as boarded_date,
    case
      when d.store like '%outlet' then 'outlet'::citext
      when d.store like '%Chev%' then 'RY1':: citext
      when d.store like '%Honda%' then 'RY2'::citext
    end as store, a.stock_number, 
    e.vehicle_type, e.vehicle_make,
    case
      when g.amount = 'NaN' then 0
      else coalesce(g.amount, 0)::integer 
    end as front_gross,
     case
      when h.amount = 'NaN' then 0
      else coalesce(h.amount, 0)::integer 
    end as fi_gross
  from board.sales_board a
  inner join board.board_types b on a.board_type_key = b.board_type_key
  inner join dds.dim_date c on a.boarded_ts::date = c.the_date
  inner join onedc.stores d on a.store_key = d.store_key
  inner join board.daily_board e on a.board_id = e.board_id
  left join board.deal_gross g on a.board_id = g.board_id
    and g.current_row = true
    and g.amount_type =  'front_end_gross'
  left join board.deal_gross h on a.board_id = h.board_id
    and h.current_row = true
    and h.amount_type = 'fi_gross'
  where not a.is_backed_on
    and  b.board_type in ('Deal')
    and b.board_sub_type in ('retail','lease')
    and c.year_month = (select year_month from sls.months where open_closed = 'open')
    and not a.is_deleted) x 
group by year_month, boarded_date, store, stock_number, vehicle_type,
  vehicle_make, front_gross, fi_gross;
  
--------------------------------------------------------------------------
-- deal counts from sold board
--------------------------------------------------------------------------
drop table if exists fp.boarded_deals_counts;
create table fp.boarded_deals_counts (
  the_date date primary key,
  year_month integer not null,
  chev integer not null,
  buick integer not null,
  cad integer not null,
  gmc integer not null,
  gm_new integer not null,
  gm_used integer not null,
  honda integer not null,
  nissan integer not null,
  hn_new integer not null,
  hn_used integer not null);
comment on table fp.acct_sales_counts is 'aggregated count of sales from fp.boarded_deals_details,
  deals that are not yet in accounting, snapshot daily';  
-- initial & nightly  
insert into fp.boarded_deals_counts
select current_date, (select year_month from dds.dim_date where the_date = current_date),
  coalesce(count(stock_number) filter (where store = 'RY1' and vehicle_type = 'N' and vehicle_make = 'chevrolet'), 0) as chev,
  coalesce(count(stock_number) filter (where store = 'RY1' and vehicle_type = 'N' and vehicle_make = 'buick'), 0) as buick,
  coalesce(count(stock_number) filter (where store = 'RY1' and vehicle_type = 'N' and vehicle_make = 'cad'), 0) as cad,
  coalesce(count(stock_number) filter (where store = 'RY1' and vehicle_type = 'N' and vehicle_make = 'gmc'), 0) as gmc,
  coalesce(count(stock_number) filter (where store = 'RY1' and vehicle_type = 'N'), 0) as gm_new,
  coalesce(count(stock_number) filter (where store = 'RY1' and vehicle_type = 'N' and vehicle_make = 'gm_used'), 0) as gm_used,
  coalesce(count(stock_number) filter (where store = 'RY2' and vehicle_type = 'N' and vehicle_make = 'honda'), 0) as honda,
  coalesce(count(stock_number) filter (where store = 'RY2' and vehicle_type = 'N' and vehicle_make = 'nissan'), 0) as nissan,
  coalesce(count(stock_number) filter (where store = 'RY2' and vehicle_type = 'N'), 0) as hn_new,
  coalesce(count(stock_number) filter (where store = 'RY2' and vehicle_type = 'N' and vehicle_make = 'hn_used'), 0) as hn_used 
from fp.boarded_deals_details a
where year_month = (select year_month from dds.dim_date where the_date = current_date)
  AND not exists (
      select stock_number 
      from fp.acct_sales_counts_details
      where stock_number = a.stock_number
        and year_month = (select year_month from dds.dim_date where the_date = current_date));

--------------------------------------------------------------------------
-- update flightplan with deal counts
--------------------------------------------------------------------------
drop table if exists fp.sales_counts;
create table fp.sales_counts (
  year_month integer not null,
  the_date date primary key,
  chev_count integer not null,
  chev_pace integer not null,
  buick_count integer not null,
  buick_pace integer not null,
  gmc_count integer not null,
  gmc_pace integer not null,
  cad_count integer not null,
  cad_pace integer not null,
  gm_used_count integer not null,
  gm_used_pace integer not null,
  honda_count integer not null,
  honda_pace integer not null,
  nissan_count integer not null,
  nissan_pace integer not null,
  hn_used_count integer not null,
  hn_used_pace integer not null,
  gm_new_count integer not null,
  hn_new_count integer not null);
  
insert into fp.sales_counts
with wd as (
  select *
  from fp.get_working_days (current_date, 'sales'))
select (select year_month from sls.months where open_closed = 'open'),
  current_date, sum(chev) as chev_count, (sum(chev) * (select month_factor from wd))::integer as chev_pace,
  sum(buick) as buick_count, (sum(buick) * (select month_factor from wd))::integer as buick_pace,
  sum(gmc) as gmc_count, (sum(gmc) * (select month_factor from wd))::integer as gmc_pace,
  sum(cad) as cad_count, (sum(cad) * (select month_factor from wd))::integer as cad_pace,
  sum(gm_used) as gm_used_count, (sum(gm_used) * (select month_factor from wd))::integer as gm_used_pace,
  sum(honda) as honda_count, (sum(honda) * (select month_factor from wd))::integer as honda_pace,
  sum(nissan) as nissan_count, (sum(nissan) * (select month_factor from wd))::integer as nissan_pace,
  sum(hn_used) as hn_used_count, (sum(hn_used) * (select month_factor from wd))::integer as hn_used_pace,
  sum(gm_new) as gm_new_count, sum(hn_new) as hn_new_count
from (    
  select *
  from fp.acct_sales_counts
  where the_date = current_date
  union all
  select *
  from fp.boarded_deals_counts
  where the_date = current_date) a;
  
update fp.flightplan
set currently = (select chev_count from fp.sales_counts where the_date = current_date),
    pace = (select chev_pace from fp.sales_counts where the_date = current_date)
where the_date = current_date
  and seq = 1;   
update fp.flightplan  
set currently = (select gmc_count from fp.sales_counts where the_date = current_date),
    pace = (select gmc_pace from fp.sales_counts where the_date = current_date)
where the_date = current_date
  and seq = 2;     
update fp.flightplan  
set currently = (select buick_count from fp.sales_counts where the_date = current_date),
    pace = (select buick_pace from fp.sales_counts where the_date = current_date)
where the_date = current_date
  and seq = 3;   
update fp.flightplan  
set currently = (select cad_count from fp.sales_counts where the_date = current_date),
    pace = (select cad_pace from fp.sales_counts where the_date = current_date)
where the_date = current_date
  and seq = 4; 
update fp.flightplan    
set currently = (select gm_used_count from fp.sales_counts where the_date = current_date),
    pace = (select gm_used_pace from fp.sales_counts where the_date = current_date)
where the_date = current_date
  and seq = 5;          


    
--------------------------------------------------------------------------
-- front gross details from accounting
--------------------------------------------------------------------------
drop table if exists fp.acct_sales_gross_details cascade;
create table fp.acct_sales_gross_details (
  year_month integer not null,
  the_date date not null,
  stock_number citext not null,
  store citext not null,
  page integer not null,
  line integer not null,
  account citext not null, 
  journal citext not null,
  amount integer not null,
  primary key (the_date, stock_number, account, journal));
comment on table fp.acct_sales_gross_details is 'sales gross from accounting';
insert into fp.acct_sales_gross_details
select b.year_month, b.the_date, a.control, d.store, d.page, d.line, 
  d.account, e.journal_code, sum(-a.amount)::integer as amount
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = (select year_month from sls.months where open_closed = 'open')
join fin.dim_account c on a.account_key = c.account_key
join fp.gl_accounts d on c.account = d.account
  and (
    (d.page in (5,7,8,9,10,14) and (d.line between 1 and 19 or d.line between 25 and 40))  or
    (d.page = 16 and d.line between 1 and 6) or
    (d.page = 17 and d.line between 1 and 19))
join fin.dim_journal e on a.journal_key  = e.journal_key    
where a.post_status = 'Y'
group by b.year_month, b.the_date, a.control, d.store, d.page, d.line, 
  d.account, e.journal_code;


--------------------------------------------------------------------------
-- front gross daily snapshot
--------------------------------------------------------------------------
create table fp.sales_gross (
  year_month integer not null,
  the_date date primary key,
  gm_new_front_gross integer not null,
  hn_new_front_gross integer not null,
  gm_used_front__gross integer not null,
  hn_used_front_gross integer not null,
  gm_fi_before integer not null,
  hn_fi_before integer not null,
  gm_fi_comp integer not null,
  hn_fi_comp integer not null,
  gm_fi_chgbk integer not null,
  hn_fi_chgbk integer not null);
comment on table fp.sales_gross is 'daily snapshot of sales gross, new & used';

insert into fp.sales_gross
select (select year_month from sls.months where open_closed = 'open'), current_date, 
  sum(gm_new_front_gross) as gm_new_front_gross, 
  sum(hn_new_front_gross) as hn_new_front_gross, 
  sum(gm_used_front_gross) as gm_used_front_gross, 
  sum(hn_used_front_gross) as hn_used_front_gross,
  sum(gm_fi_before) as gm_fi_before,
  sum(hn_fi_before) as hn_fi_before,
  sum(gm_fi_comp) as gm_fi_comp,
  sum(hn_fi_comp) as hn_fi_comp,
  sum(gm_fi_chgbk) as gm_fi_chgbk,
  sum(hn_fi_chgbk) as hn_fi_chgbk
from (
  select 
    sum(amount) filter (where page < 16 and store = 'RY1') as gm_new_front_gross,
    sum(amount) filter (where page < 16 and store = 'RY2') as hn_new_front_gross,
    sum(amount) filter (where page = 16 and store = 'RY1') as gm_used_front_gross,
    sum(amount) filter (where page = 16 and store = 'RY2') as hn_used_front_gross,
    sum(amount) filter (where page = 17 and store = 'RY1' and line in (1,2,5,6,7,11,12,15,16,17)) as gm_fi_before,
    sum(amount) filter (where page = 17 and store = 'RY2' and line in (1,2,5,6,7,11,12,15,16,17)) as hn_fi_before,
    sum(amount) filter (where page = 17 and store = 'RY1' and line in (9, 19)) as gm_fi_comp,
    sum(amount) filter (where page = 17 and store = 'RY2' and line in (9, 19)) as hn_fi_comp,
    sum(amount) filter (where page = 17 and store = 'RY1' and line in (3, 13)) as gm_fi_chgbk,
    sum(amount) filter (where page = 17 and store = 'RY2' and line in (3, 13)) as hn_fi_chgbk
  from fp.acct_sales_gross_details
  where year_month = (select year_month from sls.months where open_closed = 'open')
  union all
  select  
    coalesce(sum(front_gross) filter (where vehicle_type = 'N' and store = 'RY1'), 0) as gm_new_front_gross,
    coalesce(sum(front_gross) filter (where vehicle_type = 'N' and store = 'RY2'), 0) as hn_new_front_gross,
    coalesce(sum(front_gross) filter (where vehicle_type = 'U' and store = 'RY1'), 0) as gm_used_front_gross,
    coalesce(sum(front_gross) filter (where vehicle_type = 'U' and store = 'RY2'), 0) as hn_used_front_gross,
    coalesce(sum(fi_gross) filter (where store = 'RY1'), 0) as gm_fi_before,
    coalesce(sum(fi_gross) filter (where store = 'RY2'), 0) as hn_fi_before,
    0 as gm_fi_comp,
    0 as hn_fi_comp,
    0 as gm_fi_chgbk,
    0 as hn_fi_chgbk       
  from fp.boarded_deals_details a
  where year_month = (select year_month from sls.months where open_closed = 'open')
    AND not exists (
        select stock_number 
        from fp.acct_sales_gross_details
        where stock_number = a.stock_number
          and year_month = (select year_month from sls.months where open_closed = 'open'))) c
group by (select year_month from sls.months where open_closed = 'open'), current_date;

--------------------------------------------------------------------------
-- sales recon, expenses & net
--------------------------------------------------------------------------
-- sales comp pvr
select 
  case 
    when g.gm_new_count + g.gm_used_count = 0 then 0
    else (f.gm_sales_comp/(g.gm_new_count + g.gm_used_count))::integer
  end as gm_sales_comp_pvr,
  case 
    when g.hn_new_count + g.hn_used_count = 0 then 0
    else (f.hn_sales_comp/(g.hn_new_count + g.hn_used_count))::integer
  end as hn_sales_comp_pvr  
from (
  select sum(a.amount) filter (where d.store = 'RY1') as gm_sales_comp,
    sum(a.amount) filter (where d.store = 'RY2') as hn_sales_comp
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201808 -- (select year_month from sls.months where open_closed = 'open')
  join fin.dim_account c on a.account_key = c.account_key
  join fp.gl_accounts d on c.account = d.account
    and d.page = 3
    and d.line = 4
  join fin.dim_journal e on a.journal_key  = e.journal_key    
  where a.post_status = 'Y') f
join fp.sales_counts g on 1 = 1
  and g.the_date = current_date;

  
-- recon/unit_sold
select 
  case
    when g.gm_used_count = 0 then 0
    else f.recon/g.gm_used_count
  end as recon_per_unit_sold
from (  
  select sum(a.amount)::integer as recon
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201809 -- (select year_month from sls.months where open_closed = 'open')
  join fin.dim_account c on a.account_key = c.account_key
    and c.account in ('164700','164701','164702','165100','165101','165102')
  join fin.dim_journal e on a.journal_key  = e.journal_key   
  where a.post_status = 'Y') f
join fp.sales_counts g on 1 = 1
  and g.the_date = current_date
    
select * from fp.sales_counts order by the_date

-- GM Sales Expenses & Net    
select 
  (sum(amount) filter (where page = 3 and line = 6 and col = 1)):: integer as new_policy,
  (sum(amount) filter (where page = 3 and line = 6 and col = 5)):: integer as used_policy,
  (sum(amount) filter (where page = 3 and line = 5)):: integer as delivery_expense,
  (sum(amount) filter (where page = 3 and line between 8 and 16)):: integer as personnel,
  (sum(amount) filter (where page = 3 and line between 18 and 39)):: integer as semi_fixed,
  sum(-amount)::integer as net
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = (select year_month from sls.months where open_closed = 'open')
join fin.dim_account c on a.account_key = c.account_key
join fp.gl_accounts d on c.account = d.account
  and (( page between 5 and 10) or -- new
    (page = 16 and line between 1 and 14) or  -- used
    (page = 3 and line between 1 and 57) or  --expenses
    (page = 17 and line between 1 and 20))  -- f&i
join fin.dim_journal e on a.journal_key  = e.journal_key    
where a.post_status = 'Y'
  and d.store = 'RY1'

--------------------------------------------------------------------------
-- all that's left for sales is inventory $$
--------------------------------------------------------------------------  





--------------------------------------------------------------------------
-- fixed
--------------------------------------------------------------------------  
based on P4L2 dept & col


select 
  (sum(amount) filter (where page = 16 and line between 21 and 34)):: integer as mech_gross,
  (sum(amount) filter (where page = 16 and line between 35 and 43)):: integer as bs_gross,
  (sum(amount) filter (where page = 16 and line between 45 and 61)):: integer as parts_gross
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201808 -- (select year_month from sls.months where open_closed = 'open')
join fin.dim_account c on a.account_key = c.account_key
join fp.gl_accounts d on c.account = d.account
  and ((page = 16 and line between 20 and 61) or  
    (page = 4))
join fin.dim_journal e on a.journal_key  = e.journal_key    
where a.post_status = 'Y'
  and d.store = 'RY1'

-- mech & bs ok, parts low
-- deal with parts when i get to it
select line, sum(amount)::integer
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201808 -- (select year_month from sls.months where open_closed = 'open')
join fin.dim_account c on a.account_key = c.account_key
join fp.gl_accounts d on c.account = d.account
  and page = 16 and line between 45 and 61
join fin.dim_journal e on a.journal_key  = e.journal_key    
where a.post_status = 'Y'
  and d.store = 'RY1'
group by line
order by line


-- main shop P4L2

select sum(amount)::integer
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201808 -- (select year_month from sls.months where open_closed = 'open')
join fin.dim_account c on a.account_key = c.account_key
join fp.gl_accounts d on c.account = d.account
  and page = 16 and line between 21 and 33
join fin.dim_journal e on a.journal_key  = e.journal_key    
where a.post_status = 'Y'
  and d.store = 'RY1'


-- main shop P4L2
-- break out departments

select c.department, sum(-a.amount)::integer
from fin.fact_gl a
join (
  select bb.account_key, aa.*
  from fp.gl_accounts aa
  inner join fin.dim_account bb on aa.account = bb.account
    and aa.page = 16 
    and aa.line between 21 and 33
    and aa.store = 'RY1') c on a.account_key = c.account_key
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201808 -- (select year_month from sls.months where open_closed = 'open')    
Join fin.dim_journal e on a.journal_key  = e.journal_key    
where a.post_status = 'Y'
group by c.department

-- 200 msec
select 
  (sum(amount) filter (where page = 3 and line = 6 and col = 1)):: integer as new_policy,
  (sum(amount) filter (where page = 3 and line = 6 and col = 5)):: integer as used_policy,
  (sum(amount) filter (where page = 3 and line = 5)):: integer as delivery_expense,
  (sum(amount) filter (where page = 3 and line between 8 and 16)):: integer as personnel,
  (sum(amount) filter (where page = 3 and line between 18 and 39)):: integer as semi_fixed,
  sum(-amount)::integer as net
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201808
join fin.dim_account c on a.account_key = c.account_key
join fp.gl_accounts d on c.account = d.account
  and (( page between 5 and 10) or -- new
    (page = 16 and line between 1 and 14) or  -- used
    (page = 3 and line between 1 and 57) or  --expenses
    (page = 17 and line between 1 and 20))  -- f&i
join fin.dim_journal e on a.journal_key  = e.journal_key    
where a.post_status = 'Y'
  and d.store = 'RY1'


-- this is all good (for august) except for parts gross
select 
  (sum(amount) filter (where d.page = 4 and d.department = 'body shop' and d.line between 8 and 16)):: integer as bs_personnel,
  (sum(amount) filter (where d.page = 4 and d.department = 'body shop' and d.line between 18 and 39)):: integer as bs_semi_fixed,
  (sum(amount) filter (where d.page = 4 and d.department = 'body shop' and d.line between 41 and 55)):: integer as bs_fixed,
  (sum(amount) filter (where d.page = 4 and d.department = 'body shop' and d.line = 59)):: integer as bs_,
  (sum(amount) filter (where d.page = 4 and d.department = 'body shop')):: integer as bs_total_exp,
  (sum(-amount) filter (where d.page = 16 and d.department = 'body shop'))::integer as bs_gross,
  (sum(-amount) filter (where d.department = 'body shop'))::integer as bs_net,
  (sum(amount) filter (where d.page = 4 and d.department = 'parts' and d.line between 8 and 16)):: integer as parts_personnel,
  (sum(amount) filter (where d.page = 4 and d.department = 'parts' and d.line between 18 and 39)):: integer as parts_semi_fixed,
  (sum(-amount) filter (where d.page = 16 and d.department = 'parts'))::integer as parts_gross,
  (sum(amount) filter (where d.page = 4 and d.department not in ('body shop', 'parts') and d.line between 8 and 16)):: integer as serv_personnel,
  (sum(amount) filter (where d.page = 4 and d.department not in ('body shop', 'parts') and d.line between 18 and 39)):: integer as serv_semi_fixed,
  (sum(-amount) filter (where d.page = 16 and d.department  not in ('body shop', 'parts')))::integer as serv_gross  
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201808
join fin.dim_account c on a.account_key = c.account_key
join fp.gl_accounts d on c.account = d.account
  and (( page = 4) or 
    (page = 16 and line between 21 and 55))  -- gross 
join fin.dim_journal e on a.journal_key  = e.journal_key    
where a.post_status = 'Y'
  and d.store = 'RY1'

select *
from fp.gl_accounts
where page = 4
  and store = 'ry1'
order by line

looks like i am missing parts split accounts

-- nope the accounts are there, must be in the routing
select *
from (
  select b.year_month, c.store, b.col, d.gl_account, sum(a.amount) 
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201808
    and b.page = 4
    and b.line = 59
  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  group by b.year_month, c.store, b.col, d.gl_account
  having sum(a.amount) <> 0) e
left join fp.gl_accounts f on e.gl_account = f.account



-- 
-- 201606 parts split ok
select b.year_month, c.store, b.col, sum(a.amount) 
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201808
  and b.page = 4
  and b.line = 59
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
group by b.year_month, c.store, b.col
having sum(a.amount) <> 0
-- parts split accounts
select b.year_month, c.store, b.col, d.gl_account, sum(a.amount) 
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201808
  and b.page = 4
  and b.line = 59
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
group by b.year_month, c.store, b.col, d.gl_account
having sum(a.amount) <> 0
-- 


ok i am mind fucking
gtrying to verify queires by looking at august financial statement
so i have to do it at the page 2 level
and there, the gross is wrong (because of parts, he thinks)

--------------------------------------------------------------------------------------
-- 9/23 go ahead and do expenses for all fixed depts
-- will have to do extra work for gross/net
--------------------------------------------------------------------------------------
-- this is all good (for august) except for parts gross
select 
  (sum(amount) filter (where d.department = 'body shop' and d.line between 8 and 16)):: integer as bs_personnel,
  (sum(amount) filter (where d.department = 'body shop' and d.line between 18 and 39)):: integer as bs_semi_fixed,
  (sum(amount) filter (where d.department = 'body shop' and d.line between 41 and 55)):: integer as bs_fixed,
  (sum(amount) filter (where d.department = 'body shop')):: integer as bs_total_exp,
  (sum(amount) filter (where d.department = 'parts' and d.line between 8 and 16)):: integer as parts_personnel,
  (sum(amount) filter (where d.department = 'parts' and d.line between 18 and 39)):: integer as parts_semi_fixed,
  (sum(amount) filter (where d.department not in ('body shop', 'parts') and d.line between 8 and 16)):: integer as serv_personnel,
  (sum(amount) filter (where d.department not in ('body shop', 'parts') and d.line between 18 and 39)):: integer as serv_semi_fixed
  
select d.*, e.journal_code, a.amount  
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201808
join fin.dim_account c on a.account_key = c.account_key
join fp.gl_accounts d on c.account = d.account
  and d.page = 4
  and d.line between 8 and 55
join fin.dim_journal e on a.journal_key  = e.journal_key    
where a.post_status = 'Y'
  and d.store = 'RY1'

-------------------------------------------------------------------------------
-- fixed expenses
-- date_issue  year_month
-------------------------------------------------------------------------------
drop table if exists fp.fixed_expenses_details cascade;
create table fp.fixed_expenses_details (
  year_month integer not null,
  the_date date not null,
  store citext not null,
  department citext not null,
  line integer not null,
  account citext not null, 
  journal citext not null,  
  control citext not null,
  amount integer not null,
  primary key (the_date, control, account, journal));
create index on fp.fixed_expenses_details(department);  
create index on fp.fixed_expenses_details(line);  
create index on fp.fixed_expenses_details(year_month);  
comment on table fp.fixed_expenses_details is 'sales gross from accounting';
insert into fp.fixed_expenses_details
select b.year_month, b.the_date, d.store, d.department, d.line, 
  d.account, e.journal_code, a.control, sum(-a.amount)::integer as amount
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = (select year_month from sls.months where open_closed = 'open')
join fin.dim_account c on a.account_key = c.account_key
join fp.gl_accounts d on c.account = d.account
  and d.page = 4
  and d.line between 8 and 55
join fin.dim_journal e on a.journal_key  = e.journal_key    
where a.post_status = 'Y'
group by b.year_month, b.the_date, a.control, d.store, d.department, d.line, 
  d.account, e.journal_code;  

Service
Detail
Car Wash
Quick Lane
Parts
Body Shop


update fp.flightplan x
set currently = (
  select sum(amount)
  from fp.fixed_expenses_details
  where store = 'RY1'
    and department = 'service'
    and line between 8 and 16
    and the_date between '09/01/2018' and current_date - 6) -- year_month = (select year_month from dds.dim_date where the_date = current_date - 1)
where seq = 24
  and the_date = current_date - 6;

update fp.flightplan x
set currently = (
  select sum(amount)
  from fp.fixed_expenses_details
  where store = 'RY1'
    and department = 'service'
    and line between 18 and 39
    and the_date between '09/01/2018' and current_date - 6) -- year_month = (select year_month from dds.dim_date where the_date = current_date - 1)
where seq = 25
  and the_date = current_date - 6;

select department, sum(amount) 
from fp.fixed_expenses_details
where line between 8 and 16
  and store = 'RY1'
group by department  

select department, line from fp.fixed_expenses_details group by department, line

select department, sum(amount) as personnel
from fp.fixed_expenses_details 
where store = 'RY1'
  and the_date between '09/01/2018' and current_date - 6
  and line between 8 and 16  
group by department  