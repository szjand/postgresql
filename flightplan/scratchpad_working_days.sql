﻿/*
gm service gross pace
select 399303.0/329866  -- 1.2105

select fp.get_working_days('10/28/2018', 'service') -- 1.2105

select * from dds.working_days where department = 'service' and the_Date between '10/01/2018' and  '10/31/2018'
select 23.0/19 -- 1.2105

looks like i fucked up working days, at least mon-fri
on saturday & sunday, elapsed should be 20, not 19 
service        wd of month  elapsed
10/26 fri      20             19
10/27 sat       0             19
10/28 sun       0             19
10/29 mon      21             20
10/30 tue      22             21
10/31 wed      23             22

from dds2/scripts/dimday/working_days-postgresql.sql


departments & working days: 
  mon - fri: service, parts, body shop, detail
  mon - sat: pdq, sales
  all exc thanksgiving, ny, xmas: carwash



-- temp table wd, create for one departemnt at a time
-- creates a table of all working days for that dept config (mon-fri, etc)
drop table if exists wd;
create temp table wd as
select 'service'::citext as department, x.the_date, 
  x.wd_in_year, 
  row_number() over (partition by the_year order by the_date) - 1 as wd_of_year_elapsed, 
  wd_in_year - row_number() over (partition by the_year order by the_date) + 1 as wd_of_year_remaining,
  row_number() over (partition by the_year order by the_date) as wd_of_year, 
  x.wd_in_month, 
  row_number() over (partition by year_month order by the_date) - 1 as wd_of_month_elapsed, 
  wd_in_month - row_number() over (partition by year_month order by the_date) + 1 as wd_of_month_remaining, 
  row_number() over (partition by year_month order by the_date) as wd_of_month,  
  x.wd_in_biweekly_pay_period,
  row_number() over (partition by biweekly_pay_period_sequence order by the_date) - 1 as wd_of_biweekly_pay_period_elapsed, 
  wd_in_biweekly_pay_period - row_number() over (partition by biweekly_pay_period_sequence order by the_date) + 1 as wd_of_bi_weekly_pay_period_remaining,   
  row_number() over (partition by biweekly_pay_period_sequence order by the_date) as wd_of_biweekly_pay_period
from (
  select the_year, year_month, the_date, biweekly_pay_period_sequence, day_name, first_of_month, last_of_month,
    count(*) over (partition by the_year) as wd_in_year,
    count(*) over (partition by year_month) as wd_in_month,
    count(*) over (partition by biweekly_pay_period_sequence) as wd_in_biweekly_pay_period
  from dds.dim_date 
  where not holiday
--     and day_of_week <> 1 -- mon-sat
    and day_of_week between 2 and 6 -- mon-fri
--   where the_year > 2008 -- carwash
--     and the_date not in (select the_date from dds.dim_date where month_of_year = 1 and holiday)
--     and the_date not in (select the_date from dds.dim_date where month_of_year = 11 and holiday)
--     and the_date not in (select the_date from dds.dim_date where month_of_year = 12 and holiday)  
  order by the_date) x;

drop table if exists wd_1;
create temp table wd_1 as
select *
from wd
where the_date between '10/01/2018' and '10/31/2018'
order by the_date;


-- this is about filling in the fields that are not working days
  select cal_date, 'carwash'::citext as department, day_name, 
    case x.day_of_month
      when 1 then 
        coalesce(coalesce(wd_in_year, lead(x.wd_in_year, 1) over (order by cal_date)), lead(x.wd_in_year, 2) over (order by cal_date))
      else 
        coalesce(coalesce(wd_in_year, lag(x.wd_in_year, 1) over (order by cal_date)), lag(x.wd_in_year, 2) over (order by cal_date)) 
    end as wd_in_year,
    case x.day_of_month
      when 1 then
        coalesce(coalesce(wd_of_year_elapsed, lead(x.wd_of_year_elapsed, 1) over (order by cal_date)), lead(x.wd_of_year_elapsed, 2) over (order by cal_date))
      else
        coalesce(coalesce(wd_of_year_elapsed, lag(x.wd_of_year_elapsed, 1) over (order by cal_date)), lag(x.wd_of_year_elapsed, 2) over (order by cal_date)) 
    end as wd_of_year_elapsed,
    case x.day_of_month
      when 1 then
        coalesce(coalesce(wd_of_year_remaining, lead(x.wd_of_year_remaining, 1) over (order by cal_date)), lead(x.wd_of_year_remaining, 2) over (order by cal_date))
      else
        coalesce(coalesce(wd_of_year_remaining, lag(x.wd_of_year_remaining, 1) over (order by cal_date)), lag(x.wd_of_year_remaining, 2) over (order by cal_date))
    end as wd_of_year_remaining,
    coalesce(wd_of_year, 0) as wd_of_year,
    case x.day_of_month
      when 1 then
        coalesce(coalesce(wd_in_month, lead(x.wd_in_month, 1) over (order by cal_date)), lead(x.wd_in_month, 2) over (order by cal_date))
      else
        coalesce(coalesce(wd_in_month, lag(x.wd_in_month, 1) over (order by cal_date)), lag(x.wd_in_month, 2) over (order by cal_date))
    end as wd_in_month,
    case x.day_of_month
      when 1 then
        coalesce(coalesce(wd_of_month_elapsed, lead(x.wd_of_month_elapsed, 1) over (order by cal_date)), lead(x.wd_of_month_elapsed, 2) over (order by cal_date)) 
      else
        coalesce(coalesce(wd_of_month_elapsed, lag(x.wd_of_month_elapsed, 1) over (order by cal_date)), lag(x.wd_of_month_elapsed, 2) over (order by cal_date)) 
    end as wd_of_month_elapsed,
    case x.day_of_month
      when 1 then
        coalesce(coalesce(wd_of_month_remaining, lead(x.wd_of_month_remaining, 1) over (order by cal_date)), lead(x.wd_of_month_remaining, 2) over (order by cal_date)) 
      else
        coalesce(coalesce(wd_of_month_remaining, lag(x.wd_of_month_remaining, 1) over (order by cal_date)), lag(x.wd_of_month_remaining, 2) over (order by cal_date)) 
    end as wd_of_month_remaining,
    coalesce(wd_of_month, 0) as wd_of_month,
    coalesce(coalesce(wd_in_biweekly_pay_period, lag(x.wd_in_biweekly_pay_period, 1) over (order by cal_date)), lag(x.wd_in_biweekly_pay_period, 2) over (order by cal_date)) as wd_in_biweekly_pay_period,
    coalesce(coalesce(wd_of_biweekly_pay_period_elapsed, lag(x.wd_of_biweekly_pay_period_elapsed, 1) over (order by cal_date)), lag(x.wd_of_biweekly_pay_period_elapsed, 2) over (order by cal_date)) as wd_of_biweekly_pay_period_elapsed,
    coalesce(coalesce(wd_of_bi_weekly_pay_period_remaining, lag(x.wd_of_bi_weekly_pay_period_remaining, 1) over (order by cal_date)), lag(x.wd_of_bi_weekly_pay_period_remaining, 2) over (order by cal_date)) as wd_of_bi_weekly_pay_period_remaining,
    coalesce(coalesce(wd_of_biweekly_pay_period, lag(x.wd_of_biweekly_pay_period, 1) over (order by cal_date)), lag(x.wd_of_biweekly_pay_period, 2) over (order by cal_date)) as wd_of_biweekly_pay_period
  from (  
    select a.the_date as cal_date, a.day_of_month, a.day_name, b.*
    from dds.dim_date a
    left join wd b on a.the_Date = b.the_date
    where a.year_month = 201810) x


select *
from (  
  select a.the_date as cal_date, a.day_of_month, a.day_name, b.*
  from dds.dim_date a
  left join wd b on a.the_Date = b.the_date
  where a.year_month = 201810) x
order by cal_date

select * from wd_1

select x.cal_Date, day_of_month, day_name, 
    case x.day_of_month
      when 1 then
        coalesce(coalesce(wd_of_month_elapsed, lead(x.wd_of_month_elapsed, 1) over (order by cal_date)), lead(x.wd_of_month_elapsed, 2) over (order by cal_date)) 
      else
        coalesce(coalesce(wd_of_month_elapsed, lag(x.wd_of_month_elapsed, 1) over (order by cal_date)), lag(x.wd_of_month_elapsed, 2) over (order by cal_date)) 
    end as wd_of_month_elapsed
from (  
  select a.the_date as cal_date, a.day_of_month, a.day_name, b.*
  from dds.dim_date a
  left join wd b on a.the_Date = b.the_date
  where a.year_month = 201810) x
order by cal_date



select * 
from wd where the_Date between '07/01/2018' and '07/31/2018'
order by the_date



select a.the_date as cal_date, a.day_of_month, a.day_name, b.*
from dds.dim_date a
left join wd b on a.the_Date = b.the_date
where a.year_month = 201810
order by a.the_date

-- null rows from wd
select a.day_name, min(a.the_date), max(a.the_date), count(*)
from dds.dim_date a
left join wd b on a.the_Date = b.the_date
where b.department is null
group by a.day_name


select a.the_date as cal_date, a.day_of_month, a.day_name, b.*
from dds.dim_date a
left join wd b on a.the_Date = b.the_date
where b.department is null
  and a.day_name not in ('saturday','sunday')
order by a.the_date


select a.the_date as cal_date, a.day_of_month, a.day_name, b.*
from dds.dim_date a
left join wd b on a.the_Date = b.the_date
where a.year_month = 201811
order by a.the_date

what i am trying to (correctly) derive is the assignment of wd elapsed/remaining on non working days

select x.cal_Date, day_of_month, day_name, day_of_week, wd_of_year, wd_of_month, wd_of_month_elapsed, wd_of_month_remaining,
  lag(x.wd_of_month_elapsed, 1) over (order by cal_date), 
  lag(x.wd_of_month_elapsed, 2) over (order by cal_date),
  lead(x.wd_of_month_elapsed, 1) over (order by cal_date), 
  lead(x.wd_of_month_elapsed, 2) over (order by cal_date) 
from (  
  select a.the_date as cal_date, a.day_of_month, a.day_of_week, a.day_name, b.*
  from dds.dim_date a
  left join wd b on a.the_Date = b.the_date
  where a.year_month = 201811) x
order by cal_date

if the day of week <> sunday then lead, 2 else lead, 1


-- so far this looks good for mon-fri 201810, 201811 
select x.cal_Date, day_of_month, day_name, day_of_week, wd_in_month, wd_of_year, wd_of_month, wd_of_month_elapsed, wd_of_month_remaining,
    case 
      when x.day_of_month = 1 then -- first of month
        coalesce(coalesce(wd_of_month_elapsed, lead(x.wd_of_month_elapsed, 1) over (order by cal_date)), lead(x.wd_of_month_elapsed, 2) over (order by cal_date)) 
      when x.day_of_week = 1 then -- sunday
        coalesce(wd_of_month_elapsed, lead(x.wd_of_month_elapsed, 1) over (order by cal_date))
      when x.day_of_week = 7 then -- saturday
        coalesce(wd_of_month_elapsed, lead(x.wd_of_month_elapsed, 2) over (order by cal_date))        
      else -- holiday
        coalesce(wd_of_month_elapsed, lead(x.wd_of_month_elapsed, 1) over (order by cal_date))
    end as wd_of_month_elapsed
from (  
  select a.the_date as cal_date, a.day_of_month, a.day_of_week, a.day_name, b.*
  from dds.dim_date a
  left join wd b on a.the_Date = b.the_date
  where a.year_month = 201809) x
order by cal_date


select * 
from wd
where the_date between '09/01/2018' and '09/30/2018'
order by the_date

got an issue with 3 non wd in a row, eg beginning of september

maybe its a matter of case when lead, 1 is null  when lead, 2 is null  when lead, 3 is null

-- this looks promising
select x.cal_Date, day_of_month, day_name, day_of_week, wd_in_month, wd_of_year, wd_of_month, wd_of_month_elapsed, wd_of_month_remaining,
    x.wd_of_month_elapsed is null,
    lead(x.wd_of_month_elapsed, 1) over (order by cal_date) is null,
    lead(x.wd_of_month_elapsed, 2) over (order by cal_date) is null,
    coalesce(wd_of_month_elapsed, lead(x.wd_of_month_elapsed, 3) over (order by cal_date)) as lead_3,
    coalesce(wd_of_month_elapsed, lead(x.wd_of_month_elapsed, 2) over (order by cal_date)) as lead_2,
    coalesce(wd_of_month_elapsed, lead(x.wd_of_month_elapsed, 1) over (order by cal_date)) as lead_1
from (  
  select a.the_date as cal_date, a.day_of_month, a.day_of_week, a.day_name, b.*
  from dds.dim_date a
  left join wd b on a.the_Date = b.the_date
  where a.year_month = 201801) x
order by cal_date

-- i believe this is it, looks like it works for up to 3 nulls in a row
select x.cal_Date, day_of_month, day_name, day_of_week, wd_in_month, wd_of_year, wd_of_month, wd_of_month_elapsed, wd_of_month_remaining, 
    case 
      when lead(x.wd_of_month_elapsed, 2) over (order by cal_date) is null then
        coalesce(wd_of_month_elapsed, lead(x.wd_of_month_elapsed, 3) over (order by cal_date)) 
      when lead(x.wd_of_month_elapsed, 1) over (order by cal_date) is null then 
        coalesce(wd_of_month_elapsed, lead(x.wd_of_month_elapsed, 2) over (order by cal_date)) 
      when x.wd_of_month_elapsed is null then
        coalesce(wd_of_month_elapsed, lead(x.wd_of_month_elapsed, 1) over (order by cal_date)) 
    end as wd_mo_elapsed
from (  
  select a.the_date as cal_date, a.day_of_month, a.day_of_week, a.day_name, b.*
  from dds.dim_date a
  left join wd b on a.the_Date = b.the_date
  where a.year_month = 201806) x
order by cal_date


-- there are none with 4 nulls in row
select * from (
  select a.the_date as cal_date, a.day_of_month, a.day_of_week, a.day_name, b.*,
    lead(b.wd_of_month_elapsed, 3) over (order by a.the_date) is null 
    and 
    lead(b.wd_of_month_elapsed, 2) over (order by a.the_date) is null 
    and 
    lead(b.wd_of_month_elapsed, 1) over (order by a.the_date) is null 
    and b.wd_of_month_elapsed is null as four
  from dds.dim_date a
  left join wd b on a.the_Date = b.the_date
) x where four 


-- select * from dds.dim_date where the_date >= '12/30/2023'


now how to update everything that is wrong
what i think is wrong are all elapsed and remaining
at least for mon-fri


-- lets look at mon-sat
drop table if exists wd;
create temp table wd as
select 'mon-sat'::citext as department, x.the_date, 
  x.wd_in_year, 
  row_number() over (partition by the_year order by the_date) - 1 as wd_of_year_elapsed, 
  wd_in_year - row_number() over (partition by the_year order by the_date) + 1 as wd_of_year_remaining,
  row_number() over (partition by the_year order by the_date) as wd_of_year, 
  x.wd_in_month, 
  row_number() over (partition by year_month order by the_date) - 1 as wd_of_month_elapsed, 
  wd_in_month - row_number() over (partition by year_month order by the_date) + 1 as wd_of_month_remaining, 
  row_number() over (partition by year_month order by the_date) as wd_of_month,  
  x.wd_in_biweekly_pay_period,
  row_number() over (partition by biweekly_pay_period_sequence order by the_date) - 1 as wd_of_biweekly_pay_period_elapsed, 
  wd_in_biweekly_pay_period - row_number() over (partition by biweekly_pay_period_sequence order by the_date) + 1 as wd_of_bi_weekly_pay_period_remaining,   
  row_number() over (partition by biweekly_pay_period_sequence order by the_date) as wd_of_biweekly_pay_period
from (
  select the_year, year_month, the_date, biweekly_pay_period_sequence, day_name, first_of_month, last_of_month,
    count(*) over (partition by the_year) as wd_in_year,
    count(*) over (partition by year_month) as wd_in_month,
    count(*) over (partition by biweekly_pay_period_sequence) as wd_in_biweekly_pay_period
  from dds.dim_date 
  where not holiday
    and day_of_week <> 1 -- mon-sat
  order by the_date) x;

select *
from (  
  select a.the_date as cal_date, a.day_of_month, a.day_name, b.*
  from dds.dim_date a
  left join wd b on a.the_Date = b.the_date
  where a.year_month = 201810) x
order by cal_date

-- looks like the same failure with mon-sat
-- wd_of_month_elapsed on sunday = saturday, should be saturday + 1
select b.day_name, a.* 
from dds.working_days a
join dds.dim_date b on a.the_date = b.the_date
where a.department = 'sales'
  and a.the_Date between '10/01/2018' and '10/31/2018'



-- looks like it works for mon-sat too
select x.cal_Date, day_of_month, day_name, day_of_week, wd_in_month, wd_of_year, wd_of_month, wd_of_month_elapsed, wd_of_month_remaining, 
  case 
    when lead(x.wd_of_month_elapsed, 2) over (order by cal_date) is null then
      coalesce(wd_of_month_elapsed, lead(x.wd_of_month_elapsed, 3) over (order by cal_date)) 
    when lead(x.wd_of_month_elapsed, 1) over (order by cal_date) is null then 
      coalesce(wd_of_month_elapsed, lead(x.wd_of_month_elapsed, 2) over (order by cal_date)) 
    when x.wd_of_month_elapsed is null then
      coalesce(wd_of_month_elapsed, lead(x.wd_of_month_elapsed, 1) over (order by cal_date)) 
  end as wd_mo_elapsed
from (  
  select a.the_date as cal_date, a.day_of_month, a.day_of_week, a.day_name, b.*
  from dds.dim_date a
  left join wd b on a.the_Date = b.the_date
  where a.year_month = 200901) x
order by cal_date

so, if i am understanding correclty
wd takes care of all the working days
the second query update all the non working days
eg
  mon-fri: all saturday, sunday and holidays are null
  mon-sat: all sunday and holdays are null


-- use dds.working_days_new
-- table created, has no rows
select * from  dds.working_days_new  
-- base values: department, the_Date
insert into dds.working_days_new(department, the_date)
select 'service', the_date
from dds.dim_date
where the_year > 2008;
-- create wd temp table for service (mon-fri) one row per working day
drop table if exists wd;
create temp table wd as
select 'service'::citext as department, x.the_date, year_month,  the_year, biweekly_pay_period_sequence, day_of_week,
  x.wd_in_year, 
  row_number() over (partition by the_year order by the_date) - 1 as wd_of_year_elapsed, 
  wd_in_year - row_number() over (partition by the_year order by the_date) + 1 as wd_of_year_remaining,
  row_number() over (partition by the_year order by the_date) as wd_of_year, 
  x.wd_in_month, 
  row_number() over (partition by year_month order by the_date) - 1 as wd_of_month_elapsed, 
  wd_in_month - row_number() over (partition by year_month order by the_date) + 1 as wd_of_month_remaining, 
  row_number() over (partition by year_month order by the_date) as wd_of_month,  
  x.wd_in_biweekly_pay_period,
  row_number() over (partition by biweekly_pay_period_sequence order by the_date) - 1 as wd_of_biweekly_pay_period_elapsed, 
  wd_in_biweekly_pay_period - row_number() over (partition by biweekly_pay_period_sequence order by the_date) + 1 as wd_of_bi_weekly_pay_period_remaining,   
  row_number() over (partition by biweekly_pay_period_sequence order by the_date) as wd_of_biweekly_pay_period
from (
  select the_year, year_month, the_date, biweekly_pay_period_sequence, day_name, first_of_month, last_of_month, day_of_week,
    count(*) over (partition by the_year) as wd_in_year,
    count(*) over (partition by year_month) as wd_in_month,
    count(*) over (partition by biweekly_pay_period_sequence) as wd_in_biweekly_pay_period
  from dds.dim_date 
  where the_year > 2008
    and not holiday
    and day_of_week between 2 and 6 -- mon-fri
  order by the_date) x;  

select * from wd  
order by the_date

select * from dds.working_days_new
order by the_Date

select * from dds.dim_Date limit 10

-- i think this is it 
-- nope 09/29 & 30/2018 wd_of_month_elapsed = 0: 3 nulls in a row crossing into next month
select a.the_date as cal_date, a.day_name, a.day_of_week, a.day_of_month, 
--   (select distinct wd_in_year from wd where extract(year from the_date) = extract(year from a.the_date)) as wd_in_year,
--   coalesce(
--     coalesce(
--       coalesce(b.wd_of_year_elapsed, 
--         lead(b.wd_of_year_elapsed, 1) over (order by a.the_date)), 
--           lead(b.wd_of_year_elapsed, 2) over (order by a.the_date)), 
--             lead(b.wd_of_year_elapsed, 3) over (order by a.the_date)) as wd_of_year_elapsed,
-- --   b.wd_of_year_remaining  -- outside the query: wd_in_year - wd_of_year_elapsed
--   coalesce(b.wd_of_year, 0) as wd_of_year,
  (select distinct wd_in_month from wd where year_month = a.year_month) as wd_in_month,
  b.wd_of_month_elapsed,
  coalesce(
    coalesce(
      coalesce(b.wd_of_month_elapsed, 
        lead(b.wd_of_month_elapsed, 1) over (order by a.the_date)), 
          lead(b.wd_of_month_elapsed, 2) over (order by a.the_date)), 
            lead(b.wd_of_month_elapsed, 3) over (order by a.the_date)) as wd_of_month_elapsed,
--   b.wd_of_month_remaining  -- outside the query: wd_in_month - wd_of_month_elapsed    
  coalesce(b.wd_of_month, 0) as wd_of_month,
  lag(a.day_of_week, 1) over (order by a.the_date), 
  a.year_month, lag(a.year_month, 1) over (order by a.the_date)
--   (select distinct wd_in_biweekly_pay_period from wd where biweekly_pay_period_sequence = a.biweekly_pay_period_sequence) as wd_in_biweekly_pay_period,
--   coalesce(
--     coalesce(
--       coalesce(b.wd_of_biweekly_pay_period_elapsed, 
--         lead(b.wd_of_biweekly_pay_period_elapsed, 1) over (order by a.the_date)), 
--           lead(b.wd_of_biweekly_pay_period_elapsed, 2) over (order by a.the_date)), 
--             lead(b.wd_of_biweekly_pay_period_elapsed, 3) over (order by a.the_date)) as wd_of_biweekly_pay_period_elapsed,
-- --   b.wd_of_biweekly_pay_period_elapsed  -- outside the query: wd_in_biweekly_pay_period_elapsed - wd_of_biweekly_pay_period_elapsed_elapsed  
--   coalesce(b.wd_of_biweekly_pay_period_elapsed, 0) as wd_of_biweekly_pay_period_elapsed                             
from dds.dim_date a
left join wd b on a.the_Date = b.the_date
-- where a.the_year > 2008  
where a.year_month between 201808 and 201811
order by a.the_date



select * from dds.working_Days where department = 'service' and the_date between '11/01/2018' and '11/30/2018' order by the_date


select a.the_date as cal_date, a.day_name, a.day_of_month
from dds.dim_date a
where a.the_year > 2008
  and (
    a.day_name in ('saturday', 'sunday')
    or a.holiday)
order by a.the_date
  


select a.the_date as cal_date, a.day_name, a.day_of_week, a.day_of_month, 
  (select distinct wd_in_month from wd where year_month = a.year_month) as wd_in_month,
  b.wd_of_month_elapsed,
  coalesce(
    coalesce(
      coalesce(b.wd_of_month_elapsed, 
        lead(b.wd_of_month_elapsed, 1) over (order by a.the_date)), 
          lead(b.wd_of_month_elapsed, 2) over (order by a.the_date)), 
            lead(b.wd_of_month_elapsed, 3) over (order by a.the_date)) as wd_of_month_elapsed,
  coalesce(b.wd_of_month, 0) as wd_of_month,
  lag(a.day_of_week, 1) over (order by a.the_date), 
  a.year_month, lag(a.year_month, 1) over (order by a.the_date)                            
from dds.dim_date a
left join wd b on a.the_Date = b.the_date
where a.year_month between 201808 and 201811
order by a.the_date


-- the issue
mon - sat
wd_of_year_elapsed on sunday 9/30/2018 s/b 230 but is 229
mon - fri
wd_of_month elapsed on saturday 9/29/2018 s/b 19 but is 18

the first non working day following a working day should increment wd_elapsed but does not


select a.the_date as cal_date, a.day_name, a.day_of_week, a.day_of_month, 
  (select distinct wd_in_month from wd where year_month = a.year_month) as wd_in_month,
  b.wd_of_month_elapsed, 
  coalesce(
    coalesce(
      coalesce(b.wd_of_month_elapsed, 
        lead(b.wd_of_month_elapsed, 1) over (order by a.the_date)), 
          lead(b.wd_of_month_elapsed, 2) over (order by a.the_date)), 
            lead(b.wd_of_month_elapsed, 3) over (order by a.the_date)) as wd_of_month_elapsed_1                     
from dds.dim_date a
left join wd b on a.the_Date = b.the_date
where a.year_month between 201808 and 201811
order by a.the_date

-- 11/12/2018
still struggling with this issue

rather than find an algorith that will correctly regenerate the entire table
simply fix those that are wrong



select a.the_date as cal_date, a.day_name, a.day_of_week, a.day_of_month, b.*
from dds.dim_date a
left join wd b on a.the_Date = b.the_date
where a.the_date between '08/01/2018' and '08/04/2018'
order by a.the_date


select a.the_date as cal_date, a.day_name, a.day_of_week, a.day_of_month, 
  b.the_date, b.wd_of_month_elapsed,
  coalesce(
    coalesce(
      coalesce(b.wd_of_month_elapsed, 
        lead(b.wd_of_month_elapsed, 1) over (order by a.the_date)), 
          lead(b.wd_of_month_elapsed, 2) over (order by a.the_date)), 
            lead(b.wd_of_month_elapsed, 3) over (order by a.the_date)) as wd_of_month_elapsed_1,    
  case
    when b.the_date is null  -- non working day
      and lag(b.the_date, 1) over (order by a.the_date) is not null -- previous day is working day
    then lag(b.wd_of_month_elapsed, 1) over (order by a.the_date) + 1
  end as wd_of_month_2              
from dds.dim_date a
left join wd b on a.the_Date = b.the_date
where a.the_Date between '08/01/2018' and '08/07/2018'
order by a.the_date


-- holy shit, the fucking coalesce takes care of that situation
-- the real issue is not that but -- nope 09/29 & 30/2018 wd_of_month_elapsed = 0: 3 nulls in a row crossing into next month

select a.the_date as cal_date, a.day_name, a.day_of_week, a.day_of_month, 
  b.the_date, b.wd_of_month_elapsed,
  coalesce(
    coalesce(
      coalesce(b.wd_of_month_elapsed, 
        lead(b.wd_of_month_elapsed, 1) over (order by a.the_date)), 
          lead(b.wd_of_month_elapsed, 2) over (order by a.the_date)), 
            lead(b.wd_of_month_elapsed, 3) over (order by a.the_date)) as wd_of_month_elapsed_1,    
  case
    when b.the_date is null  -- non working day
      and lag(b.the_date, 1) over (order by a.the_date) is not null -- previous day is working day
        then lag(b.wd_of_month_elapsed, 1) over (order by a.the_date) + 1
    
  end as wd_of_month_2              
from dds.dim_date a
left join wd b on a.the_Date = b.the_date
where a.the_Date between '09/28/2018' and '10/02/2018'
order by a.the_date



select *
from dds.working_days a
where a.the_Date between '09/28/2018' and '10/02/2018'
order by department, the_date  

so maybe what i can do is stick with the original dds.working_days table
and just fix those that are wrong



select a.the_date as cal_date, a.wd_of_month_elapsed, b.day_name, b.day_of_week, b.day_of_month, 
  c.the_date, c.wd_of_month_elapsed
from dds.working_days a
join dds.dim_date b on a.the_date = b.the_date
left join wd c on a.the_Date = c.the_date
where a.department = 'service' --mon - frid
  and a.the_Date between '09/28/2018' and '10/02/2018'
order by a.the_date


generate a table of non working days with the correct wd_of_month elapsed

select a.the_date as cal_date, a.day_name, a.day_of_week, a.day_of_month, 
  b.the_date, b.wd_of_month_elapsed,
  coalesce(
    coalesce(
      coalesce(b.wd_of_month_elapsed, 
        lead(b.wd_of_month_elapsed, 1) over (order by a.the_date)), 
          lead(b.wd_of_month_elapsed, 2) over (order by a.the_date)), 
            lead(b.wd_of_month_elapsed, 3) over (order by a.the_date)) as wd_of_month_elapsed_1,    
  case
    when b.the_date is null  -- non working day
      and lag(b.the_date, 1) over (order by a.the_date) is not null -- previous day is working day
        then lag(b.wd_of_month_elapsed, 1) over (order by a.the_date) + 1
    
  end as wd_of_month_2              
from dds.dim_date a
left join wd b on a.the_Date = b.the_date
where a.the_year > 2008
order by a.the_date


-- find just the 3 nulls in a row in the same month
-- happens when a holiday is on a friday 
-- and on the weekend before a monday holiday (memorial day, labor day & when 7/4 is on a monday)
select *
from (
  select a.the_date as cal_date, a.day_name, a.day_of_week, a.day_of_month, 
    b.the_date  as non_wd_1,
    lead(b.the_date, 1) over (order by a.the_date) as non_wd_2,
    lead(b.the_date, 2) over (order by a.the_date) as non_wd_3 
  from dds.dim_date a
  left join wd b on a.the_Date = b.the_date
  where a.the_year > 2008) x
where non_wd_1 is null
  and non_wd_2 is null
  and non_wd_3 is null  
order by x.cal_date


select b.day_name, a.*
from dds.working_days a
join dds.dim_date b on a.the_date = b.the_date
  and b.year_month between 201808 and 201810
where a.department = 'service'  
order by a.the_date  




-- well, here is another problem push this to the bottom
-- missing all (mostly) data for these dates
select *
from dds.working_days
where wd_in_month is null
  and department = 'service'
union 
select *
from dds.working_days
where wd_in_year is null
  and department = 'service'
order by the_date
*/
------------------------------------------------------------------------------------
-- 11/12  lets start fresh with dds.working_days_new
------------------------------------------------------------------------------------

DROP TABLE if exists dds.working_days_new cascade;

CREATE TABLE dds.working_days_new
(
  department citext NOT NULL,
  the_year integer not null,
  year_month integer not null,
  biweekly_pay_period_sequence integer not null,
  the_date date NOT NULL,
  day_of_week integer not null,
  day_of_month integer not null,
  day_of_year integer not null,
  last_of_month date,
  wd_in_year integer,
  wd_of_year_elapsed integer,
  wd_of_year_remaining integer,
  wd_of_year integer,
  wd_in_month integer,
  wd_of_month_elapsed integer,
  wd_of_month_remaining integer,
  wd_of_month integer,
  wd_in_biweekly_pay_period integer,
  wd_of_biweekly_pay_period_elapsed integer,
  wd_of_bi_weekly_pay_period_remaining integer,
  wd_of_biweekly_pay_period integer,
  CONSTRAINT working_days_new_pkey PRIMARY KEY (department, the_date));
COMMENT ON TABLE dds.working_days_new
  IS 'department specific working day calendar, 2009 - 2023, sales,pdq:mon-sat, service,parts,body shop,detail: mon-fri, carwash: all except xmas, new year, thanksgiving';

CREATE INDEX working_days_new_the_date_idx
  ON dds.working_days_new
  USING btree
  (the_date);


DROP TABLE if exists dds.working_days_2 cascade;
CREATE TABLE dds.working_days_2
(
  department citext NOT NULL,
  the_year integer not null,
  year_month integer not null,
  biweekly_pay_period_sequence integer not null,
  the_date date NOT NULL,
  day_of_week integer not null,
  day_of_month integer not null,
  day_of_year integer not null,
  last_of_month date,
  wd_in_year integer,
  wd_of_year_elapsed integer,
  wd_of_year_remaining integer,
  wd_of_year integer,
  wd_in_month integer,
  wd_of_month_elapsed integer,
  wd_of_month_remaining integer,
  wd_of_month integer,
  wd_in_biweekly_pay_period integer,
  wd_of_biweekly_pay_period_elapsed integer,
  wd_of_bi_weekly_pay_period_remaining integer,
  wd_of_biweekly_pay_period integer,
  CONSTRAINT working_days_2_pkey PRIMARY KEY (department, the_date));
COMMENT ON TABLE dds.working_days_2
  IS 'department specific working day calendar, 2009 - 2023, sales,pdq:mon-sat, service,parts,body shop,detail: mon-fri, carwash: all except xmas, new year, thanksgiving';

CREATE INDEX working_days_2_the_date_idx
  ON dds.working_days_2
  USING btree
  (the_date);

  
-- populate dds.working_days_new, one department at a time
-- from that populate dds.working_days_2;  
truncate dds.working_days_new;
-- base rows, 1 row/day for department
insert into dds.working_days_new(department, the_year, year_month, biweekly_pay_period_sequence, the_date,
  day_of_week, day_of_month, day_of_year,last_of_month)
select 'carwash', the_year, year_month, biweekly_pay_period_sequence, the_date, 
  day_of_week, day_of_month, day_of_year, last_of_month
from dds.dim_date
where the_year > 2008;

-- create the base wd table for the department
-- this table includes working days only
drop table if exists wd;
create temp table wd as
select /*'parts'::citext as department,*/ x.the_date, x.the_year, x.year_month, x.biweekly_pay_period_sequence, 
  x.wd_in_year, 
  row_number() over (partition by the_year order by the_date) - 1 as wd_of_year_elapsed, 
  wd_in_year - row_number() over (partition by the_year order by the_date) + 1 as wd_of_year_remaining,
  row_number() over (partition by the_year order by the_date) as wd_of_year, 
  x.wd_in_month, 
  row_number() over (partition by year_month order by the_date) - 1 as wd_of_month_elapsed, 
  wd_in_month - row_number() over (partition by year_month order by the_date) + 1 as wd_of_month_remaining, 
  row_number() over (partition by year_month order by the_date) as wd_of_month,  
  x.wd_in_biweekly_pay_period,
  row_number() over (partition by biweekly_pay_period_sequence order by the_date) - 1 as wd_of_biweekly_pay_period_elapsed, 
  wd_in_biweekly_pay_period - row_number() over (partition by biweekly_pay_period_sequence order by the_date) + 1 as wd_of_bi_weekly_pay_period_remaining,   
  row_number() over (partition by biweekly_pay_period_sequence order by the_date) as wd_of_biweekly_pay_period
from (
  select the_year, year_month, the_date, biweekly_pay_period_sequence, day_name, first_of_month, last_of_month,
    count(*) over (partition by the_year) as wd_in_year,
    count(*) over (partition by year_month) as wd_in_month,
    count(*) over (partition by biweekly_pay_period_sequence) as wd_in_biweekly_pay_period
  from dds.dim_date 
--   where the_year > 2008
--     and not holiday
--     and day_of_week <> 1 -- mon-sat
--     and day_of_week between 2 and 6 -- mon-fri
  where the_year > 2008 
    and the_date not in (select the_date from dds.dim_date where month_of_year = 1 and holiday)
    and the_date not in (select the_date from dds.dim_date where month_of_year = 11 and holiday)
    and the_date not in (select the_date from dds.dim_date where month_of_year = 12 and holiday)  
  order by the_date) x;  


-- update working days in year, month, pay_period
update dds.working_days_new z
set wd_in_year = x.wd_in_year
from (
  select the_year, wd_in_year
  from wd
  group by the_year, wd_in_year) x
where z.the_year = x.the_year;  

update dds.working_days_new z
set wd_in_month = x.wd_in_month
from (
  select year_month, wd_in_month
  from wd
  group by year_month, wd_in_month) x
where z.year_month = x.year_month;  

update dds.working_days_new z
set wd_in_biweekly_pay_period = x.wd_in_biweekly_pay_period
from (
  select biweekly_pay_period_sequence, wd_in_biweekly_pay_period
  from wd
  group by biweekly_pay_period_sequence, wd_in_biweekly_pay_period) x
where z.biweekly_pay_period_sequence = x.biweekly_pay_period_sequence;

-- update working day of year, month, pay period
update dds.working_days_new z
set wd_of_year = x.wd_of_year,
    wd_of_month = x.wd_of_month,
    wd_of_biweekly_pay_period = x.wd_of_biweekly_pay_period
from (    
  select a.the_date, 
    coalesce(b.wd_of_year, 0) as wd_of_year,
    coalesce(b.wd_of_month, 0) as wd_of_month,
    coalesce(b.wd_of_biweekly_pay_period, 0) as wd_of_biweekly_pay_period
  from dds.working_days_new a
  left join wd b on a.the_date = b.the_date) x
where z.the_date = x.the_date;

-- update wd of year elapsed
update dds.working_days_new z
set wd_of_year_elapsed = x.wd_of_year_elapsed_1
from (
  select a.wd_in_year, a.the_date, a.day_of_week, a.day_of_year, b.the_year, b.wd_of_year_elapsed,
    case 
      when a.day_of_year = 1 then 0 -- new years day
      when b.the_date is null  -- non working day
        and lag(b.the_date, 1) over (order by a.the_date) is not null -- previous day is working day
            then lag(b.wd_of_year_elapsed, 1) over (order by a.the_date) + 1
      when b.the_date is null  -- non working day
        and lag(b.the_date, 1) over (order by a.the_date) is null -- previous day is a non working day
            then coalesce(lead(b.wd_of_year_elapsed, 1) over (order by a.the_date), lead(b.wd_of_year_elapsed, 2) over (order by a.the_date))
      else b.wd_of_year_elapsed
    end as wd_of_year_elapsed_1
  from dds.working_days_new a
  left join wd b on a.the_date = b.the_date) x
where z.the_date = x.the_date; 

-- udpate wd_of_year_remaining
update dds.working_days_new
set wd_of_year_remaining = wd_in_year - wd_of_year_elapsed;


-- wd of month elapsed 
update dds.working_days_new z
set wd_of_month_elapsed = x.wd_of_month_elapsed_1
from (
  select a.wd_in_month, a.the_date, a.day_of_week, a.day_of_month, a.last_of_month, b.the_year, b.wd_of_month_elapsed,
    case 
      when a.day_of_month = 1 then 0 -- new years day
      when b.the_date is null and day_of_month = 2 then 0     
      when b.the_date is null and day_of_month = 3
        and lag(b.the_date, 1) over (order by a.the_date) is null then 0 -- 1st, 2nd & 3rd of month all non working   
      when b.the_date is null  -- non working day
        and lag(b.the_date, 1) over (order by a.the_date) is not null -- previous day is working day
            then lag(b.wd_of_month_elapsed, 1) over (order by a.the_date) + 1
      when b.the_date is null  -- non working day
        and lag(b.the_date, 1) over (order by a.the_date) is null -- previous day is a non working day
          and lag(b.the_date, 2) over (order by a.the_date) is not null -- 2 days ago a working day
            then lag(b.wd_of_month_elapsed, 2) over (order by a.the_date) + 1
      when b.the_date is null  -- non working day
        and lag(b.the_date, 1) over (order by a.the_date) is null -- previous day is a non working day
          and lag(b.the_date, 2) over (order by a.the_date) is null -- 2 days ago a non working day
            then lag(b.wd_of_month_elapsed, 3) over (order by a.the_date) + 1                      
      else b.wd_of_month_elapsed
    end as wd_of_month_elapsed_1
  from dds.working_days_new a
  left join wd b on a.the_date = b.the_date) x
where z.the_date = x.the_date;  

-- udpate wd_of_year_remaining
update dds.working_days_new
set wd_of_month_remaining = wd_in_month - wd_of_month_elapsed;

-- -- tests for wd of month elapsed anomalies
-- select * from wtf where wd_of_month_elapsed_1 is null
-- select * from wtf where wd_of_month_elapsed_1 > wd_in_month
-- select * from wtf where wd_of_month_elapsed_1 = 0 and day_of_month > 3

-- 
-- insert into dds.working_days_2
-- select * from dds.working_days_new;
-- 
-- select * from dds.working_days_2 
--   where wd_of_year_elapsed is null 
--   or wd_of_month_elapsed is null
--   or wd_in_year is null
--   or wd_in_month is null


11/13/18
have not done biweekly yet

DROP TABLE if exists dds.working_days cascade;
CREATE TABLE dds.working_days
(
  department citext NOT NULL,
  the_year integer not null,
  year_month integer not null,
  biweekly_pay_period_sequence integer not null,
  the_date date NOT NULL,
  day_of_week integer not null,
  day_of_month integer not null,
  day_of_year integer not null,
  last_of_month date,
  wd_in_year integer,
  wd_of_year_elapsed integer,
  wd_of_year_remaining integer,
  wd_of_year integer,
  wd_in_month integer,
  wd_of_month_elapsed integer,
  wd_of_month_remaining integer,
  wd_of_month integer,
  wd_in_biweekly_pay_period integer,
  wd_of_biweekly_pay_period_elapsed integer,
  wd_of_bi_weekly_pay_period_remaining integer,
  wd_of_biweekly_pay_period integer,
  CONSTRAINT working_days_pkey PRIMARY KEY (department, the_date));
COMMENT ON TABLE dds.working_days
  IS 'department specific working day calendar, 2009 - 2023, sales,pdq:mon-sat, service,parts,body shop,detail: mon-fri, carwash: all except xmas, new year, thanksgiving';

CREATE INDEX working_days_the_date_idx
  ON dds.working_days
  USING btree
  (the_date);

insert into dds.working_days
select * from dds.working_days_2;


DROP TABLE if exists dds.working_days_new cascade;

DROP TABLE if exists dds.working_days_2 cascade;