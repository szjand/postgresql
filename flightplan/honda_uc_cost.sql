﻿look at honda uc gross, deals vs non deals

need to include wholesale
drop table if exists step_2;
create temp table step_2 as
-- include stocknumber on each row
select year_month, the_date, store, page, line, line_label, control, sum(unit_count) as unit_count, sum(amount) as amount
from (
  select b.year_month, b.the_date, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201808
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select b.year_month, f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201808
      and (
--         (b.page in (5,7,8,9,10,14) and (b.line between 1 and 19 or b.line between 25 and 40)) 
--         or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
--         or
--         (b.page = 17 and b.line between 1 and 20))-- f/i
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account and b.year_month = d.year_month
  where a.post_status = 'Y') h
group by year_month, the_date, store, page, line, line_label, control;


-- so this is the ry2 used from the statement
drop table if exists step_3; --21996
create temp table step_3 as
select a.year_month, a.the_date, a.store, a.page, a.line::integer, a.line_label, a.control, sum(unit_count) as unit_count,
  case
    when store = 'ry1' and page = 5 then 'Chevrolet'
    when store = 'ry1' and page = 8 then 'Buick'
    when store = 'ry1' and page = 9 then 'Cadillac'
    when store = 'ry1' and page = 10 then 'GMC'
    when store = 'ry1' and page = 14 then 'MV-1'
    when page = 16 and line in (1,4) then 'Certified'
    when page = 16 and line in (2,5) then 'Other'
    when store = 'ry2' and page = 7 then 'Honda'
    when store = 'ry2' and page = 14 then 'Nissan'
  end as make,
  case 
    when page < 16 and line < 20 then 'Car'
    when page < 16 and line > 20 then 'Truck'
    when page = 16 and line < 3 then 'Car'
    when page = 16 and line > 3 then 'Truck'
  end as shape,
  extract(day from the_date)::integer as day_of_month, extract(doy from the_date)::integer as day_of_year
from step_2 a
inner join (
  select year_month, control
  from step_2
  group by year_month, control
  -- includes out of month unwinds
  having sum(unit_count) <> 0) b on a.control = b.control and a.year_month = b.year_month
where a.store = 'ry2'  
group by a.year_month, a.the_date, a.store, a.page, a.line, a.line_label, a.control

-- and here is the ry2 uc gross from statement
-- need to include wholesale
drop table if exists step_4;
create temp table step_4 as
select b.the_date, a.control, -a.amount as amount, d.*, e.journal_code, f.description
-- select d.store, line, sum(amount) 
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join (
  select b.year_month, f.store, d.gl_account, b.page, b.line::integer
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201808
    and (
--       (b.page in (5,7,8,9,10,14) and (b.line between 1 and 19 or b.line between 25 and 40)) 
--       or
      (b.page = 16 and b.line between 1 and 14))
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
    and f.store = 'ry2') d on b.year_month = d.year_month
    and c.account = d.gl_account
inner join fin.dim_journal e on a.journal_key  = e.journal_key    
left join fin.dim_gl_description f on a.gl_description_key = f.gl_description_key
  and b.the_date between f.row_from_date and f.row_thru_date
where a.post_status = 'Y'; 


select line, sum(amount)::integer
-- select sum(amount)::integer
-- select *
from step_4
group by line
order by line

-- -12,190 non deal transactions
select a.control, a.gl_account, sum(a.amount)
-- select sum(amount)::integer
from step_4 a
full outer join step_3 b on a.control = b.control
where b.control is null
group by a.control, a.gl_account
having sum(a.amount) <> 0
 
select * 
from step_4
where control = 'h11282b'


H11282B -2800
sold on 7/12 no due bill
corrected account

h11040a -3315
sold 7/19 no due bill
ro 2781882 repl transmission per sales work due  internal

transmission noted in inspection, pencil whipped in tool, it was never
fixed  before sale

select * 
from step_4
where control = 'h11040a'

walk fb: Dan L... says they used v auto .. anyway they caught the 5th gear grinding noise but only 
figured about 1/2 of what it costs to repair for the trany. might suggest you get an estimate o
n this type of noise if you arent sure. 
also this time of year these sports cars are dropping fast so would take that 
into consideration and v or any tool wouldnt be able to do that anyway we are 
in deep here and writing it back/ Needs rear bumper replaced also





H9742B -3671
sold 5/18  no due bill
body recon 900, in tool, ro 18059537: $1071
VehicleEvaluation_ReconNote	5/16/2018 9:27:00 AM	doesnt need much, detail is about it. maybe chips away	Jared Langenstein

walk feedback: sold
RO 18061040 (cust pay)


select * 
from step_4
where control = 'H9742B'


select c.the_date, a.*
from ads.ext_fact_repair_order a
inner join ads.ext_dim_vehicle b on a.vehiclekey = b.vehiclekey
  and b.vin = '1GT424E81FF109199'
inner join dds.dim_date c on a.opendatekey = c.date_key  