﻿do
$$
declare
--     _store integer := 39; -- ry1
    _store integer := 40; -- ry2
    _n_u_type citext := 'N';
    _est_or_actual citext := 'actual';
    _front_fi_total citext := 'Front';
    _date date := '12/31/2018';
begin   

drop table if exists wtf;
create temp table wtf as 

with accounting_ids as (
  select aa.control, (sum(-aa.amount))::integer as the_sum,
    coalesce((sum(-aa.amount) filter (where c.the_page < 17))::integer, 0) as front_gross,
    coalesce((sum(-aa.amount) filter (where c.the_page = 17))::integer, 0) as back_gross
    from fin.fact_gl aa
    inner join fin.dim_account b on aa.account_key = b.account_key
    inner join (
      select distinct case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code,
      a.fxmpge as the_page, a.fxmlne as line, trim(b.g_l_acct_number) as gl_account
      from arkona.ext_eisglobal_sypffxmst a
      inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
        and a.fxmcyy = b.factory_financial_year
      where a.fxmcyy = 2018
        and coalesce(b. consolidation_grp, '1') <> '3'
        and b.g_l_acct_number <> ''
        and (
          (a.fxmpge between 5 and 15) or
          (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
          (a.fxmpge = 17 and a.fxmlne between 1 and 19))) c on b.account = c.gl_account
    where aa.post_status = 'Y' 
    and aa.control in (
      select distinct  b.stock_number
      from board.sales_board b  
      where boarded_date between date_trunc('MONTH',coalesce(_date,current_date))::DATE and coalesce(_date,current_date)
      and is_deleted = false
        )
    group by aa.control) 
select case when board_sub_type = 'N/A' then -1 else 1 end as counter,  stock_number, boarded_date, 
  case when board_sub_type = 'N/A' then board_type else board_sub_type end as board, 
  case 
    when _est_or_actual = 'est' and _front_fi_total = 'front' and front_gross is null and board_type <> 'Back-on' and is_backed_on = false then deal_front 
    when _est_or_actual = 'est' and _front_fi_total = 'fi' and front_gross is null and board_type <> 'Back-on' and is_backed_on = false then deal_fi
    when _est_or_actual = 'est' and _front_fi_total = 'total' and front_gross is null and board_type <> 'Back-on' and is_backed_on = false then deal_fi + deal_front
    when _est_or_actual = 'actual' and _front_fi_total = 'front' then front_gross
    when _est_or_actual = 'actual' and _front_fi_total = 'fi' then back_gross
    when _est_or_actual = 'actual' and _front_fi_total = 'total' then front_gross + back_gross
  end as value
from board.sales_board b
left join accounting_ids a on stock_number = a.control
inner join board.board_types c on c.board_type_key = b.board_type_key  and( board_sub_type in ('Retail','Fleet') or board_type = 'Back-on')
inner join (
  select board_id, vehicle_type,sale_code
  from board.daily_board
  group by  board_id, vehicle_type,sale_code) dd on b.board_id = dd.board_id
left join (
  select board_id ,amount as deal_fi
  from board.deal_gross d
  where amount_type = 'fi_gross' 
  and current_row = true) d on b.board_id = d.board_id 
left join (
  select board_id ,amount as deal_front
  from board.deal_gross d
  where amount_type = 'front_end_gross'
  and current_row = true ) e on b.board_id = e.board_id 
where boarded_date between date_trunc('MONTH',coalesce(_date,current_date))::DATE and coalesce(_date,current_date)
 and is_deleted = false
 and case when _n_u_type = 'both' then 1 = 1 else vehicle_type = _n_u_type end
 and store_key = _store;
end
$$;

select * from wtf;

select sum(value) from wtf

then for the "doc" piece (assuming fp.acct_sales_gross_details from daily flightplan is current)

drop table if exists doc;
create temp table doc as 
select stock_number, sum(amount) as doc_gross
from fp.acct_sales_gross_details 
where year_month = 201812
  and store = 'ry2'
  and page < 16
group by stock_number  

and the resulting spreadsheet for jeri

select *--, sum(coalesce(board_gross, 0)) over (), sum(coalesce(doc_gross, 0)) over (), abs(coalesce(doc_gross, 0)) - abs(coalesce(board_gross, 0)) as diff
-- select sum(board_gross), sum(doc_gross)
from (
select stock_number, board, sum(value) as board_gross
from wtf
group by stock_number, board) a
full outer join doc b on a.stock_number = b.stock_number