﻿-- 6/1/19 this matches db2
with  
  inventory as (
    select control
    from fin.fact_gl aa
    join (
      select account_key
      from fin.dim_account
      where account_type_code = '1'
        and department_code = 'NC'
        and typical_balance = 'Debit'
        and account <> '126104'
        and left(account, 1) <> '3') bb on aa.account_key = bb.account_key
    join dds.dim_date cc on aa.date_key = cc.date_key
      and cc.the_date between current_date - 900 and current_date
    where aa.post_status = 'Y'
    group by aa.control
    having sum(aa.amount) < 100)
select b.control
from (
  select control
  from fin.fact_gl a
  join fin.dim_account b on a.account_key = b.account_key
    and b.account in ('130402','230402')
  where a.post_status = 'Y'
  group by a.control
  having sum(a.amount) <> 0) b
join inventory c on b.control = c.control
order by b.control  
  