﻿



!!!!!!!!!!!!!!!!!!!! DO THIS IN DB2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
1/2/21 went in to the office, ran this on dbw
came up with 3 additional vehicles
BUT, this also showed the same 3 additional vehicles
lets see if we can see the date_in_invent

-----------------------------------------------------------------------------------------------------------------
--< 06/01/2023
-----------------------------------------------------------------------------------------------------------------
-- can't connect to db2 from california?!?!?!?
-- so, try it in postgres, change it to ext.inpmast and change the dates
select aa.stock_number, aa.vin, bb.ad_pack_amount
from ( -- this gives me stock number and vin of all nc currently in inventory
  select a.control as stock_number, min(c.inpmast_vin) as vin
  from fin.fact_gl a 
  join fin.dim_account b on a.account_key = b.account_key
    and b.account_type_code = '1'
    and b.department_code = 'NC'
    and b.typical_balance = 'Debit'
    and b.account  <> '126104'
    and b.account not like '3%'
  left join arkona.ext_inpmast c on a.control = c.inpmast_stock_number
  join dds.dim_date d on a.date_key = d.date_key
    and d.the_date < '06/01/2023'
  where a.post_status = 'Y'
  group by a.control
  having sum(a.amount) > 10000) aa  
join (
  select a.control, -a.amount as ad_pack_amount
  from fin.fact_gl a
  join fin.dim_account b on a.account_key = b.account_key
    and b.account = '133211'
  join dds.dim_date c on a.date_key = c.date_key
    and c.the_date < '06/01/2023'
  where a.post_status = 'Y') bb  on left(aa.stock_number, 6) = bb.control


-----------------------------------------------------------------------------------------------------------------
--/> 06/01/2023
-----------------------------------------------------------------------------------------------------------------
  
1/1/21 working from home, lets try to translate the db2 query into postgres
shit hoped i could get to the sql window in the ui, but no 5250, no sql is my guess

seems like the issue was jeri making changes in accounting on the 1st, thereby invalidating
fin.fact_gl generated at 2AM on the 1st

select a.stock_number, vin, -b.transaction_amount as ad_pack_amount 
from ( -- this gives me stock number and vin of all nc currently in inventory
  select trim(a.control_number) as stock_number,  min(c.inpmast_vin) as vin
  from rydedata.glptrns a
  join rydedata.glpmast b on trim(a.account_number) = trim(b.account_number)
    and b.year = 2020
    and b.account_type = '1'
    and b.department = 'NC'
    and b.typical_balance = 'D'
    and trim(b.account_number) <> '126104'
    and trim(b.account_number) not like '3%' --OR
  --where a.control_number like '%R'  
  join rydedata.inpmast c on trim(a.control_number) = trim(c.inpmast_stock_number)
  where a.post_status = 'Y'
  group by (a.control_number)
  having sum(a.transaction_amount) > 10000) a  
join rydedata.glptrns b on left(trim(a.stock_number), 6) = trim(b.control_number)
  and trim(b.account_number) = '133211'
  and b.post_status = 'Y'  

go into the store tomorrow after grocery shopping and compare the 2 results

-- this is the detail
-- 1/13/2021 from jeri: Are you able to run this again looking back to 12/31?  I think some vehicles got added when we reconciled inventory.  
-- added the date to each query
select aa.stock_number, aa.vin, bb.ad_pack_amount
from ( -- this gives me stock number and vin of all nc currently in inventory
  select a.control as stock_number, min(c.inpmast_vin) as vin
  from fin.fact_gl a 
  join fin.dim_account b on a.account_key = b.account_key
    and b.account_type_code = '1'
    and b.department_code = 'NC'
    and b.typical_balance = 'Debit'
    and b.account  <> '126104'
    and b.account not like '3%'
  left join arkona.ext_inpmast c on a.control = c.inpmast_stock_number
  join dds.dim_date d on a.date_key = d.date_key
    and d.the_date < '01/01/2021'
  where a.post_status = 'Y'
  group by a.control
  having sum(a.amount) > 10000) aa  
join (
  select a.control, -a.amount as ad_pack_amount
  from fin.fact_gl a
  join fin.dim_account b on a.account_key = b.account_key
    and b.account = '133211'
  join dds.dim_date c on a.date_key = c.date_key
    and c.the_date < '01/01/2021'
  where a.post_status = 'Y') bb  on left(aa.stock_number, 6) = bb.control


-- 05/01/20 355 @ 242.78 per (86.187.47) 
-- 06/01/20 279 @ 235.29 per
-- 07/01/20 205 @ 233.28 per
-- 08/01/20 203 @ 236.97 per
-- 09/01/20 204 @ 224.95 per (45890.68)
-- 10/01/20 220 @ 232.54 per (51159.14)
-- 11/01/20 169 @ 228.13 per (38555.09)
-- 12/01/20 178 @ 284.50 per (44233.94)
-- 01/01/21 143 @ 234.89 per (33589.80)
-- 01/02/21 146 @ 234.84 per (34285.91)
-- and this is the summary
select count(*), sum(ad_pack_amount)/count(*), sum(ad_pack_amount)
from ( -- this gives me stock number and vin of all nc currently in inventory
  select a.control as stock_number, min(c.inpmast_vin) as vin
  from fin.fact_gl a 
  join fin.dim_account b on a.account_key = b.account_key
    and b.account_type_code = '1'
    and b.department_code = 'NC'
    and b.typical_balance = 'Debit'
    and b.account  <> '126104'
    and b.account not like '3%'
  join arkona.ext_inpmast c on a.control = c.inpmast_stock_number
  where a.post_status = 'Y'
  group by a.control
  having sum(a.amount) > 10000) aa  
join (
  select a.control, -a.amount as ad_pack_amount
  from fin.fact_gl a
  join fin.dim_account b on a.account_key = b.account_key
    and b.account = '133211'
  where a.post_status = 'Y') bb  on left(aa.stock_number, 6) = bb.control

-- 1/2/21
-- expose date_in_invent
-- shit, all are in 2020, but i have no idea if they would have showed up yesterday
-- go ahead and send jeri a V2
select aa.stock_number, aa.vin, bb.ad_pack_amount, aa.date_in_invent
from ( -- this gives me stock number and vin of all nc currently in inventory
  select a.control as stock_number, min(c.inpmast_vin) as vin, c.date_in_invent
  from fin.fact_gl a 
  join fin.dim_account b on a.account_key = b.account_key
    and b.account_type_code = '1'
    and b.department_code = 'NC'
    and b.typical_balance = 'Debit'
    and b.account  <> '126104'
    and b.account not like '3%'
  join arkona.ext_inpmast c on a.control = c.inpmast_stock_number
  where a.post_status = 'Y'
  group by a.control, c.date_in_invent
  having sum(a.amount) > 10000) aa  
join (
  select a.control, -a.amount as ad_pack_amount
  from fin.fact_gl a
  join fin.dim_account b on a.account_key = b.account_key
    and b.account = '133211'
  where a.post_status = 'Y') bb  on left(aa.stock_number, 6) = bb.control
order by  aa.date_in_invent

------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------


-- this generates the data for jeri
select trim(a.inpmast_stock_number) as stock_number, a.inpmast_vin as vin, 
  b.num_field_value as ad_pack_amount
from arkona.xfm_inpmast a 
inner join arkona.ext_inpoptf b on a.inpmast_vin = b.vin_number
inner join arkona.ext_inpoptd c on b.company_number = c.company_number
  and b.seq_number = c.seq_number
where a.current_row = true
  and a.status = 'I'
  and b.seq_number = 14
  and b.num_field_value <> 0
and  date_in_invent < 20200201 ------------------------------------------------

201802: 474 $116021  239.71 per
201803: 407 $ 98973  243.18 per
201804: 351 $ 83017  236.52 per
208105: 372 $ 87698  235.75 per
201806: 395 $ 92350  233.80 per
201807: 466 $109414  234.80 per
201809: 351 $ 87161  248.32 per
201810: 375 $ 93851  250.26 per
201811: 469 $109094  232.61 per
201812: 411 $ 92242  224.43 per
201901: 390 $ 86505  221.81
201902: 387 $ 85132  220.00
201903: 440 $ 97086  220.65
201904: 466 $105646  226.71
201905: 447 $103606  231.78
201906: 422 $ 99197  235.06
201907: 392 $ 97263  248.12
201908: 338 $ 82024  242.67
201909: 294 $ 80303  239.13
201910: 218 $ 52319  239.99
201911: 261 $ 61184  234.42
201912: 262 $ 60959  232.67
202001: 255 $ 58544  229.58

-- this generates the total
select sum(b.num_field_value)  as total, count(*), sum(b.num_field_value)/count(*)
from arkona.xfm_inpmast a 
inner join arkona.ext_inpoptf b on a.inpmast_vin = b.vin_number
inner join arkona.ext_inpoptd c on b.company_number = c.company_number
  and b.seq_number = c.seq_number
where a.current_row = true
  and a.status = 'I'
  and b.seq_number = 14
  and b.num_field_value <> 0
and  date_in_invent < 20200201 -------------------------------------------------


----------------------------------------------------------------------------
-- 2/3/20
----------------------------------------------------------------------------
turns out i need to do it in db2 based on accounting (133211)

for the past couple of months jeri has been complaining about this being light
i sent her the the history showing how much each month and explained that, yes, the total has dropped

so, today: 
The list that you sent for ad pack shows 255 new vehicles, but there are 314 in new inventory.  This number looks about $20k light.  Are you able to grab any new vehicles in inventory that you do not show ad pack so I can verify this is correct?

Jeri Schmiess Penas, CPA

so i sent her the results of this

select trim(a.inpmast_stock_number) as stock_number, a.inpmast_vin as vin, 
  b.num_field_value as ad_pack_amount
from arkona.xfm_inpmast a 
left join arkona.ext_inpoptf b on a.inpmast_vin = b.vin_number
  and b.seq_number = 14
left  join arkona.ext_inpoptd c on b.company_number = c.company_number
  and b.seq_number = c.seq_number
where a.current_row = true
  and a.status = 'I'
and  date_in_invent < 20200201 ------------------------------------------------
and a.current_row
and a.type_n_u = 'N'
and left(a.inpmast_stock_number, 1) <> 'H'
and coalesce(b.num_field_value, 0) = 0

and just because, 
i looked up a bunch of them in dealertrack, my shit looks ok

-- 05/01 in db2 Inventory - Ad_Pack_a.sql


