﻿-- -- 04/02/18  new pay plan
-- -- output: employee_number, last_name, first_name, per_unit, units, total
-- select aa.employee_number, aa.last_name, aa.first_name, cc.level_1 as per_unit, sum(unit_count) as units,
--   sum(unit_count * cc.level_1) as total
-- from (
--     select a.store_code, a.psc_last_name as last_name, a.psc_first_name as first_name, 
--       b.employee_number, a.stock_number, 
--       case
--         when ssc_last_name = 'none' then unit_count
--         else 0.5 * unit_count
--       end as unit_count, a.odometer_at_sale, b.is_senior
--     from sls.deals_by_month a
--     inner join sls.tmp_consultants b on a.psc_employee_number = b.employee_number
--       or a.ssc_employee_number = b.employee_number
--     where a.year_month = 201803 ---------------------------------------------------------------
--     union
--     select a.store_code, a.ssc_last_name, a.ssc_first_name, b.employee_number, a.stock_number, 
--       0.5 * unit_count as unit_count, a.odometer_at_sale, b.is_senior
--     from sls.deals_by_month a
--     inner join sls.tmp_consultants b on a.ssc_employee_number = b.employee_number
--     where a.year_month = 201803----------------------------------------------------------------
--       and a.ssc_last_name <> 'none') aa
-- left join sls.personnel_payplans bb on aa.employee_number = bb.employee_number 
--   and bb.thru_date > current_date
-- left join sls.per_unit_matrix cc on bb.payplan = cc.payplan  
-- where aa.store_code = 'RY2'      
-- group by aa.employee_number, aa.last_name, aa.first_name, cc.level_1
-- 
-- -- 4/24/18 that is not working, because of tmp_consultants
-- -- without tmp_consultants
-- select aa.employee_number, aa.last_name, aa.first_name, sum(unit_count) as units
-- from (
--     select a.store_code, a.psc_last_name as last_name, a.psc_first_name as first_name, 
--       b.employee_number, a.stock_number, 
--       case
--         when ssc_last_name = 'none' then unit_count
--         else 0.5 * unit_count
--       end as unit_count, a.odometer_at_sale, b.is_senior
--     from sls.deals_by_month a
--     inner join sls.personnel b on a.psc_employee_number = b.employee_number
--       or a.ssc_employee_number = b.employee_number
--     where a.year_month = 201804 ---------------------------------------------------------------
--     union
--     select a.store_code, a.ssc_last_name, a.ssc_first_name, b.employee_number, a.stock_number, 
--       0.5 * unit_count as unit_count, a.odometer_at_sale, b.is_senior
--     from sls.deals_by_month a
--     inner join sls.personnel b on a.ssc_employee_number = b.employee_number
--     where a.year_month = 201804---------------------------------------------------------------
--       and a.ssc_last_name <> 'none') aa
-- left join sls.personnel_payplans bb on aa.employee_number = bb.employee_number 
--   and bb.thru_date > current_date
-- where aa.store_code = 'RY1'      
-- group by aa.employee_number, aa.last_name, aa.first_name
-- order by employee_number
-- 
-- 
-- -- overall store count
-- 
-- select b.store_code, sum(a.unit_count) 
-- from sls.consultant_payroll a
-- inner join sls.personnel b on a.employee_number = b.employee_number
-- where a.year_month = (
--   select year_month
--   from sls.months
--   where open_closed = 'open')
-- group by b.store_code
-- 
-- -- this is what we use in the payroll for submittal function, good place to start
-- -- need to add pay plan, output needs to be: 
-- --    emp#, emp name, per_unit, units, total
-- 
-- do
-- $$
-- declare
--   _year_month integer := (
--     select year_month
--     from sls.months
--     where open_closed = 'open');
--   _deal_store citext := 'RY2';
--   _sc_store citext := 'RY1';
-- begin
-- drop table if exists wtf;
-- create temp table wtf as
-- -- ry1 cons ry2 deal
-- select psc_last_name || ', ' || psc_first_name as name, 
--   psc_employee_number as emp_number, 
--   sum(unit_count) as unit_count
--   (select sls.per_unit_earnings(psc_employee_number, sum(unit_count))) as per_unit,
-- --   sum(unit_count) * (select sls.per_unit_earnings(psc_employee_number, sum(unit_count))) as total_due
-- from (
--       select a.psc_last_name, a.psc_employee_number, a.psc_first_name,
--         case
--           when ssc_last_name = 'none' then a.unit_count
--           else 0.5 * a.unit_count
--         end as unit_count, a.stock_number
--       from sls.deals_by_month a
--       inner join sls.personnel b on a.psc_employee_number = b.employee_number
--         and b.employee_number <> 'HSE'    
--         and b.store_code = 'RY1'
--       -- too insure including only deals on payroll
--       inner join sls.consultant_payroll c on b.employee_number = c.employee_number  
--         and a.year_month = c.year_month    
--       where a.year_month = _year_month
--         and a.store_code = 'RY2'
--       union all 
--       select a.ssc_last_name, a.ssc_employee_number, a.ssc_first_name,
--         0.5 * a.unit_count as unit_count, a.stock_number
--       from sls.deals_by_month a
--       inner join sls.personnel b on a.ssc_employee_number = b.employee_number
--         and b.employee_number <> 'HSE'   
--         and b.store_code = 'RY1'  
--       inner join sls.consultant_payroll c on b.employee_number = c.employee_number   
--         and a.year_month = c.year_month    
--       where a.year_month = _year_month
--         and a.store_code = 'RY2'     
-- union all
-- -- ry2 cons ry1 deal
--       select a.psc_last_name, a.psc_employee_number, a.psc_first_name,
--         case
--           when ssc_last_name = 'none' then a.unit_count
--           else 0.5 * a.unit_count
--         end as unit_count, a.stock_number
--       from sls.deals_by_month a
--       inner join sls.personnel b on a.psc_employee_number = b.employee_number
--         and b.employee_number <> 'HSE'    
--         and b.store_code = 'RY2'
--       -- too insure including only deals on payroll
--       inner join sls.consultant_payroll c on b.employee_number = c.employee_number  
--         and a.year_month = c.year_month    
--       where a.year_month = 201804 ---------------------
--         and a.store_code = 'RY1'
--       union all 
--       select a.ssc_last_name, a.ssc_employee_number, a.ssc_first_name,
--         0.5 * a.unit_count as unit_count, a.stock_number
--       from sls.deals_by_month a
--       inner join sls.personnel b on a.ssc_employee_number = b.employee_number
--         and b.employee_number <> 'HSE'   
--         and b.store_code = 'RY2'  
--       inner join sls.consultant_payroll c on b.employee_number = c.employee_number   
--         and a.year_month = c.year_month    
--       where a.year_month = 201804  ----------------------------
--         and a.store_code = 'RY1') e
-- group by psc_last_name, psc_employee_number, psc_first_name;           
-- end
-- $$;
-- select * from wtf;    
-- 
-- 
-- -- select sls.per_unit_earnings('2141642', 9)
-- 
-- select *
-- from sls.personnel a
-- left join sls.personnel_payplans b on a.employee_number = b.employee_number
--   and b.thru_date > current_date
-- order by a.last_name  
-- 
-- /*  4/30/18
-- 
-- Say that a GM consultant on the standard pay plan has sold 20 units.
-- 3 of those are Honda units
-- Honda will be responsible for the compensation for those units.
-- At what rate should those 3 units be billed to Honda; 300, 400 or ???
-- Thanks
-- 
-- In that case I would bill them at the rate that specific unit earned them, 
-- so if the first one is unit 11 thats $300, second one is unit 15 thats $325 (I think). and so on
-- 
-- Ben
-- 
-- I verified with him, that this is the only situation in which compensation 
-- would rely on the sequence in which a unit was sold.
-- 
-- */
-- 
-- select a.store_code as deal_store, a.stock_number, a.bopmast_id, 
--   a.psc_employee_number, b.store_code as psc_store,
--   a.ssc_employee_number, 
--   case when a.ssc_employee_number = 'none' then 'none' else c.store_code end as ssc_store,
--   unit_count
-- from sls.deals_by_month a
-- inner join sls.personnel b on a.psc_employee_number = b.employee_number
-- left join sls.personnel c on a.ssc_employee_number = c.employee_number
-- where a.year_month = 201804
-- order by a.stock_number
-- 
-- -- no history of a split deal between stores
-- -- select * from (
-- -- select a.store_code as deal_store, a.stock_number, a.bopmast_id, 
-- --   a.psc_employee_number, b.store_code as psc_store,
-- --   a.ssc_employee_number, 
-- --   case when a.ssc_employee_number = 'none' then 'none' else c.store_code end as ssc_store,
-- --   unit_count
-- -- from sls.deals_by_month a
-- -- inner join sls.personnel b on a.psc_employee_number = b.employee_number
-- -- left join sls.personnel c on a.ssc_employee_number = c.employee_number
-- -- where a.year_month = 201804
-- -- ) x where ssc_store <> 'none' and psc_store <> ssc_store
-- 
-- 
-- select *
-- from (
--   select a.store_code as deal_store, a.stock_number, a.bopmast_id, 
--     a.psc_employee_number, b.store_code as psc_store,
--     a.ssc_employee_number, 
--     case when a.ssc_employee_number = 'none' then 'none' else c.store_code end as ssc_store,
--     unit_count, 
--     aa.delivery_date,
--     rank() over (partition by a.psc_employee_number order by aa.delivery_date, a.stock_number) as seq
--   from sls.deals_by_month a
--   -- all null bopmast from -1 unit count, backons
--   left join sls.ext_bopmast_partial aa on a.bopmast_id = aa.bopmast_id
--     and a.store_code = aa.store
--   --   and a.stock_number = aa.stock_number
--   inner join sls.personnel b on a.psc_employee_number = b.employee_number
--     and a.psc_employee_number <> 'HSE'
--   left join sls.personnel c on a.ssc_employee_number = c.employee_number
--   where a.year_month = 201804) e
-- where deal_store <> psc_store



do
$$
declare
  _year_month integer := 202401; -----------------------------------------

begin
drop table if exists wtf;
create temp table wtf as

-- don't think we need secondary sc

  select deal_store, sc_store, employee_number, name, sum(unit_count) as units, per_unit, sum(unit_count) * per_unit as total
  from (
    select e.deal_store, e.employee_number, e.name, e.sc_store, e.unit_count, e.seq as seq,
      case
        when e.seq between 0 and 14.5 then g.level_1
        when e.seq between 15 and 19.5 then g.level_2
        when e.seq between 20 and 24.5 then g.level_3
        when e.seq between 25 and 100 then g.level_4
      end as per_unit
    from (
      select a.store_code as deal_store, a.stock_number, a.bopmast_id, 
        b.employee_number, b.first_name || ' ' || b.last_name as name, a.psc_employee_number, b.store_code as sc_store,
        unit_count, 
        aa.delivery_date,
        rank() over (partition by a.psc_employee_number order by aa.delivery_date, a.stock_number) as seq
      from sls.deals_by_month a
      -- all null bopmast from -1 unit count, backons
      left join sls.ext_bopmast_partial aa on a.bopmast_id = aa.bopmast_id
        and a.store_code = aa.store
      --   and a.stock_number = aa.stock_number
      inner join sls.personnel b on a.psc_employee_number = b.employee_number
        and a.psc_employee_number <> 'HSE'
      where a.year_month = _year_month) e
    left join sls.personnel_payplans f on e.psc_employee_number = f.employee_number
      and f.thru_date > current_date  
    left join sls.per_unit_matrix g on f.payplan= g.payplan  
    where e.deal_store <> e.sc_store) h
  group by deal_store, employee_number, name, sc_store, per_unit;
end
$$;
select * from wtf order by deal_store, name


select store_code, stock_number, unit_count, psc_last_name, psc_employee_number
from sls.deals_by_month
where year_month = 202401
  and psc_first_name <> 'HSE'
  and ((store_code = 'RY1' and left(psc_employee_number, 1) = '2') or (store_code = 'RY2' and left(psc_employee_number, 1) = '1'))
order by store_code, psc_last_name


GM Deals sold by Honda/Nissan Consultants:
emp#	  name	            units	per_unit	total
215883 Jessie Monreal       2     300      600



Honda/Nissan deals sold by GM Consultants:
emp#	  name	          units	per_unit	total
165470  Brad Schmacher     4    250     1000


GM Deals sold by Honda/Nissan Consultants:
emp#      name              units         per_unit              total
             --------------   NONE  --------------------

Honda/Nissan deals sold by GM Consultants:
emp#      name               units      per_unit              total
1135518   Tarr                 1          300                  300                          


GM Deals sold by Honda/Nissan Consultants:
emp#      name              units         per_unit              total
             --------------   NONE  --------------------

Honda/Nissan deals sold by GM Consultants:
emp#      name               units      per_unit              total
             --------------   NONE  --------------------

	
