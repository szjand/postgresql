﻿/*
somehow i managed to save the contents of the aubol payroll query overwriting sfe_stock_numbers_cogs_accounts_for_jeri.sql
and that query is nowhere else to be found
it is not in query history, that only saves 1000 queries and i last ran it a month ago
backup won't save me, it just overwrites and this change was saved on 10/1/2020

what it means is that ALL my fucking sql needs to be in source control
01/31/2021 and it is (not all, but this at least) done
04/01/2021 all is now in git
*/

"insert into sls.sfe values("&202401&",'"&b2&"',"&h2&",'sfe chev', null);"
"insert into sls.sfe values("&202401&",'"&b2&"',"&h2&",'sfe buick', null);"
"insert into sls.sfe values("&202401&",'"&b2&"',"&h2&",'sfe gmc', null);" -- check the columns, brad changed it in november
"insert into sls.sfe values("&202401&",'"&b4&"',"&w4&",'sfe cad', null);" -- cad is the grand total column
"insert into sls.sfe values("&202401&",'"&A2&"',"&c2&",'sfe cpo', null);"


-- match spreadsheets
select prog, count(*), sum(amount)
-- select sum(amount), count(*)  -- totals
from sls.sfe
where year_month = 202401
group by prog


 
-- 1/1/21 add prog to do a final check on the data
drop table if exists tem.sfe;
create table tem.sfe as 
select amount, c.vin, c.stock_number, d.account, prog
-- select sum(amount) -- matches total
from (
  select a.amount, a.vin, max(b.stock_number) as stock_number, prog
  from sls.sfe a
  left join sls.deals b on a.vin = b.vin
  where a.year_month = 202401 -------------------------------------------------------------
  group by a.amount, a.vin, a.prog) c 
left join (
  select distinct control, account
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  inner join fin.dim_journal c on a.journal_key = c.journal_key
  where account_type = 'cogs'
    and post_status = 'Y'
    and c.journal_code = 'VSN'
    and department_code in ('UC', 'NC')) d on c.stock_number = d.control 
where 
  case 
    when stock_number = 'G43635' then account = '1629001'
    when stock_number = 'G44853' then account = '1632001'
    when stock_number = 'G44436' then account = '1629301'
    when stock_number = 'G45381' then account = '1629301'
    when stock_number = 'G46604' then account = '1629312'
    when stock_number = 'g47688' then account = '1636001'
    else 1=1
  end; 
alter table tem.sfe alter column vin set not null;
alter table tem.sfe alter column stock_number set not null;
create unique index on tem.sfe(stock_number);
create unique index on tem.sfe(vin);
alter table tem.sfe add constraint test_amount check (amount > 0);

select * from tem.sfe where stock_number = 'G46604'

-- check for dups    
-- at least for this month, this all looks good
select stock_number
from tem.sfe
group by stock_number having count(*) > 1;

select vin
from tem.sfe
group by vin having count(*) > 1;

-- final test
select prog,count(*), sum(amount)
from tem.sfe
group by prog;

select * from tem.sfe where prog = 'sfe buick'

-- create the spreadsheet for jeri
select amount, vin, stock_number, account
from tem.sfe;



