﻿drop table if exists sls.corona_guarantee;
create table sls.corona_guarantee (
  employee_number citext not null,
  employee_name citext not null,
  guarantee numeric(8,2) not null);

select * from sls.corona_guarantee order by employee_name

insert into sls.corona_guarantee values('162800','HARRIS, SHERIDAN',round(308.666666666667,2));
insert into sls.corona_guarantee values('1149180','WILKIE, DAVID',round(6300,2));
insert into sls.corona_guarantee values('1135518','TARR, JEFF D',round(4710.47466666667,2));
insert into sls.corona_guarantee values('17534','AUBOL, THOMAS B',round(9466.176,2));
insert into sls.corona_guarantee values('164202','HELGESON, HERB',round(0,2));
insert into sls.corona_guarantee values('128530','CROAKER, CRAIG R',round(8316.26666666667,2));
insert into sls.corona_guarantee values('140500','ERICKSON, RONALD J',round(5173.33333333333,2));
insert into sls.corona_guarantee values('189100','LOVEN, ARDEN',round(3266.66666666667,2));
insert into sls.corona_guarantee values('1100710','NEFS, GERALD F',round(0,2));
insert into sls.corona_guarantee values('195460','MICHAEL, ANTHONY E',round(8413.33333333333,2));
insert into sls.corona_guarantee values('1149160','WILKENING, RONALD',round(477.333333333333,2));
insert into sls.corona_guarantee values('1147250','WARMACK, JAMES H',round(6328.42666666667,2));
insert into sls.corona_guarantee values('1147810','WEBER, JAMES A',round(7380.77866666667,2));
insert into sls.corona_guarantee values('113011','MORLOCK, BRUCE A',round(696.666666666667,2));
insert into sls.corona_guarantee values('111250','BEISWENGER, LYLE W',round(320,2));
insert into sls.corona_guarantee values('13815','ANDERSON, STEVEN L',round(1004,2));
insert into sls.corona_guarantee values('118030','MONSON, TAYLOR',round(4625.65333333333,2));
insert into sls.corona_guarantee values('1124625','SEAY, BRYN N',round(5226.66666666667,2));
insert into sls.corona_guarantee values('148080','FOSTER, SAMUEL T',round(3799.07733333333,2));
insert into sls.corona_guarantee values('1130690','STADSTAD, LARRY O',round(4493.29333333333,2));
insert into sls.corona_guarantee values('133017','DOCKENDORF, NATE J',round(5173.33333333333,2));
insert into sls.corona_guarantee values('1106225','OLDERBAK, JOHN',round(5122.83733333333,2));
insert into sls.corona_guarantee values('1132700','STOUT, RICK',round(10820,2));
insert into sls.corona_guarantee values('163700','HALEY, DYLANGER',round(3557.06666666667,2));
insert into sls.corona_guarantee values('1151450','YUNKER, GEORGE P',round(2986.93333333333,2));
insert into sls.corona_guarantee values('1121240','SALBERG, GAYLEN K',round(730.666666666667,2));
insert into sls.corona_guarantee values('112589','BEDNEY, TYLER',round(5360,2));
insert into sls.corona_guarantee values('147741','BJARNASON, JUSTIN',round(2722.87733333333,2));
insert into sls.corona_guarantee values('15552','DECOUTEAU, FRANK A',round(5693.59733333333,2));
insert into sls.corona_guarantee values('170150','IVERSON, ARNOLD W',round(803.333333333333,2));
insert into sls.corona_guarantee values('178543','VANYO, DAVID',round(7270.016,2));
insert into sls.corona_guarantee values('165983','MENARD, MICHAEL',round(3767.10933333333,2));
insert into sls.corona_guarantee values('158763','BRUNK, JUSTIN',round(2508.8,2));
insert into sls.corona_guarantee values('167834','KOHLS, ADAM',round(2453.33333333333,2));
insert into sls.corona_guarantee values('140882','LEEDAHL, GLENNA',round(208,2));
insert into sls.corona_guarantee values('141082','RUMEN, ROBERT',round(8148.71733333333,2));
insert into sls.corona_guarantee values('192348','ROBINSON, CARSON',round(3458.49333333333,2));
insert into sls.corona_guarantee values('187651','NOREIKIS, TIFFANY',round(3053.33333333333,2));
insert into sls.corona_guarantee values('154219','KOBAYASHI-DYER, NARUMI',round(2286.66666666667,2));
insert into sls.corona_guarantee values('195231','TURNER, RICHARD J',round(3522.22933333333,2));
insert into sls.corona_guarantee values('165789','HECK, SETH',round(3514.66666666667,2));
insert into sls.corona_guarantee values('132963','ERICKSON, DENNIS',round(52,2));
insert into sls.corona_guarantee values('198543','KROGSTAD, LARRY',round(64.2133333333333,2));
insert into sls.corona_guarantee values('197523','BEITER, GABRIELLE',round(2250.32266666667,2));
insert into sls.corona_guarantee values('162983','CLOSE, PRESTON',round(3163.46666666667,2));
insert into sls.corona_guarantee values('185462','CARABALLO, CHRISTOPHER',round(3445.06666666667,2));
insert into sls.corona_guarantee values('165989','WALDECK, JAMES A',round(2866.66666666667,2));
insert into sls.corona_guarantee values('159876','AANENSON, GENE',round(166.666666666667,2));
insert into sls.corona_guarantee values('158393','ROGERS, TIMOTHY',round(32,2));
insert into sls.corona_guarantee values('175838','HELLYER, HENRY',round(0,2));
insert into sls.corona_guarantee values('196575','BACH, CASPER',round(2584.32,2));
insert into sls.corona_guarantee values('175873','BUDEAU, KAYLA',round(2655.22933333333,2));
insert into sls.corona_guarantee values('159875','ROSE, BRITTANY',round(1320.424,2));
insert into sls.corona_guarantee values('159763','ROED, ZACHARY',round(640.576,2));
insert into sls.corona_guarantee values('145785','LOGAN, JAI''LA',round(693.6832,2));
insert into sls.corona_guarantee values('147852','HALE, ELISA',round(677.5552,2));
insert into sls.corona_guarantee values('166278','MOOR, JARON',round(347.312,2));
