﻿-- kim (w/o json): based on sls.fi_manager_comp
-- first run sales_pay_plan_201803/fi_mgr_comp/fi_mgr_comp.py -- updates the data and generates the email for mgrs
-- this script generates the file for kim

/*
11/29/21
Hey Jon
Lets set up Tyler Grollimund at 12% just like how Tom Aubol is at GM store, except Tom is 14%.
Make sense?
Nick Shirek
*/

-- tom aubol
first run this (with the correct month)
select sls.update_fi_manager_comp(202112, '17534',.14);


-- tyler grollimund
select sls.update_fi_manager_comp(202112, '254327',.12);


this now returns both guys in a single result set
do
$$
declare
  _year_month integer := 202112; ------------------------------------------------------
  _store citext := 'RY1';
--   _employee_number citext := (
--     select employee_number
--     from sls.personnel
--     where last_name = 'aubol');
begin
  drop table if exists wtf;
  create temp table wtf as
    select b.last_name || ', ' || b.first_name as consultant, a.employee_number, 
      a.pto_pay as "PTO (74)",

        a.fi_gross + a.chargebacks - a.draw as "F&I Comm less draw (79A)",   
        a.fi_gross + a.chargebacks - a.draw + a.pto_pay as "Total Month End Payout"  

    from sls.fi_manager_comp a
    join sls.personnel b on a.employee_number = b.employee_number
    where a.the_date = (
      select the_date 
      from dds.dim_date 
      where year_month = _year_month 
        and last_day_of_month);


end
$$;  
select * from wtf;  


delete 
from sls.fi_manager_deals
where year_month = 202111

select *
from sls.fi_manager_deals
where year_month = 202111
  and employee_number = '254327'

select * from sls.fi_manager_comp where year_month = 202111
