# encoding=utf-8
"""
populate and maintain tables sls.fi_manager_comp & sls.fi_manager_deals
from that data generate an email to tom, nick & ben c
that has comp to date in the body with an attached spreadsheet of deals
currently hard coded for tom aubol (employee_number)

9/2/2019
    too much hard coding of the date as current_date - 1
    added year_month parameter, then used that to generate last day of month
    shit subject line is wrong, says September, oh well
"""
import db_cnx
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
from email.mime.text import MIMEText
import os
import csv
import datetime


today = datetime.date.today()
yesterday = today - datetime.timedelta(days=1)
subject = "Tom Aubol compensation for " + yesterday.strftime("%B") + ' ' + yesterday.strftime("%Y")
employee_number = '17534'
percentage = .14

# subject = "Tyler Grollimund compensation for " + yesterday.strftime("%B") + ' ' + yesterday.strftime("%Y")
# employee_number = '254327'
# percentage = .12

# set the year_month before running
_year_month = 202111  *************** change the fucking month ************************

file_name = 'files/deals.csv'
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        # no need to rerun this, ran it generating kims file

        sql = """
            select sls.update_fi_manager_comp({},'{}',{});
        """.format(_year_month, employee_number, percentage)

        # populate the tables
        pg_cur.execute(sql)

        sql = """
            select stock_number, fi_gross, chargebacks
            from sls.fi_manager_deals
            where employee_number = '{}'
              and year_month = {}
            order by stock_number;         
        """.format(employee_number, _year_month)

        # create the file of deals
        pg_cur.execute(sql)
        with open(file_name, 'w') as f:
            csv.writer(f).writerow(["stock #", "fi sales", "chargebacks"])
            csv.writer(f).writerows(pg_cur)

        sql = """
            select fi_gross, chargebacks, draw, pto_pay, 
              fi_gross + chargebacks - draw + pto_pay as total
            from sls.fi_manager_comp   
            where employee_number = '{0}'
              and the_date = (
                select the_date
                from dds.dim_date
                where year_month = {1}
                  and last_day_of_month);
        """.format(employee_number, _year_month)
        pg_cur.execute(sql)

        # create the email body
        for t in pg_cur.fetchall():
            body = ('fi gross:\t       ' + str(t[0]) + '\n' +
                    'chargebacks:\t +  ' + str(t[1]) + '\n' +
                    'draw:\t        -   ' + str(t[2]) + '\n' +
                    'pto:\t         +   ' + str(t[3]) + '\n' +
                    'total:\t          ' + str(t[4]) + '\n')

try:
    COMMASPACE = ', '
    sender = 'jandrews@cartiva.com'
    recipients = ['test@cartiva.com', 'jandrews@cartiva.com']
    # recipients = ['taubol@rydellcars.com', 'jandrews@cartiva.com', 'nshirek@rydellcars.com', 'bcahalan@rydellcars.com']
    # recipients = ['tgrollimund@rydellcars.com', 'jandrews@cartiva.com', 'nshirek@rydellcars.com',
    #               'mlongoria@rydellcars.com','bcahalan@rydellcars.com']
    outer = MIMEMultipart()
    outer.attach(MIMEText(body))
    outer['To'] = COMMASPACE.join(recipients)
    outer['From'] = sender
    outer['Subject'] = subject
    attachments = [file_name]
    for x_file in attachments:
        try:
            with open(x_file, 'rb') as fp:
                msg = MIMEBase('application', "octet-stream")
                msg.set_payload(fp.read())
            encoders.encode_base64(msg)
            msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(x_file))
            outer.attach(msg)
        except Exception:
            raise
    composed = outer.as_string()
    e = smtplib.SMTP('mail.cartiva.com')
    try:
        e.sendmail(sender, recipients, composed)
    except smtplib.SMTPException:
        print("Error: unable to send email")
    e.quit()
except Exception as error:
    print(error)
