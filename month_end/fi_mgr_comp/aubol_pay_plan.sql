﻿
/*
Finance Manager Compensation Plan
Rydell Auto Center provides compensation to Finance Manager personnel as follows:

Finance
A compensation rate of 14% will be paid on the finance gross individually generated.  
Finance gross includes gross resulting from product sales as well as reserves paid by financial institutions.  
Chargebacks occurring within a six-month period after the date of vehicle sale, as well as 
incentives that are unable to be claimed as a result of ineligibility or missing documentation 
will reduce the total compensable gross in the month the chargeback occurs.

Additional Compensation (Spiffs)
From time to time, management may provide additional compensation bonuses.  
These may be paid out in cash or added to an employee’s monthly pay.  
Cash payouts will be included in compensation in the following pay period.

Allowed Time Off
See HR for Allowed Time Off.

Example #1
Finance Manager generates a total finance gross of $65,000.  There are $1,000 in chargebacks due to rebate ineligibility.  
	Finance:				 $8,960 (14% of $64,000)
	Total Monthly Compensation:	 $8,960 
	
Example #2
Finance Manager generates a total finance gross of $105,000.  There are $5,000 in chargebacks due to refinances.
	Finance:				 $14,000 (14% of $100,000)
	Total Monthly Compensation:	 $14,000 

The above compensation plan dated February 1, 2019, supersedes and replaces all previous compensation plans.
*/


select a.*
from sls.fi_chargebacks a
join sls.personnel b on a.fi_employee_number = b.employee_number
  and b.last_name = 'aubol'


select a.bopmast_id, a.seq, a.unit_Count, a.stock_number, b.*
from sls.deals_by_month a
left join sls.deals_gross_by_month b on a.stock_number = b.control
where a.year_month = 201902  
  and a.fi_last_name = 'aubol'

-- do not need to multiply by deals_by_month.unit_count,
-- unwind polarity handled in deals_gross_by_month
select *
from sls.deals_gross_by_month
where control in ('G36044XA','G35984X','H11981','G34458B')  
order by control, year_month


-- detail
select a.stock_number, sum(fi_gross) as fi_gross, sum(fi_chargeback) as fi_chargeback
from sls.deals_by_month a
left join sls.deals_gross_by_month b on a.stock_number = b.control
where a.year_month = 201902  
  and a.fi_last_name = 'aubol'
group by a.stock_number
having sum( fi_gross + fi_chargeback) <> 0

select a.stock_number, fi_chargeback
from sls.deals_by_month a
left join sls.deals_gross_by_month b on a.stock_number = b.control
where a.year_month = 201901  
  and a.fi_last_name = 'aubol'
  and fi_chargeback <> 0 

-- wtf, this table has no  chargebacks?
select *
from sls.deals_gross_by_month a
where fi_chargeback <> 0

-- but they are here
select a.*,
  ((select the_date from dds.dim_date where year_month = 201902 and first_day_of_month) - a.delivery_date)
from sls.fi_chargebacks a
where a.chargeback_year_month = 201902
  and exists (
    select 1
    from sls.deals_by_month
    where stock_number = a.stock_number
      and fi_last_name = 'aubol')
order by chargeback_year_month desc    

select the_date from dds.dim_date where year_month = 201902 and first_day_of_month
  
-- total comp
select sum(fi_gross + fi_chargeback) as fi_gross_total, round(0.14 * sum(fi_gross + fi_chargeback), 2) as total_comp
from sls.deals_by_month a
left join sls.deals_gross_by_month b on a.stock_number = b.control
where a.year_month = 201902  
  and a.fi_last_name = 'aubol'

-- draw ?  

select b.employee_name, a.*
from sls.paid_by_month a
join arkona.xfm_pymast b on a.employee_number = b.pymast_employee_number
  and b.current_row
where a.year_month = 201902
order by b.employee_name


-- pto

select a.*
from sls.xfm_fact_clock_hours a
join sls.personnel b on a.employee_number = b.employee_number
  and b.last_name = 'aubol'
where a.vacation_hours + a.pto_hours + a.holiday_hours <> 0  


select *
from sls.personnel
where last_name = 'aubol'


select * 
from sls.pto_intervals
where employee_number = '17534'


select a.employee_number, round(a.pto_hours, 2) as pto_hours, b.pto_rate, round(a.pto_hours * b.pto_rate, 2) as pto_pay
from sls.xfm_fact_clock_hours a
left join sls.pto_intervals b on a.employee_number = b.employee_number
  and a.year_month = b.year_month
where a.year_month = 201812
  and a.employee_number = '17534'
  and a.pto_hours <> 0
  and b.pto_rate <> 0

do
$$
declare
  _employee_number citext := '17534';
  _year_month integer := 201902;
  _first_of_month date := (
    select the_date
    from dds.dim_date
    where year_month = _year_month
      and first_day_of_month);
  _last_of_month date := (      
    select the_date
    from dds.dim_date
    where year_month = _year_month
      and last_day_of_month);  
begin
  drop table if exists wtf;
  create temp table wtf as
  select round(sum(a.pto_hours * b.pto_rate), 2) as pto_pay
  from sls.xfm_fact_clock_hours a
  left join sls.pto_intervals b on a.employee_number = b.employee_number
    and b.year_month = _year_month
  where a.the_date between _first_of_month and _last_of_month
    and a.employee_number = _employee_number;
end
$$;
select * from wtf;    
          
-- 2/24/19 ----------------------------------------------------------------------------------------
-- detail gross
select a.stock_number, sum(fi_gross) as fi_gross, 0 as chargeback
from sls.deals_by_month a
left join sls.deals_gross_by_month b on a.stock_number = b.control
where a.year_month = 201902  
  and a.fi_last_name = 'aubol'
group by a.stock_number
having sum( fi_gross) <> 0
union
-- detail chargebacks
select a.stock_number, 0 as fi_gross, sum(a.amount) as chargeback
from sls.fi_chargebacks a
join sls.deals_by_month b on a.stock_number = b.stock_number
  and b.fi_last_name = 'aubol'
where a.chargeback_year_month = 201902
  -- chargeback within 6 months of delivery date
  and ((select the_date from dds.dim_date where year_month = 201902 and first_day_of_month) - a.delivery_date) < 180
group by a.stock_number  



-- email summary
-- fi_gross + chargebacks - draw + pto


do
$$
declare
  _employee_number citext := '17534';
  _year_month integer := 201902;
  _first_of_month date := (
    select the_date
    from dds.dim_date
    where year_month = _year_month
      and first_day_of_month);
  _last_of_month date := (      
    select the_date
    from dds.dim_date
    where year_month = _year_month
      and last_day_of_month);  
begin
drop table if exists wtf;
create temp table wtf as

select fi_gross || ' + ' || chargeback || ' - ' || draw || ' + ' || pto_pay || ' = ' || fi_gross + chargeback - draw + pto_pay
from ( -- fi_gross
  select round(.14 * sum(fi_gross), 2) as fi_gross
  from sls.deals_by_month a
  left join sls.deals_gross_by_month b on a.stock_number = b.control
  where a.year_month = _year_month
    and a.fi_employee_number = _employee_number) s
join ( -- chargebacks
  select round(.14 * sum(a.amount), 2) as chargeback
  from sls.fi_chargebacks a
  join sls.deals_by_month b on a.stock_number = b.stock_number
    and b.fi_employee_number = _employee_number
  where a.chargeback_year_month = _year_month
    -- chargeback within 6 months of delivery date  
    and _first_of_month - a.delivery_date < 180) t on true
join ( -- draw
  select a.draw
  from sls.paid_by_month a
  join arkona.xfm_pymast b on a.employee_number = b.pymast_employee_number
    and b.current_row
  where a.year_month = _year_month
    and a.employee_number = _employee_number) u on true
join ( -- pto
  select round(sum(a.pto_hours * b.pto_rate), 2) as pto_pay
  from sls.xfm_fact_clock_hours a
  left join sls.pto_intervals b on a.employee_number = b.employee_number
    and b.year_month = _year_month
  where a.the_date between _first_of_month and _last_of_month
    and a.employee_number = _employee_number) v on true;   
   


end
$$;

select * from wtf;

drop table if exists sls.fi_manager_comp cascade;
create table sls.fi_manager_comp (
  employee_number citext not null,
  year_month integer not null,
  the_date date not null,
  fi_gross numeric(8,2) not null,
  chargebacks numeric(8,2) not null,
  draw numeric(8,2) not null,
  pto_pay numeric(8,2) not null,
  primary key (employee_number, the_date));
drop table if exists sls.fi_manager_deals cascade;  
create table sls.fi_manager_deals (
  employee_number citext not null,
  year_month integer not null,
  stock_number citext not null,
  fi_gross numeric(8,2) not null,
  chargebacks numeric(8,2) not null,
  primary key(stock_number, year_month));


updated daily with sls.update_fi_manager_comp(employee_number)

select sls.update_fi_manager_comp('17534');


-- query for the file to attach
select stock_number, fi_gross, chargebacks
from sls.fi_manager_deals
where employee_number = '17534'
  and year_month = (
    select year_month 
    from dds.dim_date
    where the_date = current_date - 1)
order by stock_number;    

-- query for the body
select * from sls.fi_manager_comp

select fi_gross, chargebacks, draw, pto_pay, 
  fi_gross + chargebacks + draw + pto_pay as total
from sls.fi_manager_comp   
where employee_number = '17534'
  and the_date = (
    select the_date
    from dds.dim_date
    where the_date = current_date - 1)


-- 3/4/19 body is not generated
select * from sls.fi_manager_deals where year_month = 201903

select * from sls.deals_by_month where year_month = 201903 order by fi_last_name

-- hard code variables
-- change to left joins, may not be any chargebacks, draw or pto
  select '17534', 201903, current_date, fi_gross, chargeback, coalesce(draw, 0), pto_pay
-- select *  
  from ( -- fi_gross
    select round(.14 * sum(fi_gross), 2) as fi_gross
    from sls.deals_by_month a
    left join sls.deals_gross_by_month b on a.stock_number = b.control
    where a.year_month = 201903
      and a.fi_employee_number = '17534') s
  left join ( -- chargebacks
    select round(.14 * sum(a.amount), 2) as chargeback
    from sls.fi_chargebacks a
    join sls.deals_by_month b on a.stock_number = b.stock_number
      and b.fi_employee_number = '17534'
    where a.chargeback_year_month = 201903
      -- chargeback within 6 months of delivery date  
      and '03/01/2019' - a.delivery_date < 180) t on true
  left join ( -- draw
    select a.draw
    from sls.paid_by_month a
    join arkona.xfm_pymast b on a.employee_number = b.pymast_employee_number
      and b.current_row
    where a.year_month = 201903
      and a.employee_number = '17534') u on true
  left join ( -- pto
    select round(sum(a.pto_hours * b.pto_rate), 2) as pto_pay
    from sls.xfm_fact_clock_hours a
    left join sls.pto_intervals b on a.employee_number = b.employee_number
      and b.year_month = 201903
    where a.the_date between '03/01/2019' and '03/31/2019'
      and a.employee_number = '17534') v on true;  



  