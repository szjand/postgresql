﻿drop table if exists sls.honda_sfe;
create table sls.honda_sfe (
  year_month integer not null,
  stock_number citext not null,
  vin citext not null,
  amount integer not null);

-- ="insert into sls.honda_sfe values("&201809&",'"&A1&"','"&B1&"',"&C1&");"

insert into sls.honda_sfe values(201809,'H11723','1N4AL3AP5JC470958',850);
insert into sls.honda_sfe values(201809,'H11719','1N4AL3AP6JC295328',850);

select *
from sls.honda_sfe


select amount, c.vin, right(c.vin, 6), c.stock_number, d.account
-- select sum(amount)
from (
  select a.amount, a.vin, max(b.stock_number) as stock_number
  from sls.honda_sfe a
  left join sls.deals b on a.vin = b.vin
  where a.year_month = 201809
  group by a.amount, a.vin) c 
left join (
  select distinct control, account
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  inner join fin.dim_journal c on a.journal_key = c.journal_key
  where account_type = 'cogs'
    and post_status = 'Y'
    and c.journal_code = 'VSN'
    and department_code in ('UC', 'NC')) d on c.stock_number = d.control     