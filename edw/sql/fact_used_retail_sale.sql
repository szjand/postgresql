﻿-- even thought this first fact table is specifically for used car retail
AFTON: 
	board data  RY3?
	nullable booleans in board.sales_board

does new/used belong in dim_vehicle? 
or is it an "attribute" of the sale(deal)  

dim_vehicle
	vin can serve as a durable key

though the initial fact is for used cars, 

mini dimension for odometer?

mileage / pricing should they be facts?

normal pricing, test pricing, promotional pricing, liquidation pricing

kimball reader ch 9
The point of this article is to encourage you to visualize the relationship between the entities
you choose as dimensions. When entities have a fixed, time-invariant strongly correlated
relationship, they should be modeled as a single dimension. In most other cases, your design
will be simpler and smaller when you separate the entities into two dimensions.

mini-dimension kimball reader ch 9
what gets weird is the only correlation between customer and demographics (vehicle & odometer) is through a fact table
and that feels inadequate somehow

kimball reader 9.19

miles is a sample of what will happen with other descriptive vehicle attributes that will change over time
	book values
	price
	
1/20/23 meeting
use and call it boarded date


sales dimension
	
-- board data retail sales
select a.board_id, a.boarded_ts, b.last_name || ', ' || b.first_name as boarded_by,
  c.board_type, c.board_sub_type, a.deal_number, a.vin, a.stock_number, d.arkona_store_key,
  a.used_Vehicle_package, a.is_deleted, a.boarded_Date, a.is_Backed_on, a.employee_deal, 
  a.rydell_delivers, a.in_out, a.deal_details->>'VehicleType'
-- select a.*  
from board.sales_board a
join nrv.users b on a.boarded_by = b.user_key
join board.board_types c on a.board_type_key = c.board_type_key
  and c.board_type_key = 5
join onedc.stores d on a.store_key = d.store_key
where  a.boarded_date > '12/31/2017'
  and c.board_type = 'Deal'
  and not a.is_deleted
order by d.arkona_store_key, a.deal_details->>'VehicleType', c.board_sub_type


-- back ons
select a.stock_number, a.vin, a.boarded_date, a.is_backed_on, count(*) over (partition by a.stock_number)
from board.sales_board a
join board.board_types b on a.board_type_key = b.board_type_key
  and b.board_type = 'deal'
where exists (
  select 1
  from board.sales_board
  where stock_number = a.stock_number
    and is_backed_on
    and boarded_date >= a.boarded_date)
and not a.is_deleted    
and a.boarded_date > '12/31/2021'
order by a.stock_number, a.boarded_date


back ons
  G41583XE,1FMJU1JT0HEA08438
  select * from board.sales_board where vin = '1FMJU1JT0HEA08438'	and not is_Deleted
  G42300YD,1GKFK13057J158073
  select * from board.sales_board where vin = '1GKFK13057J158073'	and not is_Deleted



-- i was wrong, there is a delivery_date in bopmast
-- BUT, it is not in the opentrack dealLookup
select a.bopmast_stock_number, a.date_capped, a.delivery_date, bb.boarded_ts::date, bb.boarded_Date
from arkona.ext_bopmast a
left join (
select a.stock_number, a.boarded_ts, a.boarded_Date
from board.sales_board a
  inner join board.daily_board b on a.board_id = b.board_id
  inner join board.board_types c on a.board_type_key = c.board_type_key
  where is_deleted = false
		and board_type = 'Deal') bb on a.bopmast_stock_number = bb.stock_number
where a.date_capped > '09/30/2022'
--   and bb.boarded_ts::date <> bb.boarded_Date

  and a.date_capped <> bb.boarded_date 
  and a.delivery_date <> bb.boarded_date
order by abs(a.date_capped - a.delivery_date) desc

------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------
create schema edw;
comment on schema edw is 'schema for project attempting to model vehicle inventory/sales in a star schema';

-- 1. populate the dimensions
dim_store

select * from fin.dim_account where description like '%rao%' and account_type = 'sale'
-- yes, i can get outlet deals from accounting (maybe) some months match the sales board, some don't
select c.year_month, c.the_date, a.control, a.amount, count(a.control) over (partition by c.year_month)
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
  and account in ('145002','144602')
join dds.dim_date c on a.date_key = c.date_key  
where a.amount < 0
-- where c.year_month = 202301
order by c.year_month desc

drop table if exists edw.dim_stores cascade;
create table edw.dim_stores (
  store_key serial primary key,
  store_name citext not null,
  store_code citext not null,
  current_row boolean not null,
  row_from_date date not null,
  row_thru_date date not null default '9999-12-31'::date,
  row_change_date date,
  row_change_reason citext);

***** to include outlet or not

insert into edw.dim_stores (store_name, store_code, current_row, row_from_date) values
('Unknown', 'Unknown', true, '01/01/2023'),
('GM', 'RY1', true, '01/01/2023'),
('Honda Nissan', 'RY2', true, '01/01/2023'),
('Toyota', 'RY8', true, '01/01/2023')
('Auto Outlet', 'RY3', true, '01/01/2023');

select * from edw.dim_stores

drop table if exists edw.dim_retail_sales cascade;
create table edw.dim_retail_sales ()
  retail_sales_key serial primary key,
  
-- select a.board_id, a.boarded_ts, b.last_name || ', ' || b.first_name as boarded_by,
--   c.board_type, c.board_sub_type, a.deal_number, a.vin, a.stock_number, d.arkona_store_key,
--   a.used_Vehicle_package, a.is_deleted, a.boarded_Date, a.is_Backed_on, a.employee_deal, 
--   a.rydell_delivers, a.in_out, a.deal_details->>'VehicleType'

***** deal 24358 - 2 rows
select a.deal_details->>'CompanyNumber', a.deal_details->>'DealNumber' as DealNumber, a.deal_details->>'RecordType' as RecordType,
  a.deal_details->>'VehicleType' as VehicleType, a.deal_details->>'SaleType' as SaleType,
  e.record_type, e.vehicle_type, e.sale_type
from board.sales_board a
join nrv.users b on a.boarded_by = b.user_key
join board.board_types c on a.board_type_key = c.board_type_key
  and c.board_type_key = 5
join onedc.stores d on a.store_key = d.store_key
left join arkona.ext_bopmast e on (a.deal_details->>'DealNumber')::integer = e.record_key
where  a.boarded_date > '12/31/2022'
  and c.board_type = 'Deal'
  and not a.is_deleted

***** board_id as an attribute of the dim_retail_sale




drop table if exists edw.fact_boarded_retail_sales cascade;
create table edw.fact_boarded_retail_sales (
  store_key integer not null references edw.dim_stores(store_key),
  boarded_date date,
  vehicle_key integer not null references edw.dim_vehicles(vehicle_key),
  boarded_retail_sale_key integer not null references edw.dim_boarded_retail_sales(boarded_retail_sale_key),
  sale_price numeric not null);

select * from board.board_types

insert into edw.fact_boarded_retail_sales(store_key,boarded_date,vehicle_key,boarded_retail_sale_key,sale_price)  
select dd.store_key, aa.boarded_date, bb.vehicle_key, cc.boarded_retail_sale_key,
  case
    when cc.deal_type = 'Financed_retail' then cc.lease_price 
    else cc.retail_price
  end as sale_price
from (
	select a.board_id, a.stock_number, a.vin, a.boarded_date, f.arkona_store_key
	from board.sales_board a
	join board.board_types c on a.board_type_key = c.board_type_key
		and c.board_type_key = 5
	join edw.dim_vehicles d on a.vin = d.vin  
	join ( -- grouped join on daily_board to handle dups generated by multiple rows for multiple consultants
		select board_id,deal_type,vehicle_type,vehicle_model,vehicle_make,vehicle_model_year,
			vehicle_color,customer_name,customer_city, coalesce(string_agg(distinct sales_person, '|'), 'unknown') as consultant
		from board.daily_board
		group by board_id,deal_type,vehicle_type,vehicle_model,vehicle_make,vehicle_model_year,
			vehicle_color,customer_name,customer_city) e on a.board_id = e.board_id
	join onedc.stores f on a.store_key = f.store_key
	left join arkona.ext_bopmast g on a.deal_number::integer = g.record_key
		and a.stock_number = g.bopmast_stock_number
	where  a.boarded_date > '12/31/2017'
		and c.board_type = 'Deal'
		and not a.is_deleted
		and not is_backed_on -- dup fix
		and -- dup fix
			case
				when a.deal_number = '65417' then a.stock_number = 'G41554'
				when a.deal_number = '16127' then a.stock_number = 'H10936'
				else true
			end) aa   
join edw.dim_vehicles bb on aa.vin = bb.vin
join edw.dim_boarded_retail_sales cc on aa.stock_number = cc.stock_number
  and aa.board_id::citext = cc.board_id
join edw.dim_stores dd on aa.arkona_store_key = dd.store_code;

create index on edw.fact_boarded_retail_sales(store_key);
create index on edw.fact_boarded_retail_sales(vehicle_key);
create index on edw.fact_boarded_retail_sales(boarded_date);
create index on edw.fact_boarded_retail_sales(boarded_retail_sale_key);

select a.*, b.vin 
from edw.fact_boarded_retail_sales a
join edw.dim_Vehicles b on a.vehicle_key = b.vehicle_key


select * from dds.dim_date where the_date = current_date