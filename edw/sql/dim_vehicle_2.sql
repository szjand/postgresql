﻿ 2nd shot at edw.dim_vehicle
overlooked over 4000 vehicles that should have had build data

drop table if exists edw.board_vins cascade;
create unlogged table edw.board_vins as --22356
select distinct a.vin, a.board_id
from board.sales_board a
join board.board_types c on a.board_type_key = c.board_type_key
  and c.board_type_key = 5
where  a.boarded_date > '12/31/2017'
  and c.board_type = 'Deal'
  and not a.is_deleted;
create index on edw.board_vins(vin);

-- these are all the motorcycles, chrome fails due to no license
delete from edw.board_vins 
where vin in (
	select vin
	from edw.board_vins a
	join board.daily_board b on a.board_id = b.board_id
	where b.vehicle_make in ('yamaha','kawasaki','HARLEY','HARLEY DAVIDSON','HARLEY-DAVIDSON','SUZUKI')); 

delete from edw.board_vins where vin = 'CB3602006227'  -- honda mc


select distinct vin -- 317
from  edw.board_vins a
where not exists (
  select 1
  from (
		select a.vin  -- 13625
		from  edw.board_vins a  
		join chr.describe_vehicle b on a.vin = b.vin
		union 
		select a.vin  -- 19329
		from  edw.board_vins a
		join chr.build_data_describe_vehicle b on a.vin = b.vin) c
  where vin = a.vin)
order by vin	

select *
from edw.vehicle_build_data

------------------------------------------------------ chrome build data style count = 1 ------------------------------------------------------------------

-- and subsequently the additional build data vins i discovered, needed to do the distinct on these, not sure why, but it worked
-- drop table if exists edw.vehicle_build_data cascade;
-- create unlogged table edw.vehicle_build_data as
-- select vin from (
SELECT distinct a.vin, 
  (c ->>'modelYear')::integer as model_year,
  (c ->'division'->>'_value_1')::citext as make,
  (c ->'model'->>'_value_1'::citext)::citext as model,
  (c->>'trim')::citext as trim_level,
  (b.response->>'bestStyleName')::citext as style_name,
  c->>'altStyleName' as alt_style_name,
  case
    when j.cab->>'_value_1' like 'Crew%' then 'Crew' 
    when  j.cab->>'_value_1' like 'Extended%' then 'Double'  
    when  j.cab->>'_value_1' like 'Regular%' then 'Regular'  
    else 'n/a'   
  end as cab,
  case
    when k.bed->>'_value_1' like '%Bed' then k.bed->>'_value_1'
    else 'n/a'
  end as bed,  
  c->>'altBodyType' as alt_body_type,  
  (c ->>'mfrModelCode')::citext as model_code,
  case
    when c ->>'drivetrain' like 'Rear%' then 'RWD'
    when c ->>'drivetrain' like 'Front%' then 'FWD'
    when c ->>'drivetrain' like 'Four%' then '4WD'
    when c ->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drive,
  (coalesce(b.response->'vinDescription'->>'builtMSRP', c->'basePrice'->>'msrp'))::numeric::integer as msrp,
  d->>'cylinders' as eng_cylinders, e->>'_value_1' as eng_displacement, d->'fuelType'->>'_value_1' as fuel,
  case
    when  (c ->'division'->>'_value_1')::citext = 'Nissan' then 'automatic'
    when m->>0 is null then 'unknown'
    when m->>0 like '%SPEED MANUAL%' then 'manual'
    else 'automatic'
  end as transmission,
  i->>'colorName' as ext_color,
  h->>0 as interior,
  (c->>'id')::citext as chrome_style_id, 'build'::citext as source  
from edw.board_vins a
join chr.build_data_describe_vehicle b on a.vin = b.vin
  and b.source = 'build'
  and b.style_count = 1
join jsonb_array_elements(b.response->'style') as c on true
left join jsonb_array_elements(b.response->'engine') as d on true  
  and (d->'installed'->>'cause' = 'OptionCodeBuild' or coalesce(d->'installed'->>'cause', 'xxx') = 'VIN')
left join jsonb_array_elements(d->'displacement'->'value') as e on true
  and e ->>'unit' = 'liters' 
left join jsonb_array_elements(b.response->'factoryOption') as g on true --interior
  and g->'header'->>'_value_1' = 'SEAT TRIM'
  and g->'installed'->>'cause' in ('RelatedColor','ExteriorColorBuild', 'OptionCodeBuild','InteriorColorBuild')
  and g->'optionKindId' <> 'null' -- 77 vins have multiple objects in factory options that match the interior header value, the relevant value has an optionKindId value
left join jsonb_array_elements(g->'description') as h on true
left join jsonb_array_elements(b.response->'exteriorColor') as i on true
  and i->'installed'->>'cause' in ('RelatedColor','ExteriorColorBuild', 'OptionCodeBuild','InteriorColorBuild')
left join jsonb_array_elements(c->'bodyType') with ordinality as j(cab) on true
  and j.ordinality =  2
left join jsonb_array_elements(c->'bodyType') with ordinality as k(bed) on true
  and k.ordinality =  1
left join jsonb_array_elements(b.response->'factoryOption') l on true
  and l->'header'->>'_value_1' = 'TRANSMISSION'
  and l->'installed'->>'cause' in ('VIN','OptionCodeBuild')
left join jsonb_array_elements(l->'description') m on true 
-- where vin = '1GC4YREY4LF345899'
where not exists (
  select 1
  from edw.vehicle_build_data
  where vin = a.vin)
--   ) x group by vin having count(*) > 1
order by vin; 

-- alter table edw.vehicle_build_data
-- add primary key(vin);

------------------------------------------------------ chrome build data style count = 1 />------------------------------------------------------------------


----------------------------------------------< chrome build data style count <> 1, these are a fucking mess------------------------------------------------------------------
-- nothing done with these yet, i fucking hate getting stuck on these 84 vins
select a.vin, 
  (c ->>'modelYear')::integer as chr_model_year,
  coalesce(d.year, e.year) as model_year,
  (c ->'division'->>'_value_1')::citext as chr_make,
  coalesce(d.make, e.make) as make,
  (c ->'model'->>'_value_1'::citext)::citext as chr_model,
  coalesce(d.model, e.model) as model,
  (c ->>'mfrModelCode')::citext as chr_model_code,
  coalesce(d.model_code, e.model_code) as model_code,
  (c->>'trim')::citext as chr_trim,
  coalesce(d.body_style, e.body_style) as dt_trim,
  f.trim as tool_trim, e.chrome_style_id
from  edw.board_vins a
join chr.build_data_describe_vehicle b on a.vin = b.vin
  and source = 'build'
  and b.style_count <> 1
join jsonb_array_elements(b.response->'style') as c on true
  and c->'division'->>'_value_1' <> 'Ford'
left join arkona.ext_inpmast d on a.vin = d.inpmast_vin
  and d.inpmast_company_number = 'RY1'
left join arkona.ext_inpmast e on a.vin = e.inpmast_vin
  and e.inpmast_company_number = 'RY8'  
left join ads.ext_vehicle_items f on a.vin = f.vin  
order by vin, make, model 
---------------------------------------------- chrome build data style count <> 1, these are a fucking mess />------------------------------------------------------------------

------------------------------------------------------< chrome non build data style count = 1 ------------------------------------------------------------------

-- drop table if exists edw.vehicle_describe_vehicle cascade;
-- create unlogged table edw.vehicle_describe_vehicle as
insert into edw.vehicle_describe_vehicle
select a.vin,
  (r.style ->'attributes'->>'modelYear')::integer as model_year,
  (r.style ->'division'->>'$value')::citext as make,
  (r.style ->'model'->>'$value'::citext)::citext as model,
  coalesce(r.style ->'attributes'->>'trim', 'none')::citext as trim_level,
  (c.response->'attributes'->>'bestStyleName')::citext as style_name,
  (r.style->'attributes'->>'altStyleName')::citext as alt_style_name,
  case
    when u.cab->>'$value' like 'Crew%' then 'crew' 
    when u.cab->>'$value' like 'Extended%' then 'double'  
    when u.cab->>'$value' like 'Regular%' then 'reg'  
    else 'n/a'   
  end as chr_cab,   
  case
    when v. bed->>'$value' like '%Bed' then substring(bed->>'$value', 1, position(' ' in bed->>'$value') - 1)
    else 'n/a'
  end as bed ,
  (r.style->'attributes'->>'altBodyType')::citext as alt_body_type,   
  (r.style ->'attributes'->>'mfrModelCode')::citext as model_code,
  case
    when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drive,
  (c.response->'basePrice'->'msrp'->'attributes'->>'low')::numeric::integer as msrp, -- thses are all style_count = 1, no diff between high and low mwrp 
--   (c.response->'basePrice'->'msrp'->'attributes'->>'high')::numeric::integer as high_msrp,    
  s.engine->>'cylinders' as eng_cylinders,
  coalesce(t.displacement->>'$value', 'n/a') as engine_displacement, --a.color, null::integer as configuration_id,
  s.engine->'fuelType'->>'$value' as fuel,
  coalesce(f->>'description', 'unknown') as transmission,
	coalesce(d.color, e.color, 'unknown') as ext_color, coalesce(d.trim, e.trim, 'unknown') as interior,
  (r.style ->'attributes'->>'id')::citext as chrome_style_id, 'Catalog'::citext as source
from (
	select distinct aa.vin
	from edw.board_vins aa
	join chr.describe_vehicle bb on aa.vin = bb.vin
		and bb.style_count = 1
	where not exists (
			select 1
			from edw.vehicle_build_data
			where vin = aa.vin)
		and not exists (
			select 1
			from edw.vehicle_describe_vehicle
			where vin = aa.vin)) a 
join chr.describe_vehicle c on a.vin = c.vin
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(r.style->'bodyType') with ordinality as v(bed) on true
  and v.ordinality =  1    
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
  and s.engine ? 'installed'
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->'attributes'->>'unit' = 'liters'  
left join arkona.ext_inpmast d on a.vin = d.inpmast_vin 
  and inpmast_company_number = 'RY1' 
  and make <> 'toyota'
left join arkona.ext_inpmast e on a.vin = e.inpmast_vin
  and e.inpmast_company_number = 'RY8'
  and e.make = 'toyota'
left join jsonb_array_elements(c.response->'standard') f on true
  and f->>'description' like '%Transmission:%'; 

alter table edw.vehicle_describe_vehicle
add primary key(vin);

  
------------------------------------------------------ chrome non build data style count = 1 />------------------------------------------------------------------


------------------------------------------------------< chrome non build data style count > 1 non ford, model code disambiguated ------------------------------------------------------------------

insert into edw.vehicle_describe_vehicle
select a.vin,
  (r.style ->'attributes'->>'modelYear')::integer as model_year,
  (r.style ->'division'->>'$value')::citext as make,
  (r.style ->'model'->>'$value'::citext)::citext as model,
  coalesce(r.style ->'attributes'->>'trim', 'none')::citext as trim_level,
  (c.response->'attributes'->>'bestStyleName')::citext as style_name,
  (r.style->'attributes'->>'altStyleName')::citext as alt_style_name,
  case
    when u.cab->>'$value' like 'Crew%' then 'crew' 
    when u.cab->>'$value' like 'Extended%' then 'double'  
    when u.cab->>'$value' like 'Regular%' then 'reg'  
    else 'n/a'   
  end as chr_cab,   
  case
    when v. bed->>'$value' like '%Bed' then substring(bed->>'$value', 1, position(' ' in bed->>'$value') - 1)
    else 'n/a'
  end as bed ,
  (r.style->'attributes'->>'altBodyType')::citext as alt_body_type,   
  (r.style ->'attributes'->>'mfrModelCode')::citext as model_code,
  case
    when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drive,
  (c.response->'basePrice'->'msrp'->'attributes'->>'low')::numeric::integer as msrp, -- thses are all style_count = 1, no diff between high and low mwrp 
--   (c.response->'basePrice'->'msrp'->'attributes'->>'high')::numeric::integer as high_msrp,    
  s.engine->>'cylinders' as eng_cylinders,
  coalesce(t.displacement->>'$value', 'n/a') as engine_displacement, --a.color, null::integer as configuration_id,
  s.engine->'fuelType'->>'$value' as fuel,
  coalesce(f->>'description', 'unknown') as transmission,
	coalesce(d.color, e.color, 'unknown') as ext_color, coalesce(d.trim, e.trim, 'unknown') as interior,
  (r.style ->'attributes'->>'id')::citext as chrome_style_id, 'Catalog'::citext as source
from edw.temp_vins_2 a 
join chr.describe_vehicle c on a.vin = c.vin
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(r.style->'bodyType') with ordinality as v(bed) on true
  and v.ordinality =  1    
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
  and s.engine ? 'installed'
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->'attributes'->>'unit' = 'liters'  
left join arkona.ext_inpmast d on a.vin = d.inpmast_vin 
  and inpmast_company_number = 'RY1' 
  and make <> 'toyota'
left join arkona.ext_inpmast e on a.vin = e.inpmast_vin
  and e.inpmast_company_number = 'RY8'
  and e.make = 'toyota'
left join jsonb_array_elements(c.response->'standard') f on true
  and f->>'description' like '%Transmission:%'
where (r.style ->'attributes'->>'mfrModelCode')::citext = coalesce(d.model_code, e.model_code);

------------------------------------------------------ chrome non build data style count > 1 non ford, model code disambiguated />------------------------------------------------------------------

------------------------------------------------------< chrome non build data style count > 1, tool trim disambiguated ------------------------------------------------------------------
-- string_agged transmission to handle the multiple transmissions

-- select string_agg(column_name, ',' order by ordinal_position)
-- from information_schema.columns
-- where table_schema = 'edw'
--   and table_name = 'vehicle_describe_vehicle'

insert into edw.vehicle_describe_vehicle  
select u.vin,model_year,u.make,model,trim_level,style_name,alt_style_name,chr_cab,bed,u.alt_body_type,
	model_code,drive,msrp,eng_cylinders,engine_displacement,fuel,string_agg(distinct transmission, '|'),ext_color,interior,chrome_style_id,source
from (
	select a.vin,
		(r.style ->'attributes'->>'modelYear')::integer as model_year,
		(r.style ->'division'->>'$value')::citext as make,
		(r.style ->'model'->>'$value'::citext)::citext as model,
		coalesce(r.style ->'attributes'->>'trim', 'none')::citext as trim_level,
		(c.response->'attributes'->>'bestStyleName')::citext as style_name,
		(r.style->'attributes'->>'altStyleName')::citext as alt_style_name,
		case
			when u.cab->>'$value' like 'Crew%' then 'crew' 
			when u.cab->>'$value' like 'Extended%' then 'double'  
			when u.cab->>'$value' like 'Regular%' then 'reg'  
			else 'n/a'   
		end as chr_cab,   
		case
			when v. bed->>'$value' like '%Bed' then substring(bed->>'$value', 1, position(' ' in bed->>'$value') - 1)
			else 'n/a'
		end as bed ,
		(r.style->'attributes'->>'altBodyType')::citext as alt_body_type,   
		(r.style ->'attributes'->>'mfrModelCode')::citext as model_code,
		case
			when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
			when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
			when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
			when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
			else 'XXX'
		end as drive,
		(c.response->'basePrice'->'msrp'->'attributes'->>'low')::numeric::integer as msrp, -- thses are all style_count = 1, no diff between high and low mwrp 
	--   (c.response->'basePrice'->'msrp'->'attributes'->>'high')::numeric::integer as high_msrp,    
		s.engine->>'cylinders' as eng_cylinders,
		coalesce(t.displacement->>'$value', 'n/a') as engine_displacement, --a.color, null::integer as configuration_id,
		s.engine->'fuelType'->>'$value' as fuel,
		coalesce(f->>'description', 'unknown') as transmission,
		coalesce(d.color, e.color, 'unknown') as ext_color, coalesce(d.trim, e.trim, 'unknown') as interior,
		(r.style ->'attributes'->>'id')::citext as chrome_style_id, 'Catalog'::citext as source
	from (
		select aa.vin
		from edw.board_vins aa
		where not exists (
				select 1
				from edw.vehicle_build_data
				where vin = aa.vin)
			and not exists (
				select 1
				from edw.vehicle_describe_vehicle
				where vin = aa.vin)) a			
	join chr.describe_vehicle c on a.vin = c.vin
	join jsonb_array_elements(c.response->'style') as r(style) on true
	left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
		and u.ordinality =  2
	left join jsonb_array_elements(r.style->'bodyType') with ordinality as v(bed) on true
		and v.ordinality =  1    
	left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
		and s.engine ? 'installed'
	left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
		and t.displacement ->'attributes'->>'unit' = 'liters'  
	left join arkona.ext_inpmast d on a.vin = d.inpmast_vin 
		and inpmast_company_number = 'RY1' 
		and make <> 'toyota'
	left join arkona.ext_inpmast e on a.vin = e.inpmast_vin
		and e.inpmast_company_number = 'RY8'
		and e.make = 'toyota'
	left join jsonb_array_elements(c.response->'standard') f on true
		and f->>'description' like '%Transmission:%') u
join edw.tool_trim_1 v on u.vin = v.vin
  and u.trim_level = v.chr_trim_level		
group by u.vin,model_year,u.make,model,trim_level,style_name,alt_style_name,chr_cab,bed,u.alt_body_type,model_code,drive,msrp,eng_cylinders,engine_displacement,fuel,ext_color,interior,chrome_style_id,source; 

------------------------------------------------------ chrome non build data style count > 1, tool trim disambiguated />------------------------------------------------------------------  


------------------------------------------------------< chrome non build data style count > 1 ford pickups ------------------------------------------------------------------  

insert into edw.vehicle_describe_vehicle  
select u.vin,model_year,u.make,model,trim_level,style_name,string_agg(distinct alt_style_name, '|'),chr_cab,string_agg(distinct u.bed, '|'),string_agg(distinct u.alt_body_type, '|'),
	model_code,drive,msrp,eng_cylinders,engine_displacement,fuel,string_agg(distinct u.transmission, '|'),ext_color,interior,string_agg(distinct chrome_style_id, '|'), source
from (
	select a.vin,
		(r.style ->'attributes'->>'modelYear')::integer as model_year,
		(r.style ->'division'->>'$value')::citext as make,
		(r.style ->'model'->>'$value'::citext)::citext as model,
		coalesce(r.style ->'attributes'->>'trim', 'none')::citext as trim_level,
		(c.response->'attributes'->>'bestStyleName')::citext as style_name,
		(r.style->'attributes'->>'altStyleName')::citext as alt_style_name,
		case
			when u.cab->>'$value' like 'Crew%' then 'crew' 
			when u.cab->>'$value' like 'Extended%' then 'double'  
			when u.cab->>'$value' like 'Regular%' then 'reg'  
			else 'n/a'   
		end as chr_cab,   
		case
			when v. bed->>'$value' like '%Bed' then substring(bed->>'$value', 1, position(' ' in bed->>'$value') - 1)
			else 'n/a'
		end as bed ,
		(r.style->'attributes'->>'altBodyType')::citext as alt_body_type,   
		(r.style ->'attributes'->>'mfrModelCode')::citext as model_code,
		case
			when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
			when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
			when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
			when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
			else 'XXX'
		end as drive,
		(c.response->'basePrice'->'msrp'->'attributes'->>'low')::numeric::integer as msrp, -- thses are all style_count = 1, no diff between high and low mwrp 
	--   (c.response->'basePrice'->'msrp'->'attributes'->>'high')::numeric::integer as high_msrp,    
		s.engine->>'cylinders' as eng_cylinders,
		coalesce(t.displacement->>'$value', 'n/a') as engine_displacement, --a.color, null::integer as configuration_id,
		s.engine->'fuelType'->>'$value' as fuel,
		coalesce(f->>'description', 'unknown') as transmission,
		coalesce(d.color, e.color, 'unknown') as ext_color, coalesce(d.trim, e.trim, 'unknown') as interior,
		(r.style ->'attributes'->>'id')::citext as chrome_style_id, 'Catalog'::citext as source
	from (
		select aa.vin
		from edw.board_vins aa
		where not exists (
				select 1
				from edw.vehicle_build_data
				where vin = aa.vin)
			and not exists (
				select 1
				from edw.vehicle_describe_vehicle
				where vin = aa.vin)) a			
	join chr.describe_vehicle c on a.vin = c.vin
	join jsonb_array_elements(c.response->'style') as r(style) on true
	left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
		and u.ordinality =  2
	left join jsonb_array_elements(r.style->'bodyType') with ordinality as v(bed) on true
		and v.ordinality =  1    
	left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
		and s.engine ? 'installed'
	left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
		and t.displacement ->'attributes'->>'unit' = 'liters'  
	left join arkona.ext_inpmast d on a.vin = d.inpmast_vin 
		and inpmast_company_number = 'RY1' 
		and make <> 'toyota'
	left join arkona.ext_inpmast e on a.vin = e.inpmast_vin
		and e.inpmast_company_number = 'RY8'
		and e.make = 'toyota'
	left join jsonb_array_elements(c.response->'standard') f on true
		and f->>'description' like '%Transmission:%') u
join edw.ford_pickups v on u.vin = v.vin
  and u.trim_level = v.chr_trim_level		
group by u.vin,model_year,u.make,model,trim_level,style_name,chr_cab,model_code,
	drive,msrp,eng_cylinders,engine_displacement,fuel,ext_color,interior,source; 
------------------------------------------------------ chrome non build data style count > 1 ford pickups />------------------------------------------------------------------  


------------------------------------------------------< update edw.vehicle_describe_vehicle with multiple transmissions />------------------------------------------------------------------ 

update edw.vehicle_describe_vehicle  x
set transmission = y.transmission
from (
	select aa.vin, replace(bb.transmission, 'Transmission: ', '') as transmission
	from ( -- the 77 with multiple transmissions
		select a.vin, a.make, a.model, a.chrome_style_id
		from edw.vehicle_describe_vehicle  a
		where a.transmission like '%|%') aa
	join ( -- only picking up 52 of th 77, the ones with a single chrome style id
		select a.vin, a.make, a.model, a.chrome_style_id, g->>0, c->>'description' as transmission
		from edw.vehicle_describe_vehicle  a
		join chr.describe_vehicle b on a.vin = b.vin
		join jsonb_array_elements(b.response->'standard') c on true
			and c->>'description' like '%Transmission:%'
		join jsonb_array_elements(c->'styleId') g on true 
			and g->>0 = a.chrome_style_id 
		where a.transmission like '%|%') bb on aa.vin = bb.vin) y
where x.vin = y.vin;	

------------------------------------------------------ update edw.vehicle_describe_vehicle with multiple transmissions />------------------------------------------------------------------ 


------------------------------------------------------< chrome non build data style count > 1 model_code & transmission ------------------------------------------------------------------ 
insert into edw.vehicle_describe_vehicle
select a.vin,
  (r.style ->'attributes'->>'modelYear')::integer as model_year,
  (r.style ->'division'->>'$value')::citext as make,
  (r.style ->'model'->>'$value'::citext)::citext as model,
  coalesce(r.style ->'attributes'->>'trim', 'none')::citext as trim_level,
  (c.response->'attributes'->>'bestStyleName')::citext as style_name,
  (r.style->'attributes'->>'altStyleName')::citext as alt_style_name,
  case
    when u.cab->>'$value' like 'Crew%' then 'crew' 
    when u.cab->>'$value' like 'Extended%' then 'double'  
    when u.cab->>'$value' like 'Regular%' then 'reg'  
    else 'n/a'   
  end as chr_cab,   
  case
    when v. bed->>'$value' like '%Bed' then substring(bed->>'$value', 1, position(' ' in bed->>'$value') - 1)
    else 'n/a'
  end as bed ,
  (r.style->'attributes'->>'altBodyType')::citext as alt_body_type,   
  (r.style ->'attributes'->>'mfrModelCode')::citext as model_code,
  case
    when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drive,
  (c.response->'basePrice'->'msrp'->'attributes'->>'low')::numeric::integer as msrp, -- thses are all style_count = 1, no diff between high and low mwrp 
--   (c.response->'basePrice'->'msrp'->'attributes'->>'high')::numeric::integer as high_msrp,    
  s.engine->>'cylinders' as eng_cylinders,
  coalesce(t.displacement->>'$value', 'n/a') as engine_displacement, --a.color, null::integer as configuration_id,
  s.engine->'fuelType'->>'$value' as fuel,
  -- coalesce(f->>'description', 'unknown') as transmission,
  a.transmission,
	coalesce(d.color, e.color, 'unknown') as ext_color, coalesce(d.trim, e.trim, 'unknown') as interior,
  (r.style ->'attributes'->>'id')::citext as chrome_style_id, 'Catalog'::citext as source
from edw.model_codes_transmission_1 a 
join chr.describe_vehicle c on a.vin = c.vin
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(r.style->'bodyType') with ordinality as v(bed) on true
  and v.ordinality =  1    
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
  and s.engine ? 'installed'
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->'attributes'->>'unit' = 'liters'  
left join arkona.ext_inpmast d on a.vin = d.inpmast_vin 
  and d.inpmast_company_number = 'RY1' 
  and d.make <> 'toyota'
left join arkona.ext_inpmast e on a.vin = e.inpmast_vin
  and e.inpmast_company_number = 'RY8'
  and e.make = 'toyota'
-- left join jsonb_array_elements(c.response->'standard') f on true -- don't need the join, have the trans in table edw.model_codes_transmission
--   and f->>'description' like '%Transmission:%'
where (r.style ->'attributes'->>'mfrModelCode')::citext = a.model_code;

------------------------------------------------------ chrome non build data style count > 1 model_code & transmission />------------------------------------------------------------------ 


drop table if exists edw.dim_vehicles cascade;
CREATE TABLE edw.dim_vehicle
(
  vehicle_key integer,
  vin citext NOT NULL,
  model_year integer,
  make citext NOT NULL,
  model citext NOT NULL,
  trim_level citext NOT NULL,
  style_name citext NOT NULL,
  alt_style_name citext NOT NULL,
  cab citext NOT NULL,
  bed citext NOT NULL,
  alt_body_type citext NOT NULL,
  model_code citext NOT NULL,
  drive citext NOT NULL,
  msrp integer NOT NULL,
  eng_cylinders citext NOT NULL,
  engine_displacement citext NOT NULL,
  fuel citext NOT NULL,
  transmission citext NOT NULL,
  ext_color citext NOT NULL,
  interior citext NOT NULL,
  chrome_style_id citext NOT NULL,
  source citext NOT NULL);

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'edw'
  and table_name = 'vehicle_describe_vehicle'  

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'edw'
  and table_name = 'dim_vehicles' 

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'edw'
  and table_name = 'vehicle_build_data'   
  
insert into edw.dim_vehicle (vehicle_key,vin,model_year,make,model,trim_level,style_name,
	alt_style_name,cab,bed,alt_body_type,model_code,drive,msrp,eng_cylinders,
	engine_displacement,fuel,transmission,ext_color,interior,chrome_style_id,source)  
values(1,'unknown',0,'unknown','unknown','unknown','unknown','unknown','unknown','unknown','unknown','unknown','unknown',0,
  'unknown','unknown','unknown','unknown','unknown','unknown','unknown','unknown');


-- DROP SEQUENCE edw.dim_vehicle_vehicle_key_seq;
CREATE SEQUENCE edw.dim_vehicle_vehicle_key_seq
  INCREMENT 1
  start 2;
alter table edw.dim_vehicles alter column vehicle_key
set default nextval('edw.dim_vehicle_vehicle_key_seq'::regclass)
  
insert into edw.dim_vehicles(vin,model_year,make,model,trim_level,style_name,
	alt_style_name,cab,bed,alt_body_type,
	model_code,drive,msrp,eng_cylinders,engine_displacement,
	fuel,transmission,ext_color,
	interior,chrome_style_id,source)  
select vin,model_year,make,model,trim_level,style_name,
	coalesce(alt_style_name,'unknown'),chr_cab,bed,coalesce(alt_body_type,'unknown'),
	coalesce(model_code,'unknown'),drive,msrp,eng_cylinders,engine_displacement,
	fuel,coalesce(transmission,'unknown'),coalesce(ext_color,'unknown'),
	coalesce(interior,'unknown'),chrome_style_id,source
from edw.vehicle_describe_vehicle;

insert into edw.dim_vehicles(vin,model_year,make,model,trim_level,style_name,
	alt_style_name,cab,bed,alt_body_type,
	model_code,drive,msrp,eng_cylinders,engine_displacement,
	fuel,transmission,ext_color,
	interior,chrome_style_id,source)
select vin,model_year,make,model,coalesce(trim_level,'unknown'),style_name,
  coalesce(alt_style_name,'unknown'),cab,bed,coalesce(alt_body_type,'unknown'),
  coalesce(model_code,'unknown'),drive,msrp,coalesce(eng_cylinders,'unknown'),coalesce(eng_displacement,'unknown'),
  fuel,coalesce(transmission,'unknown'),coalesce(ext_color,'unknown'),
  coalesce(interior,'unknown'),chrome_style_id,source
from edw.vehicle_build_data;

select count(*) from edw.dim_vehicle --22040

--------------------------------< and sudo fuckit on the remaining 330 vins --------------------------------------------------------
-- these are all mult style count vins in chrome with no consistent means of identifying the vehicle
insert into edw.dim_vehicles(vin,model_year,make,model,trim_level,style_name,
	alt_style_name,cab,bed,alt_body_type,
	model_code,drive,msrp,eng_cylinders,engine_displacement,
	fuel,transmission,ext_color,
	interior,chrome_style_id,source)  
select vin, year, make, model, coalesce(body_style,'unknown'), 'unknown',
  'unknown','unknown','unknown','unknown',
  coalesce(model_code,'unknown'),'unknown',0,'unknown','unknown',
  'unknown','unknown',coalesce(color,'unknown'),
  coalesce(trim,'unknown'),'unknown','dms-inpmast'
from (
	select distinct a.vin
	from edw.board_vins a
	where not exists (
			select 1
			from edw.vehicle_build_data
			where vin = a.vin)
		and not exists (
			select 1
			from edw.vehicle_describe_vehicle
			where vin = a.vin)) aa	
join arkona.ext_inpmast b on aa.vin = b.inpmast_vin		
  and b.inpmast_company_number = 'RY1';


-- and just 4 more
insert into edw.dim_vehicles(vin,model_year,make,model,trim_level,style_name,
	alt_style_name,cab,bed,alt_body_type,
	model_code,drive,msrp,eng_cylinders,engine_displacement,
	fuel,transmission,ext_color,
	interior,chrome_style_id,source)  
select vin, year, make, model, coalesce(body_style,'unknown'), 'unknown',
  'unknown','unknown','unknown','unknown',
  coalesce(model_code,'unknown'),'unknown',0,'unknown','unknown',
  'unknown','unknown',coalesce(color,'unknown'),
  coalesce(trim,'unknown'),'unknown','dms-inpmast'
from (
	select distinct a.vin
	from edw.board_vins a
	where not exists (
			select 1
			from edw.dim_vehicle
			where vin = a.vin)) aa
join arkona.ext_inpmast b on aa.vin = b.inpmast_vin		
  and b.inpmast_company_number = 'RY8';
-------------------------------- and sudo fuckit on the remaining 331 vins />--------------------------------------------------------

2/2/23
oops, forgot the primary key and indexes

alter table edw.dim_vehicles
add primary key (vehicle_key);

create unique index on edw.dim_vehicles(vin);

