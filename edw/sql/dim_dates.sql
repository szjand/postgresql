﻿2/3/23

new date table for edw
add display format for biweekly pay periods
PK the_date


-- Table: dds.dim_date

-- DROP TABLE dds.dim_date;

CREATE TABLE dds.dim_date
(
  date_key serial NOT NULL,
  the_date date NOT NULL, -- the calendar date
  day_of_week integer,
  day_name citext,
  day_of_month integer,
  day_of_year integer,
  month_of_year integer,
  month_name citext,
  first_day_of_month boolean,
  last_day_of_month boolean,
  weekday boolean,
  weekend boolean,
  holiday boolean NOT NULL,
  the_year integer,
  iso_week integer,
  date_type citext,
  year_month integer,
  year_month_short citext,
  sunday_to_saturday_week integer,
  biweekly_pay_period_start_date date,
  biweekly_pay_period_end_date date,
  year_week integer,
  mmdd citext,
  mmddyy citext,
  last_year_month integer,
  first_of_month date,
  first_of_month_ts timestamp with time zone,
  first_of_last_month date,
  first_of_last_month_ts timestamp with time zone,
  mmmdd citext,
  day_in_biweekly_pay_period integer,
  biweekly_pay_period_sequence integer,
  sunday_to_saturday_week_select_format citext,
  quarter integer,
  last_of_month date,
  biweekly_pay_period_year_month_pct numeric(5,4), -- the percentage of the pay period in the year month, round((count(the_date) over (partition by year_month, biweekly_pay_period_start_date))/14.0, 4)
--   wd_in_year integer,
--   wd_of_year_elapsed integer,
--   wd_of_year_remaining integer,
--   wd_of_year integer,
--   wd_in_month integer,
--   wd_of_month_elapsed integer,
--   wd_of_month_remaining integer,
--   wd_of_month integer,
--   wd_in_biweekly_pay_period integer,
--   wd_of_biweekly_pay_period_elapsed integer,
--   wd_of_bi_weekly_pay_period_remaining integer,
--   wd_of_biweekly_pay_period integer,
--   CONSTRAINT dim_date_pk PRIMARY KEY (date_key)
-- )
-- WITH (
--   OIDS=FALSE
-- ALTER TABLE dds.dim_date
--   OWNER TO rydell;
-- GRANT ALL ON TABLE dds.dim_date TO rydell;
-- GRANT SELECT ON TABLE dds.dim_date TO "Abby";
-- COMMENT ON COLUMN dds.dim_date.the_date IS 'the calendar date';
-- COMMENT ON COLUMN dds.dim_date.biweekly_pay_period_year_month_pct IS 'the percentage of the pay period in the year month, round((count(the_date) over (partition by year_month, biweekly_pay_period_start_date))/14.0, 4)';

select distinct date_type from dds.dim_date

select * from dds.dim_date where date_type = 'na'

-- Index: dds.dim_date_first_of_month_idx

-- DROP INDEX dds.dim_date_first_of_month_idx;

CREATE INDEX dim_date_first_of_month_idx
  ON dds.dim_date
  USING btree
  (first_of_month);

-- Index: dds.dim_date_last_of_month_idx

-- DROP INDEX dds.dim_date_last_of_month_idx;

CREATE INDEX dim_date_last_of_month_idx
  ON dds.dim_date
  USING btree
  (last_of_month);

-- Index: dds.dim_date_the_date_idx

-- DROP INDEX dds.dim_date_the_date_idx;

CREATE UNIQUE INDEX dim_date_the_date_idx
  ON dds.dim_date
  USING btree
  (the_date);

-- Index: dds.dim_date_the_year_idx

-- DROP INDEX dds.dim_date_the_year_idx;

CREATE INDEX dim_date_the_year_idx
  ON dds.dim_date
  USING btree
  (the_year);

-- Index: dds.dim_date_year_month_idx

-- DROP INDEX dds.dim_date_year_month_idx;

CREATE INDEX dim_date_year_month_idx
  ON dds.dim_date
  USING btree
  (year_month);

