﻿
drop table if exists edw.board_vins cascade;
create unlogged table edw.board_vins as --22356
select distinct a.vin -- , a.board_id
from board.sales_board a
join board.board_types c on a.board_type_key = c.board_type_key
  and c.board_type_key = 5
where  a.boarded_date > '12/31/2017'
  and c.board_type = 'Deal'
  and not a.is_deleted;
create index on edw.board_vins(vin);

-- these are all the motorcycles, chrome fails due to no license
delete from edw.board_vins 
where vin in (
	select vin
	from edw.board_vins a
	join board.daily_board b on a.board_id = b.board_id
	where b.vehicle_make in ('yamaha','kawasaki','HARLEY','HARLEY DAVIDSON','HARLEY-DAVIDSON','SUZUKI')); 
	
select distinct vin -- 317
from  edw.board_vins a
where not exists (
  select 1
  from (
		select a.vin  -- 13612
		from  edw.board_vins a  
		join chr.describe_vehicle b on a.vin = b.vin
		union 
		select a.vin  -- 14776
		from  edw.board_vins a
		join chr.build_data_describe_vehicle b on a.vin = b.vin) c
  where vin = a.vin)
order by vin


select b.source, count(*) 
from edw.board_vins a
join chr.build_data_describe_vehicle b on a.vin = b.vin
group by b.source

Sparse,3
,4
Engineered,2
Build,14551
Catalog,3307



select a.vin, b.source -- 14461
-- select distinct style_count
from edw.board_vins a
join chr.build_data_describe_vehicle b on a.vin = b.vin
  and b.source = 'build'
  and b.style_count = 1  

select a.vin, b.source -- 90
-- select distinct style_count
from edw.board_vins a
join chr.build_data_describe_vehicle b on a.vin = b.vin
  and b.source = 'build'
  and b.style_count <> 1    

-- select vin from ( -- 77 dups ?!?!?  -- interior
/*
dups caused by interior (76), peg (1)
need cab and bed, fuel type
add alt stlye name & alt body type
*/

/*
01/25/23
discovered many vehicles in edw.vehicle_describe_vehicle that should have build data
thinking somehow i overlooked a bunch, so

select * from chr.build_data_coverage
-- yeah, like 4302 of them
-- these are all vehicles that fit in build data coverage but were put into vehicle_describe_vehicle
drop table if exists edw.missed_vins cascade;
create unlogged table edw.missed_vins as
select a.vin, a.model_year, a.make
from edw.vehicle_describe_vehicle a
join chr.build_data_coverage b on a.make = b.division
  and a.model_year between b.from_year and b.thru_year

-- used this query to get build data
select distinct a.vin -- 4301
from edw.vehicle_describe_vehicle a
join chr.build_data_coverage b on a.make = b.division
  and a.model_year between b.from_year and b.thru_year
where not exists (
  select 1
  from chr.build_data_describe_vehicle
  where vin = a.vin)  

delete
-- select vin 
from edw.vehicle_describe_vehicle
where make in ('yamaha','kawasaki','HARLEY','HARLEY DAVIDSON','HARLEY-DAVIDSON','SUZUKI')  


select a.vin
from edw.vehicle_build_data a
join edw.vehicle_describe_vehicle b on a.vin = b.vin
*/
------------------------------------------------------ chrome build data ------------------------------------------------------------------

drop table if exists edw.vehicle_build_data cascade;
create unlogged table edw.vehicle_build_data as
-- select vin from (
SELECT a.vin, 
  (c ->>'modelYear')::integer as model_year,
  (c ->'division'->>'_value_1')::citext as make,
  (c ->'model'->>'_value_1'::citext)::citext as model,
  (c->>'trim')::citext as trim_level,
  (b.response->>'bestStyleName')::citext as style_name,
  c->>'altStyleName' as alt_style_name,
  case
    when j.cab->>'_value_1' like 'Crew%' then 'Crew' 
    when  j.cab->>'_value_1' like 'Extended%' then 'Double'  
    when  j.cab->>'_value_1' like 'Regular%' then 'Regular'  
    else 'n/a'   
  end as cab,
  case
    when k.bed->>'_value_1' like '%Bed' then k.bed->>'_value_1'
    else 'n/a'
  end as bed,  
  c->>'altBodyType' as alt_body_type,  
  (c ->>'mfrModelCode')::citext as model_code,
  case
    when c ->>'drivetrain' like 'Rear%' then 'RWD'
    when c ->>'drivetrain' like 'Front%' then 'FWD'
    when c ->>'drivetrain' like 'Four%' then '4WD'
    when c ->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drive,
  (coalesce(b.response->'vinDescription'->>'builtMSRP', c->'basePrice'->>'msrp'))::numeric::integer as msrp,
  d->>'cylinders' as eng_cylinders, e->>'_value_1' as eng_displacement, d->'fuelType'->>'_value_1' as fuel,
  case
    when  (c ->'division'->>'_value_1')::citext = 'Nissan' then 'automatic'
    when m->>0 is null then 'unknown'
    when m->>0 like '%SPEED MANUAL%' then 'manual'
    else 'automatic'
  end as transmission,
  i->>'colorName' as ext_color,
  h->>0 as interior,
  (c->>'id')::citext as chrome_style_id, 'build'::citext as source  
from edw.board_vins a
join chr.build_data_describe_vehicle b on a.vin = b.vin
  and b.source = 'build'
  and b.style_count = 1
join jsonb_array_elements(b.response->'style') as c on true
left join jsonb_array_elements(b.response->'engine') as d on true  
  and (d->'installed'->>'cause' = 'OptionCodeBuild' or coalesce(d->'installed'->>'cause', 'xxx') = 'VIN')
left join jsonb_array_elements(d->'displacement'->'value') as e on true
  and e ->>'unit' = 'liters' 
left join jsonb_array_elements(b.response->'factoryOption') as g on true --interior
  and g->'header'->>'_value_1' = 'SEAT TRIM'
  and g->'installed'->>'cause' in ('RelatedColor','ExteriorColorBuild', 'OptionCodeBuild','InteriorColorBuild')
  and g->'optionKindId' <> 'null' -- 77 vins have multiple objects in factory options that match the interior header value, the relevant value has an optionKindId value
left join jsonb_array_elements(g->'description') as h on true
left join jsonb_array_elements(b.response->'exteriorColor') as i on true
  and i->'installed'->>'cause' in ('RelatedColor','ExteriorColorBuild', 'OptionCodeBuild','InteriorColorBuild')
left join jsonb_array_elements(c->'bodyType') with ordinality as j(cab) on true
  and j.ordinality =  2
left join jsonb_array_elements(c->'bodyType') with ordinality as k(bed) on true
  and k.ordinality =  1
left join jsonb_array_elements(b.response->'factoryOption') l on true
  and l->'header'->>'_value_1' = 'TRANSMISSION'
  and l->'installed'->>'cause' in ('VIN','OptionCodeBuild')
left join jsonb_array_elements(l->'description') m on true order by vin; 
-- ) x group by vin having count(*) > 1  

alter table edw.vehicle_build_data
add primary key(vin);

------------------------------------------------------ chrome build data />------------------------------------------------------------------


------------------------------------------------------< chrome non build data ------------------------------------------------------------------

select distinct a.vin -- 2210
from edw.board_vins a
join chr.describe_vehicle b on a.vin = b.vin
where style_count <> 1
	and not exists (
		select 1
		from edw.vehicle_build_data
		where vin = a.vin)

select distinct a.vin -- 7512
from edw.board_vins a
join chr.describe_vehicle b on a.vin = b.vin
where style_count = 1
	and not exists (
		select 1
		from edw.vehicle_build_data
		where vin = a.vin)

drop table if exists edw.vehicle_describe_vehicle cascade;
create unlogged table edw.vehicle_describe_vehicle as
select a.vin, x.tran->>'description'
  (r.style ->'attributes'->>'modelYear')::integer as model_year,
  (r.style ->'division'->>'$value')::citext as make,
  (r.style ->'model'->>'$value'::citext)::citext as model,
  coalesce(r.style ->'attributes'->>'trim', 'none')::citext as trim_level,
  (c.response->'attributes'->>'bestStyleName')::citext as style_name,
  (r.style->'attributes'->>'altStyleName')::citext as alt_style_name,
  case
    when u.cab->>'$value' like 'Crew%' then 'crew' 
    when u.cab->>'$value' like 'Extended%' then 'double'  
    when u.cab->>'$value' like 'Regular%' then 'reg'  
    else 'n/a'   
  end as chr_cab,   
  case
    when v. bed->>'$value' like '%Bed' then substring(bed->>'$value', 1, position(' ' in bed->>'$value') - 1)
    else 'n/a'
  end as bed ,
  (r.style->'attributes'->>'altBodyType')::citext as alt_body_type,   
  (r.style ->'attributes'->>'mfrModelCode')::citext as model_code,
  case
    when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drive,
  (c.response->'basePrice'->'msrp'->'attributes'->>'low')::numeric::integer as msrp, -- thses are all style_count = 1, no diff between high and low mwrp 
--   (c.response->'basePrice'->'msrp'->'attributes'->>'high')::numeric::integer as high_msrp,    
  s.engine->>'cylinders' as eng_cylinders,
  coalesce(t.displacement->>'$value', 'n/a') as engine_displacement, --a.color, null::integer as configuration_id,
  s.engine->'fuelType'->>'$value' as fuel,
	coalesce(d.color, e.color) as ext_color, coalesce(d.trim, e.trim) as interior,
  (r.style ->'attributes'->>'id')::citext as chrome_style_id, 'Catalog'::citext as source
from (
	select distinct a.vin -- 7512
	from edw.board_vins a
	join chr.describe_vehicle b on a.vin = b.vin
	where style_count = 1
		and not exists (
			select 1
			from edw.vehicle_build_data
			where vin = a.vin)) a
join chr.describe_vehicle c on a.vin = c.vin
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(r.style->'bodyType') with ordinality as v(bed) on true
  and v.ordinality =  1    
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
  and s.engine ? 'installed'
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->'attributes'->>'unit' = 'liters'  
left join arkona.ext_inpmast d on a.vin = d.inpmast_vin 
  and inpmast_company_number = 'RY1' 
  and make <> 'toyota'
left join arkona.ext_inpmast e on a.vin = e.inpmast_vin
  and e.inpmast_company_number = 'RY8'
  and e.make = 'toyota'
where c.style_count = 1

left join jsonb_array_elements(b.response->'standard') as x(tran) on true  
  and x.tran -> 'header' ->> '$value' = 'MECHANICAL'
  and x.tran ->> 'description' like '%Transmission%'  
  

alter table edw.vehicle_describe_vehicle
add primary key(vin);

select count(*)  from edw.vehicle_describe_vehicle --7512
select * 
from (
select * from edw.vehicle_describe_vehicle limit 10) a
union (
select * from edw.vehicle_build_data limit 10) 

select b.response, a.source, model_year, make, a.model
from edw.build_data_describe_vehicle a
join chr.build_data_describe_vehicle b on a.vin = b.vin  
where make = 'toyota'
limit 20
------------------------------------------------------ chrome non build data />-----------------------------------------------------------------


------------------------------------------------------< ford multiple styles -----------------------------------------------------------------


drop table if exists ford cascade;
create temp table ford as
select distinct a.vin, d.style_count, f.model_code as dt_model_code, 
   (e.style ->'attributes'->>'mfrModelCode')::citext as chr_model_code,
   (e.style ->'attributes'->>'modelYear')::integer as model_year,
   (e.style ->'division'->>'$value')::citext as make,
   (e.style ->'attributes'->>'trim')::citext as trim_level,
   e.style->'attributes'->>'altBodyType' as alt_body_type,
   f.body_style as dt_trim,
   g.trim as tool_trim,
   count(*) over (partition by a.vin)-- d.response
from edw.board_vins a
left join (
	select vin
	from (
		select vin
		from edw.vehicle_build_data
		union
		select vin 
		from edw.vehicle_describe_vehicle) b) c on a.vin = c.vin	
join chr.describe_vehicle d on a.vin = d.vin
join jsonb_array_elements(d.response->'style') as e(style) on true	
left join arkona.ext_inpmast f on a.vin = f.inpmast_vin
  and f.inpmast_company_number = 'RY1'
left join ads.ext_vehicle_items g on a.vin = g.vin  
where c.vin is null
  and e.style ->'division'->>'$value' = 'Ford'	
--   and a.vin = '1FT7W2B64HEC97752'
order by a.vin
-- limit 30
-- offset 100

select * from ford

select * from ads.ext_vehicle_items where vin = '1FA6P8CF1J5179647'
select * from arkona.ext_inpmast where inpmast_vin = '1FT7W2BT5KEE33366'
select * from chr.describe_vehicle where vin = '1FT7W2B64HEC97752'

5.5-foot Styleside
67.1 inches long

6.5-foot Styleside
78.9 inches long

8-foot Styleside
97.6 inches long

-- 5 trims, each of which have 2 bed lengths
select b.style->'attributes'->>'trim', b.style->'attributes'->>'altBodyType'
from chr.describe_vehicle a
join jsonb_array_elements(a.response->'style') as b(style) on true
--   and b.style->'attributes'->>'trim' = 'Lariat'
where vin = '1FT7W2B64HEC97752'

------------------------------------------------------ ford multiple styles />-----------------------------------------------------------------


------------------------------------------------------< non ford multiple styles -----------------------------------------------------------------

-- 1st cut, those vehicles that are disambiguated by model code
drop table if exists non_ford cascade;
create temp table non_ford as
select distinct a.vin, d.style_count, f.model_code as dt_model_code, 
   (e.style ->'attributes'->>'mfrModelCode')::citext as chr_model_code,
   (e.style ->'attributes'->>'modelYear')::integer as chr_model_year,
   g.yearmodel as tool_year,
   
   (e.style ->'division'->>'$value')::citext as make,
   (e.style ->'attributes'->>'trim')::citext as chr_trim_level,
   e.style->'attributes'->>'altBodyType' as alt_body_type,
   f.body_style as dt_trim,
   g.trim as tool_trim,
   count(*) over (partition by a.vin) as the_count --, d.response
from edw.board_vins a
left join (
	select vin
	from (
		select vin
		from edw.vehicle_build_data
		union
		select vin 
		from edw.vehicle_describe_vehicle) b) c on a.vin = c.vin	
join chr.describe_vehicle d on a.vin = d.vin
join jsonb_array_elements(d.response->'style') as e(style) on true	
left join arkona.ext_inpmast f on a.vin = f.inpmast_vin
  and f.inpmast_company_number = 'RY1'
  and (e.style ->'attributes'->>'mfrModelCode')::citext = f.model_code
left join ads.ext_vehicle_items g on a.vin = g.vin  
where c.vin is null
  and e.style ->'division'->>'$value' <> 'Ford'	
  and f.model_code is not null
order by a.vin

select * 
from non_ford
where the_count = 1
order by vin

insert into edw.vehicle_describe_vehicle
select a.vin, 
  (r.style ->'attributes'->>'modelYear')::integer as model_year,
  (r.style ->'division'->>'$value')::citext as make,
  (r.style ->'model'->>'$value'::citext)::citext as model,
  coalesce(r.style ->'attributes'->>'trim', 'none')::citext as trim_level,
  (c.response->'attributes'->>'bestStyleName')::citext as style_name,
  (r.style->'attributes'->>'altStyleName')::citext as alt_style_name,
  case
    when u.cab->>'$value' like 'Crew%' then 'crew' 
    when u.cab->>'$value' like 'Extended%' then 'double'  
    when u.cab->>'$value' like 'Regular%' then 'reg'  
    else 'n/a'   
  end as chr_cab,   
  case
    when v. bed->>'$value' like '%Bed' then substring(bed->>'$value', 1, position(' ' in bed->>'$value') - 1)
    else 'n/a'
  end as bed ,
  (r.style->'attributes'->>'altBodyType')::citext as alt_body_type,   
  (r.style ->'attributes'->>'mfrModelCode')::citext as model_code,
  case
    when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drive,
  (c.response->'basePrice'->'msrp'->'attributes'->>'low')::numeric::integer as msrp, -- thses are all style_count = 1, no diff between high and low mwrp 
--   (c.response->'basePrice'->'msrp'->'attributes'->>'high')::numeric::integer as high_msrp,    
  s.engine->>'cylinders' as eng_cylinders,
  coalesce(t.displacement->>'$value', 'n/a') as engine_displacement, --a.color, null::integer as configuration_id,
  s.engine->'fuelType'->>'$value' as fuel,
	coalesce(d.color, e.color) as ext_color, coalesce(d.trim, e.trim) as interior,
  (r.style ->'attributes'->>'id')::citext as chrome_style_id, 'Catalog'::citext 
from (
	select *
	from non_ford
	where the_count = 1) a
join chr.describe_vehicle c on a.vin = c.vin
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(r.style->'bodyType') with ordinality as v(bed) on true
  and v.ordinality =  1    
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
  and s.engine ? 'installed'
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->'attributes'->>'unit' = 'liters'  
left join arkona.ext_inpmast d on a.vin = d.inpmast_vin 
  and d.inpmast_company_number = 'RY1' 
  and d.make <> 'toyota'
left join arkona.ext_inpmast e on a.vin = e.inpmast_vin
  and e.inpmast_company_number = 'RY8'
  and e.make = 'toyota'
where (r.style ->'attributes'->>'mfrModelCode')::citext = a.dt_model_code


-- 2nd cut, those vehicles that are disambiguated by trim
drop table if exists non_ford cascade;
create temp table non_ford as
select distinct a.vin, d.style_count, f.model_code as dt_model_code, 
   (e.style ->'attributes'->>'mfrModelCode')::citext as chr_model_code,
   (e.style ->'attributes'->>'modelYear')::integer as chr_model_year,
   g.yearmodel as tool_year,
   
   (e.style ->'division'->>'$value')::citext as make,
   (e.style ->'attributes'->>'trim')::citext as chr_trim_level,
   e.style->'attributes'->>'altBodyType' as alt_body_type,
   f.body_style as dt_trim,
   g.trim as tool_trim,
   count(*) over (partition by a.vin) as the_count --, d.response
from edw.board_vins a
left join (
	select vin
	from (
		select vin
		from edw.vehicle_build_data
		union
		select vin 
		from edw.vehicle_describe_vehicle) b) c on a.vin = c.vin	
join chr.describe_vehicle d on a.vin = d.vin
join jsonb_array_elements(d.response->'style') as e(style) on true	
left join arkona.ext_inpmast f on a.vin = f.inpmast_vin
  and f.inpmast_company_number = 'RY1'
  and (e.style ->'attributes'->>'mfrModelCode')::citext = f.model_code
left join ads.ext_vehicle_items g on a.vin = g.vin  
where c.vin is null
  and e.style ->'division'->>'$value' <> 'Ford'	
  and f.model_code is not null
order by a.vin
------------------------------------------------------ non ford multiple styles />-----------------------------------------------------------------