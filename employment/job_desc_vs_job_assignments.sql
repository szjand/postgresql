﻿-- both source spreadsheets (in /docs) were recvd from jeri on 6/28
drop table if exists jeri.job_descriptions;
create table jeri.job_descriptions (
  department citext not null,
  job_title citext not null,
  primary key (department,job_title));
  
update jeri.job_descriptions
set department = trim(department),
    job_title = trim(job_title);

drop table if exists jeri.job_assignments;
create table jeri.job_assignments (
  store_code citext not null,
  department citext not null,
  manager citext not null,
  employee_last_name citext not null,
  employee_first_name citext not null,
  job_title citext not null,
  primary key (employee_last_name,employee_first_name));

update jeri.job_assignments
set store_code = trim(store_code),
    department = trim(department),
    manager = trim(manager),
    employee_last_name = trim(employee_last_name),
    employee_first_name = trim(employee_first_name),
    job_title = trim(job_title);

  
-- assignment job titles with no match in job_descriptions
select a.employee_name, a.pymast_employee_number, b.*, c.*
from arkona.xfm_pymast a
left join  jeri.job_assignments b on a.employee_last_name = b.employee_last_name
  and a.employee_first_name = b.employee_first_name
left join jeri.job_descriptions c on b.department = c.department
  and b.job_title = c.job_title
where a.current_row = true
  and a.active_code <> 'T' 
  and c.department is null
  and b.department is not null
order by b.store_code, b.department, b.job_title

-- this is what i sent to jeri on 7/18 as not_in_job_descriptions.xlsx
select store_code, department, manager, job_title, string_agg(employee_last_name, ',')
from (
select a.employee_name, a.pymast_employee_number, b.*--, c.*
  from arkona.xfm_pymast a
  left join  jeri.job_assignments b on a.employee_last_name = b.employee_last_name
    and a.employee_first_name = b.employee_first_name
  left join jeri.job_descriptions c on b.department = c.department
    and b.job_title = c.job_title
  where a.current_row = true
    and a.active_code <> 'T' 
    and c.department is null
    and b.department is not null) x
group by store_code, department, manager, job_title    
order by store_code, department


-- department/job titles in employee listing that don't exist in job descriptions
drop table if exists no_descriptions;
create temp table no_descriptions as
select a.*
from (
  select department, job_title
  from jeri.job_assignments
  group by department, job_title) a
left join (  
  select department, job_title
  from jeri.job_descriptions
  group by department, job_title) b on a.department = b.department and a.job_title = b.job_title
where b.department is null  
order by a.department, a.job_title;

-- employees with jobs without job descriptions
select b.*
from no_descriptions a
left join jeri.job_assignments b on a.department = b.department
  and a.job_title = b.job_title
order by b.store_code, b.department, b.department, b.employee_last_name

-- current employees not in job_assignments
select a.employee_name, a.pymast_employee_number, a.pymast_company_number, aa.description
-- select a.*
from arkona.xfm_pymast a
left join arkona.ext_pypclkctl aa on a.department_code = aa.department_code and a.pymast_company_number = aa.company_number
left join  jeri.job_assignments b on a.employee_last_name = b.employee_last_name
  and a.employee_first_name = b.employee_first_name
where a.current_row = true
  and a.active_code <> 'T' 
  and a.employee_name <> 'TEST'
  and b.store_code is null
order by employee_name

-- compare hash from both tables
select a.*,
  (
    select md5(z::text) as hash
    from (
      select store_code, department, manager, job_title
      from jeri.job_Assignments
      where employee_last_name = a.employee_last_name
        and employee_first_name = a.employee_first_name) z)
from jeri.job_assignments a
where employee_last_name in ('ames','monreal')


-- 7/23 jeri would like dist codes assoc with job titles
select store_code, department, job_title, distrib_code, description, string_agg(pymast_employee_number, ';')::text as emp_numbers
from (
  select a.store_code,  a.department, a.employee_last_name, a.employee_first_name, a.job_title, b.distrib_code, c.description, b.pymast_employee_number
  from jeri.job_assignments a
  left join arkona.xfm_pymast b on a.employee_last_name = b.employee_last_name and a.employee_first_name = b.employee_first_name
    and b.current_row = true
    and b.active_code <> 'T'
  left join (
    select company_number, dist_code, description 
    from arkona.xfm_pyactgr 
    where current_row = true
    group by company_number, dist_code, description) c on b.pymast_company_number = c.company_number
      and b.distrib_code = c.dist_code
  where job_title not in ( '???' , 'Moved to Sales BDC')
    and (a.employee_first_name || ' ' || a.employee_last_name)::citext not in ('coty perez', 'grant demers','brett eken',
      'payton rhoads','laurie mathisen','corey eden','aerica hall','austin oehlke','keaton gilbertson')) x
group by store_code, department, job_title, distrib_code, description
order by store_code, department, job_title, distrib_code


-- 7/23 jeri wants to know job descriptions not being used
select *
from jeri.job_descriptions a
left join jeri.job_assignments b on a.department = b.department and a.job_title = b.job_title
where b.department is null




---------------------------------------------------------------------------------------------------------------
--< some initial clean up
--------------------------------------------------------------------------------------------------------------
1. No Job Description
      Sales   Receptionist

2. BDC vs Main Shop :: General Operator or Service Operator

3. RY2, no distinguishing between main shop and pdq

update jeri.job_assignments
  set department = 'PDQ'
where (
  (employee_last_name = 'CARLSTROM' and employee_first_name = 'NATHAN') or
  (employee_last_name = 'bina' and employee_first_name = 'noah') or
  (employee_last_name = 'edwards' and employee_first_name = 'micah') or
  (employee_last_name = 'fraser' and employee_first_name = 'braden') or
  (employee_last_name = 'enockson' and employee_first_name = 'zachary') or
  (employee_last_name = 'buckholz' and employee_first_name = 'tyler') or
  (employee_last_name = 'batzer' and employee_first_name = 'cory') or
  (employee_last_name = 'moore' and employee_first_name = 'joshua') or
  (employee_last_name = 'halgrimson' and employee_first_name = 'jared') or
  (employee_last_name = 'larson' and employee_first_name = 'cody'));
  
select *
from jeri.job_assignments aa
inner join  (
  select a.pymast_employee_number, a.employee_last_name, a.employee_first_name, b.description
  from arkona.xfm_pymast a
  left join arkona.ext_pypclkctl b on a.department_code = b.department_code and a.pymast_company_number = b.company_number
  where a.current_row = true
    and a.active_code <> 'T'
    and a.pymast_company_number = 'RY2'
    and b.description = 'PDQ') bb on aa.employee_last_name = bb.employee_last_name and aa.employee_first_name = bb.employee_first_name

4. should store be part of position definition, eg, store/department/position

5. no marketing department or positions in job definitions

6. employees missing from manager job assignments
  RY1  Michale Melling  132875  Carwash
  RY2  Kevin Jones  298342  Sales
  RY2  Benjamin Bina  254976  PDQ
  RY1  Nicholas Anderson  165983  PDQ
  RY1  Landon Langenstein  165873  Maintenance
  RY1  James Kuefler  181310  Sales
  RY2  Erik Engebretson  276431
  RY1  Curtis Lembcke  176239  carwash
  RY1  George Yunker  1151450  Sales
  RY2  Nancy Neumann  289563  Receptionist
  RY1  Christian Cahalan 123101 carwash
  



oops
update jeri.job_assignments
set department = 'Carwash'
-- select * from jeri.job_assignments
where department = 'car wash'

4.driv(e)ability
update jeri.job_assignments
set job_title = 'Drivability Technician'
-- select * from jeri.job_assignments
where job_title = 'Driveability Technician'

5. oops
update jeri.job_descriptions
set department = 'parts'
where job_title = 'Wholesale Sales Consultant';

6.
update jeri.job_assignments
set department = 'Sales Team Leader'
-- select * from jeri.job_assignments
where job_title = 'Team Leader'
  and department = 'sales';



Parts:
update jeri.job_assignments
set job_title = 'Shipping & Receiving'
-- select * from jeri.job_assignments
where job_title = 'Shipping Receiving'
  and department = 'Parts'
  
update jeri.job_assignments
set job_title = 'Wholesale Sales Consultant'
-- select * from jeri.job_assignments
where job_title = 'Wholesale Consultant'
  and department = 'Parts'
  
update jeri.job_assignments
set job_title = 'Service Sales Consultant'
-- select * from jeri.job_assignments
where job_title = 'Service Parts Consultant'
  and department = 'Parts'

update jeri.job_assignments
set job_title = 'Counter Sales Consultant'
-- select * from jeri.job_assignments
where job_title = 'Counter Parts Consultant'
  and department = 'Parts'







Body Shop:
update jeri.job_assignments
set job_title = 'Metal Repair Technician (C)'
-- select * from jeri.job_assignments 
where job_title = 'METAL TECH  C';

update jeri.job_assignments
set job_title = 'Metal Repair Technician (A)'
-- select * from jeri.job_assignments 
where job_title = 'METAL TECH  A';

update jeri.job_assignments
set job_title = 'Metal Repair Technician (B)'
-- select * from jeri.job_assignments 
where job_title = 'METAL TECH  B';

update jeri.job_assignments
set job_title = 'Paint Technician (A)'
-- select * from jeri.job_assignments 
where job_title = 'PAINT TECH  A';

update jeri.job_assignments
set job_title = 'Paint Technician (B)'
-- select * from jeri.job_assignments 
where job_title = 'PAINT TECH  B';

update jeri.job_assignments
set job_title = 'Body Shop Manager'
-- select * from jeri.job_assignments 
where job_title = 'MANAGER';

update jeri.job_assignments
set job_title = 'Body Shop Manager'
-- select * from jeri.job_assignments 
where job_title = 'ADMIN';

update jeri.job_assignments
set job_title = 'Foreman'
-- select * from jeri.job_assignments 
where job_title = 'shop foreman'
  and department = 'body shop';


insert into jeri.job_assignments values('Market','IT','Steve Symons','PETERSON','TYLER','IT Helpdesk / System Administrator');
insert into jeri.job_assignments values('Market','Cartiva','Greg Sorum','ANDREWS','JON','Developer');
insert into jeri.job_assignments values('Market','Cartiva','Greg Sorum','SYMONS','ELIZABETH','Developer');
insert into jeri.job_assignments values('Market','Cartiva','Greg Sorum','BRUGGEMAN','AFTON','Developer');
insert into jeri.job_assignments values('Market','Cartiva','Greg Sorum','NOKELBY','RONALD','Developer');
insert into jeri.job_assignments values('Market','Cartiva','Greg Sorum','SORUM','MICHAEL','Developer');
-- insert into jeri.job_assignments values('Market','Hobby Shop','Doug Peterson','SORUM','MICHAEL','Developer');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','ANDERSON','HAILEE','Service Operator');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','ANDERSON','SHAWN','Team Leader');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','AXTMAN','DALE','Driveability Technician');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','BARTA','PATRICK','Driveability Technician');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','BEAR','JEFFERY','Team Leader');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','BERG','STEPHEN','Courtesy Driver');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','BOHM','DOUGLAS','Maintenance Technician');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','BRUNK','CIERRA','Moved to Sales BDC');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','BURSINGER','TRAVIS','Service Advisor');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','CANTU','JASON','Courtesy Driver');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','CARLSON','KENNETH','Service Advisor');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','DAVID','GORDON','Maintenance Technician');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','DUCKSTAD','JARED','Driveability Technician');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','ESPELUND','KENNETH','Service Drive Mgr.');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','EULISS','NED','Service Advisor');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','FERRY','CARTER','Heavy Technician');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','FLAAT','GAVIN','Driveability Technician');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','FRAZIER','TRAYLIN','Service Operator');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','GAGNON','TRACI','Service Operator');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','GAGNON','ANASTASIA','General Operator');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','GAGNON','TAYLOR','General Operator');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','GEHRTZ','CLAYTON','Driveability Technician');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','GIRODAT','JEFF','Team Leader');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','GRANT','DESIREE','Maintenance Technician');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','GRAY','NATHAN','Team Leader');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','HEFFERNAN','JOSH','Heavy Technician');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','HOLWERDA','KIRBY','Maintenance Technician');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','KHATIWODA','TEK','Maintenance Technician');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','KIEFER','LUCAS','Accessory Technician');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','KILMER','JUSTIN','Team Leader BDC');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','KNOWLES','MAYHAYLA','General Operator');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','LINDEMANN','TASHA','Rental Agent');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','LONGORIA','BEVERLEY','Aftermarket and Used Vehicle Service Coordinator ');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','MARO','NICHOLAS','Service Advisor');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','MCVEIGH','DENNIS','Maintenance Technician');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','MONTGOMERY','CODY','Courtesy Driver');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','MORLOCK','BRUCE','Courtesy Driver');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','NEUMANN','ANDREW','Service Director');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','ODEGARD','JOSHUA','Courtesy Driver');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','OLSON','JAY','Team Leader');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','O''NEIL','TIMOTHY','Driveability Technician');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','PAYNE','LORI','General Operator');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','PEREZ','COTY','Maintenance Technician');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','RADKE','DUANE','Maintenance Technician');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','RAMBERG','GARY','Service Advisor');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','ROGERS','MITCH','Driveability Technician');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','ROGNE','CRAIG','Shop Foreman/Mgr');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','SCHUPPERT','KODEY','Maintenance Technician');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','SCHWAN','JASON','Dispatcher');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','SCHWAN','MICHAEL','Driveability Technician');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','SHOULTS','DANIEL','Courtesy Driver');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','STALLMO','RICHARD','Courtesy Driver');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','STINAR','CORY','Service Advisor');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','STOLEY','BETHANY','General Operator');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','STRANDELL','TRAVIS','Maintenance Technician');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','SYVERSON','JOSHUA','Maintenance Technician');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','THOMPSON','WYATT','Heavy Technician');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','TROFTGRUBEN','RODNEY','Service Advisor');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','TROST','SETH','General Operator');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','WALDBAUER','ALEX','Team Leader');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','WALIOR','SUSAN','Rental Team Leader');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','WINKLER','ZACHARY','Driveability Technician');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','WOINAROWICZ','EMILEE','Service Operator');
-- insert into jeri.job_assignments values('RY1','Main Shop','Andrew Neumann','ZACHA','CHARLES','Team Leader');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','ADAM','PATRICK','PREP PERSON');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','AGUILAR','ANNA','ADMIN');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','BENTH','MORGAN','ADMIN');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','BLUMHAGEN','KATHERINE','ADMIN');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','BUCHANAN','KYLE','METAL TECH');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','DIBI','ANDREW','METAL TECH   C');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','DRISCOLL','TERRANCE','METAL TECH  A');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','EBERLE','MARCUS','GLASS TECH  B');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','EVAVOLD','DANIEL','VDA/ESTIMATOR   A');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','GARDNER','CHAD','METAL TECH  A');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','GARDNER','JOHN','ASSISTANT MGR');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','GRYSKIEWICZ','JONATHAN','METAL TECH  C');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','HICKS','EMILY','SCAN/DETAIL TECH');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','HILL','BRIAN','VDA/ESTIMATOR  A');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','JACOBSON','PETER','PAINT TECH  A');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','LAMONT','BRANDON','METAL TECH  C');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','LENE','RYAN','METAL TECH  A');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','LINDOM','AARON','METAL TECH  C');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','LUEKER','DAVID','CSR/ESTIMATOR B');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','MAGNUSON','TYLER','DETAIL TECH   A');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','MAHUTGA','COLTON','METAL TECH  C');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','MAREK','DAYTON','CSR/ESTIMATOR  C');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','MAVITY','ROBERT','PAINT TECH  B');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','OLSON','CODEY','METAL TECH  B');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','OLSON','JUSTIN','METAL TECH  B');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','ORTIZ','GUADALUPE','SCAN TECH  B');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','OSTLUND','ANNE','CSR/ESTIMATOR  A');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','PETERSON','BRIAN','PDR TECH  A');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','PETERSON','MAVRIK','PAINT TECH  A');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','POWELL','GAYLA','ADMIN');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','REUTER','TIMOTHY','VDA/STAGING TECH  A');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','ROSE','CORY','METAL TECH  A');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','SATTLER','RANDY','MANAGER');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','SAULS','THOMAS','BUFFER  A');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','SEVIGNY','SCOTT','METAL TECH  A');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','SHERECK','LOREN','SHOP FOREMAN');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','STEINKE','MARK','VDA/ESTIMATOR  A');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','WALDEN','CHRIS','PAINT TECH  A');
-- insert into jeri.job_assignments values('Market','Body Shop','Randy Sattler','WALTON','JOSHUA','METAL TECH  A');
-- insert into jeri.job_assignments values('Market','Car Wash','Steve Bergh','CARTER','TYUS','Team Member');
-- insert into jeri.job_assignments values('Market','Car Wash','Steve Bergh','GIESLER','MEGAN','Team Member');
-- insert into jeri.job_assignments values('Market','Car Wash','Steve Bergh','GOULET','SHANE','Team Member');
-- insert into jeri.job_assignments values('Market','Car Wash','Steve Bergh','HALL','AERICA','Team Member');
-- insert into jeri.job_assignments values('Market','Car Wash','Steve Bergh','HOBEN','BRENDAN','Team Member');
-- insert into jeri.job_assignments values('Market','Car Wash','Steve Bergh','KILEY','COLTON','Team Member');
-- insert into jeri.job_assignments values('Market','Car Wash','Steve Bergh','MCSHANE','MICHAEL','Team Member');
-- insert into jeri.job_assignments values('Market','Car Wash','Steve Bergh','NELSON','ISAAK','Team Member');
-- insert into jeri.job_assignments values('Market','Car Wash','Steve Bergh','NELSON','ALLISON','Cashier');
-- insert into jeri.job_assignments values('Market','Car Wash','Steve Bergh','NELSON','KORDELL','Team Member');
-- insert into jeri.job_assignments values('Market','Car Wash','Steve Bergh','OEHLKE','AUSTIN','Team Member');
-- insert into jeri.job_assignments values('Market','Car Wash','Steve Bergh','PESEK','BENJAMIN','Team Member');
-- insert into jeri.job_assignments values('Market','Car Wash','Steve Bergh','RYAN','ANTONIO','Carwash Manager');
-- insert into jeri.job_assignments values('Market','Car Wash','Steve Bergh','SALVIONE','SHAYLA','Cashier');
-- insert into jeri.job_assignments values('Market','Car Wash','Steve Bergh','SANCHEZ','ROGELIO','Maintenance Technician');
-- insert into jeri.job_assignments values('Market','Car Wash','Steve Bergh','SINJEM','KATELYNN','Team Member');
-- insert into jeri.job_assignments values('Market','Car Wash','Steve Bergh','SOENKSEN','LINDSEY','Cashier');
-- insert into jeri.job_assignments values('Market','Car Wash','Steve Bergh','SORENSON','THOMAS','Team Member');
-- insert into jeri.job_assignments values('Market','Car Wash','Steve Bergh','STRAND','JETTIE','Cashier');
-- insert into jeri.job_assignments values('Market','Car Wash','Steve Bergh','TELLEZ','MARCUS','Team Member/Maintenance Technician');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','ANDERSON','STEVEN','Sales Driver');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','ANNAN','PAPA KWESI','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','AUBOL','THOMAS','Finance Manager');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','BARKER','JOSHUA','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','BEDNEY','TYLER','Floor Manager');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','BEHM','PAUL','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','BEISWENGER','LYLE','Sales Driver');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','BELGARDE','ROSS','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','BJARNASON','JUSTIN','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','BORGEN','JONATHAN','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','BROOKS','DAMIAN','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','BRUNK','JUSTIN','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','BUSETH','OLIVER','???');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','CAHALAN','BENJAMIN','General Manager');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','CHAVEZ','JESSE','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','CROAKER','CRAIG','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','DECOUTEAU','FRANK','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','DOCKENDORF','NATE','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','EDEN','COREY','Team Leader');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','ERICKSON','RONALD','Used Car Manager');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','FLAAT','STEPHEN','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','FOERSTER','KELLY','New Car Inventory Manager');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','FONTAINE','ASHLEY','New Car Admin');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','FOSTER','BENJAMIN','New Car Sales Manager');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','FOSTER','SAMUEL','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','FRANKS','RAYMOND','Sales Driver');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','HALEY','DYLANGER','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','HARRIS','SHERIDAN','Sales Driver');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','HELGESON','HERB','Sales Driver');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','HILLEBRAND','BROCK','Lot Boy');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','HOLLAND','NIKOLAI','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','IVERSON','ARNOLD','Sales Driver');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','JOHNSON','ERIC','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','KNUDSON','BENJAMIN','Used Car Sales Manager');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','KUESTER','KATIE','BDC');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','LEEDAHL','GLENNA','Sales Driver');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','LIND','KATHERINE','Customer Service Manager');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','LONGORIA','BRANDON','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','LOVEN','ARDEN','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','MENARD','MICHAEL','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','MICHAEL','ANTHONY','Team Leader');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','MONSON','TAYLOR','New Car Inventory Manager');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','NEFS','GERALD','Sales Driver');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','OLDERBAK','JOHN','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','ROBINSON','CARSON','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','RUMEN','ROBERT','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','RYDELL','BRIAN','Dealer');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','RYDELL','WESLEY','Dealer');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','SALBERG','GAYLEN','Sales Driver');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','SCHNEIDER','BRIAN','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','SEAY','BRYN','Team Leader');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','SHIREK','NICHOLAS','GSM');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','STADSTAD','LARRY','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','STOUT','RICK','Finance Manager');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','TARR','JEFF','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','THORSON','LYLE','Sales Driver');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','VANCAMP','KEVIN','Sales Consultant');
-- insert into jeri.job_assignments values('RY1','Sales','Nick Shirek','VANYO','DAVID','Sales Consultant');
-- insert into jeri.job_assignments values('Market','Office','June Gagnon','COCHRAN','MICHELLE','F&I Admin Manager');
-- insert into jeri.job_assignments values('Market','Office','June Gagnon','DAHLEN','DANA','Accounts Payable Clerk');
-- insert into jeri.job_assignments values('Market','Office','June Gagnon','GAGNON','JUNE','Office Manger');
-- insert into jeri.job_assignments values('Market','Office','June Gagnon','GREICAR','JESSICA','Title Clerk');
-- insert into jeri.job_assignments values('Market','Office','June Gagnon','HASTINGS','DAWN','Title Clerk');
-- insert into jeri.job_assignments values('Market','Office','June Gagnon','HOLWEGER','AMANDA','Admin assistant');
-- insert into jeri.job_assignments values('Market','Office','June Gagnon','METZGER','KAYLE','Accountant');
-- insert into jeri.job_assignments values('Market','Office','June Gagnon','MILLER','KIM','Accounts Receivable Payroll Clerk');
-- insert into jeri.job_assignments values('Market','Office','June Gagnon','MOULDS','CRYSTAL','Warranty Administrator');
-- insert into jeri.job_assignments values('Market','Office','June Gagnon','RAUNER','VALERIE','F&I Admin  ');
-- insert into jeri.job_assignments values('Market','Office','June Gagnon','ROTH','LAURA','Human Resource Manager');
-- insert into jeri.job_assignments values('Market','Office','June Gagnon','SCHMIESS PENAS','JERI','Controller');
-- insert into jeri.job_assignments values('Market','Office','June Gagnon','TANDESKI','HEATHER','F&I Admin');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','ADALETE','BRYCE','Upperbay Technician');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','AKER','CADEN','Upperbay Technician');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','ALSLEBEN','DEVIN','Lowerbay Technician');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','BARBER','DYLAN','Lowerbay Technician');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','BROOKS JR','DREW','Lowerbay Technician');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','BUCKHOLZ','ZACHARY','Lowerbay Technician');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','CHAVEZ','ROMAN','Advisor');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','CLARK','JOHNNY','Lowerbay Technician');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','CLARK','ADAM','Upperbay Technician');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','DEMERS','GRANT','Lowerbay Technician');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','EKEN','BRETT','Lowerbay Technician');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','GILBERTSON','KEATON','Upperbay Technician');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','GOTHBERG','CONNER','Advisor');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','GREEN','HARRISON','Advisor');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','HAGA','MASON','Lowerbay Technician');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','MILLER','MICHAEL','Advisor');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','MILLER','DILLAN','Lowerbay Technician');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','NEUMANN','NICKOLAS','PDQ Manager');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','OLSON','WYATT','Advisor');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','O''REILLY','EDWARD','Upperbay Technician');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','PICHE','STACEY','Advisor');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','RAI','SANTI','Upperbay Technician');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','RAI','BIR','Upperbay Technician');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','RHOADS','PAYTON','Lowerbay Technician');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','ROBLES','RUDOLPH','Advisor');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','RODRIGUEZ','JUSTIN','Advisor');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','RODRIGUEZ','OZKAR','Lowerbay Technician');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','ROHLFS','SPENCER','Upperbay Technician');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','ROSENBAUM','ANDREW','Upperbay Technician');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','SAPP','CORDEAU','Advisor');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','SIEDSCHLAG','JACOB','Upperbay Technician');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','TAMANG','CHATUR','Upperbay Technician');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','VELA','GEORGE','Lowerbay Technician');
-- insert into jeri.job_assignments values('RY1','PDQ','Nickolas Neumann','ZUNIGA','FELIX','Upperbay Technician');
-- insert into jeri.job_assignments values('Market','Detail','Steve Bergh','ANDERSON','MATTHEW','Technician');
-- insert into jeri.job_assignments values('Market','Detail','Steve Bergh','BARRETT','THOMAS','Picture taker');
-- insert into jeri.job_assignments values('Market','Detail','Steve Bergh','BERGH','STEVEN','Appearance Director');
-- insert into jeri.job_assignments values('Market','Detail','Steve Bergh','CHAMPAGNE','LESLIE','Cashier');
-- insert into jeri.job_assignments values('Market','Detail','Steve Bergh','DAVIS','ERIC','Technician');
-- insert into jeri.job_assignments values('Market','Detail','Steve Bergh','HEPOLA','JAMES','Technician');
-- insert into jeri.job_assignments values('Market','Detail','Steve Bergh','JOHNSON','BENJAMIN','Technician');
-- insert into jeri.job_assignments values('Market','Detail','Steve Bergh','KOHLS','ADAM','Technician');
-- insert into jeri.job_assignments values('Market','Detail','Steve Bergh','LEAVY','PAUL','Technician');
-- insert into jeri.job_assignments values('Market','Detail','Steve Bergh','MOHAMOUD','MOHAMED','Porter');
-- insert into jeri.job_assignments values('Market','Detail','Steve Bergh','MULHERN','ALAN','Technician');
-- insert into jeri.job_assignments values('Market','Detail','Steve Bergh','NYGAARD','KENT','Porter');
-- insert into jeri.job_assignments values('Market','Detail','Steve Bergh','PRESTON','APRIL','Pictures Support');
-- insert into jeri.job_assignments values('Market','Detail','Steve Bergh','RICHARDSON','DARRICK','Floor Supervisor');
-- insert into jeri.job_assignments values('Market','Detail','Steve Bergh','SMITH','TANER','Towel Support');
-- insert into jeri.job_assignments values('Market','Detail','Steve Bergh','STRAUS','TYLER','Technician');
-- insert into jeri.job_assignments values('Market','Detail','Steve Bergh','SVENSON','JAMES','Porter');
-- insert into jeri.job_assignments values('Market','Detail','Steve Bergh','TELKEN','WYATT','Technician');
-- insert into jeri.job_assignments values('Market','Detail','Steve Bergh','VANHORNE','JAY','Technician');
-- insert into jeri.job_assignments values('Market','Marketing','Morgan Hibma','DELOHERY','MICHAEL','Digital Marketing Specialist');
-- insert into jeri.job_assignments values('Market','Marketing','Morgan Hibma','HIBMA','MORGAN','Marketing Manager');
-- insert into jeri.job_assignments values('Market','Marketing','Morgan Hibma','YEM','RITHY','Digital Marketing SEO Specialist');
-- insert into jeri.job_assignments values('Market','Maintenance','Shawn Squires','EICHHORST','NOEL','Custodian');
-- insert into jeri.job_assignments values('Market','Maintenance','Shawn Squires','ERICKSON','HANNAH','Receptionist');
-- insert into jeri.job_assignments values('Market','Maintenance','Shawn Squires','GAITOR','TERRY','Custodian');
-- insert into jeri.job_assignments values('Market','Maintenance','Shawn Squires','GATHERIDGE','NATASHA','Receptionist');
-- insert into jeri.job_assignments values('Market','Maintenance','Shawn Squires','GUST','BARRY','Facility Maintenance Technician');
-- insert into jeri.job_assignments values('Market','Maintenance','Shawn Squires','HULL','DAVID','Custodian');
-- insert into jeri.job_assignments values('Market','Maintenance','Shawn Squires','JOHNSON','DAVID','Facility Maintenance Technician');
-- insert into jeri.job_assignments values('Market','Maintenance','Shawn Squires','KOENEN','DEBRA','Receptionist');
-- insert into jeri.job_assignments values('Market','Maintenance','Shawn Squires','SHUMAKER','MARTIN','Custodian');
-- insert into jeri.job_assignments values('Market','Maintenance','Shawn Squires','SQUIRES','SHAWN','Maintenance Manager');
-- insert into jeri.job_assignments values('RY2','Sales','Jared Langenstein','AMES','JOSHUA','Sales consultant');
-- insert into jeri.job_assignments values('RY2','Sales','Jared Langenstein','BISENIUS','MICHELLE','Receptionist');
-- insert into jeri.job_assignments values('RY2','Sales','Jared Langenstein','BJERK','CHRISTOPHER','Sales consultant');
-- insert into jeri.job_assignments values('RY2','Sales','Jared Langenstein','BUDEAU','KAYLA','Receptionist');
-- insert into jeri.job_assignments values('RY2','Sales','Jared Langenstein','CALCATERRA','CHAD','Finance Manager');
-- insert into jeri.job_assignments values('RY2','Sales','Jared Langenstein','GABRIELSON','JAMES','Sales consultant');
-- insert into jeri.job_assignments values('RY2','Sales','Jared Langenstein','HAGER','DELWYN','Dont Know him');
-- insert into jeri.job_assignments values('RY2','Sales','Jared Langenstein','HEAZLETT','CURTIS','Sales Driver');
-- insert into jeri.job_assignments values('RY2','Sales','Jared Langenstein','HUGHES','KAITLYN','Receptionist');
-- insert into jeri.job_assignments values('RY2','Sales','Jared Langenstein','HUGHES','KODY','Sales consultant');
-- insert into jeri.job_assignments values('RY2','Sales','Jared Langenstein','KARR','RICHARD','Sales Driver');
-- insert into jeri.job_assignments values('RY2','Sales','Jared Langenstein','LANGENSTEIN','JARED','General Sales Manager');
-- insert into jeri.job_assignments values('RY2','Sales','Jared Langenstein','LIZAKOWSKI','DANIEL','Finance Manager');
-- insert into jeri.job_assignments values('RY2','Sales','Jared Langenstein','MATHISEN','LAURIE','Receptionist');
-- insert into jeri.job_assignments values('RY2','Sales','Jared Langenstein','MONREAL','JESSIE','Sales consultant');
-- insert into jeri.job_assignments values('RY2','Sales','Jared Langenstein','NELSON','ALLEN','Sales Driver');
-- insert into jeri.job_assignments values('RY2','Sales','Jared Langenstein','PFAFF','JONATHAN','Sales consultant');
-- insert into jeri.job_assignments values('RY2','Sales','Jared Langenstein','RYDELL','ROBERT','Dealer Principal');
-- insert into jeri.job_assignments values('RY2','Sales','Jared Langenstein','SPENCER','FRED','Sales consultant');
-- insert into jeri.job_assignments values('RY2','Sales','Jared Langenstein','SABIN','BROOKE','Internet Sales Consultant');
-- insert into jeri.job_assignments values('RY2','Sales','Jared Langenstein','TANDESKI','JADE','Sales Admin');
-- insert into jeri.job_assignments values('RY2','Sales','Jared Langenstein','TANDESKI','JEROME','Sales Driver');
-- insert into jeri.job_assignments values('RY2','Sales','Jared Langenstein','TROSEN','TANNER','Sales consultant');
-- insert into jeri.job_assignments values('RY2','Sales','Jared Langenstein','WILDE','COREY','Sales consultant');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','BARON','CORY','Maintenance Technician');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','BATZER','CORY','Upperbay Technnician');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','BINA','NOAH','Upperbay Technnician');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','BUCKHOLZ','TYLER','Upperbay Technnician');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','CARLSTROM','NATHAN','Lowerbay Technician');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','DANGERFIELD','JOEL','Service Director');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','DOERR','ROY','Courtesy Driver');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','EDWARDS','MICAH','Advisor');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','ENOCKSON','ZACHARY','Upperbay Technnician');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','FRASER','BRADEN','Advisor');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','HALGRIMSON','JARED','Advisor');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','JENSON','MARK','Courtesy Driver');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','JOHNSON','ALLYN','Courtesy Driver');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','JOHNSON','BRANDON','Drivability Technician');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','KELLER','JOSIAH','Maintenance Technician');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','KNUDSON','KENNETH','Heavy Technician');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','LARSON','CODY','Lowerbay Technician');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','MARTINEZ','KENNETH','Service Advisor');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','MATHISEN','GARY','Courtesy Driver');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','MOORE','JOSHUA','Upperbay Technnician');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','PEDERSON','DAVID','Service Advisor');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','PROIS','JUSTIN','Drivability Technician');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','SCHINDLER','TIM','Maintenance Technician');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','SOBOLIK','PAUL','Drivability Technician');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','ZAPZALKA','WYATT','Maintenance Technician');
-- insert into jeri.job_assignments values('RY2','Main Shop','Joel Dangerfield','ZIDON','STEPHEN','Courtesy Driver');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','BJORNSETH','MARK','Inventory Manager');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','BRAATEN','DEAN','Driver & Shipping Receiving');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','FREITAG','TALON','Shipping Receiving');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','HANKIN','BRADLEY','Shipping Receiving');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','HANSON','CONNOR','Shipping Receiving');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','HARMON','TERRENCE','Wholesale Consultant');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','JERSTAD','KENNETH','Service Parts Consultant');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','KOLSTAD','KIRK','Service Parts Consultant');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','KOWALSKI','JASON','Aftermarket Sales');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','LAUGHLIN','LARRY','Wholesale Consultant');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','LINDQUIST','ADAM','Shipping Receiving');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','MANGAN','RICHARD','Shipping Receiving Coordinator');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','METZGER','JUSTIN','Driver  ');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','MEYERS','JAMES','Shipping Receiving');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','MORRIS','GREG','Outside Sales');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','PRESTON','ALLEN','Aftermarket Sales');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','REED','DALE','Driver');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','ROBINSON','AARON','Counter Parts Consultant');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','ROSENTHAL','DAVID','Service Parts Consultant');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','RYGG','KEN','Service Parts Consultant');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','SEEBA','BRADLEY','Wholesale Consultant');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','SHROYER','RYAN','Driver');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','SOLEM','BEN','Counter Parts Consultant');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','STEIN','DONALD','Driver');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','STINAR','DANIEL','Honch');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','STREETER','TIMOTHY','Driver');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','STREITZ','DANIELLE','Outside Sales/Marketing');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','VANHESTE II','FRED','Service Parts Consultant');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','VELA','JUAN CARLOS','Shipping Receiving');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','WINZER','CHRISTOPHER','Driver');
-- insert into jeri.job_assignments values('RY1','Parts','Dan Stinar','ZEMAN','HUNTER','Shipping Receiving');
-- insert into jeri.job_assignments values('RY2','Parts','Dan Stinar','FEIST','LUDWIG','Parts Shipping Receiving');
-- insert into jeri.job_assignments values('RY2','Parts','Dan Stinar','HELGESON','JEFFREY','Parts Consultant');
-- insert into jeri.job_assignments values('RY2','Parts','Dan Stinar','JOHNSON','DUSTIN','Parts Consultant');
-- insert into jeri.job_assignments values('RY2','Parts','Dan Stinar','OLSON','CHAD','Parts Consultant');

  

-- insert into jeri.job_descriptions values('Body Shop','Administrative Assistant');
-- insert into jeri.job_descriptions values('Body Shop','CSR');
-- insert into jeri.job_descriptions values('Body Shop','Detailer');
-- insert into jeri.job_descriptions values('Body Shop','Estimator Administrative Assistant');
-- insert into jeri.job_descriptions values('Body Shop','Estimator  ');
-- insert into jeri.job_descriptions values('Body Shop','Body Shop Manager');
-- insert into jeri.job_descriptions values('Body Shop','Metal Repair Technician (A)');
-- insert into jeri.job_descriptions values('Body Shop','Metal Repair Technician (B)');
-- insert into jeri.job_descriptions values('Body Shop','Metal Repair Technician (C)');
-- insert into jeri.job_descriptions values('Body Shop','Metal Team Leader');
-- insert into jeri.job_descriptions values('Body Shop','Parts Consultant');
-- insert into jeri.job_descriptions values('Body Shop','Porter');
-- insert into jeri.job_descriptions values('Body Shop','Production Manager');
-- insert into jeri.job_descriptions values('Body Shop','Foreman');
-- insert into jeri.job_descriptions values('Body Shop','Glass Technician');
-- insert into jeri.job_descriptions values('Body Shop','Paint Team Leader');
-- insert into jeri.job_descriptions values('Body Shop','Paint Technician (A)');
-- insert into jeri.job_descriptions values('Body Shop','Paint Technician (B)');
-- insert into jeri.job_descriptions values('Body Shop','Paint Technician (C)');
-- insert into jeri.job_descriptions values('Body Shop','PDR Technician');
-- insert into jeri.job_descriptions values('Body Shop','Staging Technician');
-- insert into jeri.job_descriptions values('Call Center','Team Leader');
-- insert into jeri.job_descriptions values('Call Center','General Operator');
-- insert into jeri.job_descriptions values('Call Center','Service Operator');
-- insert into jeri.job_descriptions values('Carwash','Appearance Director');
-- insert into jeri.job_descriptions values('Carwash','Cashier');
-- insert into jeri.job_descriptions values('Carwash','Maintenance Technician');
-- insert into jeri.job_descriptions values('Carwash','Manager');
-- insert into jeri.job_descriptions values('Carwash','Team Member');
-- insert into jeri.job_descriptions values('Detail','Assistant Manager');
-- insert into jeri.job_descriptions values('Detail','Cashier');
-- insert into jeri.job_descriptions values('Detail','Floor Supervisor');
-- insert into jeri.job_descriptions values('Detail','Porter');
-- insert into jeri.job_descriptions values('Detail','Prep Technician');
-- insert into jeri.job_descriptions values('Detail','Support');
-- insert into jeri.job_descriptions values('Detail','Technician');
-- insert into jeri.job_descriptions values('Maintenance','Custodian');
-- insert into jeri.job_descriptions values('Maintenance','Facility Maintenance Technician');
-- insert into jeri.job_descriptions values('Maintenance','Maintenance Manager');
-- insert into jeri.job_descriptions values('Guest Experience','Guest Experience Manager');
-- insert into jeri.job_descriptions values('Guest Experience','Receptionist');
-- insert into jeri.job_descriptions values('Human Resources','HR Assistant');
-- insert into jeri.job_descriptions values('Human Resources','HR Director');
-- insert into jeri.job_descriptions values('Human Resources','HR Manager');
-- insert into jeri.job_descriptions values('Human Resources','Recruiting & Benefits Coordinator');
-- insert into jeri.job_descriptions values('IT','Helpdesk Associate');
-- insert into jeri.job_descriptions values('IT','Intern');
-- insert into jeri.job_descriptions values('IT','Manager');
-- insert into jeri.job_descriptions values('IT','Video Producer');
-- insert into jeri.job_descriptions values('Main Shop','Accessory Technician');
-- insert into jeri.job_descriptions values('Main Shop','Aftermarket & Used Vehicle Service Coordinator');
-- insert into jeri.job_descriptions values('Main Shop','Drivability Technician');
-- insert into jeri.job_descriptions values('Main Shop','Heavy Technician');
-- insert into jeri.job_descriptions values('Main Shop','Maintenance Technician');
-- insert into jeri.job_descriptions values('Main Shop','Team Leader');
-- insert into jeri.job_descriptions values('Main Shop','Technician (C)');
-- insert into jeri.job_descriptions values('Main Shop','Service Director');
-- insert into jeri.job_descriptions values('Main Shop','Dispatcher');
-- insert into jeri.job_descriptions values('Main Shop','Stager');
-- insert into jeri.job_descriptions values('Main Shop','Courtesy Driver');
-- insert into jeri.job_descriptions values('Main Shop','Rental Agent');
-- insert into jeri.job_descriptions values('Main Shop','Rental Team Leader');
-- insert into jeri.job_descriptions values('Main Shop','Service Advisor');
-- insert into jeri.job_descriptions values('Main Shop','Service Drive Manager');
-- insert into jeri.job_descriptions values('Office','Accounting Admin');
-- insert into jeri.job_descriptions values('Office','Accounts Payable Clerk');
-- insert into jeri.job_descriptions values('Office','Accounts Receivable Payroll Clerk');
-- insert into jeri.job_descriptions values('Office','Controller');
-- insert into jeri.job_descriptions values('Office','F&I Admin');
-- insert into jeri.job_descriptions values('Office','File Clerk');
-- insert into jeri.job_descriptions values('Office','Sales Admin');
-- insert into jeri.job_descriptions values('Office','Title Clerk');
-- insert into jeri.job_descriptions values('Office','Warranty Administrator');
-- insert into jeri.job_descriptions values('Parts','Aftermarket Accessories Consultant');
-- insert into jeri.job_descriptions values('Parts','Aftermarket Sales Consultant');
-- insert into jeri.job_descriptions values('Parts','Counter Sales Consultant');
-- insert into jeri.job_descriptions values('Parts','Driver');
-- insert into jeri.job_descriptions values('Parts','Inventory Manager');
-- insert into jeri.job_descriptions values('Parts','Outside Sales Consultant');
-- insert into jeri.job_descriptions values('Parts','Service Sales Consultant');
-- insert into jeri.job_descriptions values('Parts','Shipping & Receiving');
-- insert into jeri.job_descriptions values('PDQ','Wholesale Sales Consultant');
-- insert into jeri.job_descriptions values('PDQ','Advisor');
-- insert into jeri.job_descriptions values('PDQ','Assistant Manager');
-- insert into jeri.job_descriptions values('PDQ','Lowerbay Technician');
-- insert into jeri.job_descriptions values('PDQ','Manager');
-- insert into jeri.job_descriptions values('PDQ','Upperbay Technician');
-- insert into jeri.job_descriptions values('Sales','Appointment Agent');
-- insert into jeri.job_descriptions values('Sales','Digital Coordinator');
-- insert into jeri.job_descriptions values('Sales','Digital Marketing Manager');
-- insert into jeri.job_descriptions values('Sales','Finance Manager');
-- insert into jeri.job_descriptions values('Sales','Internet Sales Consultant');
-- insert into jeri.job_descriptions values('Sales','New Car Inventory Manager');
-- insert into jeri.job_descriptions values('Sales','New Car Sales Manager');
-- insert into jeri.job_descriptions values('Sales','Sales Consultant');
-- insert into jeri.job_descriptions values('Sales','Sales Driver');
-- insert into jeri.job_descriptions values('Sales','Sales Team Leader');
-- insert into jeri.job_descriptions values('Sales','Used Car Inventory Manager');
-- insert into jeri.job_descriptions values('Sales','Used Car Sales Manager');

