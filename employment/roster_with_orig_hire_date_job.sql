﻿-- need to update arkona.ext_pyprhead  & arkona.ext_pyprjobd
-- prior to generating the report
select pymast_company_number as store, employee_name as name, 
  arkona.db2_integer_to_date(a.org_hire_date) as orig_hire_date, d.data as job, 
  case active_code
    when 'A' then 'full'
    when 'P' then 'part'
  end as full_part_time
-- select *
from arkona.xfm_pymast a
left join arkona.ext_pyprhead b on a.pymast_company_number = b.company_number
  and a.pymast_employee_number = b.employee_number
left join arkona.ext_pyprjobd d on b.company_number = d.company_number 
  and b.job_description = d.job_description
where a.current_row
  and a.active_code <> 'T'
  and employee_name <> 'TEST' 
order by pymast_company_number, employee_name