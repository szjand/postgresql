﻿/*
8/9/18
based on spreadsheet from jeri 8/7 called Job Position Master List
spreadsheet in E:\python projects\employment\docs

Page 7 lines 40 -48
spreadsheet does not yet include honda
honda FS goofy: don't believe those numbers are real

*/
this is going to be a helluva modelling challenge

department 
position
dist code (accounting)

Departments (FS) :: Columns (spreadsheet)
NEW                   A
USED                  B
F&I                   D
MECH                  E
BODY                  F
PARTS                 G
TOTAL


select *
from (
  select pymast_company_number, pymast_employee_number, active_code, department_code, 
    employee_last_name, employee_first_name, 
    (select dds.db2_integer_to_date(hire_date)) as hire_date, 
    (select dds.db2_integer_to_date(org_hire_date)) as org_hire_date,
    (select dds.db2_integer_to_date(termination_date)) as termination_date,
    distrib_code, row_from_date, row_thru_date, current_row
  from arkona.xfm_pymast
  where current_row = true) a
where hire_date < '07/31/2018'
  and termination_date > '07/31/2018' 

select a.company_number, a.dist_code, a.description, a.gross_dist
-- select *
from arkona.xfm_pyactgr a
limit 100

select a.account, b.store_code, b.account_type, b.description, b.department
from (
  select gross_expense_act_ as account
  from arkona.xfm_pyactgr
  where gross_expense_act_ <> 'BLANK'
  union
  select overtime_act_
  from arkona.xfm_pyactgr
  where overtime_act_ <> 'BLANK'
  union
  select overtime_act_
  from arkona.xfm_pyactgr
  where overtime_act_ <> 'BLANK'
  union
  select vacation_expense_act_
  from arkona.xfm_pyactgr
  where vacation_expense_act_ <> 'BLANK'
  union
  select holiday_expense_act_
  from arkona.xfm_pyactgr
  where holiday_expense_act_ <> 'BLANK'
  union
  select sick_leave_expense_act_
  from arkona.xfm_pyactgr
  where sick_leave_expense_act_ <> 'BLANK'
  union
  select retire_Expense_act_
  from arkona.xfm_pyactgr
  where retire_Expense_act_ <> 'BLANK'
  union
  select emplr_fica_expense
  from arkona.xfm_pyactgr
  where emplr_fica_expense <> 'BLANK'
  union
  select emplr_med_expense
  from arkona.xfm_pyactgr
  where emplr_med_expense <> 'BLANK'
  union
  select federal_un_emp_act_
  from arkona.xfm_pyactgr
  where federal_un_emp_act_ <> 'BLANK'
  union
  select state_unemp_act_
  from arkona.xfm_pyactgr
  where state_unemp_act_ <> 'BLANK'
  union
  select emplr_contributions
  from arkona.xfm_pyactgr
  where emplr_contributions <> 'BLANK') a
left join fin.dim_account b on a.account = b.account  
order by a.account  



select *
from arkona.xfm_pyactgr a
where a.current_row = true