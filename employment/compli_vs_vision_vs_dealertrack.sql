﻿-- compli
drop table if exists compli;
create temp table compli as
select locationid, lastname, firstname, userid,title, supervisorname
from ads.ext_pto_compli_users a
where a.status = '1'
order by locationid, lastname;
-- 
-- -- vision
-- drop table if exists vision;
-- create temp table vision as
-- select b.employeenumber, b.department, b.position, e.employee_name as pto_auth_by, e.pymast_employee_number as auth_by_employee_number, 
--   c.authbydepartment as auth_by_dept, c.authbyposition as auth_by_pos
-- from compli a
-- inner join ads.ext_ptopositionfulfillment b on a.userid = b.employeenumber 
-- left join ads.ext_pto_authorization c on b.department = c.authfordepartment
--   and b.position = c.authforposition
-- left join ads.ext_ptopositionfulfillment d on c.authbydepartment = d.department
--   and c.authbyposition = d.position  
-- left join arkona.ext_pymast e on d.employeenumber = e.pymast_employee_number;



-- vision
drop table if exists vision;
create temp table vision as
select a.employee_number, b.department as vision_dept, b.position as vision_position, e.employee_name as pto_auth_by
from arkona a
inner join ads.ext_ptopositionfulfillment b on a.employee_number = b.employeenumber 
left join ads.ext_pto_authorization c on b.department = c.authfordepartment
  and b.position = c.authforposition
left join ads.ext_ptopositionfulfillment d on c.authbydepartment = d.department
  and c.authbyposition = d.position  
left join arkona.ext_pymast e on d.employeenumber = e.pymast_employee_number;



-- -- arkona
-- drop table if exists arkona;
-- create temp table arkona as
-- select a.employee_name, a.pymast_company_number, a.pymast_employee_number, 
--   case a.active_code
--     when 'A' then 'full_time'
--     when 'P' then 'part_time'
--   end as status, 
--   b.job_description, d.data,
--   aa.description as department, c.employee_name as supervisor_name
-- -- select a.employee_name
-- from arkona.ext_pymast a
-- left join arkona.ext_pypclkctl aa on a.department_code = aa.department_code
--   and a.pymast_company_number = aa.company_number
-- left join arkona.ext_pyprhead b on a.pymast_company_number = b.company_number
--   and a.pymast_employee_number = b.employee_number
-- left join arkona.ext_pymast c on b.employee_number_yrmgrn = c.pymast_employee_number  
-- left join arkona.ext_pyprjobd d on b.company_number = d.company_number 
--   and b.job_description = d.job_description
-- where a.active_code <> 'T'
--   and a.pymast_company_number in ('RY1','RY2')
-- order by a.employee_name;  


select * 
from arkona a
left join compli b on a.pymast_employee_number = b.userid
left join vision c on a.pymast_employee_number = c.employeenumber


select a.locationid as store, a.lastname, a.firstname, b.status, a.title, a.supervisorname
from compli a
left join arkona b on a.userid = b.pymast_employee_number



-- arkona 6/5/18
drop table if exists arkona;
create temp table arkona as
select a.employee_last_name as last_name, a.employee_first_name as first_name, a.pymast_employee_number as employee_number, 
  case
    when a.active_code = 'P' then 'part'
    when a.active_code = 'A' then 'full'
  end as full_part_time, a.pymast_company_number as store,
  b.description as payroll_dept, a.payroll_class, a.pay_period,
  d.data as job_description, e.employee_name as manager
-- select *  
from arkona.xfm_pymast a
left join arkona.ext_pypclkctl b  on a.department_code = b.department_code
  and a.pymast_company_number = b.company_number
left join arkona.ext_pyprhead c on a.pymast_employee_number = c.employee_number
left join arkona.ext_pyprjobd d on c.company_number = d.company_number
  and c.job_description = d.job_description
left join arkona.xfm_pymast e on c.employee_number_yrmgrn = e.pymast_employee_number  
  and e.current_row = true
where a.current_row = true 
  and a.pymast_company_number in ('RY1','RY2')
  and a.active_code <> 'T';
delete from arkona where  employee_number in ( '291600','230601') and job_description is null;
delete from arkona where employee_number in ('195632','1108248','152495','159100','26442','1148583','181310');



select a.*, b.title as compli_position, b.supervisorname as compli_supervisor, 
  c.vision_dept, c.vision_position, c.pto_auth_by as vision_pto_auth_by
from arkona a
left join compli b on a.employee_number = b.userid
left join vision c on a.employee_number = c.employee_number












