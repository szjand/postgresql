﻿do $$
declare
  _this_week integer := (
    select week_id 
    from fs.weeks
    where '01/05/2020'::date between from_date and thru_date); 
  _from_date date := (
    select from_date
    from fs.weeks
    where week_id = _this_week);
  _thru_date date := (
    select 
      case
        when current_date - 1 < (select thru_date from fs.weeks where week_id = _this_week) then current_date - 1
        else thru_date
      end
    from fs.weeks
    where week_id = _this_week); 
  _store citext := 'RY1';
  _position citext := 'CSR';
begin
drop table if exists ss_data;
create temp table ss_data as
select 1 as seq, left(day_name,3) || ' ' || substring(b.the_date::text, 6,5)  as the_date, b.day_of_week, 
  a.full_name,  c.clock_hours as budgeted_clock_hours, coalesce(e.clock_hours, 0) as actual_clock_hours, coalesce(e.pto_hours, 0) as pto_hours,
  null::text,c.actual_capacity as budgeted_appointments, 
  coalesce(d.appointments, 0) as appointments 
from fs.personnel a
join dds.dim_date b on b.the_date between _from_date and _thru_date
left join fs.personnel_daily_capacities c on a.personnel_id = c.personnel_id
  and  b.date_key = c.date_key
left join fs.csr_appointments_by_day d on b.the_date = d.the_date
  and a.personnel_id = d.personnel_id
left join fs.clock_hours_by_day e on a.personnel_id = e.personnel_id
  and b.the_date = e.the_date
where not a.is_deleted
  and a.personnel_type = _position
-- order by full_name, day_of_week
union
select 2 as seq, null, null::integer as day_of_week, a.full_name,  
  sum(c.clock_hours) as clock_hours, coalesce(sum(e.clock_hours), 0) as actual_clock_hours, coalesce(sum(e.pto_hours), 0) as pto_hours,
  null::text,
  sum(c.actual_capacity) as budgeted_appointments, 
  coalesce(sum(d.appointments), 0) as appointments 
from fs.personnel a
join dds.dim_date b on b.the_date between _from_date and _thru_date
left join fs.personnel_daily_capacities c on a.personnel_id = c.personnel_id
  and  b.date_key = c.date_key
left join fs.csr_appointments_by_day d on b.the_date = d.the_date
  and a.personnel_id = d.personnel_id
left join fs.clock_hours_by_day e on a.personnel_id = e.personnel_id
  and b.the_date = e.the_date
where not a.is_deleted
  and a.personnel_type = _position
group by a.full_name

union
select 3 as seq, left(day_name,3) || ' ' || substring(b.the_date::text, 6,5)  as the_date, b.day_of_week, null,
  sum(c.clock_hours) as clock_hours, coalesce(sum(e.clock_hours), 0) as actual_clock_hours, coalesce(sum(e.pto_hours), 0) as pto_hours,
  null::text,
  sum(c.actual_capacity) as budgeted_appointments, 
  coalesce(sum(d.appointments), 0) as appointments 
from fs.personnel a
join dds.dim_date b on b.the_date between _from_date and _thru_date
left join fs.personnel_daily_capacities c on a.personnel_id = c.personnel_id
  and  b.date_key = c.date_key
left join fs.csr_appointments_by_day d on b.the_date = d.the_date
  and a.personnel_id = d.personnel_id
left join fs.clock_hours_by_day e on a.personnel_id = e.personnel_id
  and b.the_date = e.the_date
where not a.is_deleted
  and a.personnel_type = _position
group by left(day_name,3) || ' ' || substring(b.the_date::text, 6,5), b.day_of_week

union
select 4 as seq, null, null::integer as day_of_week, null,
  sum(c.clock_hours) as clock_hours, coalesce(sum(e.clock_hours), 0) as actual_clock_hours, coalesce(sum(e.pto_hours), 0) as pto_hours,
  null::text,
  sum(c.actual_capacity) as budgeted_appointments, 
  coalesce(sum(d.appointments), 0) as appointments 
from fs.personnel a
join dds.dim_date b on b.the_date between _from_date and _thru_date
left join fs.personnel_daily_capacities c on a.personnel_id = c.personnel_id
  and  b.date_key = c.date_key
left join fs.csr_appointments_by_day d on b.the_date = d.the_date
  and a.personnel_id = d.personnel_id
left join fs.clock_hours_by_day e on a.personnel_id = e.personnel_id
  and b.the_date = e.the_date
where not a.is_deleted
  and a.personnel_type = _position
order by seq, full_name, day_of_week nulls last;

end $$;

select * from ss_data;