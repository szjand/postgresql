﻿  drop table if exists techs cascade;
  create temp table techs as
--   select a.full_name, a.personnel_id, a.personnel_type, a.store_code, a.employee_number, 
--     b.technumber as tech_number, array_agg(distinct b.techkey) as tech_key
--   from fs.personnel a
--   left join ads.ext_Dim_tech b on a.employee_number = b.employeenumber
--     and b.techkey <> 100 -- exclude ancient ryan lene instance
--   where not a.is_deleted
--     and a.personnel_type in ('Main Shop Tech', 'Metal Tech')
--   group by a.full_name, a.personnel_id, a.personnel_type, a.store_code, a.employee_number, b.technumber;
  select a.personnel_id, a.employee_number, c.job, a.full_name, b.store,
    d.technumber as tech_number, array_agg(distinct d.techkey) as tech_key
  from fs.personnel a
  join fs.personnel_jobs b on a.personnel_id = b.personnel_id
    and  b.thru_date > current_date
  join fs.jobs c on b.job = c.job
    and b.store = c.store
    and c.job in ('Metal Tech','Main Shop Tech')
  join ads.ext_dim_tech d on a.employee_number = d.employeenumber
    and d.techkey <> 100
  where not a.is_deleted  
  group by a.full_name, a.personnel_id, c.job,  b.store, a.employee_number, d.technumber;

SELECT * FROM Techs order by full_name

      select count(*)
      SELECT * 
      from fs.personnel a
      join fs.personnel_jobs b on a.personnel_id = b.personnel_id
        and  b.thru_date > current_date
      join fs.jobs c on b.job = c.job
        and b.store = c.store
        and c.job in ('Metal Tech','Main Shop Tech')
      where not a.is_deleted

-- the problem is thomas berge not in techs
select * 
from (
      SELECT full_name 
      from fs.personnel a
      join fs.personnel_jobs b on a.personnel_id = b.personnel_id
        and  b.thru_date > current_date
      join fs.jobs c on b.job = c.job
        and b.store = c.store
        and c.job in ('Metal Tech','Main Shop Tech')
      where not a.is_deleted) a
full outer join 
(select full_name from techs) b on a.full_name = b.full_name

the problem is he is not in ads.ext_dim_tech

select * from dds.dim_tech where tech_name like '%berge%'

select * from fs.personnel where full_name like 'berg%'

select * from arkona.xfm_pymast where pymast_employee_number = '195359' order by pymast_key
select * from ads.ext_dim_tech where description like '%berge%'

select * from arkona.ext_sdptech_tmp order by name

the problem is no tech number in dealertrack for berge, emailed andrew and key