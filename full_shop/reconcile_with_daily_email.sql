﻿-- from fs.update_tech_flag_hours_by_day();
7/14 is a great example, difference due largely due to email includes ros by lucas kiefer and darrick richardson
ros are based solely on date, service type and payment type
so email will include ros not included in full shop, because full shop is limited to a specific list of techs

full shop is based on a list of techs

  -- create a set of relevant tech information
  -- limmit to ry1 main shop techs
  -- this excludes "departmental" techs, eg 103, 621
  drop table if exists techs cascade;
  create temp table techs as
  select a.personnel_id, a.employee_number, c.job, a.full_name, b.store,
    d.technumber as tech_number, array_agg(distinct d.techkey) as tech_key
  from fs.personnel a
  join fs.personnel_jobs b on a.personnel_id = b.personnel_id
    and  b.thru_date > current_date
    and b.store = 'RY1'
  join fs.jobs c on b.job = c.job
    and b.store = c.store
    and c.job in ('Main Shop Tech')
  join ads.ext_dim_tech d on a.employee_number = d.employeenumber
    and d.techkey <> 100
    and d.technumber <>  'D63'  -- exclude ben johnsons detail tech number
  where not a.is_deleted  
  group by a.full_name, a.personnel_id, c.job,  b.store, a.employee_number, d.technumber;
  create unique index on techs(personnel_id);
  create unique index on techs(employee_number);
  create unique index on techs(tech_number,store);
  CREATE INDEX ON techs USING GIN(tech_key);
  create index on techs(job);

select * from techs

--   insert into fs.tech_flag_hours_by_day(personnel_id,the_date,flag_hours,shop_time,training,adjustments)
  select aa.personnel_id, aa.the_date, coalesce(bb.flag_hours, 0) as flag_hours, coalesce(bb.shop, 0) as shop_time, 
    coalesce(bb.training, 0) as training, coalesce(cc.adj, 0) as adjustments
  from ( -- one row for each tech/day
    select a.the_date, b.*
    from dds.dim_date a, techs b
    where a.the_date between '07/11/2021' and '07/17/2021') aa  
  left join (
    select a.storecode, c.personnel_id, b.the_date, ((select full_name from fs.personnel where personnel_id = c.personnel_id)), 
      coalesce(sum(case when aa.opcode not like 'TRAIN%'  and aa.opcode not like 'SHOP%' then a.flaghours end), 0) as flag_hours,
      coalesce(sum(case when aa.opcode like 'SHOP%' then a.flaghours end), 0) as shop,
      coalesce(sum(case when aa.opcode like 'TRAIN%' then a.flaghours end), 0) as training
    from ads.ext_fact_repair_order a
    join techs c on a.techkey = any(c.tech_key)  
    left join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
    join dds.dim_date b on 
      case
        when c.job = 'Metal Tech' then a.closedatekey = b.date_key
        else a.flagdatekey = b.date_key
      end
      and b.the_date between '07/11/2021' and '07/17/2021'
    group by a.storecode, c.personnel_id, b.the_date)  bb on aa.the_date = bb.the_date
      and aa.personnel_id = bb.personnel_id
  left join ( -- one row for each tech/day for which there are hours
    select company_number, technician_id, arkona.db2_integer_to_date_long(trans_date) as trans_date, sum(labor_hours) as adj
    from arkona.ext_sdpxtim
    where arkona.db2_integer_to_date_long(trans_date) between'07/11/2021' and '07/17/2021'
    group by company_number, technician_id, trans_date) cc on aa.the_date = cc.trans_date
      and aa.store = cc.company_number
      and aa.tech_number = cc.technician_id;

-- weekly total
  select sum(coalesce(bb.flag_hours, 0)) as flag_hours, sum(coalesce(bb.shop, 0)) as shop_time, 
    sum(coalesce(bb.training, 0)) as training, sum(coalesce(cc.adj, 0)) as adjustments
  from ( -- one row for each tech/day
    select a.the_date, b.*
    from dds.dim_date a, techs b
    where a.the_date between '07/11/2021' and '07/17/2021') aa  
  left join (
    select a.storecode, c.personnel_id, b.the_date, ((select full_name from fs.personnel where personnel_id = c.personnel_id)), 
      coalesce(sum(case when aa.opcode not like 'TRAIN%'  and aa.opcode not like 'SHOP%' then a.flaghours end), 0) as flag_hours,
      coalesce(sum(case when aa.opcode like 'SHOP%' then a.flaghours end), 0) as shop,
      coalesce(sum(case when aa.opcode like 'TRAIN%' then a.flaghours end), 0) as training
    from ads.ext_fact_repair_order a
    join techs c on a.techkey = any(c.tech_key)  
    left join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
    join dds.dim_date b on 
      case
        when c.job = 'Metal Tech' then a.closedatekey = b.date_key
        else a.flagdatekey = b.date_key
      end
      and b.the_date between '07/11/2021' and '07/17/2021'
    group by a.storecode, c.personnel_id, b.the_date)  bb on aa.the_date = bb.the_date
      and aa.personnel_id = bb.personnel_id
  left join ( -- one row for each tech/day for which there are hours
    select company_number, technician_id, arkona.db2_integer_to_date_long(trans_date) as trans_date, sum(labor_hours) as adj
    from arkona.ext_sdpxtim
    where arkona.db2_integer_to_date_long(trans_date) between'07/11/2021' and '07/17/2021'
    group by company_number, technician_id, trans_date) cc on aa.the_date = cc.trans_date
      and aa.store = cc.company_number
      and aa.tech_number = cc.technician_id;

-- daily total
  select aa.the_date, sum(coalesce(bb.flag_hours, 0)) as flag_hours, sum(coalesce(bb.shop, 0)) as shop_time, 
    sum(coalesce(bb.training, 0)) as training, sum(coalesce(cc.adj, 0)) as adjustments
  from ( -- one row for each tech/day
    select a.the_date, b.*
    from dds.dim_date a, techs b
    where a.the_date between '07/11/2021' and '07/17/2021') aa  
  left join (
    select a.storecode, c.personnel_id, b.the_date, ((select full_name from fs.personnel where personnel_id = c.personnel_id)), 
      coalesce(sum(case when aa.opcode not like 'TRAIN%'  and aa.opcode not like 'SHOP%' then a.flaghours end), 0) as flag_hours,
      coalesce(sum(case when aa.opcode like 'SHOP%' then a.flaghours end), 0) as shop,
      coalesce(sum(case when aa.opcode like 'TRAIN%' then a.flaghours end), 0) as training
    from ads.ext_fact_repair_order a
    join techs c on a.techkey = any(c.tech_key)  
    left join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
    join dds.dim_date b on 
      case
        when c.job = 'Metal Tech' then a.closedatekey = b.date_key
        else a.flagdatekey = b.date_key
      end
      and b.the_date between '07/11/2021' and '07/17/2021'
    group by a.storecode, c.personnel_id, b.the_date)  bb on aa.the_date = bb.the_date
      and aa.personnel_id = bb.personnel_id
  left join ( -- one row for each tech/day for which there are hours
    select company_number, technician_id, arkona.db2_integer_to_date_long(trans_date) as trans_date, sum(labor_hours) as adj
    from arkona.ext_sdpxtim
    where arkona.db2_integer_to_date_long(trans_date) between'07/11/2021' and '07/17/2021'
    group by company_number, technician_id, trans_date) cc on aa.the_date = cc.trans_date
      and aa.store = cc.company_number
      and aa.tech_number = cc.technician_id
group by aa.the_date
order by aa.the_date;           

-- daily with tech
-- daily total
  select aa.the_date, aa.full_name, sum(coalesce(bb.flag_hours, 0)) as flag_hours, sum(coalesce(bb.shop, 0)) as shop_time, 
    sum(coalesce(bb.training, 0)) as training, sum(coalesce(cc.adj, 0)) as adjustments
  from ( -- one row for each tech/day
    select a.the_date, b.*
    from dds.dim_date a, techs b
    where a.the_date between '07/11/2021' and '07/17/2021') aa  
  left join (
    select a.storecode, c.personnel_id, b.the_date, ((select full_name from fs.personnel where personnel_id = c.personnel_id)), 
      coalesce(sum(case when aa.opcode not like 'TRAIN%'  and aa.opcode not like 'SHOP%' then a.flaghours end), 0) as flag_hours,
      coalesce(sum(case when aa.opcode like 'SHOP%' then a.flaghours end), 0) as shop,
      coalesce(sum(case when aa.opcode like 'TRAIN%' then a.flaghours end), 0) as training
    from ads.ext_fact_repair_order a
    join techs c on a.techkey = any(c.tech_key)  
    left join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
    join dds.dim_date b on 
      case
        when c.job = 'Metal Tech' then a.closedatekey = b.date_key
        else a.flagdatekey = b.date_key
      end
      and b.the_date between '07/11/2021' and '07/17/2021'
    group by a.storecode, c.personnel_id, b.the_date)  bb on aa.the_date = bb.the_date
      and aa.personnel_id = bb.personnel_id
  left join ( -- one row for each tech/day for which there are hours
    select company_number, technician_id, arkona.db2_integer_to_date_long(trans_date) as trans_date, sum(labor_hours) as adj
    from arkona.ext_sdpxtim
    where arkona.db2_integer_to_date_long(trans_date) between'07/11/2021' and '07/17/2021'
    group by company_number, technician_id, trans_date) cc on aa.the_date = cc.trans_date
      and aa.store = cc.company_number
      and aa.tech_number = cc.technician_id
where aa.the_date = '07/14/2021'      
group by aa.the_date, aa.full_name
order by aa.the_date, aa.full_name  

select * from fs.personnel order by full_name
select * from techs order by full_name
select * from arkona.xfm_pymast where employee_last_name = 'kiefer' order by pymast_key