﻿-- create schema fs;
-- comment on schema fs is 'a place to stick tables for the full shop project';

/*
-- 11/2919
  move capacity to separate tables
  add from_date/thru_date to each table, PK changes to emp#/thru_date
  grain of these personnel tables is a day
*/


-- bdr
drop table if exists fs.service_bdrs cascade;
create table fs.service_bdrs (
  store citext not null,
  last_name citext not null,
  first_name citext not null,
  employee_number citext not null,
  from_date date not null,
  thru_date date not null default '12/31/9999'::date,
  primary key (employee_number,thru_date));  
comment on table fs.service_bdrs is 'yet another place to store service_bdr information in the absence of a unified data model';

insert into fs.service_bdrs (store,last_name,first_name,employee_number,from_date)
select 'RY1', employee_last_name, employee_first_name, pymast_employee_number, '11/01/2019'::date
from arkona.xfm_pymast
where current_row
  and active_code <> 'T'
  and pymast_employee_number in ('149878','132987','1149710','16250','197643');

-- writers
drop table if exists fs.writers cascade;
create table fs.writers (
  store citext not null,
  last_name citext not null,
  first_name citext not null,
  employee_number citext not null,
  writer_id citext not null,
  from_date date not null,
  thru_date date not null default '12/31/9999'::date,
  primary key (employee_number,thru_date));
comment on table fs.writers is 'yet another place to store writer information in the absence of a unified data model';
comment on column fs.writers.writer_id is 'writer id from dealertrack';

insert into fs.writers (store,last_name,first_name,employee_number,writer_id,from_date)
select 'RY1', a.employee_last_name, a.employee_first_name, a.pymast_employee_number, b.writernumber, '11/01/2019'::date
from arkona.xfm_pymast a
join ads.ext_dim_service_writer b on a.pymast_employee_number = b.employeenumber
  and b.currentrow
  and b.active
where a.current_row
  and a.active_code <> 'T'
  and a.pymast_employee_number in ('195628','1117842','141060','152499','191060','1140870','172308','118520');


-- techs
drop table if exists fs.techs cascade;
create table fs.techs (
  store citext not null,
  last_name citext not null,
  first_name citext not null,
  employee_number citext not null,
  tech_id citext not null,
  from_date date not null,
  thru_date date not null default '12/31/9999'::date,
  primary key (employee_number,thru_date));
comment on table fs.techs is 'yet another place to store techs information in the absence of a unified data model';
comment on column fs.techs.tech_id is 'tech id from dealertrack';

insert into fs.techs (store,last_name,first_name,employee_number,tech_id,from_date)
select 'RY1', a.employee_last_name, a.employee_first_name, a.pymast_employee_number, b.technumber, '11/01/2019'::date
from arkona.xfm_pymast a
join ads.ext_dim_tech b on a.pymast_employee_number = b.employeenumber
  and b.currentrow
  and b.active
  and b.technumber <> '296'
where a.current_row
  and a.active_code <> 'T'
  and a.pymast_employee_number in ('136170','18005','1124436','1106399','152410','1106421',
    '150126','1117901','145801','1149300','164015','13725','152235','1137590','111210',
    '1135410','131370','194675','1150920','159863','152525','167580','168753','1146989',
    '178050','1113940','159298','152964','168573','1124400','116185');


drop table if exists fs.weeks;
create table fs.weeks (
  week_id integer primary key,
  from_date date not null,
  thru_date date not null);
create index on fs.weeks(from_date);  
create index on fs.weeks(thru_date); 
insert into fs.weeks
select sunday_to_saturday_week, min(the_date) as from_date, max(the_date) as thru_date
from dds.dim_date
where the_year > 2017
group by sunday_to_saturday_week;    