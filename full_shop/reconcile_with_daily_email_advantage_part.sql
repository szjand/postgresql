-- the original query
            SELECT -- flaghours yesterday
              (SELECT curdate() FROM system.iota),
              (SELECT curtime() FROM system.iota),
              CASE d.paymenttype
                WHEN 'customer pay' THEN 3
                WHEN 'internal' THEN 4
                WHEN 'warranty' THEN 5
                WHEN 'service contract' THEN 6
              END AS seq,
              trim (d.paymenttype) + ' Flag Hours', round(SUM(flaghours), 1)
            FROM factrepairorder a
            INNER JOIN day b on a.closedatekey = b.datekey
              AND b.datetype = 'DATE'
              AND b.thedate = curdate() -1
            INNER JOIN dimServiceType c on a.servicetypekey = c.servicetypekey
              AND c.servicetypecode IN ('AM','CM','EM','MN','MR','SV')
            INNER JOIN dimpaymenttype d on a.paymenttypekey = d.paymenttypekey
              AND d.paymenttypekey IN (2,3,4,5)
            where a.storecode = 'RY1'  
            GROUP by d.paymenttype
						
-- change to flag_date AND skip pay type (for now)
-- 11 hours less than full shop
            SELECT  round(SUM(flaghours), 1)
            FROM factrepairorder a
            INNER JOIN day b on a.flagdatekey = b.datekey
              AND b.datetype = 'DATE'
              AND b.thedate = curdate() -1
            INNER JOIN dimServiceType c on a.servicetypekey = c.servicetypekey
              AND c.servicetypecode IN ('AM','CM','EM','MN','MR','SV')
            INNER JOIN dimpaymenttype d on a.paymenttypekey = d.paymenttypekey
              AND d.paymenttypekey IN (2,3,4,5)
            where a.storecode = 'RY1'  

-- DO the week
-- each day IS a little less than fs
-- ADD qs AND no each day IS a little more than fs
-- exclude tech 103, 621 AND each day IS closer, 7/14 IS still off BY 7 hours
            SELECT b.thedate, round(SUM(flaghours), 1)
            FROM factrepairorder a
            INNER JOIN day b on a.flagdatekey = b.datekey
              AND b.datetype = 'DATE'
              AND b.thedate = '07/14/2021' -- BETWEEN '07/11/2021' AND '07/17/2021'
            INNER JOIN dimServiceType c on a.servicetypekey = c.servicetypekey
              AND c.servicetypecode IN ('AM','CM','EM','MN','MR','SV','qs')
            INNER JOIN dimpaymenttype d on a.paymenttypekey = d.paymenttypekey
              AND d.paymenttypekey IN (2,3,4,5)
            where a.storecode = 'RY1'  
						  AND a.techkey NOT IN (804,1026)
						GROUP BY b.thedate
						
										
-- ADD techs
            SELECT e.technumber, e.techkey, e.name, round(SUM(flaghours), 1)
            FROM factrepairorder a
            INNER JOIN day b on a.flagdatekey = b.datekey
              AND b.datetype = 'DATE'
              AND b.thedate = '07/14/2021'
            INNER JOIN dimServiceType c on a.servicetypekey = c.servicetypekey
              AND c.servicetypecode IN ('AM','CM','EM','MN','MR','SV', 'qs')
            INNER JOIN dimpaymenttype d on a.paymenttypekey = d.paymenttypekey
              AND d.paymenttypekey IN (2,3,4,5)
						JOIN dimtech e on a.techkey = e.techkey
            where a.storecode = 'RY1'  		
						AND a.techkey NOT IN (804,1026)
						GROUP BY e.technumber, e.techkey, e.name
						order BY e.name
						
SELECT * FROM dimtech WHERE technumber IN ('103','621') AND currentrow			

-- kiefer ro
            SELECT a.ro, e.technumber, e.techkey, e.name, a.flaghours, c.servicetype
            FROM factrepairorder a
            INNER JOIN day b on a.flagdatekey = b.datekey
              AND b.datetype = 'DATE'
              AND b.thedate = '07/14/2021'
            INNER JOIN dimServiceType c on a.servicetypekey = c.servicetypekey
              AND c.servicetypecode IN ('AM','CM','EM','MN','MR','SV', 'qs')
            INNER JOIN dimpaymenttype d on a.paymenttypekey = d.paymenttypekey
              AND d.paymenttypekey IN (2,3,4,5)
						JOIN dimtech e on a.techkey = e.techkey
            where a.storecode = 'RY1'  		
						AND a.techkey = 966