﻿
-- main shop tech capacity vs actual for the week of 12/15 -> 12/21
select 1 as seq, left(day_name,3) || ' ' || substring(b.the_date::text, 6,5)  as the_date, 
  a.full_name,  c.clock_hours, c.actual_capacity as capacity, null::text,
  coalesce(e.clock_hours, 0) as actual_clock_hours, coalesce(d.flag_hours, 0) as flag_hours,  coalesce(e.pto_hours, 0) as pto_hours
from fs.personnel a 
join dds.dim_date b on b.the_date between '12/16/2019' and '12/21/2019'
left join fs.personnel_daily_capacities c on a.personnel_id = c.personnel_id
  and b.date_key = c.date_key
left join fs.tech_flag_hours_by_day d on b.the_date = d.the_date
  and a.personnel_id = d.personnel_id 
left join fs.clock_hours_by_day e on a.personnel_id = e.personnel_id
      and b.the_date = e.the_date   
where not a.is_deleted
  and a.personnel_type = 'main shop tech'
  and a.store_code = 'RY2'
-- order by a.full_name, b.the_date
union

select 2 as seq, null, a.full_name,  sum(c.clock_hours) as clock_housr, sum(c.actual_capacity) as capacity, null::text,
  coalesce(sum(e.clock_hours), 0) as actual_clock_hours, coalesce(sum(d.flag_hours), 0) as flag_hours,  coalesce(sum(e.pto_hours), 0) as pto_hours
from fs.personnel a 
join dds.dim_date b on b.the_date between '12/16/2019' and '12/21/2019'
left join fs.personnel_daily_capacities c on a.personnel_id = c.personnel_id
  and b.date_key = c.date_key
left join fs.tech_flag_hours_by_day d on b.the_date = d.the_date
  and a.personnel_id = d.personnel_id 
left join fs.clock_hours_by_day e on a.personnel_id = e.personnel_id
      and b.the_date = e.the_date   
where not a.is_deleted
  and a.personnel_type = 'main shop tech'
  and a.store_code = 'RY2'
group by a.full_name
-- order by a.full_name
union

select 3 as seq, null, null, sum(c.clock_hours) as clock_housr, sum(c.actual_capacity) as capacity, null::text,
  coalesce(sum(e.clock_hours), 0) as actual_clock_hours, coalesce(sum(d.flag_hours), 0) as flag_hours,  coalesce(sum(e.pto_hours), 0) as pto_hours
from fs.personnel a 
join dds.dim_date b on b.the_date between '12/16/2019' and '12/21/2019'
left join fs.personnel_daily_capacities c on a.personnel_id = c.personnel_id
  and b.date_key = c.date_key
left join fs.tech_flag_hours_by_day d on b.the_date = d.the_date
  and a.personnel_id = d.personnel_id 
left join fs.clock_hours_by_day e on a.personnel_id = e.personnel_id
      and b.the_date = e.the_date   
where not a.is_deleted
  and a.personnel_type = 'main shop tech'
  and a.store_code = 'RY2'
order by seq, full_name, the_date nulls last



-- 1/3/19 been wants the order rearrange: budgeted next to actual for each metric
-- main shop tech capacity vs actual for the week of 12/15 -> 12/21
this looks good for all techs
-- tech detail
select 1 as seq, left(day_name,3) || ' ' || substring(b.the_date::text, 6,5)  as the_date, b.day_of_week, 
  a.full_name,  c.clock_hours as budgeted_clock_hours, coalesce(e.clock_hours, 0) as actual_clock_hours, coalesce(e.pto_hours, 0) as pto_hours,
  null::text,c.actual_capacity as budgeted_flag_hours, 
  coalesce(d.flag_hours, 0) + coalesce(d.adjustments, 0) as flag_hours,  
  coalesce(d.shop_time, 0) as shop_time, 
  coalesce(d.training, 0) as training
from fs.personnel a 
join dds.dim_date b on b.the_date between '12/16/2019' and '12/21/2019'
left join fs.personnel_daily_capacities c on a.personnel_id = c.personnel_id
  and b.date_key = c.date_key
left join fs.tech_flag_hours_by_day d on b.the_date = d.the_date
  and a.personnel_id = d.personnel_id 
left join fs.clock_hours_by_day e on a.personnel_id = e.personnel_id
      and b.the_date = e.the_date   
where not a.is_deleted
  and a.personnel_type = 'metal tech'
  and a.store_code = 'RY1'

union
-- tech weekly total
select 2 as seq, null, null::integer as day_of_week, a.full_name,  
  sum(c.clock_hours) as clock_hours, coalesce(sum(e.clock_hours), 0) as actual_clock_hours, coalesce(sum(e.pto_hours), 0) as pto_hours,
  null::text, 
  sum(c.actual_capacity) as capacity,
  coalesce(sum(d.flag_hours), 0)  + coalesce(sum(d.adjustments), 0)as flag_hours, 
  coalesce(sum(d.shop_time), 0) as shop_time, 
  coalesce(sum(d.training), 0) as training 
from fs.personnel a 
join dds.dim_date b on b.the_date between '12/16/2019' and '12/21/2019'
left join fs.personnel_daily_capacities c on a.personnel_id = c.personnel_id
  and b.date_key = c.date_key
left join fs.tech_flag_hours_by_day d on b.the_date = d.the_date
  and a.personnel_id = d.personnel_id 
left join fs.clock_hours_by_day e on a.personnel_id = e.personnel_id
      and b.the_date = e.the_date   
where not a.is_deleted
  and a.personnel_type = 'metal tech'
  and a.store_code = 'RY1'
group by a.full_name  

union
-- department daily total
select 3 as seq, left(day_name,3) || ' ' || substring(b.the_date::text, 6,5)  as the_date, b.day_of_week, null, 
  sum(c.clock_hours) as clock_hours, coalesce(sum(e.clock_hours), 0) as actual_clock_hours, coalesce(sum(e.pto_hours), 0) as pto_hours,
  null::text, 
  sum(c.actual_capacity) as capacity,
  coalesce(sum(d.flag_hours), 0)  + coalesce(sum(d.adjustments), 0)as flag_hours, 
  coalesce(sum(d.shop_time), 0) as shop_time, 
  coalesce(sum(d.training), 0) as training 
from fs.personnel a 
join dds.dim_date b on b.the_date between '12/16/2019' and '12/21/2019'
left join fs.personnel_daily_capacities c on a.personnel_id = c.personnel_id
  and b.date_key = c.date_key
left join fs.tech_flag_hours_by_day d on b.the_date = d.the_date
  and a.personnel_id = d.personnel_id 
left join fs.clock_hours_by_day e on a.personnel_id = e.personnel_id
      and b.the_date = e.the_date   
where not a.is_deleted
  and a.personnel_type = 'metal tech'
  and a.store_code = 'RY1'
group by left(day_name,3) || ' ' || substring(b.the_date::text, 6,5), b.day_of_week

union
-- department weekly total
select 4 as seq, null, null::integer as day_of_week, null, 
  sum(c.clock_hours) as clock_hours, coalesce(sum(e.clock_hours), 0) as actual_clock_hours, coalesce(sum(e.pto_hours), 0) as pto_hours,
  null::text,
  sum(c.actual_capacity) as capacity, 
  coalesce(sum(d.flag_hours), 0)  + coalesce(sum(d.adjustments), 0)as flag_hours, 
  coalesce(sum(d.shop_time), 0) as shop_time, 
  coalesce(sum(d.training), 0) as training  
from fs.personnel a 
join dds.dim_date b on b.the_date between '12/16/2019' and '12/21/2019'
left join fs.personnel_daily_capacities c on a.personnel_id = c.personnel_id
  and b.date_key = c.date_key
left join fs.tech_flag_hours_by_day d on b.the_date = d.the_date
  and a.personnel_id = d.personnel_id 
left join fs.clock_hours_by_day e on a.personnel_id = e.personnel_id
      and b.the_date = e.the_date   
where not a.is_deleted
  and a.personnel_type = 'metal tech'
  and a.store_code = 'RY1'
order by seq, full_name, day_of_week nulls last