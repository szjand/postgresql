﻿greg is dying to know
why for the week ending 02/01, GM main shop had 1467.3 flag hours with 1195.15 clock hours
but for the week ending 01/25, the had 1163.2 flag hours with 1207.94 clock hours


-- 02/04
-- bens observation may be the best thus far
-- end of the month, ros getting closed
-- add open and close dates to the data set
-- check ro history for times in status ?
-- need to verify flag date vs close date

-- from fs.update_tech_flag_hours_by_day()
drop table if exists techs cascade;
-- limit to gm main shop
create temp table techs as
select a.personnel_id, a.employee_number, c.job, a.full_name, b.store,
  d.technumber as tech_number, array_agg(distinct d.techkey) as tech_key
from fs.personnel a
join fs.personnel_jobs b on a.personnel_id = b.personnel_id
  and  b.thru_date > current_date
  and b.store = 'RY1'
join fs.jobs c on b.job = c.job
  and b.store = c.store
  and c.job in ('Main Shop Tech')
join ads.ext_dim_tech d on a.employee_number = d.employeenumber
  and d.techkey <> 100
where not a.is_deleted  
group by a.full_name, a.personnel_id, c.job,  b.store, a.employee_number, d.technumber;
create unique index on techs(personnel_id);
create unique index on techs(employee_number);
create unique index on techs(tech_number,store);
CREATE INDEX ON techs USING GIN(tech_key);
create index on techs(job);
  
drop table if exists ros_02_01 cascade;
create temp table ros_02_01 as
select a.storecode, a.ro, a.line, aa.opcode, d.paymenttype, e.servicetype, c.personnel_id, b.the_date, ((select full_name from fs.personnel where personnel_id = c.personnel_id)), 
  coalesce(sum(case when aa.opcode not like 'TRAIN%'  and aa.opcode not like 'SHOP%' then a.flaghours end), 0) as flag_hours,
  coalesce(sum(case when aa.opcode like 'SHOP%' then a.flaghours end), 0) as shop,
  coalesce(sum(case when aa.opcode like 'TRAIN%' then a.flaghours end), 0) as training,
  bb.the_date as open_date, b.the_date as flag_date, bbb.the_date as close_date
from ads.ext_fact_repair_order a
join techs c on a.techkey = any(c.tech_key)  
left join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
join dds.dim_date b on a.flagdatekey = b.date_key
  and b.the_date between '01/26/2020' and '02/01/2020'
join dds.dim_date bb on a.opendatekey = bb.date_key 
join dds.dim_date bbb on a.closedatekey = bbb.date_key 
join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey  
join ads.ext_dim_service_type e on a.servicetypekey = e.servicetypekey
group by a.ro, a.line, a.storecode, c.personnel_id, b.the_date, aa.opcode, d.paymenttype, e.servicetype,b.the_date, bb.the_date, bbb.the_date;

drop table if exists ros_01_25 cascade;
create temp table ros_01_25 as
select a.storecode, a.ro, a.line, aa.opcode, d.paymenttype, e.servicetype, c.personnel_id, b.the_date, ((select full_name from fs.personnel where personnel_id = c.personnel_id)), 
  coalesce(sum(case when aa.opcode not like 'TRAIN%'  and aa.opcode not like 'SHOP%' then a.flaghours end), 0) as flag_hours,
  coalesce(sum(case when aa.opcode like 'SHOP%' then a.flaghours end), 0) as shop,
  coalesce(sum(case when aa.opcode like 'TRAIN%' then a.flaghours end), 0) as training,
  bb.the_date as open_date, b.the_date as flag_date, bbb.the_date as close_date
from ads.ext_fact_repair_order a
join techs c on a.techkey = any(c.tech_key)  
left join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
join dds.dim_date b on a.flagdatekey = b.date_key
  and b.the_date between '01/19/2020' and '01/25/2020'
join dds.dim_date bb on a.opendatekey = bb.date_key 
join dds.dim_date bbb on a.closedatekey = bbb.date_key   
join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey  
join ads.ext_dim_service_type e on a.servicetypekey = e.servicetypekey
group by a.ro, a.line, a.storecode, c.personnel_id, b.the_date, aa.opcode, d.paymenttype, e.servicetype,b.the_date, bb.the_date, bbb.the_date;

select sum(flag_hours) as flag_hours from ros_01_25 -- 1163.2
select servicetype, sum(flag_hours) as flag_hours from ros_01_25 group by servicetype
select paymenttype, sum(flag_hours) as flag_hours from ros_01_25 group by paymenttype
select opcode, sum(flag_hours) as flag_hours from ros_01_25 group by opcode order by flag_hours desc
select count(distinct ro) from ros_01_25  -- 529
select sum(flag_hours)/count(distinct ro) from ros_01_25  -- 2.199

select sum(flag_hours) as flag_hours from ros_02_01 -- 1467.3
select servicetype, sum(flag_hours) as flag_hours from ros_02_01 group by servicetype
select paymenttype, sum(flag_hours) as flag_hours from ros_02_01 group by paymenttype
select opcode, sum(flag_hours) as flag_hours from ros_02_01 group by opcode order by flag_hours desc
select count(distinct ro) from ros_02_01  -- 542
select sum(flag_hours)/count(distinct ro) from ros_02_01 -- 2.707

select count(*) from ros_02_01 where close_date < current_date; --1253



select close_date, count(*) from ros_02_01 group by close_date order by close_date;
2020-01-27;166
2020-01-28;195
2020-01-29;212
2020-01-30;278
2020-01-31;305
2020-02-01;52


select close_date, count(*) from ros_01_25 group by close_date order by close_date;
2020-01-20;126
2020-01-21;218
2020-01-22;197
2020-01-23;322
2020-01-24;233
2020-01-25;85

select close_date, paymenttype, count(*) from ros_02_01 group by close_date, paymenttype order by paymenttype, close_date;

select close_date, paymenttype, count(*) from ros_01_25 group by close_date, paymenttype order by paymenttype, close_date;


