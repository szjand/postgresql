﻿-- -----------------------------------------------
-- --< writers
-- -----------------------------------------------
--   select personnel_id, the_date, count(*) as ros
--   from (
--     select c.personnel_id, b.the_date, a.ro
--     from ads.ext_fact_repair_order a
--     join dds.dim_date b on a.opendatekey = b.date_key
--       and b.the_date between current_date - 7 and current_date
--     join (
--       select a.personnel_id, b.servicewriterkey
--       from fs.personnel a
--       left join ads.ext_dim_service_writer b on a.employee_number = b.employeenumber
--         and a.store_code = b.storecode
--         and b.active
--         and b.currentrow
--       where a.personnel_type = 'Service Advisor'
--         and not a.is_deleted) c on a.servicewriterkey = c.servicewriterkey
--     group by c.personnel_id, b.the_date, a.ro) d     
--   group by personnel_id, the_date;   
-- 
-- -- full shop writers
-- select b.store, a.full_name, a.personnel_id, a.employee_number
-- from fs.personnel a
-- join fs.personnel_jobs b on a.personnel_id = b.personnel_id
--   and  b.thru_date > current_date
-- join fs.jobs c on b.job = c.job
--   and b.store = c.store
--   and c.job = 'Service Advisor'
-- order by b.store, a.full_name  
-- 
-- select * from fs.personnel_jobs order by thru_date  
-- select * from fs.personnel where personnel_id = 2
-- 
-- select * 
-- from ads.ext_dim_service_Writer a
-- where a.active
--   and a.currentrow  
--   
-- -- writer ros opened for a date range 
-- select employeenumber, count(*) as ros
-- from (
--   select a.ro, c.name, c.employeenumber, c.storecode, b.the_date
--   from ads.ext_fact_repair_order a
--   join dds.dim_date b on a.opendatekey = b.date_key  
--     and b.the_date between '01/05/2020' and '01/11/2020'
--   join ads.ext_dim_Service_writer c on a.servicewriterkey = c.servicewriterkey
--     and c.active 
--     and c.currentrow  
--   group by a.ro, c.name, c.employeenumber, c.storecode, b.the_date) x
-- group by employeenumber
-- 
-- 
-- 
-- select aa.personnel_id, bb.the_date, bb.ros
-- from ( -- full shop writers
--   select a.personnel_id, a.employee_number
--   from fs.personnel a
--   join fs.personnel_jobs b on a.personnel_id = b.personnel_id
--     and  b.thru_date > current_date
--   join fs.jobs c on b.job = c.job
--     and b.store = c.store
--     and c.job = 'Service Advisor'
--   where not a.is_deleted) aa
-- join (-- writer ros opened per day for a date range 
--   select employeenumber, the_date, count(*) as ros
--   from (
--     select a.ro, c.name, c.employeenumber, c.storecode, b.the_date
--     from ads.ext_fact_repair_order a
--     join dds.dim_date b on a.opendatekey = b.date_key  
--       and b.the_date between '01/05/2020' and '01/11/2020'
--     join ads.ext_dim_Service_writer c on a.servicewriterkey = c.servicewriterkey
--       and c.active 
--       and c.currentrow  
--     group by a.ro, c.name, c.employeenumber, c.storecode, b.the_date) x
--   group by employeenumber, the_date) bb on aa.employee_number = bb.employeenumber
-- -----------------------------------------------
-- --/> writers
-- -----------------------------------------------
-- 
-- select *
-- from fs.csr_appointments_by_day
-- where the_date = current_date - 1
-- 
-- select *
-- from fs.bdr_appointments_by_day
-- where the_date = current_date - 1
-- 
-- select *
-- from fs.advisor_ros_by_day
-- where the_date = current_date - 1
-- 
-- select *
-- from fs.clock_hours_by_day
-- where the_date = current_date - 1
-- 
-- 
-- -----------------------------------------------
-- --< techs
-- -----------------------------------------------
-- 
-- -- full shop techs
-- select a.personnel_id, a.employee_number, c.job, a.full_name, b.store
-- from fs.personnel a
-- join fs.personnel_jobs b on a.personnel_id = b.personnel_id
--   and  b.thru_date > current_date
-- join fs.jobs c on b.job = c.job
--   and b.store = c.store
--   and c.job in ('Metal Tech','Main Shop Tech')
-- where not a.is_deleted  
-- 
--   -- create a set of relevant tech information
--   drop table if exists techs cascade;
--   create temp table techs as
-- --   select a.full_name, a.personnel_id, a.personnel_type, a.store_code, a.employee_number, 
-- --     b.technumber as tech_number, array_agg(distinct b.techkey) as tech_key
-- --   from fs.personnel a
-- --   left join ads.ext_Dim_tech b on a.employee_number = b.employeenumber
-- --     and b.techkey <> 100 -- exclude ancient ryan lene instance
-- --   where not a.is_deleted
-- --     and a.personnel_type in ('Main Shop Tech', 'Metal Tech')
-- --   group by a.full_name, a.personnel_id, a.personnel_type, a.store_code, a.employee_number, b.technumber;
--   select a.personnel_id, a.employee_number, c.job, a.full_name, b.store,
--     d.technumber as tech_number, array_agg(distinct d.techkey) as tech_key
--   from fs.personnel a
--   join fs.personnel_jobs b on a.personnel_id = b.personnel_id
--     and  b.thru_date > current_date
--   join fs.jobs c on b.job = c.job
--     and b.store = c.store
--     and c.job in ('Metal Tech','Main Shop Tech')
--   join ads.ext_dim_tech d on a.employee_number = d.employeenumber
--     and d.techkey <> 100
--   where not a.is_deleted  
--   group by a.full_name, a.personnel_id, c.job,  b.store, a.employee_number, d.technumber;
--   create unique index on techs(personnel_id);
--   create unique index on techs(employee_number);
--   create unique index on techs(tech_number,store);
--   CREATE INDEX ON techs USING GIN(tech_key);
--   create index on techs(job);
-- 
-- 
--   assert (
--     (select count(*) from techs)
--     =
--     (
--       select count(*)
--       from fs.personnel a
--       join fs.personnel_jobs b on a.personnel_id = b.personnel_id
--         and  b.thru_date > current_date
--       join fs.jobs c on b.job = c.job
--         and b.store = c.store
--         and c.job in ('Metal Tech','Main Shop Tech')
--       where not a.is_deleted)), 'mismatch between techs and fs.personnel';
-- 
-- 
--   select aa.personnel_id, aa.the_date, coalesce(bb.flag_hours, 0) as flag_hours, coalesce(bb.shop, 0) as shop_time, 
--     coalesce(bb.training, 0) as training, coalesce(cc.adj, 0) as adjustments
--   from ( -- one row for each tech/day
--     select a.the_date, b.*
--     from dds.dim_date a, techs b
--     where a.the_date between _from_date and _thru_date) aa  
--   left join (
--     select a.storecode, c.personnel_id, b.the_date, ((select full_name from fs.personnel where personnel_id = c.personnel_id)), 
--       coalesce(sum(case when aa.opcode not like 'TRAIN%'  and aa.opcode not like 'SHOP%' then a.flaghours end), 0) as flag_hours,
--       coalesce(sum(case when aa.opcode like 'SHOP%' then a.flaghours end), 0) as shop,
--       coalesce(sum(case when aa.opcode like 'TRAIN%' then a.flaghours end), 0) as training
--     from ads.ext_fact_repair_order a
--     join techs c on a.techkey = any(c.tech_key)  
--     left join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
--     join dds.dim_date b on 
--       case
--         when c.job = 'Metal Tech' then a.closedatekey = b.date_key
--         else a.flagdatekey = b.date_key
--       end
--       and b.the_date between _from_date and _thru_date
--     group by a.storecode, c.personnel_id, b.the_date)  bb on aa.the_date = bb.the_date
--       and aa.personnel_id = bb.personnel_id
--   left join ( -- one row for each tech/day for which there are hours
--     select company_number, technician_id, arkona.db2_integer_to_date_long(trans_date) as trans_date, sum(labor_hours) as adj
--     from arkona.ext_sdpxtim
--     where arkona.db2_integer_to_date_long(trans_date) between _from_date and _thru_date
--     group by company_number, technician_id, trans_date) cc on aa.the_date = cc.trans_date
--       and aa.store = cc.company_number
--       and aa.tech_number = cc.technician_id;
-- 
-- 
-- select * from fs.tech_flag_hours_by_day where the_date = current_date - 1
-- 
--       
-- -----------------------------------------------
-- --/> techs
-- -----------------------------------------------

-----------------------------------------------
--/> ss_data
-----------------------------------------------

do $$
declare
  _this_week integer := (
    select week_id 
    from fs.weeks
    where '01/05/2020'::date between from_date and thru_date); 
  _from_date date := (
    select from_date
    from fs.weeks
    where week_id = _this_week);
  _thru_date date := (
    select 
      case
        when current_date - 1 < (select thru_date from fs.weeks where week_id = _this_week) then current_date - 1
        else thru_date
      end
    from fs.weeks
    where week_id = _this_week); 
  _store citext := 'RY1';
  _position citext := 'CSR';
begin
drop table if exists ss_data;
create temp table ss_data as
select 1 as seq, left(day_name,3) || ' ' || substring(b.the_date::text, 6,5)  as the_date, b.day_of_week, 
  a.full_name,  c.clock_hours as budgeted_clock_hours, coalesce(e.clock_hours, 0) as actual_clock_hours, coalesce(e.pto_hours, 0) as pto_hours,
  null::text,c.capacity as budgeted_appointments, 
  coalesce(d.appointments, 0) as appointments 
from (
  select b.store, a.full_name, a.personnel_id, a.employee_number
  from fs.personnel a
  join fs.personnel_jobs b on a.personnel_id = b.personnel_id
    and  b.thru_date > current_date
  join fs.jobs c on b.job = c.job
    and b.store = c.store
    and c.job = _position) a 
join dds.dim_date b on b.the_date between _from_date and _thru_date
left join fs.personnel_daily_capacities c on a.personnel_id = c.personnel_id
  and  b.the_date = c.the_date
left join fs.csr_appointments_by_day d on b.the_date = d.the_date
  and a.personnel_id = d.personnel_id
left join fs.clock_hours_by_day e on a.personnel_id = e.personnel_id
  and b.the_date = e.the_date
where a.store = _store

union

select 2 as seq, null, null::integer as day_of_week, a.full_name,  
  sum(c.clock_hours) as clock_hours, coalesce(sum(e.clock_hours), 0) as actual_clock_hours, coalesce(sum(e.pto_hours), 0) as pto_hours,
  null::text,
  sum(c.capacity) as budgeted_appointments, 
  coalesce(sum(d.appointments), 0) as appointments 
from (
  select b.store, a.full_name, a.personnel_id, a.employee_number
  from fs.personnel a
  join fs.personnel_jobs b on a.personnel_id = b.personnel_id
    and  b.thru_date > current_date
  join fs.jobs c on b.job = c.job
    and b.store = c.store
    and c.job = _position) a 
join dds.dim_date b on b.the_date between _from_date and _thru_date
left join fs.personnel_daily_capacities c on a.personnel_id = c.personnel_id
  and  b.the_date = c.the_date
left join fs.csr_appointments_by_day d on b.the_date = d.the_date
  and a.personnel_id = d.personnel_id
left join fs.clock_hours_by_day e on a.personnel_id = e.personnel_id
  and b.the_date = e.the_date
where a.store = _store
group by a.full_name

union

select 3 as seq, left(day_name,3) || ' ' || substring(b.the_date::text, 6,5)  as the_date, b.day_of_week, null,
  sum(c.clock_hours) as clock_hours, coalesce(sum(e.clock_hours), 0) as actual_clock_hours, coalesce(sum(e.pto_hours), 0) as pto_hours,
  null::text,
  sum(c.capacity) as budgeted_appointments, 
  coalesce(sum(d.appointments), 0) as appointments 
from (
  select b.store, a.full_name, a.personnel_id, a.employee_number
  from fs.personnel a
  join fs.personnel_jobs b on a.personnel_id = b.personnel_id
    and  b.thru_date > current_date
  join fs.jobs c on b.job = c.job
    and b.store = c.store
    and c.job = _position) a 
join dds.dim_date b on b.the_date between _from_date and _thru_date
left join fs.personnel_daily_capacities c on a.personnel_id = c.personnel_id
  and  b.the_date = c.the_date
left join fs.csr_appointments_by_day d on b.the_date = d.the_date
  and a.personnel_id = d.personnel_id
left join fs.clock_hours_by_day e on a.personnel_id = e.personnel_id
  and b.the_date = e.the_date
where a.store = _store
group by left(day_name,3) || ' ' || substring(b.the_date::text, 6,5), b.day_of_week

union

select 4 as seq, null, null::integer as day_of_week, null,
  sum(c.clock_hours) as clock_hours, coalesce(sum(e.clock_hours), 0) as actual_clock_hours, coalesce(sum(e.pto_hours), 0) as pto_hours,
  null::text,
  sum(c.capacity) as budgeted_appointments, 
  coalesce(sum(d.appointments), 0) as appointments 
from (
  select b.store, a.full_name, a.personnel_id, a.employee_number
  from fs.personnel a
  join fs.personnel_jobs b on a.personnel_id = b.personnel_id
    and  b.thru_date > current_date
  join fs.jobs c on b.job = c.job
    and b.store = c.store
    and c.job = _position) a 
join dds.dim_date b on b.the_date between _from_date and _thru_date
left join fs.personnel_daily_capacities c on a.personnel_id = c.personnel_id
  and  b.the_date = c.the_date
left join fs.csr_appointments_by_day d on b.the_date = d.the_date
  and a.personnel_id = d.personnel_id
left join fs.clock_hours_by_day e on a.personnel_id = e.personnel_id
  and b.the_date = e.the_date
where a.store = _store
  
order by seq, full_name, day_of_week nulls last;

end $$;

select * from ss_data;
-----------------------------------------------
--/> ss_data
-----------------------------------------------

