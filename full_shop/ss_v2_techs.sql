﻿do $$
declare
  _this_week integer := (
    select week_id 
    from fs.weeks
    where '01/13/2020'::date between from_date and thru_date); 
  _from_date date := (
    select from_date
    from fs.weeks
    where week_id = _this_week);
  _thru_date date := (
    select 
      case
        when current_date - 1 < (select thru_date from fs.weeks where week_id = _this_week) then current_date - 1
        else thru_date
      end
    from fs.weeks
    where week_id = _this_week); 
  _store citext := 'RY1';
--   _position citext := 'main shop tech';
  _position citext := 'metal tech';
begin
drop table if exists ss_data;
create temp table ss_data as
select 1 as seq, left(day_name,3) || ' ' || substring(b.the_date::text, 6,5)  as the_date, b.day_of_week, 
  a.full_name,  c.clock_hours as budgeted_clock_hours, coalesce(e.clock_hours, 0) as actual_clock_hours, coalesce(e.pto_hours, 0) as pto_hours,
  null::text,c.capacity as budgeted_flag_hours, 
  coalesce(d.flag_hours, 0) + coalesce(d.adjustments, 0) as flag_hours,  
  coalesce(d.shop_time, 0) as shop_time, 
  coalesce(d.training, 0) as training
from (
  select b.store, a.full_name, a.personnel_id, a.employee_number
  from fs.personnel a
  join fs.personnel_jobs b on a.personnel_id = b.personnel_id
    and  b.thru_date > current_date
  join fs.jobs c on b.job = c.job
    and b.store = c.store
    and c.job = _position) a 
join dds.dim_date b on b.the_date between _from_date and _thru_date
left join fs.personnel_daily_capacities c on a.personnel_id = c.personnel_id
  and  b.the_date = c.the_date
left join fs.tech_flag_hours_by_day d on b.the_date = d.the_date
  and a.personnel_id = d.personnel_id
left join fs.clock_hours_by_day e on a.personnel_id = e.personnel_id
  and b.the_date = e.the_date
where a.store = _store

union
-- tech weekly total
select 2 as seq, null, null::integer as day_of_week, a.full_name,  
  sum(c.clock_hours) as clock_hours, coalesce(sum(e.clock_hours), 0) as actual_clock_hours, coalesce(sum(e.pto_hours), 0) as pto_hours,
  null::text, 
  sum(c.capacity) as capacity,
  coalesce(sum(d.flag_hours), 0)  + coalesce(sum(d.adjustments), 0)as flag_hours, 
  coalesce(sum(d.shop_time), 0) as shop_time, 
  coalesce(sum(d.training), 0) as training 
from (
  select b.store, a.full_name, a.personnel_id, a.employee_number
  from fs.personnel a
  join fs.personnel_jobs b on a.personnel_id = b.personnel_id
    and  b.thru_date > current_date
  join fs.jobs c on b.job = c.job
    and b.store = c.store
    and c.job = _position) a 
join dds.dim_date b on b.the_date between _from_date and _thru_date
left join fs.personnel_daily_capacities c on a.personnel_id = c.personnel_id
  and  b.the_date = c.the_date
left join fs.tech_flag_hours_by_day d on b.the_date = d.the_date
  and a.personnel_id = d.personnel_id
left join fs.clock_hours_by_day e on a.personnel_id = e.personnel_id
  and b.the_date = e.the_date
where a.store = _store
group by a.full_name  

union

-- department daily total
select 3 as seq, left(day_name,3) || ' ' || substring(b.the_date::text, 6,5)  as the_date, b.day_of_week, null, 
  sum(c.clock_hours) as clock_hours, coalesce(sum(e.clock_hours), 0) as actual_clock_hours, coalesce(sum(e.pto_hours), 0) as pto_hours,
  null::text, 
  sum(c.capacity) as capacity,
  coalesce(sum(d.flag_hours), 0)  + coalesce(sum(d.adjustments), 0)as flag_hours, 
  coalesce(sum(d.shop_time), 0) as shop_time, 
  coalesce(sum(d.training), 0) as training 
from (
  select b.store, a.full_name, a.personnel_id, a.employee_number
  from fs.personnel a
  join fs.personnel_jobs b on a.personnel_id = b.personnel_id
    and  b.thru_date > current_date
  join fs.jobs c on b.job = c.job
    and b.store = c.store
    and c.job = _position) a 
join dds.dim_date b on b.the_date between _from_date and _thru_date
left join fs.personnel_daily_capacities c on a.personnel_id = c.personnel_id
  and  b.the_date = c.the_date
left join fs.tech_flag_hours_by_day d on b.the_date = d.the_date
  and a.personnel_id = d.personnel_id
left join fs.clock_hours_by_day e on a.personnel_id = e.personnel_id
  and b.the_date = e.the_date
where a.store = _store
group by left(day_name,3) || ' ' || substring(b.the_date::text, 6,5), b.day_of_week

union

-- department weekly total
select 4 as seq, null, null::integer as day_of_week, null, 
  sum(c.clock_hours) as clock_hours, coalesce(sum(e.clock_hours), 0) as actual_clock_hours, coalesce(sum(e.pto_hours), 0) as pto_hours,
  null::text,
  sum(c.capacity) as capacity, 
  coalesce(sum(d.flag_hours), 0)  + coalesce(sum(d.adjustments), 0)as flag_hours, 
  coalesce(sum(d.shop_time), 0) as shop_time, 
  coalesce(sum(d.training), 0) as training  
from (
  select b.store, a.full_name, a.personnel_id, a.employee_number
  from fs.personnel a
  join fs.personnel_jobs b on a.personnel_id = b.personnel_id
    and  b.thru_date > current_date
  join fs.jobs c on b.job = c.job
    and b.store = c.store
    and c.job = _position) a 
join dds.dim_date b on b.the_date between _from_date and _thru_date
left join fs.personnel_daily_capacities c on a.personnel_id = c.personnel_id
  and  b.the_date = c.the_date
left join fs.tech_flag_hours_by_day d on b.the_date = d.the_date
  and a.personnel_id = d.personnel_id
left join fs.clock_hours_by_day e on a.personnel_id = e.personnel_id
  and b.the_date = e.the_date
where a.store = _store

order by seq, full_name, day_of_week nulls last;

end $$;

select * from ss_data;