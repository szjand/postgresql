﻿-- Table: fs.personnel_daily_details

-- DROP TABLE fs.personnel_daily_details;

CREATE TABLE fs.personnel_daily_details
(
  personnel_daily_capacities_id bigint NOT NULL,
  budgeted_clock_hours numeric(8,2) NOT NULL,
  pto_clock_hours numeric(8,2),
  capacity numeric(8,2) NOT NULL,
  shop_time numeric(8,2),
  training numeric(8,2),
  ember_id serial NOT NULL,
  CONSTRAINT personnel_daily_details_pk PRIMARY KEY (personnel_daily_capacities_id),
  CONSTRAINT personnel_daily_details_personnel_daily_capacities_id_fkey FOREIGN KEY (personnel_daily_capacities_id)
      REFERENCES fs.personnel_daily_capacities (personnel_daily_capacities_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE fs.personnel_daily_details
  OWNER TO rydell;

-- Table: fs.personnel_weekly_totals

-- DROP TABLE fs.personnel_weekly_totals;

CREATE TABLE fs.personnel_weekly_totals
(
  personnel_id bigint NOT NULL,
  week_id bigint NOT NULL,
  budgeted_clock_hours numeric(8,2) NOT NULL,
  pto_clock_hours numeric(8,2),
  capacity numeric(8,2) NOT NULL,
  shop_time numeric(8,2),
  training numeric(8,2),
  ember_id serial NOT NULL,
  CONSTRAINT personnel_weekly_totals_pk PRIMARY KEY (personnel_id, week_id),
  CONSTRAINT personnel_weekly_totals_personnel_id_fkey FOREIGN KEY (personnel_id)
      REFERENCES fs.personnel (personnel_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT personnel_weekly_totals_week_id_fkey FOREIGN KEY (week_id)
      REFERENCES fs.weeks (week_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE fs.personnel_weekly_totals
  OWNER TO rydell;


-- Table: fs.job_weekly_totals

-- DROP TABLE fs.job_weekly_totals;

CREATE TABLE fs.job_weekly_totals
(
  job citext NOT NULL,
  store citext NOT NULL,
  week_id bigint NOT NULL,
  budgeted_clock_hours numeric(8,2) NOT NULL,
  pto_clock_hours numeric(8,2),
  capacity numeric(8,2) NOT NULL,
  shop_time numeric(8,2),
  training numeric(8,2),
  ember_id serial NOT NULL,
  CONSTRAINT job_weekly_totals_pk PRIMARY KEY (job, store, week_id),
  CONSTRAINT job_weekly_totals_fk FOREIGN KEY (job, store)
      REFERENCES fs.jobs (store, job) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT job_weekly_totals_week_id_fkey FOREIGN KEY (week_id)
      REFERENCES fs.weeks (week_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE fs.job_weekly_totals
  OWNER TO rydell;


-- Table: fs.job_daily_totals

-- DROP TABLE fs.job_daily_totals;

CREATE TABLE fs.job_daily_totals
(
  job citext NOT NULL,
  store citext NOT NULL,
  the_date date NOT NULL,
  budgeted_clock_hours numeric(8,2) NOT NULL,
  pto_clock_hours numeric(8,2),
  capacity numeric(8,2) NOT NULL,
  shop_time numeric(8,2),
  training numeric(8,2),
  ember_id serial NOT NULL,
  CONSTRAINT job_daily_totals_pk PRIMARY KEY (job, store, the_date),
  CONSTRAINT job_daily_totals_fk FOREIGN KEY (job, store)
      REFERENCES fs.jobs (store, job) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE fs.job_daily_totals
  OWNER TO rydell;
