﻿select *
from fs.service_bdrs

-- iso_week: monday thru sunday, but it resets each year
-- so better of using sunday_to_saturday_week
select the_date, day_name, day_of_week, year_month, holiday, iso_Week, sunday_to_saturday_week
from dds.dim_date
where sunday_to_saturday_week between 
  (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 8 
  and
  (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) 
order by the_date

/*
-- iso_week
iso_week resets each year
some years have 52, some have 53 
select the_date, day_name, day_of_week, year_month, holiday, iso_Week, sunday_to_saturday_week
from dds.dim_date
where the_year between 2018 and 2019
order by the_date

select the_year, max(iso_week)
from dds.dim_date
group by the_year
order by the_year
*/

-- base calendar for clock_hours & days worked per week
-- 8 weeks thru last complete week
select sunday_to_saturday_week, min(the_date) as from_date, max(the_date) as thru_date
from dds.dim_date
where sunday_to_saturday_week between 
  (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 8 
  and
  (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) -1
  
group by sunday_to_saturday_week  
order by sunday_to_saturday_week

drop table if exists fs.weeks;
create table fs.weeks (
  week_id integer primary key,
  from_date date not null,
  thru_date date not null,
  from_mmdd citext not null,
  thru_mmdd citext not null,
  the_week citext not null);
create index on fs.weeks(from_date);  
create index on fs.weeks(thru_date); 
insert into fs.weeks
select sunday_to_saturday_week, from_date, thru_date,
  (select mmdd from dds.dim_date where the_date = from_date) as from_mmdd,
  (select mmdd from dds.dim_date where the_date = thru_date) as thru_mmdd,
  (select mmdd from dds.dim_date where the_date = from_date)
  || ' thru ' ||
  (select mmdd from dds.dim_date where the_date = thru_date) as the_week
from (
  select sunday_to_saturday_week, min(the_date) as from_date, max(the_date) as thru_date
  from dds.dim_date
  where the_year > 2017
  group by sunday_to_saturday_week) a

-- past 8 weeks thru last complete week
select *
from fs.weeks 
where week_id between 
    (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 8 
    and
    (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) -1 

-- clock hour data
-- bdr detailed clock hour data
select a.*, b.*, c.the_date, c.day_name, coalesce(d.clock_hours, 0)
from fs.service_bdrs a
join fs.weeks b on true
  and week_id between 
    (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 8 
    and
    (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) -1 
join dds.dim_date c on c.the_date between b.from_date and b.thru_date
left join arkona.xfm_pypclockin d on a.employee_number = d.employee_number
  and c.the_date = d.the_date
order by a.last_name, c.the_date  

-- writers detailed clock hour data
select a.*, b.*, c.the_date, c.day_name, coalesce(d.clock_hours, 0)
from fs.writers a
join fs.weeks b on true
  and week_id between 
    (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 8 
    and
    (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) -1 
join dds.dim_date c on c.the_date between b.from_date and b.thru_date
left join arkona.xfm_pypclockin d on a.employee_number = d.employee_number
  and c.the_date = d.the_date
order by a.last_name, c.the_date  

-- techs detailed clock hour data
select a.*, b.*, c.the_date, c.day_name, coalesce(d.clock_hours, 0)
from fs.techs a
join fs.weeks b on true
  and week_id between 
    (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 8 
    and
    (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) -1 
join dds.dim_date c on c.the_date between b.from_date and b.thru_date
left join arkona.xfm_pypclockin d on a.employee_number = d.employee_number
  and c.the_date = d.the_date
order by a.last_name, c.the_date  


for each week
need days/week
for each week, avg hours/day

------------------------------------------------------------------------------------
--< days worked each week, avg hours per day for each week for the previous 8 weeks thru last complete week
------------------------------------------------------------------------------------
-- bdr
drop table if exists bdr_clock;
create temp table bdr_clock as
select last_name, first_name, the_week, week_id,
  count(the_date) filter (where clock_hours > 0) as days_worked_week,
  case
    when count(the_date) filter (where clock_hours > 0) = 0 then 0
    else round(sum(clock_hours)/count(the_date) filter (where clock_hours > 0), 1)
  end as hours_day
from (
  select a.*, b.*, c.the_date, c.day_name, coalesce(d.clock_hours, 0) as clock_hours
  from fs.service_bdrs a
  join fs.weeks b on true
    and week_id between 
      (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 8 
      and
      (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) -1 
  join dds.dim_date c on c.the_date between b.from_date and b.thru_date
  left join arkona.xfm_pypclockin d on a.employee_number = d.employee_number
    and c.the_date = d.the_date
  order by a.last_name, c.the_date) aa  
group by last_name, first_name, the_week, week_id;


-- past 8 weeks thru last complete week pivoted (colpivot)
-- avg hours per day for each bdr
select 'bdr days per week', 
  array_to_string(
    array(
      select the_week    
      from fs.weeks 
      where week_id between 
          (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 8 
          and
          (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) -1 
      order by week_id), ',')
union      
select 'bdr hours per day', 
  array_to_string(
    array(
      select the_week    
      from fs.weeks 
      where week_id between 
          (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 8 
          and
          (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) -1 
      order by week_id), ',');       


drop table if exists _bdr_days_per_week;
select jon.colpivot('_bdr_days_per_week', 'select * from bdr_clock',
  array['last_name'], array['the_week'], '#.days_worked_week', 'max(week_id)');
select * from _bdr_days_per_week order by last_name;  

drop table if exists _bdr_hours;
select jon.colpivot('_bdr_hours', 'select * from bdr_clock',
  array['last_name'], array['the_week'], '#.hours_day', 'max(week_id)');
select * from _bdr_hours order by last_name;  



-- writers
select last_name, first_name, the_week,
  count(the_date) filter (where clock_hours > 0) as days_worked_week,
  case
    when count(the_date) filter (where clock_hours > 0) = 0 then 0
    else round(sum(clock_hours)/count(the_date) filter (where clock_hours > 0), 1)
  end as hours_day
from (
  select a.*, b.*, c.the_date, c.day_name, coalesce(d.clock_hours, 0) as clock_hours
  from fs.writers a
  join fs.weeks b on true
    and week_id between 
      (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 8 
      and
      (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) -1 
  join dds.dim_date c on c.the_date between b.from_date and b.thru_date
  left join arkona.xfm_pypclockin d on a.employee_number = d.employee_number
    and c.the_date = d.the_date
  order by a.last_name, c.the_date) aa  
group by last_name, first_name, the_week;  


-- techs
select last_name, first_name, the_week,
  count(the_date) filter (where clock_hours > 0) as days_worked_week,
  case
    when count(the_date) filter (where clock_hours > 0) = 0 then 0
    else round(sum(clock_hours)/count(the_date) filter (where clock_hours > 0), 1)
  end as hours_day
from (
  select a.*, b.*, c.the_date, c.day_name, coalesce(d.clock_hours, 0) clock_hours
  from fs.techs a
  join fs.weeks b on true
    and week_id between 
      (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 8 
      and
      (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) -1 
  join dds.dim_date c on c.the_date between b.from_date and b.thru_date
  left join arkona.xfm_pypclockin d on a.employee_number = d.employee_number
    and c.the_date = d.the_date
  order by a.last_name, c.the_date) aa  
group by last_name, first_name, the_week;   

------------------------------------------------------------------------------------
--/> days worked each week, avg hours per day for each week for the previous 8 weeks thru last complete week
------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------
--< pto
--------------------------------------------------------------------------------------------
on any given date, need to look at current and future (how many) weeks and expose approved pto

select *
from ads.ext_pto_requests

should this show current wekk if so, pto request needs to start at beginning of current week

select *
from (
  select 'bdr' as source, first_name, last_name, employee_number
  from fs.service_bdrs
  union
  select 'writer', first_name, last_name, employee_number
  from fs.writers
  union
  select 'tech', first_name, last_name, employee_number
  from fs.techs) a
left join ads.ext_pto_requests b on a.employee_number = b.employee_number
  and b.the_date > current_date
order by source, last_name
  

is this where i get into projecting into the future, if so based on some established standards
or from history
?

-- the current week
select *
from fs.weeks
where current_date between from_date and thru_date

-- next 4 weeks
select *
from fs.weeks
where week_id between
  (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) + 1 
  and
  (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) + 4 
order by week_id

-- bdr pto data for the next 4 weeks
select a.last_name, a.first_name, a.employee_number, b.the_week, c.the_date, c.day_name, coalesce(d.hours, 0)
from fs.service_bdrs a
join fs.weeks b on true
  and week_id between 
    (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) + 1 
    and
    (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) + 4 
join dds.dim_date c on c.the_date between b.from_date and b.thru_date
left join ads.ext_pto_requests d on a.employee_number = d.employee_number
  and c.the_date = d.the_date
order by a.last_name, c.the_date  

select *
from fs.service_bdrs a
join ads.ext_pto_requests b on a.employee_number = b.employee_number
  and b.the_date > current_date

--------------------------------------------------------------------------------------------
--/> pto
--------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------
--< holidays
--------------------------------------------------------------------------------------------
-- in the next 4 weeks
select a.week_id, a.from_date, a.thru_date, a.from_mmdd, a.thru_mmdd, a.the_week,
  8 * count(b.the_date) filter (where b.holiday) as holiday_hours
from fs.weeks a
left join dds.dim_date b on b.the_date between a.from_date and a.thru_date
where a.week_id between
  (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) + 1 
  and
  (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) + 4 
group by a.week_id, a.from_date, a.thru_date, a.from_mmdd, a.thru_mmdd, a.the_week
order by a.week_id

-- in the previous 8 weeks
select a.week_id, a.from_date, a.thru_date, a.from_mmdd, a.thru_mmdd, a.the_week,
  8 * count(b.the_date) filter (where b.holiday) as holiday_hours
from fs.weeks a
left join dds.dim_date b on b.the_date between a.from_date and a.thru_date
where a.week_id between
  (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 8
  and
  (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 1
group by a.week_id, a.from_date, a.thru_date, a.from_mmdd, a.thru_mmdd, a.the_week
order by a.week_id
--------------------------------------------------------------------------------------------
--/> holidays
--------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------
--< flag hours
-- ben: same as payroll: ry1 & ry2 main shop: flag date, body shop: close date
--------------------------------------------------------------------------------------------
/**/
-- techs last 8 weeks
select last_name, first_name, the_week,
  count(the_date) filter (where clock_hours > 0) as days_worked_week,
  case
    when count(the_date) filter (where clock_hours > 0) = 0 then 0
    else round(sum(clock_hours)/count(the_date) filter (where clock_hours > 0), 1)
  end as hours_day
from (
  select a.*, b.*, c.the_date, c.day_name, coalesce(d.clock_hours, 0) clock_hours
  from fs.techs a
  join fs.weeks b on true
    and week_id between 
      (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 8 
      and
      (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) -1 
  join dds.dim_date c on c.the_date between b.from_date and b.thru_date
  left join arkona.xfm_pypclockin d on a.employee_number = d.employee_number
    and c.the_date = d.the_date
  order by a.last_name, c.the_date) aa  
group by last_name, first_name, the_week;   
*/

select a.*, b.techkey
from fs.techs a
join ads.ext_dim_tech b on a.employee_number = b.employeenumber

-- flagdate
select c.the_date, b.last_name, b.first_name, b.tech_id, a.flaghours
from ads.ext_fact_repair_order a
join (
  select a.*, b.techkey
  from fs.techs a
  join ads.ext_dim_tech b on a.employee_number = b.employeenumber) b on a.techkey = b.techkey
join dds.dim_date c on a.flagdatekey = c.date_key
  and c.year_month = 201911


-- flag hours only for the days actually worked
-- too fussy, if the date has flag hours it is a valid date i think
-- shit hard to say
-- lets get the flag hour data and see what it looks like

/*
-- techs
select last_name, first_name, the_week,
  count(the_date) filter (where clock_hours > 0) as days_worked_week,
  case
    when count(the_date) filter (where clock_hours > 0) = 0 then 0
    else round(sum(clock_hours)/count(the_date) filter (where clock_hours > 0), 1)
  end as hours_day
from (
  select a.*, b.*, c.the_date, c.day_name, coalesce(d.clock_hours, 0) clock_hours
  from fs.techs a
  join fs.weeks b on true
    and week_id between 
      (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 8 
      and
      (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) -1 
  join dds.dim_date c on c.the_date between b.from_date and b.thru_date
  left join arkona.xfm_pypclockin d on a.employee_number = d.employee_number
    and c.the_date = d.the_date
  order by a.last_name, c.the_date) aa  
group by last_name, first_name, the_week;     
*/

select c.the_date, b.last_name, b.first_name, b.tech_id, a.flaghours
from ads.ext_fact_repair_order a
join (
  select a.*, b.techkey
  from fs.techs a
  join ads.ext_dim_tech b on a.employee_number = b.employeenumber) b on a.techkey = b.techkey
join dds.dim_date c on a.flagdatekey = c.date_key
  and c.the_date between
    (
      select min(from_date)
      from fs.weeks
      where week_id between
        (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 8
        and
        (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 1)
  and
    (  
      select max(thru_date)
      from fs.weeks
      where week_id between
        (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 8
        and
        (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 1)

--------------------------------------------------------------------------------------------
--/> flag hours
--------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------
--< writers ros opened
--------------------------------------------------------------------------------------------
drop table if exists daily_ros;
create temp table daily_ros as
select the_date, last_name, first_name, writer_id, count(*) as ros
from (
  select c.the_date, b.last_name, b.first_name, b.writer_id, ro
  from ads.ext_fact_repair_order a
  join (
    select *
    from fs.writers a
    join ads.ext_dim_service_writer b on a.employee_number = b.employeenumber) b on a.servicewriterkey = b.servicewriterkey
  join dds.dim_date c on a.opendatekey = c.date_key
    and c.the_date between
      (
        select min(from_date)
        from fs.weeks
        where week_id between
          (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 8
          and
          (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 1)
    and
      (  
        select max(thru_date)
        from fs.weeks
        where week_id between
          (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 8
          and
          (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 1)
  group by c.the_date, b.last_name, b.first_name, b.writer_id, ro) aa
group by the_date, last_name, first_name, writer_id
order by writer_id, the_date;

-- ros_per_day
select a.last_name, a.first_name, b.day_name, (sum(ros)/count(*))::integer as ros_per_day
from daily_ros a
join dds.dim_date b on a.the_date = b.the_date
group by a.last_name, a.first_name, b.day_of_week, b.day_name
order by a.last_name

-- avg_ros_per_day
select a.last_name, a.first_name, (sum(ros)/count(*))::integer as ros_per_day
from daily_ros a
join dds.dim_date b on a.the_date = b.the_date
group by a.last_name, a.first_name
--------------------------------------------------------------------------------------------
--/> writers ros opened
--------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------
--< writers flag hours per ro (final closed ros)
--------------------------------------------------------------------------------------------


select * 
from ads.ext_fact_repair_order
limit 10

drop table if exists ro_flag_hours;
create temp table ro_flag_hours as
select c.the_date, b.last_name, b.first_name, b.writer_id, a.ro, a.roflaghours
from ads.ext_fact_repair_order a
join (
  select *
  from fs.writers a
  join ads.ext_dim_service_writer b on a.employee_number = b.employeenumber) b on a.servicewriterkey = b.servicewriterkey
join dds.dim_date c on a.finalclosedatekey = c.date_key
  and c.the_date between
    (
      select min(from_date)
      from fs.weeks
      where week_id between
        (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 8
        and
        (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 1)
  and
    (  
      select max(thru_date)
      from fs.weeks
      where week_id between
        (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 8
        and
        (select sunday_to_saturday_week from dds.dim_date where the_date = current_date) - 1)
group by c.the_date, b.last_name, b.first_name, b.writer_id, a.ro, a.roflaghours
having roflaghours <> 0;
create unique index on ro_Flag_hours(ro);

-- avg hours per ro for all writers
-- 2.51
select sum(roflaghours), count(*), round(sum(roflaghours)/count(*), 2)
from ro_flag_hours


-- avg hours per ro by writer
select last_name, first_name, writer_id, sum(roflaghours), count(*), round(sum(roflaghours)/count(*), 2)
from ro_flag_hours
group by last_name, first_name, writer_id




--------------------------------------------------------------------------------------------
--/> writers flag hours per ro
--------------------------------------------------------------------------------------------

12/20/19
well it has been implemented and the managers have done their admin duties
i need to change all this to use aftons data model

initially, i think , just the detail data
for each personnel type
for each day
  clock hours
  metric (flag hours, ros opened, appts made)


select *
from fs.personnel
where not is_deleted


select b.store_code, a.department, b.personnel_id, a.personnel_type, b.full_name, b.employee_number,
  c.employee_first_name, c.employee_last_name
from fs.personnel_types a
join fs.personnel b on a.personnel_type = b.personnel_type
  and not b.is_deleted
join arkona.xfm_pymast c on b.employee_number = c.pymast_employee_number
  and c.current_row
order by a.personnel_type, b.store_code,  b.full_name

-- fucking shit will have to be manually managed
select a.*, b.employee_first_name, b.employee_last_name, c.personnel_type
from ads.ext_ron_users a
left join arkona.xfm_pymast b on a.userfirstname = b.employee_first_name
  and a.userlastname = b.employee_last_name
  and b.current_row
left join fs.personnel c on b.pymast_employee_number = c.employee_number  



-- csr body shop users/
select a.createdby, b.pymast_employee_number
from ads.ext_bs_appointment_history a
left join arkona.xfm_pymast b on trim(upper(concat(b.employee_first_name,' ',b.employee_last_name))) = trim(upper(a.createdby))
  and b.current_row
where createdts::date > '12/04/2019'

select concat(b.employee_first_name,' ',b.employee_last_name)
from arkona.xfm_pymast b
limit 10

