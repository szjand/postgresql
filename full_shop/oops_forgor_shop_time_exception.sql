﻿12/31/19
conversation with ben revealed that he wants to see shop time & training time broken out, separated from flag hours

shit also forgot adjustments

select sum(flag_hours)
from (
    select c.personnel_id, b.the_date, sum(flaghours) as flag_hours,((select full_name from fs.personnel where personnel_id = c.personnel_id)),
      array_agg(distinct aa.opcode order by aa.opcode)
    from ads.ext_fact_repair_order a
    join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
--       and (aa.opcode like '%SHOP%' or aa.opcode like '%TRAIN%')
    join dds.dim_date b on a.flagdatekey = b.date_key
      and b.the_date between '12/16/2019' and '12/31/2019' -- _from_date and _thru_date
    join (
      select a.personnel_id, b.techkey, b.flagdeptcode
      from fs.personnel a
      left join ads.ext_dim_tech b on a.employee_number = b.employeenumber
        and a.store_code = b.storecode
        and b.active
        and b.currentrow
      where a.personnel_type  = 'Main Shop Tech' and store_code = 'RY1'
        and not a.is_deleted) c on a.techkey = c.techkey
    group by c.personnel_id, b.the_date) x
order by full_name, the_date    

select * from ads.ext_dim_opcode where opcode like '%TRAIN%' or opcode like 'shop%'   

select min(the_date), max(the_date) from fs.tech_Flag_hours_by_day

-- this looks good, the breakout matches the non broken out
select *
from (
select c.personnel_id, b.the_date, ((select full_name from fs.personnel where personnel_id = c.personnel_id)), sum(flaghours) as flag_hours
from ads.ext_fact_repair_order a
join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
join dds.dim_date b on a.flagdatekey = b.date_key
  and b.the_date between '12/16/2019' and '12/31/2019' -- _from_date and _thru_date
join (
  select a.personnel_id, b.techkey, b.flagdeptcode
  from fs.personnel a
  left join ads.ext_dim_tech b on a.employee_number = b.employeenumber
    and a.store_code = b.storecode
    and b.active
    and b.currentrow
  where a.personnel_type  = 'Main Shop Tech' and store_code = 'RY2'
    and not a.is_deleted) c on a.techkey = c.techkey
group by c.personnel_id, b.the_date) x
left join (
select c.personnel_id, b.the_date, ((select full_name from fs.personnel where personnel_id = c.personnel_id)), 
  coalesce(sum(case when aa.opcode like 'SHOP%' then a.flaghours end), 0) as shop,
  coalesce(sum(case when aa.opcode like 'TRAIN%' then a.flaghours end), 0) as training,
  coalesce(sum(case when aa.opcode not like 'TRAIN%'  and aa.opcode not like 'SHOP%' then a.flaghours end), 0) as flag_hours
--   sum(flaghours) filter (where aa.opcode not like 'SHOP%' and aa.opcode not like '%TRAIN%')
from ads.ext_fact_repair_order a
left join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
join dds.dim_date b on a.flagdatekey = b.date_key
  and b.the_date between '12/16/2019' and '12/31/2019' -- _from_date and _thru_date
join (
  select a.personnel_id, b.techkey, b.flagdeptcode
  from fs.personnel a
  left join ads.ext_dim_tech b on a.employee_number = b.employeenumber
    and a.store_code = b.storecode
    and b.active
    and b.currentrow
  where a.personnel_type  = 'Main Shop Tech' and store_code = 'RY2'
    and not a.is_deleted) c on a.techkey = c.techkey
group by c.personnel_id, b.the_date) y on x.personnel_id = y.personnel_id and x.the_date = y.the_date


-- main shop
select *
from (
select c.personnel_id, b.the_date, ((select full_name from fs.personnel where personnel_id = c.personnel_id)), sum(flaghours) as flag_hours
from ads.ext_fact_repair_order a
join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
join dds.dim_date b on a.flagdatekey = b.date_key
  and b.the_date between '12/16/2019' and '12/31/2019' -- _from_date and _thru_date
join (
  select a.personnel_id, b.techkey, b.flagdeptcode
  from fs.personnel a
  left join ads.ext_dim_tech b on a.employee_number = b.employeenumber
    and a.store_code = b.storecode
    and b.active
    and b.currentrow
  where a.personnel_type  = 'Main Shop Tech' and store_code = 'RY1'
    and not a.is_deleted) c on a.techkey = c.techkey
group by c.personnel_id, b.the_date) x
left join (
select c.personnel_id, b.the_date, ((select full_name from fs.personnel where personnel_id = c.personnel_id)), 
  sum(case when aa.opcode like 'SHOP%' then a.flaghours end) as shop,
  sum(case when aa.opcode like 'TRAIN%' then a.flaghours end) as training,
  sum(case when aa.opcode not like 'TRAIN%'  and aa.opcode not like 'SHOP%' then a.flaghours end) as flag_hours
--   sum(flaghours) filter (where aa.opcode not like 'SHOP%' and aa.opcode not like '%TRAIN%')
from ads.ext_fact_repair_order a
left join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
join dds.dim_date b on a.flagdatekey = b.date_key
  and b.the_date between '12/16/2019' and '12/31/2019' -- _from_date and _thru_date
join (
  select a.personnel_id, b.techkey, b.flagdeptcode
  from fs.personnel a
  left join ads.ext_dim_tech b on a.employee_number = b.employeenumber
    and a.store_code = b.storecode
    and b.active
    and b.currentrow
  where a.personnel_type  = 'Main Shop Tech' and store_code = 'RY1'
    and not a.is_deleted) c on a.techkey = c.techkey
group by c.personnel_id, b.the_date


-- body shop  -- looks good
select *
from (
    select c.personnel_id, b.the_date, sum(flaghours) as flag_hours--,((select full_name from fs.personnel where personnel_id = c.personnel_id))
    from ads.ext_fact_repair_order a
    join dds.dim_date b on a.closedatekey = b.date_key
      and b.the_date between '12/16/2019' and '12/31/2019' -- current_date - 7 and current_date - 1 -- _from_date and _thru_date
    join (
      select a.personnel_id, b.techkey, b.flagdeptcode
      from fs.personnel a
      left join ads.ext_dim_tech b on a.employee_number = b.employeenumber
        and a.store_code = b.storecode
        and b.active
        and b.currentrow
      where a.personnel_type = 'Metal Tech'
        and not a.is_deleted) c on a.techkey = c.techkey
    group by c.personnel_id, b.the_date) x
left join (    
select c.personnel_id, b.the_date, -- sum(flaghours) as flag_hours--,((select full_name from fs.personnel where personnel_id = c.personnel_id))
  sum(case when aa.opcode like 'SHOP%' then a.flaghours end) as shop,
  sum(case when aa.opcode like 'TRAIN%' then a.flaghours end) as training,
  sum(case when aa.opcode not like 'TRAIN%'  and aa.opcode not like 'SHOP%' then a.flaghours end) as flag_hours
from ads.ext_fact_repair_order a
left join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
join dds.dim_date b on a.closedatekey = b.date_key
  and b.the_date between '12/16/2019' and '12/31/2019' -- current_date - 7 and current_date - 1 -- _from_date and _thru_date
join (
  select a.personnel_id, b.techkey, b.flagdeptcode
  from fs.personnel a
  left join ads.ext_dim_tech b on a.employee_number = b.employeenumber
    and a.store_code = b.storecode
    and b.active
    and b.currentrow
  where a.personnel_type = 'Metal Tech'
    and not a.is_deleted) c on a.techkey = c.techkey
group by c.personnel_id, b.the_date) y on x.personnel_id = y.personnel_id and x.the_date = y.the_date


alter table fs.tech_flag_hours_by_day
add column shop_time numeric(8,2),
add column training numeric(8,2);

update fs.tech_flag_hours_by_day
set shop_time = 0, training = 0;

alter table fs.tech_flag_hours_by_day
alter column shop_time set not null,
alter column training set not null;


---------------------------------------------------------
-- adjustments
---------------------------------------------------------
this is what is used in tpdata
        select pttech, ptdate, round(SUM(ptlhrs), 2) AS adj
        FROM dds.stgArkonaSDPXTIM a       
        INNER JOIN tpTechs b on a.pttech = b.technumber

        
select technician_id, sum(labor_hours) as adj, arkona.db2_integer_to_date_long(trans_date) 
from arkona.ext_sdpxtim
where arkona.db2_integer_to_date_long(trans_date) between '12/16/2019' and '12/31/2019'
group by technician_id, arkona.db2_integer_to_date_long(trans_date) 
having sum(labor_hours) <> 0



select *
from (
select c.personnel_id, c.technumber, b.the_date, ((select full_name from fs.personnel where personnel_id = c.personnel_id)), 
  coalesce(sum(case when aa.opcode like 'SHOP%' then a.flaghours end), 0) as shop,
  coalesce(sum(case when aa.opcode like 'TRAIN%' then a.flaghours end), 0) as training,
  coalesce(sum(case when aa.opcode not like 'TRAIN%'  and aa.opcode not like 'SHOP%' then a.flaghours end), 0) as flag_hours
--   sum(flaghours) filter (where aa.opcode not like 'SHOP%' and aa.opcode not like '%TRAIN%')
from ads.ext_fact_repair_order a
left join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
join dds.dim_date b on a.closedatekey = b.date_key
  and b.the_date between '12/16/2019' and '12/31/2019' -- _from_date and _thru_date
join (
  select a.personnel_id, b.techkey, b.flagdeptcode, b.technumber
  from fs.personnel a
  left join ads.ext_dim_tech b on a.employee_number = b.employeenumber
    and a.store_code = b.storecode
    and b.active
    and b.currentrow
  where a.personnel_type  = 'main shop Tech' --and store_code = 'RY2'
    and not a.is_deleted) c on a.techkey = c.techkey
group by c.personnel_id, b.the_date, c.technumber) aa
left join (
  select technician_id, sum(labor_hours) as adj, arkona.db2_integer_to_date_long(trans_date) as the_date 
  from arkona.ext_sdpxtim
  where arkona.db2_integer_to_date_long(trans_date) between '12/16/2019' and '12/31/2019'
  group by technician_id, arkona.db2_integer_to_date_long(trans_date) 
  having sum(labor_hours) <> 0) bb on aa.the_date = bb.the_date
    and aa.technumber = bb.technician_id


-- fuck me #9234512354, 642 is used for a tech at each store, schuppert and prois
-- this might be the best case for techs

drop table if exists techs cascade;
create temp table techs as
select a.full_name, a.personnel_id, a.personnel_type, a.store_code, a.employee_number, 
  b.technumber as tech_number, array_agg(distinct b.techkey) as tech_key
from fs.personnel a
left join ads.ext_Dim_tech b on a.employee_number = b.employeenumber
  and b.techkey <> 100 -- exclude ancient ryan lene instance
where not a.is_deleted
  and a.personnel_type in ('Main Shop Tech', 'Metal Tech')
group by a.full_name, a.personnel_id, a.personnel_type, a.store_code, a.employee_number, b.technumber;
create unique index on techs(personnel_id);
create unique index on techs(employee_number);
create unique index on techs(tech_number,store_code);
CREATE INDEX ON techs USING GIN(tech_key);
create index on techs(personnel_type);

do
$$
begin
assert (
  (select count(*) from techs)
  =
  (select count(*)
  from fs.personnel
  where not is_deleted
    and personnel_type  in ('Main Shop Tech', 'Metal Tech'))), 'mismatch between techs and fs.personnel';
end
$$;  



-- ok, this much looks good, now to add adjustments
select c.personnel_id, b.the_date, ((select full_name from fs.personnel where personnel_id = c.personnel_id)), 
  coalesce(sum(case when aa.opcode not like 'TRAIN%'  and aa.opcode not like 'SHOP%' then a.flaghours end), 0) as flag_hours,
  coalesce(sum(case when aa.opcode like 'SHOP%' then a.flaghours end), 0) as shop,
  coalesce(sum(case when aa.opcode like 'TRAIN%' then a.flaghours end), 0) as training
from ads.ext_fact_repair_order a
left join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
join dds.dim_date b on a.flagdatekey = b.date_key
  and b.the_date between '12/16/2019' and '12/31/2019' -- _from_date and _thru_date
join techs c on a.techkey = any(c.tech_key)
  and c.personnel_type  = 'main shop Tech'
  and c.store_code = 'RY1'
group by c.personnel_id, b.the_date

select * from arkona.ext_sdpxtim a limit 100

select * from arkona.ext_sdpxtim a where technician_id = '642'

it doesnt work to add it to the main query at the same level
that query is limited to flag dates of the ros not the full calendar dates in the range
worse, prior to grouping ( at the joining level) there are many instances of each day
same thing if i take of the join to the date, many instances of the tech
it has to be outside of the main query where there is one row per tech/day

select a.technician_id, sum(a.labor_hours) as adj, arkona.db2_integer_to_date_long(a.trans_date) 
from arkona.ext_sdpxtim a
join techs b on a.technician_id = b.tech_number
   and a.company_number = b.store_code
where arkona.db2_integer_to_date_long(trans_date) between '12/16/2019' and '12/31/2019'
group by a.company_number, a.technician_id, arkona.db2_integer_to_date_long(a.trans_date) 
having sum(labor_hours) <> 0


select c.personnel_id, b.the_date, ((select full_name from fs.personnel where personnel_id = c.personnel_id)), 
  coalesce(sum(case when aa.opcode not like 'TRAIN%'  and aa.opcode not like 'SHOP%' then a.flaghours end), 0) as flag_hours,
  coalesce(sum(case when aa.opcode like 'SHOP%' then a.flaghours end), 0) as shop,
  coalesce(sum(case when aa.opcode like 'TRAIN%' then a.flaghours end), 0) as training
from ads.ext_fact_repair_order a
left join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
join dds.dim_date b on a.flagdatekey = b.date_key
  and b.the_date between '12/16/2019' and '12/31/2019' -- _from_date and _thru_date
join techs c on a.techkey = any(c.tech_key)
  and c.personnel_type  = 'main shop Tech'
  and c.store_code = 'RY1'
group by c.personnel_id, b.the_date


-- this is starting to look ok
-- but, for each date there is a row with null store,personnel_id,tech_number, name with a large number of flag hours
starting to feel like premature grouping
select *
from (
  select d.store_code, d.personnel_id, d.tech_number, a.the_date, ((select full_name from fs.personnel where personnel_id = d.personnel_id)), 
    coalesce(sum(case when c.opcode not like 'TRAIN%'  and c.opcode not like 'SHOP%' then b.flaghours end), 0) as flag_hours,
    coalesce(sum(case when c.opcode like 'SHOP%' then b.flaghours end), 0) as shop,
    coalesce(sum(case when c.opcode like 'TRAIN%' then b.flaghours end), 0) as training
  -- select e.*
  from dds.dim_date a
  -- main shop based on flag date, body shop based on close date
  left join ads.ext_fact_repair_order b on a.date_key = b.flagdatekey
  left join ads.ext_dim_opcode c on b.opcodekey = c.opcodekey
  left join techs d on b.techkey = any(d.tech_key)
    and d.personnel_type  = 'main shop Tech'
    and d.store_code = 'RY1'
  where a.the_date between '12/16/2019' and '12/31/2019' -- _from_date and _thru_date
  group by d.store_code, d.personnel_id, a.the_date, d.tech_number) aa
left join (

select company_number, technician_id, arkona.db2_integer_to_date_long(trans_date) as trans_date, sum(labor_hours)
from arkona.ext_sdpxtim
where arkona.db2_integer_to_date_long(trans_date) between '12/16/2019' and '12/31/2019'
group by company_number, technician_id, trans_date) bb on aa.store_code = bb.company_number
  and aa.tech_number = bb.technician_id 
  and aa.the_date = bb.trans_date

select company_number, technician_id, arkona.db2_integer_to_date_long(trans_date) as trans_date, labor_hours
from arkona.ext_sdpxtim
where arkona.db2_integer_to_date_long(trans_date) between '12/16/2019' and '12/31/2019'
  and technician_id = '647'


-- try to sort out the premature grouping hypothesis
select d.store_code, d.personnel_id, d.tech_number, a.the_date, ((select full_name from fs.personnel where personnel_id = d.personnel_id)), 
  coalesce(case when c.opcode not like 'TRAIN%'  and c.opcode not like 'SHOP%' then b.flaghours end, 0) as flag_hours,
  coalesce(case when c.opcode like 'SHOP%' then b.flaghours end, 0) as shop,
  coalesce(case when c.opcode like 'TRAIN%' then b.flaghours end, 0) as training
-- select e.*
from dds.dim_date a
-- main shop based on flag date, body shop based on close date
left join ads.ext_fact_repair_order b on a.date_key = b.flagdatekey
left join ads.ext_dim_opcode c on b.opcodekey = c.opcodekey
left join techs d on b.techkey = any(d.tech_key)
  and d.personnel_type  = 'main shop Tech'
  and d.store_code = 'RY1'
where a.the_date = '12/21/2019' --between '12/16/2019' and '12/31/2019' -- _from_date and _thru_date


-- back to this version
-- for flag hours (of all types) this works, need to sort out how to handle
-- adjustments, say, there are adjustments on a date for a tech that is not in this list
select c.personnel_id, b.the_date, ((select full_name from fs.personnel where personnel_id = c.personnel_id)), 
  coalesce(sum(case when aa.opcode not like 'TRAIN%'  and aa.opcode not like 'SHOP%' then a.flaghours end), 0) as flag_hours,
  coalesce(sum(case when aa.opcode like 'SHOP%' then a.flaghours end), 0) as shop,
  coalesce(sum(case when aa.opcode like 'TRAIN%' then a.flaghours end), 0) as training
from ads.ext_fact_repair_order a
left join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
join dds.dim_date b on a.flagdatekey = b.date_key
  and b.the_date between '12/16/2019' and '12/31/2019' -- _from_date and _thru_date
join techs c on a.techkey = any(c.tech_key)
  and c.personnel_type  = 'main shop Tech'
  and c.store_code = 'RY1'
group by c.personnel_id, b.the_date


damnit, this is looking ok
but what if there is an adjustment for a date for which there is no ro
that negates being able to join on the store
which means that the stores data needs to be generated separately
which enables me to join adjustments solely on the date
but wait what about tech
which means i need a cartesian base query, 1 row for each day/tech
so start over at -- * --
select aa.the_date, bb.*
from dds.dim_date aa
left join (
  select a.storecode, c.personnel_id, b.the_date, ((select full_name from fs.personnel where personnel_id = c.personnel_id)), 
    coalesce(sum(case when aa.opcode not like 'TRAIN%'  and aa.opcode not like 'SHOP%' then a.flaghours end), 0) as flag_hours,
    coalesce(sum(case when aa.opcode like 'SHOP%' then a.flaghours end), 0) as shop,
    coalesce(sum(case when aa.opcode like 'TRAIN%' then a.flaghours end), 0) as training
  from ads.ext_fact_repair_order a
  left join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
  join dds.dim_date b on a.flagdatekey = b.date_key
    and b.the_date between '12/16/2019' and '12/31/2019' -- _from_date and _thru_date
  join techs c on a.techkey = any(c.tech_key)
    and c.personnel_type  = 'main shop Tech'
    and c.store_code = 'RY1'
  group by a.storecode, c.personnel_id, b.the_date) bb on aa.the_date = bb.the_date
left join (
  select company_number, technician_id, arkona.db2_integer_to_date_long(trans_date) as trans_date, sum(labor_hours)
  from arkona.ext_sdpxtim
  where arkona.db2_integer_to_date_long(trans_date) between '12/16/2019' and '12/31/2019'
    and company_number = 'RY1'
  group by company_number, technician_id, trans_date) cc on aa.the_date = cc.


where aa.the_date between '12/16/2019' and '12/31/2019'
order by aa.the_date, bb.full_name


-- * --
-- finally, i think this is exactly it

select aa.*, coalesce(bb.flag_hours, 0) as flag_hours, coalesce(bb.shop, 0) as shop_time, 
  coalesce(bb.training, 0) as training, coalesce(cc.adj, 0) as adjustments,
  coalesce(bb.flag_hours, 0) + coalesce(bb.shop, 0) + coalesce(bb.training, 0) + coalesce(cc.adj, 0) as total
from ( -- one row for each tech/day
  select a.the_date, b.*
  from dds.dim_date a, techs b
  where a.the_date between '12/16/2019' and '12/31/2019'
    and b.personnel_type = 'main shop tech'
    and b.store_code = 'ry1') aa
left join ( -- one row for each tech/day for which there are hours
  select a.storecode, c.personnel_id, b.the_date, ((select full_name from fs.personnel where personnel_id = c.personnel_id)), 
    coalesce(sum(case when aa.opcode not like 'TRAIN%'  and aa.opcode not like 'SHOP%' then a.flaghours end), 0) as flag_hours,
    coalesce(sum(case when aa.opcode like 'SHOP%' then a.flaghours end), 0) as shop,
    coalesce(sum(case when aa.opcode like 'TRAIN%' then a.flaghours end), 0) as training
  from ads.ext_fact_repair_order a
  left join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
  join dds.dim_date b on a.flagdatekey = b.date_key
    and b.the_date between '12/16/2019' and '12/31/2019' -- _from_date and _thru_date
  join techs c on a.techkey = any(c.tech_key)
    and c.personnel_type  = 'main shop Tech'
    and c.store_code = 'RY1'
  group by a.storecode, c.personnel_id, b.the_date) bb on aa.the_date = bb.the_date
    and aa.personnel_id = bb.personnel_id
left join ( -- one row for each tech/day for which there are hours
  select company_number, technician_id, arkona.db2_integer_to_date_long(trans_date) as trans_date, sum(labor_hours) as adj
  from arkona.ext_sdpxtim
  where arkona.db2_integer_to_date_long(trans_date) between '12/16/2019' and '12/31/2019'
    and company_number = 'RY1'
  group by company_number, technician_id, trans_date) cc on aa.the_date = cc.trans_date
    and aa.store_code = cc.company_number
    and aa.tech_number = cc.technician_id   
order by aa.full_name, aa.the_date    


-- now, does this work for all
-- shit, except body shop is not flag date
-- adding the conditional join to dds.dim_date actually worked
-- going with one row per tech per day
drop table if exists wtf;
create temp table wtf as
select aa.*, coalesce(bb.flag_hours, 0) as flag_hours, coalesce(bb.shop, 0) as shop_time, 
  coalesce(bb.training, 0) as training, coalesce(cc.adj, 0) as adjustments,
  coalesce(bb.flag_hours, 0) + coalesce(bb.shop, 0) + coalesce(bb.training, 0) + coalesce(cc.adj, 0) as total
from ( -- one row for each tech/day
  select a.the_date, b.*
  from dds.dim_date a, techs b
  where a.the_date between '12/16/2019' and '12/31/2019') aa
left join (
  select a.storecode, c.personnel_id, b.the_date, ((select full_name from fs.personnel where personnel_id = c.personnel_id)), 
    coalesce(sum(case when aa.opcode not like 'TRAIN%'  and aa.opcode not like 'SHOP%' then a.flaghours end), 0) as flag_hours,
    coalesce(sum(case when aa.opcode like 'SHOP%' then a.flaghours end), 0) as shop,
    coalesce(sum(case when aa.opcode like 'TRAIN%' then a.flaghours end), 0) as training
  from ads.ext_fact_repair_order a
  join techs c on a.techkey = any(c.tech_key)  
  left join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
  join dds.dim_date b on 
    case
      when c.personnel_type = 'Metal Tech' then a.closedatekey = b.date_key
      else a.flagdatekey = b.date_key
    end
    and b.the_date between '12/16/2019' and '12/31/2019' -- _from_date and _thru_date
  group by a.storecode, c.personnel_id, b.the_date)  bb on aa.the_date = bb.the_date
    and aa.personnel_id = bb.personnel_id
left join ( -- one row for each tech/day for which there are hours
  select company_number, technician_id, arkona.db2_integer_to_date_long(trans_date) as trans_date, sum(labor_hours) as adj
  from arkona.ext_sdpxtim
  where arkona.db2_integer_to_date_long(trans_date) between '12/16/2019' and '12/31/2019'
  group by company_number, technician_id, trans_date) cc on aa.the_date = cc.trans_date
    and aa.store_code = cc.company_number
    and aa.tech_number = cc.technician_id   
  

-- a lot of dicrepancies with body shop
-- shit, its the close vs flag date thing
-- using the join to segregate body shop techs worked, now no body shop discrepancies
select *
from wtf a
inner join fs.tech_flag_hours_by_day b on a.the_date = b.the_date
  and a.personnel_id = b.personnel_id
  and a.total <> b.flag_hours


select min(the_date) from fs.tech_flag_hours_by_day

go ahead and repopulate fs.tech_flag_hours_by_day;

truncate fs.tech_flag_hours_by_day;
insert into fs.tech_flag_hours_by_day
select aa.personnel_id, aa.the_date, coalesce(bb.flag_hours, 0) as flag_hours, coalesce(bb.shop, 0) as shop_time, 
  coalesce(bb.training, 0) as training, coalesce(cc.adj, 0) as adjustments
--   coalesce(bb.flag_hours, 0) + coalesce(bb.shop, 0) + coalesce(bb.training, 0) + coalesce(cc.adj, 0) as total
from ( -- one row for each tech/day
  select a.the_date, b.*
  from dds.dim_date a, techs b
  where a.the_date between '12/16/2019' and '01/02/2020') aa  -- _from_date and _thru_date
left join (
  select a.storecode, c.personnel_id, b.the_date, ((select full_name from fs.personnel where personnel_id = c.personnel_id)), 
    coalesce(sum(case when aa.opcode not like 'TRAIN%'  and aa.opcode not like 'SHOP%' then a.flaghours end), 0) as flag_hours,
    coalesce(sum(case when aa.opcode like 'SHOP%' then a.flaghours end), 0) as shop,
    coalesce(sum(case when aa.opcode like 'TRAIN%' then a.flaghours end), 0) as training
  from ads.ext_fact_repair_order a
  join techs c on a.techkey = any(c.tech_key)  
  left join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
  join dds.dim_date b on 
    case
      when c.personnel_type = 'Metal Tech' then a.closedatekey = b.date_key
      else a.flagdatekey = b.date_key
    end
    and b.the_date between '12/16/2019' and '01/02/2020' -- _from_date and _thru_date
  group by a.storecode, c.personnel_id, b.the_date)  bb on aa.the_date = bb.the_date
    and aa.personnel_id = bb.personnel_id
left join ( -- one row for each tech/day for which there are hours
  select company_number, technician_id, arkona.db2_integer_to_date_long(trans_date) as trans_date, sum(labor_hours) as adj
  from arkona.ext_sdpxtim
  where arkona.db2_integer_to_date_long(trans_date) between '12/16/2019' and '01/02/2020'
  group by company_number, technician_id, trans_date) cc on aa.the_date = cc.trans_date
    and aa.store_code = cc.company_number
    and aa.tech_number = cc.technician_id;


select max(the_Date) from fs.tech_flag_hours_by_day    

select * from fs.tech_flag_hours_by_day where the_date >= '01/02/2020'