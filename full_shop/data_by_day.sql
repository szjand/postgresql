﻿

------------------------------------------------------------------
--< CSR
------------------------------------------------------------------

drop table if exists fs.csr_body_shop_ids cascade;
create table fs.csr_body_shop_ids (
  body_shop_id citext primary key,
  personnel_id integer not null references fs.personnel(personnel_id));
comment on table fs.csr_body_shop_ids is 'to query appointments set by csr in the body shop system, needed a
    reference to the identifier used in that system. Value retrieved from the advantage BodyShop database, users table.
    any additions to this table have to be handled manually';

insert into fs.csr_body_shop_ids
select createdby, personnel_id
--   substring(createdby, position(' ' in createdby) + 1, length(createdby) - position(' ' in createdby)) as last_name, 
--   b.pymast_employee_number, c.*
from (
  select createdby
  from ads.ext_bs_appointment_history 
  group by createdby) a
left join arkona.xfm_pymast b on (
  substring(createdby, position(' ' in a.createdby) + 1, length(a.createdby) - position(' ' in a.createdby)))::citext = b.employee_last_name
  and b.current_row
join fs.personnel c on b.pymast_employee_number = c.employee_number
  and c.personnel_type = 'CSR';
    
------------------------------------------------------------------
--/> CSR
------------------------------------------------------------------

------------------------------------------------------------------
--< BDR
------------------------------------------------------------------



drop table if exists fs.bdr_service_ids;
create table fs.bdr_service_ids (
  service_id citext primary key,
  personnel_id integer not null references fs.personnel(personnel_id));
  
comment on table fs.csr_body_shop_ids is 'to query appointments set by bdr in the service scheduler, needed a
    reference to the identifier used in that system. Value retrieved from the advantage RydellService database, users table.
    any additions to this table have to be handled manually';

insert into fs.bdr_service_ids    
select a.createdbyid, c.personnel_id
from (
  select a.createdbyid, b.userfirstname, b.userlastname
  from ads.ext_ron_appointments a
  join ads.ext_ron_users b on a.createdbyid = b.userid
  group by a.createdbyid, b.userfirstname, b.userlastname) a
left join arkona.xfm_pymast b on a.userlastname = b.employee_last_name
  and b.current_row
join fs.personnel c on b.pymast_employee_number = c.employee_number
  and c.personnel_type = 'BDR'
where similarity(a.userfirstname, b.employee_first_name)  > .2;  


------------------------------------------------------------------
--</ BDR appointments
------------------------------------------------------------------

select count(*) 
from ads.ext_Ron_appointments
where created::date > current_Date - 90

select extract(day from created), count(*)
from ads.ext_Ron_appointments a
join dds.dim_date b on a.created::date = b.the_date
group by extract(day from created)

select ops.check_for_ads_extract('ext_scheduler_appointments')
select ops.check_for_ads_extract('bs_appointments')

-- create index on ads.ext_ron_appointments(createdbyid);
-- create index on ads.ext_ron_appointments(created);
-- create index on fs.bdr_service_ids(personnel_id);

select b.personnel_id, a.created::date as the_date, count(*) as appointments
from ads.ext_ron_appointments a
join fs.bdr_service_ids b on a.createdbyid = b.service_id
group by b.personnel_id, a.created::date
order by a.created::date

work on the basis of current week
delete where current week
insert current week data

these will all be run in luigi, so they will all reference current_Date - 1


select * from fs.bdr_appointments_by_day where the_date = current_date;

drop table if exists fs.bdr_appointments_by_day;
create table fs.bdr_appointments_by_day (
  personnel_id integer not null references fs.personnel(personnel_id),
  the_date date not null,
  appointments integer not null,
  primary key (personnel_id,the_date));
create index on fs.bdr_appointments_by_day(personnel_id);
create index on fs.bdr_appointments_by_day(the_date);
comment on table fs.bdr_appointments_by_day is 'the number of appointments set each day by bdrs.
  luigi updates this table nightly from the service scheduler. Data is current through
  the last complete day';
  
create or replace function fs.update_bdr_appointments_by_day()
returns void as
$BODY$
/*
purges and repopulates fs.bdr_appointments_by_day for the current week
select fs.update_bdr_appointments_by_day();
*/
declare
  _this_week integer := (
    select week_id 
    from fs.weeks
    where current_date - 1 between from_date and thru_date); 
  _from_date date := (
    select from_date
    from fs.weeks
    where week_id = _this_week);
  _thru_date date := (
    select thru_date
    from fs.weeks
    where week_id = _this_week);    
begin
  delete 
  from fs.bdr_appointments_by_day
  where the_date between _from_date and _thru_date;

  insert into fs.bdr_appointments_by_day(personnel_id,the_date,appointments)
  select b.personnel_id, a.created::date as the_date, count(*) as appointments
  from ads.ext_ron_appointments a
  join fs.bdr_service_ids b on a.createdbyid = b.service_id
  where a.created::date between _from_date and _thru_date
  group by b.personnel_id, a.created::date;
end 
$BODY$
LANGUAGE plpgsql;
comment on function fs.update_bdr_appointments_by_day() is 'purges and repopulates fs.bdr_appointments_by_day for the current week.';

------------------------------------------------------------------
--</ BDR appointments
------------------------------------------------------------------

------------------------------------------------------------------
--< CSR appointments
------------------------------------------------------------------


select * from fs.csr_appointments_by_day where the_date = current_date - 1

drop table if exists fs.csr_appointments_by_day;
create table fs.csr_appointments_by_day (
  personnel_id integer not null references fs.personnel(personnel_id),
  the_date date not null,
  appointments integer not null,
  primary key (personnel_id,the_date));
create index on fs.csr_appointments_by_day(personnel_id);
create index on fs.csr_appointments_by_day(the_date);
comment on table fs.csr_appointments_by_day is 'the number of appointments set each day by csrs.
  luigi updates this table nightly from the body shop scheduler. Data is current through
  the last complete day';

create or replace function fs.update_csr_appointments_by_day()
returns void as
$BODY$
/*
purges and repopulates fs.csr_appointments_by_day for the current week
select fs.update_csr_appointments_by_day();
*/
declare
  _this_week integer := (
    select week_id 
    from fs.weeks
    where current_date - 1 between from_date and thru_date); 
  _from_date date := (
    select from_date
    from fs.weeks
    where week_id = _this_week);
  _thru_date date := (
    select thru_date
    from fs.weeks
    where week_id = _this_week);    
begin
  delete 
  from fs.csr_appointments_by_day
  where the_date between _from_date and _thru_date;

  insert into fs.csr_appointments_by_day(personnel_id,the_date,appointments)
  select b.personnel_id, a.createdts::date as the_date, count(*) as appointments
  from ads.ext_bs_appointment_history a
  join fs.csr_body_shop_ids b on a.createdby = b.body_shop_id
  where a.createdts::date between _from_date and _thru_date
  group by b.personnel_id, a.createdts::date;
end 
$BODY$
LANGUAGE plpgsql;
comment on function fs.update_csr_appointments_by_day() is 'purges and repopulates fs.csr_appointments_by_day for the current week.';
  
------------------------------------------------------------------
--/> CSR appointments
------------------------------------------------------------------

------------------------------------------------------------------
--< writer ros opened
------------------------------------------------------------------


-- 12/22
-- ok, dim_service_writer has been cleaned up enough that this returns valid data
-- which means dispensing with a separate writer id table
select *
from fs.personnel a
left join ads.ext_dim_service_writer b on a.employee_number = b.employeenumber
  and a.store_code = b.storecode
  and b.active
  and b.currentrow
where a.personnel_type = 'Service Advisor'
  and not a.is_deleted
order by a.store_code, a.full_name  

select * from fs.advisor_ros_by_day

drop table if exists fs.advisor_ros_by_day;
create table fs.advisor_ros_by_day (
  personnel_id integer not null references fs.personnel(personnel_id),
  the_date date not null,
  ros integer not null,
  primary key (personnel_id,the_date));
create index on fs.advisor_ros_by_day(personnel_id);
create index on fs.advisor_ros_by_day(the_date);
comment on table fs.advisor_ros_by_day is 'the number of ros opened each day by service advisors.
  luigi updates this table nightly from postgresql table ads.ext_fact_repair_order. Data is current through
  the last complete day';

  
create or replace function fs.update_advisor_ros_by_day()
returns void as
$BODY$
/*
purges and repopulates fs.advisor_ros_by_day for the current week
select fs.update_advisor_ros_by_day();
*/
declare
  _this_week integer := (
    select week_id 
    from fs.weeks
    where current_date - 1 between from_date and thru_date); 
  _from_date date := (
    select from_date
    from fs.weeks
    where week_id = _this_week);
  _thru_date date := (
    select thru_date
    from fs.weeks
    where week_id = _this_week);    
begin
  delete 
  from fs.advisor_ros_by_day
  where the_date between _from_date and _thru_date;

  insert into fs.advisor_ros_by_day(personnel_id,the_date,ros)
  select personnel_id, the_date, count(*) as ros
  from (
    select c.personnel_id, b.the_date, a.ro
    from ads.ext_fact_repair_order a
    join dds.dim_date b on a.opendatekey = b.date_key
      and b.the_date between _from_date and _thru_date
    join (
      select a.personnel_id, b.servicewriterkey
      from fs.personnel a
      left join ads.ext_dim_service_writer b on a.employee_number = b.employeenumber
        and a.store_code = b.storecode
        and b.active
        and b.currentrow
      where a.personnel_type = 'Service Advisor'
        and not a.is_deleted) c on a.servicewriterkey = c.servicewriterkey
    group by c.personnel_id, b.the_date, a.ro) d     
  group by personnel_id, the_date;    

end 
$BODY$
LANGUAGE plpgsql;
comment on function fs.update_csr_appointments_by_day() is 'purges and repopulates fs.advisor_ros_by_day for the current week.'; 
------------------------------------------------------------------
--/> writer ros opened
------------------------------------------------------------------

------------------------------------------------------------------
--< main shop tech flag hours
------------------------------------------------------------------

select * from fs.personnel

-- looks good
select *
from fs.personnel a
left join ads.ext_dim_tech b on a.employee_number = b.employeenumber
  and a.store_code = b.storecode
  and b.active
  and b.currentrow
where a.personnel_type in('Main Shop Tech','Metal Tech')
  and not a.is_deleted
order by a.store_code, personnel_type, a.full_name  


select * from fs.tech_flag_hours_by_day

drop table if exists fs.tech_flag_hours_by_day;
create table fs.tech_flag_hours_by_day (
  personnel_id integer not null references fs.personnel(personnel_id),
  the_date date not null,
  flag_hours numeric(8,2) not null,
  primary key (personnel_id,the_date));
create index on fs.tech_flag_hours_by_day(personnel_id);
create index on fs.tech_flag_hours_by_day(the_date);
comment on table fs.tech_flag_hours_by_day is 'the flag hours flagged each day by main shop and metal techs.
  luigi updates this table nightly from postgresql table ads.ext_fact_repair_order. Data is current through
  the last complete day';

-- 1/2/20 add shop_time and training
alter table fs.tech_flag_hours_by_day
add column shop_time numeric(8,2),
add column training numeric(8,2);

update fs.tech_flag_hours_by_day
set shop_time = 0, training = 0;

alter table fs.tech_flag_hours_by_day
alter column shop_time set not null,
alter column training set not null;

-- 1/3/20 add adjustments as well
alter table fs.tech_flag_hours_by_day
add column adjustments numeric(8,2);

update fs.tech_flag_hours_by_day
set adjustments = 0;

alter table fs.tech_flag_hours_by_day
alter column adjustments set not null;

in .../full_shop/sql/oops_forgor_shop_time_exception.sql,worked thru the query for the function
truncated and updated fs.tech_flag_hours_by_day for 12/16 -> 1/2

CREATE OR REPLACE FUNCTION fs.update_tech_flag_hours_by_day()
  RETURNS void AS
$BODY$
/*
main shop techs: flag date
body shop metal tech: close date
purges and repopulates fs.tech_flag_hours_by_day for the current week
creates one row for each tech for each day in fs.tech_flag_hours_by_day
select fs.update_tech_flag_hours_by_day();
*/
declare
  _this_week integer := (
    select week_id 
    from fs.weeks
    where current_date - 1 between from_date and thru_date); 
  _from_date date := (
    select from_date
    from fs.weeks
    where week_id = _this_week);
  _thru_date date := (
    select thru_date
    from fs.weeks
    where week_id = _this_week);    
begin

  -- create a set of relevant tech information
  drop table if exists techs cascade;
  create temp table techs as
  select a.full_name, a.personnel_id, a.personnel_type, a.store_code, a.employee_number, 
    b.technumber as tech_number, array_agg(distinct b.techkey) as tech_key
  from fs.personnel a
  left join ads.ext_Dim_tech b on a.employee_number = b.employeenumber
    and b.techkey <> 100 -- exclude ancient ryan lene instance
  where not a.is_deleted
    and a.personnel_type in ('Main Shop Tech', 'Metal Tech')
  group by a.full_name, a.personnel_id, a.personnel_type, a.store_code, a.employee_number, b.technumber;
  create unique index on techs(personnel_id);
  create unique index on techs(employee_number);
  create unique index on techs(tech_number,store_code);
  CREATE INDEX ON techs USING GIN(tech_key);
  create index on techs(personnel_type);

  -- assert at least a count match between techs and fs.personnel
--   do
--   $$
--   begin
  assert (
    (select count(*) from techs)
    =
    (select count(*)
    from fs.personnel
    where not is_deleted
      and personnel_type  in ('Main Shop Tech', 'Metal Tech'))), 'mismatch between techs and fs.personnel';
--   end
--   $$;  

  delete 
  from fs.tech_flag_hours_by_day
  where the_date between _from_date and _thru_date;

  insert into fs.tech_flag_hours_by_day(personnel_id,the_date,flag_hours,shop_time,training,adjustments)
  select aa.personnel_id, aa.the_date, coalesce(bb.flag_hours, 0) as flag_hours, coalesce(bb.shop, 0) as shop_time, 
    coalesce(bb.training, 0) as training, coalesce(cc.adj, 0) as adjustments
  from ( -- one row for each tech/day
    select a.the_date, b.*
    from dds.dim_date a, techs b
    where a.the_date between _from_date and _thru_date) aa  
  left join (
    select a.storecode, c.personnel_id, b.the_date, ((select full_name from fs.personnel where personnel_id = c.personnel_id)), 
      coalesce(sum(case when aa.opcode not like 'TRAIN%'  and aa.opcode not like 'SHOP%' then a.flaghours end), 0) as flag_hours,
      coalesce(sum(case when aa.opcode like 'SHOP%' then a.flaghours end), 0) as shop,
      coalesce(sum(case when aa.opcode like 'TRAIN%' then a.flaghours end), 0) as training
    from ads.ext_fact_repair_order a
    join techs c on a.techkey = any(c.tech_key)  
    left join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
    join dds.dim_date b on 
      case
        when c.personnel_type = 'Metal Tech' then a.closedatekey = b.date_key
        else a.flagdatekey = b.date_key
      end
      and b.the_date between _from_date and _thru_date
    group by a.storecode, c.personnel_id, b.the_date)  bb on aa.the_date = bb.the_date
      and aa.personnel_id = bb.personnel_id
  left join ( -- one row for each tech/day for which there are hours
    select company_number, technician_id, arkona.db2_integer_to_date_long(trans_date) as trans_date, sum(labor_hours) as adj
    from arkona.ext_sdpxtim
    where arkona.db2_integer_to_date_long(trans_date) between _from_date and _thru_date
    group by company_number, technician_id, trans_date) cc on aa.the_date = cc.trans_date
      and aa.store_code = cc.company_number
      and aa.tech_number = cc.technician_id;

end 
$BODY$
LANGUAGE plpgsql;
comment on function fs.update_tech_flag_hours_by_day() is 'purges and repopulates fs.tech_flag_hours_by_day for the current week.';   
------------------------------------------------------------------
--/> main shop tech flag hours
------------------------------------------------------------------

------------------------------------------------------------------
--< clock hours
------------------------------------------------------------------

drop table if exists fs.clock_hours_by_day;
create table fs.clock_hours_by_day (
  personnel_id integer not null references fs.personnel(personnel_id),
  the_date date not null,
  clock_hours numeric(8,2) not null,
  pto_hours numeric(8,2) not null,
  primary key (personnel_id,the_date));
create index on fs.clock_hours_by_day(personnel_id);
create index on fs.clock_hours_by_day(the_date);
comment on table fs.clock_hours_by_day is 'the clock hours and pto hours each day from dms for each full shop personnel.
  luigi updates this table nightly from postgresql table arkona.xfm_pypclockin. Data is current through
  the last complete day'; 

create or replace function fs.update_clock_hours_by_day()
returns void as
$BODY$
/*
purges and repopulates fs.clock_hours_by_day for the current week
select fs.update_clock_hours_by_day();
*/
declare
  _this_week integer := (
    select week_id 
    from fs.weeks
    where current_date - 1 between from_date and thru_date); 
  _from_date date := (
    select from_date
    from fs.weeks
    where week_id = _this_week);
  _thru_date date := (
    select thru_date
    from fs.weeks
    where week_id = _this_week);    
begin
  delete 
  from fs.clock_hours_by_day
  where the_date between _from_date and _thru_date;

  insert into fs.clock_hours_by_day(personnel_id,the_date,clock_hours,pto_hours)
  select c.personnel_id, a.the_date, a.clock_hours, a.vac_hours + a.pto_hours + a.hol_hours as pto_hours
  from arkona.xfm_pypclockin aa
  join dds.dim_date b on a.the_date = b.the_date
    and b.the_date between _from_date and _thru_date
  join fs.personnel c on a.employee_number = c.employee_number  
    and not c.is_deleted;

end 
$BODY$
LANGUAGE plpgsql;
comment on function fs.update_clock_hours_by_day() is 'purges and repopulates fs.clock_hours_by_day for the current week.';     
------------------------------------------------------------------
--/> clock hours
------------------------------------------------------------------

-- 12/28/19 checking up on the data
select 'csr',min(the_date), max(the_date), count(*) as total, count(*)/(max(the_date) - min(the_date)) as per_day from fs.csr_appointments_by_day
union
select 'bdr',min(the_date), max(the_date), count(*), count(*)/(max(the_date) - min(the_date)) from fs.bdr_appointments_by_day
union
select 'adv',min(the_date), max(the_date), count(*), count(*)/(max(the_date) - min(the_date)) from fs.advisor_ros_by_day
union
select 'tech',min(the_date), max(the_date), count(*), count(*)/(max(the_date) - min(the_date)) from fs.tech_flag_hours_by_day

select *
from luigi.luigi_log
where pipeline = 'full_shop'
order by the_date, task


select * 
from fs.personnel

select *
from fs.weeks
limit 10

select * from dds.dim_date where the_date = current_date
