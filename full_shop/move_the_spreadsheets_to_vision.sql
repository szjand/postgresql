﻿--------------------------------------------------
--< 1/13
-- first thing that happened was i felt an undeniable urge to refactor aftons data model
-- so, i did
-- apparently she like it
-- and has refactored all her code
-- these are the new tables
-- drop schema fs_1 cascade;
--------------------------------------------------
create schema fs_1;

drop table if exists fs_1.personnel cascade;
create table fs_1.personnel (
  personnel_id serial primary key,
  employee_number citext not null,
  full_name citext not null, 
  clock_hours numeric(8,2) not null,
  capacity numeric(8,2) not null,
  created_ts timestamp with time zone not null,
  deleted_ts timestamp with time zone not null default '12/31/9999 00:00:01',
  is_deleted boolean not null);
create unique index on fs_1.personnel(full_name, created_ts);
comment on table fs_1.personnel is 'clock hours and capacity set thru the admin page in full shop';

insert into fs_1.personnel(personnel_id,employee_number,full_name,
  clock_hours, capacity, created_ts,deleted_ts,is_deleted)
select a.personnel_id,a.employee_number,a.full_name,
  a.clock_hours_per_day, b.answer, a.created_ts,
  coalesce(deleted_ts,'12/31/9999 00:00:01'::timestamptz),is_deleted
from fs.personnel a
join fs.personnel_capacities b on a.personnel_id = b.personnel_id;

create table fs_1.jobs (
    store citext not null, -- RY1, RY2, MKT
    job citext not null,
    department citext not null,
    capacity_type citext not null, -- used as a column header
    question citext not null,
    primary key(store,job));
    
insert into fs_1.jobs (store,job,department,capacity_type,question)
select 'RY1', a.personnel_type, a.department, 
  case b.category
    when 'Appointment Capacity' then 'Appointments'
    when 'Flag Capacity' then 'Flag Hours'
    when 'RO Capacity' then 'ROs Opened'
  end,
  b.question
from fs.personnel_types a
left join fs.personnel_capacity_types b on a.personnel_type = b.personnel_type
where a.personnel_type <> 'BDR'
union
select 'MKT', a.personnel_type, a.department, 
  case b.category
    when 'Appointment Capacity' then 'Appointments'
    when 'Flag Capacity' then 'Flag Hours'
    when 'RO Capacity' then 'ROs Opened'
  end,
  b.question
from fs.personnel_types a
left join fs.personnel_capacity_types b on a.personnel_type = b.personnel_type
where a.personnel_type = 'BDR'
union
select 'RY2', a.personnel_type, a.department, 
  case b.category
    when 'Appointment Capacity' then 'Appointments'
    when 'Flag Capacity' then 'Flag Hours'
    when 'RO Capacity' then 'ROs Opened'
  end,
  b.question
from fs.personnel_types a
left join fs.personnel_capacity_types b on a.personnel_type = b.personnel_type
where a.personnel_type in ('Service Advisor','Main Shop Tech');


drop table if exists fs_1.personnel_jobs;
create table fs_1.personnel_jobs (
  personnel_id integer not null references fs_1.personnel(personnel_id),
  job citext not null,
  store citext not null,
  from_date date not null,
  thu_date date not null default '12/31/9999',
  primary key(personnel_id,job,store),
  foreign key (store,job) references fs_1.jobs(store,job));


insert into fs_1.personnel_jobs(personnel_id,job,store,from_date)    
select a.personnel_id, a.personnel_type, 
  case
    when a.personnel_type = 'BDR' then  'MKT'
    else
      case
        when left(a.employee_number, 1) = '1' then 'RY1'
        when left(a.employee_number, 1) = '2' then 'RY2'
      end
  end, '12/17/2019'
from fs.personnel a
where not a.is_deleted;

drop table if exists fs_1.personnel_daily_capacities;
create table fs_1.personnel_daily_capacities (
  personnel_id integer not null references fs_1.personnel(personnel_id),
  the_date date not null,
  clock_hours numeric(8,2) not null default '0',
  capacity numeric(8,2) not null default '0',
  is_changed boolean not null,
  last_changed_ts timestamp with time zone not null default '12/31/9999 00:00:01',
  personnel_daily_capacities_id integer not null DEFAULT nextval('fs.personnel_daily_capacities_ember_id_seq'::regclass),
  primary key (personnel_id, the_date));

insert into fs_1.personnel_daily_capacities
select personnel_id,the_date,clock_hours, actual_capacity,
  is_changed,last_changed_ts,personnel_daily_capacities_id
from fs.personnel_daily_capacities a
join dds.dim_Date b on a.date_key = b.date_key;

--------------------------------------------------
--/> 1/13
--------------------------------------------------

so, now we need 4 tables to simulate the spreadsheet

aftons first cut:
CREATE TABLE fs.personnel_daily_details
(
  personnel_daily_capacities_id bigint NOT NULL,
  budgeted_clock_hours numeric(8,2) NOT NULL,
  pto_clock_hours numeric(8,2),
  capacity numeric(8,2) NOT NULL,
  shop_time numeric(8,2),
  training numeric(8,2),
  ember_id serial NOT NULL,
  CONSTRAINT personnel_daily_details_pk PRIMARY KEY (personnel_daily_capacities_id),
  CONSTRAINT personnel_daily_details_personnel_daily_capacities_id_fkey FOREIGN KEY (personnel_daily_capacities_id)
      REFERENCES fs.personnel_daily_capacities (personnel_daily_capacities_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)

after talking with her, going with an all data need for the page in the table approach
these are tables that will be updated nightly

-- 1/15/20 got aftons blessing on this table
drop table if exists fs.personnel_daily_details;
CREATE TABLE fs.personnel_daily_details (
  personnel_id integer not null references fs.personnel(personnel_id),
  full_name citext not null,
  the_date date not null,
  week_id integer not null references fs.weeks(week_id),
  store citext not null,
  job citext not null,
  department citext not null,
  capacity_type citext not null,
  budgeted_clock_hours numeric(8,2) not null default '0',
  actual_clock_hours numeric(8,2) not null default '0',
  pto_hours numeric(8,2) not null default '0',
  budgeted_capacity numeric(8,2) not null default '0',
  actual_capacity numeric(8,2) not null default '0',
  shop_time numeric(8,2) default '0',
  training numeric(8,2) default '0',
  ember_id serial,
  primary key(personnel_id,the_date),
  foreign key (job,store) references fs.jobs(job,store));
create index on fs.personnel_daily_details(department);
create index on fs.personnel_daily_details(capacity_type);


insert into fs.personnel_daily_details
select a.personnel_id, a.full_name, aa.the_date, aa.week_id, c.store, c.job, c.department, c.capacity_type,
  coalesce(d.clock_hours, 0) as budgeted_clock_hours, coalesce(f.clock_hours, 0) as actual_clock_hours, 
  coalesce(f.pto_hours, 0) as pto_hours, coalesce(d.capacity, 0) as budgeted_capacity, 
  case 
    when capacity_type = 'flag hours' then coalesce(e.flag_hours, 0)
    when capacity_type = 'ros opened' then coalesce(e3.ros, 0)
    when capacity_type = 'appointments' and b.job = 'BDR' then coalesce(e2.appointments, 0)
    when capacity_type = 'appointments' and b.job = 'CSR' then coalesce(e1.appointments, 0)
  end as actual_capacity,
  case when capacity_type = 'flag hours' then coalesce(e.shop_time, 0) end as shop_time,
  case when capacity_type = 'flag hours' then coalesce(e.training, 0) end as training
from fs.personnel a
join (
  select a.week_id, b.the_date
  from fs.weeks a 
  join dds.dim_date b on b.the_date between a.from_date and a.thru_date
    and b.the_date between '12/15/2019' and '01/14/2020') aa on true
join fs.personnel_jobs b on a.personnel_id = b.personnel_id
  and  b.thru_date > current_date
join fs.jobs c on b.job = c.job
  and b.store = c.store
join fs.personnel_daily_capacities d on a.personnel_id = d.personnel_id
  and  aa.the_date = d.the_date
left join fs.tech_flag_hours_by_day e on aa.the_date = e.the_date
  and a.personnel_id = e.personnel_id
left join fs.csr_appointments_by_day e1 on aa.the_date = e1.the_date
  and a.personnel_id = e1.personnel_id
left join fs.bdr_appointments_by_day e2 on aa.the_date = e2.the_date
  and a.personnel_id = e2.personnel_id
left join fs.advisor_ros_by_day e3 on aa.the_date = e3.the_date
  and a.personnel_id = e3.personnel_id
left join fs.clock_hours_by_day f on a.personnel_id = f.personnel_id
  and aa.the_date = f.the_date  
where not a.is_deleted  

order by c.store, c.job, the_date, a.full_name


select * 
from fs.personnel_daily_details
where store = 'RY1'
  and job = 'Main Shop Tech'
  and week_id = (
    select week_id
    from fs.weeks
    where '01/08/2020' between from_date and thru_date)
  and the_date <= '01/09/2020'    
order by the_date, full_name    


now the remaining tables should just be a rollup of fs.personnel_daily_details

drop table if exists fs.personnel_weekly_totals;
CREATE TABLE fs.personnel_weekly_totals (
  personnel_id integer not null references fs.personnel(personnel_id),
  full_name citext not null,
  week_id integer not null references fs.weeks(week_id),
  store citext not null,
  job citext not null,
  department citext not null,
  capacity_type citext not null,
  budgeted_clock_hours numeric(8,2) not null default '0',
  actual_clock_hours numeric(8,2) not null default '0',
  pto_hours numeric(8,2) not null default '0',
  budgeted_capacity numeric(8,2) not null default '0',
  actual_capacity numeric(8,2) not null default '0',
  shop_time numeric(8,2) default '0',
  training numeric(8,2) default '0',
  ember_id serial,
  primary key(personnel_id,week_id),
  foreign key (job,store) references fs.jobs(job,store));
create index on fs.personnel_weekly_totals(department);
create index on fs.personnel_weekly_totals(capacity_type);

insert into fs.personnel_weekly_totals
select personnel_id, full_name, week_id, store, job, department, capacity_type,
  sum(budgeted_clock_hours) as budgeted_clock_hours,  sum(actual_clock_hours) as actual_clock_hours,
  sum(pto_hours) as pto_hours, sum(budgeted_capacity) as budgeted_capacity, 
  sum(actual_capacity) as actual_capacity,
  sum(shop_time) as shop_time, sum(training) as training
from fs.personnel_daily_details
group by personnel_id, full_name, week_id, store, job, department, capacity_type;

drop table if exists fs.job_daily_totals;
CREATE TABLE fs.job_daily_totals (
  the_date date not null,
  week_id integer not null references fs.weeks(week_id),
  store citext not null,
  job citext not null,
  department citext not null,
  capacity_type citext not null,
  budgeted_clock_hours numeric(8,2) not null default '0',
  actual_clock_hours numeric(8,2) not null default '0',
  pto_hours numeric(8,2) not null default '0',
  budgeted_capacity numeric(8,2) not null default '0',
  actual_capacity numeric(8,2) not null default '0',
  shop_time numeric(8,2) default '0',
  training numeric(8,2) default '0',
  ember_id serial,
  primary key(the_date,job,store),
  foreign key (job,store) references fs.jobs(job,store));
create index on fs.job_daily_totals(department);
create index on fs.job_daily_totals(capacity_type);

insert into fs.job_daily_totals
select the_date, week_id, store, job, department, capacity_type,
  sum(budgeted_clock_hours) as budgeted_clock_hours,  sum(actual_clock_hours) as actual_clock_hours,
  sum(pto_hours) as pto_hours, sum(budgeted_capacity) as budgeted_capacity, 
  sum(actual_capacity) as actual_capacity,
  sum(shop_time) as shop_time, sum(training) as training
from fs.personnel_daily_details
group by the_date, week_id, store, job, department, capacity_type;


drop table if exists fs.job_weekly_totals;
CREATE TABLE fs.job_weekly_totals (
  week_id integer not null references fs.weeks(week_id),
  store citext not null,
  job citext not null,
  department citext not null,
  capacity_type citext not null,
  budgeted_clock_hours numeric(8,2) not null default '0',
  actual_clock_hours numeric(8,2) not null default '0',
  pto_hours numeric(8,2) not null default '0',
  budgeted_capacity numeric(8,2) not null default '0',
  actual_capacity numeric(8,2) not null default '0',
  shop_time numeric(8,2) default '0',
  training numeric(8,2) default '0',
  ember_id serial,
  primary key(week_id,job,store),
  foreign key (job,store) references fs.jobs(job,store));
create index on fs.job_weekly_totals(department);
create index on fs.job_weekly_totals(capacity_type);

insert into fs.job_weekly_totals
select week_id, store, job, department, capacity_type,
  sum(budgeted_clock_hours) as budgeted_clock_hours,  sum(actual_clock_hours) as actual_clock_hours,
  sum(pto_hours) as pto_hours, sum(budgeted_capacity) as budgeted_capacity, 
  sum(actual_capacity) as actual_capacity,
  sum(shop_time) as shop_time, sum(training) as training
from fs.personnel_daily_details
group by week_id, store, job, department, capacity_type;