﻿select b.personnel_id, a.created::date as the_date, count(*) as appointments
from ads.ext_ron_appointments a
join fs.bdr_service_ids b on a.createdbyid = b.service_id
where a.created::date between _from_date and _thru_date
group by b.personnel_id, a.created::date;



  select 'gm' as store, b.personnel_id, a.created::date as the_date, count(*) as appointments
  from ads.ext_ron_appointments a
  join fs.bdr_service_ids b on a.createdbyid = b.service_id
  where a.created::date between current_Date - 2 and current_date - 1
  group by b.personnel_id, a.created::date  
union
select 'hn', b.personnel_id, a.created::date as the_date, count(*) as appointments
from ads.ext_ron_honda_appointments a
join fs.bdr_service_ids b on a.createdbyid = b.service_id
where a.created::date between current_Date - 2 and current_date - 1
group by b.personnel_id, a.created::date  
order by personnel_id, store

select * from fs.bdr_appointments_by_day where the_date = '01/06/2020'

update fs.bdr_appointments_by_day x
set appointments = y.appointments
from (
  select the_date, personnel_id, count(*) as appointments
  from (
  select appointmentid, b.personnel_id, a.created::date as the_date
  from ads.ext_ron_appointments a
  join fs.bdr_service_ids b on a.createdbyid = b.service_id
  where a.created::date _from_date and _thru_date
  union
  select appointmentid, b.personnel_id, a.created::date as the_date 
  from ads.ext_ron_honda_appointments a
  join fs.bdr_service_ids b on a.createdbyid = b.service_id
  where a.created::date  between _from_date and _thru_date
  group by the_date, personnel_id) y
where x.the_date = y.the_date
  and x.personnel_id = y.personnel_id

select max(the_date) from fs.bdr_appointments_by_day


select a.* , b.full_name
from fs.bdr_appointments_by_day a
join fs.personnel b on a.personnel_id = b.personnel_id
where a.the_date between '12/29/2019' and '01/04/2020'
order by a.personnel_id, the_date




  insert into fs.bdr_appointments_by_day(personnel_id,the_date,appointments)
  select the_date, personnel_id, count(*) as appointments
  from (
    select appointmentid, b.personnel_id, a.created::date as the_date
    from ads.ext_ron_appointments a
    join fs.bdr_service_ids b on a.createdbyid = b.service_id
    where a.created::date between _from_date and _thru_date
    union
    select appointmentid, b.personnel_id, a.created::date as the_date 
    from ads.ext_ron_honda_appointments a
    join fs.bdr_service_ids b on a.createdbyid = b.service_id
    where a.created::date  between _from_date and _thru_date) y
  group by personnel_id, the_date;