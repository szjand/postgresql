﻿/*
05//15/21
fuck me, in the metting i was asked and said it was GM store only
well, it wasn't, it was market
anyway, ben k asked for honda, so now, i should really do each store

original was thru 3/31, expand to 5/14/21

06/02/21
expand to 5/31/21
*/
drop table if exists bopmast cascade; --14641
create temp table bopmast as
select bopmast_company_number as store, record_key, bopmast_stock_number as stock_number, bopmast_vin, date_capped, vehicle_type as "N/U"
from arkona.ext_bopmast
where record_status = 'U'
  and date_capped between '01/01/2019' and '05/31/2021';
create index on bopmast(store);
create index on bopmast(record_key);


drop table if exists boptrad cascade;  --6149
create temp table boptrad as
select a.store, a.date_capped, b.vin, b.stock_, b.trade_allowance, b.acv
from bopmast a
join arkona.ext_boptrad b on a.store = b.company_number
  and a.record_key = b.key;
create index on boptrad(vin);  
create index on boptrad(store);


-- create index on veh.makes_models(make) ;
-- create index on veh.makes_models(model);

-- change age to start at model year -1 on sept 1
select a.*, b.year, 
	round((date_capped - to_date('01'||'01'||year::text,'MMDDYYYY'))/365.0, 1) as age_in_years_at_trade,
	b.year - 1, 
	round((date_capped - to_date('09'||'01'||(year - 1)::text,'MMDDYYYY'))/365.0, 1) as age_in_years_at_trade_2
from boptrad a
join arkona.ext_inpmast b on a.vin = b.inpmast_vin
limit 100

-- cleaned up some, inner join and 6060 -> 5766
-- 6149 -> 5853 
drop table if exists trades_1 cascade;
create temp table trades_1 as
select a.*, b.year, c.make, c.model, 
  round((date_capped - to_date('09'||'01'||(year - 1)::text,'MMDDYYYY'))/365.0, 1) as age_in_years_at_trade_2
-- 	round((date_capped - to_date('01'||'01'||year::text,'MMDDYYYY'))/365.0, 1) as age_in_years_at_trade
from boptrad a
join arkona.ext_inpmast b on a.vin = b.inpmast_vin
join veh.makes_models c on b.make = c.make and b.model = c.model
	or
		case
			when b.model like 'captiva%' then c.model = 'captiva sport fleet'
			when b.model like 'envision%' then c.model = 'envision'
			when b.model like 'cruze%' then c.model = 'cruze'
			when b.model = 'SILVERADO 2500HD DIESEL' then c.model = 'Silverado 2500HD'
			when b.model = 'SILVERADO 3500HD DIESEL' then c.model = 'Silverado 3500HD'
			when b.model like 'charger%' then c.model = 'charger'
			when b.model like 'challenger%' then c.model = 'challenger'
			when b.model like 'dart%' then c.model = 'dart'
			when b.model like 'durango%' then c.model = 'durango'
			when b.model like 'GRAND CARAVAN%' then c.model = 'grand caravan'
			when b.model like 'ram 1500%' then c.model = 'ram 1500'
			when b.model like 'edge%' then c.model = 'edge'
			when b.model like 'escape%' then c.model = 'escape'
			when b.model like 'explorer%' then c.model = 'explorer'
			when b.model like 'F-150%' then c.model = 'F-150'
			when b.model like 'fusion%' then c.model = 'fusion'
			when b.model like 'f-250%' then c.model = 'Super Duty F-250'
			when b.model like 'f-350%' then c.model = 'envision'
			when b.model like 'taurus%' then c.model = 'taurus'
			when b.model like 'SIERRA 2500HD%' then c.model = 'SIERRA 2500HD'
			when b.model like 'accord%' then c.model = 'accord sedan'
			when b.model like 'cherokee%' then c.model = 'cherokee'
			when b.model like 'grand cherokee%' then c.model = 'grand cherokee'
			when b.model like 'SORENTO%' then c.model = 'SORENTO'
			when b.model like 'PATHFINDER%' then c.model = 'PATHFINDER'
			when b.make = 'ram' and b.model like '1500 (%' then c.model = '1500'
			when b.make = 'ram' and b.model like '2500 (%' then c.model = '2500'
		end;

create index on trades_1(make);		
create index on trades_1(model);		

select * from  trades_1 limit 100

/*
06/03/21 for working on peg vehicles
drop table if exists jon.peg_trades cascade;
create table jon.peg_trades (
  store citext not null,
  date_acquired date not null,
  vin citext not null, 
  stock_number citext not null,
  trade_allowance integer,
  acv integer, 
  model_year integer not null, 
  make citext not null,
  model citext not null,
  age numeric,
  primary key(stock_number));

create index on jon.peg_trades(stock_number);
create index on jon.peg_trades(vin);
create index on jon.peg_trades(date_acquired);
create index on jon.peg_trades(model_year);
create index on jon.peg_trades(make);
create index on jon.peg_trades(model);

insert into jon.peg_trades
select store,date_capped,vin,stock_,trade_allowance::integer,
	acv::integer,year,make,max(model),age_in_years_at_trade_2
from trades_1	
group by store,date_capped,vin,stock_,trade_allowance::integer,
	acv::integer,year,make,age_in_years_at_trade_2;	

comment on table jon.peg_trades is 'trades from 2019 thru 202105 for working on peg vehicles';
comment on column jon.peg_trades.age is 'age in years at the time of acquisition';
*/	

select * from trades_1 a
join (
select stock_
from trades_1
group by stock_
having count(*) > 1) b on a.stock_ = b.stock_
order by a.stock_

-- shape size pareto
select b.shape||'-'||b.size, count(*) as the_count
from trades_1 a 
join veh.shape_size_classifications b on a.make = b.make
  and a.model = b.model
where store = 'RY2'  
group by b.shape||'-'||b.size
order by count(*) desc

-- add age at time of trade
select b.shape||'-'||b.size||'-'||(round(age_in_years_at_trade_2, 0))::text as shape_size_age , count(*) as the_count
from trades_1 a 
join veh.shape_size_classifications b on a.make = b.make
  and a.model = b.model
where store = 'RY2'  
group by b.shape||'-'||b.size||'-'||(round(age_in_years_at_trade_2, 0))::text
having count(*) > 9
order by count(*) desc

-- just age in years
select (round(age_in_years_at_trade_2, 0))::text||' years' as age , count(*) as the_count
from trades_1 a 
where store = 'RY2'
group by (round(age_in_years_at_trade_2, 0))::text||' years'
-- having count(*) > 40
order by count(*) desc

-- miles
--  5766 -> 5708
select miles_category, count(*) as the_count
from (
	select date_capped, vin, stock_, year, make, model, shape, size, age_in_years_at_trade_2, value as miles, miles_per_year,
		case
			when miles_per_year < 5000 then 'freaky low'
			when miles_per_year between 5000 and 9999 then 'low'
			when miles_per_year between 10000 and 14999 then 'avg low'
			when miles_per_year between 15000 and 19999 then 'avg hi'
			when miles_per_year between 20000 and 24999 then 'hi'
			when miles_per_year > 24999 then 'freaky hi'
		end as miles_category		
	from (  		
		select a.*, f.value, b.shape, b.size, round(f.value/case when a.age_in_years_at_trade_2 = 0 then 1 else a.age_in_years_at_trade_2 end, 0) as miles_per_year
		from trades_1 a 
		join veh.shape_size_classifications b on a.make = b.make
			and a.model = b.model
		join ads.ext_vehicle_inventory_items d on a.stock_ = d.stocknumber
		join ads.ext_vehicle_evaluations e on d.vehicleinventoryitemid = e.vehicleinventoryitemid
		join ads.ext_vehicle_item_mileages f on e.vehicleevaluationid = f.tablekey  
			and f.value > 100
		where store = 'RY2') x) xx
group by miles_category			


-- miles for the top 5 shape size
select shape_size||'-'||miles_category, count(*)
from (
select date_capped, vin, stock_, year, make, model, shape_size, age_in_years_at_trade_2, value as miles, miles_per_year,
	case
		when miles_per_year < 5000 then 'freaky low'
		when miles_per_year between 5000 and 9999 then 'low'
		when miles_per_year between 10000 and 14999 then 'avg low'
		when miles_per_year between 15000 and 19999 then 'avg hi'
		when miles_per_year between 20000 and 24999 then 'hi'
		when miles_per_year > 24999 then 'freaky hi'
	end as miles_category		
from (  		
	select a.*, f.value, b.shape||'-'||b.size as shape_size, round(f.value/case when a.age_in_years_at_trade_2 = 0 then 1 else a.age_in_years_at_trade_2 end, 0) as miles_per_year
	from trades_1 a 
	join veh.shape_size_classifications b on a.make = b.make
		and a.model = b.model
	join ads.ext_vehicle_inventory_items d on a.stock_ = d.stocknumber
	join ads.ext_vehicle_evaluations e on d.vehicleinventoryitemid = e.vehicleinventoryitemid
	join ads.ext_vehicle_item_mileages f on e.vehicleevaluationid = f.tablekey  
		and f.value > 100
-- 	where b.shape||'-'||b.size in ('Pickup-Large','SUV-Small','Car-Medium','SUV-Medium','SUV-Large')
-- 	  and store = 'RY1') x) xx
	where b.shape||'-'||b.size in ('Pickup-Large','SUV-Small','Car-Medium','SUV-Medium','Car-Small')
	  and store = 'RY2') x) xx	  
group by shape_size||'-'||miles_category	


select make, model, shape, size, miles_category, count(*)
from (
	select a.make, a.model, b.shape, b.size, 
		case
			when a.miles_per_year < 5000 then 'freaky low'
			when a.miles_per_year between 5000 and 9999 then 'low'
			when a.miles_per_year between 10000 and 14999 then 'avg low'
			when a.miles_per_year between 15000 and 19999 then 'avg hi'
			when a.miles_per_year between 20000 and 24999 then 'hi'
			when a.miles_per_year > 24999 then 'freaky hi'
		end as miles_category	
	from trades_1 a
	join veh.shape_size_classifications b on a.make = b.make
		and a.model = b.model) c
group by make, model, shape, size, miles_category
order by count(*) desc	



-- fooling around with make model
select make, model, shape_size, count(*)
from (
select year, make, model, shape_size, age_in_years_at_trade_2, value as miles, miles_per_year,
	case
		when miles_per_year < 5000 then 'freaky low'
		when miles_per_year between 5000 and 9999 then 'low'
		when miles_per_year between 10000 and 14999 then 'avg low'
		when miles_per_year between 15000 and 19999 then 'avg hi'
		when miles_per_year between 20000 and 24999 then 'hi'
		when miles_per_year > 24999 then 'freaky hi'
	end as miles_category		
from (  		
	select a.*, f.value, b.shape||'-'||b.size as shape_size, round(f.value/case when a.age_in_years_at_trade_2 = 0 then 1 else a.age_in_years_at_trade_2 end, 0) as miles_per_year
	from trades_1 a 
	join veh.shape_size_classifications b on a.make = b.make
		and a.model = b.model
	join ads.ext_vehicle_inventory_items d on a.stock_ = d.stocknumber
	join ads.ext_vehicle_evaluations e on d.vehicleinventoryitemid = e.vehicleinventoryitemid
	join ads.ext_vehicle_item_mileages f on e.vehicleevaluationid = f.tablekey  
		and f.value > 100
	where store = 'RY1') x) xx
group by make, model, shape_size
order by count(*) desc


-- generate a date range for mileage
select *, coalesce(lead(the_date - 1, 1) over (partition by vin order by the_date), '12/31/9999')
from (
select b.vin, a.vehicleitemmileagets::date as the_date,max(value) as miles
from ads.ext_vehicle_item_mileages a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
where b.vin in ('3GTU2VEC8EG557267','3GTU2VEJ0EG271119','3GTU9DED4KG191690','3GYFNGE36DS581553','4S6DM58W8Y4403789','4T1BF1FKXCU092457')
group by b.vin, a.vehicleitemmileagets::date) x
order by vin, the_date


-- 06/02/21
-- add trim, trying to get peg vehicles from trades
select * from trades_1 limit 100

select a.make, a.model, a.age_in_years_at_Trade_2::integer as age, b.trim, count(*)
from trades_1 a
left join ads.ext_vehicle_items b on a.vin = b.vin
group by a.make, a.model, a.age_in_years_at_Trade_2::integer, b.trim
order by count(*) desc 



