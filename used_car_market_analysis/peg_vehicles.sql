﻿trying to define "peg" vehicles, similar to glumps
key make/model/age configurations for which we need to track 3rd party valuation history
trades
purchaes
essentially all acquisitions
retail sales

best price and acv by day


-- uc retail sales 2021
*************************************************** TO DO ****************************************************************
-- this table, pac.variable_daily_unit_counts, was devleoped in ...postgresq\pacing\04-04.sql
-- initially populated thru 202103
-- when jeri is finished with the statement, update thru 202105
*************************************************** TO DO ****************************************************************

-- 7065 vehicles
drop table if exists jon.peg_accounting cascade;
create unlogged table jon.peg_accounting as
select a.store, a.control, a.the_date  as sale_date
-- select * 	
from pac.variable_daily_unit_counts a
where a.page = 16
  and a.line between 1 and 5
  and a.year_month > 201812
  and control not in ( -- exclude backons
		select control
		from pac.variable_daily_unit_counts
		where page = 16
			and line between 1 and 5
			and year_month > 201812
		group by control having count(*) > 1);    
alter table jon.peg_accounting add primary key(control);



-- vehicles with no inpmast record of stock_number
-- but can fill in from tool data
-- 7051
drop table if exists jon.peg_ymmt cascade;
create unlogged table jon.peg_ymmt as
select * 
from (
	select a.*, coalesce(b.inpmast_vin, d.vin) as vin, coalesce(b.year, d.yearmodel::integer) as model_year,
		coalesce(b.make, d.make) as make, coalesce(b.model, d.model) as model,
		coalesce(d.trim, b.body_Style) as the_trim,
		c.vehicleinventoryitemid
	-- select * 	
	from jon.peg_accounting a
	left join arkona.ext_inpmast b on a.control = b.inpmast_stock_number
	left join ads.ext_vehicle_inventory_items c on a.control = c.stocknumber
	left join ads.ext_vehicle_items d on c.vehicleitemid = d.vehicleitemid) x 
where model is not null  -- excludes 10 vehicles, 9 from 2019, 1 from 2020
  and control <> 'H12505A';  -- 1 dup
alter table jon.peg_ymmt add primary key(control);

select make, model, the_trim, count(*)
from jon.peg_ymmt
group by make, model, the_trim
order by count(*) desc

select make, model, the_trim, count(*)
from jon.peg_ymmt
where model = 'SILVERADO 1500'
  and the_trim in ('LT','LTZ')
group by make, model, the_trim
order by count(*) desc

select *
from jon.peg_ymmt
where model = 'SILVERADO 1500'
  and the_trim in ('LT','LTZ')
  and sale_date > '01/01/2021'  
order by sale_date

select a.*, coalesce(b.selectedacv, c.selectedacv) as acv, ee.best_price
from jon.peg_ymmt a
left join ads.ext_acvs b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and b.basistable = 'vehiclewalks'
left join ads.ext_acvs c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
  and b.basistable = ('vehicleevaluations')  
left join (
	select cc.vehicleinventoryitemid, dd.amount::integer as best_price
	from (
		select distinct aa.vehiclepricingid, aa.vehicleinventoryitemid 
		from ads.ext_vehicle_pricings aa
		join (
			select a.vehicleinventoryitemid, max(a.vehiclepricingts) as vehiclepricingts
			from ads.ext_vehicle_pricings a
			join jon.peg_ymmt b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
				and b.model = 'SILVERADO 1500'
				and b.the_trim in ('LT','LTZ')
				and b.sale_date > '01/01/2021'  
			group by a.vehicleinventoryitemid) bb on aa.vehicleinventoryitemid = bb.vehicleinventoryitemid
				and aa.vehiclepricingts = bb.vehiclepricingts) cc
	left join ads.ext_vehicle_pricing_details dd on cc.vehiclepricingid = dd.vehiclepricingid
		and dd.typ = 'VehiclePricingDetail_BestPrice') ee on a.vehicleinventoryitemid = ee.vehicleinventoryitemid
where a.model = 'SILVERADO 1500'
  and a.the_trim in ('LT','LTZ')
  and a.sale_date > '01/01/2021'  
order by a.sale_date



--< peg candidates --------------------------------------------------------------
select ((sale_date - to_date('09'||'01'||(model_year - 1)::text,'MMDDYYYY'))/365.0)::integer as age, count(*) 
from jon.peg_ymmt
group by ((sale_date - to_date('09'||'01'||(model_year - 1)::text,'MMDDYYYY'))/365.0)::integer
order by count(*) desc
limit 40

select ((sale_date - to_date('09'||'01'||(model_year - 1)::text,'MMDDYYYY'))/365.0)::integer as age, make, count(*) 
from jon.peg_ymmt
group by ((sale_date - to_date('09'||'01'||(model_year - 1)::text,'MMDDYYYY'))/365.0)::integer, make
order by count(*) desc
limit 40

select make, model, count(*) 
from jon.peg_ymmt
group by make, model
order by count(*) desc
limit 40

select ((sale_date - to_date('09'||'01'||(model_year - 1)::text,'MMDDYYYY'))/365.0)::integer as age, make, model, count(*) 
from jon.peg_ymmt
group by ((sale_date - to_date('09'||'01'||(model_year - 1)::text,'MMDDYYYY'))/365.0)::integer, make, model
order by count(*) desc
limit 40

select make, model, the_trim, count(*) 
from jon.peg_ymmt
group by make, model, the_trim
order by count(*) desc
limit 40

select ((sale_date - to_date('09'||'01'||(model_year - 1)::text,'MMDDYYYY'))/365.0)::integer as age, make, model, the_trim, count(*) 
from jon.peg_ymmt
group by((sale_date - to_date('09'||'01'||(model_year - 1)::text,'MMDDYYYY'))/365.0)::integer, make, model, the_trim
order by count(*) desc
limit 40

--/> peg candidates --------------------------------------------------------------


so what i am going to want for acquisitions is date, year, make, model, acv, first best price
-- 
-- purchases
select distinct d.fromts::date as date_acquired, e.yearmodel::integer as model_year, e.make, 
	e.model, e.trim, g.selectedacv as acv, f.best_price
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.the_year > 2018 
join fin.dim_journal c on a.journal_key = c.journal_key
  and c.journal_code = 'PVU'  
left join ads.ext_vehicle_inventory_items d on a.control = d.stocknumber
left join ads.ext_vehicle_items e on d.vehicleitemid = e.vehicleitemid  
left join ads.xfm_vehicle_pricings f on d.vehicleinventoryitemid = f.vehicleinventoryitemid
  and f.seq = 1
left join ads.ext_acvs g on f.vehicleinventoryitemid = g.vehicleinventoryitemid
where a.post_status = 'Y'
  and right(a.control, 1) in ('K','L','M','P','T','X')
  and left(a.control, 1) in ('H','G')
  and d.fromts::date between '01/01/2021' and '05/31/2021'
  and e.model = 'silverado 1500'
  and e.trim in ('LT','LTZ')
union
-- trades
-- created and populated in ...used_car_market_analysis/trades.sql
select a.date_acquired, a.model_year, a.make, a.model, b.trim, a.acv, d.best_price
-- select a.*
from jon.peg_trades a
left join ads.ext_vehicle_items b on a.vin = b.vin
left join ads.ext_vehicle_inventory_items c on a.stock_number = c.stocknumber
left join ads.xfm_vehicle_pricings d on c.vehicleinventoryitemid = d.vehicleinventoryitemid
  and d.seq = 1
where a.model = 'silverado 1500'
  and b.trim in ('LT','LTZ')
  and a.model_year between 2017 and 2020
  and date_acquired between '01/01/2021' and '05/31/2021';
  








-------------------------------------------------------------------------------------
--< vehicle pricing
-------------------------------------------------------------------------------------
-- need some pricing etl
-- specifically ads.ext_vehicle_pricings, xfm the ts into from/thru
select * 
from ads.ext_vehicle_pricings
limit 100

1. 1 pricing per vehicle per day

-- don't care who priced it

select vehicleinventoryitemid, max(vehiclepricingts) as vehiclepricingts
from ads.ext_vehicle_pricings
where vehiclepricingts::date > '12/31/2018'
group by vehicleinventoryitemid


select a.vehiclepricingid, a.vehicleinventoryitemid, a.vehiclepricingts::date as date_priced, a.basistable, a.tablekey,  a.vehicleitemid
from ads.ext_vehicle_pricings a
join ( -- max pricing ts for each vehicle for each day it is priced, so group on viiid and the date
	select vehicleinventoryitemid, vehiclepricingts::date as the_date, max(vehiclepricingts) as vehiclepricingts
	from ads.ext_vehicle_pricings
	where vehiclepricingts::date > '12/31/2018'
	group by vehicleinventoryitemid, vehiclepricingts::date) b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
		and a.vehiclepricingts = b.vehiclepricingts
order by a.vehicleinventoryitemid



-- test on viid 99c0dc86-dc78-4dc0-b995-b9974e6a2ce0 : 15 pricings
-- yep, looks good
select x.*, coalesce(lead(date_priced - 1, 1) over (partition by vehicleinventoryitemid order by date_priced), '12/31/9999'), y.amount::integer as best_price
from (
	select a.vehiclepricingid, a.vehicleinventoryitemid, a.vehiclepricingts::date as date_priced, a.basistable, a.tablekey,  a.vehicleitemid
	from ads.ext_vehicle_pricings a
	join ( -- max pricing ts for each vehicle for each day it is priced, so group on viiid and the date
		select vehicleinventoryitemid, vehiclepricingts::date as the_date, max(vehiclepricingts) as vehiclepricingts
		from ads.ext_vehicle_pricings
		where vehiclepricingts::date > '12/31/2018' and vehicleinventoryitemid = '99c0dc86-dc78-4dc0-b995-b9974e6a2ce0'
		group by vehicleinventoryitemid, vehiclepricingts::date) b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
			and a.vehiclepricingts = b.vehiclepricingts) x
left join ads.ext_vehicle_pricing_details y on x.vehiclepricingid = y.vehiclepricingid
  and y.typ = 'VehiclePricingDetail_BestPrice'			
order by x.date_priced


drop table if exists ads.xfm_vehicle_pricings cascade;
create table ads.xfm_vehicle_pricings (
  vehicleinventoryitemid citext not null,
  vehicleitemid citext not null,
  from_date date not null,
  thru_date date not null,
  best_price integer not null,
  seq integer not null,
  primary key(vehicleinventoryitemid,from_date));
comment on table ads.xfm_vehicle_pricings is 'combine ads.ext_vehicle_pricings with ads.ext_vehicle_pricing_details into a single talbe
	that has the pricing history for each vehicle inventory item, currently only has data from 1/1/2019 thru 6/2/2021 to satisfy the 
	immediate need in doing some market analysis';
	
insert into ads.xfm_vehicle_pricings
select x.vehicleinventoryitemid, vehicleitemid, date_priced as from_date, 
	coalesce(lead(date_priced - 1, 1) over (partition by vehicleinventoryitemid order by date_priced), '12/31/9999') as thru_date, 
	y.amount::integer as best_price,
	row_number() over (partition by vehicleinventoryitemid order by date_priced) as seq
from (
	select a.vehiclepricingid, a.vehicleinventoryitemid, a.vehiclepricingts::date as date_priced, a.basistable, a.tablekey,  a.vehicleitemid
	from ads.ext_vehicle_pricings a
	join ( -- max pricing ts for each vehicle for each day it is priced, so group on viiid and the date
		select vehicleinventoryitemid, vehiclepricingts::date as the_date, max(vehiclepricingts) as vehiclepricingts
		from ads.ext_vehicle_pricings
		where vehiclepricingts::date > '12/31/2018' 
		group by vehicleinventoryitemid, vehiclepricingts::date) b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
			and a.vehiclepricingts = b.vehiclepricingts) x
left join ads.ext_vehicle_pricing_details y on x.vehiclepricingid = y.vehiclepricingid
  and y.typ = 'VehiclePricingDetail_BestPrice';	

select a.*, b.selectedacv as acv 
from ads.xfm_vehicle_pricings a
left join ads.ext_acvs b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and b.basistable = 'VehicleEvaluations'
where a.vehicleinventoryitemid = '99c0dc86-dc78-4dc0-b995-b9974e6a2ce0'
  AND a.seq = 1
  

-------------------------------------------------------------------------------------
--/> vehicle pricing
-------------------------------------------------------------------------------------



-- new silverados, 8 or 6 cylinder
-- select count(*), 
--   count(*) filter (where  s.engine->>'cylinders' = '8') as "8",
--   count(*) filter (where  s.engine->>'cylinders' = '6') as "6"
-- -- select * 	
-- from pac.variable_daily_unit_counts a
-- left join nc.vehicle_sales b on a.control = b.stock_number
-- left join chr.describe_Vehicle c on b.vin = c.vin
-- left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
--   and s.engine ? 'installed'
-- where a.page = 5
--   and a.line = 31
--   and a.year_month > 201812


