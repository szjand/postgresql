﻿select *
from arkona.ext_pyhscdta
limit 10


select *
from arkona.ext_pypcodes
where ded_pay_code like 'c%'



select *
from arkona.ext_pyhscdta a
join arkona.ext_pypcodes b on a.code_id = b.ded_pay_code
  and b.ded_pay_code in ('c19','cov')
  
order by a.employee_number


select *
from arkona.ext_pymast
where pymast_employee_number = '178050'


select *
from fin.dim_account
where account = '124704'


select *
from arkona.ext_pyptbdta

select *
from arkona.ext_pyhshdta
where employee_ = '178050'
  and check_year = 20
  and check_month = 11
  and check_day = 13

select *
from arkona.ext_pyhshdta
where employee_ = '178050'
  and payroll_run_number = 1113200
  

(('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date 

select count(*) from arkona.ext_pyhscdta where code_id in ('c19','cov') -- 142

-- this comes up about 40 rows short
select a.payroll_run_number, b.employee_name, a.employee_number, a.code_id, 
  (('20'||b.check_year)::text||'-'||b.check_month::text||'-'||b.check_day::text)::date as check_date,
  a.amount
from arkona.ext_pyhscdta a
join arkona.ext_pyhshdta b on a.payroll_run_number = b.payroll_run_number
  and a.employee_number = b.employee_
join fin.fact_gl c on a.employee_number = c.control
  and a.amount = c.amount
join dds.dim_date d on (('20'||b.check_year)::text||'-'||b.check_month::text||'-'||b.check_day::text)::date = d.the_date
  and c.date_key = d.date_key 
where a.code_id in ('c19','cov')

-- so lets see if we can see what is happening
drop table if exists tem.employee_gl cascade;
create table tem.employee_gl as
select a.trans, a.seq, b.the_date, a.control, a.ref, c.account, a.amount
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.the_year = 2020
join fin.dim_account c on a.account_key = c.account_key
  and c.description not like '%abs%'  -- single day of covid pay = single day of  holiday pay 197643:7/10/20
join arkona.ext_pymast d on a.control = d.pymast_employee_number
where a.post_status = 'Y';
create index on tem.employee_gl(control);
create index on tem.employee_gl(amount);
create index on tem.employee_gl(the_date);
create index on tem.employee_gl(ref);

select a.payroll_run_number, b.employee_name, a.employee_number, a.code_id, 
  (('20'||b.check_year)::text||'-'||b.check_month::text||'-'||b.check_day::text)::date as check_date,
  a.amount
from arkona.ext_pyhscdta a
join arkona.ext_pyhshdta b on a.payroll_run_number = b.payroll_run_number
  and a.employee_number = b.employee_
join arkona.ext_pymast c on a.employee_number = c.pymast_employee_number
where a.code_id in ('c19','cov')



select a.payroll_run_number, b.employee_name, a.employee_number, a.code_id, 
  (('20'||b.check_year)::text||'-'||b.check_month::text||'-'||b.check_day::text)::date as check_date,
  a.amount, c.*
from arkona.ext_pyhscdta a
join arkona.ext_pyhshdta b on a.payroll_run_number = b.payroll_run_number
  and a.employee_number = b.employee_
left join tem.employee_gl c on a.employee_number = c.control
  and a.amount = c.amount
  and (('20'||b.check_year)::text||'-'||b.check_month::text||'-'||b.check_day::text)::date = c.the_date
  and b.reference_check_::citext = c.ref
where a.code_id in ('c19','cov')
order by check_date desc


select *
from arkona.ext_pyhshdta
where employee_ = '197643'
  and check_year = 20
  and check_month = 7
  and check_day = 10

select a.* from fin.fact_gl a join dds.dim_date b on a.date_key = b.date_key and b.the_Date = '08/07/2020' where control = '196452' order by amount

select * from fin.fact_gl a where control = '1147810' and ref = '98534' order by amount

select * from fin.dim_account where account in ('12404','12304D')

select *
from arkona.ext_pyhscdta a
where code_id in ('c19','cov')
  and employee_number = '143769'

select * from arkona.ext_pymast limit 2

select * from arkona.ext_pyactgr where dist_code = 'soff'

-- this looks good
drop table if exists tem.covid_pay_detail;
create table tem.covid_pay_detail as
select a.payroll_run_number, b.employee_name, a.employee_number, a.code_id, 
  (('20'||b.check_year)::text||'-'||b.check_month::text||'-'||b.check_day::text)::date as check_date,
  a.amount as total_amount, d.dist_code, d.gross_dist, d.gross_expense_act_,  round(a.amount * d.gross_dist/100, 2) as amount,
  case
    when (('20'||b.check_year)::text||'-'||b.check_month::text||'-'||b.check_day::text)::date between '01/01/2020' and '03/31/2020' then 1
    when (('20'||b.check_year)::text||'-'||b.check_month::text||'-'||b.check_day::text)::date between '04/01/2020' and '06/30/2020' then 2
    when (('20'||b.check_year)::text||'-'||b.check_month::text||'-'||b.check_day::text)::date between '07/01/2020' and '09/30/2020' then 3
    when (('20'||b.check_year)::text||'-'||b.check_month::text||'-'||b.check_day::text)::date between '10/01/2020' and '12/31/2020' then 4
  end as quarter
from arkona.ext_pyhscdta a
join arkona.ext_pyhshdta b on a.payroll_run_number = b.payroll_run_number
  and a.employee_number = b.employee_
join arkona.ext_pymast c on a.employee_number = c.pymast_employee_number
left join arkona.ext_pyactgr d on c.distrib_code = d.dist_code
  and c.pymast_company_number = d.company_number
where a.code_id in ('c19','cov') 
order by b.employee_name, quarter;


select quarter, employee_number, employee_name, gross_expense_act_ as account, sum(amount) as amount
-- select * 
from tem.covid_pay_detail
group by quarter, employee_number, employee_name, gross_expense_act_
order by quarter, employee_name


