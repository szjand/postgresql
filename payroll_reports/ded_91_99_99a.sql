﻿/*
8/6/19
Jon can you pull a report with the 401k percentage that employees 
are taking out of payroll they would be in field 91,  99,  and 99a

So we would want name, store and  field 91   99 and 99a

*/

-- requires updating pydeduct

select a.pymast_company_number as store, a.employee_name as name, -- pymast_employee_number,
  max(case when ded_pay_code = '91' then fixed_ded_amt end) as "91",
  max(case when ded_pay_code = '99' then fixed_ded_amt end) as "99",
  max(case when ded_pay_code = '99a' then fixed_ded_amt end) as "99a"
from arkona.xfm_pymast a
join arkona.ext_pydeduct b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
  and b.ded_pay_code in ('91','99','99a')
-- left join arkona.ext_pypcodes c on b.company_number = c.company_number and b.ded_pay_code = c.ded_pay_code  
where a.current_row
  and a.active_code <> 'T'
  and a.pymast_company_number in ('RY1','RY2')
--   and a.pymast_employee_number = '138380'
group by a.pymast_company_number, a.employee_name --, pymast_employee_number  
order by a.employee_name
