﻿/*
01/16/2020
from kim
need store, name, ded amount for anyone employed in 2019 with the amount, if any
*/
select a.pymast_company_number as store, a.employee_name as name, b.fixed_Ded_amt as ded_amt
from arkona.xfm_pymast a
join arkona.ext_pydeduct b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
  and b.ded_pay_code = '107'
left join arkona.ext_pypcodes c on b.company_number = c.company_number and b.ded_pay_code = c.ded_pay_code  
where a.current_row
  and a.pymast_company_number in ('RY1','RY2')
  and dds.db2_integer_to_date(hire_date) < '12/31/2019'
  and dds.db2_integer_to_date(termination_date) > '01/01/2019'
order by a.pymast_company_number, a.employee_name


