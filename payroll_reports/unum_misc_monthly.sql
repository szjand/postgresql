﻿/*
04/05/21
move the active query to the top of the page, here

all below is history, kim has changed the deductions she wants them to be
DENTAL (111)
STD (120)
LTD (121)
LIFE (122)
ACCIDENT (123)
HOSPITAL (124)
CRITICAL (125)
VISION (295)


this is how kim requests it: HI Jon can I get the report for the deductions in May

*/

select * from arkona.ext_pyhshdta where payroll_cen_year = 121 and employee_ = '17534' order by payroll_run_number desc limit 10

the thing to remember is, if kim asks for it on the first working doay of the month, ext_pyhshdta & ext_pyhscdta must be rerun

do $$
declare
  _year integer := 21;  -------------------------------------
  _month integer := 12; -------------------------------------
begin  
drop table if exists unum cascade;
create temp table unum as
select a.company_number as "STORE", a.employee_name as "NAME", c.ssn,
  sum(amount) filter (where b.code_id = '111') as "DENTAL (111)",
  sum(amount) filter (where b.code_id = '120') as "STD (120)",
  sum(amount) filter (where b.code_id = '121') as "LTD (121)",
  sum(amount) filter (where b.code_id = '122') as "LIFE (122)",
  sum(amount) filter (where b.code_id = '123') as "ACCIDENT (123)",
  sum(amount) filter (where b.code_id = '124') as "HOSPITAL (124)",
  sum(amount) filter (where b.code_id = '125') as "CRITICAL (125)",
  sum(amount) filter (where b.code_id = '295') as "VISION (295)"
from (
  select a.company_number, a.employee_name, payroll_run_number, employee_, payroll_cen_year
  from arkona.ext_pyhshdta a
  where a.check_year = _year
    and a.check_month = _month
  group by a.company_number, a.employee_name, payroll_run_number, employee_, payroll_cen_year) a
join arkona.ext_pyhscdta b on a.company_number = b.company_number
  and a.payroll_cen_year = b.payroll_cen_year
  and a.payroll_run_number = b.payroll_run_number
  and a.employee_ = b.employee_number
  and b.code_id in ('111','120','121','122','123','124','125','295') 
left join jon.ssn c on a.employee_ = c.employee_number
where a.company_number in ('RY1','RY2')  
group by a.company_number, a.employee_name, c.ssn  
order by a.company_number, a.employee_name;
end  
$$;
select * from unum;

-- /*
-- 6/7/19
-- Could I get a spreadsheet  with store, name, and deduction codes  123 ,  124,  125,  126.  
-- I would like this report to be what I take out of their payroll each month.
-- so like in June I will take money from 12b (6-14-91)  13b (6/28/19)  and 12s (06/30/19)
-- so if I could save that to a file that I can upload to Unum
-- 
-- 6/14/19
-- what she is actually looking for is a spreadsheet to send to unum with what was paid per employee per deduction
-- */
-- 
-- select a.pymast_company_number, a.employee_name, pymast_employee_number, a.pay_period ,
--   b.code_type, b.ded_pay_code, b.fixed_Ded_amt, b.ded_freq,
--   c.description
-- from arkona.xfm_pymast a
-- join arkona.ext_pydeduct b on a.pymast_employee_number = b.employee_number
--   and a.pymast_company_number = b.company_number
--   and b.ded_pay_code in ('123','124','125','126')
-- left join arkona.ext_pypcodes c on b.company_number = c.company_number and b.ded_pay_code = c.ded_pay_code  
-- where a.current_row
--   and a.active_code <> 'T'
--   and a.pymast_company_number in ('RY1','RY2')
-- --   and a.pymast_employee_number = '138380'
-- order by a.employee_name
-- 
-- 
-- select * 
-- from arkona.ext_pypcodes
-- where ded_pay_code in ('123','124','125','126')
-- 
-- 
-- select a.pymast_company_number as store, a.employee_name as name, -- pymast_employee_number,
--   max(case when ded_pay_code = '123' then fixed_ded_amt end) as "123 Amount",
--   max(case when ded_pay_code = '123' then ded_freq end) as "123 Freq",
--   max(case when ded_pay_code = '124' then fixed_ded_amt end) as "124 Amount",
--   max(case when ded_pay_code = '124' then ded_freq end) as "124 Freq",
--   max(case when ded_pay_code = '125' then fixed_ded_amt end) as "125 Amount",
--   max(case when ded_pay_code = '125' then ded_freq end) as "125 Freq",
--   max(case when ded_pay_code = '126' then fixed_ded_amt end) as "126 Amount",
--   max(case when ded_pay_code = '126' then ded_freq end) as "126 Freq"
-- from arkona.xfm_pymast a
-- join arkona.ext_pydeduct b on a.pymast_employee_number = b.employee_number
--   and a.pymast_company_number = b.company_number
--   and b.ded_pay_code in ('123','124','125','126')
-- -- left join arkona.ext_pypcodes c on b.company_number = c.company_number and b.ded_pay_code = c.ded_pay_code  
-- where a.current_row
--   and a.active_code <> 'T'
--   and a.pymast_company_number in ('RY1','RY2')
-- --   and a.pymast_employee_number = '138380'
-- group by a.pymast_company_number, a.employee_name --, pymast_employee_number  
-- 
-- 
-- -----------------------------------------------------------------------------------
-- -- 6/14/19
-- 
-- -- Jon  This unum reports looks good   and we can name it unum misc monthly
-- 
-- 
-- -- create or replace function nrv.get_payroll_report_unum_misc_monthly(_year integer, _month integer)
-- -- returns table (store_code citext,name citext,"GROUP ACCIDENT (123)" numeric(8,2),
-- --   "GROUP HOSPITAL (124)" numeric(8,2), "CRITICAL ILL/CA (124)" numeric(8,2),
-- --   "WHOLE LIFE INS (126)" numeric(8,2)) as
-- -- $BODY$  
-- -- /*
-- -- all required arkona table extracts done in luigi
-- -- select * from nrv.get_payroll_report_unum_misc_monthly(19, 8)
-- -- */
-- -- *** REPLACED ON 9/20/19 ***
-- -- select a.company_number as store_code, a.employee_name as name,
-- --   sum(amount) filter (where b.code_id = '123') as "GROUP ACCIDENT (123)",
-- --   sum(amount) filter (where b.code_id = '124') as "GROUP HOSPITAL (124)",
-- --   sum(amount) filter (where b.code_id = '125') as "CRITICAL ILL/CA (125)",
-- --   sum(amount) filter (where b.code_id = '126') as "WHOLE LIFE INS (126)"
-- -- from arkona.ext_pyhshdta a
-- -- join arkona.ext_pyhscdta b on a.company_number = b.company_number
-- --   and a.payroll_cen_year = b.payroll_cen_year
-- --   and a.payroll_run_number = b.payroll_run_number
-- --   and a.employee_ = b.employee_number
-- --   and b.code_id in ('123','124','125','126')
-- -- join arkona.ext_pypcodes c on a.company_number = c.company_number
-- --   and b.code_id = c.ded_pay_code  
-- -- where a.check_year = _year
-- --   and a.check_month = _month
-- -- group by a.check_year, a.check_month, a.company_number, a.employee_name
-- -- order by a.company_number, a.employee_name;
-- -- $BODY$  
-- -- language sql;
-- -- comment on function nrv.get_payroll_report_unum_misc_monthly(integer,integer) is 'generates a payroll report called
-- -- Unum Misc Monthly which eventually will be accessible through a Vision page, shows Unum deductions
-- -- for all employees for a given year/month.  All required arkona tables are extracted nightly in luigi';
-- 
-- 
-- /*
-- 
-- 9/5/19
-- 
-- Thanks Jon  can you look at Seth Heck for accident it shows 29.88 and 
-- critical 14.40 . on my print out from payroll it is 14.94 and  7.20 which is correct
-- */
-- 
-- select a.company_number as store_code, a.employee_name as name,
--   sum(amount) filter (where b.code_id = '123') as "GROUP ACCIDENT (123)",
--   sum(amount) filter (where b.code_id = '124') as "GROUP HOSPITAL (124)",
--   sum(amount) filter (where b.code_id = '125') as "CRITICAL ILL/CA (125)",
--   sum(amount) filter (where b.code_id = '126') as "WHOLE LIFE INS (126)"
-- 
-- 
-- -- this shows 2 rows for seth & batch 831190
-- select a.company_number as store_code, a.employee_name as name, a.payroll_run_number
-- from arkona.ext_pyhshdta a
--   where a.check_year = 19
--   and a.check_month = 8
--   and a.employee_name like '%heck%'
-- 
-- -- this is the problem
-- seth got 2 checks in the same batch
-- chech#
-- 84420 831190
-- 84419 831190
-- 
-- 
-- select * from arkona.ext_pyhscdta where payroll_run_number = 831190 and employee_number = '165789'
-- to kim:
-- That happened because seth was issued 2 checks, 84419 & 84420 in the payroll batch 831190.
-- Is that an unusual situation?
-- 
-- kims response:
-- Yes.   Would it happen if I had a special payroll for someone that has the insurance
-- 
-- which makes no sense to me
-- 
-- -- 
-- select payroll_run_number, employee_, string_agg(reference_check_::text, ',')
-- -- select *
-- from arkona.ext_pyhshdta 
-- where payroll_Cen_year = 119
--   and payroll_run_number = 831190
-- --   and company_number = 'RY1'
-- group by payroll_run_number, employee_  
-- 
-- -- 9/18/19
-- i susequently spoke with kim, and got agreement that having multiple checks to the same person in the 
-- same batch is unusual
-- 
-- -- need to come up with a test, maybe, do i want a test or simply return single set of deductions per payroll batch
-- 
-- -- not exactly rare
-- select check_year, check_month, payroll_run_number, employee_
-- from (
-- select check_year, check_month, payroll_run_number, reference_check_, employee_
-- from arkona.ext_pyhshdta
-- group by check_year, check_month, payroll_run_number, reference_check_, employee_) a
-- group by check_year, check_month, payroll_run_number, employee_
-- having count(*) > 1
-- order by check_year, check_month
-- 
-- select *
-- from arkona.ext_pyhshdta
-- where check_year = 19
--   and check_month = 8
--   and payroll_run_number = 809190
--   and employee_ = '1130426'
-- 
-- 
-- -- thinking query on pyhshdta needs to be a group based on payroll run number
-- 
-- drop table if exists hdta;
-- create temp table hdta as
-- select a.company_number, a.employee_name, payroll_run_number, employee_, payroll_cen_year
-- from arkona.ext_pyhshdta a
-- where a.check_year = 19
--   and a.check_month = 8
-- group by a.company_number, a.employee_name, payroll_run_number, employee_, payroll_cen_year;
-- create unique index on hdta(payroll_run_number, employee_);
-- 
-- create temp table the_new as
-- select a.company_number, a.employee_name,
--   sum(amount) filter (where b.code_id = '123') as "GROUP ACCIDENT (123)",
--   sum(amount) filter (where b.code_id = '124') as "GROUP HOSPITAL (124)",
--   sum(amount) filter (where b.code_id = '125') as "CRITICAL ILL/CA (125)",
--   sum(amount) filter (where b.code_id = '126') as "WHOLE LIFE INS (126)"
-- from hdta a
-- join arkona.ext_pyhscdta b on a.company_number = b.company_number
--   and a.payroll_cen_year = b.payroll_cen_year
--   and a.payroll_run_number = b.payroll_run_number
--   and a.employee_ = b.employee_number
--   and b.code_id in ('123','124','125','126') 
-- group by a.company_number, a.employee_name  
-- order by a.company_number, a.employee_name;
-- 
-- create temp table the_old as
-- select a.company_number as store_code, a.employee_name as name,
--   sum(amount) filter (where b.code_id = '123') as "GROUP ACCIDENT (123)",
--   sum(amount) filter (where b.code_id = '124') as "GROUP HOSPITAL (124)",
--   sum(amount) filter (where b.code_id = '125') as "CRITICAL ILL/CA (125)",
--   sum(amount) filter (where b.code_id = '126') as "WHOLE LIFE INS (126)"
-- from arkona.ext_pyhshdta a
-- join arkona.ext_pyhscdta b on a.company_number = b.company_number
--   and a.payroll_cen_year = b.payroll_cen_year
--   and a.payroll_run_number = b.payroll_run_number
--   and a.employee_ = b.employee_number
--   and b.code_id in ('123','124','125','126')
-- join arkona.ext_pypcodes c on a.company_number = c.company_number
--   and b.code_id = c.ded_pay_code  
-- where a.check_year = 19
--   and a.check_month = 8
-- group by a.check_year, a.check_month, a.company_number, a.employee_name
-- order by a.company_number, a.employee_name;
-- 
-- -- good enough for me, this is proof that the new method takes care of the heck 
-- -- anomaly and does not introduce any new ones
-- select *
-- from (
-- select company_number, employee_name, md5(a::text) as hash
-- from the_new a) aa
-- join (
-- select store_code, name, md5(b::text) as hash
-- from the_old b) bb on aa.company_number = bb.store_code and aa.employee_name = bb.name and aa.hash <> bb.hash
-- 
-- select company_number, employee_name, md5(a::text) as hash
-- from the_new a
-- except
-- select store_code, name, md5(b::text) as hash
-- from the_old b
-- 
-- select store_code, name, md5(b::text) as hash
-- from the_old b
-- except
-- select company_number, employee_name, md5(a::text) as hash
-- from the_new a
-- 
-- -- 9/20/19 this is the final version
-- --    subquery on pyhshdta resolves the multiple check per person/payroll_run_number issue
-- --    exposed by seth heck in august 2019
-- 
-- ****************
-- ****************
-- 
-- 10/1/19 kim wanted the report today, she did a payroll today dated 9/30
-- so ext_pyhshdta & ext_pyhscdta did not have the data she entered today
-- had to manually update those tables
-- 
-- damnit
-- 12/2/19
-- same thing
-- of course dummy, when she asks for the report on the first work day of the month
-- she will want the monthly payroll done that day (sales) included
-- ****************
-- ****************
-- 
-- select *
-- from arkona.ext_pyhshdta where check_year = 20 and check_month = 10
-- order by check_day desc
-- limit 10
-- -- drop function nrv.get_payroll_report_unum_misc_monthly(integer,integer);
-- 
-- create or replace function nrv.get_payroll_report_unum_misc_monthly(_year integer, _month integer)
-- returns table (store_code citext,name citext,ssn citext,"GROUP ACCIDENT (123)" numeric(8,2),
--   "GROUP HOSPITAL (124)" numeric(8,2), "CRITICAL ILL/CA (124)" numeric(8,2),
--   "WHOLE LIFE INS (126)" numeric(8,2)) as
-- $BODY$  
-- /*
-- all required arkona table extracts done in luigi
-- arkona.ext_pyhscdta & arkona.ext_pyhshdta need to be updated real time for this report
-- 05/26/20: added ssn
-- select * from arkona.ext_pyhshdta where check_year = 21 and check_month = 4 and employee_name like 'aubol%'
-- 
-- select employee_name, check_day, total_gross_pay from arkona.ext_pyhshdta where check_year = 20 and check_month = 12 and check_day > 18 order by employee_name
-- 
-- 
-- 
-- select * from nrv.get_payroll_report_unum_misc_monthly(21, 4)
-- 
-- */
-- select a.company_number as "STORE", a.employee_name as "NAME", c.ssn,
--   sum(amount) filter (where b.code_id = '123') as "GROUP ACCIDENT (123)",
--   sum(amount) filter (where b.code_id = '124') as "GROUP HOSPITAL (124)",
--   sum(amount) filter (where b.code_id = '125') as "CRITICAL ILL/CA (125)",
--   sum(amount) filter (where b.code_id = '126') as "WHOLE LIFE INS (126)"
-- from (
--   select a.company_number, a.employee_name, payroll_run_number, employee_, payroll_cen_year
--   from arkona.ext_pyhshdta a
--   where a.check_year = _year
--     and a.check_month = _month
--   group by a.company_number, a.employee_name, payroll_run_number, employee_, payroll_cen_year) a
-- join arkona.ext_pyhscdta b on a.company_number = b.company_number
--   and a.payroll_cen_year = b.payroll_cen_year
--   and a.payroll_run_number = b.payroll_run_number
--   and a.employee_ = b.employee_number
--   and b.code_id in ('123','124','125','126') 
-- left join jon.ssn c on a.employee_ = c.employee_number
-- where a.company_number in ('RY1','RY2')  
-- group by a.company_number, a.employee_name, c.ssn  
-- order by a.company_number, a.employee_name;
-- $BODY$  
-- language sql;
-- comment on function nrv.get_payroll_report_unum_misc_monthly(integer,integer) is 'generates a payroll report called
-- Unum Misc Monthly which eventually will be accessible through a Vision page, shows Unum deductions
-- for all employees for a given year/month.  All required arkona tables are extracted nightly in luigi, Yet
-- another case where we need to exclude RY5';

