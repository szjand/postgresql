﻿/*
06/17/20
unum needs the W2 line 1 wages = total gross wages - tax exempt deductions
*/

/*
06/22/20
Hey Jon-

Are you able to add in status (FT,PT) to this?

Thanks!

Laura Roth 
*/

/*
08/12/20
Hi Jon-

I am working on a new census for the new broker that we are with for voluntary benefits.  
Could you re-run this current census for just full time employees and add in gender, zip code and job title?  
Everything else should be good the way it is.

Thanks!
Laura
*/

-- these are the codes for the relevant deductions
drop table if exists the_codes;
create temp table the_codes as
select distinct a.pymast_company_number as company_number, b.ded_pay_code, c.description, c.code_type
from arkona.ext_pymast a
left join arkona.ext_pydeduct b on trim(a.pymast_employee_number) = trim(b.employee_number)
  and b.code_type = '2'
left join arkona.ext_pypcodes c on b.company_number = c.company_number
  and trim(b.ded_pay_code) = trim(c.ded_pay_code)  
  and c.exempt_1_fed_tax_ = 'Y'
  and c.code_type = '2'
where a.pymast_company_number in ('RY1','RY2')
  and a.active_code <> 'T'
  and c.exempt_1_fed_tax_ = 'Y'
order by a.pymast_company_number, b.ded_pay_code;





-- this is the spreadsheet
-- basically needs to run once a year
-- the only thing to change is the payroll_cen_year value
-- 06/22/20 added full/part, formatted column headings
select a.pymast_company_number as store, a.pymast_employee_number as "emp #", a.employee_last_name as "last name", 
  a.employee_first_name as "first name", b.ssn, arkona.db2_integer_to_date(a.hire_date) as "hire date", 
  arkona.db2_integer_to_date(a.birth_date) as "birth date",
  case 
    when a.active_code = 'A' then 'FT'
    when a.active_code = 'P' then 'PT'
  end as status,  
  sum(total_gross_pay) as "total gross",
  min(d.deductions) as "tax exempt deductions",
--   sum(total_gross_pay) -  min(d.deductions) as ws_line_1
  case
    when min(deductions) is null then sum(total_gross_pay)
    else sum(total_gross_pay) -  min(d.deductions)
  end as "w2 line 1"
from arkona.xfm_pymast a
join jon.ssn b on a.pymast_employee_number = b.employee_number
left join arkona.ext_pyhshdta c on a.pymast_employee_number = c.employee_
  and c.payroll_cen_year = 119
left join (
  select employee_number, sum(a.amount) as deductions
  from arkona.ext_pyhscdta a
  join the_codes b on a.company_number = b.company_number
    and a.code_id = b.ded_pay_code
  where payroll_cen_year = 119
  group by employee_number) d on a.pymast_employee_number = d.employee_number
where a.current_row
  and a.active_code <> 'T'
  and a.pymast_company_number in ('RY1','RY2')
group by a.pymast_company_number, a.pymast_employee_number, a.employee_last_name, 
  a.employee_first_name, b.ssn, arkona.db2_integer_to_date(a.hire_date),
  arkona.db2_integer_to_date(a.birth_date), a.active_code
order by a.pymast_company_number, a.employee_last_name

-- 08/12/20
-- add gender, zip code, job title
-- FT only

select a.pymast_company_number as store, a.pymast_employee_number as "emp #", a.employee_last_name as "last name", 
  a.employee_first_name as "first name", f.data as job, a.sex as gender, a.zip_code, b.ssn, arkona.db2_integer_to_date(a.hire_date) as "hire date", 
  arkona.db2_integer_to_date(a.birth_date) as "birth date",
  case 
    when a.active_code = 'A' then 'FT'
    when a.active_code = 'P' then 'PT'
  end as status,  
  sum(total_gross_pay) as "total gross",
  min(d.deductions) as "tax exempt deductions",
--   sum(total_gross_pay) -  min(d.deductions) as ws_line_1
  case
    when min(deductions) is null then sum(total_gross_pay)
    else sum(total_gross_pay) -  min(d.deductions)
  end as "w2 line 1"
from arkona.xfm_pymast a
join jon.ssn b on a.pymast_employee_number = b.employee_number
left join arkona.ext_pyhshdta c on a.pymast_employee_number = c.employee_
  and c.payroll_cen_year = 119
left join (
  select employee_number, sum(a.amount) as deductions
  from arkona.ext_pyhscdta a
  join the_codes b on a.company_number = b.company_number
    and a.code_id = b.ded_pay_code
  where payroll_cen_year = 119
  group by employee_number) d on a.pymast_employee_number = d.employee_number
left join arkona.ext_pyprhead e on a.pymast_employee_number = e.employee_number
  and a.pymast_company_number = e.company_number
left join arkona.ext_pyprjobd f on e.company_number = f.company_number
  and e.job_description = f.job_description   
where a.current_row
  and a.active_code = 'A'  -- Full Time only
  and a.pymast_company_number in ('RY1','RY2')
group by a.pymast_company_number, a.pymast_employee_number, a.employee_last_name, 
  a.employee_first_name, f.data, a.sex, a.zip_code, b.ssn, arkona.db2_integer_to_date(a.hire_date),
  arkona.db2_integer_to_date(a.birth_date), a.active_code
order by a.pymast_company_number, a.employee_last_name
