﻿select a.pymast_company_number as store, a.pymast_employee_number as emp_number, a.employee_name as name, 
	a.payroll_class, a.base_salary as salary, a.base_hrly_rate as hourly_rate, b.fixed_Ded_amt as lease_amount
-- select *
from arkona.ext_pymast a  -- limit 5
left join arkona.ext_pydeduct b on a.pymast_employee_number = b.employee_number
  and b.ded_pay_code = '500'
  and arkona.db2_integer_to_date(b.bank_account) > current_date
where a.active_code <> 'T'  
  and a.pymast_company_number in ('RY1','RY2')
order by store, name


