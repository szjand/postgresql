﻿-- 1/7/20 this is what i sent to kim
-- 1/10/20 updated with the new deduction codes: 91B, 91C, 99B, 99C

/*
We did a report list the fields for the 401k for each employee   91 91b 91b 99 99a 99b 99c
Can I get that report
07/09/20
*/


select b.pymast_company_number as store, b.employee_last_name as last_name, 
  b.employee_first_name as first_name, b.pymast_employee_number as "emp #", 
  max(a.fixed_ded_amt) filter (where ded_pay_code = '91') as "91",
  max(a.fixed_ded_amt) filter (where ded_pay_code = '91B') as "91B",
  max(a.fixed_ded_amt) filter (where ded_pay_code = '91C') as "91C",
  max(a.fixed_ded_amt) filter (where ded_pay_code = '99') as "99",
  max(a.fixed_ded_amt) filter (where ded_pay_code = '99A') as "99A",
  max(a.fixed_ded_amt) filter (where ded_pay_code = '99B') as "99B",
  max(a.fixed_ded_amt) filter (where ded_pay_code = '99C') as "99C"
from arkona.ext_pydeduct a
join arkona.xfm_pymast b on a.employee_number = b.pymast_employee_number
  and b.current_row
  and b.active_code <> 'T'
where a.ded_pay_code in ('91','99','99a','99b','99c','91b','91c')         
group by b.pymast_company_number, b.pymast_employee_number, b.employee_first_name, b.employee_last_name
order by b.pymast_company_number, b.employee_last_name



