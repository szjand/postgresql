﻿Can you run me a report with:  store, name, code 107- medical ins (total for the year on each employee current and termed) 2021

Kim



select a.company_number as store, a.employee_number, b.employee_name as name, a.fixed_Ded_amt as lease_amount, arkona.db2_integer_to_date(bank_account) as end_date
from arkona.ext_pydeduct a
join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
where a.ded_pay_code = '107'
  and b.active_code <> 'T'
  and arkona.db2_integer_to_date(bank_account) > current_date



select a.company_number as store, a.employee_number, b.employee_name, a.code_id, sum(a.amount) 
from arkona.ext_pyhscdta a
left join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
where a.payroll_cen_year = 121
  and a.code_id = '107'
group by a.company_number, a.employee_number, b.employee_name, a.code_id  
having sum(a.amount) <> 0
order by store, employee_name