﻿
select a.pymast_company_number as store, a.employee_name as name, b.ded_pay_code, b.fixed_ded_amt, a.pay_period
from arkona.ext_pymast a
left join arkona.ext_pydeduct b on a.pymast_employee_number = b.employee_number
where ded_pay_code in ('91', '91b', '91c', '99', '99a', '99b', '99c')
  and a.active_code <> 'T'
order by store, name