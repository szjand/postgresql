﻿/*
Good morning Jon can you run me a report with payroll fields  85 and 85a I only need it for the honda store 
So name and what was paid in those fields for the 1st qtr
Thanks
KIm
04/07/20

*/

select b.employee_name as name, -- pymast_employee_number,
  sum(a.amount) filter (where a.code_id = '85') as "85",
  sum(a.amount) filter (where a.code_id = '85A') as "85A"
-- select b.employee_name, a.amount 
from arkona.ext_pyhscdta a
join arkona.xfm_pymast b on a.employee_number = b.pymast_employee_number
  and b.current_row
where a.code_id in ('85', '85A')
  and a.payroll_cen_year = 120
  and a.company_number = 'RY2'
group by b.employee_name  
order by b.employee_name

