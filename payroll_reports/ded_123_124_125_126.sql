﻿/*
6/7/19
Could I get a spreadsheet  with store, name, and deduction codes  123 ,  124,  125,  126.  
I would like this report to be what I take out of their payroll each month.
so like in June I will take money from 12b (6-14-91)  13b (6/28/19)  and 12s (06/30/19)
so if I could save that to a file that I can upload to Unum

6/14/19
what she is actually looking for is a spreadsheet to send to unum with what was paid per employee per deduction
*/

select a.pymast_company_number, a.employee_name, pymast_employee_number, a.pay_period ,
  b.code_type, b.ded_pay_code, b.fixed_Ded_amt, b.ded_freq,
  c.description
from arkona.xfm_pymast a
join arkona.ext_pydeduct b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
  and b.ded_pay_code in ('123','124','125','126')
left join arkona.ext_pypcodes c on b.company_number = c.company_number and b.ded_pay_code = c.ded_pay_code  
where a.current_row
  and a.active_code <> 'T'
  and a.pymast_company_number in ('RY1','RY2')
--   and a.pymast_employee_number = '138380'
order by a.employee_name


select * 
from arkona.ext_pypcodes
where ded_pay_code in ('123','124','125','126')


select a.pymast_company_number as store, a.employee_name as name, -- pymast_employee_number,
  max(case when ded_pay_code = '123' then fixed_ded_amt end) as "123 Amount",
  max(case when ded_pay_code = '123' then ded_freq end) as "123 Freq",
  max(case when ded_pay_code = '124' then fixed_ded_amt end) as "124 Amount",
  max(case when ded_pay_code = '124' then ded_freq end) as "124 Freq",
  max(case when ded_pay_code = '125' then fixed_ded_amt end) as "125 Amount",
  max(case when ded_pay_code = '125' then ded_freq end) as "125 Freq",
  max(case when ded_pay_code = '126' then fixed_ded_amt end) as "126 Amount",
  max(case when ded_pay_code = '126' then ded_freq end) as "126 Freq"
from arkona.xfm_pymast a
join arkona.ext_pydeduct b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
  and b.ded_pay_code in ('123','124','125','126')
-- left join arkona.ext_pypcodes c on b.company_number = c.company_number and b.ded_pay_code = c.ded_pay_code  
where a.current_row
  and a.active_code <> 'T'
  and a.pymast_company_number in ('RY1','RY2')
--   and a.pymast_employee_number = '138380'
group by a.pymast_company_number, a.employee_name --, pymast_employee_number  


-----------------------------------------------------------------------------------
-- 6/14/19

select a.company_number as store, a.employee_name as name,
  sum(amount) filter (where b.code_id = '123') as "GROUP ACCIDENT (123)",
  sum(amount) filter (where b.code_id = '124') as "GROUP HOSPITAL (124)",
  sum(amount) filter (where b.code_id = '125') as "CRITICAL ILL/CA (125)",
  sum(amount) filter (where b.code_id = '126') as "WHOLE LIFE INS (126)"
from arkona.ext_pyhshdta a
join arkona.ext_pyhscdta b on a.company_number = b.company_number
  and a.payroll_cen_year = b.payroll_cen_year
  and a.payroll_run_number = b.payroll_run_number
  and a.employee_ = b.employee_number
  and b.code_id in ('123','124','125','126')
join arkona.ext_pypcodes c on a.company_number = c.company_number
  and b.code_id = c.ded_pay_code  
where a.check_year = 19
  and a.check_month = 5
group by a.check_year, a.check_month, a.company_number, a.employee_name
