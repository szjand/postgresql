﻿select pymast_company_number as store, employee_name as name, 
  pymast_employee_number as "emp #", payroll_class, base_hrly_rate, base_salary
-- select *
from arkona.ext_pymast
where active_code <> 'T'
  and pymast_company_number in ('RY1','RY2')
