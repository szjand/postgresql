﻿DONT USE THIS ONE

USE alerus_annual_401k_2020.sql



-- this appears to work by ohmigod it is slow
select p.pymast_company_number, p.employee_name as Name, p.pymast_employee_number as ymempn, '           ' as ssn,
  arkona.db2_integer_to_date(birth_date) as "Birth Date",
  arkona.db2_integer_to_date(org_hire_date) as "Original Hire Date",    
  arkona.db2_integer_to_date(hire_date) as "Latest Hire Date",
  arkona.db2_integer_to_date(termination_date) as "Term Date",
  coalesce(  
    case active_code
      when 'A' then '1'
      when 'P' then '2'
    else null
    end, ' ') as "Employee Type",
    case active_code
      when 'A' then 'Active'
      when 'P' then 'Part-Time'
      when 'T' then 'Termed'
    end as Status,
  cast(g.Hours as Integer) as "YTD Hours",
  g.Gross as "Gross Compensation",
  coalesce((
    select sum(amount)
    from arkona.ext_pyhscdta -- rydedata.pyhscdta
    where payroll_cen_year = '120'  -- **************************************************************
    and employee_number = p.pymast_employee_number
-- the fuckers cheated, using deduction code 99 for employee AND employer amounts
-- oops, yhctyp
-- 1/5/16
--    and trim(yhccde) = '99'
    and code_id in ('99', '99A')
    and code_type = '2'), 0) as "Roth Deferral",
  coalesce(ec.EmprCont, 0) as "Employer Match",
  coalesce((
    select sum(amount)
    from arkona.ext_pyhscdta -- rydedata.pyhscdta
    where payroll_cen_year = '120'  -- **************************************************************
    and employee_number = p.pymast_employee_number
    and code_id = '91'), 0) as "Profit Sharing",
  p.address_1 as "Street Address",
  p.city_name as City,
  p.state_code_address_ as State,
  p.zip_code as Zip
from arkona.xfm_pymast p -- rydedata.pymas
inner join ( 
  select employee_, 
    sum(reg_hours) as Hours,  -- 7 Reg Hours
    sum(curr_adj_comp) as Gross -- 8 Curr Adj Comp
  from arkona.ext_pyhshdta -- rydedata.pyhshdta 
  where payroll_cen_year = '120' -- payroll cen + year **************************************************************
  group by employee_) g on p.pymast_employee_number = g.employee_
left join ( -- employer contribution via GL
  SELECT control, abs(SUM(amount)) as EmprCont 
  FROM fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.the_date between '01/01/2020' AND '12/31/2020' -- **************************************************************
  join fin.dim_account c on a.account_key = c.account_key
    and c.account in ('133003', '133004','233001', '233004','333003', '333004')
  GROUP BY control) ec on p.pymast_employee_number = ec.control
where p.current_row  
order by p.pymast_company_number, p.employee_name 




/*
this isn't working
select a.pymast_company_number, a.employee_name, a.pymast_employee_number, sum(b.amount)
from arkona.xfm_pymast a
left join fin.fact_gl b on a.pymast_employee_number = b.control
left join dds.dim_date c on b.date_key = c.date_key
  and c.the_year = 2019
left join fin.dim_account d on b.account_key = d.account_key
  and d.account in ('133003', '133004','233001', '233004','333003', '333004')
where a.current_row
group by a.pymast_company_number, a.employee_name, a.pymast_employee_number
order by a.pymast_company_number, a.employee_namy
*/