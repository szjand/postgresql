﻿/*
6/7/19
if possible I would also like a spreadsheet with store, name, and deductions columns for 
103,107,111,115.119,120,121,122,123,124,125,126,299,295,305
This one would be with amounts that are set up in there master file.
Also be able to save it to a file.  

10/24/19
Hi Jon is it possible to add department to my master deduction report?

*/



-- call it the master_deduction_list_20190925

select a.pymast_company_number as store, a.employee_name as name, 
  -- this bizarre notation ensures leading zeros in exceel
  '="' ||a.department_code || '"' as department,  
  max(case when ded_pay_code = '103' then fixed_ded_amt end) as "103",
  max(case when ded_pay_code = '107' then fixed_ded_amt end) as "107",
  max(case when ded_pay_code = '111' then fixed_ded_amt end) as "111",
  max(case when ded_pay_code = '115' then fixed_ded_amt end) as "115",
  max(case when ded_pay_code = '119' then fixed_ded_amt end) as "119",
  max(case when ded_pay_code = '120' then fixed_ded_amt end) as "120",
  max(case when ded_pay_code = '121' then fixed_ded_amt end) as "121",
  max(case when ded_pay_code = '122' then fixed_ded_amt end) as "122",
  max(case when ded_pay_code = '123' then fixed_ded_amt end) as "123",
  max(case when ded_pay_code = '124' then fixed_ded_amt end) as "124",
  max(case when ded_pay_code = '125' then fixed_ded_amt end) as "125",
  max(case when ded_pay_code = '126' then fixed_ded_amt end) as "126",
  max(case when ded_pay_code = '299' then fixed_ded_amt end) as "299",
  max(case when ded_pay_code = '295' then fixed_ded_amt end) as "295",
  max(case when ded_pay_code = '305' then fixed_ded_amt end) as "305"
from arkona.xfm_pymast a
join arkona.ext_pydeduct b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
  and b.ded_pay_code in ('103','107','111','115','119','120','121','122','123','124','125','126','299','295','305')
-- left join arkona.ext_pypcodes c on b.company_number = c.company_number and b.ded_pay_code = c.ded_pay_code  
where a.current_row
  and a.active_code <> 'T'
  and a.pymast_company_number in ('RY1','RY2')
--   and a.pymast_employee_number = '138380'
group by a.pymast_company_number, a.employee_name, a.department_code
order by a.employee_name

