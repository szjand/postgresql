﻿/*
11/11/2020
moved over from db2, 2019 checks out, looks good

generally kim asks for this after year end, save it as alerus_standard_census_401k_2019

the additional codes have been added in 2020, so:
Hi Jon   would you be able to run the report that we do at the end of the year for the 401k  
With all the added fields that we put in I want to proof it before the end of the year 
in case anyone put in more than they can and to make sure the match is correct


*/

/*

12/11/2020: sent kim alerus_standard_census_401k_12-11-2020.xlsx
      kim likes to check on it as year end approaches

1/12/21 sent the final 2020 version to kim

alter table tem.alerus_401K
rename column pymast_company_number to "Store";
alter table tem.alerus_401K
rename column pymast_employee_number to "Emp#";
alter table tem.alerus_401K
rename column employee_name to "Name";

12/16/21
Hi Jon can I get the 401k report that we do for Alerus  every year I just want to check to make sure no one is over the maximums they can contribute.
Kim
at this time, no changes, run this for Kim, she will let me know if something needs to be changed
saved result as alerus_annual_401k_2021_preview
*/

select * from tem.alerus_401K


-- lets get rid of the hard coding (2020)
do $$
declare
  _payroll_year integer := 121;
  _from_date date := '01/01/2021';
  _thru_date date := '12/31/2021';
begin  
--   drop table if exists wtf;
--   create temp table wtf as
  truncate tem.alerus_401k;
  insert into tem.alerus_401k
  select p.pymast_company_number as "Store", p.employee_name as "Name", p.pymast_employee_number as "Emp #", s.ssn as "SSN",
    arkona.db2_integer_to_date(p.birth_date) as "Birth Date",
    arkona.db2_integer_to_date(p.org_hire_date) as "Original Hire Date",
    arkona.db2_integer_to_date(p.hire_date) as "Latest Hire Date",
  case
    when arkona.db2_integer_to_date(p.termination_date) = '12/31/9999' then null
    else arkona.db2_integer_to_date(p.termination_date)
  end as "Term Date",
    coalesce(  
      case p.active_code
        when 'A' then '1'
        when 'P' then '2'
      else null
      end, ' ') as "Employee Type",
    case p.active_code
      when 'A' then 'Active'
      when 'P' then 'Part-Time'
      when 'T' then 'Termed'
    end as Status,    
    g.hours::integer as "YTD Hours", g.gross as "Gross Compensation",
    coalesce((
      select sum(amount)
      from arkona.ext_pyhscdta
      where payroll_cen_year = _payroll_year 
      and employee_number = p.pymast_employee_number 
      and code_id in ('99','99A','99B','99C')
      and code_type = '2'), 0) as "Roth Deferral" ,
    coalesce(ec.emprcont, 0) as "employer_match",   
    coalesce((
      select sum(amount)
      from arkona.ext_pyhscdta
      where payroll_cen_year = _payroll_year 
      and employee_number = p.pymast_employee_number
      and code_id in('91','91B','91C')
      and code_type = '2'), 0) as "Profit Sharing",
    p.address_1 as "Street Address",
    p.city_name as "City",
    p.state_code_address_ as "State",
    p.zip_code as "Zip"   
  from arkona.ext_pymast p 
  join (
    select employee_,sum(reg_hours) as Hours, 
    sum(curr_adj_comp) as Gross
  from arkona.ext_pyhshdta
  where payroll_cen_year = _payroll_year
  group by employee_) g on p.pymast_employee_number = g.employee_ 
  left join ( -- employer contribution via GL
    SELECT control, abs(SUM(amount)) as EmprCont
    FROM fin.fact_gl a
    join fin.dim_account b on a.account_key = b.account_key
      and b.account in ('133003', '133004','233001', '233004')
--       and b.account in ('133001','133002','133003', '133004','133007','233000','233001', '233002','233004','233007','233008')
    join dds.dim_date c on a.date_key = c.date_key
      and the_date BETWEEN _from_date and _thru_date
    GROUP BY control) ec on p.pymast_employee_number = ec.control 
  left join jon.ssn s on p.pymast_employee_number = s.employee_number 
  order by p.pymast_company_number, p.employee_name;
end $$;

-- select * from wtf; 
select * from tem.alerus_401k;



/*
11/12/20
from kim
Good morning Jon.    I am looking over the report and looking in payroll the numbers to not match so like on ben Cahalan 
in payroll it says his 401k  total is  14827.21  and the employer match is 9975.13 but on the report the match is 9538.05  
and the profit sharing (401k)  is 22010.91  .   on mine the deferral is correct but the match in payroll says 1821.10 
on the report it is 1740.71

Thanks Jon
*/

translating
ben             dt                      report
401K          14827.21                22010.91
emp match     9975.13                  9538.05
          (2791.43 + 7183.7)

kim
roth          4552.78                  4552.78     at least one match
emp match     1821.10                  1740.71          

OK, here is the deal, had to add the code_type in the subquery for profit sharing
and, fin.fact_gl does not generate future entries, dt that kim is looking at includes a check dated 11/13
so
generated the data from db2, inserted it into tem.alerus_401k added the ssn and sent it to kim as V2

the query should be good for its intended use, after the end of the year

