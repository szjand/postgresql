﻿/*
03/23/20
Can I get a report with store, name, department, payroll class, distribution code and male/female

Thanks
KIm
*/
-- select *

select pymast_company_number as store, employee_name as name, department_code,
  payroll_class, distrib_code, sex as gender
from arkona.ext_pymast 
where active_code <> 'T'
  and pymast_company_number in ('RY1','RY2')
order by store, name 


select pymast_company_number as store, employee_name as name, pymast_employee_number as employee_id, department_code,
  payroll_class, distrib_code, sex as gender
from arkona.ext_pymast 
where active_code <> 'T'
  and pymast_company_number in ('RY1','RY2')

union

select pymast_company_number as store, employee_name as name, pymast_employee_number as employee_id, department_code,
  payroll_class, distrib_code, sex as gender
from arkona.ext_pymast 
where active_code = 'T'
  and pymast_company_number in ('RY1','RY2')
  and dds.db2_integer_to_date(termination_date) > '12/31/2020'
order by store, name  