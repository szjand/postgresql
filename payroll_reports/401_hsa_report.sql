﻿/*
11/11/21
Jon could I get a deduction report with  the 401k percent and the H S A amounts?

kim
*/

select a.company_number, c.employee_name, a.employee_number, a.ded_pay_code, b.description, fixed_ded_amt, fixed_ded_pct
from arkona.ext_pydeduct a
join arkona.ext_pypcodes b on a.ded_pay_code = b.ded_pay_code
  and a.company_number = b.company_number
  and b.code_type = '2'
  and (
		b.description like '%401%'
		or 
		b.description like '%HSA%')
join arkona.ext_pymast c on a.employee_number = c.pymast_employee_number
  and c.active_code <> 'T'
order by a.company_number, c.employee_name
  		

