﻿
/*
Good Morning can you do a report out of the charge customer name list,   with the name, customer number and tax exempt No.
We did do this once but it was a long time ago.

Thanks
Kim
04/18/2020
*/
select a.search_name, a.customer_number, b.tax_exmpt_no_ as tax_exmpt_no
from arkona.ext_glpcust a
left join arkona.xfm_bopname b on a.record_key = b.bopname_record_key
  and b.current_row
where a.company_number in ('RY1','RY2')
  and a.active  = 'C'
  and a.customer_type = 2

/*
This worked well Jon can we run one for the honda wholesale customers.
04/21

*/

customer types are in glpctyp
RY2 Wholesale is customer_type 6

select a.search_name, a.customer_number, b.tax_exmpt_no_ as tax_exmpt_no
from arkona.ext_glpcust a
left join arkona.xfm_bopname b on a.record_key = b.bopname_record_key
  and b.current_row
where a.company_number in ('RY1','RY2')
  and a.active  = 'C'
  and a.customer_type = 6

-- and put them both together:

select 
  case
    when a.customer_type = 2 then 'RY1'
    when a.customer_type = 6 then 'RY2'
  end as store,
  a.search_name, a.customer_number, b.tax_exmpt_no_ as tax_exmpt_no
from arkona.ext_glpcust a
left join arkona.xfm_bopname b on a.record_key = b.bopname_record_key
  and b.current_row
where a.company_number in ('RY1','RY2')
  and a.active  = 'C'
  and a.customer_type = 2
union
select
  case
    when a.customer_type = 2 then 'RY1'
    when a.customer_type = 6 then 'RY2'
  end as store,
    a.search_name, a.customer_number, b.tax_exmpt_no_ as tax_exmpt_no
from arkona.ext_glpcust a
left join arkona.xfm_bopname b on a.record_key = b.bopname_record_key
  and b.current_row
where a.company_number in ('RY1','RY2')
  and a.active  = 'C'
  and a.customer_type = 6  
order by store, search_name  