﻿

select b.employee_name, string_agg(state_code_address_, ',')
from (
  select employee_name
  from (
    select employee_name, state_code_address_
    from arkona.xfm_pymast
    where arkona.db2_integer_to_date(hire_date) <= '12/31/2019'
      and arkona.db2_integer_to_date(termination_date) > '12/31/2018'
    group by employee_name, state_code_address_) a
  group by employee_name
  having count(*) > 1) b
join (
  select employee_name, state_code_address_
  from arkona.xfm_pymast
  where arkona.db2_integer_to_date(hire_date) <= '12/31/2019'
    and arkona.db2_integer_to_date(termination_date) > '12/31/2018'
  group by employee_name, state_code_address_) c on b.employee_name = c.employee_name 
group by b.employee_name


 select *
 from arkona.xfm_pymast
 where employee_name = 'sorum, abigail' 

 select min(row_from_date)
 from arkona.xfm_pymast