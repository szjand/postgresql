﻿-- 01/27/21
-- kim needs 2021 gross earnings
select 
  case
    when a.pymast_company_number = 'RY1' then 'Rydell Auto Center, Inc.'
    when a.pymast_company_number = 'RY2' then 'H.G.F., Inc.'
  end as store, a.pymast_employee_number as "emp #", a.employee_last_name, a.employee_first_name, b.total_gross_pay
from arkona.ext_pymast a
left join (  
	select employee_, sum(total_gross_pay) as total_gross_pay
	from arkona.ext_pyhshdta
	where payroll_cen_year = 121
	group by employee_) b on a.pymast_employee_number = b.employee_
where a.pymast_company_number in ('RY1','RY2')	
  and length(a.employee_name) > 3
  and a.active_code <> 'T'
  and arkona.db2_integer_to_date(a.hire_date) < '01/01/2022'
order by a.pymast_company_number, a.employee_last_name

