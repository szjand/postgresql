﻿/*
6/7/19
if possible I would also like a spreadsheet with store, name, and deductions columns for 
103,107,111,115.119,120,121,122,123,124,125,126,299,295,305
This one would be with amounts that are set up in there master file.
Also be able to save it to a file.  

*/

-- Report 2  which is with all the deductions listed.  That one looks good also  we can call it Master Deduction List

select a.pymast_company_number as store, a.employee_name as name, -- pymast_employee_number,
  max(case when ded_pay_code = '103' then fixed_ded_amt end) as "103",
  max(case when ded_pay_code = '107' then fixed_ded_amt end) as "107",
  max(case when ded_pay_code = '111' then fixed_ded_amt end) as "111",
  max(case when ded_pay_code = '115' then fixed_ded_amt end) as "115",
  max(case when ded_pay_code = '119' then fixed_ded_amt end) as "119",
  max(case when ded_pay_code = '120' then fixed_ded_amt end) as "120",
  max(case when ded_pay_code = '121' then fixed_ded_amt end) as "121",
  max(case when ded_pay_code = '122' then fixed_ded_amt end) as "122",
  max(case when ded_pay_code = '123' then fixed_ded_amt end) as "123",
  max(case when ded_pay_code = '124' then fixed_ded_amt end) as "124",
  max(case when ded_pay_code = '125' then fixed_ded_amt end) as "125",
  max(case when ded_pay_code = '126' then fixed_ded_amt end) as "126",
  max(case when ded_pay_code = '299' then fixed_ded_amt end) as "299",
  max(case when ded_pay_code = '295' then fixed_ded_amt end) as "295",
  max(case when ded_pay_code = '305' then fixed_ded_amt end) as "305"
from arkona.xfm_pymast a
join arkona.ext_pydeduct b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
  and b.ded_pay_code in ('103','107','111','115','119','120','121','122','123','124','125','126','299','295','305')
-- left join arkona.ext_pypcodes c on b.company_number = c.company_number and b.ded_pay_code = c.ded_pay_code  
where a.current_row
  and a.active_code <> 'T'
  and a.pymast_company_number in ('RY1','RY2')
--   and a.pymast_employee_number = '138380'
group by a.pymast_company_number, a.employee_name --, pymast_employee_number  
order by a.employee_name

create or replace function nrv.get_payroll_report_master_deduction_list()
returns table(store_code citext, name citext, "103" numeric(8,2),"107" numeric(8,2),"111" numeric(8,2),
  "115" numeric(8,2),"119" numeric(8,2),"120" numeric(8,2),"121" numeric(8,2),"122" numeric(8,2),
  "123" numeric(8,2),"124" numeric(8,2),"125" numeric(8,2),"126" numeric(8,2),"299" numeric(8,2),
  "295" numeric(8,2),"305" numeric(8,2)) as
$BODY$
/*
select * from nrv.get_payroll_report_master_deduction_list()
*/
select a.pymast_company_number as store, a.employee_name as name, -- pymast_employee_number,
  max(case when ded_pay_code = '103' then fixed_ded_amt end) as "103",
  max(case when ded_pay_code = '107' then fixed_ded_amt end) as "107",
  max(case when ded_pay_code = '111' then fixed_ded_amt end) as "111",
  max(case when ded_pay_code = '115' then fixed_ded_amt end) as "115",
  max(case when ded_pay_code = '119' then fixed_ded_amt end) as "119",
  max(case when ded_pay_code = '120' then fixed_ded_amt end) as "120",
  max(case when ded_pay_code = '121' then fixed_ded_amt end) as "121",
  max(case when ded_pay_code = '122' then fixed_ded_amt end) as "122",
  max(case when ded_pay_code = '123' then fixed_ded_amt end) as "123",
  max(case when ded_pay_code = '124' then fixed_ded_amt end) as "124",
  max(case when ded_pay_code = '125' then fixed_ded_amt end) as "125",
  max(case when ded_pay_code = '126' then fixed_ded_amt end) as "126",
  max(case when ded_pay_code = '299' then fixed_ded_amt end) as "299",
  max(case when ded_pay_code = '295' then fixed_ded_amt end) as "295",
  max(case when ded_pay_code = '305' then fixed_ded_amt end) as "305"
from arkona.xfm_pymast a
join arkona.ext_pydeduct b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
  and b.ded_pay_code in ('103','107','111','115','119','120','121','122','123','124','125','126','299','295','305')
where a.current_row
  and a.active_code <> 'T'
  and a.pymast_company_number in ('RY1','RY2')
group by a.pymast_company_number, a.employee_name 
order by a.employee_name;
$BODY$
language sql;
  