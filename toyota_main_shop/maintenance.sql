﻿-- 1. term tech
-- 2. new tech
-- 3. term
-- 4. change flat rate

-- 1 ----------------------------------------------------------------
select * 
from ts.main_shop_flat_rate_techs

-- -	Dylan Zimmer – Toyota Main Shop Technician – 10/21/2022
update ts.main_shop_flat_rate_techs
set thru_date = '10/22/2022', -- last day of the pay period
    end_date = '10/21/2022' -- actual term date
where employee_number = '8100230';

-- 2. ----------------------------------------------------------------

-- 11/10/23 add dylan schaefer
select * from dds.dim_tech where employee_number = '8100224'

insert into ts.main_shop_flat_rate_techs(last_name,first_name,employee_number,tech_number,
	flat_rate,daily_guarantee,tool_allowance,from_date)
select 'Schaefer', 'Dylan', '8100224', '408', 21, 0, 69.42, '11/05/2023'

select * from ts.main_shop_flat_rate_techs

-- 11/6/23 monday morning, alec berg has been approved in ukg for flat rate, from hourly
select * from dds.dim_tech where employee_number = '8100201'

select * from  ts.main_shop_flat_rate_techs

insert into ts.main_shop_flat_rate_techs(last_name,first_name,employee_number,tech_number,
	flat_rate,daily_guarantee,tool_allowance,from_date)
select 'Berg', 'Alec', '8100201', '409', 21, 0, 69.42, '10/22/2023'



-- 3. ----------------------------------------------------------------
update ts.main_shop_flat_Rate_techs
set thru_date = '12/14/2022', end_date = '12/14/2022'
where employee_number = '8100212'

-- 4. ----------------------------------------------------------------
-- 08/25/23
-- Raymond Cole got a $4.00 an hour increase, from $20 an hour flat rate to $24.00 an hour flat rate 
-- and its changed in UKG. Jose Delacruz got a $2.00 an hour increase, from $20 an hour flat rate 
-- to $22.00 an hour flat rate. Can you guys update that for me since we submit payroll Monday? 
-- Dayton Marek

change the thru_date, but not the end_date
select * from ts.main_shop_flat_rate_techs
-- update old rows
update ts.main_shop_flat_rate_techs
set thru_date = '08/12/2023'
where employee_number in ('8100203', '8100205');
-- insert new rows
insert into ts.main_shop_flat_rate_Techs values
('Delacruz','Jose','8100205','401',22.00,171.13,69.42,'08/13/2023','9999-12-31','9999-12-31'),
('Cole','Raymond','8100203','402',24.00,152.16,69.42,'08/13/2023','9999-12-31','9999-12-31');