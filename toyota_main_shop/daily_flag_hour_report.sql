﻿/*
 
Thank you so much! Ben wants us to send him everyday a 5 day rolling average for flag hours for the main shop. 
Currently I have been doing it through labor profit analysis which from my understanding is only closed R.O.'s 
so I don't think it is an accurate representation on how busy we are every day. It does not have to be something very in depth. Just something simple where I can track each day for the actual flag hours recorded if that makes sense. 
 
Thank you,
 
Dayton Marek 


*/
-- 1/24/23

resulted in header:
18-Jan	19-Jan	20-Jan	21-Jan		Total	Avg
not returning the last 5 working (non sunday, non holiday) dates

-- header
with
  dates as (
		select the_date, mmdd, row_number() over (order by the_date desc) as seq
		from dds.dim_date
		where day_of_week <> 1
			and not holiday
			and the_date between current_date - 8 and current_date - 1
-- 			and the_date between '01/23/2023'::date - 8 and '01/23/2023'::date - 1 -- monday
-- 			and the_date between '01/26/2023'::date - 8 and '01/26/2023'::date - 1 -- thursday
    order by the_date desc
    limit 5)  
select null,
  (select mmdd from dates	where seq = 5),		
  (select mmdd from dates	where seq = 4),
  (select mmdd from dates	where seq = 3),
  (select mmdd from dates	where seq = 2),
  (select mmdd from dates	where seq = 1),
  'Total'::text,'Avg'::text;


-- flag hours
need the relevant from and thru dates

with
  dates as (
		select the_date, mmdd, row_number() over (order by the_date desc) as seq
		from dds.dim_date
		where day_of_week <> 1
			and not holiday
			and the_date between current_date - 8 and current_date - 1
-- 			and the_date between '01/23/2023'::date - 8 and '01/23/2023'::date - 1 -- monday
-- 			and the_date between '01/26/2023'::date - 8 and '01/26/2023'::date - 1 -- thursday
    order by the_date desc
    limit 5)  
select a.* , b.flag_hours, c.flag_hours, d.flag_hours, e.flag_hours, f.total, g.avg
from (
	select first_name || ' ' || last_name as tech, flag_hours
	from prs.data a
	JOin dds.dim_date b on a.the_date = b.the_date
		and b.day_of_week <> 1
		and not b.holiday
	where store = 'rydell toyota'
		and a.the_date = (select the_date from dates where seq = 5)) a
left join (
	select first_name || ' ' || last_name as tech, flag_hours
	from prs.data a
	JOin dds.dim_date b on a.the_date = b.the_date
		and b.day_of_week <> 1
		and not b.holiday
	where store = 'rydell toyota'
		and a.the_date = (select the_date from dates where seq = 4)) b on a.tech = b.tech				
left join (
	select first_name || ' ' || last_name as tech, flag_hours
	from prs.data a
	JOin dds.dim_date b on a.the_date = b.the_date
		and b.day_of_week <> 1
		and not b.holiday
	where store = 'rydell toyota'
		and a.the_date = (select the_date from dates where seq = 3)) c on a.tech = c.tech		
left join (
	select first_name || ' ' || last_name as tech, flag_hours
	from prs.data a
	JOin dds.dim_date b on a.the_date = b.the_date
		and b.day_of_week <> 1
		and not b.holiday
	where store = 'rydell toyota'
		and a.the_date = (select the_date from dates where seq = 2)) d on a.tech = d.tech		
left join (
	select first_name || ' ' || last_name as tech, flag_hours
	from prs.data a
	JOin dds.dim_date b on a.the_date = b.the_date
		and b.day_of_week <> 1
		and not b.holiday
	where store = 'rydell toyota'
		and a.the_date = (select the_date from dates where seq = 1)) e on a.tech = e.tech		
left join (
	select first_name || ' ' || last_name as tech, sum(flag_hours) as total
	from prs.data a
	JOin dds.dim_date b on a.the_date = b.the_date
		and b.day_of_week <> 1
		and not b.holiday
	where store = 'rydell toyota'
		and a.the_date between (select the_date from dates where seq = 5) and (select the_date from dates where seq = 1)
  group by first_name || ' ' || last_name) f on a.tech = f.tech		
left join (
	select first_name || ' ' || last_name as tech, round(sum(flag_hours)/5, 2) as avg
	from prs.data a
	JOin dds.dim_date b on a.the_date = b.the_date
		and b.day_of_week <> 1
		and not b.holiday
	where store = 'rydell toyota'
		and a.the_date between (select the_date from dates where seq = 5) and (select the_date from dates where seq = 1)
  group by first_name || ' ' || last_name) g on a.tech = g.tech		 






  			
------------------------------------------------------------------------------------------------------------
-- 01/22/23   rethinking the whole damned thing
------------------------------------------------------------------------------------------------------------
 
 	             1/16	1/17	1/18	1/19	1/20 avg
Raymond Cole	  1.2	10.6	 2.0	12.9	7.7
Jose Delazruz	  0.0	12.5	 9.6	 5.2	1.7
David Mahlum	  3.2	 6.5	11.3	 9.0	2.4
Eric Moon	      3.4	10.0	10.0	 8.2	0.0
Douglas Pede	  2.0	 1.2   1.4	 3.5	1.2
Dylan Schaefer	3.7	 9.0   5.2	 3.5	1.0
 
then the same thing for skills and shop

-- query for header


with
  dates as (
		select the_date, mmdd, row_number() over (order by the_date) as seq
		from dds.dim_date
		where day_of_week <> 1
			and not holiday
			and the_date between current_date - 5 and current_date - 1)  
select null,
  (select mmdd from dates	where seq = 1),		
  (select mmdd from dates	where seq = 2),
  (select mmdd from dates	where seq = 3),
  (select mmdd from dates	where seq = 4),
  (select mmdd from dates	where seq = 5),
  'Total','Avg' 		


select a.*, b.flag_hours, c.flag_hours, d.flag_hours, e.flag_hours, f.total, g.avg
from (
	select first_name || ' ' || last_name as tech, flag_hours
	from prs.data a
	JOin dds.dim_date b on a.the_date = b.the_date
		and b.day_of_week <> 1
		and not b.holiday
	where store = 'rydell toyota'
		and a.the_date = current_date - 5) a
left join (
	select first_name || ' ' || last_name as tech, flag_hours
	from prs.data a
	JOin dds.dim_date b on a.the_date = b.the_date
		and b.day_of_week <> 1
		and not b.holiday
	where store = 'rydell toyota'
		and a.the_date = current_date - 4) b on a.tech = b.tech		
left join (
	select first_name || ' ' || last_name as tech, flag_hours
	from prs.data a
	JOin dds.dim_date b on a.the_date = b.the_date
		and b.day_of_week <> 1
		and not b.holiday
	where store = 'rydell toyota'
		and a.the_date = current_date - 3) c on a.tech = c.tech		
left join (
	select first_name || ' ' || last_name as tech, flag_hours
	from prs.data a
	JOin dds.dim_date b on a.the_date = b.the_date
		and b.day_of_week <> 1
		and not b.holiday
	where store = 'rydell toyota'
		and a.the_date = current_date - 2) d on a.tech = d.tech		
left join (
	select first_name || ' ' || last_name as tech, flag_hours
	from prs.data a
	JOin dds.dim_date b on a.the_date = b.the_date
		and b.day_of_week <> 1
		and not b.holiday
	where store = 'rydell toyota'
		and a.the_date = current_date - 1) e on a.tech = e.tech		
left join (
	select first_name || ' ' || last_name as tech, sum(flag_hours) as total
	from prs.data a
	JOin dds.dim_date b on a.the_date = b.the_date
		and b.day_of_week <> 1
		and not b.holiday
	where store = 'rydell toyota'
		and a.the_date between current_date - 5 and current_date - 1
  group by first_name || ' ' || last_name) f on a.tech = f.tech		
left join (
	select first_name || ' ' || last_name as tech, round(sum(flag_hours)/5, 2) as avg
	from prs.data a
	JOin dds.dim_date b on a.the_date = b.the_date
		and b.day_of_week <> 1
		and not b.holiday
	where store = 'rydell toyota'
		and a.the_date between current_date - 5 and current_date - 1
  group by first_name || ' ' || last_name) g on a.tech = g.tech		  

--------------------------------------------------------------------------------------
--	 first cut, didn't last long
--------------------------------------------------------------------------------------
i was going to use the flat rate tables for this, but one of the guys is salaried, one is coming on as hourly
so, use the prs data

modified function prs.update_data() to include Dylan Schaefer as an hourly toyota tech
now it is a matter of generating the data for Dayton

eric moon is driveability
everybody else is maintenance
no heavy

5 day rolling average, include saturdays, per tech and total

yesterdays flag hours per tech and total

select * from dds.dim_Date where the_date = '01/15/2023'

select a.the_date, last_name, first_name, flag_hours
from prs.data a
JOin dds.dim_date b on a.the_date = b.the_date
  and day_of_week <> 1
where store = 'rydell toyota'
  and a.the_date between current_date - 7 and current_date
order by last_name, the_date


select a.the_date, first_name || ' ' || last_name as tech, flag_hours, clock_hours, 
	round(avg(a.flag_hours) over (partition by last_name order by a.the_date rows between 4 preceding and current row), 1) as the_avg,
	sum(a.flag_hours) over (partition by last_name order by a.the_date rows between 4 preceding and current row) as the_sum,
	count(*) over (partition by last_name order by a.the_date rows between 4 preceding and current row) as the_count
from prs.data a
JOin dds.dim_date b on a.the_date = b.the_date
  and b.day_of_week <> 1
  and not b.holiday
where store = 'rydell toyota'
  and a.the_date between current_date - 5 and current_date - 1
order by last_name, a.the_date


select the_date
from dds.dim_date
where day_of_week <> 1
  and not holiday
  and the_date between current_date - 5 a 


with
  the_dates_text as (
		select 
			(select mmdd from dds.dim_date where the_date = a.from_date) as from_date,
			(select mmdd from dds.dim_date where the_date = a.thru_date) as thru_date,
			(select mmdd from dds.dim_date where the_date = a.yesterday) as yesterday
		from (  
		select min(the_date) as from_date, max(the_date) as thru_date, max(the_date) - 1 as yesterday 
		from dds.dim_date
		where day_of_week <> 1
			and not holiday
			and the_date between current_date - 5 and current_date - 1) a)
select '5 Day Average Flag Hours by Tech from ' || (select from_date from the_dates_text) || ' thru ' || (select thru_date from the_dates_text);		

with
  the_dates as (			
		select min(the_date) as from_date, max(the_date) as thru_date, max(the_date) - 1 as yesterday 
		from dds.dim_date
		where day_of_week <> 1
			and not holiday
			and the_date between current_date - 5 and current_date - 1)
select tech, the_avg::text as "avg flag hours/day" 
from (
	select a.the_date, first_name || ' ' || last_name as tech, flag_hours, clock_hours, 
		round(avg(a.flag_hours) over (partition by last_name order by a.the_date rows between 4 preceding and current row), 1) as the_avg
	from prs.data a
	JOin dds.dim_date b on a.the_date = b.the_date
		and b.day_of_week <> 1
		and not b.holiday
	where store = 'rydell toyota'
		and a.the_date between current_date - 5 and current_date - 1) aa
where aa.the_date = (select thru_date from the_dates);		

with
  the_dates_text as (
		select 
			(select mmdd from dds.dim_date where the_date = a.from_date) as from_date,
			(select mmdd from dds.dim_date where the_date = a.thru_date) as thru_date,
			(select mmdd from dds.dim_date where the_date = a.yesterday) as yesterday
		from (  
		select min(the_date) as from_date, max(the_date) as thru_date, max(the_date) - 1 as yesterday 
		from dds.dim_date
		where day_of_week <> 1
			and not holiday
			and the_date between current_date - 5 and current_date - 1) a)
select 'Flag Hours Yesterday by Tech (' || (select thru_date from the_dates_text) || ')'; 

with
  the_dates as (			
		select min(the_date) as from_date, max(the_date) as thru_date, max(the_date) - 1 as yesterday 
		from dds.dim_date
		where day_of_week <> 1
			and not holiday
			and the_date between current_date - 5 and current_date - 1)
select tech, flag_hours as "flag hours"
from (
	select a.the_date, first_name || ' ' || last_name as tech, flag_hours
	from prs.data a
	JOin dds.dim_date b on a.the_date = b.the_date
		and b.day_of_week <> 1
		and not b.holiday
	where store = 'rydell toyota'
		and a.the_date between current_date - 5 and current_date - 1) aa
where aa.the_date = (select yesterday from the_dates);	

with
  the_dates_text as (
		select 
			(select mmdd from dds.dim_date where the_date = a.from_date) as from_date,
			(select mmdd from dds.dim_date where the_date = a.thru_date) as thru_date
		from (  
		select min(the_date) as from_date, max(the_date) as thru_date, max(the_date) - 1 as yesterday 
		from dds.dim_date
		where day_of_week <> 1
			and not holiday
			and the_date between current_date - 5 and current_date - 1) a)
select 'Shop 5 Day Average Flag Hours from ' || (select from_date from the_dates_text) || ' thru ' 
	|| (select thru_date from the_dates_text)	|| ': ' || a.avg_flag_hours || ' hours/day'
from (
	select round(sum(flag_hours)/5.0, 1)::text as avg_flag_hours
	from prs.data a
	JOin dds.dim_date b on a.the_date = b.the_date
		and b.day_of_week <> 1
		and not b.holiday
	where store = 'rydell toyota'
		and a.the_date between current_date - 5 and current_date - 1) a;


with
  the_dates_text as (
		select 
			(select mmdd from dds.dim_date where the_date = a.from_date) as from_date,
			(select mmdd from dds.dim_date where the_date = a.thru_date) as thru_date
		from (  
		select min(the_date) as from_date, max(the_date) as thru_date, max(the_date) - 1 as yesterday 
		from dds.dim_date
		where day_of_week <> 1
			and not holiday
			and the_date between current_date - 5 and current_date - 1) a)
select 'Skill Level 5 Day Average Flag Hours from ' || (select from_date from the_dates_text) || ' thru ' || (select thru_date from the_dates_text);

select skill, round(sum(flag_hours)/5.0, 1)
from (
	select 
	  case
	    when last_name = 'moon' then 'driveability'
	    else 'maintenance'
	  end as skill, flag_hours
	from prs.data a
	JOin dds.dim_date b on a.the_date = b.the_date
		and b.day_of_week <> 1
		and not b.holiday
	where store = 'rydell toyota'
		and a.the_date between current_date - 5 and current_date - 1) a
group by skill;				