﻿

SELECT
    replace(table_name, '"', ''),
    table_size,
    pg_size_pretty(table_size) AS pretty_table_size,
    pg_size_pretty(indexes_size) AS indexes_size,
    pg_size_pretty(total_size) AS total_size
FROM (
    SELECT
        table_name,
        pg_table_size(table_name) AS table_size,
        pg_indexes_size(table_name) AS indexes_size,
        pg_total_relation_size(table_name) AS total_size
    FROM (
        SELECT ('"' || table_schema || '"."' || table_name || '"') AS table_name
        FROM information_schema.tables
    ) AS all_tables
    ORDER BY table_size DESC
) AS pretty_sizes


-- generate a statement to truncate all tables > 500MB
SELECT 'truncate ' || replace(table_name, '"', '') || ';'
FROM (
    SELECT
        table_name,
        pg_table_size(table_name) AS table_size,
        pg_indexes_size(table_name) AS indexes_size,
        pg_total_relation_size(table_name) AS total_size
    FROM (
        SELECT ('"' || table_schema || '"."' || table_name || '"') AS table_name
        FROM information_schema.tables
    ) AS all_tables
    ORDER BY table_size DESC
) AS pretty_sizes
where table_size > 491847680

SELECT
    replace(table_name, '"', ''),
    table_size AS table_size,
    pg_size_pretty(indexes_size) AS indexes_size,
    pg_size_pretty(total_size) AS total_size
FROM (
    SELECT
        table_name,
        pg_table_size(table_name) AS table_size,
        pg_indexes_size(table_name) AS indexes_size,
        pg_total_relation_size(table_name) AS total_size
    FROM (
        SELECT ('"' || table_schema || '"."' || table_name || '"') AS table_name
        FROM information_schema.tables
    ) AS all_tables
    ORDER BY table_size DESC
) AS pretty_sizes




select schemaname as table_schema,
       relname as table_name,
       pg_size_pretty(pg_total_relation_size(relid)) as total_size,
       pg_size_pretty(pg_relation_size(relid)) as data_size,
       pg_size_pretty(pg_total_relation_size(relid) - pg_relation_size(relid)) 
        as external_size
from pg_catalog.pg_statio_user_tables 
order by pg_total_relation_size(relid) desc,
         pg_relation_size(relid) desc;

-- each index, with total index size per table
select a.schemaname as table_schema,
       relname as table_name,
       pg_size_pretty(pg_total_relation_size(relid)) as total_size,
       pg_size_pretty(pg_relation_size(relid)) as data_size,
       pg_size_pretty(pg_total_relation_size(relid) - pg_relation_size(relid)) as pretty_external_size,
       pg_total_relation_size(relid) - pg_relation_size(relid) as external_size,
	     b.indexname
from pg_catalog.pg_statio_user_tables a
left join pg_indexes b on a.schemaname = b.schemaname
  and a.relname = b.tablename
order by pg_total_relation_size(relid)  - pg_relation_size(relid) desc;         
		 
		 
		 


SELECT  -- 228 / 3/16/23:394GB
    pg_size_pretty (
        pg_database_size ('cartiva')
    );		 


arkona_cdc: 61G / 3/16/23: 148 GB
-- a specific schema
select pg_size_pretty(sum(total_size))
from (
	SELECT
			replace(table_name, '"', ''),
			table_size,
			pg_size_pretty(table_size) AS pretty_table_size,
			pg_size_pretty(indexes_size) AS indexes_size,
			pg_size_pretty(total_size) AS pretty_total_size, total_size
	FROM (
			SELECT
					table_name,
					pg_table_size(table_name) AS table_size,
					pg_indexes_size(table_name) AS indexes_size,
					pg_total_relation_size(table_name) AS total_size
			FROM (
					SELECT ('"' || table_schema || '"."' || table_name || '"') AS table_name
					FROM information_schema.tables
					where table_schema = 'arkona_cdc'
			) AS all_tables
			ORDER BY table_size DESC
	) AS pretty_sizes) x


-- schema sizes
select table_schema, pg_size_pretty(schema_size)
from (
select table_schema, sum(total_relation_size) as schema_size
from (
	select schemaname as table_schema,
				 relname as table_name,
				 pg_total_relation_size(relid) as total_relation_size,
				 pg_size_pretty(pg_relation_size(relid)) as data_size,
				 pg_size_pretty(pg_total_relation_size(relid) - pg_relation_size(relid)) 
					as external_size
	from pg_catalog.pg_statio_user_tables) x
group by table_schema 
order by schema_size desc) xx 



