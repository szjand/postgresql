https://stackoverflow.com/questions/15692508/a-faster-way-to-copy-a-postgresql-database-or-the-best-way
	Create your dumps with:
	pg_dump -Fc -Z 9  --file=file.dump myDb
	restore it with:
	pg_restore -Fc -j 8  file.dump

Mastering Postgresql 9.6
from ~/Desktop/luigi_backup_Script.txt:
/usr/bin/pg_dump --host 10.130.196.173 --port 5432 --username "postgres" --no-password  --format custom --blobs --verbose --file "/home/jon/Desktop/ops.backup" --schema "ops" "cartiva"

pg_restore --list returns the backups table of contents:
jon@ubuntu:~$ pg_restore --list /home/jon/Desktop/ops.backup
;
; Archive created at 2018-07-29 06:33:04 CDT
;     dbname: cartiva
;     TOC Entries: 40
;     Compression: -1
;     Dump Version: 1.13-0
;     Format: CUSTOM
;     Integer: 4 bytes
;     Offset: 8 bytes
;     Dumped from database version: 9.6.2
;     Dumped by pg_dump version: 9.6.9
;
;
; Selected TOC Entries:
...


to have bash time the back up: 
	time /usr/bin/pg_dump --host 10.130.196.173 --port 5432 --username "postgres" --no-password  --format custom --blobs --verbose --file "/home/jon/Desktop/ops.backup" --schema "ops" "cartiva"
returns:
real	0m1.337s
user	0m0.509s
sys	0m0.105s
where real is wall clock time

experiment with -Fd:
jon@ubuntu:~$ cd Desktop
jon@ubuntu:~/Desktop$ mkdir Fd_test
* note: pg_dump will create the directory if it does not already exist
1. straight format custom
time /usr/bin/pg_dump --host 10.130.196.173 --port 5432 --username "postgres" --no-password  --format custom --blobs --verbose --file "/home/jon/Desktop/arkona_data.backup" -a --schema "arkona" "cartiva"
real	2m1.777s
user	1m8.389s
sys	0m49.555s

-- with 4 workers, cuts time in half
time /usr/bin/pg_dump --host 10.130.196.173 --port 5432 --username "postgres" --no-password  -Fd cartiva -j 4 -f  /home/jon/Desktop/Fd_test --verbose -a --schema arkona
real	1m3.907s
user	1m20.493s
sys	1m5.486s

-- with 8 workers, no improvement
time /usr/bin/pg_dump --host 10.130.196.173 --port 5432 --username "postgres" --no-password  -Fd cartiva -j 8 -f  /home/jon/Desktop/fd_test --verbose -a --schema arkona
real	1m8.653s
user	1m21.094s
sys	0m59.487s

-- this worked, dump and restore
jon@ubuntu-1804:~$ time /usr/bin/pg_dump --host 10.130.196.173 --port 5432 --username postgres --no-password  -Fd cartiva -j 4 -f  /home/jon/Desktop/fd_test --verbose --exclude-table 'arkona.z_u*' --schema arkona
jon@ubuntu-1804:~$ time /usr/bin/pg_restore --host localhost --port 5432 --username postgres --no-password  --verbose -j 4 -d cartiva /home/jon/Desktop/fd_test



