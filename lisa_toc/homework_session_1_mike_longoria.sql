﻿drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
insert into step_1
select 'april', store, page, line, line_label, description, control, sum(unit_count) as unit_count
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 202204-----------------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 202204  ---------------------------------------------------------------------
      and (
        (b.page between 5 and 15 and b.line between 1 and 40) -- retail only
        or
        (b.page = 16 and b.line between 1 and 6)) -- used cars retail only
--         or
--         (b.page = 17 and b.line between 1 and 20))-- f/i counts don't make sense
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by store, page, line, line_label, description, control
order by store, page, line;

select * from step_1

drop table if exists step_2;
create temp table step_2 as
select "?column?"::text as the_month, 
  case
    when description like '%RAO%' then 'RAO'
    else store
  end as store, control, sum(unit_count)
from step_1
group by "?column?"::Text, store, control, description
having sum(unit_count) > 0;

select * from sls.deals limit 10

-----------------------------------------------------------------
--< new cars 208
-----------------------------------------------------------------
select a.the_month, a.store, b.thru_date - b.ground_date as turn
from step_2 a
join nc.vehicle_acquisitions b on a.control = b.stock_number
  and
    case -- G44524 unwound and resold in may
      when a.control = 'G44524' then b.source = 'unwind'
      else 1 = 1
    end
where a.the_month = 'may'
  and store = 'RY2'   


select store, count(*), round(1.0 * sum(turn)/count(*), 1)
from (
	select a.the_month, a.store, b.thru_date - b.ground_date as turn
	from step_2 a
	join nc.vehicle_acquisitions b on a.control = b.stock_number
		and
			case -- G44524 unwound and resold in may
				when a.control = 'G44524' then b.source = 'unwind'
				else 1 = 1
			end) c
group by store			

-----------------------------------------------------------------
--/> new cars 208
-----------------------------------------------------------------

-- used excluding lease purchase and intra market  362
-- vii, lose 1
-- walks lose 2
select the_month, store, count(*), round(1.0 * sum(turn)/count(*), 1)
from (
	select a.the_month, a.store,  a.control, b.thruts::date as sold, c.vehicle_walk_ts::date as walk, d.avail, 
		b.thruts::date - coalesce(d.avail, c.vehicle_walk_ts::date) as turn
	from step_2 a
	join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
	join ads.ext_vehicle_walks c on b.vehicleinventoryitemid = c.vehicle_inventory_item_id::citext
	left join (
		select a.control, max(c.fromts::date) as avail
		from step_2 a
		join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
		join ads.ext_vehicle_inventory_item_statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
			and c.status = 'RMFlagAV_Available'
		group by a.control) d on a.control = d.control
	where right(a.control, 1) not in ('1','2','3','4','5','6','7','8','9','0','R', 'L','G')
		and a.control not in ('G44321D','G44374P')) e
group by the_month, store



-- need to handle dup instances of g unit vins
drop table if exists dup_gs;
create temp table dup_gs as
-- multiple instances of vin 
select a.control
from step_2 a
join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
left join ads.ext_vehicle_inventory_items d on c.vehicleitemid = d.vehicleitemid
  and 
    case
      when left(b.stocknumber, 1) = 'G' then left(d.stocknumber, 1) = 'H'
      when left(b.stocknumber, 1) = 'H' then left(d.stocknumber, 1) = 'G'
    end
where right(control, 1) = 'G'
group by a.control
having count(*) > 1;

-- g units orig stock number
drop table if exists g_unit_source;
create temp table g_unit_source as
select a.control, d.stocknumber
from step_2 a
join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
left join ads.ext_vehicle_inventory_items d on c.vehicleitemid = d.vehicleitemid
  and 
    case
      when left(b.stocknumber, 1) = 'G' then left(d.stocknumber, 1) = 'H'
      when left(b.stocknumber, 1) = 'H' then left(d.stocknumber, 1) = 'G'
    end
left join dup_gs e on a.control = e.control    
where right(a.control, 1) = 'G'
  and e.control is null
  and a.control not in ('H15582G','G44519G','G44126G');

-- g units date avail
select a.control, a.stocknumber, max(c.fromts::date) as avail
from g_unit_source a
join ads.ext_vehicle_inventory_items b on a.stocknumber = b.stocknumber
join ads.ext_vehicle_inventory_item_statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
	and c.status = 'RMFlagAV_Available'
group by a.control, a.stocknumber	

-- the walk & avail dates
select the_month, store, count(*), round(1.0 * sum(turn)/count(*), 1)
from (
	select aa.the_month, aa.store, a.control, bb.thruts::date as sold, c.vehicle_walk_ts::date as walk, d.avail,
		bb.thruts::date - coalesce(d.avail, c.vehicle_walk_ts::date) as turn
	from step_2 aa
	join g_unit_source a on aa.control = a.control
	join ads.ext_vehicle_inventory_items bb on a.control = bb.stocknumber
	join ads.ext_vehicle_inventory_items b on a.stocknumber = b.stocknumber
	join ads.ext_vehicle_walks c on b.vehicleinventoryitemid = c.vehicle_inventory_item_id::citext
	left join (
		select a.control, a.stocknumber, max(c.fromts::date) as avail
		from g_unit_source a
		join ads.ext_vehicle_inventory_items b on a.stocknumber = b.stocknumber
		join ads.ext_vehicle_inventory_item_statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
			and c.status = 'RMFlagAV_Available'
		group by a.control, a.stocknumber) d on a.control = d.control) e
group by the_month, store



-- and put it all together, this is the best i can do for used for now
select store, count(*), round(1.0 * sum(turn)/count(*), 1)
from (
	select a.the_month, a.store,  a.control, b.thruts::date as sold, c.vehicle_walk_ts::date as walk, d.avail, 
		b.thruts::date - coalesce(d.avail, c.vehicle_walk_ts::date) as turn
	from step_2 a
	join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
	join ads.ext_vehicle_walks c on b.vehicleinventoryitemid = c.vehicle_inventory_item_id::citext
	left join (
		select a.control, max(c.fromts::date) as avail
		from step_2 a
		join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
		join ads.ext_vehicle_inventory_item_statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
			and c.status = 'RMFlagAV_Available'
		group by a.control) d on a.control = d.control
	where right(a.control, 1) not in ('1','2','3','4','5','6','7','8','9','0','R', 'L','G')
		and a.control not in ('G44321D','G44374P')
union
	select aa.the_month, aa.store, a.control, bb.thruts::date as sold, c.vehicle_walk_ts::date as walk, d.avail,
		bb.thruts::date - coalesce(d.avail, c.vehicle_walk_ts::date) as turn
	from step_2 aa
	join g_unit_source a on aa.control = a.control
	join ads.ext_vehicle_inventory_items bb on a.control = bb.stocknumber
	join ads.ext_vehicle_inventory_items b on a.stocknumber = b.stocknumber
	join ads.ext_vehicle_walks c on b.vehicleinventoryitemid = c.vehicle_inventory_item_id::citext
	left join (
		select a.control, a.stocknumber, max(c.fromts::date) as avail
		from g_unit_source a
		join ads.ext_vehicle_inventory_items b on a.stocknumber = b.stocknumber
		join ads.ext_vehicle_inventory_item_statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
			and c.status = 'RMFlagAV_Available'
		group by a.control, a.stocknumber) d on a.control = d.control) f
group by store		