﻿select * from toc.accounts  limit 10

update toc.accounts
set toc_account_type = 'Sale'
where department_code = 'NC' and account_type_code = '4'



update toc.accounts
set toc_account_type = 'None'
where account_number like '1463%';

update toc.accounts
set toc_account_type = 'None'
where account_number like '1464%';

update toc.accounts
set toc_account_type = 'None'
where account_number like '1473%';

update toc.accounts
set toc_account_type = 'None'
where department = 'National';

update toc.accounts
set toc_account_type = 'Mixed'
where account_number in ('147900','147901','249000','166901','11301c');

update toc.accounts
set toc_account_type = 'Other'
where account_number like '1955%';

-- sales 240120 - 244010
update toc.accounts
set toc_account_type = 'Other'
where account_number between '190000' and '190900' and length(account_number) = 6 and department  = 'General'

-- OE 12001 - 12929
update toc.accounts
set toc_account_type = 'OE'
-- select * from toc.accounts
where account_type = 'expense'
  and left(account_number, 5)::integer between 12001 and 12929

-- oe 16006 - 16102
update toc.accounts
set toc_account_type = 'OE'
-- select * from toc.accounts
where account_type = 'expense'
  and left(account_number, 5)::integer between 16006 and 16102


-- oe - 16301b - 16627
update toc.accounts
set toc_account_type = 'OE'
-- select * from toc.accounts
where account_type = 'expense'
  and left(account_number, 5)::integer between 16301 and 16627
  and toc_account_type is null
order by account_number

-- -- oe 16703c - 16904 ( not 16904c)
update toc.accounts
set toc_account_type = 'OE'
-- select * from toc.accounts
where account_type = 'expense'
  and left(account_number, 5)::integer between 16703 and 16904
  and account_number <> '16904c'
order by account_number

-- oe 16905 - 21110
update toc.accounts
set toc_account_type = 'OE'
-- select * from toc.accounts
where account_type = 'expense'
  and left(account_number, 5)::integer between 16905 and 21110
  and account_number <> '16904c'
  and toc_account_type is null
order by account_number

-- oe 21502l -22924
update toc.accounts
set toc_account_type = 'OE'
-- select * from toc.accounts
where account_type = 'expense'
  and left(account_number, 5)::integer between 21502 and 22924
--   and account_number <> '16904c'
--   and toc_account_type is null
order by account_number

-- oe 25105 - 26103
update toc.accounts
set toc_account_type = 'OE'
-- select * from toc.accounts
where account_type = 'expense'
  and left(account_number, 5)::integer between 25105 and 26103
  and account_number <> '26103C'
--   and toc_account_type is null
order by account_number


-- oe 26301 - 29900
update toc.accounts
set toc_account_type = 'OE'
-- select * from toc.accounts
where account_type = 'expense'
  and left(account_number, 5)::integer between 26301 and 29900
--   and account_number <> '26103C'
--   and toc_account_type is null
order by account_number

-- oe 166031 - 166236 --query didn't work, updated manually---------------------------------
update toc.accounts
set toc_account_type = 'OE'
-- select * from toc.accounts
where account_type = 'expense'
  and left(account_number, 5)::integer between 16603 and 16623
  and length(account_number) = 6
order by account_number

-- equity blank
update toc.accounts
set toc_account_type = 'None'
where account_type = 'Equity' 


-- nc cogs TVC  (not 1641%, leave blank  22641% blank
update toc.accounts
set toc_account_type = 'TVC'
-- select * from toc.accounts
where account_type = 'cogs'
  and department_code = 'nc'
  and account_number not like '1641%'
  and account_number not like '2641%'
  and toc_account_type is null
order by account_number  


-- mix 164800 264800 
update toc.accounts
set toc_account_type = 'Mixed'
-- select * from toc.accounts
where account_number in ('164800','264800')

-- honda body shop none
update toc.accounts
set toc_account_type = 'None'
-- select * from toc.accounts 
where intercompany = 'B'
  and department_code = 'bs';

-- 27% none
update toc.accounts
set toc_account_type = 'None'
-- select * from toc.accounts
where account_number like '27%';

-- all 1641% -> other
update toc.accounts
set toc_account_type = 'Other'
-- select * from toc.accounts 
where account_number like '1641%'


-- this was the sorting that enabled the range work 
select * from toc.accounts order by account_type desc, left(account_number,5)::integer asc



select toc_account_type, count(*)
from toc.accounts
group by toc_account_type
order by toc_account_type