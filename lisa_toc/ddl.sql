﻿/*
-- populated from db2 with:
Select account_number, account_sub_type as intercompany, account_type as account_type_code, 
  case account_type
    when '1' then 'Asset'
    when '2' then 'Liability'
    when '3' then 'Equity'
    when '4' then 'Sale'
    when '5' then 'COGS'
    when '6' then 'Income'
    when '7' then 'Other Income'
    when '8' then 'Expense'
    when '9' then 'Other Expense'
  end as account_type,
a.department as department_code, b.dept_description as department, account_desc as description, '' as toc_account_type
-- select count(*) -- 3475
-- select *
from RYDEDATA.GLPMAST a
left join rydedata.glpdept b on a.company_number = b.company_number
  and a.department = b.department_code
where year = 2021
  and active = 'Y'
  and a.company_number in ('RY1','RY2')
*/  

create schema toc;
comment on schema toc is 'schema for the lisa toc project';

drop table if exists toc.accounts cascade;
create table toc.accounts (
  account_number citext primary key,
  intercompany citext not null,
  account_type_code citext not null,
  account_type citext not null,
  department_code citext not null,
  department citext not null,
  description citext not null,
  toc_account_type citext) ;

select count(*) from toc.accounts  

update toc.accounts
set toc_account_type = null;

select * from toc.accounts 


select * 
from toc.accounts
where department_code = 'nc'
  and account_type = 'sale'

delete 
from toc.accounts
where intercompany = 'C'


  