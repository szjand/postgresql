﻿select distinct first_name, last_name, employee_number, tech_number
from tp.data
where the_date > '07/31/2022'
  and employee_number in ('1100625','1106400','150105','1125565','145789','157953','160432','184920','1118722')
order by last_name

drop table if exists jon.bs_constraint_techs;
create table jon.bs_constraint_techs as
select a.first_name, a.last_name, a.employee_number, b.tech_number, array_agg(b.tech_key) as tech_keys
from ukg.employees a
join dds.dim_tech b on a.employee_number = b.employee_number
  and b.tech_key <> 100
where a.employee_number in ('1100625','1106400','150105','1125565','145789','157953','160432','184920','1118722')
group by a.first_name, a.last_name, a.employee_number, b.tech_number
order by last_name

select *
from dds.dim_tech
where tech_key = 100

select ro_number, ro_technician_id
from arkona.ext_sdprhdr
where left(ro_number, 2) = '18'
  and length(ro_number) = 8
  and ro_technician_id = '234'

select a.ro, b.*
from dds.fact_repair_order a
join dds.dim_tech b on a.tech_key = b.tech_key
join (
select ro_number, ro_technician_id
from arkona.ext_sdprhdr
where left(ro_number, 2) = '18'
  and length(ro_number) = 8
  and ro_technician_id = '234') c on a.ro = c.ro_number


select a.stocknumber, b.* 
from ads.ext_vehicle_inventory_items a
join (
	select vehicleinventoryitemid, fromts::date, thruts::date
	from ads.ext_vehicle_inventory_item_statuses
	where status = 'RMFlagAV_Available') b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
where a.fromts::date > '05/31/2022'  

-- dont need the status



select a.stocknumber, b.vin, d.ro, d.line, d.flag_hours, e.*
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
join dds.dim_vehicle c on b.vin = c.vin
join dds.fact_repair_order d on c.vehicle_key = d.vehicle_key
  and d.open_date between a.fromts::date and a.thruts::date
join jon.bs_constraint_techs e on d.tech_key = any(e.tech_keys)
where a.fromts::date > '05/31/2022'  
order by d.ro, d.line



select a.stocknumber, b.vin, d.ro, d.line, d.flag_hours, f.*
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
join dds.dim_vehicle c on b.vin = c.vin
join dds.fact_repair_order d on c.vehicle_key = d.vehicle_key
  and d.open_date between a.fromts::date and a.thruts::date
  and d.flag_hours > 0
join dds.dim_service_type e on d.service_type_key = e.service_type_key
  and e.service_type_code = 'BS'
join dds.dim_tech f on d.tech_key = f.tech_key
where a.fromts::date > '05/31/2022'  
order by d.ro, d.line

-- group to ro/vehicle
select count(*), count(*) filter (where bb.tech_keys is not null) -- 208/387
from (
	select a.stocknumber, b.vin, d.ro, sum(d.flag_hours) as flag_hours, array_agg(distinct f.tech_number) as tech_numbers
	from ads.ext_vehicle_inventory_items a
	join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
	join dds.dim_vehicle c on b.vin = c.vin
	join dds.fact_repair_order d on c.vehicle_key = d.vehicle_key
		and d.open_date between a.fromts::date and a.thruts::date
		and d.flag_hours > 0
	join dds.dim_service_type e on d.service_type_key = e.service_type_key
		and e.service_type_code = 'BS'
	join dds.dim_tech f on d.tech_key = f.tech_key
-- 	where a.fromts::date > '05/31/2022'  
	where a.fromts::date > '12/31/2021'  
	group by a.stocknumber, b.vin, d.ro) aa
left join jon.bs_constraint_techs bb on bb.tech_number = any(aa.tech_numbers)


-- group on vehicle, not ro
select count(*), count(*) filter (where bb.tech_keys is not null) -- 199/325
from (
	select a.stocknumber, b.vin, sum(d.flag_hours) as flag_hours, array_agg(distinct f.tech_number) as tech_numbers
	from ads.ext_vehicle_inventory_items a
	join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
	join dds.dim_vehicle c on b.vin = c.vin
	join dds.fact_repair_order d on c.vehicle_key = d.vehicle_key
		and d.open_date between a.fromts::date and a.thruts::date
		and d.flag_hours > 0
	join dds.dim_service_type e on d.service_type_key = e.service_type_key
		and e.service_type_code = 'BS'
	join dds.dim_tech f on d.tech_key = f.tech_key
-- 	where a.fromts::date > '05/31/2022'  
	where a.fromts::date > '12/31/2021'
	group by a.stocknumber, b.vin) aa
left join jon.bs_constraint_techs bb on bb.tech_number = any(aa.tech_numbers)

-- need to know what kind of work the constraint techs performed
-- select count(distinct ro) from (
	select a.stocknumber, b.vin, d.open_date, d.close_date, d.ro, d.line, d.flag_hours, g.first_name, g.last_name, g.tech_number, h.opcode, h.description, i.complaint, i.correction
	from ads.ext_vehicle_inventory_items a
	join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
	join dds.dim_vehicle c on b.vin = c.vin
	join dds.fact_repair_order d on c.vehicle_key = d.vehicle_key
		and d.open_date between a.fromts::date and a.thruts::date
		and d.flag_hours > 0
	join dds.dim_service_type e on d.service_type_key = e.service_type_key
		and e.service_type_code = 'BS'
	join dds.dim_tech f on d.tech_key = f.tech_key
	join jon.bs_constraint_techs g on d.tech_key = any(g.tech_keys)
	left join dds.dim_opcode h on d.opcode_key = h.opcode_key
	left join dds.dim_ccc i on d.ccc_key = i.ccc_key
-- 	left join dds.dim_customer j on d.customer_key = j.customer_key
	where a.fromts::date > '05/31/2022'  
-- ) aa	
order by h.opcode, i.complaint	
order by b.vin, d.ro, d.line	
