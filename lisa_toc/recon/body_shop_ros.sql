﻿/*

drop table if exists toc.bs_techs;
create table toc.bs_techs (
  tech_name citext not null,
  tech_number citext not null,
  employee_number citext not null,
  skill citext);
create unique index on toc.bs_techs(tech_name);  
create unique index on toc.bs_techs(tech_number);
create unique index on toc.bs_techs(employee_number);
insert into toc.bs_techs
select tech_name, tech_number, employee_number 
from dds.dim_tech
where flag_Department = 'body shop'
  and current_row
  and active
  and tech_number in ('273','249','204','221','279','275','276','293','271','232','213','309','251','255','241');  

update toc.bs_techs
set skill = 'paint'
where tech_number in ('273','249','204');

update toc.bs_techs
set skill = 'pdr'
where tech_number in ('213','232');

update toc.bs_techs
set skill = 'glass'
where tech_number = '276';

update toc.bs_techs
set skill = 'metal'
where tech_number in ('221','279','275','293','271','309','251','255','241');


drop table if exists toc.bs_ros;
create table toc.bs_ros as
select a.open_date, a.close_date, a.ro, a.line, a.flag_hours, b.tech_number, b.tech_name, c.full_name, d.vin,
  e.correction, f.opcode, f.description
-- select a.*
from dds.fact_repair_order a
join dds.dim_tech b on a.tech_key = b.tech_key
join dds.dim_customer c on a.customer_key = c.customer_key
join dds.dim_vehicle d on a.vehicle_key = d.vehicle_key
left join dds.dim_ccc e on a.ccc_key = e.ccc_key
left join dds.dim_opcode f on a.opcode_key = f.opcode_key
left join dds.dim_opcode g on a.opcode_key = g.opcode_key
where left(a.ro, 2) = '18'
  and close_date between current_date - 365 and current_date
  and flag_hours > 0

drop table if exists toc.bs_ros_1st_quarter;
create table toc.bs_ros_1st_quarter as
select a.open_date, a.close_date, a.ro, a.line, a.flag_hours, b.tech_number, b.tech_name, bb.skill, c.full_name, d.vin,
  e.correction, f.opcode, f.description
-- select a.*
from dds.fact_repair_order a
join dds.dim_tech b on a.tech_key = b.tech_key
join toc.bs_techs bb on b.employee_number = bb.employee_number
join dds.dim_customer c on a.customer_key = c.customer_key
join dds.dim_vehicle d on a.vehicle_key = d.vehicle_key
left join dds.dim_ccc e on a.ccc_key = e.ccc_key
left join dds.dim_opcode f on a.opcode_key = f.opcode_key
where left(a.ro, 2) = '18'
  and close_date between '01/01/2022' and '03/31/2022'
  and flag_hours > 0

  

drop table if exists toc.bs_ros_2021;
create table toc.bs_ros_2021 as
select a.open_date, a.close_date, a.ro, a.line, a.flag_hours, b.tech_number, b.tech_name, bb.skill, c.full_name, d.vin,
  e.correction, f.opcode, f.description
-- select a.*
from dds.fact_repair_order a
join dds.dim_tech b on a.tech_key = b.tech_key
join toc.bs_techs bb on b.employee_number = bb.employee_number
join dds.dim_customer c on a.customer_key = c.customer_key
join dds.dim_vehicle d on a.vehicle_key = d.vehicle_key
left join dds.dim_ccc e on a.ccc_key = e.ccc_key
left join dds.dim_opcode f on a.opcode_key = f.opcode_key
where left(a.ro, 2) = '18'
  and close_date between '01/01/2021' and '12/31/2021'
  and flag_hours > 0
*/  



select full_name, tech_name, skill, opcode, description, count(*), sum(flag_hours) as flag_hours
from  toc.bs_ros_1st_quarter
group by  full_name, tech_name, skill, opcode, description
order by opcode
order by count(*) desc 

select * from  toc.bs_ros_1st_quarter where opcode = '*' limit 100

select * from toc.bs_ros_1st_quarter where ro = '18087457'

select tech_name, skill, opcode, description, 
	count(*) filter (where full_name = 'inventory') as inv_lines, 
  sum(flag_hours) filter (where full_name = 'inventory') as inv_flag_hours,
	count(*) filter (where full_name <> 'inventory') as cust_lines, 
  sum(flag_hours) filter (where full_name <> 'inventory') as cust_flag_hours
from  toc.bs_ros_1st_quarter
group by tech_name, skill, opcode, description
order by opcode



select * from  toc.bs_ros_1st_quarter where skill = 'metal' order by ro, line

select count(distinct ro) from  toc.bs_ros_1st_quarter

select * from toc.bs_ros where description like '%frame%'

select * from toc.bs_ros limit 10

select * from toc.bs_ros where correction like '%frame%'

select * from toc.bs_ros where correction like '%unibody%'

select * from toc.bs_ros where correction like '%pull%'

select * from toc.bs_techs

select opcode, description, count(*)
from toc.bs_ros_1st_quarter
group by opcode, description
order by count(*) desc 

-- first quarter flag hours, recon vs cust pay
select sum(flag_hours) filter (where full_name = 'inventory') as recon,
  sum(flag_hours) filter (where full_name <> 'inventory') as cust
from toc.bs_ros_1st_quarter
where opcode not in ('BSIW','BSCE','BSRW','BSCD','SHOP','TRAIN','ISCAN')

-- first quarter, flag hours by tech, recon vs cust pay
select * 
from (
	select tech_name, skill, sum(flag_hours) filter (where full_name = 'inventory') as recon,
		sum(flag_hours) filter (where full_name <> 'inventory') as cust
	from toc.bs_ros_1st_quarter
	where opcode not in ('BSIW','BSCE','BSRW','BSCD','SHOP','TRAIN','ISCAN')
		and tech_name is not null
	group by tech_name, skill ) a
where recon > 10
  and cust > 10
order by skill, tech_name

-- 1st quarter by flag hours tech/opcode recon vs cust pay
select * 
from (
	select tech_name, skill, opcode, sum(flag_hours) filter (where full_name = 'inventory') as recon,
		sum(flag_hours) filter (where full_name <> 'inventory') as cust, description
	from toc.bs_ros_1st_quarter
	where opcode not in ('BSIW','BSCE','BSRW','BSCD','SHOP','TRAIN','ISCAN')
		and close_date > '12/31/2021'
		and tech_name is not null
	group by tech_name, skill, opcode, description ) a
where recon > 10
  and cust > 10
order by skill, opcode, tech_name


select open_date, vin, tech_name, skill, opcode, sum(flag_hours)
from toc.bs_ros_1st_quarter
where skill = 'metal'
  and opcode not in ('shop')
group by open_date, vin, tech_name, skill, opcode
order by open_date, vin, tech_name


-- proficiency
select aa.tech_name, aa.flag/bb.clock as proficiency, bb.clock as time_clock_hours
from (
	select tech_name, sum(flag_hours) as flag
	from toc.bs_ros_1st_quarter
	where skill = 'metal'
		and open_date > '12/31/2021'
	group by tech_name) aa
join (
	select b.tech_name, sum(a.clock_hours) as clock
	from ukg.clock_hours a
	join toc.bs_techs b on a.employee_number = b.employee_number
	where a.the_date between '01/01/2022' and '03/31/2022'
	group by b.tech_name) bb on aa.tech_name = bb.tech_name

-- afton's extract from the body shop advantage data
-- this table is populated by w10_python.ext_ads.bs_afton_lead_times.py
-- currently loaded with 1st quarter 2022 data
drop table if exists toc.bs_lead_times_1 cascade;
create table toc.bs_lead_times_1 (
  jobid integer,
  ro citext,
  arrived timestamp,
  released timestamp,
  completed timestamp,
  delivered timestamp)
  	
select * from toc.bs_lead_times_1

select * from toc.bs_ros_1st_quarter limit 20

select a.ro, sum(a.flag_hours), string_agg(distinct a.tech_name || ' ' || a.skill, ','), a.full_name, a.vin, b.arrived, b.released, b.completed, b.delivered
from toc.bs_ros_1st_quarter a
left join toc.bs_lead_times_1 b on a.ro = b.ro
where full_name <> 'inventory'
group by a.ro, a.full_name, a.vin, b.arrived, b.released, b.completed, b.delivered


select *
from toc.bs_ros_1st_quarter
where skill <> 'metal'

-- lisa's dubious assertion that "most" vehicles have multiple ros
select a.*
from toc.bs_ros_1st_quarter a
join (
	select vin  -- 180 vins with multiple ros, 1058 vins: 17%
	from (
		select vin, ro
		from toc.bs_ros_1st_quarter
		group by vin, ro) a
	group by vin 
	having count(*) > 1) b on a.vin = b.vin
order by a.vin

select count(distinct vin) from toc.bs_ros_1st_quarter  -- 1058


select a.vin, count(*), array_agg(distinct ro)
from toc.bs_ros_1st_quarter a
join (
	select vin  -- 180 vins with multiple ros, 1058 vins: 17%
	from (
		select vin, ro
		from toc.bs_ros_1st_quarter
		group by vin, ro) a
	group by vin 
	having count(*) > 1) b on a.vin = b.vin
group by a.vin

select * from toc.bs_ros_1st_quarter where vin = 'KNMAT2MVXHP582186'