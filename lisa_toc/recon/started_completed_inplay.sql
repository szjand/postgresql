﻿on each day, how many go into wip, come out of wip, in play


select *
from ads.ext_vehicle_inventory_item_statuses 
where

-- go into play 
select the_date, count(distinct b.vehicleinventoryitemid), array_agg(distinct c.stocknumber)
from dds.dim_date a
join ads.ext_vehicle_inventory_item_statuses  b on a.the_date = b.fromts::date
  and b.status = 'MechanicalReconProcess_InProcess'
join ads.ext_vehicle_inventory_items c on b.vehicleinventoryitemid = c.vehicleinventoryitemid  
where a.the_date between '01/01/2021' and '03/31/2022'
group by the_date

select aa.the_date, coalesce(bb.into_play, 0) as into_play, coalesce(cc.out_of_play, 0) as out_of_play, coalesce(dd.in_play, 0) as in_play
from dds.dim_date aa
left join (-- go into play 
select the_date, count(distinct b.vehicleinventoryitemid) as into_play--, array_agg(distinct c.stocknumber)
from dds.dim_date a
join ads.ext_vehicle_inventory_item_statuses  b on a.the_date = b.fromts::date
  and b.status = 'MechanicalReconProcess_InProcess'
join ads.ext_vehicle_inventory_items c on b.vehicleinventoryitemid = c.vehicleinventoryitemid  
where a.the_date between '01/01/2021' and '03/31/2022'
group by the_date) bb on aa.the_date = bb.the_date
left join (-- come out of play 
select the_date, count(distinct b.vehicleinventoryitemid) as out_of_play--, array_agg(distinct c.stocknumber)
from dds.dim_date a
join ads.ext_vehicle_inventory_item_statuses  b on a.the_date = b.thruts::date
  and b.status = 'MechanicalReconProcess_InProcess'
join ads.ext_vehicle_inventory_items c on b.vehicleinventoryitemid = c.vehicleinventoryitemid  
where a.the_date between '01/01/2021' and '03/31/2022'
group by the_date) cc on aa.the_date = cc.the_date
left join (-- in play
select the_date, count(distinct b.vehicleinventoryitemid) as in_play--, array_agg(distinct c.stocknumber)
from dds.dim_date a
join ads.ext_vehicle_inventory_item_statuses  b on a.the_date between b.fromts::date and  b.thruts::date
  and b.status = 'MechanicalReconProcess_InProcess'
join ads.ext_vehicle_inventory_items c on b.vehicleinventoryitemid = c.vehicleinventoryitemid  
where a.the_date between '01/01/2021' and '03/31/2022'
group by the_date) dd on aa.the_date = dd.the_date

where aa.the_date between '01/01/2021' and '03/31/2022'
  and aa.weekday
  and not aa.holiday 
order by aa.the_date

select * from dds.dim_date where the_date = current_date


-- -- can't use NoIncompleteReconItems, it is reset independent of the dept
-- select a.stocknumber, count(*)
-- from ads.ext_vehicle_inventory_items a
-- left join ads.ext_vehicle_inventory_item_statuses b  on a.vehicleinventoryitemid = b.vehicleinventoryitemid
--   and b.status = 'MechanicalReconProcess_NoIncompleteReconItems'
-- where a.fromts > '12/31/2021'
-- group by a.stocknumber order by count(*) desc
-- -- having count(*) = 1
-- 
-- -- multiple instances of NoIncompleteReconItems without ever having been in wip
-- select * 
-- from ads.ext_vehicle_inventory_item_statuses
-- where vehicleinventoryitemid = 'aecc853c-a73d-4af3-b9ef-23e81112b68f'
--   and status = 'MechanicalReconProcess_InProcess'