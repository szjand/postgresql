/*
DECLARE @date date;
@date = curdate() -1;
--DROP TABLE #wtf;
SELECT thedate, 
  case when name = 'Honda Cartiva' then 'Honda' else name END AS store, 
  value,
  SUM(CASE WHEN description IS NOT NULL THEN 1 ELSE 0 END) AS Completed
-- INTO #wtf  
FROM (  
  SELECT d.thedate, b.*
  FROM dds.day d, (
    SELECT b.name, a.value, a.partyid
    FROM partycollections a
    INNER JOIN organizations b ON a.partyid = b.partyid
    WHERE b.name <> 'Crookston'
      AND a.typ = 'PartyCollection_AppearanceReconItem'
      AND a.ThruTS IS NULL) b
  WHERE d.thedate = @date) x
LEFT JOIN (
  SELECT trim(cast(b.description AS sql_varchar)) AS Description, CAST(a.completets AS sql_date) AS FinishDate, c.owninglocationid
  FROM AuthorizedReconItems a
  LEFT JOIN VehicleReconItems b ON a.VehicleReconItemID = b.VehicleReconItemID 
  LEFT JOIN VehicleInventoryItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID 
  WHERE a.status = 'AuthorizedReconItem_Complete'
    AND CAST(a.CompleteTS AS sql_date) = @date) y ON x.thedate = y.finishdate
  AND x.value = TRIM(y.description) collate ads_default_ci
  AND x.partyid = y.owninglocationid
GROUP BY x.thedate, x.name, x.value;  



  SELECT c.stocknumber, left(trim(cast(b.description AS sql_varchar)), 15) AS Description, CAST(a.completets AS sql_date) AS FinishDate, c.owninglocationid
  FROM AuthorizedReconItems a
  LEFT JOIN VehicleReconItems b ON a.VehicleReconItemID = b.VehicleReconItemID 
  LEFT JOIN VehicleInventoryItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID 
  WHERE a.status = 'AuthorizedReconItem_Complete'
    AND CAST(a.CompleteTS AS sql_date) = curdate()
	
SELECT DISTINCT typ 
FROM partycollections	

  SELECT d.thedate, b.*
  FROM dds.day d, (
    SELECT b.name, a.value, a.partyid
    FROM partycollections a
    INNER JOIN organizations b ON a.partyid = b.partyid
    WHERE b.name <> 'Crookston'
      AND a.typ = 'PartyCollection_AppearanceReconItem'
      AND a.ThruTS IS NULL) b
  WHERE d.thedate = curdate()
	
SELECT * FROM VehicleInventoryItems WHERE stocknumber = 'G44426P'	

VehicleInventoryItemID: '593100e9-8e9e-4df2-ba5d-8d715490054e'

SELECT top 10 * FROM AuthorizedReconItems 


  SELECT c.stocknumber, left(trim(cast(b.description AS sql_varchar)), 15) AS Description, CAST(a.completets AS sql_date) AS FinishDate, c.owninglocationid
  FROM AuthorizedReconItems a
  JOIN VehicleReconItems b ON a.VehicleReconItemID = b.VehicleReconItemID 
  JOIN VehicleInventoryItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID 
	  AND c.stocknumber = 'G43372A'	
  WHERE a.status = 'AuthorizedReconItem_Complete'
    AND CAST(a.CompleteTS AS sql_date) = curdate()
	
SELECT * FROM VehicleReconItems WHERE VehicleInventoryItemID = '8ec8e462-16f8-45c9-8018-6c1d2ff8f204'	

SELECT c.stocknumber, a.status, a.startts, a.completets, b.typ, bb.description, b.description
DROP TABLE #jon;
SELECT c.stocknumber
INTO #jon
FROM AuthorizedReconItems a
JOIN VehicleReconItems b on a.VehicleReconItemID = b.VehicleReconItemID  
  AND b.typ LIKE 'Mechanical%'
JOIN typdescriptions bb on b.typ = bb.typ
  AND bb.description <> 'Inspection'
JOIN VehicleInventoryItems c on b.VehicleInventoryItemID = c.VehicleInventoryItemID 
WHERE CAST(a.completets AS sql_date) = curdate()
--  and c.stocknumber = 'G44067C'	
	AND EXISTS ( 
	  SELECT 1
		FROM VehicleInventoryItemStatuses 
		WHERE VehicleInventoryItemID = c.VehicleInventoryItemID
		  AND status = 'MechanicalReconProcess_NoIncompleteReconItems'
			AND CAST(fromts AS sql_date) = curdate())
GROUP BY c.stocknumber			

SELECT COUNT(*) FROM #jon

SELECT COUNT(*)
--a.VehicleInventoryItemID, stocknumber, status, a.fromts
--VehicleInventoryItemID, status AS BodyStatus, thruts
DROP TABLE #afton;
SELECT stocknumber
INTO #afton -- 14
-- SELECT COUNT(*)
    FROM VehicleInventoryItemStatuses a
	INNER JOIN VehicleInventoryItems  b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
    WHERE a.category = 'MechanicalReconProcess'
    AND a.status = 'MechanicalReconProcess_InProcess'
    AND cast(a.thruTS AS SQL_DATE) = '04/27/2022';
		
SELECT * FROM #jon a full outer JOIN #afton b on a.stocknumber = b.stocknumber		

*/

--------------------------------------------------------------------------------
--< 04/28/22 mechanical
SELECT c.stocknumber, a.status, a.startts, a.completets, b.typ, bb.description, b.description
DROP TABLE #jon;
SELECT c.stocknumber
INTO #jon
FROM AuthorizedReconItems a
JOIN VehicleReconItems b on a.VehicleReconItemID = b.VehicleReconItemID  
  AND b.typ LIKE 'Mechanical%'
JOIN typdescriptions bb on b.typ = bb.typ
  AND bb.description <> 'Inspection'
JOIN VehicleInventoryItems c on b.VehicleInventoryItemID = c.VehicleInventoryItemID 
WHERE CAST(a.completets AS sql_date) = curdate() - 1
--  and c.stocknumber = 'G44067C'	
	AND EXISTS ( 
	  SELECT 1
		FROM VehicleInventoryItemStatuses 
		WHERE VehicleInventoryItemID = c.VehicleInventoryItemID
		  AND status = 'MechanicalReconProcess_NoIncompleteReconItems'
			AND CAST(fromts AS sql_date) = curdate() -1)
GROUP BY c.stocknumber			

-- sent to afton 4/28 11:00AM
SELECT c.stocknumber
FROM AuthorizedReconItems a
JOIN VehicleReconItems b on a.VehicleReconItemID = b.VehicleReconItemID  
JOIN typcategories bbb on b.typ = bbb.typ
  AND bbb.category = 'MechanicalReconItem'
JOIN typdescriptions bb on b.typ = bb.typ
  AND bb.description <> 'Inspection'
JOIN VehicleInventoryItems c on b.VehicleInventoryItemID = c.VehicleInventoryItemID 
  AND c.thruts IS NULL 
WHERE CAST(a.completets AS sql_date) = curdate()  -------------------
	AND EXISTS ( 
	  SELECT 1
		FROM VehicleInventoryItemStatuses 
		WHERE VehicleInventoryItemID = c.VehicleInventoryItemID
		  AND status = 'MechanicalReconProcess_NoIncompleteReconItems'
			AND CAST(fromts AS sql_date) = curdate()) -----------------------
GROUP BY c.stocknumber


/*
-- this IS a bit slower
SELECT c.stocknumber
FROM AuthorizedReconItems a
JOIN VehicleReconItems b on a.VehicleReconItemID = b.VehicleReconItemID  
JOIN typcategories bbb on b.typ = bbb.typ
  AND bbb.category = 'MechanicalReconItem'
JOIN typdescriptions bb on b.typ = bb.typ
  AND bb.description <> 'Inspection'
JOIN VehicleInventoryItems c on b.VehicleInventoryItemID = c.VehicleInventoryItemID 
  AND c.thruts IS NULL 
JOIN VehicleInventoryItemStatuses d on c.VehicleInventoryItemID = d.VehicleInventoryItemID
  AND d.status = 'MechanicalReconProcess_NoIncompleteReconItems'
	AND CAST(d.fromts AS sql_date) = curdate() -1
WHERE CAST(a.completets AS sql_date) = curdate() - 1
GROUP BY c.stocknumber
*/
-------------------------------------
-- G43463P
select * FROM VehicleInventoryItems WHERE stocknumber = 'G43463P'

SELECT * FROM VehicleReconItems WHERE VehicleInventoryItemID = 'ca6f7d23-48f6-4262-8f4c-e100240f17b2'

SELECT * FROM AuthorizedReconItems WHERE VehicleReconItemID = '415871e8-44bf-db42-919e-82622efc44b7'
-------------------------------------

-----------------------------------------------------------------------
-- sent to afton 4/28 11:00AM
-- body
--DROP TABLE #jon;
SELECT c.stocknumber
--INTO #jon
FROM AuthorizedReconItems a
JOIN VehicleReconItems b on a.VehicleReconItemID = b.VehicleReconItemID  
JOIN typcategories bbb on b.typ = bbb.typ
  AND bbb.category = 'BodyReconItem'
JOIN VehicleInventoryItems c on b.VehicleInventoryItemID = c.VehicleInventoryItemID 
  AND c.thruts IS NULL 
WHERE CAST(a.completets AS sql_date) = curdate()------------------------
	AND EXISTS ( 
	  SELECT 1
		FROM VehicleInventoryItemStatuses 
		WHERE VehicleInventoryItemID = c.VehicleInventoryItemID
		  AND status = 'BodyReconProcess_NoIncompleteReconItems'
			AND CAST(fromts AS sql_date) = curdate() ) --------------------------
GROUP BY c.stocknumber


-- SELECT COUNT(*)
--a.VehicleInventoryItemID, stocknumber, status, a.fromts
--VehicleInventoryItemID, status AS BodyStatus, thruts
DROP TABLE #afton;
SELECT stocknumber
INTO #afton
    FROM VehicleInventoryItemStatuses a
	INNER JOIN VehicleInventoryItems  b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
    WHERE a.category = 'BodyReconProcess'
    AND a.status = 'BodyReconProcess_InProcess'
    AND cast(a.thruTS AS SQL_DATE) = '04/27/2022'
		
SELECT * FROM #afton a full OUTER JOIN #jon b on a.stocknumber = b.stocknumber		
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- sent to afton 4/28 11:00AM
-- detail

-- this totally rocks it
-- DROP TABLE #wtf
DECLARE @date date;
@date = curdate() - 1;
DROP TABLE #wtf;
SELECT thedate, 
  case when name = 'Honda Cartiva' then 'Honda' else name END AS store, 
  value,
  SUM(CASE WHEN description IS NOT NULL THEN 1 ELSE 0 END) AS Completed
INTO #wtf  
FROM (  
  SELECT d.thedate, b.*
  FROM dds.day d, (
    SELECT b.name, a.value, a.partyid
    FROM partycollections a
    INNER JOIN organizations b ON a.partyid = b.partyid
    WHERE b.name <> 'Crookston'
      AND a.typ = 'PartyCollection_AppearanceReconItem'
      AND a.ThruTS IS NULL) b
  WHERE d.thedate = @date) x
LEFT JOIN (
  SELECT trim(cast(b.description AS sql_varchar)) AS Description, CAST(a.completets AS sql_date) AS FinishDate, c.owninglocationid
  FROM AuthorizedReconItems a
  LEFT JOIN VehicleReconItems b ON a.VehicleReconItemID = b.VehicleReconItemID 
  LEFT JOIN VehicleInventoryItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID 
  WHERE a.status = 'AuthorizedReconItem_Complete'
    AND CAST(a.CompleteTS AS sql_date) = @date) y ON x.thedate = y.finishdate
  AND x.value = TRIM(y.description) collate ads_default_ci
  AND x.partyid = y.owninglocationid
GROUP BY x.thedate, x.name, x.value;  

SELECT * FROM VehicleReconItems WHERE VehicleInventoryItemID = 'f66f81fd-8710-44f2-9f8c-0a7aa62d2ec7'

SELECT typ, COUNT(*)
FROM VehicleReconItems
WHERE typ LIKE '%Appearance%'
GROUP BY typ

-- sent to afton 4/28 11:00AM
-- DROP TABLE #detail;
SELECT c.stocknumber
-- INTO #detail
FROM AuthorizedReconItems a
JOIN VehicleReconItems b on a.VehicleReconItemID = b.VehicleReconItemID  
  AND b.typ LIKE '%Appearance%'
	AND b.description not like '%Pricing Rinse%'
JOIN VehicleInventoryItems c on b.VehicleInventoryItemID = c.VehicleInventoryItemID 
  AND c.thruts IS NULL 
WHERE CAST(a.completets AS sql_date) = curdate() -------------------------------
	AND EXISTS ( 
	  SELECT 1
		FROM VehicleInventoryItemStatuses 
		WHERE VehicleInventoryItemID = c.VehicleInventoryItemID
		  AND status = 'AppearanceReconProcess_NoIncompleteReconItems'
			AND CAST(fromts AS sql_date) = curdate()) --------------------------------
GROUP BY stocknumber

select * FROM typcategories WHERE typ LIKE '%Appearance%'

-- G44397A  typ: AppearanceReconItem_Other  Headlight buff
SELECT * FROM VehicleInventoryItems WHERE VehicleInventoryItemID = 'f66f81fd-8710-44f2-9f8c-0a7aa62d2ec7'

SELECT * FROM VehicleReconItems WHERE VehicleInventoryItemID = 'f66f81fd-8710-44f2-9f8c-0a7aa62d2ec7'

SELECT * FROM AuthorizedReconItems WHERE VehicleReconItemid = '2d53643f-2532-004f-9e5a-5da9dec91582'

SELECT typ, COUNT(*)
FROM VehicleReconItems
WHERE typ LIKE '%Appearance%'
GROUP BY typ

SELECT a.stocknumber, b.typ, LEFT(b.description, 10), c.category, c.status, c.fromts, c.thruts
FROM VehicleInventoryItems a
JOIN VehicleReconItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
  AND b.typ = 'AppearanceReconItem_Other'
LEFT JOIN VehicleInventoryItemStatuses c on a.VehicleInventoryItemID= c.VehicleInventoryItemID 
  AND c.status LIKE '%Appear%'	
	AND c.status <> 'AppearanceReconProcess_NoIncompleteReconItems'
WHERE a.thruts IS NULL
  AND c.thruts IS NULL 
	
SELECT top 100 * 
FROM VehicleReconItems
WHERE typ LIKE '%Appearance%'
ORDER BY ts desc	