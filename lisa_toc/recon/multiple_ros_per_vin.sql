﻿select count(distinct vin) -- 1058
from toc.bs_ros_1st_quarter

select vin
from (
	select vin, ro 
	from toc.bs_ros_1st_quarter
	where full_name not in ('BODY SHOP TIME ADJ', 'carry in')
	group by vin, ro) a
group by vin
having count(*) > 1

select 178/1058.0 -- 16.8%

-- here are the vins with multiple ros
select a.vin, a.ro, a.open_date, a.close_date
from toc.bs_ros_1st_quarter a
join (
	select vin
	from (
		select vin, ro 
		from toc.bs_ros_1st_quarter
		where full_name not in ('BODY SHOP TIME ADJ', 'carry in')
		group by vin, ro) a
	group by vin
	having count(*) > 1) b on a.vin = b.vin
group by a.vin, a.ro, a.open_date, a.close_date	
order by a.vin

select a.ro, a.vin, a.open_date, a.close_date, a.full_name
from toc.bs_ros_1st_quarter a
join (
	select a.vin, a.ro, a.open_date, a.close_date
	from toc.bs_ros_1st_quarter a
	join (
		select vin
		from (
			select vin, ro 
			from toc.bs_ros_1st_quarter
			 where full_name not in ('BODY SHOP TIME ADJ', 'carry in')
			group by vin, ro) a
		group by vin
		having count(*) > 1) b on a.vin = b.vin
	group by a.vin, a.ro, a.open_date, a.close_date	) b on a.ro = b.ro
order by a.vin, a.ro, a.line	

-- this doesn't show what what i expected or wanted
select 
  count(distinct a.vin) filter (where a.full_name = 'inventory') as recon,  -- 109
  count(distinct a.vin) filter (where a.full_name <> 'inventory') as cust  -- 92
from toc.bs_ros_1st_quarter a
join (
	select a.vin, a.ro, a.open_date, a.close_date
	from toc.bs_ros_1st_quarter a
	join (
		select vin
		from (
			select vin, ro 
			from toc.bs_ros_1st_quarter
			 where full_name not in ('BODY SHOP TIME ADJ', 'carry in')
			group by vin, ro) a
		group by vin
		having count(*) > 1) b on a.vin = b.vin
	group by a.vin, a.ro, a.open_date, a.close_date	) b on a.ro = b.ro



select a.vin, a.ro, min(a.open_date) as min_open, max(a.close_date) as max_close, full_name
from toc.bs_ros_1st_quarter a
join (
	select vin
	from (
		select vin, ro 
		from toc.bs_ros_1st_quarter
		 where full_name not in ('BODY SHOP TIME ADJ', 'carry in')
		group by vin, ro) a
	group by vin
	having count(*) > 1) b on a.vin = b.vin
group by a.vin, a.ro,full_name	
order by full_name

records with cust and inventory
1G1YY25W595105274, traded in w/insurance claim (deer hit), RO 18088998 a separate visit after sale of repaired vehicle
1G1ZD5ST4JF240398 2 separate visits


select a.vin, a.ro, min(a.open_date) as min_open, max(a.close_date) as max_close, full_name
from toc.bs_ros_1st_quarter a
join (
	select vin
	from (
		select vin, ro 
		from toc.bs_ros_1st_quarter
		 where full_name not in ('BODY SHOP TIME ADJ', 'carry in')
		group by vin, ro) a
	group by vin
	having count(*) > 1) b on a.vin = b.vin
group by a.vin, a.ro,full_name	


	select a.vin, a.ro, a.open_date, a.close_date, daterange(a.open_date, a.close_date, '[]')
	from toc.bs_ros_1st_quarter a limit 10
	join (
		select vin
		from (
			select vin, ro 
			from toc.bs_ros_1st_quarter
			 where full_name not in ('BODY SHOP TIME ADJ', 'carry in')
			group by vin, ro) a
		group by vin
		having count(*) > 1) b on a.vin = b.vin
	group by a.vin, a.ro, a.open_date, a.close_date