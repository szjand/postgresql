﻿-----------------------------------------------------
--< 03/30/222
-----------------------------------------------------
gregs wants totals per vehicle, then avg per vehicle
c: 1029 vehicles
try to be more finely grained
--16248
-- 		select aa.*, cc.ro, cc.open_date, cc.close_date, dd.service_type_code, cc.flag_hours
-- one row per vehicle
		select aa.stocknumber, aa.vin, 
		  coalesce(sum(cc.flag_hours) filter (where dd.service_type_code = 'RE'), 0) as detail,
		  coalesce(sum(cc.flag_hours) filter (where dd.service_type_code = 'BS'), 0) as body,
		  coalesce(sum(cc.flag_hours) filter (where dd.service_type_code not in ('RE','BS')), 0) as mech,
		  array_agg(distinct ro order by ro)
		from ( 
			select a.stocknumber, b.vin, a.fromts::date as from_date, a.thruts::date as thru_date
			from ads.ext_vehicle_inventory_items a
			join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
			join (
				select distinct vehicleinventoryitemid 
				from ads.ext_vehicle_inventory_item_statuses
				where status in ('BodyReconProcess_InProcess','MechanicalReconProcess_InProcess','AppearanceReconProcess_InProcess')
					and thruts::date between current_date - 180 and current_date) c on a.vehicleinventoryitemid = c.vehicleinventoryitemid) aa
		join dds.dim_vehicle bb on aa.vin = bb.vin	
		join dds.fact_repair_order cc on bb.vehicle_key = cc.vehicle_key
			and cc.open_date between aa.from_date and aa.thru_date
			and cc.flag_hours > 0
    join dds.dim_service_type dd on cc.service_type_key = dd.service_type_key	
		group by aa.stocknumber, aa.vin

-- totals and averages
select count(*) as units, sum(detail) as total_detail, sum(body) as total_body, sum(mech) as total_mech,
	round(avg(detail), 2) as avg_detail, round(avg(body), 2) as avg_body, round(avg(mech), 2) as avg_mech
from (
		select aa.stocknumber, aa.vin, 
		  coalesce(sum(cc.flag_hours) filter (where dd.service_type_code = 'RE'), 0) as detail,
		  coalesce(sum(cc.flag_hours) filter (where dd.service_type_code = 'BS'), 0) as body,
		  coalesce(sum(cc.flag_hours) filter (where dd.service_type_code not in ('RE','BS')), 0) as mech
		from ( -- 1010
			select a.stocknumber, b.vin, a.fromts::date as from_date, a.thruts::date as thru_date
			from ads.ext_vehicle_inventory_items a
			join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
			join (
				select distinct vehicleinventoryitemid 
				from ads.ext_vehicle_inventory_item_statuses
				where status in ('BodyReconProcess_InProcess','MechanicalReconProcess_InProcess','AppearanceReconProcess_InProcess')
					and thruts::date between current_date - 180 and current_date) c on a.vehicleinventoryitemid = c.vehicleinventoryitemid) aa
		join dds.dim_vehicle bb on aa.vin = bb.vin	
		join dds.fact_repair_order cc on bb.vehicle_key = cc.vehicle_key
			and cc.open_date between aa.from_date and aa.thru_date
			and cc.flag_hours > 0
    join dds.dim_service_type dd on cc.service_type_key = dd.service_type_key	
		group by aa.stocknumber, aa.vin
) x

-- look up individual vehicles
select aa.*, cc.ro, cc.open_date, cc.close_date, dd.service_type_code, cc.flag_hours
from ( 
	select a.stocknumber, b.vin, a.fromts::date as from_date, a.thruts::date as thru_date
	from ads.ext_vehicle_inventory_items a
	join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
	join (
		select distinct vehicleinventoryitemid 
		from ads.ext_vehicle_inventory_item_statuses
		where status in ('BodyReconProcess_InProcess','MechanicalReconProcess_InProcess','AppearanceReconProcess_InProcess')
			and thruts::date between current_date - 180 and current_date) c on a.vehicleinventoryitemid = c.vehicleinventoryitemid) aa
join dds.dim_vehicle bb on aa.vin = bb.vin	
join dds.fact_repair_order cc on bb.vehicle_key = cc.vehicle_key
	and cc.open_date between aa.from_date and aa.thru_date
	and cc.flag_hours > 0
join dds.dim_service_type dd on cc.service_type_key = dd.service_type_key	
where aa.stocknumber = 'G41947B' order by service_type_code, ro
-----------------------------------------------------
--< 03/30/222
-----------------------------------------------------
-----------------------------------------------------
--/> 03/30/222
-----------------------------------------------------
-- lots of especially honda with 3 ros, eg 16, 28, 18
drop table if exists the_base;
create temp table the_base as
select stocknumber, vin, 
  sum(flag_hours) filter (where ro in ('16','28')) as mech_flag,
  sum(flag_hours) filter (where ro = '18') as body_flag
from (  
	select stocknumber, vin, left(ro, 2) as ro, sum(flag_hours) as flag_hours
	from (  --16248
		select aa.*, cc.ro, cc.open_date, cc.close_date, cc.flag_hours
		from ( -- 1010
			select a.stocknumber, b.vin, a.fromts::date as from_date, a.thruts::date as thru_date
			from ads.ext_vehicle_inventory_items a
			join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
			join (
				select distinct vehicleinventoryitemid 
				from ads.ext_vehicle_inventory_item_statuses
				where status in ('BodyReconProcess_InProcess','MechanicalReconProcess_InProcess')
					and thruts::date between current_date - 180 and current_date) c on a.vehicleinventoryitemid = c.vehicleinventoryitemid) aa
		join dds.dim_vehicle bb on aa.vin = bb.vin	
		join dds.fact_repair_order cc on bb.vehicle_key = cc.vehicle_key
			and cc.open_date between aa.from_date and aa.thru_date
			and cc.flag_hours > 0) dd
	group by stocknumber, vin, left(ro, 2)) ee
group by stocknumber, vin;



-- these are the numbers sent to greg
select sum(mech_flag), count(*), sum(mech_flag)/count(*)  -- 11.5 hours
from the_base
where mech_flag is not null

select sum(body_flag), count(*), sum(body_flag)/count(*)  -- 15.6 hours
from the_base
where body_flag is not null

-- he thinks the numbers are high, sent him this, for checking individual vehicles/ros
		select aa.*, cc.ro, cc.open_date, cc.close_date, cc.flag_hours,
		  case
		    when left(ro,2) = '18' then 'body'
		    else 'mech'
		  end as dept
		from ( -- 1010
			select a.stocknumber, b.vin, a.fromts::date as from_date, a.thruts::date as thru_date
			from ads.ext_vehicle_inventory_items a
			join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
			join (
			select distinct vehicleinventoryitemid 
			from ads.ext_vehicle_inventory_item_statuses
			where status in ('BodyReconProcess_InProcess','MechanicalReconProcess_InProcess')
				and thruts::date between current_date - 180 and current_date) c on a.vehicleinventoryitemid = c.vehicleinventoryitemid) aa
		join dds.dim_vehicle bb on aa.vin = bb.vin	
		join dds.fact_repair_order cc on bb.vehicle_key = cc.vehicle_key
			and cc.open_date between aa.from_date and aa.thru_date
			and cc.flag_hours > 0

-- 03/30/222
gregs wants totals per vehicle, then avg per vehicle