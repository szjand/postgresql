﻿/*
fact_gl: control of stocknumber vs control of ro ??
here's a sample usiung a body shop ro, don't think it is any help
select a.*, c.journal_code
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
  and account = '124000'
join fin.dim_journal c on a.journal_key = c.journal_key
where a.post_status = 'Y'
and a.control = 'G43569BC'

select a.*, b.account, b.description, b.account_type, c.journal_code
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
join fin.dim_journal c on a.journal_key = c.journal_key
where a.post_status = 'Y'
and a.control = '18091748'
order by b.account
*/


drop table if exists jon.walks cascade;
create unlogged table jon.walks as
select d.fullname as walker, b.vehicleinventoryitemid, a.vehicle_walk_ts::date as walk_date, b.stocknumber as stock_number, 
  c.vin, b.fromts::date as from_Date, b.thruts::date as thru_date, c.yearmodel as model_year, c.make, c.model, c.trim as trim_level
from ads.ext_vehicle_Walks a
join ads.ext_vehicle_inventory_items b on a.vehicle_inventory_item_id::citext = b.vehicleinventoryitemid
  and b.thruts::date < current_date  -- sold vehicles only
join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
left join ads.ext_people d on a.vehicle_walker_id::citext = d.partyid
where a.vehicle_walk_ts::date > '12/31/2021';
create index on jon.walks(vin);
create index on jon.walks(from_date);
create index on jon.walks(thru_date);
create index on jon.walks(stock_number);
create index on jon.walks(vehicleinventoryitemid);
comment on TABLE jon.walks is 'populated in files sql\postgresql\lisa_toc\recon\reconcile_recon_vs_estimates.sql & reconcile_recon_vs_est_mech.sql';

select * from jon.walks

select a.control, a.doc as doc, a.ref, b.journal_code as journal, 
	c.account, c.description, a.amount, d.description
from fin.fact_gl a 
join jon.walks aa on a.control = aa.stock_number
join fin.dim_journal b on a.journal_key = b.journal_key
  and b.journal_code in ('SVI','SWA','SCA','POT')
join fin.dim_account c on a.account_key = c.account_key
join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
where a.post_Status = 'Y'
  and a.doc not like '18%' 
  and a.ref not like '18%'
  and a.doc not like '8%'
  and a.ref not like '8%'
  and a.doc not like '19%'
  and a.ref not like '19%'
  and a.control = 'G45175P'
-- 
--   
-- -- can't just depend on the ro number like with body, think this table is going to need more detail
-- -- need to separate out inspection, detail and mech, i think
-- drop table if exists jon.accounting_mech_recon_detail cascade;
-- create unlogged table jon.accounting_mech_recon_detail as
-- select a.control, a.doc as doc, a.ref, b.journal_code as journal, 
-- 	c.account, c.description as account_description, a.amount, d.description as trans_description
-- from fin.fact_gl a 
-- join jon.walks aa on a.control = aa.stock_number
-- join fin.dim_journal b on a.journal_key = b.journal_key
--   and b.journal_code in ('SVI','SWA','SCA','POT')
-- join fin.dim_account c on a.account_key = c.account_key
-- join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
-- where a.post_Status = 'Y'
--   and a.doc not like '18%' 
--   and a.ref not like '18%'
--   and a.doc not like '8%'
--   and a.ref not like '8%'
--   and a.doc not like '19%'
--   and a.ref not like '19%'
-- limit 200  

-- limit to trans where doc is an ro
drop table if exists jon.ros_from_accounting_mech_recon cascade;
create unlogged table jon.ros_from_accounting_mech_recon as
select a.control, a.doc
from fin.fact_gl a 
join jon.walks aa on a.control = aa.stock_number
join dds.fact_repair_order c on a.doc = c.ro
where a.post_Status = 'Y'
  and a.doc not like '18%' 
  and a.ref not like '18%'
  and a.doc not like '8%'
  and a.ref not like '8%'
  and a.doc not like '19%'
  and a.ref not like '19%'
group by a.control, a.doc;
create unique index on jon.ros_from_accounting_mech_recon(control,doc); 

select * from jon.ros_from_accounting_mech_recon where control = 'G43865B'

-- 9/24/22 this query doesn't make any sense
drop table if exists jon.ro_accounting cascade;
create unlogged table jon.ro_accounting as
select a.control, a.amount, b.account, b.account_type, b.description, b.department, sum(a.amount) over (partition by department)
from fin.fact_gl a
join jon.ros_from_accounting_mech_recon aa on a.control = aa.control
join fin.dim_account b on a.account_key = b.account_key
  and b.account_type = 'sale'
where a.post_status = 'Y'  ;

select * from jon.ro_accounting limit 100

select a.control, a.amount, b.account, b.account_type, b.description, b.department, sum(a.amount) over (partition by department)
select *
from fin.fact_gl a
join jon.ros_from_accounting_mech_recon aa on a.control = aa.control
join fin.dim_account b on a.account_key = b.account_key
  and b.account_type = 'sale'
where a.post_Status = 'Y'
  and a.control = 'G43865B'  

/*
this looks more like what i need than 
select * from jon.accounting_mech_recon_detail where control = 'G45175p'
-- inspection ro for G45175p
select a.control, a.amount, b.account, b.account_type, b.description, b.department, sum(a.amount) over (partition by department)
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
where a.control = '16532647'
  and b.account_type = 'sale'

-- detail (buff) ro for G45175p
select a.control, a.amount, b.account, b.account_type, b.description, b.department, sum(a.amount) over (partition by department)
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
where a.control = '16533233'
  and b.account_type = 'sale'  

-- detail (ozone) ro for G45175p
select a.control, a.amount, b.account, b.account_type, b.description, b.department, sum(a.amount) over (partition by department)
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
where a.control = '16534111'
  and b.account_type = 'sale'  
  
-- mech ro for G45175p
select a.control, a.amount, b.account, b.account_type, b.description, b.department, sum(a.amount) over (partition by department)
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
where a.control = '16533052'
  and b.account_type = 'sale' 

-- detail (pictures) ro for G45175p
select a.control, a.amount, b.account, b.account_type, b.description, b.department, sum(a.amount) over (partition by department)
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
where a.control = '16534126'
  and b.account_type = 'sale' 

-- detail (wash and wax) ro for G45175p
select a.control, a.amount, b.account, b.account_type, b.description, b.department, sum(a.amount) over (partition by department)
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
where a.control = '16536117'
  and b.account_type = 'sale'  
*/
  
-- table now has walk item detail, with a vehicle_total attribute
-- added the category to the description
drop table if exists jon.mech_recon_est cascade;
create unlogged table jon.mech_recon_est as
SELECT aa.stock_number, a.vehicleinventoryitemid,
	c.description || ':' || a.description as description, a.totalpartsamount as parts, a.laboramount as labor,
	totalpartsamount + laboramount as line_total,
	sum(totalpartsamount + laboramount) over (partition by a.vehicleinventoryitemid) as vehicle_total
FROM ads.ext_Vehicle_Recon_Items a
join jon.walks aa on a.vehicleinventoryitemid = aa.vehicleinventoryitemid
JOIN ads.ext_Authorized_Recon_Items b ON a.VehicleReconItemID = b.VehicleReconItemID
	AND b.Status = 'AuthorizedReconItem_Complete'
left join ads.ext_typ_descriptions c on a.typ = c.typ	
WHERE a.typ like 'mech%';
create index on jon.mech_recon_est(stock_number);  

select * from jon.mech_recon_est limit 100

select * from ads.ext_Vehicle_recon_items where vehicleinventoryitemid = '002d97c4-f30a-4cbd-8b15-342de7cdf4c9'

select * from ads.ext_typ_descriptions where typ like 'mech%'