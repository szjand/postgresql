﻿-- go with only the first instance of pricing buffer
-- best shot at the whole system on the vehicle
select a.stocknumber, a.vehicleinventoryitemid, count(*)
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and b.status = 'RMFlagPB_PricingBuffer'
  and b.fromts::date between '09/01/2021' and '03/31/2022'
group by a.stocknumber, a.vehicleinventoryitemid
having count(*) > 1

-- pricing buffer ts becomes the end of the timeframe
-- when the recon is all completed prior to the vehicle being pulled, 
-- the pricing buffer flag does not manifest until it is pulled, so that delay
-- could be months, so, need to add the flr ts
drop table if exists pb_vehicles cascade;
create temp table pb_vehicles as
select aa.*, max(bb.fromts) as flr_ts
from (
	select stocknumber, vehicleinventoryitemid, fromts as pb_ts
	from (
		select a.stocknumber, a.vehicleinventoryitemid,b.fromts, row_number() over (partition by a.stocknumber order by b.fromts) as seq
		from ads.ext_vehicle_inventory_items a
		join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
			and b.status = 'RMFlagPB_PricingBuffer'
			and b.fromts::date between '01/01/2021' and '03/31/2022' where stocknumber = 'G39265XA') c
	where seq = 1) aa
left join ads.ext_vehicle_inventory_item_statuses bb on aa.vehicleinventoryitemid = bb.vehicleinventoryitemid
  and bb.status = 'RMFlagFLR_FrontLineReady'
  and bb.fromts <= aa.pb_ts
group by aa.stocknumber, aa.vehicleinventoryitemid, aa.pb_ts  

-- 04/07 the basic set neeeds to be based on the vehicles not status timestamps
time to start a v2

-- vehicles for which there is no wip prior to pricing buffer
-- some looks legitimate, some i don't know what is going on
-- eg H14950A, i don't know what to make of the status sequence, no wip 
Sales buffer flag					Sales buffer flag								10/20/2021 6:33:27 PM	10/21/2021 10:46:07 AM	Samuel Foster
Available flag						Available flag									10/19/2021 9:35:00 AM	10/21/2021 10:46:57 AM	RJ Erickson
Pricing Buffer flag				Pricing Buffer flag							10/19/2021 7:50:14 AM	10/19/2021 9:35:00 AM	Haley Kloety
Front line ready flag			Front line ready flag						10/19/2021 7:50:14 AM	10/19/2021 9:35:00 AM	Haley Kloety
Body recon process				Body recon no incomplete				10/19/2021 7:50:14 AM	10/21/2021 10:46:59 AM	Haley Kloety
Appearance recon process	Appearance recon no incomplete	10/15/2021 2:44:50 PM	10/21/2021 10:46:59 AM	Hannah Appleby
Dispatched to body				Dispatched to body							10/14/2021 10:28:23 AM	10/19/2021 7:50:14 AM	RJ Erickson

select * 
from pb_vehicles aa
left join (
	select a.stocknumber, a.vehicleinventoryitemid, min(b.fromts) as wip_from_ts
	from pb_vehicles a
	join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
		and b.status like '%inprocess'
		and b.fromts < a.pb_ts
	group by a.stocknumber, a.vehicleinventoryitemid) bb on aa.vehicleinventoryitemid = bb.vehicleinventoryitemid

-- ok, i am going to create a subset where all 3 depts touch the vehicle prior to prricing buffer  

select * -- this yields 568 vehicles
from pb_vehicles a
where exists (
  select 1
  from ads.ext_Vehicle_inventory_item_statuses
  where vehicleinventoryitemid = a.vehicleinventoryitemid
    and status = 'AppearanceReconProcess_InProcess'
    and fromts < a.pb_ts)
and exists (
  select 1
  from ads.ext_Vehicle_inventory_item_statuses
  where vehicleinventoryitemid = a.vehicleinventoryitemid
    and status = 'BodyReconProcess_InProcess'
    and fromts < a.pb_ts)
and exists (
  select 1
  from ads.ext_Vehicle_inventory_item_statuses
  where vehicleinventoryitemid = a.vehicleinventoryitemid
    and status = 'MechanicalReconProcess_InProcess'
    and fromts < a.pb_ts)

-- select a.*, b.status, b.fromts, b.thruts, tstzrange(b.fromts,b.thruts), c.status, c.fromts, c.thruts, d.status, d.fromts,d.thruts
-- from pb_vehicles a
-- join ads.ext_Vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
--   and b.fromts < a.pb_ts
--   and b.status = 'AppearanceReconProcess_InProcess'
-- join ads.ext_Vehicle_inventory_item_statuses c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
--   and c.fromts < a.pb_ts
--   and c.status = 'BodyReconProcess_InProcess'
-- join ads.ext_Vehicle_inventory_item_statuses d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
--   and d.fromts < a.pb_ts
--   and d.status = 'MechanicalReconProcess_InProcess'
-- 
-- 
-- select distinct status 
-- from ads.ext_vehicle_inventory_item_statuses
-- where vehicleinventoryitemid = '021ab67e-5c97-44a7-bd1a-fdf5682c7add'
--   and status like '%inprocess'

-- -- 04/06
-- -- need to get the beginning of the time frame
-- 
-- select a.*, b.status, b.fromts
-- from pb_vehicles a
-- join ads.ext_Vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
--   and b.fromts < a.pb_ts
--   and b.status = 'AppearanceReconProcess_InProcess'
-- union
-- select a.*, b.status, b.fromts
-- from pb_vehicles a
-- join ads.ext_Vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
--   and b.fromts < a.pb_ts
--   and b.status = 'BodyReconProcess_InProcess'  
-- union
-- select a.*, b.status, b.fromts
-- from pb_vehicles a
-- join ads.ext_Vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
--   and b.fromts < a.pb_ts
--   and b.status = 'MechanicalReconProcess_InProcess'  
-- order by stocknumber

-- select stocknumber, vehicleinventoryitemid, status, tstzrange(fromts,thruts, '[]') as wip_to_pb
-- from (
-- 	select *, row_number() over (partition by stocknumber order by fromts) as seq 
-- 	from (-- this actually give me all the data in rows
-- 		select a.*, b.status, b.fromts, b.thruts
-- 		from pb_vehicles a
-- 		join ads.ext_Vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
-- 			and b.fromts < a.pb_ts
-- 			and b.status = 'AppearanceReconProcess_InProcess'
-- 		join ads.ext_Vehicle_inventory_item_statuses c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
-- 			and c.fromts < a.pb_ts
-- 			and c.status = 'BodyReconProcess_InProcess'
-- 		join ads.ext_Vehicle_inventory_item_statuses d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
-- 			and d.fromts < a.pb_ts
-- 			and d.status = 'MechanicalReconProcess_InProcess'
-- 		union
-- 		select a.*, c.status, c.fromts, c.thruts -- , d.status, d.fromts,d.thruts
-- 		from pb_vehicles a
-- 		join ads.ext_Vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
-- 			and b.fromts < a.pb_ts
-- 			and b.status = 'AppearanceReconProcess_InProcess'
-- 		join ads.ext_Vehicle_inventory_item_statuses c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
-- 			and c.fromts < a.pb_ts
-- 			and c.status = 'BodyReconProcess_InProcess'
-- 		join ads.ext_Vehicle_inventory_item_statuses d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
-- 			and d.fromts < a.pb_ts
-- 			and d.status = 'MechanicalReconProcess_InProcess'
-- 		union
-- 		select a.*,d.status, d.fromts,d.thruts
-- 		from pb_vehicles a
-- 		join ads.ext_Vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
-- 			and b.fromts < a.pb_ts
-- 			and b.status = 'AppearanceReconProcess_InProcess'
-- 		join ads.ext_Vehicle_inventory_item_statuses c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
-- 			and c.fromts < a.pb_ts
-- 			and c.status = 'BodyReconProcess_InProcess'
-- 		join ads.ext_Vehicle_inventory_item_statuses d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
-- 			and d.fromts < a.pb_ts
-- 			and d.status = 'MechanicalReconProcess_InProcess') x) y
-- where seq = 1			
-- order by stocknumber  

-- figure out the gaps and islands
-- after the revelation about front line ready vs pricing buffer, change these statuses to be < flr_ts
drop table if exists test_1;
create temp table test_1 as
-- select *, row_number() over (partition by stocknumber order by fromts) as seq, thruts - fromts
select stocknumber, vehicleinventoryitemid, pb_ts, flr_ts, (left(status, position('Recon' in status) - 1))::citext as dept, 
	fromts, thruts, row_number() over (partition by stocknumber order by fromts) as seq
from (-- this actually give me all the data in rows
	select a.*, b.status, b.fromts, b.thruts
	from pb_vehicles a
	join ads.ext_Vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
		and b.fromts < a.flr_ts -- a.pb_ts
		and b.status = 'AppearanceReconProcess_InProcess'
	join ads.ext_Vehicle_inventory_item_statuses c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
		and c.fromts < a.flr_ts -- a.pb_ts
		and c.status = 'BodyReconProcess_InProcess'
	join ads.ext_Vehicle_inventory_item_statuses d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
		and d.fromts < a.flr_ts -- a.pb_ts
		and d.status = 'MechanicalReconProcess_InProcess'
	union
	select a.*, c.status, c.fromts, c.thruts -- , d.status, d.fromts,d.thruts
	from pb_vehicles a
	join ads.ext_Vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
		and b.fromts < a.flr_ts -- a.pb_ts
		and b.status = 'AppearanceReconProcess_InProcess'
	join ads.ext_Vehicle_inventory_item_statuses c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
		and c.fromts < a.flr_ts -- a.pb_ts
		and c.status = 'BodyReconProcess_InProcess'
	join ads.ext_Vehicle_inventory_item_statuses d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
		and d.fromts < a.flr_ts -- a.pb_ts
		and d.status = 'MechanicalReconProcess_InProcess'
	union
	select a.*,d.status, d.fromts,d.thruts
	from pb_vehicles a
	join ads.ext_Vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
		and b.fromts < a.flr_ts -- a.pb_ts
		and b.status = 'AppearanceReconProcess_InProcess'
	join ads.ext_Vehicle_inventory_item_statuses c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
		and c.fromts < a.flr_ts -- a.pb_ts
		and c.status = 'BodyReconProcess_InProcess'
	join ads.ext_Vehicle_inventory_item_statuses d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
		and d.fromts < a.flr_ts -- a.pb_ts
		and d.status = 'MechanicalReconProcess_InProcess') x
where stocknumber in ('G37242AZB','G37242AZA','G35731XZA','G41272B')	-- more than 3 wip sessions	

select *, lead(fromts, 1) over (partition by stocknumber order by seq) as next_start
from test_1
order by stocknumber, seq

G38648RA (G39248RA) recon complete on 10/19/20, but it did not go into pricing buffer until 3/15/21, because it was not pulled until then

select * from ads.ext_vehicle_inventory_item_statuses where vehicleinventoryitemid = '624eb092-0166-47e7-9a82-34910e225ce8'

-- can be multiple flr before pricing buffer, need just the max, introducing flr should not add multiple rows per vehicle
-- select stocknumber from (
select *, pb_ts - flr_ts from (
select a.*, max(b.fromts) as flr_ts
from pb_vehicles a
left join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and b.status = 'RMFlagFLR_FrontLineReady'
  and b.fromts <= a.pb_ts
group by a.stocknumber, a.vehicleinventoryitemid, a.pb_ts  
) x
-- ) x group by stocknumber having count(*) > 1
-- where a.stocknumber in ('G38648RA','G39248RA')  

-- timestamp diff in decimal hours
select a.*, extract(epoch from thruts - fromts)/(60*60) as wip, 
  next_start - thruts as wait_dif, extract(epoch from next_start - thruts)/(60*60) as wait
from (
select *, lead(fromts, 1) over (partition by stocknumber order by seq) as next_start -- coalesce(lead(fromts, 1) over (partition by stocknumber order by seq), pb_ts) as next_start
from test_1) a
order by stocknumber, seq



select a.*, extract(epoch from thruts - fromts)/(60*60) as wip
--   next_start - thruts as wait_dif, extract(epoch from next_start - thruts)/(60*60) as wait
from (
select *, coalesce(lead(fromts, 1) over (partition by stocknumber order by seq), pb_ts) as next_start
from test_1
where stocknumber = 'G39265XA') a
order by stocknumber, seq


select a.stocknumber, 
  sum(extract(epoch from thruts - fromts)/(60*60)) filter (where dept = 'mechanical') as mech_wip, 
  sum(extract(epoch from thruts - fromts)/(60*60)) filter (where dept = 'body') as body_wip, 
  sum(extract(epoch from thruts - fromts)/(60*60)) filter (where dept = 'appearance') as app_wip, 
  sum(extract(epoch from next_start - thruts)/(60*60)) as wait
from (
select *, coalesce(lead(fromts, 1) over (partition by stocknumber order by seq), flr_ts) as next_start
from test_1
where stocknumber = 'G39265XA') a
group by stocknumber
order by stocknumber

G39265XA wait time of 4010 does not make sense
the problem is that i am capturing pb status between the dates '01/01/2021' and '03/31/2022', 
so in this case, there are pricing buffers before those dates
so i need to base this on a set of vehicles