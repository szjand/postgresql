﻿/*

*** this file is body shop recon only ****

we have, all based on walks performed in 2022 on vehicles sold as of the date all this was run (8/20/22):
accounting: jon.accounting_body_recon, from fin.fact_gl where journal in svi,swa,sca,pot and ref like 18% (body shop ros)
ros: derived from accounting(fin.fact_gl) attribute ref
		 populated with data from dds.fact_repair_order
		 data at line level (jon.body_shop_recon_ro_lines) and at ro level (jon.body_shop_recon_ro_totals)
tool estimate: from table VehicleReconItems


*/


-- 1640 walks
-- only want sold vehicles
-- need the walker, add vehicle info,
drop table if exists jon.walks cascade;
create unlogged table jon.walks as
select d.fullname as walker, b.vehicleinventoryitemid, a.vehicle_walk_ts::date as walk_date, b.stocknumber as stock_number, 
  c.vin, b.fromts::date as from_Date, b.thruts::date as thru_date, c.yearmodel as model_year, c.make, c.model, c.trim as trim_level
from ads.ext_vehicle_Walks a
join ads.ext_vehicle_inventory_items b on a.vehicle_inventory_item_id::citext = b.vehicleinventoryitemid
  and b.thruts::date < current_date  -- sold vehicles only
join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
left join ads.ext_people d on a.vehicle_walker_id::citext = d.partyid
where a.vehicle_walk_ts::date > '12/31/2021';
create index on jon.walks(vin);
create index on jon.walks(from_date);
create index on jon.walks(thru_date);
create index on jon.walks(stock_number);
create index on jon.walks(vehicleinventoryitemid);



drop table if exists jon.accounting_body_recon cascade;
create unlogged table jon.accounting_body_recon as
select a.control, array_agg(a.doc) as doc, array_agg(a.ref) as ref, array_agg(distinct b.journal_code) as journal, 
	array_agg(distinct c.account) as account, sum(a.amount) as body_recon
from fin.fact_gl a 
join jon.walks aa on a.control = aa.stock_number
join fin.dim_journal b on a.journal_key = b.journal_key
  and b.journal_code in ('SVI','SWA','SCA','POT')
join fin.dim_account c on a.account_key = c.account_key
--   and c.account in ('124000','124100','224000','224100')
where a.post_Status = 'Y'
  and (a.doc like '18%' or a.ref like '18%')
--   and control = 'G45056GA'
group by a.control;
create index on jon.accounting_body_recon(control);
comment on TABLE jon.accounting_body_recon is 'populated in file sql\postgresql\lisa_toc\recon\reconcile_recon_vs_estimates.sql';
select * from jon.accounting_body_recon

-- need detail as explained below, experiment with 18091642
-- there is no additional detail to be found in accounting, so get ro detail
-- opcode/complaint by line, labor by line, parts by line, sublet by line
-- ro_shop_supplies by ro, ro_paint_materials by ro
-- ro_discount, ro_hazardous_materials these 2 are negligible, no discounts, 14 with $2 - $4 hazardous
drop table if exists jon.the_ros cascade;
create unlogged table jon.the_ros as
select distinct stock_number, ro
from (
	select control as stock_number, unnest(ref) as ro
	from jon.accounting_body_recon) a
where left(ro, 2) = '18'
  and length(ro) = 8;
create unique index on jon.the_ros(stock_number,ro);  


drop table if exists jon.the_ros cascade;
create unlogged table jon.the_ros as
select distinct stock_number, ro
from (
	select control as stock_number, 
	  case
	    -- copy pasta errors in accounting, doc is correct, ref has wrong ro for these controls
	    when control in ('H15237B','H15242LA','H15376P','H15284GB','H15260A','H15063A') then unnest(doc)
	    else unnest(ref) 
	  end as ro
	from jon.accounting_body_recon) a
where left(ro, 2) = '18'
  and length(ro) = 8;
create unique index on jon.the_ros(stock_number,ro);  



drop table if exists jon.body_shop_recon_ro_lines cascade;
create unlogged table jon.body_shop_recon_ro_lines as
select aa.stock_number, aa.ro, aa.line, aa.opcode, aa.complaint, coalesce(bb.labor, 0) as labor,
  coalesce(cc.parts, 0) as parts, coalesce(dd.sublet, 0) as sublet
from ( -- opcode/complaint
	select a.stock_number, b.ro, b.line, c.opcode || '::' || c.description as opcode, d.complaint
	from jon.the_ros a
	join dds.fact_repair_order b on a.ro = b.ro
	join dds.dim_opcode c on b.opcode_key = c.opcode_key
	join dds.dim_ccc d on b.ccc_key = d.ccc_key
	group by a.stock_number, b.ro, b.line, c.opcode || '::' || c.description, d.complaint) aa
left join (-- labor
	select a.stock_number, b.ro, b.line, sum(labor_sales)::integer as labor
	from jon.the_ros a
	join dds.fact_repair_order b on a.ro = b.ro
	group by a.stock_number, b.ro, b.line
	having sum(labor_sales) <> 0) bb on aa.ro = bb.ro and aa.line = bb.line and aa.stock_number = bb.stock_number
left join (-- parts
	select aaa.*, sum(bb.ptqty * bb.ptnet):: integer as parts
	from (
		select b.stock_number, a.ro, a.line
		from dds.fact_repair_order a
		join jon.the_ros b on a.ro = b.ro
		group by b.stock_number, a.ro, a.line) aaa
	join arkona.ext_pdptdet bb on aaa.ro = bb.ptinv_
		and aaa.line = bb.ptline	
	group by aaa.stock_number, aaa.ro, aaa.line) cc on aa.ro = cc.ro and aa.line = cc.line and aa.stock_number = cc.stock_number
left join (-- sublet
	select b.stock_number, a.ro, a.line, sum(a.sublet)::integer as sublet
	from dds.fact_repair_order a
	join jon.the_ros b on a.ro = b.ro  
	where a.sublet <> 0
	group by b.stock_number, a.ro, a.line) dd on aa.ro = dd.ro and aa.line = dd.line and aa.stock_number = dd.stock_number;
create unique index on jon.body_shop_recon_ro_lines(stock_number,ro,line);


-- now i need ro level totals
drop table if exists jon.body_shop_recon_ro_totals cascade;
create table jon.body_shop_recon_ro_totals as
select a.*, 
  coalesce(b.paint_materials) as paint_materials,
  coalesce(b.shop_supplies) as shop_supplies,
  coalesce(a.labor, 0)+coalesce(a.parts, 0)+ coalesce(a.sublet, 0)+coalesce(b.paint_materials)+coalesce(b.shop_supplies) as bs_total
from (
select stock_number, ro, sum(labor) as labor, sum(parts) as parts, sum(sublet) as sublet
from jon.body_shop_recon_ro_lines
group by stock_number, ro) a
left join (-- ro level stuff
	select b.stock_number, a.ro, a.ro_shop_supplies::integer as shop_supplies, a.ro_paint_materials::integer as paint_materials
	from dds.fact_repair_order a
	join jon.the_ros b on a.ro = b.ro  
	group by b.stock_number, a.ro, a.ro_shop_supplies, a.ro_paint_materials) b on a.ro = b.ro and a.stock_number = b.stock_number;
create unique index on jon.body_shop_recon_ro_totals(ro)


select * from jon.body_shop_recon_ro_totals where ro = '18088006'





-- table now has walk item detail, with a vehicle_total attribute
drop table if exists jon.body_recon_est cascade;
create unlogged table jon.body_recon_est as
SELECT aa.stock_number, a.vehicleinventoryitemid,
	a.description, a.totalpartsamount as parts, a.laboramount as labor,
	totalpartsamount + laboramount as line_total,
	sum(totalpartsamount + laboramount) over (partition by a.vehicleinventoryitemid) as vehicle_total
FROM ads.ext_Vehicle_Recon_Items a
join jon.walks aa on a.vehicleinventoryitemid = aa.vehicleinventoryitemid
JOIN ads.ext_Authorized_Recon_Items b ON a.VehicleReconItemID = b.VehicleReconItemID
	AND b.Status = 'AuthorizedReconItem_Complete'
WHERE a.typ like 'body%';
create index on jon.body_recon_est(stock_number);  



-- totals by month
select aa.year_month,sum(coalesce(b.body_recon, 0))::integer as recon, sum(coalesce(c.vehicle_total, 0)) as recon_est,
  sum(coalesce(b.body_recon, 0))::integer - sum(coalesce(c.vehicle_total, 0)) as dif
from jon.walks a
join dds.dim_date aa on a.thru_date = aa.the_date
left join jon.accounting_body_recon b on a.stock_number = b.control
left join (
  select vehicleinventoryitemid, vehicle_total
  from jon.body_recon_est
  group by vehicleinventoryitemid, vehicle_total) c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
where a.stock_number not like '%G'
group by aa.year_month
order by year_month
----------------------------------------------------------------------------------------------------------
-- lets looks at some recent bs detail
/*
any chance of an output with detail comparing estimate to actual means the estimate table will have to have more detail
and we will need an ro table
*/
----------------------------------------------------------------------------------------------------------
alright, lets start building the comparison page, show the totals comparison followed by details

walk info
total info
detail info

-- limit to walks performed in august, better chance of folks remembering the vehicle
-- start with totals
-- separate query for details
drop table if exists step_1 cascade;
create temp table step_1 as
select distinct a.stock_number, a.vin, a.walk_date, a.thru_date as sale_date, a.walker, a.model_year || ' ' || a.make || ' ' || a.model || ' ' || a.trim_level as vehicle,
  b.vehicle_total as estimate,
  c.body_recon::integer as accounting,
  c.body_recon::integer - b.vehicle_total as diff
--   d.description, d.parts, d.labor,
--   e.opcode, e.complaint, e.labor, e.parts, e.sublet
from jon.walks a
left join ( -- estimate total
  select stock_number, vehicle_total
  from jon.body_recon_est
  group by stock_number, vehicle_total) b on a.stock_number = b.stock_number
left join jon.accounting_body_recon c on a.stock_number = c.control 
-- left join ( -- estimate details
-- 	select stock_number, description, parts, labor
-- 	from jon.body_recon_est) d on a.stock_number = d.stock_number
-- left join ( -- ro lines
-- 	select stock_number, opcode, complaint, labor, parts, sublet 
-- 	from jon.body_shop_recon_ro_lines
-- 	order by ro, line) e on a.stock_number = e.stock_number 	f
where b.vehicle_total is not null 
  and c.body_recon is not null
  and a.thru_date > '07/31/2022'
--   and a.stock_number = 'G44161P'  
order by diff desc  
limit 20;

select * from step_1 order by stock_number
  
-- estimate details
drop table if exists step_2_estimate cascade;
create temp table step_2_estimate as
select a.stock_number, a.description, a.parts, a.labor
from jon.body_recon_est a
join step_1 b on a.stock_number = b.stock_number
order by a.stock_number;

-- ro line details 
drop table if exists step_3_ro_details cascade;
create temp table step_3_ro_details as
select a.stock_number, a.opcode, a.complaint, a.labor, a.parts, a.sublet 
from jon.body_shop_recon_ro_lines a
join step_1 b on a.stock_number = b.stock_number
order by stock_number, a.ro, a.line;

-- ro totals
drop table if exists step_4_ro_totals cascade;
create temp table step_4_ro_totals as
select a.*
from jon.body_shop_recon_ro_totals a
join step_1 b on a.stock_number = b.stock_number
order by stock_number, ro;

select * from jon.body_recon_est where stock_number = 'G44161P'
select * from jon.accounting_body_recon where control = 'G44161P'
select * from jon.body_shop_recon_ro_lines where stock_number = 'G44161P' limit 10


/* 
-- some earlier doodles
select a.*, b.*, c.vehicle_total, b.body_recon - c.vehicle_total
from jon.walks a
join dds.dim_date aa on a.thru_date = aa.the_date
left join jon.body_recon b on a.stock_number = b.control
left join (
  select vehicleinventoryitemid, vehicle_total
  from jon.body_recon_est
  group by vehicleinventoryitemid, vehicle_total) c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
where a.stock_number = 'G43948A'

select *
from dds.fact_repair_order a
where a.ro in ('18091642','18091808')   
order by a.ro, a.line 

select a.*, c.account
from fin.fact_gl a 
join jon.walks aa on a.control = aa.stock_number
join fin.dim_journal b on a.journal_key = b.journal_key
  and b.journal_code in ('SVI','SWA','SCA','POT')
join fin.dim_account c on a.account_key = c.account_key
--   and c.account in ('124000','124100','224000','224100')
where a.post_Status = 'Y'
--   and (a.doc like '18%' or a.ref like '18%')
  and control = 'G44598A'

SELECT a.description, a.totalpartsamount, a.laboramount
FROM ads.ext_Vehicle_Recon_Items a
join jon.walks aa on a.vehicleinventoryitemid = aa.vehicleinventoryitemid
JOIN ads.ext_Authorized_Recon_Items b ON a.VehicleReconItemID = b.VehicleReconItemID
	AND b.Status = 'AuthorizedReconItem_Complete'
WHERE a.typ like 'body%'	 
  and aa.stock_number = 'G44598A'
*/














-----------------------------------------------------------------------------------------------
-- -- intramarket wholesales
-----------------------------------------------------------------------------------------------
-- do $$
-- declare
--   vii_id citext := -- '4a875d49-d149-4662-bb2e-4a68f69e424c';  -- G44041A  
--                    '261539d9-8d9c-4c78-ad5e-975da1f66cc3'; -- G45329G
--   old_vii_id citext := NULL; 
-- begin
-- 	if exists (
-- 		select 1
-- 		from ads.ext_vehicle_acquisitions
-- 		where vehicleinventoryitemid = vii_id
-- 			and typ = 'VehicleAcquisition_IntraMarketPurchase')
-- 	then old_vii_id = (
-- 		select vehicleinventoryitemid
-- 		from ads.ext_vehicle_inventory_items 
-- 		where vehicleitemid = (
-- 			select vehicleitemid
-- 			from ads.ext_vehicle_inventory_items
-- 			where vehicleinventoryitemid = vii_id)
-- 		order by fromts desc
-- 		limit 1 offset 1);
-- 	end IF;
-- 
-- 	
-- 	drop table if exists wtf;
-- 	create temp table wtf as
-- 	select old_vii_id;
-- end $$;
-- 
-- select * from wtf;
-- 
-- -- 261539d9-8d9c-4c78-ad5e-975da1f66cc3,G45329G
-- -- 0ec05d5e-3f6c-41d0-8978-004654d07cd9,H15213PA
-- -- 7a6f6e40-4c70-4738-a8c4-65bb4932fb37,H14126X

