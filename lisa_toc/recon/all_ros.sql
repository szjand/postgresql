﻿-- basically the same format as body shop ros, but all ros
drop table if exists toc.all_ros;
create table toc.all_ros as
select a.open_date, a.close_date, a.ro, a.line, a.flag_hours, b.tech_number, b.tech_name, 
  case
    when c.full_name like '%inventory%' then 'internal'::citext
    else 'external'::citext
  end as "int/ext",
  d.vin,
  e.correction, f.opcode, f.description
-- select a.*
from dds.fact_repair_order a
join dds.dim_tech b on a.tech_key = b.tech_key
  and b.tech_number not in ('999', '103', '501')
join dds.dim_customer c on a.customer_key = c.customer_key
join dds.dim_vehicle d on a.vehicle_key = d.vehicle_key
left join dds.dim_ccc e on a.ccc_key = e.ccc_key
left join dds.dim_opcode f on a.opcode_key = f.opcode_key
left join dds.dim_opcode g on a.opcode_key = g.opcode_key
where close_date between current_date - 365 and current_date
  and flag_hours > 0
order by open_date, ro

create index on toc.all_ros(opcode);
create index on toc.all_ros(vin);
create index on toc.all_ros(ro);
create index on toc.all_ros(open_date);
create index on toc.all_ros(tech_number);
create index on toc.all_ros(description);
create index on toc.all_ros(correction);


select opcode, count(*)
from toc.all_ros
group by opcode

select correction, count(*)
from toc.all_ros
group by correction
order by count(*) desc;


select * from toc.all_ros
limit 10000

select * from dds.dim_tech where tech_number = '621'