﻿-- 4296
drop table if exists vehicles;
create temp table vehicles as
select a.stocknumber, a.vehicleinventoryitemid, fromts::date as from_date, thruts::date as thru_date
from ads.ext_vehicle_inventory_items a
where a.fromts::date > '01/01/2021'
  and a.thruts::date < '03/31/2022';

-- the first instance of FLR
-- 3296
drop table if exists flr_vehicles;
create temp table flr_vehicles as
select a.stocknumber, a.vehicleinventoryitemid, a.from_date, a.thru_date,
  min(b.fromts) as flr_ts
from vehicles a
join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and b.status = 'RMFlagFLR_FrontLineReady'
group by a.stocknumber, a.vehicleinventoryitemid, a.from_date, a.thru_date ;

drop table if exists test_1;
create temp table test_1 as
-- select *, row_number() over (partition by stocknumber order by fromts) as seq, thruts - fromts
select stocknumber, vehicleinventoryitemid, flr_ts, (left(status, position('Recon' in status) - 1))::citext as dept, 
	fromts, thruts, row_number() over (partition by stocknumber order by fromts) as seq
from (-- this actually give me all the data in rows
	select a.*, b.status, b.fromts, b.thruts
	from flr_vehicles a
	join ads.ext_Vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
		and b.fromts < a.flr_ts -- a.pb_ts
		and b.status = 'AppearanceReconProcess_InProcess'
	join ads.ext_Vehicle_inventory_item_statuses c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
		and c.fromts < a.flr_ts -- a.pb_ts
		and c.status = 'BodyReconProcess_InProcess'
	join ads.ext_Vehicle_inventory_item_statuses d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
		and d.fromts < a.flr_ts -- a.pb_ts
		and d.status = 'MechanicalReconProcess_InProcess'
	union
	select a.*, c.status, c.fromts, c.thruts -- , d.status, d.fromts,d.thruts
	from flr_vehicles a
	join ads.ext_Vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
		and b.fromts < a.flr_ts -- a.pb_ts
		and b.status = 'AppearanceReconProcess_InProcess'
	join ads.ext_Vehicle_inventory_item_statuses c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
		and c.fromts < a.flr_ts -- a.pb_ts
		and c.status = 'BodyReconProcess_InProcess'
	join ads.ext_Vehicle_inventory_item_statuses d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
		and d.fromts < a.flr_ts -- a.pb_ts
		and d.status = 'MechanicalReconProcess_InProcess'
	union
	select a.*,d.status, d.fromts,d.thruts
	from flr_vehicles a
	join ads.ext_Vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
		and b.fromts < a.flr_ts -- a.pb_ts
		and b.status = 'AppearanceReconProcess_InProcess'
	join ads.ext_Vehicle_inventory_item_statuses c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
		and c.fromts < a.flr_ts -- a.pb_ts
		and c.status = 'BodyReconProcess_InProcess'
	join ads.ext_Vehicle_inventory_item_statuses d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
		and d.fromts < a.flr_ts -- a.pb_ts
		and d.status = 'MechanicalReconProcess_InProcess') x;

-- next_start is the next status fromts following this row's thruts
-- the last recon line will show null
select *, lead(fromts, 1) over (partition by stocknumber order by seq) as next_start -- coalesce(lead(fromts, 1) over (partition by stocknumber order by seq), pb_ts) as next_start
from test_1

-- we will fill that null with the flr_ts
select *, coalesce(lead(fromts, 1) over (partition by stocknumber order by seq), flr_ts) as next_start
from test_1

-- this gives me hours in wip in each department and the total wait time in hours
select a.stocknumber, max(a.flr_ts) as flr_ts, max(fromts) filter (where seq = 1) as start_ts, 
  round((sum(extract(epoch from thruts - fromts)/(60*60)) filter (where dept = 'mechanical')::numeric), 1) as mech_wip, 
  round((sum(extract(epoch from thruts - fromts)/(60*60)) filter (where dept = 'body')::numeric), 1) as body_wip, 
  round((sum(extract(epoch from thruts - fromts)/(60*60)) filter (where dept = 'appearance')::numeric), 1) as app_wip, 
  round((sum(extract(epoch from next_start - thruts)/(60*60))::numeric), 1) as wait
from (
	select *, coalesce(lead(fromts, 1) over (partition by stocknumber order by seq), flr_ts) as next_start
	from test_1) a
group by stocknumber




	select *, coalesce(lead(fromts, 1) over (partition by stocknumber order by seq), flr_ts) as next_start
	from test_1 
	where stocknumber = 'G35731XZA'