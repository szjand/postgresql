﻿select * 
from greg.uc_Base_vehicles
where sale_date between current_date -180 and current_date


select *
from greg.uc_base_vehicle_prices
limit 100




select *
from greg.uc_daily_snapshot_beta_1
limit 10




select store_code as store, year, make, model, trim, shape, shape_size, best_price, status, days_owned, days_in_status, sale_type, sale_date, 
   case 
     when miles/(2021 - year::integer)  < 5000 then 'freaky low'
     when miles/(2021 - year::integer)  between 5000 and 10000 then '5 - 10'
     when miles/(2021 - year::integer) between 10001 and 15000 then '10-15'
     when miles/(2021 - year::integer) between 15001 and 20000 then '15-20'
     when miles/(2021 - year::integer) between 20001 and 25000 then '20-25'
     when miles/(2021 - year::integer) > 25000 then 'freaky hi'
   end as mile_cat
from greg.uc_daily_snapshot_beta_1
where sale_date between current_date - 180 and current_date
  and the_Date = sale_date
  and best_price between 15000 and 20000
  and sale_type = 'retail'
--   and status <> 'avail_aged' -- in ('raw', 'avail_fresh')
order by shape,shape_size, make, model, days_owned

-- sent as price_15K-30K_retail_sales_6_months.csv
-- expand pb range
select store_code as store, year, make, model, trim, shape, shape_size, best_price, status, days_owned, days_in_status, sale_type, sale_date, 
   case 
     when miles/(2021 - year::integer)  < 5000 then 'freaky low'
     when miles/(2021 - year::integer)  between 5000 and 10000 then '5 - 10'
     when miles/(2021 - year::integer) between 10001 and 15000 then '10-15'
     when miles/(2021 - year::integer) between 15001 and 20000 then '15-20'
     when miles/(2021 - year::integer) between 20001 and 25000 then '20-25'
     when miles/(2021 - year::integer) > 25000 then 'freaky hi'
   end as mile_cat
from greg.uc_daily_snapshot_beta_1
where sale_date between current_date - 180 and current_date
  and the_Date = sale_date
  and best_price between 15000 and 30000
  and sale_type = 'retail'
   and year <> '2021'
--   and status <> 'avail_aged' -- in ('raw', 'avail_fresh')
order by shape,shape_size, make, model, days_owned


select store_code as store, year, make, model, trim, drive, color, shape, shape_size, best_price, status, days_owned, days_in_status, sale_type, sale_date, 
   case 
     when miles/(2021 - year::integer)  < 5000 then 'freaky low'
     when miles/(2021 - year::integer)  between 5000 and 10000 then '5 - 10'
     when miles/(2021 - year::integer) between 10001 and 15000 then '10-15'
     when miles/(2021 - year::integer) between 15001 and 20000 then '15-20'
     when miles/(2021 - year::integer) between 20001 and 25000 then '20-25'
     when miles/(2021 - year::integer) > 25000 then 'freaky hi'
   end as mile_cat
from greg.uc_daily_snapshot_beta_1
where sale_date between current_date - 180 and current_date
  and the_Date = sale_date
  and best_price between 20001 and 25000
  and sale_type = 'retail'
  and (shape = 'suv' or model like 'rogue%')
--   and status <> 'avail_aged' -- in ('raw', 'avail_fresh')
order by shape_size, model, days_owned


select shape, price_band, count(*)
from (
select shape, best_price,
  case  
   when best_price between 0 and 5000 then '0-5'
   when best_price between 5001 and 10000 then '0-5'
   when best_price between 10001 and 15000 then '10-15'
   when best_price between 15001 and 20000 then '15-20'
   when best_price between 20001 and 25000 then '20-25'
   when best_price between 25001 and 30000 then '25-30'
   when best_price between 30001 and 35000 then '30-35'
   when best_price between 35001 and 40000 then '35-40'
   when best_price between 40001 and 45000 then '40-45'
   when best_price between 45001 and 50000 then '45-50'
   when best_price between 50001 and 55000 then '50-55'
   when best_price between 55001 and 60000 then '55-60'
   when best_price > 60 then '60+'
  end as price_band
from greg.uc_daily_snapshot_beta_1
where sale_date between current_date - 180 and current_date
  and the_Date = sale_date
  and sale_type = 'retail'
  and best_price > 0) x
group by shape, price_band
order by count(*) desc    


select the_date, 
  count(*) filter (where drive = 'FWD') fwd,
  count(*) filter (where drive = 'AWD') awd,
  count(*) as total,
  count(*) filter (where sale_date = the_date and drive = 'FWD' and sale_type = 'retail') as fwd_sales,
  count(*) filter (where sale_date = the_date and drive = 'AWD' and sale_type = 'retail') as awd_sales,
  count(*) filter (where sale_date = the_date and sale_type = 'retail') as total_sales
from greg.uc_daily_snapshot_beta_1
where model = 'equinox'd
	and best_price between 15000 and 30000
	and the_date between current_date - 180 and current_date
group by the_date
order by the_date


select x.* ,
  sum(total_sales) over (order by the_date rows between 30 preceding and current row) as prev_30_sales
from (
	select the_date, 
		count(*) filter (where drive = 'FWD') fwd,
		count(*) filter (where drive = 'AWD') awd,
		count(*) as total,
		count(*) filter (where sale_date = the_date and drive = 'FWD' and sale_type = 'retail') as fwd_sales,
		count(*) filter (where sale_date = the_date and drive = 'AWD' and sale_type = 'retail') as awd_sales,
		count(*) filter (where sale_date = the_date and sale_type = 'retail') as total_sales
	from greg.uc_daily_snapshot_beta_1
	where model = 'equinox'
		and best_price between 15000 and 30000
		and the_date between current_date - 180 and current_date
	group by the_date) x
order by the_date

-- 12/16/21 homework

select shape, price_band, count(*)
from greg.uc_daily_snapshot_beta_1
where the_date between current_date - 180 and current_date
  and sale_date = the_date
group by shape, price_band
order by count(*) desc



select * 
from greg.uc_daily_snapshot_beta_1
where the_date between current_date - 365 and current_date
  and sale_date = the_date
  and price_band = 'not priced'
  and shape = 'suv'