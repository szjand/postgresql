﻿select a.* 
from arkona.ext_sdptime a
left join arkona.ext_sdprdet b on a.ro_number = b.ro_number
  and a.line_number = b.line_number
  and a.technician_id = b.technician_id
where a.transaction_date between '04/04/2022' and '04/08/2022'
limit 10


select a.company_number, a.ro_number, a.line_number, a.technician_id, a.hours, 
from arkona.ext_sdprdet a
left join arkona.ext_sdptime b on a.ro_number = b.ro_number
  and a.line_number = b.line_number
  and a.technician_id = b.technician_id
where a.transaction_date betwen '04/04/2022' and '04/08/2022'  

-- punch time
drop table if exists punch_times;
create temp table punch_times as
select bb.*, cc.punch_time
from arkona.ext_sdprhdr aa
join (
	select company_number, ro_number, line_number, technician_id, sum(labor_hours) as flag_hours
	from arkona.ext_sdprdet a
	where a.line_type = 'L'  -- only the lines with tech and time
	group by company_number, ro_number, line_number, technician_id
	having sum(labor_hours) <> 0) bb on aa.ro_number = bb.ro_number
left join (
	select company_number, ro_number, line_number, technician_id, sum(hours)  as punch_time
	from arkona.ext_sdptime
	group by company_number, ro_number, line_number, technician_id) cc on bb.company_number = cc.company_number
		and bb.ro_number = cc.ro_number
		and bb.line_number = cc.line_number
		and bb.technician_id = cc.technician_id	
where aa.close_date between 20220401 and 20220408

select technician_id, sum(flag_hours) as flag, sum(punch_time) as  punch 
from punch_times
where punch_time is not null
group by technician_id


-- techs not punching time
select bb.technician_id, count(*)
from arkona.ext_sdprhdr aa
join (
	select company_number, ro_number, line_number, technician_id, sum(labor_hours) as flag_hours
	from arkona.ext_sdprdet a
	where a.line_type = 'L'  -- only the lines with tech and time
	group by company_number, ro_number, line_number, technician_id
	having sum(labor_hours) <> 0) bb on aa.ro_number = bb.ro_number
left join (
	select company_number, ro_number, line_number, technician_id, sum(hours)  as punch_time
	from arkona.ext_sdptime
	group by company_number, ro_number, line_number, technician_id) cc on bb.company_number = cc.company_number
		and bb.ro_number = cc.ro_number
		and bb.line_number = cc.line_number
		and bb.technician_id = cc.technician_id	
where aa.close_date between 20220406 and 20220408
  and cc.punch_time is null 
group by bb.technician_id  
order by count(*) desc
