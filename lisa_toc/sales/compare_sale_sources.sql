﻿-- 362
drop table if exists tool;
create temp table tool as
select stock_number, vin, sale_date, sale_type
-- select *
from greg.uc_daily_snapshot_beta_1
where the_date between '03/01/2022' and '03/31/2022'
  and the_date = sale_date;

-------------------------------------------------------------------------------------------------------------
-- 362
drop table if exists greg;
create temp table greg as
select b.stocknumber, c.vin, a.soldts::date as sale_date, replace(a.typ, 'VehicleSale_','') as sale_type
-- select * 
from ads.ext_vehicle_sales a
join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
where a.soldts::date between '03/01/2022' and '03/31/2022'
  and status <> 'VehicleSale_SaleCanceled';

--------------------------------------------------------------------------------------------------------------

-- financial statement
drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select store, page, line, line_label, control, sum(unit_count) as unit_count
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 202203-----------------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 202203  ---------------------------------------------------------------------
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
--         or
--         (b.page = 17 and b.line between 1 and 20))-- f/i counts don't make sense
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by store, page, line, line_label, control
order by store, page, line;

-- used cars
select sum(unit_count) -- 377
from step_1
where page = 16



-- 380
-- need tool data for wholesale vins
select a.store, a.control, a.unit_count, coalesce(b.inpmast_vin, d.vin) as vin,
  case
    when a.line < 10 then 'retail'
    else 'wholesale'
  end as sale_type
from step_1 a
left join arkona.ext_inpmast b on a.control = b.inpmast_stock_number
left join ads.ext_vehicle_inventory_items c on a.control = c.stocknumber
left join ads.ext_vehicle_items d on c.vehicleitemid = d.vehicleitemid
where a.page = 16
  and a.control not in (
		select control
		from step_1
		where unit_count < 0)
order by control


------------------------------------------------------------------------------------------------------------------
-- boards
-- used
select count(*) as all_boarded_deals, count(*) filter (where is_backed_on = true) as backed_on_deals, 
count(*) filter (where is_backed_on = false) as total_deals
from board.sales_board a
inner join board.daily_board b on a.board_id = b.board_id and vehicle_type = 'U'
where is_deleted = false
and boarded_date between '03/03/2022' and current_date
and board_type_key in (5, 17)  -- retail & wholesale deals


-- new
select vehicle_make, count(*) as all_boarded_deals, count(*) filter (where is_backed_on = true) as backed_on_deals, 
	count(*) filter (where is_backed_on = false) as total_deals
from board.sales_board a
inner join board.daily_board b on a.board_id = b.board_id and vehicle_type = 'N'
where is_deleted = false
	and boarded_date between '03/03/2022' and current_date
	and board_type_key in (5, 6)  -- retail & fleet
group by b.vehicle_make

select * 
from board.sales_board
limit 10

select * 
from board.daily_board
limit 10

select * 
from board.board_types
order by board_type_key

select count(*) as all_boarded_deals, count(*) filter (where is_backed_on = true) as backed_on_deals, 
count(*) filter (where is_backed_on = false) as total_deals
from board.sales_board a
inner join board.daily_board b on a.board_id = b.board_id and vehicle_type = 'U'
where is_deleted = false
and boarded_date between '03/03/2022' and current_date
and board_type_key in (16)