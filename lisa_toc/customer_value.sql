﻿/*
Greg  10/25/21 2:26 PM
First query is lifetime customer value (gross and/or sales). Trying to track a few customers through different vehicle purchases 
and their associated service business for those vehicles when they owned them. Test on a long-term customer like Art Bakken could be an example.

Another question came up about how long our new car customers keep their cars. Some type of time bucket report for years owned by the same customer.

Finally — somehow looking at our parts sales area. Rick has made comments about us shipping motors all over the place and not just our tri-state market.
*/


-- get some multi car buyers
select a.bopmast_company_number, a.buyer_number, b.bopname_search_name,count(*)
from arkona.ext_bopmast a
join arkona.ext_bopname b on a.buyer_number = b.bopname_record_key
  and a.bopmast_company_number = b.bopname_company_number
  and b.bopname_search_name not in ('ADESA','1')
  and b.company_individ is not null
where  a.record_status = 'U'  
--   and a.buyer_number = 254671
group by a.bopmast_company_number, a.buyer_number, b.bopname_search_name  
having count(*) > 5
-- order by count(*) desc
order by bopname_search_name

-- car deals
drop table if exists car_deals cascade;
create temp table car_deals as
select bopmast_company_number, date_capped,sale_type, vehicle_type, buyer_number, bopmast_search_name, record_type, bopmast_stock_number, bopmast_vin
from arkona.ext_bopmast
where buyer_number in(1080796, 231642, 1080796, 239973, 315486, 254671, 254713, 263898)
  and record_status = 'U';

-- board gross accounts
drop table if exists gross_accounts cascade;
create temp table gross_accounts as 
select b.g_l_acct_number as account, c.account_type, c.account_key
from arkona.ext_eisglobal_sypffxmst a
inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
	and b.factory_financial_year  = (select max(factory_financial_year) from arkona.ext_ffpxrefdta)
join fin.dim_account c on b.g_l_acct_number = c.account
  and c.current_row
where a.fxmcyy = (select max(fxmcyy) from arkona.ext_eisglobal_sypffxmst)
	and coalesce(b. consolidation_grp, '1') <> '3'
	and b.g_l_acct_number <> ''
	and (
		(a.fxmpge between 5 and 15) or
		(a.fxmpge = 16 and a.fxmlne between 1 and 14) or
		(a.fxmpge = 17 and a.fxmlne between 1 and 19))
	and b.g_l_acct_number not in ('144502','164502','285010', '285011', '285100','285101');


-- deals with accounting
select a.date_capped, a.sale_type, a.bopmast_search_name, a.record_type, a.bopmast_stock_number, c.account_type, sum(b.amount)
from car_deals a
join fin.fact_gl b on a.bopmast_stock_number = b.control
  and b.post_status = 'Y'
join gross_accounts c on b.account_key = c.account_key
group by a.date_capped, a.sale_type, a.bopmast_search_name, a.record_type, a.bopmast_stock_number, c.account_type
order by a.bopmast_search_name

-- 1 row per customer w/sales & gross totalled
select a.buyer_number, max(a.bopmast_search_name), min(date_capped) n, max(date_capped),
  count(distinct bopmast_stock_number) as vehicles,
  sum(-b.amount) filter (where c.account_type in ('Sale','Other Income')) as sales,
  sum(-b.amount) as gross
from car_deals a
join fin.fact_gl b on a.bopmast_stock_number = b.control
  and b.post_status = 'Y'
join gross_accounts c on b.account_key = c.account_key
group by a.buyer_number



-- ros in the name of the customer from car_deals and the vin from car_deals
drop table if exists ros;
create temp table ros as
select a.ro, d.bopmast_search_name 
from dds.fact_repair_order a
join dds.dim_customer b on a.customer_key = b.customer_key
join dds.dim_vehicle c on a.vehicle_key = c.vehicle_key
join car_deals d on c.vin = d.bopmast_vin
  and b.bnkey = d.buyer_number
group by a.ro, d.bopmast_search_name;


-- ros with accounting
select aa.*, a.amount, b.department_code, b.account_type
from ros aa
join fin.fact_gl a on aa.ro = a.control
join fin.dim_Account b on a.account_key = b.account_key
  and b.current_row
  and b.account_type_code in ('4','5')
where a.post_status = 'Y'
order by b.department_code

-- 1 row per customer w/sales & gross totalled pd, bs, ql, re, sd
select grp, count(distinct ro) as ros,
  (sum(-amount) filter (where department_code = 'PD' and account_type = 'Sale'))::integer as parts_sales,
  (sum(-amount) filter (where department_code = 'PD'))::integer as parts_gross,
  (sum(-amount) filter (where department_code = 'SD' and account_type = 'Sale'))::integer as service_sales,
  (sum(-amount) filter (where department_code = 'SD'))::integer as service_gross,
  (sum(-amount) filter (where department_code = 'BS' and account_type = 'Sale'))::integer as bs_sales,
  (sum(-amount) filter (where department_code = 'BS'))::integer as bs_gross,
  (sum(-amount) filter (where department_code = 'QL' and account_type = 'Sale'))::integer as pdq_sales,
  (sum(-amount) filter (where department_code = 'QL'))::integer as pdq_gross,  
  (sum(-amount) filter (where department_code = 'RE' and account_type = 'Sale'))::integer as detail_sales,
  (sum(-amount) filter (where department_code = 'RE'))::integer as detail_gross,
  sum(-amount)::integer as total_gross
from (
	select aa.*, a.amount, b.department_code, b.account_type,  left(bopmast_search_name, 5) as grp
	from ros aa
	join fin.fact_gl a on aa.ro = a.control
	join fin.dim_Account b on a.account_key = b.account_key
		and b.current_row
		and b.account_type_code in ('4','5')
	where a.post_status = 'Y') x
where department_code <> 'CW'
group by grp


-- -- all together

-- 1 row per customer w/sales & gross totalled
-- sent to greg 11/13
select aa.*, bb.ros, bb.total_sales as sales, bb.total_gross as gross
from ( -- car deals
	select a.buyer_number, min(a.bopmast_search_name) as customer, min(date_capped) as first_purch, max(date_capped) as latest_purch,
		count(distinct bopmast_stock_number) as vehicles,
		(sum(-b.amount) filter (where c.account_type in ('Sale','Other Income')))::integer as sales,
		sum(-b.amount)::integer as gross
	from car_deals a
	join fin.fact_gl b on a.bopmast_stock_number = b.control
		and b.post_status = 'Y'
	join gross_accounts c on b.account_key = c.account_key
	group by a.buyer_number) aa
left join (
	select grp, count(distinct ro) as ros,
		(sum(-amount) filter (where department_code = 'PD' and account_type = 'Sale'))::integer as parts_sales,
		(sum(-amount) filter (where department_code = 'PD'))::integer as parts_gross,
		(sum(-amount) filter (where department_code = 'SD' and account_type = 'Sale'))::integer as service_sales,
		(sum(-amount) filter (where department_code = 'SD'))::integer as service_gross,
		(sum(-amount) filter (where department_code = 'BS' and account_type = 'Sale'))::integer as bs_sales,
		(sum(-amount) filter (where department_code = 'BS'))::integer as bs_gross,
		(sum(-amount) filter (where department_code = 'QL' and account_type = 'Sale'))::integer as pdq_sales,
		(sum(-amount) filter (where department_code = 'QL'))::integer as pdq_gross,  
		(sum(-amount) filter (where department_code = 'RE' and account_type = 'Sale'))::integer as detail_sales,
		(sum(-amount) filter (where department_code = 'RE'))::integer as detail_gross,
		(sum(-amount) filter (where account_type = 'sale'))::integer as total_sales,
		sum(-amount)::integer as total_gross
	from (
		select aa.*, a.amount, b.department_code, b.account_type,  left(bopmast_search_name, 5) as grp
		from ros aa
		join fin.fact_gl a on aa.ro = a.control
		join fin.dim_Account b on a.account_key = b.account_key
			and b.current_row
			and b.account_type_code in ('4','5')
		where a.post_status = 'Y') x
	where department_code <> 'CW'
	group by grp) bb on left(aa.customer, 5) = bb.grp
---------------------------------------------------------------------------------------------
-- Another question came up about how long our new car customers keep their cars. Some type of time bucket report for years owned by the same customer.
---------------------------------------------------------------------------------------------
select record_type, count(*) from arkona.ext_bopmast group by record_type
select *
select bopmast_company_number, bopmast_vin, bopmast_stock_number, buyer_number, date_capped
from arkona.ext_bopmast
where vehicle_type = 'N'
  and record_status = 'U'
  and record_type in ('C','F')
  and date_capped between '01/01/2015' and '12/31/2015'
limit 50


select * 
from arkona.ext_bopvref 
where vin not like '0%'
  and length(vin) = 17
order by vin, start_date
limit 100

/* BOPVREF */
BVTYPE (TYPE_CODE) Customer/Vehicle Reference
S = Buyer on the Deal
2 = Co-Buyer on the Deal
C = Service Customer
T = Trade In
4 = ?? (Of the 212409 rows in BOPVREF table, only 59 are type 4)

-- sent this to greg on 11/12/21
this is based on the table bopvref which shows dated (start_date  and end_date) interactions for a customer and a vin with the store (sales and service)
this query looks at all cash/finance new car deals in 2015
for those interactions that show no end date, the presumption is that the customer still owns the vehicle, but 
in truth it simply means they have not been back to the store
another caveat on the data, if a customer purchases a car under one customer number and services it under another customer number, that breaks the chain of owernship
if you like, i can replace customer numbers with names in the joins and bypass some of those broken chains
not sure what you are really looking for
so, this is a start
let me know what is next for this

select a.bopmast_search_name as custiner, a.bopmast_stock_number as stock_number, a.bopmast_vin as vin, aa.year, aa.make, aa.model, aa.body_style, 
  min(arkona.db2_integer_to_date(start_date)) as start_date, max(arkona.db2_integer_to_date(end_date)) as end_date, 
  case
    when max(arkona.db2_integer_to_date(end_date)) = '12/31/9999' then 'still owns'
    else round((max(arkona.db2_integer_to_date(end_date)) - min(arkona.db2_integer_to_date(start_date)))/365.0, 2)::text || ' ' || 'years'
  end as "owned for"  
from (
	select bopmast_company_number, bopmast_search_name, bopmast_vin, bopmast_stock_number, buyer_number, date_capped
	from arkona.ext_bopmast
	where vehicle_type = 'N'
		and record_status = 'U'
		and record_type in ('C','F')
		and date_capped between '01/01/2015' and '12/31/2015') a
join arkona.ext_inpmast aa on a.bopmast_vin = aa.inpmast_vin	
join arkona.ext_bopvref  b on a.bopmast_vin = b.vin	
  and a.buyer_number = b.customer_key
where bopmast_search_name not like '%RYDELL%'		  
group by a.bopmast_search_name, a.bopmast_stock_number, a.bopmast_vin, aa.year, aa.make, aa.model, aa.body_style		  
order by a.bopmast_search_name

		
---------------------------------------------------------------------------------------------
-- Finally — somehow looking at our parts sales area. Rick has made comments about us shipping motors all over the place and not just our tri-state market.
---------------------------------------------------------------------------------------------
select company_individ, count(*)
from arkona.ext_bopname
where company_individ is not null
group by company_individ

select bopname_company_number, bopname_record_key, bopname_search_name, city, state_code
from arkona.ext_bopname
where company_individ is not null
	and state_code is not null
	and state_code not in ('ND','SD','MN')
limit 100	

select * 
from arkona.ext_pdpthdr a
where a.parts_total > 2000
limit 10



select arkona.db2_integer_to_date(trans_date), a.sort_name, a.gross_profit, b.city, b.state_code, d.part_number, d.part_description
from arkona.ext_pdpthdr a
join arkona.ext_bopname b on a.company_number = b.bopname_company_number
  and a.customer_key = b.bopname_record_key
  and b.company_individ is not null
	and b.state_code is not null
	and b.state_code not in ('ND','SD','MN')
join arkona.ext_pdptdet c on a.invoice_number = c.ptinv_
join arkona.ext_pdpmast d on c.ptpart = d.part_number
  and d.part_description like '%engine%'
where arkona.db2_integer_to_date(trans_date) > '01/01/2021'


---------------------------------------------------------------------------------------------------------------------
11/13/21 from gregv: Very cool/interesting. You can put things on hold until we have a group discussion on the results.
deals dont match with dealertrack, lets dig into that a bit
1. fact_gl only goes back to 1/1/2011, arkona "lifetime" starts in Jul 2009, so that accounts for some missing deals
2. some gross diffs are deal vs accounting

as i dug further into the dealertrack CV screen, details are available

select * from car_Deals where bopmast_search_name like 'deer%' order by date_capped desc

select * 
from arkona.ext_bopmast
where bopmast_stock_number = '99425'

select b.amount, c.*
from fin.fact_gl b
join gross_accounts c on b.account_key = c.account_key
where b.post_status = 'Y'
  and b.control = '99425'

 select min(b.the_date)
 from fin.fact_gl a
 join dds.dim_date b on a.date_key = b.date_key 