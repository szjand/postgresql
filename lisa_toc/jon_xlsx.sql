﻿select min(open_date), max(open_date) from dds.fact_Repair_order


select a.open_date, a.ro, a.customer_key, a.vehicle_key, b.full_name, b.bnkey, c.vin
from dds.fact_repair_order a
join dds.dim_customer b on a.customer_key = b.customer_key
  and b.customer_key not in (1,2,13)
  and b.customer_type_code <> 'C'
join dds.dim_vehicle c on a.vehicle_key = c.vehicle_key  
where a.open_date > '12/31/2015'
group by a.open_date, a.ro, a.customer_key, a.vehicle_key, b.full_name, b.bnkey, c.vin
limit 50