﻿-- 10/22/21 from lisa: •	John to examine the throughput generated over the life of a customer
-- these queries are specifically aimed at matching dealertrack customer value screens
-- these are the queries where i decided dealertrack customer value is based on operational data as opposed to accounting data
select * 
from arkona.ext_bopname
where bopname_search_name = 'SCHRUM, ALBERT W'

-- these deals identically match the dealertrack customer value screen
-- the dt gross column = comm_gross + f_i_gross
-- in the dt deal screen, the gross matches house gross from the summary modal
drop table if exists schrum_deals cascade;
create temp table schrum_deals as
select a.buyer_number, a.bopmast_stock_number, a.date_capped, a.retail_price, a.comm_gross, a.f_i_gross, (a.comm_gross + a.f_i_gross), a.record_type
from arkona.ext_bopmast a
join arkona.ext_bopname b on a.buyer_number = b.bopname_record_key
  and b.bopname_search_name = 'SCHRUM, ALBERT W'
where bopmast_stock_number is not null  
order by a.date_capped  desc

select * from schrum_deals
-- jumped back into this on 6/24/22
-- figure accounting gross
-- board gross accounts
drop table if exists gross_accounts cascade;
create temp table gross_accounts as 
select b.g_l_acct_number as account, c.account_type, c.account_key,
  case when a.fxmpge = 17 then 'fin' else 'front' end as category
from arkona.ext_eisglobal_sypffxmst a
inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
	and b.factory_financial_year  = (select max(factory_financial_year) from arkona.ext_ffpxrefdta)
join fin.dim_account c on b.g_l_acct_number = c.account
  and c.current_row
where a.fxmcyy = (select max(fxmcyy) from arkona.ext_eisglobal_sypffxmst)
	and coalesce(b. consolidation_grp, '1') <> '3'
	and b.g_l_acct_number <> ''
	and (
		(a.fxmpge between 5 and 15) or
		(a.fxmpge = 16 and a.fxmlne between 1 and 14) or
		(a.fxmpge = 17 and a.fxmlne between 1 and 19))
	and b.g_l_acct_number not in ('144502','164502','285010', '285011', '285100','285101');

-- compare the 2 sources of gross
select aa.*, bb.front, bb.fin, bb.total_gross
from schrum_deals aa
left join (
	select a.control, 
	  sum(-a.amount) filter (where c.category = 'front') as front,
	  sum(-a.amount) filter (where c.category = 'fin') as fin,
	  sum(-a.amount) as total_gross
	from fin.fact_gl a
	join schrum_deals b on a.control = b.bopmast_Stock_number
	join gross_accounts c on a.account_key = c.account_key
	where a.post_status = 'Y'
	group by a.control) bb on aa.bopmast_stock_number = bb.control
order by aa.date_capped desc 	


-- service
select a.close_date, a.final_close_date, a.parts_total, a.labor_total
from arkona.ext_sdprhdr_history a
join arkona.ext_bopname b on a.customer_key = b.bopname_record_key
  and b.bopname_search_name = 'SCHRUM, ALBERT W'

select min(close_date) from arkona.ext_sdprhdr  

select min(close_date), max(close_date) from arkona.ext_sdprhdr_history

select min(close_date), max(close_date) from (
select a.ro_number, a.close_date, a.final_close_date, a.parts_total, a.labor_total
from arkona.ext_sdprhdr_history a
where close_date > 20090900
union 
select a.ro_number, a.close_date, a.final_close_date, a.parts_total, a.labor_total
from arkona.ext_sdprhdr a) b


select bopname_search_name, array_agg(distinct bopname_record_key), count(*)
from arkona.ext_bopname
where company_individ = 'I'
  and left(bopname_search_name, 1) not in ('-','.','0','1','2','3','4','5','6','7','8','9','#')
group by bopname_search_name
order by bopname_search_name
