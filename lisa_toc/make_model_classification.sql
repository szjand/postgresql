﻿-- tool vs vision
select * 
from (
select make, model, substring(vehicletype, position('_' in vehicletype) + 1, 10), substring(vehiclesegment, position('_' in vehiclesegment) + 1, 10), luxury, sport 
from ads.ext_make_model_classifications) a
full outer join (
select make, model, shape, size, luxury, sport 
from veh.shape_size_classifications) b on a.make = b.make and a.model = b.model
order by coalesce(a.make, b.make), coalesce(a.model, b.model)