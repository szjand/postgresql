﻿10 customers from every year starting in 2016\
No more than 2 vehicles purchased in a 24 month period
5 of the 10 vehicles should have the first vehicle not purchased with us but, other vehicles purchased.

-- first visit is on a vehicle not acquired here
select 
from dds.fact_repair_order a
join dds.dim_date b on a.open_date = b.the_date
  and b.the_year = 2016
join dds.dim_customer c on a.customer_key = c.customer_key
join dds.dim_vehicle d on a.vehicle_key = d.vehicle_key
left join arkona.ext_bopmast e on d.vin = e.bopmast_vin

select * from dds.dim_vehicle limit 10  

select * from sls.deals limit 10

select * from dds.dim_customer limit 10

select min(capped_date) 1/2/2017
from sls.deals 
where deal_status_code = 'U'

select * from arkona.ext_bopmast limit 2


select * from dds.dim_customer limit 10

select * from arkona.ext_bopvref limit 10

select * from arkona.ext_bopname limit 10

select * from arkona.ext_bopname where coalesce(company_individ, 'x') not in ('i','c') limit 10
-- dim customer is generated excluding rows where company_individ is null
select a.* 
from dds.dim_customer a
join (
	select * from arkona.ext_bopname 
	where coalesce(company_individ, 'x') 
	not in ('i','c') limit 10) b on a.bnkey = b.bopname_record_key
	  and a.store_code  = b.bopname_company_number

-- first instance of a customer transaction that is service, occurred in 2016, with a vehicle not purchased here
-- 7574
select aa.*, bb.full_name
from (
	select company_number, customer_key, vin, 
		min(dds.db2_integer_to_date(start_date)) as min_start, max(dds.db2_integer_to_date(start_date)) as max_start,
		count(*),
		count(*) over (partition by customer_key)
	from arkona.ext_bopvref 
	where customer_key <> 0  
	  and type_code = 'C'
	group by company_number, customer_key, vin
	having min(dds.db2_integer_to_date(start_date))  between '01/01/2016' and '12/31/2016') aa
join dds.dim_customer bb on aa.company_number = bb.store_code
  and aa.customer_key = bb.bnkey	
  and bb.customer_type = 'person'
left join arkona.ext_bopmast cc on aa.vin = cc.bopmast_vin
where cc.bopmast_vin is null   

-- this might prove to be useful  418917, add company 418938, exclude rydell (319) 417993
select company_number, customer_key, vin
from arkona.ext_bopvref
where customer_key not in (0, 319)
group by company_number, customer_key, vin

drop table if exists customers_bopvref cascade;
create temp table customers_bopvref as -- 229014
select customer_key, count(*) as the_count
from arkona.ext_bopvref
where company_number = 'RY1' -- see below, company does not matter
  and customer_key not in (0, 319, 270)
group by customer_key;
alter table customers_bopvref
add primary key(customer_key);

select * from customers_bopvref
order by the_count desc
limit 100

drop table if exists customers cascade;
create temp table customers as
select a.customer_key, b.bopname_search_name as full_name
from customers_bopvref a
join arkona.ext_bopname b on a.customer_key = b.bopname_record_key
  and b.company_individ = 'I'
  and b.bopname_search_name not like '%rydell%'
  and b.bopname_search_name not like 'dealer%'
  and b.bopname_search_name not like 'inventory%'
  and b.bopname_search_name not like 'strata%'
  and b.bopname_search_name not like 'bnsf%'
  and b.bopname_search_name not like 'system%'
  and b.bopname_search_name not like '%adesa%'
  and b.bopname_search_name not like '%city of%'
  and b.bopname_search_name not like '%wheels%'
  and b.bopname_search_name not like '%auto finance%'
  and b.bopname_search_name not like '%lithia%'
  and b.bopname_search_name not like '%solution%'
  and b.bopname_search_name not like '%farms%'
  and b.bopname_search_name not like '%altru%'
  and b.bopname_search_name not like '%.%'
  and b.bopname_search_name not like '%public%'
  and b.bopname_search_name not like '%inventory%'
  and b.bopname_search_name not like '%grand forks%'
  and b.bopname_search_name not like '%DOT, ND%'
  and a.the_count < 100
  and a.the_count > 2;
alter table customers
add primary key(customer_key);


/*
select a.customer_key, a.full_name, b.vin, count(*)  
from customers a
join arkona.ext_bopvref b on a.company_number = b.company_number
  and a.customer_key = b.customer_key
group by a.customer_key, a.full_name, b.vin
order by count(*) desc

select * 
from arkona.ext_bopvref 
where customer_key = 213370
order by start_date, vin

so, out of all this, can i satisfy any of the spec requirements?

select *  from customers  
where company_number = 'RY2'
limit 100

-- holy shit virtually nothing for ry2 & ry3
select company_number, count(*)
from arkona.ext_bopvref
group by company_number

select bopname_company_number, bopname_search_name, bopname_record_key 
from arkona.ext_bopname
where company_individ = 'I'
limit 100

-- wtf, they are all RY1  time to look at db2, same in db2
-- ok, they are all limited to non I company_individ
select bopname_company_number, count(*)
from arkona.ext_bopname
-- where company_individ = 'I'
group by bopname_company_number

select store_code, count(*) 
from sls.deals
group by store_code

select * from sls.deals limit 10


select bopname_search_name, count(*)
from arkona.ext_bopname
where bopname_company_number = 'ry2'
group by bopname_search_name
order by count(*) desc

-- this is  my conclusion, for the types of deals/customers that i am looking for
-- company number is irrelevant
select a.delivery_date, a.stock_number, a.vin, b.bopname_company_number, b.bopname_record_key, b.bopname_search_name 
from sls.deals a
join arkona.ext_bopname b on a.buyer_bopname_id = b.bopname_record_key
  and b.company_individ = 'I'
where a.store_code = 'RY2'
  and delivery_date > '12/31/2020'
order by stock_number
*/

drop table if exists cust_vin cascade;
create temp table cust_vin as
select a.*, b.vin, dds.db2_integer_to_date(b.start_date) as start_date,
  case
    when b.end_date = 0 then '12/31/9999'
    else dds.db2_integer_to_date(b.end_date)
  end as end_date,
  case b.type_code
    when 's' then 'buyer'
    when '2' then 'co-buyer'
    when 'c' then 'service'
    when 't' then 'trade'
    else 'xxx'
  end as trans_type
  -- row_number() over (partition by a.customer_key, b.vin order by start_date)
-- select *  
from customers a
join arkona.ext_bopvref b on a.customer_key = b.customer_key
where length(b.vin) = 17
  and left(b.vin, 1) <> '0'
  and dds.db2_integer_to_date(b.start_date) > '12/31/2015'
  and b.start_date <> b.end_date
order by a.full_name, vin, start_date;
create index on cust_vin(start_date);

-- order by b.vin, a.customer_key, start_date 

/* this is going down the clean up the name file rabbit hole, not now
select * 
from ( -- name with multiple customer key
	select full_name
	from (
	select customer_key, full_name
	from cust_vin
	group by customer_key, full_name) a
	group by full_name
	having count(*) > 1) aa
join cust_vin bb on aa.full_name = bb.full_name
order by aa.full_name	

select * 
from cust_vin
where full_name = 'ABRAHAMSON, RONALD'

select full_name, vin, array_agg(distinct customer_key)
from cust_vin
group by full_name, vin
having count(*) > 1
order by full_name, vin
*/	

drop table if exists toc.jon_spec cascade;
create table toc.jon_spec (
  customer_key integer not null,
  full_name citext not null,
  vin citext not null,
  the_year integer not null,
  purchased_here boolean not null);

insert into toc.jon_spec
select customer_key,  full_name, vin, 2016, false  
from (
-- first transaction is in 2016 service, vehicle not bought by the cust here
select a.*, random() as ordering
from (
	select *, row_number() over (partition by full_name, vin order by start_date) as seq
	from cust_vin
	where start_date between '01/01/2016' and '12/31/2016') a
where trans_type = 'service'
  and seq = 1  -- ensure multiple transactions ?
  and end_date < current_date
  and not exists (
    select 1
    from arkona.ext_bopmast
    where record_status = 'U'
      and buyer_number = a.customer_key
      and bopmast_vin = a.vin)
order by ordering limit 5) aa;

insert into toc.jon_spec
select customer_key,  full_name, vin, 2016, true  
from (
-- first transaction is in 2016 service, vehicle bought by the cust here
select a.*, random() as ordering
from (
	select *, row_number() over (partition by full_name, vin order by start_date) as seq
	from cust_vin
	where start_date between '01/01/2016' and '12/31/2016') a
where seq = 1  -- ensure multiple transactions ?
  and end_date < current_date
  and exists (
    select 1
    from arkona.ext_bopmast
    where record_status = 'U'
      and buyer_number = a.customer_key
      and bopmast_vin = a.vin)
order by ordering limit 5) aa

select * 
from toc.jon_spec a
left join arkona.ext_bopvref b on a.customer_key = b.customer_key
order by a.customer_key, a.vin, b.start_date


select * 
from ads.ext_Vehicle_acquisitions A
JOIN ads.ext_Vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
join ads.ext_Vehicle_items c on b.vehicleitemid = c.vehicleitemid
  and c.vin = '4S4WMAFD9K3451424'
limit 10

select * 
from arkona.ext_boptrad
-- limit 10
-- where vin = '4S4WMAFD9K3451424'
where stock_ = 'G43247A'

select * 
from arkona.ext_bopmast
where bopmast_vin = '4S4WMAFD9K3451424'
-- dammit back to the name file rabbit hole
select * 
from arkona.ext_bopname
where bopname_search_name = 'COLE, JENNIFER'

-- 243231 - 200726
select bopname_record_key, bopname_Search_name, address_1, zip_code, email_address
from arkona.ext_bopname
where bopname_company_number = 'RY1'
  and company_individ = 'I'
  and address_1 is not null
  and zip_code <> 0
group by bopname_record_key, bopname_Search_name, address_1, zip_code, email_address

select *
from arkona.ext_bopname
where bopname_search_name like 'aafed%'

select * from customers limit 10

select * from customers_bopvref limit 10

select a.* , b.bopname_search_name
from customers_bopvref a
join arkona.ext_bopname b on a.customer_key = b.bopname_record_key
  and b.bopname_company_number = 'RY1'
  and b.company_individ = 'I'
order by the_count desc 
limit 1000

select * 
from (
select bopname_company_number, company_individ, bopname_Search_name, bopname_record_key,address_1, zip_code, email_address
from arkona.ext_bopname
where bopname_search_name in ('WILWAND, TIM','HECK, SHANE','BALL, DENNIS','TOUSIGMNANT, JOSEPH',
  'SPICER, RICHARD','KIRKEBY, KARLA ALLAN','LINDSETH, MICHAEL','GRANGER, CURWYNN GENE',
  'SCHRUM, ALBERT W','ADAMS, GARY WAYNE','RUST, DANIEL','HOWARD, TAREK','AAFEDT, TERRY','AAFEDT, TERRY LEE')) a
left join arkona.ext_bopvref b on a.bopname_record_key = b.customer_key  
order by bopname_search_name, dds.db2_integer_to_date(b.start_date)

1 key
BALL, DENNIS
TOUSIGMNANT, JOSEPH
WILWAND, TIM
SPICER, RICHARD
LINDSETH, MICHAEL
ADAMS, GARY WAYNE
KIRKEBY, KARLA ALLAN
GRANGER, CURWYNN GENE
3 key
HOWARD, TAREK
SCHRUM, ALBERT W
4 key
HECK, SHANE
RUST, DANIEL

select * from arkona.ext_bopname limit 1
select * from ukg.employees limit 10
---------------------------------------------------------------------------------------------------------------------------------
-- 06/29/22 after talking with greg this seems to make more sense
-- some customers with an range of business extending from 2016 thru current
drop table if exists sales;
create temp table sales as
select a.buyer_number, b.bopname_search_name, count(*), min(a.date_capped), max(a.date_capped), array_agg(distinct a.date_capped)
from arkona.ext_bopmast a
join arkona.ext_bopname b on a.buyer_number = b.bopname_record_key
  and b.bopname_company_number = 'RY1'
  and b.company_individ = 'I'
where a.record_status = 'U'  
  and a.date_capped > '01/01/2015'
  and not exists (
    select 1
    from ukg.employees
    where first_name = b.first_name
      and last_name = b.last_company_name)
group by a.buyer_number, b.bopname_search_name
having count(*) > 2
  and extract(year from max(a.date_capped)) in (2022, 2021)
-- having min(a.date_capped) > '12/31/2015'
-- order by b.bopname_search_name
order by min(a.date_capped)
order by count(*) desc 

drop table if exists service cascade;
create temp table service as
select aa.customer_key, bb.bopname_search_name, count(*), min(aa.open_date), max(aa.open_date) 
from (
	select a.ro_number, a.customer_key, a.open_date
	from arkona.ext_sdprhdr a 
	union
	select b.ro_number, b.customer_key, b.open_date
	from arkona.ext_sdprhdr_history b
	where b.open_date > 20151231) aa
join arkona.ext_bopname bb on aa.customer_key = bb.bopname_record_key
  and bb.bopname_company_number = 'RY1'
  and bb.company_individ = 'I'
group by aa.customer_key, bb.bopname_search_name
order by count(*) desc 
limit 10000

select * 
from sales a order by count desc 
join service b on a.buyer_number = b.customer_key
where dds.db2_integer_to_date(b.min) < a.min
order by a.count desc
limit 200


select * 
from sales a 
where extract(year from min) = 2016
order by count desc 

-- sales that look good
262526,QUIBELL, STUART,8
224615,JOHNSON, JEFFREY KENNETH,7,2016-09-06,2021-10-29
315486,WEBER, THOMAS DWAYNE,7,2016-07-07,2021-10-11
1039252,NOVACEK, RONALD WAYNE,6,2016-02-12,2021-11-29
1105411,EMERSON, CHAD MICHAEL,6
273912,MCLEAN, KELLY GENE,5,2016-06-21,2022-05-27
1036351,BAUMGARTEN, CHAD,5,2016-10-24,2022-01-12
1038747,SCHANILEC, NOEL,5,2016-04-20,2021-09-20
1100728,WHEELER, WENDI RAE,4,2016-04-30,2022-05-19
290391,BROSSEAU, JAMES,5,2016-05-23,2021-07-22



select * 
from service 
where customer_key in (262526,224615,315486,1039252,1105411,273912,1036351,1038747,1100728,290391)

select * from dds.dim_Service_type

select a.customer_key, c.vin, a.ro, a.open_date, array_agg(distinct d.opcode), 
	array_agg(distinct e.service_type) as service_type, array_agg(distinct f.payment_type) as payment_type
from dds.fact_repair_order a
join dds.dim_customer b on a.customer_key = b.customer_key
  and b.bnkey in (262526,224615,315486,1039252,1105411,273912,1036351,1038747,1100728,290391)
join dds.dim_vehicle c on a.vehicle_key = c.vehicle_key  
join dds.dim_opcode d on a.opcode_key = d.opcode_key
  and d.opcode not in ('NDEL','HDEL','TSP''DT')
join dds.dim_service_type e on a.service_type_key = e.service_type_key
join dds.dim_payment_type f on a.payment_type_key = f.payment_type_key
group by a.customer_key, c.vin, a.ro, a.open_date
order by service_type

select *
from dds.dim_opcode 
where opcode in ('PS1','DT','DLX','TDW','TSP','TOT','PIC','PS2')

select b.bopname_search_name, a.buyer_number, a.bopmast_stock_number, a.bopmast_vin, a.date_capped, 
  a.retail_price, a.comm_gross, a.f_i_gross, (a.comm_gross + a.f_i_gross), a.record_type
from arkona.ext_bopmast a
join sales b on a.buyer_number = b.buyer_number
where a.buyer_number in (262526,224615,315486,1039252,1105411,273912,1036351,1038747,1100728,290391)
order by buyer_number, date_capped


-- fs statement service gross
select c.store, b.year_month, b.line, b.col, d.gl_account, e.description, sum(-a.amount) as gross
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
join fin.dim_fs_Account d on a.fs_account_key = d.fs_account_key
join fin.dim_account e on d.gl_account = e.account
  and e.current_row
where b.year_month = 202206
  and page = 16
  and line between 21 and 29
  and c.store in ('ry1'	,'ry2')
group by c.store, b.year_month, b.line, b.col, d.gl_account, e.description

-- this looks good enough for service numbers
drop table if exists service_sales;
create temp table service_sales as
select a.control, d.open_date, d.bnkey, d.vin, b.account, b.description, c.journal_code, c.journal, a.amount, b.account_type, b.department
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
  and b.account_type in ('sale','cogs')
  and b.account not in ('166301','166304','266324')
join fin.dim_journal c on a.journal_key = c.journal_key
join (
	select a.ro, open_date, b.bnkey, e.vin
	from dds.fact_repair_order a
	join dds.dim_customer b on a.customer_key = b.customer_key
		and b.bnkey in (262526,224615,315486,1039252,1105411,273912,1036351,1038747,1100728,290391)
	join dds.dim_opcode d on a.opcode_key = d.opcode_key
		and d.opcode not in ('NDEL','HDEL','TSP','PIC')
  join dds.dim_vehicle e on a.vehicle_key = e.vehicle_key		
	group by a.ro, open_date, b.bnkey, e.vin) d on a.control = d.ro
where a.post_status = 'Y'

select * from service_sales where vin = '3GCPYFED7MG384723'

1GCVKREC3GZ405587: 3 service, 4 pdq

select bnkey, vin, min(open_date) as first_visit, max(open_date) as last_visit,
	sum(-parts_sales) as parts_sales, sum(parts_cost) as parts_cost, sum(-parts_sales) - sum(parts_cost) as parts_gross,
	count(*) filter (where service_sales is not null) as service_visits,
	sum(-service_sales) as service_sales, sum(service_cost) as service_cost, sum(-service_sales) - sum(service_cost) as service_gross,
	count(*) filter (where bs_sales is not null) as bs_visits,
	sum(-bs_sales) as bs_sales, sum(bs_cost) as bs_cost, sum(-bs_sales) - sum(bs_cost) as bs_gross,
	count(*) filter (where pdq_sales is not null) as pdq_visits,
	sum(-pdq_sales) as pdq_sales, sum(pdq_cost) as pdq_cost, sum(-pdq_sales) - sum(pdq_cost) as pdq_gross,
	count(*) filter (where detail_sales is not null or detail_cost is not null) as detail_visits,
	sum(-detail_sales) as detail_sales, sum(detail_cost) as detail_cost, coalesce(sum(-detail_sales), 0) - coalesce(sum(detail_cost), 0) as detail_gross			
from (
	select control, open_date, bnkey, vin,
-- 	  count(*) over (partition by bnkey, vin, detail_sales is not null),
		sum(amount) filter (where department = 'parts' and account_type = 'sale') as parts_sales, 
		sum(amount) filter (where department = 'parts' and account_type = 'cogs') as parts_cost,
		sum(amount) filter (where department = 'service' and account_type = 'sale') as service_sales, 
		sum(amount) filter (where department = 'service' and account_type = 'cogs') as service_cost, 
		sum(amount) filter (where department = 'body shop' and account_type = 'sale') as bs_sales, 
		sum(amount) filter (where department = 'body shop' and account_type = 'cogs') as bs_cost, 
		sum(amount) filter (where department = 'quick lane' and account_type = 'sale') as pdq_sales, 
		sum(amount) filter (where department = 'quick lane' and account_type = 'cogs') as pdq_cost,				
		sum(amount) filter (where department = 'detail' and account_type = 'sale') as detail_sales, 
		sum(amount) filter (where department = 'detail' and account_type = 'cogs') as detail_cost
	from service_sales
	group by control, open_date, bnkey, vin) a
group by bnkey, vin	
order by bnkey, vin

select distinct department from service_sales
Detail
Parts
Service
Body Shop
Car Wash
Quick Lane

	select control, open_date, bnkey, vin,
-- 	  count(*) over (partition by bnkey, vin, detail_sales is not null),
		sum(amount) filter (where department = 'parts' and account_type = 'sale') as parts_sales, 
		sum(amount) filter (where department = 'parts' and account_type = 'cogs') as parts_cost,
		sum(amount) filter (where department = 'detail' and account_type = 'sale') as detail_sales, 
		sum(amount) filter (where department = 'detail' and account_type = 'cogs') as detail_cost, 
		sum(amount) filter (where department = 'service' and account_type = 'sale') as service_sales, 
		sum(amount) filter (where department = 'service' and account_type = 'cogs') as service_cost, 
		sum(amount) filter (where department = 'body shop' and account_type = 'sale') as bs_sales, 
		sum(amount) filter (where department = 'body shop' and account_type = 'cogs') as bs_cost, 
		sum(amount) filter (where department = 'car wash' and account_type = 'sale') as cw_sales, 
		sum(amount) filter (where department = 'car wash' and account_type = 'cogs') as cw_cost, 
		sum(amount) filter (where department = 'quick lane' and account_type = 'sale') as pdq_sales, 
		sum(amount) filter (where department = 'quick lane' and account_type = 'cogs') as pdq_cost
	from service_sales
	where vin = '1GCVKREC3GZ405587'
	group by control, open_date, bnkey, vin

-- dealer vault
-- does not have honda data
create index on dv.service_data(ro_number);

select * from dv.service_data where ro_number = '2747473'

select left(ro_number, 1), count(*) from dv.service_Data group by left(ro_number,1)

select * 
from (
	select 'sales' as source, stock_number, vin, delivery_date
	from dv.sales_data
	where customer_number = '262526') a
full outer  join (
	select 'service' as source, vin, ro_number, '12/31/1899'::date + open_date::integer
	from dv.service_data
	where customer_number = '262526') b on a.vin = b.vin
order by a.delivery_date

select * 
from dv.sales_data
where stock_number = 'G35119G'

select ro_number, payment_method
from dv.service_data
where customer_number::integer in (262526,224615,315486,1039252,1105411,273912,1036351,1038747,1100728,290391)
  and internal_total_sale::numeric = 0
--   and internal_total_sale = total_sale
order by ro_number


payment_type: BS, RE
internal/customer/warranty: broken out as custerom_cost/customer_sale/customer_labor_cost etc


select * from dv.service_data where ro_number = '16318233'