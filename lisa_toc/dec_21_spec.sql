﻿/*
07/3/22
take over from jon_spec.sql
*/

-----------------------------------------------------------------------------------------------
--< sales
-----------------------------------------------------------------------------------------------
-- 06/29/22 after talking with greg this seems to make more sense
-- some customers with an range of business extending from 2016 thru current
drop table if exists toc.sales_1;
create table toc.sales_1 as
select a.buyer_number, b.bopname_search_name, count(*), min(a.date_capped), max(a.date_capped), array_agg(distinct a.date_capped)
from arkona.ext_bopmast a
join arkona.ext_bopname b on a.buyer_number = b.bopname_record_key
  and b.bopname_company_number = 'RY1'
  and b.company_individ = 'I'
where a.record_status = 'U'  
  and a.date_capped > '01/01/2015'
  and not exists (
    select 1
    from ukg.employees
    where first_name = b.first_name
      and last_name = b.last_company_name)
group by a.buyer_number, b.bopname_search_name
having count(*) > 2
  and extract(year from max(a.date_capped)) in (2022, 2021);
comment on table toc.sales_1 is 'first cut at sales data for the dec_21 spec';



-- from this list
select * 
from toc.sales_1 a 
where extract(year from min) = 2016
order by count desc 

-- chose these 10 sales customers as the base
262526,QUIBELL, STUART,8
224615,JOHNSON, JEFFREY KENNETH,7,2016-09-06,2021-10-29
315486,WEBER, THOMAS DWAYNE,7,2016-07-07,2021-10-11
1039252,NOVACEK, RONALD WAYNE,6,2016-02-12,2021-11-29
1105411,EMERSON, CHAD MICHAEL,6
273912,MCLEAN, KELLY GENE,5,2016-06-21,2022-05-27
1036351,BAUMGARTEN, CHAD,5,2016-10-24,2022-01-12
1038747,SCHANILEC, NOEL,5,2016-04-20,2021-09-20
1100728,WHEELER, WENDI RAE,4,2016-04-30,2022-05-19
290391,BROSSEAU, JAMES,5,2016-05-23,2021-07-22

-- sales data
-- go with the accounting sales data
drop table if exists toc.sales_2;
create table toc.sales_2 as
select b.bopname_search_name, a.buyer_number, a.bopmast_stock_number, a.bopmast_vin, a.date_capped, 
  case
    when a.record_type = 'L' then a.lease_price
    else a.retail_price
  end as sold_amount, a.house_gross as vehicle_gross, a.comm_gross, a.house_gross, a.f_i_gross, (a.comm_gross + a.f_i_gross), a.record_type
from arkona.ext_bopmast a
join toc.sales_1 b on a.buyer_number = b.buyer_number
where a.buyer_number in (262526,224615,315486,1039252,1105411,273912,1036351,1038747,1100728,290391)
order by a.buyer_number, a.date_capped

-- get gross numbers from accounting
drop table if exists toc.sales_accounts_1;
create table toc.sales_accounts_1 as
-- get gross from accounting
select distinct d.gl_account, e.account_type, e.department
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201601 and 202206
  and b.page between 5 and 15 -- new cars
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
  and e.account not in ('144502','164502','285010', '285011', '285100','285101')
union
-- used car gross accounts
select distinct d.gl_account, e.account_type, e.department
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201601 and 202206
  and b.page = 16
  and b.line between 1 and 14
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
  and e.account not in ('144502','164502','285010', '285011', '285100','285101')
union
-- f/i accounts
select distinct d.gl_account, e.account_type, e.department
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201601 and 202206
  and b.page = 17
  and b.line between 1 and 20
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
  and e.account not in ('144502','164502','285010', '285011', '285100','285101');
create index on toc.sales_accounts_1(gl_account);

select a.bopmast_stock_number, d.account_type, d.department, sum(b.amount)
from toc.sales_2 a
join  fin.fact_gl b on a.bopmast_stock_number = b.control
  and b.post_status = 'Y'
join fin.dim_Account c on b.account_key = c.account_key
join toc.sales_accounts_1 d on c.account = d.gl_account
group by a.bopmast_stock_number, d.account_type, d.department
order by d.department, d.account_type

select a.bopmast_stock_number, 
  sum(-b.amount) filter (where d.department <> 'finance' and d.account_type in ('other income','sale')) as sale,
  sum(b.amount) filter (where d.department <> 'finance' and d.account_type in ('cogs')) as cost,
  -sum(b.amount) filter (where d.department <> 'finance') as front,
  sum(-b.amount) filter (where d.department = 'finance' and d.account_type in ('other income','sale')) as fi_sale,
  sum(b.amount) filter (where d.department = 'finance' and d.account_type in ('cogs','Other Expense')) as fi_cost,
  -sum(b.amount) filter (where d.department = 'finance') as fi_gross
from toc.sales_2 a
join  fin.fact_gl b on a.bopmast_stock_number = b.control
  and b.post_status = 'Y'
join fin.dim_Account c on b.account_key = c.account_key
join toc.sales_accounts_1 d on c.account = d.gl_account
group by a.bopmast_stock_number

drop table if exists toc.sales_3;
create table toc.sales_3 as
select a.bopname_search_name, a.buyer_number, a.bopmast_stock_number, a.bopmast_vin, a.date_capped, 
  b.front_sale, b.front_cost, b.front_gross, b.fi_sale, b.fi_cost, b.fi_gross
from toc.sales_2 a
left join (
	select a.bopmast_stock_number, 
		sum(-b.amount) filter (where d.department <> 'finance' and d.account_type in ('other income','sale')) as front_sale,
		sum(b.amount) filter (where d.department <> 'finance' and d.account_type in ('cogs')) as front_cost,
		-sum(b.amount) filter (where d.department <> 'finance') as front_gross,
		sum(-b.amount) filter (where d.department = 'finance' and d.account_type in ('other income','sale')) as fi_sale,
		sum(b.amount) filter (where d.department = 'finance' and d.account_type in ('cogs','Other Expense')) as fi_cost,
		-sum(b.amount) filter (where d.department = 'finance') as fi_gross
	from toc.sales_2 a
	join  fin.fact_gl b on a.bopmast_stock_number = b.control
		and b.post_status = 'Y'
	join fin.dim_Account c on b.account_key = c.account_key
	join toc.sales_accounts_1 d on c.account = d.gl_account
	group by a.bopmast_stock_number) b on a.bopmast_stock_number = b.bopmast_stock_number
where a.bopmast_Stock_number not in ('10127XX','20912','29687L') -- brosseau, 2010 deal, BAUMGARTEN lease in 2010, bought out in 2016
order by a.bopname_search_name, a.date_capped;
comment on table toc.sales_3 is 'sales data with accounting values';

select 'sales',  bopname_search_name, buyer_number, bopmast_vin
from toc.sales_3

-----------------------------------------------------------------------------------------------
--/> sales
-----------------------------------------------------------------------------------------------	
-----------------------------------------------------------------------------------------------
--< service
-----------------------------------------------------------------------------------------------
-- drop table if exists toc.service_1 cascade;
-- create table toc.service_1 as
-- select aa.customer_key, bb.bopname_search_name, count(*), min(aa.open_date), max(aa.open_date) 
-- from (
-- 	select a.ro_number, a.customer_key, a.open_date
-- 	from arkona.ext_sdprhdr a 
-- 	union
-- 	select b.ro_number, b.customer_key, b.open_date
-- 	from arkona.ext_sdprhdr_history b
-- 	where b.open_date > 20151231) aa
-- join arkona.ext_bopname bb on aa.customer_key = bb.bopname_record_key
--   and bb.bopname_company_number = 'RY1'
--   and bb.company_individ = 'I'
-- group by aa.customer_key, bb.bopname_search_name;
-- comment on table toc.service_1 is 'first cut at service data for the dec_21 spec';

drop table if exists toc.service_1;
create table toc.service_1 as
select a.control, d.open_date, d.bnkey, d.vin, b.account, b.description, c.journal_code, c.journal, a.amount, b.account_type, b.department
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
  and b.account_type in ('sale','cogs')
  and b.account not in ('166301','166304','266324')
join fin.dim_journal c on a.journal_key = c.journal_key
join (
	select a.ro, open_date, b.bnkey, e.vin
	from dds.fact_repair_order a
	join dds.dim_customer b on a.customer_key = b.customer_key
		and b.bnkey in (262526,224615,315486,1039252,1105411,273912,1036351,1038747,1100728,290391)
	join dds.dim_opcode d on a.opcode_key = d.opcode_key
		and d.opcode not in ('NDEL','HDEL','TSP','PIC')
  join dds.dim_vehicle e on a.vehicle_key = e.vehicle_key		
	group by a.ro, open_date, b.bnkey, e.vin) d on a.control = d.ro
where a.post_status = 'Y';

drop table if exists toc.service_2;
create table toc.service_2 as
select bnkey, vin, min(open_date) as first_visit, max(open_date) as last_visit,
	sum(-parts_sales) as parts_sales, sum(parts_cost) as parts_cost, sum(-parts_sales) - sum(parts_cost) as parts_gross,
	count(*) filter (where service_sales is not null) as service_visits,
	sum(-service_sales) as service_sales, sum(service_cost) as service_cost, sum(-service_sales) - sum(service_cost) as service_gross,
	count(*) filter (where bs_sales is not null) as bs_visits,
	sum(-bs_sales) as bs_sales, sum(bs_cost) as bs_cost, sum(-bs_sales) - sum(bs_cost) as bs_gross,
	count(*) filter (where pdq_sales is not null) as pdq_visits,
	sum(-pdq_sales) as pdq_sales, sum(pdq_cost) as pdq_cost, sum(-pdq_sales) - sum(pdq_cost) as pdq_gross,
	count(*) filter (where detail_sales is not null or detail_cost is not null) as detail_visits,
	sum(-detail_sales) as detail_sales, sum(detail_cost) as detail_cost, coalesce(sum(-detail_sales), 0) - coalesce(sum(detail_cost), 0) as detail_gross			
from (
	select control, open_date, bnkey, vin,
-- 	  count(*) over (partition by bnkey, vin, detail_sales is not null),
		sum(amount) filter (where department = 'parts' and account_type = 'sale') as parts_sales, 
		sum(amount) filter (where department = 'parts' and account_type = 'cogs') as parts_cost,
		sum(amount) filter (where department = 'service' and account_type = 'sale') as service_sales, 
		sum(amount) filter (where department = 'service' and account_type = 'cogs') as service_cost, 
		sum(amount) filter (where department = 'body shop' and account_type = 'sale') as bs_sales, 
		sum(amount) filter (where department = 'body shop' and account_type = 'cogs') as bs_cost, 
		sum(amount) filter (where department = 'quick lane' and account_type = 'sale') as pdq_sales, 
		sum(amount) filter (where department = 'quick lane' and account_type = 'cogs') as pdq_cost,				
		sum(amount) filter (where department = 'detail' and account_type = 'sale') as detail_sales, 
		sum(amount) filter (where department = 'detail' and account_type = 'cogs') as detail_cost
	from toc.service_1
	group by control, open_date, bnkey, vin) a
group by bnkey, vin	
order by bnkey, vin;


select *
from toc.service_1
where control = '19300381'
-----------------------------------------------------------------------------------------------
--/> service
-----------------------------------------------------------------------------------------------
saved as dec_21_spec_v1.xlsx
manually edited spreadsheet: 
	removed garbage detail data
	removed zeros

select a.bopname_search_name  || ' / ' || a.buyer_number::text as customer, 
  a.bopmast_vin as vin, a.date_capped as acq_date, a.front_sale, a.front_cost, a.front_gross, a.fi_sale, a.fi_cost, a.fi_gross, 
	b.parts_sales, b.parts_cost, b.parts_gross,
	b.service_visits, b.service_sales, b.service_cost, b.service_gross,
	b.bs_visits, b.bs_sales, b.bs_cost, b.bs_gross, 
	b.pdq_visits, b.pdq_sales, b.pdq_cost, b.pdq_gross,
	b.detail_visits, b.detail_sales, b.detail_cost, b.detail_gross,
	b.first_visit as first_transaction, b.last_visit as last_transaction
from (
	select *
	from toc.sales_3) a
left join (
	select *
	from toc.service_2) b on a.buyer_number = b.bnkey
		and a.bopmast_vin = b.vin
order by a.bopname_search_name, a.date_capped, a.bopmast_vin  


