-- aftons query
SELECT a.*, b.fromts AS arrived, c.fromts AS released, d.fromts AS completed, e.fromts AS delivered,
round(cast(TIMESTAMPDIFF(SQL_TSI_HOUR,b.fromts ,e.fromts) AS  SQL_NUMERIC)/24,1) AS cust_lead_days,
round(cast(TIMESTAMPDIFF(SQL_TSI_HOUR,c.fromts ,d.fromts) AS  SQL_NUMERIC)/24,1) AS wip_lead_days
FROM (
		 SELECT bsj.jobid, bsj.customer, bsj.ronumber, bsv.vehicleyear, bsv.vehiclemake, bsv.vehiclemodel,bsv.vin
 		 FROM BSJobs bsj
 		 INNER JOIN BSVehicles bsv ON bsv.JobID = bsj.JobID
 		 WHERE bsj.jobid = 184985) A 
left JOIN ( -- arrived
		 SELECT * 
		 FROM JobProgressStatuses
		 WHERE status = 'RepairVehicleArrived_RepairVehicleArrived') AS b on a.jobid = b.jobid
left JOIN ( -- released earliest BETWEEN blueprint AND repair
		 SELECT jobid, MIN(fromts) AS fromts
		 FROM JobProgressStatuses
		 WHERE status in ('Release_Blueprint', 'Release_Repair')
		 GROUP BY jobid) AS c on a.jobid = c.jobid
left JOIN ( -- completed
		 SELECT * 
		 FROM JobProgressStatuses
		 WHERE status = 'JobStatus_JobCompleted') AS d on a.jobid = d.jobid
left JOIN ( -- delivered
		 SELECT * 
		 FROM JobProgressStatuses
		 WHERE status = 'Delivered_Delivered') AS e on a.jobid = e.jobid


SELECT jobid
FROM bsjobs aa
JOIN (
		 SELECT ro 	 				 									 
		 FROM DDS.factrepairorder a
		 JOIN dds.day b on a.closedatekey = b.datekey
		   AND b.thedate BETWEEN '01/01/2022' AND '03/31/2022'
		 GROUP BY ro)as bb on aa.ronumber = CAST(bb.ro as sql_integer)
		 
-- limit to 1st quarter ros		 
SELECT a.*, b.fromts AS arrived, c.fromts AS released, d.fromts AS completed, e.fromts AS delivered,
round(cast(TIMESTAMPDIFF(SQL_TSI_HOUR,b.fromts ,e.fromts) AS  SQL_NUMERIC)/24,1) AS cust_lead_days,
round(cast(TIMESTAMPDIFF(SQL_TSI_HOUR,c.fromts ,d.fromts) AS  SQL_NUMERIC)/24,1) AS wip_lead_days
FROM (
		 SELECT bsj.jobid, bsj.customer, bsj.ronumber, bsv.vehicleyear, bsv.vehiclemake, bsv.vehiclemodel,bsv.vin
 		 FROM BSJobs bsj
 		 INNER JOIN BSVehicles bsv ON bsv.JobID = bsj.JobID
 		 WHERE bsj.jobid IN (
		 			 SELECT jobid
				   FROM bsjobs aa
					 JOIN (
		 			 			SELECT ro 	 				 									 
		 						FROM DDS.factrepairorder a
		 						JOIN dds.day b on a.closedatekey = b.datekey
		   							 AND b.thedate BETWEEN '01/01/2022' AND '03/31/2022'
								WHERE left(ro,2) = '18'
		 			 			GROUP BY ro)as bb on aa.ronumber = CAST(bb.ro as sql_integer))) A 
left JOIN ( -- arrived
		 SELECT * 
		 FROM JobProgressStatuses
		 WHERE status = 'RepairVehicleArrived_RepairVehicleArrived') AS b on a.jobid = b.jobid
left JOIN ( -- released earliest BETWEEN blueprint AND repair
		 SELECT jobid, MIN(fromts) AS fromts
		 FROM JobProgressStatuses
		 WHERE status in ('Release_Blueprint', 'Release_Repair')
		 GROUP BY jobid) AS c on a.jobid = c.jobid
left JOIN ( -- completed
		 SELECT * 
		 FROM JobProgressStatuses
		 WHERE status = 'JobStatus_JobCompleted') AS d on a.jobid = d.jobid
left JOIN ( -- delivered
		 SELECT * 
		 FROM JobProgressStatuses
		 WHERE status = 'Delivered_Delivered') AS e on a.jobid = e.jobid		 
		 
		 
-- TRIM down to just the fields i need
-- DELETE FROM z_jon_afton_lead_times
-- INSERT INTO z_jon_afton_lead_times
-- 04/27/22 don't need a TABLE, put this query IN the python program that
--   extracts the advantage data AND loads the postgresql TABLE
-- (python_projects\ext_ads\bs_afton_lead_times.py)
SELECT a.jobid, a.ronumber, b.fromts AS arrived, c.fromts AS released, d.fromts AS completed, e.fromts AS delivered
-- round(cast(TIMESTAMPDIFF(SQL_TSI_HOUR,b.fromts ,e.fromts) AS  SQL_NUMERIC)/24,1) AS cust_lead_days,
-- round(cast(TIMESTAMPDIFF(SQL_TSI_HOUR,c.fromts ,d.fromts) AS  SQL_NUMERIC)/24,1) AS wip_lead_days
--INTO z_jon_afton_lead_times
FROM (
		 SELECT bsj.jobid, bsj.customer, bsj.ronumber, bsv.vehicleyear, bsv.vehiclemake, bsv.vehiclemodel,bsv.vin
 		 FROM BSJobs bsj
 		 INNER JOIN BSVehicles bsv ON bsv.JobID = bsj.JobID
 		 WHERE bsj.jobid IN (
		 			 SELECT jobid
				   FROM bsjobs aa
					 JOIN (
		 			 			SELECT ro 	 				 									 
		 						FROM DDS.factrepairorder a
		 						JOIN dds.day b on a.closedatekey = b.datekey
		   							 AND b.thedate BETWEEN '04/01/2022' AND '04/16/2022'
								WHERE left(ro,2) = '18'
		 			 			GROUP BY ro)as bb on aa.ronumber = CAST(bb.ro as sql_integer))) A 
left JOIN ( -- arrived
		 SELECT * 
		 FROM JobProgressStatuses
		 WHERE status = 'RepairVehicleArrived_RepairVehicleArrived') AS b on a.jobid = b.jobid
left JOIN ( -- released earliest BETWEEN blueprint AND repair
		 SELECT jobid, MIN(fromts) AS fromts
		 FROM JobProgressStatuses
		 WHERE status in ('Release_Blueprint', 'Release_Repair')
		 GROUP BY jobid) AS c on a.jobid = c.jobid
left JOIN ( -- completed
		 SELECT * 
		 FROM JobProgressStatuses
		 WHERE status = 'JobStatus_JobCompleted') AS d on a.jobid = d.jobid
left JOIN ( -- delivered
		 SELECT * 
		 FROM JobProgressStatuses
		 WHERE status = 'Delivered_Delivered') AS e on a.jobid = e.jobid		 
								
SELECT * FROM z_jon_afton_lead_times						

DROP TABLE 	z_jon_afton_lead_times	