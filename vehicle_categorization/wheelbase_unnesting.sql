﻿
select aa.*, bb.shape, bb.size
from (
  select distinct make , model, unnest((replace(wheelbase, '{,', '{'))::numeric[]) as wheelbase
  from ( -- remove comma immediately after first bracket
    select make, model, replace(wheelbase, ',}', '}') as wheelbase
    from ( -- remove trailing comma before bracket
      select make, model, '{' || replace(wheelbase, ',,', ',')  ||'}' as wheelbase
      from ( -- remove - TBD -
        select make, model, replace(wheelbase, '- TBD -', '') as wheelbase
        from ( -- remove none
          select make, model, replace(wheelbase, 'none', '') as wheelbase
          from veh.vehicle_types) a) b) c) d) aa
left join veh.shape_size_classifications bb on aa.make = bb.make and aa.model = bb.model          
order by wheelbase


select bb.shape, bb.size, min(wheelbase), max(wheelbase), count(*)
from ( -- remove comma immediately after first bracket
  select distinct make , model, unnest((replace(wheelbase, '{,', '{'))::numeric[]) as wheelbase
  from ( -- remove trailing comma before bracket
    select make, model, replace(wheelbase, ',}', '}') as wheelbase
    from ( -- remove double commas (from removing none & TBD
      select make, model, '{' || replace(wheelbase, ',,', ',')  ||'}' as wheelbase
      from ( -- remove - TBD -
        select make, model, replace(wheelbase, '- TBD -', '') as wheelbase
        from ( -- remove none
          select make, model, replace(wheelbase, 'none', '') as wheelbase
          from veh.vehicle_types) a) b) c) d) aa
left join veh.shape_size_classifications bb on aa.make = bb.make and aa.model = bb.model  
group by bb.shape, bb.size
order by bb.shape, 
  case
    when size = 'compact' then 1
    when size = 'small' then 2
    when size = 'medium' then 3
    when size = 'large' then 4
    when size = 'extra large' then 5
  end 