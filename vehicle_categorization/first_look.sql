﻿
/*

1/24/21

everything relevant in this script has been moved to ...\vehicles\additional_classification_data.sql






*/
select *
from jon.configurations
limit 100

select count(*)
from jon.configurations


select model_year, make, model, trim_level, market_class, body_type
from jon.configurations
group by model_year, make, model, trim_level, market_class, body_type


select model_year, make, model  -- 369
from jon.configurations
group by model_year, make, model

select model_year, make, model, trim_level -- 1402
from jon.configurations
group by model_year, make, model, trim_level

select model_year, make, model, trim_level, body_type  -- 1642
from jon.configurations
group by model_year, make, model, trim_level, body_type

-- multiple body types, eg, extended cap, crew cab
select model_year, make, model, trim_level
from (
select model_year, make, model, trim_level, body_type  
from jon.configurations
group by model_year, make, model, trim_level, body_type) a
group by model_year, make, model, trim_level
having count(*) > 1

select model_year, make, model, trim_level, body_type, market_class -- 2252
from jon.configurations
group by model_year, make, model, trim_level, body_type, market_class

-- multiple market class: 2WD Sport Utility Vehicles vs 4WD Sport Utility Vehicles
select model_year, make, model, trim_level, body_type
from (
select model_year, make, model, trim_level, body_type, market_class -- 2252
from jon.configurations
group by model_year, make, model, trim_level, body_type, market_class) a
group by model_year, make, model, trim_level, body_type
having count(*) > 1

select *
from jon.configurations
where model_year = 2020
  and model = 'suburban' 
  and trim_level = 'premier'
  and body_type = 'sport utility'

select * 


select *
from greg.uc_Base_vehicles 
limit 100

select *
from ads.ext_vehicle_items
where vehicleitemid = 'e474c905-849a-3a44-bb4f-37d97b9b579e'

select *
from (
select yearmodel as model_year, make, model, trim  -- 264
from ads.ext_vehicle_items
where vinresolved
  and make in ('chevrolet','cadillac','buick','gmc','honda','nissan')
  and yearmodel in ('2018','2019','2020','20201')
group by yearmodel, make, model, trim) a 
full outer join (
select vin_year_model as model_year, make, model, series -- 461
from polk.vehicle_categories
where make in ('chevrolet','cadillac','buick','gmc','honda','nissan')
  and vin_year_model in ('2018','2019','2020','20201')
group by vin_year_model, make, model, series) b on a.model_year = b.model_year
  and a.make = b.make
  and a.model = b.model
  and a.trim = b.series


select *
from (
select yearmodel as model_year, make, model, trim  -- 264
from ads.ext_vehicle_items
where vinresolved
  and make in ('chevrolet','cadillac','buick','gmc','honda','nissan')
  and yearmodel in ('2018','2019','2020','20201')
group by yearmodel, make, model, trim) a 
full outer join (
select model_year, make, model, trim_level
from jon.configurations
group by model_year, make, model, trim_level) b on a.model_year::integer = b.model_year
  and a.make = b.make
  and a.model = b.model
  and a.trim = b.trim_level
order by b.model_year, b.make, b.model, b.trim_level  

select *
from ads.ext_vehicle_items
where yearmodel = '2020'
  and model = 'malibu'

select * 
from (
select model_year, make, model
from jon.configurations
group by model_year, make, model) a  
full outer join (
select yearmodel as model_year, make, model
from ads.ext_vehicle_items
where vinresolved
  and make in ('chevrolet','cadillac','buick','gmc','honda','nissan')
  and yearmodel in ('2018','2019','2020','20201')
group by yearmodel, make, model) b on a.model_year = b.model_year::integer and a.make = b.make and a.model = b.model
full outer join (
select vin_year_model as model_year, make, model
from polk.vehicle_categories
where make in ('chevrolet','cadillac','buick','gmc','honda','nissan')
  and vin_year_model in ('2018','2019','2020','20201')
group by vin_year_model, make, model) c on a.model_year = c.model_year::integer and a.make = c.make and a.model = c.model




select *
from jon.describe_vehicle_by_style_id
limit 1000

select count(distinct chrome_style_id)  -- 3317
from jon.describe_Vehicle_by_style_id



-- most have pictures
select max(chrome_style_id), model_year, make, model, max(picture_url)
from (
  select e.chrome_style_id,
    f.style -> 'attributes' ->> 'modelYear' as model_year,
    f.style -> 'division'  ->> '$value' as make,
    f.style -> 'model'  ->> '$value' as model,
    f.style -> 'stockImage' -> 'attributes' ->> 'url' as picture_url
  from jon.describe_vehicle_by_style_id  e 
  left join jsonb_array_elements(e.response->'style') as f(style) on true 
  where e.current_row) x
group by model_year, make, model  



-- flesh out the chrome data
-- first level of categorization size and shape, based on make and model
-- need to backfill chrome data to 2005, wilkie said 15 years was good
-- as well as current off brand models for 2018 - 2020

select a.model_year, b.division, c.model, d.style_name, d.chrome_Style_id
from jon.model_years a
left join jon.divisions b on a.model_year = b.model_year
left join jon.models c on b.model_year = c.model_year
  and b.division_id = c.division_id
left join jon.styles d on c.model_id = d.model_id 
order by a.model_year, b.division, c.model

-- to get a picture, will need a sample chrome_Style_id for each model
select a.model_year, b.division, c.model, min(d.chrome_Style_id), max(chrome_style_id)
from jon.model_years a
left join jon.divisions b on a.model_year = b.model_year
left join jon.models c on b.model_year = c.model_year
  and b.division_id = c.division_id
left join jon.styles d on c.model_id = d.model_id 
where c.model is null
  and a.model_year > 2004
group by a.model_year, b.division, c.model
order by a.model_year, b.division


-- is it really necessary to have type 2 history?
select * from jon.describe_Vehicle_by_style_id order by chrome_Style_id offset 1000 limit 100

select chrome_Style_id, count(*) 
from jon.describe_vehicle_by_style_id
group by chrome_Style_id
order by count(*) desc 

-- 33 versions of this style
select *
from jon.describe_vehicle_by_style_id
where chrome_style_id = '406774'
order by from_date

-- days since first inserted to latest update
select * 
from (
select chrome_Style_id, count(*), 
--   array_agg(from_date),
  case when count(*) > 1 then min(from_date) end as min_from, 
  case when count(*) > 1 then max(from_date) end as max_from,
  case when count(*) > 1 then max(from_date) - min(from_date) end as diff_days
from jon.describe_vehicle_by_style_id
group by chrome_Style_id
order by chrome_Style_id) x
order by diff_days desc nulls last 

--------------------------------------------------------------------------


-- 10/31/20 to get started, just do make/model
drop table if exists tem.chrome cascade;
create table tem.chrome as
select max(x.chrome_style_id) as chrome_style_id, x.make::citext, x.model::citext, max(x.picture_url) as picture_url, string_agg(distinct y.market_class, ',') as chrome_market_class
from (
  select e.chrome_style_id,
--     f.style -> 'attributes' ->> 'modelYear' as model_year,
    f.style -> 'division'  ->> '$value' as make,
    f.style -> 'model'  ->> '$value' as model,
    f.style -> 'stockImage' -> 'attributes' ->> 'url' as picture_url
  from jon.describe_vehicle_by_style_id  e 
  left join jsonb_array_elements(e.response->'style') as f(style) on true 
  where e.current_row) x
left join jon.configurations y on x.make = y.make 
  and x.model = y.model  
where x.model not like '%LCF%'  
group by x.make, x.model;
create unique index on tem.chrome(make,model);
comment on table tem.chrome is '2018 and up gm, honda & nissan only';

select * from tem.chrome;

select * from polk.vehicle_categories limit 10;

drop table if exists tem.polk cascade;
create table tem.polk as
select make::citext, model::citext, string_agg(distinct segment, ',') as polk_segment
from polk.vehicle_categories
where make in ('chevrolet','buick','gmc','cadillac','honda','nissan')
  and vin_year_model in ('2018','2019','2020','20201')
group by make, model ;
create index on tem.polk(make);
create index on tem.polk(model);

select * from tem.polk;

select * from ads.ext_make_model_classifications limit 10;

select * from ads.ext_make_model_classifications where commercial

-- too much noise if i don't limit model_year for now
drop table if exists tem.tool cascade;
create table tem.tool as
select a.make, a.model, string_agg(distinct split_part(b.vehicletype, '_', 2) ||'-'||split_part(b.vehiclesegment, '_', 2),',') as tool_shape_size,
  case when string_agg(distinct luxury::text,',') like '%true%' then 'true' else null end as luxury, 
  case when string_agg(distinct commercial::text,',') like '%true%' then 'true' else null end as comm, 
  case when string_agg(distinct sport::text,',') like '%true%' then 'true' else null end as sport
from ads.ext_vehicle_items a
join ads.ext_make_model_classifications b on a.make = b.make
  and a.model = b.model
where a.make in ('chevrolet','buick','gmc','cadillac','honda','nissan')
  and a.yearmodel in ('2018','2019','2020','20201') 
group by a.make, a.model;
create index on tem.tool(make);
create index on tem.tool(model);

-- -- this throws this: ERROR:  FULL JOIN is only supported with merge-joinable or hash-joinable join conditions
-- -- believe it is caused by the or in the join
-- select a.*, b.model as polk_model, b.polk_segment, c.model as tool_model, c.tool_shape_size
-- -- select *
-- from tem.chrome a
-- full outer join tem.polk b on a.make = b.make
--   and a.model = b.model
-- full outer join tem.tool c on (a.make = c.make or b.make = c.make)
--   and (a.model = c.model  or b.model = c.model)
-- -- order by a.make, a.model, b.make, b.model
-- order by coalesce(a.make, b.make, c.make), coalesce(a.model, b.model, c.model)

-- this seems to fix it, polk regal on same line as tool regal, polk ats on same line as tool ats
-- save it as compare_chrome_polk_black_book_v1
-- this was used as first cut of the wilkie page
select a.*, b.model as polk_model, b.polk_segment, c.model as tool_model, c.tool_shape_size, c.luxury, c.comm, c.sport
from tem.chrome a
full outer join tem.polk b on a.make = b.make
  and a.model = b.model
full outer join tem.tool c on coalesce(a.make, b.make) = c.make
  and coalesce(a.model, b.model) = c.model
order by coalesce(a.make, b.make, c.make), coalesce(a.model, b.model, c.model)


-- 12/12/2020
-- add the additional makes from chrome, chr2.base_1, some model years 2018 - 2021

select * from chr2.base_1 limit 50

select division, model, count(*), count(*) over ()  -- 469 make/models
from chr2.base_1
where subdivision not like '%medium%'
group by division, model

-- where i want to go with this is, for attributes with multiple values, return min(value) - max(value)
-- 12/15 modified to use veh.vehicle_types

alter table tem.chrome_2
rename to z_unused_chrome_2;

drop table if exists tem.chrome_2 cascade;
create table tem.chrome_2 as
select a.make, 
  case 
    when a.make = a.subdivision then null
    else a.subdivision
  end as subdivision, 
  a.model, 
  case
    when cardinality(array_agg(distinct a.wheelbase)) = 1 then (min(a.wheelbase))::text
    else (min(a.wheelbase))::text || '-' || (max(a.wheelbase))::text
  end as wheelbase,
  case
    when cardinality(array_agg(distinct a.overall_length)) = 1 then (min(a.overall_length))::text
    else (min(a.overall_length))::text || '-' || (max(a.overall_length))::text
  end as overall_length,
  case
    when cardinality(array_agg(distinct a.pass_doors)) = 1 then (min(a.pass_doors))::text
    else (min(a.pass_doors))::text || '-' || (max(a.pass_doors))::text
  end as pass_doors,  string_agg(distinct a.drivetrain, ',') as drivetrain,
  max(b.type_segment) as type_segment, min(image_url) as image_url_1, max(image_url) as image_url_2
from veh.vehicle_types a --chr2.base_1 a
left join ( -- eliminates Medium Duty trucks and generates a vertical likst of types/segments
  select make, model, string_agg(distinct coalesce(vehicle_type, '') ||  ' : ' || coalesce(segment, ''), E'\n') as type_segment 
  from veh.vehicle_types --chr2.base_1
  where subdivision not like '%medium%'
    and vehicle_type is not null
    and segment is not null
  group by make, model) b on a.make = b.make and a.model = b.model
where a.subdivision not like '%medium%'
group by a.make, a.subdivision, a.model;
create unique index on tem.chrome_2(make,subdivision, model);

select division 
from chr2.base_1
group by division

select * from veh.vehicle_types where subdivision like '%medium%'

drop table if exists tem.polk_2 cascade;
create table tem.polk_2 as
select make::citext, model::citext, string_agg(distinct segment, ',') as polk_segment
from polk.vehicle_categories
where make in (
  select division 
  from chr2.base_1
  group by division)
  and vin_year_model in ('2018','2019','2020','20201')
group by make, model ;
create index on tem.polk_2(make);
create index on tem.polk_2(model);

drop table if exists tem.tool_2 cascade;
create table tem.tool_2 as
select a.make, a.model, string_agg(distinct split_part(b.vehicletype, '_', 2) ||'-'||split_part(b.vehiclesegment, '_', 2),',') as tool_shape_size,
  case when string_agg(distinct luxury::text,',') like '%true%' then 'true' else null end as luxury, 
  case when string_agg(distinct commercial::text,',') like '%true%' then 'true' else null end as comm, 
  case when string_agg(distinct sport::text,',') like '%true%' then 'true' else null end as sport
from ads.ext_vehicle_items a
join ads.ext_make_model_classifications b on a.make = b.make
  and a.model = b.model
where a.make in (
  select division 
  from chr2.base_1
  group by division)
  and a.yearmodel in ('2018','2019','2020','2021') 
group by a.make, a.model;
create index on tem.tool_2(make);
create index on tem.tool_2(model);

-- 01/12/21 don't limit tool to vehicle items
-- case in point, volkswagen beetle, it exists in make_model_classifications, but not in vehicle_items for these model_years

drop table if exists tem.tool_2 cascade;
create table tem.tool_2 as
select b.make, b.model, string_agg(distinct split_part(b.vehicletype, '_', 2) ||'-'||split_part(b.vehiclesegment, '_', 2),',') as tool_shape_size,
  case when string_agg(distinct luxury::text,',') like '%true%' then 'true' else null end as luxury, 
  case when string_agg(distinct commercial::text,',') like '%true%' then 'true' else null end as comm, 
  case when string_agg(distinct sport::text,',') like '%true%' then 'true' else null end as sport
from ads.ext_make_model_classifications b
where b.make in (
  select division 
  from chr2.base_1
  group by division)
group by b.make, b.model;
create index on tem.tool_2(make);
create index on tem.tool_2(model);



select a.*, b.model as polk_model, b.polk_segment, c.model as tool_model, c.tool_shape_size, c.luxury, c.comm, c.sport
from tem.chrome_2 a
full outer join tem.polk_2 b on a.make = b.make
  and a.model = b.model
full outer join tem.tool_2 c on coalesce(a.make, b.make) = c.make
  and coalesce(a.model, b.model) = c.model
order by coalesce(a.make, b.make, c.make), coalesce(a.model, b.model, c.model)



-- chrome - polk shim


select a.make, a.model, b.* -- b.model as polk_model, b.polk_segment
from tem.chrome_2 a
full outer join tem.polk_2 b on a.make = b.make
  and a.model = b.model
order by coalesce(a.make, b.make), coalesce(a.model, b.model)