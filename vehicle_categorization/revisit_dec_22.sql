﻿/*
12/22/22
the immediate issue is that  in generating the new toc categories for the uc replenishment project
the categories in the tool are incorrect
my intial thoughts are to synchronize the entire set across advantage and postgresql
the initial rabbit hole is figuring out how to get new makes/models into the vision classification page
the whole batch upload process (python_projects\vehicles)
putting that off 
1. there is a very large mismatch between advantage (black book)  and postgres in just make and model names, eg F150 & F-150
2. a lot of makes and models in advantage not in postgres
hoping to "short cut" it like i did in the query synch_with_the_tool to just get a match of the 8 shape/size
	defined catgegories. but this is much more comprehensive: all pickups, all cars, all suvs all vans
think i will start with #2
  and the first step in #2 will be to resolve the differences in classifications between adv and pg
*/

select * 
from ads.ext_make_model_classifications a
full outer join veh.shape_size_classifications b on a.make = b.make and a.model = b.model;

select a.make, a.model, a.vehiclesegment, a.vehicletype, b.make, b.model, b.shape, b.size 
from ads.ext_make_model_classifications a
full outer join veh.shape_size_classifications b on a.make = b.make and a.model = b.model



-- 244 vehicle items not classified
select a.make, a.model
from ads.ext_vehicle_items a
left join ads.ext_make_model_classifications b on a.make = b.make and a.model = b.model
where b.make is null

-- only 1 in current inventory  done
select a.make, a.model, c.stocknumber
from ads.ext_vehicle_items a
left join ads.ext_make_model_classifications b on a.make = b.make and a.model = b.model
left join ads.ext_vehicle_inventory_items c on a.vehicleitemid = c.vehicleitemid
  and c.thruts::date > current_date
where b.make is null 
  and c.stocknumber is not null 

-- this is informative but, holy shit how to make it all happen
select *, (aa.shape = bb.shape and aa.size = bb.size) 
from (                             
	select b.make, b.model, split_part(b.vehicletype,'_',2) as shape, 
	  case
	    when split_part(b.vehiclesegment,'_',2) = 'Extra' then 'Extra Large'
	    else split_part(b.vehiclesegment,'_',2)
	  end as size, count(*) as the_count
	from ads.ext_vehicle_items a
	join ads.ext_make_model_classifications b on a.make = b.make and a.model = b.model
-- 	where a.make = 'ford'
	group by b.make, b.model, split_part(b.vehicletype,'_',2), split_part(b.vehiclesegment,'_',2)) aa
full outer join (
  select *
  from veh.shape_size_classifications) bb on aa.make = bb.make and aa.model = bb.model
--   where make = 'ford') bb on aa.make = bb.make and aa.model = bb.model
order by the_count desc nulls last


-- ok, this is a worthwhile, manageable easy to handle update
select * 
from (                             
	select b.make, b.model, split_part(b.vehicletype,'_',2) as shape, split_part(b.vehiclesegment,'_',2), count(*) as the_count
	from ads.ext_vehicle_items a
	join ads.ext_make_model_classifications b on a.make = b.make and a.model = b.model
	where b.vehicletype = 'Vehicletype_Crossover'
	group by b.make, b.model, split_part(b.vehicletype,'_',2), split_part(b.vehiclesegment,'_',2)) aa
left join (
  select *
  from veh.shape_size_classifications) bb on aa.make = bb.make and aa.model = bb.model
order by coalesce(aa.model, bb.model)

that worked ok for ford
took all matching models and updated sizes to match pg

-- generate teh sql rather than writing 147 queries
-- took awhile,  but it looks like this works
select ' UPDATE MakeModelClassifications SET vehiclesegment = '||'''VehicleSegment_'|| bb.size || '''' || ' where make = ' || '''' ||aa.make || '''' || ' and model = '|| '''' || aa.model || '''' || ';'
-- select * 
from (                             
	select b.make, b.model, split_part(b.vehicletype,'_',2) as shape, 
	  case
	    when split_part(b.vehiclesegment,'_',2) = 'Extra' then 'Extra Large'
	    else split_part(b.vehiclesegment,'_',2)
	  end as size, count(*) as the_count
	from ads.ext_vehicle_items a
	join ads.ext_make_model_classifications b on a.make = b.make and a.model = b.model
	group by b.make, b.model, split_part(b.vehicletype,'_',2), split_part(b.vehiclesegment,'_',2)) aa
join (
  select *
  from veh.shape_size_classifications) bb on aa.make = bb.make and aa.model = bb.model
    and bb.size <> aa.size
limit 50    

-- and a few for conflicting shape
select ' UPDATE MakeModelClassifications SET vehicletype = '||'''VehicleType_'|| bb.shape || '''' || ' where make = ' || '''' ||aa.make || '''' || ' and model = '|| '''' || aa.model || '''' || ';'
-- select * 
from (                             
	select b.make, b.model, split_part(b.vehicletype,'_',2) as shape, 
	  case
	    when split_part(b.vehiclesegment,'_',2) = 'Extra' then 'Extra Large'
	    else split_part(b.vehiclesegment,'_',2)
	  end as size, count(*) as the_count
	from ads.ext_vehicle_items a
	join ads.ext_make_model_classifications b on a.make = b.make and a.model = b.model
	group by b.make, b.model, split_part(b.vehicletype,'_',2), split_part(b.vehiclesegment,'_',2)) aa
join (
  select *
  from veh.shape_size_classifications) bb on aa.make = bb.make and aa.model = bb.model
    and bb.shape <> aa.shape

update veh.shape_size_classifications
set shape = 'SUV'
-- select * from veh.shape_size_classifications
where make = 'cadillac' and model = 'srx'

--ok, after doing all that, still have a lot of work to do
-- vehicleitems with no match in veh.shape_size_classifications
select aa.*
from (                             
	select b.make, b.model, split_part(b.vehicletype,'_',2) as shape, 
	  case
	    when split_part(b.vehiclesegment,'_',2) = 'Extra' then 'Extra Large'
	    else split_part(b.vehiclesegment,'_',2)
	  end as size, count(*) as the_count
	from ads.ext_vehicle_items a
	join ads.ext_make_model_classifications b on a.make = b.make and a.model = b.model
-- 	where a.make = 'ford'
	group by b.make, b.model, split_part(b.vehicletype,'_',2), split_part(b.vehiclesegment,'_',2)) aa
full outer join (
  select *
  from veh.shape_size_classifications) bb on aa.make = bb.make and aa.model = bb.model
--   where make = 'ford') bb on aa.make = bb.make and aa.model = bb.model
where bb.make is null
order by the_count desc nulls last

select * 
from veh.shape_size_classifications
where make = 'ford'
  and shape = 'pickup'
order by model      

select * 
from veh.shape_size_classifications
where make = 'honda'
  and model like '%accord%'
order by model    

select * 
from veh.shape_size_classifications
where make = 'chevrolet'
  and shape = 'pickup'
order by model      


-- for fs_data when looking at sales per month, to get the appropriate price
-- if sold after end of month Least pricingts::Date
 select least('12/10/2022','12/15/2022')
 select greatest('12/10/2022','12/15/2022')