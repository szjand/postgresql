﻿select a.vin, a.model_year, a.make, a.model, r.style ->'attributes'->>'name',
  array_agg(t.tech ->> 'titleId' order by t.tech ->> 'titleId')
-- select *
from nc.vehicles a
join chr.describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'style') as r(style) on true
join jsonb_array_elements(b.response->'technicalSpecification') as t(tech) on true
where cab <> 'n/a'
  and make = 'chevrolet'
  and model_year = 2020
  and model like '%1500%'
group by a.vin, a.model_year, a.make, a.model, r.style ->'attributes'->>'name'
limit 10  



