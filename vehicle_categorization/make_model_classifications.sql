﻿select *
-- select count(*)
from ads.ext_make_model_classifications 
limit 10

update tem.make_model_classifications
set shape = 'Car'
-- select * from tem.make_model_classifications 
where shape = 'small';

drop table if exists tem.make_model_classifications cascade;
create table tem.make_model_classifications as 
select make, model, split_part(vehicletype, '_', 2)::citext as shape,
  split_part(vehiclesegment, '_', 2)::citext as size, 
  luxury, sport, commercial, null::integer as seq
from ads.ext_make_model_classifications a;

update tem.make_model_classifications
set shape = 'Car'
-- select * from tem.make_model_classifications 
where shape = 'small';

update tem.make_model_classifications
set size = 'Extra_Large'
where size = 'Extra';

update tem.make_model_classifications
set seq = 
  case 
    when size = 'compact' then 1
    when size = 'small' then 2
    when size = 'midsize' then 3
    when size = 'large' then 4
    when size = 'extra_large' then 5
  end;

select shape, size, the_count -- 19
from (
  select shape, size, seq, count(*) as the_count
  from tem.make_model_classifications a
  group by shape, size, seq
  order by shape, seq) b;

select shape, size, luxury, sport, commercial, the_count -- 30
from (
  select shape, size, luxury, sport, commercial, seq, count(*) as the_count-- 39
  from tem.make_model_classifications a
  group by shape, size, luxury, commercial, sport, seq
  order by shape, size, seq) b;

select make, shape, size, luxury, sport, commercial, the_count 
from (
  select make, shape, size, luxury, sport, commercial, seq, count(*) as the_count -- 338
  from tem.make_model_classifications a
  group by make, shape, size, luxury, sport, commercial, seq
  order by shape, seq, make) b;

select make, model, shape, size, luxury, sport, commercial -- 831
from tem.make_model_classifications a
order by shape, seq, make, model;


