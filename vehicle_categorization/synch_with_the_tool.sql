﻿

select * 
from ads.ext_make_model_classifications limit 10

select * -- VehicleSegment_Extra_Large
from ads.ext_make_model_classifications 
where vehiclesegment like 'VehicleSegment_Extra%'


select *
from veh.shapes a
join veh.sizes b on true
order by shape, size

select * 
from veh.shape_size_classifications
limit 10

select distinct

-- GM Large Pickup
select *
from (
	select make, model, shape, size 
	from veh.shape_size_classifications
	where make in ('chevrolet', 'buick', 'cadillac', 'gmc')
		and shape = 'pickup'
		and size = 'large'
		order by make, model) a
full outer join (  
	select make, model, vehicletype, vehiclesegment 
	from ads.ext_make_model_classifications
	where make in ('chevrolet', 'buick', 'cadillac', 'gmc')
		and vehicletype = 'VehicleType_Pickup'
		and vehiclesegment = 'VehicleSegment_Large'
		order by make, model) b on a.make  = b.make
    and a.model = b.model

-- GM Small SUV
select *
from (
	select make, model, shape, size 
	from veh.shape_size_classifications
	where make in ('chevrolet', 'buick', 'cadillac', 'gmc')
		and shape = 'suv'
		and size = 'small'
		order by make, model) a
full outer join (  
	select make, model, vehicletype, vehiclesegment 
	from ads.ext_make_model_classifications
	where make in ('chevrolet', 'buick', 'cadillac', 'gmc')
		and vehicletype = 'VehicleType_SUV'
		and vehiclesegment = 'VehicleSegment_Small'
		order by make, model) b on a.make  = b.make
    and a.model = b.model    



-- IMPORT Small SUVS
select *
from (
	select make, model, shape, size 
	from veh.shape_size_classifications
	where make in ('honda', 'nissan', 'toyota')
		and shape = 'suv'
		and size = 'small'
		order by make, model) a
full outer join (  
	select make, model, vehicletype, vehiclesegment 
	from ads.ext_make_model_classifications
	where make in ('honda', 'nissan', 'toyota')
		and vehicletype = 'VehicleType_SUV'
		and vehiclesegment = 'VehicleSegment_Small'
		order by make, model) b on a.make  = b.make
    and a.model = b.model    

select * from veh.additional_classification_data where chrome_model in ('rogue','rogue sport')     
-- extra large pickup
SELECT * 
FROM MakeModelClassifications 
WHERE model LIKE '%2500%'
  AND vehicletype = 'VehicleType_Pickup'
	
UPDATE MakeModelClassifications
SET vehiclesegment = 'VehicleSegment_Extra_Large' 
WHERE model LIKE '%2500%'
  AND vehicletype = 'VehicleType_Pickup';
	

SELECT * 
FROM MakeModelClassifications 
WHERE model LIKE '%3500%'
  AND vehicletype = 'VehicleType_Pickup'
	
UPDATE MakeModelClassifications
SET vehiclesegment = 'VehicleSegment_Extra_Large' 
WHERE model LIKE '%3500%'
  AND vehicletype = 'VehicleType_Pickup';	
	
UPDATE MakeModelClassifications
SET vehiclesegment = 'VehicleSegment_Extra_Large' 
WHERE model LIKE '%4500%'
  AND vehicletype = 'VehicleType_Pickup';		
	
-- gm small suv	

SELECT * 
FROM MakeModelClassifications 
WHERE vehiclesegment = 'VehicleSegment_Small'
  AND vehicletype = 'VehicleType_SUV'
	AND make IN ('chevrolet','gmc','buick','cadillac')
	
UPDATE MakeModelClassifications
SET vehiclesegment = 'VehicleSegment_Compact' 
-- SELECT * FROM MakeModelClassifications 
WHERE model = 'encore';
		
UPDATE MakeModelClassifications
SET vehiclesegment = 'VehicleSegment_Small' 
-- SELECT * FROM MakeModelClassifications 
WHERE model = 'equinox';	

UPDATE MakeModelClassifications
SET vehiclesegment = 'VehicleSegment_Compact' 
-- SELECT * FROM MakeModelClassifications 
WHERE model in ('tracker','trax');	

UPDATE MakeModelClassifications
SET vehiclesegment = 'VehicleSegment_Small' 
-- SELECT * FROM MakeModelClassifications 
WHERE model in ('jimmy','terrain');	


-- gm small suv	

SELECT * 
FROM MakeModelClassifications 
WHERE vehiclesegment = 'VehicleSegment_Small'
  AND vehicletype = 'VehicleType_SUV'
	AND make IN ('honda', 'nissan', 'toyota')
	
-- first those that are small suv AND should NOT be
UPDATE MakeModelClassifications
SET vehiclesegment = 'VehicleSegment_Compact' 
-- SELECT * FROM MakeModelClassifications 
WHERE model in ('fit', 'cube', 'juke', 'hr-v');	

-- THEN those that aren't but should be
UPDATE MakeModelClassifications
SET vehiclesegment = 'VehicleSegment_Small',
    vehicletype = 'VehicleType_SUV'
-- SELECT * FROM MakeModelClassifications 
WHERE model in ('rogue', 'rogue select', 'rav4', 'venza');	

SELECT * FROM MakeModelClassifications 
WHERE model LIKE 'rav4%';	