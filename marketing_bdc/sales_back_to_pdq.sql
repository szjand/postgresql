﻿select year_month, count(*)
from sls.deals_by_month
where year_month between 201804 and 201806
  and sale_type <> 'Wholesale'
  and unit_count = 1
  and store_code = 'RY1'
group by year_month  


select *
from sls.deals_by_month a
where year_month between 201804 and 201806
  and a.sale_type <> 'Wholesale'
  and a.unit_count = 1
  and a.store_code = 'RY1'

select *
from sls.deals
limit 10

select *
from sls.deals_by_month a
left join sls.deals b on a.stock_number = b.stock_number
where a.year_month between 201804 and 201806
  and a.sale_type <> 'Wholesale'
  and a.unit_count = 1
  and a.store_code = 'RY1'

select * from sls.deals_by_month where stock_number = '30144'

select a.year_month, a.stock_number, a.vin, b.*
-- select *
from sls.deals a
-- left join ads.ext_dim_vehicle b on a.vin = b.vin
where a.year_month between 201804 and 201806
  and a.deal_status_code = 'U'
  and a.sale_type_code <> 'W'
  and a.store_code = 'RY1'
-- group by year_month  


drop table if exists sales;
create temp table sales as
select a.year_month, a.delivery_date, a.vin
from sls.deals a
where a.year_month between 201804 and 201806
  and a.deal_status_code = 'U'
  and a.sale_type_code <> 'W'
  and a.store_code = 'RY1';

drop table if exists ros;
create temp table ros as
select a.ro, b.the_date as open_date, c.vin
from ads.ext_fact_repair_order a  
join dds.dim_date b on a.opendatekey = b.date_key
join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey
join sales d on c.vin = d.vin
where ro like '19%'
group by a.ro, b.the_date, c.vin;

select * 
from sales a
left join ros b on a.vin = b.vin
  and b.open_date > a.delivery_Date


select year_month as month, count(*) as sales, sum(pdq) as pdq
from (
  select a.*,
    case
      when exists (
        select 1
        from ros 
        where vin = a.vin
          and open_date > a.delivery_date) then 1
      else 0
    end as pdq
  from sales a) b
group by year_month  


select sale_type_code, count(*), min(stock_number), max(stock_number)
from sls.deals a
where a.year_month = 201804
  and a.deal_status_code = 'U'
  and a.sale_type_code <> 'W'
  and a.store_code = 'RY1'
group by sale_type_code  


-- exclude fleet
drop table if exists sales;
create temp table sales as
select a.year_month, a.delivery_date, a.vin
from sls.deals a
where a.year_month between 201804 and 201806
  and a.deal_status_code = 'U'
  and a.sale_type_code not in ( 'W','F')
  and a.store_code = 'RY1';

drop table if exists ros;
create temp table ros as
select a.ro, b.the_date as open_date, c.vin
from ads.ext_fact_repair_order a  
join dds.dim_date b on a.opendatekey = b.date_key
join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey
join sales d on c.vin = d.vin
where ro like '19%'
group by a.ro, b.the_date, c.vin;


select year_month as month, count(*) as sales, sum(pdq) as pdq
from (
  select a.*,
    case
      when exists (
        select 1
        from ros 
        where vin = a.vin
          and open_date > a.delivery_date) then 1
      else 0
    end as pdq
  from sales a) b
group by year_month  