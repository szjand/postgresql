﻿each month, just repopulate the table jon.morgan_deal_gross, then get the gross in 10/12/2020

----------------------------------------------------------------------
--< 03/03/21
----------------------------------------------------------------------
february 21

drop table if exists jon.morgan_deal_gross;
create table jon.morgan_deal_gross (
  stock_number citext,
  vin citext);



select * from jon.morgan_deal_gross order by stock_number
select count(distinct stock_number) from jon.morgan_deal_Gross
select stock_number from jon.morgan_deal_gross group by stock_number having count(*) > 1
select vin from jon.morgan_deal_gross group by vin having count(*) > 1  


delete from jon.morgan_deal_gross where stock_number = ''
----------------------------------------------------------------------
--< 02/01/21
----------------------------------------------------------------------
6 spreadsheets: 
  january Sold
  December Sold
  November Sale
  October Sale
  sold2
  September Sales



----------------------------------------------------------------------
--< 10/12/2020
----------------------------------------------------------------------
3 spreadsheets:
  Sold.csv
  April - August Drive Gross Data.csv
  April - August AM Gross Data.xlsx
-- populate with formula on spreadsheet
drop table if exists jon.morgan_deal_gross;
create table jon.morgan_deal_gross (
  stock_number citext,
  vin citext);


select * from jon.morgan_deal_gross order by stock_number
select stock_number, count(*) from jon.morgan_deal_gross group by stock_number having count(*) > 1 order by count(*) desc
select vin from jon.morgan_deal_gross group by vin having count(*) > 1  

-- -- fuck, AM Gross Data has no stocknumber and 5 dup vins, manually deleted the dups from the spreadsheet
-- 
-- drop table if exists jon.morgan_deal_gross_tmp;
-- create table jon.morgan_deal_gross_tmp (
--   stock_number citext,
--   vin citext);
--   
-- delete from jon.morgan_deal_gross;
-- insert into jon.morgan_deal_gross
-- select distinct b.stock_number, a.vin
-- from jon.morgan_deal_gross_tmp a
-- join sls.deals b on a.vin = b.vin
--   and b.deal_status = 'capped'
--   and b.run_date > '04/01/2020'
--   and b.sale_type_Code <> 'W'
--   and b.bopmast_id not in (60800,61748,50060,61184,61780,60984)

  

-- -- and this is the total gross
-- 202109 & 202110 no vin, just stock_number
-- select a.*, sum(-b.amount) as gross
select a.stock_number, sum(-b.amount) as gross
from jon.morgan_deal_gross a
join fin.fact_gl b on a.stock_number = b.control
  and b.post_status = 'Y'
join fin.dim_account c on b.account_key = c.account_key
  and c.account in (
    select b.g_l_acct_number::citext as account
    from arkona.ext_eisglobal_sypffxmst a
    inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
      and b.factory_financial_year  = (select max(factory_financial_year) from arkona.ext_ffpxrefdta)
    where a.fxmcyy = (select max(fxmcyy) from arkona.ext_eisglobal_sypffxmst)
      and coalesce(b. consolidation_grp, '1') <> '3'
      and b.g_l_acct_number <> ''
      and (
        (a.fxmpge between 5 and 15) or
        (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
        (a.fxmpge = 17 and a.fxmlne between 1 and 19))
      and b.g_l_acct_number not in ('144502','164502')) 
group by a.vin, a.stock_number      
-- order by a.vin      
-- group by a.stock_number      
order by a.stock_number


----------------------------------------------------------------------
--/> 10/12/2020
----------------------------------------------------------------------

----------------------------------------------------------------------
--< 09/02/2020
----------------------------------------------------------------------
-- populate with formula on spreadsheet
drop table if exists jon.morgan_deal_gross;
create table jon.morgan_deal_gross (
  stock_number citext,
  vin citext);

-- 287 rows
select * from jon.morgan_deal_gross  
select stock_number from jon.morgan_deal_gross group by stock_number having count(*) > 1
select vin from jon.morgan_deal_gross group by vin having count(*) > 1  

-- and this is the total gross
select a.*, sum(-b.amount) as gross
from jon.morgan_deal_gross a
join fin.fact_gl b on a.stock_number = b.control
  and b.post_status = 'Y'
join fin.dim_account c on b.account_key = c.account_key
  and c.account in (
    select b.g_l_acct_number::citext as account
    from arkona.ext_eisglobal_sypffxmst a
    inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
      and b.factory_financial_year  = (select max(factory_financial_year) from arkona.ext_ffpxrefdta)
    where a.fxmcyy = (select max(fxmcyy) from arkona.ext_eisglobal_sypffxmst)
      and coalesce(b. consolidation_grp, '1') <> '3'
      and b.g_l_acct_number <> ''
      and (
        (a.fxmpge between 5 and 15) or
        (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
        (a.fxmpge = 17 and a.fxmlne between 1 and 19))
      and b.g_l_acct_number not in ('144502','164502')) 
group by a.vin, a.stock_number      
order by a.vin
----------------------------------------------------------------------
--/> 09/02/2020
----------------------------------------------------------------------
/*
Hi Jon,
 
Would you be able to provide the gross on each of vins in the attached document?
 
Thank you,
Morgan 
08/09/20
*/
-- populate with formula on spreadsheet
drop table if exists jon.morgan_deal_gross;
create table jon.morgan_deal_gross (
  stock_number citext,
  vin citext);

--327 rows
select * from jon.morgan_deal_gross  
select stock_number from jon.morgan_deal_gross group by stock_number having count(*) > 1
select vin from jon.morgan_deal_gross group by vin having count(*) > 1


-- and this is the total gross
select a.*, sum(-b.amount) as gross
from jon.morgan_deal_gross a
join fin.fact_gl b on a.stock_number = b.control
  and b.post_status = 'Y'
join fin.dim_account c on b.account_key = c.account_key
  and c.account in (
    select b.g_l_acct_number::citext as account
    from arkona.ext_eisglobal_sypffxmst a
    inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
      and b.factory_financial_year  = (select max(factory_financial_year) from arkona.ext_ffpxrefdta)
    where a.fxmcyy = (select max(fxmcyy) from arkona.ext_eisglobal_sypffxmst)
      and coalesce(b. consolidation_grp, '1') <> '3'
      and b.g_l_acct_number <> ''
      and (
        (a.fxmpge between 5 and 15) or
        (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
        (a.fxmpge = 17 and a.fxmlne between 1 and 19))
      and b.g_l_acct_number not in ('144502','164502')) 
group by a.vin, a.stock_number      
order by a.vin
/*

Hi Jon,

Would you be able to pull the gross for the stock numbers attached?

Thank you for your help!

Morgan 
07/07/20
*/

-- populate with formula on spreadsheet
drop table if exists jon.morgan_deal_gross;
create table jon.morgan_deal_gross (
  stock_number citext,
  vin citext);
-- select * from jon.morgan_deal_gross
select stock_number from jon.morgan_deal_gross group by stock_number having count(*) > 1

-- and this is the total gross
select a.*, sum(-b.amount) as gross
from jon.morgan_deal_gross a
join fin.fact_gl b on a.stock_number = b.control
  and b.post_status = 'Y'
join fin.dim_account c on b.account_key = c.account_key
  and c.account in (
    select b.g_l_acct_number::citext as account
    from arkona.ext_eisglobal_sypffxmst a
    inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
      and b.factory_financial_year  = (select max(factory_financial_year) from arkona.ext_ffpxrefdta)
    where a.fxmcyy = (select max(fxmcyy) from arkona.ext_eisglobal_sypffxmst)
      and coalesce(b. consolidation_grp, '1') <> '3'
      and b.g_l_acct_number <> ''
      and (
        (a.fxmpge between 5 and 15) or
        (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
        (a.fxmpge = 17 and a.fxmlne between 1 and 19))
      and b.g_l_acct_number not in ('144502','164502')) 
group by a.vin, a.stock_number      
order by a.vin

/*
Hi Jon,

Would you be able to pull the total gross profit for the vehicles on the attached spreadsheet?  Both front and back end gross profit.

Thank you,
Morgan
6/17
*/
-- populate with formula on spreadsheet
drop table if exists jon.morgan_deal_gross;
create table jon.morgan_deal_gross (
  stock_number citext,
  vin citext);
-- select * from jon.morgan_deal_gross
select stock_number from jon.morgan_deal_gross group by vin having count(*) > 1

select a.*, sum(-b.amount) as gross
from jon.morgan_deal_gross a
join fin.fact_gl b on a.stock_number = b.control
  and b.post_status = 'Y'
join fin.dim_account c on b.account_key = c.account_key
  and c.account in (
    select b.g_l_acct_number::citext as account
    from arkona.ext_eisglobal_sypffxmst a
    inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
      and b.factory_financial_year  = (select max(factory_financial_year) from arkona.ext_ffpxrefdta)
    where a.fxmcyy = (select max(fxmcyy) from arkona.ext_eisglobal_sypffxmst)
      and coalesce(b. consolidation_grp, '1') <> '3'
      and b.g_l_acct_number <> ''
      and (
        (a.fxmpge between 5 and 15) or
        (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
        (a.fxmpge = 17 and a.fxmlne between 1 and 19))
      and b.g_l_acct_number not in ('144502','164502')) 
group by a.vin, a.stock_number      
order by a.vin



inner join on the accounting excludes this: G38904;1GCGTCEN6L1179431;849.85;;;
there was a deal never finished , not sold in may
select * from arkona.xfm_bopmast where bopmast_stock_number = 'g38904'


-- why the fuck does this give me 2 different numbers ????
-- left join vs inner join on fin.dim_account
select * 
from (
  select a.*, sum(-b.amount) as gross
  from jon.morgan_deal_gross a
  left join fin.fact_gl b on a.stock_number = b.control
    and b.post_status = 'Y'
  left join fin.dim_account c on b.account_key = c.account_key
    and c.account in (
      select b.g_l_acct_number::citext as account
      from arkona.ext_eisglobal_sypffxmst a
      inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
        and b.factory_financial_year  = (select max(factory_financial_year) from arkona.ext_ffpxrefdta)
      where a.fxmcyy = (select max(fxmcyy) from arkona.ext_eisglobal_sypffxmst)
        and coalesce(b. consolidation_grp, '1') <> '3'
        and b.g_l_acct_number <> ''
        and (
          (a.fxmpge between 5 and 15) or
          (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
          (a.fxmpge = 17 and a.fxmlne between 1 and 19))
        and b.g_l_acct_number not in ('144502','164502')) 
  group by a.vin, a.stock_number) aa     
left join (
  select a.*, sum(-b.amount) as gross
  from jon.morgan_deal_gross a
  left join fin.fact_gl b on a.stock_number = b.control
    and b.post_status = 'Y'
  join fin.dim_account c on b.account_key = c.account_key
    and c.account in (
      select b.g_l_acct_number::citext as account
      from arkona.ext_eisglobal_sypffxmst a
      inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
        and b.factory_financial_year  = (select max(factory_financial_year) from arkona.ext_ffpxrefdta)
      where a.fxmcyy = (select max(fxmcyy) from arkona.ext_eisglobal_sypffxmst)
        and coalesce(b. consolidation_grp, '1') <> '3'
        and b.g_l_acct_number <> ''
        and (
          (a.fxmpge between 5 and 15) or
          (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
          (a.fxmpge = 17 and a.fxmlne between 1 and 19))
        and b.g_l_acct_number not in ('144502','164502')) 
  group by a.vin, a.stock_number) bb on aa.vin = bb.vin
order by aa.vin


/*
Hi Jon,

Would you be able to send me the total gross for the stock numbers attached?

Thank you,
Morgan 
05/01/20
*/
-- populate with formula on spreadsheet
drop table if exists jon.morgan_deal_gross;
create table jon.morgan_deal_gross (
  stock_number citext,
  vin citext);

select a.*, sum(-b.amount) as gross
from jon.morgan_deal_gross a
left join fin.fact_gl b on a.stock_number = b.control
  and b.post_status = 'Y'
join fin.dim_account c on b.account_key = c.account_key
  and c.account in (
    select b.g_l_acct_number::citext as account
    from arkona.ext_eisglobal_sypffxmst a
    inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
      and b.factory_financial_year  = (select max(factory_financial_year) from arkona.ext_ffpxrefdta)
    where a.fxmcyy = (select max(fxmcyy) from arkona.ext_eisglobal_sypffxmst)
      and coalesce(b. consolidation_grp, '1') <> '3'
      and b.g_l_acct_number <> ''
      and (
        (a.fxmpge between 5 and 15) or
        (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
        (a.fxmpge = 17 and a.fxmlne between 1 and 19))
      and b.g_l_acct_number not in ('144502','164502')) 
group by a.vin, a.stock_number      
order by a.vin


/*
Hi Jon and Afton,

Would you be able to help me pull what our total gross for each of the vehicles listed on the two attached spreadsheets are.  

Thank you!!!
Morgan
4/15/20
*/
this time her spreadsheet includes the stocknumber
-- populate with formula on spreadsheet
create table jon.morgan_deal_gross (
  year_month integer,
  stock_number citext,
  vin citext);
  
select a.*, sum(-b.amount) as gross
from jon.morgan_deal_gross a
left join fin.fact_gl b on a.stock_number = b.control
  and b.post_status = 'Y'
join fin.dim_account c on b.account_key = c.account_key
  and c.account in (
    select b.g_l_acct_number::citext as account
    from arkona.ext_eisglobal_sypffxmst a
    inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
      and b.factory_financial_year  = (select max(factory_financial_year) from arkona.ext_ffpxrefdta)
    where a.fxmcyy = (select max(fxmcyy) from arkona.ext_eisglobal_sypffxmst)
      and coalesce(b. consolidation_grp, '1') <> '3'
      and b.g_l_acct_number <> ''
      and (
        (a.fxmpge between 5 and 15) or
        (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
        (a.fxmpge = 17 and a.fxmlne between 1 and 19))
      and b.g_l_acct_number not in ('144502','164502'))
where a.year_month = 202003      
group by a.year_month, a.vin, a.stock_number      
order by a.vin




--------------------------------------------------------------------------------
drop table if exists jon.morgan_vins;
create table jon.morgan_vins (vin citext primary key, seq serial);

drop table if exists step_1;
create temp table step_1 as
select a.vin, a.seq, b.stock_number
from jon.morgan_vins a
left join sls.deals b on a.vin = b.vin
  and b.run_date > '11/30/2019'
  and b.run_date = (
    select max(run_date)
    from sls.deals
    where vin = b.vin);


select a.vin, a.seq, a.stock_number, sum(-b.amount) as gross
from step_1 a
join fin.fact_gl b on a.stock_number = b.control
  and b.post_status = 'Y'
join fin.dim_account c on b.account_key = c.account_key
  and c.account in (
    select b.g_l_acct_number::citext as account
    from arkona.ext_eisglobal_sypffxmst a
    inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
      and b.factory_financial_year  = (select max(factory_financial_year) from arkona.ext_ffpxrefdta)
    where a.fxmcyy = (select max(fxmcyy) from arkona.ext_eisglobal_sypffxmst)
      and coalesce(b. consolidation_grp, '1') <> '3'
      and b.g_l_acct_number <> ''
      and (
        (a.fxmpge between 5 and 15) or
        (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
        (a.fxmpge = 17 and a.fxmlne between 1 and 19))
      and b.g_l_acct_number not in ('144502','164502'))
group by a.vin, a.seq, a.stock_number      
order by seq