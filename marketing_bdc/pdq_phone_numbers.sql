﻿insert into jon.phone values ('2187797191');


select bopname_record_key, bopname_search_name, phone_number
from arkona.xfm_bopname a
join jon.phone b on a.phone_number::citext = b.phone
where a.current_row
order by bopname_search_name


select phone
from jon.phone
group by phone

drop table if exists phones;
create temp table phones as
select a.phone, b.bopname_record_key, b.bopname_search_name
from jon.phone a
left join arkona.xfm_bopname b on a.phone = b.phone_number::citext
group by a.phone, b.bopname_record_key, b.bopname_search_name

drop table if exists ros;
create temp table ros as
select b.the_date, a.ro, a.customerkey
from ads.ext_fact_repair_order a
join dds.dim_date b on a.opendatekey = b.date_key
  and b.the_date > '01/31/2019'
group by b.the_date, a.ro, a.customerkey;

-- do it with sdprhdr
drop table if exists ros;
create temp table ros as
select open_date, ro_number, customer_key, cust_name
from arkona.ext_sdprhdr
where open_date > 20190000

select * from ros

-- oil change ros
drop table if exists oc_ros;
create temp table oc_ros as
select ro_number -- pdq ros: 19xxxxxx
from phones a
join ros b on bopname_record_key = b.customer_key
--   and open_Date > 20190200
  and open_Date > 20190000
where left(b.ro_number, 2) = '19'  
union
select a.ro -- non pdq ros
from ads.ext_fact_repair_order a
join ads.ext_dim_opcode b on a.opcodekey = b.opcodekey
  and b.description like '%oil%'
where a.ro in (
  select ro_number 
  from phones a
  join ros b on bopname_record_key = b.customer_key
    and open_Date > 20190200)
and left(a.ro, 2) <> '19';


select a.ro_number, customer_key, cust_name, (select dds.db2_integer_to_date(open_date))
from ros a
join oc_ros b on a.ro_number = b.ro_number
order by cust_name

-- here is the spreadsheet for morgan, in the original order of her spreadsheet
select aa.phone as "Phone Number", 
  case
    when bb.phone is null then 'no'
    else 'yes'
  end as "Quick Lane",
  ro_date as "Date"
from (
  select a.*, row_number() over ()
  from jon.phone a) aa
left join (
  select a.phone, max(ro_date) as ro_date
  from phones a
  join (
    select a.ro_number, customer_key, cust_name, (select dds.db2_integer_to_date(open_date)) as ro_date
    from ros a
    join oc_ros b on a.ro_number = b.ro_number) b on a.bopname_record_key = b.customer_key 
  group by a.phone) bb on aa.phone = bb.phone
order by aa.row_number  



