﻿/*
Hey Jon,

Can you get a list of all the Toyota vehicles in our DMS for me?  Let me know if you have any questions.

Name
City
Phone Number 1
Phone Number 2
Email
Year
Make
Model
VIN

Justin Kilmer
*/

select *
from arkona.ext_inpmast
where make = 'toyota'
  and status = 'i'
 limit 10 

-- use max from date
-- before grouping 63946, after 53012, 46758 (company_indivi), 39663 (null city), final: 37948
drop table if exists toyotas cascade;
create temp table toyotas as
select c.bopname_search_name as name, c.city, phone_number, cell_phone, 
  c.email_address as email, b.year, b.make, b.model, b.inpmast_vin as vin, row_number() over()
from arkona.ext_bopvref a
join arkona.ext_inpmast b on a.vin = b.inpmast_vin
  and b.make = 'toyota'
join arkona.ext_bopname c on a.customer_key = c.bopname_Record_key  
where a.end_date = 0
  and length(vin) = 17
  and company_individ is not null
  and city is not null
group by c.bopname_search_name, c.city, phone_number, cell_phone, 
  c.email_address, b.year, b.make, b.model, b.inpmast_vin  
order by name;

select count(*) from toyotas

select * from toyotas order by name

select company_individ, count(*) from arkona.ext_bopname group by company_individ

select * from toyotas where name like '%toyota%'

delete 
from toyotas 
where row_number between 9 and 18
  or name like 'adesa%'
  or name like 'ally%'
  or name like 'rydell%'
  or name like '%toyota%'
  or name like '%enterprise%'
  or name like 'flagstaff%'
  or name = 'HARBOR CHEV'
  or name like '%hertz%'
  or name like '%honda%'
  or name like '%lithia%'
  or name like '%manheim%'
  or name like '%auction%'
  or name like 'minot%'
  or name like 'modern%'
