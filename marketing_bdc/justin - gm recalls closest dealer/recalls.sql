﻿9/22/23
spreadsheet from justin with list of customer/vehicles:

-- From the list provided is there any way to easily cross reference the VIN number of 
-- each vehicle and see if we have there phone number and email address within our systems?
-- 
-- Thanks!

Justin Kilmer
create schema mkt;
comment on schema mkt is 'tables for marketing reports';

drop table if exists mkt.recall_vins;
create table mkt.recall_vins (
  vin citext,
  full_name citext,
  address citext,
  city citext);

update mkt.recall_vins
set full_name = trim(full_name)


 inserted 2352 rows; 

select a.*, b.*, c.bopname_search_name, similarity(a.full_name, c.bopname_search_name)
from (
  select distinct vin, full_name
  from mkt.recall_vins) a
join arkona.ext_bopvref b on a.vin = b.vin
  and b.end_Date = 0
  and b.company_number = 'RY1'
join arkona.ext_inpmast d on a.vin = d.inpmast_vin  
left join arkona.ext_bopname c on b.customer_key = c.bopname_record_key
  and b.company_number = c.bopname_company_number
  and company_individ is not null
order by similarity(a.full_name, c.bopname_search_name)


drop table if exists ry1;
create temp table ry1 as
select distinct a.vin -- 855
from mkt.recall_vins a
join arkona.ext_inpmast b on a.vin = b.inpmast_vin
  and b.inpmast_company_number = 'RY1';
union
drop table if exists ry8;
create temp table ry8 as
select distinct a.vin -- 26
from mkt.recall_vins a
join arkona.ext_inpmast b on a.vin = b.inpmast_vin
  and b.inpmast_company_number = 'RY8';

select * from ry1  -- 862
union
select * from ry8  

select a.full_name, a.vin
from mkt.recall_vins a
join (
  select vin
  from mkt.recall_vins
  group by vin
  having count(*) > 1) b on a.vin = b.vin
order by a.vin  

select * -- 19 in both stores
from ry1 a
join ry8 b on a.vin = b.vin

-- there are no vins with multiple owners
select vin  -- 1993
from (
	select full_name, vin  -- 1993
	from mkt.recall_vins
	group by full_name, vin) a
group by vin
having count(*) > 1

select distinct vin from mkt.recall_vins -- 1993

-- of the 1993 distinct vins, only 862 show up in dealertrack
select vin, full_name, address 
from mkt.recall_vins a
where exists (
  select 1
  from arkona.ext_inpmast
  where inpmast_vin = a.vin)
group by vin, full_name, address


-- shit 33 multiple  bopvref records
drop table if exists dups;
create temp table dups as
select vin from (
select aa.*, b.customer_key
from (
	select a.vin, a.full_name, a.address
	from mkt.recall_vins a
	where exists (
		select 1
		from arkona.ext_inpmast
		where inpmast_vin = a.vin)
	group by a.vin, full_name, address) aa
left join arkona.ext_bopvref b on aa.vin = b.vin
  and b.end_date = 0
  and b.company_number = 'RY1'
) c group by vin having count(*) > 1

select a.* 
from arkona.ext_bopvref a
join dups b on a.vin = b.vin
order by a.vin

select * from arkona.ext_bopvref where vin = '1G2MN35B79Y105166'

drop taBLE IF EXISTS maybe_1;
create temp table maybe_1 as
select aa.*, b.customer_key, b.end_date, c.bopname_search_name, c.address_1, c.phone_number, c.cell_phone, c.email_address, c.share_info, similarity(aa.full_name, c.bopname_search_name) as name, similarity(aa.address, c.address_1) as addr
from (
	select vin, full_name, address, city 
	from mkt.recall_vins a
	where exists (
		select 1
		from arkona.ext_inpmast
		where inpmast_vin = a.vin)
	and not exists (
	  select 1
	  from dups
	  where vin = a.vin)		
	group by vin, full_name, address, city) aa
left join arkona.ext_bopvref b on aa.vin = b.vin
  and b.end_date = 0
  and b.company_number = 'ry1'	
left join arkona.ext_bopname c on b.customer_key = c.bopname_record_key  
where similarity(aa.full_name, c.bopname_search_name) > 0.3
order by aa.full_name

select vin
from maybe_1
group by vin 
having count(*) > 1

select vin, full_name, phone_number, cell_phone, email_address 
from maybe_1
where share_info <> 'N'
  and (phone_number <> '0'
		or cell_phone <> '0'
		or email_address is not null
		or email_address not in ('wng','do not contact'))


drop table if exists mkt.sh1;
create unlogged table mkt.sh1 (
  "Registered Owner Name" citext,
  "Address" citext,
  "City" citext,
  "State" citext,
  "Country" citext,
  "Postal Code" citext,
  "Field Action Number" citext,
  "Field Action Description" citext,
  "Vin" citext,
  "Model Year" citext,  
  "Make" citext,
  "Model" citext);

alter table mkt.sh1 
add column "Home Phone" citext,
add column "Cell Phone" citext,
add column "Email"citext;


select * 
from mkt.sh1 a --limit 10
left join (
	select vin, full_name, phone_number, cell_phone, email_address 
	from maybe_1
	where share_info <> 'N'
		and (phone_number <> '0'
			or cell_phone <> '0'
			or email_address is not null
			or email_address not in ('wng','do not contact'))) b on a."Vin" = b.vin
order by a."Vin"			

-- this updates mkt.sh1 with avail phone and email data from dealertrack
update mkt.sh1 x
set "Home Phone" = y.phone_number,
    "Cell Phone" = y.cell_phone,
    "Email" = y.email_address
from (
	select vin, full_name, phone_number, cell_phone, email_address 
	from maybe_1
	where share_info <> 'N'
		and (phone_number <> '0'
			or cell_phone <> '0'
			or email_address is not null
			or email_address not in ('wng','do not contact'))) y
where x."Vin" = y.vin;


select *
from mkt.sh1 
order by "Registered Owner Name"

 