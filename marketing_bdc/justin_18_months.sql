﻿-- first least for each store, any guest in the past 18 months

drop table if exists sales cascade;
create temp table sales as
select distinct c.bopname_search_name as name, c.address_1 as address, c.city as city, c.state_code as state, c.zip_Code as zip,
  c.phone_number as phone, c.email_address as email , 
  d.year as year, d.make, d.model, 
  a.bopmast_vin as vin--, similarity(a.bopmast_search_name, c.bopname_search_name)
from arkona.ext_bopmast a
join arkona.ext_bopvref b on a.bopmast_vin = b.vin
  and b.end_date = 0
  and a.buyer_number = b.customer_key
--   and (a.buyer_number = b.customer_key or a.co_buyer_number = b.customer_key)
  and b.type_code in ('S','C')
join arkona.ext_bopname c on a.buyer_number = c.bopname_record_key
  and c.company_individ is not null
join arkona.ext_inpmast d on a.bopmast_vin = d.inpmast_vin  
where a.record_status = 'U'
  and a.bopmast_company_number = 'RY2'
  and a.sale_type in ('L','R')
  and a.date_capped between current_date - interval '18 months' and current_Date
  and similarity(a.bopmast_search_name, c.bopname_search_name) > .31;
create unique index on sales(vin,name);  


select company_individ, count(*)
from arkona.ext_bopname
where company_individ is null
group by company_individ

select * from dds.dim_vehicle limit 30

select * from dds.fact_repair_order where store_code = 'ry2' limit 30

drop table if exists ros cascade;
create temp table ros as
select e.bopname_search_name as name, e.address_1 as address, e.city, e.state_code as state, e.zip_code as zip,
  e.phone_number as phone, e.email_address as email,
  b.model_year::integer as year, b.make, b.model, b.vin
from dds.fact_repair_order a
join dds.dim_vehicle b on a.vehicle_key = b.vehicle_key
join dds.dim_customer c on a.customer_key = c.customer_key
join arkona.ext_bopvref d on b.vin = d.vin
  and d.end_date = 0
  and d.type_code = 'C'
  and c.bnkey = d.customer_key
join arkona.ext_bopname e on d.customer_key = e.bopname_record_key
  and e.company_individ is not null  
where open_date between current_date - interval '18 months' and current_Date
  and a.store_code = 'RY2'
group by e.bopname_search_name, e.address_1, e.city, e.state_code, e.zip_code,
  e.phone_number, e.email_address,
  b.model_year, b.make, b.model, b.vin;


select vin
from (
select *
from sales 
union
select * 
from ros) a
group by vin
having count(*) > 1

-- second list, any guest from 18 - 24 months ago that have not been back to the store in the past 18 months
-- the implication is not back for service

drop table if exists sales_2_ry2 cascade;
create temp table sales_2_ry2 as
select distinct c.bopname_search_name as name, c.address_1 as address, c.city as city, c.state_code as state, c.zip_Code as zip,
  c.phone_number as phone, c.email_address as email , 
  d.year as year, d.make, d.model, 
  a.bopmast_vin as vin--, similarity(a.bopmast_search_name, c.bopname_search_name)
from arkona.ext_bopmast a
join arkona.ext_bopvref b on a.bopmast_vin = b.vin
  and b.end_date = 0
  and a.buyer_number = b.customer_key
--   and (a.buyer_number = b.customer_key or a.co_buyer_number = b.customer_key)
  and b.type_code in ('S','C')
join arkona.ext_bopname c on a.buyer_number = c.bopname_record_key
  and c.company_individ is not null
join arkona.ext_inpmast d on a.bopmast_vin = d.inpmast_vin  
where a.record_status = 'U'
  and a.bopmast_company_number = 'RY2'
  and a.sale_type in ('L','R')
  and a.date_capped between current_date - interval '24 months' and  current_date - interval '18 months'
  and similarity(a.bopmast_search_name, c.bopname_search_name) > .31;
create unique index on sales_2_ry2(vin,name);  

drop table if exists ros_2_ry2 cascade;
create temp table ros_2_ry2 as
select e.bopname_search_name as name, e.address_1 as address, e.city, e.state_code as state, e.zip_code as zip,
  e.phone_number as phone, e.email_address as email,
  b.model_year::integer as year, b.make, b.model, b.vin
from dds.fact_repair_order a
join dds.dim_vehicle b on a.vehicle_key = b.vehicle_key
join dds.dim_customer c on a.customer_key = c.customer_key
join arkona.ext_bopvref d on b.vin = d.vin
  and d.end_date = 0
  and d.type_code = 'C'
  and c.bnkey = d.customer_key
join arkona.ext_bopname e on d.customer_key = e.bopname_record_key
  and e.company_individ is not null  
where open_date between current_date - interval '24 months' and current_date - interval '18 months'
  and a.store_code = 'RY2'
group by e.bopname_search_name, e.address_1, e.city, e.state_code, e.zip_code,
  e.phone_number, e.email_address,
  b.model_year, b.make, b.model, b.vin;

drop table if exists ry1_guests_18_24;
create temp table ry1_guests_18_24 as
select *
from sales_2_ry1 
union
select * 
from ros_2_ry1;

drop table if exists ry2_guests_18_24;
create temp table ry2_guests_18_24 as
select *
from sales_2_ry2 
union
select * 
from ros_2_ry2;

-- ry2 not back past 18 months
select aa.*
from ry2_guests_18_24 aa
left join (
  select e.bopname_search_name as name, b.vin
  from dds.fact_repair_order a
  join dds.dim_vehicle b on a.vehicle_key = b.vehicle_key
  join dds.dim_customer c on a.customer_key = c.customer_key
  join arkona.ext_bopvref d on b.vin = d.vin
    and d.end_date = 0
    and d.type_code = 'C'
    and c.bnkey = d.customer_key
  join arkona.ext_bopname e on d.customer_key = e.bopname_record_key
    and e.company_individ is not null  
  where open_date between current_date - interval '18 months' and current_date
    and a.store_code = 'RY2'
  group by e.bopname_search_name, b.vin) bb on aa.name = bb.name and aa.vin = bb.vin
where bb.vin is null;  


-- ry1 not back past 18 months
select aa.*
from ry1_guests_18_24 aa
left join (
  select e.bopname_search_name as name, b.vin
  from dds.fact_repair_order a
  join dds.dim_vehicle b on a.vehicle_key = b.vehicle_key
  join dds.dim_customer c on a.customer_key = c.customer_key
  join arkona.ext_bopvref d on b.vin = d.vin
    and d.end_date = 0
    and d.type_code = 'C'
    and c.bnkey = d.customer_key
  join arkona.ext_bopname e on d.customer_key = e.bopname_record_key
    and e.company_individ is not null  
  where open_date between current_date - interval '18 months' and current_date
    and a.store_code = 'RY1'
  group by e.bopname_search_name, b.vin) bb on aa.name = bb.name and aa.vin = bb.vin
where bb.vin is null;  