﻿/*
Hi Jon,

Would you please be able to provide the information needed in the attached spreadsheet for any vehicle that has NiceCare or an Extended Warranty.  
Two different spreadsheets.  
NiceCare can we please go back 2 years from today and for Extended Warranty can we please go back 7 years from today.
Please let me know what further information you may need from me.
Thank you,
morgan 01/29/20
*/

Reqd fields: Vin	First Name	Last Name	Phone Number	Email 	Address	City 	State	Zip

-- -- from ...misc_Sql/jeri/mn_service_contracts.sql
-- select a.bopmast_company_number, a.bopmast_stock_number, a.bopmast_vin, a.date_capped,
--   b.contract_name, bopname_search_name, c.state_code, c.zip_code
-- from arkona.xfm_bopmast a
-- join arkona.ext_bopsrvs b on a.bopmast_Company_number = b.company_number
--   and a.record_key = b.deal_key
-- join arkona.xfm_bopname c on b.company_number = c.bopname_company_number
--   and b.customer_key = c.bopname_record_key
--   and c.state_code = 'MN'
-- where a.current_row
--   and a.record_Status = 'U'
--   and a.date_capped between '01/01/2016' and '12/31/2018'




-- -- turns out i have no idea how to tell if a deal was nice care
-- 
--  select *
--  from arkona.xfm_bopmast
--  where bopmast_Stock_number = 'G38894M' 
-- 
--  select *
--  from arkona.ext_bopsrvs
--  where deal_key in (
--    select record_key
--    from arkona.xfm_bopmast
--    where bopmast_stock_number in ('g39206x','g38242xa'))
-- 
--   select *
--   from fin.dim_account
--   where description like '%nice%' 
-- 
-- 
-- select c.the_date, a.control
-- from fin.fact_gl a
-- join fin.dim_account b on a.account_key = b.account_key
--   and b.account = '133208'
-- join dds.dim_date c on a.date_key = c.date_key
--   and c.the_year = 2020 
-- join fin.dim_journal d on a.journal_key = d.journal_key
--   and d.journal_code = 'VSU'  

-- nice care
-- fuck it, use the sale board

--2/03/20
Hi Jon,

Would we be able to add two more columns to this sheet?
•	Date Purchased
•	Mileage on vehicle when purchased

Thank you,
Morgan 


select distinct a.bopmast_vin, c.first_name, c.last_company_name, c.phone_number, 
  c.email_address, c.address_1, c.city, c.state_code, c.zip_code, 
  a.date_capped as date_purchased, a.odometer_at_sale 
from arkona.xfm_bopmast a
join (
  select stock_number, vin, boarded_date
  from board.sales_board
  where used_Vehicle_package = 'nice care'
    and boarded_date between (current_date - INTERVAL '2 years')::date and current_date) b on a.bopmast_vin = b.vin and a.bopmast_stock_number = b.stock_number
join arkona.xfm_bopname c on a.bopmast_company_number = c.bopname_company_number
  and a.buyer_number = c.bopname_record_key
where a.current_row
  and a.record_Status = 'U'
  and a.date_capped between (current_date - INTERVAL '2 years')::date and current_date


-- extended warranty

select distinct a.bopmast_vin, c.first_name, c.last_company_name, c.phone_number, 
  c.email_address, c.address_1, c.city, c.state_code, c.zip_code,
  a.date_capped as date_purchased, a.odometer_at_sale 
from arkona.xfm_bopmast a
join arkona.ext_bopsrvs b on a.bopmast_Company_number = b.company_number
  and a.record_key = b.deal_key
join arkona.xfm_bopname c on b.company_number = c.bopname_company_number
  and b.customer_key = c.bopname_record_key
where a.current_row
  and a.record_Status = 'U'
  and a.date_capped between (current_date - INTERVAL '7 years')::date and current_date


