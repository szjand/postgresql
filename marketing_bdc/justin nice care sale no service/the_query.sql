﻿------------------------------------------------------------------------------------------------------------------------------
-- 9/13/23 this is what i used for the september report (mar 23 sales)
-- 10/10/23 - april 23
-- 3 steps: create temp table deals, update package, generate spreadsheet dataset
drop table if exists deals;
create temp table deals as
select distinct 
  case d.arkona_store_key
    when 'RY1' then 'GM'
    when 'RY2' then 'Honda Nissan'
    when 'RY3' then 'GM'
    when 'RY8' then 'Toyota'
    else 'XXX'
  end as store,
  case
    when e.first_name is null then e.last_company_name
    else e.first_name || ' ' || e.last_company_name
  end as customer,
  a.boarded_ts::date as sale_date, e.phone_number, e.business_phone, e.cell_phone,
  e.city, e.state_code as state, a.stock_number, a.vin, f.year, f.make, f.model, a.used_Vehicle_package
      
-- select a.stock_number, a.deal_details->>'CompanyNumber', e.bopname_company_number, (a.deal_details->>'BuyerNumber')::integer
from board.sales_board a
join board.board_types b on a.board_type_key = b.board_type_key
	and b.board_type_key = 5 --retail deal
	and b.board_type_key not in (16) -- exclude imws
join board.daily_board c on a.board_id = c.board_id
	and c.vehicle_make not in ('yamaha','kawasaki','HARLEY','HARLEY DAVIDSON','HARLEY-DAVIDSON','SUZUKI')
	and c.vehicle_type = 'U'
left join onedc.stores d on a.store_key = d.store_key		
left join arkona.ext_bopname e on (a.deal_details->>'BuyerNumber')::integer = e.bopname_record_key
  and 
    case 
      when a.deal_details->>'CompanyNumber' = 'RY2' then e.bopname_company_number = 'RY1'
      else a.deal_details->>'CompanyNumber' = e.bopname_company_number
    end
  and e.company_individ is not null
left join arkona.ext_inpmast f on a.deal_details->>'VIN' = f.inpmast_vin
  and 
		case
			when a.deal_details->>'CompanyNumber' = 'RY2' then f.inpmast_company_number = 'RY1'
			else a.deal_details->>'CompanyNumber' = f.inpmast_company_number
		end
-- left join (
-- 	select a.open_date, b.vin 
-- 	from dds.fact_repair_order a
-- 	join dds.dim_vehicle b on a.vehicle_key = b.vehicle_key
-- 	where a.open_date > '01/01/2023'  -----------------------------------------------------------------------------------
-- 	group by a.open_date, b.vin ) g on a.vin = g.vin
-- 	  and g.open_date > a.boarded_ts::date 		
where a.boarded_ts::date >= '04/01/2023' and boarded_ts::date < '05/01/2023'  ----------------------------------------- for report on 10/1/23
  and not a.is_deleted
--   and a.used_vehicle_package = 'Nice Care'
--   and g.vin is null
-- order by store, customer
order by stock_number

select store, count(*) from deals group by store
Toyota,36
Honda Nissan,50
GM,184

select store, used_Vehicle_package, count(*) from deals group by store, used_Vehicle_package order by used_vehicle_package, store
GM,Nice Care,101
Honda Nissan,Nice Care,28
Toyota,Nice Care,10

select * from ads.ext_Selected_recon_packages limit 10

-- intra market don't have a package in board data, get it from ads
update deals x
set used_vehicle_package = y.recon_package
from (
	select a.stock_number , 'Nice Care' as recon_package
	from deals a
	left join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
	left join ads.ext_selected_recon_packages c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
	where used_Vehicle_package = 'none'
		and split_part(c.typ, '_', 2) = 'Nice') y
where x.stock_number = y.stock_number  


select store, used_Vehicle_package, count(*) from deals group by store, used_Vehicle_package order by used_vehicle_package, store
GM,Nice Care,113
Honda Nissan,Nice Care,37
Toyota,Nice Care,14


-- the spreadsheet
select aa.* 
from deals aa
left join (
	select a.open_date, b.vin 
	from dds.fact_repair_order a
	join dds.dim_vehicle b on a.vehicle_key = b.vehicle_key
	where a.open_date > '01/01/2023'  -----------------------------------------------------------------------------------
	group by a.open_date, b.vin ) bb on aa.vin = bb.vin
	  and bb.open_date > aa.sale_date
where aa.used_vehicle_package = 'Nice Care'
  and bb.vin is null
order by aa.store, aa.stock_number

-- did a bunch of spot checks, the old way returned wholesale units and missed service visits
-- so, this method produces a significantly smaller data set, i believe it to be more accurate
-- nice care retail with no post sale service visits

select * from deals where store = 'Honda Nissan' order by stock_number


/*
3/18/23
time to take this out of advantage, dimcustomer is all fucked up
this is Justins monthly report of used nice care vehicles sold 6 months ago that have not been in for service

where a.boarded_ts::date >= '08/01/2022' and boarded_ts::date < '09/01/2022' for feb 1 2023
where a.boarded_ts::date >= '09/01/2022' and boarded_ts::date < '10/01/2022' for mar 1 2023
where a.boarded_ts::date >= '10/01/2022' and boarded_ts::date < '11/01/2022' for apr 1 2023
where a.boarded_ts::date >= '11/01/2022' and boarded_ts::date < '12/01/2022' for may 1 2023  done 6/5
where a.boarded_ts::date >= '12/01/2022' and boarded_ts::date < '01/01/2023' for may 1 2023  done 6/5 -- fucked this up, sent a month early
*/

/*
09/11/23
don't even remember when i started this, but, until today, when the report for 202303 returned 0 toyotas
and justin noticed and complained, apparently the old way was good enough
now it is not
*/


-- this is what i came up with back in march of 23, using board data
-- it is way short as i look at it today
select distinct 
  case d.arkona_store_key
    when 'RY1' then 'GM'
    when 'RY2' then 'Honda Nissan'
    when 'RY3' then 'GM'
    when 'RY8' then 'Toyota'
    else 'XXX'
  end as store,
  case
    when e.first_name is null then e.last_company_name
    else e.first_name || ' ' || e.last_company_name
  end as customer,
  a.boarded_ts::date as sale_date, e.phone_number, e.business_phone, e.cell_phone,
  e.city, e.state_code as state, a.stock_number, a.vin, f.year, f.make, f.model
      
-- select a.stock_number, a.deal_details->>'CompanyNumber', e.bopname_company_number, (a.deal_details->>'BuyerNumber')::integer
from board.sales_board a
join board.board_types b on a.board_type_key = b.board_type_key
	and b.board_type_key = 5 --retail deal
	and b.board_type_key not in (16) -- exclude imws
join board.daily_board c on a.board_id = c.board_id
	and c.vehicle_make not in ('yamaha','kawasaki','HARLEY','HARLEY DAVIDSON','HARLEY-DAVIDSON','SUZUKI')
	and c.vehicle_type = 'U'
left join onedc.stores d on a.store_key = d.store_key		
left join arkona.ext_bopname e on (a.deal_details->>'BuyerNumber')::integer = e.bopname_record_key
  and 
    case 
      when a.deal_details->>'CompanyNumber' = 'RY2' then e.bopname_company_number = 'RY1'
      else a.deal_details->>'CompanyNumber' = e.bopname_company_number
    end
  and e.company_individ is not null
left join arkona.ext_inpmast f on a.deal_details->>'VIN' = f.inpmast_vin
  and 
		case
			when a.deal_details->>'CompanyNumber' = 'RY2' then f.inpmast_company_number = 'RY1'
			else a.deal_details->>'CompanyNumber' = f.inpmast_company_number
		end
left join (
	select a.open_date, b.vin 
	from dds.fact_repair_order a
	join dds.dim_vehicle b on a.vehicle_key = b.vehicle_key
	where a.open_date > '01/01/2023'  -----------------------------------------------------------------------------------
	group by a.open_date, b.vin ) g on a.vin = g.vin
	  and g.open_date > a.boarded_ts::date 		
where a.boarded_ts::date >= '03/01/2023' and boarded_ts::date < '04/01/2023'  ----------------------------------------- for report on 9/1/23
  and not a.is_deleted
  and a.used_vehicle_package = 'Nice Care'
  and g.vin is null
--   and a.stock_number = 'H15643A'
order by store, customer

select * from arkona.ext_bopname where bopname_record_key =   1175988

select * from arkona.ext_inpmast where inpmast_vin = '5NPE24AF4FH171033'

select a.open_date, b.vin 
from dds.fact_repair_order a
join dds.dim_vehicle b on a.vehicle_key = b.vehicle_key
where a.open_date > '01/01/2022'
group by a.open_date, b.vin
limit 100

------------------------------------------------------------------------------------------------------------------------------
-- today 9/11/23, did this, a quicky translation from advantage to postgresql

drop table if exists deals;
create temp table deals as
select d.*, e.vehicle_key, e.model_year
from (
  SELECT a.stocknumber, c.vin, b.soldts::Date as sale_date, e.name, c.make, c.model, c.yearmodel
  FROM ads.ext_Vehicle_Inventory_Items a 
  INNER JOIN ads.ext_vehicle_Sales b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  INNER JOIN ads.ext_Vehicle_Items c on a.VehicleItemID = c.VehicleItemID 
  INNER JOIN ads.ext_selected_recon_packages d on a.VehicleInventoryItemID = d.VehicleInventoryItemID
    AND d.typ = 'ReconPackage_Nice'
  LEFT JOIN ads.ext_organizations e on a.locationid = e.partyid
  WHERE b.soldts::date BETWEEN '03/01/2023' AND '03/31/2023' ----------------- sales in mar 23 for report on 9/1/23
    AND b.status = 'VehicleSale_Sold'
  GROUP BY a.stocknumber, c.vin, b.soldts::date, e.name, c.make, c.model, c.yearmodel) d
left join dds.dim_vehicle e on d.vin = e.vin  
left join (
  select b.the_date, a.ro, c.vehicle_key
  from dds.fact_repair_order a
  join dds.dim_date b on a.open_date = b.the_date
  join dds.dim_vehicle c on a.vehicle_key = c.vehicle_key
  join dds.dim_service_type d on a.service_type_key = d.service_type_key
    and d.service_type_code in ('AM','MR','QL','QS','MN')
  where b.year_month > 202301
  group by b.the_date, a.ro, c.vehicle_key) f on e.vehicle_key = f.vehicle_key
    and f.the_date = d.sale_date
where f.ro is null    

-- but it gets wacky with dimcustomer
SELECT a.name, c.full_name, a.sale_date, c.home_phone, c.business_phone, c.cell_phone, c.city, c.state,
  a.stocknumber, a.vin, a.model_year, a.make, a.model
-- SELECT COUNT(*)
-- SELECT *
FROM deals a
LEFT JOIN ads.ext_fact_vehicle_sale b on a.vehicle_key = b.vehiclekey
  AND a.stocknumber = b.stocknumber
LEFT JOIN dds.dim_customer c on b.buyerkey = c.bnkey  
-- WHERE c.customer_key IS NOT NULL
ORDER BY a.name, sale_date

