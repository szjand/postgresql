﻿/*
Hey Jon,

That last list has been working out pretty good.  Can you create me a list of Nissan VIN’s that have been in for service work (main shop or oil change) over the past 3 years that we did not sell?

Thanks,

Justin Kilmer

*/

select current_date - interval '1 year'

drop table if exists service_vins;
create temp table service_vins as
select b.the_Date, a.ro, c.vin, e.fullname
from ads.ext_fact_Repair_order a
join dds.dim_date b on a.opendatekey = b.date_key
  and b.the_date > current_date - interval '3 year'
join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey
  and c.make = 'nissan'
join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey  
  and d.paymenttypecode not in ('U','I') -- unknown, internal
join ads.ext_dim_customer e on a.customerkey = e.customerkey  
  and e.customerkey not in (1,2)  -- unknown, inventory
group by b.the_Date, a.ro, c.vin, e.fullname;


select a.*, b.bopmast_Stock_number, b.record_Status, b.date_capped, b.bopmast_Search_name
from service_vins a
left join arkona.xfm_bopmast b on a.vin = b.bopmast_vin
  and b.current_row
  
select a.vin
from service_vins a
left join arkona.xfm_bopmast b on a.vin = b.bopmast_vin
  and b.current_row
where b.bopmast_Vin is null  
group by a.vin

