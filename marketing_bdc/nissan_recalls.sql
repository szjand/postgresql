﻿/*
8/6/19
Hey Jon,

Here is that Nissan Recall list we were talking about.

Can I get the Name, Phone, Email, City, Year of Vehicle, and Model of Vehicle?

Thanks,

Justin Kilmer
*/
create table jon.nissan_recalls (
  vin citext,
  recall citext,
  seq integer primary key);

populate it from the spreadsheet

-- 479 vins

-- this gives me 495 rows, doubles up on some vins
-- inpmast gives me a bopname_key, but not a store
-- adding the stocknumber to the bopname join gets us back to 479, but missing a shitload of bopname
select a.vin, b.year, b.make, b.model, bopname_search_name
from jon.nissan_Recalls a
left join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
left join arkona.xfm_bopname c on b.bopname_key = c.bopname_Record_key
  and c.current_row
--   and case
--     when left(b.inpmast_stock_number, 1) = 'H' then c.bopname_company_number = 'RY2'
--     else c.bopname_company_number = 'RY1'
--   end
where a.vin <> ''
order by a.vin  


select * from arkona.xfm_bopname where bopname_record_key = 1002840
select * from arkona.xfm_inpmast where bopname_key = 1002840

try bopvref

select vin, max(start_date)
from arkona.ext_bopvref
group by vin

-- this is sort of close
-- 8/8 close enough
-- add row_number to do some clean up
drop table if exists base;
create temp table base as
select row_number() over (order by a.seq), a.*, d.bopname_search_name, d.phone_number, d.email_address, d.city, 
  c.year, c.model
from jon.nissan_recalls a
left join arkona.ext_bopvref b on a.vin = b.vin
  and b.end_date = 0
left join arkona.xfm_inpmast c on a.vin = c.inpmast_vin
  and c.current_row
  and b.customer_key = c.bopname_key
left join arkona.xfm_bopname d on c.bopname_key = d.bopname_Record_key
  and d.current_row  
where a.vin <> ''
order by a.seq;


delete 
from base
where row_number in (
  select row_number 
  from base a
  join (
    select vin
    from base
    group by vin having count(*) > 1) b on a.vin = b.vin
  where a.bopname_search_name is null);


select * 
from base
where seq in (
  select seq
  from base
  group by seq
  having count(*) > 1)


delete
from base
where row_number in(7,9,15,16,67,114,139,147,196,205,309,356,388,493,543,553,561,573)


select *
from base a
join (
  select vin
  from base
  group by vin
  having count(*) > 1) b on a.vin = b.vin

delete from base where row_number in (6,17);

delete from base
where row_number in (
  select b.row_number
  from jon.nissan_recalls a
  left join base b on a.vin = b.vin
  where a.vin <> ''
    and bopname_Search_name is null);


-- this is what i sent to justin
select a.vin, a.recall, b.bopname_search_name as name, 
  b.phone_number, b.email_address, b.city, b.year, b.model
from jon.nissan_recalls a
left join base b on a.vin = b.vin;



