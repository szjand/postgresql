﻿drop table if exists jon.justin_05_07_20;
create table jon.justin_05_07_20 (
  customer citext,
  vin citext,
  service_advisor citext,
  ro_date date);
create unique index on jon.justin_05_07_20(vin);
-- 868 rows

-- 5 null bopvref
select *
from jon.justin_05_07_20 a
left join arkona.ext_bopvref b on a.vin = b.vin
  and b.end_date = 0
where b.vin is null

-- that's weird, but they are in inpmast
select *
from arkona.xfm_inpmast
where current_row
  and inpmast_vin in (
    select a.vin
    from jon.justin_05_07_20 a
    left join arkona.ext_bopvref b on a.vin = b.vin
      and b.end_date = 0
    where b.vin is null)

select *
from arkona.ext_bopvref
where vin = '1GCVKREC7GZ398675'    
order by start_date

select a.*
from arkona.xfm_inpmast a
where a.inpmast_vin = '1GCVKREC7GZ398675' 
order by a.inpmast_key

select * 
from arkona.xfm_bopname
where bopname_record_key = 1070547

-- -- #1 conclusion from this query, bopname_key is not reliable
-- select a.inpmast_stock_number, a.status, a.year, a.make, a.model, a.bopname_key, a.row_From_date, a.row_thru_date, b.bopname_search_name
-- from arkona.xfm_inpmast a
-- left join arkona.xfm_bopname b on a.bopname_key = b.bopname_record_key
--   and b.current_row
-- where a.inpmast_vin = '1GCVKREC7GZ398675' 
-- order by a.inpmast_key

select a.inpmast_stock_number, a.status, a.year, a.make, a.model, a.bopname_key, a.row_From_date, a.row_thru_date
from arkona.xfm_inpmast a
where a.inpmast_vin = '1GCVKREC7GZ398675' 
order by a.inpmast_key


select a.inpmast_stock_number, a.status, a.year, a.make, a.model, min(a.row_From_date) as from_date, max(a.row_thru_date) as thru_date
from arkona.xfm_inpmast a
where a.inpmast_vin = '1GCVKREC7GZ398675' 
group by a.inpmast_stock_number, a.status, a.year, a.make, a.model


select *
from arkona.ext_sdprhdr
where vin = '1GCVKREC7GZ398675'


select min(open_date), max(open_date)
from arkona.ext_sdprhdr

select *
from arkona.ext_bopvref
where vin = '1G1155S33EU143267'

-- 875
-- 855 w/join on bopname
select a.*,
  b.customer_key, b.start_date, b.type_code, 
  c.bopname_search_name,
  similarity(a.customer, c.bopname_search_name)
-- the query for the spreadsheet
select a.customer as "Customer Name", c.phone_number as "Customer Phone", c.address_1 as "Address", c.city as "City",
  a.vin as "VIN", d.year, d.make, d.model,
  a.service_advisor as "Service Advisor", a.ro_date as "R.O. Date"
from jon.justin_05_07_20 a
join arkona.ext_bopvref b on a.vin = b.vin
  and b.end_date = 0
  and b.company_number <> 'RY5'
join arkona.xfm_bopname c on b.customer_key = c.bopname_Record_key
  and c.current_row  
  and coalesce(similarity(a.customer, c.bopname_search_name), 0) > .5
  and bopname_company_number <> 'RY5'
join arkona.xfm_inpmast d on a.vin = d.inpmast_vin
  and d.current_row
order by ro_date desc   
  
