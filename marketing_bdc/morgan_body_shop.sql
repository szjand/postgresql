﻿/*
Hi Jon,

Can you please help me by pulling two data sets?

I am looking to get a list of all non internal work complete in the body shop from June 1st 2020 – October 31st 2020 and August 1st 2019 – October 31st 2019.  

The information I am looking for is:
•	Guest Name
•	Email
•	Date Work Completed

Thank you,
Morgan 
11/06/2020
*/
select * from dds.dim_payment_type
select * from arkona.ext_bopname limit 10
select * from dds.fact_repair_order limit 10

drop table if exists tem.ro_1 cascade;
create table tem.ro_1 as
select a.store_code, ro, close_date, customer_key
from dds.fact_Repair_order a
join dds.dim_payment_type b on a.payment_type_key = b.payment_type_key
  and b.payment_type_code <> 'I'
where a.close_date between '06/01/2020' and '10/31/2020'
  and left(ro, 2) = '18'
group by a.store_code, ro, close_date, customer_key;

select distinct b.full_name, b.email, a.close_date
from tem.ro_1 a
join dds.dim_customer b on a.customer_key = b.customer_key
  and b.bnkey <> 0
where b.full_name not like all(array['CARRY%','FIVE%','ABRA%','LITHIA%','RYDELL%'])
--   and b.email is not null -- 974
order by b.full_name;

-- was hoping bopname would have more emails, actually lost a few
select distinct b.full_name, c.email_address, a.close_date
from tem.ro_1 a
join dds.dim_customer b on a.customer_key = b.customer_key
  and b.bnkey <> 0
join arkona.ext_bopname c on a.store_code = c.bopname_company_number
  and b.bnkey = c.bopname_record_key
  and c.company_individ is not null
where b.full_name not like all(array['CARRY%','FIVE%','ABRA%','LITHIA%','RYDELL%'])
order by b.full_name;



drop table if exists tem.ro_2 cascade;
create table tem.ro_2 as
select a.store_code, ro, close_date, customer_key
from dds.fact_Repair_order a
join dds.dim_payment_type b on a.payment_type_key = b.payment_type_key
  and b.payment_type_code <> 'I'
where a.close_date between '08/01/2019' and '10/31/2019'
  and left(ro, 2) = '18'
group by a.store_code, ro, close_date, customer_key;

select distinct b.full_name, c.email_address, a.close_date
from tem.ro_2 a
join dds.dim_customer b on a.customer_key = b.customer_key
  and b.bnkey <> 0
join arkona.ext_bopname c on a.store_code = c.bopname_company_number
  and b.bnkey = c.bopname_record_key
  and c.company_individ is not null
where b.full_name not like all(array['CARRY%','FIVE%','ABRA%','LITHIA%','RYDELL%'])
order by b.full_name;