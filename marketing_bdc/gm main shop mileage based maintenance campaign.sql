﻿/*
10/04/22 from andrew:
How difficult would it be to pull a list from our DMS based of being strictly GM, Ford and Dodge with 45,000 to 110,000 mile as of their last visit. One other criteria would be that they have been in our store once within the past 24 months. We are trying to think of some lists to make up for our BDC campaigns for outbound activity. Thoughts?

Yes it would include that Ford in the list. You have exactly the items I would want to see. 
Year, Make, Model, Date of last visit, Mileage at last visit, phone, email and customer name. That would be perfect. 

*/

/* BOPVREF */
BVTYPE (TYPE_CODE) Customer/Vehicle Reference
S = Buyer on the Deal
2 = Co-Buyer on the Deal
C = Service Customer
T = Trade In
4 = ?? (Of the 212409 rows in BOPVREF table, only 59 are type 4)

select * 
-- select count(*) -- 100377, 72.792
from arkona.ext_bopvref a
join arkona.ext_inpmast b on a.vin = b.inpmast_vin
  and b.make in ('chevrolet','gmc','cadillac','buick','ford','dodge','ram')
where type_code = 'C'
  and end_date = 0
limit 10

select * from dds.dim_customer limit 10

select count(*) from (  -- 86411
select a.ro, a.open_date, a.miles, b.model_year, b.make, b.model, c.full_name, c.bnkey,  c.home_phone, c.cell_phone, c.email
from dds.fact_repair_order a
join dds.dim_vehicle b on a.vehicle_key = b.vehicle_key
  and b.make in ('chevrolet','gmc','cadillac','buick','ford','dodge','ram')
join dds.dim_customer c  on a.customer_key = c.customer_key
  and c.bnkey <> 0
where a.open_date > current_date - 730
group by a.ro, a.open_date, a.miles, b.model_year, b.make, b.model, c.full_name, c.bnkey, c.home_phone, c.cell_phone, c.email
) x

select count(*) from (
select a.ro, a.open_date, a.miles, b.model_year, b.make, b.model, c.full_name, c.bnkey,  c.home_phone, c.cell_phone, c.email, d.*
from dds.fact_repair_order a
join dds.dim_vehicle b on a.vehicle_key = b.vehicle_key
  and b.make in ('chevrolet','gmc','cadillac','buick','ford','dodge','ram')
join dds.dim_customer c  on a.customer_key = c.customer_key
  and c.bnkey <> 0
join arkona.ext_bopvref d on b.vin = d.vin
  and c.bnkey = d.customer_key
  and c.type_code = 
  and d.end_date = 0
  and dds.db2_integer_to_date(d.start_date) > '10/01/2020'
where a.open_date > '10/01/2020'
) x


-- this gives me the ability to isolate the most recent visit 
drop table if exists step_1 cascade;
create temp table step_1 as
select a.ro, a.open_date, a.miles, b.model_year, b.make, b.model, b.vin, c.full_name, c.bnkey,  c.home_phone, c.cell_phone, c.email,
  row_number() over (partition by full_name, vin order by open_date desc) as seq
from dds.fact_repair_order a
join dds.dim_vehicle b on a.vehicle_key = b.vehicle_key
  and b.make in ('chevrolet','gmc','cadillac','buick','ford','dodge','ram')
join dds.dim_customer c  on a.customer_key = c.customer_key
  and c.bnkey <> 0
  and full_name not in ('.','1','UNKNOWN','RYDELL AUTO CENTER','AVIS','ND DOT','RYDELL TOYOTA OF GRAND FORKS','CITY OF GRAND FORKS','COURTESY DELIVERY','ENTERPRISE')
where a.open_date > '10/01/2020'
group by a.ro, a.open_date, a.miles, b.model_year, b.make, b.model, b.vin, c.full_name, c.bnkey, c.home_phone, c.cell_phone, c.email

-- the most recent visit for customer/vin only
drop table if exists step_2 cascade;
create temp table step_2 as
select * 
from step_1
where seq = 1;

delete from step_2 where full_name like 'rydell%';

-- remove the large count full_names and impose the mileage restriction
drop table if exists step_3 cascade;
create temp table step_3 as
select a.*
from step_2 a
left join (
	select full_name, count(*)
	from (
		select full_name
		from step_2 
		where seq = 1) a
	group by full_name 
	having count(*) > 5) b on a.full_name = b.full_name
where b.full_name is null
  and a.miles between 45000 and 110000;

/*
-- step_3 is only the most recent visit for any customer/vin
select full_name, vin
from step_3
group by full_name, vin
having count(*) > 1

select vin, count(*)
from step_3
group by vin
having count(*) > 1
order by count(*) desc

select * from step_3 where vin = '1GKS2MEF7ER218449' order by open_date
*/

next is to use bopvref to assert current ownership
ok, this gives me 1785 rows, reasonable start, even though i know there is junk data in here,
but that is unavoidable
this is assuming the ro open date = bopvref.start_date
select a.* 
from step_3 a
join arkona.ext_bopvref b on a.vin = b.vin
  and a.bnkey = b.customer_key
  and a.open_date = arkona.db2_integer_to_date(b.start_date)
  and b.end_date = 0
  and b.type_code = 'C'
where a.vin not in ('1D7RV1GTXAS198712')  
order by a.full_name, a.vin  

this is good enough format it for spreadsheet and send it to andrew

select a.full_name, a.home_phone, a.cell_phone, a.email, open_date as last_visit, a.vin, model_year, make, model, miles
-- select *
from step_3 a
join arkona.ext_bopvref b on a.vin = b.vin
  and a.bnkey = b.customer_key
  and a.open_date = arkona.db2_integer_to_date(b.start_date)
  and b.end_date = 0
  and b.type_code = 'C'
where a.vin not in ('1D7RV1GTXAS198712')  
order by a.full_name, a.vin  
-- limit 10