﻿/*
03/13/21
Can you get me a list from 2015 to present of all the 2016-2019 Nissan Sentra’s that have been sold or serviced at either store?  

I would need the following info:
First Name
Last Name
Home Phone
Cell Phone
Email
City
Year
Make
Model
VIN

Thanks!  Hope you are doing well 😊

Justin Kilmer
*/

-- sales
drop table if exists sales cascade;
create temp table sales as
select distinct c.first_name, c.last_company_name as last_name, 
  c.phone_number as home_phone, c.cell_phone as cell_phone, c.email_address as email, c.city,
  d.year as year, d.make, d.model, 
  a.bopmast_vin as vin
from arkona.ext_bopmast a
join arkona.ext_bopvref b on a.bopmast_vin = b.vin
  and b.end_date = 0
  and a.buyer_number = b.customer_key
  and b.type_code in ('S','C')
join arkona.ext_bopname c on a.buyer_number = c.bopname_record_key
  and c.company_individ is not null  -- this gets rid of the bogus dups
join arkona.ext_inpmast d on a.bopmast_vin = d.inpmast_vin  
  and d.year::integer between 2016 and 2019
  and d.model = 'sentra'
where a.record_status = 'U'
  and a.sale_type in ('L','R')
  and a.date_capped between '01/01/2015' and current_date
  and similarity(a.bopmast_search_name, c.bopname_search_name) > .31
  and c.last_company_name = 'martin';
create unique index on sales(vin,last_name);  

drop table if exists ros cascade;
create temp table ros as
select distinct e.first_name, e.last_company_name as last_name, 
  e.phone_number as home_phone, e.cell_phone as cell_phone, e.email_address as email, e.city,
  b.model_year::integer as year, b.make, b.model, b.vin
from dds.fact_repair_order a
join dds.dim_vehicle b on a.vehicle_key = b.vehicle_key
  and b.model_year::integer between 2016 and 2019
  and b.model = 'sentra'
join dds.dim_customer c on a.customer_key = c.customer_key
join arkona.ext_bopvref d on b.vin = d.vin
  and d.end_date = 0
  and d.type_code = 'C'
  and c.bnkey = d.customer_key
join arkona.ext_bopname e on d.customer_key = e.bopname_record_key
  and e.company_individ is not null  
where open_date between '01/01/2015' and current_date;


select * 
from sales a
union
select * 
from ros
