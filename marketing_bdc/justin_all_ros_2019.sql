﻿/*
07/08/20
Hey Jon,

I was wondering if you could get me a new list together.  I need a list of every customer that has had an RO number from 1/1/2019 – 1/1/2020.

First Name
Last Name
Street Address
City 
State
Zip
Year
Make
Model
VIN

If a customer has had more than one vehicle in is there a way to group them together?  If they have had a vehicle in more than once is there a way to remove any duplicates and just go with one instance of the individual being on the list?

Thanks,


Justin Kilmer

jon: What do you mean by “group them together”?

justin: 
Let’s say for instance I have 2 vehicles under my name.  2019 Silverado and 2019 Encore.  Both have been in the dealership within last year so both will have an RO tied to them.  Is there anyway to just list me once and have both vehicles listed under me.

Kinda like this if it’s possible, or if this is a lot of work or can’t be done, we can just filter all of them when the list is done.

Justin Kilmer  2182078558 jkilmer@rydellcars.com  831 24th st south, gf, nd 58201 2019 Chevrolet Silverado VIN / 2019 Buick Encore VIN

I’m sorry I also forgot to add in we need Phone and Email.  Just put them right after the customer name.

jon:
I can surely do that, but it will get a bit awkward when, say, Altru, has 112 vehicles.
I can do it, but are you sure that is what you want?

If what you need is a single row per customer, that is probably the best way to do it.

justin:
You know what I think maybe we should just create the list like we would creating a normal one, and then we can just sort it, otherwise it might reduce our sorting ability, or cause some other unforeseen issue.  Forget I mentioned grouping them together.

*/
select a.ro_number, a.cust_name, a.customer_key, a.vin
from arkona.ext_Sdprhdr_history a
where arkona.db2_integer_to_date(a.open_date) between '01/01/2019' and '01/01/2020'
  and a.cust_name <> '*VOIDED REPAIR ORDER*'
  and a.cust_name not like 'INVENTORY%'
order by a.cust_name


drop table if exists ros;
create temp table ros as
select max(a.cust_name) as cust_name, a.customer_key, a.vin
from arkona.ext_Sdprhdr_history a
where arkona.db2_integer_to_date(a.open_date) between '01/01/2019' and '01/01/2020'
  and a.cust_name <> '*VOIDED REPAIR ORDER*'
  and a.cust_name not like 'INVENTORY%' 
  and a.cust_name not like '%AUCTIO%'
  and a.cust_name not like '%ADESA%'
  and a.cust_name not like 'AVIS%'
  and a.cust_name not like 'LITHIA%'
  and a.cust_name not in ('C&M FORD SALES','CORWIN HONDA','CROWN MOTORS','DAN PORTER MOTORS','DEALER TRADE 2019',
    'DVORAK MOTORS','EIDE MOTORS','ENTERPRISE','ENTERPRISE CAR RENTAL','ENTERPRISE CAR RENTAL, ENTERPR',
    'ENTERPRISE HOLDINGS, INC','ENTERPRISE RENT A CAR','GATEWAY NISSAN','HAUSMANN MOTOR SPORTS, L.L.C.',
    'HONDA NISSAN OF GRAND FORKS','HONDA OF GRAND FORKS','HONDA INVENTORY','KUPPER CHEVROLET',
    'RYAN HONDA OF MINOT','RYDELL AUTO CENTER','RYDELL HONDA OF GRAND FORKS','RYDELL AUTO CENTER INC',
    'RYDELL HONDA OF GRAND FORKS','RYDELL NISSAN/GRAND FORKS','TWIN CITY MOTOR')
group by a.customer_key, a.vin; 
create unique index on ros(customer_key,vin);
create index on ros(customer_key);
create index on ros(vin);

select count(*) from ros -- 49642

select count(*) from ros where cust_name like '%ALTRU%'

select * from ros where customer_key = 320696

select customer_key, count(*)
from ros
group by customer_key
order by count(*) desc


-- given justin's fluctuating, i will send him a list with vins grouped and a list without
--29111 rows
-- with grouped vins
select b.last_company_name, b.first_name, b.phone_number, b.email_address, 
  b.address_1 as address, b.city, b.state_code as state, b.zip_code, string_agg(concat_ws('::',c.inpmast_vin,c.make,c.model,c.year::citext), '||') as vehicles
from ros a
join arkona.xfm_bopname b on a.customer_key = b.bopname_record_key
  and b.current_row
  and b.company_individ is not null 
join arkona.xfm_inpmast c on a.vin = c.inpmast_vin
  and c.current_row  
where b.address_1 is not null  
  and a.customer_key not in (1106133,1140275,1141337,284418)
  and b.last_company_name not like 'HONDA%'
group by a.customer_key, b.last_company_name, b.first_name, b.phone_number, b.email_address, 
  b.address_1, b.city, b.state_code, b.zip_code  
order by b.last_company_name, b.first_name  


-- without vin grouping, 36110
select b.last_company_name, b.first_name, b.phone_number, b.email_address, 
  b.address_1 as address, b.city, b.state_code as state, b.zip_code, c.inpmast_vin,c.make,c.model,c.year 
from ros a
join arkona.xfm_bopname b on a.customer_key = b.bopname_record_key
  and b.current_row
  and b.company_individ is not null 
join arkona.xfm_inpmast c on a.vin = c.inpmast_vin
  and c.current_row  
where b.address_1 is not null  
  and a.customer_key not in (1106133,1140275,1141337,284418)
  and b.last_company_name not like 'HONDA%'
order by b.last_company_name, b.first_name, c.inpmast_vin 