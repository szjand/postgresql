﻿-- requires sdprhdr update

select *
from (
  select distinct a.bopmast_vin
  from arkona.xfm_bopmast a
  join arkona.xfm_inpmast b on a.bopmast_vin = b.inpmast_vin
    and b.current_row
    and b.year in (2017, 2018)
    and b.make = 'nissan'
    and b.model = 'rogue'
  where a.current_row
    and a.date_capped > current_date - '3 years'::interval 
    and a.record_status = 'U'
  union 
  select distinct a.vin
  from arkona.ext_sdprhdr a
  join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
    and b.current_row
    and b.year in (2017, 2018)
    and b.make = 'nissan'
    and b.model = 'rogue'
  where a.open_date > 20160827) x
order by bopmast_vin

select * from arkona.ext_sdprhdr limit 10
