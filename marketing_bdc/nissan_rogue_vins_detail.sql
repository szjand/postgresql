﻿create table jon.rogue_vins (
  vin citext primary key,
  seq integer not null);

populate from justins spreadsheet, 146 vins

select * from jon.rogue_vins


select *
from (
  select a.vin, b.customer_key, row_number() over (partition by a.vin order by a.vin, b.start_date desc)
  from jon.rogue_vins a
  left join arkona.ext_bopvref b on a.vin = b.vin) x
where x.row_number = 1  
order by vin


select x.seq, x.vin, z.bopname_search_name as name, z.city, z.phone_number, z.email_address, y.year, y.make, y.model
from (
  select a.vin, a.seq, b.customer_key, row_number() over (partition by a.vin order by a.vin, b.start_date desc)
  from jon.rogue_vins a
  left join arkona.ext_bopvref b on a.vin = b.vin) x
join arkona.xfm_inpmast y on x.vin = y.inpmast_vin
  and y.current_row  
left join arkona.xfm_bopname z on x.customer_key = z.bopname_record_key  
  and z.current_row
where x.row_number = 1  
--   and bopname_search_name <> 'SST' -- this is the one double record key, they both have same dates
order by seq

