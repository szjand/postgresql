﻿/*
Hi Jon,

Would you be able to provide me with a list of anyone that has had a Cadillac serviced with us in two years that was not purchased with us.

If we are able to do that, could we get the below information

Owner Name
Cell Phone Number
Home Phone Number
Email Address
Make
Model
Year
Last Recorded Miles
Last Time In for Service
Vin Number

Morgan Hibma
*/

drop table if exists service_customers;
create temp table service_customers as
select c.bopname_record_key, a.cust_name, c.cell_phone, c.phone_number, c.email_address,
  b.make, b.model,b.year, max(a.odometer_out) as odometer_out, 
  max(arkona.db2_integer_to_date_long(open_Date)) as open_date, a.vin 
from arkona.ext_sdprhdr a
join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
  and b.make = 'cadillac'
join arkona.xfm_bopname c on a.customer_key = c.bopname_record_key
  and a.cust_name = c.bopname_search_name
  and c.current_row
where arkona.db2_integer_to_date_long(open_Date) > current_date - 730  
group by c.bopname_record_key, a.cust_name, c.cell_phone, c.phone_number, c.email_address,
  b.make, b.model,b.year, a.vin  

select * from service_customers

drop table if exists no_deals;
create temp table no_deals as
select a.*, b.date_capped, b.bopmast_Stock_number, b.record_Status
from service_customers a
left join arkona.xfm_Bopmast b on a.vin = b.bopmast_vin
  and a.bopname_record_key = b.buyer_number
  and b.record_status = 'U'
  and b.current_row
where b.bopmast_vin is null  
  and a.cust_name not like 'RYDELL%';

-- looks good enough
selecT a.*, b.bopmast_search_name, b.date_capped
from no_deals a
left join arkona.xfm_bopmast b on a.vin = b.bopmast_vin
  and b.current_row



select cust_name as owner_name, cell_phone, phone_number, email_address,
  make, model, year, odometer_out as last_recorded_miles, 
  open_Date as last_time_in_for_service, vin
from no_deals
order by cust_name  


