﻿select * 
from greg.uc_base_vehicle_statuses
where status in ('avail_fresh','avail_aged')
  and the_date between '06/01/2021' and current_date
limit 100

select distinct status
from greg.uc_base_vehicle_statuses

09/07/21
Hi Jon,

Is there a way to get an inventory snapshot of  on average how many new and used vehicles we had on the lot for June, July and August?

Thank you,
Morgan 


select * 
from greg.uc_base_vehicle_statuses a
join greg.uc_base_vehicles b on a.vehicle_inventory_item_id = b.vehicle_inventory_item_id
  and b.store_code = 'RY1'
where a.status in ('avail_fresh','avail_aged')
  and a.the_date = current_date - 1

select 
	(count(*) filter (where a.the_date between '6/01/2021' and '06/30/2021'))/30 as june,
	(count(*) filter (where a.the_date between '7/01/2021' and '07/31/2021'))/30 as july,
	(count(*) filter (where a.the_date between '8/01/2021' and '08/31/2021'))/30 as august
from greg.uc_base_vehicle_statuses a
join greg.uc_base_vehicles b on a.vehicle_inventory_item_id = b.vehicle_inventory_item_id
  and b.store_code = 'RY1'
where a.status in ('avail_fresh','avail_aged')
  and a.the_date between '06/01/2021' and '08/31/2021'

select * 
from nc.vehicle_acquisitions a
where a.stock_number not like 'H%'
  and a.ground_date < '07/01/2021'
  and a.thru_date > '07/01/2021'
order by ground_date  

june: 41


select distinct status
from nc.daily_inventory a


select 
	(count(*) filter (where a.the_date between '6/01/2021' and '06/30/2021'))/30 as june,
	(count(*) filter (where a.the_date between '7/01/2021' and '07/31/2021'))/30 as july,
	(count(*) filter (where a.the_date between '8/01/2021' and '08/31/2021'))/30 as august
from nc.daily_inventory a
where a.the_date between '06/01/2021' and '08/31/2021'  
	and a.status = 'inv_og'
  and a.stock_number not like 'H%'