﻿/*
Would you be able to pull a list of anyone and everyone that has purchased a vehicle from us in the last 6 years, and anyone that has had their vehicle serviced with us in the last 3 years.

What needed on list:
First Name
Last Name
Phone Number
Email
City 
State
Vehicle
Last Service Date (if they have serviced with us)
Date Purchased (if purchased from us)


Thank you for any and all help on this Jon!
Morgan 

Morgan Hibma
*/

going for only the most recent sale and most recent ro

drop table if exists sales cascade;
create temp table sales as
select bopmast_company_number, buyer_number, bopmast_search_name, date_capped, bopmast_vin
from (
  select a.bopmast_company_number, a.date_capped, a.buyer_number, a.bopmast_search_name,
    row_number() over (partition by bopmast_search_name order by date_capped desc),
    a.bopmast_vin
  from arkona.xfm_bopmast a
  where a.current_row
    and a.date_capped > current_date - '6 years'::interval
    and a.record_status = 'U'
    and a.sale_type <> 'W'
    and bopmast_search_name not in ('ALTRU HEALTH SYSTEM','RYDELL AUTO CENTER',
      'HANSEN INVESTMENTS INC.','GELCO CORPORATION','CITY OF GRAND FORKS',
      'RYDELL NISSAN OF GRAND FORKS','GELCO FLEET TRUST','WILWAND FARMS')) b
where row_number = 1;
create unique index on sales(bopmast_search_name)

select * from sales

drop table if exists serv_1;
create temp table serv_1 as
select a.company_number, a.customer_key, a.cust_name,
  arkona.db2_integer_to_date_long(a.open_date) as open_date, a.vin
from arkona.ext_sdprhdr a
where arkona.db2_integer_to_date_long(a.open_date) > current_date - '3 years':: interval
  and cust_name not in ('*VOIDED REPAIR ORDER*','RYDELL AUTO CENTER','SHOP TIME',
  'ALTRU HEALTH SYSTEM','ND DOT','CARRY IN','COURTESY DELIVERY','EIDE MOTORS',
  'GATEWAY CENEX','AVIS','ENTERPRISE','SYSTEM, ALTRU','BODY SHOP TIME ADJ')
  and customer_key <> '0';

drop table if exists service cascade;
create temp table service as
select company_number, customer_key, cust_name, open_date, vin
from (
  select a.*,
    row_number() over (partition by cust_name order by open_date desc)
  from serv_1 a) a
where row_number = 1;
create unique index on service(cust_name);  

drop table if exists combined;
create temp table combined as
select 'sales' as dept, a.* 
from sales a
union
select 'service', b.*
from service b;

create temp table temp_1 as
select dept, bopmast_company_number as store, buyer_number as cust_key, bopmast_Search_name as customer, 
  date_capped as the_date, bopmast_vin as vin
from (  
  select a.*,
    row_number() over (partition by bopmast_search_name order by date_capped desc)
  from combined a) b
where row_number = 1;  

-- up until now, have grouped based on name
-- for this final cut, group on cust_key

drop table if exists customers;
create temp table customers as
select dept, store, cust_key, customer, the_date, vin
from (
  select a.*,
    row_number() over (partition by cust_key order by the_date desc)
  from temp_1 a) b
where row_number = 1;

-- that left a shitload of dups, so lets trim down by name first

drop table if exists customers;
create temp table customers as
select dept, store, cust_key, customer, the_date, vin
from (
  select a.*,
    row_number() over (partition by customer order by the_date desc)
  from temp_1 a) b
where row_number = 1;



drop table if exists final_1;
create temp table final_1 as
select row_number() over(), b.first_name, b.last_company_name, phone_number, email_address, city, state_code, 
  c.year, c.make, c.model, a.dept, a.the_date
from customers a
left join arkona.xfm_bopname b on a.cust_key = b.bopname_record_key
  and b.current_row
  and bopname_company_number = 'RY1'
left join arkona.xfm_inpmast c on a.vin = c.inpmast_vin  
  and c.current_row
order by a.customer;

drop table if exists final_2;
create temp table final_2 as
select first_name, last_company_name, phone_number, email_address, city, state_code, year, make, model, dept, the_date
from (
  select first_name, last_company_name, phone_number, email_address, city, state_code, year, make, model, dept, the_date,
    row_number() over (partition by first_name, last_company_name order by the_date desc)
  from final_1 a) b
where row_number = 1



select * 
from final_2
order by last_company_name, first_name

delete from final_2 where last_company_name in ('031379','.','176980','191704','6','670998','754757',
'A')

select *
from final_2













delete from final_1 where row_number in (4104,1463, 18600,45006,39005,4302,30694,21879,23622,3916,29616,
21034,41842,30694,47036,51099,17256,


select * 
from final_1 a
join (
  select first_name, last_company_name
  from final_1
  group by first_name,last_company_name
  having count(*) > 1) b on a.first_name = b.first_name and a.last_company_name = b.last_company_name
order by a.last_company_name, a.first_name, row_number