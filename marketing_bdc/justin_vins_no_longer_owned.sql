﻿/*
10/24/21
Hey Jon, is there any way you can cross reference this list with the DMS and remove the ones that say RYDELL GM NO LONGER OWNS.  
I hope that makes sense.  When a guest no longer own a vehicle we transfer them to that in the DMS.  
I would like those removed from this Excel spread sheet.

Thanks,

Justin Kilmer
*/
PS, these are takata air bag recalls

select * 
from arkona.ext_inpmast
where inpmast_vin = '1GNFK13067R295107'

select * 
from arkona.ext_bopvref
where vin = '1GNFK13067R295107'

select * 
from arkona.ext_bopname
where bopname_record_key = '1149275'

-- these are the 4 customer numbers
select * 
from arkona.ext_bopname
where bopname_search_name like '%longer%'
  or last_company_name like '%longer%'

select *
from arkona.xfm_bopvref
where  

drop table if exists jon.justin_vins_1 cascade;
create unlogged table jon.justin_vins_1 (
  vin citext);
  
-- 565 rows

select a.vin, c.bopname_search_name, c.last_company_name
from jon.justin_vins_1 a
join arkona.ext_bopvref b on a.vin = b.vin
  and end_date = 0
left join arkona.ext_bopname c on b.customer_key = c.bopname_record_key
  and b.company_number = c.bopname_company_number
where c.bopname_search_name like '%longer%'
  or c.last_company_name like '%longer%'


drop table if exists jon.temp;
create table jon.temp(
  first_name citext,
  last_name citext,
  city citext,
  home_phone citext,
  email citext,
  vehicle_year citext,
  vehicle_make citext,
  vehicle_model citext,
  vin citext);

-- 6510
select count(*) from jon.temp

so, inserted the entire spreadsheet into jon.temp:
insert into jon.temp values('','Interstate Improvement','Faribault','320 3640559','','2012','Chevrolet','Silverado HD','1GB4CZCL4CF179572');
insert into jon.temp values('Jacob Michael','Wilde','Thief River Falls','760 9089402','','2013','Chevrolet','Silverado HD','1GB4KZC8XDF220206');
insert into jon.temp values('','Valley Petroleum Equipment Inc','Grand Forks','','','2013','Chevrolet','Silverado HD','1GB5CZCG0DZ147905');
insert into jon.temp values('','Minnkota Power Cooperative Inc','Grand Forks','701 7415944','kmillette@minnkota.com','2011','Chevrolet','Silverado HD','1GB5KZCG3BZ407844');
...

now do a join excluding the above rows which decode to NO LONGER OWNED

select a.* 
from jon.temp a
left join (
	select a.vin, c.bopname_search_name, c.last_company_name
	from jon.justin_vins_1 a
	join arkona.ext_bopvref b on a.vin = b.vin
		and end_date = 0
	left join arkona.ext_bopname c on b.customer_key = c.bopname_record_key
		and b.company_number = c.bopname_company_number
	where c.bopname_search_name like '%longer%'
		or c.last_company_name like '%longer%') b on a.vin = b.vin
where b.vin is null


11/15/21 looks like there are 6 more
updated the spreadsheet removing those 6 vins and sent to justin
select * 
from arkona.ext_bopvref
where vin in ('3GTEK13C97G507531','1GNUKJE33AR257629','1GTEK19008Z313520','1GNFK13067R295107','1GNSKJE7XDR213688','1GCVKREC3EZ223675')
order by vin, start_date