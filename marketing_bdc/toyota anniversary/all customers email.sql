﻿-- 6/19/23
-- Hi Jon,
-- 
-- Are you able to pull a list of everyone that has done business with us in the past year at Toyota.  
-- Basically anyone we have in the DMS that is tied to Toyota in someway.  
-- I am looking for just their name and email address.  
-- We are running a one year anniversary event for Toyota and we are sending invites out through email.
-- 
-- Please let me know if you need anything further from me on this.
-- 
-- Thank you,
-- Morgan 

select ro, open_date, customer_key
from dds.fact_Repair_order
where ro like '8%'
limit 10


select bopname_Search_name as name, email_Address as email
-- select *
from arkona.ext_bopname
where bopname_company_number = 'RY8'
  and company_individ is not null
limit 20

-- change the delimiter in the output
-- then open from within excel not file explorer
select bopname_Search_name as name, email_Address as email
from arkona.ext_bopname
where bopname_company_number = 'RY8'
  and company_individ is not null
  and allow_contact_by_email <> 'N'
  AND email_address is not null
  and email_address not like 'NOEMAIL@%'
  and email_address not like '%NONE@%'
  and length(email_address) > 5
order by bopname_Search_name  

-- redo to ensure distinct email, this eliminates the names where one version has middle initial and the other doesn't
select max(bopname_Search_name) as name, email_Address as email
from arkona.ext_bopname
where bopname_company_number = 'RY8'
  and company_individ is not null
  and allow_contact_by_email <> 'N'
  AND email_address is not null
  and email_address not like 'NOEMAIL@%'
  and email_address not like '%NONE@%'
  and length(email_address) > 5
group by email_address  
order by name  