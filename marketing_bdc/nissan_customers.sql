﻿/*
07/02/21
Hey Jon,

Can you get me a list of all Nissan guests that have either purchased from us, or had service done with us?

Years 2010 and newer vehicles.

Would need the following info.

First Name
Last Name
Phone 1
Phone 2
Email
City
Year
Model
VIN

Justin Kilmer
*/
-- started out using the earlier queries in this file, but they no longer make sense to me
-- going with the queires from justin_18_months.sql
-- the key part is to use bopvref to weed out customers that no longer own the vehicles, whether sales or service
-- assuming 5 years unless i hear differently from justin

select * from arkona.ext_bopname limit 20
-- assume 5 years
drop table if exists sales cascade;
create temp table sales as
select distinct c.first_name, c.last_company_name as last_name,c.city, --c.address_1 as address,  c.state_code as state, c.zip_Code as zip,
  c.phone_number as home_phone, c.cell_phone, c.email_address as email , 
  d.year as year, --d.make, 
  d.model, 
  a.bopmast_vin as vin--, similarity(a.bopmast_search_name, c.bopname_search_name)
from arkona.ext_bopmast a
join arkona.ext_bopvref b on a.bopmast_vin = b.vin
  and b.end_date = 0 -- current
  and a.buyer_number = b.customer_key
--   and (a.buyer_number = b.customer_key or a.co_buyer_number = b.customer_key)
  and b.type_code in ('S','C') -- buyer or co-buyer
join arkona.ext_bopname c on a.buyer_number = c.bopname_record_key
  and c.company_individ is not null
join arkona.ext_inpmast d on a.bopmast_vin = d.inpmast_vin  
  and d.make = 'nissan'
  and d.year::integer >= 2010
where a.record_status = 'U'
  and a.sale_type in ('L','R')
  and a.date_capped between current_date - interval '5 years' and current_Date
  and similarity(a.bopmast_search_name, c.bopname_search_name) > .31;
create unique index on sales(vin,last_name);  

drop table if exists ros cascade;
create temp table ros as
-- select e.bopname_search_name as name, e.address_1 as address, e.city, e.state_code as state, e.zip_code as zip,
--   e.phone_number as phone, e.email_address as email,
--   b.model_year::integer as year, b.make, b.model, b.vin
select e.first_name, e.last_company_name as last_name, e.city, e.phone_number as home_phone, e.cell_phone, e.email_address as email,
	b.model_year::integer as year, b.model, b.vin
from dds.fact_repair_order a
join dds.dim_vehicle b on a.vehicle_key = b.vehicle_key
  and b.make = 'nissan'
  and b.model_year::integer >= 2010
join dds.dim_customer c on a.customer_key = c.customer_key
join arkona.ext_bopvref d on b.vin = d.vin
  and d.end_date = 0
  and d.type_code = 'C'
  and c.bnkey = d.customer_key
join arkona.ext_bopname e on d.customer_key = e.bopname_record_key
  and e.company_individ is not null  
where open_date between current_date - interval '5 years' and current_Date
group by e.first_name, e.last_company_name, e.city, e.phone_number, e.cell_phone, e.email_address,
	b.model_year::integer, b.model, b.vin;

select * 
from (
	select * from ros
	union 
	select * from sales) a
where last_name not like 'ENTERPRISE%'	
  and last_name not like 'DEALER%'
  and last_name not like 'EIDE%'
  and last_name not like 'FIVE%'
  and last_name not like 'HERTZ%'
  and last_name not like 'HONDA%'
  and last_name not like 'NATIONAL%'
  and last_name not like 'TWIN%'
order by last_name, first_name, vin;

--07/05/21 sent 2010+_nissan_guests.xlsx
      
/*
02/04/20, from justin
Hey Jon,

Can you pull me a list of all the guest that have had service done here or purchased a vehicle from us in the past 5 years?  Only Nissan vehicles.

First Name
Last Name
City
Phone Number 1
Phone Number 2
Email
Year
Model
VIN

*/

drop table if exists sales;
create temp table sales as
select a.bopmast_company_number, a.bopmast_search_name, a.buyer_number, a.date_capped, a.bopmast_vin, 
  b.year, b.model
from arkona.xfm_bopmast a
join arkona.xfm_inpmast b on a.bopmast_vin = b.inpmast_vin
  and b.current_row
  and b.make = 'nissan'
where a.current_row
  and a.record_status = 'U'
  and a.bopmast_search_name <> 'ADESA'
  and a.date_capped between (current_date - interval '5 years')::date and current_date;

/* BOPVREF */
BVTYPE (TYPE_CODE) Customer/Vehicle Reference
S = Buyer on the Deal
2 = Co-Buyer on the Deal
C = Service Customer
T = Trade In
4 = ?? (Of the 212409 rows in BOPVREF table, only 59 are type 4)

-- drop table if exists sales_2;
-- create temp table sales_2 as
-- select a.*, b.start_date, b.end_date,
--   case b.type_code
--     when 'S' then 'buyer'
--     when '2' then 'co-buyer'
--     when 'C' then 'service'
--     when 'T' then 'trade in'
--     else 'xxxxxx'
--   end as bopvref_type,
--   case when a.buyer_number = b.customer_key then 'cust_match' else null end as cust_match,
--   row_number() over (partition by a.bopmast_vin order by a.bopmast_vin, b.start_date desc)
-- from sales a
-- left join arkona.ext_bopvref b on a.bopmast_vin = b.vin;




drop table if exists bopvref;
create temp table bopvref as
select a.*, b.year, b.make, b.model
from arkona.ext_bopvref a
join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
  and b.make = 'nissan'
where a.end_date = 0 
  and a.type_code in ('S','C')
  and a.start_date > 20150000
  and a.vin not in ( -- exclude double vins
   select vin from (
      select a.*, b.year, b.make, b.model
      from arkona.ext_bopvref a
      join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
        and b.current_row
        and b.make = 'nissan'
      where a.end_date = 0 
        and a.type_code in ('S','C')
        and a.start_date > 20150000
   ) x group by vin having count(*) > 1);
create unique index on bopvref(vin);   
create index on bopvref(type_code);  
create index on bopvref(customer_key);  

drop table if exists wtf;
create temp table wtf as
select a.*, b.bopmast_company_number, b.bopmast_search_name, c.bopname_search_name, c.bopname_company_number
from bopvref a 
join sales b on a.vin = b.bopmast_vin
  and a.customer_key = b.buyer_number
left join arkona.xfm_bopname c on a.customer_key = c.bopname_record_key
  and c.current_row
where a.type_code = 'S';


select a.*, similarity(bopmast_search_name, bopname_search_name) 
from wtf a
order by similarity(bopmast_search_name, bopname_search_name) 



drop table if exists ros;
create temp table ros as
select distinct a.cust_name, a.customer_key,b.inpmast_vin, b.year, b.make, b.model
from arkona.ext_Sdprhdr a
join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
  and b.make = 'nissan'
where arkona.db2_integer_to_date(open_date) between (arkona.db2_integer_to_date(open_date) - interval '5 years')::date and current_date
  and a.cust_name not like '*VOID%'
  and a.cust_name not like 'INVENTORY%'

select *
from ros

select *
from bopvref a 
join ros b on a.vin = b.inpmast_vin
  and a.customer_key = b.customer_key
where a.type_code = 'C'
order by b.cust


--02/09/20 bopvref / bopname good enough 
/*
First Name
Last Name
City
Phone Number 1
Phone Number 2
Email
Year
Model
VIN
*/
drop table if exists customers;
create temp table customers as
select c.first_name, c.last_company_name as last_name, 
  c.phone_number as phone_number_1, c.business_phone as phone_number_2, c.email_address, a.year, a.model, a.vin
from bopvref a
join arkona.xfm_bopname c on a.customer_key = c.bopname_record_key
  and c.current_row
  and last_company_name not in ('ADESA','DEALER TRADE 2019','EIDE MOTORS','HERTZ CAR RENTAL',
    'HONDA NISSAN OF GRAND FORKS','AUTO FINANCE SUPER CENTER','AUTO FINANCE')
  and last_company_name not like ('ENTERPRISE%')
  and last_company_name not like ('DEALER%')
  and last_company_name not like ('EIDE%')
  and last_company_name not like ('TWIN CITY%')
  and last_company_name not like ('ALTRU%')
  and last_company_name not like ('MIDSTATE%')
  and last_company_name not like ('LITHIA%');

select *
from customers a
join (
  select vin
  from customers
  group by vin
  having count(*) > 1) b on a.vin = b.vin

-- get rid of 87 dup vins
delete from customers
where vin in (
  select vin
  from customers
  group by vin
  having count(*) > 1)


select last_name, count(*)
from customers
group by last_name   
order by last_name 


/*
Hey Jon, 

I hope you are doing well.  I was wondering if you could pull me a list of guest that have been to the dealership with a Nissan vehicle.  Let’s say anyone that has been in for service or purchased a Nissan from us in the past 5 years.  With the criteria as follows.

Name
Phone
Email
City
VIN
Year 
Model

Let me know if you have any questions.  Have a great day Jon!

03/25/20
Justin Kilmer
*/


select *
from customers a