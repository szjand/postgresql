﻿/*
10/24/2022
Subject: Toyota 100000

From Justin
	Hey Jon,
	I hope you are doing well.  Can you create me a list of Toyota’s that are between 95,000-115,000 miles that 
	have never had their coolant flushed?  Not sure if you can narrow it all the way down to Coolant Flush but 
	if you can get me the vehicles in that mileage range that would be awesome, and then we can check if 
	they’ve had the flush ourselves.  Just Toyota vehicles that have been here in the past 2 years.
	Name
	Number
	Phone 1
	Phone 2
	Email
	City
	Year
	Model
	VIN
Dayton:
	BGCFX is the only one I know of. Let me ask the other and make sure im not missing one.
Justin:
	Jon can we also create a list for Honda / Nissan’s as well?  Honda/Nissan within the same time frame (95,000-115,000 Miles) 
	that have not had a coolant flush done.  Dayton can you get the opecodes and pricing for Toyota?  Nick can you get the 
	opecodes and pricing for Honda and Nissan?
Nick:
	Labor op codes for Honda Nissan
	HWCF
	NWCF
	Cost is just shy of $200 (121.50 labor plus parts)
	
*/

decided, its time for a schema for call lists, i do enough of them and they are all done in temp tables,
create schema cl;
comment on schema cl is 'for bdc/departmental call lists';

/* BOPVREF */
BVTYPE (TYPE_CODE) Customer/Vehicle Reference
S = Buyer on the Deal
2 = Co-Buyer on the Deal
C = Service Customer
T = Trade In
4 = ?? (Of the 212409 rows in BOPVREF table, only 59 are type 4)

drop table if exists cl.coolant_flushes cascade;
create table cl.coolant_flushes (
  name citext not null,
  the_date date,
  description citext);
insert into cl.coolant_flushes values ('coolant flush', current_date,
'From Justin
	Hey Jon,
	I hope you are doing well.  Can you create me a list of Toyota’s that are between 95,000-115,000 miles that 
	have never had their coolant flushed?  Not sure if you can narrow it all the way down to Coolant Flush but 
	if you can get me the vehicles in that mileage range that would be awesome, and then we can check if 
	they’ve had the flush ourselves.  Just Toyota vehicles that have been here in the past 2 years.
	Name
	Number
	Phone 1
	Phone 2
	Email
	City
	Year
	Model
	VIN
Dayton:
	BGCFX is the only one I know of. Let me ask the other and make sure im not missing one.
Justin:
	Jon can we also create a list for Honda / Nissan’s as well?  Honda/Nissan within the same time frame (95,000-115,000 Miles) 
	that have not had a coolant flush done.  Dayton can you get the opecodes and pricing for Toyota?  Nick can you get the 
	opecodes and pricing for Honda and Nissan?
Nick:
	Labor op codes for Honda Nissan
	HWCF
	NWCF
	Cost is just shy of $200 (121.50 labor plus parts)');

-- first, ros in the last 2 years for 95 - 115k vehicles
-- yikes, don't have the history on toyota
-- populated arkona.ext_sdprhdr_toyota_history & ext_sdprdet_toyota_history, full scrapes thru today, 10/27/22
-- will need to do ttoyota separately and in 2 parts, toyota_history & fact_repair_order

create index on arkona.ext_sdprhdr_toyota_history(ro_number);
create index on arkona.ext_sdprhdr_toyota_history(customer_key);
create index on arkona.ext_sdprhdr_toyota_history(vin);
create index on arkona.ext_sdprdet_toyota_history(ro_number);
select * from arkona.ext_sdprhdr_toyota_history where open_date = 20201027
select * from arkona.ext_bopname where bopname_record_key = 1023549
select * from arkona.ext_inpmast where inpmast_vin = 'JF1GPAA67GH309906'
select * from arkona.ext_sdprdet_toyota_history where labor_operation_code = 'BGCFX' limit 10

/*
need to add the store, but it needs to be the store from bopname
close to end of the day on 10/28, give up on trying to match bopname_search_name, just go with store and bopname_record_key
*/
-- toyota vehicle that have had the coolant flush done in the last 2 years
-- on second thought, don't limit it to 2 years, any vehicle that has had the flush done. (period)
drop table if exists cl.coolant_done_ty cascade;
create unlogged table cl.coolant_done_ty as
select *
from (-- fact repair order, vehicles that have had the coolant flush
	select b.vin
	from dds.fact_repair_order a
	join dds.dim_vehicle b on a.vehicle_key = b.vehicle_key
		and b.make in ('toyota')
	join dds.dim_opcode d on a.opcode_key = d.opcode_key
		and d.opcode in ('BGCFX')  
group by b.vin
union -- sdprhdr, vehicles that have had the coolant flush
	select a.vin
	from arkona.ext_sdprhdr_toyota_history a
	join arkona.ext_sdprdet_toyota_history b on a.ro_number = b.ro_number
		and b.labor_operation_code = 'BGCFX'
	group by a.vin) aa;
comment on table cl.coolant_done_ty is 'toyota vins from dds.fact_repair_order that have received a coolant flush';
alter table cl.coolant_done_ty add primary key(vin);

-- rydell toyota
drop table if exists cl.coolant_ty_step_1 cascade;
create unlogged table cl.coolant_ty_step_1 as
select * 
from (
	select d.bopname_company_number as store, a.ro, a.open_date, a.miles, b.model_year, b.make, b.model, b.vin, c.full_name, c.bnkey,  c.city, c.home_phone, c.cell_phone, c.email,
		row_number() over (partition by full_name, vin order by open_date desc) as seq
	from dds.fact_repair_order a
	join dds.dim_vehicle b on a.vehicle_key = b.vehicle_key
		and b.make = 'toyota'
	join dds.dim_customer c  on a.customer_key = c.customer_key
		and c.bnkey <> 0
		and c.full_name not in ('.','1','UNKNOWN','ND DOT','COURTESY DELIVERY','ENTERPRISE','STATE OF MINNESOTA')	
		and c.full_name not similar to '%(AVIS|FINANCE|LITHIA|T1|BODY|RYDELL|ADESA|COLLISION|ABRA|AUCTION|BMW|CHEVROLET|CORWIN|EAN HOLDINGS|ENTERPRISE|GRAND FORKS|MANHEIM|MERCEDES BENZ)%'
	join arkona.ext_bopname d on c.full_name = d.bopname_search_name
	  and c.bnkey = d.bopname_record_key
	where a.open_date > (current_date - interval '2 years')::date
	  and a.miles between 95000 and 115000
		and not exists ( -- exclude vehicles that had the flush done 
			select 1
			from cl.coolant_done_ty
			where vin = b.vin)
	group by d.bopname_company_number, a.ro, a.open_date, a.miles, b.model_year, b.make, b.model, b.vin, c.full_name, c.bnkey, c.city, c.home_phone, c.cell_phone, c.email) aa
where seq = 1;
comment on table cl.coolant_ty_step_1 is 'the most recent visit of toyotas in the past 2 years that have not had a coolant flush from dds.fact_repair_order';
alter table cl.coolant_ty_step_1 add primary key(full_name, vin);



-- lithia toyota
-- 136744, 112987, 102901, 14136, 13963, 456
-- select * from arkona.ext_sdprhdr_toyota_history where open_date > 20221001 limit 10
drop table if exists cl.coolant_ty_step_2 cascade;
create unlogged table cl.coolant_ty_step_2 as
select * 
from (
	select d.bopname_company_number as store, a.ro_number as ro, arkona.db2_integer_to_date(a.open_date) as open_date, a.odometer_in as miles, c.year::citext, c.make, c.model, a.vin,
		d.bopname_search_name as full_name, d.bopname_record_key, d.city, d.phone_number::citext, d.cell_phone::citext, d.email_address,
		row_number() over (partition by d.bopname_search_name, a.vin order by arkona.db2_integer_to_date(a.open_date) desc) as seq
	-- select count(*)
	from arkona.ext_sdprhdr_toyota_history a
	join arkona.ext_sdprdet_toyota_history b on a.ro_number = b.ro_number
	join arkona.ext_inpmast c on a.vin = c.inpmast_vin
		and c.inpmast_company_number = 'RY8'
		and c.make = 'toyota'
	join arkona.ext_bopname d on a.company_number  = d.bopname_company_number
		and a.customer_key = d.bopname_record_key 
		and d.bopname_search_name not in ('.','1','UNKNOWN','ND DOT','COURTESY DELIVERY','ENTERPRISE','STATE OF MINNESOTA')	
		and d.bopname_search_name not similar to '%(AVIS|FINANCE|LITHIA|T1|BODY|RYDELL|ADESA|COLLISION|ABRA|AUCTION|BMW|CHEVROLET|CORWIN|EAN HOLDINGS|ENTERPRISE|GRAND FORKS|MANHEIM|MERCEDES BENZ)%'
	where arkona.db2_integer_to_date(a.open_date) > (current_date - interval '2 years')::date
	  and a.odometer_in between 95000 and 115000
		and length(a.vin) = 17
		and not exists (
			select 1
			from cl.coolant_done_ty
			where vin = a.vin)
	group by d.bopname_company_number, a.ro_number, arkona.db2_integer_to_date(a.open_date), a.odometer_in, c.year::citext, c.make, c.model, a.vin,
		d.bopname_search_name, d.bopname_record_key, d.city, d.phone_number::citext, d.cell_phone::citext, d.email_address) aa    
where seq = 1;
comment on table cl.coolant_ty_step_2 is 'the most recent visit of toyotas in the past 2 years that have not had a coolant flush from arkona.ext_sdprhdr_toyota_history,
  the toyota history imported from lithia has not been processed into dds.fact_repair_order';
alter table cl.coolant_ty_step_2 add primary key(full_name, vin) 

select full_name, vin
from (
select * from cl.coolant_ty_step_1
union
select * from cl.coolant_ty_step_2) a
group by full_name, vin
having count(*) > 1

drop table if exists cl.coolant_ty_step_3 cascade;
create unlogged table cl.coolant_ty_step_3 as
select * 
	from (
		select *, row_number() over (partition by full_name, vin order by open_date desc) as seq_2
		from (
			select * from cl.coolant_ty_step_1
			union
			select * from cl.coolant_ty_step_2) a) b
where seq_2 = 1;
comment on table cl.coolant_ty_step_3 is 'combined data of cl.coolant_ty_step_1 and cl.coolant_ty_step_2';
alter table cl.coolant_ty_step_3 add primary key(full_name, vin);

-- whew, this knocks it down to 88 from 615 total rows
select *
from cl.coolant_ty_step_3 a
join arkona.ext_bopvref b on a.vin = b.vin
  and a.bnkey = b.customer_key
  and a.open_date = arkona.db2_integer_to_date(b.start_date)
  and a.store = b.company_number
  and b.end_date = 0
  and b.type_code = 'C'
order by a.full_name, a.vin  

select a.full_name, a.bnkey, a.vin, a.open_date, b.company_number, b.customer_key, b.start_date, b.end_date, b.type_code,
  case when c.full_name is not null then 'MATCHES' else null end
from cl.coolant_ty_step_3 a
left join arkona.ext_bopvref b on a.vin = b.vin
left join (
	select a.*
	from cl.coolant_ty_step_3 a
	join arkona.ext_bopvref b on a.vin = b.vin
		and a.bnkey = b.customer_key
		and a.open_date = arkona.db2_integer_to_date(b.start_date)
		and a.store_code = b.company_number
		and b.end_date = 0
		and b.type_code = 'C') c on a.full_name = c.full_name and a.vin = c.vin
order by a.vin, b.start_date


select b.bopname_company_number, a.* from cl.coolant_ty_step_3 a
left join arkona.ext_bopname b on a.full_name = b.bopname_search_name and a.bnkey = b.bopname_record_key
where a.store_code <> b.bopname_company_number

-- 10/29/222 need to explore alternative means of using bopvref, 

select b.full_name, a.*
from arkona.ext_bopvref a
join cl.coolant_ty_step_3 b on a.vin = b.vin
  and b.bnkey = a.customer_key
  and a.company_number = b.store
order by b.full_name, a.start_date  

-- join just on the vin,  add bopname to bopvref
-- one thing i am learning, is that bopvref is not complete, missing pdq, body shop
-- add end date to row number, bartz, philip, 2 rows for max date, one with end one without, want to ensure end date is include if max start
-- leaning toward if the last open interaction matches the customer key, that is good enough
select * from (
select -- a.store, a.open_date, a.full_name, a.bnkey, 
	b.vin, b.start_date, b.end_date, b.customer_key,
  row_number() over (partition by b.vin order by b.start_date desc, b.end_date desc) as seq
from cl.coolant_ty_step_3 a
join arkona.ext_bopvref b on a.vin = b.vin) aa 
where seq = 1
-- order by b.vin, b.start_date, seq desc

-- now, this returns 478 rows, will send this to justin
-- 10/29/22
drop table if exists cl.coolant_ty_step_4 cascade;
create unlogged table cl.coolant_ty_step_4 as
select a.full_name as name, a.home_phone, a.cell_phone, a.email, a.city, a.model_year as year, a.model, a.vin
from cl.coolant_ty_step_3 a
join (
	select * 
	from (
		select -- a.store, a.open_date, a.full_name, a.bnkey, 
			b.vin, b.start_date, b.end_date, b.customer_key, b.company_number,
			row_number() over (partition by b.vin order by b.start_date desc, b.end_date desc) as seq
		from cl.coolant_ty_step_3 a
		join arkona.ext_bopvref b on a.vin = b.vin) aa 
	where seq = 1) bb on a.vin = bb.vin
	  and a.bnkey = bb.customer_key
	  and a.store = bb.company_number
order by a.full_name, a.vin;
comment on table cl.coolant_ty_step_4 is 'final toyota coolant flush data to send to BDC, formated for spreadsheet';
alter table cl.coolant_ty_step_4 add primary key(name, vin);

select * 
from cl.coolant_ty_step_4
where (home_phone <> '0' or cell_phone <> '0') 

/****************************************************************************************************************/
/****************************************************************************************************************/
-- well, now that toyota is well and fucked up, lets try hn, at least it will all be in fact_repair_order


drop table if exists cl.coolant_done_hn cascade;
create unlogged table cl.coolant_done_hn as
select *
from (-- fact repair order, vehicles that have had the coolant flush
	select b.vin
	from dds.fact_repair_order a
	join dds.dim_vehicle b on a.vehicle_key = b.vehicle_key
		and b.make in ('honda','nissan')
	join dds.dim_opcode d on a.opcode_key = d.opcode_key
		and d.opcode in ('HWCF','NWCF')) aa 
group by vin;
comment on table cl.coolant_done_hn is 'honda & nissan vins from dds.fact_repair_order that have received a coolant flush';
alter table cl.coolant_done_hn add primary key(vin);

drop table if exists cl.coolant_hn_step_1 cascade;
create unlogged table cl.coolant_hn_step_1 as
select * 
from (
	select d.bopname_company_number as store, a.ro, a.open_date, a.miles, b.model_year, b.make, b.model, b.vin, c.full_name, c.bnkey,  c.city, c.home_phone, c.cell_phone, c.email,
		row_number() over (partition by full_name, vin order by open_date desc) as seq
	from dds.fact_repair_order a
	join dds.dim_vehicle b on a.vehicle_key = b.vehicle_key
		and b.make in ('honda','nissan')
	join dds.dim_customer c  on a.customer_key = c.customer_key
		and c.bnkey <> 0
		and c.full_name not in ('.','1','UNKNOWN','ND DOT','COURTESY DELIVERY','ENTERPRISE','STATE OF MINNESOTA')	
		and c.full_name not similar to '%(AVIS|FINANCE|LITHIA|T1|BODY|RYDELL|ADESA|COLLISION|ABRA|AUCTION|BMW|CHEVROLET|CORWIN|EAN HOLDINGS|ENTERPRISE|GRAND FORKS|MANHEIM|MERCEDES BENZ|ALTRU)%'
	join arkona.ext_bopname d on c.bnkey = d.bopname_record_key
	  and c.full_name = d.bopname_search_name
	where a.open_date > (current_date - interval '2 years')::date
	  and a.miles between 95000 and 115000
		and not exists ( -- exclude vehicles that had the flush done 
			select 1
			from cl.coolant_done_hn
			where vin = b.vin)
	group by d.bopname_company_number, a.ro, a.open_date, a.miles, b.model_year, b.make, b.model, b.vin, c.full_name, c.bnkey, c.city, c.home_phone, c.cell_phone, c.email) aa
where seq = 1;
comment on table cl.coolant_hn_step_1 is 'the most recent visit of hondas and nissans in the past 2 years that have not had a coolant flush from dds.fact_repair_order';
alter table cl.coolant_hn_step_1 add primary key(full_name, vin);

select * from cl.coolant_hn_step_1 order by full_name desc

-- now, this returns 653 rows, will send this to justin
-- 10/29/22
drop table if exists cl.coolant_hn_step_2 cascade;
create unlogged table cl.coolant_hn_step_2 as
select a.full_name as name, a.home_phone, a.cell_phone, a.email, a.city, a.model_year as year, a.make, a.model, a.vin
from cl.coolant_hn_step_1 a
join (
	select * 
	from (
		select b.vin, b.start_date, b.end_date, b.customer_key, b.company_number,
			row_number() over (partition by b.vin order by b.start_date desc, b.end_date desc) as seq
		from cl.coolant_hn_step_1 a
		join arkona.ext_bopvref b on a.vin = b.vin) aa 
	where seq = 1) bb on a.vin = bb.vin
	  and a.bnkey = bb.customer_key
	  and a.store = bb.company_number
order by a.full_name, a.vin;
comment on table cl.coolant_hn_step_2 is 'final honda nissan coolant flush data to send to BDC, formated for spreadsheet';
alter table cl.coolant_hn_step_2 add primary key(name, vin);

select * 
from cl.coolant_hn_step_2
where (home_phone <> '0' or cell_phone <> '0') 



/* the bopname rabbit hole */

select distinct bopname_search_name from arkona.ext_bopname where company_individ is null

select a.cust_Name, a.customer_key, b.bopname_company_number 
from arkona.ext_sdprhdr a
left join arkona.ext_bopname b on a.cust_name = b.bopname_search_name
  and a.customer_key = b.bopname_record_key
  and b.company_individ is not null
where left(ro_number, 1) = '2'
  and b.bopname_company_number is null
  and a.customer_key <> 0

RY1,16491652,2021-11-03,112856,2008,HONDA,ACCORD,JHMCP268X8C063827,AASAND, HARVEY,1039255,DRAYTON,7013601769,7013601769,dmaasand@gmail.com,1
RY2,2835650,2021-03-11,96206,2016,HONDA,CIVIC,19XFC1F78GE034278,ABBAS, SHAWN ADAM,1160596,PORTLAND,7012136722,7012136722,,1

select * from arkona.ext_bopname where bopname_record_key in (1039255,1160596)

select count(distinct bopname_search_name), sum(the_count)
from (
select bopname_search_name, count(*) as the_count
from arkona.ext_bopname
where bopname_company_number in ('RY1','RY2','RY8')
  and company_individ is not null
group by bopname_search_name
having count(*) > 1) a
order by count(*) desc


select * 
from arkona.xfm_bopname a
where row_from_date = current_date - 1
  and company_individ is not null
  and exists (
    select 1
    from arkona.ext_bopname
    where bopname_search_name = a.bopname_search_name
      and bopname_record_key <> a.bopname_record_key)
limit 10

RY1,1066937,I,1064172,SHEGGERUD, VIRGIL
select * from arkona.ext_bopname where bopname_search_name = 'SHEGGERUD, VIRGIL'

select * from arkona.ext_sdprhdr where customer_key = 1066937
select * from dds.dim_Service_writer where writer_number = '439'

select * from arkona.xfm_bopname where bopname_record_key = 1066937

select bopname_record_key, row_number() over (partition by bopname_record_key order by row_from_date)

drop table if exists jon.new_keys cascade;
create unlogged table jon.new_keys as
select bopname_record_key
from arkona.xfm_bopname a
where row_from_date = current_date - 1
  and (
    select count(*) 
    from arkona.xfm_bopname
    where bopname_record_key = a.bopname_record_key) = 1;
comment on table jon.new_keys is 'new bopname keys created on 10/28/22';  
alter table jon.new_keys add primary key(bopname_record_key);

select a.*, b.bopname_search_name, b.address_1, c.*
from jon.new_keys a
join arkona.xfm_bopname b on a.bopname_record_key = b.bopname_record_key  
  and b.company_individ is not null
left join arkona.ext_bopname c on b.bopname_search_name = c.bopname_search_name
  and c.company_individ is not null
where a.bopname_record_key <> c.bopname_record_key  
order by a.bopname_record_key

/* the bopname rabbit hole */  
    
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------

select * 
-- select count(*) -- 100377, 72.792
from arkona.ext_bopvref a
join arkona.ext_inpmast b on a.vin = b.inpmast_vin
  and b.make in ('toyota','honda','nissan')
where type_code = 'C'
  and end_date = 0
limit 10

select * from dds.dim_customer limit 10

select count(*) from (  -- 86411
select a.ro, a.open_date, a.miles, b.model_year, b.make, b.model, c.full_name, c.bnkey,  c.home_phone, c.cell_phone, c.email
from dds.fact_repair_order a
join dds.dim_vehicle b on a.vehicle_key = b.vehicle_key
  and b.make in ('chevrolet','gmc','cadillac','buick','ford','dodge','ram')
join dds.dim_customer c  on a.customer_key = c.customer_key
  and c.bnkey <> 0
where a.open_date > current_date - 730
group by a.ro, a.open_date, a.miles, b.model_year, b.make, b.model, c.full_name, c.bnkey, c.home_phone, c.cell_phone, c.email
) x

select count(*) from (
select a.ro, a.open_date, a.miles, b.model_year, b.make, b.model, c.full_name, c.bnkey,  c.home_phone, c.cell_phone, c.email, d.*
from dds.fact_repair_order a
join dds.dim_vehicle b on a.vehicle_key = b.vehicle_key
  and b.make in ('chevrolet','gmc','cadillac','buick','ford','dodge','ram')
join dds.dim_customer c  on a.customer_key = c.customer_key
  and c.bnkey <> 0
join arkona.ext_bopvref d on b.vin = d.vin
  and c.bnkey = d.customer_key
  and c.type_code = 
  and d.end_date = 0
  and dds.db2_integer_to_date(d.start_date) > '10/01/2020'
where a.open_date > '10/01/2020'
) x


-- this gives me the ability to isolate the most recent visit 
drop table if exists step_1 cascade;
create temp table step_1 as
select a.ro, a.open_date, a.miles, b.model_year, b.make, b.model, b.vin, c.full_name, c.bnkey,  c.home_phone, c.cell_phone, c.email,
  row_number() over (partition by full_name, vin order by open_date desc) as seq
from dds.fact_repair_order a
join dds.dim_vehicle b on a.vehicle_key = b.vehicle_key
  and b.make in ('chevrolet','gmc','cadillac','buick','ford','dodge','ram')
join dds.dim_customer c  on a.customer_key = c.customer_key
  and c.bnkey <> 0
  and full_name not in ('.','1','UNKNOWN','RYDELL AUTO CENTER','AVIS','ND DOT','RYDELL TOYOTA OF GRAND FORKS','CITY OF GRAND FORKS','COURTESY DELIVERY','ENTERPRISE')
where a.open_date > '10/01/2020'
group by a.ro, a.open_date, a.miles, b.model_year, b.make, b.model, b.vin, c.full_name, c.bnkey, c.home_phone, c.cell_phone, c.email

-- the most recent visit for customer/vin only
drop table if exists step_2 cascade;
create temp table step_2 as
select * 
from step_1
where seq = 1;

delete from step_2 where full_name like 'rydell%';

-- remove the large count full_names and impose the mileage restriction
drop table if exists step_3 cascade;
create temp table step_3 as
select a.*
from step_2 a
left join (
	select full_name, count(*)
	from (
		select full_name
		from step_2 
		where seq = 1) a
	group by full_name 
	having count(*) > 5) b on a.full_name = b.full_name
where b.full_name is null
  and a.miles between 45000 and 110000;

/*
-- step_3 is only the most recent visit for any customer/vin
select full_name, vin
from step_3
group by full_name, vin
having count(*) > 1

select vin, count(*)
from step_3
group by vin
having count(*) > 1
order by count(*) desc

select * from step_3 where vin = '1GKS2MEF7ER218449' order by open_date
*/

next is to use bopvref to assert current ownership
ok, this gives me 1785 rows, reasonable start, even though i know there is junk data in here,
but that is unavoidable
this is assuming the ro open date = bopvref.start_date
select a.* 
from step_3 a
join arkona.ext_bopvref b on a.vin = b.vin
  and a.bnkey = b.customer_key
  and a.open_date = arkona.db2_integer_to_date(b.start_date)
  and b.end_date = 0
  and b.type_code = 'C'
where a.vin not in ('1D7RV1GTXAS198712')  
order by a.full_name, a.vin  

this is good enough format it for spreadsheet and send it to andrew

select a.full_name, a.home_phone, a.cell_phone, a.email, open_date as last_visit, a.vin, model_year, make, model, miles
-- select *
from step_3 a
join arkona.ext_bopvref b on a.vin = b.vin
  and a.bnkey = b.customer_key
  and a.open_date = arkona.db2_integer_to_date(b.start_date)
  and b.end_date = 0
  and b.type_code = 'C'
where a.vin not in ('1D7RV1GTXAS198712')  
order by a.full_name, a.vin  
-- limit 10