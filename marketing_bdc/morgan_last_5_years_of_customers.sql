﻿/*
4/15/22
Hi Jon and Afton,

Are we able to pull all the email address we have for people that have done business with us in the last 5 years.  This would include Sales, Service, Detail, and Bodyshop.

Would I be able to get the below information?

•	First Name
•	Last Name
•	Email
•	City
•	State
•	Last Date of Business (optional)

Thank you,
Morgan
*/

drop table if exists deals;
create temp table deals as
select b.email_address, b.first_name, b.last_company_name, 
  b.city, b.state_code as state, max(a.date_capped) as the_date
from arkona.ext_bopmast a
join arkona.ext_bopname b on a.bopmast_company_number = b.bopname_company_number
  and a.buyer_number = b.bopname_record_key
  and b.company_individ is not null
  and b.last_company_name not in ('.','1')
  and b.email_address is not null
  and b.email_address not like '%wng%'
  and b.email_address <> 'n/a'
where a.date_capped > ( select current_date - interval '5 years')::date
  and a.sale_type <> 'W'
  and coalesce(a.record_status, 'xxx') = 'U'
group by b.email_address,b.first_name, b.last_company_name, 
  b.city, b.state_code
order by b.last_company_name;

delete from deals where email_address like '%dnh%';
delete from deals where email_address not like '%@%';
delete from deals where length(email_address) < 12;




drop table if exists ros;
create temp table ros as
select b.email_address, b.first_name, b.last_company_name, 
  b.city, b.state_code as state, max(a.the_date) as the_date
from (
	select company_number, customer_key, max(arkona.db2_integer_to_date(close_date)) as  the_date
	from arkona.ext_sdprhdr 
	where arkona.db2_integer_to_date(close_date) > ( select current_date - interval '5 years')::date
		and customer_key <> 0
		and company_number in ('RY1','RY2')
	group by company_number, customer_key  
	union
	select company_number, customer_key, max(arkona.db2_integer_to_date(close_date)) as  close_date
	from arkona.ext_sdprhdr_history 
	where arkona.db2_integer_to_date(close_date) > ( select current_date - interval '5 years')::date
		and customer_key <> 0
		and company_number in ('RY1','RY2')
	group by company_number, customer_key) a
join arkona.ext_bopname b on a.company_number = b.bopname_company_number
  and a.customer_key = b.bopname_record_key
  and b.company_individ is not null
  and b.last_company_name not in ('.','1')
  and b.email_address is not null
  and b.email_address not like '%wng%'
  and b.email_address <> 'n/a'
-- where a.date_capped > ( select current_date - interval '5 years')::date
--   and a.sale_type <> 'W'
--   and coalesce(a.record_status, 'xxx') = 'U'
group by b.email_address,b.first_name, b.last_company_name, 
  b.city, b.state_code
order by b.last_company_name;

delete from ros where email_address not like '%@%';
delete from ros where length(email_address) < 12;
delete from ros where email_address like '%dnh%';

drop table if exists the_results;
create temp table the_results as
select first_name, last_company_name, email_address as email, city, state, the_date as last_transaction, source as type,
	CASE 
		when email_address is null then false
		WHEN length(email_address) = 0 THEN False
		WHEN position('@' IN email_address) = 0 THEN False
		WHEN position ('.' IN email_address) = 0 THEN False
		WHEN position('wng' IN email_address) > 0 THEN False
		WHEN position('dnh' IN email_address) > 0 THEN False
		WHEN position('noemail' IN email_address) > 0 THEN False
		WHEN position('none@' IN email_address) > 0 THEN False
		WHEN position('dng' IN email_address) > 0 THEN False
		ELSE True
	END AS email_valid
from (
	select *, row_number() over (partition by email_address, first_name, last_company_name, city, state order by the_date desc) as seq 
	from (
	select 'deal' as source, * from deals
	union
	select 'service' as service, * from ros
	order by last_company_name, first_name, email_address) a) b
where seq = 1
  and the_date < current_date;

delete 
from the_results
where not email_valid;


-- dups look like different cities
select a.* 
from the_results a
join (
select first_name, last_company_name, email
from the_results
group by first_name, last_company_name, email
having count(*) > 1) b on a.first_name = b.first_name
  and a.last_company_name = b.last_company_name
  and a.email = b.email


-- this is what i sent to morgan (and afton)
select first_name, last_company_name, email, city, state, last_transaction, type 
from the_results 
order by last_transaction desc, last_company_name asc, first_name asc
limit 100



-- seems like deals are under represented
-- so, here are deals from 3/31
-- nah don't think it is a real problem, 4/15 and 3/31 match
select * 
from ( -- deals
	select b.email_address, b.first_name, b.last_company_name, 
		b.city, b.state_code as state, max(a.date_capped) as the_date
	from arkona.ext_bopmast a
	join arkona.ext_bopname b on a.bopmast_company_number = b.bopname_company_number
		and a.buyer_number = b.bopname_record_key
		and b.company_individ is not null
		and b.last_company_name not in ('.','1')
		and b.email_address is not null
		and b.email_address not like '%wng%'
		and b.email_address <> 'n/a'
	where a.date_capped = '03/31/2022'
		and a.sale_type <> 'W'
		and coalesce(a.record_status, 'xxx') = 'U'
	group by b.email_address,b.first_name, b.last_company_name, 
		b.city, b.state_code) a 
left join the_results b  on coalesce(a.first_name, 'xxx') = coalesce(b.first_name, 'xxx')
  and a.last_company_name = b.last_company_name
  and a.email_address = b.email


for example
alexis.rodriguez1320@gmail.com has a deal on 3/31
select * from the_results where email = 'alexis.rodriguez1320@gmail.com'
but
select * from arkona.ext_bopname where email_address = 'alexis.rodriguez1320@gmail.com'

select * from arkona.ext_sdprhdr where customer_key in (1103852,1103853)
also had a service visit on 4/1

this is about the customer, not the vehicle and the most recent transaction for the customer



