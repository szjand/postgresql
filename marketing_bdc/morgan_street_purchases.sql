﻿/*
06/16/21
morgan wants the customers name if possible
it exists in ads.ext_vehicle_item_owner_info for purchases that are done as a trade, but not those done as an auction

did a one time scrape of that table, if this continues to be a requirement, that table will have to be updated
*/

select * from ads.ext_vehicle_item_owner_info limit 100

-- first redo may
select distinct inpmast_stock_number as "stock #", inpmast_vin as vin, 
	arkona.db2_integer_to_date(date_in_invent) as "date acquired", c.last_name ||', '|| c.first_name as owner
from arkona.xfm_inpmast a
left join ads.ext_vehicle_items b on a.inpmast_vin = b.vin
left join ads.ext_vehicle_item_owner_info c on b.vehicleitemid = c.vehicle_item_id
  and arkona.db2_integer_to_date(date_in_invent) between c.from_ts:: date and coalesce(c.thru_ts::date, '12/31/9999')
where arkona.db2_integer_to_date(date_in_invent) between '05/01/2021' and '05/31/2021'
  and right(inpmast_stock_number, 1) = 'P'

-- and june
select distinct inpmast_stock_number as "stock #", inpmast_vin as vin, 
	arkona.db2_integer_to_date(date_in_invent) as "date acquired", c.last_name ||', '|| c.first_name as owner
from arkona.xfm_inpmast a
left join ads.ext_vehicle_items b on a.inpmast_vin = b.vin
left join ads.ext_vehicle_item_owner_info c on b.vehicleitemid = c.vehicle_item_id
  and arkona.db2_integer_to_date(date_in_invent) between c.from_ts:: date and coalesce(c.thru_ts::date, '12/31/9999')
where arkona.db2_integer_to_date(date_in_invent) between '06/01/2021' and '06/30/2021'
  and right(inpmast_stock_number, 1) = 'P'


  
/*
06/01/21
Hi Afton and Jon,

I cannot remember who sent me the previous report from 2019, but could one of you send me the list of vehicles that we purchased “off the street” in May 2021.  These stock numbers will end with a P.

Thank you,
Morgan 
*/
select distinct inpmast_stock_number, inpmast_vin, arkona.db2_integer_to_date(date_in_invent)
from arkona.xfm_inpmast
where arkona.db2_integer_to_date(date_in_invent) between '01/01/2019' and '12/31/2019'
  and right(inpmast_stock_number, 1) = 'P'

select distinct inpmast_stock_number as "stock #", inpmast_vin as vin, 
	arkona.db2_integer_to_date(date_in_invent) as "date acquired"
from arkona.xfm_inpmast
where arkona.db2_integer_to_date(date_in_invent) between '05/01/2021' and '05/31/2021'
  and right(inpmast_stock_number, 1) = 'P'