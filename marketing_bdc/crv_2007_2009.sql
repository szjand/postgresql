﻿/*
Hey Jon,

Hope you are doing well today.  Can you get me a list of all the 2007-2009 Honda CR-V’s in our DMS System.
Name, number, Email, city, VIN, year and Model.  No huge rush if you are tied up with some other stuff.

Thanks!

Justin Kilmer
*/
drop table if exists crv cascade;
create temp table crv as
select inpmast_vin as vin, year as model_year, model
from arkona.xfm_inpmast a
where a.current_row
  and a.year between 2007 and 2009
  and a.make = 'honda'
  and a.model like '%CR-V%'
  and length(a.inpmast_vin) = 17;
create unique index on crv(vin);

-- most recent customer assignment
drop table if exists bopvref cascade;
create temp table bopvref as --484
select *
from (
  select b.vin, b.customer_key, b.start_date, b.end_date, b.type_code, b.salesperson,
    row_number() over (partition by b.vin order by b.start_date desc)
  from crv a
  join arkona.ext_bopvref b on a.vin = b.vin
    and b.type_code <> '2') c  -- eliminates cobuyer
where row_number = 1;
create unique index on bopvref(vin);


----------------------------------------------------------------------
--< multiple bopname records for customer_key
----------------------------------------------------------------------
-- cull out the multiple name records
drop table if exists dups;
create temp table dups as
select a.vin
from bopvref a
join arkona.xfm_bopname b on a.customer_key = b.bopname_Record_key
  and b.current_row
group by a.vin
having count(*) > 1;  

-- ok, these are easy when one is a customer and one is a bank, but what about when we have 2 customers, 5J6RE48569L039827 / 1000341
-- lets put the name key from inpmast into the mix, shit, that does nothing
select a.*, c.bopname_search_name, c.bopname_company_number, c.company_individ, c.optional_field
from bopvref a
join dups b on a.vin = b.vin
join arkona.xfm_bopname c on a.customer_key = c.bopname_record_key
  and c.current_row



select * 
from arkona.xfm_bopname 
where bopname_record_key = 1002843


select * 
from arkona.xfm_bopname 
where bopname_record_key = 1001710

select * 
from arkona.xfm_bopname 
where bopname_record_key = 1136499

-- lets try this, the must recently updated

select a.bopname_record_key, a.bopname_search_name, a.phone_number, a.email_address, a.city, a.timestamp_updated, b.vin, 
  row_number() over (partition by a.bopname_record_key order by a.timestamp_updated desc)
from arkona.xfm_bopname a
join bopvref b on a.bopname_record_key = b.customer_key
order by a.bopname_record_key

1136499 - JHLRE48759C000950  makes not sense, most recently updated in bopname shows ALLY (updated 4/16/19), dealercrap shows mccowan (cash deal on 4/2/19)
in bopname, optional_field shows: BOPTRAD   1C4RJFBG2FC744028

ok, this is not rational, but for this subset of dups, in each case, the invalid dup has a value in optional_field
and in open track, OptionalField has no definition
so, in spite of rationality, lets assume this is a valid observation

-- and now, there are no dups
-- cull out the multiple name records
select a.vin
from bopvref a
join arkona.xfm_bopname b on a.customer_key = b.bopname_Record_key
  and b.current_row
  and b.optional_field is null
group by a.vin
having count(*) > 1;  

----------------------------------------------------------------------
--/> multiple bopname records for customer_key
----------------------------------------------------------------------

-- and this is the final for justin, sort multiple ways to scan for obvious bad records
select c.bopname_search_name, c.phone_number, c.email_address, c.city,
  a.vin, b.model_year, b.model
from bopvref a
join crv b on a.vin = b.vin
join arkona.xfm_bopname c on a.customer_key = c.bopname_record_key
  and c.current_row
  and c.optional_field is null
where c.bopname_search_name not like '%DEALER TRADE%'  
  and c.bopname_search_name not like 'ernst%'
  and c.bopname_search_name not in ('FISHER MOTORS','HARR MOTORS')
  and c.bopname_search_name not like '%HONDA%'
  and c.bopname_search_name not like 'SATURN%'
  and c.bopname_search_name not like '%AUCTION%'
  and c.bopname_search_name not like 'VOLKS%'
order by c.bopname_search_name  


