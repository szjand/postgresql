﻿/*
Hey Jon,

I’m wondering if you can create a list of guest that have purchased a maintenance plan from us that haven’t had an open repair order in the past 4 months?  Do you think that is possible?  

First Name
Last Name
Phone Number
Email
City
Year
Make
Model

Justin Kilmer
06/30

justin: Can you differentiate between service contracts and maintenance plans?

justin 7/2:
  Hey Jon,
  I hope this isn’t too much to ask but can I have you make a couple different list?  Sorry the list I had you working on has morphed into this.h
  GM Store Guest with a non expired service contract
  GM store Guest with a non expired Maintenance contract
  Honda/Nissan Store Guest with a non expired service contract
  Honda/Nissan Store Guest with a non expired Maintenance contract
  Is this possible?  We don’t need to worry about if they’ve had an RO open.  I would like the total list of all.
  Also is there a way to get sent list of new contract purchases after each month?  
  Please let me know if you have any questions.
  Thanks,
  Justin
  
justin 7/2:
  Ignore this email I sent, I kinda jumped the gun, apparently due to certain laws I can’t call 
  those extended service contracts.  I would still like the list of Maintenance Contracts if 
  that is possible though, and if those customer with a Maintenance contract have a Service 
  contact I would like to know, I can still call those people.

  Justin

jon 7/6:  
  OK, to begin with, I have no idea what the difference between a Maintenance Contract and a Service Contract is.
  None.

  So, here is a list from Dealertrack of what they refer to as “service contracts”
  Of those contracts that are currently not expired, these are all the various designations/names/companies listed
  Which are “Maintenance” and which are “Service”

  STORE	CONTRACT_NAME
  RY1	AUL
  RY1	CNA NATIONAL WARRANTY COR
  RY1	CNA Western National
  RY1	GM PROTECTION
  RY1	GM PROTECTION PLAN (GMPP)
  RY1	GMPP
  RY1	WARRANTY SOLUTIONS
  RY2	AUL WARRANTY
  RY2	CNA LIFETIME POWERTR
  RY2	CNA Warranty Corp.
  RY2	CNA WARRANTY CORP
  RY2	Honda Care Sentinel
  RY2	HONDA CARE
  RY2	NISSAN MAINTENANCE

justin 7/9:
  These are the Maintenance plans. 
  STORE	CONTRACT_NAME
  RY1	GMPP
  RY2	Honda Care Sentinel
  RY2	NISSAN MAINTENANCE
     
  Thanks
   Justin



*/

07/09 
given justin new request


select contract_name, count(*)
from arkona.ext_bopsrvs a
where a.contract_expiration_date > 20200709
  and company_number in ('RY1','RY2')
  and (contract_name like 'GM%' or contract_name in( 'Honda Care Sentinel', 'Honda Care','NISSAN MAINTENANCE'))
group by contract_name  

-- these are the current maintenance plans
select *
from arkona.ext_bopsrvs a
where a.contract_expiration_date > 20200709
  and company_number in ('RY1','RY2')
  and contract_name in( 'GMPP','Honda Care Sentinel','NISSAN MAINTENANCE')
order by contract_name   



select d.first_name, d.last_company_name, d.phone_number, d.email_address, d.city, c.year, c.make, c.model
from arkona.ext_bopsrvs a
join arkona.xfm_inpmast c on a.vin = c.inpmast_vin
  and c.current_row  
join arkona.xfm_bopname d on a.customer_key = d.bopname_record_key  
  and d.current_row
  and d.company_individ is not null
where a.contract_expiration_date > 20200709
  and a.company_number in ('RY1','RY2')
  and a.contract_name in( 'GMPP','Honda Care Sentinel','NISSAN MAINTENANCE')







------------------------------------------------------------------------------------
-- prior to justin making up his mind
-----------------------------------------------------------------------------------
--07/06/2020
select *
from arkona.ext_bopsrvs a
where a.contract_expiration_date > 20200704
order by a.contract_expiration_date desc
limit 100


select company_number, bo_record_key, sd_record_key, contract_name, count(*)
from arkona.ext_bopsrvs a
where a.contract_expiration_date > 20200704
group by company_number, bo_record_key, sd_record_key, contract_name
order by company_number, bo_record_key


select a.company_number, a.vin, a.deal_key, a.bo_record_key, a.sd_record_key, 
  a.contract_name, a.contract_start_date, a.contract_expiration_date, 
  b.company_name,
  c.serv_cont_company, c.serv_cont_str_dat, c.serv_cont_exp_dat
from arkona.ext_bopsrvs a
left join arkona.ext_sdpsvcd b on a.company_number = b.company_number
  and a.sd_record_key = b.record_key
left join arkona.xfm_bopmast c on a.deal_key = c.record_key
  and c.current_row
where a.contract_expiration_date > 20200704
  and a.contract_start_date > 20190000
  and a.contract_name <> b.company_name
order by a.company_number, a.bo_record_key, a.sd_record_key


-- ok, generate a list of valid contract names to send to justin to qualify

select a.company_number, contract_name, b.company_name, count(*), min(vin), max(vin)
from arkona.ext_bopsrvs a
join arkona.ext_bopsrvc b on a.bo_record_key = b.record_key
  and a.company_number = b.company_number
where a.contract_expiration_date > 20200704
group by a.company_number, a.contract_name, b.company_name



select * from arkona.ext_bopsrvs
where bo_record_key = 1
limit 10
order by contract_Start_date desc


-- from ....misc_sql\jeri\service_contracts_outside_of_car_deals.sql
select *
from arkona.xfm_Bopname
where current_row
  and last_company_name in ('bergeron','danielson','rygg','hoefs')
order by last_company_name, bopname_Search_name  
-- looks like contracts outside of a deal have a deal_key of 0
select * from arkona.ext_bopsrvs where customer_key in ( 307241,1148620,222654)


select count(*) from arkona.ext_bopsrvs  --20235, scraped 06/30/20

select count(*) from arkona.ext_bopsrvs where contract_expiration_date > 20200630  -- 8792

-- contracts 10846, join on bopvref.customer 6640
drop table if exists sc;
create temp table sc as
select a.vin,  a.contract_name, d.first_name, d.last_company_name, d.phone_number, d.email_address, d.city, c.year, c.make, c.model
--   a.vin, a.customer_key, 
--   a.contract_name, a.contract_start_date, a.contract_expiration_date,
--   a.contract_expiration_months, starting_odometer, a.contract_Expiration_miles, a.ending_miles
from arkona.ext_bopsrvs a
jOIN ARKONA.EXT_bopvref b on a.vin = b.vin
  and a.customer_key = b.customer_key
  and b.end_date = 0
join arkona.xfm_inpmast c on b.vin = c.inpmast_vin
  and c.current_row  
join arkona.xfm_bopname d on a.customer_key = d.bopname_record_key  
  and d.current_row
  and d.company_individ is not null
where a.contract_expiration_date > 20200630;

select *
from arkona.ext_bopvref
limit 100


select bopname_Record_key, bopname_search_name
from arkona.xfm_bopname 
where current_row 
  and company_individ is not null 
  and bopname_company_number = 'RY1' 

-- last ro before 202003
select vin
from (
  select a.vin, max(open_date) as open_date
  from arkona.ext_sdprhdr_history a
  join sc b on a.vin = b.vin
  group by a.vin) c
where open_date < 20200300

of the 6640 non expired service contracts sold to parties who still own the vehicle,
5115 of those vehicle have not had an open ro since 03/01/2020

select distinct aa.*
from sc aa
join (
  select vin
  from (
    select a.vin, max(open_date) as open_date
    from arkona.ext_sdprhdr_history a
    join sc b on a.vin = b.vin
    group by a.vin) c
  where open_date < 20200300) bb on aa.vin = bb.vin


 select vin, max(open_Date)
 from arkona.ext_Sdprhder 

drop table if exists wtf ;
create temp table wtf as
select vin, max(open_date) as open_date from arkona.ext_sdprhdr_tmp group by vin
union
select vin,max(open_date) as open_date from arkona.ext_sdprhdr group by vin

select *
from sc a
join wtf b on a.vin = b.vin
where open_date > 20200300

4488
sent this as V2
drop table if exists v3;
create temp table v3 as
select distinct aa.*
from sc aa
join (
  select vin
  from (
    select a.vin, max(open_date) as open_date
    from arkona.ext_sdprhdr_history a
    join sc b on a.vin = b.vin
    group by a.vin) c
  where open_date < 20200300) bb on aa.vin = bb.vin
 and aa.vin not in (
   select vin
   from wtf
   where open_date > 20200300);

create unique index on v3(vin);   

select * from v3 where vin = 'KNMAT2MV8JP508786'

select * from arkona.ext_bopsrvs where vin = 'KNMAT2MV8JP508786'

select * from arkona.xfm_Bopmast
where bopmast_company_number = 'RY2' and record_key in (14751,15951)

select vin
from v3
group by vin having count(*) > 1