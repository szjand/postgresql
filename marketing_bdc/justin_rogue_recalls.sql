﻿/*
Hey Jon,

I hope you’re doing well!  Can you get me a list together of 2014-2016 Nissan Rogues that have either had service done here at anytime or were purchased from here at any time please?

First Name
Last Name
Primary Number
Secondary Number
Email
City
State
Year
Make
Model
VIN

Thanks!  Have a great day!

Justin Kilmer
*/

/* BOPVREF */
BVTYPE (TYPE_CODE) Customer/Vehicle Reference
S = Buyer on the Deal
2 = Co-Buyer on the Deal
C = Service Customer
T = Trade In
4 = ?? (Of the 212409 rows in BOPVREF table, only 59 are type 4)


drop table if exists rogues;
create temp table rogues as 
select year, make, model, inpmast_vin, inpmast_stock_number
from arkona.ext_inpmast
where model in ('rogue','rogue sport')
  and year between 2014 and 2022;

select company_individ, count(*) from arkona.ext_bopname group by company_individ

select * from arkona.ext_bopname limit 10

select distinct inpmast_company_number from arkona.ext_inpmast limit 20

select * from arkona.ext_bopvref where vin in ( '5N1AT2MV4FC840119','5N1AT2MV5GC733243','KNMAT2MV7FP502792') order by vin, customer_key, start_date

select distinct c.first_name, c.last_company_name, c.phone_number, c.cell_phone, c.email_address,
  c.city, c.state_code, b.year, b.make, b.model, b.inpmast_vin
from arkona.ext_bopvref a
join rogues b on a.vin = b.inpmast_vin
join arkona.ext_bopname c on a.customer_key = c.bopname_record_key
  and c.bopname_company_number = a.company_number
  and c.company_individ is not null
  and c.last_company_name not like 'dealer%'
  and c.last_company_name not like 'enterpri%'
  and c.last_company_name not like 'adesa%'
  and c.last_company_name not like 'auto%'
  and c.last_company_name not like 'rydell honda%'
  and c.last_company_name not like 'inventory%'
  and c.last_company_name not like 'rydell%'
  and c.last_company_name not like 'EDGEWOOD%'
  and c.last_company_name not in ('HERTZ CAR RENTAL', 'FRIENDLY CHEVROLET','HONDA NISSAN OF GRAND FORKS','KATIE OLSON','LAMBERSON',
    'NATIONAL CAR RENTAL CORP','NISSAN NORTH AMERICA INC.','POLK COUNTY','ROCHESTER TOYOTA','SIOUX FALLS FORD LINCOLN',
    'THIBERTS CHEV','WASCHKE FAMILY GM CENTER','BISMARCK MOTOR COMPANY','BMW OF SPOKANE','DRISCOLL')
where a.end_date = 0
  and a.type_code not in ('2','T')
order by c.last_company_name
