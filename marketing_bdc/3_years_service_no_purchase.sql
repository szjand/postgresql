﻿/*
8/28/19

Hey Jon,

Can you get the Name, City, Number, Email, Year, Make, Model of these VINs?

Thanks,

Justin Kilmer
*/
-- create table jon.nissan_recalls (
--   vin citext,
--   recall citext,
--   seq integer primary key);
-- 
-- populate it from the spreadsheet
-- 
-- -- 479 vins

drop table if exists jon.the_vins;
create table jon.the_vins (
  vin citext primary key,
  seq integer not null);
  
218 vins

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
select * 
from jon.the_vins a
left join arkona.ext_bopvref b on a.vin = b.vin
order by b.customer_key

JN8DR09Y53W816403

select * 
from arkona.xfm_inpmast
where inpmast_vin = 'JN8DR09Y53W816403'
order by inpmast_key

select * 
from arkona.xfm_bopname 
where bopname_record_key = 249075


select bopname_company_number, count(*), max(bopname_record_key)
from arkona.xfm_bopname 
where current_row
group by bopname_company_number


select * 
from (
  select bopname_company_number, bopname_Record_key
  from arkona.xfm_bopname
  where current_row
    and bopname_company_number = 'RY1') a 
join (  
  select bopname_company_number, bopname_Record_key
  from arkona.xfm_bopname
  where current_row
    and bopname_company_number = 'RY2') b on a.bopname_record_key = b.bopname_record_key

-- bopname_record_key s that exist for both ry1 & ry2
create temp table multiple_bopname as
select bopname_record_key, bopname_company_number, bopname_search_name
from arkona.xfm_bopname
where current_row
  and bopname_Record_key in (
    select a.bopname_record_key 
    from (
      select bopname_company_number, bopname_Record_key
      from arkona.xfm_bopname
      where current_row
        and bopname_company_number = 'RY1') a 
    join (  
      select bopname_company_number, bopname_Record_key
      from arkona.xfm_bopname
      where current_row
        and bopname_company_number = 'RY2') b on a.bopname_record_key = b.bopname_record_key)    
  and bopname_company_number in ('RY1','RY2','RY3')        
order by bopname_record_key, bopname_company_number;      


-- in my list of vins only 3 of the matching bopvref rows match up to multiple bopname records
select * 
from jon.the_vins a
left join arkona.ext_bopvref b on a.vin = b.vin
  and b.customer_key in (select bopname_record_key from multiple_bopname)
order by b.customer_key    
-- exclude the 3 rows with cust key for both stores, and pick the most recent from bopvref
select *
from (
  select a.vin, b.customer_key, row_number() over (partition by a.vin order by a.vin, b.start_date desc)
  from jon.the_vins a
  left join arkona.ext_bopvref b on a.vin = b.vin  
  where a.seq not in (126,165,184)) x
where x.row_number = 1  


select x.seq, x.vin, z.bopname_search_name, z.city, z.phone_number, z.email_address, y.year, y.make, y.model
from (
  select a.vin, a.seq, b.customer_key, row_number() over (partition by a.vin order by a.vin, b.start_date desc)
  from jon.the_vins a
  left join arkona.ext_bopvref b on a.vin = b.vin) x
join arkona.xfm_inpmast y on x.vin = y.inpmast_vin
  and y.current_row  
left join arkona.xfm_bopname z on x.customer_key = z.bopname_record_key  
  and z.current_row
where x.row_number = 1  
  and bopname_search_name <> 'SST' -- this is the one double record key, they both have same dates
order by seq


-- now lets see if i can figure out the 3 dup cust key
-- noe help, may need to go to the ro
select a.seq, a.vin, b.customer_key, b.start_date, b.end_date, c.bopname_search_name, c.bopname_company_number, b.company_number
from jon.the_vins a
left join arkona.ext_bopvref b on a.vin = b.vin
left join arkona.xfm_bopname c on b.customer_key = c.bopname_record_key
  and c.current_row
where seq in (126,165,184)
order by a.vin, b.start_date


1N4AL11D55C369418  - miller

1N4AL3AP8FN302940  - olson

JN1DA31A42T301253  - jama

select * 
from arkona.xfm_inpmast