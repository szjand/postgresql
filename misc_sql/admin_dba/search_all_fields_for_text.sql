﻿CREATE OR REPLACE FUNCTION public.search_columns(
    needle text,
    haystack_tables name[] default '{}',
    haystack_schema name[] default '{}'
)
RETURNS table(schemaname text, tablename text, columnname text, rowctid text)
AS $$
/*
sample usage: 
*/
begin
  FOR schemaname,tablename,columnname IN
      SELECT c.table_schema,c.table_name,c.column_name
      FROM information_schema.columns c
      JOIN information_schema.tables t ON
        (t.table_name=c.table_name AND t.table_schema=c.table_schema)
      WHERE (c.table_name=ANY(haystack_tables) OR haystack_tables='{}')
        AND (c.table_schema=ANY(haystack_schema) OR haystack_schema='{}')
        AND t.table_type='BASE TABLE'
  LOOP
    EXECUTE format('SELECT ctid FROM %I.%I WHERE cast(%I as text)=%L',
       schemaname,
       tablename,
       columnname,
       needle
    ) INTO rowctid;
    IF rowctid is not null THEN
      RETURN NEXT;
    END IF;
 END LOOP;
END;
$$ language plpgsql;


8/13/19

select * from search_columns('this is a test performed by jon impersonating nick','{}','{oo}');

select * from oo.market_one_on_ones

select * from search_columns('%consens%','{}','{oo}');

-- nick entered one for taylor, notes included ABCDE
select * from search_columns('ABCDE');
select * from search_columns('ABCD','{}','{oo}');
does not show one on one archive on team monson summary page

but on gsm-summary team monson shows 0 Days ago and no "new for team leader" option

but when i logged in as nick and went to the gsm-summary page, i showed 36 DAy(S) AGO for taylr
and was able to do a one on one for taylor

did a new one on one as nick for taylor
notes: jon as nick does taylor

-- returns nothing
select * from search_columns('jon as nick does taylor','{}','{oo}');

-- returns nothing (after 17 minutes)
select * from search_columns('jon as nick does taylor');

does not show one on one archive on team monson summary page

but on gsm-summary team monson shows 0 Days ago and no "new for team leader" option


oneOnOnes

SELECT sls.json_get_teams()




select * from search_columns('%jon%','{}','{oo}');


