﻿select * from pg_stat_statements order by queryid

-- initiall, this is the longest running query
-- what i wonder is, is the queryid persistent?
-- today, queryid for this query appears to be 

581003707;select nrv.get_camera_ips() as server


-- possibly another one that will run frequently
692670894;copy keys.ext_keyper_tmp from stdin with csv encoding 'latin-1 '


select * from pg_stat_statements order by calls desc 

select * from pg_stat_statements order by mean_time desc 

-- 7/23/2019

select * from pg_stat_activity where state = 'active'

select calls, mean_time, trim(regexp_replace(query, '\n', '->', 'g')) from pg_stat_statements where mean_time > 1000 order by calls desc 

select pg_stat_statements_reset()

select * from pg_stat_activity where state = 'active'

select * from pg_stat_statements where mean_time > 1000 order by calls desc 


select * from pg_stat_statements where query like '%get_vehicles%'

select * from gmgl.get_Vehicles_log


select * from pg_stat_statements where query like '%json_get_sold_board%'

select * from nrv.user_route_logs where log_ts::date = current_date - 1
    