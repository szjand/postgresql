﻿-- -- DROP TRIGGER boarded_notify_event ON board.deal_gross;
-- -- CREATE TRIGGER boarded_notify_event
-- --   AFTER INSERT OR UPDATE
-- --   ON board.deal_gross
-- --   FOR EACH ROW
-- --   EXECUTE PROCEDURE public.notify_gross_changes();

DROP TRIGGER boarded_pending_board ON board.pending_boards;
-- CREATE TRIGGER boarded_pending_board
--   AFTER INSERT OR UPDATE OR DELETE
--   ON board.pending_boards
--   FOR EACH ROW
--   EXECUTE PROCEDURE public.notify_pending_board();

DROP TRIGGER boarded_sales_board ON board.pending_boards;
-- CREATE TRIGGER boarded_sales_board
--   AFTER UPDATE
--   ON board.pending_boards
--   FOR EACH ROW
--   EXECUTE PROCEDURE public.notify_pending_board();

DROP TRIGGER boarded_notify_event ON board.sales_board;
-- CREATE TRIGGER boarded_notify_event
--   AFTER INSERT OR UPDATE
--   ON board.sales_board
--   FOR EACH ROW
--   EXECUTE PROCEDURE public.notify_boarded_vehicle();

DROP TRIGGER pricing_changes ON gmgl.vin_incentive_cash;
-- CREATE TRIGGER pricing_changes
--   AFTER INSERT OR UPDATE OR DELETE
--   ON gmgl.vin_incentive_cash
--   FOR EACH ROW
--   EXECUTE PROCEDURE public.pricing_change();

DROP TRIGGER pricing_changes ON gmgl.vin_margins;
-- CREATE TRIGGER pricing_changes
--   AFTER INSERT OR UPDATE OR DELETE
--   ON gmgl.vin_margins
--   FOR EACH ROW
--   EXECUTE PROCEDURE public.pricing_change();

DROP TRIGGER pricing_changes ON gmgl.vehicle_estimated_aftermarket;
-- CREATE TRIGGER pricing_changes
--   AFTER INSERT OR UPDATE OR DELETE
--   ON gmgl.vehicle_estimated_aftermarket
--   FOR EACH ROW
--   EXECUTE PROCEDURE public.pricing_change();

DROP TRIGGER new_camera_status ON nrv.camera_task_logs;
-- CREATE TRIGGER new_camera_status
--   AFTER INSERT OR UPDATE OR DELETE
--   ON nrv.camera_task_logs
--   FOR EACH ROW
--   EXECUTE PROCEDURE public.new_camera_log();

DROP TRIGGER new_task_log ON tsk.task_logs;
-- CREATE TRIGGER new_task_log
--   AFTER INSERT OR UPDATE OR DELETE
--   ON tsk.task_logs
--   FOR EACH ROW
--   EXECUTE PROCEDURE public.new_task_log();
