﻿select *
from ads.ext_vehicle_inventory_item_statuses a
where a.category = 'RMFlagPulled'
  and a.thruts > now()
limit 1000

create index on ads.ext_vehicle_inventory_item_statuses(status);
create index on ads.ext_vehicle_inventory_item_statuses(thruts);
create index on ads.ext_vehicle_inventory_item_statuses(vehicleinventoryitemid);
create index on ads.ext_vehicle_inventory_item_statuses(category);
create index on ads.ext_vehicle_inventory_items(vehicleitemid);

drop table if exists greg.tool_used_priorities;
create table greg.tool_used_priorities (
  stock_number citext not null,
  current_priority integer not null,
  model_year citext not null,
  make citext not null,
  model citext not null,
  color citext not null, 
  mech_status citext not null,
  body_status citext not null,
  app_status citext not null,
  the_date date not null,
  the_time time not null,
  primary key (stock_number,the_date,the_time));

insert into greg.tool_used_priorities
select a.stocknumber, a.currentpriority, c.yearmodel, c.make, c.model, c.exteriorcolor,
(SELECT 
  CASE status
    WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
    WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
    WHEN 'MechanicalReconProcess_NotStarted' THEN --'Not Started'
      CASE 
        WHEN EXISTS (
          SELECT 1 
          FROM ads.ext_vehicle_inventory_item_statuses
          WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
          AND category = 'MechanicalReconDispatched'
          AND ThruTS > now()) THEN 'Dispatched'
        ELSE 'Not Started'
      END 
  END         
  FROM ads.ext_vehicle_inventory_item_statuses
  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID 
  AND category = 'MechanicalReconProcess'
  AND ThruTS > now()) as mechanical_status,
(SELECT 
  CASE status
    WHEN 'BodyReconProcess_InProcess' THEN 'WIP'
    WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
    WHEN 'BodyReconProcess_NotStarted' THEN --'Not Started'
      CASE 
        WHEN EXISTS (
          SELECT 1 
          FROM ads.ext_vehicle_inventory_item_statuses
          WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
          AND category = 'BodyReconDispatched'
          AND ThruTS > now()) THEN 'Dispatched'
        ELSE 'Not Started'
      END 
  END         
  FROM ads.ext_vehicle_inventory_item_statuses
  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID 
  AND category = 'BodyReconProcess'
  AND ThruTS > now()) as body_status,
(SELECT 
  CASE status
    WHEN 'AppearanceReconProcess_InProcess' THEN 'WIP'
    WHEN 'AppearanceReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
    WHEN 'AppearanceReconProcess_NotStarted' THEN --'Not Started'
      CASE 
        WHEN EXISTS (
          SELECT 1 
          FROM ads.ext_vehicle_inventory_item_statuses
          WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
          AND category = 'AppearanceReconDispatched'
          AND ThruTS > now()) THEN 'Dispatched'
        ELSE 'Not Started'
      END 
  END         
  FROM ads.ext_vehicle_inventory_item_statuses
  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID 
  AND category = 'AppearanceReconProcess'
  AND ThruTS > now()) as appearance_status,
  now()::date,
  now()::time
-- select * 
from ads.ext_vehicle_inventory_items a
inner join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and b.category = 'RMFlagPulled'
  and b.thruts > now()
inner join ads.ext_vehicle_items c on a.vehicleitemid = c.vehicleitemid
where a.owninglocationid = 'B6183892-C79D-4489-A58C-B526DF948B06';

select * from greg.tool_used_priorities where the_Date = current_Date order by the_date, the_time, stock_number

select * from greg.tool_used_priorities where the_Date = '07/31/2017' and the_time > '16:40:00'


-- 8/2/17
select distinct the_Date, the_time from greg.tool_used_priorities order by the_Date, the_time

-- this will work 
select stock_number, model_year, make, model, color,
  max(case when seq = 1 then current_priority end) as "7 priority",
  max(case when seq = 1 then status end) as "7 status",
  max(case when seq = 2 then current_priority end) as "2 priority",
  max(case when seq = 2 then status end) as "2 status"  
from (  
  select 1 as seq, stock_number, current_priority, model_year, make, model, color,
    'M:'||mech_status || ' :: ' || 'B:' || body_status || ' :: ' || 'A:' || app_status as status
  from greg.tool_used_priorities
  where the_Date = '08/01/2017'
    and extract(hour from the_time) = 15  
  union
  select 2 as seq, stock_number, current_priority, model_year, make, model, color,
    'M:'||mech_status || ' :: ' || 'B:' || body_status || ' :: ' || 'A:' || app_status
  from greg.tool_used_priorities
  where the_Date = '08/02/2017'
    and extract(hour from the_time) = 7) x 
group by stock_number, model_year, make, model, color  
order by stock_number


-- compact the statuses
select distinct mech_status from greg.tool_used_priorities
union
select distinct body_status from greg.tool_used_priorities
union
select distinct app_status from greg.tool_used_priorities



                select '0  stock #' as stock_number, 'year','make', 'model', 'color', -- header row for spreadsheet
                  case -- three days ago
                    when extract(dow from current_date) in (1,2,3) then to_char(current_date -4, 'Dy Mon DD')
                        || ': 9 PM Priority'    
                    else to_char(current_date -3, 'Dy Mon DD') || ': 9 PM Priority'
                  end,                
                  case -- 2 days ago
                    when extract(dow from current_date) in (1, 2) then to_char(current_date -3, 'Dy Mon DD')
                        || ': 9 PM Priority'
                    else to_char(current_date -2, 'Dy Mon DD') || ': 9 PM Priority'
                  end,
                  case -- 1 day ago 
                    when extract(dow from current_date) = 1 then to_char(current_date -2, 'Dy Mon DD')
                        || ': 9 PM Priority'
                    else to_char(current_date -1, 'Dy Mon DD') || ': 9 PM Priority'
                  end,
                  to_char(current_date, 'Dy Mon DD') || ': 7 AM Priority',
                  to_char(current_date, 'Dy Mon DD') || ': 2 PM Priority',
                  to_char(current_date, 'Dy Mon DD') || ': 9 PM Priority',
                  case -- three days ago
                    when extract(dow from current_date) in (1,2,3) then to_char(current_date -4, 'Dy Mon DD')
                        || ': 9 PM Status'    
                    else to_char(current_date -3, 'Dy Mon DD') || ': 9 PM Status'
                  end,                   
                  case -- 2 days ago
                    when extract(dow from current_date) in (1, 2) then to_char(current_date -3, 'Dy Mon DD')
                        || ': 9 PM Status'
                    else to_char(current_date -2, 'Dy Mon DD') || ': 9 PM Status'
                  end,
                  case -- 1 ay ago
                    when extract(dow from current_date) = 1 then to_char(current_date -2, 'Dy Mon DD')
                        || ': 9 PM Status'
                    else to_char(current_date -1, 'Dy Mon DD') || ': 9 PM Status'
                  end,
                  to_char(current_date, 'Dy Mon DD') || ': 7 AM Status',
                  to_char(current_date, 'Dy Mon DD') || ': 2 PM Status',
                  to_char(current_date, 'Dy Mon DD') || ': 9 PM Status'
                union
                select stock_number, model_year, make, model, color,
                  max(case when seq = 6 then current_priority end)::text as "3 days ago priority",
                  max(case when seq = 1 then current_priority end)::text as "2 days ago priority",
                  max(case when seq = 2 then current_priority end)::text as "yesterday priority",
                  max(case when seq = 3 then current_priority end)::text as "7 priority",
                  max(case when seq = 4 then current_priority end)::text as "2 priority",
                  max(case when seq = 5 then current_priority end)::text as "9 priority",
                  max(case when seq = 6 then status end) as "3 days ago status",
                  max(case when seq = 1 then status end) as "2 days ago status",
                  max(case when seq = 2 then status end) as "yesterday status",
                  max(case when seq = 3 then status end) as "2 status",
                  max(case when seq = 4 then status end) as "7 status",
                  max(case when seq = 5 then status end) as "9 status"
                from (
                  select 6 as seq, stock_number, current_priority, model_year, make, model, color,
                    case
                      when mech_status = 'no open items' and body_status = 'no open items'
                            and app_status = 'no open items' then 'All Recon Done'
                    else
                      'M:'||case mech_status when 'no open items' then 'Done' when 'dispatched' then 'Disp'
                            when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                      || ' - ' || 'B:'||case body_status when 'no open items' then 'Done'
                            when 'dispatched' then 'Disp' when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                      || ' - ' || 'A:'||case app_status when 'no open items' then 'Done'
                            when 'dispatched' then 'Disp' when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                    end as status
                  from greg.tool_used_priorities
                  where the_date =
                    case
                      when extract(dow from current_date) in (1, 2, 3) then current_date - 4
                      else current_date - 3
                    end
                    and extract(hour from the_time) = 21
                  union                                  
                  select 1 as seq, stock_number, current_priority, model_year, make, model, color,
                    case
                      when mech_status = 'no open items' and body_status = 'no open items'
                            and app_status = 'no open items' then 'All Recon Done'
                    else
                      'M:'||case mech_status when 'no open items' then 'Done' when 'dispatched' then 'Disp'
                            when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                      || ' - ' || 'B:'||case body_status when 'no open items' then 'Done'
                            when 'dispatched' then 'Disp' when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                      || ' - ' || 'A:'||case app_status when 'no open items' then 'Done'
                            when 'dispatched' then 'Disp' when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                    end as status
                  from greg.tool_used_priorities
                  where the_date =
                    case
                      when extract(dow from current_date) in (1, 2) then current_date - 3
                      else current_date - 2
                    end
                    and extract(hour from the_time) = 21
                  union
                  select 2 as seq, stock_number, current_priority, model_year, make, model, color,
                    case
                      when mech_status = 'no open items' and body_status = 'no open items'
                            and app_status = 'no open items' then 'All Recon Done'
                    else
                      'M:'||case mech_status when 'no open items' then 'Done' when 'dispatched' then 'Disp'
                            when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                      || ' - ' || 'B:'||case body_status when 'no open items' then 'Done'
                            when 'dispatched' then 'Disp' when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                      || ' - ' || 'A:'||case app_status when 'no open items' then 'Done'
                            when 'dispatched' then 'Disp' when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                    end as status
                  from greg.tool_used_priorities
                  where the_date =
                    case
                      when extract(dow from current_date) = 1 then current_date - 2
                      else current_date - 1
                    end
                    and extract(hour from the_time) = 21
                  union
                  select 3 as seq, stock_number, current_priority, model_year, make, model, color,
                    case
                      when mech_status = 'no open items' and body_status = 'no open items'
                            and app_status = 'no open items' then 'All Recon Done'
                    else
                      'M:'||case mech_status when 'no open items' then 'Done' when 'dispatched' then 'Disp'
                            when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                      || ' - ' || 'B:'||case body_status when 'no open items' then 'Done'
                            when 'dispatched' then 'Disp' when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                      || ' - ' || 'A:'||case app_status when 'no open items' then 'Done'
                            when 'dispatched' then 'Disp' when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                    end as status
                  from greg.tool_used_priorities
                  where the_Date = current_date
                    and extract(hour from the_time) = 7
                  union
                  select 4 as seq, stock_number, current_priority, model_year, make, model, color,
                    case
                      when mech_status = 'no open items' and body_status = 'no open items'
                            and app_status = 'no open items' then 'All Recon Done'
                    else
                      'M:'||case mech_status when 'no open items' then 'Done' when 'dispatched' then 'Disp'
                            when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                      || ' - ' || 'B:'||case body_status when 'no open items' then 'Done'
                            when 'dispatched' then 'Disp' when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                      || ' - ' || 'A:'||case app_status when 'no open items' then 'Done'
                            when 'dispatched' then 'Disp' when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                    end as status
                  from greg.tool_used_priorities
                  where the_Date = current_date
                    and extract(hour from the_time) = 14
                  union
                  select 5 as seq, stock_number, current_priority, model_year, make, model, color,
                    case
                      when mech_status = 'no open items' and body_status = 'no open items'
                            and app_status = 'no open items' then 'All Recon Done'
                    else
                      'M:'||case mech_status when 'no open items' then 'Done' when 'dispatched' then 'Disp'
                            when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                      || ' - ' || 'B:'||case body_status when 'no open items' then 'Done'
                            when 'dispatched' then 'Disp' when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                      || ' - ' || 'A:'||case app_status when 'no open items' then 'Done'
                            when 'dispatched' then 'Disp' when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                    end as status
                  from greg.tool_used_priorities
                  where the_Date = current_date
                    and extract(hour from the_time) = 21) x
                group by stock_number, model_year, make, model, color
                order by stock_number;


------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------

select *
from greg.tool_used_priorities 
where stock_number = '31184a'


select *
from ads.ext_vehicle_inventory_items a
inner join ads.ext_vehicle_inventory_item_Statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
where a.stocknumber = '31184a'

need to exclude sunday, when days ago = sunday, change days ago to saturday
sun  8-6
mon  8-7
tue  8-8
wed  8-9
thu  8-10


       3 days ago   2 days ago  1 day ago
sun     thu             fri         sat
mon     fri             sat         sun
tue     sat             sun         mon
wed     sun             mon         tue
thu     mon             tue         wed        


when current_date - 3 = 1 then current_date - 4
when current_Date - 2 = 1 then current_date - 3
when current_Date - 1 = 1 then current_date - 2


do
$$
  declare _date date = '08/07/2017'::date;
begin

drop table if exists test;
create temp table test as
  select '0  stock #' as stock_number, 'year' as year ,'make' as make, 'model' as model, 'color' as color, -- header row for spreadsheet
    case
      when extract(dow from _date) in (1,2,3) then to_char(_date -4, 'Dy Mon DD')
          || ': 9 PM Priority'    
      else to_char(_date -3, 'Dy Mon DD') || ': 9 PM Priority'
    end as "pr 3 days ago",
    case
      when extract(dow from _date) in (1, 2) then to_char(_date -3, 'Dy Mon DD')
          || ': 9 PM Priority'    
      else to_char(_date -2, 'Dy Mon DD') || ': 9 PM Priority'
    end as "pr 2 days ago",
    case
      when extract(dow from _date) = 1 then to_char(_date -2, 'Dy Mon DD')
          || ': 9 PM Priority'
      else to_char(_date -1, 'Dy Mon DD') || ': 9 PM Priority'
    end as "pr 1 day ago",
    to_char(_date, 'Dy Mon DD') || ': 7 AM Priority' as "pr 7",
    to_char(_date, 'Dy Mon DD') || ': 2 PM Priority' as "pr 2",
    to_char(_date, 'Dy Mon DD') || ': 9 PM Priority' as "pr 9";
end
$$;  

select * from test


