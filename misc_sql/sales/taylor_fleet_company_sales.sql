﻿C: company
select company_individ, count(*) from arkona.xfm_bopname where current_row group by company_individ

C: cash, F: fin L: lease O: owner
select record_type, count(*) from arkona.xfm_bopmast where current_row group by record_type

R: retail, F: fleet
select sale_type, count(*) from arkona.xfm_bopmast where current_row group by sale_type

select vehicle_type, count(*) from arkona.xfm_bopmast where current_row group by vehicle_type

-- 2046
select 
  case a.sale_type
    when 'F' then 'Fleet'
    when 'L' then 'Lease'
    when 'R' then 'Retail'
  end as sale_type,
  case a.record_type
    when 'C' then 'Cash'
    when 'F' then 'Finance'
    when 'L' then 'Lease'
    when 'O' then 'Outside Lien'
  end as deal_type,
  a.bopmast_Stock_number, a.bopmast_vin, a.date_capped, a.bopmast_Search_name,
  c.year, c.make, c.model, c.body_style, a.f_i_gross, sales_person_name
from arkona.xfm_bopmast a
join arkona.xfm_bopname b on a.bopmast_company_number = b.bopname_company_number
  and a.buyer_number = b.bopname_Record_key
  and b.current_row
join arkona.xfm_inpmast c on a.bopmast_vin = c.inpmast_vin
  and c.current_row  
left join arkona.ext_bopslsp d on a.primary_salespers = d.sales_person_id
  and a.bopmast_company_number = d.company_number
where a.current_row
  and a.vehicle_type = 'N'
  and a.record_status = 'U'
  and (a.sale_type = 'F' or b.company_individ = 'C')
  and a.sale_type <> 'W'
  and a.bopmast_search_name not like 'RYDELL%'
  and a.date_capped > '2009-01-01'
order by a.date_capped