﻿One more request Jon

Can I get what is in our current used inventory by same segment

RY1 car truck and suv
RY2 car truck and suv

Thanks man

Nick 


-- used car sales
/*
drop table if exists uc_sales;
create temp table uc_sales as
select a.bopmast_stock_number, bopmast_vin, aa.make, aa.model, a.date_capped,
  substring(d.vehicletype, position('_' in d.vehicletype) + 1, 9) as segment, 
  substring(e.typ, position('_' in e.typ) + 1, 9) as sale_type,
  coalesce (f.cpo, false) as cpo
from arkona.xfm_bopmast a
left join arkona.xfm_inpmast aa on a.bopmast_vin = aa.inpmast_vin
  and aa.current_row
left join ads.ext_vehicle_inventory_items b on a.bopmast_Stock_number = b.stocknumber
left join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
left join ads.ext_make_model_classifications d on c.make = d.make
  and c.model = d.model
left join ads.ext_vehicle_sales e on b.vehicleinventoryitemid = e.vehicleinventoryitemid  
left join (
  select x.vehicleinventoryitemid, true as cpo
  from ads.ext_selected_recon_packages x
  where substring(typ, position('_' in typ) + 1, 7) = 'Factory'
    and selectedreconpackagets = (
      select max(selectedreconpackagets)
      from ads.ext_selected_recon_packages
      where vehicleinventoryitemid = x.vehicleinventoryitemid)) f on b.vehicleinventoryitemid= f.vehicleinventoryitemid
where a.current_row
  and a.date_capped between '01/01/2019' and '05/31/2019'
  and record_Status = 'U'
  and vehicle_type = 'U';

update uc_sales
set segment = 'Car'
where segment is null
  and model in ('accord','accord coupe','altima','aveo','BONNEVILLE',
    'CR-V','FOCUS','GLS','IMPALA','MALIBU','MAXIMA','MKC','ROGUE',
    'SENTRA','VERSA');   
update uc_sales
set segment = 'SUV'
where segment is null
  and model in ('armada','EQUINOX','EXPLORER','GSX-R1000','MURANO','NV200','PILOT');    

update uc_sales
set segment = 'Crossover'
where segment is null
  and model in ('Enclave');    

update uc_sales
set segment = 'Van'
where segment is null
  and model in ('ODYSSEY');    

update uc_sales
set segment = 'Pickup'
where segment is null
  and model in ('SIERRA 1500','SILVERADO 1500');     

update uc_sales 
set sale_type = 'Retail'
where sale_type is null;  
*/

sales numbers need to come from accounting

drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select year_month, store, page, line, line_label, control, sum(unit_count) as unit_count
from (
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201901 and 201905 --------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select distinct f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month between 201901 and 201905 -------------------------------------------------------------------
      and b.page = 16 and b.line between 1 and 14
--         or
--         (b.page = 17 and b.line between 1 and 20))-- f/i
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account 
  where a.post_status = 'Y') h
group by year_month, store, page, line, line_label, control
order by year_month, store, page, line;

-- this all checks agains financial statements
select year_month, store, line, line_label, sum(unit_count) 
from step_1
group by year_month, store, line, line_label
order by year_month, store, line

select * from step_1




select 2 as seq,'Trade Ins' as source, b.company_number as store, 
  count(b.vin) filter (where d.year_month = 201901) as jan,
  count(b.vin) filter (where d.year_month = 201902) as feb,
  count(b.vin) filter (where d.year_month = 201903) as mar,
  count(b.vin) filter (where d.year_month = 201904) as apr,
  count(b.vin) filter (where d.year_month = 201905) as may
--   b.vin, b.stock_, c.status, c.date_in_invent, arkona.db2_integer_to_date_long(c.date_in_invent)
from arkona.ext_boptrad b 
join arkona.xfm_inpmast c on b.vin = c.inpmast_vin
  and c.current_row
join dds.dim_date d on arkona.db2_integer_to_date_long(c.date_in_invent) = d.the_date
where c.date_in_invent between 20190101 and 20190531
group by b.company_number
union
select 1 as seq, 'Street Purchases',
  case when left(a.inpmast_Stock_number, 1) = 'H' then 'RY2' else 'RY1' end as store,
  count(a.inpmast_vin) filter (where b.year_month = 201901) as jan,
  count(a.inpmast_vin) filter (where b.year_month = 201902) as feb,
  count(a.inpmast_vin) filter (where b.year_month = 201903) as mar,
  count(a.inpmast_vin) filter (where b.year_month = 201904) as apr,
  count(a.inpmast_vin) filter (where b.year_month = 201905) as may  
from arkona.xfm_inpmast a
join dds.dim_date b on arkona.db2_integer_to_date_long(a.date_in_invent) = b.the_date
where a.current_row 
  and a.date_in_invent between 20190101 and 20190531
  and right(trim(a.inpmast_stock_number), 1) = 'P'
group by case when left(a.inpmast_Stock_number, 1) = 'H' then 'RY2' else 'RY1' end
union
select 3 as seq, 'Auction Purchases',
  case when left(a.inpmast_Stock_number, 1) = 'H' then 'RY2' else 'RY1' end as store,
  count(a.inpmast_vin) filter (where b.year_month = 201901) as jan,
  count(a.inpmast_vin) filter (where b.year_month = 201902) as feb,
  count(a.inpmast_vin) filter (where b.year_month = 201903) as mar,
  count(a.inpmast_vin) filter (where b.year_month = 201904) as apr,
  count(a.inpmast_vin) filter (where b.year_month = 201905) as may  
from arkona.xfm_inpmast a
join dds.dim_date b on arkona.db2_integer_to_date_long(a.date_in_invent) = b.the_date
where a.current_row 
  and a.date_in_invent between 20190101 and 20190531
  and right(trim(a.inpmast_stock_number), 1) in ('X','T','M')
group by case when left(a.inpmast_Stock_number, 1) = 'H' then 'RY2' else 'RY1' end
union
select 4 as seq, 'CPO Sales', upper(store),
  sum(unit_count) filter (where year_month = 201901) as jan,
  sum(unit_count) filter (where year_month = 201902) as feb,
  sum(unit_count) filter (where year_month = 201903) as mar,
  sum(unit_count) filter (where year_month = 201904) as apr,
  sum(unit_count) filter (where year_month = 201905) as may  
from step_1
where line in (1,4)
group by store
union
select 5 as seq, 'non-CPO Sales', upper(store),
  sum(unit_count) filter (where year_month = 201901) as jan,
  sum(unit_count) filter (where year_month = 201902) as feb,
  sum(unit_count) filter (where year_month = 201903) as mar,
  sum(unit_count) filter (where year_month = 201904) as apr,
  sum(unit_count) filter (where year_month = 201905) as may  
from step_1
where line in (2,5)
group by store
union
select 6 as seq, 'Wholesales (excludes intra market)', upper(store),
  sum(a.unit_count) filter (where a.year_month = 201901) as jan,
  sum(a.unit_count) filter (where a.year_month = 201902) as feb,
  sum(a.unit_count) filter (where a.year_month = 201903) as mar,
  sum(a.unit_count) filter (where a.year_month = 201904) as apr,
  sum(a.unit_count) filter (where a.year_month = 201905) as may  
from step_1 a
join sls.deals b on a.control = b.stock_number -- wholesale: exclude intra market
where line in (8,10)
group by store
union
select 7 as seq, 'R Units Sold', 'RY1',
  count(bopmast_vin) filter (where year_month = 201901) as jan,
  count(bopmast_vin) filter (where year_month = 201902) as feb,
  count(bopmast_vin) filter (where year_month = 201903) as mar,
  count(bopmast_vin) filter (where year_month = 201904) as apr,
  count(bopmast_vin) filter (where year_month = 201905) as may  
from arkona.xfm_bopmast a
join dds.dim_date b on a.date_capped = b.the_date
where a.current_row
  and a.date_capped between '01/01/2019' and '05/31/2019'
  and record_Status = 'U'
  and vehicle_type = 'N' 
  and right(trim(bopmast_stock_number), 1) = 'R'
order by seq, store  





-- segments: retail only, van & crossover to SUV



-- 1887
drop table if exists segments;
create temp table segments as
select x.*, y.make, y.model, substring(z.vehicletype, position('_' in vehicletype) + 1, 9) as segment
from (
  selecT a.*, coalesce(b.inpmast_vin, d.vin) as vin
  from step_1 a
  left join arkona.xfm_inpmast b on a.control = b.inpmast_stock_number
    and b.current_row
  left join ads.ext_vehicle_inventory_items c on a.control = c.stocknumber
  left join ads.ext_vehicle_items d on c.vehicleitemid = d.vehicleitemid
  where a.line not in (8,10)) x
left join arkona.xfm_inpmast y on x.vin = y.inpmast_vin
  and y.current_row
left join ads.ext_make_model_classifications z on y.make = z.make and y.model = z.model;

create unique index on segments(year_month, control, vin)

select *, case when segment is null then 'null segment' end
from segments
where segment is null
order by model

update segments
set segment = 'Car'
where segment is null
  and model in ('1 SERIES','5 SERIES','LUCERNE CXL V6',
    'ATS SEDAN','CT6 SEDAN','CTS SEDAN','AVEO5','CAPTIVA SPORT FLEET',
    'CAVALIER 4-DR','CRUZE LT 4D SEDAN','IMPALA LT SEDAN','COBALT',
    'CIVIC 4DR','CIVIC COUPE','CIVIC SEDAN','XG','JX35','ALT 2.5 SV CVT',
    'ROGUE S AWD','VERSA SEDAN','L200','VUE-FWD V6','IMPREZA WAGON','JETTA SEDAN',
    'JETTA SPORTWAGEN','ACCORD COUPE','ACCORD SDN','ACCORD SEDAN','SONATA HYBRID');

    
update segments
set segment = 'Truck'
where segment is null
  and model in ('1/2 TON PICKUPS','CK 10','F-150','F150 PU','F-250 SD','K20 SILVERADO','F-350',
    'NEW SIERRA 1500','NV200','RAM','SIERRA','SIERRA 2500HD','SILVERADO','SILVERADO 2500HD',
    'SILVERADO CLASSIC 2500HD','SUPER DUTY F-250','SUPER DUTY F-250 SRW','SUPER DUTY F-350 SRW',
    'TUNDRA 4WD','TUNDRA 4WD TRUCK','F-350 SD','S-10 PICKUP','SILVERADO 3500HD');

    
update segments
set segment = 'SUV'
where segment is null
  and model in ('ACADIA','COUNTRY','ECONOLINE CARGO VAN','ENCLAVE CXL','ENVISION ESSENCE',
    'EXPRESS CARGO VAN','EXPRESS HD CARG','EXPRESS PASSENGER','GSX-R1000','MKC','MURANO SL AWD',
    'PROMASTER CITY CARGO VAN','SAVANA CARGO VAN','YUKON DENALI','ACADIA AWD SLT-');
    
      
update segments
set segment = 'Truck'
where segment = 'Pickup';

update segments
set segment = 'SUV'
where segment in('Crossover','Van');

select upper(store), 
  sum(unit_count) filter (where year_month = 201901 and segment = 'Car') as "January Car",
  sum(unit_count) filter (where year_month = 201901 and segment = 'SUV') as "January SUV",
  sum(unit_count) filter (where year_month = 201901 and segment = 'Truck') as "January Truck",
  sum(unit_count) filter (where year_month = 201902 and segment = 'Car') as "February Car",
  sum(unit_count) filter (where year_month = 201902 and segment = 'SUV') as "February SUV",
  sum(unit_count) filter (where year_month = 201902 and segment = 'Truck') as "February Truck",
  sum(unit_count) filter (where year_month = 201903 and segment = 'Car') as "March Car",
  sum(unit_count) filter (where year_month = 201903 and segment = 'SUV') as "March SUV",
  sum(unit_count) filter (where year_month = 201903 and segment = 'Truck') as "March Truck",
  sum(unit_count) filter (where year_month = 201904 and segment = 'Car') as "April Car",
  sum(unit_count) filter (where year_month = 201904 and segment = 'SUV') as "April SUV",
  sum(unit_count) filter (where year_month = 201904 and segment = 'Truck') as "April Truck",  
  sum(unit_count) filter (where year_month = 201905 and segment = 'Car') as "May Car",
  sum(unit_count) filter (where year_month = 201905 and segment = 'SUV') as "May SUV",
  sum(unit_count) filter (where year_month = 201905 and segment = 'Truck') as "May Truck"  
from segments
group by store
order by store


select store, segment, sum(unit_count)
from segments
group by store, segment
order by store, segment


drop table if exists cur_inv;
create temp table cur_inv as
select inpmast_company_number, inpmast_stock_number, inpmast_vin, a.make, a.model, 
  substring(b.vehicletype, position('_' in b.vehicletype) + 1, 9) as segment
from arkona.xfm_inpmast a
left join ads.ext_make_model_classifications b on a.make = b.make and a.model = b.model
where current_row
  and type_n_u = 'U'
  and status = 'I';

select a.*, case when segment is null then 'null segment' end
from cur_inv a
order by model;

update cur_inv
set segment = 'Truck'
where segment = 'Pickup';

update cur_inv
set segment = 'SUV'
where segment in ('Crossover','Van');

update cur_inv
set segment = 'Truck'
where segment is null
  and model in ('1/2 TON PICKUPS','1500 PICKUP','C/K 1500','CLASSIC','CTS SEDAN','F-150',
    'F-250 SD','F-350 SD','K20 PICKUP 4 WH','S-10 PICKUP','SIERRA','SIERRA 1500 4WD',
    'SIERRA 2500HD','SILVERADO','SILVERADO 2500HD','SILVERADO 2500 HD','SILVERADO 3500HD',
    'SILVERADO 4WD C','SUPER DUTY F-250','SUPER DUTY F-250 SRW','TRUCK');

update cur_inv
set segment = 'Car'
where segment is null
  and model in ('ACCORD 4DR EX-L','ACCORD COUPE','ACCORD HYBRID','ACCORD LX 4DR','ACCORD SEDAN','ATS SEDAN',  
    'CIVIC SEDAN','LEGACY WAGON','REGAL SPORTBACK','S-CLASS');

update cur_inv
set segment = 'SUV'
where segment is null
  and model in ('EXPRESS CARGO VAN','TRANSIT VAN','YUKON DENALI','YUKON XL DENALI');


select case when left(trim(inpmast_stock_number), 1) = 'H' then 'RY2' else 'RY1' end as store,
  count(model) filter (where segment = 'Car') as car,
  count(model) filter (where segment = 'Truck') as truck,
  count(model) filter (where segment = 'SUV') as suv
-- select *  
from cur_inv  
group by case when left(trim(inpmast_stock_number), 1) = 'H' then 'RY2' else 'RY1' end