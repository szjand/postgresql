﻿-- 9/5
-- fuck, ben wants this report again, i dont remember how jon.vehicles was populated
-- ah, some help in the original emails
/* 
These are the 50 most recent from an age standpoint units to hit the used car lot.
 
What I am looking for is the Date Dave “pulled” the unit for the lot. How many days the vehicle sat in each recon department. Detail, Mechanical, Body. 
 
I have “WIP, Wait time etc etc.” and I know sometimes detail is done first, or second, or third in the process, so not too concerned with each department, just how long the unit sat in each step.
 
Make sense?
 
Nick

Ben:
So Jon, don’t worry about departments. Date pulled, time it sat before a department put it in WIP, 
how long in WIP, how long it sat waiting for another dept to touch it, etc. 
Then finish with total time it was in recon. 
We are late getting this to you. If possible we could use this Monday morning sometime. 

jon:
Good Enough?
 
All numbers are in days.
If a vehicle is pulled today and becomes available today, pulled = 1
 
I removed G36539GA & G33116B, they have been pulled multiple times (back on, to auction)
 
There are a few anomalies (negative wait times), the tool is not used as rigorously as it once was. There are multiple instances of work having been done, but no wip time logged.
 
Overall, I feel pretty good about the numbers (accuracy not flow)
 
Other wait times could be time in pricing buffer, incomplete tool usage, …

*/



create table jon.vehicles (
  stock_number citext,
  primary key (stock_number));

truncate jon.vehicles;
insert into jon.vehicles
select b.stocknumber
from ads.ext_vehicle_inventory_items b 
join ads.ext_vehicle_inventory_item_statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
  and c.category = 'RMFlagAV'
  and c.thruts > now()
where left(b.stocknumber, 1) <> 'H'  
limit 75;
  
select distinct category, status from ads.ext_vehicle_inventory_item_statuses limit 100

select * from ads.ext_vehicle_inventory_item_statuses limit 100

-- find the multiple pulls
select stock_number
from (
  select a.stock_number, b.vehicleinventoryitemid, b.vehicleitemid, c.category, c.status, c.fromts, c.thruts
  from jon.vehicles a
  join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
  join ads.ext_vehicle_inventory_item_statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
    and category = 'RMFlagPulled'
  order by a.stock_number, c.fromts, c.thruts) d
group by stock_number
having count(*) > 1  





select a.stock_number, b.vehicleinventoryitemid, b.vehicleitemid, c.category, c.status, c.fromts, c.thruts
from jon.vehicles a
join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
join ads.ext_vehicle_inventory_item_statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
  and (
    c.category in( 'RMFlagPulled', 'RMFlagAV')
    or c.status similar to '%(Dispatched|InProcess)')
where a.stock_number not in ('G35207RC','G36206X','G36352X','G37696GA')
--   and a.stock_number = '30097RZ'
  and c.fromts >= (
    select fromts
    from ads.ext_vehicle_inventory_item_statuses
    where vehicleinventoryitemid = c.vehicleinventoryitemid
      and category = 'RMFlagPulled')
order by a.stock_number, c.fromts, c.thruts


select x.* , date_trunc('hour', time_pulled), extract(days from time_pulled),extract(hours from time_pulled)
from (
select a.stock_number, 
  sum(c.thruts - c.fromts) filter (where category = 'RMFlagPulled') time_pulled
from jon.vehicles a
join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
join ads.ext_vehicle_inventory_item_statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
--   and category in( 'RMFlagPulled', 'RMFlagAV')
where a.stock_number not in ('G36539GA','G33116B')  
group by a.stock_number) x
order by x.stock_number


select a.stock_number, 
  sum(extract(days from (c.thruts - c.fromts))) filter (where category = 'RMFlagPulled') as days_pulled
from jon.vehicles a
join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
join ads.ext_vehicle_inventory_item_statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
--   and category in( 'RMFlagPulled', 'RMFlagAV')
where a.stock_number not in ('G36539GA','G33116B')  
group by a.stock_number
order by a.stock_number


select stock_number, mech_wait - coalesce(mech_wip, 0) as mech_wait, mech_wip, 
  body_wait - coalesce(body_wip, 0) as body_wait, body_wip, 
  detail_wait - coalesce(detail_wip, 0) as detail_wait, detail_wip, 
  case 
    when pulled - (coalesce(mech_wait, 0) + coalesce(body_wait, 0) + coalesce(detail_wait, 0)) < 0 then 0
    else pulled - (coalesce(mech_wait, 0) + coalesce(body_wait, 0) + coalesce(detail_wait, 0))
  end as other_wait, pulled
from (
  select a.stock_number,
    sum(extract(days from (c.thruts - c.fromts))) filter (where category = 'RMFlagPulled') + 1 as pulled,
    sum(extract(days from (c.thruts - c.fromts))) filter (where status = 'BodyReconProcess_InProcess') + 1 as body_wip,
    sum(extract(days from (c.thruts - c.fromts))) filter (where status = 'AppearanceReconProcess_InProcess') + 1 as detail_wip,
    sum(extract(days from (c.thruts - c.fromts))) filter (where status = 'MechanicalReconProcess_InProcess') + 1 as mech_wip,
    sum(extract(days from (c.thruts - c.fromts))) filter (where category = 'MechanicalReconDispatched') + 1 as mech_wait,
    sum(extract(days from (c.thruts - c.fromts))) filter (where category = 'AppearanceReconDispatched') + 1 as detail_wait,
    sum(extract(days from (c.thruts - c.fromts))) filter (where category = 'BodyReconDispatched') + 1 as body_wait
  from jon.vehicles a
  join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
  join ads.ext_vehicle_inventory_item_statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
    and (
      c.category in( 'RMFlagPulled', 'RMFlagAV')
      or c.status similar to '%(Dispatched|InProcess)')
  where a.stock_number not in ('G35207RC','G36206X','G36352X','G37696GA','G35907XZ','G37077C','G37831P')
    and c.fromts >= (
      select fromts
      from ads.ext_vehicle_inventory_item_statuses
      where vehicleinventoryitemid = c.vehicleinventoryitemid
        and category = 'RMFlagPulled')
  group by a.stock_number) x    
order by x.stock_number



