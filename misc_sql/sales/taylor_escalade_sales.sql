﻿/*
Hey Jon,

Could you please pull a list of guests that have purchased a new Cadillac Escalade or Escalade ESV from 2012–2018? Would need name, date purchased, year/model/trim, cash or finance and phone number. Today would be ideal  or whenever it fits. 

Thank you Jon. 

Taylor Monson
02/18/2020
*/

select a.bopmast_stock_number as stock_number, a.bopmast_vin as vin, 
  case record_type when 'C' then 'cash' else 'finance' end as "cash/fin", bopmast_search_name as customer, a.date_capped as sale_date, b.year, b.make, b.model, b.body_style, c.phone_number
from arkona.xfm_bopmast a
join arkona.xfm_inpmast b on a.bopmast_vin = b.inpmast_vin
  and b.current_row
  and b.make = 'cadillac'
  and b.model like 'ESC%'
join arkona.xfm_bopname c on a.buyer_number = c.bopname_record_key
  and c.current_row
where a.current_row
  and a.record_status = 'U'
  and a.vehicle_type = 'N'
  and a.date_capped between '01/01/2012' and '12/31/2018'
  and similarity(a.bopmast_search_name, c.bopname_search_name) > .2


/*
This list was perfect. Would you mind doing the same thing for Buick Envision/Encore buyers from 2015 to current?

Taylor Monson
02/19/20
*/

drop table if exists wtf;
create temp table wtf as
select a.bopmast_stock_number as stock_number, a.bopmast_vin as vin, 
  case record_type when 'C' then 'cash' else 'finance' end as "cash/fin", bopmast_search_name as customer, 
  a.date_capped as sale_date, b.year, b.make, b.model, b.body_style, c.phone_number, c.bopname_search_name, similarity(a.bopmast_search_name, c.bopname_search_name)
from arkona.xfm_bopmast a
join arkona.xfm_inpmast b on a.bopmast_vin = b.inpmast_vin
  and b.current_row
  and b.make = 'buick'
  and b.model in ('envision','encore')
join arkona.xfm_bopname c on a.buyer_number = c.bopname_record_key
  and c.current_row
where a.current_row
  and a.record_status = 'U'
  and a.vehicle_type = 'N'
  and a.date_capped between '01/01/2015' and current_date
--   and similarity(a.bopmast_search_name, c.bopname_search_name) > .2
-- order by similarity(a.bopmast_search_name, c.bopname_search_name)
order by bopmast_vin

select *
from wtf a
join (select vin from wtf group by vin having count(*) > 1) b on a.vin = b.vin

-- get rid of the rydell deals on vehicles subsequently sold as rentals
delete 
from wtf
where stock_number in ('29747','25301','29660','G33385','29839','28527','31418','29840','G33294')

-- and misc others
delete 
-- select *
from wtf
where (
  (stock_number = '32114' and bopname_Search_name like 'citizens%')
  or
  (stock_number = 'G36236' and bopname_Search_name like 'nissan%')
  or
  (stock_number = 'G33651' and bopname_Search_name like 'university%'))

update wtf set customer = 'WEBER, JAMES ALLEN' where stock_number = '29660R'

select *
from wtf  
order by similarity

and just the fields taylor wants

select a.bopmast_stock_number as stock_number, a.bopmast_vin as vin, 
  case record_type when 'C' then 'cash' else 'finance' end as "cash/fin", bopmast_search_name as customer, a.date_capped as sale_date, b.year, b.make, b.model, b.body_style, c.phone_number

select stock_number, vin, "cash/fin", customer, sale_date, year, make, model, body_style, phone_number
from wtf  

/*
Customers that have leased/purchased new Tahoe, Suburban, Yukon, Yukon XL, Escalade and Escalade ESV vehicles from 2015 to current. 

Name
Phone #
VIN/stock #
Year/make/model/trim
Date purchased/leased
Cash/lease/finance

Could you please pull this as soon as you have a chance?

Taylor Monson
02/26/20
*/


drop table if exists wtf;
create temp table wtf as
select a.bopmast_stock_number as stock_number, a.bopmast_vin as vin, 
  case record_type when 'C' then 'cash' else 'finance' end as "cash/fin", bopmast_search_name as customer, 
  a.date_capped as sale_date, b.year, b.make, b.model, b.body_style, c.phone_number, c.bopname_search_name, similarity(a.bopmast_search_name, c.bopname_search_name)
from arkona.xfm_bopmast a
join arkona.xfm_inpmast b on a.bopmast_vin = b.inpmast_vin
  and b.current_row
  and b.model in ('tahoe','suburban','yukon','yukon xl','escalade','escalade esv')
join arkona.xfm_bopname c on a.buyer_number = c.bopname_record_key
  and c.current_row
where a.current_row
  and a.record_status = 'U'
  and a.vehicle_type = 'N'
  and a.date_capped between '01/01/2015' and current_date
  and similarity(a.bopmast_search_name, c.bopname_search_name) > .2
-- order by similarity(a.bopmast_search_name, c.bopname_search_name)
-- order by bopmast_vin

select * from wtf


select *
from wtf a
join (select vin from wtf group by vin having count(*) > 1) b on a.vin = b.vin
order by a.vin

-- get rid of the rydell deals on vehicles subsequently sold as rentals
delete 
-- select *
from wtf
where customer like 'RYDELL%'

-- spreadsheet: taylor_large_suv_sales
select stock_number, vin, "cash/fin", customer, sale_date, year, make, model, body_style, phone_number
from wtf  
