﻿select *
from arkona.xfm_inpmast a
where inpmast_stock_number = 'H12773'
order by row_from_date


select * 
from greg.uc_daily_snapshot_beta_1
where sale_date = the_date
  and sale_date between '04/01/2022' and '04/30/2022'
  and right(stock_number, 1) = 'P'

-- came into inventory before 04/30 and sold after '04/30
select * -- april: 404
from ads.ext_vehicle_inventory_items a
where a.fromts::date < '05/01/2022'  
  and a.thruts::date > '04/30/2022'

select * -- may: 384
from ads.ext_vehicle_inventory_items a
where a.fromts::date < '06/01/2022'  
  and a.thruts::date > '05/31/2022'  

select year_month, split_part(typ, '_', 2), count(*)
from ads.ext_vehicle_sales a
join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and right(b.stocknumber, 1) = 'P'
join dds.dim_date c on a.soldts::date = c.the_date
  and c.year_month between 202204 and 202205
where a.status <> 'VehicleSale_SaleCanceled'
group by year_month, split_part(typ, '_', 2)

select * from pdq.opcodes


-- P sales
drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
-- truncate step_1;
-- insert into step_1
select year_month, store, page, line, line_label, control, sum(unit_count) as unit_count
from (
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 202205 -----------------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 202205  ---------------------------------------------------------------------
      and b.page = 16 and b.line between 1 and 14 -- used cars

    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y') h
where right(control, 1) = 'P'  
group by year_month, store, page, line, line_label, control
order by store, page, line;

april: retail: 26  ws: 10
may: retail: 41  ws: 16
select year_month, sum(unit_count)
from step_1
where line between 8 and 10
group by year_month

select year_month, sum(unit_count)
from step_1
group by year_month


-- used car gross accounts
-- ry1 by line, sales only
select d.gl_account, b.line, e.description
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202204
  and b.page = 16
  and b.line between 1 and 14
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
  and e.account_type_code = '4'
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store = 'ry1'  


drop table if exists gross_accounts;
create temp table gross_accounts as
select distinct d.gl_account, e.account_type, e.department, e.description
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 202204 and 202205
  and b.page = 16
  and b.line between 1 and 14
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
union
-- f/i accounts
select distinct d.gl_account, e.account_type, e.department, e.description
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 202204 and 202205
  and b.page = 17
  and b.line between 1 and 20
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account;  

-- retail only gross
select b.year_month, -- d.department, d.description, 
  sum(-a.amount) filter (where d.department in ('Used Vehicle','Auto Outlet')) as front,
  sum(-a.amount) filter (where d.department = 'Finance') as fin
-- select a.amount, b.*, d.*
from fin.fact_gl a
join step_1 b on a.control = b.control
join fin.dim_account c on a.account_key = c.account_key
join gross_accounts d on c.account = d.gl_account
where a.post_status = 'Y'
  and line in (2,4,5)
group by b.year_month --, b.store, d.department, d.description

select year_month, sum(recon_gross_running + post_sale_recon_gross)
from step_1 a
left join greg.uc_daily_snapshot_beta_1 b on a.control = b.stock_number
  and b.the_date = b.sale_date
where a.line < 8
  and a.unit_count <> - 1
group by year_month  