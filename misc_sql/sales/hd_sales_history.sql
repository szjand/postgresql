﻿select * 
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
  and b.model similar to '%(2500|3500)%'
order by delivery_date desc   


drop table if exists base;
create temp table base as
select distinct c.the_year, c.month_name, 
  case when a.sale_type = 'F' then 'Fleet' end as fleet, a.bopmast_stock_number as stock_number, a.bopmast_vin as vin, 
  a.bopmast_search_name as customer, a.date_capped, a.primary_salespers, d.fullname as consultant, b.year, b.make,
  bb.style -> 'attributes' ->> 'trim' as trim_level, 
  case 
    when b.make = 'GMC' then left(bb.style -> 'model' ->> '$value', 13)
    when b.make = 'chevrolet' then left(bb.style -> 'model' ->> '$value', 16)
  end as model
from arkona.xfm_bopmast a
join arkona.xfm_inpmast b on a.bopmast_vin = b.inpmast_vin
  and b.current_row
  and b.model similar to '%(2500|3500)%'
join dds.dim_date c on a.date_capped = c.the_date  
join chr.describe_vehicle aa on a.bopmast_vin = aa.vin
join jsonb_array_elements(aa.response->'style') as bb(style) on true
left join ads.ext_dim_salesperson d on a.primary_salespers = d.salespersonid
  and d.storecode = 'ry1'
  and d.salespersontypecode = 's'
where a.current_row
  and a.date_capped > '12/31/2014'
  and a.vehicle_type = 'N'
  and a.record_status = 'U' 
  and b.model not like 'SAV%';
create unique index on base(vin) ; 

select * from base order by primary_salespers, consultant

update base
set consultant = 'Eric Johnson'
where primary_salespers = 'EJO'
  and consultant is null;

update base
set consultant = 'Joshua Barker'
where primary_salespers = 'BAR'
  and consultant is null;  

update base
set consultant = 'Bobby Pearson'
where primary_salespers = 'RPE'
  and consultant is null;  



select * from base where date_capped between '01/01/2017' and '12/31/2018'

select distinct model, trim_level from base where date_capped between '01/01/2017' and '12/31/2018' 

select b.year_month, 
  count(vin) filter (where model = 'Sierra 2500HD' and trim_level = 'Denali') as "Sierra 2500HD Denali",
  count(vin) filter (where model = 'Sierra 2500HD' and trim_level = 'SLE') as "Sierra 2500HD SLE",
  count(vin) filter (where model = 'Sierra 2500HD' and trim_level = 'SLT') as "Sierra 2500HD SLT",
  count(vin) filter (where model = 'Sierra 2500HD' and trim_level is null) as "Sierra 2500HD",
  count(vin) filter (where model = 'Sierra 3500HD' and trim_level = 'Denali') as "Sierra 3500HD Denali",
  count(vin) filter (where model = 'Sierra 3500HD' and trim_level = 'SLE') as "Sierra 3500HD SLE",
  count(vin) filter (where model = 'Sierra 3500HD' and trim_level = 'SLT') as "Sierra 3500HD SLT",
  count(vin) filter (where model = 'Sierra 3500HD' and trim_level is null) as "Sierra 3500HD",
  count(vin) filter (where model = 'Silverado 2500HD' and trim_level = 'High Country') as "Silverado 2500HD High Country",
  count(vin) filter (where model = 'Silverado 2500HD' and trim_level = 'LT') as "Silverado 2500HD LT",
  count(vin) filter (where model = 'Silverado 2500HD' and trim_level = 'LTZ') as "Silverado 2500HD LTZ",
  count(vin) filter (where model = 'Silverado 2500HD' and trim_level = 'Work Truck') as "Silverado 2500HD Work Truck",
  count(vin) filter (where model = 'Silverado 3500HD' and trim_level = 'LT') as "Silverado 3500HD LT",
  count(vin) filter (where model = 'Silverado 3500HD' and trim_level = 'LTZ') as "Silverado 3500HD LTZ",
  count(vin) filter (where model = 'Silverado 3500HD' and trim_level = 'Work Truck') as "Silverado 3500HD Work Truck"
from base a
join dds.dim_date b on a.date_capped = b.the_date
where date_capped between '01/01/2017' and '12/31/2018' 
group by b.year_month
order by b.year_month

select * from base where trim_level is null


select model, trim_level, count(*) 
from base where date_capped between '01/01/2017' and '12/31/2018'
group by model, trim_level



