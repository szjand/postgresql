﻿/*
Im working on some used car stuff and wondering if you can help me find something

I am looking for a total number broken out by 
“P”
“X”
“L” 
Units we added to used car inventory as an acquisition by month from January 2019 thru March 2022

I have collected the units for the time period we took in as trade ins at the curb, but don’t know if you have an easy way of pulling the above.



Can you send me info for the Honda Nissan store like you did for me for GM last week.

Street Purchases/Auction Purchases/ Lease Purchases for each month from January 2019 thru March 2022 please?

*/

-- honda
select b.year_month, 
	count(a.stocknumber) filter (where right(trim(stocknumber),1) = 'P') as P, 
	count(a.stocknumber) filter (where right(trim(stocknumber),1) = 'X') as X, 
	count(a.stocknumber) filter (where right(trim(stocknumber),1) = 'L') as L 
from ads.ext_vehicle_inventory_items a 
join ( 
	select distinct year_month, first_of_month, last_of_month  
	from dds.dim_date  
	where year_month between 201901 and 202203) b on a.fromts::date between b.first_of_month and b.last_of_month 
where fromts::Date between '01/01/2019' and '03/31/2022' 
	and right(trim(stocknumber),1) in ('P','X','L')  
	and left(stocknumber, 1) = 'H' 
group by b.year_month 

-- gm
select b.year_month, 
	count(a.stocknumber) filter (where right(trim(stocknumber),1) = 'P') as P, 
	count(a.stocknumber) filter (where right(trim(stocknumber),1) = 'X') as X, 
	count(a.stocknumber) filter (where right(trim(stocknumber),1) = 'L') as L 
from ads.ext_vehicle_inventory_items a 
join ( 
	select distinct year_month, first_of_month, last_of_month  
	from dds.dim_date  
	where year_month between 201901 and 202203) b on a.fromts::date between b.first_of_month and b.last_of_month 
where fromts::Date between '01/01/2019' and '03/31/2022' 
	and right(trim(stocknumber),1) in ('P','X','L')  
	and left(stocknumber, 1) = 'G' 
group by b.year_month 