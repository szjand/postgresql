﻿avg_recon_hours


drop table if exists tmp_recon_ros;
create temp table tmp_recon_ros as
select control as stock_number, delivery_date, doc as ro, 
--   sum(case when c.account_type = 'sale' then amount else 0 end) as sales,
--   sum(case when c.account_type = 'cogs' then amount else 0 end) as cogs,
  sum(amount) as gross
from fin.fact_gl a
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join (
  select stock_number, delivery_date
  from sls.deals
  where store_code = 'ry1'
    and vehicle_type_code = 'u'
    and year_month between 201901 and 201907
  group by stock_number, delivery_date
  having sum(unit_count) > 0  ) e on a.control = e.stock_number
where a.post_status = 'Y'
  and
    case
      when c.account in ('124000','124001','124100','124101') then d.journal_code in ('SVI','POT')
      when c.account in (
        select gl_account
        from fin.fact_fs a
        inner join fin.dim_fs_account b on a.fs_account_key = b.fs_account_key
        inner join fin.dim_fs c on a.fs_key = c.fs_key
        inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
        inner join fin.dim_account e on b.gl_account = e.account
        where c.page = 16
          and e.department_code = 'uc'
          and e.account_type_code = '5'
          and d.store = 'ry1'
        group by gl_account) then d.journal_code in ('SVI','SCA','POT')
    end
group by control, doc, delivery_date
having sum(amount) <>  0;   
create unique index on tmp_recon_ros(stock_number, ro);

-- no, i don't think this is it
select flagdeptcode, count(*), sum(flaghours)
from (
select a.stock_number, a.delivery_date, c.flagdeptcode, sum(b.flaghours) as flaghours
-- select * 
from tmp_Recon_ros a
left join ads.ext_fact_repair_order b on a.ro = b.ro -- limit 10
-- left join ads.ext_dim_service_Writer c on b.servicewriterkey = c.servicewriterkey
left join ads.ext_dim_tech c on b.techkey = c.techkey
group by a.stock_number, a.delivery_date, c.flagdeptcode
having sum(b.flaghours) <> 0) x
group by flagdeptcode


select a.stock_number,
  sum(b.flaghours) filter (where flagdeptcode in ('MR','QL','QS')) as mech_flag,
  sum(b.flaghours) filter (where flagdeptcode = 'BS') as body_flag,  
  sum(b.flaghours) filter (where flagdeptcode = 'RE') as detail_flag   
-- select * 
from tmp_Recon_ros a
join ads.ext_fact_repair_order b on a.ro = b.ro -- limit 10
-- left join ads.ext_dim_service_Writer c on b.servicewriterkey = c.servicewriterkey
join ads.ext_dim_tech c on b.techkey = c.techkey
  and currentrow = true
group by a.stock_number
having sum(b.flaghours) <> 0

-- 7/23/19 sent this to greg
select 'mech' as dept, count(distinct stock_number) as units, round(sum(b.flaghours), 0) as flag_hours, 
  round(sum(b.flaghours)/count(distinct stock_number), 1) as avg_flag_hours
-- select * 
from tmp_Recon_ros a
join ads.ext_fact_repair_order b on a.ro = b.ro -- limit 10
  and b.flaghours <> 0
join ads.ext_dim_tech c on b.techkey = c.techkey
  and currentrow = true
  and flagdeptcode in ('MR','QL','QS')
union
select 'body', count(distinct stock_number), round(sum(b.flaghours), 0), 
  round(sum(b.flaghours)/count(distinct stock_number), 1)
-- select * 
from tmp_Recon_ros a
join ads.ext_fact_repair_order b on a.ro = b.ro -- limit 10
  and b.flaghours <> 0
join ads.ext_dim_tech c on b.techkey = c.techkey
  and currentrow = true
  and flagdeptcode= 'BS' 
union
select 'detail', count(distinct stock_number), round(sum(b.flaghours), 0), 
  round(sum(b.flaghours)/count(distinct stock_number), 1)
-- select * 
from tmp_Recon_ros a
join ads.ext_fact_repair_order b on a.ro = b.ro -- limit 10
  and b.flaghours <> 0
join ads.ext_dim_tech c on b.techkey = c.techkey
  and currentrow = true
  and flagdeptcode= 'RE'   


-- he (greg) will want to break out pdr

select c.opcode, c.description, count(*)
from tmp_Recon_ros a
join ads.ext_fact_repair_order b on a.ro = b.ro -- limit 10
  and b.flaghours <> 0
join ads.ext_dim_opcode c on b.opcodekey = c.opcodekey
where a.ro like '18%' 
group by c.opcode, c.description
order by c.description


-- from above: 1069 units 8889 hours
-- 19% of body shop hours are pdr
select 'body', count(distinct stock_number), 
  sum(b.flaghours) filter (where d.opcode = 'PDR') as pdr_hours, 
  sum(b.flaghours) filter (where d.opcode <> 'PDR') as non_pdr_hours, 
  round(sum(b.flaghours)/count(distinct stock_number), 1)
-- select * 
from tmp_Recon_ros a
join ads.ext_fact_repair_order b on a.ro = b.ro -- limit 10
  and b.flaghours <> 0
join ads.ext_dim_tech c on b.techkey = c.techkey
  and currentrow = true
  and flagdeptcode= 'BS' 
join ads.ext_dim_opcode d on b.opcodekey = d.opcodekey  

break out pdr vs non-pdr hours



