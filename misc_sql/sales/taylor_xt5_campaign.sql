﻿/*
9/9/19
Hey Jon,

Would you be able to pull two lists of data for me to help with an XT5 campaign? Here’s what I’m looking for:

-	List #1: Anyone that has leased any Cadillac, Traverse, Enclave or Acadia in the last 18 months – 3 years.
o	Name 
o	Phone number
o	Year/Make/Model of what they leased
o	Date they leased
o	Date their lease expires
o	Sales person on the deal
-	List #2: Anyone that has purchased any Cadillac, Traverse, Enclave or Acadia in the last 1-4 years:
o	Name 
o	Phone number
o	Year/Make/Model of what they purchased
o	Date they purchased
o	Sales person on the deal

and then, of course, morgan has 2 cents
This is what the BDC will need to call out on this XT5/Detail campaign, I can also send out an email campaign.

Name
Phone Number
Email
Address
Date Purchased/Lease
Is it a Purchase or Lease
Last miles recorded in service 
Number of details we can schedule a day
What is the process of picking up/dropping off/like dates times
Who do we notify
How do we want to schedule these (like what notes put in scheduler so Leslie knows what these are about)
Are there certain days we want these scheduled for

*/

need to upate arkona.ext_bopslsp, ext_sdprhdr, ext_bopvref


-- question, does the bopname_key in inpmast match the current bopname_key in bopvref
-- -- bopvref codes
-- 2 = Co-Buyer on the Deal
-- C = Service Customer
-- T = Trade In
-- 4 = ?? (Of the 212409 rows in BOPVREF table, only 59 are type 4)
drop table if exists test_1;
create temp table test_1 as
select *
from (
  select a.bopmast_search_name, a.buyer_number, b.bopname_key, 
    c.customer_key, start_date, c.end_Date, c.type_code, c.salesperson,
    c.vin, row_number() over (partition by c.vin order by c.start_date desc)
  from list_1 a
  join arkona.xfm_inpmast b on a.bopmast_vin = b.inpmast_vin
    and b.current_row
  join arkona.ext_bopvref c on a.bopmast_vin = c.vin  
    and c.type_code <> '2' -- this gets rid of false mismatches
  order by a.bopmast_vin, c.start_date desc ) aa
where row_number = 1;

select * 
from test_1
where buyer_number <> customer_key

--------------------------------------------------------------------------------------
--< list_1
--------------------------------------------------------------------------------------
so with this new info, start over on list_1
lets be methodical, want cust info at time of purchase and current cust info

1. bopmast, limited to vehicle spec, and purchser is current owner, within limits of bopvref

-- at this point i am as good is i'll get, these are legitimate customers
drop table if exists list_1_1 cascade;
create temp table list_1_1 as
select aa.bopmast_vin, aa.buyer_number, aa.bopmast_Search_name, aa.lease_date, aa.lease_term,
  aa.lease_exp, aa.sales_person_name, aa.year, aa.make, aa.model, bb.bopname_search_name,
  similarity(bopmast_search_name, bopname_search_name),
  bb.phone_number, bb.email_address, bb.address_1 as address, bb.city, bb.state_code as state,
  bb.zip_code as zip
from ( -- bopmast, inpmast, bopslsp
  select a.bopmast_vin, a.buyer_number, a.bopmast_search_name,
    date_capped as lease_date, lease_term, date_capped + lease_term * 30 as lease_exp,
    d.sales_person_name, b.year, make, model,
    c.customer_key, row_number() over (partition by c.vin order by c.start_date desc)
  from arkona.xfm_bopmast a
  join arkona.xfm_inpmast b on a.bopmast_vin = b.inpmast_vin
    and b.current_row
    and (b.make = 'cadillac' or model in ('traverse', 'enclave', 'acadia'))
  join arkona.ext_bopvref c on a.bopmast_vin = c.vin
    and c.type_code <> '2' -- eliminate cobuyer
  left join arkona.ext_bopslsp d on a.bopmast_company_number = d.company_number
    and a.primary_salespers = d.sales_person_id    
  where a.current_row
    and a.date_capped between current_date - '3 years'::interval and current_date - '18 months'::interval
    and a.record_status = 'U'
    and a.sale_type = 'L') aa
left join arkona.xfm_bopname bb on aa.buyer_number = bb.bopname_record_key
  and bb.current_row 
where row_number = 1
  and buyer_number = customer_key
  and similarity(bopmast_search_name, bopname_search_name) > .1
order by similarity(bopmast_search_name, bopname_search_name);
create unique index on list_1_1(bopmast_vin);

using list_1_1, flesh out the requested list with service info

select * from list_1_1 where bopname_search_name like 'schmie%'

-- sent this to taylor as list_1
select a.bopname_search_name as name, a.phone_number, a.email_address, a.address,
  a.city, a.state, a.zip, 
  a.lease_date, a.lease_term, a.lease_exp, a.sales_person_name as consultant, 
  b.close_date as last_service, b.odometer_out as last_miles,
  a.year, a.make, a.model
-- select *  
from list_1_1 a
left join (
  select *
    from (
      select aa.bopmast_vin, bb.cust_name, bb.customer_key, bb.close_date, bb.odometer_out,
        row_number() over (partition by aa.bopmast_vin order by bb.close_date desc)
      from list_1_1 aa
      join arkona.ext_sdprhdr bb on aa.bopmast_vin = bb.vin
        and bb.cust_name not in ('*VOIDED REPAIR ORDER*')) cc
    where row_number = 1) b on a.bopmast_vin = b.bopmast_vin

--------------------------------------------------------------------------------------
--/> list_1
--------------------------------------------------------------------------------------

name	phone_number	email_address	address	city	state	zip	lease_date	lease_term	lease_exp	sales_person_name	last_serv	last_miles	year	make	model
bopname
  name	phone_number	email_address	address	city	state	zip	
bopmast
  lease_date	lease_term	lease_exp	
bopslsp
  sales_person_name	
sdprhdr
  last_serv	last_miles	
inpmast
  year	make	model


-----------------------------------------------------------------------------------------------
--< list 2
-----------------------------------------------------------------------------------------------
-- at this point i am as good is i'll get, these are legitimate customers
drop table if exists list_2_1 cascade;
create temp table list_2_1 as
select aa.bopmast_vin, aa.buyer_number, aa.bopmast_Search_name, aa.purch_date, aa.sales_person_name, 
  aa.year, aa.make, aa.model, bb.bopname_search_name,
--   similarity(bopmast_search_name, bopname_search_name)
  bb.phone_number, bb.email_address, bb.address_1 as address, bb.city, bb.state_code as state,
  bb.zip_code as zip
from ( -- bopmast, inpmast, bopslsp
  select a.bopmast_vin, a.buyer_number, a.bopmast_search_name,
    date_capped as purch_date, 
    d.sales_person_name, b.year, make, model,
    c.customer_key, row_number() over (partition by c.vin order by c.start_date desc)
  from arkona.xfm_bopmast a
  join arkona.xfm_inpmast b on a.bopmast_vin = b.inpmast_vin
    and b.current_row
    and (b.make = 'cadillac' or model in ('traverse', 'enclave', 'acadia'))
  join arkona.ext_bopvref c on a.bopmast_vin = c.vin
    and c.type_code <> '2' -- eliminate cobuyer
  left join arkona.ext_bopslsp d on a.bopmast_company_number = d.company_number
    and a.primary_salespers = d.sales_person_id    
  where a.current_row
    and  a.date_capped between current_date - '4 years'::interval and current_date - '1 year'::interval
    and a.record_status = 'U'
    and a.sale_type not in ('L','W')) aa
left join arkona.xfm_bopname bb on aa.buyer_number = bb.bopname_record_key
  and bb.current_row 
where row_number = 1
  and buyer_number = customer_key
  and similarity(bopmast_search_name, bopname_search_name) > .5  -- these are messier
order by similarity(bopmast_search_name, bopname_search_name);
create unique index on list_2_1(bopmast_vin);


select *
from list_2_1

-- and the list for taylor
select a.bopname_search_name as name, a.phone_number, a.email_address, a.address,
  a.city, a.state, a.zip, 
  a.purch_date, a.sales_person_name as consultant, 
  b.close_date as last_service, b.odometer_out as last_miles,
  a.year, a.make, a.model
-- select *  
from list_2_1 a
left join (
  select *
    from (
      select aa.bopmast_vin, bb.cust_name, bb.customer_key, bb.close_date, bb.odometer_out,
        row_number() over (partition by aa.bopmast_vin order by bb.close_date desc)
      from list_2_1 aa
      join arkona.ext_sdprhdr bb on aa.bopmast_vin = bb.vin
        and bb.cust_name not in ('*VOIDED REPAIR ORDER*')) cc
    where row_number = 1) b on a.bopmast_vin = b.bopmast_vin
    
-----------------------------------------------------------------------------------------------
--/> list 2
-----------------------------------------------------------------------------------------------



