﻿/*
Jon

Can you help me with this?

I am trying to get some info for the Auto Outlet

Thanks

Best,

Nick Shirek
Month	January	February	March	April	May
Front Gross					
F+I Gross					
Recon Gross					
Total Gross					

*/

/* 
1/18/22
This looks great Jon

Can I get it for 2020 and 2019 as well?

Nick Shirek
*/

select distinct account_type_code, account_type from fin.dim_Account

select * from fin.dim_account where account in ('180802','180803','191002', '165102')

select * from fin.fact_gl limit 10

drop table if exists outlet_accounts;
create temp table outlet_accounts as
select d.gl_account, b.line, e.description, e.account_type, d.gm_account
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202112
  and b.page in (16,17)
  and b.line between 1 and 20
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
  and e.current_row
  and e.account_type_code in  ('4', '5', '7', '9')
  and e.description not like '%RECON%'
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store = 'ry1'  
  and e.description like '%RAO%'
order by d.gl_account;  

drop table if exists acct_sales;
create temp table acct_sales as 
select b.account, b.description as acct_desc, a.control, a.amount, d.description as gl_desc, b.department, c.the_date 
from fin.fact_gl a 
join fin.dim_account b on a.account_key = b.account_key 
join outlet_accounts bb on b.account = bb.gl_account 
join dds.dim_date c on a.date_key = c.date_key 
--   and c.the_year = 2021  
--   and c.the_year = 2020
  and c.the_year = 2019
join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key   
where a.post_status = 'Y' 
order by a.control, b.account 


drop table if exists control_year_month;
create temp table control_year_month as
-- select b.account, b.description as acct_desc, a.control, a.amount, d.description as gl_desc, b.department, c.the_date
select distinct a.control, d.year_month
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
join outlet_accounts bb on b.account = bb.gl_account
join dds.dim_date c on a.date_key = c.date_key
  and c.the_year = 2019 -- 2020 -- 2021  -- and c.year_month = 202111
join sls.deals_by_month d on a.control = d.stock_number  
-- join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key  
where a.post_status = 'Y'
  AND a.control not in ('G42929P','G41879BB'); -- 2021

-- for 2020, 2019
drop table if exists control_year_month_2;
create temp table control_year_month_2 as
select * 
from (
	select a.control, a.year_month, row_number() over (partition by a.control order by a.year_month desc) as seq
	from control_year_month a) b
where seq = 1;
  
-- based on  sls.update_deals_gross_by_month()
drop table if exists sales_gross;
create table sales_gross as
select a.control,
	sum(case when gross_category = 'front' and account_type in ('sale', 'Other Income')
		then -amount else 0 end) as front_sales,
	sum(case when gross_category = 'front' and account_type = 'cogs'
		then amount else 0 end) as front_cogs,
	sum(case when gross_category = 'front' then -amount else 0 end) as front_gross,
	sum(case when gross_category = 'fi' and account_type in ('sale', 'Other Income')
		then -amount else 0 end) as fi_sales,
	sum(case when gross_category = 'fi' and account_type = 'cogs' then amount else 0 end) as fi_cogs,
--       0 as fi_gross,
	coalesce(sum(amount) filter (where gl_account in ('180601','180801','180803')), 0) as acq_fee,  
	sum(case when gross_category = 'fi' then -amount else 0 end) as fi_gross
from sls.deals_accounting_detail a
-- join control_year_month b on a.control = b.control -- 2021
join control_year_month_2 b on a.control = b.control -- 2020, 2019
group by a.control;


-- ros
drop table if exists control_ro;
create temp table control_ro as
select aa.control, bb.ro
from (
	select a.control, b.year_month
	from acct_sales a
	join sls.deals_by_month b on a.control = b.stock_number
-- 	where a.control not in ('G42929P','G41879BB') -- 2021
	where a.control not in (select control from control_year_month group by control having count(*) > 1) -- 2020, 2019
	group by a.control, b.year_month) aa
left join greg.uc_recon_ros bb on aa.control = bb.stock_number;


drop table if exists ro_gross ;
create table ro_gross as
select control, sum(-amount) as gross
from fin.fact_gl a
join fin.dim_Account b on a.account_key = b.account_key
  and b.account_type in ('sale','cogs')
join (
select bb.ro
from (
	select a.control, b.year_month
	from acct_sales a
	join sls.deals_by_month b on a.control = b.stock_number
	where a.control not in ('G42929P','G41879BB')
	group by a.control, b.year_month) aa
left join greg.uc_recon_ros bb on aa.control = bb.stock_number) c on a.control = c.ro  
where a.post_status = 'Y'  
group by control;

-- gross by stock number
select a.*, front_gross, coalesce(fi_gross, 0), coalesce(c.recon_gross, 0)
from control_year_month a
left join sales_gross b on a.control = b.control
left join (
	select a.control, sum(gross) as recon_gross
	from control_ro a
	left join ro_gross b on a.ro = b.control
	group by a.control) c on a.control = c.control

-- year_month columns
select d.*, front_gross + fi_gross + recon_gross as total_gross 
from (
	select a.year_month, sum(front_gross)::integer as front_gross, sum(coalesce(fi_gross, 0))::integer as fi_gross, 
		sum(coalesce(c.recon_gross, 0))::integer as recon_gross
	from control_year_month a
	left join sales_gross b on a.control = b.control
	left join (
		select a.control, sum(gross) as recon_gross
		from control_ro a
		left join ro_gross b on a.ro = b.control
		group by a.control) c on a.control = c.control	
-- 	where year_month > 202100	-- 2021
-- 	where year_month between 202001 and 202012 -- 2020
	where year_month between 201901 and 201912 -- 2019
	group by a.year_month) d	
order by year_month



