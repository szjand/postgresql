﻿
select * from sls.personnel where last_name = 'wilde'

select bopmast_id, unit_count 
from sls.deals 
where primary_sc = 'WDE' or secondary_sc = 'WDE' order by run_date

select bopmast_id, year_month, unit_count 
-- select *
from sls.deals_by_month
where (psc_last_name = 'wilde' or ssc_last_name = 'wilde')

select record_key, record_status, bopmast_Stock_number, date_capped
from arkona.xfm_bopmast
where (primary_salespers = 'WDE' or secondary_slspers2 = 'WDE')
  and record_key not in (
    select bopmast_id
    -- select *
    from sls.deals_by_month
    where (psc_last_name = 'wilde' or ssc_last_name = 'wilde'))  
  and date_capped > '12/31/2016'
  and record_status = 'U'



select 
  sum(unit_count) filter (where year_month between 201701 and 201712) as sales_2017,
  sum(unit_count) filter (where year_month between 201801 and 201812) as sales_2018,
  sum(unit_count) filter (where year_month between 201901 and 201908) as sales_2019
-- select *
from sls.deals_by_month
where (psc_last_name = 'wilde' or ssc_last_name = 'wilde')
