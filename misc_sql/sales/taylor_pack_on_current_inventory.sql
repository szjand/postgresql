﻿-- 9/18
-- taylor called and asked for pack on current inventory

select *
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
where a.control = 'G37266'
order by b.account

select * 
from fin.dim_account
where description like '%pack%'



drop table if exists inv_acct cascade;
create temp table inv_acct as
select a.control as stock_number, f.inpmast_vin as vin, h.model_code, min(b.the_date) as acct_date, sum(a.amount)::integer as vehicle_cost
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account in ( -- all new vehicle inventory account
    select b.account
    from fin.dim_account b 
    where b.account_type = 'asset'
      and b.department_code = 'nc'
      and b.typical_balance = 'debit'
      and b.current_row
      and b.account <> '126104')   
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
left join arkona.xfm_inpmast f on a.control = f.inpmast_stock_number
  and f.current_row
left join ( -- open_orders reqd bc nc.vehicles is only on the ground
  select vin, configuration_id
  from nc.vehicles
  union
  select vin, configuration_id
  from nc.open_orders
  where vin is not null) g on f.inpmast_vin = g.vin
left join nc.vehicle_configurations h on g.configuration_id = h.configuration_id  
where a.post_status = 'Y'
  and a.control not like 'H%'
group by a.control, f.inpmast_vin, h.model_code
having sum(a.amount) > 10000;
create unique index on inv_acct(stock_number);
create unique index on inv_acct(vin);


select a.stock_number, b.make, b.model,
  sum(-f.amount) filter (where account = '133210') as pack,
  sum(-f.amount) filter (where account = '133211') as adv_pack,
  sum(coalesce(-f.amount, 0)) as total
from inv_acct a
join (
  select make, model, model_code
  from chr.configurations
  group by make, model, model_code) b on a.model_code = b.model_code
left join (
  select c.control, c.amount, d.account
  from fin.fact_gl c
  join fin.dim_account d on c.account_key = d.account_key
    and d.account in ('133210','133211')  
  join inv_acct e on c.control = e.stock_number) f on a.stock_number = f.control
group by a.stock_number, b.make, b.model
order by b.make, b.model
    