﻿-- deals
drop table if exists deals;
create temp table deals as
select vin, year_month, make, model, model_year
from sls.deals_by_month a
where a.year_month between 201709 and 201908
  and a.sale_type <> 'wholesale'
  and a.vehicle_type = 'new'
  and a.store_code = 'RY1'
group by vin, year_month, make, model, model_year
having sum(unit_count) > 0;
  
-- step 1
-- sls deals decode to forecast models via configuration configuration
drop table if exists step_1;
create temp table step_1 as
select aa.year_month, aa.vin, bb.category, bb.fc_make, bb.fc_model
from deals aa
join (
  select a.vin, b.category, b.make as fc_make, b.model as fc_model
  from nc.vehicles a
  join nc.forecast_model_configurations b on a.configuration_id = b.configuration_id) bb on aa.vin = bb.vin;

-- -- eliminate from deals where not in forecast models
-- select distinct make, model 
-- from deals a
-- where not exists (
--   select 1 
--   from step_1
--   where vin = a.vin)
-- order by make, model  

select * 
-- delete
from deals
where (
  (make = 'buick' and model in ('lacrosse', 'regal')) or
  (make = 'cadillac' and model in ('ats sedan', 'cts sedan', 'xts')) or
  (make = 'chevrolet' and model in ('bolt ev','cruze', 'impala')) or
  (make = 'gmc' and model in ('acadia limited','SAVANA 3500 CARGO VAN','SIERRA DENALI 2500')));

-- take care of the non pickups
-- year_month, vin, category, fc_make, fc_model
insert into step_1 
select a.year_month, a.vin, b.category, b.fc_make, b.fc_model
from deals a
left join (
  select a.category, a.make as fc_make, a.model as fc_model, b.make, b.model
  from nc.forecast_model_configurations a
  join nc.vehicles b on a.configuration_id = b.configuration_id
  group by a.category, a.make, a.model, b.make, b.model) b on a.make = b.make and a.model = b.model
where a.model not similar to '%(1500|2500|3500)%'
  and not exists (
    select 1 
    from step_1
    where vin = a.vin);  

-- lets get cab from inpmast for pickups
insert into step_1
select a.year_month, a.vin,
  case
    when a.make = 'chevrolet' then 'Chevy Truck'
    when a.make = 'gmc' then 'GMC'
  end as category,
  a.make as fc_make,
  case
    when a.make = 'chevrolet' and a.model = 'SILVERADO 1500' and b.body_style like '%crew%' then '1500 Crew'
    when a.make = 'chevrolet' and a.model = 'SILVERADO 1500' and b.body_style like '%double%' then '1500 Double'
    when a.make = 'chevrolet' and a.model = 'SILVERADO 1500' and b.body_style like '%reg%' then '1500 Reg'
    when a.make = 'chevrolet' and a.model like '%2500%' and b.body_style like '%crew%' then '2500 Crew'
    when a.make = 'chevrolet' and a.model like '%2500%' and b.body_style like '%double%' then '2500 Double'
    when a.make = 'chevrolet' and a.model like '%2500%' and b.body_style like '%reg%' then '2500 Reg'    
    when a.make = 'chevrolet' and a.model like '%3500%' and b.body_style like '%crew%' then '3500 Crew'
    when a.make = 'chevrolet' and a.model like '%3500%' and b.body_style like '%double%' then '3500 Double'   
    when a.make = 'gmc' and a.model like '%1500%' and b.body_style like '%crew%' then '1500 Crew'
    when a.make = 'gmc' and a.model like '%1500%' and b.body_style like '%double%' then '1500 Double'
    when a.make = 'gmc' and a.model like '%1500%' and b.body_style like '%reg%' then '1500 Reg'  
    when a.make = 'gmc' and a.model like '%2500%' and b.body_style like '%crew%' then '2500 Crew'
    when a.make = 'gmc' and a.model like '%2500%' and b.body_style like '%double%' then '2500 Double' 
    when a.make = 'gmc' and a.model like '%3500%' and b.body_style like '%crew%' then '3500 Crew'          
    else '---------'
  end as fc_model
from deals a
left join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
where not exists (
  select 1
  from step_1
  where vin = a.vin);

-- ok, everything we need is in step_1
so just in case i dont get done today, lets put this in a phyical table
select * from step_1

create table jon.taylor_sales_history (
  year_month integer,
  vin citext,
  category citext,
  make citext,
  model citext);
comment on table jon.taylor_sales_history is 'taylor periodically wants retail/fleet sales history
  based on forecast models to aid in forecasing';

insert into jon.taylor_sales_history
select * from step_1;

select * from (
select make, model,
  count(vin) filter (where year_month = 201709) as sep_17,
  count(vin) filter (where year_month = 201710) as oct_17,
  count(vin) filter (where year_month = 201711) as nov_17,
  count(vin) filter (where year_month = 201712) as dec_17,
  count(vin) filter (where year_month = 201801) as jan_18,
  count(vin) filter (where year_month = 201802) as feb_18,
  count(vin) filter (where year_month = 201803) as mar_18,
  count(vin) filter (where year_month = 201804) as apr_18,
  count(vin) filter (where year_month = 201805) as may_18,
  count(vin) filter (where year_month = 201806) as jun_18,
  count(vin) filter (where year_month = 201807) as jul_18,
  count(vin) filter (where year_month = 201808) as aug_18
from jon.taylor_sales_history
where year_month between 201709 and 201808
group by category, make, model
order by make, model
) a 

full outer join (
select make, model,
  count(vin) filter (where year_month = 201809) as sep_18,
  count(vin) filter (where year_month = 201810) as oct_18,
  count(vin) filter (where year_month = 201811) as nov_18,
  count(vin) filter (where year_month = 201812) as dec_18,
  count(vin) filter (where year_month = 201901) as jan_19,
  count(vin) filter (where year_month = 201902) as feb_19,
  count(vin) filter (where year_month = 201903) as mar_19,
  count(vin) filter (where year_month = 201904) as apr_19,
  count(vin) filter (where year_month = 201905) as may_19,
  count(vin) filter (where year_month = 201906) as jun_19,
  count(vin) filter (where year_month = 201907) as jul_19,
  count(vin) filter (where year_month = 201908) as aug_19
from jon.taylor_sales_history
where year_month between 201809 and 201908
group by category, make, model
order by make, model
) b on a.make = b.make and a.model = b.model