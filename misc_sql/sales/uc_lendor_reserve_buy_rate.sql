﻿-- 5/7/19 from nick
-- April used car finance report.
-- The attached excel spreadsheet shows you the fields I need filled in, 
-- I need this for any used car that was sold in the month of April, 
-- that was financed through any of our banks.
-- 6/4 added apr

select bopmast_company_number, bopmast_stock_number, date_capped, 
  bopmast_search_name, b.lendor_name, 
  a.buy_rate, apr, term, finance_reserve
-- select *
from arkona.xfm_bopmast a
join arkona.ext_boplsrc b on a.lending_source = b.key
  and a.bopmast_company_number = b.company_number
where date_capped between '05/01/2019' and '05/31/2019'
  and vehicle_type = 'U'
  and sale_Type <> 'W'
  and a.current_row
