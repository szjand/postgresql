﻿drop table ft.jon_tmp_cert_question_video_titles;

drop table if exists ft.ext_test_questions;
create table ft.ext_test_questions(
  item integer not null,
  question citext not null,
  answer citext not null,
  correct boolean not null,
  video_title citext not null,
  chapter citext not null,
  video_id integer not null references ft.videos(id));
comment on table ft.ext_test_questions is 'from spreadsheet prepared by ben foster';



select * from ft.ext_test_questions