﻿drop table if exists users cascade;
create temp table users as
select 'new' as source, email_address, user_key::text
from ft.users a
-- inner join roles b on case when b.roles = 'Non-Manager' then permission = 'Employee' else permission <> 'Employee' end
-- --inner join subscriptions c on case when 
-- inner join user_dip_stores c on a.user_key = c.user_key
-- 
-- where department in (select json_array_elements_text(_department)::citext)
-- and is_dip_store::citext in (select json_array_elements_text(_subscription)::citext)
where is_active = true --and email_address like 'michael.dunl%'
union
select 'old', email, max(user_id)
from ft.ext_users a
left join ft.users c on a.email = c.email_address
-- inner join roles b on case when b.roles = 'Non-Manager' then the_role = 'Employee' else the_role <> 'Employee' end
where a.thru_date is null
-- and a.department in (select json_array_elements_text(_department)::citext)
  and c.email_address is null 
group by email  
order by email_address;
create unique index on users(email_address);

select * from ft.ext_users where thru_date is null


select * from users

-- drop table if exists ft.jon_last_login;
-- create table ft.jon_last_login (
-- email citext,
-- last_login timestamptz)
-- 
-- insert into ft.jon_last_login values ('gsorum@cartiva.com','08/04/2019 08:48:18');
-- 
-- update ft.ext_user_last_login a
-- set last_login = x.last_login
-- from (
--   select b.* 
--   from ft.ext_user_last_login a
--   left join ft.jon_last_login b on a.email = b.email
--   where a.thru_date is null
--     and b.last_login > a.last_login) x
-- where a.email = x.email    

select * from ft.ext_user_last_login order by last_login desc 

-- all users (new & old) and lost login dates (new & old)
select a.source, a.email_address, b.last_login::date as old_site, c.new_site
from users a
left join ft.ext_user_last_login b on a.email_address = b.email
left join (
  select user_key::text, max(session_started)::date as new_site
  from ft.user_sessions
  group by user_key::text) c on a.user_key = c.user_key
order by email_address


-- there are no exclusively old site users that have visited the new site
select * 
from (
  select a.source, a.email_address, b.last_login::date as old_site, c.new_site
  from users a
  left join ft.ext_user_last_login b on a.email_address = b.email
  left join (
    select user_key::text, max(session_started)::date as new_site
    from ft.user_sessions
    group by user_key::text) c on a.user_key = c.user_key) x
where source = 'old'
  and new_site is not null


  