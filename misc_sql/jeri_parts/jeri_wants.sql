﻿PTCODE - Transaction Code
PTCODE - Transaction Code
AP = Add Part
BO = Back Ordered
CM = Comment
CN = Cancelled
CP = RO Customer Pay Sale
CQ = Converted Quantity
CR = RO Correction
CS = RO Cause
DC = Discounts
DP = Delete Part
FR = Factory Return
GC = Stock Group Change
IA = Manual Inventory Adjustment
IS = RO Internal Sale
LA = Lifo Adjust
LS = Lost Sale
MP = Merged Part
OF = Fees
OR = Ordered
PA = Physical Inventory Adjust
PC = Part count from last physical inventory
PO = Purchase order
RC = Special Order Receipt
RC = Received
RO = Reorder
RS = Restocking charge
RT = Counter Return
SA = Counter Sale
SC = RO Service Contract Sale
SH = Shipping
SL = Sublet
SR = RO Return
TT = RO Tech Time
WS = RO Warranty sale
ZA = assign core part
ZR = remove core part

select count(*) -- 333033
from arkona.ext_pdpmast


select *
from arkona.ext_pdpmast
limit 10

select count(distinct part_number) -- 20202
from arkona.ext_pdpmast
where 
  qty_on_hand <> 0 or
  qty_on_order <> 0 or
  qty_on_back_order <> 0 or
  qty_on_spec_order <> 0


select *
from arkona.ext_pdphist
limit 10


select min(transaction_time)::date  -- 01/01/2019
from arkona.ext_pdphist


select count(distinct part_number)  -- 25184
from arkona.ext_pdphist
where (
  (quantity_on_stock_order_adjustment <> 0
  or quantity_back_order_adjustment <> 0
  or quantity_on_special_order_adjustment <> 0))
  and transaction_time::date > current_date - 180

start with a calendar table
for each day, sum of sales for that day and next 6 days

-- 02/21/20
results from conversation in the 9:30
attribtutes:
  counter pad report
  stocking group
  stock code
  12 months sales history
  max sales in any 7 day period for each part (quantity)


drop table if exists parts_1 cascade;
create temp table parts_1 as
select part_number
from arkona.ext_pdpmast
where 
  qty_on_hand <> 0 or
  qty_on_order <> 0 or
  qty_on_back_order <> 0 or
  qty_on_spec_order <> 0
group by part_number; 
create unique index on parts_1(part_number); 

drop table if exists parts_2 cascade;
create temp table parts_2 as
select b.part_number, b.company_number, b.manufacturer,  b.part_description, 
  b.stocking_group, b.group_code, b.qty_on_hand, b.cost
from parts_1 a
join arkona.ext_pdpmast b on a.part_number = b.part_number
  and b.company_number = 'RY1';
create unique index on parts_2(part_number, manufacturer); 

select *
from arkona.ext_pdpmast
where part_number = 'R84356'
5297428

ATR0520
-- from misc_sql\parts\stinar\stinar_list_20190118.sql

If I was looking for this info, it would be under “Parts in Inventory.”  
After that part number is put in the top column, I would press enter and 
then function 6-showing the 12 month demand.  I can do it individually, 
but wondering if there is an automated process you can run. 

looking at several random part numbers and getting a match with the arkona monthly demand page for that part number

select e.*, sum(quantity) over (partition by year_month)
from (
  select d.year_month, c.ptcode, sum(c.ptqty) as quantity
  from arkona.ext_pdpmast b 
  left join arkona.ext_pdptdet c on b.part_number = c.ptpart
    and c.ptco_ = 'RY1'
    and c.ptcode not in ('or','dp', 'rc','bo','ia','la','pc','cn')
  --   and c.ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
    and (c.ptsoep is null or c.ptsoep in ('N', 'E'))
  inner join dds.dim_date d on (select arkona.db2_integer_to_date_long(c.ptdate)) = d.the_date  
  -- where d.the_date between '01/15/2018' and '01/15/2019'  
--   where d.year_month between 201904 and 202001
  where d.year_month between 201907 and 202006
    and b.part_number in ('ATR0520')
  group by d.year_month, c.ptcode) e


-- from misc_sql\parts\gm_lists\parts_marks_gm_list.sql

select /*a.list_name,*/ b.part_number, b.part_description, b.cost, b.qty_on_hand, 
  b.qty_on_order, b.qty_on_back_order,
  sum(case when d.year_month = 201902 then ptqty else 0 end) as feb_19,
  sum(case when d.year_month = 201903 then ptqty else 0 end) as mar_19,
  sum(case when d.year_month = 201904 then ptqty else 0 end) as apr_19,
  sum(case when d.year_month = 201905 then ptqty else 0 end) as may_19,
  sum(case when d.year_month = 201906 then ptqty else 0 end) as jun_19,
  sum(case when d.year_month = 201907 then ptqty else 0 end) as jul_19,
  sum(case when d.year_month = 201908 then ptqty else 0 end) as aug_19,
  sum(case when d.year_month = 201909 then ptqty else 0 end) as sep_19,
  sum(case when d.year_month = 201910 then ptqty else 0 end) as oct_19,
  sum(case when d.year_month = 201911 then ptqty else 0 end) as nov_19,
  sum(case when d.year_month = 201912 then ptqty else 0 end) as dec_19,
  sum(case when d.year_month = 202001 then ptqty else 0 end) as jan_20
-- from lists a
-- left join arkona.ext_pdpmast b on a.part_number = b.part_number
--   and b.company_number = 'RY1'
from arkona.ext_pdpmast b
left join arkona.ext_pdptdet c on b.part_number = c.ptpart
  and c.ptco_ = 'RY1'
  and c.ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
  and (c.ptsoep is null or c.ptsoep in ('N', 'E'))
inner join dds.dim_date d on (select arkona.db2_integer_to_date_long(c.ptdate)) = d.the_date
where d.year_month between 201902and 202001
  and b.part_number in ('5297428')
group by /*a.list_name,*/ b.part_number, b.part_description, b.cost, b.qty_on_hand, 
  b.qty_on_order, b.qty_on_back_order
order by /*a.list_name,*/ b.part_number;


-- both queries look good
-- lets do full detail in both at the day grain

-- leave out the code
drop table if exists parts_3 cascade;
create temp table parts_3 as
-- select d.year_month, c.ptcode, sum(c.ptqty) as quantity
select a.part_number, a.manufacturer, d.the_date, sum(c.ptqty) as quantity
from parts_2 a  
left join arkona.ext_pdptdet c on a.part_number = c.ptpart
  and a.manufacturer = c.ptmanf
  and c.ptco_ = 'RY1'
  and c.ptcode not in ('or','dp', 'rc','bo','ia','la','pc','cn')
--   and c.ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
  and (c.ptsoep is null or c.ptsoep in ('N', 'E'))
inner join dds.dim_date d on (select arkona.db2_integer_to_date(c.ptdate)) = d.the_date  
-- where d.the_date between '01/15/2018' and '01/15/2019'  
where d.year_month between 201902 and 202001
group by a.part_number, a.manufacturer, d.the_date
having sum(c.ptqty) <> 0;
create index on parts_3(part_number);
create index on parts_3(manufacturer);
create index on parts_3(the_date);

select ptcode, count(*)
from parts_3
group by ptcode
order by ptcode

-- leave out the code
drop table if exists parts_4 cascade;
create temp table parts_4 as
select b.part_number, b.manufacturer, d.the_date, sum(c.ptqty) as quantity
from parts_2 b
left join arkona.ext_pdptdet c on b.part_number = c.ptpart
  and b.manufacturer = c.ptmanf
  and c.ptco_ = 'RY1'
  and c.ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
  and (c.ptsoep is null or c.ptsoep in ('N', 'E'))
inner join dds.dim_date d on (select arkona.db2_integer_to_date(c.ptdate)) = d.the_date
where d.year_month between 201902and 202001
group by b.part_number, b.manufacturer, d.the_date
having sum(c.ptqty) <> 0;
create index on parts_4(part_number);
create index on parts_4(manufacturer);
create index on parts_4(the_date);


11102751

select part_number, manufacturer, 
  coalesce(sum(quantity) filter (where d.year_month = 201902),0) as feb_19,
  coalesce(sum(quantity) filter (where d.year_month = 201903),0) as mar_19,
  coalesce(sum(quantity) filter (where d.year_month = 201904),0) as apr_19,
  coalesce(sum(quantity) filter (where d.year_month = 201905),0) as may_19,
  coalesce(sum(quantity) filter (where d.year_month = 201906),0) as jun_19,
  coalesce(sum(quantity) filter (where d.year_month = 201907),0) as jul_19,
  coalesce(sum(quantity) filter (where d.year_month = 201908),0) as aug_19,
  coalesce(sum(quantity) filter (where d.year_month = 201909),0) as sep_19,
  coalesce(sum(quantity) filter (where d.year_month = 201910),0) as oct_19,
  coalesce(sum(quantity) filter (where d.year_month = 201911),0) as nov_19,
  coalesce(sum(quantity) filter (where d.year_month = 201912),0) as dec_19,
  coalesce(sum(quantity) filter (where d.year_month = 202001),0) as jan_20
from parts_4 a
join dds.dim_date d on a.the_date = d.the_date
group by part_number, manufacturer


11516807


select part_number, manufacturer, 
  coalesce(sum(quantity) filter (where d.year_month = 201902),0) as feb_19,
  coalesce(sum(quantity) filter (where d.year_month = 201903),0) as mar_19,
  coalesce(sum(quantity) filter (where d.year_month = 201904),0) as apr_19,
  coalesce(sum(quantity) filter (where d.year_month = 201905),0) as may_19,
  coalesce(sum(quantity) filter (where d.year_month = 201906),0) as jun_19,
  coalesce(sum(quantity) filter (where d.year_month = 201907),0) as jul_19,
  coalesce(sum(quantity) filter (where d.year_month = 201908),0) as aug_19,
  coalesce(sum(quantity) filter (where d.year_month = 201909),0) as sep_19,
  coalesce(sum(quantity) filter (where d.year_month = 201910),0) as oct_19,
  coalesce(sum(quantity) filter (where d.year_month = 201911),0) as nov_19,
  coalesce(sum(quantity) filter (where d.year_month = 201912),0) as dec_19,
  coalesce(sum(quantity) filter (where d.year_month = 202001),0) as jan_20
from parts_3 a
join dds.dim_date d on a.the_date = d.the_date
where part_number = '12345615'
group by part_number, manufacturer

select max(_7_days)
from (
  select a.the_date, b.*, sum(quantity) over (order by a.the_Date rows between current row and 6 following) as _7_days
  from dds.dim_date a
  left join parts_3 b on a.the_date = b.the_date
    and b. part_number = '12356150B'
  where a.the_date between '02/01/2019' and '01/31/2020'
  order by a.the_date) x





CREATE OR REPLACE FUNCTION jon.for_loop_through_query() 
RETURNS VOID AS $$
DECLARE
    rec RECORD;
BEGIN
    drop table if exists wtf;
    create temp table wtf (field_1 citext);
    FOR rec IN 
      select distinct part_number
      from parts_3
      where part_number in ('11102751','11516807')
    LOOP 
    insert into wtf values( rec.part_number);
    END LOOP;
END;
$$ LANGUAGE plpgsql;

select jon.for_loop_through_query();
select * from wtf;


CREATE OR REPLACE FUNCTION jon.for_loop_through_query() 
RETURNS VOID AS $$
DECLARE
    rec RECORD;
BEGIN
    drop table if exists wtf;
    create temp table wtf (field_1 citext, max_7_days integer);
    FOR rec IN 
      select distinct part_number
      from parts_3
--       limit 100
--       where part_number in ('11102751','11516807')
    loop
      insert into wtf    
      select rec.part_number, max(_7_days) as the_max
      from (
        select a.the_date, b.part_number, sum(quantity) over (order by a.the_Date, b.part_number rows between current row and 6 following) as _7_days
        from dds.dim_date a
        left join parts_3 b on a.the_date = b.the_date
          and b.part_number = rec.part_number
        where a.the_date between '02/01/2019' and '01/31/2020'
        order by b.part_number, a.the_date) x;
      end loop;
end;
$$ language plpgsql;


--------------------------------------------------------------------------
-- jeri wants parts
--------------------------------------------------------------------------

1. run the counter pad report from parts, include qw month demand

2. re-create jon.counter_pad
-- raw counter pad data -4/10/20
-- 06/11/20 (renamed the month columns as generic attribute names, don't need actual months until the spreadsheet)
drop table if exists jon.counter_pad;
create table jon.counter_pad (
  manf citext,
  part citext,
  part_number citext,
  description citext,
  stock_group citext,
  status citext,
  oem_group citext,
  on_hand citext,
  on_order citext,
  on_back_order citext,
  cost citext,
  m1 citext,  -- most recent month, on 6/11 it is jun_20
  m2 citext,
  m3 citext,
  m4 citext,
  m5 citext,
  m6 citext,
  m7 citext,
  m8 citext,
  m9 citext,
  m10 citext,
  m11 citext,
  m12 citext);  -- most distant month, on 6/11 it is jul_19

3. 
insert the counter pad data
this is the formula to generate insert statements on the counter pad spreadsheet
"insert into jon.counter_pad values('"&A2&"','"&B2&"','','"&"','"&C2&"','"&D2&"','"&E2&"','"&G2&"','"&H2&"','"&I2&"','"&K2&"','"&O2&"','"&P2&"','"&Q2&"','"&R2&"','"&S2&"','"&T2&"','"&U2&"','"&V2&"','"&W2&"','"&X2&"','"&Y2&"','"&Z2&"');"

select count(*)  -- 17466/17574/ 6-10:17729
from jon.counter_pad

4. recreate jon.counter_pad_1
-- xfm'd counter pad data
-- 6/11/20: replaced months with generic attribute n ames
drop table if exists jon.counter_pad_1 cascade;
create table jon.counter_pad_1 (
  manf citext not null,
  part_number citext not null,
  description citext not null,
  stock_group citext not null,
  status citext,
  oem_group citext,
  on_hand integer default '0' not null,
  on_order integer default '0' not null,
  on_back_order integer default '0' not null,
  cost numeric(10,2),
  m1 integer default '0' not null,
  m2 integer default '0' not null,
  m3 integer default '0' not null,
  m4 integer default '0' not null,
  m5 integer default '0' not null,
  m6 integer default '0' not null,
  m7 integer default '0' not null,
  m8 integer default '0' not null,
  m9 integer default '0' not null,
  m10 integer default '0' not null,
  m11 integer default '0' not null,
  m12 integer default '0' not null,
  primary key(manf,part_number));
  

5. generate a list of part numbers with *** quantity
ah, the counter pad output for the months shows *** literally where the quantity > 999
this gives me the list of part numbers with *** quantity
select *  
from jon.counter_pad a
where trim(left(a.part, position (' ' in a.part))) in (
  select trim(left(part, position (' ' in part)))
  from jon.counter_pad
  where (
    m1 = '***' or m2 = '***' or m3 = '***' or m4 = '***' or m5 = '***' or m6 = '***' or m7 = '***' or m8 = '***' 
    or m9 = '***' or m10 = '***' or m11 = '***' or m12 = '***' or on_hand = '***' or on_order = '***'));

6. insert into jon.counter_pad_1
   
insert into jon.counter_pad_1
select manf, trim(left(part, position (' ' in part))), trim(substring(part from position( ':' in part) + 2)), 
  stock_group, status, oem_group, 
  coalesce(nullif(on_hand,''),'0')::integer, coalesce(nullif(on_order,''),'0')::integer,
  coalesce(nullif(on_back_order,''),'0')::integer, cost::numeric, 
  coalesce(nullif(m1,''),'0')::integer, coalesce(nullif(m2,''),'0')::integer,
  coalesce(nullif(m3,''),'0')::integer, coalesce(nullif(m4,''),'0')::integer,
  coalesce(nullif(m5,''),'0')::integer, coalesce(nullif(m6,''),'0')::integer,
  coalesce(nullif(m7,''),'0')::integer, coalesce(nullif(m8,''),'0')::integer, 
  coalesce(nullif(m9,''),'0')::integer, coalesce(nullif(m10,''),'0')::integer, 
  coalesce(nullif(m11,''),'0')::integer, coalesce(nullif(m12,''),'0')::integer 
-- select *  
from jon.counter_pad a
where trim(left(a.part, position (' ' in a.part))) not in ( -- the list of part numbers with *** quantites from above
  '12356150B','19383806','12345615','19417001','19417038','88865638','88865700','89021512','89021636')




    
oem = pdppmex.stock_code  :: 02 = RIM

need:
  pdppmex.stock_level
  pdpmast: group_code


status: 
  A: active
  N: non-stocked
  C: core part number
  R: replaced

select max(ptdate) from arkona.ext_pdptdet

7. recreate temp table parts_3, modify the date range
-- generate pdptdet data at part_number/date grain
drop table if exists parts_3 cascade;
create temp table parts_3 as
select a.part_number, a.manf, d.the_date, sum(c.ptqty) as quantity
from jon.counter_pad_1 a  
left join arkona.ext_pdptdet c on a.part_number = c.ptpart
  and a.manf = c.ptmanf
  and c.ptco_ = 'RY1'
  and c.ptcode not in ('or','dp', 'rc','bo','ia','la','pc','cn')
  and (c.ptsoep is null or c.ptsoep in ('N', 'E'))
inner join dds.dim_date d on (select arkona.db2_integer_to_date(c.ptdate)) = d.the_date  
where d.year_month between 201907 and 202006 ----------- change the date range
group by a.part_number, a.manf, d.the_date
having sum(c.ptqty) <> 0;
create index on parts_3(part_number);
create index on parts_3(manf);
create index on parts_3(the_date);

8. modify the date range and re-create the function jon.for_loop_through_query()
-- generate the max 7 day sales figure for each part number
CREATE OR REPLACE FUNCTION jon.for_loop_through_query() 
RETURNS VOID AS $$
DECLARE
    rec RECORD;
BEGIN
    drop table if exists wtf;
    create temp table wtf (field_1 citext, max_7_days integer);
    FOR rec IN 
      select distinct part_number
      from parts_3
    loop
      insert into wtf    
      select rec.part_number, max(_7_days) as the_max
      from (
        select a.the_date, b.part_number, sum(quantity) over (order by a.the_Date, b.part_number rows between current row and 6 following) as _7_days
        from dds.dim_date a
        left join parts_3 b on a.the_date = b.the_date
          and b.part_number = rec.part_number
        where a.the_date between '07/01/2019' and current_date --------------- change the start date
        order by b.part_number, a.the_date) x;
      end loop;
end;
$$ language plpgsql;

9. run the function jon.for_loop_through_query()
select jon.for_loop_through_query();
    
10. modify the alias for the months and create the spreadsheet for jeri names as jeri_wants_parts_todays date
-- this is the spreadsheet: jeri_wants_parts_v1  02/14/2020
-- and 4/10/20
-- and 06/11/20
select a.manf, a.part_number, a.description as part_desc, a.stock_group, 
  e.description as stk_grp_desc, a.oem_group, c.group_code, b.stock_level, 
  a.status, a.on_hand, a.on_order, a.on_back_order, a.cost, 
  d.max_7_days as max_7_day_sale,
  m1 as jun_20, m2 as may_20, m3 as apr_20, m4 as mar_20, m5 as feb_20,
  m6 as jan_20, m7 as dec_19, m8 as nov_19, m9 as oct_19, m10 as sep_19,
  m11 as aug_19, m12 as jul_19
from jon.counter_pad_1 a
left Join arkona.ext_pdppmex b on a.part_number = b.part_number
  and a.manf = b.manufacturer
left join arkona.ext_pdpmast c on a.part_number = c.part_number
  and a.manf = c.manufacturer
  and c.company_number = 'RY1'
left join wtf d on a.part_number = d.field_1  
left join arkona.ext_pdpsgrp e on a.manf = e.manufacturer
  and a.stock_group = e.stock_group
  and e.company_number = 'RY1';





