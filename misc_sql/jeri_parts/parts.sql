﻿select count(*)
from arkona.ext_pdphist
where transaction_time::date = current_Date 


join pdpmast on pdphist


select * from jeri.inventory_accounts

select *
from jeri.inventory_balances a
join jeri.inventory_accounts b on a.account = b.account
  and b.store_code = 'RY1'
  and b.department = 'parts'
where a.the_year = 2020
  and a.the_month = 2  


select *
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account
  and c.store_code = 'RY1'
  and c.department = 'parts'
  and c.account = '124300' -- inv tires
where a.year = 2020  

select *
from arkona.ext_pdpsgrp
-- stk group 513: tires

select company_number, part_number, manufacturer, part_description, cost, qty_on_hand, qty_reserved, qty_on_order, qty_on_back_order, qty_on_spec_order
from arkona.ext_pdpmast
where stocking_group = '513'
  and qty_on_hand + qty_reserved + qty_on_order + qty_on_back_order + qty_on_spec_order <> 0

select sum(cost * qty_on_hand)
from arkona.ext_pdpmast
where stocking_group = '513'
  and qty_on_hand + qty_reserved + qty_on_order + qty_on_back_order + qty_on_spec_order <> 0 



select *
from arkona.ext_pdphist
where part_number = '20967936'

select * from arkona.ext_pdpmast where part_number = '20967936'


-- this should be current balance, right?
-- 2881438.86  jeris page: 3,503,518
select sum(cost * qty_on_hand)
from arkona.ext_pdpmast
where company_number = 'RY1'
-- where qty_on_hand + qty_reserved + qty_on_order + qty_on_back_order + qty_on_spec_order <> 0 


no fucking way
jeri is only including 124200, 124201, 124219, 124300, 124400, excludes body shop, detail & car wash
how the fuck do i correlate that to part numbers


-- pdpmast, stocking groups, 
select a.stock_group, a.description, sum(cost * qty_on_hand)::integer
-- select sum(cost * qty_on_hand)::integer
from arkona.ext_pdpsgrp a
join arkona.ext_pdpmast b on a.stock_group = b.stocking_group
where a.company_number = 'RY1'
group by a.stock_group, a.description
order by a.stock_group


-- this is whats driving jeris page for parts inventory
select a.*, b.factory_account, b.description
from jeri.inventory_balances a
join jeri.inventory_accounts b on a.account = b.account
--   and b.store_code = 'RY1'
  and b.department = 'parts'
where a.the_year = 2020
  and a.the_month = 2  

------------------------------------------------------------------
--< 02/10/20 glpmast 
------------------------------------------------------------------
-- account 124300: Tires
-- stocking group 513: Tires

select b.the_date, a.control, a.doc, a.ref, c.account, a.amount, d.journal_code, e.description
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month > 201911
join fin.dim_account c on a.account_key = c.account_key
  and c.account = '124300'
join fin.dim_journal d on a.journal_key = d.journal_key
join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key 
where a.post_status = 'Y'  
order by the_date desc 



select *
from arkona.ext_glpmast
where account_number = '124300'
  and year = 2020

beginning balance: 11413.59
jan balance: 12407.25
feb_balance: 5757.67  

-- ok, it is as i expected, accounting transactions for january + glpmast 2020 beginning balance = glpmast jan balance
select b.the_date, a.control, a.doc, a.ref, c.account, a.amount, d.journal_code, e.description
select sum(a.amount)
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 202001
join fin.dim_account c on a.account_key = c.account_key
  and c.account = '124300'
join fin.dim_journal d on a.journal_key = d.journal_key
join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key   
where a.post_status = 'Y'

------------------------------------------------------------------
--/> 02/10/20 glpmast
------------------------------------------------------------------
select 3499150 - 2889145

select * from arkona.ext_pdpsgrp

-- stocking group totals
select a.stock_group, a.description , sum(cost * qty_on_hand)::integer
-- select sum(cost * qty_on_hand)::integer
from arkona.ext_pdpsgrp a
join arkona.ext_pdpmast b on a.stock_group = b.stocking_group
where a.company_number = 'RY1'
group by a.stock_group, a.description
union
select null, 'TOTAL', sum(cost * qty_on_hand)::integer
-- select sum(cost * qty_on_hand)::integer
from arkona.ext_pdpsgrp a
join arkona.ext_pdpmast b on a.stock_group = b.stocking_group
where a.company_number = 'RY1'
order by stock_group nulls last 



select a.stock_group, a.description , a._0207, b._0208, c._0209
from (
  select a.stock_group, a.description , sum(cost * qty_on_hand)::integer as _0207
  from arkona.ext_pdpsgrp a
  join arkona_cdc.pdpmast_20200207 b on a.stock_group = b.stocking_group
  where a.company_number = 'RY1'
  group by a.stock_group, a.description
  union
  select 'TOTAL', 'TOTAL', sum(cost * qty_on_hand)::integer
  -- select sum(cost * qty_on_hand)::integer
  from arkona.ext_pdpsgrp a
  join arkona_cdc.pdpmast_20200207 b on a.stock_group = b.stocking_group
  where a.company_number = 'RY1') a
left join (
  select a.stock_group, a.description , sum(cost * qty_on_hand)::integer as _0208
  from arkona.ext_pdpsgrp a
  join arkona_cdc.pdpmast_20200208 b on a.stock_group = b.stocking_group
  where a.company_number = 'RY1'
  group by a.stock_group, a.description
  union
  select 'TOTAL', 'TOTAL', sum(cost * qty_on_hand)::integer
  -- select sum(cost * qty_on_hand)::integer
  from arkona.ext_pdpsgrp a
  join arkona_cdc.pdpmast_20200208 b on a.stock_group = b.stocking_group
  where a.company_number = 'RY1') b on a.stock_group = b.stock_group and a.description = b.description
left join (
  select a.stock_group, a.description , sum(cost * qty_on_hand)::integer as _0209
  from arkona.ext_pdpsgrp a
  join arkona_cdc.pdpmast_20200209 b on a.stock_group = b.stocking_group
  where a.company_number = 'RY1'
  group by a.stock_group, a.description
  union
  select 'TOTAL', 'TOTAL', sum(cost * qty_on_hand)::integer
  -- select sum(cost * qty_on_hand)::integer
  from arkona.ext_pdpsgrp a
  join arkona_cdc.pdpmast_20200209 b on a.stock_group = b.stocking_group
  where a.company_number = 'RY1') c on a.stock_group = c.stock_group and a.description = c.description
order by a.stock_group nulls last 


select *
from arkona.ext_pdphist
where transaction_time::date = '02/09/2020'

drop table if exists transactions;
create temp table transactions as
select b.year_month, b.the_date, stocking_group, part_number, cost_amount, sum(quantity_on_hand_adjustment) as adjustment
-- select *
from arkona.ext_pdphist a
join dds.dim_date b on a.transaction_time::date = b.the_date
  and b.the_date between '01/01/2019' and current_date
where a.company_number = 'RY1'
  and a.quantity_on_hand_adjustment <> 0
group by b.year_month, b.the_date, stocking_group, part_number, cost_amount
having sum(quantity_on_hand_adjustment) <> 0;

select stocking_group, 
  coalesce(sum(adjustment * cost_amount) filter (where year_month = 202002), 0)::integer as feb_20,
  coalesce(sum(adjustment * cost_amount) filter (where year_month = 202001), 0)::integer as jan_20,
  coalesce(sum(adjustment * cost_amount) filter (where year_month = 201912), 0)::integer as dec_19,
  coalesce(sum(adjustment * cost_amount) filter (where year_month = 201911), 0)::integer as nov_19,
  coalesce(sum(adjustment * cost_amount) filter (where year_month = 201910), 0)::integer as oct_19,
  coalesce(sum(adjustment * cost_amount) filter (where year_month = 201909), 0)::integer as sep_19,
  coalesce(sum(adjustment * cost_amount) filter (where year_month = 201908), 0)::integer as aug_19,
  coalesce(sum(adjustment * cost_amount) filter (where year_month = 201907), 0)::integer as jul_19,
  coalesce(sum(adjustment * cost_amount) filter (where year_month = 201906), 0)::integer as jun_19,
  coalesce(sum(adjustment * cost_amount) filter (where year_month = 201905), 0)::integer as may_19,
  coalesce(sum(adjustment * cost_amount) filter (where year_month = 201904), 0)::integer as apr_19,
  coalesce(sum(adjustment * cost_amount) filter (where year_month = 201903), 0)::integer as mar_19,
  coalesce(sum(adjustment * cost_amount) filter (where year_month = 201902), 0)::integer as feb_19,
  coalesce(sum(adjustment * cost_amount) filter (where year_month = 201901), 0)::integer as jan_19
  from transactions
group by stocking_group
order by stocking_group


-- 02/11/20

--------------------------------------------------------------
-- counter pad report
--------------------------------------------------------------
select *
from jon.counter_pad

select company_number, part_number, manufacturer, part_description, cost, qty_on_hand, qty_reserved, qty_on_order, qty_on_back_order, qty_on_spec_order
from arkona.ext_pdpmast
where qty_on_order <> 0
order by part_number

select sum(qty_on_order * cost)  -- 83068
from arkona.ext_pdpmast
where qty_on_order <> 0



select company_number, part_number, manufacturer, part_description, cost, qty_on_hand, qty_reserved, qty_on_order, qty_on_back_order, qty_on_spec_order
from arkona.ext_pdpmast
where qty_on_back_order <> 0

select sum(qty_on_back_order * cost)  -- 221376
from arkona.ext_pdpmast
where qty_on_back_order <> 0

select sum(qty_on_spec_order * cost)  -- 138171
from arkona.ext_pdpmast
where qty_on_spec_order <> 0

select *
from arkona.ext_pdpmast
where part_number = '10290381'

--------------------------------------------------------------
--/> counter pad report
--------------------------------------------------------------

-- yesterday, i was unhappy with the results of backfilling monthly balance using pdphist
-- sg_adj_by_month.xlsx
-- so today i am going to try to verify the difference between pdpmast_20200207 and pdpmast_20200210
-- the dates in the table names represent inventory at the end of that day
-- so, to track changes from pdpmast_20200207 to pdpmast_20200210, need transactions for 02/08, 02/09/ & 02/10

i worry about including stuff on order
of on 2/7  i have 50 items @ a cost of $5000 on order, on 2/10 have received them all, are  they now on hand ???


-- results are spotty


select aa.*, bb.adjustment
from (
  select a.stock_group, a.description , a._0207, b._0210, b._0210 - a._0207 as diff
  from (
    select a.stock_group, a.description , sum(cost * (qty_on_hand + qty_on_order + qty_on_back_order))::integer as _0207
    from arkona.ext_pdpsgrp a
    join arkona_cdc.pdpmast_20200207 b on a.stock_group = b.stocking_group
    where a.company_number = 'RY1'
    group by a.stock_group, a.description
    union
    select 'TOTAL', 'TOTAL', sum(cost * (qty_on_hand + qty_on_order + qty_on_back_order))::integer
    from arkona.ext_pdpsgrp a
    join arkona_cdc.pdpmast_20200207 b on a.stock_group = b.stocking_group
    where a.company_number = 'RY1') a
  left join (
    select a.stock_group, a.description , sum(cost * (qty_on_hand + qty_on_order + qty_on_back_order))::integer as _0210
    from arkona.ext_pdpsgrp a
    join arkona_cdc.pdpmast_20200210 b on a.stock_group = b.stocking_group
    where a.company_number = 'RY1'
    group by a.stock_group, a.description
    union
    select 'TOTAL', 'TOTAL', sum(cost * (qty_on_hand + qty_on_order + qty_on_back_order))::integer
    from arkona.ext_pdpsgrp a
    join arkona_cdc.pdpmast_20200210 b on a.stock_group = b.stocking_group
    where a.company_number = 'RY1') b on a.stock_group = b.stock_group and a.description = b.description
  order by a.stock_group nulls last) aa  
left join (
  select stocking_group, sum(quantity_on_hand_adjustment * cost_amount)::integer as adjustment
  from arkona.ext_pdphist
  where transaction_time::Date between '02/08/2020' and '02/10/2020'
    and quantity_on_hand_adjustment <> 0
    and company_number = 'RY1'
  group by stocking_group
  union 
  select 'TOTAL', sum(quantity_on_hand_adjustment * cost_amount)::integer as adjustment 
  from arkona.ext_pdphist
  where transaction_time::Date between '02/08/2020' and '02/10/2020'
    and quantity_on_hand_adjustment <> 0
    and company_number = 'RY1' ) bb on aa.stock_group = bb.stocking_group
order by stock_group

the differences (in my opinion) between the pdpmast and pdphist queries is due to including different quantities

pdpmast                   pdphist
qty_on_hand               quantity_on_hand_adjustment
qty_reserved              quantity_reserve_adjustment
qty_on_order              quantity_on_stock_order_adjustment
qty_on_back_order         quantity_back_order_adjustment
qty_on_spec_order         quantity_on_special_order_adjustment

the biggest difference is sg 122 Collision Volume:  pdpmast diff: 2119  pdphist diff: 25447

    select '_0207', a.stock_group, a.description , 
      sum(cost * qty_on_hand)::integer as on_hand,
      sum(cost * qty_reserved)::integer as reserved,
      sum(cost * qty_on_order)::integer as order,
      sum(cost * qty_on_back_order)::integer as back_order,
      sum(cost * qty_on_spec_order)::integer as spec_order
    from arkona.ext_pdpsgrp a
    join arkona_cdc.pdpmast_20200207 b on a.stock_group = b.stocking_group
    where a.company_number = 'RY1'
      and a.stock_group = '122'
    group by a.stock_group, a.description
    union
    select '_0210', a.stock_group, a.description , 
      sum(cost * qty_on_hand)::integer as on_hand,
      sum(cost * qty_reserved)::integer as reserved,
      sum(cost * qty_on_order)::integer as order,
      sum(cost * qty_on_back_order)::integer as back_order,
      sum(cost * qty_on_spec_order)::integer as spec_order
    from arkona.ext_pdpsgrp a
    join arkona_cdc.pdpmast_20200210 b on a.stock_group = b.stocking_group
    where a.company_number = 'RY1'
      and a.stock_group = '122'
    group by a.stock_group, a.description    
union
select 'pdphist', stocking_group, 'desc',
  sum(quantity_on_hand_adjustment * cost_amount)::integer,
  sum(quantity_reserve_adjustment * cost_amount)::integer,
  sum(quantity_on_stock_order_adjustment * cost_amount)::integer,
  sum(quantity_back_order_adjustment * cost_amount)::integer,
  sum(quantity_on_special_order_adjustment * cost_amount)::integer
from arkona.ext_pdphist
where stocking_Group = '122'
  and transaction_time::Date between '02/08/2020' and '02/10/2020'
group by stocking_group  


select part_number, transaction_time::Date, quantity_on_hand_adjustment,
  quantity_reserve_adjustment, quantity_on_stock_order_adjustment,
  quantity_back_order_adjustment, quantity_on_special_order_adjustment, cost_amount
from arkona.ext_pdphist
where stocking_Group = '122'
  and transaction_time::Date between '02/08/2020' and '02/10/2020'

select *
from arkona.ext_pdphist
where part_number = '84208320'
  and transaction_time::Date between '02/08/2020' and '02/10/2020'

-- 02/13/20
after talking to greg, try to get trending for on hand only, reconciliation with accounting is another project entirely



select '_0207', a.stock_group, a.description , 
  sum(cost * qty_on_hand)::integer as on_hand
from arkona.ext_pdpsgrp a
join arkona_cdc.pdpmast_20200207 b on a.stock_group = b.stocking_group
where a.company_number = 'RY1'
  and a.stock_group = '122'
group by a.stock_group, a.description
union
select '_0212', a.stock_group, a.description , 
  sum(cost * qty_on_hand)::integer as on_hand
from arkona.ext_pdpsgrp a
join arkona_cdc.pdpmast_20200212 b on a.stock_group = b.stocking_group
where a.company_number = 'RY1'
  and a.stock_group = '122'
group by a.stock_group, a.description    
union
select 'pdphist', stocking_group, 'desc',
  sum(quantity_on_hand_adjustment * cost_amount)::integer
from arkona.ext_pdphist
where stocking_Group = '122'
  and transaction_time::Date between '02/08/2020' and '02/12/2020'
group by stocking_group  

select 424853 - 2904 - 421949 , pretty fing close (421962)

-- this looks pretty fucking good
select aa.*, bb._0214, bb._0214 - aa._0207 as delta, cc.pdphist, abs(bb._0214 - aa._0207) - abs(pdphist) as diff
FROM (
  select a.stock_group, a.description , 
    sum(cost * qty_on_hand)::integer as _0207
  from arkona.ext_pdpsgrp a
  join arkona_cdc.pdpmast_20200207 b on a.stock_group = b.stocking_group
  where a.company_number = 'RY1'
  group by a.stock_group, a.description) aa
left join (
  select a.stock_group, a.description , 
    sum(cost * qty_on_hand)::integer as _0214
  from arkona.ext_pdpsgrp a
  join arkona_cdc.pdpmast_20200214 b on a.stock_group = b.stocking_group
  where a.company_number = 'RY1'
  group by a.stock_group, a.description) bb on aa.stock_group = bb.stock_group  
left join (
  select stocking_group, 
    sum(quantity_on_hand_adjustment * cost_amount)::integer as pdphist
  from arkona.ext_pdphist
  where transaction_time::Date between '02/08/2020' and '02/14/2020'
  group by stocking_group) cc on aa.stock_group = cc.stocking_group  
order by aa.stock_group, aa.description



-- current
select aa.*, bb.feb
  from (
  select a.stock_group, a.description , 
    sum(cost * qty_on_hand)::integer as on_hand_0214
  from arkona.ext_pdpsgrp a
  join arkona_cdc.pdpmast_20200214 b on a.stock_group = b.stocking_group
  where a.company_number = 'RY1'
  group by a.stock_group, a.description) aa
left join (
  select stocking_group, 
    sum(quantity_on_hand_adjustment * cost_amount)::integer as feb
  from arkona.ext_pdphist
  where transaction_time::Date between '02/01/2020' and '02/14/2020'
  group by stocking_group) bb on aa.stock_group = bb.stocking_group    


-- so, lets sketch out total on hand inventory going back to june
-- starting with the on hand total on 02/14, 
-- subtract feb transactions to get balance at the end of january
-- subtract jan transactions to get balance at the end of december
-- this is exactly what i wanted to see, mirrors the accounting grap
select aa.*, aa.on_hand_0214 - bb.feb as jan, (aa.on_hand_0214 - bb.feb) - cc.jan as dec,
  ((aa.on_hand_0214 - bb.feb) - cc.jan) - dd.dec as nov, 
  (((aa.on_hand_0214 - bb.feb) - cc.jan) - dd.dec) - nov as oct,
  ((((aa.on_hand_0214 - bb.feb) - cc.jan) - dd.dec) - nov) - oct as sep,
  (((((aa.on_hand_0214 - bb.feb) - cc.jan) - dd.dec) - nov) - oct) - sep as aug,
  ((((((aa.on_hand_0214 - bb.feb) - cc.jan) - dd.dec) - nov) - oct) - sep) - aug as jul,
  (((((((aa.on_hand_0214 - bb.feb) - cc.jan) - dd.dec) - nov) - oct) - sep) - aug) - jul as jun
from (
  select sum(cost * qty_on_hand)::integer as on_hand_0214
  from arkona.ext_pdpsgrp a
  join arkona_cdc.pdpmast_20200214 b on a.stock_group = b.stocking_group
  where a.company_number = 'RY1') aa
join ( -- all feb 2020 transactions
  select sum(quantity_on_hand_adjustment * cost_amount)::integer as feb
  from arkona.ext_pdphist
  where transaction_time::Date between '02/01/2020' and '02/14/2020') bb on true
join ( -- 
  select sum(quantity_on_hand_adjustment * cost_amount)::integer as jan
  from arkona.ext_pdphist
  where transaction_time::Date between '01/01/2020' and '01/31/2020') cc on true
join ( -- 
  select sum(quantity_on_hand_adjustment * cost_amount)::integer as dec
  from arkona.ext_pdphist
  where transaction_time::Date between '12/01/2019' and '12/31/2019') dd on true  
join ( -- 
  select sum(quantity_on_hand_adjustment * cost_amount)::integer as nov
  from arkona.ext_pdphist
  where transaction_time::Date between '11/01/2019' and '11/30/2019') ee on true    
join ( -- 
  select sum(quantity_on_hand_adjustment * cost_amount)::integer as oct
  from arkona.ext_pdphist
  where transaction_time::Date between '10/01/2019' and '10/31/2019') ff on true 
join ( -- 
  select sum(quantity_on_hand_adjustment * cost_amount)::integer as sep
  from arkona.ext_pdphist
  where transaction_time::Date between '09/01/2019' and '09/30/2019') gg on true  
join ( -- 
  select sum(quantity_on_hand_adjustment * cost_amount)::integer as aug
  from arkona.ext_pdphist
  where transaction_time::Date between '08/01/2019' and '08/31/2019') hh on true         
join ( -- 
  select sum(quantity_on_hand_adjustment * cost_amount)::integer as jul
  from arkona.ext_pdphist
  where transaction_time::Date between '07/01/2019' and '07/31/2019') ii on true     

-- reverse the x axis
select 
  (((((((aa.on_hand_0214 - bb.feb) - cc.jan) - dd.dec) - nov) - oct) - sep) - aug) - jul as jun,
  ((((((aa.on_hand_0214 - bb.feb) - cc.jan) - dd.dec) - nov) - oct) - sep) - aug as jul,
  (((((aa.on_hand_0214 - bb.feb) - cc.jan) - dd.dec) - nov) - oct) - sep as aug,
  ((((aa.on_hand_0214 - bb.feb) - cc.jan) - dd.dec) - nov) - oct as sep,
  (((aa.on_hand_0214 - bb.feb) - cc.jan) - dd.dec) - nov as oct,
  ((aa.on_hand_0214 - bb.feb) - cc.jan) - dd.dec as nov,
  (aa.on_hand_0214 - bb.feb) - cc.jan as dec,
  aa.on_hand_0214 - bb.feb as jan, 
  aa.*
from (
  select sum(cost * qty_on_hand)::integer as on_hand_0214
  from arkona.ext_pdpsgrp a
  join arkona_cdc.pdpmast_20200214 b on a.stock_group = b.stocking_group
  where a.company_number = 'RY1') aa
join ( -- all feb 2020 transactions
  select sum(quantity_on_hand_adjustment * cost_amount)::integer as feb
  from arkona.ext_pdphist
  where transaction_time::Date between '02/01/2020' and '02/14/2020') bb on true
join ( -- 
  select sum(quantity_on_hand_adjustment * cost_amount)::integer as jan
  from arkona.ext_pdphist
  where transaction_time::Date between '01/01/2020' and '01/31/2020') cc on true
join ( -- 
  select sum(quantity_on_hand_adjustment * cost_amount)::integer as dec
  from arkona.ext_pdphist
  where transaction_time::Date between '12/01/2019' and '12/31/2019') dd on true  
join ( -- 
  select sum(quantity_on_hand_adjustment * cost_amount)::integer as nov
  from arkona.ext_pdphist
  where transaction_time::Date between '11/01/2019' and '11/30/2019') ee on true    
join ( -- 
  select sum(quantity_on_hand_adjustment * cost_amount)::integer as oct
  from arkona.ext_pdphist
  where transaction_time::Date between '10/01/2019' and '10/31/2019') ff on true 
join ( -- 
  select sum(quantity_on_hand_adjustment * cost_amount)::integer as sep
  from arkona.ext_pdphist
  where transaction_time::Date between '09/01/2019' and '09/30/2019') gg on true  
join ( -- 
  select sum(quantity_on_hand_adjustment * cost_amount)::integer as aug
  from arkona.ext_pdphist
  where transaction_time::Date between '08/01/2019' and '08/31/2019') hh on true         
join ( -- 
  select sum(quantity_on_hand_adjustment * cost_amount)::integer as jul
  from arkona.ext_pdphist
  where transaction_time::Date between '07/01/2019' and '07/31/2019') ii on true     




drop table if exists transactions;
create temp table transactions as
select stocking_group, 
  coalesce((sum(quantity_on_hand_adjustment * cost_amount) filter (where transaction_time::Date between '03/01/2019' and '03/31/2019'))::integer, 0) as mar_19,
  coalesce((sum(quantity_on_hand_adjustment * cost_amount) filter (where transaction_time::Date between '04/01/2019' and '04/30/2019'))::integer, 0) as apr_19,
  coalesce((sum(quantity_on_hand_adjustment * cost_amount) filter (where transaction_time::Date between '05/01/2019' and '05/31/2019'))::integer, 0) as may_19,
  coalesce((sum(quantity_on_hand_adjustment * cost_amount) filter (where transaction_time::Date between '06/01/2019' and '06/30/2019'))::integer, 0) as jun_19,
  coalesce((sum(quantity_on_hand_adjustment * cost_amount) filter (where transaction_time::Date between '07/01/2019' and '07/31/2019'))::integer, 0) as jul_19,
  coalesce((sum(quantity_on_hand_adjustment * cost_amount) filter (where transaction_time::Date between '08/01/2019' and '08/31/2019'))::integer, 0) as aug_19,
  coalesce((sum(quantity_on_hand_adjustment * cost_amount) filter (where transaction_time::Date between '09/01/2019' and '09/30/2019'))::integer, 0) as sep_19,
  coalesce((sum(quantity_on_hand_adjustment * cost_amount) filter (where transaction_time::Date between '10/01/2019' and '10/31/2019'))::integer, 0) as oct_19,
  coalesce((sum(quantity_on_hand_adjustment * cost_amount) filter (where transaction_time::Date between '11/01/2019' and '11/30/2019'))::integer, 0) as nov_19,
  coalesce((sum(quantity_on_hand_adjustment * cost_amount) filter (where transaction_time::Date between '12/01/2019' and '12/31/2019'))::integer, 0) as dec_19,
  coalesce((sum(quantity_on_hand_adjustment * cost_amount) filter (where transaction_time::Date between '01/01/2020' and '01/31/2020'))::integer, 0) as jan_20,
  coalesce((sum(quantity_on_hand_adjustment * cost_amount) filter (where transaction_time::Date between '02/01/2020' and '02/14/2020'))::integer, 0) as feb_20
from arkona.ext_pdphist
where transaction_time::Date between '03/01/2019' and '02/14/2020'
  and company_number = 'RY1'
  and stocking_group is not null
  and stocking_group not like '9%'
group by stocking_group;


select *
from transactions




select *
from arkona.ext_pdphist
where  transaction_time::Date = current_date - 1

select company_number, stocking_group
from arkona.ext_pdphist
where  transaction_time::Date = current_date - 1
group by company_number, stocking_group
order by company_number, stocking_group

select aa.stock_group, aa.description,
  aa.on_hand_0214 - bb.feb_20 - jan_20 - dec_19 - nov_19 - oct_19 - sep_19 - aug_19 - jul_19 - jun_19 - may_19 - apr_19 - mar_19 as feb_19,
  aa.on_hand_0214 - bb.feb_20 - jan_20 - dec_19 - nov_19 - oct_19 - sep_19 - aug_19 - jul_19 - jun_19 - may_19 - apr_19 as mar_19,
  aa.on_hand_0214 - bb.feb_20 - jan_20 - dec_19 - nov_19 - oct_19 - sep_19 - aug_19 - jul_19 - jun_19 - may_19 as apr_19,
  aa.on_hand_0214 - bb.feb_20 - jan_20 - dec_19 - nov_19 - oct_19 - sep_19 - aug_19 - jul_19 - jun_19 as may_19,
  aa.on_hand_0214 - bb.feb_20 - jan_20 - dec_19 - nov_19 - oct_19 - sep_19 - aug_19 - jul_19 as jun_19,
  aa.on_hand_0214 - bb.feb_20 - jan_20 - dec_19 - nov_19 - oct_19 - sep_19 - aug_19 as jul_19,
  aa.on_hand_0214 - bb.feb_20 - jan_20 - dec_19 - nov_19 - oct_19 - sep_19 as aug_19,
  aa.on_hand_0214 - bb.feb_20 - jan_20 - dec_19 - nov_19 - oct_19 as sep_19,
  aa.on_hand_0214 - bb.feb_20 - jan_20 - dec_19 - nov_19 as oct_19,
  aa.on_hand_0214 - bb.feb_20 - jan_20 - dec_19 as nov_19,
  aa.on_hand_0214 - bb.feb_20 - jan_20 as dec_19,
  aa.on_hand_0214 - bb.feb_20 as jan_20,
  aa.on_hand_0214 as feb_20
from (
  select a.stock_group, a.description , 
    sum(cost * qty_on_hand)::integer as on_hand_0214
  from arkona.ext_pdpsgrp a
  join arkona.ext_pdpmast b on a.stock_group = b.stocking_group
  where a.company_number = 'RY1'
  group by a.stock_group, a.description) aa
join transactions bb on aa.stock_group = bb.stocking_group  





--------------------------------------------------------------------------
-- holy fucking shit, trying to reconcile pdphist to pdptdet
-- at least for this one part, 9003BP, pdphist gives me better info
-- drilling into stk grp 120, pdphist: -505, pdptdet: -1269
--------------------------------------------------------------------------
select ptco_, ptinv_, ptcode, ptdate, ptpart, ptsgrp, ptqty,  ptcost
-- select sum(ptqty * ptcost) -- 1269
from arkona.ext_pdptdet
where arkona.db2_integer_to_date(ptdate) between '02/01/2020' and '02/12/2020'
  and ptsgrp = '120'
  and ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 

select *
from (
  select part_number, transaction_time::date, quantity_on_hand_adjustment, transaction_type, source_document_Type, source_Document_number
  -- select sum(quantity_on_hand_adjustment * cost_amount)::integer -- 505
  from arkona.ext_pdphist  
  where transaction_time::Date between '02/01/2020' and '02/12/2020'
    and stocking_group = '120') a
full outer join  (
  select ptco_, ptinv_, ptcode, ptdate, ptpart, ptsgrp, ptqty,  ptcost
  -- select sum(ptqty * ptcost) -- 1269
  from arkona.ext_pdptdet
  where arkona.db2_integer_to_date(ptdate) between '02/01/2020' and '02/12/2020'
    and ptsgrp = '120'
    and ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') ) b on a.part_number = b.ptpart and a.source_document_number = b.ptinv_

select *
from arkona.ext_pdptdet
where ptpart = '9003BP'
  and arkona.db2_integer_to_date(ptdate) between '01/01/2020' and '02/28/2020'

  select *
from arkona.ext_pdphist
where part_number = '9003BP'

--------------------------------------------------------------------------
  
  