﻿do
$$
declare
  _date date := current_date - 1;
  _store integer := 39;
begin
drop table if exists wtf;
create temp table wtf as

    select
    --TODAY
    count(case when e.code in('NBT', 'NBC') and a.vehicle_type = 'N' and boarded_date = _date::date and b.store_key = 39  and d.board_sub_type = 'Retail' then a.board_id end) as today_buick,
    count(case when e.code in('NBT', 'NBC') and a.vehicle_type = 'N' and boarded_date = _date::date and b.store_key = 39  and d.board_type = 'Back-on' then a.board_id end) as today_buick_back,
    count(case when e.code in('NCT', 'NCC') and a.vehicle_type = 'N' and boarded_date = _date::date and b.store_key = 39 and d.board_sub_type = 'Retail' then a.board_id end) as today_chev,
    count(case when e.code in('NCT', 'NCC') and a.vehicle_type = 'N' and boarded_date = _date::date and b.store_key = 39 and d.board_type = 'Back-on' then a.board_id end) as today_chev_back,
    count(case when e.code in('CAC', 'CAT') and a.vehicle_type = 'N' and boarded_date = _date::date and b.store_key = 39  and d.board_sub_type = 'Retail' then a.board_id end) as today_cad, 
    count(case when e.code in('CAC', 'CAT') and a.vehicle_type = 'N' and boarded_date = _date::date and b.store_key = 39  and d.board_type = 'Back-on' then a.board_id end) as today_cad_back, 
    count(case when e.code = 'NGT' and a.vehicle_type = 'N' and boarded_date = _date::date and b.store_key = 39 and d.board_sub_type = 'Retail'  then a.board_id end) as today_gmc,
    count(case when e.code = 'NGT' and a.vehicle_type = 'N' and boarded_date = _date::date and b.store_key = 39 and d.board_type = 'Back-on' then a.board_id end) as today_gmc_back,
    count(case when d.board_sub_type = 'Fleet' and boarded_date = _date::date and b.store_key = 39 then a.board_id end) as today_fleet, 
    count(case when e.code in ('NBT','CAT','NCT','NGT','NBC','NCC','CAC') and a.vehicle_type = 'N' and boarded_date = _date::date and d.board_sub_type = 'Retail' and b.store_key = 39 then a.board_id end) as today_new_gm, 
    count(case when e.code in ('NBT','CAT','NCT','NGT','NBC','NCC','CAC') and a.vehicle_type = 'N' and boarded_date = _date::date and d.board_type = 'Back-on' and b.store_key = 39 then a.board_id end) as today_new_gm_back, 
    count(case when d.board_type = 'Deal' and d.board_sub_type  in ('Wholesale', 'Intercompany Wholesale', 'Coaches', 'Drivers Training')  and boarded_date = _date::date and b.store_key = 39 then a.board_id end) as today_wholesale_gm,
    count(case when d.board_sub_type = 'Trade' and boarded_date = _date::date and b.store_key = 39 then a.board_id end) as today_trade_gm,
    count(case when a.vehicle_type = 'U'  and boarded_date = _date::date and d.board_sub_type = 'Retail' and b.store_key = 39 then a.board_id end) as today_gm_used, 
    count(case when a.vehicle_type = 'U'  and boarded_date = _date::date and d.board_type = 'Back-on' and b.store_key = 39 then a.board_id end) as today_gm_used_back,
    count(case when e.code in ('CAT','CAC') and boarded_date = _date::date and d.board_sub_type = 'Fleet' and b.store_key = 39 then a.board_id end) as today_fleet_cad_count, 
    count(case when e.code in ('NCT','NCC') and boarded_date = _date::date and d.board_sub_type = 'Fleet' and b.store_key = 39 then a.board_id end) as today_fleet_chev_count,
    count(case when e.code in ('NBC','NBT') and boarded_date = _date::date and d.board_sub_type = 'Fleet' and b.store_key = 39 then a.board_id end) as today_fleet_buick_count, 
    count(case when e.code in ('NGT') and boarded_date = _date::date and d.board_sub_type = 'Fleet' and b.store_key = 39 then a.board_id end) as today_fleet_gmc_count,  
    -- BUICK
    count(case when e.code = 'NBT' and b.store_key = 39 and a.vehicle_type = 'N' and d.board_sub_type = 'Retail' then a.board_id end) as new_buick_truck_count, 
    count(case when e.code = 'NBC' and b.store_key = 39 and a.vehicle_type = 'N' and d.board_sub_type = 'Retail' then a.board_id end) as new_buick_car_count, 
    count(case when e.code = 'NBT' and b.store_key = 39 and a.vehicle_type = 'N' and d.board_type = 'Back-on' then a.board_id end) as new_buick_truck_back_count, 
    count(case when e.code = 'NBC' and b.store_key = 39 and a.vehicle_type = 'N' and d.board_type = 'Back-on' then a.board_id end) as new_buick_car_back_count, 
    --CHEV
    count(case when e.code = 'NCC' and b.store_key = 39 and a.vehicle_type = 'N' and d.board_sub_type = 'Retail'  then a.board_id end) as new_chev_car_count, 
    count(case when e.code = 'NCT' and b.store_key = 39 and a.vehicle_type = 'N' and d.board_sub_type = 'Retail' then a.board_id end) as new_chev_truck_count, 
    count(case when e.code = 'NCC' and b.store_key = 39 and a.vehicle_type = 'N' and d.board_type = 'Back-on' then a.board_id end) as new_chev_car_back_count, 
    count(case when e.code = 'NCT' and b.store_key = 39 and a.vehicle_type = 'N'  and d.board_type = 'Back-on' then a.board_id end) as new_chev_truck_back_count, 
    --CADILLAC
    count(case when e.code = 'CAC' and b.store_key = 39 and a.vehicle_type = 'N' and d.board_sub_type = 'Retail' then a.board_id end) as new_cad_car_count, 
    count(case when e.code = 'CAT' and b.store_key = 39  and a.vehicle_type = 'N' and d.board_sub_type = 'Retail' then a.board_id end) as new_cad_truck_count,
    count(case when e.code = 'CAC' and b.store_key = 39 and a.vehicle_type = 'N' and d.board_type = 'Back-on' then a.board_id end) as new_cad_car_back_count, 
    count(case when e.code = 'CAT' and b.store_key = 39 and a.vehicle_type = 'N'  and d.board_type = 'Back-on' then a.board_id end) as new_cad_truck_back_count,
    --GMC
    count(case when e.code = 'NGT' and b.store_key = 39 and a.vehicle_type = 'N' and d.board_sub_type = 'Retail'  then a.board_id end) as new_gmc_count, 
    count(case when e.code = 'NGT' and b.store_key = 39 and a.vehicle_type = 'N' and d.board_type = 'Back-on' then a.board_id end) as new_gmc_back_count, 
    --FLEET
    count(case when e.code in ('NBT','CAT','NCT','NGT') and d.board_sub_type = 'Fleet' and b.store_key = 39 then a.board_id end) as new_fleet_truck_count, 
    count(case when e.code in ('NBC','NCC','CAC') and d.board_sub_type = 'Fleet' and b.store_key = 39 then a.board_id end) as new_fleet_car_count,
    count(case when e.code in ('NBT') and d.board_sub_type = 'Fleet' and b.store_key = 39 then a.board_id end) as fleet_buick_truck_count, 
    count(case when e.code in ('NBC') and d.board_sub_type = 'Fleet' and b.store_key = 39 then a.board_id end) as fleet_buick_car_count, 
    count(case when e.code in ('NBC','NBT') and d.board_sub_type = 'Fleet' and b.store_key = 39 then a.board_id end) as fleet_buick_count, 
    count(case when e.code in ('NCC') and d.board_sub_type = 'Fleet' and b.store_key = 39 then a.board_id end) as fleet_chev_car_count, 
    count(case when e.code in ('NCT') and d.board_sub_type = 'Fleet' and b.store_key = 39 then a.board_id end) as fleet_chev_truck_count,
    count(case when e.code in ('NCT','NCC') and d.board_sub_type = 'Fleet' and b.store_key = 39 then a.board_id end) as fleet_chev_count,
    count(case when e.code in ('CAC') and d.board_sub_type = 'Fleet' and b.store_key = 39 then a.board_id end) as fleet_cad_car_count,  
    count(case when e.code in ('CAT') and d.board_sub_type = 'Fleet' and b.store_key = 39 then a.board_id end) as fleet_cad_truck_count,  
    count(case when e.code in ('CAT','CAC') and d.board_sub_type = 'Fleet' and b.store_key = 39 then a.board_id end) as fleet_cad_count,  
    count(case when e.code in ('NGT') and d.board_sub_type = 'Fleet' and b.store_key = 39 then a.board_id end) as fleet_gmc_truck_count,  
    -- GM NEW TOTAL
    count(case when e.code in ('NBT','CAT','NCT','NGT','NBC','NCC','CAC') and a.vehicle_type = 'N' and d.board_sub_type = 'Retail' and b.store_key = 39 then a.board_id end) as new_gm_count, 
    count(case when e.code in ('NBT','CAT','NCT','NGT','NBC','NCC','CAC') and a.vehicle_type = 'N' and d.board_type = 'Back-on' and b.store_key = 39 and b.board_id <> '24251' then a.board_id end) as new_back_gm_count, 
    count(case when e.code in ('NBT','CAT','NCT','NGT') and a.vehicle_type = 'N' and d.board_sub_type = 'Retail' and b.store_key = 39 then a.board_id end) as new_gm_truck_count,
    count(case when e.code in ('NBT','CAT','NCT','NGT') and a.vehicle_type = 'N' and d.board_type = 'Back-on' and b.store_key = 39 and b.board_id <> '24251' then a.board_id end) as new_gm_back_truck_count,
    count(case when e.code in ('NBC','NCC','CAC') and a.vehicle_type = 'N' and d.board_sub_type = 'Retail' and b.store_key = 39 then a.board_id end) as new_gm_car_count,
    count(case when e.code in ('NBC','NCC','CAC') and a.vehicle_type = 'N' and d.board_type = 'Back-on' and b.store_key = 39 then a.board_id end) as new_gm_car_back_count,
    --GM USED
    count(case when vehicle_type = 'U' and d.board_sub_type = 'Retail' and b.store_key = 39 then a.board_id end) as gm_used_retail_count, 
    count(case when vehicle_type = 'U' and d.board_type = 'Back-on' and b.store_key = 39 then a.board_id end) as gm_used_retail_back_count,
    count(case when e.code in ('UC','UCC', 'UCO') and d.board_sub_type = 'Retail' and b.store_key = 39 then a.board_id end) as used_gm_car_count,
    count(case when e.code in ('UC','UCC', 'UCO') and d.board_type = 'Back-on' and b.store_key = 39 then a.board_id end) as used_gm_car_back_count,
    count(case when e.code in ('UT','UTC', 'UTO') and d.board_sub_type = 'Retail' and b.store_key = 39 then a.board_id end) as used_gm_truck_count,
    count(case when e.code in ('UT','UTC', 'UTO') and d.board_type = 'Back-on' and b.store_key = 39 then a.board_id end) as used_gm_truck_back_count,
    count(case when e.code in ('UC','UCC', 'UCO') and b.used_vehicle_package = 'Factory Certified' and d.board_sub_type = 'Retail' and b.store_key = 39 then a.board_id end) as gm_used_car_cert_count, 
    count(case when e.code in ('UC','UCC', 'UCO') and b.used_vehicle_package = 'Factory Certified' and d.board_type = 'Back-on' and b.store_key = 39 then a.board_id end) as gm_used_back_car_cert_count, 
    count(case when e.code in ('UC','UCC', 'UCO') and b.used_vehicle_package <> 'Factory Certified' and d.board_sub_type = 'Retail' and b.store_key = 39 then a.board_id end) as gm_used_car_other_count, 
    count(case when e.code in ('UC','UCC', 'UCO') and b.used_vehicle_package <> 'Factory Certified' and d.board_type = 'Back-on' and b.store_key = 39 then a.board_id end) as gm_used_back_car_other_count, 
    count(case when e.code in ('UT','UTC', 'UTO') and b.used_vehicle_package = 'Factory Certified' and d.board_sub_type = 'Retail' and b.store_key = 39 then a.board_id end) as gm_used_truck_cert_count, 
    count(case when e.code in ('UT','UTC', 'UTO') and b.used_vehicle_package = 'Factory Certified' and d.board_type = 'Back-on' and b.store_key = 39 then a.board_id end) as gm_used_back_truck_cert_count, 
    count(case when e.code in ('UT','UTC', 'UTO') and b.used_vehicle_package <> 'Factory Certified' and d.board_sub_type = 'Retail' and b.store_key = 39 then a.board_id end) as gm_used_truck_other_count, 
    count(case when e.code in ('UT','UTC', 'UTO') and b.used_vehicle_package <> 'Factory Certified' and d.board_type = 'Back-on' and b.store_key = 39 then a.board_id end) as gm_used_back_truck_other_count, 
    --GM Wholesale
    count(case when  d.board_type = 'Deal' and d.board_sub_type  in ('Wholesale', 'Intercompany Wholesale', 'Coaches','Drivers Training')  and b.store_key = 39 then a.board_id end) as gm_wholesale_count,
    count(case when e.code in ('UC','UCC', 'UCO') and d.board_sub_type  in ('Wholesale', 'Intercompany Wholesale', 'Coaches','Drivers Training')  and b.store_key = 39 then a.board_id end) as used_gm_car_whole_count,
    count(case when e.code in ('UT','UTC', 'UTO') and d.board_sub_type  in ('Wholesale', 'Intercompany Wholesale', 'Coaches','Drivers Training')  and b.store_key = 39 then a.board_id end) as used_gm_truck_whole_count,
    -- GM TRADES
    count(case when  a.sale_code in ('UC','UCC', 'UCO','UT','UTC', 'UTO','OUT')and d.board_sub_type = 'Trade' and b.store_key = 39 then a.board_id end) as gm_trade_count,
    count(case when d.board_type = 'Deal' and a.sale_code in ('UC','UCC', 'UCO') and d.board_sub_type = 'Trade' and b.store_key = 39 then a.board_id end) as used_gm_car_trade_count,
    count(case when d.board_type = 'Deal' and a.sale_code in ('UT','UTC', 'UTO','OUT') and d.board_sub_type = 'Trade' and b.store_key = 39 then a.board_id end) as used_gm_truck_trade_count,
    -- TODAY HN
    count(case when e.code in ('2HT', '2HC') and boarded_date = _date::date and b.store_key = 40  and a.vehicle_type = 'N' and d.board_sub_type = 'Retail' then a.board_id end) as honda_today,
    count(case when e.code in ('2HT', '2HC') and boarded_date = _date::date and b.store_key = 40 and a.vehicle_type = 'N' and d.board_sub_type = 'Back-on' then a.board_id end) as honda_back_today,
    count(case when e.code in ('2NT', '2NC') and boarded_date = _date::date and b.store_key = 40  and a.vehicle_type = 'N' and d.board_sub_type = 'Retail' then a.board_id end) as nissan_today,
    count(case when e.code in ('2NT', '2NC') and boarded_date = _date::date and b.store_key = 40  and a.vehicle_type = 'N' and d.board_sub_type = 'Back-on' then a.board_id end) as nissan_back_today,
    count(case when e.code in ('2HT','2HC', '2NT','2NC') and boarded_date = _date::date and a.vehicle_type = 'N' and d.board_sub_type = 'Retail' and b.store_key = 40 then a.board_id end) as hn_new_today,
    count(case when e.code in ('2HT','2HC', '2NT','2NC') and boarded_date = _date::date and  a.vehicle_type = 'N' and d.board_sub_type = 'Back-on' and b.store_key = 40 then a.board_id end) as hn_new_back_today,
    count(case when a.sale_code in ('HCT', 'HUT', 'NCT', 'NUT', 'OUT','HCC', 'HUC', 'NCC', 'NUC', 'OUC', 'UCO') and boarded_date = _date::date and d.board_sub_type = 'Trade' and b.store_key = 40 then a.board_id end) as hn_trade_today, 
    count(case when d.board_type = 'Deal'  and boarded_date = _date::date and d.board_sub_type  in ('Wholesale', 'Intercompany Wholesale', 'Coaches','Drivers Training')  and b.store_key = 40 then a.board_id end) as hn_wholesale_today, 
    count(case when a.vehicle_type = 'U' and d.board_sub_type = 'Retail' and boarded_date = _date::date and b.store_key = 40 then a.board_id end) as hn_used_today, 
    count(case when a.vehicle_type = 'U' and d.board_sub_type = 'Back-on' and boarded_date = _date::date and b.store_key = 40 then a.board_id end) as hn_used_back_today, 
    --HONDA
    count(case when e.code = '2HT' and a.vehicle_type = 'N' and b.store_key = 40  and d.board_sub_type = 'Retail' then a.board_id end) as new_honda_truck_count, 
    count(case when e.code = '2HC' and a.vehicle_type = 'N' and b.store_key = 40 and d.board_sub_type = 'Retail' then a.board_id end) as new_honda_car_count, 
    count(case when e.code = '2HT' and a.vehicle_type = 'N' and b.store_key = 40 and d.board_type = 'Back-on' then a.board_id end) as new_honda_truck_back_count, 
    count(case when e.code = '2HC' and a.vehicle_type = 'N' and b.store_key = 40 and d.board_type = 'Back-on' then a.board_id end) as new_honda_car_back_count, 
    --NISSAN
    count(case when e.code = '2NT' and a.vehicle_type = 'N' and b.store_key = 40  and d.board_sub_type = 'Retail' then a.board_id end) as new_nissan_truck_count, 
    count(case when e.code = '2NC' and a.vehicle_type = 'N' and b.store_key = 40 and d.board_sub_type = 'Retail' then a.board_id end) as new_nissan_car_count, 
    count(case when e.code = '2NT' and a.vehicle_type = 'N' and b.store_key = 40 and d.board_type = 'Back-on' then a.board_id end) as new_nissan_truck_back_count, 
    count(case when e.code = '2NC' and a.vehicle_type = 'N' and b.store_key = 40 and d.board_type = 'Back-on' then a.board_id end) as new_nissan_car_back_count,
    -- HN TRADES
    count(case when a.sale_code in ('HCT', 'HUT', 'NCT', 'NUT', 'OUT','HCC', 'HUC', 'NCC', 'NUC', 'OUC', 'UCO') and d.board_sub_type = 'Trade' and b.store_key = 40 then a.board_id end) as hn_trade_count, 
    count(case when a.sale_code in ('HCT', 'HUT', 'NCT', 'NUT', 'OUT') and d.board_sub_type = 'Trade' and b.store_key = 40 then a.board_id end) as hn_used_truck_trade_count, 
    count(case when a.sale_code in ('HCC', 'HUC', 'NCC', 'NUC', 'OUC', 'UCO') and d.board_sub_type = 'Trade' and b.store_key = 40 then a.board_id end) as hn_used_car_trade_count, 
    -- HN WHOLESALE
    count(case when d.board_type = 'Deal' and d.board_sub_type  in ('Wholesale', 'Intercompany Wholesale', 'Coaches', 'Drivers Training')  and b.store_key = 40 then a.board_id end) as hn_wholesale_count, 
    count(case when d.board_type = 'Deal' and e.code in ('HCT', 'HUT', 'NCT', 'NUT', 'OUT') and d.board_sub_type  in ('Wholesale', 'Intercompany Wholesale', 'Coaches', 'Drivers Training')  and b.store_key = 40 then a.board_id end) as hn_used_truck_whole_count, 
    count(case when d.board_type = 'Deal' and e.code in ('HCC', 'HUC', 'NCC', 'NUC', 'OUC', 'UCO') and d.board_sub_type  in ('Wholesale', 'Intercompany Wholesale', 'Coaches', 'Drivers Training')  and b.store_key = 40 then a.board_id end) as hn_used_car_whole_count, 

    -- HN TOTAL
    count(case when e.code in ('2HT','2HC', '2NT','2NC') and a.vehicle_type = 'N' and d.board_sub_type = 'Retail' and b.store_key = 40 then a.board_id end) as new_hn_count, 
    count(case when e.code in  ('2HT','2HC', '2NT','2NC') and a.vehicle_type = 'N' and d.board_type = 'Back-on' and b.store_key = 40 then a.board_id end) as new_back_hn_count, 
    count(case when e.code in ('2HT', '2NT') and a.vehicle_type = 'N' and d.board_sub_type = 'Retail' and b.store_key = 40 then a.board_id end) as new_hn_truck_count, 
    count(case when e.code in  ('2HT', '2NT') and a.vehicle_type = 'N' and d.board_type = 'Back-on' and b.store_key = 40 then a.board_id end) as new_back_hn_truck_count, 
    count(case when e.code in ('2HC', '2NC') and a.vehicle_type = 'N' and d.board_sub_type = 'Retail' and b.store_key = 40 then a.board_id end) as new_hn_car_count, 
    count(case when e.code in  ('2HC', '2NC') and a.vehicle_type = 'N' and d.board_type = 'Back-on' and b.store_key = 40 then a.board_id end) as new_back_hn_car_count, 
    --HN USED 
    count(case when a.vehicle_type = 'U' and d.board_sub_type = 'Retail' and b.store_key = 40 then a.board_id end) as honda_used_retail_count, 
    count(case when a.vehicle_type = 'U' and d.board_type = 'Back-on' and b.store_key = 40 then a.board_id end) as honda_used_retail_back_count,
    count(case when a.vehicle_type = 'U' and d.board_type = 'Deal' and d.board_sub_type  in ('Wholesale', 'Intercompany Wholesale', 'Coaches', 'Drivers Training')  and b.store_key = 40 then a.board_id end) as honda_used_wholesale_count,
    count(case when e.code in ('HCC', 'HUC', 'NCC', 'NUC', 'OUC', 'UCO') and d.board_sub_type = 'Retail' and b.store_key = 40 then a.board_id end) as hn_used_car_retail_count, 
    count(case when e.code in ('HCC', 'HUC', 'NCC', 'NUC', 'OUC', 'UCO') and d.board_type = 'Back-on' and b.store_key = 40 then a.board_id end) as hn_used_car_back_count, 
    count(case when e.code in ('HCT', 'HUT', 'NCT', 'NUT', 'OUT') and d.board_sub_type = 'Retail' and b.store_key = 40 then a.board_id end) as hn_used_truck_retail_count, 
    count(case when e.code in ('HCT', 'HUT', 'NCT', 'NUT', 'OUT') and d.board_sub_type = 'Back-on' and b.store_key = 40 then a.board_id end) as hn_used_truck_back_count, 

    count(case when  e.code in ('HCC', 'HUC', 'NCC', 'NUC', 'OUC', 'UCO') and d.board_sub_type = 'Retail' and b.used_vehicle_package = 'Factory Certified' and b.store_key = 40 then a.board_id end) as hn_used_car_cert, 
    count(case when  e.code in ('HCC', 'HUC', 'NCC', 'NUC', 'OUC', 'UCO') and d.board_type = 'Back-on' and b.used_vehicle_package= 'Factory Certified'  and b.store_key = 40 then a.board_id end) as hn_used_car_cert_back,
    count(case when  e.code in ('HCC', 'HUC', 'NCC', 'NUC', 'OUC', 'UCO') and d.board_sub_type = 'Retail' and b.used_vehicle_package <> 'Factory Certified' and b.store_key = 40 then a.board_id end) as hn_used_car_other, 
    count(case when  e.code in ('HCC', 'HUC', 'NCC', 'NUC', 'OUC', 'UCO') and d.board_type = 'Back-on' and b.used_vehicle_package <> 'Factory Certified'  and b.store_key = 40 then a.board_id end) as hn_used_car_other_back,

    count(case when  e.code in ('HCT', 'HUT', 'NCT', 'NUT', 'OUT') and d.board_sub_type = 'Retail' and b.used_vehicle_package = 'Factory Certified' and b.store_key = 40 then a.board_id end) as hn_used_truck_cert, 
    count(case when  e.code in ('HCT', 'HUT', 'NCT', 'NUT', 'OUT') and d.board_type = 'Back-on' and b.used_vehicle_package= 'Factory Certified'  and b.store_key = 40 then a.board_id end) as hn_used_truck_cert_back,
    count(case when  e.code in ('HCT', 'HUT', 'NCT', 'NUT', 'OUT') and d.board_sub_type = 'Retail' and b.used_vehicle_package <> 'Factory Certified' and b.store_key = 40 then a.board_id end) as hn_used_truck_other, 
    count(case when  e.code in ('HCT', 'HUT', 'NCT', 'NUT', 'OUT') and d.board_type = 'Back-on' and b.used_vehicle_package <> 'Factory Certified'  and b.store_key = 40 then a.board_id end) as hn_used_truck_other_back,

    -- TODAY OUTLET
    count(case when  a.sale_code in ('UC','UCC', 'UCO','UT','UTC','OUC', 'UTO','OUT','HUC','NUC') and boarded_date = _date::date and d.board_sub_type = 'Retail' and b.store_key = 41 then a.board_id end) as today_outlet,
    count(case when  a.sale_code in ('UC','UCC', 'UCO','UT','UTC', 'OUC','UTO','OUT','HUC','NUC')and boarded_date = _date::date and d.board_sub_type = 'Back-on' and b.store_key = 41 then a.board_id end) as today_outlet_back,
    count(case when  boarded_date = _date::date and d.board_sub_type = 'Trade' and b.store_key = 41 then a.board_id end) as today_outlet_trade,

    --AUTO OUTLET
    count(case when  a.sale_code in ('UC','UCC', 'UCO','UT','UTC','OUC', 'UTO','OUT','HUC','NUC')and d.board_sub_type = 'Retail' and b.store_key = 41 then a.board_id end) as out_used_count,
    count(case when a.sale_code in ('UC','UCC', 'UCO','HUC','NUC') and d.board_sub_type = 'Retail' and b.store_key = 41 then a.board_id end) as used_out_car_count,
    count(case when a.sale_code in ('UT','UTC', 'UTO','OUT') and d.board_sub_type = 'Retail' and b.store_key = 41 then a.board_id end) as used_out_truck_count,

    count(case when a.sale_code in ('UC','UCC', 'UCO','UT','UTC', 'OUC','UTO','OUT','HUC','NUC') and d.board_type = 'Back-on' and b.store_key = 41 then a.board_id end) as out_used_back_count,
    count(case when a.sale_code in ('UC','UCC', 'UCO','HUC','NUC') and d.board_type = 'Back-on' and b.store_key = 41 then a.board_id end) as used_out_car_back_count,
    count(case when a.sale_code in ('UT','UTC', 'UTO','OUT') and d.board_type = 'Back-on' and b.store_key = 41 then a.board_id end) as used_out_truck_back_count,

    count(case when  a.sale_code in ('UC','UCC', 'UCO','UT','UTC','OUC', 'UTO','OUT')and d.board_sub_type = 'Trade' and b.store_key = 41 then a.board_id end) as out_trade_count,
    count(case when a.sale_code in ('UC','UCC', 'UCO') and d.board_sub_type = 'Trade' and b.store_key = 41 then a.board_id end) as used_out_car_trade_count,
    count(case when a.sale_code in ('UT','UTC', 'UTO','OUT') and d.board_sub_type = 'Trade' and b.store_key = 41 then a.board_id end) as used_out_truck_trade_count,

    round( coalesce( sum(case when b.store_key = _store and a.vehicle_type = 'N' and boarded_date = _date::date and d.board_sub_type in( 'Retail' , 'Fleet', 'N/A') then f.amount end),0) )as total_new_fi_gross_f,
    round( coalesce(sum(case when b.store_key = _store and a.vehicle_type = 'N' and boarded_date = _date::date and d.board_sub_type in( 'Retail' , 'Fleet','N/A') then g.amount end),0)) as total_new_front_gross_f,

    round( coalesce( sum(case when b.store_key = _store and a.vehicle_type = 'N' and boarded_date = _date::date and d.board_sub_type = 'Retail'  then f.amount end),0) )as total_new_fi_gross,
    round( coalesce(sum(case when b.store_key = _store and a.vehicle_type = 'N' and boarded_date = _date::date and d.board_sub_type = 'Retail' then g.amount end),0)) as total_new_front_gross,
    round(  coalesce( sum(case when b.store_key = _store and a.vehicle_type = 'N' and boarded_date = _date::date and d.board_sub_type = 'Fleet'  then f.amount end),0) )as total_fleet_fi_gross,
    round(coalesce(  sum(case when b.store_key = _store and a.vehicle_type = 'N' and boarded_date = _date::date and d.board_sub_type = 'Fleet' then g.amount end),0))as total_fleet_front_gross,
    round(coalesce(sum(case when b.store_key = _store and a.vehicle_type = 'U' and boarded_date = _date::date and d.board_sub_type in ('Retail', 'Wholesale','N/A')  then f.amount end),0)) as total_used_fi_gross,
    round(coalesce(sum(case when b.store_key = _store and a.vehicle_type = 'U' and boarded_date = _date::date and d.board_sub_type in ('Retail', 'Wholesale','N/A') then g.amount end),0)) as total_used_front_gross,
    round(coalesce(sum(case when b.store_key = _store and boarded_date = _date::date  then h.amount end),0)) as total_trade_allowance,
    round(coalesce(sum(case when b.store_key = _store and boarded_date = _date::date  then i.amount end),0)) as total_trade_acv
-- select b.*
    from board.sales_board  b
--     join board.daily_board a on b.board_id = a.board_id -- 2 rows when there are 2 consultants
    join (
      select a.board_id, a.sale_code, a.vehicle_type
      from board.daily_board a
      join board.sales_board b on a.board_id = b.board_id
        and b.boarded_date between date_trunc('MONTH',coalesce(_date::date,current_date))::DATE and coalesce(_date::date,current_date)
      group by a.board_id, a.sale_code, a.vehicle_type) a on b.board_id = a.board_id
--     left join ( -- A
--       select board_id, deal_type, vehicle_type, vehicle_model, vehicle_make, vehicle_model_year, vehicle_color,fi_gross, front_end_gross,days_in_inventory,
--       customer_name, customer_city, trade_mileage, sale_code
--       from board.daily_board
--       group by board_id, deal_type, vehicle_type, vehicle_model, vehicle_make, vehicle_model_year, vehicle_color,days_in_inventory,
--       customer_name, customer_city, trade_mileage, sale_code,fi_gross, front_end_gross
--     ) a on a.board_id = b.board_id
    inner join board.board_types d on b.board_type_key = d.board_type_key -- and d.board_type_key <> 4
    left join sls.sale_groups e on a.sale_code = e.code  and case when b.store_key = 41 then 39 else b.store_key end = (select store_key from onedc.stores where arkona_store_key  = e.store_code) 
    left join board.deal_gross f on a.board_id = f.board_id and f.current_row = true and f.amount_type = 'fi_gross'
    left join board.deal_gross g on a.board_id = g.board_id and g.current_row = true and g.amount_type = 'front_end_gross'
    left join board.deal_gross h on a.board_id = h.board_id and h.current_row = true and h.amount_type = 'trade_allowance'  -- this is the one that kills it
    left join board.deal_gross i on a.board_id = i.board_id and i.current_row = true and i.amount_type = 'acv' -- and this one

    where is_deleted = false 
     and boarded_date between date_trunc('MONTH',coalesce(_date::date,current_date))::DATE and coalesce(_date::date,current_date);
end
$$;
select * from wtf;     

-- create index on board.daily_board(board_id);
-- create index on board.daily_board(sale_code);
-- create index on board.daily_board(vehicle_type);

-- select * from wtf where board_id in (
-- select board_id from wtf group by board_id having count(*) > 1)
-- 
-- select * from board.daily_board where board_id in (26118,26636,26766)

