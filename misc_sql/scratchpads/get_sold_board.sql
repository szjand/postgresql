﻿-- select board.json_get_sold_board(current_date - 1)


-- new_vehicles
      select a.board_id 
      from board.sales_board b  
      inner join board.board_types d on b.board_type_key = d.board_type_key  and board_sub_type in ('Retail','Fleet')
      left join board.daily_board a on a.board_id = b.board_id
--       where gg.store_key = b.store_key 
--         and b.is_deleted = false 
      where b.is_deleted = false
        -- and boarded_date = coalesce(_date,current_date)
        and boarded_date = coalesce(null,current_date - 1)
        and a.vehicle_type = 'N'
      -- this makes no sense, only one field being returned ??  
      order by case when d.board_sub_type = 'Retail' then 1 when d.board_sub_type = 'Fleet' then 2 end;

-- used_vehicles      
      select b.board_id 
      from board.sales_board b  
      inner join board.board_types d on b.board_type_key = d.board_type_key  and board_sub_type in ('Retail')
      left join board.daily_board a on a.board_id = b.board_id
--       where gg.store_key = b.store_key 
      where b.is_deleted = false 
        and boarded_date = coalesce(null,current_date - 1)
        and a.vehicle_type = 'U';
-- other_outgoing_vehicles
      select a.board_id
      from board.sales_board b  
      inner join board.board_types d on b.board_type_key = d.board_type_key and board_sub_type not in ('Retail','Fleet','Trade')
      and board_type not in ('Addition','Back-on')
      left join board.daily_board a on a.board_id = b.board_id
      where  b.is_deleted = false 
        and boarded_date = coalesce(null,current_date - 1);
-- back_on_vehicles
      select distinct b.board_id 
      from board.sales_board b  
      inner join board.board_types d on b.board_type_key = d.board_type_key and board_type  in ('Back-on')
      left join board.daily_board a on a.board_id = b.board_id
      where b.is_deleted = false 
        and boarded_date = coalesce(null,current_date - 1);
-- new_back_on_vehicles
      select a.board_id 
      from board.sales_board b  
      inner join board.board_types d on b.board_type_key = d.board_type_key and board_type  in ('Back-on')
      left join board.daily_board a on a.board_id = b.board_id
      where b.is_deleted = false 
        and boarded_date = coalesce(null,current_date - 1)
        and a.vehicle_type = 'N';
-- used_back_on_vehicles
      select a.board_id 
      from board.sales_board b  
      inner join board.board_types d on b.board_type_key = d.board_type_key and board_type  in ('Back-on')
      left join board.daily_board a on a.board_id = b.board_id
      where b.is_deleted = false 
        and boarded_date = coalesce(null,current_date - 1)
        and a.vehicle_type = 'U';
-- addition_vehicles
      select a.board_id 
      from board.sales_board b  
      inner join board.board_types d on b.board_type_key = d.board_type_key and board_type  in ('Addition') and board_sub_type <> 'Trade'
      left join board.daily_board a on a.board_id = b.board_id
      where b.is_deleted = false 
        and boarded_date = coalesce(null,current_date - 1);
-- gross_wholesale_vehicles
      select a.board_id 
      from board.sales_board b  
      inner join board.board_types d on b.board_type_key = d.board_type_key and board_type  in ('Deal') and board_sub_type =  'Wholesale'
      left join board.daily_board a on a.board_id = b.board_id
      inner join board.deal_gross c on b.board_id = c.board_id --and from_date::date = coalesce(_date,current_date)
      where b.is_deleted = false 
        and deal_number <> 'N/A'
        and boarded_date = coalesce(null,current_date - 1);

do
$$
declare
  _date date := current_date;
begin
drop table if exists wtf;
create temp table wtf as  
select store_key as id, store_key as sales_store,(select board.board_stats(_date::date,gg.store_key) ) as board_counts
from onedc.stores gg;
end
$$;
select * from wtf; 
        
select board.board_stats(current_date - 1,39)



select pid, client_port, backend_Start, query from pg_stat_activity where state = 'active'

select * from pg_stat_statements where calls > 100 and mean_time > 1000 order by calls desc 

select pg_cancel_backend(4099)

select pg_terminate_backend(42582)

