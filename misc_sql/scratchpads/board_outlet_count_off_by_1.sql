﻿select a.unit_count, a.stock_number, a.vin, a.bopmast_id, a.psc_last_name,
	b.capped_date, b.delivery_date
from sls.deals_by_month a
left join sls.deals b on a.stock_number = b.stock_number
  and b.delivery_date between '08/01/2021' and current_date
where a.psc_last_name in ('dubois','olderbak')
  and a.year_month = 202108
order by a.stock_number  

select * from sls.deals limit 10


select a.boarded_ts, a.boarded_date, a.stock_number, a.vin, a.is_backed_on, b.board_type, b.board_sub_type
-- select * 
from board.sales_board a
left join board.board_types b on a.board_type_key = b.board_type_key
where not a.is_deleted
  and a.boarded_date between '08/01/2021' and current_date
limit 10

  select * from board.board_types 
-- board_type 11, back on, is not reflected in the attribute is_backed_on
select * from board.sales_board where board_type_key = 11  
select * from board.sales_board where is_backed_on

select is_backed_on, count(*) from board.sales_board group by is_backed_on

select stock_number, boarded_date, is_backed_on, board_type_key, b->>'SalesPersonName'
from board.sales_board a
left join json_array_elements(a.deal_details->'SalesPersons'->'SalesPerson') b on true
  and (b->>'SalesPersonName' like '%OLDER%' or b->>'SalesPersonName' like '%DUBOIS%')
  and b->>'SalesPersonType' = 'S'
where not a.is_deleted
	and exists (
		select 1
		from board.sales_board
		where is_backed_on-- or board_type_key = 11
			and stock_number = a.stock_number)
  and a.boarded_date between '08/01/2021' and current_date
  and not is_backed_on
order by a.stock_number

/*
So the outlet store count is off on the vision page. It should be 17. On 8/14 we had 17. Then on 8/16 we sold one and backed one on but 
it doesn’t seem to have counted the sold one cause the count went down to 16 when it should’ve stayed the same.
 
Michelle Cochran
*/

select 
	count(case when  a.sale_code in ('UC','UCC', 'UCO','UT','UTC','OUC', 'UTO','OUT','HUC','NUC')and d.board_sub_type = 'Retail' and b.store_key = 41 then a.board_id end) as out_used_count,
	count(case when a.sale_code in ('UC','UCC', 'UCO','UT','UTC', 'OUC','UTO','OUT','HUC','NUC','NUT') and d.board_type = 'Back-on' and b.store_key = 41 then a.board_id end) as out_used_back_count
    from board.sales_board  b
    join (
      select a.board_id, a.sale_code, a.vehicle_type
      from board.daily_board a
      join board.sales_board b on a.board_id = b.board_id
        and b.boarded_date between '08/01/2021' and current_date
      group by a.board_id, a.sale_code, a.vehicle_type) a on b.board_id = a.board_id
    inner join board.board_types d on b.board_type_key = d.board_type_key -- and d.board_type_key <> 4
    left join sls.sale_groups e on a.sale_code = e.code  and case when b.store_key = 41 then 39 else b.store_key end = (select store_key from onedc.stores where arkona_store_key  = e.store_code) 
    where is_deleted = false 
     and boarded_date between '08/01/2021' and current_date

select b.stock_number, b.vin, b.boarded_ts, b.boarded_date, d.board_type, d.board_sub_type, a.sale_code, b.board_type_key
-- 	count(case when  a.sale_code in ('UC','UCC', 'UCO','UT','UTC','OUC', 'UTO','OUT','HUC','NUC')and d.board_sub_type = 'Retail' and b.store_key = 41 then a.board_id end) as out_used_count,
-- 	count(case when a.sale_code in ('UC','UCC', 'UCO','UT','UTC', 'OUC','UTO','OUT','HUC','NUC','NUT') and d.board_type = 'Back-on' and b.store_key = 41 then a.board_id end) as out_used_back_count
    from board.sales_board  b
    join (
      select a.board_id, a.sale_code, a.vehicle_type
      from board.daily_board a
      join board.sales_board b on a.board_id = b.board_id
        and b.boarded_date between '08/01/2021' and current_date
      group by a.board_id, a.sale_code, a.vehicle_type) a on b.board_id = a.board_id
    inner join board.board_types d on b.board_type_key = d.board_type_key -- and d.board_type_key <> 4
    left join sls.sale_groups e on a.sale_code = e.code  and case when b.store_key = 41 then 39 else b.store_key end = (select store_key from onedc.stores where arkona_store_key  = e.store_code) 
    where is_deleted = false 
     and boarded_date between '08/01/2021' and current_date
     and a.sale_code in ('UC','UCC', 'UCO','UT','UTC', 'OUC','UTO','OUT','HUC','NUC','NUT')
     and (d.board_sub_type = 'Retail' or d.board_type = 'Back-on')
     and b.store_key = 41
order by b.stock_number


select * from board.sales_board where stock_number = 'G43022G'

-- problem turned out to be a bad sale code in daily_board
update board.daily_board
set sale_code = 'UTO'
-- select * from board.daily_board 
where board_id = 50396


