﻿select * from (
select inpmast_Stock_number
from ( -- nc.vehicles
                    SELECT 'Rydell GM Auto Center', inpmast_vin, inpmast_stock_number, 
                      'NEW', year, make, model, model_code, replace(body_Style, '"', ''), odometer,
                      color as EXTCOLOR, 
                      trim as INTCOLOR,
                      e.num_field_value, --invoice
                      list_price, -- msrp
                      b.num_field_value, -- internet price
                      color_code
                    FROM arkona.ext_INPMAST a
                    left join arkona.ext_inpoptf b on a.inpmast_vin = b.vin_number
                    join arkona.ext_inpoptd c on b.company_number = c.company_number
                      and b.seq_number = c.seq_number
                      and c.field_descrip = 'Internet Price'
                    left join arkona.ext_inpoptf e on a.inpmast_vin = e.vin_number
                    join arkona.ext_inpoptd f on e.company_number = f.company_number
                      and e.seq_number = f.seq_number
                      and f.field_descrip = 'Flooring/Payoff'  
                    where a.status = 'I'
                      and a.type_n_u = 'N'
) x where not exists (select 1 from nc.vehicles where vin = inpmast_vin)  ) xx                    

full outer join (  -- homenet feed
                      select a.stock --, a.vin, b.ro_date, d.keyper_date
                      from ifd.homenet_new a
                      left join (
                        select d.vin, min(b.the_date) as ro_date
                        from ads.ext_fact_repair_order a
                        join dds.dim_date b on a.opendatekey = b.date_key
                        join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
                        join ifd.homenet_new e on d.vin = e.vin
                        group by d.vin) b on a.vin = b.vin
                      left join (
                        select c.stock_number, c.keyper_date
                        from (
                          select a.stock_number, transaction_date::date as keyper_date, 
                            row_number() over (partition by a.stock_number order by a.transaction_date desc)
                          from keys.ext_keyper a
                          join ifd.homenet_new b on a.stock_number = b.stock) c
                        where row_number = 1) d on a.stock = d.stock_number
                      where ro_date is null
                        and keyper_date is null)  yy on xx.inpmast_stock_number = yy.stock                  


select * from nc.vehicle_acquisitions where stock_number in ('G37450','G37446','G37449','H12486')                            