﻿create table jon.fin_report (
  manager citext not null,
  reserve integer not null default 0,
  total integer not null default 0,
  stock_number citext not null);
create unique index on jon.fin_report(stock_number);

delete from jon.fin_report where manager = 'corey eden'

insert into jon.fin_report values
('Tom Aubol',0,0,'31162A'),
('Tom Aubol',1023,2706,'31559'),
('Tom Aubol',200,2509,'30643B'),
('Tom Aubol',0,0,'31128PA'),
('Tom Aubol',0,0,'31722'),
('Tom Aubol',150,524,'31447'),
('Tom Aubol',231,231,'30396B'),
('Tom Aubol',0,800,'13245'),
('Tom Aubol',200,574,'29788'),
('Tom Aubol',3708,6593,'31313'),
('Tom Aubol',300,2609,'31241A'),
('Tom Aubol',0,0,'31352P'),
('Tom Aubol',200,200,'31047A'),
('Tom Aubol',474,1274,'31560A'),
('Tom Aubol',705,2832,'31157'),
('Tom Aubol',577,2935,'30109RB'),
('Tom Aubol',200,200,'31355'),
('Tom Aubol',1341,3868,'31511'),
('Tom Aubol',920,1294,'31770'),
('Tom Aubol',2160,4985,'31444'),
('Tom Aubol',0,0,'30802B'),
('Tom Aubol',200,1853,'31272B'),
('Tom Aubol',2426,2926,'31685'),
('Tom Aubol',0,2058,'30377R'),
('Tom Aubol',707,1190,'31790l'),
('Tom Aubol',100,1100,'31655XXA'),
('Tom Aubol',0,0,'31247A'),
('Tom Aubol',0,0,'31615'),
('Tom Aubol',2086,4896,'31582XX'),
('Tom Aubol',1069,1069,'31875'),
('Tom Aubol',941,3799,'28382XXZ'),
('Tom Aubol',743,743,'31307'),



-- check against statement
-- looks ok
select c.store_code, line, col, sum(amount)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join (
  select gl_account, line, col
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where year_month = 201708
    and store = 'ry1'
    and page = 17
    and line between 1 and 20) e on c.account = e.gl_Account
where a.post_Status = 'Y'
  and year_month = 201708    
group by c.store_code, line, col  
order by store_code, line, col    

select *
from jon.fin_report m
left join (
  select a.control, 
    sum(case when line in (1, 11) then -amount else 0 end) as fin_inc,
    sum(-amount) as fin_total
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join fin.dim_journal d on a.journal_key = d.journal_key
  inner join (
    select gl_account, line, col
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
    inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
    inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
    where year_month = 201708
      and store = 'ry1'
      and page = 17
      and line between 1 and 20) e on c.account = e.gl_account
  where a.post_status = 'Y'
    and year_month = 201708
  group by a.control) n on m.stock_number = n.control

-- totals by manager
select manager, res,fs_inc,tot,fs_total
from (
  select *,
    sum(reserve) over (partition by manager) as res,
    sum(fin_inc) over (partition by manager)::integer as fs_inc,
    sum(total) over (partition by manager) as tot,
    sum(fin_total) over (partition by manager)::integer as fs_total
  from jon.fin_report m
  left join (
    select a.control, 
      sum(case when line in (1, 11) then -amount else 0 end) as fin_inc,
      sum(-amount) as fin_total
    from fin.fact_gl a
    inner join dds.dim_date b on a.date_key = b.date_key
    inner join fin.dim_account c on a.account_key = c.account_key
    inner join fin.dim_journal d on a.journal_key = d.journal_key
    inner join (
      select gl_account, line, col
      from fin.fact_fs a
      inner join fin.dim_fs b on a.fs_key = b.fs_key
      inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
      inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
      where year_month = 201708
        and store = 'ry1'
        and page = 17
        and line between 1 and 20) e on c.account = e.gl_account
    where a.post_status = 'Y'
      and year_month = 201708
    group by a.control) n on m.stock_number = n.control) o
group by manager, res,fs_inc,tot,fs_total


-- 9/14/
-- from ben: Ok so the sheet has 301 stock #'s on it, I believe we sold 332.  
-- Can you tell me the 31 stock numbers that are not on that sheet but we did deliver last month?
-- unit counts -----------------------------------------------------------------------------------------------------------------
drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select store, page, line, line_label, control, sum(unit_count) as unit_count
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201708
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201708
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
--         or
--         (b.page = 17 and b.line between 1 and 20))-- f/i
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by store, page, line, line_label, control
order by store, page, line;


-- ok, count is still good
select store, page, line, line_label, sum(unit_count)
from (
  select *
  from step_1) a
group by store, page, line, line_label
order by store, page, line, line_label

-- by store, page
select store, page, sum(unit_count)
from (
  select *
  from step_1) a
group by store, page
order by store, page 

select page, line, line_label, cardinality(array_agg(control)), sum(unit_count), string_agg(control, ',')
from step_1
where store = 'ry1'
  and unit_count <> 0
group by page, line, line_label
order by page, line, line_label

-- from Desktop/sql/ucinv_from_fs
-- the difference between actual deliveries and fs count

select control, sum(unit_count)
from greg.uc_base_Vehicle_accounting
where year_month = 201708
  and store_code = 'ry1'
  and page = 16
  and account_type = 'sale'
  and line = 2
group by control
having sum(unit_count) <> 0

lists 40 rows including 30758A with unit_count = -1 (sold in july, unw in august)
fs = (39 * 1) + (1 * -1) = 38
39 actual deliveries

-- actual deliveries
select sum(unit_count)
from step_1
where page = 16 and line = 2
  and store = 'ry1'
  and unit_count > 0 
-- fs unit count
select sum(unit_count)
from step_1
where page = 16 
  and line = 2
  and store = 'ry1'


-- actual deliveries
select sum(unit_count)
from step_1
where page = 16 and line = 4
  and store = 'ry1'
  and unit_count > 0 
-- fs unit count
select sum(unit_count)
from step_1
where page = 16 
  and line = 4
  and store = 'ry1'


-- actual deliveries
select sum(unit_count)
from step_1
where page = 16 and line = 5
  and store = 'ry1'
  and unit_count > 0 
-- fs unit count
select sum(unit_count)
from step_1
where page = 16 
  and line = 5
  and store = 'ry1'  


-- P7 by line, right on
select line, col, sum(-amount)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join (
  select gl_account, line, col
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where year_month = 201708
    and store = 'ry1'
    and page = 17
    and line between 1 and 20) e on c.account = e.gl_account
where a.post_status = 'Y'
  and year_month = 201708
group by line, col


select m.*, n.line_label, o.manager, o.reserve, o.total,
  sum(amount) over(partition by m.line, m.col),
  sum(amount) over()
from (
  select line, line_label, col, control, amount
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join fin.dim_journal d on a.journal_key = d.journal_key
  inner join (
    select gl_account, line, col, line_label
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
    inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
    inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
    where year_month = 201708
      and store = 'ry1'
      and page = 17
      and line between 1 and 20) e on c.account = e.gl_account
  where a.post_status = 'Y'
    and year_month = 201708) m
left join step_1 n on m.control = n.control
left join jon.fin_report o on m.control = o.stock_number 
order by m.line, m.col, control


-- all page 7 transactions w/august deliveries & finance spreadsheet info

select m.line, m.line_label, m.col, m.control, sum(amount) as amount, 
  n.line_label as august_vehicle, o.manager, o.reserve, o.total
from (
  select line, line_label, col, control, amount
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join fin.dim_journal d on a.journal_key = d.journal_key
  inner join (
    select gl_account, line, col, line_label
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
    inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
    inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
    where year_month = 201708
      and store = 'ry1'
      and page = 17
      and line between 1 and 20) e on c.account = e.gl_account
  where a.post_status = 'Y'
    and year_month = 201708) m
left join step_1 n on m.control = n.control
left join jon.fin_report o on m.control = o.stock_number 
group by m.line, m.line_label, m.col, m.control, n.line_label, o.manager, o.reserve, o.total
order by m.line, m.col, m.control


-- what ben wants is a list of un-logged retail deals from august

select *
from step_1
where store = 'ry1'
  and case when page = 16 then line not in (8,10) end
  and unit_count <> 0

select * from step_1 where unit_count = 0

select a.*
from step_1 a
where control in (
  select control
  from step_1
  where store = 'ry1'
    and case when page = 16 then line not in (8,10) else 1 = 1 end
    and unit_count <> 0
  group by control
  having sum(unit_count) = 1)  
and (line_label = 'internal' or page = 14)


-- i believe this is the actual retail deals delivered 
select a.*,
  sum(unit_count) over (partition by page, line) as line_total,
  sum(unit_count) over (partition by page) as page_total,
  sum(unit_count) over () as total
from step_1 a
where control in (
  select control
  from step_1
  where store = 'ry1'
    and case when page = 16 then line not in (8,10) else 1 = 1 end
    and unit_count <> 0
    and line_label <> 'internal'
    and page <> 14
  group by control
  having sum(unit_count) = 1)  


-- the unlogged deals
select control
from (
select a.*,
  sum(unit_count) over (partition by page, line) as line_total,
  sum(unit_count) over (partition by page) as page_total,
  sum(unit_count) over () as total
from step_1 a
where control in (
  select control
  from step_1
  where store = 'ry1'
    and case when page = 16 then line not in (8,10) else 1 = 1 end
    and unit_count <> 0
    and line_label <> 'internal'
    and page <> 14
  group by control
  having sum(unit_count) = 1)) x
left join jon.fin_report y on x.control = y.stock_number  
where y.manager is null

drop table if exists ben; 
create temp table ben as
select *
from (
  select control
  from (
  select a.*,
    sum(unit_count) over (partition by page, line) as line_total,
    sum(unit_count) over (partition by page) as page_total,
    sum(unit_count) over () as total
  from step_1 a
  where control in (
    select control
    from step_1
    where store = 'ry1'
      and case when page = 16 then line not in (8,10) else 1 = 1 end
      and unit_count <> 0
      and line_label <> 'internal'
      and page <> 14
    group by control
    having sum(unit_count) = 1)) x
  left join jon.fin_report y on x.control = y.stock_number  
  where y.manager is null) m
left join sls.deals n on m.control = n.stock_number
  and n.year_month = 201708
  and n.bopmast_id not in (43450, 43543)
  and n.unit_count = 1
  and n.seq = (
    select max(e.seq)
    from sls.deals e
    where e.stock_number = n.stock_number
      and year_month = 201708
    group by e.stock_number)
order by control  






