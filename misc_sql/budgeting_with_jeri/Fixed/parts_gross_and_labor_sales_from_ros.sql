﻿-- from E:\DDS2\Scripts\reports\Jeri\parts_service_oct_15_sep_16.sql
-- what i sent her last year, parts_+sales_gross_ros.xlsx is just parts as generated below
-- the spread sheet from jeri includes the parts info with elr & other info, don't know where or how she got that
-- any way, i shall proceed with parts, then go on from there

-- -- -- ï»¿Iâ€™m looking for a report that lists parts sales and gross on repair orders for the following service types by month from Oct 2015 through Sept 2016:
-- -- -- 
-- -- -- MN Customer Pay
-- -- -- EM  Customer Pay
-- -- -- SV Customer Pay
-- -- -- MR Customer Pay
-- -- -- 
-- -- -- MN Internal
-- -- -- MR Internal
-- -- -- 
-- -- -- MR Warranty
-- -- -- MR Service Contract
-- -- -- 
-- -- -- 
-- -- -- create table ads.repair_order(
-- -- --   store_code citext,
-- -- --   the_date date,
-- -- --   ro citext,
-- -- --   line integer,
-- -- --   service_type citext, 
-- -- --   payment_type citext);
-- -- -- create index on ads.repair_order(ro);  
-- -- -- create index on ads.repair_order(line);  
-- -- -- create index on ads.repair_order(service_type);  
-- -- -- create index on ads.repair_order(payment_type); 
-- -- -- 
-- -- -- delete from ads.repair_order
-- -- -- 
-- -- -- select count(*) 
-- -- -- from ads.ext_fact_repair_order
-- -- -- 
-- -- -- 
-- -- -- select b.year_month, a.ro, a.line, c.servicetypecode, d.paymenttype
-- -- -- from ads.ext_fact_repair_order a
-- -- -- inner join dds.dim_date b on a.finalclosedatekey = b.date_key
-- -- --   and b.year_month between 201610 and 201709
-- -- -- inner join ads.ext_dim_service_type c on a.servicetypekey = c.servicetypekey
-- -- --   and c.servicetypecode in ('MN','EM','SV','MR','QS')
-- -- -- inner join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey
-- -- -- limit 100
-- -- -- 
-- -- -- 
-- -- -- 
-- -- -- select *
-- -- -- from dds.ext_pdptdet
-- -- -- where ptdate > 20161000
-- -- -- limit 500  
-- -- -- create index on dds.ext_pdptdet (ptinv_);
-- -- -- create index on dds.ext_pdptdet(ptline);
-- -- -- 
-- -- -- select a.*, b.ptqty, b.ptqty * b.ptcost as cogs, b.ptqty * b.ptnet as sales, b.ptqty * ptnet - b.ptqty * ptcost as gross
-- -- -- from ads.repair_order a
-- -- -- left join dds.ext_pdptdet b on a.ro = b.ptinv_
-- -- --   and a.line = b.ptline
-- -- -- where a.ro = '16194613'  
-- -- --   and b.ptinv_ is not null 
-- -- -- order by line  
-- -- -- 
-- -- -- select * from ads.repair_order where ro = '16194613' order by line
-- -- --   
-- -- -- select line, sum(ptcost), sum(ptlist), sum(ptnet)
-- -- -- from ads.repair_order a
-- -- -- left join dds.ext_pdptdet b on a.ro = b.ptinv_
-- -- --   and a.line = b.ptline
-- -- -- where a.ro = '16194613'  
-- -- --   and b.ptinv_ is not null 
-- -- -- group by line  
-- -- -- limit 100  
-- -- -- 
-- -- -- 
-- -- -- select a.*, b.account, b.account_type, b.department, b.description
-- -- -- from fin.fact_gl a
-- -- -- inner join fin.dim_Account b on a.account_key = b.account_key
-- -- --   and b.department_code = 'PD'
-- -- -- where a.control = '16194613'
-- -- -- 
-- -- -- 
-- -- -- 
-- -- -- 
-- -- -- select c.yearmonth, a.service_type, a.payment_type, 
-- -- --   round(sum(b.ptqty * b.ptnet), 0) as parts_sales,
-- -- --   round(sum(b.ptqty * b.ptcost), 0) as parts_cogs,  
-- -- --   round(sum(b.ptqty * ptnet - b.ptqty * ptcost), 0) as parts_gross,
-- -- --   count(distinct a.ro) as ro_count
-- -- -- from ads.repair_order a
-- -- -- left join dds.ext_pdptdet b on a.ro = b.ptinv_
-- -- --   and a.line = b.ptline
-- -- -- left join dds.day c on a.the_date = c.thedate  
-- -- -- where b.ptinv_ is not null 
-- -- --   and (
-- -- --     (service_type = 'MN' and payment_type = 'customer pay')
-- -- --     or
-- -- --     (service_type = 'EM' and payment_type = 'customer pay')
-- -- --     or
-- -- --     (service_type = 'SV' and payment_type = 'customer pay')
-- -- --     or
-- -- --     (service_type = 'MR' and payment_type = 'customer pay')
-- -- --     or
-- -- --     (service_type = 'MN' and payment_type = 'internal')
-- -- --     or
-- -- --     (service_type = 'MR' and payment_type = 'internal')
-- -- --     or
-- -- --     (service_type = 'MR' and payment_type = 'warranty')
-- -- --     or
-- -- --     (service_type = 'MR' and payment_type = 'service contract'))                                
-- -- -- group by c.yearmonth, a.service_type, a.payment_type
-- -- -- -- order by a.payment_type, service_type
-- -- -- ) x group by yearmonth
-- --


-- 10/13/2017


-- parts
-- good enuf

drop table if exists parts_ros;
create temp table parts_ros as
select b.year_month, a.ro, a.line, c.servicetypecode, d.paymenttype
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.finalclosedatekey = b.date_key
  and b.year_month between 201610 and 201709
inner join ads.ext_dim_service_type c on a.servicetypekey = c.servicetypekey
  and c.servicetypecode in ('MN','EM','SV','MR','QS')
inner join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey
group by b.year_month, a.ro, a.line, c.servicetypecode, d.paymenttype;
create unique index on parts_ros(ro,line);
create index on parts_ros(ro);
create index on parts_ros(line);

drop table if exists parts_gross;
create temp table parts_gross as
select a.year_month, a.servicetypecode, a.paymenttype, --e.ptqty, e.ptnet, e.ptcost
  round(sum(e.ptqty * e.ptnet), 0) as parts_sales,
--   round(sum(e.ptqty * e.ptcost), 0) as parts_cogs,  
  round(sum(e.ptqty * e.ptnet - e.ptqty * ptcost), 0) as parts_gross
--   count(distinct a.ro) as ro_count
from parts_ros a
inner join dds.ext_pdptdet e on a.ro = e.ptinv_
  and a.line = e.ptline
where (
  (servicetypecode = 'MN' and paymenttype = 'customer pay')
  or
  (servicetypecode = 'QS' and paymenttype = 'customer pay')  
  or
  (servicetypecode = 'EM' and paymenttype = 'customer pay')
  or
  (servicetypecode = 'SV' and paymenttype = 'customer pay')
  or
  (servicetypecode = 'MR' and paymenttype = 'customer pay')
  or
  (servicetypecode = 'MN' and paymenttype = 'internal')
  or
  (servicetypecode = 'MR' and paymenttype = 'internal')
  or
  (servicetypecode = 'MR' and paymenttype = 'warranty')
  or
  (servicetypecode = 'MR' and paymenttype = 'service contract'))                                
group by a.year_month,a.servicetypecode, a.paymenttype  
order by a.paymenttype, a.servicetypecode;
create unique index on parts_gross(year_month, servicetypecode, paymenttype); 


select servicetypecode, paymenttype, 'parts sales' as cat,
  sum(case when year_month = 201610 then parts_sales else 0 end) as Oct_16,
  sum(case when year_month = 201611 then parts_sales else 0 end) as Nov_16,
  sum(case when year_month = 201612 then parts_sales else 0 end) as Dec_16,
  sum(case when year_month = 201701 then parts_sales else 0 end) as Jan_17,
  sum(case when year_month = 201702 then parts_sales else 0 end) as Feb_17,
  sum(case when year_month = 201703 then parts_sales else 0 end) as Mar_17,
  sum(case when year_month = 201704 then parts_sales else 0 end) as Apr_17,
  sum(case when year_month = 201705 then parts_sales else 0 end) as May_17,  
  sum(case when year_month = 201706 then parts_sales else 0 end) as Jun_17,
  sum(case when year_month = 201707 then parts_sales else 0 end) as Jul_17,
  sum(case when year_month = 201708 then parts_sales else 0 end) as Aug_17,
  sum(case when year_month = 201709 then parts_sales else 0 end) as Sep_17
from parts_gross
group by servicetypecode, paymenttype
union
select servicetypecode, paymenttype, 'parts gross' as cat,
  sum(case when year_month = 201610 then parts_gross else 0 end) as Oct_16,
  sum(case when year_month = 201611 then parts_gross else 0 end) as Nov_16,
  sum(case when year_month = 201612 then parts_gross else 0 end) as Dec_16,
  sum(case when year_month = 201701 then parts_gross else 0 end) as Jan_17,
  sum(case when year_month = 201702 then parts_gross else 0 end) as Feb_17,
  sum(case when year_month = 201703 then parts_gross else 0 end) as Mar_17,
  sum(case when year_month = 201704 then parts_gross else 0 end) as Apr_17,
  sum(case when year_month = 201705 then parts_gross else 0 end) as May_17,  
  sum(case when year_month = 201706 then parts_gross else 0 end) as Jun_17,
  sum(case when year_month = 201707 then parts_gross else 0 end) as Jul_17,
  sum(case when year_month = 201708 then parts_gross else 0 end) as Aug_17,
  sum(case when year_month = 201709 then parts_gross else 0 end) as Sep_17
from parts_gross
group by servicetypecode, paymenttype
order by servicetypecode, paymenttype, cat


-- labor
drop table if exists ro_labor;
create temp table ro_labor as
select b.year_month, c.servicetypecode, d.paymenttype, sum(a.flaghours)::integer as flaghours, 
  sum(a.laborsales)::integer as sales, count(distinct ro) as ros,
  case
    when sum(a.flaghours) = 0 then 0
    else round(sum(a.laborsales)/sum(a.flaghours), 2)
  end as elr, 
  case
    when count(distinct ro) = 0 then 0
    else round(sum(flaghours)/count(distinct ro), 2) 
  end as hrs_per_ro
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.finalclosedatekey = b.date_key
  and b.year_month between 201610 and 201709
inner join ads.ext_dim_service_type c on a.servicetypekey = c.servicetypekey
  and c.servicetypecode in ('MN','EM','SV','MR','QS')
inner join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey
where storecode = 'RY1'
  and (
  (servicetypecode = 'MN' and paymenttype = 'customer pay')
  or
  (servicetypecode = 'QS' and paymenttype = 'customer pay')  
  or
  (servicetypecode = 'EM' and paymenttype = 'customer pay')
  or
  (servicetypecode = 'SV' and paymenttype = 'customer pay')
  or
  (servicetypecode = 'MR' and paymenttype = 'customer pay')
  or
  (servicetypecode = 'MN' and paymenttype = 'internal')
  or
  (servicetypecode = 'MR' and paymenttype = 'internal')
  or
  (servicetypecode = 'MR' and paymenttype = 'warranty')
  or
  (servicetypecode = 'MR' and paymenttype = 'service contract'))  
group by b.year_month, c.servicetypecode, d.paymenttype;
create unique index on ro_labor(year_month, servicetypecode,paymenttype);


select * from ro_labor limit 100

select servicetypecode, paymenttype, 'parts sales' as cat, 
  sum(case when year_month = 201610 then parts_sales else 0 end) as Oct_16,
  sum(case when year_month = 201611 then parts_sales else 0 end) as Nov_16,
  sum(case when year_month = 201612 then parts_sales else 0 end) as Dec_16,
  sum(case when year_month = 201701 then parts_sales else 0 end) as Jan_17,
  sum(case when year_month = 201702 then parts_sales else 0 end) as Feb_17,
  sum(case when year_month = 201703 then parts_sales else 0 end) as Mar_17,
  sum(case when year_month = 201704 then parts_sales else 0 end) as Apr_17,
  sum(case when year_month = 201705 then parts_sales else 0 end) as May_17,  
  sum(case when year_month = 201706 then parts_sales else 0 end) as Jun_17,
  sum(case when year_month = 201707 then parts_sales else 0 end) as Jul_17,
  sum(case when year_month = 201708 then parts_sales else 0 end) as Aug_17,
  sum(case when year_month = 201709 then parts_sales else 0 end) as Sep_17,
  5 as sort
from parts_gross
group by servicetypecode, paymenttype
union
select servicetypecode, paymenttype, 'parts gross' as cat, 
  sum(case when year_month = 201610 then parts_gross else 0 end) as Oct_16,
  sum(case when year_month = 201611 then parts_gross else 0 end) as Nov_16,
  sum(case when year_month = 201612 then parts_gross else 0 end) as Dec_16,
  sum(case when year_month = 201701 then parts_gross else 0 end) as Jan_17,
  sum(case when year_month = 201702 then parts_gross else 0 end) as Feb_17,
  sum(case when year_month = 201703 then parts_gross else 0 end) as Mar_17,
  sum(case when year_month = 201704 then parts_gross else 0 end) as Apr_17,
  sum(case when year_month = 201705 then parts_gross else 0 end) as May_17,  
  sum(case when year_month = 201706 then parts_gross else 0 end) as Jun_17,
  sum(case when year_month = 201707 then parts_gross else 0 end) as Jul_17,
  sum(case when year_month = 201708 then parts_gross else 0 end) as Aug_17,
  sum(case when year_month = 201709 then parts_gross else 0 end) as Sep_17,
  6
from parts_gross
group by servicetypecode, paymenttype
union
select servicetypecode, paymenttype, 'ELR' as cat, 
  sum(case when year_month = 201610 then elr else 0 end) as Oct_16,
  sum(case when year_month = 201611 then elr else 0 end) as Nov_16,
  sum(case when year_month = 201612 then elr else 0 end) as Dec_16,
  sum(case when year_month = 201701 then elr else 0 end) as Jan_17,
  sum(case when year_month = 201702 then elr else 0 end) as Feb_17,
  sum(case when year_month = 201703 then elr else 0 end) as Mar_17,
  sum(case when year_month = 201704 then elr else 0 end) as Apr_17,
  sum(case when year_month = 201705 then elr else 0 end) as May_17,  
  sum(case when year_month = 201706 then elr else 0 end) as Jun_17,
  sum(case when year_month = 201707 then elr else 0 end) as Jul_17,
  sum(case when year_month = 201708 then elr else 0 end) as Aug_17,
  sum(case when year_month = 201709 then elr else 0 end) as Sep_17,
  1
from ro_labor
group by servicetypecode, paymenttype
union 
select servicetypecode, paymenttype, 'ROs' as cat, 
  sum(case when year_month = 201610 then ros else 0 end) as Oct_16,
  sum(case when year_month = 201611 then ros else 0 end) as Nov_16,
  sum(case when year_month = 201612 then ros else 0 end) as Dec_16,
  sum(case when year_month = 201701 then ros else 0 end) as Jan_17,
  sum(case when year_month = 201702 then ros else 0 end) as Feb_17,
  sum(case when year_month = 201703 then ros else 0 end) as Mar_17,
  sum(case when year_month = 201704 then ros else 0 end) as Apr_17,
  sum(case when year_month = 201705 then ros else 0 end) as May_17,  
  sum(case when year_month = 201706 then ros else 0 end) as Jun_17,
  sum(case when year_month = 201707 then ros else 0 end) as Jul_17,
  sum(case when year_month = 201708 then ros else 0 end) as Aug_17,
  sum(case when year_month = 201709 then ros else 0 end) as Sep_17,
  2
from ro_labor
group by servicetypecode, paymenttype
union 
select servicetypecode, paymenttype, 'Hours' as cat, 
  sum(case when year_month = 201610 then flaghours else 0 end) as Oct_16,
  sum(case when year_month = 201611 then flaghours else 0 end) as Nov_16,
  sum(case when year_month = 201612 then flaghours else 0 end) as Dec_16,
  sum(case when year_month = 201701 then flaghours else 0 end) as Jan_17,
  sum(case when year_month = 201702 then flaghours else 0 end) as Feb_17,
  sum(case when year_month = 201703 then flaghours else 0 end) as Mar_17,
  sum(case when year_month = 201704 then flaghours else 0 end) as Apr_17,
  sum(case when year_month = 201705 then flaghours else 0 end) as May_17,  
  sum(case when year_month = 201706 then flaghours else 0 end) as Jun_17,
  sum(case when year_month = 201707 then flaghours else 0 end) as Jul_17,
  sum(case when year_month = 201708 then flaghours else 0 end) as Aug_17,
  sum(case when year_month = 201709 then flaghours else 0 end) as Sep_17,
  3 
from ro_labor
group by servicetypecode, paymenttype
union 
select servicetypecode, paymenttype, 'Hours/RO' as cat, 
  sum(case when year_month = 201610 then hrs_per_ro else 0 end) as Oct_16,
  sum(case when year_month = 201611 then hrs_per_ro else 0 end) as Nov_16,
  sum(case when year_month = 201612 then hrs_per_ro else 0 end) as Dec_16,
  sum(case when year_month = 201701 then hrs_per_ro else 0 end) as Jan_17,
  sum(case when year_month = 201702 then hrs_per_ro else 0 end) as Feb_17,
  sum(case when year_month = 201703 then hrs_per_ro else 0 end) as Mar_17,
  sum(case when year_month = 201704 then hrs_per_ro else 0 end) as Apr_17,
  sum(case when year_month = 201705 then hrs_per_ro else 0 end) as May_17,  
  sum(case when year_month = 201706 then hrs_per_ro else 0 end) as Jun_17,
  sum(case when year_month = 201707 then hrs_per_ro else 0 end) as Jul_17,
  sum(case when year_month = 201708 then hrs_per_ro else 0 end) as Aug_17,
  sum(case when year_month = 201709 then hrs_per_ro else 0 end) as Sep_17,
  4
from ro_labor
group by servicetypecode, paymenttype
order by servicetypecode, paymenttype, sort


-------------------------------------------------------------------------------------------------
-- 10/17/17 same thing for body shop
-------------------------------------------------------------------------------------------------



drop table if exists parts_ros;
create temp table parts_ros as
select b.year_month, a.ro, a.line, c.servicetypecode, d.paymenttype
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.finalclosedatekey = b.date_key
  and b.year_month between 201610 and 201709
inner join ads.ext_dim_service_type c on a.servicetypekey = c.servicetypekey
  and c.servicetypecode = 'BS'
inner join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey
group by b.year_month, a.ro, a.line, c.servicetypecode, d.paymenttype;
create unique index on parts_ros(ro,line);
create index on parts_ros(ro);
create index on parts_ros(line);

drop table if exists parts_gross;
create temp table parts_gross as
select a.year_month, a.servicetypecode, a.paymenttype, --e.ptqty, e.ptnet, e.ptcost
  round(sum(e.ptqty * e.ptnet), 0) as parts_sales,
--   round(sum(e.ptqty * e.ptcost), 0) as parts_cogs,  
  round(sum(e.ptqty * e.ptnet - e.ptqty * ptcost), 0) as parts_gross
--   count(distinct a.ro) as ro_count
from parts_ros a
inner join dds.ext_pdptdet e on a.ro = e.ptinv_
  and a.line = e.ptline
where (
  (servicetypecode = 'BS' and paymenttype = 'customer pay')
  or
  (servicetypecode = 'BS' and paymenttype = 'internal')
  or
  (servicetypecode = 'BS' and paymenttype = 'warranty'))                                
group by a.year_month,a.servicetypecode, a.paymenttype  
order by a.paymenttype, a.servicetypecode;
create unique index on parts_gross(year_month, servicetypecode, paymenttype); 


-- labor
drop table if exists ro_labor;
create temp table ro_labor as
select b.year_month, c.servicetypecode, d.paymenttype, sum(a.flaghours)::integer as flaghours, 
  sum(a.laborsales)::integer as sales, count(distinct ro) as ros,
  case
    when sum(a.flaghours) = 0 then 0
    else round(sum(a.laborsales)/sum(a.flaghours), 2)
  end as elr, 
  case
    when count(distinct ro) = 0 then 0
    else round(sum(flaghours)/count(distinct ro), 2) 
  end as hrs_per_ro
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.finalclosedatekey = b.date_key
  and b.year_month between 201610 and 201709
inner join ads.ext_dim_service_type c on a.servicetypekey = c.servicetypekey
  and c.servicetypecode = 'BS'
inner join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey
where storecode = 'RY1'
  and (
  (servicetypecode = 'BS' and paymenttype = 'customer pay')
  or
  (servicetypecode = 'BS' and paymenttype = 'internal')
  or
  (servicetypecode = 'BS' and paymenttype = 'warranty'))  
group by b.year_month, c.servicetypecode, d.paymenttype;
create unique index on ro_labor(year_month, servicetypecode,paymenttype);





select servicetypecode, paymenttype, 'parts sales' as cat, 
  sum(case when year_month = 201610 then parts_sales else 0 end) as Oct_16,
  sum(case when year_month = 201611 then parts_sales else 0 end) as Nov_16,
  sum(case when year_month = 201612 then parts_sales else 0 end) as Dec_16,
  sum(case when year_month = 201701 then parts_sales else 0 end) as Jan_17,
  sum(case when year_month = 201702 then parts_sales else 0 end) as Feb_17,
  sum(case when year_month = 201703 then parts_sales else 0 end) as Mar_17,
  sum(case when year_month = 201704 then parts_sales else 0 end) as Apr_17,
  sum(case when year_month = 201705 then parts_sales else 0 end) as May_17,  
  sum(case when year_month = 201706 then parts_sales else 0 end) as Jun_17,
  sum(case when year_month = 201707 then parts_sales else 0 end) as Jul_17,
  sum(case when year_month = 201708 then parts_sales else 0 end) as Aug_17,
  sum(case when year_month = 201709 then parts_sales else 0 end) as Sep_17,
  5 as sort
from parts_gross
group by servicetypecode, paymenttype
union
select servicetypecode, paymenttype, 'parts gross' as cat, 
  sum(case when year_month = 201610 then parts_gross else 0 end) as Oct_16,
  sum(case when year_month = 201611 then parts_gross else 0 end) as Nov_16,
  sum(case when year_month = 201612 then parts_gross else 0 end) as Dec_16,
  sum(case when year_month = 201701 then parts_gross else 0 end) as Jan_17,
  sum(case when year_month = 201702 then parts_gross else 0 end) as Feb_17,
  sum(case when year_month = 201703 then parts_gross else 0 end) as Mar_17,
  sum(case when year_month = 201704 then parts_gross else 0 end) as Apr_17,
  sum(case when year_month = 201705 then parts_gross else 0 end) as May_17,  
  sum(case when year_month = 201706 then parts_gross else 0 end) as Jun_17,
  sum(case when year_month = 201707 then parts_gross else 0 end) as Jul_17,
  sum(case when year_month = 201708 then parts_gross else 0 end) as Aug_17,
  sum(case when year_month = 201709 then parts_gross else 0 end) as Sep_17,
  6
from parts_gross
group by servicetypecode, paymenttype
union
select servicetypecode, paymenttype, 'ELR' as cat, 
  sum(case when year_month = 201610 then elr else 0 end) as Oct_16,
  sum(case when year_month = 201611 then elr else 0 end) as Nov_16,
  sum(case when year_month = 201612 then elr else 0 end) as Dec_16,
  sum(case when year_month = 201701 then elr else 0 end) as Jan_17,
  sum(case when year_month = 201702 then elr else 0 end) as Feb_17,
  sum(case when year_month = 201703 then elr else 0 end) as Mar_17,
  sum(case when year_month = 201704 then elr else 0 end) as Apr_17,
  sum(case when year_month = 201705 then elr else 0 end) as May_17,  
  sum(case when year_month = 201706 then elr else 0 end) as Jun_17,
  sum(case when year_month = 201707 then elr else 0 end) as Jul_17,
  sum(case when year_month = 201708 then elr else 0 end) as Aug_17,
  sum(case when year_month = 201709 then elr else 0 end) as Sep_17,
  1
from ro_labor
group by servicetypecode, paymenttype
union 
select servicetypecode, paymenttype, 'ROs' as cat, 
  sum(case when year_month = 201610 then ros else 0 end) as Oct_16,
  sum(case when year_month = 201611 then ros else 0 end) as Nov_16,
  sum(case when year_month = 201612 then ros else 0 end) as Dec_16,
  sum(case when year_month = 201701 then ros else 0 end) as Jan_17,
  sum(case when year_month = 201702 then ros else 0 end) as Feb_17,
  sum(case when year_month = 201703 then ros else 0 end) as Mar_17,
  sum(case when year_month = 201704 then ros else 0 end) as Apr_17,
  sum(case when year_month = 201705 then ros else 0 end) as May_17,  
  sum(case when year_month = 201706 then ros else 0 end) as Jun_17,
  sum(case when year_month = 201707 then ros else 0 end) as Jul_17,
  sum(case when year_month = 201708 then ros else 0 end) as Aug_17,
  sum(case when year_month = 201709 then ros else 0 end) as Sep_17,
  2
from ro_labor
group by servicetypecode, paymenttype
union 
select servicetypecode, paymenttype, 'Hours' as cat, 
  sum(case when year_month = 201610 then flaghours else 0 end) as Oct_16,
  sum(case when year_month = 201611 then flaghours else 0 end) as Nov_16,
  sum(case when year_month = 201612 then flaghours else 0 end) as Dec_16,
  sum(case when year_month = 201701 then flaghours else 0 end) as Jan_17,
  sum(case when year_month = 201702 then flaghours else 0 end) as Feb_17,
  sum(case when year_month = 201703 then flaghours else 0 end) as Mar_17,
  sum(case when year_month = 201704 then flaghours else 0 end) as Apr_17,
  sum(case when year_month = 201705 then flaghours else 0 end) as May_17,  
  sum(case when year_month = 201706 then flaghours else 0 end) as Jun_17,
  sum(case when year_month = 201707 then flaghours else 0 end) as Jul_17,
  sum(case when year_month = 201708 then flaghours else 0 end) as Aug_17,
  sum(case when year_month = 201709 then flaghours else 0 end) as Sep_17,
  3 
from ro_labor
group by servicetypecode, paymenttype
union 
select servicetypecode, paymenttype, 'Hours/RO' as cat, 
  sum(case when year_month = 201610 then hrs_per_ro else 0 end) as Oct_16,
  sum(case when year_month = 201611 then hrs_per_ro else 0 end) as Nov_16,
  sum(case when year_month = 201612 then hrs_per_ro else 0 end) as Dec_16,
  sum(case when year_month = 201701 then hrs_per_ro else 0 end) as Jan_17,
  sum(case when year_month = 201702 then hrs_per_ro else 0 end) as Feb_17,
  sum(case when year_month = 201703 then hrs_per_ro else 0 end) as Mar_17,
  sum(case when year_month = 201704 then hrs_per_ro else 0 end) as Apr_17,
  sum(case when year_month = 201705 then hrs_per_ro else 0 end) as May_17,  
  sum(case when year_month = 201706 then hrs_per_ro else 0 end) as Jun_17,
  sum(case when year_month = 201707 then hrs_per_ro else 0 end) as Jul_17,
  sum(case when year_month = 201708 then hrs_per_ro else 0 end) as Aug_17,
  sum(case when year_month = 201709 then hrs_per_ro else 0 end) as Sep_17,
  4
from ro_labor
group by servicetypecode, paymenttype
order by servicetypecode, paymenttype, sort