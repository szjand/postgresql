﻿-- 9/27 list the currently used temp tables
fs_accounts_lines
fs_unit_counts
fs_amounts
bud.f_l_penetration_detail - physical table
-------------9/26 ------------------------------------------------------------
---- get back on track with jeri ---------------------------------------------
unit count per /sales_pay_plan/sql/financial_Statement_unit_count

drop table if exists fs_count;
create temp table fs_count as
-- include stocknumber on each row
select store, page, line, line_label, control, sum(unit_count) as unit_count
from ( -- h
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a   
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201708
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201708
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
    and aa.journal_code in ('VSN','VSU')   
    where a.post_status = 'Y' order by control) h
group by store, page, line, line_label, control
order by store, page, line;



-- page/line count is good
select store, page, line, line_label, sum(unit_count)
from (
  select *
  from fs_count
  where store = 'ry2') a
group by store, page, line, line_label
order by store, page, line, line_label

select sum(unit_count) -- 71
from fs_count
where store = 'ry2'
  and page < 16

-- from /sql/ucinv/base_vehicles.sql
-- 1 row per control/date/unit_count
-- damnit, need to correlate the year of sypffxmst with the year of the transaction
-- so, this is close, but i would like it to be better
-- thinking do year_month and accounts separately
drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select year_month, store_code, control, the_date, sum(unit_count) as unit_count
from ( -- h
  select b.year_month, c.store_code, d.g_l_acct_number, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count, b.the_date
  from fin.fact_gl a   
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201201 and 201708
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account_type = 'Sale'
  inner join ( -- relevant accounts
    select distinct g_l_acct_number
    from arkona.ext_eisglobal_sypffxmst a
    inner join (
      select factory_financial_year, g_l_acct_number, factory_account, fact_account_
      from arkona.ext_ffpxrefdta) b on a.fxmcyy = b.factory_financial_year and a.fxmact = b.factory_account
    where a.fxmcyy = 2017
      and fxmpge = 16
      and fxmlne between 1 and 14) d on c.account = d.g_l_acct_number
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
    and aa.journal_code in ('VSN','VSU')   
  where a.post_status = 'Y') h
group by year_month, store_code, control, the_date;
create unique index on step_1(control,the_date,unit_count);
create index on step_1(control);


-- thinking do year_month and accounts separately
drop table if exists fs_accounts_lines; 
create temp table fs_accounts_lines as
select a.fxmcyy as the_year, a.fxmpge as page, a.fxmlne as line, a.fxmcol as col,
  a.fxmact as gm_account, b.g_l_acct_number as gl_account
from arkona.ext_eisglobal_sypffxmst a
inner join (
  select factory_financial_year, g_l_acct_number, factory_account, fact_account_
  from arkona.ext_ffpxrefdta) b on a.fxmcyy = b.factory_financial_year and a.fxmact = b.factory_account
where a.fxmcyy > 2011
  and (
    (fxmpge = 16 and fxmlne between 1 and 14) -- uc sales
    or
    (fxmpge between 5 and 14 and fxmlne between 1 and 40) -- nc sales
    or
    (fxmpge = 17 and fxmlne between 1 and 20)); -- fi
create unique index on fs_accounts_lines(the_year, page, line, col, gl_account);
create index on fs_accounts_lines(gm_account);

select * 
from fs_accounts_lines

-- excessive unit counts , need to break out department as well journal
-- include page, line, department, journal
drop table if exists fs_unit_counts cascade;
create table fs_unit_counts as
select year_month, store_code, control, the_date, sum(unit_count) as unit_count,
  department_code, page, line, journal_code
from ( -- h
  select b.year_month, c.store_code, d.gl_account, 
    a.control, a.amount,
    case 
      when c.department_code in ('nc','uc','ao') then
        case 
          when a.amount < 0 then 1
          else -1 
        end 
      else 0
    end as unit_count, b.the_date, c.department_code, d.page, d.line, aa.journal_code
  from fin.fact_gl a   
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201201 and 201708
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account_type = 'Sale'
  inner join fs_accounts_lines d on c.account = d.gl_account
    and b.the_year = d.the_year
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
    and aa.journal_code in ('VSN','VSU')   
  where a.post_status = 'Y') h
group by year_month, store_code, control, the_date,department_code, page, line, journal_code;
create unique index on fs_unit_counts(year_month, store_code, control, the_date,department_code, page, line, journal_code);
create index on fs_unit_counts(control);

select *
from fs_unit_counts
limit 100

-- this looks ok
select page, sum(unit_count)
from fs_unit_counts
where store_code = 'ry2'
  and year_month = 201708
group by page  

-- count
select year_month, 
  sum(case when page = 7 then unit_count else 0 end) as new_honda,
  sum(case when page = 14 then unit_count else 0 end) as new_nissan,
  sum(case when page = 16 and line between 1 and 5 then unit_count else 0 end) as uc_retail,
  sum(case when page = 16 and line between 8 and 11 then unit_count else 0 end) as uc_whsl
from fs_unit_counts
where store_code = 'ry2'
  and page in (7,14,16)
group by year_month
order by year_month


-- $$$
drop table if exists fs_amounts cascade;
create table fs_amounts as
select year_month, store_code, control, the_date, 
  department_code, page, line, col, journal_code, gl_account, sum(-amount)::integer as amount
from ( -- h
  select b.year_month, c.store_code, d.gl_account, 
    a.control, a.amount,
    b.the_date, c.department_code, d.page, d.line, d.col, aa.journal_code
  from fin.fact_gl a   
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201201 and 201708
  inner join fin.dim_account c on a.account_key = c.account_key
--     and c.account_type = 'Sale'
  inner join fs_accounts_lines d on c.account = d.gl_account
    and b.the_year = d.the_year
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
--     and aa.journal_code in ('VSN','VSU')   
  where a.post_status = 'Y') h
group by year_month, store_code, control, the_date,department_code, page, line, col, gl_account, journal_code;
create unique index on fs_amounts(year_month, store_code, control, the_date,department_code, page, line, col, journal_code, gl_account);
create index on fs_amounts(control);

select *
from fs_amounts
limit 500

-- gross
select year_month, 
  sum(case when page = 7 then amount else 0 end) as new_honda,
  sum(case when page = 14 then amount else 0 end) as new_nissan,
  sum(case when page = 16 and line between 1 and 5 then amount else 0 end) as uc_retail,
  sum(case when page = 16 and line between 8 and 11 then amount else 0 end) as uc_whsl
from fs_amounts
where store_code = 'ry2'
  and page in (7,14,16)
group by year_month
order by year_month


-- pvr
select a.year_month, a.new_honda as new_honda_count, (b.new_honda/a.new_honda)::integer as new_honda_pvr,
  a.new_nissan as new_nissan_count, (b.new_nissan/a.new_nissan)::integer as new_nissan_pvr,
  a.new_honda + a.new_nissan as new_count, (b.new_honda + b.new_nissan)/(a.new_honda + a.new_nissan)::integer as new_pvr,
  a.uc_retail as uc_retail_count, (b.uc_retail/a.uc_retail)::integer as uc_retail_pvr,
  a.uc_whsl as uc_whsl_count, (b.uc_whsl/a.uc_whsl)::integer as uc_whsl_pvr,
  a.uc_retail + a.uc_whsl as uc_count, (b.uc_whsl + b.uc_retail)/(a.uc_retail + a.uc_whsl)::integer as uc_pvr
-- select *
from ( -- count
  select year_month, 
    sum(case when page = 7 then unit_count else 0 end) as new_honda,
    sum(case when page = 14 then unit_count else 0 end) as new_nissan,
    sum(case when page = 16 and line between 1 and 5 then unit_count else 0 end) as uc_retail,
    sum(case when page = 16 and line between 8 and 11 then unit_count else 0 end) as uc_whsl
  from fs_unit_counts
  where store_code = 'ry2'
    and page in (7,14,16)
  group by year_month) a
left join ( -- gross
  select year_month, 
    sum(case when page = 7 then amount else 0 end) as new_honda,
    sum(case when page = 14 then amount else 0 end) as new_nissan,
    sum(case when page = 16 and line between 1 and 5 then amount else 0 end) as uc_retail,
    sum(case when page = 16 and line between 8 and 11 then amount else 0 end) as uc_whsl
  from fs_amounts
  where store_code = 'ry2'
    and page in (7,14,16)
  group by year_month) b on a.year_month = b.year_month
order by a.year_month  

-- -- -- -- this is all playing with getting penetration
-- -- -- -- adding vehicle sales knocks it down to 216 rows
-- -- -- select *
-- -- -- from fs_unit_counts a
-- -- -- left join sls.ext_bopmast_partial b on a.control = b.stock_number
-- -- -- left join ads.ext_dim_car_deal_info c on b.deal_type = c.dealtypecode
-- -- --   and b.sale_type = c.saletypecode
-- -- --   and b.vehicle_type = c.vehicletypecode
-- -- --   and b.deal_status = c.dealstatuscode
-- -- -- left join (
-- -- --   select b.stocknumber, a.soldts, a.typ, a.status, a.soldto
-- -- --   from ads.ext_vehicle_sales a
-- -- --   inner join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
-- -- --   where a.typ = 'VehicleSale_Wholesale') d on a.control = d.stocknumber
-- -- -- where a.unit_count <> 0
-- -- --   and b.stock_number is null
-- -- --   and d.stocknumber is null
-- -- -- 
-- -- -- 
-- -- -- -- get rid of offsetting month/deals
-- -- -- select year_month, control
-- -- -- from fs_unit_counts
-- -- -- where unit_count <> 0
-- -- -- group by year_month, control
-- -- -- having sum(unit_count) <> 0
-- -- -- 
-- -- -- 
-- -- -- create temp table test as
-- -- -- select year_month, control
-- -- -- from ( -- get rid of offsetting month/deals
-- -- --   select year_month, control
-- -- --   from fs_unit_counts
-- -- --   where unit_count <> 0
-- -- --   group by year_month, control
-- -- --   having sum(unit_count) <> 0) a
-- -- -- left join sls.ext_bopmast_partial b on a.control = b.stock_number
-- -- -- left join ads.ext_dim_car_deal_info c on b.deal_type = c.dealtypecode
-- -- --   and b.sale_type = c.saletypecode
-- -- --   and b.vehicle_type = c.vehicletypecode
-- -- --   and b.deal_status = c.dealstatuscode
-- -- -- left join (
-- -- --   select b.stocknumber, a.soldts, a.typ, a.status, a.soldto
-- -- --   from ads.ext_vehicle_sales a
-- -- --   inner join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
-- -- --   where a.typ = 'VehicleSale_Wholesale') d on a.control = d.stocknumber
-- -- -- where b.stock_number is null
-- -- --   and d.stocknumber is null -- ) x group by year_month order by year_month
-- -- -- order by a.year_month, a.control  
-- -- -- 
-- -- -- select a.*, b.vin, c.inpmast_vin, d.stock_number, d.delivery_Date,
-- -- --   100 * extract(year from d.delivery_date) + extract(month from d.delivery_date),
-- -- --   d.*,
-- -- --   e.*
-- -- -- from test a
-- -- -- left join uc.base_vehicles b on a.control = b.stock_number
-- -- -- left join arkona.ext_inpmast c on a.control = c.inpmast_stock_number
-- -- -- left join sls.ext_bopmast_partial d on c.inpmast_vin = d.vin
-- -- --   and a.year_month = 100 * extract(year from d.delivery_date) + extract(month from d.delivery_date)
-- -- -- left join (
-- -- --   select stock_number, deal_type_code, sale_type_code, vehicle_type_code
-- -- --   from sls.deals
-- -- --   group by stock_number, deal_type_code, sale_type_code, vehicle_type_code) e on a.control = e.stock_number
-- -- -- order by a.year_month
-- -- -- 
-- -- -- -- this is all i can get, down to 67 rows of unidentifiable deals
-- -- -- select a.year_month, a.control, 
-- -- --   d.deal_status, 
-- -- --   coalesce(d.deal_type, e.deal_type_code) as deal_type,
-- -- --   coalesce(d.sale_type, e.sale_type_code) as sale_type,
-- -- --   coalesce(d.vehicle_type, e.vehicle_type_code) as vehicle_type
-- -- -- from test a
-- -- -- left join uc.base_vehicles b on a.control = b.stock_number
-- -- -- left join arkona.ext_inpmast c on a.control = c.inpmast_stock_number
-- -- -- left join sls.ext_bopmast_partial d on c.inpmast_vin = d.vin
-- -- --   and a.year_month = 100 * extract(year from d.delivery_date) + extract(month from d.delivery_date)
-- -- -- left join (
-- -- --   select stock_number, deal_type_code, sale_type_code, vehicle_type_code
-- -- --   from sls.deals
-- -- --   group by stock_number, deal_type_code, sale_type_code, vehicle_type_code) e on a.control = e.stock_number
-- -- -- where coalesce(d.deal_type, e.deal_type_code) is null  
-- -- -- order by a.year_month
-- -- -- 
-- -- -- select * 
-- -- -- from fs_unit_counts
-- -- -- where control = '31135c'
-- -- -- 
-- -- -- select *
-- -- -- from sls.deals
-- -- -- where stock_number = '31135c'
-- -- -- order by seq
-- -- -- 
-- -- -- select stock_number, deal_type_code, sale_type_code, vehicle_type_code
-- -- -- from sls.deals
-- -- -- group by stock_number, deal_type_code, sale_type_code, vehicle_type_code

-- 9/27 ----------------------------------------------------------------------------------------------------------
-- ok let's put it together, this is lease/finance penetration
-- shit, forgot, this generates some dups
-- make it a physical table, clean up the dups and be done with it,
-- but neeed to figure out how to maintain it instead of recreating it regularly
-- drop table if exists f_l_penetration_detail;
-- create temp table f_l_penetration_detail as
-- DO NOT DROP THIS TABLE UNLESS YOU ARE READY TO CLEAN UP DUPS
create table bud.f_l_penetration_detail (
  year_month integer not null,
  control citext not null,
  deal_status_code citext,
  deal_status citext,
  deal_type_code citext,
  deal_type citext,
  vehicle_type_code citext,
  vehicle_type citext,
  sale_type_code citext,
  sale_type citext);
insert into bud.f_l_penetration_detail  
select *
from (
  select a.year_month, a.control, 
    c.dealstatuscode, c.dealstatus, c.dealtypecode, c.dealtype, 
    case 
      when c.vehicletypecode is not null then c.vehicletypecode
      when c.vehicletypecode is null and d.stocknumber is not null then 'U'
    end as vehicletypecode,
    case 
      when c.vehicletypecode is not null then c.vehicletype
      when c.vehicletypecode is null and d.stocknumber is not null then 'Used'
    end as vehicletype,    
    case
      when c.saletypecode is not null then c.saletypecode
      when c.saletypecode is null and d.stocknumber is not null then 'W'
    end as saletypecode,
    case
      when c.saletypecode is not null then c.saletype
      when c.saletypecode is null and d.stocknumber is not null then 'Wholesale'
    end as saletype
  from ( -- get rid of offsetting month/deals
    select year_month, control
    from fs_unit_counts
    where unit_count <> 0
    group by year_month, control
    having sum(unit_count) <> 0) a
  left join sls.ext_bopmast_partial b on a.control = b.stock_number
  left join ads.ext_dim_car_deal_info c on b.deal_type = c.dealtypecode
    and b.sale_type = c.saletypecode
    and b.vehicle_type = c.vehicletypecode
    and b.deal_status = c.dealstatuscode
  left join (
    select b.stocknumber, a.soldts, a.typ, a.status, a.soldto
    from ads.ext_vehicle_sales a
    inner join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
    where a.typ = 'VehicleSale_Wholesale') d on a.control = d.stocknumber) aa
where saletypecode is not null
--
union
--
select bb.year_month, bb.control, 'U', 'Capped', h.*
from (
  select aa.*, 
    f.deal_status, 
    coalesce(f.deal_type, g.deal_type_code) as deal_type,
    coalesce(f.sale_type, g.sale_type_code) as sale_type, 
    coalesce(f.vehicle_type, g.vehicle_type_code) as vehicle_type
  from (
    select a.year_month, a.control, 
      c.dealstatuscode, c.dealstatus, c.dealtypecode, c.dealtype, 
      case 
        when c.vehicletypecode is not null then c.vehicletypecode
        when c.vehicletypecode is null and d.stocknumber is not null then 'U'
      end as vehicletypecode,
      case 
        when c.vehicletypecode is not null then c.vehicletype
        when c.vehicletypecode is null and d.stocknumber is not null then 'Used'
      end as vehicletype,    
      case
        when c.saletypecode is not null then c.saletypecode
        when c.saletypecode is null and d.stocknumber is not null then 'W'
      end as saletypecode,
      case
        when c.saletypecode is not null then c.saletype
        when c.saletypecode is null and d.stocknumber is not null then 'Wholesale'
      end as saletype
    from ( -- get rid of offsetting month/deals
      select year_month, control
      from fs_unit_counts
      where unit_count <> 0
      group by year_month, control
      having sum(unit_count) <> 0) a
    left join sls.ext_bopmast_partial b on a.control = b.stock_number
    left join ads.ext_dim_car_deal_info c on b.deal_type = c.dealtypecode
      and b.sale_type = c.saletypecode
      and b.vehicle_type = c.vehicletypecode
      and b.deal_status = c.dealstatuscode
    left join (
      select b.stocknumber, a.soldts, a.typ, a.status, a.soldto
      from ads.ext_vehicle_sales a
      inner join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
      where a.typ = 'VehicleSale_Wholesale') d on a.control = d.stocknumber) aa
  left join arkona.ext_inpmast e on aa.control = e.inpmast_stock_number
  left join sls.ext_bopmast_partial f on e.inpmast_vin = f.vin
    and aa.year_month = 100 * extract(year from f.delivery_date) + extract(month from f.delivery_date)
  left join (
    select stock_number, deal_type_code, sale_type_code, vehicle_type_code
    from sls.deals
    group by stock_number, deal_type_code, sale_type_code, vehicle_type_code) g on aa.control = g.stock_number    
  where aa.saletypecode is null)  bb
left join (
  select distinct dealtypecode,dealtype,vehicletypecode,vehicletype,saletypecode,saletype
  from ads.ext_dim_car_deal_info) h on bb.deal_type = h.dealtypecode
    and bb.sale_type = h.saletypecode
    and bb.vehicle_type = h.vehicletypecode;
create unique index on bud.f_l_penetration_detail(year_month,control);

-- yikes, got some clean up to do  
-- -- -- don't erase this
-- -- select *
-- -- from bud.f_l_penetration_detail a
-- -- inner join (
-- -- select control, year_month
-- -- from bud.f_l_penetration_detail
-- -- group by control, year_month having count(*) > 1) b on a.control = b.control
-- -- order by a.control, a.year_month
-- -- 
-- -- select *
-- -- -- delete
-- -- from bud.f_l_penetration_detail a
-- -- where control = '31284X'
-- --   and (year_month = 201706)
-- --   and deal_type_code = 'c')

-- not real sure about completeness/accuracy, but probably ok
select a.year_month, a.control, a.unit_count,
  case 
    when a.page = 7 then 'new_honda'
    when a.page = 14 then 'new_nissan'
    when a.page = 16 and line between 1 and 5 then 'uc_retail'
    when a.page = 16 and line between 8 and 11 then 'uc_whsl'
  end as category,
  b.*
from fs_unit_counts a
left join f_l_penetration b on a.control = b.control
  and a.year_month = b.year_month
where a.unit_count <> 0
  and a.store_code = 'ry2'
  and a.page in (7,14,16)
--   and b.control is null
and not exists ( -- exclude same month unwind/rewind
    select 1
    from (
      select m.year_month, m.control
      from fs_unit_counts m
      where m.page in (7,14,16)
      group by m.year_month, m.control
      having sum(unit_count) = 0) n
    where n.year_month = a.year_month
      and n.control = a.control )


select dealtypecode, dealtype, saletypecode,saletype
from ads.ext_dim_car_deal_info
group by dealtypecode, dealtype, saletypecode,saletype

dealtype: L : Financed Lease
saletype: L : Lease

-- penetration percentages
select m.year_month,
  round(100 * (n.new_honda_lease/m.new_honda), 1) as new_honda_lease,
  round(100 * (n.new_nissan_lease/m.new_nissan), 1) as new_nissan_lease,
  round(100 * ((n.new_honda_lease + n.new_nissan_lease)/(m.new_honda + m.new_nissan)), 1) as new_lease,
  round(100 * (n.new_honda_fin/m.new_honda), 1) as new_honda_fin,
  round(100 * (n.new_nissan_fin/m.new_nissan), 1) as new_nissan_fin,
  round(100 * ((n.new_honda_fin + n.new_nissan_fin)/(m.new_honda + m.new_nissan)), 1) as new_fin,
  round(100 * (n.uc_fin/m.uc_retail), 1) as uc_fin
-- select *
from ( -- count
  select year_month, 
    sum(case when page = 7 then unit_count else 0 end) as new_honda,
    sum(case when page = 14 then unit_count else 0 end) as new_nissan,
    sum(case when page = 16 and line between 1 and 5 then unit_count else 0 end) as uc_retail,
    sum(case when page = 16 and line between 8 and 11 then unit_count else 0 end) as uc_whsl
  from fs_unit_counts
  where store_code = 'ry2'
    and page in (7,14,16)
  group by year_month) m
left join ( -- finand and lease penetration  
  select d.year_month, 
    -- finance
    sum(case when d.deal_type_code = 'L' and d.category = 'new_honda' then 1 else 0 end) as new_honda_lease,
    sum(case when d.deal_type_code = 'L' and d.category = 'new_nissan' then 1 else 0 end) as new_nissan_lease,
    sum(case when d.deal_type_code = 'L' and d.category in('new_nissan','new_honda') then 1 else 0 end) as new_lease,
    sum(case when d.deal_type_code = 'F' and d.category = 'new_honda' then 1 else 0 end) as new_honda_fin,
    sum(case when d.deal_type_code = 'F' and d.category = 'new_nissan' then 1 else 0 end) as new_nissan_fin,
    sum(case when d.deal_type_code = 'F' and d.category in('new_nissan','new_honda') then 1 else 0 end) as new_fin,
    sum(case when d.deal_type_code = 'F' and d.category = 'uc_retail' then 1 else 0 end) as uc_fin
  -- select dealtypecode, dealtype, saletypecode, saletype, count(*)
  -- select *
  from (
    select a.year_month, a.control, a.unit_count,
      case 
        when a.page = 7 then 'new_honda'
        when a.page = 14 then 'new_nissan'
        when a.page = 16 and line between 1 and 5 then 'uc_retail'
        when a.page = 16 and line between 8 and 11 then 'uc_whsl'
      end as category,
      b.deal_type_code, b.deal_type, b.sale_type_code, b.sale_type
    from fs_unit_counts a
    left join bud.f_l_penetration_detail b on a.control = b.control
      and a.year_month = b.year_month
    where a.unit_count <> 0
      and a.store_code = 'ry2'
      and a.page in (7,14,16)
    --   and b.control is null
    and not exists ( -- exclude same month unwind/rewind
        select 1
        from (
          select m.year_month, m.control
          from fs_unit_counts m
          where m.page in (7,14,16)
          group by m.year_month, m.control
          having sum(unit_count) = 0) n
        where n.year_month = a.year_month
          and n.control = a.control)) d
  where category in ('new_honda','new_nissan','uc_retail')
  group by d.year_month) n on m.year_month = n.year_month
order by m.year_month


-- 9/27 ----------------------------------------------------------------------------------------------------------
reserve and product pvr

select *
from fs_amounts a
left join fs_unit_counts b on a.control = b.control
where b.control is not null
limit 100

select a.*, b.description
from fs_amounts a
left join fin.dim_account b on a.gl_Account = b.account
where a.page = 17
  and a.store_code = 'ry2'
  and a.year_month = 201708
order by page, line, gl_account

select year_month, page, line, col, gl_account, description, sum(amount)
from fs_amounts a
left join fin.dim_account b on a.gl_Account = b.account
where a.page = 17
  and a.store_code = 'ry2'
  and a.year_month = 201708
group by year_month, page, line, col, gl_account, description
order by line

line 1: 280600: new_honda_reserve
        280601: new_nissan_reserve
line 6/7: 244300, 264300, 244402, 264402: new_honda_product
          244301, 264301, 244400, 264400: new_nissan_prod
line 11: all: used_reserve       
line 16/17: all: used_product
  

select m.year_month,
  (n.new_honda_reserve/m.new_honda)::integer as new_honda_reserve_pvr,
  (n.new_nissan_reserve/m.new_nissan)::integer as new_nissan_reserve_pvr,
  ((n.new_honda_reserve + n.new_nissan_reserve)/(m.new_honda + m.new_nissan))::integer as new_reserve_pvr,
  (n.new_honda_product/m.new_honda)::integer as new_honda_product_pvr,
  (n.new_nissan_product/m.new_nissan)::integer as new_nissan_product_pvr,
  ((n.new_honda_product + n.new_nissan_product)/(m.new_honda + m.new_nissan))::integer as new_product_pvr,
  (n.uc_reserve/m.uc_retail)::integer as uc_reserve_pvr,
  (n.uc_product/m.uc_retail)::integer as uc_product_pvr
from ( -- count
  select year_month, 
    sum(case when page = 7 then unit_count else 0 end) as new_honda,
    sum(case when page = 14 then unit_count else 0 end) as new_nissan,
    sum(case when page = 16 and line between 1 and 5 then unit_count else 0 end) as uc_retail,
    sum(case when page = 16 and line between 8 and 11 then unit_count else 0 end) as uc_whsl
  from fs_unit_counts
  where store_code = 'ry2'
    and page in (7,14,16)
  group by year_month) m
left join ( -- reserve/product  
  select year_month,
    sum(case when line = 1 and gl_account = '280600' then amount else 0 end) as new_honda_reserve,
    sum(case when line = 1 and gl_account = '280601' then amount else 0 end) as new_nissan_reserve,
    sum(case when line = 1 then amount else 0 end) as new_reserve,
    sum(case when line in (6, 7) and gl_account in ('244300','264300','244402','264402') then amount else 0 end) as new_honda_product,
    sum(case when line in (6, 7) and gl_account in ('244301','264301','244400','264400') then amount else 0 end) as new_nissan_product,
    sum(case when line in (6, 7) then amount else 0 end) as new_product,
    sum(case when line = 11 then amount else 0 end) as uc_reserve,
    sum(case when line in (16, 17) then amount else 0 end) as uc_product
  from fs_amounts a
  left join fin.dim_account b on a.gl_Account = b.account
  where a.page = 17
    and a.store_code = 'ry2'
  group by year_month) n on m.year_month = n.year_month
order by year_month




-- 9/28 same thing for r1y ----------------------------------------------------------------------------------------------------------
-- count, exclude fleet & internal
select year_month,
  sum(case when page = 5 and ((line between 1 and 19) or (line between 25 and 40)) then unit_count else 0 end) as new_chev,
  sum(case when page = 8 and ((line between 1 and 19) or (line between 25 and 40)) then unit_count else 0 end) as new_buick,
  sum(case when page = 9 and ((line between 1 and 19) or (line between 25 and 40)) then unit_count else 0 end) as new_cad,
  sum(case when page = 10 and ((line between 1 and 19) or (line between 25 and 40)) then unit_count else 0 end) as new_gmc,
  sum(case when page = 14 and ((line between 1 and 19) or (line between 25 and 40)) then unit_count else 0 end) as new_mv1,
  sum(case when page between 5 and 14 and ((line between 1 and 19) or (line between 25 and 40)) then unit_count else 0 end) as new_total,
  sum(case when page = 16 and line between 1 and 5 then unit_count else 0 end) as uc_retail,
  sum(case when page = 16 and line between 8 and 11 then unit_count else 0 end) as uc_whsl,
  sum(case when page = 16 and line between 1 and 11 then unit_count else 0 end) as uc_total
from fs_unit_counts
where store_code = 'ry1'
--   and year_month = 201708
group by year_month
order by year_month


-- gross
select year_month,
  sum(case when page = 5 and ((line between 1 and 19) or (line between 25 and 40)) then amount else 0 end) as new_chev,
  sum(case when page = 8 and ((line between 1 and 19) or (line between 25 and 40)) then amount else 0 end) as new_buick,
  sum(case when page = 9 and ((line between 1 and 19) or (line between 25 and 40)) then amount else 0 end) as new_cad,
  sum(case when page = 10 and ((line between 1 and 19) or (line between 25 and 40)) then amount else 0 end) as new_gmc,
  sum(case when page = 14 and ((line between 1 and 19) or (line between 25 and 40)) then amount else 0 end) as new_mv1,
  sum(case when page between 5 and 14 and ((line between 1 and 19) or (line between 25 and 40)) then amount else 0 end) as new_total,
  sum(case when page = 16 and line between 1 and 5 then amount else 0 end) as uc_retail,
  sum(case when page = 16 and line between 8 and 11 then amount else 0 end) as uc_whsl,
  sum(case when page = 16 and line between 1 and 11 then amount else 0 end) as uc_total  
from fs_amounts
where store_code = 'ry1'
  and page in (5,8,9,10,14,16)
--   and year_month = 201708
group by year_month
order by year_month



select a.year_month, a.new_honda as new_honda_count, (b.new_honda/a.new_honda)::integer as new_honda_pvr,
  a.new_nissan as new_nissan_count, (b.new_nissan/a.new_nissan)::integer as new_nissan_pvr,
  a.new_honda + a.new_nissan as new_count, (b.new_honda + b.new_nissan)/(a.new_honda + a.new_nissan)::integer as new_pvr,
  a.uc_retail as uc_retail_count, (b.uc_retail/a.uc_retail)::integer as uc_retail_pvr,
  a.uc_whsl as uc_whsl_count, (b.uc_whsl/a.uc_whsl)::integer as uc_whsl_pvr,
  a.uc_retail + a.uc_whsl as uc_count, (b.uc_whsl + b.uc_retail)/(a.uc_retail + a.uc_whsl)::integer as uc_pvr

  
-- pvr
select a.year_month,
  a.new_chev as new_chev_count, (b.new_chev/a.new_chev):: integer as new_chev_pvr,
  a.new_buick as new_buick, (b.new_buick/a.new_buick):: integer as new_buick_pvr,
  a.new_cad as new_cad_count, (b.new_cad/a.new_cad):: integer as new_cad_pvr,
  a.new_gmc as new_gmc_count, (b.new_gmc/a.new_gmc):: integer as new_gmc_pvr,
  a.new_mv1 as new_mv1_count, case when a.new_mv1 = 0 then 0 else (b.new_mv1/a.new_mv1):: integer end as new_mv1_pvr,
  a.new_total as new_total_count, (b.new_total/a.new_total):: integer as new_total_pvr,
  a.uc_retail as uc_retail_count, (b.uc_retail/a.uc_retail):: integer as uc_retail_pvr,
  a.uc_whsl as uc_whsl_count, (b.uc_whsl/a.uc_whsl):: integer as uc_whsl_pvr,
  a.uc_total as uc_total_count, (b.uc_total/a.uc_total):: integer as uc_total_pvr
from ( -- count
  select year_month,
    sum(case when page = 5 and ((line between 1 and 19) or (line between 25 and 40)) then unit_count else 0 end) as new_chev,
    sum(case when page = 8 and ((line between 1 and 19) or (line between 25 and 40)) then unit_count else 0 end) as new_buick,
    sum(case when page = 9 and ((line between 1 and 19) or (line between 25 and 40)) then unit_count else 0 end) as new_cad,
    sum(case when page = 10 and ((line between 1 and 19) or (line between 25 and 40)) then unit_count else 0 end) as new_gmc,
    sum(case when page = 14 and ((line between 1 and 19) or (line between 25 and 40)) then unit_count else 0 end) as new_mv1,
    sum(case when page between 5 and 14 and ((line between 1 and 19) or (line between 25 and 40)) then unit_count else 0 end) as new_total,
    sum(case when page = 16 and line between 1 and 5 then unit_count else 0 end) as uc_retail,
    sum(case when page = 16 and line between 8 and 11 then unit_count else 0 end) as uc_whsl,
    sum(case when page = 16 and line between 1 and 11 then unit_count else 0 end) as uc_total
  from fs_unit_counts
  where store_code = 'ry1'
  group by year_month) a
left join ( -- gross
  select year_month,
    sum(case when page = 5 and ((line between 1 and 19) or (line between 25 and 40)) then amount else 0 end) as new_chev,
    sum(case when page = 8 and ((line between 1 and 19) or (line between 25 and 40)) then amount else 0 end) as new_buick,
    sum(case when page = 9 and ((line between 1 and 19) or (line between 25 and 40)) then amount else 0 end) as new_cad,
    sum(case when page = 10 and ((line between 1 and 19) or (line between 25 and 40)) then amount else 0 end) as new_gmc,
    sum(case when page = 14 and ((line between 1 and 19) or (line between 25 and 40)) then amount else 0 end) as new_mv1,
    sum(case when page between 5 and 14 and ((line between 1 and 19) or (line between 25 and 40)) then amount else 0 end) as new_total,
    sum(case when page = 16 and line between 1 and 5 then amount else 0 end) as uc_retail,
    sum(case when page = 16 and line between 8 and 11 then amount else 0 end) as uc_whsl,
    sum(case when page = 16 and line between 1 and 11 then amount else 0 end) as uc_total  
  from fs_amounts
  where store_code = 'ry1'
    and page in (5,8,9,10,14,16)
  group by year_month) b on a.year_month = b.year_month 
order by year_month



-- fi penetration percentages
select m.year_month,
  round(100 * (n.new_chev_lease/m.new_chev), 1) as new_chev_lease,
  round(100 * (n.new_buick_lease/m.new_buick), 1) as new_buick_lease,
  round(100 * (n.new_cad_lease/m.new_cad), 1) as new_cad_lease,
  round(100 * (n.new_gmc_lease/m.new_gmc), 1) as new_gmc_lease,
  case when m.new_mv1 = 0 then 0.0 else round(100 * (n.new_mv1_lease/m.new_mv1), 1) end as new_mv1_lease,
  round(100 * (n.new_total_lease/m.new_total), 1) as new_total_lease,
  round(100 * (n.new_chev_fin/m.new_chev), 1) as new_chev_fin,
  round(100 * (n.new_buick_fin/m.new_buick), 1) as new_buick_fin,
  round(100 * (n.new_cad_fin/m.new_cad), 1) as new_cad_fin,
  round(100 * (n.new_gmc_fin/m.new_gmc), 1) as new_gmc_fin,
  case when m.new_mv1 = 0 then 0.0 else round(100 * (n.new_mv1_fin/m.new_mv1), 1) end as new_mv1_fin,
  round(100 * (n.new_total_fin/m.new_total), 1) as new_total_fin,  
  round(100 * (n.uc_fin/m.uc_retail), 1) as uc_fin
from ( -- count
  select year_month,
    sum(case when page = 5 and ((line between 1 and 19) or (line between 25 and 40)) then unit_count else 0 end) as new_chev,
    sum(case when page = 8 and ((line between 1 and 19) or (line between 25 and 40)) then unit_count else 0 end) as new_buick,
    sum(case when page = 9 and ((line between 1 and 19) or (line between 25 and 40)) then unit_count else 0 end) as new_cad,
    sum(case when page = 10 and ((line between 1 and 19) or (line between 25 and 40)) then unit_count else 0 end) as new_gmc,
    sum(case when page = 14 and ((line between 1 and 19) or (line between 25 and 40)) then unit_count else 0 end) as new_mv1,
    sum(case when page between 5 and 14 and ((line between 1 and 19) or (line between 25 and 40)) then unit_count else 0 end) as new_total,
    sum(case when page = 16 and line between 1 and 5 then unit_count else 0 end) as uc_retail,
    sum(case when page = 16 and line between 8 and 11 then unit_count else 0 end) as uc_whsl,
    sum(case when page = 16 and line between 1 and 11 then unit_count else 0 end) as uc_total
  from fs_unit_counts
  where store_code = 'ry1'
  group by year_month) m
left join (
  select d.year_month, 
    sum(case when d.deal_type_code = 'L' and d.category = 'new_chev' then 1 else 0 end) as new_chev_lease,
    sum(case when d.deal_type_code = 'L' and d.category = 'new_buick' then 1 else 0 end) as new_buick_lease,
    sum(case when d.deal_type_code = 'L' and d.category = 'new_cad' then 1 else 0 end) as new_cad_lease,
    sum(case when d.deal_type_code = 'L' and d.category = 'new_gmc' then 1 else 0 end) as new_gmc_lease,
    sum(case when d.deal_type_code = 'L' and d.category = 'new_mvi' then 1 else 0 end) as new_mv1_lease,
    sum(case when d.deal_type_code = 'L' and d.category like 'new%' then 1 else 0 end) as new_total_lease,
    sum(case when d.deal_type_code = 'F' and d.category = 'new_chev' then 1 else 0 end) as new_chev_fin,
    sum(case when d.deal_type_code = 'F' and d.category = 'new_buick' then 1 else 0 end) as new_buick_fin,
    sum(case when d.deal_type_code = 'F' and d.category = 'new_cad' then 1 else 0 end) as new_cad_fin,
    sum(case when d.deal_type_code = 'F' and d.category = 'new_gmc' then 1 else 0 end) as new_gmc_fin,
    sum(case when d.deal_type_code = 'F' and d.category = 'new_mvi' then 1 else 0 end) as new_mv1_fin,
    sum(case when d.deal_type_code = 'F' and d.category like 'new%' then 1 else 0 end) as new_total_fin,  
    sum(case when d.deal_type_code = 'F' and d.category = 'uc_retail' then 1 else 0 end) as uc_fin  
  from (
    select a.year_month, a.control, a.unit_count,
      case 
        when a.page = 5 then 'new_chev'
        when a.page = 8 then 'new_buick'
        when a.page = 9 then 'new_cad'
        when a.page = 10 then 'new_gmc'
        when a.page = 14 then 'new_mv1'
        when a.page = 16 and line between 1 and 5 then 'uc_retail'
        when a.page = 16 and line between 8 and 11 then 'uc_whsl'
      end as category,
      b.deal_type_code, b.deal_type, b.sale_type_code, b.sale_type
    from fs_unit_counts a
    left join bud.f_l_penetration_detail b on a.control = b.control
      and a.year_month = b.year_month
    where a.unit_count <> 0
      and a.store_code = 'ry1'
      and a.page in (5,8,9,10,14,16)
    --   and b.control is null
    and not exists ( -- exclude same month unwind/rewind
        select 1
        from (
          select m.year_month, m.control
          from fs_unit_counts m
          where m.page in (5,8,9,10,14,16)
          group by m.year_month, m.control
          having sum(unit_count) = 0) n
        where n.year_month = a.year_month
          and n.control = a.control)) d
  where category in ('new_chev','new_buick','new_cad','new_gmc','new_mv1','uc_retail')
  group by d.year_month) n on m.year_month = n.year_month
order by m.year_month  


-- reserve and product pvr ---------------------------------------------------------------------------------------------------
ruh roh, makes are not separated by accounts for gm finance
store is not accustomed to looking at reserve/product broken out by make
i am going to bypass it for now

select year_month, page, line, col, gl_account, description, sum(amount)
from fs_amounts a
left join fin.dim_account b on a.gl_Account = b.account
where a.page = 17
  and a.store_code = 'ry1'
  and a.year_month = 201708
group by year_month, page, line, col, gl_account, description
order by line

select line, sum(amount)
from fs_amounts
where page = 17
  and store_code = 'ry1'
  and year_month = 201708
  and line in (1,6,7,11,16,17)
group by line
order by line



select m.year_month,
  (n.new_reserve/m.new_total)::integer as new_reserve_pvr,
  (n.new_product/m.new_total)::integer as new_product_pvr,
  (n.uc_reserve/m.uc_retail)::integer as uc_reserve_pvr,
  (n.uc_product/m.uc_retail)::integer as uc_product_pvr  
from (
  select year_month,
    sum(case when page between 5 and 14 and ((line between 1 and 19) or (line between 25 and 40)) then unit_count else 0 end) as new_total,
    sum(case when page = 16 and line between 1 and 5 then unit_count else 0 end) as uc_retail
  from fs_unit_counts
  where store_code = 'ry1'
  group by year_month) m
left join (  
  select year_month,
    sum(case when line = 1 then amount else 0 end) as new_reserve,
    sum(case when line in (6, 7) then amount else 0 end) as new_product,
    sum(case when line = 11 then amount else 0 end) as uc_reserve,
    sum(case when line in (16, 17) then amount else 0 end) as uc_product
  from fs_amounts a
  left join fin.dim_account b on a.gl_Account = b.account
  where a.page = 17
    and a.store_code = 'ry1'
  group by year_month) n on m.year_month = n.year_month
order by m.year_month




















  
-- -- -- buick line labels changing over the years
-- -- -- select *
-- -- -- from (
-- -- --   select flcyy, flflne, left(fldata, 20)
-- -- --   from arkona.ext_eisglobal_sypfflout
-- -- --   where flpage = 8
-- -- --     and flcyy = 2012
-- -- --     and (
-- -- --       (flflne between 1 and 18)
-- -- --       or
-- -- --       (flflne between 25 and 39))) a
-- -- -- left join (      
-- -- --   select flcyy, flflne, left(fldata, 20)
-- -- --   from arkona.ext_eisglobal_sypfflout
-- -- --   where flpage = 8
-- -- --     and flcyy = 2013
-- -- --     and (
-- -- --       (flflne between 1 and 18)
-- -- --       or
-- -- --       (flflne between 25 and 39))) b on a.flflne = b.flflne    
-- -- -- left join (      
-- -- --   select flcyy, flflne, left(fldata, 20)
-- -- --   from arkona.ext_eisglobal_sypfflout
-- -- --   where flpage = 8
-- -- --     and flcyy = 2014
-- -- --     and (
-- -- --       (flflne between 1 and 18)
-- -- --       or
-- -- --       (flflne between 25 and 39))) c on a.flflne = c.flflne  
-- -- -- left join (      
-- -- --   select flcyy, flflne, left(fldata, 20)
-- -- --   from arkona.ext_eisglobal_sypfflout
-- -- --   where flpage = 8
-- -- --     and flcyy = 2015
-- -- --     and (
-- -- --       (flflne between 1 and 18)
-- -- --       or
-- -- --       (flflne between 25 and 39))) d on a.flflne = d.flflne  
-- -- -- left join (      
-- -- --   select flcyy, flflne, left(fldata, 20)
-- -- --   from arkona.ext_eisglobal_sypfflout
-- -- --   where flpage = 8
-- -- --     and flcyy = 2016
-- -- --     and (
-- -- --       (flflne between 1 and 18)
-- -- --       or
-- -- --       (flflne between 25 and 39))) e on a.flflne = e.flflne  
-- -- -- left join (      
-- -- --   select flcyy, flflne, left(fldata, 20)
-- -- --   from arkona.ext_eisglobal_sypfflout
-- -- --   where flpage = 8
-- -- --     and flcyy = 2017
-- -- --     and (
-- -- --       (flflne between 1 and 18)
-- -- --       or
-- -- --       (flflne between 25 and 39))) f on a.flflne = f.flflne                          
-- -- -- order by a.flflne