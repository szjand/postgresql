﻿select aa.year_month, 
  bb.store_code, bb.last_name, bb.first_name, bb.employee_number, bb.total_gross_pay,
  cc.unit_count
from (
  select distinct year_month
  from dds.dim_date
  where year_month between 201707 and 201801) aa
left join (
  select a.store_code, a.last_name, a.first_name, a.employee_number, 
    b.payroll_ending_year, b.payroll_ending_month, sum(b.total_gross_pay) as total_gross_pay,
    100*(2000 + b.payroll_ending_year) + b.payroll_ending_month as year_month
  from sls.personnel a
  inner join arkona.ext_pyhshdta b on a.employee_number = b.employee_
  where (
    (b.payroll_ending_year = 17 and b.payroll_ending_month between 7 and 12)
    or
    (b.payroll_ending_year = 18 and b.payroll_ending_month = 1))
  group by a.store_code, a.last_name, a.first_name, a.employee_number, 
    b.payroll_ending_year, b.payroll_ending_month) bb on aa.year_month = bb.year_month 
left join monthly_count cc on aa.year_month = cc.year_month and bb.employee_number = cc.psc_employee_number    
where aa.year_month between 201707 and 201801    
  and cc.year_month is not null

select store_code, last_name, first_name, employee_number,
  sum(case when year_month = 201707 then total_gross_pay end) as "Jul Paid",
  sum(case when year_month = 201707 then unit_count end) as "Jul Count",
  sum(case when year_month = 201708 then total_gross_pay end) as "Aug Paid",
  sum(case when year_month = 201708 then unit_count end) as "Aug Count",
  sum(case when year_month = 201709 then total_gross_pay end) as "Sep Paid",
  sum(case when year_month = 201709 then unit_count end) as "Sep Count",
  sum(case when year_month = 201710 then total_gross_pay end) as "Oct Paid",
  sum(case when year_month = 201710 then unit_count end) as "Oct Count",
  sum(case when year_month = 201711 then total_gross_pay end) as "Nov Paid",
  sum(case when year_month = 201711 then unit_count end) as "Nov Count",
  sum(case when year_month = 201712 then total_gross_pay end) as "Dec Paid",
  sum(case when year_month = 201712 then unit_count end) as "Dec Count",  
  sum(case when year_month = 201801 then total_gross_pay end) as "Jan Paid",
  sum(case when year_month = 201801 then unit_count end) as "Jan Count" 
from (
select aa.year_month, 
  bb.store_code, bb.last_name, bb.first_name, bb.employee_number, bb.total_gross_pay,
  cc.unit_count
from (
  select distinct year_month
  from dds.dim_date
  where year_month between 201707 and 201801) aa
left join (
  select a.store_code, a.last_name, a.first_name, a.employee_number, 
    b.payroll_ending_year, b.payroll_ending_month, sum(b.total_gross_pay) as total_gross_pay,
    100*(2000 + b.payroll_ending_year) + b.payroll_ending_month as year_month
  from sls.personnel a
  inner join arkona.ext_pyhshdta b on a.employee_number = b.employee_
  where (
    (b.payroll_ending_year = 17 and b.payroll_ending_month between 7 and 12)
    or
    (b.payroll_ending_year = 18 and b.payroll_ending_month = 1))
  group by a.store_code, a.last_name, a.first_name, a.employee_number, 
    b.payroll_ending_year, b.payroll_ending_month) bb on aa.year_month = bb.year_month 
left join monthly_count cc on aa.year_month = cc.year_month and bb.employee_number = cc.psc_employee_number    
where aa.year_month between 201707 and 201801    
  and cc.year_month is not null) d
group by store_code, last_name, first_name, employee_number  


  
drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select year_month, store, page, line, line_label, control, sum(unit_count) as unit_count
from (
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201707 and 201801 --------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select distinct f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month between 201707 and 201801 -------------------------------------------------------------------
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
--         or
--         (b.page = 17 and b.line between 1 and 20))-- f/i
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by year_month, store, page, line, line_label, control, year_month
order by store, page, line;

-- checked counts 201707 & 201801 both stores good
select *, sum(unit_count) over (partition by year_month, store, page, line)
from step_1  
order by unit_count

select * from sls.deals_by_month where stock_number = '32055A'

-- "missing" deals are intra market wholesale, no consultant anyway
select a.*, b.psc_employee_number, b.ssc_employee_number
from (
  select year_month, store, control
  from step_1
  group by year_month, store, control
  having sum(unit_count) = 1) a
  left join sls.deals_by_month b on a.year_month = b.year_month
    and a.control = b.stock_number
where b.stock_number is null 


select * from monthly_count

drop table if exists monthly_count;
create temp table monthly_count as
select year_month, psc_employee_number, sum(unit_count) as unit_count
from (
  select a.*, b.psc_employee_number, 1 as unit_count
  from (
    select year_month, store, control
    from step_1
    group by year_month, store, control
    having sum(unit_count) = 1) a
  left join sls.deals_by_month b on a.year_month = b.year_month
    and a.control = b.stock_number
  where psc_employee_number is not null  
    and psc_employee_number <> 'HSE'
    and ssc_employee_number = 'none'
  union all
  select a.*, b.psc_employee_number, .5 as unit_count
  from (
    select year_month, store, control
    from step_1
    group by year_month, store, control
    having sum(unit_count) = 1) a
  left join sls.deals_by_month b on a.year_month = b.year_month
    and a.control = b.stock_number
  where psc_employee_number is not null  
    and psc_employee_number <> 'HSE'
    and ssc_employee_number <> 'none'  
  union all
  select a.*, b.ssc_employee_number, .5 as unit_count
  from (
    select year_month, store, control
    from step_1
    group by year_month, store, control
    having sum(unit_count) = 1) a
  left join sls.deals_by_month b on a.year_month = b.year_month
    and a.control = b.stock_number
  where psc_employee_number is not null  
    and psc_employee_number <> 'HSE'
    and ssc_employee_number <> 'none') c
group by year_month, psc_employee_number