﻿

select b.the_date, c.employeenumber, c.name, c.technumber, a.ro, a.line, a.flaghours, d.opcode, d.description
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.finalclosedatekey = b.date_key
  and b.year_month = 201804
inner join ads.ext_dim_tech c on a.techkey = c.techkey  
  and c.technumber in ('627', '574','577','608','614','587','637','528',
    '575','636','593','642','511','285')
inner join ads.ext_dim_opcode d on a.opcodekey = d.opcodekey
  and d.opcode <> 'SHOP'  
where a.flaghours <> 0
  and a.storecode = 'RY1'



select *
from ads.ext_dim_tech
where currentrow = true
  and active = true
  and name ~~ ANY('{%thompson%,%kief%,%waldbau%,%kief%,%winkler%,%strandell%,%anderson%,%holw%,%ferry%,%gehrtz%,%schwan%,%bear%,%schup%,%heffern%,%gray%,%neil%}')



select name, technumber, employeenumber,
  feb_flag_hours, feb_clock_hours, case when feb_clock_hours = 0 then 0 else round(100.0 * feb_flag_hours/feb_clock_hours, 2) end as feb_prof,
  mar_flag_hours, mar_clock_hours, case when mar_clock_hours = 0 then 0 else round(100.0 * mar_flag_hours/mar_clock_hours, 2) end as mar_prof,
  apr_flag_hours, apr_clock_hours, case when apr_clock_hours = 0 then 0 else round(100.0 * apr_flag_hours/apr_clock_hours, 2) end as apr_prof,
--   round(100 * three_month_flag_hours/three_month_clock_hours, 2) as "3_month_avg",
  may_flag_hours, may_clock_hours, case when may_clock_hours = 0 then 0 else round(100.0 * may_flag_hours/may_clock_hours, 2) end as may_prof,
  jun_flag_hours, jun_clock_hours, case when jun_clock_hours = 0 then 0 else round(100.0 * jun_flag_hours/jun_clock_hours, 2) end as jun_prof,
  jul_flag_hours, jul_clock_hours, case when jul_clock_hours = 0 then 0 else round(100.0 * jul_flag_hours/jul_clock_hours, 2) end as jul_prof,
  round(100 * three_month_flag_hours/three_month_clock_hours, 2) as "3_month_avg"
--   
--   jul_flag_hours, jul_clock_hours, case when jul_clock_hours = 0 then 0 else round(100.0 * jul_flag_hours/jul_clock_hours, 2) end as jul_prof
from (
  select c.employeenumber, c.name, c.technumber, 
    coalesce(sum(flaghours) filter (where b.year_month = 201802), 0) as feb_flag_hours,
    coalesce(sum(flaghours) filter (where b.year_month = 201803), 0) as mar_flag_hours,
    coalesce(sum(flaghours) filter (where b.year_month = 201804), 0) as apr_flag_hours,
    coalesce(sum(flaghours) filter (where b.year_month = 201805), 0) as may_flag_hours,
    coalesce(sum(flaghours) filter (where b.year_month = 201806), 0) as jun_flag_hours,
    coalesce(sum(flaghours) filter (where b.year_month = 201807), 0) as jul_flag_hours,
    coalesce(sum(flaghours) filter (where b.year_month between 201804 and 201807), 0) as three_month_flag_hours
  from ads.ext_fact_repair_order a
  inner join dds.dim_date b on a.finalclosedatekey = b.date_key
    and b.year_month > 201801
  inner join ads.ext_dim_tech c on a.techkey = c.techkey  
    and c.technumber in ('627', '574','577','608','614','587','637','528',
      '575','636','593','642','511','285','640')
  inner join ads.ext_dim_opcode d on a.opcodekey = d.opcodekey
    and d.opcode <> 'SHOP'  
  where a.flaghours <> 0
    and a.storecode = 'RY1'
  group by c.employeenumber, c.name, c.technumber) x  
left join (
  select a.employee_number, 
    coalesce(sum(clock_hours) filter (where b.year_month = 201802), 0) as feb_clock_hours,
    coalesce(sum(clock_hours) filter (where b.year_month = 201803), 0) as mar_clock_hours,
    coalesce(sum(clock_hours) filter (where b.year_month = 201804), 0) as apr_clock_hours,
    coalesce(sum(clock_hours) filter (where b.year_month = 201805), 0) as may_clock_hours,
    coalesce(sum(clock_hours) filter (where b.year_month = 201806), 0) as jun_clock_hours,
    coalesce(sum(clock_hours) filter (where b.year_month = 201807), 0) as jul_clock_hours,
    coalesce(sum(clock_hours) filter (where b.year_month between 201804 and 201807), 0) as three_month_clock_hours
  from arkona.xfm_pypclockin a
  inner join dds.dim_date b on a.the_date = b.the_date
    and b.year_month > 201801
  where a.employee_number in ('167580', '111210','164015','1146989','178050','1149300','1106421','1137590',
      '152410','1133500','150126','1124400','1124436','159863','13725')
  group by a.employee_number) y on x.employeenumber = y.employee_number
order by name


select *

