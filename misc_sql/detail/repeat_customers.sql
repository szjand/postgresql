﻿select *
from ads.ext_dim_opcode
where opcode in ('TOT','SHO','INT')

-- multiple lines with same opcode, so group
drop table if exists wtf1;
create temp table wtf1 as
select a.ro, b.the_date, e.vin, c.opcode, d.fullname, d.bnkey, d.homephone, d.cellphone
--   case when emailvalid then email else null end as email,
--   case when email2valid then email2 else null end as email2
-- select *  
from ads.ext_fact_repair_order a
join dds.dim_date b on a.opendatekey = b.date_key
  and b.the_year > 2015
join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
  and c.opcode in ('TOT','SHO','INT')
join ads.ext_dim_customer d on a.customerkey = d.customerkey  
join ads.ext_dim_vehicle e on a.vehiclekey = e.vehiclekey
group by a.ro, b.the_date, c.opcode, d.fullname, d.homephone, d.cellphone, e.vin, d.bnkey
-- order by d.fullname 
order by e.vin

select *
from ads.ext_dim_customer
limit 10


select a.vin, a.fullname, a.homephone, array_agg(distinct the_date order by the_date)
from wtf1 a
join (
  select vin
  from wtf1
  group by vin 
  having count(*) > 1) b on a.vin = b.vin
group by a.vin, a.fullname, a.homephone  
order by vin

-- clean up phone
drop table if exists wtf2;
create temp table wtf2 as
select * 
from (
  select vin, fullname, bnkey,
    case
      when homephone = '0' then
        case
          when cellphone <> '0' then cellphone
        end
      else homephone
    end as phone, the_date
  from wtf1) a
where phone is not null  
  and vin is not null
  and left(vin,3) <> '000'
  and length(vin) = 17

drop table if exists wtf3;
create temp table wtf3 as
select a.vin, a.fullname, a.bnkey, a.phone, array_agg(distinct the_Date order by the_date) as dates
from wtf2 a
join (
  select vin
  from wtf2
  group by vin
  having count(*) > 1) b on a.vin = b.vin
group by a.vin, a.fullname, a.bnkey, a.phone;


select a.vin, a.fullname, a.phone, a.dates
from wtf3 a
join arkona.ext_bopvref b on a.vin = b.vin 
  and a.bnkey = b.customer_key
  and end_date = 0
where cardinality(dates) > 1
  and a.vin not in ('1GNKRHKD7FJ100796','1GYS4KKJ7GR213140')
group by a.vin, a.fullname, a.phone, a.dates
order by fullname


'1C6RR7LT2ES240648'
'1G1ZJ57749F103137'
'1G1ZT61836F248649'
'1G4HR57Y87U204951'
'1GC1KWE84FF163742'
'1GC4KZCG0FF173919'
'1GCRKSE79DZ357486'


select a.vin, a.fullname, a.bnkey, a.phone, string_agg(the_date::text, ',' order by the_date)
from wtf2 a
join (
  select vin
  from wtf2
  group by vin
  having count(*) > 1) b on a.vin = b.vin
group by a.vin, a.fullname, a.bnkey, a.phone;