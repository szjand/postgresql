﻿-- 2382
-- nc vehicles only missing 1 vehicle, good enough
-- missing 11 from nc.vehicle_sales
drop table if exists wtf;
create temp table wtf as
select a.record_key, bopmast_stock_number, bopmast_vin, date_capped, 
  case
    when b.make = 'honda' then 'honda'
    when b.make = 'nissan' then 'nissan'
    else 'GM'
  end as make, d.year_month, f.ground_date, a.date_capped - f.ground_date as age
from arkona.xfm_bopmast a
join nc.vehicles b on a.bopmast_vin = b.vin
join dds.dim_date d on a.date_capped = d.the_date
join nc.vehicle_sales e on a.record_key = e.bopmast_id and a.bopmast_vin = e.vin 
join nc.vehicle_acquisitions f on b.vin = f.vin and e.ground_date = f.ground_date
where a.current_row
  and a.bopmast_company_number in ('RY1','RY2')
  and a.record_Status = 'U'
  and a.vehicle_type = 'N'
  and a.sale_type <> 'W'
  and a.date_capped between '01/01/2019' and '12/31/2019'
 

select year_month, make, count(*) as total,
  round(100.0*(count(make) filter (where age < 31))/count(*), 1) as "% 0-30",
  round(100.0*(count(make) filter (where age between 31 and 60))/count(*), 1) as "% 31-60",
  round(100.0*(count(make) filter (where age between 61 and 90))/count(*), 1) as "%61-90",
  round(100.0*(count(make) filter (where age between 91 and 120))/count(*), 1) as "% 91-120",
  round(100.0*(count(make) filter (where age > 120))/count(*), 1) as "% 120+"

  
--   count(make) filter (where age between 31 and 60) as "31-60",
--   count(make) filter (where age between 61 and 90) as "61-90",
--   count(make) filter (where age between 91 and 120) as "91-120",
--   count(make) filter (where age > 120) as "120+"
from wtf
group by make, year_month
order by make, year_month


select 52 + 17.6 + 13.7 + 4.9 + 11.8