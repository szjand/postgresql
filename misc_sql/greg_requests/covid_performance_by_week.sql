﻿/*
03/23/2020
The Rydell Company is looking for stores to report business activity.

Can you produce a spreadsheet that starts with the first week in March that shows weekly:

New Car Sales(retail)
Used Car Sales 
Service Hours
Body Shop Hours
Oil Changes (PDQ)

Thanks

03/23
Thanks can you do one for Honda too please?

------------------------------------------------
-- 3/24 gm shop total for previous weeks is lower today than it was yesterday
-- yesterday agreed with vision, today they do not

the issue is zach winkler
in fs.personnel he was deleted on 3/23/2020
select *
from fs.personnel 
where full_name like 'winkler%'
*/
-- dates
drop table if exists dates cascade;
create temp table dates as
select sunday_to_saturday_week_select_format, sunday_to_saturday_week, min(the_date) as from_Date, 
  case
    when max(the_date)  < min(the_date) + 6 then min(the_date) + 6
    else max(the_date)
  end as thru_date
from dds.dim_date 
where the_date between '03/01/2020' and current_date
group by sunday_to_saturday_week_select_format, sunday_to_saturday_week
order by sunday_to_saturday_week;
create unique index on dates(sunday_to_saturday_week);
create index on dates(from_date);
create index on dates(thru_date);

-- deals: bopmast
select a.sunday_to_saturday_week, a.sunday_to_saturday_week_select_format, 
  count(b.vehicle_type) filter (where b.vehicle_type = 'N') as new_sales,
  count(b.vehicle_type) filter (where b.vehicle_type = 'U') as used_sales
from dates a
left join (
  select bopmast_company_number, vehicle_type, sale_type, date_capped
  from arkona.xfm_bopmast
  where current_row
    and date_capped >= '03/01/2020'
    and sale_type <> 'W'
    and bopmast_company_number = 'RY1') b on b.date_capped between a.from_date and a.thru_date
group by a.sunday_to_saturday_week, a.sunday_to_saturday_week_select_format
order by a.sunday_to_saturday_week;

-- service hours: full shop
-- body shop: fs metal techs only ?
select a.sunday_to_saturday_week, a.sunday_to_saturday_week_select_format, 
  sum(c.flag_hours) filter (where c.job = 'Main Shop Tech') as service_hours,
  sum(c.flag_hours) filter (where c.job = 'Metal Tech') as body_shop_hours
from dates a
left join (
  select a.the_date, c.job, sum(flag_hours) as flag_hours
  from fs.tech_flag_hours_by_day a
  join fs.personnel b on a.personnel_id = b.personnel_id
    and not b.is_deleted
  join fs.personnel_jobs c on a.personnel_id = c.personnel_id  
    and c.store = 'RY1'
    and c.job in ('Metal Tech', 'Main Shop Tech')  
  group by a.the_date, c.job) c on c.the_date between a.from_date and a.thru_date
group by a.sunday_to_saturday_week, a.sunday_to_saturday_week_select_format
order by a.sunday_to_saturday_week; 

-- lof; pdq
select a.sunday_to_saturday_week, a.sunday_to_saturday_week_select_format, count(*)
from dates a
left join pdq.ros b on b.the_date between a.from_date and a.thru_date
  and category = 'LOF'
  and store = 'RY1'
group by a.sunday_to_saturday_week, a.sunday_to_saturday_week_select_format
order by a.sunday_to_saturday_week;


-- all together
select 'GM' as store, aa.sunday_to_saturday_week, aa.sunday_to_saturday_week_select_format, aa.new_sales, aa.used_sales, 
  bb.service_hours, bb.body_shop_hours, cc.lofs
from (
  select a.sunday_to_saturday_week, a.sunday_to_saturday_week_select_format, 
    count(b.vehicle_type) filter (where b.vehicle_type = 'N') as new_sales,
    count(b.vehicle_type) filter (where b.vehicle_type = 'U') as used_sales
  from dates a
  left join (
    select bopmast_company_number, vehicle_type, sale_type, date_capped
    from arkona.xfm_bopmast
    where current_row
      and date_capped >= '03/01/2020'
      and sale_type <> 'W'
      and bopmast_company_number = 'RY1') b on b.date_capped between a.from_date and a.thru_date
  group by a.sunday_to_saturday_week, a.sunday_to_saturday_week_select_format) aa
left join (
  select a.sunday_to_saturday_week, a.sunday_to_saturday_week_select_format, 
    sum(c.flag_hours) filter (where c.job = 'Main Shop Tech') as service_hours,
    sum(c.flag_hours) filter (where c.job = 'Metal Tech') as body_shop_hours
  from dates a
  left join (
    select a.the_date, c.job, sum(flag_hours) as flag_hours
    from fs.tech_flag_hours_by_day a
    join fs.personnel b on a.personnel_id = b.personnel_id
--       and not b.is_deleted
    join fs.personnel_jobs c on a.personnel_id = c.personnel_id  
      and c.store = 'RY1'
      and c.job in ('Metal Tech', 'Main Shop Tech')  
    group by a.the_date, c.job) c on c.the_date between a.from_date and a.thru_date
  group by a.sunday_to_saturday_week, a.sunday_to_saturday_week_select_format
  order by a.sunday_to_saturday_week) bb on aa.sunday_to_saturday_week_select_format = bb.sunday_to_saturday_week_select_format
left join (
  select a.sunday_to_saturday_week, a.sunday_to_saturday_week_select_format, count(*) as lofs
  from dates a
  left join pdq.ros b on b.the_date between a.from_date and a.thru_date
    and category = 'LOF'
    and store = 'RY1'
  group by a.sunday_to_saturday_week, a.sunday_to_saturday_week_select_format) cc on aa.sunday_to_saturday_week_select_format = cc.sunday_to_saturday_week_select_format
-- order by aa.sunday_to_saturday_week  
union all
select 'Honda/Nissan' as store, aa.sunday_to_saturday_week, aa.sunday_to_saturday_week_select_format, aa.new_sales, aa.used_sales, 
  bb.service_hours, bb.body_shop_hours, cc.lofs
from (
  select a.sunday_to_saturday_week, a.sunday_to_saturday_week_select_format, 
    count(b.vehicle_type) filter (where b.vehicle_type = 'N') as new_sales,
    count(b.vehicle_type) filter (where b.vehicle_type = 'U') as used_sales
  from dates a
  left join (
    select bopmast_company_number, vehicle_type, sale_type, date_capped
    from arkona.xfm_bopmast
    where current_row
      and date_capped >= '03/01/2020'
      and sale_type <> 'W'
      and bopmast_company_number = 'RY2') b on b.date_capped between a.from_date and a.thru_date
  group by a.sunday_to_saturday_week, a.sunday_to_saturday_week_select_format) aa
left join (
  select a.sunday_to_saturday_week, a.sunday_to_saturday_week_select_format, 
    sum(c.flag_hours) filter (where c.job = 'Main Shop Tech') as service_hours,
    sum(c.flag_hours) filter (where c.job = 'Metal Tech') as body_shop_hours
  from dates a
  left join (
    select a.the_date, c.job, sum(flag_hours) as flag_hours
    from fs.tech_flag_hours_by_day a
    join fs.personnel b on a.personnel_id = b.personnel_id
--       and not b.is_deleted
    join fs.personnel_jobs c on a.personnel_id = c.personnel_id  
      and c.store = 'RY2'
      and c.job in ('Metal Tech', 'Main Shop Tech')  
    group by a.the_date, c.job) c on c.the_date between a.from_date and a.thru_date
  group by a.sunday_to_saturday_week, a.sunday_to_saturday_week_select_format
  order by a.sunday_to_saturday_week) bb on aa.sunday_to_saturday_week_select_format = bb.sunday_to_saturday_week_select_format
left join (
  select a.sunday_to_saturday_week, a.sunday_to_saturday_week_select_format, count(*) as lofs
  from dates a
  left join pdq.ros b on b.the_date between a.from_date and a.thru_date
    and category = 'LOF'
    and store = 'RY2'
  group by a.sunday_to_saturday_week, a.sunday_to_saturday_week_select_format) cc on aa.sunday_to_saturday_week_select_format = cc.sunday_to_saturday_week_select_format
order by store, sunday_to_saturday_week  


