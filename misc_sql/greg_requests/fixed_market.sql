﻿select *
from arkona.ext_sdprhdr
limit 100


select min(open_date), max(open_date)
from arkona.ext_sdprhdr

select min(the_date), max(the_Date) 
from pdq.ros
limit 10



select 
  sum(case when e.account_type = 'Sale' then amount else 0 end) as sales,
  sum(case when e.account_type in ('cogs','expense') then amount else 0 end) as cogs
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202003
  and (
    (b.page = 16 and b.line between 20 and 42))
--     or
--     (b.page = 4 and b.line = 59)) -- parts split
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.department = 'service'
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key  
inner join fin.dim_account e on d.gl_account = e.account

select * from fin.dim_fs_org


select distinct b.line, b.col, d.gl_account, e.account_type, e.department, dd.description, f.store, f.sub_department, a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202003
  and b.page = 16
  and b.line between 20 and 42
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
join fin.dim_account dd on d.gl_account = dd.account
inner join fin.dim_account e on d.gl_account = e.account
join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key

select f.store, b.line, b.col, e.department, sum(a.amount) --d.gl_account, e.account_type, e.department, dd.description, f.store, f.sub_department, sum(a.amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202003
  and b.page = 16
  and b.line between 20 and 42
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
join fin.dim_account dd on d.gl_account = dd.account
inner join fin.dim_account e on d.gl_account = e.account
join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
where e.account_type = 'sale' and b.col = 1
group by f.store, b.line, b.col, e.department -- , d.gl_account, e.account_type, e.department, dd.description, f.store, f.sub_department


select * 
from fin.dim_fs
where year_month = 202003
  and page = 16
  and line between 20 and 42

select * 
from fin.fact_fs
limit 10  


-- this gives me accurate year end totals
select c.store, c.department, line, col, sum(amount)
--   sum(case when b.page = 16 then amount end) as gross,
--   sum(case when b.page = 4 then amount end) as parts_split,
--   sum(amount) as total
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.the_year = 2019 -- b.year_month = 202003
  and (
    (b.page = 16 and b.line between 20 and 42))
--     or
--     (b.page = 4 and b.line = 59)) -- parts split
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.area = 'fixed'
group by c.store, c.department, line, col
having sum(amount) <> 0
order by c.store,line, col

-- can i match the counts from this

-- this gives me accurate line totals for 202003
select c.store, c.department, line, col, sum(amount), array_agg(d.gl_account)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202003
  and (
    (b.page = 16 and b.line between 20 and 42))
--     or
--     (b.page = 4 and b.line = 59)) -- parts split
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.area = 'fixed'
join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key 
group by c.store, c.department, line, col
having sum(amount) <> 0
order by c.store,line, col



select a.*
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 202003
join fin.dim_account c on a.account_key = c.account_key
  and c.account = '146001'
--   and c.account in ('146020','146024D','146000','146000D','146027','146007')  
where a.post_status = 'Y'  
order by a.control


line 21, this gives me 411626 for sales instead of 403802 a difference of 7824
the issue is DISCOUNTS, they are a cogs account, but are routed to column 1, 146000D and 146024D
select a.*, b.line, b.col, b.line_label, c.gl_account, d.store, d.sub_department, e.account_type, e.description, a.amount,
--   sum(- a.amount) filter (where e.account_type = 'sale') over (partition by d.store, b.line) as sales,
--   sum(- a.amount) filter (where e.account_type = 'cogs') over (partition by d.store, b.line) as cogs,
  sum(- a.amount) filter (where b.col = 1) over (partition by d.store, b.line) as sales,
  sum(- a.amount) filter (where b.col = 3) over (partition by d.store, b.line) as cogs,  
  sum(- a.amount) over (partition by d.store, b.line) as gross
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202003
  and b.page = 16
  and b.line 
join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  and c.current_row
join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  and d.area = 'fixed'
join fin.dim_account e on c.gl_account = e.account 
  and e.current_row
order by gl_account




-- this gives me accurate line totals for 202003
select c.store, c.department, c.sub_department, line, col, sum(amount), array_agg(d.gl_account order by d.gl_account)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202003
  and (
    (b.page = 16 and b.line between 20 and 42))
--     or
--     (b.page = 4 and b.line = 59)) -- parts split
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.area = 'fixed'
join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key 
group by c.store, c.department, c.sub_department, line, col
having sum(amount) <> 0
order by c.store,department,sub_department,line, col


-- this gives me accurate year end totals and the relevant lines (excluding internal)
select c.store, c.department, line, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.the_year = 2019 -- b.year_month = 202003
  and b.page = 16 and ((b.line between 20 and 27) or b.line between 35 and 37)
  and b.col in (1,2)
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.area = 'fixed'
group by c.store, c.department, line
having sum(amount) <> 0
order by c.store,line

-- this gives me accurate year end totals and the relevant lines (excluding internal)
-- and breaks out main shop, body shop & pdq
select c.store, c.department, c.sub_department, line, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.the_year = 2019 
  and b.page = 16 and ((b.line between 20 and 27) or (b.line between 35 and 37))
  and b.col in (1,2)
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.area = 'fixed'
  and c.sub_department not in ('car wash','detail')
group by c.store, c.department, c.sub_department, line
having sum(amount) <> 0
order by c.store,line

-- this gives me accurate year end totals and the relevant lines (excluding internal)
-- and breaks out main shop, body shop & pdq and all the accounts
select c.store, c.department, c.sub_department, line, sum(amount), array_agg(distinct d.gl_account order by d.gl_account)
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.the_year = 2019 
  and b.page = 16 and ((b.line between 20 and 27) or b.line between 35 and 37)
  and b.col in (1,2)
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.area = 'fixed'
  and c.sub_department not in ('car wash','detail')
join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key  
group by c.store, c.department, c.sub_department, line
order by c.store, c.department, c.sub_department, line


-- accounts
drop table if exists accounts;
create temp table accounts as
select store, department, sub_department, unnest(accounts) as account
from (
  select c.store, c.department, c.sub_department, array_agg(distinct d.gl_account order by d.gl_account) as accounts
  from fin.fact_fs a
  join fin.dim_fs b on a.fs_key = b.fs_key
    and b.the_year = 2019 
    and b.page = 16 and ((b.line between 20 and 27) or b.line between 35 and 37)
    and b.col in (1,2)
  join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
    and c.area = 'fixed'
    and c.sub_department not in ('car wash','detail')
  join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key  
  group by c.store, c.department, c.sub_department) aa;

-- ros
drop table if exists ros;
create temp table ros as
select a.control, d.store, d.department, d.sub_department, sum(a.amount)
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.the_year = 2019
join fin.dim_account c on a.account_key = c.account_key
join accounts d on c.account = d.account
group by a.control, d.store, d.department, d.sub_department;

drop table if exists ro_zip;
create temp table ro_zip as
select a.ro, b.fullname, b.zip
from ads.ext_fact_repair_order a
join ros aa on a.ro = aa.control
join ads.ext_dim_customer b on a.customerkey = b.customerkey
where zip is not null
group by a.ro, b.fullname, b.zip;


select a.control
from ros a
join ro_zip b on a.control = b.ro
group by a.control having count(*) > 1

-- how do you want to deal with ros for multiple departments
select *
from ros a
join ro_zip b on a.control = b.ro
where control = '16343374'


select count(*) from ros;  -- 74523
select count(*) from ro_zip; -- 64432

select * -- 3353, mostly INVENTORY
from ros a
left join ro_zip b on a.control = b.ro
where b.ro is null


select count(*) --71170
from ros a
join ro_zip b on a.control = b.ro

-- department zip code count
select a.store, a.department, a.sub_department, b.zip, count(*)
from ros a
join ro_zip b on a.control = b.ro
group by a.store, a.department, a.sub_department, b.zip
having count(*) > 10
order by a.store, a.department, a.sub_department asc, count(*) desc 

-- total department labor sales
select c.store, c.department, c.sub_department, sum(amount), array_agg(distinct d.gl_account order by d.gl_account)
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.the_year = 2019 
  and b.page = 16 and ((b.line between 20 and 27) or b.line between 35 and 37)
  and b.col in (1,2)
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.area = 'fixed'
  and c.sub_department not in ('car wash','detail')
join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key  
group by c.store, c.department, c.sub_department


------------------------------------------------------------
-- this is it
------------------------------------------------------------
-- accounts
drop table if exists accounts;
create temp table accounts as
select store, department, sub_department, unnest(accounts) as account
from (
  select c.store, c.department, c.sub_department, array_agg(distinct d.gl_account order by d.gl_account) as accounts
  from fin.fact_fs a
  join fin.dim_fs b on a.fs_key = b.fs_key
    and b.the_year = 2019 
    and b.page = 16 and ((b.line between 20 and 27) or (b.line between 35 and 37))
    and b.col in (1,2)
  join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
    and c.area = 'fixed'
    and c.sub_department not in ('car wash','detail')
  join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key  
  group by c.store, c.department, c.sub_department) aa;


drop table if exists ros;
create temp table ros as
select a.control, d.store, d.department, d.sub_department, sum(-a.amount) as sales
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.the_year = 2019
join fin.dim_account c on a.account_key = c.account_key
join accounts d on c.account = d.account
group by a.control, d.store, d.department, d.sub_department;

drop table if exists ro_zip;
create temp table ro_zip as
select a.ro, b.fullname, b.zip
from ads.ext_fact_repair_order a
join ros aa on a.ro = aa.control
join ads.ext_dim_customer b on a.customerkey = b.customerkey
where zip is not null
group by a.ro, b.fullname, b.zip;

-- this is it
-- v1 spreadsheet
select a.store, a.department, a.sub_department, b.zip, count(*), sum(a.sales)::integer
from ros a
join ro_zip b on a.control = b.ro
group by a.store, a.department, a.sub_department, b.zip
having count(*) > 10
order by a.store, a.department, a.sub_department asc, count(*) desc 
-------------------------------------------
--
--  fuck i don't know if he wants labor or parts and labor, start with the assumption of labor
--  4/13 nope, he wants parts included:Keep it as a total sales amount without split
-------------------------------------------

----------------------------------------------------------------------------------------
--< v2
----------------------------------------------------------------------------------------
-- accounts
drop table if exists accounts;
create temp table accounts as
select store, department, sub_department, unnest(accounts) as account
from (
  select c.store, c.department, c.sub_department, array_agg(distinct d.gl_account order by d.gl_account) as accounts
  from fin.fact_fs a
  join fin.dim_fs b on a.fs_key = b.fs_key
    and b.the_year = 2019 
    and b.page = 16 and ((b.line between 20 and 27) or (b.line between 35 and 37))
    and b.col in (1,2)
  join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
    and c.area = 'fixed'
    and c.sub_department not in ('car wash','detail')
  join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key  
  group by c.store, c.department, c.sub_department) aa;

-- ro numbers (fact_gl.control) and labor sales
drop table if exists ros;
create temp table ros as
select a.control, d.store, d.department, d.sub_department, sum(-a.amount) as labor_sales
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.the_year = 2019
join fin.dim_account c on a.account_key = c.account_key
join accounts d on c.account = d.account
group by a.control, d.store, d.department, d.sub_department;



-- get the zip code for each ro
drop table if exists ro_zip;
create temp table ro_zip as
select a.ro, b.fullname, b.zip
from ads.ext_fact_repair_order a
join ros aa on a.ro = aa.control
join ads.ext_dim_customer b on a.customerkey = b.customerkey
where zip is not null
group by a.ro, b.fullname, b.zip;

-- added line 57 (tires) and assumed mechanical (no reasonable way to differentiate RY2)
drop table if exists parts_accounts;
create table parts_accounts as
select store, 
  case 
    when line in (46.0, 47.0, 57.0) then 'service'
    when line = 48.0 then 'service'
    when line = 49.0 then 'body shop'
    when line = 45.0 then 'warranty'
  end as department,
  case 
    when line in (46.0, 47.0, 57.0) then 'mechanical'
    when line = 48.0 then 'pdq'
    when line = 49.0 then 'none'
    when line = 45.0 then 'warranty'
  end as sub_department, 
  accounts as account
from (  
  select c.store, line, unnest(array_agg(distinct d.gl_account order by d.gl_account)) as accounts
  from fin.fact_fs a
  join fin.dim_fs b on a.fs_key = b.fs_key
    and b.the_year = 2019 
    and b.page = 16 and b.line in (45,46,47,48,49,57)
    and b.col in (1,2)
  join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
    and c.area = 'fixed'
    and c.sub_department not in ('car wash','detail')
  join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key  
  group by c.store, line) aa


drop table if exists all_sales;
create temp table all_sales as
select aa.*, coalesce(bb.parts_sales, 0) as parts_sales
from ros aa
left join ( -- parts sales
  select a.control, d.store, d.department, d.sub_department, sum(-b.amount) as parts_sales
  from ros a
  join fin.fact_gl b on a.control = b.control
  join fin.dim_account c on b.account_key = c.account_key
  join parts_accounts d on c.account = d.account
    and a.store = d.store
    and a.sub_department =
      case 
        when d.sub_department = 'warranty' and d.store = 'ry1' and left(a.control, 2) = '16' then 'mechanical'
        when d.sub_department = 'warranty' and d.store = 'ry1' and left(a.control, 2) = '19' then 'pdq'
        when d.sub_department = 'warranty' and d.store = 'ry2' and a.labor_sales < 20 then 'pdq'
        when d.sub_department = 'warranty' and d.store = 'ry2' and a.labor_sales >= 20 then 'mechanical'
        else d.sub_department
      end
  group by a.control, d.store, d.department, d.sub_department) bb on aa.control = bb.control
  and aa.store = bb.store
  and aa.department = bb.department
  and aa.sub_department = bb.sub_department  

-- v2 spreadsheet
select a.store, a.department, a.sub_department, b.zip, count(*), sum(a.labor_sales + a.parts_sales)::integer
from all_sales a
join ro_zip b on a.control = b.ro
group by a.store, a.department, a.sub_department, b.zip
having count(*) > 10
order by a.store, a.department, a.sub_department asc, count(*) desc 
----------------------------------------------------------------------------------------
--/> v2
----------------------------------------------------------------------------------------
-- -- ros with lines for multiple departments
-- -- need to do parts to match departments
-- 
-- 16336328 is a good test case has mech/pdq/bs labor and parts
-- drop table if exists multiple_lines;
-- create temp table multiple_lines as
-- select a.*
-- from ros a
-- join (
--   select control
--   from ros
--   group by control
--   having count(*) > 1) b on a.control = b.control
-- 
-- 45 : warranty no way to tell the diffence with honda gm: use the ro number, 16 = mech, 19 = pdq
--         with honda < $20 = pdq > $20 = medh
-- 46 : mech
-- 47 : mech
-- 48 : pdq
-- 49 : bs
-- 
-- service pdq
-- service mechanical
-- body shop none
-- 
-- -- added line 57 (tires) and assumed mechanical (no reasonable way to differentiate RY2)
-- drop table if exists parts_accounts;
-- create table parts_accounts as
-- select store, 
--   case 
--     when line in (46.0, 47.0, 57.0) then 'service'
--     when line = 48.0 then 'service'
--     when line = 49.0 then 'body shop'
--     when line = 45.0 then 'warranty'
--   end as department,
--   case 
--     when line in (46.0, 47.0, 57.0) then 'mechanical'
--     when line = 48.0 then 'pdq'
--     when line = 49.0 then 'none'
--     when line = 45.0 then 'warranty'
--   end as sub_department, 
--   accounts as account
-- from (  
--   select c.store, line, unnest(array_agg(distinct d.gl_account order by d.gl_account)) as accounts
--   from fin.fact_fs a
--   join fin.dim_fs b on a.fs_key = b.fs_key
--     and b.the_year = 2019 
--     and b.page = 16 and b.line in (45,46,47,48,49,57)
--     and b.col in (1,2)
--   join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
--     and c.area = 'fixed'
--     and c.sub_department not in ('car wash','detail')
--   join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key  
--   group by c.store, line) aa
-- 
-- 
-- select *
-- from ros a
-- where control = '16336328'
-- 
-- -- ok, this is close, but there are multiple parts lines, so the labor gets listed twice
-- -- need to just return parts half, grouped
-- select a.*, d.*, b.amount
-- from ros a
-- join fin.fact_gl b on a.control = b.control
-- join fin.dim_account c on b.account_key = c.account_key
-- join parts_accounts d on c.account = d.account
--   and a.store = d.store
--   and a.sub_department =
--     case 
--       when d.sub_department = 'warranty' and d.store = 'ry1' and left(a.control, 2) = '16' then 'mechanical'
--       when d.sub_department = 'warranty' and d.store = 'ry1' and left(a.control, 2) = '19' then 'pdq'
--       when d.sub_department = 'warranty' and d.store = 'ry2' and a.labor_sales < 20 then 'pdq'
--       when d.sub_department = 'warranty' and d.store = 'ry2' and a.labor_sales >= 20 then 'mechanical'
--       else d.sub_department
--     end
-- where a.control = '16336328'
-- 
-- -- this is correct for this ro
-- select a.control, d.store, d.department, d.sub_department, sum(-b.amount) as parts_sales
-- from ros a
-- join fin.fact_gl b on a.control = b.control
-- join fin.dim_account c on b.account_key = c.account_key
-- join parts_accounts d on c.account = d.account
--   and a.store = d.store
--   and a.sub_department =
--     case 
--       when d.sub_department = 'warranty' and d.store = 'ry1' and left(a.control, 2) = '16' then 'mechanical'
--       when d.sub_department = 'warranty' and d.store = 'ry1' and left(a.control, 2) = '19' then 'pdq'
--       when d.sub_department = 'warranty' and d.store = 'ry2' and a.labor_sales < 20 then 'pdq'
--       when d.sub_department = 'warranty' and d.store = 'ry2' and a.labor_sales >= 20 then 'mechanical'
--       else d.sub_department
--     end
-- where a.control = '16336328'
-- group by a.control, d.store, d.department, d.sub_department
-- 
-- -- and works when matched with ros for the full picture (16336328, 16336223)
-- select *
-- from ros aa
-- left join ( -- parts sales
--   select a.control, d.store, d.department, d.sub_department, sum(-b.amount) as parts_sales
--   from ros a
--   join fin.fact_gl b on a.control = b.control
--   join fin.dim_account c on b.account_key = c.account_key
--   join parts_accounts d on c.account = d.account
--     and a.store = d.store
--     and a.sub_department =
--       case 
--         when d.sub_department = 'warranty' and d.store = 'ry1' and left(a.control, 2) = '16' then 'mechanical'
--         when d.sub_department = 'warranty' and d.store = 'ry1' and left(a.control, 2) = '19' then 'pdq'
--         when d.sub_department = 'warranty' and d.store = 'ry2' and a.labor_sales < 20 then 'pdq'
--         when d.sub_department = 'warranty' and d.store = 'ry2' and a.labor_sales >= 20 then 'mechanical'
--         else d.sub_department
--       end
--   where a.control = '16336328'
--   group by a.control, d.store, d.department, d.sub_department) bb on aa.control = bb.control
--   and aa.store = bb.store
--   and aa.department = bb.department
--   and aa.sub_department = bb.sub_department
-- where aa.control = '16336328'
-- 
-- -- now lets test it on multiple lines
-- -- 13518 rows, where each ro has multiple sub_departments
-- -- spot checks look ok
-- select aa.*, coalesce(bb.parts_sales, 0)
-- from multiple_lines aa
-- left join (
--   select a.control, d.store, d.department, d.sub_department, sum(-b.amount) as parts_sales
--   from multiple_lines a
--   join fin.fact_gl b on a.control = b.control
--   join fin.dim_account c on b.account_key = c.account_key
--   join parts_accounts d on c.account = d.account
--     and a.store = d.store
--     and a.sub_department =
--       case 
--         when d.sub_department = 'warranty' and d.store = 'ry1' and left(a.control, 2) = '16' then 'mechanical'
--         when d.sub_department = 'warranty' and d.store = 'ry1' and left(a.control, 2) = '19' then 'pdq'
--         when d.sub_department = 'warranty' and d.store = 'ry2' and a.labor_sales < 20 then 'pdq'
--         when d.sub_department = 'warranty' and d.store = 'ry2' and a.labor_sales >= 20 then 'mechanical'
--         else d.sub_department
--       end
--   group by a.control, d.store, d.department, d.sub_department) bb on aa.control = bb.control
--   and aa.store = bb.store
--   and aa.department = bb.department
--   and aa.sub_department = bb.sub_department
-- 
-- drop table if exists all_sales;
-- create temp table all_sales as
-- select aa.*, coalesce(bb.parts_sales, 0) as parts_sales
-- from ros aa
-- left join ( -- parts sales
--   select a.control, d.store, d.department, d.sub_department, sum(-b.amount) as parts_sales
--   from ros a
--   join fin.fact_gl b on a.control = b.control
--   join fin.dim_account c on b.account_key = c.account_key
--   join parts_accounts d on c.account = d.account
--     and a.store = d.store
--     and a.sub_department =
--       case 
--         when d.sub_department = 'warranty' and d.store = 'ry1' and left(a.control, 2) = '16' then 'mechanical'
--         when d.sub_department = 'warranty' and d.store = 'ry1' and left(a.control, 2) = '19' then 'pdq'
--         when d.sub_department = 'warranty' and d.store = 'ry2' and a.labor_sales < 20 then 'pdq'
--         when d.sub_department = 'warranty' and d.store = 'ry2' and a.labor_sales >= 20 then 'mechanical'
--         else d.sub_department
--       end
--   group by a.control, d.store, d.department, d.sub_department) bb on aa.control = bb.control
--   and aa.store = bb.store
--   and aa.department = bb.department
--   and aa.sub_department = bb.sub_department
-- 
-- select * 
-- from all_sales
-- where parts_Sales > 1000
-- order by control
-- 
-- select * from all_sales where control = '18067324'
-- 
-- select *
-- from all_sales a
-- join (
-- select control
-- from all_sales
-- group by control 
-- having count(*) = 3) b on a.control = b.control
-- order by a.control