﻿-- 5716
select a.bopmast_company_number, a.vehicle_type, a.buyer_number, a.bopmast_search_name
from arkona.xfm_bopmast a
where a.current_row 
  and a.record_status = 'U'
  and a.sale_type <> 'W'
  and extract(year from a.date_capped) = 2019
  and bopmast_company_number in ('RY1','RY2')

drop table if exists wtf;
create temp table wtf as  
select a.bopmast_company_number, a.vehicle_type, a.buyer_number, a.bopmast_search_name, b.bopname_search_name, b.bopname_record_key, b.zip_code, similarity(a.bopmast_search_name, b.bopname_search_name)
from arkona.xfm_bopmast a
left join arkona.xfm_bopname b on a.buyer_number = b.bopname_record_key
--   and a.bopmast_company_number = b.bopname_company_number
  and b.current_row
  and b.bopname_company_number in ('RY1','RY2')
where a.current_row 
  and a.record_status = 'U'
  and a.sale_type <> 'W'
  and extract(year from a.date_capped) = 2019
  and bopmast_company_number in ('RY1','RY2')
order by similarity(a.bopmast_search_name, b.bopname_search_name);


select bopmast_company_number, vehicle_type, zip_code, count(*)
from wtf a
where similarity > .39
group by bopmast_company_number, vehicle_type, zip_code
order by bopmast_company_number, vehicle_type, zip_code

-- sales_market
-- 07/23/20
-- for gregs pricing page, a map of our market by actual sales

drop table if exists wtf;
create temp table wtf as  
select a.bopmast_company_number, a.vehicle_type, a.buyer_number, a.bopmast_search_name, b.bopname_search_name, 
  b.bopname_record_key, b.zip_code, similarity(a.bopmast_search_name, b.bopname_search_name)
from arkona.xfm_bopmast a
left join arkona.xfm_bopname b on a.buyer_number = b.bopname_record_key
--   and a.bopmast_company_number = b.bopname_company_number
  and b.company_individ is not null
  and b.current_row
  and b.bopname_company_number in ('RY1','RY2')
where a.current_row 
  and a.record_status = 'U'
  and a.sale_type <> 'W'
  and a.date_capped between current_date - 366 and current_date - 1
  and bopmast_company_number in ('RY1','RY2')
  and similarity(a.bopmast_search_name, b.bopname_search_name) > .33
order by similarity(a.bopmast_search_name, b.bopname_search_name);


select * from wtf

-- for afton
select zip_code, count(*)
from wtf a
where bopmast_company_number = 'RY1'
  and length(zip_code::Text) > 1
group by zip_code
order by count(*) desc 
  