﻿
select a.task_id, a.task, a.next_run_ts, 
  b.*, c.*
-- select *
from tsk.tasks a
left join tsk.task_logs b on a.task_id = b.task_id
  and b.start_ts::Date = current_date
left join tsk.task_errors c on b.log_id = c.log_id
where a.is_active
  and a.task_id = 49