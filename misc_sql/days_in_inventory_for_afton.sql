﻿-- 667 vehicles
create temp table inpmast as
select a.inpmast_stock_number, a.inventory_account, a.inpmast_vehicle_cost, a.list_price, 
  (select arkona.db2_integer_to_date_long(a.date_in_invent)),
  current_date - arkona.db2_integer_to_date_long(a.date_in_invent) as days_in_invent
-- select *
from arkona.xfm_inpmast a
where a.current_row = true
  and a.status = 'I'
  and a.type_n_u = 'N'
order by length(a.inpmast_stock_number), a.inpmast_stock_number


select a.inpmast_stock_number, a.inventory_account, a.inpmast_vehicle_cost, a.list_price, 
  (select arkona.db2_integer_to_date_long(a.date_in_invent)),
  current_date - arkona.db2_integer_to_date_long(a.date_in_invent) as days_in_invent,
  d.the_date, e.journal_code, c.*
-- select *
from arkona.xfm_inpmast a
left join fin.dim_account b on a.inventory_account = b.account
left join fin.fact_gl c on b.account_key = c.account_key
  and a.inpmast_stock_number = c.control
  and c.post_status = 'Y'
left join dds.dim_date d on c.date_key = d.date_key  
left join fin.dim_journal e on c.journal_key = e.journal_key
where a.current_row = true
  and a.status = 'I'
  and a.type_n_u = 'N'
  and e.journal_code not in ('svi','pot','afm')
order by length(a.inpmast_stock_number), a.inpmast_stock_number, d.the_date 


-- 666 rows, 32267R is missing, not in accounting yet
drop table if exists acct;
create temp table acct as
select a.inpmast_stock_number, a.inventory_account, a.inpmast_vehicle_cost, a.list_price, 
  (select arkona.db2_integer_to_date_long(a.date_in_invent)) as date_in_invent,
  current_date - arkona.db2_integer_to_date_long(a.date_in_invent) as days_in_invent,  min(d.the_date) as acct_date,
  current_date - min(d.the_date) as acct_days,
  sum(c.amount)
-- select *
from arkona.xfm_inpmast a
left join fin.dim_account b on a.inventory_account = b.account
left join fin.fact_gl c on b.account_key = c.account_key
  and a.inpmast_stock_number = c.control
  and c.post_status = 'Y'
left join dds.dim_date d on c.date_key = d.date_key  
left join fin.dim_journal e on c.journal_key = e.journal_key
where a.current_row = true
  and a.status = 'I'
  and a.type_n_u = 'N'
  and e.journal_code not in ('svi','pot','afm')
group by a.inpmast_stock_number, a.inventory_account, a.inpmast_vehicle_cost, a.list_price, 
  (select arkona.db2_integer_to_date_long(a.date_in_invent)),
  current_date - arkona.db2_integer_to_date_long(a.date_in_invent)
order by length(a.inpmast_stock_number), a.inpmast_stock_number

-- where dates are different, only 30 rows
select * from acct where date_in_invent <> acct_date

select b.the_date, d.journal_code, c.account, a.* 
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
where a.post_status = 'Y'
--   and d.journal_code not in ('svi','pot','afm')
  and a.control = '28739'
order by b.the_date

select 39086.01 + 75 + 19.6 + 224.89
-- 2/12 oops, why am i doing new car only????

drop table if exists acct;
create temp table acct as
select a.inpmast_stock_number, a.inventory_account, a.inpmast_vehicle_cost, a.list_price, 
  (select arkona.db2_integer_to_date_long(a.date_in_invent)) as date_in_invent,
  current_date - arkona.db2_integer_to_date_long(a.date_in_invent) as days_in_invent,  min(d.the_date) as acct_date,
  current_date - min(d.the_date) as acct_days,
  sum(c.amount)
-- select *
from arkona.xfm_inpmast a
left join fin.dim_account b on a.inventory_account = b.account
left join fin.fact_gl c on b.account_key = c.account_key
  and a.inpmast_stock_number = c.control
  and c.post_status = 'Y'
left join dds.dim_date d on c.date_key = d.date_key  
left join fin.dim_journal e on c.journal_key = e.journal_key
where a.current_row = true
  and a.status = 'I'
--   and a.type_n_u = 'N'
  and e.journal_code not in ('svi','pot','afm')
group by a.inpmast_stock_number, a.inventory_account, a.inpmast_vehicle_cost, a.list_price, 
  (select arkona.db2_integer_to_date_long(a.date_in_invent)),
  current_date - arkona.db2_integer_to_date_long(a.date_in_invent)
order by length(a.inpmast_stock_number), a.inpmast_stock_number

select * from acct

-- 2/12 look at it from the perspective of inventory accounts with a balance
-- close enuf
drop table if exists inv_accts;
create temp table inv_accts as
select b.account_key, b.account, b.description, count(*)
from arkona.xfm_inpmast a
left join fin.dim_Account b on a.inventory_account = b.account 
where a.inventory_account is not null
  and b.description is not null
  and b.description not like 'C/S%'
group by b.account_key, b.account, b.description
having count(*) > 1;

select * from inv_accts order by account

-- so this says 1262 vehicles in inventory
select control, b.account, b.description, max(c.the_date) as last_date, sum(amount) as amount
from fin.fact_gl a
inner join inv_accts b on a.account_key = b.account_key
inner join dds.dim_date c on a.date_key = c.date_key
where a.post_status = 'Y'
group by control, b.account, b.description
having sum(amount) > 0
order by max(c.the_date)



select b.account, b.description, count(*)
from fin.fact_gl a
inner join inv_accts b on a.account_key = b.account_key
inner join dds.dim_date c on a.date_key = c.date_key
where a.post_status = 'Y'
group by b.account, b.description
having sum(amount) > 0
order by b.account
  

select * 
from acct a
full outer join (
  select control, b.account, b.description, max(c.the_date) as last_date, sum(amount) as amount
  from fin.fact_gl a
  inner join inv_accts b on a.account_key = b.account_key
  inner join dds.dim_date c on a.date_key = c.date_key
  where a.post_status = 'Y'
  group by control, b.account, b.description
  having sum(amount) > 0) b on a.inpmast_Stock_number = b.control
where a.inpmast_stock_number is null oR b.control is null


select b.account_key
from arkona.xfm_inpmast a
left join fin.dim_Account b on a.inventory_account = b.account 
where a.inventory_account is not null
  and b.description is not null
  and b.description not like 'C/S%'
group by b.account_key

select current_date - min(b.the_date)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join (
  select b.account_key, b.account
  from arkona.xfm_inpmast a
  inner join fin.dim_Account b on a.inventory_account = b.account 
  where a.inventory_account is not null
    and b.description is not null
    and b.description not like 'C/S%'
  group by b.account_key, b.account) c on a.account_key = c.account_key
where a.control = _stock_number;

  
do
$$
declare 
   _stock_number citext := 'g33238';
begin
drop table if exists days;
create temp table days as
select current_date - min(b.the_date)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join (
  select b.account_key, b.account
  from arkona.xfm_inpmast a
  inner join fin.dim_Account b on a.inventory_account = b.account 
  where a.inventory_account is not null
    and b.description is not null
    and b.description not like 'C/S%'
  group by b.account_key, b.account) c on a.account_key = c.account_key
where a.control = _stock_number;
end
$$;

select * from days;


-- this is faster and is what i sent to afton on 2/13
do
$$
declare 
   _stock_number citext := 'g33238';
begin
drop table if exists days;
create temp table days as
select current_date - min(b.the_date)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join (
  select b.account_key
  from arkona.xfm_inpmast a
  inner join fin.dim_Account b on a.inventory_account = b.account 
  where a.inventory_account is not null
    and b.description is not null
    and b.description not like 'C/S%'
    and inpmast_stock_number = _stock_number) c on a.account_key = c.account_key
where a.control = _stock_number;
end
$$;

select * from days;
