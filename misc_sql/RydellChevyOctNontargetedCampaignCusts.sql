﻿
drop table if exists name_ids_chev cascade;
create temp table name_ids_chev as
select a.*, b.bopname_company_number, b.bopname_Search_name, b.bopname_record_key--, c.stock_number, c.vin, c.delivery_date
from jon.oct_non_target a
join arkona.xfm_bopname b on a.last_name = b.last_company_name
  and coalesce(a.first_name, '') = coalesce(b.first_name, '')
  and b.current_row
where make = 'chevrolet';
create index on name_ids_chev(bopname_record_key);
create index on name_ids_chev(bopname_company_number);

drop table if exists name_ids_gmc cascade;
create temp table name_ids_gmc as
select a.*, b.bopname_company_number, b.bopname_Search_name, b.bopname_record_key--, c.stock_number, c.vin, c.delivery_date
from jon.oct_non_target a
left join arkona.xfm_bopname b on a.last_name = b.last_company_name
  and coalesce(a.first_name, '') = coalesce(b.first_name, '')
  and b.current_row
where make = 'chevrolet';
create index on name_ids_gmc(bopname_record_key);
create index on name_ids_gmc(bopname_company_number);

-- this is some inflated due to a row for buyer and cobuyer
select b.first_name, b.last_name, c.stock_number, c.delivery_date, c.vehicle_type,
  count(*) over (partition by vehicle_type)
from name_ids_chev b  
left join sls.ext_bopmast_partial c on (b.bopname_record_key = c.buyer_bopname_id or b.bopname_record_key = c.cobuyer_bopname_id)
  and b.bopname_company_number = c.store
  and c.deal_status = 'U' 
where c.delivery_date between '10/01/2018' and '10/31/2018'
group by b.first_name, b.last_name, c.stock_number, c.delivery_date, c.vehicle_type
order by last_name

-- now down to 200 rows
select b.vin, c.stock_number, c.delivery_date, c.vehicle_type,
  count(*) over (partition by vehicle_type)
from name_ids_chev b  
left join sls.ext_bopmast_partial c on (b.bopname_record_key = c.buyer_bopname_id or b.bopname_record_key = c.cobuyer_bopname_id)
  and b.bopname_company_number = c.store
  and c.deal_status = 'U' 
where c.delivery_date between '10/01/2018' and '10/31/2018'
group by b.vin, c.stock_number, c.delivery_date, c.vehicle_type

-- now down to 200 rows
select b.vin, c.stock_number, c.delivery_date, c.vehicle_type,
  count(*) over (partition by vehicle_type)
from name_ids_gmc b  
left join sls.ext_bopmast_partial c on (b.bopname_record_key = c.buyer_bopname_id or b.bopname_record_key = c.cobuyer_bopname_id)
  and b.bopname_company_number = c.store
  and c.deal_status = 'U' 
where c.delivery_date between '10/01/2018' and '10/31/2018'
group by b.vin, c.stock_number, c.delivery_date, c.vehicle_type



1GC2KVEG8FZ139657

select * from name_ids_chev where vin = '1GC2KVEG8FZ139657'


select * from arkona.xfm_bopname where bopname_search_name like '%zahui%'

select * from sls.ext_bopmast_partial where stock_number in ('G34356','G34800')


select * 
from jon.oct_non_target a
where last_name = 'Mcveigh'


select *
from arkona.ext_bopvref limit 10


select *
from name_ids_chev a
join arkona.ext_bopvref b on a.vin = b.vin
  and a.bopname_Record_key = b.customer_key
limit 200


select max(start_date) from arkona.ext_bopvref where start_date < 20190000


select *
from jon.oct_non_target a
left join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
limit 10  


1GNDT13S022439564



select *
from name_ids_chev a
join name_ids_gmc b on a.last_name = b.last_name and a.first_name = b.first_name


select first_name, last_name
from jon.oct_non_target
where make = 'chevrolet'
group by first_name, last_name

drop table if exists name_ids_chev cascade;
create temp table name_ids_chev as
select a.first_name, a.last_name, b.bopname_search_name, b.bopname_record_key, bopname_company_number
from jon.oct_non_target a
join arkona.xfm_bopname b on a.last_name = b.last_company_name
  and coalesce(a.first_name, '') = coalesce(b.first_name, '')
  and b.current_row
where make = 'chevrolet'
group by a.first_name, a.last_name, b.bopname_search_name, b.bopname_record_key, bopname_company_number;

select c.stock_number, c.delivery_date, c.vehicle_type, string_agg(b.bopname_search_name, ','),
  count(*) over (partition by vehicle_type)
from name_ids_chev b  
left join sls.ext_bopmast_partial c on (b.bopname_record_key = c.buyer_bopname_id or b.bopname_record_key = c.cobuyer_bopname_id)
  and b.bopname_company_number = c.store
  and c.deal_status = 'U' 
where c.delivery_date between '10/01/2018' and '10/31/2018'
group by c.stock_number, c.delivery_date, c.vehicle_type



drop table if exists name_ids_gmc cascade;
create temp table name_ids_gmc as
select a.first_name, a.last_name, b.bopname_search_name, b.bopname_record_key, bopname_company_number
from jon.oct_non_target a
join arkona.xfm_bopname b on a.last_name = b.last_company_name
  and coalesce(a.first_name, '') = coalesce(b.first_name, '')
  and b.current_row
where make = 'gmc'
group by a.first_name, a.last_name, b.bopname_search_name, b.bopname_record_key, bopname_company_number;

select c.stock_number, c.delivery_date, c.vehicle_type, string_agg(b.bopname_search_name, ','),
  count(*) over (partition by vehicle_type)
from name_ids_gmc b  
left join sls.ext_bopmast_partial c on (b.bopname_record_key = c.buyer_bopname_id or b.bopname_record_key = c.cobuyer_bopname_id)
  and b.bopname_company_number = c.store
  and c.deal_status = 'U' 
where c.delivery_date between '10/01/2018' and '10/31/2018'
group by c.stock_number, c.delivery_date, c.vehicle_type


select *
from (
select c.stock_number, c.delivery_date, c.vehicle_type, string_agg(b.bopname_search_name, ','),
  count(*) over (partition by vehicle_type)
from name_ids_chev b  
left join sls.ext_bopmast_partial c on (b.bopname_record_key = c.buyer_bopname_id or b.bopname_record_key = c.cobuyer_bopname_id)
  and b.bopname_company_number = c.store
  and c.deal_status = 'U' 
where c.delivery_date between '10/01/2018' and '10/31/2018'
group by c.stock_number, c.delivery_date, c.vehicle_type) aa
left join sls.deals_by_month bb on aa.stock_number = bb.stock_number


1GKS2CKJ7GR147629

select first_name, last_name,
from jon.oct_non_target
where make = 'chevrolet'
group by first_name, last_name

-- does this ensure that the correct customer is selected
-- eg 3 different bob smiths, this at leasts verifies the association to the vehicle 
-- referenced in the orig file
-- results in multiple rows for cust/vehicle if mult rows in bopvref
select a.*, b.bopname_search_name, b.bopname_record_key, bopname_company_number, c.*
from jon.oct_non_target a
join arkona.xfm_bopname b on a.last_name = b.last_company_name
  and coalesce(a.first_name, '') = coalesce(b.first_name, '')
  and b.current_row
join arkona.ext_bopvref c on a.vin = c.vin  
  and b.bopname_Record_key = c.customer_key
where a.make = 'gmc'

-------------------------------------------------------------------------------------------------------
-- so, group it, all i really need for getting deals is the store and bopname_Record_key
-- gives me 2808 rows
drop table if exists oct_gmc_non_target_customers cascade;
create temp table oct_gmc_non_target_customers as
select b.bopname_company_number, b.bopname_search_name, b.bopname_record_key
from jon.oct_non_target a
join arkona.xfm_bopname b on a.last_name = b.last_company_name
  and coalesce(a.first_name, '') = coalesce(b.first_name, '')
  and b.current_row
join arkona.ext_bopvref c on a.vin = c.vin  
  and b.bopname_Record_key = c.customer_key
where a.make = 'gmc'
group by b.bopname_company_number, b.bopname_search_name, b.bopname_record_key;
create unique index on oct_gmc_non_target_customers (bopname_company_number, bopname_record_key);

select distinct stock_number  -- 76 deals in october
from oct_gmc_non_target_customers b
left join sls.ext_bopmast_partial c on (b.bopname_record_key = c.buyer_bopname_id or b.bopname_record_key = c.cobuyer_bopname_id)
  and b.bopname_company_number = c.store
  and c.deal_status = 'U' 
where c.delivery_date between '10/01/2018' and '10/31/2018'



drop table if exists oct_chev_non_target_customers cascade;
create temp table oct_chev_non_target_customers as
select b.bopname_company_number, b.bopname_search_name, b.bopname_record_key
from jon.oct_non_target a
join arkona.xfm_bopname b on a.last_name = b.last_company_name
  and coalesce(a.first_name, '') = coalesce(b.first_name, '')
  and b.current_row
join arkona.ext_bopvref c on a.vin = c.vin  
  and b.bopname_Record_key = c.customer_key
where a.make = 'chevrolet'
group by b.bopname_company_number, b.bopname_search_name, b.bopname_record_key;
create unique index on oct_chev_non_target_customers (bopname_company_number, bopname_record_key);

select distinct stock_number  -- 187 deals in october
from oct_chev_non_target_customers b
left join sls.ext_bopmast_partial c on (b.bopname_record_key = c.buyer_bopname_id or b.bopname_record_key = c.cobuyer_bopname_id)
  and b.bopname_company_number = c.store
  and c.deal_status = 'U' 
where c.delivery_date between '10/01/2018' and '10/31/2018'



create table jon.oct_target (
  vin citext,
  first_name citext,
  last_name citext,
  make citext);



drop table if exists oct_chev_target_customers cascade;
create temp table oct_chev_target_customers as
select b.bopname_company_number, b.bopname_search_name, b.bopname_record_key
from jon.oct_target a
join arkona.xfm_bopname b on a.last_name = b.last_company_name
  and coalesce(a.first_name, '') = coalesce(b.first_name, '')
  and b.current_row
join arkona.ext_bopvref c on a.vin = c.vin  
  and b.bopname_Record_key = c.customer_key
where a.make = 'chevrolet'
group by b.bopname_company_number, b.bopname_search_name, b.bopname_record_key;
create unique index on oct_chev_target_customers (bopname_company_number, bopname_record_key);

select distinct stock_number  -- 15 deals in october
from oct_chev_target_customers b
left join sls.ext_bopmast_partial c on (b.bopname_record_key = c.buyer_bopname_id or b.bopname_record_key = c.cobuyer_bopname_id)
  and b.bopname_company_number = c.store
  and c.deal_status = 'U' 
where c.delivery_date between '10/01/2018' and '10/31/2018'



drop table if exists oct_gmc_target_customers cascade;
create temp table oct_gmc_target_customers as
select b.bopname_company_number, b.bopname_search_name, b.bopname_record_key
from jon.oct_target a
join arkona.xfm_bopname b on a.last_name = b.last_company_name
  and coalesce(a.first_name, '') = coalesce(b.first_name, '')
  and b.current_row
join arkona.ext_bopvref c on a.vin = c.vin  
  and b.bopname_Record_key = c.customer_key
where a.make = 'gmc'
group by b.bopname_company_number, b.bopname_search_name, b.bopname_record_key;
create unique index on oct_gmc_target_customers (bopname_company_number, bopname_record_key);

select distinct stock_number  -- 5 deals in october
from oct_gmc_target_customers b
left join sls.ext_bopmast_partial c on (b.bopname_record_key = c.buyer_bopname_id or b.bopname_record_key = c.cobuyer_bopname_id)
  and b.bopname_company_number = c.store
  and c.deal_status = 'U' 
where c.delivery_date between '10/01/2018' and '10/31/2018'




select count(distinct bopname_record_key)  
from oct_chev_non_target_customers

gmc targeted:
select 500.0/465  1.07%

chev targeted
select 1500.0/1214  1.23%

gmc non targeted
select 7600.0/2808  2.71%

chev non targeted   2.81%
select 18700.0/6662







select *
from jon.oct_non_target a
join jon.oct_target b on a.vin = b.vin




create table jon.am_deals (deal_number integer);

select *
from sls.ext_bopmast_partial
where bopmast_id in (50734,50779,50793,50818,50839,50842,50828,50862,50873,50890,50899,50904,50917,
  50918,50933,50950,50956,50970,50948,51006,50884,51026,50953,51058,50995,50980,51075,51101,51114,
  51115,51138,51156,51149,51150,51182,51175,51190,51204,51197,51209,51213,51230,51219,51221,51271,
  51275,51270,51269,51238)

select a.deal_number, b.stock_number, 
  c.bopname_search_name as chev_target, d.bopname_search_name as chev_non_target,
  e.bopname_search_name as gmc_target, f.bopname_search_name as gmc_non_target
from jon.am_deals a
left join sls.ext_bopmast_partial b on a.deal_number = b.bopmast_id
-- left join arkona.xfm_bopname bb on (bb.buyer_bopname_id = bb.bopname_record_key or b.cobuyer_bopname_id = c.bopname_record_key)
left join oct_chev_target_customers c on (b.buyer_bopname_id = c.bopname_record_key or b.cobuyer_bopname_id = c.bopname_record_key)
left join oct_chev_non_target_customers d on (b.buyer_bopname_id = d.bopname_record_key or b.cobuyer_bopname_id = d.bopname_record_key)
left join oct_gmc_target_customers e on (b.buyer_bopname_id = e.bopname_record_key or b.cobuyer_bopname_id = e.bopname_record_key)
left join oct_gmc_non_target_customers f on (b.buyer_bopname_id = f.bopname_record_key or b.cobuyer_bopname_id = f.bopname_record_key)



select * from oct_chev_target_customers where bopname_Search_name like 'brown%'

select * from oct_chev_non_target_customers where bopname_Search_name like 'brown%'


select *
from oct_chev_target_customers a
join oct_chev_non_target_customers b on a.bopname_Record_key = b.bopname_Record_key
order by a.bopname_search_name


select *
from (
  select a.*, 'non_target'
  from jon.oct_non_target a
  union
  select b.*, 'target'
  from jon.oct_target b) c
order by last_name, first_name


select * from arkona.xfm_bopname limit 10

-- 11/29/18 sent this to greg, takes the am list of attributed deals and correlates the relevant list
select a.deal_number, bb.bopname_search_name,
  case when c.bopname_search_name is not null then 'X' end as chev_target, 
  case when d.bopname_search_name is not null then 'X' end as chev_non_target,
  case when e.bopname_search_name is not null then 'X' end as gmc_target, 
  case when f.bopname_search_name is not null then 'X' end as gmc_non_target
from jon.am_deals a
left join sls.ext_bopmast_partial b on a.deal_number = b.bopmast_id
left join arkona.xfm_bopname bb on b.buyer_bopname_id = bb.bopname_record_key
  and b.store = bb.bopname_company_number
  and bb.current_row
-- left join arkona.xfm_bopname bb on (bb.buyer_bopname_id = bb.bopname_record_key or b.cobuyer_bopname_id = c.bopname_record_key)
left join oct_chev_target_customers c on (b.buyer_bopname_id = c.bopname_record_key or b.cobuyer_bopname_id = c.bopname_record_key)
left join oct_chev_non_target_customers d on (b.buyer_bopname_id = d.bopname_record_key or b.cobuyer_bopname_id = d.bopname_record_key)
left join oct_gmc_target_customers e on (b.buyer_bopname_id = e.bopname_record_key or b.cobuyer_bopname_id = e.bopname_record_key)
left join oct_gmc_non_target_customers f on (b.buyer_bopname_id = f.bopname_record_key or b.cobuyer_bopname_id = f.bopname_record_key)
order by deal_number


-- 11/29/18 next step, greg wants to see the financing details on the deals from the lists
-- eg the deal for the customer and the vin

select distinct e.customer_key, e.vin
from (
  select a.first_name, a.last_name, b.*, c.last_company_name, c.first_name
  from jon.oct_target a
  left join arkona.ext_bopvref b on a.vin = b.vin
  left join arkona.xfm_bopname c on b.customer_key = c.bopname_record_key
    and c.current_row
  where make = 'gmc'
    and a.last_name = c.last_company_name
    and a.first_name = c.first_name) e
order by a.vin


select g.last_name, g.first_name, f.*
from sls.ext_bopmast_partial f
join (
  select distinct e.customer_key, e.vin, e.last_name, e.first_name
  from (
    select a.first_name, a.last_name, b.*
    from jon.oct_target a
    left join arkona.ext_bopvref b on a.vin = b.vin
    left join arkona.xfm_bopname c on b.customer_key = c.bopname_record_key
      and c.current_row
    where make = 'gmc'
      and a.last_name = c.last_company_name
      and a.first_name = c.first_name) e) g on f.vin = g.vin and (f.buyer_bopname_id = g.customer_key or f.cobuyer_bopname_id = g.customer_key)

select *
from sls.ext_bopmast_partial
where vin = '3GTU2WEC4FG271052'  
