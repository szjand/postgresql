-- fact_fs : gm_account/year_month/store
select year_month, page, line, col, line_label, gm_account, gl_account, store, area, department, sub_Department, amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where gm_account in ('454','455')
  and year_month > 201700
  and store = 'ry1'

-- fs : accounts for fs page/line
select *
from arkona.ext_eisglobal_sypffxmst a
-- -- *a*
left join (
  select factory_financial_year, 
  case
    when coalesce(consolidation_grp, '1')  = '1' then 'RY1'::citext
    when coalesce(consolidation_grp, '1')  = '2' then 'RY2'::citext
    else 'XXX'::citext
  end as store_code, g_l_acct_number, factory_account, fact_account_
  from arkona.ext_ffpxrefdta) b on a.fxmcyy = b.factory_financial_year and a.fxmact = b.factory_account
where a.fxmcyy = 2017
  and fxmpge = 2
  and fxmlne between 4 and 54
order by fxmpge, fxmlne, fxmcol

-- fs : service (main shop & pdq) customer pay sale accounts (FS: P6 L21-23)
select 
  case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code,
  a.fxmpge as the_page, a.fxmlne::integer as line, trim(b.g_l_acct_number) as gl_account, fxmact, c.account_type
from arkona.ext_eisglobal_sypffxmst a
inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
inner join fin.dim_account c on b.g_l_acct_number = c.account  
where a.fxmcyy = 2017
  and trim(a.fxmcde) = 'GM'
  and coalesce(b.consolidation_grp, 'RY1') in('2','RY1')
  and b.factory_code = 'GM'
  and trim(b.factory_account) <> '331A'
  and c.account_type = 'sale'
  and a.fxmpge = 16 and a.fxmlne in (21,23)
order by line, case b.consolidation_grp when '2' then 'RY2' else 'RY1' end, fxmact, gl_Account

-- fact_gl : control/account/store
select b.the_date, c.account, c.description, d.journal_code, a.amount, e.description
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and control = 'h9535a'
  and c.account = '245002'
  and c.store_code = 'RY1'

-- fact_gl : new cars
select b.the_date, c.account, c.description, d.journal_code, a.amount, e.description, 
  account_type_code, account_type, department
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and control = 'h9734'
  and journal_code = 'VSN'
  and account_type = 'sale'
  and department = 'new vehicle' 

-- fact_gl : used cars
select b.the_date, c.account, c.description, d.journal_code, a.amount, e.description, 
  account_type_code, account_type, department
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and control = '30625a'
  and journal_code = 'VSU'
  and account_type = 'sale'
  and department = 'used vehicle'  

-- list of a table's fields, comma separated
SELECT string_agg(column_name, ',')
FROM information_schema.columns
WHERE table_schema = 'sls'
  AND table_name   = 'deals_by_month'
group by table_name  

-- list of column names for temp table
SELECT string_agg(column_name, ',')
FROM information_schema.columns
-- WHERE table_schema = 'sls'
where table_name   = 'running_days'
group by table_name  
