﻿
/*

8/6/19
Are you able to pull a report showing the following for both stores:

Employee name
Hours worked YTD
Wages YTD
Birthdate
Hire date
401k deduction percentage

Jeri Schmiess Penas, CPA
*/

-- requires pydeduct refresh

select * 
from dds.working_days
limit 100


select wd_of_year, wd_of_year * 8, wd_of_year_elapsed
from dds.working_days
where department = 'service'
  and the_date = current_date

drop table if exists base;
create temp table base as
select a.payroll_class, a.pay_period, a.base_salary, a.distrib_code, a.employee_name, a.pymast_employee_number, arkona.db2_integer_to_date(birth_date) as birth_date,
  arkona.db2_integer_to_date(hire_date) as hire_date,
  b.wages_ytd, c.hours_worked_ytd, d."401k", d."401k Roth", d."401k Roth BO"
-- select *
from arkona.xfm_pymast a
left join (
  select employee_, sum(total_gross_pay) as wages_ytd
  from arkona.ext_pyhshdta
  where payroll_cen_year = 119
    and seq_void <> '0J'
  group by employee_) b on a.pymast_employee_number = b.employee_
left join (
  select employee_number, sum(clock_hours) as hours_worked_ytd
  from arkona.xfm_pypclockin 
  where the_date > '12/31/2018'
  group by employee_number) c on a.pymast_employee_number = c.employee_number  
left join (
  select employee_number, 
    case when ded_pay_code = '91' then fixed_ded_amt end as "401k",
    case when ded_pay_code = '99' then fixed_ded_amt end as "401k Roth",
    case when ded_pay_code = '99a' then fixed_ded_amt end as "401k Roth BO"
  from arkona.ext_pydeduct
  where ded_pay_code in('91','99','99a')) d on a.pymast_employee_number = d.employee_number
where a.current_row
  and a.pymast_company_number in ('RY1','RY2')
  and a.active_code <> 'T';



update base a
set hours_worked_ytd = x.the_hours
from (
  select a.pymast_employee_number,
    case
      when hire_date < '01/01/2019' then (select wd_of_year * 8 from dds.working_days where department = 'service' and the_date = current_date)
      else (select count(*) * 8 from dds.working_days where department = 'service' and the_date between a.hire_date and current_date)
    end as the_hours
  from base a
  where hours_worked_ytd is null
    and payroll_class <> 'H') x
where a.pymast_employee_number = x.pymast_employee_number;  

update base a
set hours_worked_ytd = x.the_hours
from (
  select a.pymast_employee_number,
    case
      when hire_date < '01/01/2019' then (select wd_of_year * 8 from dds.working_days where department = 'service' and the_date = current_date)
      else (select count(*) * 8 from dds.working_days where department = 'service' and the_date between a.hire_date and current_date)
    end as the_hours
  from base a
  where hours_Worked_ytd < 100
    and payroll_class <> 'H') x
where a.pymast_employee_number = x.pymast_employee_number;  

update base
set hours_worked_ytd = 1232
where pymast_employee_number = '190910';

select employee_name, hours_worked_ytd, wages_ytd, birth_date, hire_date, 
  min("401k") as "401k", min("401k Roth") as "401k Roth", min("401k Roth BO") as "401k Roth BO"
from base
group by employee_name, hours_worked_ytd, wages_ytd, birth_date, hire_date
order by employee_name



