﻿select a.bopmast_company_number, a.bopmast_stock_number, a.bopmast_vin, a.date_capped,
  b.contract_name, bopname_search_name, c.state_code, c.zip_code
from arkona.xfm_bopmast a
join arkona.ext_bopsrvs b on a.bopmast_Company_number = b.company_number
  and a.record_key = b.deal_key
join arkona.xfm_bopname c on b.company_number = c.bopname_company_number
  and b.customer_key = c.bopname_record_key
  and c.state_code = 'MN'
where a.current_row
  and a.record_Status = 'U'
  and a.date_capped between '01/01/2016' and '12/31/2018'
