﻿drop table if exists time_clock;
create temp table time_clock as
SELECT b.the_date, a.overtimehours, c.pydeptcode, c.pydept, c.distcode, left(c.name, 45) as name, 
  c.employeenumber, c.payrollclass, c.hourlyrate, c.payperiod
FROM ads.ext_edw_Clock_Hours_Fact a
INNER JOIN dds.dim_date b on a.datekey = b.date_key
INNER JOIN ads.ext_edw_Employee_Dim c on a.employeekey = c.employeekey
WHERE b.the_year = 2018
  AND a.overtimehours <> 0
order by c.payperiod desc;  


select * -- overtime_amount, overtime_hours
from arkona.ext_pyhshdta
where payroll_run_number = 223180
  and employee_ = '18005'

select * 
from arkona.ext_pyptbdta
where payroll_run_number = 223180


select distinct biweekly_pay_period_start_date, biweekly_pay_period_end_date
from dds.dim_date
where the_year = 2018
  and the_date < current_date
order by biweekly_pay_period_start_date  

need to compare paychecks vs clock

so, all checks that include overtime pay

select seq_void, count(*) from arkona.ext_pyhshdta group by seq_void

drop table if exists checks;
create temp table checks as
select a.company_number, a.employee_name, a.distrib_code, a.department_code,
  a.employee_, a.overtime_amount, a.overtime_hours,
  (select dds.db2_integer_to_date(b.payroll_start_date)) as start_date, 
  (select dds.db2_integer_to_date(b.payroll_end_date)) as end_date
from arkona.ext_pyhshdta a
inner join arkona.ext_pyptbdta b on a.company_number = b.company_number
  and a.payroll_run_number = b.payroll_run_number
where a.payroll_cen_year = 118
  and a.overtime_amount <> 0 
  and a.overtime_hours <> 0;


select *
from checks a
left join time_clock b on a.employee_ = b.employeenumber
  and b.the_date between a.start_date and a.end_date
order by employee_name, start_Date

select start_date, end_date, a.company_number, a.employee_name, a.distrib_code, b.pydept, a.overtime_hours, a.overtime_amount, 
  sum(b.overtimehours) as tc_hours, round(sum(b.overtimehours * 1.5 * b.hourlyrate), 2) as tc_amount,
  a.overtime_amount - round(sum(b.overtimehours * 1.5 * b.hourlyrate), 2) as diff
from checks a
left join time_clock b on a.employee_ = b.employeenumber
  and b.the_date between a.start_date and a.end_date  
group by start_date, end_date, a.company_number, a.employee_name, a.distrib_code, b.pydept, a.overtime_hours, a.overtime_amount
order by employee_name, start_Date

select *
from (
  select start_date, end_date, a.company_number, a.employee_name, a.distrib_code, b.pydept, a.overtime_hours, a.overtime_amount, 
    sum(b.overtimehours) as tc_hours, round(sum(b.overtimehours * 1.5 * b.hourlyrate), 2) as tc_amount,
    a.overtime_amount - round(sum(b.overtimehours * 1.5 * b.hourlyrate), 2) as diff
  from checks a
  left join time_clock b on a.employee_ = b.employeenumber
    and b.the_date between a.start_date and a.end_date  
  group by start_date, end_date, a.company_number, a.employee_name, a.distrib_code, b.pydept, a.overtime_hours, a.overtime_amount
  order by employee_name, start_Date) x
where diff <> 0

diff:
  maintenance employees are off i believe because of the auto deduct is not being persisted into edwClockHoursFact
  i think edwClockHoursFact is fucked up at year end, not sure why, may diffs are in 12/24 pay period
  cory stinar, 2/4 - 2/17, hourly rate 16 thru 2/13, then 8 currently
  
select voided_flag, count(*) from arkona.ext_pyhshdta group by voided_flag

select * from arkona.ext_pyhshdta where voided_Flag = 'V'

select * from arkona.ext_pyptbdta where payroll_run_number in (1229174,1269003)

select payrollclass, payperiod, count(*) from ads.ext_edw_employee_dim where currentrow = true and active = 'active' group by payrollclass, payperiod order  by payrollclass, payperiod  

select name, payrollclass, payperiod, pydept, distcode from ads.ext_edw_employee_dim where currentrow = true and active = 'active' order  by payrollclass, payperiod  

select * from ads.ext_edw_employee_dim where employeenumber = '1132590'


SELECT b.the_date, a.*
FROM ads.ext_edw_Clock_Hours_Fact a
INNER JOIN dds.dim_date b on a.datekey = b.date_key
INNER JOIN ads.ext_edw_Employee_Dim c on a.employeekey = c.employeekey
WHERE b.the_date between '12/24/2017' and '02/06/2018'
  AND employeenumber = '118010'


select *
from dds.ext_pypclockin
where trim(pymast_employee_number) = '118010'
and yiclkind between '12/24/2017' and '01/06/2018'    




-- multiple same clockin time 
select *
from arkona.ext_pypclockin
where pymast_employee_number = '144788'
and yiclkind = '01/11/2018'


SELECT b.the_date, a.*
FROM ads.ext_edw_Clock_Hours_Fact a
INNER JOIN dds.dim_date b on a.datekey = b.date_key
INNER JOIN ads.ext_edw_Employee_Dim c on a.employeekey = c.employeekey
WHERE b.the_date = '01/11/2018'
  AND employeenumber = '144788'

select 
-- extracted all off pypclockin into arkona.ext_pypclockin  
-- limited to ry1, ry2
-- converted 24:00:00 to 23:59:59
-- the temptation is to start generating fact_clock_hours in postgres


select b.year_month, count(*)
from arkona.ext_pypclockin a
inner join dds.dim_date b on a.yiclkind = b.the_date
group by b.year_month
order by b.year_month


select name, employeenumber, distcode, min(employeekeyfromdate) as from_date, max(employeekeythrudate)
from ads.ext_edw_employee_dim
group by name, employeenumber, distcode
order by employeenumber, from_date
