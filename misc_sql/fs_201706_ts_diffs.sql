﻿P2L2 does not match FS, but page 5-17 does

-- variable --------------------------------------------------------------------------------------------------
select *, abs(amount_x) - abs(amount_xx)
from ( --page 2
  select gl_account, sum(amount) as amount_x
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where year_month = 201706
    and page = 2
    and col = 7
    and line = 2
    and store = 'ry1'
  group by store, page, line, gl_Account
  having sum(amount) <> 0) x
full outer join ( -- page 5-17
  select gl_account, sum(amount) as amount_xx
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where year_month = 201706
    and ((page = 16  and line between 1 and 14)
    or (page between 5 and 15)
    or (page = 17 and line between 1 and 21))
    and store = 'ry1'
  group by gl_Account
  having sum(amount) <> 0) xx on x.gl_account = xx.gl_account 
  order by x.gl_account

page 2 missing 165300


select year_month, page, line, col, line_label, gm_account, gl_account, area, d.department, sub_Department, amount, e.description
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
inner join fin.dim_account e on c.gl_account = e.account
where year_month = 201706
  and gl_Account = '165300'

-- only 3 entries in all history
select b.the_date, c.account, c.description, d.journal_code, a.amount, e.description
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and c.account = '165300'
  and c.store_code = 'RY1'


-- variable --------------------------------------------------------------------------------------------------

-- fixed --------------------------------------------------------------------------------------------------  
select line, sum(amount) 
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where year_month = 201706
  and page = 16  and line between 21 and 61
  and store = 'ry1'
group by line
order by line

-- line 30 way off (adj cost labor)
select year_month, page, line, col, line_label, gm_account, gl_account, area, d.department, sub_Department, amount, e.description
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
inner join fin.dim_account e on c.gl_account = e.account
where year_month = 201706
  and page = 16
  and line = 30
  and d.store = 'ry1'


select b.the_date, c.account, c.description, d.journal_code, a.amount, e.description
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and c.account in ('166504A','166504')
  and c.store_code = 'RY1'
  and year_month = 201706

this is weird, 166504 & 166504A, in fact_fs4 lines instead of 2 and each line is twice what 
it should be, eg 166504 = 5220.44, in fact_fs it is 10441 on 2 lines for a total of 20882
turns out 166504, 166504a had type2 2 changes in june, so script was double dipping
and that fixed the remaining issues 

select *
from fin.dim_account
where account in ('166504','166504A')

select *
from fin.dim_account a
where exists (
  select 1
  from fin.dim_account
  where account = a.account
    and current_row = false)
order by account, current_row


    



-- fixed --------------------------------------------------------------------------------------------------  