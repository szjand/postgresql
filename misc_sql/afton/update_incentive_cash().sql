﻿
-- 
-- -- DROP FUNCTION gmgl.update_incentive_cash();
-- 
-- CREATE OR REPLACE FUNCTION gmgl.update_incentive_cash()
--   RETURNS void AS
-- $BODY$
-- /*
-- select gmgl.update_incentive_cash() 
-- */
-- 
-- update gmgl.vin_incentive_cash 
-- set thru_ts = now()
-- where thru_ts = '12/31/9999';

/*
8/24/19
  cte vehicle_data not required

8/25
  uh oh, did not factor in best_incentive
  eg G34814, G36220  returns 2 majors, should only be one
  will come back to this when i figure out the best price before incentives for monday
*/

 with _current_inventory as (
 select inpmast_vin as vin, c.control as stock_number,"trim", body_style,min(d.the_date) as first_date_in_acct, 
 current_date - min(d.the_date) as days_in_inventory, model, year, a.make,  inpmast_company_number, a.inpmast_vehicle_cost as vehicle_cost
  from ( -- a
    select inventory_account,"trim",body_style,
    inpmast_vin,  inpmast_stock_number, inpmast_company_number,
    inpmast_vehicle_cost,model, year, make
    from arkona.xfm_inpmast a
    where a.inventory_account is not null
      and a.status = 'I'
      and a.type_n_u = 'N'
      and a.current_row = true

-- and a.inpmast_vin = '1GCUYDED0KZ220394'

      ) a
    inner join fin.dim_Account b on a.inventory_account = b.account
    inner join fin.fact_gl c on b.account_key = c.account_key
      and c.post_status = 'Y'
      and c.control = a.inpmast_stock_number
    inner join dds.dim_date d on c.date_key = d.date_key
     where a.make in ('BUICK','CHEVROLET','GMC','CADILLAC')
group by  inpmast_vin, c.control, model, year, a.make, inpmast_company_number, inpmast_vehicle_cost,"trim", body_style
),
-- vehicle_data as (
--   select distinct vin, b.year, a.make, a.model, a.trim , a.oem_body_style as body_type
--   from dao.veh_trim_styles a
--   inner join (
--     select distinct a.*, b.vin
--     from dao.vin_reference a
--     inner join gmgl.vehicle_option_codes b on vin_pattern = concat(left(b.vin,8), substring(vin,10,2))
--     inner join _current_inventory c on b.vin = c.vin
--     where mfr_package_code = b.option_code and mfr_model_num = model_code
--   ) b on a.vehicle_id = b.vehicle_id),


_custom_rules as ( -- cadillac 4%
select distinct rule_name, case when math_operator = '*' then the_value * second_value::numeric end as custom_incentive, vin
from (
select d.rule_name,
  case
    when first_value_type = 'MSRP' 
  then total_msrp 
  end as the_value , math_operator, second_value, a.vin
from _current_inventory a
inner join gmgl.clean_gm_vehicles b on a.vin = b.vin
inner join gmgl.custom_incentive_rule_distributions c on b.make = c.make and c.model_year = b.model_year
inner join gmgl.custom_incentive_rules d on d.rule_key = c.rule_key
inner join gmgl.vehicle_invoices e on a.vin = e.vin and e.thru_date > now() ) A
),

-- select * from _custom_rules 

  programs as (
  select distinct
  a.program_start_date,a.program_end_date,a.major_incentive_key,gg.vin, --a.program_pdf,
  coalesce(a.program_dollar_amount,0) as major_program_dollar_amount,a.program_number as major_program_number,
  a.program_name as major_program_name, coalesce(c.program_dollar_amount,0) as stackable_dollar_amount, 
  c.program_number as stackable_program,c.program_name as stackable_program_name, c.color, 
  ember_id,
    case 
      when a.has_sup_inc = true 
    then d.supinc 
      else 0 end as major_sup_inc,
    case 
      when c.has_sup_inc = true 
    then d.supinc 
      else 0 end as stackable_sup_inc,
          case 
      when a.has_emp_inc = true 
    then d.empinc 
      else 0 end as major_emp_inc,
    case 
      when c.has_emp_inc = true 
    then d.empinc 
      else 0 end as stackable_emp_inc,
      is_best_incentive, 
      a.has_sup_inc as has_major_sup_inc, a.has_emp_inc as has_major_emp_inc,
      c.has_emp_inc as has_stackable_emp_inc,
      c.has_sup_inc as has_stackable_sup_inc, 
    coalesce((select custom_incentive from _custom_rules zz where zz.vin = gg.vin),0) as cadillac_incentive
  from _current_inventory gg
  inner join (
    select a.*, program_name, program_number, has_sup_inc, has_emp_inc, program_start_date, program_end_date,program_pdf
    from gmgl.major_incentives a
    inner join gmgl.incentive_programs b on a.program_key  = b.program_key
    inner join gmgl.incentive_program_rules c on b.program_key = c.program_key and current_row = true
    where lookup_date = current_date) A on gg.vin = a.vin
  left join (
    select c.*,program_name, program_number, has_sup_inc,has_emp_inc, program_start_date, program_end_date,program_pdf
    from gmgl.stackable_incentives c 
    inner join gmgl.incentive_programs d on c.program_key = d.program_key
    inner join gmgl.incentive_program_rules e on d.program_key = e.program_key 
      and current_row = true
    where lookup_date = current_date) c on a.major_incentive_key = c.major_incentive_key
    inner join gmgl.vehicle_invoices d on gg.vin = d.vin and thru_date > now()
    where gg.vin is not null)
-- 
-- select vin, sum(major_sup_inc + stackable_sup_inc + major_emp_inc + stackable_emp_inc + cadillac_incentive)
-- from (
--   select vin, major_sup_inc, stackable_sup_inc, major_emp_inc, stackable_emp_inc, cadillac_incentive
--   from programs 
--   group by vin, major_sup_inc, stackable_sup_inc, major_emp_inc, stackable_emp_inc, cadillac_incentive) aa
-- group by vin  
-- having sum(major_sup_inc + stackable_sup_inc + major_emp_inc + stackable_emp_inc + cadillac_incentive) <> 0

-- select vin, 'Name:Rydell Discount'||'_Price:'||bb.amount::text||'_Category:Global'||'_Disclaimer:TBD|' as rd
-- from (
--   select vin, sum(major_sup_inc + stackable_sup_inc + major_emp_inc + stackable_emp_inc + cadillac_incentive) as amount
--   from (
--     select vin, major_sup_inc, stackable_sup_inc, major_emp_inc, stackable_emp_inc, cadillac_incentive
--     from programs 
--     group by vin, major_sup_inc, stackable_sup_inc, major_emp_inc, stackable_emp_inc, cadillac_incentive) aa
--   group by vin  
--   having sum(major_sup_inc + stackable_sup_inc + major_emp_inc + stackable_emp_inc + cadillac_incentive) <> 0) bb

-- select gg.vin, stock_number, a.program_number, a.program_name, a.has_sup_inc, a.has_emp_inc, a.program_dollar_amount
--   from _current_inventory gg
--   inner join (
--     select a.*, program_name, program_number, has_sup_inc, has_emp_inc, program_start_date, program_end_date,program_pdf
--     from gmgl.major_incentives a
--     inner join gmgl.incentive_programs b on a.program_key  = b.program_key
--     inner join gmgl.incentive_program_rules c on b.program_key = c.program_key and current_row = true
--     where lookup_date = current_date) A on gg.vin = a.vin
--   left join (
--     select c.*,program_name, program_number, has_sup_inc,has_emp_inc, program_start_date, program_end_date,color
--     from gmgl.stackable_incentives c 
--     inner join gmgl.incentive_programs d on c.program_key = d.program_key
--     inner join gmgl.incentive_program_rules e on d.program_key = e.program_key 
--       and current_row = true
--     where lookup_date = current_date) c on a.major_incentive_key = c.major_incentive_key
--     inner join gmgl.vehicle_invoices d on gg.vin = d.vin and thru_date > now()
--     where gg.vin = '3GTU9FEL4KG263510'
--     order by vin

-- select *
-- take 1
-- select '|'||string_agg('Name:'||program_name||'_Price:'||program_dollar_amount::text||'_Category:Global'||'_Disclaimer:TBD','|')||'|'||','||aa.vin , max(bb."Rydell Discount")
-- -- take 2 added rydell discount
-- select '|'||string_agg('Name:'||program_name||'_Price:'||program_dollar_amount::text||'_Category:Global'||'_Disclaimer:TBD','|')||'|'||
--   max(coalesce(
--     case
--       when cc.vin is not null then rd || ','||aa.vin
--     end, ','||aa.vin))
-- take 3
-- Thank you for the update.  Would it be possible to add headers to the file and also additional column labeled dealerID?  
-- We will need the new column to associate the data from the file to your dealership.  
-- You can use number "5663" for each vehicle if you want. " 
select '|'||string_agg('Name:'||program_name||'_Price:'||program_dollar_amount::text||'_Category:Global'||'_Disclaimer:TBD','|')||'|'||
  max(coalesce(
    case
      when cc.vin is not null then rd 
    end, '')) as rebates,
  aa.vin, '5663'::text as dealerid
from (
  select gg.vin, stock_number, 'major', a.program_number, a.program_name, a.has_sup_inc, a.has_emp_inc, a.program_dollar_amount
    from _current_inventory gg
    inner join (
      select a.*, program_name, program_number, has_sup_inc, has_emp_inc, program_start_date, program_end_date,program_pdf
      from gmgl.major_incentives a
      inner join gmgl.incentive_programs b on a.program_key  = b.program_key
      inner join gmgl.incentive_program_rules c on b.program_key = c.program_key and current_row = true
      where lookup_date = current_date) A on gg.vin = a.vin 
  union
  select gg.vin, stock_number, 'stackable', c.program_number, c.program_name, c.has_sup_inc, c.has_emp_inc, c.program_dollar_amount
    from _current_inventory gg
    inner join (
      select a.*, program_name, program_number, has_sup_inc, has_emp_inc, program_start_date, program_end_date,program_pdf
      from gmgl.major_incentives a
      inner join gmgl.incentive_programs b on a.program_key  = b.program_key
      inner join gmgl.incentive_program_rules c on b.program_key = c.program_key and current_row = true
      where lookup_date = current_date) A on gg.vin = a.vin
    left join (
      select c.*,program_name, program_number, has_sup_inc,has_emp_inc, program_start_date, program_end_date,color
      from gmgl.stackable_incentives c 
      inner join gmgl.incentive_programs d on c.program_key = d.program_key
      inner join gmgl.incentive_program_rules e on d.program_key = e.program_key 
        and current_row = true
      where lookup_date = current_date) c on a.major_incentive_key = c.major_incentive_key
      inner join gmgl.vehicle_invoices d on gg.vin = d.vin and thru_date > now()) aa
left join (
  select vin, 'Name:Rydell Discount'||'_Price:'||bb.amount::text||'_Category:Global'||'_Disclaimer:TBD|' as rd
  from (
    select vin, sum(major_sup_inc + stackable_sup_inc + major_emp_inc + stackable_emp_inc + cadillac_incentive)::integer as amount
    from (
      select vin, major_sup_inc, stackable_sup_inc, major_emp_inc, stackable_emp_inc, cadillac_incentive
      from programs 
      group by vin, major_sup_inc, stackable_sup_inc, major_emp_inc, stackable_emp_inc, cadillac_incentive) aa
    group by vin  
    having sum(major_sup_inc + stackable_sup_inc + major_emp_inc + stackable_emp_inc + cadillac_incentive) <> 0) bb) cc on aa.vin = cc.vin   
where program_number is not null
  and stock_number = 'g37621'
group by aa.vin
-- 
-- 
--     


-- colors are no longer used
-- select color, max(lookup_date), count(*) from gmgl.stackable_incentives group by color
-- 
-- select a.*, b.program_name from gmgl.stackable_incentives a join gmgl.incentive_programs b on a.program_key = b.program_key where lookup_date = current_Date and program_dollar_amount <> 0 order by program_key


-- select *
-- from gmgl.incentive_programs a
-- where not exists (
--   select 1
--   from gmgl.incentive_program_rules
--   where program_key = a.program_key)



select lookup_date, major_json->>'description' as major_rebate, major_json->>'contractInd' as major_contract_ind,
case
 when major_json->>'contractInd' = 'false'
   then 'Black'
 else 'Red' end as major_color,
c->>'description' as stackable_rebate, c->>'contractInd' as stackable_contract_ind,
case
 when c->>'contractInd' = 'false'
   then 'Black'
 else 'Red' end as stackable_color
from gmgl.vin_incentives_json
left join json_array_elements(stackable_json) c on 1 = 1
where vin = '1GCPYCEF3KZ303326'
and lookup_date = current_date
and major_json->>'description' = 'Buick & GMC Consumer Offer'