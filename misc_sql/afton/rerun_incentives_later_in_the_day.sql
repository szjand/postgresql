﻿/*
8/15/19
  taylor says suburbans and tahoes have changes, need to rerun them
*/  


-- generate the vins
-- substitute this query in Function: gmgl.vin_incentive_scraper_vins()
  select inpmast_vin, case when inpmast_stock_number like '%R' then true else false end as ctp
  from arkona.xfm_inpmast a
  left join gmgl.major_incentives b on a.inpmast_vin = b.vin
    and b.lookup_date = current_date   
  left join gmgl.vin_recalls c on a.inpmast_vin = c.vin
    and c.lookup_date = current_date 
  left join gmgl.vin_incentive_not_found d on a.inpmast_vin = d.vin
    and d.lookup_date = current_date 
  where status = 'I'
    and type_n_u = 'N'
    and current_row = true
    and model in ('suburban', 'tahoe')
    and b.vin is null   
    and c.vin is null 
    and d.vin is null       
  order by case when inpmast_stock_number like '%R' then 2 else 1 end;


-- data to delete
-- incentive data (major_incentive_key) from today for suburbans and tahoes
drop table if exists major_keys;
create temp table major_keys as
select distinct major_incentive_key
from gmgl.major_incentives a 
join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
  and b.model in ('suburban','tahoe')
where a.lookup_date = current_date;

delete
-- select * 
from gmgl.stackable_incentives a
where major_incentive_key in (
  select major_incentive_key
  from major_keys);

  
delete
-- select * 
from gmgl.major_incentives a
where major_incentive_key in (
  select major_incentive_key
  from major_keys);


  
/*
8/27/19

They released new programs today for Labor Day. Would one of you be able to please re run the scraper as soon as possible? Programs added for the following:

-	2019 Silverado 1500 Crew 1LT/1SP
-	2019 Silverado 1500 Double 
-	2019 Trax
-	2019/2020 Equinox 
-	2019/2020 Traverse
-	All GMC
*/


select distinct model, body_style from arkona.xfm_inpmast where current_row and year = 2019 and model like '%silverado 1500%' 

-- incentive data (major_incentive_key) from today for suburbans and tahoes
drop table if exists major_keys;
create temp table major_keys as
select distinct major_incentive_key
from gmgl.major_incentives a 
join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
  and (
     (model in ('EQUINOX','TRAVERSE', 'TRAX', 'SILVERADO 1500', 'SILVERADO 1500 LD') and year = 2019)
     or
     (model in ('EQUINOX','TRAVERSE') and year = 2020)
     or
     (make = 'GMC'))
where a.lookup_date = current_date;

these 2 delete queries took 7 minutes to run
delete
-- select * 
from gmgl.stackable_incentives a
where major_incentive_key in (
  select major_incentive_key
  from major_keys);
delete
-- select * 
from gmgl.major_incentives a
where major_incentive_key in (
  select major_incentive_key
  from major_keys);  
  

-- substitute this query in Function: gmgl.vin_incentive_scraper_vins()
  select inpmast_vin, case when inpmast_stock_number like '%R' then true else false end as ctp
  -- select distinct year, make, model
  from arkona.xfm_inpmast a
  left join gmgl.major_incentives b on a.inpmast_vin = b.vin
    and b.lookup_date = current_date   
  left join gmgl.vin_recalls c on a.inpmast_vin = c.vin
    and c.lookup_date = current_date 
  left join gmgl.vin_incentive_not_found d on a.inpmast_vin = d.vin
    and d.lookup_date = current_date 
  where status = 'I'
    and type_n_u = 'N'
    and current_row = true
  and (
     (model in ('EQUINOX','TRAVERSE', 'TRAX', 'SILVERADO 1500', 'SILVERADO 1500 LD') and year = 2019)
     or
     (model in ('EQUINOX','TRAVERSE') and year = 2020)
     or
     (make = 'GMC'))
    and b.vin is null   
    and c.vin is null 
    and d.vin is null       
    and inpmast_vin not in ('3GKALVEV5LL106226','1GTU9FEL7KZ359373')
  order by case when inpmast_stock_number like '%R' then 2 else 1 end;  


  