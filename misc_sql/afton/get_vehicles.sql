﻿/*
gmgl.get_vehicles: populates the pricing/inventory pages

-- Function: gmgl.get_vehicles()

-- DROP FUNCTION gmgl.get_vehicles();

CREATE OR REPLACE FUNCTION gmgl.get_vehicles()
  RETURNS SETOF json AS
$BODY$


delete from gmgl.get_vehicles_log where field1::date < current_date - 7;
insert into gmgl.get_vehicles_log select now();  

with _major as (
  select c.program_start_date::timestamp with time zone,c.program_end_date::timestamp with time zone,a.major_incentive_key as id,a.vin as vehicle, c.program_pdf,c.program_number,
    c.program_name, b.is_best_incentive, total_cash,total_stackable as stackable_cash, 
    (total_incentives - cadillac_four_percent) as total_incentives, total_major, sup_inc as supinc, emp_inc as empinc,
    case 
      when sup_inc > 0 
        then true 
      else false 
    end as has_sup_inc,
        case 
      when emp_inc > 0 
        then true 
      else false 
    end as has_emp_inc
  from gmgl.vin_incentive_cash b
  inner join gmgl.major_incentives a on b.major_incentive_key = a.major_incentive_key
  inner join gmgl.incentive_programs c on a.program_key  = c.program_key
  where a.vin is not null
    and b.thru_ts > now()),
    
 _stackable as (
  select b.ember_id, major_incentive_key,acknowledged
  from gmgl.incentive_program_rules a
  inner join gmgl.stackable_incentives b on a.program_key = b.program_key
    and lookup_date = current_date
  inner join gmgl.incentive_programs d on a.program_key = d.program_key
  inner join _major c on b.major_incentive_key = c.id
  where current_row = true
  group by ember_id, major_incentive_key,d.acknowledged
  order by d.acknowledged ),
  
_incentive_side_load as (
  select json_agg(row_to_json(A))
  from ( -- A
    select * , array ( -- Array
      select ember_id
      from _stackable
      where major_incentive_key = id
      order by acknowledged) as vehicle_stackable_incentives
    from _major ) A ),

    _vehicle_first_pictures as (
    select vin, url
    from nrv.new_vehicle_pictures
    where sequence_number =1 
    and thru_ts > now()),

    _vins_without_acknowledged_rebates as(
    select a.vin
      from gmgl.daily_inventory a
      inner join gmgl.major_incentives b on a.vin = b.vin and b.lookup_date = current_date
      inner join gmgl.stackable_incentives d on b.major_incentive_key = d.major_incentive_key
      left join gmgl.incentive_programs c on b.program_key = c.program_key and c.acknowledged = false
      left join gmgl.incentive_programs e on d.program_key = e.program_key and e.acknowledged = false
      where c.acknowledged = false or e.acknowledged = false
      group by a.vin)


select row_to_json(B)
from ( -- B
  select json_agg(row_to_json(A)) as vehicles, (
  -- Side Loading Incentives
    select *
    from _incentive_side_load) as vehicle_major_incentives,
    (
    select json_agg(row_to_json(A))
    from (select user_key as user, comment_ts as date, 
    comment_text as comment, vin as vehicle, id
    from gmgl.vehicle_comments) A ) as vehicle_comments
  from ( -- A
    select a.vin as id, a.vin, case when margin is null then true else false end as missing_margin, (select keys.get_key_location(a.stock_number)) as key_location, a.days_on_ground,a.stock_number, a.days_in_accounting as days_in_inventory, a.vehicle_cost as cost, b.body_type as body,
      b.best_price, b.best_price_diff, b.model_year as year, b.model_trim as trim, b.model, b.make, b.color,
      (b.total_incentives - b.cadillac_four_percent) as total_incentives, 
      (b.total_incentives - b.cadillac_four_percent + sup_inc + emp_inc) as total_incentives_plus_inc, 
      b.total_cash, b.total_stackable, b.total_major, b.margin, b.price, b.total_msrp as msrp, 
      b.aftermarket, b.sup_inc as supinc, b.emp_inc as empinc, b.cadillac_four_percent as cadillac_incentive,a.engine_size,
      a.drive_type,case when a.has_recall = true then 'text-danger' when hh.vin is not null or ff.stock_number is not null then 'text-warning' else 'text-success' end as vehicle_status,
      case when a.has_recall = true then 'Recall' when hh.vin is not null then 'On Sold Board' when ff.stock_number is not null then 'On Pending Board' end as status_description,
      b.has_inc, b.has_sup_inc, b.has_emp_inc,b.website_url as rydell_cars_url,
       price as rydell_best_price,price_before_incentives,dealer_cash, dealer_cash_checked,testing_price,
       round(msrp - price_before_incentives + coalesce(a.aftermarket,0)) as rydell_discount,
       price_before_incentives_consultant,price_leader, pick_of_the_week, case when price_leader = true or pick_of_the_week = true then true else false end as has_specials,
       round(msrp - price_before_incentives_consultant + coalesce(a.aftermarket,0)) as rydell_discount_consultant, total_customer_cash,
      a.supplr,a.employ, in_transit, is_retail, is_sold, is_fleet, a.order_type, advertised_price,
      coalesce(a.has_recall, false) as has_recall, ad_price_diff,invoice,has_ad_covenant, vehicle_estimated_cost, estimated_aftermarket, estimated_aftermarket_notes,
      case when estimated_aftermarket <> 0 or a.aftermarket <> 0 then true else false end as has_any_aftermarket,
      url as picture_url,total_incentives_for_consultants,
      ad_price_value as ad_covenant_value, (
      select comment_text
      from gmgl.vehicle_comments hh
      where hh.vin = a.vin 
      and comment_ts = (
      select max(comment_ts)
      from gmgl.vehicle_comments gg
      where gg.vin = a.vin)
      ) as latest_comment,
      (
      select comment_ts
      from gmgl.vehicle_comments hh
      where hh.vin = a.vin 
      and comment_ts = (
      select max(comment_ts)
      from gmgl.vehicle_comments gg
      where gg.vin = a.vin)
      ) as latest_comment_ts,
      case 
        when b.emp_diff < 0 or b.sup_diff < 0 or coalesce(a.has_recall, false) = true then true else false end as has_error,
      case 
        when b.emp_diff < 0
          then true
        else false end as incorrect_empl,
    --     RENAME to sup_diff_error
      case 
        when b.sup_diff < 0
          then true
        else false end as incorrect_supplier,
      case
        when b.best_price_diff > 1 or b.best_price_diff < -1
          then true 
        else false 
        end as is_best_price_diff,
     case
        when (b.ad_price_diff > 1 or b.ad_price_diff < -1) and a.order_type = 'Retail' or a.order_type is null
          then true 
        else false 
        end as has_ad_price_diff,
        case 
          when gg.vin is not null 
            then false
          else true
          end as is_reviewed,
        case 
          when gg.vin is not null 
            then reason
          end as reviewed_reason,
      array ( -- Vehicle Major Incentives
        select ww.id
        from _major ww
        where a.vin = ww.vehicle) as vehicle_major_incentives,
      array ( -- Vehicle Comments
        select kk.id
        from gmgl.vehicle_comments kk
        where kk.vin = a.vin) as vehicle_comments,
        case when a.stock_number like '%R%' then true else false end as is_ctp,coalesce(model_code,'None') as model_code,coalesce(peg,'N/A') as peg,
        case when ii.vin is not null then true else false end as pending_rebate_acknowledgement
    from gmgl.daily_inventory a
    left join gmgl.get_vehicle_prices() b on a.vin = b.vin
    left join gmgl.vehicle_orders zz on a.vin = zz.vin
    left join gmgl.reviewed_vins gg on a.vin = gg.vin 
      and gg.thru_ts > now() 
    left join board.sales_board hh on a.vin = hh.vin 
      and hh.is_deleted = false 
      and is_backed_on = false
      and board_type_key not in (19,11,12)
    left join board.board_types hhh on hh.board_type_key = hhh.board_type_key
    left join board.pending_boards ff on a.stock_number = ff.stock_number
      and ff.is_deleted = false
      and is_delivered = false
    left join _vehicle_first_pictures c on a.vin = c.vin
    left join _vins_without_acknowledged_rebates ii on a.vin = ii.vin
    where a.make is not null and a.model is not null and a.color is not null
  ) A
) B
  $BODY$
  LANGUAGE sql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION gmgl.get_vehicles()
  OWNER TO rydell;

*/
with _major as (
  select c.program_start_date::timestamp with time zone,c.program_end_date::timestamp with time zone,a.major_incentive_key as id,a.vin as vehicle, c.program_pdf,c.program_number,
    c.program_name, b.is_best_incentive, total_cash,total_stackable as stackable_cash, 
    (total_incentives - cadillac_four_percent) as total_incentives, total_major, sup_inc as supinc, emp_inc as empinc,
    case 
      when sup_inc > 0 
        then true 
      else false 
    end as has_sup_inc,
        case 
      when emp_inc > 0 
        then true 
      else false 
    end as has_emp_inc
  from gmgl.vin_incentive_cash b
  inner join gmgl.major_incentives a on b.major_incentive_key = a.major_incentive_key
  inner join gmgl.incentive_programs c on a.program_key  = c.program_key
  where a.vin = '1GCUYDED0KZ220394'
    and b.thru_ts > now()),
    
 _stackable as (
  select b.ember_id, major_incentive_key,acknowledged
  from gmgl.incentive_program_rules a
  inner join gmgl.stackable_incentives b on a.program_key = b.program_key
    and lookup_date = current_date
  inner join gmgl.incentive_programs d on a.program_key = d.program_key
  inner join _major c on b.major_incentive_key = c.id
  where current_row = true
  group by ember_id, major_incentive_key,d.acknowledged
  order by d.acknowledged ),

_incentive_side_load as (
  select json_agg(row_to_json(A))
  from ( -- A
    select * , array ( -- Array
      select ember_id
      from _stackable
      where major_incentive_key = id
      order by acknowledged) as vehicle_stackable_incentives
    from _major ) A )  
  
select * from _incentive_side_load


-- select row_to_json(B)
-- from ( -- B
--   select json_agg(row_to_json(A)) as vehicles
--   from ( -- A
    select a.vin as id, a.vin, 
      case when margin is null then true else false end as missing_margin, (select keys.get_key_location(a.stock_number)) as key_location, a.days_on_ground,a.stock_number, a.days_in_accounting as days_in_inventory, a.vehicle_cost as cost, b.body_type as body,
      b.best_price, b.best_price_diff, b.model_year as year, b.model_trim as trim, b.model, b.make, b.color,
      (b.total_incentives - b.cadillac_four_percent) as total_incentives, 
      (b.total_incentives - b.cadillac_four_percent + sup_inc + emp_inc) as total_incentives_plus_inc, 
      b.total_cash, b.total_stackable, b.total_major, b.margin, b.price, b.total_msrp as msrp, 
      b.aftermarket, b.sup_inc as supinc, b.emp_inc as empinc, b.cadillac_four_percent as cadillac_incentive,a.engine_size,
      a.drive_type,case when a.has_recall = true then 'text-danger' when hh.vin is not null or ff.stock_number is not null then 'text-warning' else 'text-success' end as vehicle_status,
      case when a.has_recall = true then 'Recall' when hh.vin is not null then 'On Sold Board' when ff.stock_number is not null then 'On Pending Board' end as status_description,
      b.has_inc, b.has_sup_inc, b.has_emp_inc,b.website_url as rydell_cars_url,
       price as rydell_best_price,price_before_incentives,dealer_cash, dealer_cash_checked,testing_price,
       round(msrp - price_before_incentives + coalesce(a.aftermarket,0)) as rydell_discount,
       price_before_incentives_consultant,price_leader, pick_of_the_week, case when price_leader = true or pick_of_the_week = true then true else false end as has_specials,
       round(msrp - price_before_incentives_consultant + coalesce(a.aftermarket,0)) as rydell_discount_consultant, total_customer_cash,
      a.supplr,a.employ, in_transit, is_retail, is_sold, is_fleet, a.order_type, advertised_price,
      coalesce(a.has_recall, false) as has_recall, ad_price_diff,invoice,has_ad_covenant, vehicle_estimated_cost, estimated_aftermarket, estimated_aftermarket_notes,
      case when estimated_aftermarket <> 0 or a.aftermarket <> 0 then true else false end as has_any_aftermarket,
--       url as picture_url,total_incentives_for_consultants,
      ad_price_value as ad_covenant_value, (
      select comment_text
      from gmgl.vehicle_comments hh
      where hh.vin = a.vin 
      and comment_ts = (
      select max(comment_ts)
      from gmgl.vehicle_comments gg
      where gg.vin = a.vin)
      ) as latest_comment,
      (
      select comment_ts
      from gmgl.vehicle_comments hh
      where hh.vin = a.vin 
      and comment_ts = (
      select max(comment_ts)
      from gmgl.vehicle_comments gg
      where gg.vin = a.vin)
      ) as latest_comment_ts,
      case 
        when b.emp_diff < 0 or b.sup_diff < 0 or coalesce(a.has_recall, false) = true then true else false end as has_error,
      case 
        when b.emp_diff < 0
          then true
        else false end as incorrect_empl,
    --     RENAME to sup_diff_error
      case 
        when b.sup_diff < 0
          then true
        else false end as incorrect_supplier,
      case
        when b.best_price_diff > 1 or b.best_price_diff < -1
          then true 
        else false 
        end as is_best_price_diff,
     case
        when (b.ad_price_diff > 1 or b.ad_price_diff < -1) and a.order_type = 'Retail' or a.order_type is null
          then true 
        else false 
        end as has_ad_price_diff,
        case 
          when gg.vin is not null 
            then false
          else true
          end as is_reviewed,
        case 
          when gg.vin is not null 
            then reason
          end as reviewed_reason,
      array ( -- Vehicle Major Incentives
        select ww.id
        from _major ww
        where a.vin = ww.vehicle) as vehicle_major_incentives,
      array ( -- Vehicle Comments
        select kk.id
        from gmgl.vehicle_comments kk
        where kk.vin = a.vin) as vehicle_comments,
        case when a.stock_number like '%R%' then true else false end as is_ctp,coalesce(model_code,'None') as model_code,coalesce(peg,'N/A') as peg
--         case when ii.vin is not null then true else false end as pending_rebate_acknowledgement
    from gmgl.daily_inventory a
    left join gmgl.get_vehicle_prices() b on a.vin = b.vin
    left join gmgl.vehicle_orders zz on a.vin = zz.vin
    left join gmgl.reviewed_vins gg on a.vin = gg.vin 
      and gg.thru_ts > now() 
    left join board.sales_board hh on a.vin = hh.vin 
      and hh.is_deleted = false 
      and is_backed_on = false
      and board_type_key not in (19,11,12)
    left join board.board_types hhh on hh.board_type_key = hhh.board_type_key
    left join board.pending_boards ff on a.stock_number = ff.stock_number
      and ff.is_deleted = false
      and is_delivered = false
--     left join _vehicle_first_pictures c on a.vin = c.vin
--     left join _vins_without_acknowledged_rebates ii on a.vin = ii.vin
--     where a.make is not null and a.model is not null and a.color is not null
    where a.vin = '1GCUYDED0KZ220394'
--   ) A
-- ) B


select * 
from gmgl.incentive_programs a
join gmgl.major_incentives b on a.program_key = b.program_key
  and b.vin = '1GCUYDED0KZ220394'
  and b.lookup_date = current_date - 1
where a.lookup_ts::date = current_date - 1  



