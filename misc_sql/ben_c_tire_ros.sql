﻿SELECT ro
-- SELECT COUNT(DISTINCT ro)
FROM ads.ext_fact_repair_order a
INNER JOIN dds.dim_date b on a.finalclosedatekey = b.date_key
  AND b.year_month IN (201607, 201608, 201609, 201707, 201708, 201709)
INNER JOIN (
  SELECT opcodekey
  FROM ads.ext_dim_opcode
  WHERE opcode = '4nt'
    AND storecode = 'ry1') c on a.opcodekey = c.opcodekey   
group by ro    


select c.year_month, a.control, a.amount,
  count(year_month) over (partition by year_month)
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
  and b.account_type = 'Sale'
inner join dds.dim_date c on a.date_key = c.date_key  
where a.control in (
  SELECT ro
  -- SELECT COUNT(DISTINCT ro)
  FROM ads.ext_fact_repair_order a
  INNER JOIN dds.dim_date b on a.finalclosedatekey = b.date_key
    AND b.year_month IN (201607, 201608, 201609, 201707, 201708, 201709)
  INNER JOIN (
    SELECT opcodekey
    FROM ads.ext_dim_opcode
    WHERE opcode = '4nt'
      AND storecode = 'ry1') c on a.opcodekey = c.opcodekey   
  group by ro)
order by year_month, control



select c.year_month, sum(-a.amount) as sales, count(distinct control) as ro_count, (sum(-a.amount)/count(distinct control))::integer as avg_per_ro
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
  and b.account_type = 'Sale'
inner join dds.dim_date c on a.date_key = c.date_key  
  and c.year_month in (201607, 201608, 201609, 201707, 201708, 201709)
where a.control in (
  SELECT ro
  -- SELECT COUNT(DISTINCT ro)
  FROM ads.ext_fact_repair_order a
  INNER JOIN dds.dim_date b on a.finalclosedatekey = b.date_key
    AND b.year_month IN (201607, 201608, 201609, 201707, 201708, 201709)
  INNER JOIN (
    SELECT opcodekey
    FROM ads.ext_dim_opcode
    WHERE opcode = '4nt'
      AND storecode = 'ry1') c on a.opcodekey = c.opcodekey   
  group by ro)
group by year_month