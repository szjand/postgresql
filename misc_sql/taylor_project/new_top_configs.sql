﻿/*
top configs
by allocation group
narrow it down to top configs all colors
sales by month for 2019

start with sierras
what are the allocation groups for 2019/2020 sierra 1500s

select distinct alloc_group 
from jon.configurations
where model like 'sierra 1%'
  and model_year > 2018

select  *
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
join jon.configurations c on b.chrome_style_id = c.chrome_style_id
  and c.model like 'sierra 1%'
  and c.model_year > 2018
where extract(year from a.delivery_Date) = 2019 
  and a.sale_type in ('retail','fleet')


select d.year_month, c.alloc_group, c.cab, c.drive, c.box, c.trim_level, b.color, count(*)
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
join jon.configurations c on b.chrome_style_id = c.chrome_style_id
  and c.model like 'sierra 1%'
  and c.model_year > 2018
join dds.dim_date d on a.delivery_date = d.the_date  
where extract(year from a.delivery_Date) = 2019 
  and a.sale_type in ('retail','fleet')  
group by d.year_month, c.alloc_group, c.cab, c.drive, c.box, c.trim_level, b.color  

select  *
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
  and b.model like 'sierra 1%'
  and b.model_year > 2018
where extract(year from a.delivery_Date) = 2019 
  and a.sale_type in ('retail','fleet')    

select *
from jon.configurations a
where a.model like 'sierra 1%'
  and a.model_year > 2018 


select the_date, status, count(*)
from nc.daily_inventory  
where extract(year from the_date) = 2020
  and status like 'inv%'
  and model like 'sierra 1%'
group by the_date, status
order by the_date, status

select status
from nc.daily_inventory
group by status


start with configurations
add monthly sales
not sure how to add availability


-- here are sales
-- since we are doing all of 2019, need to include model_year 2018
-- sale defined as retail or fleet
-- wait a minute, i don't care about the year model, if its a sale in 2019 it is in
drop table if exists sales;
create temp table sales as
select *
from (
  select a.chrome_style_id, a.model_year, a.alloc_group, a.cab, a.trim_level, 
    b.vin, b.engine, b.color, b.drive,
    (select year_month from dds.dim_date where the_date = c.delivery_date)
  from jon.configurations a
  left join nc.vehicles b on a.chrome_style_id = b.chrome_style_id
  left join (
    select vin, max(delivery_date) as delivery_date
    from nc.vehicle_sales
    where sale_type in ('fleet','retail')
      and extract(year from delivery_date) = 2019
    group by vin) c on b.vin = c.vin
  where a.model = 'sierra 1500') d
--     and a.model_year > 2017) d
where year_month is not null  

select * 
from sales


select alloc_group,
  count(*) filter (where year_month = 201901) as jan,
  count(*) filter (where year_month = 201902) as feb,
  count(*) filter (where year_month = 201903) as mar,
  count(*) filter (where year_month = 201904) as apr,
  count(*) filter (where year_month = 201905) as may,
  count(*) filter (where year_month = 201906) as jun,
  count(*) filter (where year_month = 201907) as jul,
  count(*) filter (where year_month = 201908) as aug,
  count(*) filter (where year_month = 201909) as sep,
  count(*) filter (where year_month = 201910) as oct,
  count(*) filter (where year_month = 201911) as nov,
  count(*) filter (where year_month = 201912) as dec,
  count(*) as total
from sales
group by alloc_group
order by count(*) desc


need to deal with alloc group as the top level
but, for sierra at least, multiple allocation groups based on model
so, need to expose or just use the fact that for sierras, alloc group is essentially just the cab plus light or heavy duty
so, for sierra 1500, the cab becomes the top level


select cab,
  count(*) filter (where year_month = 201901) as jan,
  count(*) filter (where year_month = 201902) as feb,
  count(*) filter (where year_month = 201903) as mar,
  count(*) filter (where year_month = 201904) as apr,
  count(*) filter (where year_month = 201905) as may,
  count(*) filter (where year_month = 201906) as jun,
  count(*) filter (where year_month = 201907) as jul,
  count(*) filter (where year_month = 201908) as aug,
  count(*) filter (where year_month = 201909) as sep,
  count(*) filter (where year_month = 201910) as oct,
  count(*) filter (where year_month = 201911) as nov,
  count(*) filter (where year_month = 201912) as dec
from sales
group by cab

-- how many sierras on the ground each day
drop table if exists inventory_on_ground;
create temp table inventory_on_ground as
select bb.cab, a.the_date, count(*) as units
from nc.daily_inventory a
join nc.vehicles b on a.vin = b.vin
join nc.vehicle_configurations bb on b.configuration_id = bb.configuration_id
join dds.dim_date c on a.the_date = c.the_date
where c.the_year = 2019
  and a.status = 'inv_og'
  and a.model = 'sierra 1500'
group by bb.cab, a.the_date  
order by bb.cab, a.the_date;

select * 
from inventory_on_ground
where cab = 'crew'

select distinct cab from inventory_on_ground

-- on 12/01/2019, 30 crew cabs on the ground ?!?!?
-- ok, 31, close enough
select *
from nc.vehicle_acquisitions a 
join nc.vehicles b on a.vin = b.vin
  and b.model = 'sierra 1500'
  and b.cab = 'crew'
where '12/01/2019' between a.ground_date and a.thru_date


-- needc to use the dates from nc.inventory, if a missed a day updating the table,
-- the date will be missing
select a.the_date
from nc.daily_inventory a
join dds.dim_date b on a.the_date = b.the_date
  and b.the_year = 2019
group by a.the_date  
order by a.the_date  

-- here are the missing dates
select a.the_date, b.the_date
from dds.dim_date a
left join ( -- dates from nc.inventory
  select a.the_date
  from nc.daily_inventory a
  join dds.dim_date b on a.the_date = b.the_date
    and b.the_year = 2019
  group by a.the_date) b on a.the_date = b.the_date
where a.the_year = 2019
  and b.the_date is null
order by a.the_date

-- on 12/01/2019, 30 crew cabs on the ground ?!?!?
-- ok, 31, close enough
select *
from nc.vehicle_acquisitions a 
join nc.vehicles b on a.vin = b.vin
  and b.model = 'sierra 1500'
  and b.cab = 'crew'
where '12/01/2019' between a.ground_date and a.thru_date

-- 2019 dates
-- here are the missing dates
drop table if exists missing_dates cascade;
create temp table missing_dates as
select a.the_date-- , b.the_date
from dds.dim_date a
left join ( -- dates from nc.inventory
  select a.the_date
  from nc.daily_inventory a
  join dds.dim_date b on a.the_date = b.the_date
    and b.the_year = 2019
  group by a.the_date) b on a.the_date = b.the_date
where a.the_year = 2019
  and b.the_date is null
order by a.the_date;

select * from missing_dates;

-- fill in the missing days with data from nc.vehicle_acquisitions
select a.*, b.*
from nc.vehicle_acquisitions a 
join nc.vehicles b on a.vin = b.vin
  and b.model = 'sierra 1500'
  and b.cab = 'crew'
where a.ground_date < '01/01/2020'
  and a.thru_date > '12/31/2018'
order by a.thru_date

select c.the_date,
  count(c.the_date) filter (where b.cab = 'crew') as crew,
  count(c.the_date) filter (where b.cab = 'double') as dbl,
  count(c.the_date) filter (where b.cab = 'reg') as reg
from nc.vehicle_acquisitions a 
join nc.vehicles b on a.vin = b.vin
  and b.model = 'sierra 1500'
  and b.cab = 'crew'
join missing_dates c on a.ground_date <= c.the_date
  and a.thru_date > c.the_date  
group by c.the_date

-- 2019 dates
-- here are the missing dates
select a.the_date, b.the_date, 
  coalesce(max(case when c.cab = 'crew' then units end), 0) as crew,
  coalesce(max(case when c.cab = 'double' then units end), 0) as dbl,
  coalesce(max(case when c.cab = 'reg' then units end), 0) as reg
from dds.dim_date a
left join ( -- dates from nc.inventory
  select a.the_date
  from nc.daily_inventory a
  join dds.dim_date b on a.the_date = b.the_date
    and b.the_year = 2019
  group by a.the_date) b on a.the_date = b.the_date
left join inventory_on_ground c on a.the_date = c.the_date
--   and c.cab = 'crew'  
where a.the_year = 2019
group by a.the_date, b.the_date
order by a.the_date

-- fuck it too much fucking around, just use acquisitions and the calendar
-- looking to start with a dataset of full inventory every day
select a.the_date, count(*)
from dds.dim_date a
left join nc.vehicle_acquisitions b on b.ground_date <= a.the_date
  and b.thru_date > a.the_date
where a.the_year = 2019
group by a.the_Date

-- going to want to breakout by model, cab, trim, engine, color
-- start out with an inner join,  then join that to a calendar
drop table if exists inv_og;
create temp table inv_og as
select a.year_month, a.the_date, d.chrome_style_id, d.model_year, d.make, d.model, d.cab, d.trim_level, d.engine, c.color, e.peg, e.alloc_group
from dds.dim_date a
join nc.vehicle_acquisitions b on b.ground_date <= a.the_date
  and b.thru_date > a.the_date
join nc.vehicles c on b.vin = c.vin  
  and make not in ('honda','nissan')
join nc.vehicle_configurations d on c.configuration_id = d.configuration_id
join jon.configurations e on d.chrome_style_id = e.chrome_style_id
where a.the_year = 2019

-- daily equinox inventory
select *
from inv_og
where model = 'sierra 1500'
-- we don't have inventory every day for double cabs
select the_date, year_month, count(*) as units
from inv_og
where model = 'sierra 1500'
  and cab = 'double'
group by the_date, year_month

-- days stocked out-- copy this down below and try to streamline it
select aa.year_month,coalesce(max(bb.days_stocked_out),0)
from dds.dim_date aa
left join (
  select a.year_month, count(*) as days_stocked_out
  from dds.dim_date a
  left join (
    select the_date, count(*) as units
    from inv_og
    where model = 'sierra 1500'
      and cab = 'reg'
    group by the_date) b on a.the_date = b.the_date
  where a.the_year = 2019  
    and b.the_date is null
  group by a.year_month) bb on aa.year_month = bb.year_month  
where aa.the_year = 2019  
group by aa.year_month

*/

-- 1/10/20  add model_code to both tables

drop table if exists inv_og cascade;
create temp table inv_og as
select a.year_month, a.the_date, d.chrome_style_id, d.model_year, d.make, d.model, d.model_code, 
  d. drive, d.cab, d.trim_level, d.engine, c.color, e.peg, e.alloc_group
from dds.dim_date a
join nc.vehicle_acquisitions b on b.ground_date <= a.the_date
  and b.thru_date > a.the_date
join nc.vehicles c on b.vin = c.vin  
  and make not in ('honda','nissan')
join nc.vehicle_configurations d on c.configuration_id = d.configuration_id
join jon.configurations e on d.chrome_style_id = e.chrome_style_id
where a.the_year = 2019;
create index on inv_og(chrome_style_id);
create index on inv_og(the_date);
create index on inv_og(drive);
create index on inv_og(cab);

 -- 2019 sales
drop table if exists sales;
create temp table sales as
select b.year_month, a.delivery_date, d.chrome_style_id, d.model_year, d.make, d.model, d.model_code,
  d.drive, d.cab, d.trim_level, d.engine, c.color, e.peg, e.alloc_group
from (
  select vin, max(delivery_date) as delivery_date
  from nc.vehicle_sales a
  join dds.dim_date b on a.delivery_date = b.the_date
    and b.the_year = 2019
  where sale_type in ('retail','fleet')
  group by a.vin) a
join nc.vehicle_sales aa on a.vin = aa.vin
  and a.delivery_date = aa.delivery_date  
join dds.dim_date b on a.delivery_date = b.the_date  
join nc.vehicles c on a.vin = c.vin  
  and c.make not in ('honda','nissan')
join nc.vehicle_configurations d on c.configuration_id = d.configuration_id
join jon.configurations e on d.chrome_style_id = e.chrome_style_id;


select * 
from sales
------------------------------------------------------------------------------
--< all sierra 1500 sold in 2019
------------------------------------------------------------------------------
select 1 as seq, null::citext as cab,null::citext as trim_level,null::citext as engine,null::citext as color,
  count(*) filter (where year_month = 201901) as jan,
  count(*) filter (where year_month = 201902) as feb,
  count(*) filter (where year_month = 201903) as mar,
  count(*) filter (where year_month = 201904) as apr,
  count(*) filter (where year_month = 201905) as may,
  count(*) filter (where year_month = 201906) as jun,
  count(*) filter (where year_month = 201907) as jul,
  count(*) filter (where year_month = 201908) as aug,
  count(*) filter (where year_month = 201909) as sep,
  count(*) filter (where year_month = 201910) as oct,
  count(*) filter (where year_month = 201911) as nov,
  count(*) filter (where year_month = 201912) as dec,
  count(*) as total
from sales
where model = 'sierra 1500'
union  -- cab
select 2, initcap(cab),null::citext as trim_level,null::citext as engine,null::citext as color,
  count(*) filter (where year_month = 201901) as jan,
  count(*) filter (where year_month = 201902) as feb,
  count(*) filter (where year_month = 201903) as mar,
  count(*) filter (where year_month = 201904) as apr,
  count(*) filter (where year_month = 201905) as may,
  count(*) filter (where year_month = 201906) as jun,
  count(*) filter (where year_month = 201907) as jul,
  count(*) filter (where year_month = 201908) as aug,
  count(*) filter (where year_month = 201909) as sep,
  count(*) filter (where year_month = 201910) as oct,
  count(*) filter (where year_month = 201911) as nov,
  count(*) filter (where year_month = 201912) as dec,
  count(*) as total
from sales
where model = 'sierra 1500'
group by cab
union -- trim
select 3, initcap(cab), trim_level,null::citext as engine,null::citext as color,
  count(*) filter (where year_month = 201901) as jan,
  count(*) filter (where year_month = 201902) as feb,
  count(*) filter (where year_month = 201903) as mar,
  count(*) filter (where year_month = 201904) as apr,
  count(*) filter (where year_month = 201905) as may,
  count(*) filter (where year_month = 201906) as jun,
  count(*) filter (where year_month = 201907) as jul,
  count(*) filter (where year_month = 201908) as aug,
  count(*) filter (where year_month = 201909) as sep,
  count(*) filter (where year_month = 201910) as oct,
  count(*) filter (where year_month = 201911) as nov,
  count(*) filter (where year_month = 201912) as dec,
  count(*) as total
from sales
where model = 'sierra 1500' and cab = 'crew'
group by cab, trim_level
union
select 4, initcap(cab), trim_level, engine,null::citext as color,
  count(*) filter (where year_month = 201901) as jan,
  count(*) filter (where year_month = 201902) as feb,
  count(*) filter (where year_month = 201903) as mar,
  count(*) filter (where year_month = 201904) as apr,
  count(*) filter (where year_month = 201905) as may,
  count(*) filter (where year_month = 201906) as jun,
  count(*) filter (where year_month = 201907) as jul,
  count(*) filter (where year_month = 201908) as aug,
  count(*) filter (where year_month = 201909) as sep,
  count(*) filter (where year_month = 201910) as oct,
  count(*) filter (where year_month = 201911) as nov,
  count(*) filter (where year_month = 201912) as dec,
  count(*) as total
from sales  -- engine
where model = 'sierra 1500' and cab = 'crew' and trim_level in ('slt','denali','at4')
group by cab, trim_level, engine
union
select 5, initcap(cab), trim_level, engine, initcap(color),
  count(*) filter (where year_month = 201901) as jan,
  count(*) filter (where year_month = 201902) as feb,
  count(*) filter (where year_month = 201903) as mar,
  count(*) filter (where year_month = 201904) as apr,
  count(*) filter (where year_month = 201905) as may,
  count(*) filter (where year_month = 201906) as jun,
  count(*) filter (where year_month = 201907) as jul,
  count(*) filter (where year_month = 201908) as aug,
  count(*) filter (where year_month = 201909) as sep,
  count(*) filter (where year_month = 201910) as oct,
  count(*) filter (where year_month = 201911) as nov,
  count(*) filter (where year_month = 201912) as dec,
  count(*) as total
from sales
where model = 'sierra 1500' and cab = 'crew'
  and (
    (trim_level = 'slt')
    or
    (trim_level in ('at4', 'denali') and engine = '6.2'))
group by cab, trim_level, engine, color
order by seq, total desc

------------------------------------------------------------------------------
--/> all sierra 1500 sold in 2019
------------------------------------------------------------------------------

select * from sales limit 100
------------------------------------------------------------------------------
--< all equinox sold in 2019
------------------------------------------------------------------------------
select 1 as seq, null::citext as drive,null::citext as trim_level,null::citext as engine,null::citext as color,
  count(*) filter (where year_month = 201901) as jan,
  count(*) filter (where year_month = 201902) as feb,
  count(*) filter (where year_month = 201903) as mar,
  count(*) filter (where year_month = 201904) as apr,
  count(*) filter (where year_month = 201905) as may,
  count(*) filter (where year_month = 201906) as jun,
  count(*) filter (where year_month = 201907) as jul,
  count(*) filter (where year_month = 201908) as aug,
  count(*) filter (where year_month = 201909) as sep,
  count(*) filter (where year_month = 201910) as oct,
  count(*) filter (where year_month = 201911) as nov,
  count(*) filter (where year_month = 201912) as dec,
  count(*) as total
from sales
where model = 'equinox'
union  -- drive
select 2, drive,null::citext as trim_level,null::citext as engine,null::citext as color,
  count(*) filter (where year_month = 201901) as jan,
  count(*) filter (where year_month = 201902) as feb,
  count(*) filter (where year_month = 201903) as mar,
  count(*) filter (where year_month = 201904) as apr,
  count(*) filter (where year_month = 201905) as may,
  count(*) filter (where year_month = 201906) as jun,
  count(*) filter (where year_month = 201907) as jul,
  count(*) filter (where year_month = 201908) as aug,
  count(*) filter (where year_month = 201909) as sep,
  count(*) filter (where year_month = 201910) as oct,
  count(*) filter (where year_month = 201911) as nov,
  count(*) filter (where year_month = 201912) as dec,
  count(*) as total
from sales
where model = 'equinox'
group by drive
union -- trim
select 3, drive, trim_level,null::citext as engine,null::citext as color,
  count(*) filter (where year_month = 201901) as jan,
  count(*) filter (where year_month = 201902) as feb,
  count(*) filter (where year_month = 201903) as mar,
  count(*) filter (where year_month = 201904) as apr,
  count(*) filter (where year_month = 201905) as may,
  count(*) filter (where year_month = 201906) as jun,
  count(*) filter (where year_month = 201907) as jul,
  count(*) filter (where year_month = 201908) as aug,
  count(*) filter (where year_month = 201909) as sep,
  count(*) filter (where year_month = 201910) as oct,
  count(*) filter (where year_month = 201911) as nov,
  count(*) filter (where year_month = 201912) as dec,
  count(*) as total
from sales
where model = 'equinox' 
group by drive, trim_level
union -- engine
select 4, drive, trim_level, engine,null::citext as color,
  count(*) filter (where year_month = 201901) as jan,
  count(*) filter (where year_month = 201902) as feb,
  count(*) filter (where year_month = 201903) as mar,
  count(*) filter (where year_month = 201904) as apr,
  count(*) filter (where year_month = 201905) as may,
  count(*) filter (where year_month = 201906) as jun,
  count(*) filter (where year_month = 201907) as jul,
  count(*) filter (where year_month = 201908) as aug,
  count(*) filter (where year_month = 201909) as sep,
  count(*) filter (where year_month = 201910) as oct,
  count(*) filter (where year_month = 201911) as nov,
  count(*) filter (where year_month = 201912) as dec,
  count(*) as total
from sales 
where model = 'equinox' 
group by drive, trim_level, engine -- order by total desc 
union
select 5, drive, trim_level, engine, initcap(color),
  count(*) filter (where year_month = 201901) as jan,
  count(*) filter (where year_month = 201902) as feb,
  count(*) filter (where year_month = 201903) as mar,
  count(*) filter (where year_month = 201904) as apr,
  count(*) filter (where year_month = 201905) as may,
  count(*) filter (where year_month = 201906) as jun,
  count(*) filter (where year_month = 201907) as jul,
  count(*) filter (where year_month = 201908) as aug,
  count(*) filter (where year_month = 201909) as sep,
  count(*) filter (where year_month = 201910) as oct,
  count(*) filter (where year_month = 201911) as nov,
  count(*) filter (where year_month = 201912) as dec,
  count(*) as total
from sales
where model = 'equinox' -- and cab = 'crew'
group by drive, trim_level, engine, color
order by seq, total desc

------------------------------------------------------------------------------
--/> all equinox sold in 2019
------------------------------------------------------------------------------

-- equinox trim, color only
select 2, trim_level, color,
  count(*) filter (where year_month = 201901) as jan,
  count(*) filter (where year_month = 201902) as feb,
  count(*) filter (where year_month = 201903) as mar,
  count(*) filter (where year_month = 201904) as apr,
  count(*) filter (where year_month = 201905) as may,
  count(*) filter (where year_month = 201906) as jun,
  count(*) filter (where year_month = 201907) as jul,
  count(*) filter (where year_month = 201908) as aug,
  count(*) filter (where year_month = 201909) as sep,
  count(*) filter (where year_month = 201910) as oct,
  count(*) filter (where year_month = 201911) as nov,
  count(*) filter (where year_month = 201912) as dec,
  count(*) as total
from sales
where model = 'equinox'
group by trim_level, color order by total desc


-- -- 1/10/20 while this is very cool, instead of cab, go for model code
-- -- nope, model code changes over years
-- select 1 as seq, null::citext as cab,null::citext as trim_level,null::citext as engine,null::citext as color,
--   count(*) filter (where year_month = 201901) as jan,
--   count(*) filter (where year_month = 201902) as feb,
--   count(*) filter (where year_month = 201903) as mar,
--   count(*) filter (where year_month = 201904) as apr,
--   count(*) filter (where year_month = 201905) as may,
--   count(*) filter (where year_month = 201906) as jun,
--   count(*) filter (where year_month = 201907) as jul,
--   count(*) filter (where year_month = 201908) as aug,
--   count(*) filter (where year_month = 201909) as sep,
--   count(*) filter (where year_month = 201910) as oct,
--   count(*) filter (where year_month = 201911) as nov,
--   count(*) filter (where year_month = 201912) as dec,
--   count(*) as total
-- from sales
-- where model = 'sierra 1500'
-- union
-- select 2, initcap(cab),null::citext as trim_level,null::citext as engine,null::citext as color,
--   count(*) filter (where year_month = 201901) as jan,
--   count(*) filter (where year_month = 201902) as feb,
--   count(*) filter (where year_month = 201903) as mar,
--   count(*) filter (where year_month = 201904) as apr,
--   count(*) filter (where year_month = 201905) as may,
--   count(*) filter (where year_month = 201906) as jun,
--   count(*) filter (where year_month = 201907) as jul,
--   count(*) filter (where year_month = 201908) as aug,
--   count(*) filter (where year_month = 201909) as sep,
--   count(*) filter (where year_month = 201910) as oct,
--   count(*) filter (where year_month = 201911) as nov,
--   count(*) filter (where year_month = 201912) as dec,
--   count(*) as total
-- from sales
-- where model = 'sierra 1500'
-- group by cab
-- union
-- select 3, initcap(cab),model_code::citext as trim_level,null::citext as engine,null::citext as color,
--   count(*) filter (where year_month = 201901) as jan,
--   count(*) filter (where year_month = 201902) as feb,
--   count(*) filter (where year_month = 201903) as mar,
--   count(*) filter (where year_month = 201904) as apr,
--   count(*) filter (where year_month = 201905) as may,
--   count(*) filter (where year_month = 201906) as jun,
--   count(*) filter (where year_month = 201907) as jul,
--   count(*) filter (where year_month = 201908) as aug,
--   count(*) filter (where year_month = 201909) as sep,
--   count(*) filter (where year_month = 201910) as oct,
--   count(*) filter (where year_month = 201911) as nov,
--   count(*) filter (where year_month = 201912) as dec,
--   count(*) as total
-- from sales
-- where model = 'sierra 1500'
-- group by cab, model_code
-- order by seq, total desc
---------------------------------------------------------------------------------------

-- -- this is not as useful in this format becuase chrome_Style_id changes over model years
-- select *
-- from (
-- select chrome_style_id,
--   count(*) filter (where year_month = 201901) as jan,
--   count(*) filter (where year_month = 201902) as feb,
--   count(*) filter (where year_month = 201903) as mar,
--   count(*) filter (where year_month = 201904) as apr,
--   count(*) filter (where year_month = 201905) as may,
--   count(*) filter (where year_month = 201906) as jun,
--   count(*) filter (where year_month = 201907) as jul,
--   count(*) filter (where year_month = 201908) as aug,
--   count(*) filter (where year_month = 201909) as sep,
--   count(*) filter (where year_month = 201910) as oct,
--   count(*) filter (where year_month = 201911) as nov,
--   count(*) filter (where year_month = 201912) as dec,
--   count(*) as total
-- from sales
-- where sale_type in ('retail','fleet')
-- --   and model = 'sierra 1500'
-- group by chrome_style_id
-- order by count(*) desc) aa
-- left join jon.configurations bb on aa.chrome_Style_id = bb.chrome_style_id

-- -- why 2 rows for silverado 1500 crew, looks like alloc group , eg, the second roww is no sales until oct ????
-- -- 2020s are Crew others are crew, remember any string function on citext -> text
-- -- i like this one as the top
-- select 
--   case
--     when model like '%1500%' then (left(model, 14) || ' ' || cab)::citext
--     else alloc_group
--   end,
--   count(*) filter (where year_month = 201901) as jan,
--   count(*) filter (where year_month = 201902) as feb,
--   count(*) filter (where year_month = 201903) as mar,
--   count(*) filter (where year_month = 201904) as apr,
--   count(*) filter (where year_month = 201905) as may,
--   count(*) filter (where year_month = 201906) as jun,
--   count(*) filter (where year_month = 201907) as jul,
--   count(*) filter (where year_month = 201908) as aug,
--   count(*) filter (where year_month = 201909) as sep,
--   count(*) filter (where year_month = 201910) as oct,
--   count(*) filter (where year_month = 201911) as nov,
--   count(*) filter (where year_month = 201912) as dec,
--   count(*) as total
-- from sales
-- group by 
--   case
--     when model like '%1500%' then (left(model, 14) || ' ' || cab)::citext
--     else alloc_group
--   end
-- order by count(*) desc





select the_date, 
  case
    when model like '%1500%' then (left(model, 14) || ' ' || cab)::citext
    else alloc_group
  end, count(*)
from inv_og
where year_month = 201912
group by the_date,
  case
    when model like '%1500%' then (left(model, 14) || ' ' || cab)::citext
    else alloc_group
  end

-- 17885 rows: cartesian 49 alloc_groups * 365 days
drop table if exists base_1 cascade;
create temp table base_1 as
select *
from (
  select 
    case
      when model like '%1500%' then (left(model, 14) || ' ' || cab)::citext
      else alloc_group
    end
  from inv_og
  group by   
    case
      when model like '%1500%' then (left(model, 14) || ' ' || cab)::citext
      else alloc_group
    end) a,
(
  select the_date, year_month
  from dds.dim_Date
  where the_year = 2019) b;
create unique index on base_1(alloc_group, the_date);  

select * 
from base_1
where alloc_group = 'EQUINX'

-- shit this looks like it
-- wait a minute i don't want just stocked out data,
-- i want days stocked out for each year_month/alloc group, MANY WILL BE 0
drop table if exists base_2 cascade;
create temp table base_2 as
select a.alloc_group, a.the_date, a.year_month, coalesce(b.units, 0) as units
from base_1 a -- cartesian of alloc_groups and date
left join ( -- daily counts when we have inventory
  select the_date, 
    case
      when model like '%1500%' then (left(model, 14) || ' ' || cab)::citext
      else alloc_group
    end, count(*) as units
  from inv_og
  group by the_date,
    case
      when model like '%1500%' then (left(model, 14) || ' ' || cab)::citext
      else alloc_group
    end) b on a.the_date = b.the_date and a.alloc_Group = b.alloc_group;

  select * from base_2  where units between 0 and 2


select alloc_group, 
  count(*) filter (where year_month = 201901 and units = 0) as jan,
  count(*) filter (where year_month = 201902 and units = 0) as feb,
  count(*) filter (where year_month = 201903 and units = 0) as mar,
  count(*) filter (where year_month = 201904 and units = 0) as apr,
  count(*) filter (where year_month = 201905 and units = 0) as may,
  count(*) filter (where year_month = 201906 and units = 0) as jun,
  count(*) filter (where year_month = 201907 and units = 0) as jul,
  count(*) filter (where year_month = 201908 and units = 0) as aug,
  count(*) filter (where year_month = 201909 and units = 0) as sep,
  count(*) filter (where year_month = 201910 and units = 0) as oct,
  count(*) filter (where year_month = 201911 and units = 0) as nov,
  count(*) filter (where year_month = 201912 and units = 0) as dec
from base_2
group by alloc_group  
-- 
-- -- i don't like this
-- select 0 as seq, 'alloc_group', 'jan', 'feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec','total'
-- union
-- select 1, x.alloc_group, sjan::text || ' - ' || sojan::text,sfeb::text || ' - ' || sofeb::text,
--   smar::text || ' - ' || somar::text,sapr::text || ' - ' || soapr::text,
--   smay::text || ' - ' || somay::text,sjun::text || ' - ' || sojun::text,
--   sjul::text || ' - ' || sojul::text,saug::text || ' - ' || soaug::text,
--   ssep::text || ' - ' || sosep::text,soct::text || ' - ' || sooct::text,
--   snov::text || ' - ' || sonov::text,sdec::text || ' - ' || sodec::text,
--   x.total::text
-- from (
-- select 
--   case
--     when model like '%1500%' then (left(model, 14) || ' ' || cab)::citext
--     else alloc_group
--   end,
--   count(*) filter (where year_month = 201901) as sjan,
--   count(*) filter (where year_month = 201902) as sfeb,
--   count(*) filter (where year_month = 201903) as smar,
--   count(*) filter (where year_month = 201904) as sapr,
--   count(*) filter (where year_month = 201905) as smay,
--   count(*) filter (where year_month = 201906) as sjun,
--   count(*) filter (where year_month = 201907) as sjul,
--   count(*) filter (where year_month = 201908) as saug,
--   count(*) filter (where year_month = 201909) as ssep,
--   count(*) filter (where year_month = 201910) as soct,
--   count(*) filter (where year_month = 201911) as snov,
--   count(*) filter (where year_month = 201912) as sdec,
--   count(*) as total
-- from sales
-- where sale_type in ('retail','fleet')
-- group by 
--   case
--     when model like '%1500%' then (left(model, 14) || ' ' || cab)::citext
--     else alloc_group
--   end
-- order by count(*) desc) x
-- left join (
-- select 1, alloc_group, 
--   count(*) filter (where year_month = 201901 and units = 0) as sojan,
--   count(*) filter (where year_month = 201902 and units = 0) as sofeb,
--   count(*) filter (where year_month = 201903 and units = 0) as somar,
--   count(*) filter (where year_month = 201904 and units = 0) as soapr,
--   count(*) filter (where year_month = 201905 and units = 0) as somay,
--   count(*) filter (where year_month = 201906 and units = 0) as sojun,
--   count(*) filter (where year_month = 201907 and units = 0) as sojul,
--   count(*) filter (where year_month = 201908 and units = 0) as soaug,
--   count(*) filter (where year_month = 201909 and units = 0) as sosep,
--   count(*) filter (where year_month = 201910 and units = 0) as sooct,
--   count(*) filter (where year_month = 201911 and units = 0) as sonov,
--   count(*) filter (where year_month = 201912 and units = 0) as sodec,
--   null::text as total
-- from base_2
-- group by alloc_group) y on x.alloc_group = y.alloc_group  
-- order by seq


-- this made a decent spreadsheet which could serve as a template
select x.alloc_group, 
  sjan,sojan,sfeb,sofeb,
  smar,somar,sapr,soapr,
  smay,somay,sjun,sojun,
  sjul,sojul,saug,soaug,
  ssep,sosep,soct,sooct,
  snov,sonov,sdec,sodec,
  total, sototal
from (
select 
  case
    when model like '%1500%' then (left(model, 14) || ' ' || cab)::citext
    else alloc_group
  end,
  count(*) filter (where year_month = 201901) as sjan,
  count(*) filter (where year_month = 201902) as sfeb,
  count(*) filter (where year_month = 201903) as smar,
  count(*) filter (where year_month = 201904) as sapr,
  count(*) filter (where year_month = 201905) as smay,
  count(*) filter (where year_month = 201906) as sjun,
  count(*) filter (where year_month = 201907) as sjul,
  count(*) filter (where year_month = 201908) as saug,
  count(*) filter (where year_month = 201909) as ssep,
  count(*) filter (where year_month = 201910) as soct,
  count(*) filter (where year_month = 201911) as snov,
  count(*) filter (where year_month = 201912) as sdec,
  count(*) as total
from sales
-- where sale_type in ('retail','fleet')
group by 
  case
    when model like '%1500%' then (left(model, 14) || ' ' || cab)::citext
    else alloc_group
  end
order by count(*) desc) x
left join (
select 1, alloc_group, 
  count(*) filter (where year_month = 201901 and units = 0) as sojan,
  count(*) filter (where year_month = 201902 and units = 0) as sofeb,
  count(*) filter (where year_month = 201903 and units = 0) as somar,
  count(*) filter (where year_month = 201904 and units = 0) as soapr,
  count(*) filter (where year_month = 201905 and units = 0) as somay,
  count(*) filter (where year_month = 201906 and units = 0) as sojun,
  count(*) filter (where year_month = 201907 and units = 0) as sojul,
  count(*) filter (where year_month = 201908 and units = 0) as soaug,
  count(*) filter (where year_month = 201909 and units = 0) as sosep,
  count(*) filter (where year_month = 201910 and units = 0) as sooct,
  count(*) filter (where year_month = 201911 and units = 0) as sonov,
  count(*) filter (where year_month = 201912 and units = 0) as sodec,
  count(*) filter (where units = 0) as sototal
from base_2
group by alloc_group) y on x.alloc_group = y.alloc_group  
order by total desc 

-- need to do the drilldown on stockouts

does model_Code work as base config
select *
from jon.configurations
where model = 'silverado 1500'
order by model_code

select *
from inv_og
where model = 'sierra 1500'
order by model_code

model_code: make, model, cab, drive, box


drop table if exists base_1 cascade;
-- cartesian: 365 days with whatever characteristic of vehicle
create temp table base_1 as 
select *
from (
  select alloc_group
  from inv_og
  where model = 'sierra 1500'
  group by alloc_group) a,
(
  select the_date, year_month
  from dds.dim_Date
  where the_year = 2019) b;
create unique index on base_1(alloc_Group, the_date);  

select * from base_1

drop table if exists base_2 cascade;
create temp table base_2 as
select a.alloc_group, a.the_date, a.year_month, coalesce(b.units, 0) as units
from base_1 a -- cartesian of model_codes and date
left join ( -- daily counts when we have inventory
  select the_date, alloc_group, count(*) as units
  from inv_og
  group by the_date, alloc_group) b on a.the_date = b.the_date and a.alloc_group = b.alloc_group;

select alloc_group, 
  count(*) filter (where year_month = 201901 and units = 0) as sojan,
  count(*) filter (where year_month = 201902 and units = 0) as sofeb,
  count(*) filter (where year_month = 201903 and units = 0) as somar,
  count(*) filter (where year_month = 201904 and units = 0) as soapr,
  count(*) filter (where year_month = 201905 and units = 0) as somay,
  count(*) filter (where year_month = 201906 and units = 0) as sojun,
  count(*) filter (where year_month = 201907 and units = 0) as sojul,
  count(*) filter (where year_month = 201908 and units = 0) as soaug,
  count(*) filter (where year_month = 201909 and units = 0) as sosep,
  count(*) filter (where year_month = 201910 and units = 0) as sooct,
  count(*) filter (where year_month = 201911 and units = 0) as sonov,
  count(*) filter (where year_month = 201912 and units = 0) as sodec,
  count(*) filter (where units = 0) as sototal
from base_2
group by alloc_group


-- ! unique
select model_code, model_year, trim_level, peg
from jon.configurations
group by model_code, model_year, trim_level, peg
having count(*) > 1


select count(distinct model_code) from sales

select *
from jon.configurations
where model = 'sierra 1500'
order by model_code


-- this does not include engines
select a.make, a.model, a.model_code, a.cab, a.drive, a.box,
  array_agg(distinct a.chrome_style_id order by a.chrome_style_id),
  array_agg(distinct a.model_year), array_agg(distinct a.trim_level),
  array_agg(distinct a.peg),--, array_agg(distinct case when b.diesel then 'D-'||b.displacement else b.displacement end),  count(*)\
  count(*)
from jon.configurations a
-- left join jon.engines b on a.chrome_style_id = b.chrome_style_id
where a.model = 'sierra 1500'
group by a.make, a.model, a.model_code, a.cab, a.drive, box
order by a.cab, a.drive, a.box


select count(distinct chrome_style_id) from jon.configurations where model = 'sierra 1500'
--------------------------------------------------------------------------
--< equinox on ground
--------------------------------------------------------------------------
-- 68 unique combinations of relevant attributes
select drive, trim_level, engine, color
from inv_og 
where model = 'equinox'
group by drive, trim_level, engine, color



what i need is the number of days each moth for stocked out for each configuration at each config level

so, starting with drive

drop table if exists equinox_og;
create temp table equinox_og as
select year_month, the_date, drive, trim_level, engine, color
from inv_og 
where model = 'equinox';


-- 0
select a.year_month 
from dds.dim_date a
left join equinox_og b on a.year_month = b.year_month
where a.the_year = 2019
  and b.year_month is null

select a.year_month, a.the_date, b.the_date
from dds.dim_date a
left join (
  select year_month, the_date, drive
  from equinox_og
  where drive = 'FWD'
    and trim_level = 'LS') b on a.the_date = b.the_date
where a.the_year = 2019
order by a.the_date

select year_month
select a.year_month, count(*) as days_so
from dds.dim_date a
left join (
  select year_month, the_date, drive
  from equinox_og
  where drive = 'FWD'
    and trim_level = 'LS') b on a.the_date = b.the_date
where a.the_year = 2019
  and b.the_date is null
group by  a.year_month


select a.year_month, coalesce(b.days_so, 0) 
from sls.months a
left join (
select a.year_month, count(*) as days_so
from dds.dim_date a
left join (
  select year_month, the_date, drive
  from equinox_og
  where drive = 'FWD'
    and trim_level = 'LS') b on a.the_date = b.the_date
where a.the_year = 2019
  and b.the_date is null
group by  a.year_month) b on a.year_month = b.year_month
where yyyy = 2019
order by a.year_month

/*
1/16/20
having a really hard time getting traction on this
right now, feels like i need to accept stock out will require a multiple level query to accomodate
both the full calendar and the stock out stats for each level of configurations

or better yet, remember the presentation i did, with alloc sales/so in one spreadsheet and config drilldown on sales for sierras
in another, what i took out of that was an interest in days stocked out only at the color level

i have not gotten any traction on the notion of needing to redefine top configs,
so simply go with bens selection (top_configs_2019_sales_with_ben.xlsx)
but for colors, do all colors

i just asked ben: in terms of past year sales, would top config candidate be any config
that averagedd at leeast 2 sales per month, roughly speaking?
ben: roughly, yes. i would like to look at it as like one sale for every 15 calendar days we had one on the 
ground. so it doesn't throuw it out of a top config if it was out of stock for 60 days.

*/

so, for equinox, the configuration levels are (bens):
LT 1.5 AWD
Premier 1.5 AWD

or from the all equinox sold in 2019 query above
--------------------------------------------------------------------------
--/> equinox on ground
--------------------------------------------------------------------------

---------------------------------------------------------
--< 1/18/20 -- silverados
-- !!! bear in mind this is about getting base configurations that will work for proving out
--     ben's test for top configs
---------------------------------------------------------
-- going for base configurations
-- start with alloc_groups with an array of chrome_style_ids
-- ended up with what i think will be good for pickups
drop table if exists alloc_groups;
create temp table alloc_groups as
select (
  case
    when model like '%1500%' and model like 'silverado%' then 'Silverado 1500 ' || cab::citext
    when model like '%1500%' and model like 'sierra%' then 'Sierra 1500 ' || cab::citext
    else alloc_group
  end)::citext, array_agg(distinct chrome_style_id) as style_ids
from jon.configurations
where alloc_group <> 'n/a'
group by   
  case
    when model like '%1500%' and model like 'silverado%' then 'Silverado 1500 ' || cab::citext
    when model like '%1500%' and model like 'sierra%' then 'Sierra 1500 ' || cab::citext
    else alloc_group
  end;

select *
from alloc_groups

select * from jon.configurations where chrome_style_id = '389870'

select *
from jon.configurations
where alloc_group = 'equinx'
order by model_code, model_year

need to verify model code changing over years

select model_year, model_code, drive, peg

select *
from jon.configurations a
join alloc_groups b on b.alloc_group = 'acadia' and a.chrome_style_id = any(b.style_ids)
order by a.model_code, a.model_year

-- model_code & peg look promising
-- looking for a categorization that transcends model year
drop table if exists test1;
create temp table test1 as
select model_code, peg
from jon.configurations
where make not in ('honda','nissan')
group by model_code, peg;

select * -- 923 
from test1

-- ok, here is where model code changes
-- 2018 1/2 ton is xx15xxx, 2019 1/2 ton is xx10xxx
select a.*, array_agg(distinct b.model_year), array_agg(distinct b.chrome_style_id), array_agg(distinct b.cab || '-' 
from test1 a
join jon.configurations b on a.model_code = b.model_code 
  and a.peg = b.peg
  and b.model like 'silverado%'
  and b.model like '%1500%'
group by a.model_code, a.peg  
order by a.model_code, a.peg

so, maybe its just a fucking matter of categorization for trucks is different
instead of model_code it is cab,box & drive

drop table if exists test2;
create temp table test2 as
select cab, drive, box, peg
from jon.configurations
where make not in ('honda','nissan')
group by cab, drive, box, peg;

select a.*, array_agg(distinct b.model_year), array_agg(distinct b.chrome_style_id), array_agg(distinct b.trim_level)
from test2 a
join jon.configurations b on a.cab = b.cab
  and a.drive = b.drive
  and a.box = b.box 
  and a.peg = b.peg
  and b.model like 'silverado%'
  and b.model like '%1500%'
group by a.cab, a.drive, a.box, a.peg
-- order by a.model_code, a.peg


---------------------------------------------------------
--/> 1/18/20 -- silverados
---------------------------------------------------------


---------------------------------------------------------
--< 1/18/20 -- all except pickups
-- for these, i think model code and peg will work
---------------------------------------------------------
drop table if exists test1;
create temp table test1 as
select model, alloc_group, model_code, peg
from jon.configurations
where alloc_group <> 'n/a'
  and model not like 'sierra%'
  and model not like 'silverado%'
group by model, alloc_group, model_code, peg;



select a.*, array_agg(distinct b.model_year), array_agg(distinct b.chrome_style_id)
from test1 a
join jon.configurations b on a.model_code = b.model_code 
  and a.peg = b.peg
  and a.alloc_group = b.alloc_group
  and a.model = b.model
group by a.alloc_group, a.model, a.model_code, a.peg  
order by a.model, a.alloc_group, a.model_code, a.peg


drop table if exists trav_sales;
create temp table trav_sales as
select *
from sales
where model in ('equinox','acadia', 'traverse','blazer');

drop table if exists trav_og;
create temp table trav_og as
select *
from inv_og
where model in ('equinox','acadia', 'traverse','blazer');

select aa.model, aa.model_code, aa.peg, days_og, min_sales, sales
from (
select model, model_code, peg, count(distinct the_date) days_og, count(distinct the_date)/15 as min_sales
from trav_og
group by model, model_code, peg) aa
join (
select model, model_code, peg, count(*) as sales
from trav_sales
group by model, model_code, peg) bb on aa.model_code = bb.model_code
  and aa.peg = bb.peg

select * from trav_sales where model = 'equinox'

-- the big take away here (getting trim_level to add to the basic config) is, peg is consistent
-- accross model years, but peg changes: equinox peg 1LT has trims 1LT and LT w/1LT
-- but only because thats the way i code into nc.vehicle_configurations in all_nc_inventory_nightly.sql 

select y.model, y.model_code, y.peg, x.drive, days_og, min_sales, sales, array_agg(distinct x.trim_level), array_agg(distinct x.engine)
from (
  select model, model_code, peg, drive, trim_level, engine
  from sales
  group by model, model_code, peg, drive, trim_level, engine) x
join (
  select aa.model, aa.model_code, aa.peg, days_og, min_sales, sales
  from (
  select model, model_code, peg, count(distinct the_date) days_og, count(distinct the_date)/15 as min_sales
  from trav_og
  group by model, model_code, peg) aa
join (
  select model, model_code, peg, count(*) as sales
  from trav_sales
  group by model, model_code, peg) bb on aa.model_code = bb.model_code
    and aa.peg = bb.peg) y on x.model_code = y.model_code and x.peg = y.peg
group by  y.model, y.model_code, y.peg, x.drive, days_og, min_sales, sales
order by model, sales desc  

select *
from trav_sales
where model_code = '1xy26'

select a.*, count(*) over (partition by color) from sales a where model = 'equinox' and peg = '1lz' order by color

select *, a.delivery_date - ground_date
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
  and b.chrome_style_id in ('398681','406431')
---------------------------------------------------------
--/> 1/18/20 -- all except pickups
---------------------------------------------------------


---------------------------------------------------------
--< 1/19/20 -- drill into traverse
---------------------------------------------------------
select model, model_code, peg, color, count(distinct the_date) days_og, count(distinct the_date)/15 as min_sales
from trav_og
where model = 'traverse'
group by model, model_code, peg, color


select 1 as seq, null::citext as model_code,null::citext as peg,null::citext as color,
  count(*) filter (where year_month = 201901) as jan,
  count(*) filter (where year_month = 201902) as feb,
  count(*) filter (where year_month = 201903) as mar,
  count(*) filter (where year_month = 201904) as apr,
  count(*) filter (where year_month = 201905) as may,
  count(*) filter (where year_month = 201906) as jun,
  count(*) filter (where year_month = 201907) as jul,
  count(*) filter (where year_month = 201908) as aug,
  count(*) filter (where year_month = 201909) as sep,
  count(*) filter (where year_month = 201910) as oct,
  count(*) filter (where year_month = 201911) as nov,
  count(*) filter (where year_month = 201912) as dec,
  count(*) as total,
  b.days_og, b.min_sales
from sales a
left join (
  select model, count(distinct the_date) days_og, count(distinct the_date)/15 as min_sales
  from trav_og
  where model = 'traverse'
  group by model) b on true
where a.model = 'traverse'
group by a.model, b.days_og, b.min_sales
union  -- model_code
select 2, a.model_code,null::citext as trim_level,null::citext as color,
  count(*) filter (where year_month = 201901) as jan,
  count(*) filter (where year_month = 201902) as feb,
  count(*) filter (where year_month = 201903) as mar,
  count(*) filter (where year_month = 201904) as apr,
  count(*) filter (where year_month = 201905) as may,
  count(*) filter (where year_month = 201906) as jun,
  count(*) filter (where year_month = 201907) as jul,
  count(*) filter (where year_month = 201908) as aug,
  count(*) filter (where year_month = 201909) as sep,
  count(*) filter (where year_month = 201910) as oct,
  count(*) filter (where year_month = 201911) as nov,
  count(*) filter (where year_month = 201912) as dec,
  count(*) as total,
  b.days_og, b.min_sales
from sales a
left join (
  select model, model_code, count(distinct the_date) days_og, count(distinct the_date)/15 as min_sales
  from trav_og
  where model = 'traverse'
  group by model, model_code) b on a.model_code = b.model_code
where a.model = 'traverse'
group by a.model_code, b.days_og, b.min_sales
union -- peg
select 3, a.model_code, a.peg,null::citext as color,
  count(*) filter (where year_month = 201901) as jan,
  count(*) filter (where year_month = 201902) as feb,
  count(*) filter (where year_month = 201903) as mar,
  count(*) filter (where year_month = 201904) as apr,
  count(*) filter (where year_month = 201905) as may,
  count(*) filter (where year_month = 201906) as jun,
  count(*) filter (where year_month = 201907) as jul,
  count(*) filter (where year_month = 201908) as aug,
  count(*) filter (where year_month = 201909) as sep,
  count(*) filter (where year_month = 201910) as oct,
  count(*) filter (where year_month = 201911) as nov,
  count(*) filter (where year_month = 201912) as dec,
  count(*) as total,
  b.days_og, b.min_sales
from sales a
left join (
  select model, model_code, peg, count(distinct the_date) days_og, count(distinct the_date)/15 as min_sales
  from trav_og
  where model = 'traverse'
  group by model, model_code, peg) b on a.model_code = b.model_code and a.peg = b.peg
where  a.model = 'traverse' 
group by a.model_code, a.peg, b.days_og, b.min_sales
union -- engine
select 4, a.model_code, a.peg, a.color,
  count(*) filter (where year_month = 201901) as jan,
  count(*) filter (where year_month = 201902) as feb,
  count(*) filter (where year_month = 201903) as mar,
  count(*) filter (where year_month = 201904) as apr,
  count(*) filter (where year_month = 201905) as may,
  count(*) filter (where year_month = 201906) as jun,
  count(*) filter (where year_month = 201907) as jul,
  count(*) filter (where year_month = 201908) as aug,
  count(*) filter (where year_month = 201909) as sep,
  count(*) filter (where year_month = 201910) as oct,
  count(*) filter (where year_month = 201911) as nov,
  count(*) filter (where year_month = 201912) as dec,
  count(*) as total,
  b.days_og, b.min_sales
from sales a
left join (
  select model, model_code, peg, color, count(distinct the_date) days_og, count(distinct the_date)/15 as min_sales
  from trav_og
  where model = 'traverse'
  group by model, model_code, color, peg) b on a.model_code = b.model_code and a.peg = b.peg and a.color = b.color
where  a.model = 'traverse' 
group by a.model_code, a.peg, a.color, b.days_og, b.min_sales -- order by total desc 
order by seq, total desc

-- current traverse inventory
select b.ground_date, current_date - b.ground_date as age_on_ground, d.model, d.model_code, e.peg, c.color
from nc.vehicle_acquisitions b 
join nc.vehicles c on b.vin = c.vin  
  and model = 'traverse'
join nc.vehicle_configurations d on c.configuration_id = d.configuration_id
join jon.configurations e on d.chrome_style_id = e.chrome_style_id
where b.thru_date > current_date
order by d.model_code, e.peg

---------------------------------------------------------
--/> 1/19/20 -- drill into traverse
---------------------------------------------------------
-- 385 cars
-- multiple trim levels (depending on where we get trim see above on equinox), model years per config

select alloc_group, model, model_code, peg, array_agg(distinct model_year), array_agg(distinct trim_level)-- , array_agg(distinct cab || '-' || drive || '-' || box)
from jon.configurations
where model not like 'sierra%'
  and model not like 'silverado%'
  and model not in ('canyon','colorado')
  and alloc_group <> 'n/a'
group by alloc_group, model, model_code, peg 
order by model, model_code, peg

-- 137 trucks
-- this has some issues, get canyon and sierra in same config, same cab/drive/box/peg
-- which means that can not be a PK
select make, cab, drive, box, peg, array_agg(distinct alloc_group), array_agg(distinct model), 
  array_agg(distinct model_year),array_agg(distinct model_code), array_agg(distinct trim_level)
from jon.configurations
where (
  (model like '%1500%' and (model like 'sierra%' or model like 'silverado%'))
  or
  (model in ('colorado','canyon')))
group by make, cab, drive, box, peg
order by array_agg(distinct model)

-- add trim_level: 152 trucks
-- still end up with multiple models: double: silverado 1500/silverado 1500 LD & sierra 1500/sierra 1500 limited
select make, cab, drive, box, peg, trim_level, array_agg(distinct alloc_group), array_agg(distinct model), 
  array_agg(distinct model_year),array_agg(distinct model_code)-- , array_agg(distinct trim_level)
from jon.configurations
where (
  (model like '%1500%' and (model like 'sierra%' or model like 'silverado%'))
  or
  (model in ('colorado','canyon')))
group by make, cab, drive, box, peg, trim_level
order by array_agg(distinct model)

-- 1/20/20
the struggle is to find a config NK for trucks
cant use model because of the 2 models for sierra and silverado double cabs (LD & limited)

-----------------------------------------------------------------------------------------
-- 1/27/20 start with alloc groups
-----------------------------------------------------------------------------------------

  select alloc_group, model, model_code, peg, count(distinct the_date) days_og, count(distinct the_date)/15 as min_sales
  from inv_og
  where cab = 'N/A' -- cars
  group by alloc_group, model, model_code, peg


select alloc_group, model, model_code, peg
from jon.configurations
where cab = 'N/A'
  and make not in ('honda','nissan')
group by alloc_group, model, model_code, peg
order by alloc_group
-- non pickups
select *
from (
select a.alloc_group, a.model, a.model_code, a.peg,
  count(*) filter (where year_month = 201901) as jan,
  count(*) filter (where year_month = 201902) as feb,
  count(*) filter (where year_month = 201903) as mar,
  count(*) filter (where year_month = 201904) as apr,
  count(*) filter (where year_month = 201905) as may,
  count(*) filter (where year_month = 201906) as jun,
  count(*) filter (where year_month = 201907) as jul,
  count(*) filter (where year_month = 201908) as aug,
  count(*) filter (where year_month = 201909) as sep,
  count(*) filter (where year_month = 201910) as oct,
  count(*) filter (where year_month = 201911) as nov,
  count(*) filter (where year_month = 201912) as dec,
  count(*) as total_sales,
  b.days_og, b.min_sales, count(*) - b.min_sales as diff
from sales a
left join (
  select alloc_group, model, model_code, peg, count(distinct the_date) days_og, count(distinct the_date)/15 as min_sales
  from inv_og
  where cab = 'N/A' -- cars
  group by alloc_group, model, model_code, peg) b on a.alloc_group = b.alloc_group
    and a.model = b.model
    and a.model_code = b.model_code
    and a.peg = b.peg
where a.cab = 'N/A'
group by a.alloc_group, a.model, a.model_code, a.peg, b.days_og, b.min_sales) aa
where total_sales >= min_sales  
order by total_sales desc


-- pickups
-- a little messier than anticipated
-- need to limit this to light duty
drop table if exists test2;
create temp table test2 as
select cab, drive, box, peg
from jon.configurations
where make not in ('honda','nissan')
group by cab, drive, box, peg;

select a.model, a.cab, a.drive, a.box, a.peg, array_agg(distinct a.model_year), array_agg(distinct a.chrome_style_id), array_agg(distinct a.trim_level)
from jon.configurations a
group by a.cab, a.drive, a.box, a.peg
-- order by a.model_code, a.peg
















---------------------------------------------------------------------------------------
--< 01/28/20 this is it
---------------------------------------------------------------------------------------



drop table if exists inv_og cascade;
create temp table inv_og as
select a.year_month, a.the_date, d.chrome_style_id, d.model_year, d.make, d.model, d.model_code, 
  d. drive, d.cab, d.trim_level, d.engine, c.color, e.peg, e.alloc_group
from dds.dim_date a
join nc.vehicle_acquisitions b on b.ground_date <= a.the_date
  and b.thru_date > a.the_date
join nc.vehicles c on b.vin = c.vin  
  and make not in ('honda','nissan')
join nc.vehicle_configurations d on c.configuration_id = d.configuration_id
join jon.configurations e on d.chrome_style_id = e.chrome_style_id
where a.the_year = 2019;
create index on inv_og(chrome_style_id);
create index on inv_og(the_date);
create index on inv_og(drive);
create index on inv_og(cab);

 -- 2019 sales
drop table if exists sales;
create temp table sales as
select b.year_month, a.delivery_date, d.chrome_style_id, d.model_year, d.make, d.model, d.model_code,
  d.drive, d.cab, d.trim_level, d.engine, c.color, e.peg, e.alloc_group
from (
  select vin, max(delivery_date) as delivery_date
  from nc.vehicle_sales a
  join dds.dim_date b on a.delivery_date = b.the_date
    and b.the_year = 2019
  where sale_type in ('retail','fleet')
  group by a.vin) a
join nc.vehicle_sales aa on a.vin = aa.vin
  and a.delivery_date = aa.delivery_date  
join dds.dim_date b on a.delivery_date = b.the_date  
join nc.vehicles c on a.vin = c.vin  
  and c.make not in ('honda','nissan')
join nc.vehicle_configurations d on c.configuration_id = d.configuration_id
join jon.configurations e on d.chrome_style_id = e.chrome_style_id;

-- maybe start with allocation groups
drop table if exists pickup_alloc_groups;
create temp table pickup_alloc_groups as
select (
  case
    when model like '%1500%' and model like 'silverado%' then 'Silverado 1500 ' || cab::citext
    when model like '%1500%' and model like 'sierra%' then 'Sierra 1500 ' || cab::citext
    else alloc_group
  end)::citext, array_agg(distinct chrome_style_id) as style_ids
from jon.configurations
where alloc_group <> 'n/a' -- exclude honda nissan 
  and model not similar to '%(2500|3500|4500|5500|6500)%' -- exclude HD
  and cab <> 'n/a' -- exclude cars
group by   
  case
    when model like '%1500%' and model like 'silverado%' then 'Silverado 1500 ' || cab::citext
    when model like '%1500%' and model like 'sierra%' then 'Sierra 1500 ' || cab::citext
    else alloc_group
  end;


drop table if exists pickups cascade;
create temp table pickups as
select a.alloc_group, b.cab, b.drive, b.box, b.peg, array_agg(distinct b.model_year) as model_year, 
  array_agg(distinct b.model) as model, array_agg(distinct b.chrome_style_id) as chrome_style_id, 
  array_agg(distinct b.trim_level) as trim_level
from pickup_alloc_groups a
join jon.configurations b on b.chrome_style_id = any(a.style_ids)
group by a.alloc_group, b.cab, b.drive, b.box, b.peg;
create unique index on pickups(alloc_group,cab,drive,box,peg);
create index on pickups(chrome_style_id);

-- refactor cars
drop table if exists cars cascade;
create temp table cars as
select alloc_group, model, model_code, drive, peg, 
  array_agg(distinct model_year) as model_year, array_agg(distinct trim_level) as trim_level, 
  array_agg(chrome_style_id) as chrome_style_id
from jon.configurations
where cab = 'N/A'
  and make not in ('honda','nissan')
group by alloc_group, model, model_code, drive, peg;
create unique index on cars(alloc_group, model_code, peg);
create index on cars(chrome_style_id);

select aa.*, bb.model_year, bb.model, bb.chrome_Style_id, bb.trim_level
from (
  select b.alloc_group, b.cab, b.drive, b.box, b.peg, 
    count(*) filter (where year_month = 201901) as jan,
    count(*) filter (where year_month = 201902) as feb,
    count(*) filter (where year_month = 201903) as mar,
    count(*) filter (where year_month = 201904) as apr,
    count(*) filter (where year_month = 201905) as may,
    count(*) filter (where year_month = 201906) as jun,
    count(*) filter (where year_month = 201907) as jul,
    count(*) filter (where year_month = 201908) as aug,
    count(*) filter (where year_month = 201909) as sep,
    count(*) filter (where year_month = 201910) as oct,
    count(*) filter (where year_month = 201911) as nov,
    count(*) filter (where year_month = 201912) as dec,
    count(*) as total_sales, 
    c.days_og, c.min_sales, count(*) - c.min_sales as diff 
  from sales a
  join pickups b on a.chrome_style_id = any(b.chrome_style_id)
  join (
    select b.alloc_group, b.cab, b.drive, b.box, b.peg, count(distinct the_date) days_og, count(distinct the_date)/15 as min_sales
    from inv_og a
    join pickups b on a.chrome_style_id = any(b.chrome_style_id)
    group by b.alloc_group, b.cab, b.drive, b.box, b.peg) c on b.alloc_group = c.alloc_group
      and b.cab = c.cab
      and b.drive = c.drive
      and b.box = c.box
      and b.peg = c.peg
  group by b.alloc_group, b.cab, b.drive, b.box, b.peg, c.days_og, c.min_sales) aa
join pickups bb on aa.alloc_group = bb.alloc_group
      and aa.cab = bb.cab
      and aa.drive = bb.drive
      and aa.box = bb.box
      and aa.peg = bb.peg
where total_sales >= min_sales  
order by total_sales desc;




select aa.*, bb.model_year, bb.model, bb.chrome_Style_id, bb.trim_level
from (
  select b.alloc_group, b.model, b.model_code, b.drive, b.peg, 
    count(*) filter (where year_month = 201901) as jan,
    count(*) filter (where year_month = 201902) as feb,
    count(*) filter (where year_month = 201903) as mar,
    count(*) filter (where year_month = 201904) as apr,
    count(*) filter (where year_month = 201905) as may,
    count(*) filter (where year_month = 201906) as jun,
    count(*) filter (where year_month = 201907) as jul,
    count(*) filter (where year_month = 201908) as aug,
    count(*) filter (where year_month = 201909) as sep,
    count(*) filter (where year_month = 201910) as oct,
    count(*) filter (where year_month = 201911) as nov,
    count(*) filter (where year_month = 201912) as dec,
    count(*) as total_sales, 
    c.days_og, c.min_sales, count(*) - c.min_sales as diff 
  from sales a
  join cars b on a.chrome_style_id = any(b.chrome_style_id)
  join (
    select b.alloc_group, b.model, b.model_code, b.drive, b.peg, count(distinct the_date) days_og, count(distinct the_date)/15 as min_sales
    from inv_og a
    join cars b on a.chrome_style_id = any(b.chrome_style_id)
    group by b.alloc_group, b.model, b.model_code, b.drive, b.peg) c on b.alloc_group = c.alloc_group
      and b.model = c.model
      and b.model_code = c.model_code
      and b.peg = c.peg
  group by b.alloc_group, b.model, b.model_code, b.drive, b.peg, c.days_og, c.min_sales) aa
join cars bb on aa.alloc_group = bb.alloc_group
      and aa.model = bb.model
      and aa.model_code = bb.model_code
      and aa.peg = bb.peg
-- where total_sales >= min_sales  
-- order by total_sales - min_sales  desc
order by total_sales desc

---------------------------------------------------------------------------------------
--/> 01/28/20 this is it
---------------------------------------------------------------------------------------
