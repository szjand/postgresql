﻿-- /*
-- 
-- Taylors list from 12/18/19
-- generated and emailed spreadsheet with this data (top_configs_2019_sales.xlsx)
-- 
-- Equinox:
-- -	LT AWD – pacific blue 
-- -	LT AWD – nightfall gray
-- -	LT AWD – white
-- -	Premier AWD –  nightfall gray
-- -	Premier AWD – cajun
-- 
-- Silverado 1500 DBL:
-- -	LT – white
-- -	LT - northsky blue
-- -	LT – satin steel
-- -	LT - black
-- -	LT – shadow gray
-- 
-- Silverado 1500 Crew:
-- -	LT – white  
-- -	LT – black
-- -	LT – northsky blue
-- -	LTZ – black
-- -	LTZ - white
-- 
-- Sierra 1500 Crew:
-- -	SLT – black
-- -	SLT – white frost
-- -	SLT – white
-- -	SLT – red quartz
-- -	Denali w/ 6.2L - black
-- */
-- 
-- drop table if exists sierra cascade;
-- create temp table sierra as 
-- -- sierra 1500
-- select b.model, '' as cab, '' as trim, '' as engine, '' as color, count(*) as sales-- 214
-- from nc.vehicle_sales a
-- join nc.vehicles b on a.vin = b.vin
--   and b.model like 'sierra 1%'
-- join dds.dim_date c on a.delivery_date = c.the_date  
-- where extract(year from a.delivery_date) = 2019
--   and a.sale_type in ('retail','fleet')
-- group by model  
-- union
-- -- cab
-- select b.model, b.cab, '' as trim, '' as engine, '' as color, count(*) as sales-- 214
-- from nc.vehicle_sales a
-- join nc.vehicles b on a.vin = b.vin
--   and b.model like 'sierra 1%'
--   and b.cab = 'crew'
-- join dds.dim_date c on a.delivery_date = c.the_date  
-- where extract(year from a.delivery_date) = 2019
-- and a.sale_type in ('retail','fleet')
-- group by b.model, b.cab  
-- union
-- -- trim/cab
-- select b.model, b.cab, b.trim_level, '' as engine, '' as color, count(*)
-- from nc.vehicle_sales a
-- join nc.vehicles b on a.vin = b.vin
--   and b.model like 'sierra 1%'
--   and b.cab = 'crew'
--   and b.trim_level in ('slt','denali')
-- join dds.dim_date c on a.delivery_date = c.the_date  
-- where extract(year from a.delivery_date) = 2019
--   and a.sale_type in ('retail','fleet')
-- group by b.cab, b.model, b.trim_level
-- union
-- -- trim/cab/engine
-- select b.model, b.cab, b.trim_level, b.engine, '' as color, count(*)
-- from nc.vehicle_sales a
-- join nc.vehicles b on a.vin = b.vin
--   and b.model like 'sierra 1%'
--   and b.cab = 'crew'
--   and ((b.trim_level = 'slt' and engine = '5.3') or (b.trim_level = 'denali' and engine = '6.2'))
-- join dds.dim_date c on a.delivery_date = c.the_date  
-- where extract(year from a.delivery_date) = 2019
--   and a.sale_type in ('retail','fleet')
-- group by b.model, b.trim_level, b.cab, b.engine
-- union
-- -- trim/cab/engine/color
-- select b.model, b.cab, b.trim_level, b.engine, initcap(color), count(*)
-- from nc.vehicle_sales a
-- join nc.vehicles b on a.vin = b.vin
--   and b.model like 'sierra 1%'
--   and b.cab = 'crew'
--   and ((b.trim_level = 'slt' and engine = '5.3') or (b.trim_level = 'denali' and engine = '6.2'))
--   and b.color in ('onyx black','summit white','WHITE FROST TRICOAT','red quartz tintcoat')
-- join dds.dim_date c on a.delivery_date = c.the_date  
-- where extract(year from a.delivery_date) = 2019
--   and a.sale_type in ('retail','fleet')
-- group by b.model, b.trim_level, b.cab, b.engine, color
-- order by cab, trim, engine, sales desc nulls first;
-- 
-- 
-- -- Equinox:
-- -- -	LT AWD – pacific blue 
-- -- -	LT AWD – nightfall gray
-- -- -	LT AWD – white
-- -- -	Premier AWD –  nightfall gray
-- -- -	Premier AWD – cajun
-- drop table if exists equinox cascade;
-- create temp table equinox as
-- select b.model, '' as trim, '' as engine, '' as drive, '' as color, count(*) as sales-- 214
-- from nc.vehicle_sales a
-- join nc.vehicles b on a.vin = b.vin
--   and b.model = 'equinox'
-- join dds.dim_date c on a.delivery_date = c.the_date  
-- where extract(year from a.delivery_date) = 2019
--   and a.sale_type in ('retail','fleet')
-- group by model  
-- union
-- select b.model, case when b.trim_level like 'LT%' then 'LT' else b.trim_level end as trim, b.engine, '' as drive, '' as color, count(*) as sales-- 214
-- from nc.vehicle_sales a
-- join nc.vehicles b on a.vin = b.vin
--   and b.model = 'equinox'
--   and b.engine = '1.5'
--   and (b.trim_level like 'LT%' or b.trim_level = 'premier')
-- join dds.dim_date c on a.delivery_date = c.the_date  
-- where extract(year from a.delivery_date) = 2019
-- and a.sale_type in ('retail','fleet')
-- group by b.model, case when b.trim_level like 'LT%' then 'LT' else b.trim_level end ,b.engine
-- union
-- select b.model, case when b.trim_level like 'LT%' then 'LT' else b.trim_level end as trim, b.engine, b.drive, '' as color, count(*) as sales-- 214
-- from nc.vehicle_sales a
-- join nc.vehicles b on a.vin = b.vin
--   and b.model = 'equinox'
--   and b.engine = '1.5'
--   and b.drive = 'AWD'
--   and (b.trim_level like 'LT%' or b.trim_level = 'premier')
-- join dds.dim_date c on a.delivery_date = c.the_date  
-- where extract(year from a.delivery_date) = 2019
-- and a.sale_type in ('retail','fleet')
-- group by b.model, case when b.trim_level like 'LT%' then 'LT' else b.trim_level end ,b.engine, b.drive
-- union
-- select b.model, case when b.trim_level like 'LT%' then 'LT' else b.trim_level end as trim, b.engine, b.drive, initcap(b.color) as color, count(*) as sales-- 214
-- from nc.vehicle_sales a
-- join nc.vehicles b on a.vin = b.vin
--   and b.model = 'equinox'
--   and b.engine = '1.5'
--   and b.drive = 'AWD'
--   and (b.trim_level like 'LT%' or b.trim_level = 'premier')
--   and 
--     case
--       when b.trim_level like 'LT%' then b.color in ('CAJUN RED TINTCOAT','NIGHTFALL GRAY METALLIC',
--         'MOSAIC BLACK METALLIC','SUMMIT WHITE','SILVER ICE METALLIC','PACIFIC BLUE METALLIC')
--       when b.trim_level = 'Premier' then b.color in ('MOSAIC BLACK METALLIC','7','NIGHTFALL GRAY METALLIC')
--     end
-- join dds.dim_date c on a.delivery_date = c.the_date  
-- where extract(year from a.delivery_date) = 2019
-- and a.sale_type in ('retail','fleet')
-- group by b.model, case when b.trim_level like 'LT%' then 'LT' else b.trim_level end ,b.engine, b.drive, b.color
-- order by trim, engine, drive, sales desc nulls first;
-- 
-- 
-- drop table if exists silverado_crew cascade;
-- create temp table silverado_crew as 
-- -- silverado crew 1500
-- select left(initcap(b.model), 14) as model,  '' as cab, '' as trim, '' as engine, '' as color, count(*) as sales-- 214
-- from nc.vehicle_sales a
-- join nc.vehicles b on a.vin = b.vin
--   and b.model like 'silverado 1%'
-- join dds.dim_date c on a.delivery_date = c.the_date  
-- where extract(year from a.delivery_date) = 2019
--   and a.sale_type in ('retail','fleet')
-- group by left(initcap(b.model), 14)  
-- union
-- -- cab
-- select left(initcap(b.model), 14), b.cab, '' as trim, '' as engine,  '' as color, count(*) as sales-- 214
-- from nc.vehicle_sales a
-- join nc.vehicles b on a.vin = b.vin
--   and b.model like 'silverado 1%'
--   and b.cab = 'crew'
-- join dds.dim_date c on a.delivery_date = c.the_date  
-- where extract(year from a.delivery_date) = 2019
-- and a.sale_type in ('retail','fleet')
-- group by left(initcap(b.model), 14), b.cab
-- union
-- -- trim/cab
-- select left(initcap(b.model), 14), b.cab, b.trim_level, b.engine, '' as color, count(*)
-- from nc.vehicle_sales a
-- join nc.vehicles b on a.vin = b.vin
--   and b.model like 'silverado 1%'
--   and b.cab = 'crew'
--   and b.engine = '5.3'  
--   and b.trim_level in ('lt','ltz')
-- join dds.dim_date c on a.delivery_date = c.the_date  
-- where extract(year from a.delivery_date) = 2019
--   and a.sale_type in ('retail','fleet')
-- group by b.cab, left(initcap(b.model), 14), b.trim_level, b.engine
-- 
-- union
-- -- trim/cab/engine/color
-- select b.model, b.cab, b.trim_level, b.engine, initcap(color), count(*)
-- from nc.vehicle_sales a
-- join nc.vehicles b on a.vin = b.vin
--   and b.model like 'silverado 1%'
--   and b.cab = 'crew'
--   and b.engine = '5.3'  
--   and b.trim_level in ('lt','ltz')
--   and 
--     case
--       when b.trim_level ='LT' then b.color in ('Black','Summit White','Northsky Blue Metallic')
--       when b.trim_level = 'LTZ' then b.color in ('Black','Summit White')
--     end  
-- join dds.dim_date c on a.delivery_date = c.the_date  
-- where extract(year from a.delivery_date) = 2019
--   and a.sale_type in ('retail','fleet')
-- group by b.model, b.trim_level, b.cab, b.engine, color
-- order by cab, trim, engine, sales desc nulls first;
-- 
-- 
-- 
-- drop table if exists silverado_dbl cascade;
-- create temp table silverado_dbl as 
-- -- silverado crew 1500
-- select left(initcap(b.model), 14) as model,  '' as cab, '' as trim, '' as engine, '' as color, count(*) as sales-- 214
-- from nc.vehicle_sales a
-- join nc.vehicles b on a.vin = b.vin
--   and b.model like 'silverado 1%'
-- join dds.dim_date c on a.delivery_date = c.the_date  
-- where extract(year from a.delivery_date) = 2019
--   and a.sale_type in ('retail','fleet')
-- group by left(initcap(b.model), 14)  
-- union
-- -- cab
-- select left(initcap(b.model), 14), b.cab, '' as trim, '' as engine,  '' as color, count(*) as sales-- 214
-- from nc.vehicle_sales a
-- join nc.vehicles b on a.vin = b.vin
--   and b.model like 'silverado 1%'
--   and b.cab = 'double'
-- join dds.dim_date c on a.delivery_date = c.the_date  
-- where extract(year from a.delivery_date) = 2019
-- and a.sale_type in ('retail','fleet')
-- group by left(initcap(b.model), 14), b.cab
-- union
-- -- trim/cab
-- select left(initcap(b.model), 14), b.cab, b.trim_level, b.engine, '' as color, count(*)
-- from nc.vehicle_sales a
-- join nc.vehicles b on a.vin = b.vin
--   and b.model like 'silverado 1%'
--   and b.cab = 'double'
--   and b.engine = '5.3'  
--   and b.trim_level = 'LT'
-- join dds.dim_date c on a.delivery_date = c.the_date  
-- where extract(year from a.delivery_date) = 2019
--   and a.sale_type in ('retail','fleet')
-- group by b.cab, left(initcap(b.model), 14), b.trim_level, b.engine
-- 
-- union
-- -- trim/cab/engine/color
-- select b.model, b.cab, b.trim_level, b.engine, initcap(color), count(*)
-- from nc.vehicle_sales a
-- join nc.vehicles b on a.vin = b.vin
--   and b.model like 'silverado 1%'
--   and b.cab = 'double'
--   and b.engine = '5.3'  
--   and b.trim_level = 'LT'
-- --   and 
-- --     case
-- --       when b.trim_level ='LT' then b.color in ('Black','Summit White','Northsky Blue Metallic')
-- --       when b.trim_level = 'LTZ' then b.color in ('Black','Summit White')
-- --     end  
-- join dds.dim_date c on a.delivery_date = c.the_date  
-- where extract(year from a.delivery_date) = 2019
--   and a.sale_type in ('retail','fleet')
-- group by b.model, b.trim_level, b.cab, b.engine, color
-- order by cab, trim, engine, sales desc nulls first;
-- 
-- select * from silverado_dbl
-- union
-- select * from sierra
-- union
-- select * from silverado_crew
-- union 
-- select * from equinox
-- order by model, cab, trim, engine, sales desc nulls first;

-- ---------------------------------------------------------------------------------------------
-- 12/27/19 top configs, limited to bens selections, in the format of consensus worksheet
-- 
-- 
-- select *
-- from equinox
-- where trim = 'LT'
--   and color in ('Cajun Red Tintcoat','Nightfall Gray Metallic','Mosaic Black Metallic','Summit White')
-- 
-- select *
-- from sierra  
-- where cab = 'crew'
--   and (
--     (trim = 'Denali' and engine = '6.2' and color in ('Onyx Black','White Frost Tricoat'))
--     or
--     (trim = 'SLT' and engine = '5.3' and color in ('Onyx Black','Summit White')))
-- 
-- select *
-- from silverado_crew  
-- where engine = '5.3'
--   and (
--     (trim = 'LT' and color in ('Black','Summit White'))
--     or
--     (trim = 'LTZ' and color = 'Black'))
-- 
-- select * 
-- from silverado_dbl    
-- where color in ('Black','Summit White')
-- 
-- ------------------------------------------------------------------------------------
-- --< equinox  
-- ------------------------------------------------------------------------------------
-- drop table if exists equinox cascade;
-- create temp table equinox as 
-- select a.chrome_style_id, array_agg(distinct a.model_code) as model_codes, array_agg(distinct c.color) as colors, a.trim_level
-- from jon.configurations a
-- join jon.engines b on a.chrome_style_id = b.chrome_style_id
--   and b.displacement = '1.5'
-- left join jon.exterior_colors c on a.chrome_style_id = c.chrome_style_id  
--   and c.color in ('Cajun Red Tintcoat','Nightfall Gray Metallic','Mosaic Black Metallic','Summit White')
-- where a.model = 'equinox'
--   and a.trim_level = 'LT'
--   and a.drive = 'AWD'
-- group by a.chrome_style_id;
-- 
-- select distinct unnest(colors)
-- from equinox
-- 
-- select * from equinox
-- 
-- -- in system/transit
-- select color,
--   count(*) filter (where category = 'in system') as in_system,
--   count(*) filter (where category = 'in transit') as in_transit
-- from nc.open_orders
-- where alloc_group = 'equinx'  
--   and model_code = '1XY26'
--   and color in ('Cajun Red Tintcoat','Nightfall Gray Metallic','Mosaic Black Metallic','Summit White')
-- group by color
-- 
-- 
-- -- sales
-- select b.color, count(*)
-- from nc.vehicle_sales a
-- join nc.vehicles b on a.vin = b.vin
-- join equinox c on b.chrome_style_id = c.chrome_style_id
-- where a.delivery_date between current_date - 65 and current_date - 1
--   and a.sale_type in ('retail','fleet')
-- group by b.color  
-- 
-- -- on ground
-- select b.color, count(*)
-- from nc.vehicle_acquisitions a
-- join nc.vehicles b on a.vin = b.vin
-- join equinox c on b.chrome_style_id = c.chrome_style_id
-- where a.thru_date > current_date
-- group by b.color
-- 
-- 
-- select a.color, coalesce(b.in_system, 0) as in_system, coalesce(b.in_transit, 0) as in_transit,
--   coalesce(c.in_stock, 0) as in_stock, coalesce(d.sales, 0) as sales
-- from ( -- colors
--   select distinct unnest(colors) as color
--   from equinox) a
-- left join ( --in_system/transit
--   select color,
--     count(*) filter (where category = 'in system') as in_system,
--     count(*) filter (where category = 'in transit') as in_transit
--   from nc.open_orders
--   where alloc_group = 'equinx'  
--     and model_code = '1XY26'
--     and color in ('Cajun Red Tintcoat','Nightfall Gray Metallic','Mosaic Black Metallic','Summit White')
--   group by color) b on a.color = b.color  
-- left join ( -- in stock
--   select b.color, count(*) as in_stock
--   from nc.vehicle_acquisitions a
--   join nc.vehicles b on a.vin = b.vin
--   join equinox c on b.chrome_style_id = c.chrome_style_id
--   where a.thru_date > current_date
--   group by b.color) c on a.color = c.color  
-- left join ( -- sales
--   select b.color, count(*) as sales
--   from nc.vehicle_sales a
--   join nc.vehicles b on a.vin = b.vin
--   join equinox c on b.chrome_style_id = c.chrome_style_id
--   where a.delivery_date between current_date - 65 and current_date - 1
--     and a.sale_type in ('retail','fleet')
--   group by b.color) d on a.color = d.color  
-- ------------------------------------------------------------------------------------
-- --/> equinox  
-- ------------------------------------------------------------------------------------
-- 
-- ------------------------------------------------------------------------------------
-- --< sierra  
-- 1500 Crew Denali  6.2 Onyx Black
-- 1500 Crew Denali  6.2 Whie Frost Tintcoat
-- 1500 Crew SLT     5.3 Onyx Black
-- 1500 Crew SLT     5.3 Summit White
-- ------------------------------------------------------------------------------------
-- -- this is all possible configurations that meet the speec
-- drop table if exists sierra cascade;
-- create temp table sierra as 
-- select *
-- from (
--   select a.chrome_style_id, a.model_code, a.trim_level, c.color, b.displacement, b.option_code, a.peg, a.alloc_group, a.model_year
--   from jon.configurations a
--   join jon.engines b on a.chrome_style_id = b.chrome_style_id
--   left join jon.exterior_colors c on a.chrome_style_id = c.chrome_style_id  
--     and c.color in ('Onyx Black','White Frost Tricoat','Summit White')
--   where a.model like 'sierra 1%'
--     and a.trim_level in ('Denali','SLT')
--     and cab = 'crew') aa
-- where (
--   (trim_level = 'denali' and displacement = '6.2' and color in ('Onyx Black','White Frost Tricoat'))
--   or
--   (trim_level = 'SLT' and displacement = '5.3' and color in ('Onyx Black','Summit White')));
-- 
-- select *
-- from sierra
-- 
-- select distinct trim_level, displacement, color
-- from sierra a
-- 
-- 
-- -- fucking finally, these are the in system/transit units
-- select a.*, b.order_number
-- from sierra a
-- join nc.open_orders b on a.model_code = b.model_code
--   and a.peg = b.peg
--   and a.color = b.color
--   and a.model_year = b.model_year
-- join gmgl.vehicle_order_options c on b.order_number = c.order_number
--   and a.option_code = c.option_code
--   and c.thru_ts > now()
-- 
-- -- in stock
--   select *
--   from nc.vehicle_acquisitions a
--   join nc.vehicles b on a.vin = b.vin
--   join sierra c on b.chrome_style_id = c.chrome_style_id
--     and b.engine = c.displacement
--     and b.color = c.color
--   where a.thru_date > current_date
-- 
-- -- sales
--   select *
--   from nc.vehicle_sales a
--   join nc.vehicles b on a.vin = b.vin
--   join sierra c on b.chrome_style_id = c.chrome_style_id
--     and b.engine = c.displacement
--     and b.color = c.color
--   where a.delivery_date between current_date - 65 and current_date - 1
--     and a.sale_type in ('retail','fleet')
-- 
-- select aa.*, bb.in_system, bb.in_transit, coalesce(cc.in_stock, 0), dd.sales
-- from ( -- base config
--   select distinct trim_level, displacement, color
--   from sierra) aa
-- left join ( -- in system/transit
--   select a.trim_level, a.displacement, a.color, 
--     count(*) filter (where b.category = 'in system') as in_system,
--     count(*) filter (where b.category = 'in transit') as in_transit
--   from sierra a
--   join nc.open_orders b on a.model_code = b.model_code
--     and a.peg = b.peg
--     and a.color = b.color
--     and a.model_year = b.model_year
--   join gmgl.vehicle_order_options c on b.order_number = c.order_number
--     and a.option_code = c.option_code
--     and c.thru_ts > now()
--   group by a.trim_level, a.displacement, a.color) bb on aa.trim_level = bb.trim_level and aa.color = bb.color
-- left join ( -- on ground
--   select c.trim_level, c.displacement, c.color, count(*) as in_stock
--   from nc.vehicle_acquisitions a
--   join nc.vehicles b on a.vin = b.vin
--   join sierra c on b.chrome_style_id = c.chrome_style_id
--     and b.engine = c.displacement
--     and b.color = c.color
--   where a.thru_date > current_date  
--   group by c.trim_level, c.displacement, c.color) cc on aa.trim_level = cc.trim_level and aa.color = cc.color
-- left join ( -- sales  
--   select c.trim_level, c.displacement, c.color, count(*) as sales
--   from nc.vehicle_sales a
--   join nc.vehicles b on a.vin = b.vin
--   join sierra c on b.chrome_style_id = c.chrome_style_id
--     and b.engine = c.displacement
--     and b.color = c.color
--   where a.delivery_date between current_date - 65 and current_date - 1
--     and a.sale_type in ('retail','fleet')
--   group by c.trim_level, c.displacement, c.color) dd on aa.trim_level = dd.trim_level and aa.color = dd.color
-- order by aa.trim_level, aa.displacement, aa.color
-- ------------------------------------------------------------------------------------
-- --/> sierra  
-- ------------------------------------------------------------------------------------
-- 
-- 
-- ------------------------------------------------------------------------------------
-- --< silverado crew 
-- 1500 Crew LT      5.3 Black
-- 1500 Crew LT      5.3 Summit White
-- 1500 Crew LTZ     5.3 Black
-- ------------------------------------------------------------------------------------
-- -- this is all possible configurations that meet the speec
-- drop table if exists silverado_crew cascade;
-- create temp table silverado_crew as 
-- select *
-- from (
--   select a.chrome_style_id, a.model_code, a.trim_level, c.color, b.displacement, b.option_code, a.peg, a.alloc_group, a.model_year
--   from jon.configurations a
--   join jon.engines b on a.chrome_style_id = b.chrome_style_id
--     and b.displacement = '5.3'
--   left join jon.exterior_colors c on a.chrome_style_id = c.chrome_style_id  
--     and c.color in ('Black','Summit White')
--   where a.model like 'silverado 1%'
--     and a.trim_level in ('LT','LTZ')
--     and cab = 'crew') aa
-- where (
--   (trim_level = 'LT' and color in ('Black','Summit White'))
--   or
--   (trim_level = 'LTZ' and color = 'Black'))
--   and model_year > 2018;
-- 
-- select aa.trim_level || ' ' || aa.displacement, null, aa.color, bb.in_system, bb.in_transit, coalesce(cc.in_stock, 0) as in_stock, dd.sales
-- from ( -- base config
--   select distinct trim_level, displacement, color
--   from silverado_crew) aa
-- left join ( -- in system/transit
--   select a.trim_level, a.displacement, a.color, 
--     count(*) filter (where b.category = 'in system') as in_system,
--     count(*) filter (where b.category = 'in transit') as in_transit
--   from silverado_crew a
--   join nc.open_orders b on a.model_code = b.model_code
--     and a.peg = b.peg
--     and a.color = b.color
--     and a.model_year = b.model_year
--   join gmgl.vehicle_order_options c on b.order_number = c.order_number
--     and a.option_code = c.option_code
--     and c.thru_ts > now()
--   group by a.trim_level, a.displacement, a.color) bb on aa.trim_level = bb.trim_level and aa.color = bb.color
-- left join ( -- on ground
--   select c.trim_level, c.displacement, c.color, count(*) as in_stock
--   from nc.vehicle_acquisitions a
--   join nc.vehicles b on a.vin = b.vin
--   join silverado_crew c on b.chrome_style_id = c.chrome_style_id
--     and b.engine = c.displacement
--     and b.color = c.color
--   where a.thru_date > current_date  
--   group by c.trim_level, c.displacement, c.color) cc on aa.trim_level = cc.trim_level and aa.color = cc.color
-- left join ( -- sales  
--   select c.trim_level, c.displacement, c.color, count(*) as sales
--   from nc.vehicle_sales a
--   join nc.vehicles b on a.vin = b.vin
--   join silverado_crew c on b.chrome_style_id = c.chrome_style_id
--     and b.engine = c.displacement
--     and b.color = c.color
--   where a.delivery_date between current_date - 65 and current_date - 1
--     and a.sale_type in ('retail','fleet')
--   group by c.trim_level, c.displacement, c.color) dd on aa.trim_level = dd.trim_level and aa.color = dd.color
-- order by aa.trim_level, aa.displacement, aa.color
-- 
-- ------------------------------------------------------------------------------------
-- --/> silverado  crew
-- ------------------------------------------------------------------------------------
-- 
-- 
-- ------------------------------------------------------------------------------------
-- --< silverado double 
-- 1500 Double LT      5.3 Black
-- 1500 Double LT      5.3 Summit White
-- ------------------------------------------------------------------------------------
-- -- this is all possible configurations that meet the speec
-- drop table if exists silverado_double cascade;
-- create temp table silverado_double as 
-- select a.chrome_style_id, a.model_code, a.trim_level, c.color, b.displacement, b.option_code, a.peg, a.alloc_group, a.model_year
-- from jon.configurations a
-- join jon.engines b on a.chrome_style_id = b.chrome_style_id
--   and b.displacement = '5.3'
-- left join jon.exterior_colors c on a.chrome_style_id = c.chrome_style_id  
--   and c.color in ('Black','Summit White')
-- where a.model like 'silverado 1%'
--   and a.trim_level = 'LT'
--   and cab = 'double'
--   and a.model_year > 2018;
-- 
-- select aa.trim_level || ' ' || aa.displacement, null, aa.color, bb.in_system, bb.in_transit, coalesce(cc.in_stock, 0), dd.sales
-- from ( -- base config
--   select distinct trim_level, displacement, color
--   from silverado_double) aa
-- left join ( -- in system/transit
--   select a.trim_level, a.displacement, a.color, 
--     count(*) filter (where b.category = 'in system') as in_system,
--     count(*) filter (where b.category = 'in transit') as in_transit
--   from silverado_double a
--   join nc.open_orders b on a.model_code = b.model_code
--     and a.peg = b.peg
--     and a.color = b.color
--     and a.model_year = b.model_year
--   join gmgl.vehicle_order_options c on b.order_number = c.order_number
--     and a.option_code = c.option_code
--     and c.thru_ts > now()
--   group by a.trim_level, a.displacement, a.color) bb on aa.trim_level = bb.trim_level and aa.color = bb.color
-- left join ( -- on ground
--   select c.trim_level, c.displacement, c.color, count(*) as in_stock
--   from nc.vehicle_acquisitions a
--   join nc.vehicles b on a.vin = b.vin
--   join silverado_double c on b.chrome_style_id = c.chrome_style_id
--     and b.engine = c.displacement
--     and b.color = c.color
--   where a.thru_date > current_date  
--   group by c.trim_level, c.displacement, c.color) cc on aa.trim_level = cc.trim_level and aa.color = cc.color
-- left join ( -- sales  
--   select c.trim_level, c.displacement, c.color, count(*) as sales
--   from nc.vehicle_sales a
--   join nc.vehicles b on a.vin = b.vin
--   join silverado_double c on b.chrome_style_id = c.chrome_style_id
--     and b.engine = c.displacement
--     and b.color = c.color
--   where a.delivery_date between current_date - 65 and current_date - 1
--     and a.sale_type in ('retail','fleet')
--   group by c.trim_level, c.displacement, c.color) dd on aa.trim_level = dd.trim_level and aa.color = dd.color
-- order by aa.trim_level, aa.displacement, aa.color
-- ------------------------------------------------------------------------------------
-- --/> silverado double
-- ------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
--< top_configs_combined_consensus_worksheet_data 01/24/20
------------------------------------------------------------------------------------
/*
*** may need to use PEG in addition to trim_level ***
*/
drop table if exists top_configs;
create temp table top_configs as
select 'equinox' as model, a.chrome_style_id, a.model_code, a.trim_level, c.color, b.displacement, b.option_code, a.peg, a.alloc_group, a.model_year
from jon.configurations a
join jon.engines b on a.chrome_style_id = b.chrome_style_id
  and b.displacement = '1.5'
left join jon.exterior_colors c on a.chrome_style_id = c.chrome_style_id  
  and c.color in ('Cajun Red Tintcoat','Nightfall Gray Metallic','Mosaic Black Metallic','Summit White')
where a.model = 'equinox'
  and a.trim_level in ( 'LT') and a.peg = '1LT'
  and a.drive = 'AWD'
union
select 'sierra', aa.*
from (
  select a.chrome_style_id, a.model_code, a.trim_level, c.color, b.displacement, b.option_code, a.peg, a.alloc_group, a.model_year
  from jon.configurations a
  join jon.engines b on a.chrome_style_id = b.chrome_style_id
  left join jon.exterior_colors c on a.chrome_style_id = c.chrome_style_id  
    and c.color in ('Onyx Black','White Frost Tricoat','Summit White')
  where a.model like 'sierra 1%'
    and a.trim_level in ('Denali','SLT')
    and cab = 'crew') aa
where (
  (trim_level = 'denali' and displacement = '6.2' and color in ('Onyx Black','White Frost Tricoat'))
  or
  (trim_level = 'SLT' and displacement = '5.3' and color in ('Onyx Black','Summit White')))
union
select 'silverado crew', aa.*
from (
  select a.chrome_style_id, a.model_code, a.trim_level, c.color, b.displacement, b.option_code, a.peg, a.alloc_group, a.model_year
  from jon.configurations a
  join jon.engines b on a.chrome_style_id = b.chrome_style_id
    and b.displacement = '5.3'
  left join jon.exterior_colors c on a.chrome_style_id = c.chrome_style_id  
    and c.color in ('Black','Summit White')
  where a.model like 'silverado 1%'
    and a.trim_level in ('LT','LTZ')
    and cab = 'crew') aa
where (
  (trim_level = 'LT' and color in ('Black','Summit White'))
  or
  (trim_level = 'LTZ' and color = 'Black'))
  and model_year > 2018
union
select 'silverado_double', a.chrome_style_id, a.model_code, a.trim_level, c.color, b.displacement, b.option_code, a.peg, a.alloc_group, a.model_year
from jon.configurations a
join jon.engines b on a.chrome_style_id = b.chrome_style_id
  and b.displacement = '5.3'
left join jon.exterior_colors c on a.chrome_style_id = c.chrome_style_id  
  and c.color in ('Black','Summit White')
where a.model like 'silverado 1%'
  and a.trim_level = 'LT'
  and cab = 'double'
  and a.model_year > 2018; 

-- select * from top_configs order by model, color


 select aa.model || ' ' || aa.trim_level || ' ' || aa.displacement, null, aa.color, 
   coalesce(bb.in_system, 0), coalesce(bb.in_transit, 0), coalesce(cc.in_stock, 0), dd.sales
from ( -- base config
  select distinct model, trim_level, displacement, color
  from top_configs) aa
left join ( -- in system/transit
  select a.model, a.trim_level, a.displacement, a.color, 
    count(*) filter (where b.category = 'in system') as in_system,
    count(*) filter (where b.category = 'in transit') as in_transit
  from top_configs a
  join nc.open_orders b on a.model_code = b.model_code
    and a.peg = b.peg
    and a.color = b.color
    and a.model_year = b.model_year
  join gmgl.vehicle_order_options c on b.order_number = c.order_number
    and a.option_code = c.option_code
    and c.thru_ts > now()
  group by a.model, a.trim_level, a.displacement, a.color) bb on aa.model = bb.model and aa.trim_level = bb.trim_level and aa.color = bb.color
left join ( -- on ground
  select c.model, c.trim_level, c.displacement, c.color, count(*) as in_stock
  from nc.vehicle_acquisitions a
  join nc.vehicles b on a.vin = b.vin
  join top_configs c on b.chrome_style_id = c.chrome_style_id
    and b.engine = c.displacement
    and b.color = c.color
  where a.thru_date > current_date  
  group by c.model, c.trim_level, c.displacement, c.color) cc on aa.model = cc.model and aa.trim_level = cc.trim_level and aa.color = cc.color
left join ( -- sales  
  select c.model, c.trim_level, c.displacement, c.color, count(*) as sales
  from nc.vehicle_sales a
  join nc.vehicles b on a.vin = b.vin
  join top_configs c on b.chrome_style_id = c.chrome_style_id
    and b.engine = c.displacement
    and b.color = c.color
--     and b.trim_level = c.trim_level
  where a.delivery_date between current_date - 65 and current_date - 1
    and a.sale_type in ('retail','fleet')
  group by c.model, c.trim_level, c.displacement, c.color) dd on aa.model = dd.model and aa.trim_level = dd.trim_level and aa.color = dd.color
order by aa.model, aa.trim_level, aa.displacement, aa.color 

------------------------------------------------------------------------------------
--/> top_configs_combined_consensus_worksheet_data 01/24/20
------------------------------------------------------------------------------------