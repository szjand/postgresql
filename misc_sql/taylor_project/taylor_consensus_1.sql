﻿-- ----------------------------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------------------------
-- 11/20/19
-- this is an extract from ...taylor_projects/sql/forecast_model_sales_history.sql of the current verions
-- of this project
-- ----------------------------------------------------------------------------------------------------------
-- create schema ta;
-- comment on schema ta is 'temporary location for tables used to generate taylors consensus page';

-- 11/24 all done and shipped out as V3
-- lots of changes to incorporate
-- each worksheet should be for a forecast model
-- 
-- a totals column that shows the totals (where relevant) for all active model years
-- model year            2018   2019   2020
-- total units sold       100    83     25
-- 
-- for the options section (colors, packages, engines, interiors, transmissions)
-- in the left column show all possible options in each category for that forecast model
-- in the data, if the option is applicable to a model but there are 0 data points display 0 / 0
--              if the option is not applicable to a model display blank
-- 
-- in the left column include these vague descriptors of the eventual goal(s):
-- days supply on the ground
-- days supply in system/in transit
-- balance to go
-- currently consensed
-- should now consense for
-- somehow coordinate around consensus calendar

        
-------------------------------------------------
--< headers
-------------------------------------------------
-- initially looking only at silverado 1500
-- 11/23/19: categorization needs to be by forecast model, this is a cheap and easy way to add it
-- also need make in headers
drop table if exists ta.headers;
create table ta.headers (
  chrome_style_id citext not null primary key,
  make citext not null,
  model_year integer not null,
  model_code citext not null,
  peg citext not null,
  description citext not null,
  forecast_model citext not null,
  seq integer);
create unique index on ta.headers(model_year, model_code, peg);
comment on table ta.headers is 'all possible configurations from chr.configurations for 2018 and above Silverado 1xxx';  
insert into ta.headers
select chrome_style_id, make, model_year, model_code, peg, 
  model_year::citext || ' ' || model || ' ' || drive || ' ' ||cab || ' Cab' ||' ' || box || ' Box' || ' ' || trim_level as description, 
  case cab
    when 'crew' then '1500 Crew'
    when 'double' then '1500 Double'
    when 'regular' then '1500 Reg'
  end as forecast_model,
  0 as seq
-- select *
from chr.configurations
where model like 'silverado 1%'
  and model_year > 2017
group by make, model_year, model_code, peg, chrome_style_id, model || ' ' || drive || ' ' ||cab || ' ' || box || ' ' || trim_level;


-------------------------------------------------
--/> headers
-------------------------------------------------



-------------------------------------------------
--< sales
-------------------------------------------------

drop table if exists ta.sales_22 cascade;
create table ta.sales_22 (
  stock_number citext not null,
  vin citext not null,
  bopmast_id integer not null,
  ground_date date not null,
  delivery_date date not null,
  sale_type citext not null,
  row_number integer not null);
comment on table ta.sales_22 is 'staging table for fleet and retail sales for model_year 2018 and above';

insert into ta.sales_22
select a.control, d.vin, d.bopmast_id, d.ground_date, d.delivery_date, d.sale_type, 
  row_number() over (partition by d.stock_number order by d.delivery_date)
from ( -- stock number from accounting sales
  select a.control
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.year_month >= 201701
  join fin.dim_account c on a.account_key = c.account_key
    and account_type = 'Sale'
    and department_code = 'NC'
    and typical_balance = 'Credit'
  join fin.dim_journal d on a.journal_key = d.journal_key
    and d.journal_code = 'VSN'
  where a.post_status = 'Y' 
  group by a.control
    having  sum(a.amount) > 10000 or sum(a.amount) < -10000) a
join arkona.xfm_bopmast b on a.control = b.bopmast_stock_number
  and b.record_status = 'U'
  and b.current_row
join arkona.xfm_inpmast c on b.bopmast_vin = c.inpmast_vin
  and c.current_row
  and c.year > 2017
join nc.vehicle_sales d on b.record_key = d.bopmast_id;

drop table if exists ta.sales_33 cascade;
create table ta.sales_33 (
  stock_number citext not null primary key,
  vin citext not null,
  bopmast_id integer not null,
  ground_date date not null,
  delivery_date date not null,
  sale_type citext not null,
  row_number integer not null);
comment on table ta.sales_33 is 'staging table for fleet and retail sales for model_year 2018 and above, most recent sale only';
insert into ta.sales_33
select a.*
from ta.sales_22 a
join (
  select stock_number, max(row_number) as row_number
  from ta.sales_22
  group by stock_number) b on a.stock_number = b.stock_number and a.row_number = b.row_number;

drop table if exists ta.sales_detail cascade;
create table ta.sales_detail (
  model_year integer not null,
  model_code citext not null,
  peg citext not null,
  chrome_style_id citext not null,
  stock_number citext primary key,
  vin citext not null,
  ground_date date not null,
  delivery_date date not null,
  sale_type citext not null,
  model citext not null,
  drive citext not null,
  cab citext not null,
  trim_level citext not null,
  color_code citext not null,
  engine_code citext not null,
  transmission_code citext not null,
  interior_code citext not null);
comment on table ta.sales_detail is 'detail data for all retail/fleet sales of model year 2018 and above silverado 1XXX';  

-- use invoice to generate color, engine, transmission, interior option codes
insert into ta.sales_detail 
select c.model_year, c.model_code, c.peg, c.chrome_style_id, 
  a.stock_number, a.vin, a.ground_date, a.delivery_date, a.sale_type,
  b.model, b.drive, b.cab, b.trim_level, 
  substring(d.raw_invoice from position(E'\n' in  d.raw_invoice) + 1 for 3) as color_code,
  substring(d.raw_invoice from position('ENGINE,' in d.raw_invoice) - 4 for 3) as eng_code, 
  substring(d.raw_invoice from position('TRANSMISSION,' in d.raw_invoice) - 4 for 3) as tran_code,
  substring(d.raw_invoice from position('RENAISSANCE' in d.raw_invoice) - 45 for 3) as int_code
from ta.sales_33 a
join nc.vehicles b on a.vin = b.vin
join ta.headers c on b.chrome_style_id = c.chrome_style_id
join gmgl.vehicle_invoices d on a.vin = d.vin
  and d.thru_date > current_date 
where a.sale_type in ('retail','fleet')
  and substring(d.raw_invoice from position('ENGINE,' in d.raw_invoice) - 4 for 3) <> '';
create index on ta.sales_detail(color_code);
create index on ta.sales_detail(engine_code);
create index on ta.sales_detail(interior_code);
create index on ta.sales_detail(transmission_code);
create index on ta.sales_detail(chrome_style_id);



-------------------------------------------------
--/> sales
-------------------------------------------------

-------------------------------------------------
--< set seq in headers include all sales, inventory & orders
-------------------------------------------------

-- this has to be run AFTER all the sales/inventory/order data has been generated
-- sales is 1st priority then inventory then in_sys_tran
update ta.headers x
set seq = y.seq
from (
  select aa.model_year, aa.model_code, aa.peg, aa.chrome_style_id, 
    coalesce(bb.sales,0) as sales, coalesce(cc.on_ground,0) as on_ground, coalesce(dd.in_sys_tran,0) as in_sys_tran, 
    coalesce(bb.sales,0) + coalesce(cc.on_ground,0)  + coalesce(dd.in_sys_tran,0) as total,
    row_number() over (order by aa.model_year, coalesce(bb.sales,0) desc,  coalesce(cc.on_ground,0) desc, coalesce(dd.in_sys_tran,0) desc) as seq
-- select *   
  from ta.headers aa
  left join (
    select model_year, model_code, peg, chrome_Style_id, 'sales' as source, count(*) as sales
    from ta.sales_detail
    group by model_year, model_code, peg, chrome_Style_id) bb on aa.model_year = bb.model_year and aa.chrome_style_id = bb.chrome_style_id
  left join (
    select c.model_year, c.model_code, c.peg, c.chrome_style_id, 'on_ground', count(*) as on_ground
    from nc.vehicle_acquisitions a
    join nc.vehicles b on a.vin = b.vin
    join ta.headers c on b.chrome_style_id = c.chrome_Style_id
    where a.thru_date > current_date
    group by c.chrome_style_id, c.model_year, c.model_code, c.peg  ) cc on aa.model_year = cc.model_year and aa.chrome_style_id = cc.chrome_style_id
  left join (
    select b.model_year, b.model_code, b.peg, b.chrome_style_id, 'in_sys_tran', count(*) as in_sys_tran
    from nc.open_orders a
    join ta.headers b on a.model_year = b.model_year
      and a.model_code = b.model_code
      and a.peg = b.peg
    group by b.model_year, b.model_code, b.peg, b.chrome_style_id) dd on aa.model_year = dd.model_year and aa.chrome_style_id = dd.chrome_style_id  
  order by aa.model_year, coalesce(bb.sales,0) desc,  coalesce(cc.on_ground,0) desc, coalesce(dd.in_sys_tran,0) desc) y
where x.model_year = y.model_year
  and x.model_code = y.model_code
  and x.peg = y.peg;      
-------------------------------------------------
--/> set seq in headers include all sales, inventory & orders
-------------------------------------------------
-------------------------------------------------
--< $$$
-------------------------------------------------

drop table if exists ta.gross_detail cascade;
create table ta.gross_detail (
  stock_number citext not null,
  chrome_style_id citext not null,
  gross_type citext not null,
  amount integer not null,
  primary key (stock_number, gross_type));
comment on table ta.gross_detail is 'gross for all vehicles in ta.sales_detail';
insert into ta.gross_detail  
select a.stock_number, a.chrome_style_id, d.gross_type, sum(-b.amount)::integer as amount
from ta.sales_detail a
join fin.fact_gl b on a.stock_number = b.control
  and b.post_status = 'Y'
join fin.dim_account c on b.account_key = c.account_key
join ( -- all gross accounts: types sales & f/i
  select distinct 'sales' as gross_type, d.gl_account, e.account_type, e.department
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month between 201701 and 201911
    and b.page between 5 and 15 -- new cars
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  inner join fin.dim_account e on d.gl_account = e.account
  union  -- f/i accounts
  select distinct 'f/i' as gross_type, d.gl_account, e.account_type, e.department
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month between 201701 and 201911
    and b.page = 17
    and b.line between 1 and 20
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  inner join fin.dim_account e on d.gl_account = e.account) d on c.account = d.gl_account  
group by a.stock_number, a.chrome_style_id, d.gross_type;


-------------------------------------------------
--/> $$$
-------------------------------------------------



-------------------------------------------------
--< forecast
-------------------------------------------------

-------------------------------------------------
--/> forecast
-------------------------------------------------

-------------------------------------------------
--< current inventory
-------------------------------------------------
drop table if exists ta.current_detail cascade;
create table ta.current_detail (
  chrome_style_id citext not null,
  order_number citext,
  vin citext, 
  color_code citext not null,
  engine_code citext not null,
  transmission_code citext not null,
  interior_code citext not null);
insert into ta.current_detail (chrome_style_id, order_number, color_code,
  engine_code, transmission_code, interior_code)
select aa.chrome_style_id, aa.order_number, 
  b.color_code, d.option_code as engine_code, h.option_code as transmission_code, f.option_code as interior_code
from (
  select a.order_number, b.chrome_style_id 
  from nc.open_orders a
  join ta.headers b on a.model_year = b.model_year
    and a.model_code = b.model_code
    and a.peg = b.peg
  left join arkona.xfm_inpmast c on a.vin = c.inpmast_vin
    and c.current_row
  where c.inpmast_vin is null) aa
join chr.exterior_colors b on aa.chrome_style_id = b.chrome_style_id
join gmgl.vehicle_order_options c on aa.order_number = c.order_number 
  and b.color_code = c.option_code
  and c.thru_ts > now()
join chr.engines d on aa.chrome_style_id = d.chrome_style_id
join gmgl.vehicle_order_options e on aa.order_number = e.order_number 
  and d.option_code = e.option_code
  and e.thru_ts > now()
join chr.interiors f on aa.chrome_style_id = f.chrome_style_id
join gmgl.vehicle_order_options g on aa.order_number = g.order_number 
  and f.option_code = g.option_code
  and g.thru_ts > now()
join chr.transmissions h on aa.chrome_style_id = h.chrome_style_id
join gmgl.vehicle_order_options i on aa.order_number = i.order_number 
  and h.option_code = i.option_code
  and i.thru_ts > now() where aa.chrome_Style_id = '399783';

insert into ta.current_detail (chrome_style_id, vin, color_code,
  engine_code, transmission_code, interior_code)  
select b.chrome_style_id, a.inpmast_vin, 
  substring(c.raw_invoice from position(E'\n' in  c.raw_invoice) + 1 for 3) as color_code,
  case
    when substring(c.raw_invoice from position('ENGINE,' in c.raw_invoice) - 4 for 3) = ''
    then substring(c.raw_invoice from position('ENGINE:' in c.raw_invoice) - 4 for 3)
    else substring(c.raw_invoice from position('ENGINE,' in c.raw_invoice) - 4 for 3)
  end as engine_code,
  case
    when substring(c.raw_invoice from position('TRANSMISSION,' in c.raw_invoice) - 4 for 3) = ''
    then substring(c.raw_invoice from position('TRANSMISSION:' in c.raw_invoice) - 4 for 3)
    else substring(c.raw_invoice from position('TRANSMISSION,' in c.raw_invoice) - 4 for 3)
  end as transmission_code,
  substring(c.raw_invoice from position('RENAISSANCE' in c.raw_invoice) - 45 for 3) as int_code
from arkona.xfm_inpmast a
join ta.headers b on a.chrome_style_id::citext = b.chrome_style_id
join gmgl.vehicle_invoices c on a.inpmast_vin = c.vin
  and c.thru_date > current_date
where a.current_row
  and a.status = 'I'
  and a.type_n_u = 'N';

create unique index on ta.current_detail(coalesce(vin, order_number));    
create index on ta.current_detail(chrome_style_id);    
create index on ta.current_detail(engine_code);  
create index on ta.current_detail(color_code);  
create index on ta.current_detail(transmission_code);  
create index on ta.current_detail(interior_code);  

-------------------------------------------------
--/> current inventory
-------------------------------------------------


-------------------------------------------------
--< timeline
-------------------------------------------------

-- days order to invoice
drop table if exists order_to_invoice;
create temp table order_to_invoice as
select chrome_style_id, avg(invoice_date - date_ordered)::integer as avg_days_order_to_invoice
from (
  select aa.model_year, aa.chrome_style_id, cc.invoice_date, bb.date_ordered
  from ta.sales_detail aa
  join (
    select a.order_number, a.order_type, a.vin, a.model_year, a.alloc_group, a.make, a.model_code, a.peg,
      min(b.from_date) filter ( where left(b.event_code,1) = '3') as date_ordered
    from gmgl.vehicle_orders a
    join gmgl.vehicle_order_events b on a.order_number = b.order_number
      and left(b.event_code, 1) = '3'
    group by a.order_number, a.order_type, a.vin, a.model_year, a.alloc_group, a.make, a.model_code, a.peg) bb on aa.vin = bb.vin
  join nc.vehicle_invoices cc on aa.vin = cc.vin  
  union -- current inventory
  select aa.year, aa.chrome_style_id::citext, cc.invoice_date, bb.date_ordered
  from arkona.xfm_inpmast aa
  join (
    select a.order_number, a.order_type, a.vin, a.model_year, a.alloc_group, a.make, a.model_code, a.peg,
      min(b.from_date) filter ( where left(b.event_code,1) = '3') as date_ordered
    from gmgl.vehicle_orders a
    join gmgl.vehicle_order_events b on a.order_number = b.order_number
      and left(b.event_code, 1) = '3'
    group by a.order_number, a.order_type, a.vin, a.model_year, a.alloc_group, a.make, a.model_code, a.peg) bb on aa.inpmast_vin = bb.vin
  join nc.vehicle_invoices cc on aa.inpmast_vin = cc.vin  
  join ta.headers dd on aa.chrome_style_id::citext = dd.chrome_style_id
  where aa.current_row
    and aa.status = 'I'
    and aa.type_n_u = 'N'
    and aa.inpmast_Stock_number not like 'H%') ee
group by chrome_style_id;  

-- days ground to sold
drop table if exists ground_to_sold;
create temp table ground_to_sold as
select chrome_style_id, avg(delivery_date - ground_date)::integer as avg_days_ground_to_sold
from ta.sales_detail
group by chrome_style_id;

-------------------------------------------------
--/> timeline
-------------------------------------------------


-- initially, this looks promising to generate an excellish presentation
-- will need a comprehensive list of header from sales plus inventory plus orders
-- what is needed for each "row table" is chrome_Style_id and a value
-- added col_3 as a placeholder, most of these rows can/should have a total column first, before config breakouts
-- 11/23/19 add forecast model as part of the header and as a filter
-- col 3 becomes total, where relevant (avg msrp, 
do $$
declare
  _model_year integer := 2019;
  _forecast_model citext := '1500 Crew';
  _year_1 integer := 2018;
  _year_2 integer := 2019;
  _year_3 integer := 2020;
begin  
  drop table if exists wtf;
    create temp table wtf as
    select *
    from (
      select 1 as the_order, _model_year::citext || ' ' || _forecast_model as col_2,  '' as col_3, ''  
      union
      select 1.1, 'model code', '' as col_3, array_to_string(
        array(
          select model_code 
          from ta.headers a
          where model_year = _model_year
            and forecast_model = _forecast_model
          order by seq), ',')
      union
      select 2, 'peg',  '' as col_3, array_to_string(
        array(
          select peg 
          from ta.headers a
          where model_year = _model_year
            and forecast_model = _forecast_model
          order by seq), ',')
      union
      select 2.5, 'chrome style id',  '' as col_3, array_to_string(
        array(
          select chrome_style_id 
          from ta.headers a
          where model_year = _model_year
            and forecast_model = _forecast_model
          order by seq), ',')                      
      union
      select 3, 'description',  'totals', array_to_string(
        array(
          select description 
          from ta.headers a
          where model_year = _model_year
            and forecast_model = _forecast_model
          order by seq), ',')   
      union
      select 3.1, '',  '2018 / 2019 /2020', ''    
      union
      select 4, 'avg msrp',     
          ( -- totals
          select 
            coalesce(((avg(coalesce(b.total_msrp, c.list_price)) filter (where a.model_year = _year_1))::integer)::text, '0')
            || ' / ' ||
            coalesce(((avg(coalesce(b.total_msrp, c.list_price)) filter (where a.model_year = _year_2))::integer)::text, '0')
            || ' / ' ||
            coalesce(((avg(coalesce(b.total_msrp, c.list_price)) filter (where a.model_year = _year_3))::integer)::text, '0')            
          from ta.sales_detail a
          join ta.headers aa on a.chrome_style_id = aa.chrome_style_id
            and aa.forecast_model = _forecast_model
          left join gmgl.vehicle_invoices b on a.vin = b.vin
            and b.thru_date > current_date
          left join arkona.xfm_inpmast c on a.stock_number = c.inpmast_stock_number
            and c.current_row),   
        array_to_string(
          array(
            select coalesce(b.avg_msrp, 0)::text as avg_msrp
            from ta.headers a
            left join (
              select a.chrome_style_id, avg(coalesce(b.total_msrp, c.list_price))::integer as avg_msrp
              from ta.sales_detail a
              left join gmgl.vehicle_invoices b on a.vin = b.vin
                and b.thru_date > current_date
              left join arkona.xfm_inpmast c on a.stock_number = c.inpmast_stock_number
                and c.current_row  
              group by a.chrome_style_id) b on a.chrome_style_id = b.chrome_Style_id
            where a.model_year = _model_year
              and forecast_model = _forecast_model
            order by a.seq), ',')      
      union
      select 5, 'total units sold',  
        ( -- total
          select 
            coalesce(((count(*) filter (where a.model_year = _year_1))::integer)::text, '0')
            || ' / ' ||
            coalesce(((count(*) filter (where a.model_year = _year_2))::integer)::text, '0')
            || ' / ' ||
            coalesce(((count(*) filter (where a.model_year = _year_3))::integer)::text, '0')                       
          from ta.sales_detail a
          join ta.headers b on a.chrome_style_id = b.chrome_style_id
            and forecast_model = _forecast_model),         
        array_to_string(
          array(
            select coalesce(c.sales, 0)::text as sales
            from ta.headers a
            left join (
              select b.chrome_style_id, count(*) as sales
              from ta.sales_detail a
              join ta.headers b on a.chrome_style_id = b.chrome_style_id
              group by b.chrome_style_id
              having count(*) > 0) c on a.chrome_style_id = c.chrome_Style_id
            where a.model_year = _model_year
              and forecast_model = _forecast_model
            order by a.seq), ',')
      union
      select 6, 'avg gross',  
        ( -- total
          select 
            case
              when count(*) filter (where b.model_year = _year_1) = 0 then '0'
              else 
                ((sum(a.amount) filter (where b.model_year = _year_1) 
                /
                count(*) filter (where b.model_year = _year_1))::integer)::text
            end 
            || ' / ' ||
            case
              when count(*) filter (where b.model_year = _year_2) = 0 then '0'
              else 
                ((sum(a.amount) filter (where b.model_year = _year_2) 
                /
                count(*) filter (where b.model_year = _year_2))::integer)::text
            end    
            || ' / ' ||
            case
              when count(*) filter (where b.model_year = _year_3) = 0 then '0'
              else 
                ((sum(a.amount) filter (where b.model_year = _year_3) 
                /
                count(*) filter (where b.model_year = _year_3))::integer)::text
            end                          
          from ta.gross_detail a
          join ta.headers b on a.chrome_style_id = b.chrome_style_id
            and b.forecast_model = _forecast_model),      
        array_to_string(
          array(
            select coalesce(c.avg_gross, 0)::text as sales
            from ta.headers a
            left join (
              select a.chrome_style_id, (sum(b.amount)/count(a.vin))::integer as avg_gross
              from ta.sales_detail a
              join ( -- total gross
                select stock_number, sum(amount) as amount
                from ta.gross_detail
                group by stock_number) b on a.stock_number = b.stock_number
              group by a.chrome_style_id) c on a.chrome_style_id = c.chrome_Style_id
            where a.model_year = _model_year
              and forecast_model = _forecast_model
            order by a.seq), ',')  
      union
      select 7, 'avg front gross',  
        ( -- total
          select 
            case 
              when aa.sales = 0 then 0::text
              else ((bb.amount/aa.sales)::integer)::text
            end as avg_front_gross
          from ( -- total sales
            select count(*) as sales
            from ta.sales_detail a
            join ta.headers b on a.chrome_style_id = b.chrome_style_id
              AND a.model_year = _model_year
              and forecast_model = _forecast_model) aa
          join (    
            select sum(a.amount) as amount
            from ta.gross_detail a
            join ta.headers b on a.chrome_style_id = b.chrome_style_id
              and b.model_year = _model_year
              and b.forecast_model = _forecast_model
            where a.gross_type = 'sales') bb on true),      
        array_to_string(
          array(
            select coalesce(bbb.avg_front_gross, 0)::text as sales
            from ta.headers aaa
            left join (
              select aa.chrome_style_id, (bb.amount/aa.sales)::integer as avg_front_gross
              from ( -- total sales
                select b.chrome_style_id, count(*) as sales
                from ta.sales_detail a
                join ta.headers b on a.chrome_style_id = b.chrome_style_id
                group by b.chrome_style_id
                having count(*) > 0) aa
              join ( -- total front gross
                select chrome_style_id, sum(amount) as amount
                from ta.gross_detail
                where gross_type = 'sales'
                group by chrome_style_id) bb on aa.chrome_style_id = bb.chrome_style_id) bbb on aaa.chrome_style_id = bbb.chrome_Style_id
            where aaa.model_year = _model_year
              and forecast_model = _forecast_model
            order by aaa.seq), ',')
      union
      select 8, 'avg f/i gross',  
        ( -- total
          select 
            case 
              when aa.sales = 0 then 0::text
              else ((bb.amount/aa.sales)::integer)::text
            end as avg_front_gross
          from ( -- total sales
            select count(*) as sales
            from ta.sales_detail a
            join ta.headers b on a.chrome_style_id = b.chrome_style_id
              AND a.model_year = _model_year
              and forecast_model = _forecast_model) aa
          join (    
            select coalesce(sum(a.amount), 0) as amount
            from ta.gross_detail a
            join ta.headers b on a.chrome_style_id = b.chrome_style_id
              and b.model_year = _model_year
              and b.forecast_model = _forecast_model
            where a.gross_type = 'f/i') bb on true),          
        array_to_string(
          array(
            select coalesce(bbb.avg_fi_gross, 0)::text as sales
            from ta.headers aaa
            left join (
              select aa.chrome_style_id, (bb.amount/aa.sales)::integer as avg_fi_gross
              from ( -- total sales
                select b.chrome_style_id, count(*) as sales
                from ta.sales_detail a
                join ta.headers b on a.chrome_style_id = b.chrome_style_id
                group by b.chrome_style_id
                having count(*) > 0) aa
              join ( -- total fi gross
                select chrome_style_id, sum(amount) as amount
                from ta.gross_detail
                where gross_type = 'f/i'
                group by chrome_style_id) bb on aa.chrome_style_id = bb.chrome_style_id) bbb on aaa.chrome_style_id = bbb.chrome_Style_id
            where aaa.model_year = _model_year
              and forecast_model = _forecast_model
            order by aaa.seq), ',')    
      union
      select 9, 'sold in last 90 days',  
        ( -- total
          select 
            coalesce(((count(*) filter (where a.model_year = _year_1))::integer)::text, '0')
            || ' / ' ||
            coalesce(((count(*) filter (where a.model_year = _year_2))::integer)::text, '0')
            || ' / ' ||
            coalesce(((count(*) filter (where a.model_year = _year_3))::integer)::text, '0')                       
          from ta.sales_detail a
          join ta.headers b on a.chrome_style_id = b.chrome_style_id
            and forecast_model = _forecast_model  
          where a.delivery_date > current_date - 90),      
        array_to_string(
          array(
            select coalesce(c.sales, 0)::text as sales
            from ta.headers a
            left join (
              select b.chrome_style_id, count(*) as sales
              from ta.sales_detail a
              join ta.headers b on a.chrome_style_id = b.chrome_style_id
              where a.delivery_date > current_date - 90 
              group by b.chrome_style_id
              having count(*) > 0) c on a.chrome_style_id = c.chrome_Style_id
            where a.model_year = _model_year
              and forecast_model = _forecast_model
            order by a.seq), ',')  
      union
      select 10, 'forecast for next 90 days', 
        ( 
          select sum(forecast)::text as forecast
          from nc.forecast a
          join (
            select forecast_model, make
            from ta.headers b 
            where b.model_year = _model_year
              and b.forecast_model = _forecast_model
            group by forecast_model, make) c on a.model = c.forecast_model
              and a.make = c.make
          where a.thru_ts > now()
            and a.year_month in (
              select distinct year_month
              from dds.dim_date
              where year_month > (
                select distinct year_month
                from dds.dim_date
                where the_date = current_date) 
              order by year_month
              limit 3)), ''         
      union
      select 11, 'on the ground',  
        ( -- total
          select 
            coalesce(((count(*) filter (where c.model_year = _year_1))::integer)::text, '0')
            || ' / ' ||
            coalesce(((count(*) filter (where c.model_year = _year_2))::integer)::text, '0')
            || ' / ' ||
            coalesce(((count(*) filter (where c.model_year = _year_3))::integer)::text, '0') 
          from nc.vehicle_acquisitions a
          join nc.vehicles b on a.vin = b.vin
          join ta.headers c on b.chrome_style_id = c.chrome_Style_id
            and c.forecast_model = _forecast_model
          where a.thru_date > current_date),        
        array_to_string(
          array(
            select coalesce(bb.on_ground, 0)::text as on_ground
            from ta.headers aa
            left join (
              select c.chrome_style_id, count(*) as on_ground
              from nc.vehicle_acquisitions a
              join nc.vehicles b on a.vin = b.vin
              join ta.headers c on b.chrome_style_id = c.chrome_Style_id
              where a.thru_date > current_date
              group by c.chrome_style_id) bb on aa.chrome_style_id = bb.chrome_Style_id
            where aa.model_year = _model_year
              and forecast_model = _forecast_model
            order by aa.seq), ',')
      union
      select 12, 'in system/in transit', 
        ( -- total
          select 
            coalesce(((count(*) filter (where b.model_year = _year_1))::integer)::text, '0')
            || ' / ' ||
            coalesce(((count(*) filter (where b.model_year = _year_2))::integer)::text, '0')
            || ' / ' ||
            coalesce(((count(*) filter (where b.model_year = _year_3))::integer)::text, '0')           
          from nc.open_orders a
          join ta.headers b on a.model_year = b.model_year
            and a.model_code = b.model_code
            and a.peg = b.peg
            and b.forecast_model = _forecast_model),         
        array_to_string(
          array(
            select coalesce(bb.in_sys_tran, 0)::text as in_sys_tran
            from ta.headers aa
            left join (
              select b.chrome_style_id, count(*) as in_sys_tran
              from nc.open_orders a
              join ta.headers b on a.model_year = b.model_year
                and a.model_code = b.model_code
                and a.peg = b.peg
              group by b.chrome_style_id) bb on aa.chrome_style_id = bb.chrome_Style_id
            where aa.model_year = _model_year
              and forecast_model = _forecast_model
            order by aa.seq), ',')          
      union
      select 13, 'long/short', '', '' as col_3
      union
      select 14, 'avg # days from order to invoice',  '' as col_3, array_to_string(
        array(
          select coalesce(b.avg_days_order_to_invoice, 0)::text as in_sys_tran
          from ta.headers a
          left join order_to_invoice b on a.chrome_style_id = b.chrome_Style_id
          where a.model_year = _model_year
            and forecast_model = _forecast_model
          order by a.seq), ',')
      union
      select 15, 'avg # days on the ground to sold',  '' as col_3, array_to_string(
        array(
          select coalesce(b.avg_days_ground_to_sold, 0)::text as in_sys_tran
          from ta.headers a
          left join ground_to_sold b on a.chrome_style_id = b.chrome_Style_id
          where a.model_year = _model_year
            and forecast_model = _forecast_model
          order by a.seq), ',')
      union
      select 16, 'days supply on the ground', '', '' as col_3
      union
      select 17, 'days supply in system/in transit', '', '' as col_3  
      union
      select 18, 'THE GOAL IS TO PRODUCE:', '', '' as col_3
      union
      select 21.5, '   include balance to go', '', '' as col_3
      union
      select 20, '   currently consensed/should now consense for', '', '' as col_3
      union
      select 21, '   somehow coordinate around the consenseus calendar', '', '' as col_3   
      union
      select 22, 'EXTERIOR COLOR', '' as col_3, array_to_string(
        array(
          select b.sub_header
          from ta.headers a
          left join (
            select model_year, chrome_style_id, ('sold / current')::text as sub_header
            from ta.headers
            where model_year = _model_year
              and forecast_model = _forecast_model) b on a.model_year = b.model_year and a.chrome_Style_id = b.chrome_style_id        
          where a.model_year = _model_year
            and forecast_model = _forecast_model
          order by a.seq), ',')                               
    ) aa                 
    order by the_order, col_2;
end $$;    
select * from wtf;




-------------------------------------------------
--< colors
-------------------------------------------------
-- 11/24/19
-- ok, time to try to figure out how to implement taylors request
-- first got engines to work
-- to reiterate
-- left column should list all possible options for the forecast model
-- if the option is not available for the model the cell should be empty
-- if the option is available for the model and the sales & current data = 0, the cell should show 0%/0%
-- this is the table to be fed into jon.colpivot
-- in fact it appears that at least for interiors, transmissions & engines, the current display distinguishes
-- between the 2 cases, simply with a display of just ' / ' for 0
-- not sure about colors 
-- pretty sure packages dont list all options

drop table if exists ta.colors;
create table ta.colors (
  model_year integer not null,
  forecast_model citext not null,
  chrome_style_id citext not null,
  color citext not null,
  perc_sold_current citext not null,
  seq integer not null,
  primary key (chrome_style_id,color));
insert into ta.colors  
select aa.model_year, aa.forecast_model, aa.chrome_style_id, aa.color_code || '-' || aa.color as color, 
  coalesce(bb.colors_perc_sold::text || '%', '0%') ||' ' || '  /  ' || coalesce(cc.colors_perc_current::text || '%','0%') || '' as perc_sold_current, aa.seq
from ( -- all colors
  select a.model_year, a.forecast_model, a.chrome_style_id, a.seq, b.color_code, b.color
  from ta.headers a
  join chr.exterior_colors b on a.chrome_style_id = b.chrome_style_id) aa
--   where a.model_year = 2019) aa
left join (-- colors_perc_sold
  select a.chrome_style_id, b.color_code, (100.0 * b.color_sales/a.total_sales)::integer as colors_perc_sold 
  from (
    select chrome_style_id, count(*) as total_sales 
    from ta.sales_detail
--     where model_year = 2019
    group by chrome_style_id) a
  join (
    select model_year, chrome_style_id, color_code, count(*) as color_sales
    from ta.sales_detail
--     where model_year = 2019
    group by model_year, chrome_style_id, color_code) b on a.chrome_style_id = b.chrome_style_id) bb on aa.chrome_style_id = bb.chrome_style_id
    and aa.color_code = bb.color_code
left join ( -- colors_perc_current
  select a.chrome_style_id, b.color_code, (100.0 * b.color_current/a.total_current)::integer as colors_perc_current
  from (
    select chrome_style_id, count(*) as total_current
    from ta.current_detail
    group by chrome_style_id) a
  join (
    select chrome_style_id, color_code, count(*) as color_current
    from ta.current_detail
    group by chrome_style_id, color_code) b on a.chrome_style_id = b.chrome_style_id) cc on aa.chrome_style_id = cc.chrome_style_id
    and aa.color_code = cc.color_code;
 

-- drop table if exists _test_ext_colors;
-- select jon.colpivot('_test_ext_colors', 'select * from ta.colors where model_year = 2019 and forecast_model = ''1500 Reg'' and color not like ''01%''',
--     array['color'], array['chrome_style_id'], '#.perc_sold_current', 'max(seq)');
-- select .01*(row_number() over()), a.* from _test_ext_colors a;  


-------------------------------------------------
--/> colors
-------------------------------------------------




-------------------------------------------------
--< interiors
-------------------------------------------------

-- this is  the end result for colors
-- this is the table to be fed into jon.colpivot
drop table if exists ta.interiors;
create table ta.interiors (
  model_year integer not null,
  forecast_model citext not null,
  chrome_style_id citext not null,
  interior citext not null,
  perc_sold_current citext not null,
  seq integer not null,
  primary key (chrome_style_id,interior));
insert into ta.interiors
select aa.model_year, aa.forecast_model, aa.chrome_style_id, aa.option_code || '-' || initcap(aa.interior), 
  coalesce(bb.interiors_perc_sold::text || '%', '0%') ||'' || '  /  ' || coalesce(cc.interiors_perc_current::text || '%','0%') || '' as perc_sold_current, aa.seq
from ( -- all interiors
  select a.model_year, a.forecast_model, a.chrome_style_id, a.seq, b.option_code, b.material || '-' || b.color as interior
  from ta.headers a
  join chr.interiors b on a.chrome_style_id = b.chrome_style_id) aa
--   where a.model_year = 2019) aa
left join (-- interiors_perc_sold
  select a.chrome_style_id, b.interior_code, (100.0 * b.interior_sales/a.total_sales)::integer as interiors_perc_sold 
  from (
    select chrome_style_id, count(*) as total_sales 
    from ta.sales_detail
    group by chrome_style_id) a
  join (
    select model_year, chrome_style_id, interior_code, count(*) as interior_sales
    from ta.sales_detail
--     where model_year = 2019
    group by model_year, chrome_style_id, interior_code) b on a.chrome_style_id = b.chrome_style_id) bb on aa.chrome_style_id = bb.chrome_style_id
    and aa.option_code = bb.interior_code
left join ( -- interiors_perc_current
  select a.chrome_style_id, b.interior_code, (100.0 * b.interior_current/a.total_current)::integer as interiors_perc_current
  from (
    select chrome_style_id, count(*) as total_current 
    from ta.current_detail
    group by chrome_style_id) a
  join (
    select chrome_style_id, interior_code, count(*) as interior_current
    from ta.current_detail
    group by chrome_style_id, interior_code) b on a.chrome_style_id = b.chrome_style_id) cc on aa.chrome_style_id = cc.chrome_style_id
    and aa.option_code = cc.interior_code;

 
-- drop table if exists _test_ext_interiors;
-- select jon.colpivot('_test_ext_interiors', 'select * from ta.interiors where model_year = 2019 and forecast_model = ''1500 Double''',
--     array['interior'], array['chrome_style_id'], '#.perc_sold_current', 'max(seq)');
-- select (row_number() over()), a.* from _test_ext_interiors a;  

-------------------------------------------------
--/> interiors
-------------------------------------------------


-------------------------------------------------
--< engines
-------------------------------------------------

drop table if exists ta.engines;
create table ta.engines (
  model_year integer not null,
  forecast_model citext not null,
  chrome_style_id citext not null,
  engine citext not null,
  perc_sold_current citext not null,
  seq integer not null,
  primary key (chrome_style_id,engine));
insert into ta.engines
select aa.model_year, aa.forecast_model, aa.chrome_style_id, aa.option_code || '-' || initcap(replace(aa.engine, ',', ';')) as engine, 
  coalesce(bb.engines_perc_sold::text || '%', '0%') ||'' || '  /  ' || coalesce(cc.engines_perc_current::text || '%','0%') || '' as perc_sold_current, aa.seq
from ( -- all engines
  select a.model_year, a.forecast_model, a.chrome_style_id, a.seq, b.option_code, b.description as engine
  from ta.headers a
  join chr.engines b on a.chrome_style_id = b.chrome_style_id) aa
--   where a.model_year = 2019) aa
left join (-- engines_perc_sold
  select a.chrome_style_id, b.engine_code, (100.0 * b.engine_sales/a.total_sales)::integer as engines_perc_sold 
  from (
    select chrome_style_id, count(*) as total_sales 
    from ta.sales_detail
    group by chrome_style_id) a
  join (
    select model_year, chrome_style_id, engine_code, count(*) as engine_sales
    from ta.sales_detail
--     where model_year = 2019
    group by model_year, chrome_style_id, engine_code) b on a.chrome_style_id = b.chrome_style_id) bb on aa.chrome_style_id = bb.chrome_style_id
    and aa.option_code = bb.engine_code
left join ( -- engines_perc_current
  select a.chrome_style_id, b.engine_code, (100.0 * b.engine_current/a.total_current)::integer as engines_perc_current
  from (
    select chrome_style_id, count(*) as total_current
    from ta.current_detail
    group by chrome_style_id) a
  join (
    select chrome_style_id, engine_code, count(*) as engine_current
    from ta.current_detail
    group by chrome_style_id, engine_code) b on a.chrome_style_id = b.chrome_style_id) cc on aa.chrome_style_id = cc.chrome_style_id
    and aa.option_code = cc.engine_code;



-- drop table if exists _test_ext_engines;
-- select jon.colpivot('_test_ext_engines', 'select * from ta.engines where model_year = 2019 and forecast_model = ''1500 Crew''',
--     array['engine'], array['chrome_style_id'], '#.perc_sold_current', 'max(seq)');
-- select 100*(row_number() over()), a.* from _test_ext_engines a;  



-------------------------------------------------
--/> engines
-------------------------------------------------

-------------------------------------------------
--< transmissions
-------------------------------------------------

drop table if exists ta.transmissions;
create table ta.transmissions (
  model_year integer not null,
  forecast_model citext not null,
  chrome_style_id citext not null,
  transmission citext not null,
  perc_sold_current citext not null,
  seq integer not null,
  primary key (chrome_style_id,transmission));
insert into ta.transmissions
select aa.model_year, aa.forecast_model, aa.chrome_style_id, aa.option_code || '-' || initcap(replace(aa.transmission, ',', ';')) as transmission,   
  coalesce(bb.transmissions_perc_sold::text || '%', '0%') ||'' || '  /  ' || coalesce(cc.transmissions_perc_current::text || '%','0%') || '' as perc_sold_current, aa.seq
from ( -- all transmission
  select a.model_year, a.forecast_model, a.chrome_style_id, a.seq, b.option_code, b.description as transmission
  from ta.headers a
  join chr.transmissions b on a.chrome_style_id = b.chrome_style_id) aa
--   where a.model_year = 2019) aa
left join (-- transmissions_perc_sold
  select a.chrome_style_id, b.transmission_code, (100.0 * b.transmission_sales/a.total_sales)::integer as transmissions_perc_sold 
  from (
    select chrome_style_id, count(*) as total_sales 
    from ta.sales_detail
    group by chrome_style_id) a
  join (
    select model_year, chrome_style_id, transmission_code, count(*) as transmission_sales
    from ta.sales_detail
--     where model_year = 2019
    group by model_year, chrome_style_id, transmission_code) b on a.chrome_style_id = b.chrome_style_id) bb on aa.chrome_style_id = bb.chrome_style_id
    and aa.option_code = bb.transmission_code
left join ( -- transmissions_perc_current
  select a.chrome_style_id, b.transmission_code, (100.0 * b.transmission_current/a.total_current)::integer as transmissions_perc_current
  from (
    select chrome_style_id, count(*) as total_current
    from ta.current_detail
    group by chrome_style_id) a
  join (
    select chrome_style_id, transmission_code, count(*) as transmission_current
    from ta.current_detail
    group by chrome_style_id, transmission_code) b on a.chrome_style_id = b.chrome_style_id) cc on aa.chrome_style_id = cc.chrome_style_id
    and aa.option_code = cc.transmission_code;


-- drop table if exists _test_ext_transmissions;
-- select jon.colpivot('_test_ext_transmissions', 'select * from ta.transmissions where model_year = 2019 and forecast_model = ''1500 Crew''',
--     array['transmission'], array['chrome_style_id'], '#.perc_sold_current', 'max(seq)');
-- select (10^4)*(row_number() over()), a.* from _test_ext_transmissions a;  

-------------------------------------------------
--/> transmissions
-------------------------------------------------



------------------------------------------------------------------
--< packages
------------------------------------------------------------------

drop table if exists ta.packages;
create table ta.packages (
  model_year integer not null,
  forecast_model citext not null,
  chrome_style_id citext not null,
  packages citext not null,
  perc_sold_current citext not null,
  seq integer not null,
  primary key (chrome_style_id,packages));
insert into ta.packages
select aa.model_year, aa.forecast_model, aa.chrome_style_id, aa.option_code || '-' || replace(aa.package, ',', ';') as package, 
  coalesce(bb.packages_perc_sold::text || '%', '0%') ||' ' || '  /  ' || coalesce(cc.packages_perc_current::text || '%','0%') || '' as perc_sold_current, aa.seq
from ( -- all package
  select a.model_year, a.forecast_model, a.chrome_style_id, a.seq, b.option_code, b.description as package
  from ta.headers a
  join chr.packages b on a.chrome_style_id = b.chrome_style_id) aa
--   where a.model_year = 2019) aa
left join (-- packages_perc_sold
  select a.chrome_style_id, b.option_code, (100.0 * b.package_sales/a.total_sales)::integer as packages_perc_sold 
  from (
    select chrome_style_id, count(*) as total_sales 
    from ta.sales_detail
    group by chrome_style_id) a
  join (
    select a.chrome_style_id, b.option_code, count(*) as package_sales
    from ta.sales_detail a
    join chr.packages b on a.chrome_style_id = b.chrome_style_id
    join gmgl.vehicle_option_codes c on a.vin = c.vin
      and b.option_code = c.option_code
    group by a.chrome_style_id, b.option_code) b on a.chrome_style_id = b.chrome_style_id) bb on aa.chrome_style_id = bb.chrome_style_id
    and aa.option_code = bb.option_code
left join ( -- packages_perc_current
  select a.chrome_style_id, b.option_code, (100.0 * b.package_current/a.total_current)::integer as packages_perc_current
  from (
    select chrome_style_id, count(*) as total_current
    from ta.current_detail
    group by chrome_style_id) a
  join (
    select chrome_style_id, option_code, count(*) package_current
    from (
      select 'order' as source, a.chrome_style_id, b.option_code
      from ta.current_detail a
      join chr.packages b on a.chrome_style_id = b.chrome_style_id
      join gmgl.vehicle_order_options c on a.order_number = c.order_number
        and b.option_code = c.option_code
        and c.thru_ts > now()
      union all
      select 'vin' as source, a.chrome_style_id, b.option_code
      from ta.current_detail a
      join chr.packages b on a.chrome_style_id = b.chrome_style_id
      join gmgl.vehicle_option_codes c on a.vin = c.vin 
        and b.option_code = c.option_code) x
    group by chrome_style_id, option_code ) b on a.chrome_style_id = b.chrome_style_id) cc on aa.chrome_style_id = cc.chrome_style_id
    and aa.option_code = cc.option_code;




-- drop table if exists _test_ext_packages;
-- -- select jon.colpivot('_test_ext_packages', 'select * from ta.packages where model_year = 2019 and trim(perc_sold_current) <> ''/''',
-- --     array['packages'], array['chrome_style_id'], '#.perc_sold_current', 'max(seq)');
-- select jon.colpivot('_test_ext_packages', 'select * from ta.packages where model_year = 2019',-- and forecast_model = ''1500 Double''',
--     array['packages'], array['chrome_style_id'], '#.perc_sold_current', 'max(seq)');    
-- select (10^6)*(row_number() over()), a.* from _test_ext_packages a order by packages;  


------------------------------------------------------------------
--/> packages
------------------------------------------------------------------


drop table if exists _test_ext_colors;
select jon.colpivot('_test_ext_colors', 'select * from ta.colors where model_year = 2019 and forecast_model = ''1500 Crew'' and color not like ''01%''',
    array['color'], array['chrome_style_id'], '#.perc_sold_current', 'max(seq)');
drop table if exists _test_ext_interiors;
select jon.colpivot('_test_ext_interiors', 'select * from ta.interiors where model_year = 2019 and forecast_model = ''1500 Crew''',
    array['interior'], array['chrome_style_id'], '#.perc_sold_current', 'max(seq)');
drop table if exists _test_ext_engines;
select jon.colpivot('_test_ext_engines', 'select * from ta.engines where model_year = 2019 and forecast_model = ''1500 Crew''',
    array['engine'], array['chrome_style_id'], '#.perc_sold_current', 'max(seq)');
drop table if exists _test_ext_transmissions;
select jon.colpivot('_test_ext_transmissions', 'select * from ta.transmissions where model_year = 2019 and forecast_model = ''1500 Crew''',
    array['transmission'], array['chrome_style_id'], '#.perc_sold_current', 'max(seq)');
drop table if exists _test_ext_packages;
select jon.colpivot('_test_ext_packages', 'select * from ta.packages where model_year = 2019 and forecast_model = ''1500 Crew''',
    array['packages'], array['chrome_style_id'], '#.perc_sold_current', 'max(seq)');    
 

 

select *
from (
select .01*(row_number() over(order by color)) as seq, a.* from _test_ext_colors a) aa
union  
select *
from (
select (row_number() over(order by interior)), a.* from _test_ext_interiors a) bb
union
select * 
from (
select 10^2*(row_number() over(order by engine)), a.* from _test_ext_engines a ) cc
union
select *
from (
select (10^4)*(row_number() over(order by transmission)), a.* from _test_ext_transmissions a) dd
union   
select *
from (
select (10^6)*(row_number() over(order by packages)), a.* from _test_ext_packages a ) ee
order by seq

