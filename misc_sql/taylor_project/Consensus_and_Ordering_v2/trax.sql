﻿-- og
drop table if exists og;
create temp table og as
select a.year_month, a.the_date, d.chrome_style_id, d.model_year, d.make, d.model, d.model_code, 
  d. drive, d.cab, d.trim_level, d.engine, c.color, e.peg, e.alloc_group
from dds.dim_date a
join nc.vehicle_acquisitions b on b.ground_date <= a.the_date
  and b.thru_date > a.the_date
join nc.vehicles c on b.vin = c.vin  
  and model = 'trax'
join nc.vehicle_configurations d on c.configuration_id = d.configuration_id
join jon.configurations e on d.chrome_style_id = e.chrome_style_id
where a.the_date between '01/01/2019' and current_date;

-- sales
drop table if exists sales;
create table sales as
select b.year_month, a.delivery_date, d.chrome_style_id, d.model_year, d.make, d.model, d.model_code,
  d.drive, d.cab, d.trim_level, d.engine, c.color, e.peg, e.alloc_group
from (
  select vin, max(delivery_date) as delivery_date
  from nc.vehicle_sales a
  join dds.dim_date b on a.delivery_date = b.the_date
    and the_date between '01/01/2019' and current_date
  where sale_type in ('retail','fleet')
  group by a.vin) a
join nc.vehicle_sales aa on a.vin = aa.vin
  and a.delivery_date = aa.delivery_date  
join dds.dim_date b on a.delivery_date = b.the_date  
join nc.vehicles c on a.vin = c.vin  
  and model = 'trax'
join nc.vehicle_configurations d on c.configuration_id = d.configuration_id
join jon.configurations e on d.chrome_style_id = e.chrome_style_id;


drop table if exists cars;
create table cars as
select alloc_group, model, model_code, drive, peg, 
  array_agg(distinct model_year) as model_year, array_agg(distinct trim_level) as trim_level, 
  array_agg(chrome_style_id) as chrome_style_id
from jon.configurations
where model = 'trax'
group by alloc_group, model, model_code, drive, peg;

select aa.*, bb.model_year, bb.model, bb.chrome_Style_id, bb.trim_level
from (
  select b.alloc_group, b.model, b.model_code, b.drive, b.peg, 
    count(*) filter (where year_month = 201901) as jan,
    count(*) filter (where year_month = 201902) as feb,
    count(*) filter (where year_month = 201903) as mar,
    count(*) filter (where year_month = 201904) as apr,
    count(*) filter (where year_month = 201905) as may,
    count(*) filter (where year_month = 201906) as jun,
    count(*) filter (where year_month = 201907) as jul,
    count(*) filter (where year_month = 201908) as aug,
    count(*) filter (where year_month = 201909) as sep,
    count(*) filter (where year_month = 201910) as oct,
    count(*) filter (where year_month = 201911) as nov,
    count(*) filter (where year_month = 201912) as dec,
    count(*) as total_sales, 
    c.days_og, c.min_sales, count(*) - c.min_sales as diff 
  from sales a
  join cars b on a.chrome_style_id = any(b.chrome_style_id)
  join (
    select b.alloc_group, b.model, b.model_code, b.drive, b.peg, count(distinct the_date) days_og, count(distinct the_date)/15 as min_sales
    from og a
    join cars b on a.chrome_style_id = any(b.chrome_style_id)
    group by b.alloc_group, b.model, b.model_code, b.drive, b.peg) c on b.alloc_group = c.alloc_group
      and b.model = c.model
      and b.model_code = c.model_code
      and b.peg = c.peg
  group by b.alloc_group, b.model, b.model_code, b.drive, b.peg, c.days_og, c.min_sales) aa
join cars bb on aa.alloc_group = bb.alloc_group
      and aa.model = bb.model
      and aa.model_code = bb.model_code
      and aa.peg = bb.peg
-- where total_sales >= min_sales  
-- order by total_sales - min_sales  desc
order by total_sales desc


select *
from nc.vehicle_acquisitions a
join nc.vehicles b on a.vin = b.vin
  and b.model = 'trax'
order by ground_Date  


select *
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
  and b.model = 'trax'
where sale_type in ('retail','fleet')  
order by delivery_Date 

select the_date, count(*) 
from og
group by the_date
order by the_date


select *
from nc.vehicle_acquisitions a
join nc.vehicles b on a.vin = b.vin
  and b.model = 'trax'
where ground_date <= '12/02/2019' and thru_date > '12/02/2019'  

select a.year_month, a.the_date, d.chrome_style_id, d.model_year, d.make, d.model, d.model_code, 
  d. drive, d.cab, d.trim_level, d.engine, c.color, e.peg, e.alloc_group
from dds.dim_date a
join nc.vehicle_acquisitions b on b.ground_date <= a.the_date
  and b.thru_date > a.the_date
join nc.vehicles c on b.vin = c.vin  
  and model = 'trax'
join nc.vehicle_configurations d on c.configuration_id = d.configuration_id
join jon.configurations e on d.chrome_style_id = e.chrome_style_id
where a.the_date between '01/01/2019' and current_date;
