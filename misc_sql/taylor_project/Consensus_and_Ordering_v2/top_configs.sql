﻿/*
this is the query from ...\taylor_project\sql\new_top_configs\sql used to determine
top configs based on days on ground
*/


---------------------------------------------------------------------------------------
--< 03/25/20 modified from calendar year 2019, to past year
-- exclude fleet orders from sales
---------------------------------------------------------------------------------------
drop table if exists inv_og cascade;
create temp table inv_og as
-- row for each day a chrome_Style_id is on the ground is on the ground
select a.year_month, a.the_date, d.chrome_style_id, d.model_year, d.make, d.model, d.model_code, 
  d. drive, d.cab, d.trim_level, d.engine, c.color, e.peg, e.alloc_group
from dds.dim_date a
join nc.vehicle_acquisitions b on b.ground_date <= a.the_date
  and b.thru_date > a.the_date
join nc.vehicles c on b.vin = c.vin  
  and make not in ('honda','nissan')
join nc.vehicle_configurations d on c.configuration_id = d.configuration_id
join jon.configurations e on d.chrome_style_id = e.chrome_style_id
-- where a.the_year = 2019;
where a.the_date between '03/01/2019' and '02/29/2020';
create index on inv_og(chrome_style_id);
create index on inv_og(the_date);
create index on inv_og(drive);
create index on inv_og(cab);

 -- past year sales
 -- exclude fleet orders
drop table if exists sales;
create temp table sales as
select b.year_month, a.delivery_date, d.chrome_style_id, d.model_year, d.make, d.model, d.model_code,
  d.drive, d.cab, d.trim_level, d.engine, c.color, e.peg, e.alloc_group
from (
  select vin, max(delivery_date) as delivery_date
  from nc.vehicle_sales a
  join dds.dim_date b on a.delivery_date = b.the_date
--     and b.the_year = 2019
    and b.the_date between '03/01/2019' and '02/29/2020'
  where sale_type in ('retail','fleet')
  group by a.vin) a
join nc.vehicle_sales aa on a.vin = aa.vin
  and a.delivery_date = aa.delivery_date  
join dds.dim_date b on a.delivery_date = b.the_date  
join nc.vehicles c on a.vin = c.vin  
  and c.make not in ('honda','nissan')
join nc.vehicle_configurations d on c.configuration_id = d.configuration_id
join jon.configurations e on d.chrome_style_id = e.chrome_style_id
where not exists (
  select 1
  from gmgl.vehicle_orders
  where vin = a.vin 
    and coalesce(order_type, 'ok') in ('FBC','FNR'));

-- maybe start with allocation groups
drop table if exists pickup_alloc_groups;
create temp table pickup_alloc_groups as
select (
  case
    when model like '%1500%' and model like 'silverado%' then 'Silverado 1500 ' || cab::citext
    when model like '%1500%' and model like 'sierra%' then 'Sierra 1500 ' || cab::citext
    else alloc_group
  end)::citext, array_agg(distinct chrome_style_id) as style_ids
from jon.configurations
where alloc_group <> 'n/a' -- exclude honda nissan 
  and model not similar to '%(2500|3500|4500|5500|6500)%' -- exclude HD
  and cab <> 'n/a' -- exclude cars
group by   
  case
    when model like '%1500%' and model like 'silverado%' then 'Silverado 1500 ' || cab::citext
    when model like '%1500%' and model like 'sierra%' then 'Sierra 1500 ' || cab::citext
    else alloc_group
  end;

drop table if exists pickups cascade;
create temp table pickups as
select a.alloc_group, b.cab, b.drive, b.box, b.peg, array_agg(distinct b.model_year) as model_year, 
  array_agg(distinct b.model) as model, array_agg(distinct b.chrome_style_id) as chrome_style_id, 
  array_agg(distinct b.trim_level) as trim_level
from pickup_alloc_groups a
join jon.configurations b on b.chrome_style_id = any(a.style_ids)
group by a.alloc_group, b.cab, b.drive, b.box, b.peg;
create unique index on pickups(alloc_group,cab,drive,box,peg);
create index on pickups(chrome_style_id);  

-- refactor cars
drop table if exists cars cascade;
create temp table cars as
select alloc_group, model, model_code, drive, peg, 
  array_agg(distinct model_year) as model_year, array_agg(distinct trim_level) as trim_level, 
  array_agg(chrome_style_id) as chrome_style_id
from jon.configurations
where cab = 'N/A'
  and make not in ('honda','nissan')
group by alloc_group, model, model_code, drive, peg;
create unique index on cars(alloc_group, model_code, peg);
create index on cars(chrome_style_id);


select aa.*, bb.model_year, bb.model, bb.chrome_Style_id, bb.trim_level
from (
  select b.alloc_group, b.cab, b.drive, b.box, b.peg, 
    count(*) filter (where year_month = 201903) as mar,
    count(*) filter (where year_month = 201904) as apr,
    count(*) filter (where year_month = 201905) as may,
    count(*) filter (where year_month = 201906) as jun,
    count(*) filter (where year_month = 201907) as jul,
    count(*) filter (where year_month = 201908) as aug,
    count(*) filter (where year_month = 201909) as sep,
    count(*) filter (where year_month = 201910) as oct,
    count(*) filter (where year_month = 201911) as nov,
    count(*) filter (where year_month = 201912) as dec,
    count(*) filter (where year_month = 202001) as jan,
    count(*) filter (where year_month = 202002) as feb,
    count(*) as total_sales, 
    c.days_og, c.min_sales, count(*) - c.min_sales as diff 
  from sales a
  join pickups b on a.chrome_style_id = any(b.chrome_style_id)
  join (
    select b.alloc_group, b.cab, b.drive, b.box, b.peg, count(distinct the_date) days_og, count(distinct the_date)/15 as min_sales
    from inv_og a
    join pickups b on a.chrome_style_id = any(b.chrome_style_id)
    group by b.alloc_group, b.cab, b.drive, b.box, b.peg) c on b.alloc_group = c.alloc_group
      and b.cab = c.cab
      and b.drive = c.drive
      and b.box = c.box
      and b.peg = c.peg
  group by b.alloc_group, b.cab, b.drive, b.box, b.peg, c.days_og, c.min_sales) aa
join pickups bb on aa.alloc_group = bb.alloc_group
      and aa.cab = bb.cab
      and aa.drive = bb.drive
      and aa.box = bb.box
      and aa.peg = bb.peg
-- where total_sales >= min_sales  
order by  total_sales - min_sales desc;




select aa.*, bb.model_year, bb.model, bb.chrome_Style_id, bb.trim_level
from (
  select b.alloc_group, b.model, b.model_code, b.drive, b.peg, 
    count(*) filter (where year_month = 201903) as mar,
    count(*) filter (where year_month = 201904) as apr,
    count(*) filter (where year_month = 201905) as may,
    count(*) filter (where year_month = 201906) as jun,
    count(*) filter (where year_month = 201907) as jul,
    count(*) filter (where year_month = 201908) as aug,
    count(*) filter (where year_month = 201909) as sep,
    count(*) filter (where year_month = 201910) as oct,
    count(*) filter (where year_month = 201911) as nov,
    count(*) filter (where year_month = 201912) as dec,
    count(*) filter (where year_month = 202001) as jan,
    count(*) filter (where year_month = 202002) as feb,
    count(*) as total_sales, 
    c.days_og, c.min_sales, count(*) - c.min_sales as diff 
  from sales a
  join cars b on a.chrome_style_id = any(b.chrome_style_id)
  join (
    select b.alloc_group, b.model, b.model_code, b.drive, b.peg, count(distinct the_date) days_og, count(distinct the_date)/15 as min_sales
    from inv_og a
    join cars b on a.chrome_style_id = any(b.chrome_style_id)
    group by b.alloc_group, b.model, b.model_code, b.drive, b.peg) c on b.alloc_group = c.alloc_group
      and b.model = c.model
      and b.model_code = c.model_code
      and b.peg = c.peg
  group by b.alloc_group, b.model, b.model_code, b.drive, b.peg, c.days_og, c.min_sales) aa
join cars bb on aa.alloc_group = bb.alloc_group
      and aa.model = bb.model
      and aa.model_code = bb.model_code
      and aa.peg = bb.peg
-- where total_sales >= min_sales  
-- order by total_sales - min_sales  desc
order by  total_sales - min_sales desc

---------------------------------------------------------------------------------------
--< 03/25/20 modified from calendar year 2019, to past year
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
--< 01/28/20 this is it
---------------------------------------------------------------------------------------



drop table if exists inv_og cascade;
create temp table inv_og as
select a.year_month, a.the_date, d.chrome_style_id, d.model_year, d.make, d.model, d.model_code, 
  d. drive, d.cab, d.trim_level, d.engine, c.color, e.peg, e.alloc_group
from dds.dim_date a
join nc.vehicle_acquisitions b on b.ground_date <= a.the_date
  and b.thru_date > a.the_date
join nc.vehicles c on b.vin = c.vin  
  and make not in ('honda','nissan')
join nc.vehicle_configurations d on c.configuration_id = d.configuration_id
join jon.configurations e on d.chrome_style_id = e.chrome_style_id
where a.the_year = 2019;
create index on inv_og(chrome_style_id);
create index on inv_og(the_date);
create index on inv_og(drive);
create index on inv_og(cab);

 -- 2019 sales
drop table if exists sales;
create temp table sales as
select b.year_month, a.delivery_date, d.chrome_style_id, d.model_year, d.make, d.model, d.model_code,
  d.drive, d.cab, d.trim_level, d.engine, c.color, e.peg, e.alloc_group
from (
  select vin, max(delivery_date) as delivery_date
  from nc.vehicle_sales a
  join dds.dim_date b on a.delivery_date = b.the_date
    and b.the_year = 2019
  where sale_type in ('retail','fleet')
  group by a.vin) a
join nc.vehicle_sales aa on a.vin = aa.vin
  and a.delivery_date = aa.delivery_date  
join dds.dim_date b on a.delivery_date = b.the_date  
join nc.vehicles c on a.vin = c.vin  
  and c.make not in ('honda','nissan')
join nc.vehicle_configurations d on c.configuration_id = d.configuration_id
join jon.configurations e on d.chrome_style_id = e.chrome_style_id;

-- maybe start with allocation groups
drop table if exists pickup_alloc_groups;
create temp table pickup_alloc_groups as
select (
  case
    when model like '%1500%' and model like 'silverado%' then 'Silverado 1500 ' || cab::citext
    when model like '%1500%' and model like 'sierra%' then 'Sierra 1500 ' || cab::citext
    else alloc_group
  end)::citext, array_agg(distinct chrome_style_id) as style_ids
from jon.configurations
where alloc_group <> 'n/a' -- exclude honda nissan 
  and model not similar to '%(2500|3500|4500|5500|6500)%' -- exclude HD
  and cab <> 'n/a' -- exclude cars
group by   
  case
    when model like '%1500%' and model like 'silverado%' then 'Silverado 1500 ' || cab::citext
    when model like '%1500%' and model like 'sierra%' then 'Sierra 1500 ' || cab::citext
    else alloc_group
  end;


drop table if exists pickups cascade;
create temp table pickups as
select a.alloc_group, b.cab, b.drive, b.box, b.peg, array_agg(distinct b.model_year) as model_year, 
  array_agg(distinct b.model) as model, array_agg(distinct b.chrome_style_id) as chrome_style_id, 
  array_agg(distinct b.trim_level) as trim_level
from pickup_alloc_groups a
join jon.configurations b on b.chrome_style_id = any(a.style_ids)
group by a.alloc_group, b.cab, b.drive, b.box, b.peg;
create unique index on pickups(alloc_group,cab,drive,box,peg);
create index on pickups(chrome_style_id);

-- refactor cars
drop table if exists cars cascade;
create temp table cars as
select alloc_group, model, model_code, drive, peg, 
  array_agg(distinct model_year) as model_year, array_agg(distinct trim_level) as trim_level, 
  array_agg(chrome_style_id) as chrome_style_id
from jon.configurations
where cab = 'N/A'
  and make not in ('honda','nissan')
group by alloc_group, model, model_code, drive, peg;
create unique index on cars(alloc_group, model_code, peg);
create index on cars(chrome_style_id);

select aa.*, bb.model_year, bb.model, bb.chrome_Style_id, bb.trim_level
from (
  select b.alloc_group, b.cab, b.drive, b.box, b.peg, 
    count(*) filter (where year_month = 201901) as jan,
    count(*) filter (where year_month = 201902) as feb,
    count(*) filter (where year_month = 201903) as mar,
    count(*) filter (where year_month = 201904) as apr,
    count(*) filter (where year_month = 201905) as may,
    count(*) filter (where year_month = 201906) as jun,
    count(*) filter (where year_month = 201907) as jul,
    count(*) filter (where year_month = 201908) as aug,
    count(*) filter (where year_month = 201909) as sep,
    count(*) filter (where year_month = 201910) as oct,
    count(*) filter (where year_month = 201911) as nov,
    count(*) filter (where year_month = 201912) as dec,
    count(*) as total_sales, 
    c.days_og, c.min_sales, count(*) - c.min_sales as diff 
  from sales a
  join pickups b on a.chrome_style_id = any(b.chrome_style_id)
  join (
    select b.alloc_group, b.cab, b.drive, b.box, b.peg, count(distinct the_date) days_og, count(distinct the_date)/15 as min_sales
    from inv_og a
    join pickups b on a.chrome_style_id = any(b.chrome_style_id)
    group by b.alloc_group, b.cab, b.drive, b.box, b.peg) c on b.alloc_group = c.alloc_group
      and b.cab = c.cab
      and b.drive = c.drive
      and b.box = c.box
      and b.peg = c.peg
  group by b.alloc_group, b.cab, b.drive, b.box, b.peg, c.days_og, c.min_sales) aa
join pickups bb on aa.alloc_group = bb.alloc_group
      and aa.cab = bb.cab
      and aa.drive = bb.drive
      and aa.box = bb.box
      and aa.peg = bb.peg
-- where total_sales >= min_sales  
order by  total_sales - min_sales desc;




select aa.*, bb.model_year, bb.model, bb.chrome_Style_id, bb.trim_level
from (
  select b.alloc_group, b.model, b.model_code, b.drive, b.peg, 
    count(*) filter (where year_month = 201901) as jan,
    count(*) filter (where year_month = 201902) as feb,
    count(*) filter (where year_month = 201903) as mar,
    count(*) filter (where year_month = 201904) as apr,
    count(*) filter (where year_month = 201905) as may,
    count(*) filter (where year_month = 201906) as jun,
    count(*) filter (where year_month = 201907) as jul,
    count(*) filter (where year_month = 201908) as aug,
    count(*) filter (where year_month = 201909) as sep,
    count(*) filter (where year_month = 201910) as oct,
    count(*) filter (where year_month = 201911) as nov,
    count(*) filter (where year_month = 201912) as dec,
    count(*) as total_sales, 
    c.days_og, c.min_sales, count(*) - c.min_sales as diff 
  from sales a
  join cars b on a.chrome_style_id = any(b.chrome_style_id)
  join (
    select b.alloc_group, b.model, b.model_code, b.drive, b.peg, count(distinct the_date) days_og, count(distinct the_date)/15 as min_sales
    from inv_og a
    join cars b on a.chrome_style_id = any(b.chrome_style_id)
    group by b.alloc_group, b.model, b.model_code, b.drive, b.peg) c on b.alloc_group = c.alloc_group
      and b.model = c.model
      and b.model_code = c.model_code
      and b.peg = c.peg
  group by b.alloc_group, b.model, b.model_code, b.drive, b.peg, c.days_og, c.min_sales) aa
join cars bb on aa.alloc_group = bb.alloc_group
      and aa.model = bb.model
      and aa.model_code = bb.model_code
      and aa.peg = bb.peg
-- where total_sales >= min_sales  
-- order by total_sales - min_sales  desc
order by  total_sales - min_sales desc

---------------------------------------------------------------------------------------
--/> 01/28/20 this is it
---------------------------------------------------------------------------------------
