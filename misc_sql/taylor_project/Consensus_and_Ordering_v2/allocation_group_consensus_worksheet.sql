﻿/*
02/12/20
these are the queries from ...taylor_project\sql\consensus_rules.sql that generated the spreadsheet
...docs\summary_01_29_20\docs\summary_01_29_20
that now generates the data for ..taylor_project\Consensus_and_Ordering_v2\docs\allocation_group_consensus_worksheet.xlsx

02/25/20
  sales needs to exclude fleet orders
  per ben, includes SRE: sold retail orders
*/
drop table if exists models;
create temp table models as
select bb.category, bb.make, bb.forecast_model, array_agg(distinct aa.alloc_group) as alloc_group,
  array_agg(distinct bb.model_code) as model_code
from (
  select alloc_group
  from jon.configurations
  where alloc_group <> 'n/a'
  group by alloc_group) aa
join (
  select a.category, a.make, a.model as forecast_model, b.alloc_group, b.chrome_style_id, b.model_code
  from nc.forecast_model_configurations a
  join nc.vehicle_configurations b on a.configuration_id = b.configuration_id) bb on aa.alloc_group = bb.alloc_group
group by bb.category, bb.make, bb.forecast_model;


drop table if exists in_system_transit;
create temp table in_system_transit as
select a.category, a.make, a.forecast_model, coalesce(sum(b.in_system), 0) as in_system, coalesce(sum(b.in_transit), 0) as in_transit
from models a
join (
select alloc_group, model_code,
  count(*) filter (where category = 'in system') as in_system,
  count(*) filter (where category = 'in transit') as in_transit
from nc.open_orders
group by alloc_group, model_code) b on b.alloc_group = any(a.alloc_group) and b.model_code = any(a.model_code)
group by a.category, a.make, a.forecast_model
order by a.category, a.make, a.forecast_model;

-- drop table if exists sales;
-- create temp table sales as
-- select a.category, a.make, a.forecast_model, coalesce(sum(b.sales), 0) as sales
-- from models a
-- join (
--   select c.alloc_group, b.model_code, count(*) as sales
--   from nc.vehicle_sales a
--   join nc.vehicles b on a.vin = b.vin
--   join jon.configurations c on b.chrome_style_id = c.chrome_style_id
--   where a.sale_type in ('retail','fleet')
--     and a.delivery_date between current_date - 65 and current_date - 6
--     and a.stock_number not like 'H%'
--   group by c.alloc_group, b.model_code) b on b.alloc_group = any(a.alloc_group) and b.model_code = any(a.model_code)
-- group by a.category, a.make, a.forecast_model
-- order by a.category, a.make, a.forecast_model;

drop table if exists sales;
create temp table sales as
select a.category, a.make, a.forecast_model, coalesce(sum(b.sales), 0) as sales
from models a
join (
  select c.alloc_group,  b.model_code, count(*) as sales
  from nc.vehicle_sales a
  join nc.vehicles b on a.vin = b.vin
  join jon.configurations c on b.chrome_style_id = c.chrome_style_id
  left join gmgl.vehicle_orders d on a.vin = d.vin
  where a.sale_type in ('retail','fleet')
    and a.delivery_date between current_date - 65 and current_date - 6
    and a.stock_number not like 'H%'
    and coalesce(d.order_type, 'ok') not in ('FBC','FNR')
  group by c.alloc_group, b.model_code) b on b.alloc_group = any(a.alloc_group) and b.model_code = any(a.model_code)
group by a.category, a.make, a.forecast_model
order by a.category, a.make, a.forecast_model;

drop table if exists in_stock;
create temp table in_stock as
select a.category, a.make, a.forecast_model, coalesce(sum(b.in_stock), 0) as in_stock
from models a
join (
  select c.alloc_group, c.model_code, count(*) as in_stock
  from nc.vehicle_acquisitions a
  join nc.vehicles b on a.vin = b.vin
  join jon.configurations c on b.chrome_style_id = c.chrome_style_id
  where a.thru_date > current_date
    and a.stock_number not like 'H%'
  group by c.alloc_group, c.model_code) b on b.alloc_group = any(a.alloc_group) and b.model_code = any(a.model_code)
group by a.category, a.make, a.forecast_model
order by a.category, a.make, a.forecast_model;

! -------------- hard coded forecast months ------------------------!
drop table if exists forecast_1;
create temp table forecast_1 as
select a.category, a.make, a.forecast_model, coalesce(sum(b.forecast), 0) as forecast
from models a
join (
  select category, make, model, sum(forecast) as forecast
  from nc.forecast a
  where a.current_row
    and year_month in (202002, 202003)
  group by category, make, model) b on a.make = b.make and a.forecast_model = b.model
group by a.category, a.make, a.forecast_model
order by a.category, a.make, a.forecast_model;  

! -------------- hard coded forecast months ------------------------!
drop table if exists forecast_2;
create temp table forecast_2 as
select a.category, a.make, a.forecast_model, coalesce(sum(b.forecast), 0) as forecast
from models a
join (
  select category, make, model, sum(forecast) as forecast
  from nc.forecast a
  where a.current_row
    and year_month in (202004, 202005)
  group by category, make, model) b on a.make = b.make and a.forecast_model = b.model
group by a.category, a.make, a.forecast_model
order by a.category, a.make, a.forecast_model;  

select a.category, a.make, a.forecast_model, 
  coalesce(b.in_system, 0) as in_system, coalesce(b.in_transit, 0) as in_transit, 
  coalesce(c.in_stock, 0) as in_stock, coalesce(d.sales, 0) as sales,
  coalesce(e.forecast, 0) as forecast_1, coalesce(f.forecast, 0) as forecast_2
from models a
left join in_system_transit b on a.make = b.make
  and a.forecast_model = b.forecast_model
left join in_stock c on a.make = c.make
  and a.forecast_model = c.forecast_model
left join sales d on a.make = d.make
  and a.forecast_model = d.forecast_model  
left join forecast_1 e on a.make = e.make
  and a.forecast_model = e.forecast_model    
left join forecast_2 f on a.make = f.make
  and a.forecast_model = f.forecast_model      
where f.forecast <> 0  
order by category, make, forecast_model
