﻿/*

2/16/20

start with ...\misc_sql\taylor_project\sql\top_configs.sql
which was used to generate the consensus_worksheet_top_configs.xlsl which was one of the summary_01_29_20
group of spreadsheets that was distributed to all

the purpose is to look at in stock vs in system at the color level (forecast not available at this level)

i thinks this is the 2nd place to go in v2

once we no what to consense to at the allocation group level, look at status at this much finer level

think this is part of what i think is desired, a drilldown into performance/status

*/
------------------------------------------------------------------------------------
--< top_configs_combined_consensus_worksheet_data 01/24/20
------------------------------------------------------------------------------------
/*
*** may need to use PEG in addition to trim_level ***
*/
drop table if exists top_configs;
create temp table top_configs as
select 'equinox' as model, a.chrome_style_id, a.model_code, a.trim_level, c.color, b.displacement, b.option_code, a.peg, a.alloc_group, a.model_year
from jon.configurations a
join jon.engines b on a.chrome_style_id = b.chrome_style_id
  and b.displacement = '1.5'
left join jon.exterior_colors c on a.chrome_style_id = c.chrome_style_id  
  and c.color in ('Cajun Red Tintcoat','Nightfall Gray Metallic','Mosaic Black Metallic','Summit White')
where a.model = 'equinox'
  and a.trim_level in ( 'LT') and a.peg = '1LT'
  and a.drive = 'AWD'
union
select 'sierra', aa.*
from (
  select a.chrome_style_id, a.model_code, a.trim_level, c.color, b.displacement, b.option_code, a.peg, a.alloc_group, a.model_year
  from jon.configurations a
  join jon.engines b on a.chrome_style_id = b.chrome_style_id
  left join jon.exterior_colors c on a.chrome_style_id = c.chrome_style_id  
    and c.color in ('Onyx Black','White Frost Tricoat','Summit White')
  where a.model like 'sierra 1%'
    and a.trim_level in ('Denali','SLT')
    and cab = 'crew') aa
where (
  (trim_level = 'denali' and displacement = '6.2' and color in ('Onyx Black','White Frost Tricoat'))
  or
  (trim_level = 'SLT' and displacement = '5.3' and color in ('Onyx Black','Summit White')))
union
select 'silverado crew', aa.*
from (
  select a.chrome_style_id, a.model_code, a.trim_level, c.color, b.displacement, b.option_code, a.peg, a.alloc_group, a.model_year
  from jon.configurations a
  join jon.engines b on a.chrome_style_id = b.chrome_style_id
    and b.displacement = '5.3'
  left join jon.exterior_colors c on a.chrome_style_id = c.chrome_style_id  
    and c.color in ('Black','Summit White')
  where a.model like 'silverado 1%'
    and a.trim_level in ('LT','LTZ')
    and cab = 'crew') aa
where (
  (trim_level = 'LT' and color in ('Black','Summit White'))
  or
  (trim_level = 'LTZ' and color = 'Black'))
  and model_year > 2018
union
select 'silverado_double', a.chrome_style_id, a.model_code, a.trim_level, c.color, b.displacement, b.option_code, a.peg, a.alloc_group, a.model_year
from jon.configurations a
join jon.engines b on a.chrome_style_id = b.chrome_style_id
  and b.displacement = '5.3'
left join jon.exterior_colors c on a.chrome_style_id = c.chrome_style_id  
  and c.color in ('Black','Summit White')
where a.model like 'silverado 1%'
  and a.trim_level = 'LT'
  and cab = 'double'
  and a.model_year > 2018; 

-- select * from top_configs order by model, color


 select aa.model || ' ' || aa.trim_level || ' ' || aa.displacement, null, aa.color, 
   coalesce(bb.in_system, 0), coalesce(bb.in_transit, 0), coalesce(cc.in_stock, 0), dd.sales
from ( -- base config
  select distinct model, trim_level, displacement, color
  from top_configs) aa
left join ( -- in system/transit
  select a.model, a.trim_level, a.displacement, a.color, 
    count(*) filter (where b.category = 'in system') as in_system,
    count(*) filter (where b.category = 'in transit') as in_transit
  from top_configs a
  join nc.open_orders b on a.model_code = b.model_code
    and a.peg = b.peg
    and a.color = b.color
    and a.model_year = b.model_year
  join gmgl.vehicle_order_options c on b.order_number = c.order_number
    and a.option_code = c.option_code
    and c.thru_ts > now()
  group by a.model, a.trim_level, a.displacement, a.color) bb on aa.model = bb.model and aa.trim_level = bb.trim_level and aa.color = bb.color
left join ( -- on ground
  select c.model, c.trim_level, c.displacement, c.color, count(*) as in_stock
  from nc.vehicle_acquisitions a
  join nc.vehicles b on a.vin = b.vin
  join top_configs c on b.chrome_style_id = c.chrome_style_id
    and b.engine = c.displacement
    and b.color = c.color
  where a.thru_date > current_date  
  group by c.model, c.trim_level, c.displacement, c.color) cc on aa.model = cc.model and aa.trim_level = cc.trim_level and aa.color = cc.color
left join ( -- sales  
  select c.model, c.trim_level, c.displacement, c.color, count(*) as sales
  from nc.vehicle_sales a
  join nc.vehicles b on a.vin = b.vin
  join top_configs c on b.chrome_style_id = c.chrome_style_id
    and b.engine = c.displacement
    and b.color = c.color
--     and b.trim_level = c.trim_level
  where a.delivery_date between current_date - 65 and current_date - 1
    and a.sale_type in ('retail','fleet')
  group by c.model, c.trim_level, c.displacement, c.color) dd on aa.model = dd.model and aa.trim_level = dd.trim_level and aa.color = dd.color
order by aa.model, aa.trim_level, aa.displacement, aa.color 

------------------------------------------------------------------------------------
--/> top_configs_combined_consensus_worksheet_data 01/24/20
------------------------------------------------------------------------------------

-- lets start out looking at silverado 1500 double cab
-- is this where i want to consider days on ground?
-- we will see
drop table if exists silverado_1500_configs;
create temp table silverado_1500_configs as
select a.chrome_Style_id, a.model_year, a.alloc_group, a.drive, a.model_code, a.box, a.trim_level, a.peg, 
  b.option_code as engine_option_code, b.displacement, b.diesel,
  c.color_code, c.color
-- select *
from jon.configurations a
join jon.engines b on a.chrome_style_id = b.chrome_style_id
join jon.exterior_colors c on a.chrome_Style_id = c.chrome_style_id
where a.model like 'silverado 1%'
  and a.cab = 'double'
  and a.model_year = 2020
order by a.drive, a.chrome_style_id; 


-- in system/transit
select a.chrome_style_id, a.box, a.peg, a.trim_level, a.color, a.displacement, a.diesel,
  count(*) filter (where b.category = 'in system') as in_system,
  count(*) filter (where b.category = 'in transit') as in_transit
from silverado_1500_configs a
join nc.open_orders b on a.model_code = b.model_code
  and order_type = 'TRE'
  and a.peg = b.peg
  and a.model_year =  b.model_year
  and a.color_code = b.color_code
join gmgl.vehicle_order_options c on b.order_number = c.order_number
  and a.engine_option_code = c.option_code
  and c.thru_ts > now() 
group by a.chrome_style_id, a.box, a.peg, a.trim_level, a.color, a.displacement, a.diesel  

-- on ground
select c.model_code, c.trim_level, c.displacement, c.color, count(*) as in_stock
from nc.vehicle_acquisitions a
join nc.vehicles b on a.vin = b.vin
join silverado_1500_configs c on b.chrome_style_id = c.chrome_style_id
  and b.engine = c.displacement
  and b.color = c.color
where a.thru_date > current_date  
group by c.model_code, c.trim_level, c.displacement, c.color

-- 60 days sales
select c.model_code, c.trim_level, c.displacement, c.color, count(*) as sales
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
join silverado_1500_configs c on b.chrome_style_id = c.chrome_style_id
  and b.engine = c.displacement
  and b.color = c.color
--     and b.trim_level = c.trim_level
where a.delivery_date between current_date - 65 and current_date - 1
  and a.sale_type in ('retail','fleet')
group by c.model_code, c.trim_level, c.displacement, c.color


-- do i want to do top config candidates for silverado 1500 dbl ?
-- sure
-- from ...\taylor_project\sql\new_top_configs.sql
-- but include color in the base

--ok, so, looking at days on ground and sales for 1/1/19 thru now
-- model year includes 2018 - 2021
drop table if exists silverado_1500_dbl_configs cascade;
create temp table silverado_1500_dbl_configs as
select a.chrome_Style_id, a.model_year, a.alloc_group, a.drive, a.model_code, a.box, a.trim_level, a.peg, 
  b.option_code as engine_option_code, b.displacement, b.diesel,
  c.color_code, c.color
-- select *
from jon.configurations a
join jon.engines b on a.chrome_style_id = b.chrome_style_id
join jon.exterior_colors c on a.chrome_Style_id = c.chrome_style_id
where a.model like 'silverado 1%'
  and a.cab = 'double'
  and a.model_year > 2017;




drop table if exists inv_og cascade;
create temp table inv_og as
select a.year_month, a.the_date, d.chrome_style_id, d.model_year, d.make, d.model, d.model_code, 
  d. drive, d.cab, d.trim_level, d.engine, c.color, e.peg, e.alloc_group
from dds.dim_date a
join nc.vehicle_acquisitions b on b.ground_date <= a.the_date
  and b.thru_date > a.the_date
join nc.vehicles c on b.vin = c.vin  
join nc.vehicle_configurations d on c.configuration_id = d.configuration_id
join silverado_1500_dbl_configs e on d.chrome_style_id = e.chrome_style_id
  and c.color = e.color
where a.the_date between '01/01/2019' and current_Date;
create index on inv_og(chrome_style_id);
create index on inv_og(the_date);
create index on inv_og(drive);
create index on inv_og(cab);

 -- 2019 sales
drop table if exists sales cascade;
create temp table sales as
select b.year_month, a.delivery_date, d.chrome_style_id, d.model_year, d.make, d.model, d.model_code,
  d.drive, d.cab, d.trim_level, d.engine, c.color, e.peg, e.alloc_group
from (
  select vin, max(delivery_date) as delivery_date
  from nc.vehicle_sales a
  join dds.dim_date b on a.delivery_date = b.the_date
    and b.the_date between '01/01/2019' and current_Date
  where sale_type in ('retail','fleet')
  group by a.vin) a
join nc.vehicle_sales aa on a.vin = aa.vin
  and a.delivery_date = aa.delivery_date  
join dds.dim_date b on a.delivery_date = b.the_date  
join nc.vehicles c on a.vin = c.vin  
join nc.vehicle_configurations d on c.configuration_id = d.configuration_id
join silverado_1500_dbl_configs e on d.chrome_style_id = e.chrome_style_id
  and e.color = c.color;


drop table if exists configs cascade;
create temp table configs as
select alloc_group, drive, box, model_code, peg, trim_level, color_code, color, array_agg(distinct model_year) as model_year, 
  array_agg(distinct chrome_Style_id) as chrome_style_id, 
  array_agg(distinct displacement || case when diesel then '-D' else '' end) as engine
from silverado_1500_dbl_configs
group by alloc_group, drive, box, model_code, peg, trim_level, color_code, color;


--02/17/20 this is close
select aa.*, bb.model_year, bb.model_code, bb.chrome_Style_id, bb.trim_level, bb.engine
from (
  select b.alloc_group, b.drive, b.box, b.peg, a.color,
    count(*) as total_sales, 
    c.days_og, c.min_sales, count(*) - c.min_sales as diff 
  from sales a
  join configs b on a.chrome_style_id = any(b.chrome_style_id)
    and a.color = b.color
  join (
    select b.alloc_group, b.drive, b.box, b.peg, count(distinct the_date) days_og, count(distinct the_date)/15 as min_sales
    from inv_og a
    join configs b on a.chrome_style_id = any(b.chrome_style_id)
      and a.color = b.color
    group by b.alloc_group, b.drive, b.box, b.peg) c on b.alloc_group = c.alloc_group
--       and b.cab = c.cab
      and b.drive = c.drive
      and b.box = c.box
      and b.peg = c.peg
  group by b.alloc_group, b.drive, b.box, b.peg, c.days_og, c.min_sales, a.color) aa
join configs bb on aa.alloc_group = bb.alloc_group
--       and aa.cab = bb.cab
      and aa.drive = bb.drive
      and aa.box = bb.box
      and aa.peg = bb.peg
      and aa.color = bb.color
order by  total_sales - min_sales desc
-- order by total_sales - min_sales   desc;
