﻿-- https://www.cureffi.org/2013/03/19/automatically-creating-pivot-table-column-names-in-postgresql/
-- tablename: name of source table you want to pivot
-- rowc: the name of the column in source table you want to be the rows
-- colc: the name of the column in source table you want to be the columns
-- cellc: an aggregate expression determining how the cell values will be created
-- celldatatype: desired data type for the cells
create or replace function jon.pivotcode (tablename CITEXT, rowc CITEXT, colc CITEXT, cellc CITEXT, celldatatype CITEXT) returns CITEXT language plpgsql as $$
declare
    dynsql1 CITEXT;
    dynsql2 CITEXT;
    columnlist CITEXT;
begin
    -- 1. retrieve list of column names.
    dynsql1 = 'select string_agg(distinct ''_''||'||colc||'||'' '||celldatatype||''','','' order by ''_''||'||colc||'||'' '||celldatatype||''') from '||tablename||';';
    execute dynsql1 into columnlist;
    -- 2. set up the crosstab query
    dynsql2 = 'select * from crosstab (
 ''select '||rowc||','||colc||','||cellc||' from '||tablename||' group by 1,2 order by 1,2'',
 ''select distinct '||colc||' from '||tablename||' order by 1''
 )
 as newtable (
 '||rowc||' CITEXT,'||columnlist||'
 );';
    return dynsql2;
end
$$;

-- toy example to show how it works
drop table if exists jon.table_to_pivot;
create table jon.table_to_pivot (
   rowname CITEXT,
   colname CITEXT,
   cellval numeric
);
insert into jon.table_to_pivot values ('row1','col1',11);
insert into jon.table_to_pivot values ('row1','col2',12);
insert into jon.table_to_pivot values ('row1','col3',13);
insert into jon.table_to_pivot values ('row2','col1',21);
insert into jon.table_to_pivot values ('row2','col2',22);
insert into jon.table_to_pivot values ('row2','col3',23);
insert into jon.table_to_pivot values ('row3','col1',31);
insert into jon.table_to_pivot values ('row3','col2',32);
insert into jon.table_to_pivot values ('row3','col3',33);
select jon.pivotcode('jon.table_to_pivot','rowname','colname','max(cellval)','integer');

select * from jon.table_to_pivot

select * from crosstab (
 'select rowname,colname,max(cellval) from jon.table_to_pivot group by 1,2 order by 1,2',
 'select distinct colname from jon.table_to_pivot order by 1'
 )
 as newtable (
 rowname CITEXT,_col1 integer,_col2 integer,_col3 integer
 );

 select *
 from jon.headers 
 where model_year = 2019
 order by seq

select string_agg('_' || chrome_style_id || 'c citext', ',' order by seq) from jon.headers where model_year = 2019






-- looks like what i want but i can't get it to work (yet)
-- https://postgresql.verite.pro/blog/2018/06/19/crosstab-pivot.html
CREATE FUNCTION jon.dynamic_pivot(central_query text, headers_query text)
 RETURNS refcursor AS
$$
DECLARE
  left_column text;
  header_column text;
  value_column text;
  h_value text;
  headers_clause text;
  query text;
  j json;
  r record;
  curs refcursor;
  i int:=1;
BEGIN
  -- find the column names of the source query
  EXECUTE 'select row_to_json(_r.*) from (' ||  central_query || ') AS _r' into j;
  FOR r in SELECT * FROM json_each_text(j)
  LOOP
    IF (i=1) THEN left_column := r.key;
      ELSEIF (i=2) THEN header_column := r.key;
      ELSEIF (i=3) THEN value_column := r.key;
    END IF;
    i := i+1;
  END LOOP;

  --  build the dynamic transposition query (based on the canonical model)
  FOR h_value in EXECUTE headers_query
  LOOP
    headers_clause := concat(headers_clause,
     format(chr(10)||',min(case when %I=%L then %I::text end) as %I',
           header_column,
	   h_value,
	   value_column,
	   h_value ));
  END LOOP;

  query := format('SELECT %I %s FROM (select *,row_number() over() as rn from (%s) AS _c) as _d GROUP BY %I order by min(rn)',
           left_column,
	   headers_clause,
	   central_query,
	   left_column);

  -- open the cursor so the caller can FETCH right away
  OPEN curs FOR execute query;
  RETURN curs;
END 
$$ LANGUAGE plpgsql; 







SELECT Array_to_string(Array_agg(number_of 
                                 || ' ' 
                                 || status), ', ') summary_status, 
       Max(user_id)                                USER 
FROM   (SELECT Count(*) number_of, 
               status, 
               user_id 
        FROM   jobs 
        GROUP  BY status, 
                  user_id 
        ORDER  BY user_id) AS foo 
GROUP  BY user_id;


SELECT Array_to_string(Array_agg(number_of 
                                 || ' ' 
                                 || status), ',  ') summary_status, 
       Max(user_id) USER 
FROM   (SELECT Count(*) number_of, 
               status, 
               user_id 
        FROM   jobs 
        GROUP  BY status, 
                  user_id 
        ORDER  BY user_id) AS foo 
GROUP  BY user_id; 