﻿drop table if exists nc.btg_vs_ordered cascade;
create table nc.btg_vs_ordered as
select current_date as the_date, a.consensus_timing, a.alloc_group, a.model_year, a.btg_allocation, a.total_allocation, a.total_avail,
  b.report_date, b.weekly_total_placed, b.desired_quantity, b.desired_quantity_balance, b.total_available
  ,a.btg_allocation - b.weekly_total_placed::integer as btg_minus_placed
-- select *
from (
select *
from nc.final_allocation_by_week
where seq = (
  select max(seq)
  from nc.final_allocation_by_week)) a
full outer join (
select *
from nc.weekly_order_placement  
where report_date = (
  select max(report_date)
  from nc.weekly_order_placement)) b on a.alloc_group = b.allocation_group
  and a.model_year = b.model_year::integer;

alter table nc.btg_vs_ordered
add primary key (the_date, alloc_group);

-- create unique index on nc.btg_vs_ordered(the_date, alloc_group);

comment on table nc.btg_vs_ordered is 'subset of fields from tables nc.final_allocation_by_week and nc.weekly_order_placement  
  to track the daily value of balance to go';

create or replace function nc.update_btg_vs_ordered()
returns void as
$BODY$
/*
select nc.update_btg_vs_ordered();
*/
insert into nc.btg_vs_ordered
select current_date as the_date, a.consensus_timing, a.alloc_group, a.model_year, a.btg_allocation, a.total_allocation, a.total_avail,
  b.report_date, b.weekly_total_placed, b.desired_quantity, b.desired_quantity_balance, b.total_available
  ,a.btg_allocation - b.weekly_total_placed::integer as btg_minus_placed
from (
select *
from nc.final_allocation_by_week
where seq = (
  select max(seq)
  from nc.final_allocation_by_week)) a
full outer join (
select *
from nc.weekly_order_placement  
where report_date = (
  select max(report_date)
  from nc.weekly_order_placement)) b on a.alloc_group = b.allocation_group
  and a.model_year = b.model_year::integer;
  
$BODY$ 
language sql;