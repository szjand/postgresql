﻿/*
/*
drop table if exists fc_models_model_codes cascade;
create temp table fc_models_model_codes as
select category, make, fc_model, model_codes
from (  
  select a.category, a.make, a.model as fc_model, array_agg(distinct c.model_code) as model_codes
  from nc.forecast_models a
  left join nc.forecast_model_configurations b on a.make = b.make
    and a.model = b.model
  left join nc.vehicle_configurations c on b.configuration_id = c.configuration_id  
--   left join nc.vehicle_configurations c on b.configuration_id = c.configuration_id  
  group by a.category, a.make, a.model) e;
create unique index on fc_models_model_codes(make,fc_model);



select d.year_month, a.sale_type, c.make
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
--   and b.make not in ('honda','nissan')
join fc_models_model_codes c on b.model_code = any(c.model_codes)
join dds.dim_date d on a.delivery_date = d.the_date
where extract(year from a.delivery_date) = 2019


select c.make, year_month, sale_type, count(*)
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
--   and b.make not in ('honda','nissan')
join fc_models_model_codes c on b.model_code = any(c.model_codes)
join dds.dim_date d on a.delivery_date = d.the_date
where extract(year from a.delivery_date) = 2019
group by c.make, year_month, sale_type
order by year_month, make, sale_type

-- from gl, exclude ry2 & used
drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select store, page, line, line_label, control, sum(unit_count) as unit_count
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201901--------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201901 -------------------------------------------------------------------
      and (
        (b.page between 5 and 15 and b.line between 1 and 45)) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
--         or
--         (b.page = 16 and b.line between 1 and 14)) -- used cars
-- --         or
-- --         (b.page = 17 and b.line between 1 and 20))-- f/i
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
      and f.store = 'RY1') d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by store, page, line, line_label, control
order by store, page, line;

select * 
from step_1 a
left join nc.vehicle_sales b on a.control = b.stock_number
  and extract(month from b.delivery_date) = 1
order by a.control

select * 
from step_1 a
join nc.vehicle_sales b on a.control = b.stock_number
  and extract(month from b.delivery_date) = 1
order by a.control

select * from step_1 where page = 5 and line = 31 order by control



-- count by line is accurate
select store, page, line, line_label, sum(unit_count) as unit_count
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201901--------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201901 -------------------------------------------------------------------
      and (
        (b.page between 5 and 15 and b.line between 1 and 45)) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
--         or
--         (b.page = 16 and b.line between 1 and 14)) -- used cars
-- --         or
-- --         (b.page = 17 and b.line between 1 and 20))-- f/i
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
      and f.store = 'RY1') d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by store, page, line, line_label--, control
order by store, page, line;


select * from fin.dim_fs_org

drop table if exists step_2;
create temp table step_2 as
select store, page, line, line_label, control
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201901--------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201901 -------------------------------------------------------------------
      and (
        (b.page between 5 and 15 and b.line between 1 and 45)) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
--         or
--         (b.page = 16 and b.line between 1 and 14)) -- used cars
-- --         or
-- --         (b.page = 17 and b.line between 1 and 20))-- f/i
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
      and f.store = 'RY1') d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by store, page, line, line_label, control
  having sum(unit_count) = 1
order by store, page, line;


what i want from gl is the stocknumbers for sales for each month
-- count on chev silverado (5/31) is going to be 1 higher than statement 
-- because this is actual sales and does not include the backon
-- otherwise perfect
select page, 
  count(*) filter (where line < 20) as retail_car,
  count(*) filter (where line = 22) as fleet_car,
  count(*) filter (where line = 23) as internal_car,
  count(*) filter (where line between 25 and 40) as retail_truck,
  count(*) filter (where line = 43) as fleet_truck,
  count(*) filter (where line = 44) as internal_truck
from step_2
group by page
order by page

select *
from (
  select * 
  from nc.vehicle_sales
  where sale_type = 'dealer trade'
    and extract(month from delivery_date) = 1
    and extract(year from delivery_Date) = 2019) a
left join step_2 b on a.stock_number = b.control    


-- ok, down to 15 void dealer trades that were subsequently retailed
select a.*, b.bopmast_Stock_number, b.date_capped, b.record_key, c.*
from nc.vehicle_sales a
join arkona.xfm_bopmast b on a.vin = b.bopmast_vin
  and b.current_row
  and b.record_status = 'U'
left join nc.vehicle_sales c on b.record_key = c.bopmast_id  
where a.sale_type = 'dealer trade'
  and a.stock_number not like 'H%'

select * from nc.vehicle_Sales where stock_number = 'g35925'

select * from arkona.xfm_bopmast where bopmast_stock_number = 'g35925'

-- yep, shows up as a voided sales transaction for the dt
-- not all as it may seem 33744 stocknumber changed
-- G35167 retail unwind then dealer traded

select a.control, a.post_Status, b.account, c.journal_code, d.description
from fin.fact_gl a
join fin.dim_Account b on a.account_key = b.account_key
  and b.account = '123706'
join fin.dim_journal c on a.journal_key = c.journal_key
join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
where a.control = 'g35925'


need to come up with some clean up and maintenance for void dealer trades


select *
from nc.vehicle_acquisitions a
join (
  select vin
  from nc.vehicle_acquisitions
  group by vin
  having count(*) > 1) b on a.vin = b.vin
left join nc.vehicle_sales c on a.vin = c.vin and a.ground_date = c.ground_date  
order by a.vin, a.ground_date      


--- yeah yeah all well and good, midgets and giants come back to the anomalies
-- for now, using gl, get basic counts for the past year


drop table if exists step_2;
create temp table step_2 as
select year_month, store, page, line, line_label, control, sum(unit_count) as unit_count
from (
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201809 and 201908
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select b.year_month, f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month between 201809 and 201908
      and (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
      and f.store = 'RY1') d on c.account = d.gl_account and b.year_month = d.year_month
  where a.post_status = 'Y') h
group by year_month, store, page, line, line_label, control
order by year_month, store, page, line;

-- this looks (comparing to fin statments) close enough
select year_month, page, 
  count(*) filter (where line < 20) as retail_car,
  count(*) filter (where line = 22) as fleet_car,
  count(*) filter (where line = 23) as internal_car,
  count(*) filter (where line between 25 and 40) as retail_truck,
  count(*) filter (where line = 43) as fleet_truck,
  count(*) filter (where line = 44) as internal_truck
from step_2
group by year_month, page
order by year_month, page


select a.year_month, d.category, d.make, d.fc_model,
  case
    when line in (22,43) then 'fleet'
    when line in (23,44) then 'internal'
    else 'retail' 
  end as sale_type
from step_2 a
join (
  select stock_number, vin
  from nc.vehicle_acquisitions
  union
  select stock_number, vin
  from nc.vehicle_sales) b on a.control = b.stock_number
join nc.vehicles c on b.vin = c.vin  
join fc_models_model_codes d on c.model_code = any(d.model_codes)
where a.unit_count > 0
  
drop table if exists sales;
create temp table sales as
select year_month, category, make, fc_model, 
  count(*) filter (where sale_type = 'retail') as retail,
  count(*) filter (where sale_type = 'fleet') as fleet,
  count(*) filter (where sale_type = 'internal') as internal
from (
  select a.year_month, d.category, d.make, d.fc_model,
    case
      when line in (22,43) then 'fleet'
      when line in (23,44) then 'internal'
      else 'retail' 
    end as sale_type
  from step_2 a
  join (
    select stock_number, vin
    from nc.vehicle_acquisitions
    union
    select stock_number, vin
    from nc.vehicle_sales) b on a.control = b.stock_number
  join nc.vehicles c on b.vin = c.vin  
  join fc_models_model_codes d on c.model_code = any(d.model_codes)
  where a.unit_count > 0) aa
group by year_month, category, make, fc_model;

  
select * from sales


select a.category, a.make, a.model, a.year_month, a.forecast, b.retail, b.fleet, b.internal
from nc.forecast a
left join sales b on a.category = b.category
  and a.make = b.make
  and a.model = b.fc_model
  and a.year_month = b.year_month
where a.current_row
  and a.year_month < 201909
order by a.category, a.make, a.model, a.year_month


select *
from step_2 a
left join nc.vehicle_sales b on a.control = b.stock_number



---------------------------------------------------------------------------
10/7/19
step_3
trying to get all sales for all model_year 2018 and above

the problem is that we lose the stocknumber in inpmast
once that happens, no  way to correlate accounting control number to a vin
bopmast for deals, but dealer trades, no
but wait
dealer trades, should be no subsequent transactions causing loss of stocknumber in inpmast
hmmm


select a.bopmast_stock_number, a.bopmast_vin, b.year, a.date_capped
from arkona.xfm_bopmast a
join arkona.xfm_inpmast b on a.bopmast_vin = b.inpmast_vin
  and b.current_row
  and b.year = 2018
where a.current_row  
  and a.record_status = 'U'
  and a.vehicle_type = 'N'
order by a.date_capped 
limit 100

select jsonb_pretty(response) from chr.describe_vehicle where vin = '1GKKNXLS6HZ157303'

-- start with all accounting starting in 2017
-- this does not include dealer trades
drop table if exists step_3;
create temp table step_3 as
select year_month, store, page, line, line_label, control, sum(unit_count) as unit_count
from (
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_year >= 2017
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select b.year_month, f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month between 201701 and 201910
      and (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
      and f.store = 'RY1') d on c.account = d.gl_account and b.year_month = d.year_month
  where a.post_status = 'Y') h
group by year_month, store, page, line, line_label, control
order by year_month, store, page, line;

select * from step_3

-- step_3a
-- step_3 + unit_count > 1, control exists in bopmast, model_year > 2017
drop table if exists step_3a;
create temp table step_3a as
select control as stock_number, year_month, bopmast_vin as vin, max(sale_type) as sale_type
from (
  select a.year_month, a.control, 
    case
      when a.line < 20 then 'retail' --'retail_car'
      when a.line = 22 then 'fleet' --'fleet_car'
      when a.line = 23 then 'internal' --'internal_car'
      when a.line between 25 and 40 then 'retail' -- 'retail_truck'
      when a.line = 43 then 'fleet' -- 'fleet_truck'
      when a.line = 44 then 'internal' --'internal_truck'
    end as sale_type,
    c.bopmast_vin
  from step_3 a
  join arkona.xfm_bopmast c on a.control = c.bopmast_stock_number 
    and c.record_status = 'U'
    and c.current_row
  join arkona.xfm_inpmast d on c.bopmast_vin = d.inpmast_vin
    and d.current_row  
    and d.year > 2017
  where unit_count > 0) x
group by stock_number, year_month, vin;

select * from step_3a

select sale_type, count(*) from step_3a group by sale_type



why does step_3 not have dealer trades?
because it is limited to sale accounts
dealer trades are relieved from inventory, not sold

-- from all_nc_inventory_nightly.sql, sales are
-- step_4 dealer trades
drop table if exists dealer_trades;
create temp table dealer_trades as
select a.control as stock_number, e.description, a.amount, b.the_date, f.inpmast_vin as vin
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
join fin.dim_account c on a.account_key = c.account_key
  and c.account in ( -- all new vehicle inventory account
    select b.account
    from fin.dim_account b 
    where b.account_type = 'asset'
      and b.department_code = 'nc'
      and b.typical_balance = 'debit'
      and b.current_row
      and b.account <> '126104')    
join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code = 'VSN'
join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
-- can we get the vin
join arkona.xfm_inpmast f on a.control = f.inpmast_stock_number
  and f.current_row
  and f.year > 2017
where a.post_status = 'Y'
  and left(c.account, 1) <> '2'
  and abs(a.amount) > 10000
  and b.the_year::integer >= 2017
  and not exists (
    select 1
    from step_3
    where control = a.control);
--   and f.inpmast_vin is null -- only 9 missing vin

drop table if exists step_5  cascade;
create temp table step_5 as
select stock_number, b.year_month, vin, 'dealer_trade' as sale_type from dealer_trades a join dds.dim_date b on a.the_date = b.the_date
union
select stock_number, year_month, vin, sale_type from step_3a;
create index on step_5(vin);

select a.*, coalesce(b.total_msrp, c.list_price) as msrp
-- select a.*
from step_5 a
left join gmgl.vehicle_invoices b on a.vin = b.vin
  and b.thru_date > current_date
left join arkona.xfm_inpmast c on a.stock_number = c.inpmast_stock_number
  and c.current_row

  
select *
from step_5 a
join nc.vehicles b on a.vin = b.vin
  and b.model like 'silverado 1%'
join chr.configurations c on b.chrome_style_id = c.chrome_style_id  


select b.model_year, b.model_code, c.peg
from step_5 a
join nc.vehicles b on a.vin = b.vin
  and b.model like 'silverado 1%'
join chr.configurations c on b.chrome_style_id = c.chrome_style_id  
group by b.model_year, b.model_code, c.peg

-- is peg 1:1 with trim ?
-- yes within the same year make model_code
select model_year, make, model_code, peg
from (
  select model_year, make, model_code, peg, trim_level
  from chr.configurations
  group by model_year, make, model_code, peg, trim_level) a
group by model_year, make, model_code, peg
having count(*) > 1

-- engines
-- the diesel designation, possibly description required
-- currently only saving displacement in nc.vehicles

select * from chr.engines

select option_code, description, displacement, diesel, string_agg(chrome_style_id, ',' order by chrome_style_id)
from chr.engines
group by option_code, description, displacement, diesel

-- engines for the the same chrome_style_id with the same displacement that have both diesel and non-diesel
select *
from chr.engines aa
join (
  select chrome_style_id
  from (
    select option_code, description, displacement, diesel, chrome_style_id
    from chr.engines a
    where exists (
      select 1 
      from chr.engines 
      where chrome_Style_id = a.chrome_style_id 
        and displacement = a.displacement 
        and diesel)
    group by option_code, description, displacement, diesel, chrome_style_id) a
  group by chrome_style_id 
  having count(*) > 1) bb on aa.chrome_style_id = bb.chrome_style_id
order by aa.chrome_style_id


-- level_headers for silverado 1500
select model_year, level_header
from nc.vehicle_configurations
where model_year > 2017
  and model like 'silverado 1%'
order by model_year, level_header  

-- 10/21/19 per taylor, headers for her page consists of year_model, model_code & peg
select * 
from chr.configurations
where model like 'silverado 1%'

-- 90
-- so these are the column headers
select model_year, model_code, peg, chrome_style_id
from chr.configurations
where model like 'silverado 1%'
  and model_year > 2017
group by model_year, model_code, peg, chrome_style_id


-- 
-- -- excluded fleet only csi    
-- select *
-- from step_5 a
-- join nc.vehicles b on a.vin = b.vin
-- left join chr.configurations c on b.chrome_style_id = c.chrome_style_id
-- where c.chrome_style_id is null


select a.vin, b.chrome_style_id, b.model_code, c.peg, d.msrp, a.sale_type
-- select a.*
from step_5 a
join nc.vehicles b on a.vin = b.vin
join chr.configurations c on b.chrome_style_id = c.chrome_style_id -- excludes 2 fleet only vehicles
join (
  select a.vin,  max(coalesce(b.total_msrp, c.list_price)) as msrp
  from step_5 a
  left join gmgl.vehicle_invoices b on a.vin = b.vin
    and b.thru_date > current_date
  left join arkona.xfm_inpmast c on a.vin = c.inpmast_vin
    and c.current_row 
  group by a.vin  ) d on a.vin = d.vin

select c.model_year, b.chrome_style_id, b.model_code, c.peg, avg(d.msrp)::integer as avg_msrp, 
  count(*) filter (where sale_type = 'retail') as retail_sales,
  count(*) filter (where sale_type = 'fleet') as fleet_sales,
  count(*) filter (where sale_type = 'internal') as internal_sales
-- select a.*
from step_5 a
join nc.vehicles b on a.vin = b.vin
join chr.configurations c on b.chrome_style_id = c.chrome_style_id -- excludes 2 fleet only vehicles
  and c.model like 'silverado 1%'
join (
  select a.vin,  max(coalesce(b.total_msrp, c.list_price)) as msrp
  from step_5 a
  left join gmgl.vehicle_invoices b on a.vin = b.vin
    and b.thru_date > current_date
  left join arkona.xfm_inpmast c on a.vin = c.inpmast_vin
    and c.current_row 
  group by a.vin  ) d on a.vin = d.vin
group by c.model_year, b.chrome_style_id, b.model_code, c.peg



-------------------------------------------------
--< sales
-- anomalies to address (skipped over for now):
--   -- no nc.vehicle_sales record, initial cut of sales_2 excludes these 53 rows
-- --     select a.*
-- --     from sales_1 a
-- --     join arkona.xfm_bopmast c on a.control = c.bopmast_stock_number 
-- --       and c.record_status = 'U'
-- --       and c.current_row
-- --     join arkona.xfm_inpmast d on c.bopmast_vin = d.inpmast_vin
-- --       and d.current_row  
-- --       and d.year > 2017
-- --     -- inner join excludes anomaly: no nc.vehicle_sales record  
-- --     left join nc.vehicle_sales e on c.record_key = e.bopmast_id
-- --     where unit_count > 0 
-- --       and e.vin is null
--   
-- 
--   -------------------------------------------------
-- -- start with all accounting starting in 2017
-- -- this is what i use to validate sales to financial statement
-- -- this does not include dealer trades
-- -- sales_1: control, year_month, sale_type, unit_count
-- drop table if exists sales_1;
-- create temp table sales_1 as
-- select year_month, store, page, line, line_label, control, sum(unit_count) as unit_count
-- from (
--   select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
--     a.control, a.amount,
--     case when a.amount < 0 then 1 else -1 end as unit_count
--   from fin.fact_gl a
--   inner join dds.dim_date b on a.date_key = b.date_key
--     and b.the_year >= 2017
--   inner join fin.dim_account c on a.account_key = c.account_key
-- -- add journal
--   inner join fin.dim_journal aa on a.journal_key = aa.journal_key
--     and aa.journal_code in ('VSN','VSU')
--   inner join ( -- d: fs gm_account page/line/acct description
--     select b.year_month, f.store, d.gl_account, b.page, b.line, b.line_label, e.description
--     from fin.fact_fs a
--     inner join fin.dim_fs b on a.fs_key = b.fs_key
--       and b.year_month between 201701 and 201910
--       and (b.page between 5 and 15 and b.line between 1 and 45) 
--     inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
--     inner join fin.dim_account e on d.gl_account = e.account
--       and e.account_type_code = '4'
--     inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
--       and f.store = 'RY1') d on c.account = d.gl_account and b.year_month = d.year_month
--   where a.post_status = 'Y') h
-- group by year_month, store, page, line, line_label, control
-- order by year_month, store, page, line;
-- 
-- select * from sales_1
-- 10/31/2019 refactoring sales_1 to use fact_gl instead of fact_fs to get the current month deals
-- lose the ability to generate sale type
-- all i really want are the stocknumbers for the sales 


-- -- sales_2
-- -- all non dealer trade sales
-- -- sales_1 + unit_count > 0, control exists in bopmast(no dealer trades), model_year > 2017
-- drop table if exists sales_2;
-- create temp table sales_2 as
-- select a.year_month, a.control, 
--   case
--     when a.line < 20 then 'retail' --'retail_car'
--     when a.line = 22 then 'fleet' --'fleet_car'
--     when a.line = 23 then 'internal' --'internal_car'
--     when a.line between 25 and 40 then 'retail' -- 'retail_truck'
--     when a.line = 43 then 'fleet' -- 'fleet_truck'
--     when a.line = 44 then 'internal' --'internal_truck'
--   end as sale_type_1,
--   c.bopmast_vin, c.date_capped, e.*,
-- --   row_number() over (partition by c.bopmast_vin order by c.date_capped) -- not a bad idea, but R units fuck it up
--   row_number() over (partition by a.control order by c.date_capped)
-- from sales_1 a
-- join arkona.xfm_bopmast c on a.control = c.bopmast_stock_number 
--   and c.record_status = 'U'
--   and c.current_row
-- join arkona.xfm_inpmast d on c.bopmast_vin = d.inpmast_vin
--   and d.current_row  
--   and d.year > 2017
-- -- inner join excludes anomaly: no nc.vehicle_sales record  
-- join nc.vehicle_sales e on c.record_key = e.bopmast_id
-- where unit_count > 0 ;
-- 
-- select * from sales_2


-- -- shit, forgot dealer trades
-- -- dealer trades are relieved from inventory, no deal
-- -- 11/04 per Taylor, not of interest at this time
-- insert into sales_44
-- select g.model_year, g.model_code, h.peg, g.chrome_Style_id, a.control as stock_number, 
--   g.vin, i.ground_date, i.delivery_date, i.sale_type, g.model, g.drive, g.cab,
--   g.trim_level, g.engine, g.color
--   -- select a.*
-- from (
--   select a.control
--   from fin.fact_gl a
--   join dds.dim_date b on a.date_key = b.date_key
--   join fin.dim_account c on a.account_key = c.account_key
--     and c.account in ( -- all new vehicle inventory account
--       select b.account
--       from fin.dim_account b 
--       where b.account_type = 'asset'
--         and b.department_code = 'nc'
--         and b.typical_balance = 'debit'
--         and b.current_row
--         and b.account <> '126104')    
--   join fin.dim_journal d on a.journal_key = d.journal_key
--     and d.journal_code = 'VSN'
--   where a.post_status = 'Y'
--     and left(c.account, 1) <> '2'
--     and abs(a.amount) > 10000
--     and b.the_year::integer >= 2017
--     and not exists (
--       select 1
--       from sales_44
--       where stock_number = a.control) 
--   group by a.control) a    
-- join arkona.xfm_inpmast f on a.control = f.inpmast_stock_number
--   and f.current_row
--   and f.year > 2017
--   and f.model like 'silverado 1%'
-- join nc.vehicles g on f.inpmast_vin = g.vin  
-- join chr.configurations h on g.chrome_style_id = h.chrome_style_id
-- join nc.vehicle_sales i on a.control = i.stock_number 
-- 


-- -- total sales by header -- broken out by sale type
-- select a.model_year, a.model_code, a.peg, 
--   coalesce(b.retail_sales, 0) as retail_sales,
--   coalesce(b.fleet_sales, 0) as fleet_sales,
--   coalesce(b.ctp_sales, 0) as ctp_sales,
--   coalesce(b.dt_sales, 0) as dt_sales,
--   coalesce(b.total_sales, 0) as total_sales
-- from headers a
-- left join (
--   select model_year, model_code, peg, 
--     count(*) filter (where sale_type = 'retail') as retail_sales,
--     count(*) filter (where sale_type = 'fleet') as fleet_sales,
--     count(*) filter (where sale_type = 'ctp') as ctp_sales,
--     count(*) filter (where sale_type = 'dealer trade') as dt_sales,
--     count(*) as total_sales
--   from sales_44
--   group by model_year, model_code, peg) b on a.model_year = b.model_year and a.model_code = b.model_code and a.peg = b.peg
-- order by a.model_year, coalesce(b.retail_sales, 0) desc, coalesce(b.fleet_sales, 0) desc, coalesce(b.ctp_sales, 0) desc, coalesce(b.dt_sales, 0) desc
-- -- last 90 day sales by header
-- select a.model_year, a.model_code, a.peg, 
--   coalesce(b.retail_sales, 0) as retail_sales,
--   coalesce(b.fleet_sales, 0) as fleet_sales,
--   coalesce(b.ctp_sales, 0) as ctp_sales,
--   coalesce(b.dt_sales, 0) as dt_sales
-- from headers a
-- left join (
--   select model_year, model_code, peg, 
--     count(*) filter (where sale_type = 'retail') as retail_sales,
--     count(*) filter (where sale_type = 'fleet') as fleet_sales,
--     count(*) filter (where sale_type = 'ctp') as ctp_sales,
--     count(*) filter (where sale_type = 'dealer trade') as dt_sales
--   from sales_44
--   where delivery_date > current_date - 90
--   group by model_year, model_code, peg) b on a.model_year = b.model_year and a.model_code = b.model_code and a.peg = b.peg
-- order by a.model_year, coalesce(b.retail_sales, 0) desc, coalesce(b.fleet_sales, 0) desc, coalesce(b.ctp_sales, 0) desc, coalesce(b.dt_sales, 0) desc

*/
----------------------------------------------------------------------------------------------------------
10/24/19
----------------------------------------------------------------------------------------------------------
create schema ta;
comment on schema ta is 'temporary location for tables used to generate taylors consensus page';
-------------------------------------------------
--< headers
-------------------------------------------------
-- initially looking only at silverado 1500

drop table if exists ta.headers;
create table ta.headers (
  chrome_style_id citext not null primary key,
  model_year integer not null,
  model_code citext not null,
  peg citext not null,
  description citext not null,
  seq integer);
create unique index on ta.headers(model_year, model_code, peg);
comment on table ta.headers is 'all possible configurations from chr.configurations for 2018 and above Silverado 1xxx';  
insert into ta.headers
select chrome_style_id, model_year, model_code, peg, 
  model || ' ' || drive || ' ' ||cab || ' Cab' ||' ' || box || ' Box' || ' ' || trim_level as description, 0 as seq
-- select *
from chr.configurations
where model like 'silverado 1%'
  and model_year > 2017
group by model_year, model_code, peg, chrome_style_id, model || ' ' || drive || ' ' ||cab || ' ' || box || ' ' || trim_level;


-- -- commonality between years
-- select model_code, peg, array_agg(model_year order by model_year) 
-- from headers
-- group by model_code, peg;
-- 
--  select * from headers

-------------------------------------------------
--/> headers
-------------------------------------------------



-------------------------------------------------
--< sales
-------------------------------------------------

drop table if exists ta.sales_22 cascade;
create table ta.sales_22 (
  stock_number citext not null,
  vin citext not null,
  bopmast_id integer not null,
  ground_date date not null,
  delivery_date date not null,
  sale_type citext not null,
  row_number integer not null);
comment on table ta.sales_22 is 'staging table for fleet and retail sales for model_year 2018 and above';

insert into ta.sales_22
select a.control, d.vin, d.bopmast_id, d.ground_date, d.delivery_date, d.sale_type, 
  row_number() over (partition by d.stock_number order by d.delivery_date)
from ( -- stock number from accounting sales
  select a.control
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.year_month >= 201701
  join fin.dim_account c on a.account_key = c.account_key
    and account_type = 'Sale'
    and department_code = 'NC'
    and typical_balance = 'Credit'
  join fin.dim_journal d on a.journal_key = d.journal_key
    and d.journal_code = 'VSN'
  where a.post_status = 'Y' 
  group by a.control
    having  sum(a.amount) > 10000 or sum(a.amount) < -10000) a
join arkona.xfm_bopmast b on a.control = b.bopmast_stock_number
  and b.record_status = 'U'
  and b.current_row
join arkona.xfm_inpmast c on b.bopmast_vin = c.inpmast_vin
  and c.current_row
  and c.year > 2017
join nc.vehicle_sales d on b.record_key = d.bopmast_id;

drop table if exists ta.sales_33 cascade;
create table ta.sales_33 (
  stock_number citext not null primary key,
  vin citext not null,
  bopmast_id integer not null,
  ground_date date not null,
  delivery_date date not null,
  sale_type citext not null,
  row_number integer not null);
comment on table ta.sales_33 is 'staging table for fleet and retail sales for model_year 2018 and above, most recent sale only';
insert into ta.sales_33
select a.*
from ta.sales_22 a
join (
  select stock_number, max(row_number) as row_number
  from ta.sales_22
  group by stock_number) b on a.stock_number = b.stock_number and a.row_number = b.row_number;

drop table if exists ta.sales_detail cascade;
create table ta.sales_detail (
  model_year integer not null,
  model_code citext not null,
  peg citext not null,
  chrome_style_id citext not null,
  stock_number citext primary key,
  vin citext not null,
  ground_date date not null,
  delivery_date date not null,
  sale_type citext not null,
  model citext not null,
  drive citext not null,
  cab citext not null,
  trim_level citext not null,
  color_code citext not null,
  engine_code citext not null,
  transmission_code citext not null,
  interior_code citext not null);
comment on table ta.sales_detail is 'detail data for all retail/fleet sales of model year 2018 and above silverado 1XXX';  

-- use invoice to generate color, engine, transmission, interior option codes
insert into ta.sales_detail 
select c.model_year, c.model_code, c.peg, c.chrome_style_id, 
  a.stock_number, a.vin, a.ground_date, a.delivery_date, a.sale_type,
  b.model, b.drive, b.cab, b.trim_level, 
  substring(d.raw_invoice from position(E'\n' in  d.raw_invoice) + 1 for 3) as color_code,
  substring(d.raw_invoice from position('ENGINE,' in d.raw_invoice) - 4 for 3) as eng_code, 
  substring(d.raw_invoice from position('TRANSMISSION,' in d.raw_invoice) - 4 for 3) as tran_code,
  substring(d.raw_invoice from position('RENAISSANCE' in d.raw_invoice) - 45 for 3) as int_code
from ta.sales_33 a
join nc.vehicles b on a.vin = b.vin
join ta.headers c on b.chrome_style_id = c.chrome_style_id
join gmgl.vehicle_invoices d on a.vin = d.vin
  and d.thru_date > current_date 
where a.sale_type in ('retail','fleet')
  and substring(d.raw_invoice from position('ENGINE,' in d.raw_invoice) - 4 for 3) <> '';
create index on ta.sales_detail(color_code);
create index on ta.sales_detail(engine_code);
create index on ta.sales_detail(interior_code);
create index on ta.sales_detail(transmission_code);
create index on ta.sales_detail(chrome_style_id);



  
-- -- 11/04/19
-- -- had a conversation with Taylor, for sales, all she is interested in is retail and fleet, no need (for now) to 
-- -- include ctp or dealer trades, no need (for now) to break out retail vs fleet





-- -- https://lerner.co.il/2014/05/23/turning-postgresql-rows-arrays-array/
-- -- make an array of the headers
-- select array(
--   select model_code || ' ' || peg
--   from (
--     select *
--     from sales_detail
--     where model_year = 2019
--     order by 1) a)
-- 
-- -- and convert that array to a string (which can be exported as a csv ...)
-- select array_to_string(
-- array(
--   select model_code || ' ' || peg
--   from (
--     select *
--     from sales
--     where model_year = 2019
--     order by seq) a), ',')    






-- for each row in sales_detail, we have one row in 
--   nc.vehicles
--   chr.describe_vehicle
--   nc.vehicle_acquisitions (at least one row)
--   nc.vehicle_sales (at least one row)
--   nc.vehicle_invoices (missing one dt acquisition/sale: 3GCUKSEC4JG154380)
--   nc.vehicle_orders: missing 351 vins
--   arkona.xfm_inpmast (where current_row)
-------------------------------------------------
--/> sales
-------------------------------------------------

-------------------------------------------------
--< set seq in headers include all sales, inventory & orders
-------------------------------------------------

-- this has to be run AFTER all the sales/inventory/order data has been generated
-- sales is 1st priority then inventory then in_sys_tran
update ta.headers x
set seq = y.seq
from (
  select aa.model_year, aa.model_code, aa.peg, aa.chrome_style_id, 
    coalesce(bb.sales,0) as sales, coalesce(cc.on_ground,0) as on_ground, coalesce(dd.in_sys_tran,0) as in_sys_tran, 
    coalesce(bb.sales,0) + coalesce(cc.on_ground,0)  + coalesce(dd.in_sys_tran,0) as total,
    row_number() over (order by aa.model_year, coalesce(bb.sales,0) desc,  coalesce(cc.on_ground,0) desc, coalesce(dd.in_sys_tran,0) desc) as seq
-- select *   
  from ta.headers aa
  left join (
    select model_year, model_code, peg, chrome_Style_id, 'sales' as source, count(*) as sales
    from ta.sales_detail
    group by model_year, model_code, peg, chrome_Style_id) bb on aa.model_year = bb.model_year and aa.chrome_style_id = bb.chrome_style_id
  left join (
    select c.model_year, c.model_code, c.peg, c.chrome_style_id, 'on_ground', count(*) as on_ground
    from nc.vehicle_acquisitions a
    join nc.vehicles b on a.vin = b.vin
    join ta.headers c on b.chrome_style_id = c.chrome_Style_id
    where a.thru_date > current_date
    group by c.chrome_style_id, c.model_year, c.model_code, c.peg  ) cc on aa.model_year = cc.model_year and aa.chrome_style_id = cc.chrome_style_id
  left join (
    select b.model_year, b.model_code, b.peg, b.chrome_style_id, 'in_sys_tran', count(*) as in_sys_tran
    from nc.open_orders a
    join ta.headers b on a.model_year = b.model_year
      and a.model_code = b.model_code
      and a.peg = b.peg
    group by b.model_year, b.model_code, b.peg, b.chrome_style_id) dd on aa.model_year = dd.model_year and aa.chrome_style_id = dd.chrome_style_id  
  order by aa.model_year, coalesce(bb.sales,0) desc,  coalesce(cc.on_ground,0) desc, coalesce(dd.in_sys_tran,0) desc) y
where x.model_year = y.model_year
  and x.model_code = y.model_code
  and x.peg = y.peg;      
-------------------------------------------------
--/> set seq in headers include all sales, inventory & orders
-------------------------------------------------
-------------------------------------------------
--< $$$
-------------------------------------------------

drop table if exists ta.gross_detail cascade;
create table ta.gross_detail (
  stock_number citext not null,
  chrome_style_id citext not null,
  gross_type citext not null,
  amount integer not null,
  primary key (stock_number, gross_type));
comment on table ta.gross_detail is 'gross for all vehicles in ta.sales_detail';
insert into ta.gross_detail  
select a.stock_number, a.chrome_style_id, d.gross_type, sum(-b.amount)::integer as amount
from ta.sales_detail a
join fin.fact_gl b on a.stock_number = b.control
  and b.post_status = 'Y'
join fin.dim_account c on b.account_key = c.account_key
join ( -- all gross accounts: types sales & f/i
  select distinct 'sales' as gross_type, d.gl_account, e.account_type, e.department
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month between 201701 and 201909
    and b.page between 5 and 15 -- new cars
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  inner join fin.dim_account e on d.gl_account = e.account
  union  -- f/i accounts
  select distinct 'f/i' as gross_type, d.gl_account, e.account_type, e.department
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month between 201701 and 201909
    and b.page = 17
    and b.line between 1 and 20
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  inner join fin.dim_account e on d.gl_account = e.account) d on c.account = d.gl_account  
group by a.stock_number, a.chrome_style_id, d.gross_type;


-------------------------------------------------
--/> $$$
-------------------------------------------------



-------------------------------------------------
--< forecast
-------------------------------------------------
/*
select * 
from nc.forecast_models

select *
from nc.forecast_model_configurations a
join nc.vehicle_configurations b on a.configuration_id = b.configuration_id
join headers c on b.chrome_style_id = c.chrome_style_id


select * 
from nc.forecast a
where current_row 
  and make = 'chevrolet'
  and model like '1500%'
order by year_month desc 

this does not make any sense, we forecast 1500s based on cab and nothing else, how to allocate the 3 numbers ???

*/
-------------------------------------------------
--/> forecast
-------------------------------------------------

-------------------------------------------------
--< current inventory
-------------------------------------------------
-- -- inventory: in accounting (from all_nc_inventory_nightly)
-- drop table if exists inv_acct cascade;
-- create temp table inv_acct as
-- select a.control as stock_number, f.inpmast_vin as vin, min(b.the_date) as acct_date, sum(a.amount)::integer as vehicle_cost
-- from fin.fact_gl a
-- inner join dds.dim_date b on a.date_key = b.date_key
-- inner join fin.dim_account c on a.account_key = c.account_key
--   and c.account in ( -- all new vehicle inventory account
--     select b.account
--     from fin.dim_account b 
--     where b.account_type = 'asset'
--       and b.department_code = 'nc'
--       and b.typical_balance = 'debit'
--       and b.current_row
--       and b.account <> '126104')   
-- inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
-- left join arkona.xfm_inpmast f on a.control = f.inpmast_stock_number
--   and f.current_row
-- where a.post_status = 'Y'
--   and a.control not like 'H%'
-- group by a.control, f.inpmast_vin
-- having sum(a.amount) > 10000;
-- create unique index on inv_acct(stock_number);
-- create unique index on inv_acct(vin);
-- 
-- 
-- -- inpmast
-- drop table if exists inv_inpmast cascade;
-- create temp table inv_inpmast as
-- select inpmast_stock_number as stock_number, inpmast_vin as vin, make, 
--   arkona.db2_integer_to_date_long(date_in_invent) as date_in_invent, 
--   inpmast_vehicle_cost::integer as vehicle_cost
-- from arkona.xfm_inpmast a
-- where current_row
--   and status = 'I'
--   and type_n_u = 'N'
--   and inpmast_Stock_number not like 'H%';
-- create unique index on inv_inpmast(stock_number);
-- create unique index on inv_inpmast(vin);



-- -- on the ground
-- drop table if exists on_ground cascade;
-- create temp table on_ground as
-- select c.chrome_style_id, count(*) as on_ground
-- from nc.vehicle_acquisitions a
-- join nc.vehicles b on a.vin = b.vin
-- join headers c on b.chrome_style_id = c.chrome_Style_id
-- where a.thru_date > current_date
-- group by c.chrome_style_id;
-- 
-- -- in system / in transit
-- drop table if exists in_sys_tran cascade;
-- create temp table in_sys_tran as
-- select b.chrome_style_id, count(*) as in_sys_tran
-- from nc.open_orders a
-- join headers b on a.model_year = b.model_year
--   and a.model_code = b.model_code
--   and a.peg = b.peg
-- group by b.chrome_style_id; 

-------------------------------------------------
--/> current inventory
-------------------------------------------------


-------------------------------------------------
--< timeline
-------------------------------------------------
-- avg days order to invoice
-- avg days on ground to sold


-- 
-- select a.*, b.source, c.order_number
-- from sales_44 a
-- join nc.vehicle_acquisitions b on a.vin = b.vin
--   and a.ground_date = b.ground_date
--   and b.source = 'factory'
-- left join gmgl.vehicle_orders c on a.vin = c.vin
-- where a.model_year > 2018
-- 
-- 
-- select model_year, count(*)
-- from sales_44
-- group by model_year
-- 
-- -- only 2739 of 3609 orders have a 3xxx event code
-- select a.order_number, a.vin, coalesce(min(b.from_date), '12/31/9999'::date) as order_date
-- from gmgl.vehicle_orders a
-- join gmgl.vehicle_order_events b on a.order_number = b.order_number
--   and b.event_code like '3%'
-- group by  a.order_number, a.vin
-- 
-- -- only 2243 of 2589 orders of model year > 2018 have a 3xxx event code
-- select a.order_number, a.vin, coalesce(min(b.from_date), '12/31/9999'::date) as order_date
-- from gmgl.vehicle_orders a
-- join gmgl.vehicle_order_events b on a.order_number = b.order_number
--   and b.event_code like '3%'
-- where a.model_year > 2018  
-- group by  a.order_number, a.vin
-- 
-- 
-- it was not until 201804 that we had good order data  
-- select bb.year_month, count(*)
-- from (
--   select a.order_number, a.vin, coalesce(min(b.from_date), '12/31/9999'::date) as order_date
--   from gmgl.vehicle_orders a
--   left join gmgl.vehicle_order_events b on a.order_number = b.order_number
--   group by  a.order_number, a.vin) aa
-- join dds.dim_date bb on aa.order_date = bb.the_date
-- group by bb.year_month
-- order by bb.year_month

-- select *
-- from nc.vehicle_invoices
-- limit 10

-- days order to invoice
drop table if exists order_to_invoice;
create temp table order_to_invoice as
select chrome_style_id, avg(invoice_date - date_ordered)::integer as avg_days_order_to_invoice
from (
  select aa.model_year, aa.chrome_style_id, cc.invoice_date, bb.date_ordered
  from ta.sales_detail aa
  join (
    select a.order_number, a.order_type, a.vin, a.model_year, a.alloc_group, a.make, a.model_code, a.peg,
      min(b.from_date) filter ( where left(b.event_code,1) = '3') as date_ordered
    from gmgl.vehicle_orders a
    join gmgl.vehicle_order_events b on a.order_number = b.order_number
      and left(b.event_code, 1) = '3'
    group by a.order_number, a.order_type, a.vin, a.model_year, a.alloc_group, a.make, a.model_code, a.peg) bb on aa.vin = bb.vin
  join nc.vehicle_invoices cc on aa.vin = cc.vin  
  union -- current inventory
  select aa.year, aa.chrome_style_id::citext, cc.invoice_date, bb.date_ordered
  from arkona.xfm_inpmast aa
  join (
    select a.order_number, a.order_type, a.vin, a.model_year, a.alloc_group, a.make, a.model_code, a.peg,
      min(b.from_date) filter ( where left(b.event_code,1) = '3') as date_ordered
    from gmgl.vehicle_orders a
    join gmgl.vehicle_order_events b on a.order_number = b.order_number
      and left(b.event_code, 1) = '3'
    group by a.order_number, a.order_type, a.vin, a.model_year, a.alloc_group, a.make, a.model_code, a.peg) bb on aa.inpmast_vin = bb.vin
  join nc.vehicle_invoices cc on aa.inpmast_vin = cc.vin  
  join ta.headers dd on aa.chrome_style_id::citext = dd.chrome_style_id
  where aa.current_row
    and aa.status = 'I'
    and aa.type_n_u = 'N'
    and aa.inpmast_Stock_number not like 'H%') ee
group by chrome_style_id;  

-- days ground to sold
drop table if exists ground_to_sold;
create temp table ground_to_sold as
select chrome_style_id, avg(delivery_date - ground_date)::integer as avg_days_ground_to_sold
from ta.sales_detail
group by chrome_style_id;

-------------------------------------------------
--/> timeline
-------------------------------------------------

-------------------------------------------------
--< colors
-------------------------------------------------

-- -- 11/14/19
-- -- sales detail now includes option codes for ext_color, engine, trans, interior
-- -- 910 in sales_detail
-- -- 9 2018's with no color
-- select a.*, coalesce(b.color, 'Other')
-- from ta.sales_detail a
-- left join chr.exterior_colors b on a.chrome_style_id = b.chrome_Style_id
--   and a.color_code = b.color_code
-- where b.color_code is null  
-- -- good on all the others, nothing missing
-- select *
-- from ta.sales_detail a
-- join chr.engines b on a.chrome_style_id = b.chrome_style_id
--   and a.engine_code = b.option_code
-- join chr.interiors c on a.chrome_Style_id = c.chrome_style_id
--   and a.interior_code = c.option_code  
-- join chr.transmissions d on a.chrome_style_id = d.chrome_style_id
--   and a.transmission_code = d.option_code


drop table if exists ta.current_detail cascade;
create table ta.current_detail (
  chrome_style_id citext not null,
  order_number citext,
  vin citext, 
  color_code citext not null,
  engine_code citext not null,
  transmission_code citext not null,
  interior_code citext not null);
insert into ta.current_detail (chrome_style_id, order_number, color_code,
  engine_code, transmission_code, interior_code)
select aa.chrome_style_id, aa.order_number, 
  b.color_code, d.option_code as engine_code, f.option_code as interior_code, h.option_code as transmission_code
from (
  select a.order_number, b.chrome_style_id 
  from nc.open_orders a
  join ta.headers b on a.model_year = b.model_year
    and a.model_code = b.model_code
    and a.peg = b.peg
  left join arkona.xfm_inpmast c on a.vin = c.inpmast_vin
    and c.current_row
  where c.inpmast_vin is null) aa
join chr.exterior_colors b on aa.chrome_style_id = b.chrome_style_id
join gmgl.vehicle_order_options c on aa.order_number = c.order_number 
  and b.color_code = c.option_code
  and c.thru_ts > now()
join chr.engines d on aa.chrome_style_id = d.chrome_style_id
join gmgl.vehicle_order_options e on aa.order_number = e.order_number 
  and d.option_code = e.option_code
  and e.thru_ts > now()
join chr.interiors f on aa.chrome_style_id = f.chrome_style_id
join gmgl.vehicle_order_options g on aa.order_number = g.order_number 
  and f.option_code = g.option_code
  and g.thru_ts > now()
join chr.transmissions h on aa.chrome_style_id = h.chrome_style_id
join gmgl.vehicle_order_options i on aa.order_number = i.order_number 
  and h.option_code = i.option_code
  and i.thru_ts > now();

insert into ta.current_detail (chrome_style_id, vin, color_code,
  engine_code, transmission_code, interior_code)  
select b.chrome_style_id, a.inpmast_vin, 
  substring(c.raw_invoice from position(E'\n' in  c.raw_invoice) + 1 for 3) as color_code,
  case
    when substring(c.raw_invoice from position('ENGINE,' in c.raw_invoice) - 4 for 3) = ''
    then substring(c.raw_invoice from position('ENGINE:' in c.raw_invoice) - 4 for 3)
    else substring(c.raw_invoice from position('ENGINE,' in c.raw_invoice) - 4 for 3)
  end as engine_code,
  case
    when substring(c.raw_invoice from position('TRANSMISSION,' in c.raw_invoice) - 4 for 3) = ''
    then substring(c.raw_invoice from position('TRANSMISSION:' in c.raw_invoice) - 4 for 3)
    else substring(c.raw_invoice from position('TRANSMISSION,' in c.raw_invoice) - 4 for 3)
  end as transmission_code,
  substring(c.raw_invoice from position('RENAISSANCE' in c.raw_invoice) - 45 for 3) as int_code
from arkona.xfm_inpmast a
join ta.headers b on a.chrome_style_id::citext = b.chrome_style_id
join gmgl.vehicle_invoices c on a.inpmast_vin = c.vin
  and c.thru_date > current_date
where a.current_row
  and a.status = 'I'
  and a.type_n_u = 'N';

create unique index on ta.current_detail(coalesce(vin, order_number));    
create index on ta.current_detail(chrome_style_id);    
create index on ta.current_detail(engine_code);  
create index on ta.current_detail(color_code);  
create index on ta.current_detail(transmission_code);  
create index on ta.current_detail(interior_code);  

-- !!!!!!!!!!!!!!!!!!
-- the assumption i keep making is that there is no chrome_style_id that crosses model_years
-- which for the 812th time is correct
-- select chrome_style_id
-- from (
-- select chrome_Style_id, model_year
-- from chr.configurations
-- group by chrome_Style_id, model_year
-- ) a
-- group by chrome_style_id having count(*) > 1


-- this is  the end result for colors
-- this is the table to be fed into jon.colpivot
drop table if exists ta.colors;
create table ta.colors (
  model_year integer not null,
  chrome_style_id citext not null,
  color citext not null,
  perc_sold_current citext not null,
  seq integer not null,
  primary key (chrome_style_id,color));
insert into ta.colors  
select aa.model_year, aa.chrome_style_id, aa.color_code || '-' || aa.color as color, 
  coalesce(bb.colors_perc_sold::text || '%', '') ||'' || '  /  ' || coalesce(cc.colors_perc_current::text || '%','') || '' as perc_sold_current, aa.seq
from ( -- all colors
  select a.model_year, a.chrome_style_id, a.seq, b.color_code, b.color
  from ta.headers a
  join chr.exterior_colors b on a.chrome_style_id = b.chrome_style_id) aa
--   where a.model_year = 2019) aa
left join (-- colors_perc_sold
  select a.chrome_style_id, b.color_code, (100.0 * b.color_sales/a.total_sales)::integer as colors_perc_sold 
  from (
    select chrome_style_id, count(*) as total_sales 
    from ta.sales_detail
--     where model_year = 2019
    group by chrome_style_id) a
  join (
    select model_year, chrome_style_id, color_code, count(*) as color_sales
    from ta.sales_detail
--     where model_year = 2019
    group by model_year, chrome_style_id, color_code) b on a.chrome_style_id = b.chrome_style_id) bb on aa.chrome_style_id = bb.chrome_style_id
    and aa.color_code = bb.color_code
left join ( -- colors_perc_current
  select a.chrome_style_id, b.color_code, (100.0 * b.color_current/a.total_current)::integer as colors_perc_current
  from (
    select chrome_style_id, count(*) as total_current
    from ta.current_detail
    group by chrome_style_id) a
  join (
    select chrome_style_id, color_code, count(*) as color_current
    from ta.current_detail
    group by chrome_style_id, color_code) b on a.chrome_style_id = b.chrome_style_id) cc on aa.chrome_style_id = cc.chrome_style_id
    and aa.color_code = cc.color_code;
 

drop table if exists _test_ext_colors;
select jon.colpivot('_test_ext_colors', 'select * from ta.colors where model_year = 2019 and color not like ''01%''',
    array['color'], array['chrome_style_id'], '#.perc_sold_current', 'max(seq)');
select * from _test_ext_colors a;  


/*

drop table if exists colors_sold;
create temp table colors_sold as
select aa.chrome_style_id, bb.color_code, bb.color::citext, (100.0 * bb.sales/aa.total_sales)::integer as perc_sales
from (
  select chrome_Style_id, count(*) as total_sales
  from ta.sales_detail
  group by chrome_Style_id) aa
left join (
  select a.chrome_style_id, b.color_code, initcap(b.color) as color, count(*)  as sales
  from ta.sales_detail a
  join chr.exterior_colors b on a.chrome_style_id = b.chrome_style_id
    and a.color_code = b.color_code
  group by a.chrome_style_id, b.color, b.color_code) bb on aa.chrome_style_id = bb.chrome_style_id  order by aa.chrome_style_id, bb.color_code;
create unique index on colors_sold(chrome_style_id, color_code); 

drop table if exists orders_detail;
create temp table orders_detail as
-- orders (non invoiced)
select b.model_year, b.model_code, b.peg, b.chrome_style_id, a.order_number, a.color_code, coalesce(a.color, c.color) as color
from nc.open_orders a
join ta.headers b on a.model_year = b.model_year
  and a.model_code = b.model_code
  and a.peg = b.peg
join chr.exterior_colors c on b.chrome_style_id = c.chrome_Style_id
  and a.color_code = c.color_code  
where not exists (
  select 1
  from arkona.xfm_inpmast
  where inpmast_vin = a.vin);
create unique index on orders_detail(order_number);  

drop table if exists current_detail;
create temp table current_detail as
select model_year, model_code, chrome_style_id, color_code, color 
from orders_detail
union all
-- invoiced inventory
select b.model_year, b.model_code, b.chrome_Style_id, a.color_code, initcap(c.color) as color
from arkona.xfm_inpmast a
join ta.headers b on a.chrome_style_id::citext = b.chrome_style_id
join chr.exterior_colors c on b.chrome_style_id = c.chrome_style_id
  and a.color_code = c.color_code 
where a.current_row
  and a.status = 'I'
  and a.type_n_u = 'N';




drop table if exists colors_current;
create temp table colors_current as 
select a.model_year, a.chrome_style_id, b.color_code, b.color, (100.0 * b.current_color/current_total)::integer as perc_current
from (  
  select model_year, chrome_style_id, count(*) current_total
  from current_detail
  group by model_year, chrome_style_id) a
left join (
  select model_year, chrome_style_id, color_code, color, count(*) as current_color
  from current_detail
  group by model_year, chrome_style_id, color_code, color) b on a.model_year = b.model_year 
    and a.chrome_style_id = b.chrome_style_id;
create unique index on colors_current(model_year, chrome_style_id, color_code); 


drop table if exists ext_colors;
create temp table ext_colors as
select aa.chrome_style_id, aa.color_code || '-' || aa.color as color, 
--   coalesce(bb.perc_sales, 0)::text ||'%' || '  /  ' || coalesce(cc.perc_current, 0)::text || '%'as perc_sold_current, aa.seq
  coalesce(bb.perc_sales::text || '%', '') ||'' || '  /  ' || coalesce(cc.perc_current::text || '%','') || '' as perc_sold_current, aa.seq
from (
  select *
  from (
    select chrome_style_id, seq
    from ta.headers a
    where a.model_year = 2019) a,
  (
    select color_code, color 
    from chr.exterior_colors a
    where model_year = 2019
      and exists (
        select 1
        from ta.headers 
        where chrome_style_id = a.chrome_style_id)
      and color_code not like '01%'
    group by color_code, color) b) aa
left join colors_sold bb on aa.chrome_style_id = bb.chrome_style_id
  and aa.color_code = bb.color_code 
left join colors_current cc on aa.chrome_style_id = cc.chrome_style_id
  and aa.color_code = cc.color_code; 
create unique index on ext_colors(chrome_style_id, color);


-- https://www.cureffi.org/2013/03/19/automatically-creating-pivot-table-column-names-in-postgresql/
-- create or replace function jon.pivotcode (tablename CITEXT, rowc CITEXT, colc CITEXT, cellc CITEXT, celldatatype CITEXT) returns CITEXT language plpgsql as $$
do $$

currently this block just generates a sql statement, which i am subsequently manually running

declare
    col_table citext := 'headers';
    tablename citext := 'ext_colors';
    rowc citext := 'color';
    colc citext := 'chrome_style_id';
    cellc citext := 'perc_sold_current';
    celldatatype citext := 'citext';
    dynsql1 CITEXT;
    dynsql2 CITEXT;
    columnlist CITEXT;
begin
    -- 1. retrieve list of column names.
    dynsql1 = 'select string_agg(''_'' || chrome_style_id || '' citext'', '','' order by seq) from jon.headers where model_year = 2019;';
--     dynsql1 = 'select string_agg(distinct ''_''||'||colc||'||'' '||celldatatype||''','','' order by ''_''||'||colc||'||'' '||celldatatype||''') from '||tablename||';';
    execute dynsql1 into columnlist;
    -- 2. set up the crosstab query

    dynsql2 = 'select * from crosstab (
 ''select '||rowc||','||colc||',max('||cellc||') from '||tablename||' group by 1,2 order by 1,2'',
 ''select chrome_style_id from headers where model_year = 2019 order by seq''
 )
 as newtable (
 '||rowc||' CITEXT,'||columnlist||'
 );';

    drop table if exists ct;
    create temp table ct as select dynsql2;
end
$$;

select * from ct;


drop table if exists xtab;
create temp table xtab as 
select * from crosstab (
 'select color,chrome_style_id,max(perc_sold_current) from ext_colors group by 1,2 order by 1,2',
 'select chrome_style_id from ta.headers where model_year = 2019 order by seq'
 )
 as newtable (
 color CITEXT,_399780 citext,_399783 citext,_399773 citext,_398577 citext,_399782 citext,_399781 citext,_399778 citext,_399784 citext,_399779 citext,_399774 citext,_399772 citext,_398576 citext,_399788 citext,_399770 citext,_399791 citext,_399771 citext,_399751 citext,_399792 citext,_399776 citext,_399769 citext,_399786 citext,_398571 citext,_399790 citext,_399753 citext,_399785 citext,_399765 citext,_399760 citext,_399787 citext,_399763 citext,_399758 citext,_399768 citext,_398573 citext,_398574 citext,_399766 citext,_399756 citext,_403472 citext,_399759 citext,_399775 citext,_403471 citext,_399767 citext,_399755 citext,_399757 citext,_399777 citext,_399762 citext,_399754 citext,_399789 citext,_399764 citext,_399761 citext,_399752 citext
 );

select * from crosstab (
 'select color,chrome_style_id,max(perc_sold_current) from ext_colors group by 1,2 order by 1,2',
 'select chrome_style_id from headers where model_year = 2019 order by seq'
 )
 as newtable (
 color CITEXT,_399780 citext,_399783 citext,_399773 citext,_398577 citext,_399782 citext,_399781 citext,_399778 citext,_399784 citext,_399779 citext,_399774 citext,_399772 citext,_398576 citext,_399788 citext,_399770 citext,_399791 citext,_399771 citext,_399751 citext,_399792 citext,_399776 citext,_399769 citext,_399786 citext,_398571 citext,_399790 citext,_399753 citext,_399785 citext,_399765 citext,_399760 citext,_399787 citext,_399763 citext,_399758 citext,_399768 citext,_398573 citext,_398574 citext,_399766 citext,_399756 citext,_403472 citext,_399759 citext,_399775 citext,_403471 citext,_399767 citext,_399755 citext,_399757 citext,_399777 citext,_399762 citext,_399754 citext,_399789 citext,_399764 citext,_399761 citext,_399752 citext
 );

select * from xtab
 
-- generates the column list for a model_year
-- unfortunately, for now, this is a manual process, past the results of this into a query
-- could probably go the dynamic sql route
select string_agg('_'|| chrome_style_id,',' order by seq) from headers where model_year = 2019 

-- fucking finally
-- array_agg aggregates columns, array[] turns a list of values into an array
-- except below, array_agg aggregates an array, but it will not turn a list into an array
select color, array_to_string(the_array, ',','0')
from (
  select color, array_agg(array[_399780,_399783,_399773,_398577,_399782,_399781,
    _399778,_399784,_399779,_399774,_399772,_398576,_399788,_399770,_399791,_399771,
    _399751,_399792,_399776,_399769,_399786,_398571,_399790,_399753,_399785,_399765,
    _399760,_399787,_399763,_399758,_399768,_398573,_398574,_399766,_399756,_403472,
    _399759,_399775,_403471,_399767,_399755,_399757,_399777,_399762,_399754,_399789,
    _399764,_399761,_399752]) as the_array
  from xtab
  group by color
  order by color) a
*/


  
-------------------------------------------------
--/> colors
-------------------------------------------------




-------------------------------------------------
--< interiors
-------------------------------------------------

-- this is  the end result for colors
-- this is the table to be fed into jon.colpivot
drop table if exists ta.interiors;
create table ta.interiors (
  model_year integer not null,
  chrome_style_id citext not null,
  interior citext not null,
  perc_sold_current citext not null,
  seq integer not null,
  primary key (chrome_style_id,interior));
insert into ta.interiors
select aa.model_year, aa.chrome_style_id, aa.option_code || '-' || initcap(aa.interior), 
  coalesce(bb.interiors_perc_sold::text || '%', '') ||'' || '  /  ' || coalesce(cc.interiors_perc_current::text || '%','') || '' as perc_sold_current, aa.seq
from ( -- all interiors
  select a.model_year, a.chrome_style_id, a.seq, b.option_code, b.material || '-' || b.color as interior
  from ta.headers a
  join chr.interiors b on a.chrome_style_id = b.chrome_style_id) aa
--   where a.model_year = 2019) aa
left join (-- interiors_perc_sold
  select a.chrome_style_id, b.interior_code, (100.0 * b.interior_sales/a.total_sales)::integer as interiors_perc_sold 
  from (
    select chrome_style_id, count(*) as total_sales 
    from ta.sales_detail
    group by chrome_style_id) a
  join (
    select model_year, chrome_style_id, interior_code, count(*) as interior_sales
    from ta.sales_detail
--     where model_year = 2019
    group by model_year, chrome_style_id, interior_code) b on a.chrome_style_id = b.chrome_style_id) bb on aa.chrome_style_id = bb.chrome_style_id
    and aa.option_code = bb.interior_code
left join ( -- interiors_perc_current
  select a.chrome_style_id, b.interior_code, (100.0 * b.interior_current/a.total_current)::integer as interiors_perc_current
  from (
    select chrome_style_id, count(*) as total_current 
    from ta.current_detail
    group by chrome_style_id) a
  join (
    select chrome_style_id, interior_code, count(*) as interior_current
    from ta.current_detail
    group by chrome_style_id, interior_code) b on a.chrome_style_id = b.chrome_style_id) cc on aa.chrome_style_id = cc.chrome_style_id
    and aa.option_code = cc.interior_code;


drop table if exists _test_ext_interiors;
select jon.colpivot('_test_ext_interiors', 'select * from ta.interiors where model_year = 2019',
    array['interior'], array['chrome_style_id'], '#.perc_sold_current', 'max(seq)');
select * from _test_ext_interiors a;  


-- 
-- -- here is the list of all interior options for each chrome style id
-- drop table if exists interior_master;
-- create temp table interior_master as
-- select a.model_year, a.chrome_style_id, a.model_code, a.seq, b.option_code, b.material, b.color,
--   b.option_code || '-' || b.material || '-' || b.color as interior
-- from ta.headers a
-- left join chr.interiors b on a.chrome_style_id = b.chrome_style_id;
-- -- where a.model_year = 2019;
-- create unique index on interior_master(chrome_style_id,interior);
-- 
-- -- sold
-- select a.model_year, a.chrome_style_id, right(trim(left(raw_invoice, 151)), 3) as interior_code, c.interior
-- from sales_detail a
-- join gmgl.vehicle_invoices b on a.vin = b.vin
--   and b.thru_date > current_date
-- left join interior_master c on a.chrome_style_id = c.chrome_style_id
--   and right(trim(left(raw_invoice, 151)), 3) = c.oem_code
-- where a.model_year = 2019
-- 
-- -- orders (non invoiced)
-- -- if the vin exists, take interior from the invoice, maybe
-- select a.vin, b.model_year, b.model_code, b.peg, b.chrome_style_id, a.order_number, a.color_code, c.oem_code, c.interior, b.seq
-- select b.chrome_style_id
-- from nc.open_orders a
-- join headers b on a.model_year = b.model_year
--   and a.model_code = b.model_code
--   and a.peg = b.peg
--   and b.model_year = 2019
-- join interior_master c on b.chrome_style_id = c.chrome_style_id
-- join gmgl.vehicle_order_options d on a.order_number = d.order_number
--   and c.oem_code = d.option_code
--   and d.thru_ts > now()
-- where not exists (
--   select 1
--   from arkona.xfm_inpmast
--   where inpmast_vin = a.vin)
-- 
-- -- invoiced inventory -- 54
-- select a.inpmast_vin, aa.model_year, aa.model_code, aa.chrome_Style_id, a.color_code, c.*
-- from arkona.xfm_inpmast a
-- join headers aa on a.chrome_style_id::citext = aa.chrome_style_id
-- join gmgl.vehicle_invoices b on a.inpmast_vin = b.vin
--   and b.thru_date > current_date
-- left join interior_master c on a.chrome_style_id::citext = c.chrome_style_id
--   and right(trim(left(raw_invoice, 151)), 3) = c.oem_code
-- where a.current_row
--   and a.status = 'I'
--   and a.type_n_u = 'N'
-- order by a.inpmast_vin  
-- 
-- 
-- 
-- drop table if exists ext_colors;
-- create temp table ext_colors as
-- select aa.chrome_style_id, aa.color_code || '-' || aa.color as color, 
-- --   coalesce(bb.perc_sales, 0)::text ||'%' || '  /  ' || coalesce(cc.perc_current, 0)::text || '%'as perc_sold_current, aa.seq
--   coalesce(bb.perc_sales::text, '') ||'%' || '  /  ' || coalesce(cc.perc_current::text,'') || '%'as perc_sold_current, aa.seq
-- from (
--   select *
--   from (
--     select chrome_style_id, seq
--     from headers a
--     where a.model_year = 2019) a,
--   (
--     select color_code, color 
--     from chr.exterior_colors a
--     where model_year = 2019
--       and exists (
--         select 1
--         from headers 
--         where chrome_style_id = a.chrome_style_id)
--       and color_code not like '01%'
--     group by color_code, color) b) aa
-- left join colors_sold bb on aa.chrome_style_id = bb.chrome_style_id
--   and aa.color_code = bb.color_code 
-- left join colors_current cc on aa.chrome_style_id = cc.chrome_style_id
--   and aa.color_code = cc.color_code; 
-- create unique index on ext_colors(chrome_style_id, color);
-- 
-- select * from colors_sold
-- 
-- 
-- drop table if exists interiors_sold;
-- create temp table interiors_sold as
-- select aa.model_year, aa.chrome_style_id, bb.interior_code, bb.interior::citext, (100.0 * bb.sales/aa.total_sales)::integer as int_perc_sales
-- from (
--   select model_year, chrome_Style_id, count(*) as total_sales
--   from sales_detail
--   where model_year = 2019
--   group by model_year, chrome_Style_id) aa
-- left join (
--   select a.model_year, a.chrome_style_id, right(trim(left(raw_invoice, 151)), 3) as interior_code, 
--     c.interior, count(*) as sales
--   from sales_detail a
--   join gmgl.vehicle_invoices b on a.vin = b.vin
--     and b.thru_date > current_date
--   left join interior_master c on a.chrome_style_id = c.chrome_style_id
--     and right(trim(left(raw_invoice, 151)), 3) = c.oem_code
--   where a.model_year = 2019
--   group by a.model_year, a.chrome_style_id, right(trim(left(raw_invoice, 151)), 3), c.interior) bb on aa.model_year = bb.model_year
--     and aa.chrome_style_id = bb.chrome_style_id;
-- create unique index on interiors_sold(chrome_Style_id, interior_code);    
-- 
--   select *
--   from (
--     select chrome_style_id, seq
--     from headers a
--     where a.model_year = 2019) a,
--   (
--   select distinct oem_code, color, material
--   from chr.interiors a
--   where exists (
--     select 1
--     from headers 
--     where model_year = 2019
--       and chrome_style_id = a.chrome_style_id)) b
-- left join interiors_sold bb on aa.chrome_style_id = bb.chrome_style_id
--   and aa.oem_code = bb.interior_code       
-------------------------------------------------
--/> interiors
-------------------------------------------------


-------------------------------------------------
--< engines
-------------------------------------------------

drop table if exists ta.engines;
create table ta.engines (
  model_year integer not null,
  chrome_style_id citext not null,
  engine citext not null,
  perc_sold_current citext not null,
  seq integer not null,
  primary key (chrome_style_id,engine));
insert into ta.engines
select aa.model_year, aa.chrome_style_id, aa.option_code || '-' || initcap(aa.engine) as engine, 
  coalesce(bb.engines_perc_sold::text || '%', '') ||'' || '  /  ' || coalesce(cc.engines_perc_current::text || '%','') || '' as perc_sold_current, aa.seq
from ( -- all engines
  select a.model_year, a.chrome_style_id, a.seq, b.option_code, b.description as engine
  from ta.headers a
  join chr.engines b on a.chrome_style_id = b.chrome_style_id) aa
--   where a.model_year = 2019) aa
left join (-- engines_perc_sold
  select a.chrome_style_id, b.engine_code, (100.0 * b.engine_sales/a.total_sales)::integer as engines_perc_sold 
  from (
    select chrome_style_id, count(*) as total_sales 
    from ta.sales_detail
    group by chrome_style_id) a
  join (
    select model_year, chrome_style_id, engine_code, count(*) as engine_sales
    from ta.sales_detail
--     where model_year = 2019
    group by model_year, chrome_style_id, engine_code) b on a.chrome_style_id = b.chrome_style_id) bb on aa.chrome_style_id = bb.chrome_style_id
    and aa.option_code = bb.engine_code
left join ( -- engines_perc_current
  select a.chrome_style_id, b.engine_code, (100.0 * b.engine_current/a.total_current)::integer as engines_perc_current
  from (
    select chrome_style_id, count(*) as total_current
    from ta.current_detail
    group by chrome_style_id) a
  join (
    select chrome_style_id, engine_code, count(*) as engine_current
    from ta.current_detail
    group by chrome_style_id, engine_code) b on a.chrome_style_id = b.chrome_style_id) cc on aa.chrome_style_id = cc.chrome_style_id
    and aa.option_code = cc.engine_code;


drop table if exists _test_ext_engines;
select jon.colpivot('_test_ext_engines', 'select * from ta.engines where model_year = 2019',
    array['engine'], array['chrome_style_id'], '#.perc_sold_current', 'max(seq)');
select * from _test_ext_engines a;  


-------------------------------------------------
--/> engines
-------------------------------------------------

-------------------------------------------------
--< transmissions
-------------------------------------------------

drop table if exists ta.transmissions;
create table ta.transmissions (
  model_year integer not null,
  chrome_style_id citext not null,
  transmission citext not null,
  perc_sold_current citext not null,
  seq integer not null,
  primary key (chrome_style_id,transmission));
insert into ta.transmissions
select aa.model_year, aa.chrome_style_id, aa.option_code || '-' || initcap(aa.transmission) as transmission, 
  coalesce(bb.transmissions_perc_sold::text || '%', '') ||'' || '  /  ' || coalesce(cc.transmissions_perc_current::text || '%','') || '' as perc_sold_current, aa.seq
from ( -- all transmission
  select a.model_year, a.chrome_style_id, a.seq, b.option_code, b.description as transmission
  from ta.headers a
  join chr.transmissions b on a.chrome_style_id = b.chrome_style_id) aa
--   where a.model_year = 2019) aa
left join (-- transmissions_perc_sold
  select a.chrome_style_id, b.transmission_code, (100.0 * b.transmission_sales/a.total_sales)::integer as transmissions_perc_sold 
  from (
    select chrome_style_id, count(*) as total_sales 
    from ta.sales_detail
    group by chrome_style_id) a
  join (
    select model_year, chrome_style_id, transmission_code, count(*) as transmission_sales
    from ta.sales_detail
--     where model_year = 2019
    group by model_year, chrome_style_id, transmission_code) b on a.chrome_style_id = b.chrome_style_id) bb on aa.chrome_style_id = bb.chrome_style_id
    and aa.option_code = bb.transmission_code
left join ( -- transmissions_perc_current
  select a.chrome_style_id, b.transmission_code, (100.0 * b.transmission_current/a.total_current)::integer as transmissions_perc_current
  from (
    select chrome_style_id, count(*) as total_current
    from ta.current_detail
    group by chrome_style_id) a
  join (
    select chrome_style_id, transmission_code, count(*) as transmission_current
    from ta.current_detail
    group by chrome_style_id, transmission_code) b on a.chrome_style_id = b.chrome_style_id) cc on aa.chrome_style_id = cc.chrome_style_id
    and aa.option_code = cc.transmission_code;


drop table if exists _test_ext_transmissions;
select jon.colpivot('_test_ext_transmissions', 'select * from ta.transmissions where model_year = 2019',
    array['transmission'], array['chrome_style_id'], '#.perc_sold_current', 'max(seq)');
select * from _test_ext_transmissions a;  


select * from ta.current_detail

-------------------------------------------------
--/> transmissions
-------------------------------------------------



------------------------------------------------------------------
--< packages
------------------------------------------------------------------

taylor has given me a spreadsheet with 2020 silverado packages for model_code/peg

-- drop table if exists test_1;
-- create temp table test_1 as
-- -- down to 10 sold units with no package, good enough
-- select aa.vin, cc.raw_invoice
-- from ta.sales_detail aa
-- join gmgl.vehicle_invoices cc on aa.vin = cc.vin
-- left join (
-- select vin from (
--   select a.vin, a.chrome_style_id, b.option_code 
--   from ta.sales_detail a
--   join chr.packages b on a.chrome_style_id = b.chrome_style_id
--   join gmgl.vehicle_option_codes c on a.vin = c.vin and b.option_code = c.option_code) x group by vin) bb on aa.vin = bb.vin
-- where bb.vin is null;  


  and option_code = 'z71'

-- dont do a master list, make the left column a result of all sale/current 


select chrome_style_id, option_code, count(*) package_current
from (
  select 'order' as source, a.chrome_style_id, b.option_code
  from ta.current_detail a
  join chr.packages b on a.chrome_style_id = b.chrome_style_id
  join gmgl.vehicle_order_options c on a.order_number = c.order_number
    and b.option_code = c.option_code
    and c.thru_ts > now()
  union all
  select 'vin' as source, a.chrome_style_id, b.option_code
  from ta.current_detail a
  join chr.packages b on a.chrome_style_id = b.chrome_style_id
  join gmgl.vehicle_option_codes c on a.vin = c.vin 
    and b.option_code = c.option_code) x
group by chrome_style_id, option_code    

select distinct option_code, description
from pkgs  
order by option_code

select * 
from pkgs

select a.chrome_style_id, b.option_code, count(*) as package_sales
from ta.sales_detail a
join chr.packages b on a.chrome_style_id = b.chrome_style_id
join gmgl.vehicle_option_codes c on a.vin = c.vin
  and b.option_code = c.option_code
group by a.chrome_style_id, b.option_code  






drop table if exists ta.packages;
create table ta.packages (
  model_year integer not null,
  chrome_style_id citext not null,
  packages citext not null,
  perc_sold_current citext not null,
  seq integer not null,
  primary key (chrome_style_id,packages));
insert into ta.packages
select aa.model_year, aa.chrome_style_id, aa.option_code || '-' || replace(aa.package, ',', ';') as package, 
  coalesce(bb.packages_perc_sold::text || '%', '') ||'' || '  /  ' || coalesce(cc.packages_perc_current::text || '%','') || '' as perc_sold_current, aa.seq
from ( -- all package
  select a.model_year, a.chrome_style_id, a.seq, b.option_code, b.description as package
  from ta.headers a
  join chr.packages b on a.chrome_style_id = b.chrome_style_id) aa
--   where a.model_year = 2019) aa
left join (-- packages_perc_sold
  select a.chrome_style_id, b.option_code, (100.0 * b.package_sales/a.total_sales)::integer as packages_perc_sold 
  from (
    select chrome_style_id, count(*) as total_sales 
    from ta.sales_detail
    group by chrome_style_id) a
  join (
    select a.chrome_style_id, b.option_code, count(*) as package_sales
    from ta.sales_detail a
    join chr.packages b on a.chrome_style_id = b.chrome_style_id
    join gmgl.vehicle_option_codes c on a.vin = c.vin
      and b.option_code = c.option_code
    group by a.chrome_style_id, b.option_code) b on a.chrome_style_id = b.chrome_style_id) bb on aa.chrome_style_id = bb.chrome_style_id
    and aa.option_code = bb.option_code
left join ( -- packages_perc_current
  select a.chrome_style_id, b.option_code, (100.0 * b.package_current/a.total_current)::integer as packages_perc_current
  from (
    select chrome_style_id, count(*) as total_current
    from ta.current_detail
    group by chrome_style_id) a
  join (
    select chrome_style_id, option_code, count(*) package_current
    from (
      select 'order' as source, a.chrome_style_id, b.option_code
      from ta.current_detail a
      join chr.packages b on a.chrome_style_id = b.chrome_style_id
      join gmgl.vehicle_order_options c on a.order_number = c.order_number
        and b.option_code = c.option_code
        and c.thru_ts > now()
      union all
      select 'vin' as source, a.chrome_style_id, b.option_code
      from ta.current_detail a
      join chr.packages b on a.chrome_style_id = b.chrome_style_id
      join gmgl.vehicle_option_codes c on a.vin = c.vin 
        and b.option_code = c.option_code) x
    group by chrome_style_id, option_code ) b on a.chrome_style_id = b.chrome_style_id) cc on aa.chrome_style_id = cc.chrome_style_id
    and aa.option_code = cc.option_code;




drop table if exists _test_ext_packages;
select jon.colpivot('_test_ext_packages', 'select * from ta.packages where model_year = 2019 and trim(perc_sold_current) <> ''/''',
    array['packages'], array['chrome_style_id'], '#.perc_sold_current', 'max(seq)');
select * from _test_ext_packages order by packages;  


------------------------------------------------------------------
--/> packages
------------------------------------------------------------------


-- initially, this looks promising to generate an excellish presentation
-- will need a comprehensive list of header from sales plus inventory plus orders
-- what is needed for each "row table" is chrome_Style_id and a value
-- added col_3 as a placeholder, most of these rows can/should have a total column first, before config breakouts
do $$
declare
  _model_year integer := 2019;
begin  
  drop table if exists wtf;
    create temp table wtf as
    select *
    from (
      select 1 as the_order, 'model code' as col_2, '' as col_3, array_to_string(
        array(
          select model_code 
          from ta.headers a
          where model_year = _model_year
          order by seq), ',')
      union
      select 2, 'peg',  '' as col_3, array_to_string(
        array(
          select peg 
          from ta.headers a
          where model_year = _model_year
          order by seq), ',')
      union
      select 2.5, 'chrome style id',  '' as col_3, array_to_string(
        array(
          select chrome_style_id 
          from ta.headers a
          where model_year = _model_year
          order by seq), ',')          
      union
      select 3, 'description',  '' as col_3, array_to_string(
        array(
          select description 
          from ta.headers a
          where model_year = _model_year
          order by seq), ',')
      union
      select 4, 'avg msrp',  '' as col_3, array_to_string(
        array(
          select coalesce(b.avg_msrp, 0)::text as avg_msrp
          from ta.headers a
          left join (
            select a.chrome_style_id, avg(coalesce(b.total_msrp, c.list_price))::integer as avg_msrp
            from ta.sales_detail a
            left join gmgl.vehicle_invoices b on a.vin = b.vin
              and b.thru_date > current_date
            left join arkona.xfm_inpmast c on a.stock_number = c.inpmast_stock_number
              and c.current_row  
            group by a.chrome_style_id) b on a.chrome_style_id = b.chrome_Style_id
          where a.model_year = _model_year
          order by a.seq), ',')      
      union
      select 5, 'total units sold',  '' as col_3, array_to_string(
        array(
          select coalesce(c.sales, 0)::text as sales
          from ta.headers a
          left join (
            select b.chrome_style_id, count(*) as sales
            from ta.sales_detail a
            join ta.headers b on a.chrome_style_id = b.chrome_style_id
            group by b.chrome_style_id
            having count(*) > 0) c on a.chrome_style_id = c.chrome_Style_id
          where a.model_year = _model_year
          order by a.seq), ',')
      union
      select 6, 'avg gross',  '' as col_3, array_to_string(
        array(
          select coalesce(c.avg_gross, 0)::text as sales
          from ta.headers a
          left join (
            select a.chrome_style_id, (sum(b.amount)/count(a.vin))::integer as avg_gross
            from ta.sales_detail a
            join ( -- total gross
              select stock_number, sum(amount) as amount
              from ta.gross_detail
              group by stock_number) b on a.stock_number = b.stock_number
            group by a.chrome_style_id) c on a.chrome_style_id = c.chrome_Style_id
          where a.model_year = _model_year
          order by a.seq), ',')  
      union
      select 7, 'avg front gross',  '' as col_3, array_to_string(
        array(
          select coalesce(bbb.avg_front_gross, 0)::text as sales
          from ta.headers aaa
          left join (
            select aa.chrome_style_id, (bb.amount/aa.sales)::integer as avg_front_gross
            from ( -- total sales
              select b.chrome_style_id, count(*) as sales
              from ta.sales_detail a
              join ta.headers b on a.chrome_style_id = b.chrome_style_id
              group by b.chrome_style_id
              having count(*) > 0) aa
            join ( -- total front gross
              select chrome_style_id, sum(amount) as amount
              from ta.gross_detail
              where gross_type = 'sales'
              group by chrome_style_id) bb on aa.chrome_style_id = bb.chrome_style_id) bbb on aaa.chrome_style_id = bbb.chrome_Style_id
          where aaa.model_year = _model_year
          order by aaa.seq), ',')
      union
      select 8, 'avg f/i gross',  '' as col_3, array_to_string(
        array(
          select coalesce(bbb.avg_fi_gross, 0)::text as sales
          from ta.headers aaa
          left join (
            select aa.chrome_style_id, (bb.amount/aa.sales)::integer as avg_fi_gross
            from ( -- total sales
              select b.chrome_style_id, count(*) as sales
              from ta.sales_detail a
              join ta.headers b on a.chrome_style_id = b.chrome_style_id
              group by b.chrome_style_id
              having count(*) > 0) aa
            join ( -- total fi gross
              select chrome_style_id, sum(amount) as amount
              from ta.gross_detail
              where gross_type = 'f/i'
              group by chrome_style_id) bb on aa.chrome_style_id = bb.chrome_style_id) bbb on aaa.chrome_style_id = bbb.chrome_Style_id
          where aaa.model_year = _model_year
          order by aaa.seq), ',')    
      union
      select 9, 'sold in last 90 days',  '' as col_3, array_to_string(
        array(
          select coalesce(c.sales, 0)::text as sales
          from ta.headers a
          left join (
            select b.chrome_style_id, count(*) as sales
            from ta.sales_detail a
            join ta.headers b on a.chrome_style_id = b.chrome_style_id
            where a.delivery_date > current_date - 90 
            group by b.chrome_style_id
            having count(*) > 0) c on a.chrome_style_id = c.chrome_Style_id
          where a.model_year = _model_year
          order by a.seq), ',')  
      union
      select 10, 'forecast for next 90 days', '' as col_3, ''
      union
      select 11, 'on the ground',  '' as col_3, array_to_string(
        array(
          select coalesce(bb.on_ground, 0)::text as on_ground
          from ta.headers aa
          left join (
            select c.chrome_style_id, count(*) as on_ground
            from nc.vehicle_acquisitions a
            join nc.vehicles b on a.vin = b.vin
            join ta.headers c on b.chrome_style_id = c.chrome_Style_id
            where a.thru_date > current_date
            group by c.chrome_style_id) bb on aa.chrome_style_id = bb.chrome_Style_id
          where aa.model_year = _model_year
          order by aa.seq), ',')
      union
      select 12, 'in system/in transit', '' as col_3, array_to_string(
        array(
          select coalesce(bb.in_sys_tran, 0)::text as in_sys_tran
          from ta.headers aa
          left join (
            select b.chrome_style_id, count(*) as in_sys_tran
            from nc.open_orders a
            join ta.headers b on a.model_year = b.model_year
              and a.model_code = b.model_code
              and a.peg = b.peg
            group by b.chrome_style_id) bb on aa.chrome_style_id = bb.chrome_Style_id
          where aa.model_year = _model_year
          order by aa.seq), ',')          
      union
      select 13, 'long/short', '', '' as col_3
      union
      select 14, 'avg # days from order to invoice',  '' as col_3, array_to_string(
        array(
          select coalesce(b.avg_days_order_to_invoice, 0)::text as in_sys_tran
          from ta.headers a
          left join order_to_invoice b on a.chrome_style_id = b.chrome_Style_id
          where a.model_year = _model_year
          order by a.seq), ',')
      union
      select 15, 'avg # days on the ground to sold',  '' as col_3, array_to_string(
        array(
          select coalesce(b.avg_days_ground_to_sold, 0)::text as in_sys_tran
          from ta.headers a
          left join ground_to_sold b on a.chrome_style_id = b.chrome_Style_id
          where a.model_year = _model_year
          order by a.seq), ',')
      union
      select 16, 'days supply on the ground', '', '' as col_3
      union
      select 17, 'days supply in system/in transit', '', '' as col_3  
      union
      select 18, 'EXTERIOR COLOR', '' as col_3, array_to_string(
        array(
          select b.sub_header
          from ta.headers a
          left join (
            select model_year, chrome_style_id, ('sold / current')::text as sub_header
            from ta.headers
            where model_year = 2019) b on a.model_year = b.model_year and a.chrome_Style_id = b.chrome_style_id        
          where a.model_year = 2019
          order by a.seq), ',')
--       union
--       -- current includes invoiced and non invoiced vehicles
--       select 19, color,  '' as col_3, array_to_string(the_array, ',')
--       from (
--         select color, array_agg(array[_399780,_399783,_399773,_398577,_399782,_399781,
--           _399778,_399784,_399779,_399774,_399772,_398576,_399788,_399770,_399791,_399771,
--           _399751,_399792,_399776,_399769,_399786,_398571,_399790,_399753,_399785,_399765,
--           _399760,_399787,_399763,_399758,_399768,_398573,_398574,_399766,_399756,_403472,
--           _399759,_399775,_403471,_399767,_399755,_399757,_399777,_399762,_399754,_399789,
--           _399764,_399761,_399752]) as the_array
--         from xtab
--         where color is not null
--         group by color) a 
    ) aa                 
    order by the_order, col_2;
end $$;    
select * from wtf;





