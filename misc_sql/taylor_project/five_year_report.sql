﻿
drop table if exists test_1;
create temp table test_1 as
select a.bopmast_stock_number as stock_number, a.bopmast_vin as vin, a.date_capped, 
  b.year, b.make, b.model, b.body_style, b.model_code, b.chrome_Style_id,
  case
    when delivery_date between '11/01/2014' and '10/31/2015' then 2015
    when delivery_date between '11/01/2015' and '10/31/2016' then 2016
    when delivery_date between '11/01/2016' and '10/31/2017' then 2017
    when delivery_date between '11/01/2017' and '10/31/2018' then 2018
    when delivery_date between '11/01/2018' and '10/31/2019' then 2019
  end as the_year
from arkona.xfm_bopmast a
join arkona.xfm_inpmast b on a.bopmast_vin = b.inpmast_vin
  and b.current_row
where a.current_row
  and a.date_capped between '10/01/2014' and '10/31/2019'
  and a.record_status = 'U'
  and a.vehicle_type = 'N'
  and a.sale_type <> 'W'
  and a.bopmast_stock_number not like 'H%';

  

select make, model, body_style, count(*)
from test_1
where the_year = 2019
group by the_year, make, model, body_style
order by count(*) desc
limit 10