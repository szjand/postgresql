﻿/*
need to exclude fleet sales that were fleet orders
*/

drop table if exists models;
create temp table models as
select bb.category, bb.make, bb.forecast_model, array_agg(distinct aa.alloc_group) as alloc_group,
  array_agg(distinct bb.model_code) as model_code
from (
  select alloc_group
  from jon.configurations
  where alloc_group <> 'n/a'
  group by alloc_group) aa
join (
  select a.category, a.make, a.model as forecast_model, b.alloc_group, b.chrome_style_id, b.model_code
  from nc.forecast_model_configurations a
  join nc.vehicle_configurations b on a.configuration_id = b.configuration_id) bb on aa.alloc_group = bb.alloc_group
group by bb.category, bb.make, bb.forecast_model;


drop table if exists sales;
create temp table sales as
select a.category, a.make, a.forecast_model, coalesce(sum(b.sales), 0) as sales
from models a
join (
  select c.alloc_group, b.model_code, count(*) as sales
  from nc.vehicle_sales a
  join nc.vehicles b on a.vin = b.vin
  join jon.configurations c on b.chrome_style_id = c.chrome_style_id
  where a.sale_type in ('retail','fleet')
    and a.delivery_date between current_date - 65 and current_date - 6
    and a.stock_number not like 'H%'
  group by c.alloc_group, b.model_code) b on b.alloc_group = any(a.alloc_group) and b.model_code = any(a.model_code)
group by a.category, a.make, a.forecast_model
order by a.category, a.make, a.forecast_model;




-- 02/23 excluding fleet orders, 772 sales
select a.*, d.source, e.order_number, e.order_type, b.make, b.model, f.vehicle_order_type, b.configuration_id
-- select a.*
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
-- join jon.configurations c on b.chrome_style_id = c.chrome_style_id
join nc.vehicle_acquisitions d on a.stock_number = d.stock_number and a.ground_date = d.ground_date
left join gmgl.vehicle_orders e on a.vin = e.vin
-- left join gmgl.vehicle_order_types f on e.order_type = f.vehicle_order_type_key
where a.sale_type in ('retail','fleet')
  and a.delivery_date between '09/01/2019' and current_date
  and a.stock_number not like 'H%'
  and coalesce(e.order_type, 'ok') not in ('FBC','FNR')
--   and e.order_type <> 'TRE'
order by e.order_type  


select * from gmgl.vehicle_orders limit 100

-- wholesale nc sales that were not ctp
-- parts trucks, wes, etc
select record_key, record_type, a.bopmast_Stock_number, a.bopmast_Vin, buyer_number, bopmast_Search_name, b.bopmast_Stock_number
from arkona.xfm_bopmast a
left join (
select bopmast_vin, bopmast_stock_number 
from arkona.xfm_bopmast where  bopmast_Stock_number like '%R' 
  and current_row
  and date_capped > '01/01/2019') b on a.bopmast_vin = b.bopmast_vin
left join arkona.ext_inpcmnt c on a.bopmast_vin = c.vin  
where current_row
  and vehicle_type = 'N' 
  and sale_Type = 'W'
  and date_capped > '01/01/2019'

select vin, transaction_date::text || '::' || array_agg(sequence_number::Text || '-' ||comment order by sequence_number, transaction_time)
from arkona.ext_inpcmnt
where vin in ( '1GTV2MEC3JZ147128','1G1ZD5ST8KF123960')
group by vin, transaction_date
order by vin, transaction_date


/*
-- factory source, no order, turns out they are all dealer trades
select a.*, d.source, e.order_number, e.order_type, b.make, b.model, f.vehicle_order_type, g.invoice_number, h.order_number as vis_order, h.delivery_store, g.raw_invoice
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
join jon.configurations c on b.chrome_style_id = c.chrome_style_id
join nc.vehicle_acquisitions d on a.stock_number = d.stock_number and a.ground_date = d.ground_date
left join gmgl.vehicle_orders e on a.vin = e.vin
left join gmgl.vehicle_order_types f on e.order_type = f.vehicle_order_type_key
left join gmgl.vehicle_invoices g on a.vin = g.vin
left join gmgl.vehicle_vis h on a.vin = h.vin
where a.sale_type in ('retail','fleet')
  and a.delivery_date between '01/01/2019' and current_date
  and a.stock_number not like 'H%'
  and source = 'factory'
  and coalesce(e.order_type, 'wtf') = 'wtf'
order by e.order_type  

--ok, looking at invoices, these are actually dealer trade acquisitions, not facxtory
update nc.vehicle_acquisitions
set source = 'dealer trade'
where stock_number = 'G38933'
  and vin = '1GKKNRLS1LZ131552';
update nc.vehicle_acquisitions
set source = 'dealer trade'
where stock_number = 'G39055'
  and vin = '1GYKNDRS5KZ154959';  
update nc.vehicle_acquisitions
set source = 'dealer trade'
where stock_number = 'G39056'
  and vin = '3GNKBKRS8LS586289';     
update nc.vehicle_acquisitions
set source = 'dealer trade'
where stock_number = 'G39045'
  and vin = '2GCVKPEC4K1240476';     
update nc.vehicle_acquisitions
set source = 'dealer trade'
where stock_number = 'G39380'
  and vin = '1GTP9BEK1LZ157482';     
update nc.vehicle_acquisitions
set source = 'dealer trade'
where stock_number = 'G39384'
  and vin = '1GNSKGKC1LR173889';    
 */ 


-- 02/23 excluding fleet orders, 772 sales
-- select a.*, d.source, e.order_number, e.order_type, b.make, b.model, b.configuration_id, f.*
select g.year_month, f.category, f.make, f.model, count(*)
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
join nc.vehicle_acquisitions d on a.stock_number = d.stock_number and a.ground_date = d.ground_date
left join gmgl.vehicle_orders e on a.vin = e.vin
join nc.forecast_model_configurations f on b.configuration_id = f.configuration_id
join dds.dim_date g on a.delivery_date = g.the_date
  and g.year_month > 201909
where a.sale_type in ('retail','fleet')
  and a.stock_number not like 'H%'
  and coalesce(e.order_type, 'ok') not in ('FBC','FNR')
group by g.year_month, f.category, f.make, f.model


select nc.get_forecast() 

select * from nc.forecast_model_configurations

select * 
from sls.months
limit 10