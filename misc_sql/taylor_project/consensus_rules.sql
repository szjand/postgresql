﻿/*
12/23/19
last week used this to generate data for the spreadsheet consensus_worksheet_V2/V3

now we want to generate the same thing for the recently categorized top configs

difference being, there is no forecast at the config level
*/


-- in system / in transit
select alloc_group, 
  count(*) filter (where category = 'in system') as in_system,
  count(*) filter (where category = 'in transit') as in_transit
from nc.open_orders
group by alloc_group, category
order by alloc_group, category

-- in stock
select c.alloc_group, count(*)
from nc.vehicle_acquisitions a
join nc.vehicles b on a.vin = b.vin
join jon.configurations c on b.chrome_style_id = c.chrome_style_id
where a.thru_date > current_date
  and a.stock_number not like 'H%'
group by c.alloc_group
order by c.alloc_group  

-- past  60 days sales
select c.alloc_group, count(*)
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
join jon.configurations c on b.chrome_style_id = c.chrome_style_id
where a.sale_type in ('retail','fleet')
  and a.delivery_date between current_date - 65 and current_date - 6
  and a.stock_number not like 'H%'
group by c.alloc_group
order by c.alloc_group

-- forecast
select category, make, model, sum(forecast) as forecast
from nc.forecast a
where a.current_row
  and year_month in (202002, 202003)
group by category, make, model
order by category, make, model

-- need to formulize this picking of the months of forecast
select the_date, year_month
-- select *
from dds.dim_date
where the_Date = current_date + 43

-- forecast_model/alloc_Group
select aa.alloc_group, bb.category, bb.make, bb.forecast_model
from (
  select alloc_group
  from jon.configurations
  where alloc_group <> 'n/a'
  group by alloc_group) aa
join (
  select a.category, a.make, a.model as forecast_model, b.alloc_group, b.chrome_style_id
  from nc.forecast_model_configurations a
  join nc.vehicle_configurations b on a.configuration_id = b.configuration_id) bb on aa.alloc_group = bb.alloc_group
group by aa.alloc_group, bb.category, bb.make, bb.forecast_model
order by  bb.category, bb.make, bb.forecast_model


--------------------------------------------------------------------
drop table if exists models;
create temp table models as
select bb.category, bb.make, bb.forecast_model, array_agg(distinct aa.alloc_group) as alloc_group
from (
  select alloc_group
  from jon.configurations
  where alloc_group <> 'n/a'
  group by alloc_group) aa
join (
  select a.category, a.make, a.model as forecast_model, b.alloc_group, b.chrome_style_id
  from nc.forecast_model_configurations a
  join nc.vehicle_configurations b on a.configuration_id = b.configuration_id
  where not (b.alloc_group = 'CAMCON' and a.model = 'Camaro')  ) bb on aa.alloc_group = bb.alloc_group
group by bb.category, bb.make, bb.forecast_model;

drop table if exists in_system_transit;
create temp table in_system_transit as
select a.category, a.make, a.forecast_model, coalesce(sum(b.in_system), 0) as in_system, coalesce(sum(b.in_transit), 0) as in_transit
from models a
join (
select alloc_group, 
  count(*) filter (where category = 'in system') as in_system,
  count(*) filter (where category = 'in transit') as in_transit
from nc.open_orders
group by alloc_group) b on b.alloc_group = any(a.alloc_group)
group by a.category, a.make, a.forecast_model
order by a.category, a.make, a.forecast_model;

drop table if exists in_stock;
create temp table in_stock as
select a.category, a.make, a.forecast_model, coalesce(sum(b.in_stock), 0) as in_stock
from models a
join (
  select c.alloc_group, count(*) as in_stock
  from nc.vehicle_acquisitions a
  join nc.vehicles b on a.vin = b.vin
  join jon.configurations c on b.chrome_style_id = c.chrome_style_id
  where a.thru_date > current_date
    and a.stock_number not like 'H%'
  group by c.alloc_group) b on b.alloc_group = any(a.alloc_group)
group by a.category, a.make, a.forecast_model
order by a.category, a.make, a.forecast_model;

drop table if exists sales;
create temp table sales as
select a.category, a.make, a.forecast_model, coalesce(sum(b.sales), 0) as sales
from models a
join (
  select c.alloc_group, count(*) as sales
  from nc.vehicle_sales a
  join nc.vehicles b on a.vin = b.vin
  join jon.configurations c on b.chrome_style_id = c.chrome_style_id
  where a.sale_type in ('retail','fleet')
    and a.delivery_date between current_date - 65 and current_date - 6
    and a.stock_number not like 'H%'
  group by c.alloc_group) b on b.alloc_group = any(a.alloc_group)
group by a.category, a.make, a.forecast_model
order by a.category, a.make, a.forecast_model;


drop table if exists forecast;
create temp table forecast as
select a.category, a.make, a.forecast_model, coalesce(sum(b.forecast), 0) as forecast
from models a
join (
  select category, make, model, sum(forecast) as forecast
  from nc.forecast a
  where a.current_row
    and year_month in (202002, 202003)
  group by category, make, model) b on a.make = b.make and a.forecast_model = b.model
group by a.category, a.make, a.forecast_model
order by a.category, a.make, a.forecast_model;  



select a.category, a.make, a.forecast_model, 
  coalesce(b.in_system, 0) as in_system, coalesce(b.in_transit, 0) as in_transit, 
  coalesce(c.in_stock, 0) as in_stock, coalesce(d.sales, 0) as sales,
  coalesce(e.forecast, 0) as forecast
from models a
left join in_system_transit b on a.make = b.make
  and a.forecast_model = b.forecast_model
left join in_stock c on a.make = c.make
  and a.forecast_model = c.forecast_model
left join sales d on a.make = d.make
  and a.forecast_model = d.forecast_model  
left join forecast e on a.make = e.make
  and a.forecast_model = e.forecast_model    
where forecast <> 0  
order by category, make, forecast_model

/*
-- the issue is forecast model camaro is showing 2 alloc_groups: CAM & CAMCON
-- CAMCON should be for model camaro convert
select * 
from nc.forecast_models 
where model = 

select * 
from nc.allocation_groups
where model like 'cam%'

2 rows for each of the pickups as well see CLDCRW
shit after adding forecast, 3 rows
*/

BTG: assume 8
in system : 26
in transit: 40
in stock: 38
sales: 64
forecast: 44
assume a specified days supply of 60

step 2: days supply of in system based on sales: select 64.0/60 = 1.1/day  select 34/1.1 = 30.9 days supply
step 3: days suppy of in system based on forecast: select 44/60 = .73/day select 34/.7 - 48.6 days supply
step 4: if diff <= 10 use step 3 (forecast), else use step 2 (sales), 
  diff is 17.7, so use step 2 sales, 1.1/day * 60 days = 66 units we should have in stock + in transit, actual is 78, therefor we are long, no consensuss


/*
12/15 from greg
My concern was that it looked like the 2500 and 3500 numbers were high. Ben is thinking you’re double counting. May be in same allocation group. 
*/

yep, fucking HD , same allocation group for 2500 & 3500
select *
from nc.allocation_Groups
where model like '%2500%'
  or model like '%3500%'
order by make, model, model_year,model_code

select *
from nc.vehicle_configurations
where model like '%2500%'
  or model like '%3500%'

select *
from jon.configurations
where model like '%2500%'
  or model like '%3500%'

orders are an issue, thinking model code may be the solution
-- try adding model-code to models
select *
from models

-- sales
select c.alloc_group, b.model, count(*)
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
join jon.configurations c on b.chrome_style_id = c.chrome_style_id
where a.sale_type in ('retail','fleet')
  and a.delivery_date between current_date - 65 and current_date - 6
  and a.stock_number not like 'H%'
group by c.alloc_group,b.model
order by b.model

-- in system/transit
select a.category, a.make, a.forecast_model, coalesce(sum(b.in_system), 0) as in_system, coalesce(sum(b.in_transit), 0) as in_transit
from models a
join (
select alloc_group, 
  count(*) filter (where category = 'in system') as in_system,
  count(*) filter (where category = 'in transit') as in_transit
from nc.open_orders
group by alloc_group) b on b.alloc_group = any(a.alloc_group)
group by a.category, a.make, a.forecast_model
order by a.category, a.make, a.forecast_model;


select alloc_group, model_code,
  count(*) filter (where category = 'in system') as in_system,
  count(*) filter (where category = 'in transit') as in_transit
from nc.open_orders
group by alloc_group, model_code

--------------------------------------------------------------------
--< include model_code in models
-- 12/17 added dec/jan forecast, forecast 1 & 2
-- 03/25/20 hoping this is the data generation for consensus_worksheet_v3_ben
-- why no Sonic, Colorado ?
--------------------------------------------------------------------
-- select * from models
drop table if exists models;
create temp table models as
select bb.category, bb.make, bb.forecast_model, array_agg(distinct aa.alloc_group) as alloc_group,
  array_agg(distinct bb.model_code) as model_code
from (
  select alloc_group
  from jon.configurations
  where alloc_group <> 'n/a'
  group by alloc_group) aa
join (
  select a.category, a.make, a.model as forecast_model, b.alloc_group, b.chrome_style_id, b.model_code
  from nc.forecast_model_configurations a
  join nc.vehicle_configurations b on a.configuration_id = b.configuration_id) bb on aa.alloc_group = bb.alloc_group
group by bb.category, bb.make, bb.forecast_model;
--   where not (b.alloc_group = 'CAMCON' and a.model = 'Camaro')  ) bb on aa.alloc_group = bb.alloc_group
-- group by bb.category, bb.make, bb.forecast_model;
---------------------------------------------------------------------------
select * from nc.open_orders where alloc_group = 'chdreg'
there are actually 3 chev 2500 in nc.open orders, but the model these model codes dont yet exist in 
nc.vehicle_configurations (we havent yet seen these vehicles), but, of course, they do exist in jon.configurations

-- select * from in_system_transit
drop table if exists in_system_transit;
create temp table in_system_transit as
select a.category, a.make, a.forecast_model, coalesce(sum(b.in_system), 0) as in_system, coalesce(sum(b.in_transit), 0) as in_transit
from models a
join (
  select alloc_group, model_code,
    count(*) filter (where category = 'in system') as in_system,
    count(*) filter (where category = 'in transit') as in_transit
  from nc.open_orders
  group by alloc_group, model_code) b on b.alloc_group = any(a.alloc_group) and b.model_code = any(a.model_code)
group by a.category, a.make, a.forecast_model
order by a.category, a.make, a.forecast_model;

---------------------------------------------------------------------
-- need to exclude sales of fleet orders
-- select * from sales
drop table if exists sales;
create temp table sales as
select a.category, a.make, a.forecast_model, coalesce(sum(b.sales), 0) as sales
from models a
join (
  select c.alloc_group, b.model_code, count(*) as sales
  from nc.vehicle_sales a
  join nc.vehicles b on a.vin = b.vin
  join jon.configurations c on b.chrome_style_id = c.chrome_style_id
  where a.sale_type in ('retail','fleet')
    and a.delivery_date between current_date - 65 and current_date - 6
    and a.stock_number not like 'H%'
    and not exists (
      select 1
      from gmgl.vehicle_orders
      where vin = a.vin
        and coalesce(order_type, 'ok') in ('FBC','FNR'))
  group by c.alloc_group, b.model_code) b on b.alloc_group = any(a.alloc_group) and b.model_code = any(a.model_code)
group by a.category, a.make, a.forecast_model
order by a.category, a.make, a.forecast_model;
---------------------------------------------------------------------
-- select * from in_stock
drop table if exists in_stock;
create temp table in_stock as
select a.category, a.make, a.forecast_model, coalesce(sum(b.in_stock), 0) as in_stock
from models a
join (
  select c.alloc_group, c.model_code, count(*) as in_stock
  from nc.vehicle_acquisitions a
  join nc.vehicles b on a.vin = b.vin
  join jon.configurations c on b.chrome_style_id = c.chrome_style_id
  where a.thru_date > current_date
    and a.stock_number not like 'H%'
  group by c.alloc_group, c.model_code) b on b.alloc_group = any(a.alloc_group) and b.model_code = any(a.model_code)
group by a.category, a.make, a.forecast_model
order by a.category, a.make, a.forecast_model;
---------------------------------------------------------------------
-- march & april
drop table if exists forecast_1;
create temp table forecast_1 as
select a.category, a.make, a.forecast_model, coalesce(sum(b.forecast), 0) as forecast
from models a
join (
  select category, make, model, sum(forecast) as forecast
  from nc.forecast a
  where a.current_row
    and year_month in (202003, 202004)
  group by category, make, model) b on a.make = b.make and a.forecast_model = b.model
group by a.category, a.make, a.forecast_model
order by a.category, a.make, a.forecast_model;  
---------------------------------------------------------------------
-- may and june
drop table if exists forecast_2;
create temp table forecast_2 as
select a.category, a.make, a.forecast_model, coalesce(sum(b.forecast), 0) as forecast
from models a
join (
  select category, make, model, sum(forecast) as forecast
  from nc.forecast a
  where a.current_row
    and year_month in (202005, 202006)
  group by category, make, model) b on a.make = b.make and a.forecast_model = b.model
group by a.category, a.make, a.forecast_model
order by a.category, a.make, a.forecast_model;  
---------------------------------------------------------------------
-- 03/26, no sonic, colorado, was limiting to f.forecast <> 0, changed it to e.forecast + f.forecast <> 0  
select a.category, a.make, a.forecast_model, 
  coalesce(b.in_system, 0) as in_system, coalesce(b.in_transit, 0) as in_transit, 
  coalesce(c.in_stock, 0) as in_stock, coalesce(d.sales, 0) as sales,
  coalesce(e.forecast, 0) as forecast_1, coalesce(f.forecast, 0) as forecast_2
from models a
left join in_system_transit b on a.make = b.make
  and a.forecast_model = b.forecast_model
left join in_stock c on a.make = c.make
  and a.forecast_model = c.forecast_model
left join sales d on a.make = d.make
  and a.forecast_model = d.forecast_model  
left join forecast_1 e on a.make = e.make
  and a.forecast_model = e.forecast_model    
left join forecast_2 f on a.make = f.make
  and a.forecast_model = f.forecast_model      
where ((e.forecast + f.forecast <> 0) or (d.sales <> 0))  
order by category, make, forecast_model;

--------------------------------------------------------------------
--/> include model_code in models
--------------------------------------------------------------------

order by alloc group
configure by configuration (chrome styleid)
forecast by forecast_model
grouping elements
  alloc_group
  model_year
  model
  model_code
  chrome_style_id
  forecast_model

-- this is ok as far as it goes, but these are only for vehicles we have seen
-- for example, this shows 4 model_codes for blazer
-- but there are actually 7
-- select count(distinct  model_code) from jon.configurations where model = 'blazer'
select a.category, a.make, a.model,
  array_agg(distinct b.alloc_group) as alloc_group,
  array_agg(distinct b.model_code) as model_code
from nc.forecast_model_configurations a
left join nc.vehicle_configurations b on a.configuration_id = b.configuration_id
group by a.category, a.make, a.model

in jon.configurations, model_year/model_code/peg are unique in addition to chrome_style_id

so, lets enhance the above with jon.configurations 

select a.category, a.make, a.model,
  array_agg(distinct b.alloc_group) as alloc_group,
  array_agg(distinct b.model_code) as model_code
from nc.forecast_model_configurations a
left join nc.vehicle_configurations b on a.configuration_id = b.configuration_id
group by a.category, a.make, a.model


-- the camaro issue, should 1AL67 be both alloc_group, cam & camcon
-- fucking global is shut down for orders today, check it out tomorrow, establish what is up with camaro alloc groups/model codes
-- 12/23/19 camaro issue fixed, had wrong forecast model in nc.forecast_model_configurations
-- using global order vehicles page
-- for all model years
-- CAM : 1AG37 - 1AL37
-- CAMCON : 1AG67 - 1AL67
-- update nc.forecast_model_configurations
-- set model = 'Camaro Convert'
-- where configuration_id in (606,611)
-- select *
-- from nc.vehicle_configurations
-- where model = 'camaro'
-- order by model_code
