﻿/*
Hey Jon,

Would you be able to put together an excel sheet showing the months of January and February forecast columns, per the Forecasting Page in Vision and then actual sales (retail and fleet) of each, for each Jan and Feb? Our inventory is now heavy and getting heavier and it would help to see that I think. 

Taylor Monson
02/14/20
*/


select make, model,
  max(case when year_month = 202001 then forecast end) as jan_fcast,
  max(case when year_month = 202002 then forecast end) as feb_fcast
from nc.forecast
where year_month in (202001,202002)
  and thru_Ts > now()
group by make, model   
order by make, model  


select d.make, d.model, 
  count(a.stock_number) filter (where a.delivery_date between '01/01/2020' and '01/31/2020') as jan_sales,
  count(a.stock_number) filter (where a.delivery_date between '02/01/2020' and '01/28/2020') as jan_sales
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
join nc.vehicle_configurations c on b.configuration_id = c.configuration_id
join nc.forecast_model_configurations d on c.configuration_id = d.configuration_id
where a.sale_type in ('retail','fleet')
  and left(a.stock_number, 1) <> 'H'
group by d.make, d.model


-- spreadsheet jan_feb_fcst_sales
select aa.make, aa.model, aa.jan_fcast, coalesce(bb.jan_sales, 0) as jan_sales, aa.feb_fcast, coalescE(bb.feb_sales, 0) as feb_sales
from (
  select make, model,
    max(case when year_month = 202001 then forecast end) as jan_fcast,
    max(case when year_month = 202002 then forecast end) as feb_fcast
  from nc.forecast
  where year_month in (202001,202002)
    and thru_Ts > now()
  group by make, model)  aa
left join (
  select d.make, d.model, 
    count(a.stock_number) filter (where a.delivery_date between '01/01/2020' and '01/31/2020') as jan_sales,
    count(a.stock_number) filter (where a.delivery_date between '02/01/2020' and '02/28/2020') as feb_sales
  from nc.vehicle_sales a
  join nc.vehicles b on a.vin = b.vin
  join nc.vehicle_configurations c on b.configuration_id = c.configuration_id
  join nc.forecast_model_configurations d on c.configuration_id = d.configuration_id
  where a.sale_type in ('retail','fleet')
    and left(a.stock_number, 1) <> 'H'
  group by d.make, d.model) bb on aa.make = bb.make and aa.model = bb.model
order by aa.make, aa.model  