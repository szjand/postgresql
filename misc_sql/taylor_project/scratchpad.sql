﻿select b.forecast_model, count(*)
from chr.engines a
join ta.headers b on a.chrome_Style_id = b.chrome_style_id
  and b.model_year = 2019
group by b.forecast_model  


-- ok, not all csi have the same number of engine options
select b.chrome_style_id, count(*)
from chr.engines a
join ta.headers b on a.chrome_Style_id = b.chrome_style_id
  and b.model_year = 2019
  and forecast_model = '1500 crew' 
group by b.chrome_style_id  


select *
from chr.engines a
join ta.headers b on a.chrome_Style_id = b.chrome_style_id
  and b.model_year = 2019
  and forecast_model = '1500 crew' 
  and b.chrome_style_id in ('399780','399761')  


-- all engines

select a.model_year, a.chrome_style_id, a.seq, b.option_code, b.description as engine
from ta.headers a
join chr.engines b on a.chrome_style_id = b.chrome_style_id
where a.model_year = 2019
  and a.forecast_model = '1500 Crew'
order by b.chrome_style_id  

select *
from ta.engines
where model_year = 2019

the challenge is to distinguish between an engine is not available for a model vs no vehicles of a model sold with the engine



select engine, array_agg(distinct a.chrome_Style_id order by a.chrome_style_id)
from ta.engines a
join ta.headers b on a.chrome_Style_id = b.chrome_style_id
  and b.model_year = 2019
  and b.forecast_model = '1500 Double'
group by a.engine  
order by engine


select interior, array_agg(distinct a.chrome_Style_id order by a.chrome_style_id)
from ta.interiors a
join ta.headers b on a.chrome_Style_id = b.chrome_style_id
  and b.model_year = 2019
  and b.forecast_model = '1500 Double'
group by a.interior  
order by a.interior


select packages, array_agg(distinct a.chrome_Style_id order by a.chrome_style_id)
from ta.packages a
join ta.headers b on a.chrome_Style_id = b.chrome_style_id
  and b.model_year = 2019
  and b.forecast_model = '1500 Double'
group by a.packages  
order by a.packages