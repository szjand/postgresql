﻿create table jon.car_guru_email_leads(
  the_date date,
  first_name citext,
  last_name citext,
  zip citext,
  phone citext,
  car citext,
  vin citext);

create table jon.car_guru_phone_leads(
  the_date date,
  name citext,
  phone citext,
  zip citext,
  car citext,
  vin citext);


  
select a.*, b.model_year, b.unit_count, b.sale_type, b.stock_number, b.model_year, b.make, b.model, b.buyer, b.cobuyer
from jon.car_guru_email_leads a
left join sls.deals_by_month b on a.vin = b.vin
where b.vin is not null
  and 

select *
from sls.ext_deals
limit 10

select *
from sls.deals_by_month
limit 10


select a.*, b.model_year, b.unit_count, b.sale_type, b.stock_number, b.model_year, b.make, b.model, b.buyer, b.cobuyer, c.*
--   position(upper(last_name) in upper(buyer)),
--   position(upper(first_name) in upper(buyer))
from jon.car_guru_email_leads a
left join sls.deals_by_month b on a.vin = b.vin
left join sls.ext_bopmast_partial c on b.stock_number = c.stock_number
where b.vin is not null
  and (
    b.buyer like '%' || a.first_name || '%'
    or
    b.buyer like '%' || a.last_name || '%')

select a.*, b.model_year, b.unit_count, b.sale_type, b.stock_number, b.model_year, b.make, b.model, b.buyer, b.cobuyer, c.*
--   position(upper(last_name) in upper(buyer)),
--   position(upper(first_name) in upper(buyer))
from jon.car_guru_email_leads a
left join sls.deals_by_month b on a.vin = b.vin
left join sls.ext_bopmast_partial c on b.stock_number = c.stock_number
where b.vin is not null
  and b.buyer like '%' || a.first_name || '%'

select *
from jon.car_guru_email_leads a

-- bopmast subset based on vins in leads
select a.stock_number, a.vin, a.sale_type, a.buyer_bopname_id, a.cobuyer_bopname_id, a.delivery_Date,
  b.last_company_name, b.first_name, b.middle_init, b.zip_code, b.phone_number, b.business_phone, b.cell_phone
from sls.ext_bopmast_partial a
left join arkona.ext_bopname b on a.buyer_bopname_id = b.bopname_Record_key
where vin in (
  select vin
  from jon.car_guru_email_leads)

-- group this and we're good
-- eyeballed for ratinality
select aa.*, bb.stock_number, bb.last_company_name, bb.first_name, bb.zip_code, 
  bb.co_buyer_last, bb.co_buyer_first, bb.delivery_date
from jon.car_guru_email_leads aa
inner join (-- bopmast subset based on vins in leads
  select a.stock_number, a.vin, a.sale_type, a.buyer_bopname_id, a.cobuyer_bopname_id, a.delivery_Date,
    b.last_company_name, b.first_name, b.middle_init, b.zip_code, b.phone_number, b.business_phone, b.cell_phone,
    c.last_company_name as co_buyer_last, c.first_name as co_buyer_first
  from sls.ext_bopmast_partial a
  left join arkona.ext_bopname b on a.buyer_bopname_id = b.bopname_Record_key
  left join arkona.ext_bopname c on a.cobuyer_bopname_id = c.bopname_Record_key
  where vin in (
    select vin
    from jon.car_guru_email_leads)) bb on aa.vin = bb.vin
where (
    bb.co_buyer_last like '%' || aa.last_name || '%'
    or
    bb.last_company_name like '%' || aa.last_name || '%')
  and aa.vin not in ('1GNSKJKC1GR144993','3GCEK13M78G182310','1GNKVLED3CJ280414')

create temp table email_leads as
select first_name, last_name, stock_number, vin  
from (
  select aa.first_name, aa.last_name, aa.vin, bb.stock_number, bb.zip_code, 
    bb.co_buyer_last, bb.co_buyer_first, bb.delivery_date
  from jon.car_guru_email_leads aa
  inner join (-- bopmast subset based on vins in leads
    select a.stock_number, a.vin, a.sale_type, a.buyer_bopname_id, a.cobuyer_bopname_id, a.delivery_Date,
      b.last_company_name, b.first_name, b.middle_init, b.zip_code, b.phone_number, b.business_phone, b.cell_phone,
      c.last_company_name as co_buyer_last, c.first_name as co_buyer_first
    from sls.ext_bopmast_partial a
    left join arkona.ext_bopname b on a.buyer_bopname_id = b.bopname_Record_key
    left join arkona.ext_bopname c on a.cobuyer_bopname_id = c.bopname_Record_key
    where vin in (
      select vin
      from jon.car_guru_email_leads)) bb on aa.vin = bb.vin
  where (
      bb.co_buyer_last like '%' || aa.last_name || '%'
      or
      bb.last_company_name like '%' || aa.last_name || '%')
    and aa.vin not in ('1GNSKJKC1GR144993','3GCEK13M78G182310','1GNKVLED3CJ280414')) x
group by first_name, last_name, stock_number, vin;

select * from email_leads

-- nothing on phone number matches where vin is blank
select a.the_date, a.name, a.phone, replace(right(a.phone,8), '-','')
from jon.car_guru_phone_leads a
left join arkona.ext_bopname b on b.phone_number::text like '%' || replace(right(a.phone,8), '-','') ||'&'
  or b.cell_phone::text like '%' || replace(right(a.phone,8), '-','') ||'&'
  or b.business_phone::text like '%' || replace(right(a.phone,8), '-','') ||'&'
where a.vin = ''

-- this is the only phone lead that matches a deal
select b.*, c.bopmast_id, c.stock_number, 
  d.last_company_name, d.first_name, d.zip_code, d.phone_number, d.business_phone, d.cell_phone
select 'phone', d.first_name, d.last_company_name, c.stock_number, b.vin
from (
  select a.the_date, a.name, a.phone, a.zip, a.car, a.vin
  from jon.car_guru_phone_leads a
  where a.vin <> ''
  group by a.the_date, a.name, a.phone, a.zip, a.car, a.vin) b
inner join sls.ext_bopmast_partial c on b.vin = c.vin
left join arkona.ext_bopname d on c.buyer_bopname_id = d.bopname_Record_key
where b.vin = '2G1FB1E39F9305873'



select bb.lead_type, bb.first_name, bb.last_name, bb.stock_number, bb.vin, 
  sum(case when department_code = 'uc' then -amount else 0 end)::integer as front_gross,
  sum(case when department_code = 'fi' then -amount else 0 end)::integer as fi_gross,
  sum(-amount)::integer as total_gross
from (
  select 'email' as lead_type, first_name, last_name, stock_number, vin 
  from email_leads
  union
  select 'phone', d.first_name, d.last_company_name, c.stock_number, b.vin
  from (
    select a.the_date, a.name, a.phone, a.zip, a.car, a.vin
    from jon.car_guru_phone_leads a
    where a.vin <> ''
    group by a.the_date, a.name, a.phone, a.zip, a.car, a.vin) b
  inner join sls.ext_bopmast_partial c on b.vin = c.vin
  left join arkona.ext_bopname d on c.buyer_bopname_id = d.bopname_Record_key
  where b.vin = '2G1FB1E39F9305873') bb
left join (
  select a.control, a.amount, b.account, b.account_type, c.journal_code, b.department_code
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
    and b.current_row = true
    and b.account_Type in ('Sale','COGS')
    and b.department_code in ('uc','fi')
  inner join fin.dim_journal c on a.journal_key = c.journal_key
  where a.post_Status = 'Y')  d on bb.stock_number = d.control
group by bb.lead_type, bb.first_name, bb.last_name, bb.stock_number, bb.vin
order by bb.stock_number



select a.control, 
  sum(case when department_code = 'uc' then -amount else 0 end)::integer as front_gross,
  sum(case when department_code = 'fi' then -amount else 0 end)::integer as fi_gross,
  sum(-amount)::integer as total_gross
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
  and b.current_row = true
  and b.account_Type in ('Sale','COGS', 'Other Income', 'Expense')
  and b.department_code in ('uc','fi')
inner join fin.dim_journal c on a.journal_key = c.journal_key
where a.post_Status = 'Y'
  and control in ('32281','31448A','32284l','31161r','32186xx')
group by a.control  


