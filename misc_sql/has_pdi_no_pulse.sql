﻿select control
from (
  with
    inventory as (
      select a.control
      from fin.fact_gl a
      inner join dds.dim_date b on a.date_key = b.date_key
        and b.the_date between current_Date - 900 and current_date--'06/22/2018'
      inner join fin.dim_account c on a.account_key = c.account_key 
      inner join nc.inventory_accounts cc on c.account = cc.account
      where a.post_status = 'Y'
      group by a.control
      having sum(a.amount) > 10000),
    inv_vins as (    
      select b.control, a.inpmast_vin as vin
      from arkona.xfm_inpmast a
      join inventory b on a.inpmast_stock_number = b.control
      where a.current_row )   
  select aa.*, bb.opcodes, bb.opcodes like '%PDI%' as has_pdi, case when cc.control is null then false else true end as has_pulse
  from (select * from inv_vins) aa
  left join (
    select d.vin, string_agg(c.opcode, ',') as opcodes
    from ads.ext_fact_repair_order a
    inner join dds.dim_date b on a.opendatekey = b.date_key
    inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
  --     and c.opcode = 'pdi'
    inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
    where d.vin in (select vin from inv_vins)
    group by d.vin) bb on aa.vin = bb.vin 
  left join (
    select control
    from fin.fact_gl a
    join fin.dim_account b on a.account_key = b.account_key
      and b.account in ('130402','230402')) cc on aa.control = cc.control order by aa.control) dd
where has_pdi and not has_pulse
order by control