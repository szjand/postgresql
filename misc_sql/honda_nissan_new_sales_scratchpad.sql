﻿select c.make, c.model,
  count(*) filter (where b.year_month = 201801) as jan,
  count(*) filter (where b.year_month = 201802) as feb,
  count(*) filter (where b.year_month = 201803) as mar,
  count(*) filter (where b.year_month = 201804) as apr,
  count(*) filter (where b.year_month = 201805) as may,
  count(*) filter (where b.year_month = 201806) as jun,
  count(*) filter (where b.year_month = 201807) as jul
from sls.ext_bopmast_partial a
inner join dds.dim_date b on a.delivery_date = b.the_date
inner join arkona.xfm_inpmast c on a.vin = c.inpmast_vin
  and c.current_row = true
where a.store = 'RY2'
  and a.delivery_date between '01/01/2018' and current_date
  and a.vehicle_type = 'N'
group by c.make, c.model
union
select 'z','z',
  count(*) filter (where b.year_month = 201801) as jan,
  count(*) filter (where b.year_month = 201802) as feb,
  count(*) filter (where b.year_month = 201803) as mar,
  count(*) filter (where b.year_month = 201804) as apr,
  count(*) filter (where b.year_month = 201805) as may,
  count(*) filter (where b.year_month = 201806) as jun,
  count(*) filter (where b.year_month = 201807) as jul
from sls.ext_bopmast_partial a
inner join dds.dim_date b on a.delivery_date = b.the_date
inner join arkona.xfm_inpmast c on a.vin = c.inpmast_vin
  and c.current_row = true
where a.store = 'RY2'
  and a.delivery_date between '01/01/2018' and current_date
  and a.vehicle_type = 'N'

  
select *
from arkona.xfm_inpmast
limit 10

select a.*
from sls.ext_bopmast_partial a
inner join dds.dim_date b on a.delivery_date = b.the_date
  and b.year_month = 201806
inner join arkona.xfm_inpmast c on a.vin = c.inpmast_vin
  and c.current_row = true
where a.store = 'RY2'
  and a.delivery_date between '01/01/2018' and current_date
  and a.vehicle_type = 'N'


select c.make, c.model, 
  count(*) filter (where year_month = 201801) as jan,
  count(*) filter (where year_month = 201802) as feb,
  count(*) filter (where year_month = 201803) as mar,
  count(*) filter (where year_month = 201804) as apr,
  count(*) filter (where year_month = 201805) as may,
  count(*) filter (where year_month = 201806) as jun
from sls.deals_by_month a
inner join arkona.xfm_inpmast c on a.vin = c.inpmast_vin
  and c.current_row = true
where a.year_month > 201712
  and a.store_code = 'ry2'
  and a.vehicle_type = 'new'
  and a.unit_count > 0
group by c.make, c.model

-- they want trim
select c.make, c.model, body_style,
  count(*) filter (where year_month = 201801) as jan,
  count(*) filter (where year_month = 201802) as feb,
  count(*) filter (where year_month = 201803) as mar,
  count(*) filter (where year_month = 201804) as apr,
  count(*) filter (where year_month = 201805) as may,
  count(*) filter (where year_month = 201806) as jun
from sls.deals_by_month a
inner join arkona.xfm_inpmast c on a.vin = c.inpmast_vin
  and c.current_row = true
where a.year_month > 201712
  and a.store_code = 'ry2'
  and a.vehicle_type = 'new'
  and a.unit_count > 0
group by c.make, c.model, body_style





select a.make, a.model, a.body_style, inpmast_vin
-- select *
from arkona.xfm_inpmast a
where a.make in ('honda','nissan')
  and a.status = 'I'
  and a.type_n_u = 'n'
limit 10
  

select a.vin, 
  jsonb_array_elements(b.response->'style')->'model'->>'$value',
  jsonb_array_elements(b.response->'style')->'attributes'->>'name',
  jsonb_array_elements(b.response->'style')->'attributes'->>'trim',
  b.style_count,
  c.make, c.model, c.body_style
from sls.deals_by_month a
left join chr.describe_vehicle b on a.vin = b.vin
left join arkona.xfm_inpmast c on a.vin = c.inpmast_vin
  and c.current_row = true
where a.year_month > 201712
  and a.store_code = 'ry2'
  and a.vehicle_type = 'new'
  and b.vin is not null


