﻿create table greg.uc_transactions (
  store citext not null,
  the_date date not null, 
  control citext not null,
  trans bigint not null,
  seq integer not null,
  year_month integer not null,
  gl_account citext not null,
  journal_code citext not null,
  account_type citext not null,
  line integer not null,
  amount numeric(12,2) not null,
  unit_count integer not null,
  trans_desc citext not null,
  is_trade boolean not null,
  constraint uc_transactions_pkey primary key(trans, seq));
/*
drop table if exists accounts_lines;
create temp table accounts_lines as
select a.fxmcyy, 
  case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code,
  a.fxmpge as the_page, a.fxmlne::integer as line, trim(b.g_l_acct_number) as gl_account
from arkona.ext_eisglobal_sypffxmst a
inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
where a.fxmcyy > 2011
  and trim(a.fxmcde) = 'GM'
  and coalesce(b.consolidation_grp, '1') <> '3'
  and b.factory_code = 'GM'
  and trim(b.factory_account) <> '331A'
  and b.company_number = 'RY1'
  and b.g_l_acct_number <> ''
  and a.fxmpge = 16 
  and a.fxmlne between 1 and 14;

-- drop table if exists uc_trans;
-- create table uc_trans as
truncate greg.uc_transactions;
insert into greg.uc_transactions
select d.store_code::citext as store, b.the_date, a.control, a.trans, a.seq, b.year_month, 
  d.gl_account::citext, aa.journal_code, c.account_type, d.line, a.amount,
  case 
    when c.account_type = 'sale' and aa.journal_code in ('VSN','VSU') then
      case
        when a.amount < 0 then 1
        else
          case
            when a.amount > 0 then -1
            else 0
          end
      end
    else 0
  end as unit_count,
  e.description as trans_desc,
  case
    when right(trim(a.control),1)::citext in ('a','b','c','d','e','f') then true
    else false
  end as is_trade
from fin.fact_gl a   
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_year > 2011
inner join fin.dim_account c on a.account_key = c.account_key
inner join accounts_lines d on b.the_year = d.fxmcyy
  and c.account = d.gl_account
inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y';
-- create unique index on uc_trans(trans,seq);


drop table if exists delivered_trans;
create temp table delivered_trans as
select a.store, a.control,
  a.year_month, a.line, max(a.trans_desc) as trans_desc, sum(a.amount) as amount
from uc_trans a
inner join (
  select store, year_month, control, trans_desc
  from uc_trans
  where account_type = 'sale'
  group by store, year_month, control, trans_desc
  having sum(unit_count) = 1) b on a.store = b.store
    and a.year_month = b.year_month 
    and a.control = b.control 
    and a.trans_desc = b.trans_desc
where a.account_type = 'sale'
group by a.store, a.control, a.year_month, a.line;   

*/


select a.store, a.control,
  a.year_month, a.line, string_agg(a.trans_desc, '|') as trans_desc, sum(a.amount) as amount
from uc_trans a
inner join (
  select store, year_month, control, trans_desc
  from uc_trans
  where account_type = 'sale'
  group by store, year_month, control, trans_desc
  having sum(unit_count) = 1) b on a.store = b.store
    and a.year_month = b.year_month 
    and a.control = b.control 
    and a.trans_desc = b.trans_desc
where a.account_type = 'sale'
  and a.control = '29678p'
group by a.store, a.control, a.year_month, a.line;

-- v1 spreadsheet
select a.store, a.year_month,
  sum(case when a.account_type = 'sale' and a.line between 1 and 5 then a.unit_count else 0 end) as units_retailed,
  sum(case when a.account_type = 'sale' and a.line between 8 and 10 then a.unit_count else 0 end) as units_wholesaled,
  -1 * sum(case when a.line between 1 and 5 then amount else 0 end)::integer as total_retail_used_car_gross,
  -1 * sum(case when a.line between 8 and 10 then amount else 0 end)::integer as total_wholesale_used_car_gross,
  -(sum(case when a.line between 1 and 5 then amount else 0 end)/sum(case when a.account_type = 'sale' and a.line between 1 and 5 then a.unit_count else 0 end))::integer as retail_pvr,
  -(sum(case when a.line between 8 and 10 then amount else 0 end)/sum(case when a.account_type = 'sale' and a.line between 8 and 10 then a.unit_count else 0 end))::integer as wholesale_pvr,
  -(sum(case when a.is_trade = true and a.line between 1 and 10 then amount else 0 end)/sum(case when a.account_type = 'sale' and a.line between 1 and 5 then a.unit_count else 0 end))::integer as trade_pvr,
  -(sum(case when a.is_trade = false and a.line between 1 and 10 then amount else 0 end)/sum(case when a.account_type = 'sale' and a.line between 1 and 5 then a.unit_count else 0 end))::integer as non_trade_pvr
from uc_trans a
inner join (
  select store, year_month, control, trans_desc
  from uc_trans
  where account_type = 'sale'
  group by store, year_month, control, trans_desc
  having sum(unit_count) = 1) b on a.store = b.store
    and a.year_month = b.year_month 
    and a.control = b.control 
    and a.trans_desc = b.trans_desc
group by a.store, a.year_month


-- why the diff between this and desktop/sql/ucinv_Recon_2.sql
select a.control, a.gl_account, sum(a.amount)
from uc_trans a
inner join (
  select store, year_month, control, trans_desc
  from uc_trans
  where account_type = 'sale'
  group by store, year_month, control, trans_desc
  having sum(unit_count) = 1) b on a.store = b.store
    and a.year_month = b.year_month 
    and a.control = b.control 
    and a.trans_desc = b.trans_desc
where a.year_month = 201706
  and a.store = 'ry1'
  and gl_account in ('164700','164701','165100','165101')   
group by a.control, a.gl_account
order by a.control

-- so far, 2 cases where the trans_desc for a recon entry does not match the trans_desc for 
-- the sale entry, therefore the recon does not show up
-- 29185D, 28119RA, 29501B, 29678p
select *
from uc_trans
where control = '29678p'


select *
from uc_trans
where control = '29569b'

-- i do not believe there is a way to attribute recon transactions to the correct instance of a multiple
-- sale stock number
-- 9/14/17 except maybe time frame, now that i am looking at representing (in uc) multiple sales
-- -- as a single vehicle instance with periods of sold status
-- also i would like to see instances of post sale recon

wait a minute, the delivered deal is only for sale accounts

see if casing the join on trans_Desc matters
yes, made a big difference

select a.control, a.gl_account, sum(a.amount)
from uc_trans a
inner join (
  select store, year_month, control, trans_desc
  from uc_trans
  where account_type = 'sale'
  group by store, year_month, control, trans_desc
  having sum(unit_count) = 1) b on a.store = b.store
    and a.year_month = b.year_month 
    and a.control = b.control 
    and 
      case 
        when a.account_type = 'sale' then a.trans_desc = b.trans_desc
        else 1 = 1
      end
where a.year_month = 201706
  and a.store = 'ry1'
  and gl_account in ('164700','164701','165100','165101')   
group by a.control, a.gl_account
order by a.control



-- v2 spreadsheet, with casing trans_desc join
select a.store, a.year_month,
  sum(case when a.account_type = 'sale' and a.line between 1 and 5 then a.unit_count else 0 end) as units_retailed,
  sum(case when a.account_type = 'sale' and a.line between 8 and 10 then a.unit_count else 0 end) as units_wholesaled,
  -1 * sum(case when a.line between 1 and 5 then amount else 0 end)::integer as total_retail_used_car_gross,
  -1 * sum(case when a.line between 8 and 10 then amount else 0 end)::integer as total_wholesale_used_car_gross,
  -(sum(case when a.line between 1 and 5 then amount else 0 end)/sum(case when a.account_type = 'sale' and a.line between 1 and 5 then a.unit_count else 0 end))::integer as retail_pvr,
  -(sum(case when a.line between 8 and 10 then amount else 0 end)/sum(case when a.account_type = 'sale' and a.line between 8 and 10 then a.unit_count else 0 end))::integer as wholesale_pvr,
  -(sum(case when a.is_trade = true and a.line between 1 and 10 then amount else 0 end)/sum(case when a.account_type = 'sale' and a.line between 1 and 5 then a.unit_count else 0 end))::integer as trade_pvr,
  -(sum(case when a.is_trade = false and a.line between 1 and 10 then amount else 0 end)/sum(case when a.account_type = 'sale' and a.line between 1 and 5 then a.unit_count else 0 end))::integer as non_trade_pvr
from uc_trans a
inner join (
  select store, year_month, control, trans_desc
  from uc_trans
  where account_type = 'sale'
  group by store, year_month, control, trans_desc
  having sum(unit_count) = 1) b on a.store = b.store
    and a.year_month = b.year_month 
    and a.control = b.control 
    and 
      case 
        when a.account_type = 'sale' then a.trans_desc = b.trans_desc
        else 1 = 1
      end
group by a.store, a.year_month

/*
Getting there… The PVR’s don’t make sense to me.

Take the current month:

Retail PVR 823
Trade PVR 556
Non-trade PVR 209

I’m assuming retail sales is made up of sales of trades and non-trades. Doesn’t make sense that they are both smaller than the combined set.

Also some huge outliers in total wholesale loss.
*/

-- v3
select a.store, a.year_month,
  sum(case when a.account_type = 'sale' and a.line between 1 and 5 then a.unit_count else 0 end) as units_retailed,
  sum(case when a.account_type = 'sale' and a.line between 8 and 10 then a.unit_count else 0 end) as units_wholesaled,
  -1 * sum(case when a.line between 1 and 5 then amount else 0 end)::integer as total_retail_used_car_gross,
  -1 * sum(case when a.line between 8 and 10 then amount else 0 end)::integer as total_wholesale_used_car_gross,
  -(sum(case when a.line between 1 and 5 then amount else 0 end)/sum(case when a.account_type = 'sale' 
      and a.line between 1 and 5 then a.unit_count else 0 end))::integer as retail_pvr,
  -(sum(case when a.line between 8 and 10 then amount else 0 end)/sum(case when a.account_type = 'sale' 
      and a.line between 8 and 10 then a.unit_count else 0 end))::integer as wholesale_pvr,
  -(sum(case when a.is_trade = true and a.line between 1 and 10 then amount else 0 end)/
      sum(case when a.account_type = 'sale' and a.line between 1 and 10 
          and is_trade = true then a.unit_count else 0 end))::integer as trade_pvr,
  -(sum(case when a.is_trade = false and a.line between 1 and 10 then amount else 0 end)/
      sum(case when a.account_type = 'sale' and a.line between 1 and 10 
          and is_trade = false then a.unit_count else 0 end))::integer as non_trade_pvr
from greg.uc_transactions a
inner join ( -- actual deliveries for month
  select store, year_month, control, trans_desc
  from greg.uc_transactions
  where account_type = 'sale'
  group by store, year_month, control, trans_desc
  having sum(unit_count) = 1) b on a.store = b.store
    and a.year_month = b.year_month 
    and a.control = b.control 
    and 
      case 
        when a.account_type = 'sale' then a.trans_desc = b.trans_desc
        else 1 = 1
      end
group by a.store, a.year_month


-- v3
select a.store, a.year_month,
  sum(case when a.account_type = 'sale' and a.line between 1 and 5 then a.unit_count else 0 end) as units_retailed,
  sum(case when a.account_type = 'sale' and a.line between 8 and 10 then a.unit_count else 0 end) as units_wholesaled,
  -1 * sum(case when a.line between 1 and 5 then amount else 0 end)::integer as total_retail_used_car_gross,
  -1 * sum(case when a.line between 8 and 10 then amount else 0 end)::integer as total_wholesale_used_car_gross,
  -(sum(case when a.line between 1 and 5 then amount else 0 end)/sum(case when a.account_type = 'sale' 
      and a.line between 1 and 5 then a.unit_count else 0 end))::integer as retail_pvr,
  -(sum(case when a.line between 8 and 10 then amount else 0 end)/sum(case when a.account_type = 'sale' 
      and a.line between 8 and 10 then a.unit_count else 0 end))::integer as wholesale_pvr,
  -(sum(case when a.is_trade = true and a.line between 1 and 10 then amount else 0 end)/
      sum(case when a.account_type = 'sale' and a.line between 1 and 10 
          and is_trade = true then a.unit_count else 0 end))::integer as trade_pvr,
  -(sum(case when a.is_trade = false and a.line between 1 and 10 then amount else 0 end)/
      sum(case when a.account_type = 'sale' and a.line between 1 and 10 
          and is_trade = false then a.unit_count else 0 end))::integer as non_trade_pvr
from greg.uc_transactions a
inner join ( -- actual deliveries for month
  select store, year_month, control, trans_desc
  from greg.uc_transactions
  where account_type = 'sale'
  group by store, year_month, control, trans_desc
  having sum(unit_count) = 1) b on a.store = b.store
    and a.year_month = b.year_month 
    and a.control = b.control 
    and 
      case 
        when a.account_type = 'sale' then a.trans_desc = b.trans_desc
        else 1 = 1
      end   
group by a.store, a.year_month
order by total_wholesale_used_car_gross

select *
-- select sum(amount)
select control, sum(amount)::integer
from greg.uc_transactions a
where year_month = 201306
  and store = 'ry1'
  and line between 8 and 10
group by control
order by abs(amount) desc   



select store, year_month, line, sum(unit_count)
from greg.uc_transactions a
where year_month = 201306
  and store = 'ry1'
group by store, year_month, line
order by store, year_month, line


select store, year_month, sum(amount)
from greg.uc_transactions a
where year_month = 201306
  and store = 'ry1'
group by store, year_month
order by store, year_month


select *
from ( -- with delivery only)
select a.control, sum(a.amount)::integer as amount
from greg.uc_transactions a
inner join ( -- actual deliveries for month
  select store, year_month, control, trans_desc
  from greg.uc_transactions
  where account_type = 'sale'
  group by store, year_month, control, trans_desc
  having sum(unit_count) = 1) b on a.store = b.store
    and a.year_month = b.year_month 
    and a.control = b.control 
    and 
      case 
        when a.account_type = 'sale' then a.trans_desc = b.trans_desc
        else 1 = 1
      end
where a.year_month = 201306  
  and a.store = 'ry1' 
  and line between 8 and 10
group by a.control, a.year_month) x
inner join ( --without delivery
select control, sum(amount)::integer as amount
from greg.uc_transactions a
where year_month = 201306
  and store = 'ry1'
  and line between 8 and 10
group by control) xx on x.control = xx.control and x.amount::integer <> xx.amount::integer
order by x.control

-- the problem with 201306 is multiple trans_desc per control
select *
from greg.uc_transactions
where control in ('18069B','18269XX','18432XX','18831XX','18831XX','19152XX','19259XX','19348XX','19492XX','19498XX')
  and account_type = 'sale'
order by control, gl_account


-- same thing 18583A 201301
select *
from greg.uc_transactions
where control in ('18583a')
  and account_type = 'sale'
order by control, gl_account


select *
from ( -- with delivery only)
select a.control, sum(a.amount)::integer as amount
from greg.uc_transactions a
inner join ( -- actual deliveries for month
  select store, year_month, control, trans_desc
  from greg.uc_transactions
  where account_type = 'sale'
  group by store, year_month, control, trans_desc
  having sum(unit_count) = 1) b on a.store = b.store
    and a.year_month = b.year_month 
    and a.control = b.control 
    and 
      case 
        when a.account_type = 'sale' then a.trans_desc = b.trans_desc
        else 1 = 1
      end
where a.year_month = 201504
  and a.store = 'ry1' 
  and line between 8 and 10
group by a.control, a.year_month) x
inner join ( --without delivery
select control, sum(amount)::integer as amount
from greg.uc_transactions a
where year_month = 201504
  and store = 'ry1'
  and line between 8 and 10
group by control) xx on x.control = xx.control and x.amount::integer <> xx.amount::integer
order by x.control

-- same thing 25401x 201504
select *
from greg.uc_transactions
where control in ('25401x')
  and account_type = 'sale'
order by control, gl_account

/*
greg: the PVR numbers aren't quite there yet -- non-trade and trade both lower than retail pvr

later that same day (after i emailed my confusion)

If we sold 177 retail units in June ’17 then the 177 should split into two components:
  Trades: 176, gross 99107, per 563
  Non-trades: 

As I’m reading your table, I see the issue — trades and non-trades need to be split out into retail and wholesale also.

Then I can make a good retail and wholesale analysis.

*/

-- V4
drop table if exists v4;
create temp table v4 as
select a.store, a.year_month,
  sum(case when a.account_type = 'sale' and a.line between 1 and 5 then a.unit_count else 0 end) as units_retailed,
  -1 * sum(case when a.line between 1 and 5 then amount else 0 end)::integer as total_retail_used_car_gross,
  -(sum(case when a.line between 1 and 5 then amount else 0 end)/sum(case when a.account_type = 'sale' 
      and a.line between 1 and 5 then a.unit_count else 0 end))::integer as retail_pvr,  
      
  sum(case when a.account_type = 'sale' and a.line between 8 and 10 then a.unit_count else 0 end) as units_wholesaled,
  -1 * sum(case when a.line between 8 and 10 then amount else 0 end)::integer as total_wholesale_used_car_gross,  
  -(sum(case when a.line between 8 and 10 then amount else 0 end)/sum(case when a.account_type = 'sale' 
      and a.line between 8 and 10 then a.unit_count else 0 end))::integer as wholesale_pvr,       

  sum(case when a.account_type = 'sale' and a.line between 1 and 10 and is_trade = true then a.unit_count else 0 end) as trade_units,   
  -1 * sum(case when a.line between 1 and 10 and is_trade = true then amount else 0 end)::integer as trade_gross, 
  -(sum(case when a.is_trade = true and a.line between 1 and 10 then amount else 0 end)/
      sum(case when a.account_type = 'sale' and a.line between 1 and 10 
          and is_trade = true then a.unit_count else 0 end))::integer as trade_pvr,  
          
  sum(case when a.account_type = 'sale' and a.line between 1 and 10 and is_trade = false then a.unit_count else 0 end) as non_trade_units,  
  -1 * sum(case when a.line between 1 and 10 and is_trade = false then amount else 0 end)::integer as non_trade_gross, 
  -(sum(case when a.is_trade = false and a.line between 1 and 10 then amount else 0 end)/
      sum(case when a.account_type = 'sale' and a.line between 1 and 10 
          and is_trade = false then a.unit_count else 0 end))::integer as non_trade_pvr,

  sum(case when a.account_type = 'sale' and a.line between 1 and 5 and is_trade = true then a.unit_count else 0 end) as trade_retail_units,   
  -1 * sum(case when a.line between 1 and 5 and is_trade = true then amount else 0 end)::integer as trade_retail_gross, 
  -(sum(case when a.is_trade = true and a.line between 1 and 5 then amount else 0 end)/
      sum(case when a.account_type = 'sale' and a.line between 1 and 5
          and is_trade = true then a.unit_count else 0 end))::integer as trade_retail_pvr,
          
  sum(case when a.account_type = 'sale' and a.line between 8 and 10 and is_trade = true then a.unit_count else 0 end) as trade_whsl_units,   
  -1 * sum(case when a.line between 8 and 10 and is_trade = true then amount else 0 end)::integer as trade_whsl_gross, 
--   case
--     when sum(case when a.account_type = 'sale' and a.line between 8 and 10 and is_trade = true then a.unit_count else 0 end) = 0 then 0
--     else
    -(sum(case when a.is_trade = true and a.line between 8 and 10 then amount else 0 end)/
        sum(case when a.account_type = 'sale' and a.line between 8 and 10
            and is_trade = true then a.unit_count else 0 end))::integer as trade_whsl_pvr,   
--   end as trade_whsl_pvr,     

  sum(case when a.account_type = 'sale' and a.line between 1 and 5 and is_trade = false then a.unit_count else 0 end) as non_trade_retail_units,   
  -1 * sum(case when a.line between 1 and 5 and is_trade = false then amount else 0 end)::integer as non_trade_retail_gross, 
  -(sum(case when a.is_trade = false and a.line between 1 and 5 then amount else 0 end)/
      sum(case when a.account_type = 'sale' and a.line between 1 and 5
          and is_trade = false then a.unit_count else 0 end))::integer as non_trade_retail_pvr,
          
  sum(case when a.account_type = 'sale' and a.line between 8 and 10 and is_trade = false then a.unit_count else 0 end) as non_trade_whsl_units,   
  -1 * sum(case when a.line between 8 and 10 and is_trade = false then amount else 0 end)::integer as non_trade_whsl_gross, 
  case
    when sum(case when a.account_type = 'sale' and a.line between 8 and 10 and is_trade = false then a.unit_count else 0 end) = 0 then 0
    else
    -(sum(case when a.is_trade = true and a.line between 8 and 10 then amount else 0 end)/
        sum(case when a.account_type = 'sale' and a.line between 8 and 10
            and is_trade = false then a.unit_count else 0 end))::integer 
  end as non_trade_whsl_pvr
                                     
from greg.uc_transactions a
inner join ( -- actual deliveries for month
  select store, year_month, control, trans_desc
  from greg.uc_transactions
  where account_type = 'sale'
  group by store, year_month, control, trans_desc
  having sum(unit_count) = 1) b on a.store = b.store
    and a.year_month = b.year_month 
    and a.control = b.control 
    and 
      case 
        when a.account_type = 'sale' then a.trans_desc = b.trans_desc
        else 1 = 1
      end   
group by a.store, a.year_month;


-- units = 0 gross <> 0
select * 
from v4
where non_trade_whsl_units = 0
  and non_trade_whsl_gross <> 0

select a.*
from greg.uc_transactions a
inner join ( -- actual deliveries for month
  select store, year_month, control, trans_desc
  from greg.uc_transactions
  where account_type = 'sale'
  group by store, year_month, control, trans_desc
  having sum(unit_count) = 1) b on a.store = b.store
    and a.year_month = b.year_month 
    and a.control = b.control 
    and 
      case 
        when a.account_type = 'sale' then a.trans_desc = b.trans_desc
        else 1 = 1
      end 
where a.is_trade = false
  and ((a.store = 'ry1' and a.year_month = 201210) or (a.store = 'ry2' and a.year_month = 201409))
  and a.line between 8 and 10
  and a.account_type = 'sale'


select *
from greg.uc_transactions
where control = '17855X'