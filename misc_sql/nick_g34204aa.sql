﻿/*
select * from board.sales_board where stock_number = 'g34204aa'
board_id = 14822

*/

do 
$$
declare
_date date := current_date - 1;
begin
drop table if exists wtf;
create temp table wtf as
    select store_key as id, store_key as sales_store,(select board.board_stats(_date::date,gg.store_key) ) as board_counts,

    array(
      select a.board_id 
      from board.sales_board b  
      inner join board.board_types d on b.board_type_key = d.board_type_key  and board_sub_type in ('Retail')
      left join board.daily_board a on a.board_id = b.board_id
      where gg.store_key = b.store_key 
        and b.is_deleted = false 
        and boarded_date = coalesce(_date,current_date)
        and a.vehicle_type = 'U') as used_vehicles,
--     array(
--       select a.board_id 
--       from board.sales_board b  
--       inner join board.board_types d on b.board_type_key = d.board_type_key and board_sub_type not in ('Retail','Fleet','Trade')
--       and board_type not in ('Addition','Back-on')
--       left join board.daily_board a on a.board_id = b.board_id
--       where gg.store_key = b.store_key 
--         and b.is_deleted = false 
--         and boarded_date = coalesce(_date,current_date)) as other_outgoing_vehicles
--     array(
--       select a.board_id 
--       from board.sales_board b  
--       inner join board.board_types d on b.board_type_key = d.board_type_key and board_type  in ('Back-on')
--       left join board.daily_board a on a.board_id = b.board_id
--       where gg.store_key = b.store_key 
--         and b.is_deleted = false 
--         and boarded_date = coalesce(_date,current_date)) as back_on_vehicles,
--             array(
--       select a.board_id 
--       from board.sales_board b  
--       inner join board.board_types d on b.board_type_key = d.board_type_key and board_type  in ('Back-on')
--       left join board.daily_board a on a.board_id = b.board_id
--       where gg.store_key = b.store_key 
--         and b.is_deleted = false 
--         and boarded_date = coalesce(_date,current_date)
--         and a.vehicle_type = 'N') as new_back_on_vehicles,
--             array(
--       select a.board_id 
--       from board.sales_board b  
--       inner join board.board_types d on b.board_type_key = d.board_type_key and board_type  in ('Back-on')
--       left join board.daily_board a on a.board_id = b.board_id
--       where gg.store_key = b.store_key 
--         and b.is_deleted = false 
--         and boarded_date = coalesce(_date,current_date)
--         and a.vehicle_type = 'U') as used_back_on_vehicles,
--     array(
--       select a.board_id 
--       from board.sales_board b  
--       inner join board.board_types d on b.board_type_key = d.board_type_key and board_type  in ('Addition') and board_sub_type <> 'Trade'
--       left join board.daily_board a on a.board_id = b.board_id
--       where gg.store_key = b.store_key 
--         and b.is_deleted = false 
--         and boarded_date = coalesce(_date,current_date)) as addition_vehicles,
--             array(
--       select a.board_id 
--       from board.sales_board b  
--       inner join board.board_types d on b.board_type_key = d.board_type_key and board_type  in ('Deal') and board_sub_type =  'Wholesale'
--       left join board.daily_board a on a.board_id = b.board_id
--       inner join board.deal_gross c on b.board_id = c.board_id --and from_date::date = coalesce(_date,current_date)
--       where gg.store_key = b.store_key 
--         and b.is_deleted = false 
--         and deal_number <> 'N/A'
--         and boarded_date = coalesce(_date,current_date)) as gross_wholesale_vehicles
      from onedc.stores gg;
end
$$;  
select * from wtf    