﻿select technumber, name
from ads.ext_dim_tech
where active
  and currentrow
order by name

-- jay olson 631, october

select b.the_Date, a.*, c.*
from ads.ext_fact_repair_order a
join dds.dim_date b on a.finalclosedatekey = b.date_key
  and b.year_month = 201810
join ads.ext_dim_tech c on a.techkey = c.techkey
  and c.technumber = '631'  
where ro = '16319919'


-- things i know for sure about labor profit analysis
/*
1. uses final close date
2. does not include the .2 hours for inspection
3. cost = tech laborcost
4. rate comes from pricing table based on payment type and service type

*/
select a.ro, sum(flaghours)
from ads.ext_fact_repair_order a
join dds.dim_date b on a.finalclosedatekey = b.date_key
  and b.year_month = 201810
join ads.ext_dim_tech c on a.techkey = c.techkey
  and c.technumber = '631'  
group by a.ro
order by a.ro


select b.*, a.*
from fin.fact_gl a
join fin.dim_Account b on a.account_key = b.account_key
where a.control = '16319919'

-- 11/21/18
-- this gives me all the pieces, but ben wants inventory and jeri says andrew doesn't need this
select a.ro, a.line, a.flaghours, aa.franchise_code, c.name, c.technumber, c.laborcost, 
  d.paymenttypecode, d.paymenttype, f.servicetypecode,
  e.opcode, g.a_b_c_or_d as labor_rate_level, 
  case g.a_b_c_or_d
    when 'A' then h.splr1a
    when 'B' then h.splr1b
    when 'C' then h.splr1c
    when 'D' then h.splr1d
    else -1
  end as labor_rate
from ads.ext_fact_repair_order a
left join arkona.ext_sdprhdr aa on a.ro = aa.ro_number
join ads.ext_dim_tech c on a.techkey = c.techkey
  and c.technumber = '631'  
join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey  
join ads.ext_Dim_opcode e on a.corcodekey = e.opcodekey
  and a.storecode = e.storecode 
join ads.ext_dim_service_type f on a.servicetypekey = f.servicetypekey  
left join arkona.ext_sdplopc g on e.opcode = g.operaction_code 
  and a.storecode = g.company_number
left join arkona.ext_sdpprice h on a.storecode = h.spco_
  and f.servicetypecode = h.spsvctyp
  and d.paymenttypecode = h.splpym
  and aa.franchise_code = h.spfran
where a.ro = '16319919'

need the franchise for pricing: sdprhdr.franchise_code

select * from arkona.ext_sdprhdr where ro_number = '16319919'


the opcode labor rate level

select * from arkona.ext_sdprdet where ptro_ = '16319919'