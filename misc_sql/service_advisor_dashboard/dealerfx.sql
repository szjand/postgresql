﻿maintenance_menu_perc
dmpi_sent_perc

select * from dfx.advisor_checkin_usage_report limit 10

select * from dfx.dynamic_mpi_usage_report limit 10

select * from sad.writer_days where the_date > '08/31/2020'


select a.*, b.push_count, b.numbers_of_maintenance_menus_generated,
  coalesce(b.push_count, 0), coalesce(b.numbers_of_maintenance_menus_generated, 0),
  case
    when coalesce(b.push_count, 0) = 0 then 0
    else round(100.0 * coalesce(b.numbers_of_maintenance_menus_generated, 0)/coalesce(b.push_count, 0), 1)
  end as maintenance_menu_perc
from sad.writer_days a
left join dfx.advisor_checkin_usage_report b on a.writer_name = b.writer_name
  and a.the_Date = b.report_date
where the_date > '08/31/2020'
order by a.writer_name, a.the_date  




select a.*, 
  case
    when coalesce(b.push_count, 0) = 0 then 0
    else round(100.0 * coalesce(b.numbers_of_maintenance_menus_generated, 0)/coalesce(b.push_count, 0), 1)
  end as maintenance_menu_perc
from sad.writer_days a
left join dfx.advisor_checkin_usage_report b on a.writer_name = b.writer_name
  and a.the_date = b.report_date
order by a.the_date  



select a.*, 
  case
    when coalesce(b.mpi_completed_number, 0) = 0 then 0
    else round(100.0 * coalesce(b.dmpi_sent_number, 0)/coalesce(b.mpi_completed_number, 0), 1)
  end as dmpi_perc
from sad.writer_days a
left join dfx.dynamic_mpi_usage_report b on a.writer_name = b.writer_name
  and a.the_date = b.report_date
order by a.the_date  


drop table if exists sad.dfx_reports_daily cascade;
create table sad.dfx_reports_daily (
  writer_name citext not null,
  the_date date not null,
  maintenance_menu_generation_percentage numeric(4,1) default '0',
  dmpi_sent_percentage numeric(4,1) default '0',
  primary key (the_date,writer_name),
  foreign key (the_date,writer_name) references sad.writer_days(the_date,writer_name));
  
insert into sad.dfx_reports_daily
select aa.writer_name, aa.the_date, aa.maintenance_menu_generation_percentage, bb.dmpi_sent_percentage 
from (  
  select a.writer_name, a.the_date, 
    case
      when coalesce(b.push_count, 0) = 0 then 0
      else round(100.0 * coalesce(b.numbers_of_maintenance_menus_generated, 0)/coalesce(b.push_count, 0), 1)
    end as maintenance_menu_generation_percentage
  from sad.writer_days a
  left join dfx.advisor_checkin_usage_report b on a.writer_name = b.writer_name
    and a.the_date = b.report_date) aa
left join (    
  select a.writer_name, a.the_date, 
    case
      when coalesce(b.mpi_completed_number, 0) = 0 then 0
      else round(100.0 * coalesce(b.dmpi_sent_number, 0)/coalesce(b.mpi_completed_number, 0), 1)
    end as dmpi_sent_percentage
  from sad.writer_days a
  left join dfx.dynamic_mpi_usage_report b on a.writer_name = b.writer_name
    and a.the_date = b.report_date) bb on aa.writer_name = bb.writer_name
      and aa.the_date = bb.the_date
where aa.the_date >= current_date - 32
on conflict (the_date,writer_name)
do update
  set (maintenance_menu_generation_percentage, dmpi_sent_percentage)
  = (excluded.maintenance_menu_generation_percentage, excludeddmpi_sent_percentage);

drop table if exists sad.dfx_reports_weekly;
CREATE TABLE sad.dfx_reports_weekly (
  writer_name citext NOT NULL,
  week_id integer NOT NULL,
  week_of date NOT NULL,
  the_week daterange NOT NULL,
  maintenance_menu_generation_percentage numeric(4,1) default '0',
  PRIMARY KEY (week_id, writer_name),
  FOREIGN KEY (week_id, writer_name) REFERENCES sad.writer_weeks (week_id, writer_name));


insert into sad.dfx_reports_weekly
select aa.writer_name, aa.week_id, aa.week_of, aa.the_week, aa.maintenance_menu_generation_percentage, bb.dmpi_sent_percentage 
from ( 
  select a.writer_name, a.week_id, a.week_of, a.the_week,
    case
      when sum(coalesce(b.push_count, 0)) = 0 then 0
      else round(100.0 * sum(coalesce(b.numbers_of_maintenance_menus_generated, 0))/sum(coalesce(b.push_count, 0)), 1)
    end as maintenance_menu_generation_percentage
  from sad.writer_weeks a
  left join dfx.advisor_checkin_usage_report b on a.writer_name = b.writer_name
    and a.the_week @> b.report_date
  group by a.writer_name, a.week_id, a.week_of, a.the_week) aa
left join (    
  select a.writer_name, a.week_id, a.week_of, a.the_week,
    case
      when sum(coalesce(b.mpi_completed_number, 0)) = 0 then 0
      else round(100.0 * sum(coalesce(b.dmpi_sent_number, 0))/sum(coalesce(b.mpi_completed_number, 0)), 1)
    end as dmpi_sent_percentage
  from sad.writer_weeks a
  left join dfx.dynamic_mpi_usage_report b on a.writer_name = b.writer_name
    and a.the_week @> b.report_date
  group by a.writer_name, a.week_id, a.week_of, a.the_week) bb on aa.writer_name = bb.writer_name
      and aa.week_id = bb.week_id order by week_id
on conflict (week_id,writer_name)
do update
  set (maintenance_menu_generation_percentage, dmpi_sent_percentage)
  = (excluded.maintenance_menu_generation_percentage, excludeddmpi_sent_percentage);

select * from dfx.dynamic_mpi_usage_report where report_date between '09/06//2020' and '09/12/2020' order by writer_name, report_date
