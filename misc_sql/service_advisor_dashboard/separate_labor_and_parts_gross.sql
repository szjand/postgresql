﻿-- drop schema sad cascade;
-- create schema sad;

-- select * from sad.writers_mtd;

-- need to separate out labor and parts gross
-- don't run these yet !!!
-- leaving out shop tables for now, does not sound like andrews wants a total comprised of just main shop writers


!!!!!!!!!!!!! done on 9/21/20 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- writers_daily
alter table sad.writers_daily
rename column gross to labor_gross;
alter table sad.writers_daily
add column parts_gross numeric(8,2);
-- writers_monthly
alter table sad.writers_monthly
rename column gross to labor_gross;
alter table sad.writers_monthly
add column parts_gross numeric(8,2);
-- writers_mtd
alter table sad.writers_mtd
rename column gross to labor_gross;
alter table sad.writers_mtd
add column parts_gross numeric(12,2);
-- writers_pay_periodly
alter table sad.writers_pay_periodly
rename column gross to labor_gross;
alter table sad.writers_pay_periodly
add column parts_gross numeric(8,2);
-- writers_pptd
alter table sad.writers_pptd
rename column gross to labor_gross;
alter table sad.writers_pptd
add column parts_gross numeric(12,2);
-- writers_weekly
alter table sad.writers_weekly
rename column gross to labor_gross;
alter table sad.writers_weekly
add column parts_gross numeric(8,2);
-- writers_wtd
alter table sad.writers_wtd
rename column gross to labor_gross;
alter table sad.writers_wtd
add column parts_gross numeric(8,2);

-- ---------------------------------------------------------------------------------
--   the following will be a replacement for the daily,weekly,pay_periodly,monthly & __td data tables
--   in the function sad.dms_data_nightly_update()
--   excludes shop
--   there will need to be an initial run to backfill all the data to jan 2019
--   can't test the __td tables until this is all done, they rely on writers_daily
--   backup schema to linux vm and see if this all runs
-- 1. truncate and populate the tables
-- ---------------------------------------------------------------------------------

-- writers_daily
truncate sad.writers_daily;
insert into sad.writers_daily (writer_name,the_date,ros_opened,labor_sales,parts_sales,discount,labor_gross,parts_gross,
  labor_hours,hours_per_ro,elr,clock_hours,ros_final_closed)
select writer_name, the_date, ros_opened, labor_sales, parts_sales, discount, labor_gross, parts_gross,
  labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed
from (
  select aa.writer_name, aa.the_date, aa.ros_opened, bb.labor_sales, cc.parts_sales, dd.discount, 
    bb.labor_sales - bb.labor_cost  as labor_gross,
    cc.parts_sales - cc.parts_cost as parts_gross,
    ee.labor_hours, ee.hours_per_ro,
    case
      when ee.labor_hours = 0 then 0
      else round(bb.labor_sales/ee.labor_hours, 2) 
    end as elr,
    ff.clock_hours, ee.ros_final_closed
  from (
    select a.writer_name, a.the_date, count(b.ro) as ros_opened
    from sad.writer_days a
    left join sad.ros b on a.writer_name = b.writer_name
      and a.the_date = b.open_date
    group by a.writer_name, a.the_date) aa
  left join (
    select a.writer_name, a.the_date, coalesce(sum(labor_sales), 0) as labor_sales,
      coalesce(sum(labor_cost), 0) as labor_cost
    from sad.writer_days a
    left join sad.labor_details b on a.writer_name = b.writer_name
      and a.the_date = b.the_date
    group by a.writer_name, a.the_date) bb on aa.writer_name = bb.writer_name
    and aa.the_date = bb.the_date
  left join (
    select a.writer_name, a.the_date, coalesce(sum(parts_sales), 0) as parts_sales,
       coalesce(sum(parts_cost), 0) as parts_cost
    from sad.writer_days a
    left join sad.parts_details b on a.writer_name = b.writer_name
      and a.the_date = b.the_date
    group by a.writer_name, a.the_date) cc on aa.writer_name = cc.writer_name
    and aa.the_date = cc.the_date  
  left join (
    select a.writer_name, a.the_date, coalesce(sum(discount), 0) as discount
    from sad.writer_days a
    left join sad.discount_details b on a.writer_name = b.writer_name
      and a.the_date = b.the_date
    group by a.writer_name, a.the_date) dd on aa.writer_name = dd.writer_name
    and aa.the_date = dd.the_date    
  left join (
    select a.writer_name, a.the_date, coalesce(sum(b.ro_flag_hours), 0) as labor_hours,
      count(b.ro) as ros_final_closed,
      case
        when count(b.ro) = 0 then 0
        else round(coalesce(sum(b.ro_flag_hours), 0)/count(b.ro), 2)
      end as hours_per_ro
    from sad.writer_days a
    left join sad.ros b on a.writer_name = b.writer_name
      and a.the_date = b.final_close_date
    group by a.writer_name, a.the_date) ee on aa.writer_name = ee.writer_name
    and aa.the_date = ee.the_date  
  left join (
    select a.writer_name, a.the_date, coalesce(sum(c.clock_hours), 0) as clock_hours
    from sad.writer_days a
    left join sad.writers b on a.writer_name = b.writer_name
    left join arkona.xfm_pypclockin c on b.employee_number = c.employee_number
      and a.the_date = c.the_date
    group by a.writer_name, a.the_date) ff on aa.writer_name = ff.writer_name
    and aa.the_date = ff.the_date) x;
-- where the_date >= current_date - 90    
-- on conflict (the_date,writer_name)
-- do update
--   set (ros_opened, labor_sales, parts_sales, discount, labor_gross,parts_gross,
--     labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed)
--   =
--   (excluded.ros_opened, excluded.labor_sales, excluded.parts_sales, excluded.discount, 
--     excluded.labor_gross, excluded.parts_gross, excluded.labor_hours, excluded.hours_per_ro, 
--     excluded.elr, excluded.clock_hours, excluded.ros_final_closed);

-- writers_weekly
truncate sad.writers_weekly;
insert into sad.writers_weekly (writer_name,week_id, week_of,the_week,ros_opened,labor_sales,parts_sales,discount,labor_gross,parts_gross,
    labor_hours,hours_per_ro,elr,clock_hours,ros_final_closed)
select writer_name, week_id, week_of, the_week, ros_opened, labor_sales, parts_sales, discount, labor_gross, parts_gross,
    labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed
from (    
  select aa.writer_name, aa.week_id, aa.week_of, aa.the_week, aa.ros_opened, bb.labor_sales, cc.parts_sales, dd.discount, 
    bb.labor_sales - bb.labor_cost as labor_gross,
    cc.parts_sales - cc.parts_cost as parts_gross,
    ee.labor_hours, ee.hours_per_ro,
    case
      when ee.labor_hours = 0 then 0
      else round(bb.labor_sales/ee.labor_hours, 2) 
    end as elr, ff.clock_hours, ee.ros_final_closed 
  from (
    select a.writer_name, a.week_id, a.week_of, a.the_week, count(b.ro) as ros_opened
    from sad.writer_weeks a
    left join sad.ros b on a.writer_name = b.writer_name
      and a.the_week @> b.open_date
    group by a.writer_name, a.week_id, a.week_of, a.the_week) aa
  left join (
    select a.writer_name, a.week_id, a.week_of, a.the_week, coalesce(sum(labor_sales), 0) as labor_sales,
      coalesce(sum(labor_cost), 0) as labor_cost
    from sad.writer_weeks a
    left join sad.labor_details b on a.writer_name = b.writer_name
      and a.the_week @> b.the_date
    group by a.writer_name, a.week_id, a.week_of, a.the_week) bb on aa.writer_name = bb.writer_name
    and aa.week_id = bb.week_id
  left join (
    select a.writer_name, a.week_id, a.week_of, a.the_week, coalesce(sum(parts_sales), 0) as parts_sales,
      coalesce(sum(parts_cost), 0) as parts_cost
    from sad.writer_weeks a
    left join sad.parts_details b on a.writer_name = b.writer_name
      and a.the_week @> b.the_date
    group by a.writer_name, a.week_id, a.week_of, a.the_week) cc on aa.writer_name = cc.writer_name
    and aa.week_id = cc.week_id    
  left join (
    select a.writer_name, a.week_id, a.week_of, a.the_week, coalesce(sum(discount), 0) as discount
    from sad.writer_weeks a
    left join sad.discount_details b on a.writer_name = b.writer_name
      and a.the_week @> b.the_date
    group by a.writer_name, a.week_id, a.week_of, a.the_week) dd on aa.writer_name = dd.writer_name
    and aa.week_id = dd.week_id     
  left join (
    select a.writer_name, a.week_id, a.week_of, a.the_week, coalesce(sum(b.ro_flag_hours), 0) as labor_hours,
      count(b.ro) as ros_final_closed,
      case
        when count(b.ro) = 0 then 0
        else round(coalesce(sum(b.ro_flag_hours), 0)/count(b.ro), 2)
      end as hours_per_ro
      from sad.writer_weeks a
      left join sad.ros b on a.writer_name = b.writer_name
        and a.the_week @> b.final_close_date
      group by a.writer_name, a.week_id, a.week_of, a.the_week) ee on aa.writer_name = ee.writer_name
        and aa.week_id = ee.week_id
  left join (
    select a.writer_name, a.week_id, a.week_of, a.the_week, coalesce(sum(c.clock_hours), 0) as clock_hours
    from sad.writer_weeks a
    left join sad.writers b on a.writer_name = b.writer_name
    left join arkona.xfm_pypclockin c on b.employee_number = c.employee_number
      and a.the_week @> c.the_date
    group by a.writer_name, a.week_id, a.week_of, a.the_week) ff on aa.writer_name = ff.writer_name
      and aa.week_id = ff.week_id) x;
-- on conflict (week_id,writer_name)
-- do update
--     set(week_of, the_week, ros_opened, labor_sales, parts_sales, discount, labor_gross, parts_gross,
--       labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed)
--     =
--     (excluded.week_of, excluded.the_week, excluded.ros_opened, excluded.labor_sales, 
--       excluded.parts_sales, excluded.discount, excluded.labor_gross, excluded.parts_gross, excluded.labor_hours, 
--       excluded.hours_per_ro, excluded.elr, excluded.clock_hours, excluded.ros_final_closed);

-- writers_pay_periodly
truncate sad.writers_pay_periodly;
insert into sad.writers_pay_periodly (writer_name,pay_period_id,from_date,thru_date,the_pay_period, 
  ros_opened,labor_sales,parts_sales,discount,labor_gross,parts_gross,
  labor_hours,hours_per_ro,elr,clock_hours,ros_final_closed)
select writer_name, pay_period_id, from_date, thru_date, the_pay_period, 
  ros_opened, labor_sales, parts_sales, discount, labor_gross, parts_gross,
  labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed
from (  
  select aa.writer_name, aa.pay_period_id, aa.from_date, aa.thru_date, aa.the_pay_period, 
    aa.ros_opened, bb.labor_sales, cc.parts_sales, dd.discount, 
    bb.labor_sales - bb.labor_cost as labor_gross,
    cc.parts_sales - cc.parts_cost as parts_gross,
    ee.labor_hours, ee.hours_per_ro,
    case
      when ee.labor_hours = 0 then 0
      else round(bb.labor_sales/ee.labor_hours, 2) 
    end as elr, ff.clock_hours, ee.ros_final_closed 
  from (
    select a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, count(b.ro) as ros_opened
    from sad.writer_pay_periods a
    left join sad.ros b on a.writer_name = b.writer_name
      and a.the_pay_period @> b.open_date
    group by a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) aa
  left join (
    select a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, coalesce(sum(labor_sales), 0) as labor_sales,
      coalesce(sum(labor_cost), 0) as labor_cost
    from sad.writer_pay_periods a
    left join sad.labor_details b on a.writer_name = b.writer_name
      and a.the_pay_period @> b.the_date
    group by a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) bb on aa.writer_name = bb.writer_name
    and aa.pay_period_id = bb.pay_period_id
  left join (
    select a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, coalesce(sum(parts_sales), 0) as parts_sales,
      coalesce(sum(parts_cost), 0) as parts_cost
    from sad.writer_pay_periods a
    left join sad.parts_details b on a.writer_name = b.writer_name
      and a.the_pay_period @> b.the_date
    group by a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) cc on aa.writer_name = cc.writer_name
    and aa.pay_period_id = cc.pay_period_id    
  left join (
    select a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, coalesce(sum(discount), 0) as discount
    from sad.writer_pay_periods a
    left join sad.discount_details b on a.writer_name = b.writer_name
      and a.the_pay_period @> b.the_date
    group by a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) dd on aa.writer_name = dd.writer_name
    and aa.pay_period_id = dd.pay_period_id     
  left join (
    select a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, coalesce(sum(b.ro_flag_hours), 0) as labor_hours,
      count(b.ro) as ros_final_closed,
      case
        when count(b.ro) = 0 then 0
        else round(coalesce(sum(b.ro_flag_hours), 0)/count(b.ro), 2)
      end as hours_per_ro
      from sad.writer_pay_periods a
      left join sad.ros b on a.writer_name = b.writer_name
        and a.the_pay_period @> b.final_close_date
      group by a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) ee on aa.writer_name = ee.writer_name
        and aa.pay_period_id = ee.pay_period_id
  left join (
    select a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, coalesce(sum(c.clock_hours), 0) as clock_hours
    from sad.writer_pay_periods a
    left join sad.writers b on a.writer_name = b.writer_name
    left join arkona.xfm_pypclockin c on b.employee_number = c.employee_number
      and a.the_pay_period @> c.the_date
    group by a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) ff on aa.writer_name = ff.writer_name
      and aa.pay_period_id = ff.pay_period_id) x;     
-- on conflict (writer_name,pay_period_id)
-- do update
--   set (from_date, thru_date, the_pay_period, 
--     ros_opened, labor_sales, parts_sales, discount, labor_gross, parts_gross,
--     labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed) 
--   =
--   (excluded.from_date, excluded.thru_date, excluded.the_pay_period, 
--     excluded.ros_opened, excluded.labor_sales, excluded.parts_sales, excluded.discount, 
--     excluded.labor_gross, excluded.parts_gross, excluded.labor_hours, 
--     excluded.hours_per_ro, excluded.elr, excluded.clock_hours, excluded.ros_final_closed);

-- writers_monthly    
truncate sad.writers_monthly;
insert into sad.writers_monthly (writer_name,year_month,the_month,month_name, 
  ros_opened,labor_sales,parts_sales,discount,labor_gross,parts_gross,
  labor_hours,hours_per_ro,elr,clock_hours,ros_final_closed)
select writer_name, year_month, the_month, month_name, 
  ros_opened, labor_sales, parts_sales, discount, labor_gross, parts_gross,
  labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed
from (
  select aa.writer_name, aa.year_month, aa.the_month, aa.month_name, aa.ros_opened, 
    bb.labor_sales, cc.parts_sales, dd.discount, 
    bb.labor_sales - bb.labor_cost as labor_gross,
    cc.parts_sales - cc.parts_cost as parts_gross,
    ee.labor_hours, ee.hours_per_ro,
    case
      when ee.labor_hours = 0 then 0
      else round(bb.labor_sales/ee.labor_hours, 2) 
    end as elr, ff.clock_hours, ee.ros_final_closed 
  from (
    select a.writer_name, a.year_month, a.the_month, a.month_name, count(b.ro) as ros_opened
    from sad.writer_months a
    left join sad.ros b on a.the_month @> b.open_date and a.writer_name = b.writer_name
    group by a.writer_name, a.year_month, a.the_month, a.month_name) aa
  left join (
    select a.writer_name, a.year_month, a.the_month, a.month_name, coalesce(sum(labor_sales), 0) as labor_sales,
      coalesce(sum(labor_cost), 0) as labor_cost
    from sad.writer_months a
    left join sad.labor_details b on a.the_month @> b.the_date and a.writer_name = b.writer_name
    group by a.writer_name, a.year_month, a.the_month, a.month_name) bb on aa.writer_name = bb.writer_name and aa.year_month = bb.year_month
  left join (
    select a.writer_name, a.year_month, a.the_month, a.month_name, coalesce(sum(parts_sales), 0) as parts_sales,
      coalesce(sum(parts_cost), 0) as parts_cost
    from sad.writer_months a
    left join sad.parts_details b on a.the_month @> b.the_date and a.writer_name = b.writer_name
    group by a.writer_name, a.year_month, a.the_month, a.month_name) cc on aa.writer_name = cc.writer_name and aa.year_month = cc.year_month    
  left join (
    select a.writer_name, a.year_month, a.the_month, a.month_name, coalesce(sum(discount), 0) as discount
    from sad.writer_months a
    left join sad.discount_details b on a.the_month @> b.the_date and a.writer_name = b.writer_name
    group by a.writer_name, a.year_month, a.the_month, a.month_name) dd on aa.writer_name = dd.writer_name and aa.year_month = dd.year_month     
  left join (
    select a.writer_name, a.year_month, a.the_month, a.month_name, coalesce(sum(b.ro_flag_hours), 0) as labor_hours,
      count(b.ro) as ros_final_closed,
      case
        when count(b.ro) = 0 then 0
        else round(coalesce(sum(b.ro_flag_hours), 0)/count(b.ro), 2)
      end as hours_per_ro
      from sad.writer_months a
      left join sad.ros b on a.the_month @> b.final_close_date and a.writer_name = b.writer_name
      group by a.writer_name, a.year_month, a.the_month, a.month_name) ee on aa.writer_name = ee.writer_name and aa.year_month = ee.year_month
  left join (
    select a.writer_name, a.year_month, a.the_month, a.month_name, coalesce(sum(c.clock_hours), 0) as clock_hours
    from sad.writer_months a
    left join sad.writers b on a.writer_name = b.writer_name
    left join arkona.xfm_pypclockin c on b.employee_number = c.employee_number
      and a.the_month @> c.the_date
    group by a.writer_name, a.year_month, a.the_month, a.month_name) ff on aa.writer_name = ff.writer_name and aa.year_month = ff.year_month) x;
-- on conflict (year_month,writer_name)
-- do update
--   set (the_month, month_name, ros_opened, labor_sales, parts_sales, discount, labor_gross, parts_gross,
--     labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed)
--   =    
--   (excluded.the_month, excluded.month_name, excluded.ros_opened, excluded.labor_sales, excluded.parts_sales, 
--     excluded.discount, excluded.labor_gross, excluded.parts_gross,
--     excluded.labor_hours, excluded.hours_per_ro, excluded.elr, excluded.clock_hours, excluded.ros_final_closed);

-- writers_wtd    
truncate sad.writers_wtd;
insert into sad.writers_wtd (the_date,week_id,week_of,writer_name,ros_opened,labor_sales,parts_sales, 
  discount,labor_gross,parts_gross,
  labor_hours,hours_per_ro,elr,clock_hours,ros_final_closed)
select the_date, week_id, week_of, writer_name, ros_opened, labor_sales, parts_sales, 
  discount, labor_gross, parts_gross,
  labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed
from (    
  select a.the_date, b.week_id, week_of, a.writer_name, 
    sum(a.ros_opened) over (partition by b.week_id, a.writer_name order by a.the_date asc) as ros_opened,
    sum(a.labor_sales) over (partition by b.week_id, a.writer_name order by a.the_date asc) as labor_sales,
    sum(a.parts_sales) over (partition by b.week_id, a.writer_name order by a.the_date asc) as parts_sales,
    sum(a.discount) over (partition by b.week_id, a.writer_name order by a.the_date asc) as discount,
    sum(a.labor_gross) over (partition by b.week_id, a.writer_name order by a.the_date asc) as labor_gross,
    sum(a.parts_gross) over (partition by b.week_id, a.writer_name order by a.the_date asc) as parts_gross,
    sum(a.labor_hours) over (partition by b.week_id, a.writer_name order by a.the_date asc) as labor_hours,
    case
      when sum(a.ros_final_closed) over (partition by b.week_id, a.writer_name order by a.the_date asc) = 0 then 0
      else 
        round(
          (sum(a.labor_hours) over (partition by b.week_id, a.writer_name order by a.the_date asc)
          /
          sum(a.ros_final_closed) over (partition by b.week_id, a.writer_name order by a.the_date asc)), 2) 
      end as hours_per_ro,
    case
      when sum(a.labor_hours) over (partition by b.week_id, a.writer_name order by a.the_date asc) = 0 then 0
      else
        round(
          (sum(a.labor_sales) over (partition by b.week_id, a.writer_name order by a.the_date asc)
          /
          sum(a.labor_hours) over (partition by b.week_id, a.writer_name order by a.the_date asc)), 2) 
      end as elr,
    sum(a.clock_hours) over (partition by b.week_id, a.writer_name order by a.the_date asc) as clock_hours,
    sum(a.ros_final_closed) over (partition by b.week_id, a.writer_name order by a.the_date asc) as ros_final_closed
  from sad.writers_daily a
  join sad.writer_weeks b on b.the_week @> a.the_date 
    and a.writer_name = b.writer_name
  order by a.writer_name, the_date) x;
-- on conflict(the_date,writer_name)
-- do update
--   set (week_id, week_of, ros_opened, labor_sales, parts_sales, discount, labor_gross, parts_gross,
--     labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed)
--   =
--   (excluded.week_id, excluded.week_of, excluded.ros_opened, excluded.labor_sales, 
--     excluded.parts_sales, excluded.discount, excluded.labor_gross, excluded.parts_gross,
--     excluded.labor_hours, excluded.hours_per_ro, excluded.elr, excluded.clock_hours, excluded.ros_final_closed);

-- writers_pptd   
truncate sad.writers_pptd;
insert into sad.writers_pptd (the_date, pay_period_id, from_date, thru_date, writer_name, 
  ros_opened, labor_sales, parts_sales, discount, labor_gross, parts_gross, labor_hours, 
  hours_per_ro, elr, clock_hours, ros_final_closed)
select the_date, pay_period_id, from_date, thru_date, writer_name, ros_opened, labor_sales, parts_sales, 
  discount, labor_gross, parts_gross, labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed
from (  
  select a.the_date, pay_period_id, from_date, thru_date, a.writer_name, 
    sum(a.ros_opened) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) as ros_opened,
    sum(a.labor_sales) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) as labor_sales,
    sum(a.parts_sales) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) as parts_sales,
    sum(a.discount) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) as discount,
    sum(a.labor_gross) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) as labor_gross,
    sum(a.parts_gross) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) as parts_gross,
    sum(a.labor_hours) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) as labor_hours,
    case
      when sum(a.ros_final_closed) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) = 0 then 0
      else 
        round(
          (sum(a.labor_hours) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc)
          /
          sum(a.ros_final_closed) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc)), 2) 
      end as hours_per_ro,
    case
      when sum(a.labor_hours) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) = 0 then 0
      else
        round(
          (sum(a.labor_sales) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc)
          /
          sum(a.labor_hours) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc)), 2) 
      end as elr,
    sum(a.clock_hours) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) as clock_hours,
    sum(a.ros_final_closed) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) as ros_final_closed
  from sad.writers_daily a
  join sad.writer_pay_periods b on b.the_pay_period @> a.the_date 
    and a.writer_name = b.writer_name
  order by a.writer_name, the_date) x;
-- on conflict(writer_name,the_date)
-- do update
--   set (pay_period_id, from_date, thru_date, ros_opened, labor_sales, parts_sales, 
--     discount, labor_gross, parts_gross, labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed)
--   =
--   (excluded.pay_period_id, excluded.from_date, excluded.thru_date, excluded.ros_opened, excluded.labor_sales, 
--     excluded.parts_sales, excluded.discount, excluded.labor_gross, excluded.parts_gross,
--     excluded.labor_hours, excluded.hours_per_ro, excluded.elr, excluded.clock_hours, excluded.ros_final_closed);  

-- writers_mtd    
truncate sad.writers_mtd;
insert into sad.writers_mtd (the_date, year_month, month_name, writer_name,
  ros_opened, labor_sales, parts_sales, discount, labor_gross, parts_gross,
  labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed)
select the_date, year_month, month_name, writer_name,
  ros_opened, labor_sales, parts_sales, discount, labor_gross, parts_gross,
  labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed
from (  
  select a.the_date, year_month, month_name, a.writer_name, 
    sum(a.ros_opened) over (partition by b.year_month, a.writer_name order by a.the_date asc) as ros_opened,
    sum(a.labor_sales) over (partition by b.year_month, a.writer_name order by a.the_date asc) as labor_sales,
    sum(a.parts_sales) over (partition by b.year_month, a.writer_name order by a.the_date asc) as parts_sales,
    sum(a.discount) over (partition by b.year_month, a.writer_name order by a.the_date asc) as discount,
    sum(a.labor_gross) over (partition by b.year_month, a.writer_name order by a.the_date asc) as labor_gross,
    sum(a.parts_gross) over (partition by b.year_month, a.writer_name order by a.the_date asc) as parts_gross,
    sum(a.labor_hours) over (partition by b.year_month, a.writer_name order by a.the_date asc) as labor_hours,
    case
      when sum(a.ros_final_closed) over (partition by b.year_month, a.writer_name order by a.the_date asc) = 0 then 0
      else 
        round(
          (sum(a.labor_hours) over (partition by b.year_month, a.writer_name order by a.the_date asc)
          /
          sum(a.ros_final_closed) over (partition by b.year_month, a.writer_name order by a.the_date asc)), 2) 
      end as hours_per_ro,
    case
      when sum(a.labor_hours) over (partition by b.year_month, a.writer_name order by a.the_date asc) = 0 then 0
      else
        round(
          (sum(a.labor_sales) over (partition by b.year_month, a.writer_name order by a.the_date asc)
          /
          sum(a.labor_hours) over (partition by b.year_month, a.writer_name order by a.the_date asc)), 2) 
      end as elr,
    sum(a.clock_hours) over (partition by b.year_month, a.writer_name order by a.the_date asc) as clock_hours,
    sum(a.ros_final_closed) over (partition by b.year_month, a.writer_name order by a.the_date asc) as ros_final_closed
  from sad.writers_daily a
  join sad.writer_months b on b.the_month @> a.the_date 
    and a.writer_name = b.writer_name
  order by a.writer_name, the_date) x;
-- on conflict(writer_name,the_date)
-- do update  
--   set (year_month, month_name, 
--     ros_opened, labor_sales, parts_sales, discount, labor_gross, parts_gross
--     labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed)
--   =
--   (excluded.year_month, excluded.month_name, 
--     excluded.ros_opened, excluded.labor_sales, excluded.parts_sales, excluded.discount, 
--     excluded.labor_gross, excluded.parts_gross, excluded.labor_hours, excluded.hours_per_ro, 
--     excluded.elr, excluded.clock_hours, excluded.ros_final_closed); 

