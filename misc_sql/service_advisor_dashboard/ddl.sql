﻿-- create schema sad;
-- comment on schema sad is 'schema for service advisor dashboard data';

-- select * from sad.writer_weeks order by week_id desc
-----------------------------------------------------------------
-----------------------------------------------------------------
drop table if exists sad.writers cascade;
create table sad.writers (
  writer_name citext primary key,
  writer_number citext [],
  employee_number citext not null, 
  service_writer_key integer []);
create unique index on sad.writers(writer_number);
create unique index on sad.writers(employee_number);  
create index on sad.writers using gin(service_writer_key);
insert into sad.writers
select writer_name, array_agg(distinct writer_number), employee_number, array_agg(service_writer_key)
from dds.dim_service_writer 
where writer_name in ('EULISS, NED','TROFTGRUBEN, RODNEY','MARO, NICHOLAS','JANKOWSKI, GARRETT',
  'GOTHBERG, CONNER J','RODRIGUEZ, JUSTIN','CARLSON, KENNETH A')
group by writer_name, employee_number;   
comment on table sad.writers is 'due to the lack of a reliable hr maintenance routine, this table will now and forever
be based on a hard coded list of writers, but will need to be refreshed nightly due to dependence on each writer
possibly having multiple valid service_writer_key values which is the link to dds.fact_repair_order';
-----------------------------------------------------------------
-----------------------------------------------------------------
drop table if exists sad.ros cascade;
create table sad.ros (
  ro citext primary key,
  ro_labor_sales numeric(8,2) not null default '0',
  ro_flag_hours numeric(8,2) not null default '0',
  ro_discount numeric(8,2) not null default '0',
  writer_name citext not null references sad.writers(writer_name),
--   writer_number citext not null,
  open_date date not null,
  close_date date not null,
  final_close_date date not null);
create index on sad.ros(writer_name); 
create index on sad.ros(open_date); 
create index on sad.ros(close_date); 
create index on sad.ros(final_close_date); 
insert into sad.ros
select a.ro, a.ro_labor_sales, a.ro_flag_hours, a.ro_discount, b.writer_name, -- b.writer_number, 
  open_date, close_date, final_close_date
from dds.fact_repair_order a
join sad.writers b on a.service_writer_key = any(b.service_writer_key)
where a.final_close_date >= '01/01/2019'
group by a.ro, a.ro_labor_sales, a.ro_flag_hours, a.ro_discount, b.writer_name, b.writer_number,
  open_date, close_date, final_close_date;
comment on table sad.ros is 'base ro information derived from dds.fact_repair_order for service writers in sad.writers';  
-----------------------------------------------------------------
-----------------------------------------------------------------
drop table if exists sad.labor_accounts cascade;
create table sad.labor_accounts (
  account citext primary key,
  account_key integer not null,
  description citext not null, 
  department citext not null,
  account_type citext not null);
create unique index on sad.labor_accounts(account_key);
insert into sad.labor_accounts
select e.account, e.account_key, e.description, e.department, e.account_type
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.page = 16 
  and b.line between 20 and 28
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.area = 'fixed'
join fin.dim_fs_account d on a.fs_Account_key = d.fs_account_key
  and d.current_row
join fin.dim_account e on d.gl_account = e.account
  and e.current_row
  and e.description not like '%discount%'
where c.store = 'RY1'
group by e.account, e.account_key, e.description, e.department, e.account_type; 
comment on table sad.labor_accounts is 'labor sale and cost accounts derived from fs routing';
-----------------------------------------------------------------
-----------------------------------------------------------------
drop table if exists sad.parts_accounts cascade;
create table sad.parts_accounts (
  account citext primary key,
  account_key integer not null,
  description citext not null, 
  department citext not null,
  account_type citext not null);
create unique index on sad.parts_accounts(account_key);
insert into sad.parts_accounts
select e.account, e.account_key, e.description, e.department, e.account_type
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.page = 16 
  and b.line between 45 and 60
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.area = 'fixed'
join fin.dim_fs_account d on a.fs_Account_key = d.fs_account_key
  and d.current_row
join fin.dim_account e on d.gl_account = e.account
  and e.current_row
  and e.description not like '%discount%'
where c.store = 'RY1'
group by e.account, e.account_key, e.description, e.department, e.account_type;
comment on table sad.parts_accounts is 'parts sale and cost accounts derived from fs routing'; 

-- 09/21/ additional accounts that apply, brought to my attention by the sa pay plan doc from jeri
-- adds: 146401, 146604, 146901
insert into sad.parts_accounts
select e.account, e.account_key, e.description, e.department, e.account_type
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.page = 16 
--   and b.line between 20 and 28
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.area = 'fixed'
join fin.dim_fs_account d on a.fs_Account_key = d.fs_account_key
  and d.current_row
  and gl_account in  ('146401','146901','146604')
join fin.dim_account e on d.gl_account = e.account
  and e.current_row
group by e.account, e.account_key, e.description, e.department, e.account_type
-----------------------------------------------------------------
-----------------------------------------------------------------
drop table if exists sad.discount_accounts cascade;
create table sad.discount_accounts (
  account citext primary key,
  account_key integer not null,
  description citext not null, 
  department citext not null);
create unique index on sad.discount_accounts(account_key);
insert into sad.discount_accounts
select a.account, a.account_key, a.description, 
  case
    when a.department = 'Parts' then 'Parts'
    else 'Labor'
  end as department
from fin.dim_account a
join fin.dim_fs_account b on a.account = b.gl_account
join fin.fact_fs c on b.fs_account_key = c.fs_account_key
join fin.dim_fs d on c.fs_key = d.fs_key
join fin.dim_fs_org e on c.fs_org_key = e.fs_org_key  
  and e.area = 'fixed'  
where description like '%discount%'
  and a.store_code = 'RY1'
group by a.account, a.account_key, a.description, a.department;
-----------------------------------------------------------------
-----------------------------------------------------------------
drop table if exists sad.labor_details cascade;
create table sad.labor_details (
  ro citext primary key references sad.ros(ro),
  writer_name citext not null,
  the_date date not null,
  labor_sales numeric(8,2) not null default '0',
  labor_cost numeric(8,2) not null default '0');
create index on sad.labor_details(writer_name);
create index on sad.labor_details(the_date);  
insert into sad.labor_details 
select ro, writer_name, c.final_close_date, 
  coalesce(sum(-a.amount) filter (where b.account_type = 'Sale'), 0) as labor_sales,
  coalesce(sum(a.amount) filter (where b.account_type = 'COGS'), 0) as labor_cost
from fin.fact_gl a
join sad.labor_accounts b on a.account_key = b.account_key
join sad.ros c on a.control = c.ro
where a.post_Status = 'Y'
group by c.ro, c.final_close_date, c.writer_name;  
comment on table sad.labor_details is 'total labor sales & cost from accounting on ros with a final close date, grain is ro';
-----------------------------------------------------------------
-----------------------------------------------------------------
drop table if exists sad.parts_details cascade;
create table sad.parts_details (
  ro citext primary key references sad.ros(ro),
  writer_name citext not null,
  the_date date not null,
  parts_sales numeric(8,2) not null default '0',
  parts_cost numeric(8,2) not null default '0');
create index on sad.parts_details(writer_name);
create index on sad.parts_details(the_date);  
insert into sad.parts_details 
select ro, writer_name, c.final_close_date, 
  coalesce(sum(-a.amount) filter (where b.account_type = 'Sale'), 0) as parts_sales,
  coalesce(sum(a.amount) filter (where b.account_type = 'COGS'), 0) as parts_cost
from fin.fact_gl a
join sad.parts_accounts b on a.account_key = b.account_key
join sad.ros c on a.control = c.ro
  and c.final_close_date between '01/01/2019' and current_date
where a.post_Status = 'Y'
group by c.ro, c.final_close_date, c.writer_name;
comment on table sad.parts_details is 'total parts sales & cost from accounting on ros with a final close date, grain is ro';
-----------------------------------------------------------------
-----------------------------------------------------------------
drop table if exists sad.discount_details cascade;
create table sad.discount_details (
  ro citext primary key references sad.ros(ro),
  writer_name citext not null,
  the_date date not null,
  discount numeric (8,2) not null);
create index on sad.discount_details(writer_name);
create index on sad.discount_details(the_date);  
insert into sad.discount_details 
select ro, writer_name, c.final_close_date, sum(a.amount) as labor_sales 
from fin.fact_gl a
join sad.discount_accounts b on a.account_key = b.account_key
join sad.ros c on a.control = c.ro
  and c.final_close_date between '01/01/2019' and current_date
where a.post_Status = 'Y'
group by c.ro, c.final_close_date, c.writer_name;
comment on table sad.discount_details is 'total discounts from accounting on ros with a final close date, grain is ro';
-----------------------------------------------------------------
-----------------------------------------------------------------
drop table if exists sad.writer_days cascade;
create table sad.writer_days (
  the_date date not null,
  writer_name citext not null,
  primary key(the_date,writer_name));
insert into sad.writer_days
select a.the_date, b.writer_name
from dds.dim_date a, sad.writers b
where a.the_year >= 2019
  and a.the_date < current_Date
group by a.the_date, b.writer_name;  
-----------------------------------------------------------------
drop table if exists sad.writers_daily cascade;
create table sad.writers_daily (
  writer_name citext not null,
  the_date date not null,
  ros_opened integer not null,
  labor_sales numeric(8,2) not null,
  parts_sales Numeric(8,2) not null,
  discount numeric(8,2) not null,
  gross numeric(8,2) not null,
  labor_hours numeric(5,2) not null,
  hours_per_ro numeric(5,2) not null,
  elr numeric(5,2) not null,
  clock_hours numeric(5,2) not null,
  ros_final_closed integer not null,
  primary key(the_date,writer_name),  
  foreign key(the_date,writer_name)  references sad.writer_days(the_date,writer_name));
-----------------------------------------------------------------
insert into sad.writers_daily
select aa.writer_name, aa.the_date, aa.ros_opened, bb.labor_sales, cc.parts_sales, dd.discount, 
  bb.labor_sales + cc.parts_sales - bb.labor_cost - cc.parts_cost - dd.discount as gross,
  ee.labor_hours, ee.hours_per_ro,
  case
    when ee.labor_hours = 0 then 0
    else round(bb.labor_sales/ee.labor_hours, 2) 
  end as elr,
  ff.clock_hours, ee.ros_final_closed
from (
  select a.writer_name, a.the_date, count(b.ro) as ros_opened
  from sad.writer_days a
  left join sad.ros b on a.writer_name = b.writer_name
    and a.the_date = b.open_date
  group by a.writer_name, a.the_date) aa
left join (
  select a.writer_name, a.the_date, coalesce(sum(labor_sales), 0) as labor_sales,
    coalesce(sum(labor_cost), 0) as labor_cost
  from sad.writer_days a
  left join sad.labor_details b on a.writer_name = b.writer_name
    and a.the_date = b.the_date
  group by a.writer_name, a.the_date) bb on aa.writer_name = bb.writer_name
  and aa.the_date = bb.the_date
left join (
  select a.writer_name, a.the_date, coalesce(sum(parts_sales), 0) as parts_sales,
     coalesce(sum(parts_cost), 0) as parts_cost
  from sad.writer_days a
  left join sad.parts_details b on a.writer_name = b.writer_name
    and a.the_date = b.the_date
  group by a.writer_name, a.the_date) cc on aa.writer_name = cc.writer_name
  and aa.the_date = cc.the_date  
left join (
  select a.writer_name, a.the_date, coalesce(sum(discount), 0) as discount
  from sad.writer_days a
  left join sad.discount_details b on a.writer_name = b.writer_name
    and a.the_date = b.the_date
  group by a.writer_name, a.the_date) dd on aa.writer_name = dd.writer_name
  and aa.the_date = dd.the_date    
left join (
  select a.writer_name, a.the_date, coalesce(sum(b.ro_flag_hours), 0) as labor_hours,
    count(b.ro) as ros_final_closed,
    case
      when count(b.ro) = 0 then 0
      else round(coalesce(sum(b.ro_flag_hours), 0)/count(b.ro), 2)
    end as hours_per_ro
  from sad.writer_days a
  left join sad.ros b on a.writer_name = b.writer_name
    and a.the_date = b.final_close_date
  group by a.writer_name, a.the_date) ee on aa.writer_name = ee.writer_name
  and aa.the_date = ee.the_date  
left join (
  select a.writer_name, a.the_date, coalesce(sum(c.clock_hours), 0) as clock_hours
  from sad.writer_days a
  left join sad.writers b on a.writer_name = b.writer_name
  left join arkona.xfm_pypclockin c on b.employee_number = c.employee_number
    and a.the_date = c.the_date
  group by a.writer_name, a.the_date) ff on aa.writer_name = ff.writer_name
  and aa.the_date = ff.the_date;
-----------------------------------------------------------------
drop table if exists sad.shop_daily cascade;
create table sad.shop_daily (
  the_date date not null primary key,
  ros_opened integer not null,
  labor_sales numeric(8,2) not null,
  parts_sales Numeric(8,2) not null,
  discount numeric(8,2) not null,
  gross numeric(8,2) not null,
  labor_hours numeric(8,2) not null,
  hours_per_ro numeric(5,2) not null,
  elr numeric(5,2) not null,
  clock_hours numeric(5,2) not null,
  ros_final_closed integer not null);
-----------------------------------------------------------------
insert into sad.shop_daily
select aa.the_date, aa.ros_opened, bb.labor_sales, cc.parts_sales, dd.discount, 
  bb.labor_sales + cc.parts_sales - bb.labor_cost - cc.parts_cost - dd.discount as gross,
  ee.labor_hours, ee.hours_per_ro,
  case
    when ee.labor_hours = 0 then 0
    else round(bb.labor_sales/ee.labor_hours, 2) 
  end as elr,
  ff.clock_hours, ee.ros_final_closed
from (
  select a.the_date, count(b.ro) as ros_opened
  from sad.writer_days a
  left join sad.ros b on a.the_date = b.open_date
  group by a.the_date) aa
left join (
  select a.the_date, coalesce(sum(labor_sales), 0) as labor_sales,
    coalesce(sum(labor_cost), 0) as labor_cost
  from sad.writer_days a
  left join sad.labor_details b on a.the_date = b.the_date
  group by a.the_date) bb on aa.the_date = bb.the_date
left join (
  select a.the_date, coalesce(sum(parts_sales), 0) as parts_sales,
     coalesce(sum(parts_cost), 0) as parts_cost
  from sad.writer_days a
  left join sad.parts_details b on a.the_date = b.the_date
  group by a.the_date) cc on aa.the_date = cc.the_date  
left join (
  select a.the_date, coalesce(sum(discount), 0) as discount
  from sad.writer_days a
  left join sad.discount_details b on a.the_date = b.the_date
  group by a.the_date) dd on aa.the_date = dd.the_date    
left join (
  select a.the_date, coalesce(sum(b.ro_flag_hours), 0) as labor_hours,
    count(b.ro) as ros_final_closed,
    case
      when count(b.ro) = 0 then 0
      else round(coalesce(sum(b.ro_flag_hours), 0)/count(b.ro), 2)
    end as hours_per_ro
  from sad.writer_days a
  left join sad.ros b on a.the_date = b.final_close_date
  group by a.the_date) ee on aa.the_date = ee.the_date  
left join (
  select a.the_date, coalesce(sum(c.clock_hours), 0) as clock_hours
  from sad.writer_days a
  left join sad.writers b on a.writer_name = b.writer_name
  left join arkona.xfm_pypclockin c on b.employee_number = c.employee_number
    and a.the_date = c.the_date
  group by a.the_date) ff on aa.the_date = ff.the_date;  
-----------------------------------------------------------------
-----------------------------------------------------------------
drop table if exists sad.writer_weeks cascade;
create table sad.writer_weeks (
  week_id integer not null,
  week_of date not null,
  the_week daterange not null,
  writer_name citext not null,
  primary key(week_id,writer_name));
create index on sad.writer_weeks(week_of); 
create index on sad.writer_weeks using gist(the_week); 
insert into sad.writer_weeks
select aa.*, bb.writer_name
from (
  select a.sunday_to_saturday_week as week_id, min(a.the_date) as week_of, 
    daterange(min(a.the_date), max(the_date), '[]') as the_week
  from dds.dim_date a
  join (
    select distinct sunday_to_saturday_week 
    from dds.dim_date
    where the_date between '01/01/2019' and current_date) b on a.sunday_to_saturday_week = b.sunday_to_saturday_week
  group by a.sunday_to_saturday_week) aa, sad.writers bb;    
-----------------------------------------------------------------
drop table if exists sad.writers_weekly cascade;
create table sad.writers_weekly (
  writer_name citext not null,
  week_id integer not null,
  week_of date not null,
  the_week daterange not null,
  ros_opened integer not null,
  labor_sales numeric(8,2) not null,
  parts_sales Numeric(8,2) not null,
  discount numeric(8,2) not null,
  gross numeric(8,2) not null,
  labor_hours numeric(5,2) not null,
  hours_per_ro numeric(5,2) not null,
  elr numeric(5,2) not null,
  clock_hours numeric(5,2) not null,
  ros_final_closed integer not null,
  primary key(week_id,writer_name),  
  foreign key(week_id,writer_name)  references sad.writer_weeks(week_id,writer_name));  
-----------------------------------------------------------------
insert into sad.writers_weekly  
select aa.writer_name, aa.week_id, aa.week_of, aa.the_week, aa.ros_opened, bb.labor_sales, cc.parts_sales, dd.discount, 
  bb.labor_sales + cc.parts_sales - bb.labor_cost - cc.parts_cost - dd.discount as gross,
  ee.labor_hours, ee.hours_per_ro,
  case
    when ee.labor_hours = 0 then 0
    else round(bb.labor_sales/ee.labor_hours, 2) 
  end as elr, ff.clock_hours, ee.ros_final_closed 
from (
  select a.writer_name, a.week_id, a.week_of, a.the_week, count(b.ro) as ros_opened
  from sad.writer_weeks a
  left join sad.ros b on a.writer_name = b.writer_name
    and a.the_week @> b.open_date
  group by a.writer_name, a.week_id, a.week_of, a.the_week) aa
left join (
  select a.writer_name, a.week_id, a.week_of, a.the_week, coalesce(sum(labor_sales), 0) as labor_sales,
    coalesce(sum(labor_cost), 0) as labor_cost
  from sad.writer_weeks a
  left join sad.labor_details b on a.writer_name = b.writer_name
    and a.the_week @> b.the_date
  group by a.writer_name, a.week_id, a.week_of, a.the_week) bb on aa.writer_name = bb.writer_name
  and aa.week_id = bb.week_id
left join (
  select a.writer_name, a.week_id, a.week_of, a.the_week, coalesce(sum(parts_sales), 0) as parts_sales,
    coalesce(sum(parts_cost), 0) as parts_cost
  from sad.writer_weeks a
  left join sad.parts_details b on a.writer_name = b.writer_name
    and a.the_week @> b.the_date
  group by a.writer_name, a.week_id, a.week_of, a.the_week) cc on aa.writer_name = cc.writer_name
  and aa.week_id = cc.week_id    
left join (
  select a.writer_name, a.week_id, a.week_of, a.the_week, coalesce(sum(discount), 0) as discount
  from sad.writer_weeks a
  left join sad.discount_details b on a.writer_name = b.writer_name
    and a.the_week @> b.the_date
  group by a.writer_name, a.week_id, a.week_of, a.the_week) dd on aa.writer_name = dd.writer_name
  and aa.week_id = dd.week_id     
left join (
  select a.writer_name, a.week_id, a.week_of, a.the_week, coalesce(sum(b.ro_flag_hours), 0) as labor_hours,
    count(b.ro) as ros_final_closed,
    case
      when count(b.ro) = 0 then 0
      else round(coalesce(sum(b.ro_flag_hours), 0)/count(b.ro), 2)
    end as hours_per_ro
    from sad.writer_weeks a
    left join sad.ros b on a.writer_name = b.writer_name
      and a.the_week @> b.final_close_date
    group by a.writer_name, a.week_id, a.week_of, a.the_week) ee on aa.writer_name = ee.writer_name
      and aa.week_id = ee.week_id
left join (
  select a.writer_name, a.week_id, a.week_of, a.the_week, coalesce(sum(c.clock_hours), 0) as clock_hours
  from sad.writer_weeks a
  left join sad.writers b on a.writer_name = b.writer_name
  left join arkona.xfm_pypclockin c on b.employee_number = c.employee_number
    and a.the_week @> c.the_date
  group by a.writer_name, a.week_id, a.week_of, a.the_week) ff on aa.writer_name = ff.writer_name
    and aa.week_id = ff.week_id;
-----------------------------------------------------------------
drop table if exists sad.shop_weekly cascade;
create table sad.shop_weekly (
  week_id integer primary key,
  week_of date not null,
  the_week daterange not null,
  ros_opened integer not null,
  labor_sales numeric(8,2) not null,
  parts_sales Numeric(8,2) not null,
  discount numeric(8,2) not null,
  gross numeric(8,2) not null,
  labor_hours numeric(8,2) not null,
  hours_per_ro numeric(5,2) not null,
  elr numeric(5,2) not null,
  clock_hours numeric(8,2) not null,
  ros_final_closed integer not null);  
-----------------------------------------------------------------
insert into sad.shop_weekly  
select aa.week_id, aa.week_of, aa.the_week, aa.ros_opened, bb.labor_sales, cc.parts_sales, dd.discount, 
  bb.labor_sales + cc.parts_sales - bb.labor_cost - cc.parts_cost - dd.discount as gross,
  ee.labor_hours, ee.hours_per_ro,
  case
    when ee.labor_hours = 0 then 0
    else round(bb.labor_sales/ee.labor_hours, 2) 
  end as elr, ff.clock_hours, ee.ros_final_closed 
from (
  select a.week_id, a.week_of, a.the_week, count(b.ro) as ros_opened
  from sad.writer_weeks a
  left join sad.ros b on a.the_week @> b.open_date
  group by a.week_id, a.week_of, a.the_week) aa
left join (
  select a.week_id, a.week_of, a.the_week, coalesce(sum(labor_sales), 0) as labor_sales,
    coalesce(sum(labor_cost), 0) as labor_cost
  from sad.writer_weeks a
  left join sad.labor_details b on a.the_week @> b.the_date
  group by a.week_id, a.week_of, a.the_week) bb on aa.week_id = bb.week_id
left join (
  select a.week_id, a.week_of, a.the_week, coalesce(sum(parts_sales), 0) as parts_sales,
    coalesce(sum(parts_cost), 0) as parts_cost
  from sad.writer_weeks a
  left join sad.parts_details b on a.the_week @> b.the_date
  group by a.week_id, a.week_of, a.the_week) cc on aa.week_id = cc.week_id    
left join (
  select a.week_id, a.week_of, a.the_week, coalesce(sum(discount), 0) as discount
  from sad.writer_weeks a
  left join sad.discount_details b on a.the_week @> b.the_date
  group by a.week_id, a.week_of, a.the_week) dd on aa.week_id = dd.week_id     
left join (
  select a.week_id, a.week_of, a.the_week, coalesce(sum(b.ro_flag_hours), 0) as labor_hours,
    count(b.ro) as ros_final_closed,
    case
      when count(b.ro) = 0 then 0
      else round(coalesce(sum(b.ro_flag_hours), 0)/count(b.ro), 2)
    end as hours_per_ro
    from sad.writer_weeks a
    left join sad.ros b on a.the_week @> b.final_close_date
    group by a.week_id, a.week_of, a.the_week) ee on aa.week_id = ee.week_id
left join (
  select a.week_id, a.week_of, a.the_week, coalesce(sum(c.clock_hours), 0) as clock_hours
  from sad.writer_weeks a
  left join sad.writers b on a.writer_name = b.writer_name
  left join arkona.xfm_pypclockin c on b.employee_number = c.employee_number
    and a.the_week @> c.the_date
  group by a.week_id, a.week_of, a.the_week) ff on aa.week_id = ff.week_id;
-----------------------------------------------------------------
-----------------------------------------------------------------
drop table if exists sad.writer_pay_periods cascade;
CREATE TABLE sad.writer_pay_periods(
  pay_period_id integer NOT NULL,
  from_date date NOT NULL,
  thru_date date NOT NULL,
  the_pay_period daterange NOT NULL,
  writer_name citext NOT NULL,
  CONSTRAINT writer_pay_periods_pkey PRIMARY KEY (pay_period_id, writer_name));
create index on sad.writer_pay_periods(from_date);
create index on sad.writer_pay_periods(thru_date);
create index on sad.writer_pay_periods using gist(the_pay_period); 
-----------------------------------------------------------------
insert into sad.writer_pay_periods
select aa.*, bb.writer_name
from (
  select a.biweekly_pay_period_sequence as week_id, min(a.the_date) as from_date, max(a.the_date) as thru_date,
    daterange(min(a.the_date), max(the_date), '[]') as the_pay_period
  from dds.dim_date a
  join (
    select distinct biweekly_pay_period_sequence 
    from dds.dim_date
    where the_date between '01/01/2019' and current_date) b on a.biweekly_pay_period_sequence = b.biweekly_pay_period_sequence
  group by a.biweekly_pay_period_sequence) aa, sad.writers bb;    
-----------------------------------------------------------------
drop table if exists sad.writers_pay_periodly cascade;
create table sad.writers_pay_periodly (
  writer_name citext not null,
  pay_period_id integer not null,
  from_date date not null,
  thru_date date not null,
  the_pay_period daterange not null,
  ros_opened integer not null,
  labor_sales numeric(8,2) not null,
  parts_sales Numeric(8,2) not null,
  discount numeric(8,2) not null,
  gross numeric(8,2) not null,
  labor_hours numeric(5,2) not null,
  hours_per_ro numeric(5,2) not null,
  elr numeric(5,2) not null,
  clock_hours numeric(5,2) not null,
  ros_final_closed integer not null,
  primary key(pay_period_id,writer_name));  
-----------------------------------------------------------------
insert into sad.writers_pay_periodly
select aa.writer_name, aa.pay_period_id, aa.from_date, aa.thru_date, aa.the_pay_period, 
  aa.ros_opened, bb.labor_sales, cc.parts_sales, dd.discount, 
  bb.labor_sales + cc.parts_sales - bb.labor_cost - cc.parts_cost - dd.discount as gross,
  ee.labor_hours, ee.hours_per_ro,
  case
    when ee.labor_hours = 0 then 0
    else round(bb.labor_sales/ee.labor_hours, 2) 
  end as elr, ff.clock_hours, ee.ros_final_closed 
from (
  select a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, count(b.ro) as ros_opened
  from sad.writer_pay_periods a
  left join sad.ros b on a.writer_name = b.writer_name
    and a.the_pay_period @> b.open_date
  group by a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) aa
left join (
  select a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, coalesce(sum(labor_sales), 0) as labor_sales,
    coalesce(sum(labor_cost), 0) as labor_cost
  from sad.writer_pay_periods a
  left join sad.labor_details b on a.writer_name = b.writer_name
    and a.the_pay_period @> b.the_date
  group by a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) bb on aa.writer_name = bb.writer_name
  and aa.pay_period_id = bb.pay_period_id
left join (
  select a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, coalesce(sum(parts_sales), 0) as parts_sales,
    coalesce(sum(parts_cost), 0) as parts_cost
  from sad.writer_pay_periods a
  left join sad.parts_details b on a.writer_name = b.writer_name
    and a.the_pay_period @> b.the_date
  group by a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) cc on aa.writer_name = cc.writer_name
  and aa.pay_period_id = cc.pay_period_id    
left join (
  select a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, coalesce(sum(discount), 0) as discount
  from sad.writer_pay_periods a
  left join sad.discount_details b on a.writer_name = b.writer_name
    and a.the_pay_period @> b.the_date
  group by a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) dd on aa.writer_name = dd.writer_name
  and aa.pay_period_id = dd.pay_period_id     
left join (
  select a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, coalesce(sum(b.ro_flag_hours), 0) as labor_hours,
    count(b.ro) as ros_final_closed,
    case
      when count(b.ro) = 0 then 0
      else round(coalesce(sum(b.ro_flag_hours), 0)/count(b.ro), 2)
    end as hours_per_ro
    from sad.writer_pay_periods a
    left join sad.ros b on a.writer_name = b.writer_name
      and a.the_pay_period @> b.final_close_date
    group by a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) ee on aa.writer_name = ee.writer_name
      and aa.pay_period_id = ee.pay_period_id
left join (
  select a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, coalesce(sum(c.clock_hours), 0) as clock_hours
  from sad.writer_pay_periods a
  left join sad.writers b on a.writer_name = b.writer_name
  left join arkona.xfm_pypclockin c on b.employee_number = c.employee_number
    and a.the_pay_period @> c.the_date
  group by a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) ff on aa.writer_name = ff.writer_name
    and aa.pay_period_id = ff.pay_period_id;
-----------------------------------------------------------------
drop table if exists sad.shop_pay_periodly cascade;
create table sad.shop_pay_periodly (
  pay_period_id integer primary key,
  from_date date not null,
  thru_date date not null,
  the_pay_period daterange not null,
  ros_opened integer not null,
  labor_sales numeric(12,2) not null,
  parts_sales Numeric(12,2) not null,
  discount numeric(8,2) not null,
  gross numeric(12,2) not null,
  labor_hours numeric(8,2) not null,
  hours_per_ro numeric(5,2) not null,
  elr numeric(5,2) not null,
  clock_hours numeric(5,2) not null,
  ros_final_closed integer not null);  
-----------------------------------------------------------------
insert into sad.shop_pay_periodly
select aa.pay_period_id, aa.from_date, aa.thru_date, aa.the_pay_period, 
  aa.ros_opened, bb.labor_sales, cc.parts_sales, dd.discount, 
  bb.labor_sales + cc.parts_sales - bb.labor_cost - cc.parts_cost - dd.discount as gross,
  ee.labor_hours, ee.hours_per_ro,
  case
    when ee.labor_hours = 0 then 0
    else round(bb.labor_sales/ee.labor_hours, 2) 
  end as elr, ff.clock_hours, ee.ros_final_closed 
from (
  select a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, count(b.ro) as ros_opened
  from sad.writer_pay_periods a
  left join sad.ros b on a.the_pay_period @> b.open_date
  group by a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) aa
left join (
  select a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, coalesce(sum(labor_sales), 0) as labor_sales,
    coalesce(sum(labor_cost), 0) as labor_cost
  from sad.writer_pay_periods a
  left join sad.labor_details b on a.the_pay_period @> b.the_date
  group by a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) bb on aa.pay_period_id = bb.pay_period_id
left join (
  select a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, coalesce(sum(parts_sales), 0) as parts_sales,
    coalesce(sum(parts_cost), 0) as parts_cost
  from sad.writer_pay_periods a
  left join sad.parts_details b on a.the_pay_period @> b.the_date
  group by a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) cc on aa.pay_period_id = cc.pay_period_id    
left join (
  select a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, coalesce(sum(discount), 0) as discount
  from sad.writer_pay_periods a
  left join sad.discount_details b on a.the_pay_period @> b.the_date
  group by a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) dd on aa.pay_period_id = dd.pay_period_id     
left join (
  select a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, coalesce(sum(b.ro_flag_hours), 0) as labor_hours,
    count(b.ro) as ros_final_closed,
    case
      when count(b.ro) = 0 then 0
      else round(coalesce(sum(b.ro_flag_hours), 0)/count(b.ro), 2)
    end as hours_per_ro
    from sad.writer_pay_periods a
    left join sad.ros b on a.the_pay_period @> b.final_close_date
    group by a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) ee on aa.pay_period_id = ee.pay_period_id
left join (
  select a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, coalesce(sum(c.clock_hours), 0) as clock_hours
  from sad.writer_pay_periods a
  left join sad.writers b on a.writer_name = b.writer_name
  left join arkona.xfm_pypclockin c on b.employee_number = c.employee_number
    and a.the_pay_period @> c.the_date
  group by a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) ff on aa.pay_period_id = ff.pay_period_id;
-----------------------------------------------------------------
-----------------------------------------------------------------
drop table if exists sad.writer_months cascade;   
CREATE TABLE sad.writer_months(
  year_month integer NOT NULL,
  the_month daterange not null,
  month_name citext not null,
  writer_name citext NOT NULL,
  PRIMARY KEY (year_month, writer_name));
create index on sad.writer_months(month_name);
create index on sad.writer_months using gist(the_month); 
-----------------------------------------------------------------
insert into sad.writer_months
select aa.*, bb.writer_name
from (
  select a.year_month, 
    daterange(min(a.the_date), max(the_date), '[]') as the_month, month_name
  from dds.dim_date a
  join (
    select distinct year_month 
    from dds.dim_date
    where the_date between '01/01/2019' and current_date) b on a.year_month = b.year_month
  group by a.year_month, a.month_name) aa, sad.writers bb;    
------------------------------------------------------------------------------------
drop table if exists sad.writers_monthly cascade;
create table sad.writers_monthly (
  writer_name citext not null,
  year_month integer not null,
  the_month daterange not null,
  month_name citext not null,
  ros_opened integer not null,
  labor_sales numeric(8,2) not null,
  parts_sales Numeric(8,2) not null,
  discount numeric(8,2) not null,
  gross numeric(8,2) not null,
  labor_hours numeric(8,2) not null,
  hours_per_ro numeric(5,2) not null,
  elr numeric(5,2) not null,
  clock_hours numeric(8,2) not null,
  ros_final_closed integer not null,
  primary key(year_month,writer_name),  
  foreign key(year_month,writer_name)  references sad.writer_months(year_month,writer_name));  
-----------------------------------------------------------------
insert into sad.writers_monthly  
select aa.writer_name, aa.year_month, aa.the_month, aa.month_name, aa.ros_opened, bb.labor_sales, cc.parts_sales, dd.discount, 
  bb.labor_sales + cc.parts_sales - bb.labor_cost - cc.parts_cost - dd.discount as gross,
  ee.labor_hours, ee.hours_per_ro,
  case
    when ee.labor_hours = 0 then 0
    else round(bb.labor_sales/ee.labor_hours, 2) 
  end as elr, ff.clock_hours, ee.ros_final_closed 
from (
  select a.writer_name, a.year_month, a.the_month, a.month_name, count(b.ro) as ros_opened
  from sad.writer_months a
  left join sad.ros b on a.the_month @> b.open_date and a.writer_name = b.writer_name
  group by a.writer_name, a.year_month, a.the_month, a.month_name) aa
left join (
  select a.writer_name, a.year_month, a.the_month, a.month_name, coalesce(sum(labor_sales), 0) as labor_sales,
    coalesce(sum(labor_cost), 0) as labor_cost
  from sad.writer_months a
  left join sad.labor_details b on a.the_month @> b.the_date and a.writer_name = b.writer_name
  group by a.writer_name, a.year_month, a.the_month, a.month_name) bb on aa.writer_name = bb.writer_name and aa.year_month = bb.year_month
left join (
  select a.writer_name, a.year_month, a.the_month, a.month_name, coalesce(sum(parts_sales), 0) as parts_sales,
    coalesce(sum(parts_cost), 0) as parts_cost
  from sad.writer_months a
  left join sad.parts_details b on a.the_month @> b.the_date and a.writer_name = b.writer_name
  group by a.writer_name, a.year_month, a.the_month, a.month_name) cc on aa.writer_name = cc.writer_name and aa.year_month = cc.year_month    
left join (
  select a.writer_name, a.year_month, a.the_month, a.month_name, coalesce(sum(discount), 0) as discount
  from sad.writer_months a
  left join sad.discount_details b on a.the_month @> b.the_date and a.writer_name = b.writer_name
  group by a.writer_name, a.year_month, a.the_month, a.month_name) dd on aa.writer_name = dd.writer_name and aa.year_month = dd.year_month     
left join (
  select a.writer_name, a.year_month, a.the_month, a.month_name, coalesce(sum(b.ro_flag_hours), 0) as labor_hours,
    count(b.ro) as ros_final_closed,
    case
      when count(b.ro) = 0 then 0
      else round(coalesce(sum(b.ro_flag_hours), 0)/count(b.ro), 2)
    end as hours_per_ro
    from sad.writer_months a
    left join sad.ros b on a.the_month @> b.final_close_date and a.writer_name = b.writer_name
    group by a.writer_name, a.year_month, a.the_month, a.month_name) ee on aa.writer_name = ee.writer_name and aa.year_month = ee.year_month
left join (
  select a.writer_name, a.year_month, a.the_month, a.month_name, coalesce(sum(c.clock_hours), 0) as clock_hours
  from sad.writer_months a
  left join sad.writers b on a.writer_name = b.writer_name
  left join arkona.xfm_pypclockin c on b.employee_number = c.employee_number
    and a.the_month @> c.the_date
  group by a.writer_name, a.year_month, a.the_month, a.month_name) ff on aa.writer_name = ff.writer_name and aa.year_month = ff.year_month;  
-----------------------------------------------------------------
drop table if exists sad.shop_monthly cascade;
create table sad.shop_monthly (
  year_month integer primary key,
  the_month daterange not null,
  month_name citext not null,
  ros_opened integer not null,
  labor_sales numeric(12,2) not null,
  parts_sales Numeric(12,2) not null,
  discount numeric(8,2) not null,
  gross numeric(12,2) not null,
  labor_hours numeric(8,2) not null,
  hours_per_ro numeric(5,2) not null,
  elr numeric(5,2) not null,
  clock_hours numeric(8,2) not null,
  ros_final_closed integer not null);  
-----------------------------------------------------------------
insert into sad.shop_monthly  
select aa.year_month, aa.the_month, aa.month_name, aa.ros_opened, bb.labor_sales, cc.parts_sales, dd.discount, 
  bb.labor_sales + cc.parts_sales - bb.labor_cost - cc.parts_cost - dd.discount as gross,
  ee.labor_hours, ee.hours_per_ro,
  case
    when ee.labor_hours = 0 then 0
    else round(bb.labor_sales/ee.labor_hours, 2) 
  end as elr, ff.clock_hours, ee.ros_final_closed 
from (
  select a.year_month, a.the_month, a.month_name, count(b.ro) as ros_opened
  from sad.writer_months a
  left join sad.ros b on a.the_month @> b.open_date
  group by a.year_month, a.the_month, a.month_name) aa
left join (
  select a.year_month, a.the_month, a.month_name, coalesce(sum(labor_sales), 0) as labor_sales,
    coalesce(sum(labor_cost), 0) as labor_cost
  from sad.writer_months a
  left join sad.labor_details b on a.the_month @> b.the_date
  group by a.year_month, a.the_month, a.month_name) bb on aa.year_month = bb.year_month
left join (
  select a.year_month, a.the_month, a.month_name, coalesce(sum(parts_sales), 0) as parts_sales,
    coalesce(sum(parts_cost), 0) as parts_cost
  from sad.writer_months a
  left join sad.parts_details b on a.the_month @> b.the_date
  group by a.year_month, a.the_month, a.month_name) cc on aa.year_month = cc.year_month    
left join (
  select a.year_month, a.the_month, a.month_name, coalesce(sum(discount), 0) as discount
  from sad.writer_months a
  left join sad.discount_details b on a.the_month @> b.the_date
  group by a.year_month, a.the_month, a.month_name) dd on aa.year_month = dd.year_month     
left join (
  select a.year_month, a.the_month, a.month_name, coalesce(sum(b.ro_flag_hours), 0) as labor_hours,
    count(b.ro) as ros_final_closed,
    case
      when count(b.ro) = 0 then 0
      else round(coalesce(sum(b.ro_flag_hours), 0)/count(b.ro), 2)
    end as hours_per_ro
    from sad.writer_months a
    left join sad.ros b on a.the_month @> b.final_close_date
    group by a.year_month, a.the_month, a.month_name) ee on aa.year_month = ee.year_month
left join (
  select a.year_month, a.the_month, a.month_name, coalesce(sum(c.clock_hours), 0) as clock_hours
  from sad.writer_months a
  left join sad.writers b on a.writer_name = b.writer_name
  left join arkona.xfm_pypclockin c on b.employee_number = c.employee_number
    and a.the_month @> c.the_date
  group by a.year_month, a.the_month, a.month_name) ff on aa.year_month = ff.year_month;  


-----------------------------------------------------------------
-----------------------------------------------------------------
drop table if exists sad.writers_wtd cascade;
create table sad.writers_wtd (
  the_date date not null,
  week_id integer not null,
  week_of date not null,
  writer_name citext not null,
  ros_opened integer not null,
  labor_sales numeric(8,2) not null,
  parts_sales Numeric(8,2) not null,
  discount numeric(8,2) not null,
  gross numeric(8,2) not null,
  labor_hours numeric(8,2) not null,
  hours_per_ro numeric(5,2) not null,
  elr numeric(5,2) not null,
  clock_hours numeric(8,2) not null,
  ros_final_closed integer not null,
  primary key(the_date,writer_name));  
-----------------------------------------------------------------
insert into sad.writers_wtd
select a.the_date, b.week_id, week_of, a.writer_name, 
  sum(a.ros_opened) over (partition by b.week_id, a.writer_name order by a.the_date asc) as ros_opened,
  sum(a.labor_sales) over (partition by b.week_id, a.writer_name order by a.the_date asc) as labor_sales,
  sum(a.parts_sales) over (partition by b.week_id, a.writer_name order by a.the_date asc) as parts_sales,
  sum(a.discount) over (partition by b.week_id, a.writer_name order by a.the_date asc) as discount,
  sum(a.gross) over (partition by b.week_id, a.writer_name order by a.the_date asc) as gross,
  sum(a.labor_hours) over (partition by b.week_id, a.writer_name order by a.the_date asc) as labor_hours,
  case
    when sum(a.ros_final_closed) over (partition by b.week_id, a.writer_name order by a.the_date asc) = 0 then 0
    else 
      round(
        (sum(a.labor_hours) over (partition by b.week_id, a.writer_name order by a.the_date asc)
        /
        sum(a.ros_final_closed) over (partition by b.week_id, a.writer_name order by a.the_date asc)), 2) 
    end as hours_per_ro,
  case
    when sum(a.labor_hours) over (partition by b.week_id, a.writer_name order by a.the_date asc) = 0 then 0
    else
      round(
        (sum(a.labor_sales) over (partition by b.week_id, a.writer_name order by a.the_date asc)
        /
        sum(a.labor_hours) over (partition by b.week_id, a.writer_name order by a.the_date asc)), 2) 
    end as elr,
  sum(a.clock_hours) over (partition by b.week_id, a.writer_name order by a.the_date asc) as clock_hours,
  sum(a.ros_final_closed) over (partition by b.week_id, a.writer_name order by a.the_date asc) as ros_final_closed
from sad.writers_daily a
join sad.writer_weeks b on b.the_week @> a.the_date 
  and a.writer_name = b.writer_name
order by a.writer_name, the_date;
-----------------------------------------------------------------
drop table if exists sad.writers_pptd cascade;
create table sad.writers_pptd (
  the_date date not null,
  pay_period_id integer not null,
  from_date date not null,
  thru_date date not null,
  writer_name citext not null,
  ros_opened integer not null,
  labor_sales numeric(12,2) not null,
  parts_sales Numeric(12,2) not null,
  discount numeric(8,2) not null,
  gross numeric(12,2) not null,
  labor_hours numeric(12,2) not null,
  hours_per_ro numeric(5,2) not null,
  elr numeric(5,2) not null,
  clock_hours numeric(12,2) not null,
  ros_final_closed integer not null,
  primary key(the_date,writer_name));  
  
-- select * from  sad.writer_pay_periods
-----------------------------------------------------------------
insert into sad.writers_pptd
select a.the_date, pay_period_id, from_date, thru_date, a.writer_name, 
  sum(a.ros_opened) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) as ros_opened,
  sum(a.labor_sales) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) as labor_sales,
  sum(a.parts_sales) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) as parts_sales,
  sum(a.discount) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) as discount,
  sum(a.gross) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) as gross,
  sum(a.labor_hours) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) as labor_hours,
  case
    when sum(a.ros_final_closed) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) = 0 then 0
    else 
      round(
        (sum(a.labor_hours) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc)
        /
        sum(a.ros_final_closed) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc)), 2) 
    end as hours_per_ro,
  case
    when sum(a.labor_hours) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) = 0 then 0
    else
      round(
        (sum(a.labor_sales) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc)
        /
        sum(a.labor_hours) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc)), 2) 
    end as elr,
  sum(a.clock_hours) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) as clock_hours,
  sum(a.ros_final_closed) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) as ros_final_closed
from sad.writers_daily a
join sad.writer_pay_periods b on b.the_pay_period @> a.the_date 
  and a.writer_name = b.writer_name
order by a.writer_name, the_date;

-----------------------------------------------------------------
drop table if exists sad.writers_mtd cascade;
create table sad.writers_mtd (
  the_date date not null,
  year_month integer not null,
  month_name citext not null,
  writer_name citext not null,
  ros_opened integer not null,
  labor_sales numeric(12,2) not null,
  parts_sales Numeric(12,2) not null,
  discount numeric(8,2) not null,
  gross numeric(12,2) not null,
  labor_hours numeric(12,2) not null,
  hours_per_ro numeric(5,2) not null,
  elr numeric(5,2) not null,
  clock_hours numeric(12,2) not null,
  ros_final_closed integer not null,
  primary key(the_date,writer_name));  
  
-- select * from  sad.writer_months
-----------------------------------------------------------------
insert into sad.writers_mtd
select a.the_date, year_month, month_name, a.writer_name, 
  sum(a.ros_opened) over (partition by b.year_month, a.writer_name order by a.the_date asc) as ros_opened,
  sum(a.labor_sales) over (partition by b.year_month, a.writer_name order by a.the_date asc) as labor_sales,
  sum(a.parts_sales) over (partition by b.year_month, a.writer_name order by a.the_date asc) as parts_sales,
  sum(a.discount) over (partition by b.year_month, a.writer_name order by a.the_date asc) as discount,
  sum(a.gross) over (partition by b.year_month, a.writer_name order by a.the_date asc) as gross,
  sum(a.labor_hours) over (partition by b.year_month, a.writer_name order by a.the_date asc) as labor_hours,
  case
    when sum(a.ros_final_closed) over (partition by b.year_month, a.writer_name order by a.the_date asc) = 0 then 0
    else 
      round(
        (sum(a.labor_hours) over (partition by b.year_month, a.writer_name order by a.the_date asc)
        /
        sum(a.ros_final_closed) over (partition by b.year_month, a.writer_name order by a.the_date asc)), 2) 
    end as hours_per_ro,
  case
    when sum(a.labor_hours) over (partition by b.year_month, a.writer_name order by a.the_date asc) = 0 then 0
    else
      round(
        (sum(a.labor_sales) over (partition by b.year_month, a.writer_name order by a.the_date asc)
        /
        sum(a.labor_hours) over (partition by b.year_month, a.writer_name order by a.the_date asc)), 2) 
    end as elr,
  sum(a.clock_hours) over (partition by b.year_month, a.writer_name order by a.the_date asc) as clock_hours,
  sum(a.ros_final_closed) over (partition by b.year_month, a.writer_name order by a.the_date asc) as ros_final_closed
from sad.writers_daily a
join sad.writer_months b on b.the_month @> a.the_date 
  and a.writer_name = b.writer_name
order by a.writer_name, the_date;

-----------------------------------------------------------------
-----------------------------------------------------------------
drop table if exists sad.shop_wtd cascade;
create table sad.shop_wtd (
  the_date date not null primary key,
  week_id integer not null,
  week_of date not null,
  ros_opened integer not null,
  labor_sales numeric(8,2) not null,
  parts_sales Numeric(8,2) not null,
  discount numeric(8,2) not null,
  gross numeric(8,2) not null,
  labor_hours numeric(8,2) not null,
  hours_per_ro numeric(5,2) not null,
  elr numeric(5,2) not null,
  clock_hours numeric(8,2) not null,
  ros_final_closed integer not null);  
-----------------------------------------------------------------
insert into sad.shop_wtd
select a.the_date, b.week_id, week_of,
  sum(a.ros_opened) over (partition by b.week_id order by a.the_date asc) as ros_opened,
  sum(a.labor_sales) over (partition by b.week_id order by a.the_date asc) as labor_sales,
  sum(a.parts_sales) over (partition by b.week_id order by a.the_date asc) as parts_sales,
  sum(a.discount) over (partition by b.week_id order by a.the_date asc) as discount,
  sum(a.gross) over (partition by b.week_id order by a.the_date asc) as gross,
  sum(a.labor_hours) over (partition by b.week_id order by a.the_date asc) as labor_hours,
  case
    when sum(a.ros_final_closed) over (partition by b.week_id order by a.the_date asc) = 0 then 0
    else 
      round(
        (sum(a.labor_hours) over (partition by b.week_id order by a.the_date asc)
        /
        sum(a.ros_final_closed) over (partition by b.week_id order by a.the_date asc)), 2) 
    end as hours_per_ro,
  case
    when sum(a.labor_hours) over (partition by b.week_id order by a.the_date asc) = 0 then 0
    else
      round(
        (sum(a.labor_sales) over (partition by b.week_id order by a.the_date asc)
        /
        sum(a.labor_hours) over (partition by b.week_id order by a.the_date asc)), 2) 
    end as elr,
  sum(a.clock_hours) over (partition by b.week_id order by a.the_date asc) as clock_hours,
  sum(a.ros_final_closed) over (partition by b.week_id order by a.the_date asc) as ros_final_closed
from sad.shop_daily a
join (
  select week_id, week_of, the_week 
  from sad.writer_weeks
  group by week_id, week_of, the_week) b on b.the_week @> a.the_date 
order by the_date;

-----------------------------------------------------------------
drop table if exists sad.shop_pptd cascade;
create table sad.shop_pptd (
  the_date date not null primary key,
  pay_period_id integer not null,
  from_date date not null,
  thru_date date not null,
  ros_opened integer not null,
  labor_sales numeric(12,2) not null,
  parts_sales Numeric(12,2) not null,
  discount numeric(8,2) not null,
  gross numeric(12,2) not null,
  labor_hours numeric(12,2) not null,
  hours_per_ro numeric(5,2) not null,
  elr numeric(5,2) not null,
  clock_hours numeric(8,2) not null,
  ros_final_closed integer not null);  
-----------------------------------------------------------------
insert into sad.shop_pptd
select a.the_date, b.pay_period_id, from_date, thru_date,
  sum(a.ros_opened) over (partition by b.pay_period_id order by a.the_date asc) as ros_opened,
  sum(a.labor_sales) over (partition by b.pay_period_id order by a.the_date asc) as labor_sales,
  sum(a.parts_sales) over (partition by b.pay_period_id order by a.the_date asc) as parts_sales,
  sum(a.discount) over (partition by b.pay_period_id order by a.the_date asc) as discount,
  sum(a.gross) over (partition by b.pay_period_id order by a.the_date asc) as gross,
  sum(a.labor_hours) over (partition by b.pay_period_id order by a.the_date asc) as labor_hours,
  case
    when sum(a.ros_final_closed) over (partition by b.pay_period_id order by a.the_date asc) = 0 then 0
    else 
      round(
        (sum(a.labor_hours) over (partition by b.pay_period_id order by a.the_date asc)
        /
        sum(a.ros_final_closed) over (partition by b.pay_period_id order by a.the_date asc)), 2) 
    end as hours_per_ro,
  case
    when sum(a.labor_hours) over (partition by b.pay_period_id order by a.the_date asc) = 0 then 0
    else
      round(
        (sum(a.labor_sales) over (partition by b.pay_period_id order by a.the_date asc)
        /
        sum(a.labor_hours) over (partition by b.pay_period_id order by a.the_date asc)), 2) 
    end as elr,
  sum(a.clock_hours) over (partition by b.pay_period_id order by a.the_date asc) as clock_hours,
  sum(a.ros_final_closed) over (partition by b.pay_period_id order by a.the_date asc) as ros_final_closed
from sad.shop_daily a
join (
  select pay_period_id, from_date, thru_date, the_pay_period 
  from sad.writer_pay_periods
  group by pay_period_id, from_date, thru_date, the_pay_period) b on b.the_pay_period @> a.the_date 
order by a.the_date;

-----------------------------------------------------------------
-- select * from sad.writer_months

drop table if exists sad.shop_mtd cascade;
create table sad.shop_mtd (
  the_date date not null primary key,
  year_month integer not null,
  month_name citext not null,
  ros_opened integer not null,
  labor_sales numeric(12,2) not null,
  parts_sales Numeric(12,2) not null,
  discount numeric(8,2) not null,
  gross numeric(12,2) not null,
  labor_hours numeric(12,2) not null,
  hours_per_ro numeric(5,2) not null,
  elr numeric(5,2) not null,
  clock_hours numeric(8,2) not null,
  ros_final_closed integer not null);  
-----------------------------------------------------------------
insert into sad.shop_mtd
select a.the_date, b.year_month, b.month_name,
  sum(a.ros_opened) over (partition by b.year_month order by a.the_date asc) as ros_opened,
  sum(a.labor_sales) over (partition by b.year_month order by a.the_date asc) as labor_sales,
  sum(a.parts_sales) over (partition by b.year_month order by a.the_date asc) as parts_sales,
  sum(a.discount) over (partition by b.year_month order by a.the_date asc) as discount,
  sum(a.gross) over (partition by b.year_month order by a.the_date asc) as gross,
  sum(a.labor_hours) over (partition by b.year_month order by a.the_date asc) as labor_hours,
  case
    when sum(a.ros_final_closed) over (partition by b.year_month order by a.the_date asc) = 0 then 0
    else 
      round(
        (sum(a.labor_hours) over (partition by b.year_month order by a.the_date asc)
        /
        sum(a.ros_final_closed) over (partition by b.year_month order by a.the_date asc)), 2) 
    end as hours_per_ro,
  case
    when sum(a.labor_hours) over (partition by b.year_month order by a.the_date asc) = 0 then 0
    else
      round(
        (sum(a.labor_sales) over (partition by b.year_month order by a.the_date asc)
        /
        sum(a.labor_hours) over (partition by b.year_month order by a.the_date asc)), 2) 
    end as elr,
  sum(a.clock_hours) over (partition by b.year_month order by a.the_date asc) as clock_hours,
  sum(a.ros_final_closed) over (partition by b.year_month order by a.the_date asc) as ros_final_closed
from sad.shop_daily a
join (
  select year_month, month_name, the_month
  from sad.writer_months
  group by year_month, month_name, the_month) b on b.the_month @> a.the_date 
order by a.the_date;
