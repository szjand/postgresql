﻿select extract(day from a.the_date)::integer as dom, a.gross::integer as jan, b.gross::integer as feb, c.gross::integer as mar,
  d.gross::integer as apr, e.gross::integer as may, f.gross::integer as jun, g.gross::integer as jul
from sad.shop_mtd a
left join sad.shop_mtd b on extract(day from a.the_date) = extract(day from b.the_date)
  and b.year_month = 202002
left join sad.shop_mtd c on extract(day from a.the_date) = extract(day from c.the_date)
  and c.year_month = 202003  
left join sad.shop_mtd d on extract(day from a.the_date) = extract(day from d.the_date)
  and d.year_month = 202004  
left join sad.shop_mtd e on extract(day from a.the_date) = extract(day from e.the_date)
  and e.year_month = 202005  
left join sad.shop_mtd f on extract(day from a.the_date) = extract(day from f.the_date)
  and f.year_month = 202006   
left join sad.shop_mtd g on extract(day from a.the_date) = extract(day from g.the_date)
  and g.year_month = 202007         
where a.year_month = 202001




select extract(day from a.the_date)::integer as dom, a.gross::integer as jan, b.gross::integer as feb, c.gross::integer as mar,
  d.gross::integer as apr, e.gross::integer as may, f.gross::integer as jun, g.gross::integer as jul
from sad.shop_mtd a
left join sad.shop_mtd b on extract(day from a.the_date) = extract(day from b.the_date)
  and b.year_month = 201902
left join sad.shop_mtd c on extract(day from a.the_date) = extract(day from c.the_date)
  and c.year_month = 201903  
left join sad.shop_mtd d on extract(day from a.the_date) = extract(day from d.the_date)
  and d.year_month = 201904  
left join sad.shop_mtd e on extract(day from a.the_date) = extract(day from e.the_date)
  and e.year_month = 201905  
left join sad.shop_mtd f on extract(day from a.the_date) = extract(day from f.the_date)
  and f.year_month = 201906   
left join sad.shop_mtd g on extract(day from a.the_date) = extract(day from g.the_date)
  and g.year_month = 201907         
where a.year_month = 201901