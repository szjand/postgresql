﻿
DROP TABLE if exists gmgl.service_advisor_csi_ext cascade;

CREATE TABLE gmgl.service_advisor_csi_ext (
employee_name citext,
number_of_responses citext,
response_rate citext,
blended_metric_index citext,
blended_metric_tb citext,
sss_overall_satisfaction_tb citext,
recommend_dealer citext,
sss_overall_satisfaction_index citext,
courteous_and_transparent_treatment citext,
kept_informed_of_status citext,
explanation_of_work_performed citext,
condition_of_vehicle citext,
fixed_right_first_time citext,
file_name citext,
primary key(employee_name,file_name));

select * from  gmgl.service_advisor_csi_ext;
drop table if exists gmgl.service_advisor_csi cascade;
CREATE TABLE gmgl.service_advisor_csi (
  period citext not null,
  make citext not null,
  report_date date not null,
  employee_name citext not null,
  number_of_responses citext,
  response_rate citext,
  blended_metric_index citext,
  blended_metric_tb citext,
  sss_overall_satisfaction_tb citext,
  recommend_dealer citext,
  sss_overall_satisfaction_index citext,
  courteous_and_transparent_treatment citext,
  kept_informed_of_status citext,
  explanation_of_work_performed citext,
  condition_of_vehicle citext,
  fixed_right_first_time citext,
  file_name citext,
  primary key(employee_name,period,make,report_date));
select * from gmgl.service_advisor_csi;

CREATE OR REPLACE FUNCTION gmgl.service_advisor_csi_update()
  RETURNS void AS
$BODY$
/*
select gmgl.service_advisor_csi_update();
*/
insert into gmgl.service_advisor_csi
select right(replace(split_part(file_name, '-', 3),'Serviceadvisor',''), 3) as period,
  left(replace(split_part(file_name, '-', 3),'Serviceadvisor',''), length(replace(split_part(file_name, '-', 3),'Serviceadvisor','')) - 3) as make,
  (split_part(file_name, '-', 5) || '/' || left(split_part(file_name, '-', 6), 2) || '/' || split_part(file_name, '-', 4))::date as report_date,
  employee_name, number_of_responses, response_rate, blended_metric_tb, blended_metric_tb, 
  sss_overall_satisfaction_tb, recommend_dealer, sss_overall_satisfaction_index,
  courteous_and_transparent_treatment, kept_informed_of_status, explanation_of_work_performed, 
  condition_of_vehicle, fixed_right_first_time  
from gmgl.service_advisor_csi_ext;
$BODY$
LANGUAGE sql;


drop table if exists gmgl.service_make_csi_ext;
CREATE TABLE gmgl.service_make_csi_ext(
  date_range citext NOT NULL,
  blended_metric_tb citext,
  blended_metric_index citext,
  recommend_dealer citext,
  sss_overall_satisfaction_tb citext,
  sss_overall_satisfaction_index citext,
  knowledge_of_personnel citext,
  ease_of_service_experience citext,
  satisfied_with_work_performed citext,
  appointment_scheduling citext,
  greeted_promptly citext,
  amenities_of_dealership citext,
  alternative_transportation_offered citext,
  understood_needs citext,
  kept_informed_of_status citext,
  ready_at_time_promised citext,
  timing_to_complete citext,
  explanation_of_work_performed citext,
  multi_point_inspection citext,
  review_recommended_maintenance citext,
  advice_on_future_needs citext,
  overall_consultant citext,
  fixed_right_first_time citext,
  condition_of_vehicle citext,
  my_rewards citext,
  contacted_by_dealership citext,
  recommend_brand citext,
  file_name citext NOT NULL,
  CONSTRAINT service_make_csi_ext_pkey PRIMARY KEY (date_range, file_name));

drop table if exists gmgl.service_make_csi cascade;
CREATE TABLE gmgl.service_make_csi(
  report_date date not null,
  period citext NOT NULL,
  make citext not null,
  blended_metric_tb citext,
  blended_metric_index citext,
  recommend_dealer citext,
  sss_overall_satisfaction_tb citext,
  sss_overall_satisfaction_index citext,
  knowledge_of_personnel citext,
  ease_of_service_experience citext,
  satisfied_with_work_performed citext,
  appointment_scheduling citext,
  greeted_promptly citext,
  amenities_of_dealership citext,
  alternative_transportation_offered citext,
  understood_needs citext,
  kept_informed_of_status citext,
  ready_at_time_promised citext,
  timing_to_complete citext,
  explanation_of_work_performed citext,
  multi_point_inspection citext,
  review_recommended_maintenance citext,
  advice_on_future_needs citext,
  overall_consultant citext,
  fixed_right_first_time citext,
  condition_of_vehicle citext,
  my_rewards citext,
  contacted_by_dealership citext,
  recommend_brand citext,
  CONSTRAINT service_make_csi_pkey PRIMARY KEY (report_date,period,make));

CREATE OR REPLACE FUNCTION gmgl.service_make_csi_update()
  RETURNS void AS
$BODY$
/*

*/
insert into gmgl.service_make_csi
select current_date as report_date, date_range as period,  
  replace(split_part(file_name, '-', 3), 'Servicedepartment', '') as make,
  blended_metric_tb,blended_metric_index,recommend_dealer,sss_overall_satisfaction_tb,
  sss_overall_satisfaction_index,knowledge_of_personnel,ease_of_service_experience citext,
  satisfied_with_work_performed,appointment_scheduling,greeted_promptly,
  amenities_of_dealership,alternative_transportation_offered,understood_needs,
  kept_informed_of_status,ready_at_time_promised,timing_to_complete,explanation_of_work_performed,
  multi_point_inspection,review_recommended_maintenance,advice_on_future_needs,overall_consultant,
  fixed_right_first_time,condition_of_vehicle,my_rewards,contacted_by_dealership,recommend_brand  
from gmgl.service_make_csi_ext;
$BODY$
LANGUAGE sql;



select * from gmgl.service_make_csi


  