﻿

-- 1. writers --------------------------------------------------------------------------------

insert into sad.writers
select writer_name, array_agg(distinct writer_number), employee_number, array_agg(service_writer_key)
from dds.dim_service_writer 
where writer_name in ('EULISS, NED','TROFTGRUBEN, RODNEY','MARO, NICHOLAS','JANKOWSKI, GARRETT',
  'GOTHBERG, CONNER J','RODRIGUEZ, JUSTIN','CARLSON, KENNETH A')
group by writer_name, employee_number
on conflict(writer_name)
do update
  set (writer_number,employee_number,service_writer_key)
  =
  (excluded.writer_number,excluded.employee_number,excluded.service_writer_key);

-- 2.ros --------------------------------------------------------------------------------

insert into sad.ros (ro,ro_labor_sales,ro_flag_hours,ro_discount, writer_name,open_date,close_date,final_close_date)
select a.ro, a.ro_labor_sales, a.ro_flag_hours, a.ro_discount, b.writer_name, -- b.writer_number, 
  open_date, close_date, final_close_date
from dds.fact_repair_order a
join sad.writers b on a.service_writer_key = any(b.service_writer_key)
where a.final_close_date >= current_date - 90
group by a.ro, a.ro_labor_sales, a.ro_flag_hours, a.ro_discount, b.writer_name, b.writer_number,
  open_date, close_date, final_close_date
on conflict (ro)
do update
    set(ro_labor_sales, ro_flag_hours, ro_discount, writer_name)
    =
    (excluded.ro_labor_sales, excluded.ro_flag_hours, excluded.ro_discount, excluded.writer_name);


-- 3. accounts --------------------------------------------------------------------------------

insert into sad.labor_accounts
select e.account, e.account_key, e.description, e.department, e.account_type
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.page = 16 
  and b.line between 20 and 28
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.area = 'fixed'
join fin.dim_fs_account d on a.fs_Account_key = d.fs_account_key
  and d.current_row
join fin.dim_account e on d.gl_account = e.account
  and e.current_row
  and e.description not like '%discount%'
where c.store = 'RY1'
group by e.account, e.account_key, e.description, e.department, e.account_type
on conflict(account)
do update
  set (account_key,description,department,account_type)
  =
  (excluded.account_key,excluded.description,excluded.department,excluded.account_type);

insert into sad.parts_accounts
select e.account, e.account_key, e.description, e.department, e.account_type
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.page = 16 
  and b.line between 45 and 60
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.area = 'fixed'
join fin.dim_fs_account d on a.fs_Account_key = d.fs_account_key
  and d.current_row
join fin.dim_account e on d.gl_account = e.account
  and e.current_row
  and e.description not like '%discount%'
where c.store = 'RY1'
group by e.account, e.account_key, e.description, e.department, e.account_type
on conflict(account)
do update
  set (account_key,description,department,account_type)
  =
  (excluded.account_key,excluded.description,excluded.department,excluded.account_type);

insert into sad.discount_accounts
select a.account, a.account_key, a.description, 
  case
    when a.department = 'Parts' then 'Parts'
    else 'Labor'
  end as department
from fin.dim_account a
join fin.dim_fs_account b on a.account = b.gl_account
join fin.fact_fs c on b.fs_account_key = c.fs_account_key
join fin.dim_fs d on c.fs_key = d.fs_key
join fin.dim_fs_org e on c.fs_org_key = e.fs_org_key  
  and e.area = 'fixed'  
where description like '%discount%'
  and a.store_code = 'RY1'
group by a.account, a.account_key, a.description, a.department
on conflict(account)
do update
  set (account_key,description,department)
  =
  (excluded.account_key,excluded.description,excluded.department);

-- 4. details --------------------------------------------------------------------------------
insert into sad.labor_details 
select ro, writer_name, c.final_close_date, 
  coalesce(sum(-a.amount) filter (where b.account_type = 'Sale'), 0) as labor_sales,
  coalesce(sum(a.amount) filter (where b.account_type = 'COGS'), 0) as labor_cost
from fin.fact_gl a
join sad.labor_accounts b on a.account_key = b.account_key
join sad.ros c on a.control = c.ro
  and c.final_close_date >= current_date - 45
where a.post_Status = 'Y'
group by c.ro, c.final_close_date, c.writer_name
on conflict(ro)
do update
  set (writer_name,the_date,labor_sales,labor_cost)
  =
  (excluded.writer_name,excluded.the_date,excluded.labor_sales,excluded.labor_cost);

insert into sad.parts_details 
select ro, writer_name, c.final_close_date, 
  coalesce(sum(-a.amount) filter (where b.account_type = 'Sale'), 0) as parts_sales,
  coalesce(sum(a.amount) filter (where b.account_type = 'COGS'), 0) as parts_cost
from fin.fact_gl a
join sad.parts_accounts b on a.account_key = b.account_key
join sad.ros c on a.control = c.ro
  and c.final_close_date >= current_date - 45
where a.post_Status = 'Y'
group by c.ro, c.final_close_date, c.writer_name
on conflict(ro)
do update
  set (writer_name,the_date,parts_sales,parts_cost)
  =
  (excluded.writer_name,excluded.the_date,excluded.parts_sales,excluded.parts_cost);  

insert into sad.discount_details 
select ro, writer_name, c.final_close_date, sum(a.amount) as labor_sales 
from fin.fact_gl a
join sad.discount_accounts b on a.account_key = b.account_key
join sad.ros c on a.control = c.ro
  and c.final_close_date >= current_date - 45
where a.post_Status = 'Y'
group by c.ro, c.final_close_date, c.writer_name
on conflict(ro)
do update
  set (writer_name,the_date,discount)
  =
  (excluded.writer_name,excluded.the_date,excluded.discount); 

-- 5. time period tables --------------------------------------------------------------------------------

insert into sad.writer_days
select a.the_date, b.writer_name
from dds.dim_date a, sad.writers b
where a.the_year >= 2019
  and a.the_date < current_Date
on conflict(the_date,writer_name)
do nothing;  

insert into sad.writer_weeks
select aa.*, bb.writer_name
from (
  select a.sunday_to_saturday_week as week_id, min(a.the_date) as week_of, 
    daterange(min(a.the_date), max(the_date), '[]') as the_week
  from dds.dim_date a
  join (
    select distinct sunday_to_saturday_week 
    from dds.dim_date
    where the_date between '01/01/2019' and current_date) b on a.sunday_to_saturday_week = b.sunday_to_saturday_week
  group by a.sunday_to_saturday_week) aa, sad.writers bb
on conflict(week_id,writer_name)
do nothing;  

insert into sad.writer_pay_periods
select aa.*, bb.writer_name
from (
  select a.biweekly_pay_period_sequence as week_id, min(a.the_date) as from_date, max(a.the_date) as thru_date,
    daterange(min(a.the_date), max(the_date), '[]') as the_pay_period
  from dds.dim_date a
  join (
    select distinct biweekly_pay_period_sequence 
    from dds.dim_date
    where the_date between '01/01/2019' and current_date) b on a.biweekly_pay_period_sequence = b.biweekly_pay_period_sequence
  group by a.biweekly_pay_period_sequence) aa, sad.writers bb
on conflict(pay_period_id,writer_name)
do nothing;       

insert into sad.writer_months
select aa.*, bb.writer_name
from (
  select a.year_month, 
    daterange(min(a.the_date), max(the_date), '[]') as the_month, month_name
  from dds.dim_date a
  join (
    select distinct year_month 
    from dds.dim_date
    where the_date between '01/01/2019' and current_date) b on a.year_month = b.year_month
  group by a.year_month, a.month_name) aa, sad.writers bb
on conflict (year_month,writer_name)
do nothing;  

-- 5. daily data --------------------------------------------------------------------------------
-- writers
insert into sad.writers_daily
select writer_name, the_date, ros_opened, labor_sales, parts_sales, discount, gross,
  labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed
from (
  select aa.writer_name, aa.the_date, aa.ros_opened, bb.labor_sales, cc.parts_sales, dd.discount, 
    bb.labor_sales + cc.parts_sales - bb.labor_cost - cc.parts_cost - dd.discount as gross,
    ee.labor_hours, ee.hours_per_ro,
    case
      when ee.labor_hours = 0 then 0
      else round(bb.labor_sales/ee.labor_hours, 2) 
    end as elr,
    ff.clock_hours, ee.ros_final_closed
  from (
    select a.writer_name, a.the_date, count(b.ro) as ros_opened
    from sad.writer_days a
    left join sad.ros b on a.writer_name = b.writer_name
      and a.the_date = b.open_date
    group by a.writer_name, a.the_date) aa
  left join (
    select a.writer_name, a.the_date, coalesce(sum(labor_sales), 0) as labor_sales,
      coalesce(sum(labor_cost), 0) as labor_cost
    from sad.writer_days a
    left join sad.labor_details b on a.writer_name = b.writer_name
      and a.the_date = b.the_date
    group by a.writer_name, a.the_date) bb on aa.writer_name = bb.writer_name
    and aa.the_date = bb.the_date
  left join (
    select a.writer_name, a.the_date, coalesce(sum(parts_sales), 0) as parts_sales,
       coalesce(sum(parts_cost), 0) as parts_cost
    from sad.writer_days a
    left join sad.parts_details b on a.writer_name = b.writer_name
      and a.the_date = b.the_date
    group by a.writer_name, a.the_date) cc on aa.writer_name = cc.writer_name
    and aa.the_date = cc.the_date  
  left join (
    select a.writer_name, a.the_date, coalesce(sum(discount), 0) as discount
    from sad.writer_days a
    left join sad.discount_details b on a.writer_name = b.writer_name
      and a.the_date = b.the_date
    group by a.writer_name, a.the_date) dd on aa.writer_name = dd.writer_name
    and aa.the_date = dd.the_date    
  left join (
    select a.writer_name, a.the_date, coalesce(sum(b.ro_flag_hours), 0) as labor_hours,
      count(b.ro) as ros_final_closed,
      case
        when count(b.ro) = 0 then 0
        else round(coalesce(sum(b.ro_flag_hours), 0)/count(b.ro), 2)
      end as hours_per_ro
    from sad.writer_days a
    left join sad.ros b on a.writer_name = b.writer_name
      and a.the_date = b.final_close_date
    group by a.writer_name, a.the_date) ee on aa.writer_name = ee.writer_name
    and aa.the_date = ee.the_date  
  left join (
    select a.writer_name, a.the_date, coalesce(sum(c.clock_hours), 0) as clock_hours
    from sad.writer_days a
    left join sad.writers b on a.writer_name = b.writer_name
    left join arkona.xfm_pypclockin c on b.employee_number = c.employee_number
      and a.the_date = c.the_date
    group by a.writer_name, a.the_date) ff on aa.writer_name = ff.writer_name
    and aa.the_date = ff.the_date) x
where the_date >= current_date - 90    
on conflict (the_date,writer_name)
do update
  set (ros_opened, labor_sales, parts_sales, discount, gross,
    labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed)
  =
  (excluded.ros_opened, excluded.labor_sales, excluded.parts_sales, excluded.discount, 
    excluded.gross, excluded.labor_hours, excluded.hours_per_ro, excluded.elr, excluded.clock_hours, excluded.ros_final_closed);

-- shop  
insert into sad.shop_daily
select the_date, ros_opened, labor_sales, parts_sales, discount, gross,
  labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed
from (
  select aa.the_date, aa.ros_opened, bb.labor_sales, cc.parts_sales, dd.discount, 
    bb.labor_sales + cc.parts_sales - bb.labor_cost - cc.parts_cost - dd.discount as gross,
    ee.labor_hours, ee.hours_per_ro,
    case
      when ee.labor_hours = 0 then 0
      else round(bb.labor_sales/ee.labor_hours, 2) 
    end as elr,
    ff.clock_hours, ee.ros_final_closed
  from (
    select a.the_date, count(b.ro) as ros_opened
    from sad.writer_days a
    left join sad.ros b on a.the_date = b.open_date
    group by a.the_date) aa
  left join (
    select a.the_date, coalesce(sum(labor_sales), 0) as labor_sales,
      coalesce(sum(labor_cost), 0) as labor_cost
    from sad.writer_days a
    left join sad.labor_details b on a.the_date = b.the_date
    group by a.the_date) bb on aa.the_date = bb.the_date
  left join (
    select a.the_date, coalesce(sum(parts_sales), 0) as parts_sales,
       coalesce(sum(parts_cost), 0) as parts_cost
    from sad.writer_days a
    left join sad.parts_details b on a.the_date = b.the_date
    group by a.the_date) cc on aa.the_date = cc.the_date  
  left join (
    select a.the_date, coalesce(sum(discount), 0) as discount
    from sad.writer_days a
    left join sad.discount_details b on a.the_date = b.the_date
    group by a.the_date) dd on aa.the_date = dd.the_date    
  left join (
    select a.the_date, coalesce(sum(b.ro_flag_hours), 0) as labor_hours,
      count(b.ro) as ros_final_closed,
      case
        when count(b.ro) = 0 then 0
        else round(coalesce(sum(b.ro_flag_hours), 0)/count(b.ro), 2)
      end as hours_per_ro
    from sad.writer_days a
    left join sad.ros b on a.the_date = b.final_close_date
    group by a.the_date) ee on aa.the_date = ee.the_date  
  left join (
    select a.the_date, coalesce(sum(c.clock_hours), 0) as clock_hours
    from sad.writer_days a
    left join sad.writers b on a.writer_name = b.writer_name
    left join arkona.xfm_pypclockin c on b.employee_number = c.employee_number
      and a.the_date = c.the_date
    group by a.the_date) ff on aa.the_date = ff.the_date) x
where the_date >= current_Date - 90
on conflict(the_date)
do update
  set(ros_opened, labor_sales, parts_sales, discount, gross,
    labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed)
  =
  (excluded.ros_opened, excluded.labor_sales, excluded.parts_sales, excluded.discount, 
    excluded.gross, excluded.labor_hours, excluded.hours_per_ro, excluded.elr, excluded.clock_hours, excluded.ros_final_closed);

-- 6. weekly data --------------------------------------------------------------------------------  
-- writers
insert into sad.writers_weekly  
select writer_name, week_id, week_of, the_week, ros_opened, labor_sales, parts_sales, discount, gross,
    labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed
from (    
  select aa.writer_name, aa.week_id, aa.week_of, aa.the_week, aa.ros_opened, bb.labor_sales, cc.parts_sales, dd.discount, 
    bb.labor_sales + cc.parts_sales - bb.labor_cost - cc.parts_cost - dd.discount as gross,
    ee.labor_hours, ee.hours_per_ro,
    case
      when ee.labor_hours = 0 then 0
      else round(bb.labor_sales/ee.labor_hours, 2) 
    end as elr, ff.clock_hours, ee.ros_final_closed 
  from (
    select a.writer_name, a.week_id, a.week_of, a.the_week, count(b.ro) as ros_opened
    from sad.writer_weeks a
    left join sad.ros b on a.writer_name = b.writer_name
      and a.the_week @> b.open_date
    group by a.writer_name, a.week_id, a.week_of, a.the_week) aa
  left join (
    select a.writer_name, a.week_id, a.week_of, a.the_week, coalesce(sum(labor_sales), 0) as labor_sales,
      coalesce(sum(labor_cost), 0) as labor_cost
    from sad.writer_weeks a
    left join sad.labor_details b on a.writer_name = b.writer_name
      and a.the_week @> b.the_date
    group by a.writer_name, a.week_id, a.week_of, a.the_week) bb on aa.writer_name = bb.writer_name
    and aa.week_id = bb.week_id
  left join (
    select a.writer_name, a.week_id, a.week_of, a.the_week, coalesce(sum(parts_sales), 0) as parts_sales,
      coalesce(sum(parts_cost), 0) as parts_cost
    from sad.writer_weeks a
    left join sad.parts_details b on a.writer_name = b.writer_name
      and a.the_week @> b.the_date
    group by a.writer_name, a.week_id, a.week_of, a.the_week) cc on aa.writer_name = cc.writer_name
    and aa.week_id = cc.week_id    
  left join (
    select a.writer_name, a.week_id, a.week_of, a.the_week, coalesce(sum(discount), 0) as discount
    from sad.writer_weeks a
    left join sad.discount_details b on a.writer_name = b.writer_name
      and a.the_week @> b.the_date
    group by a.writer_name, a.week_id, a.week_of, a.the_week) dd on aa.writer_name = dd.writer_name
    and aa.week_id = dd.week_id     
  left join (
    select a.writer_name, a.week_id, a.week_of, a.the_week, coalesce(sum(b.ro_flag_hours), 0) as labor_hours,
      count(b.ro) as ros_final_closed,
      case
        when count(b.ro) = 0 then 0
        else round(coalesce(sum(b.ro_flag_hours), 0)/count(b.ro), 2)
      end as hours_per_ro
      from sad.writer_weeks a
      left join sad.ros b on a.writer_name = b.writer_name
        and a.the_week @> b.final_close_date
      group by a.writer_name, a.week_id, a.week_of, a.the_week) ee on aa.writer_name = ee.writer_name
        and aa.week_id = ee.week_id
  left join (
    select a.writer_name, a.week_id, a.week_of, a.the_week, coalesce(sum(c.clock_hours), 0) as clock_hours
    from sad.writer_weeks a
    left join sad.writers b on a.writer_name = b.writer_name
    left join arkona.xfm_pypclockin c on b.employee_number = c.employee_number
      and a.the_week @> c.the_date
    group by a.writer_name, a.week_id, a.week_of, a.the_week) ff on aa.writer_name = ff.writer_name
      and aa.week_id = ff.week_id) x
on conflict (week_id,writer_name)
do update
    set(week_of, the_week, ros_opened, labor_sales, parts_sales, discount, gross,
      labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed)
    =
    (excluded.week_of, excluded.the_week, excluded.ros_opened, excluded.labor_sales, 
      excluded.parts_sales, excluded.discount, excluded.gross, excluded.labor_hours, excluded.hours_per_ro, excluded.elr, 
      excluded.clock_hours, excluded.ros_final_closed);

-- shop    
insert into sad.shop_weekly  
select week_id, week_of, the_week, ros_opened, labor_sales, parts_sales, discount, gross,
  labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed
from (
  select aa.week_id, aa.week_of, aa.the_week, aa.ros_opened, bb.labor_sales, cc.parts_sales, dd.discount, 
    bb.labor_sales + cc.parts_sales - bb.labor_cost - cc.parts_cost - dd.discount as gross,
    ee.labor_hours, ee.hours_per_ro,
    case
      when ee.labor_hours = 0 then 0
      else round(bb.labor_sales/ee.labor_hours, 2) 
    end as elr, ff.clock_hours, ee.ros_final_closed 
  from (
    select a.week_id, a.week_of, a.the_week, count(b.ro) as ros_opened
    from sad.writer_weeks a
    left join sad.ros b on a.the_week @> b.open_date
    group by a.week_id, a.week_of, a.the_week) aa
  left join (
    select a.week_id, a.week_of, a.the_week, coalesce(sum(labor_sales), 0) as labor_sales,
      coalesce(sum(labor_cost), 0) as labor_cost
    from sad.writer_weeks a
    left join sad.labor_details b on a.the_week @> b.the_date
    group by a.week_id, a.week_of, a.the_week) bb on aa.week_id = bb.week_id
  left join (
    select a.week_id, a.week_of, a.the_week, coalesce(sum(parts_sales), 0) as parts_sales,
      coalesce(sum(parts_cost), 0) as parts_cost
    from sad.writer_weeks a
    left join sad.parts_details b on a.the_week @> b.the_date
    group by a.week_id, a.week_of, a.the_week) cc on aa.week_id = cc.week_id    
  left join (
    select a.week_id, a.week_of, a.the_week, coalesce(sum(discount), 0) as discount
    from sad.writer_weeks a
    left join sad.discount_details b on a.the_week @> b.the_date
    group by a.week_id, a.week_of, a.the_week) dd on aa.week_id = dd.week_id     
  left join (
    select a.week_id, a.week_of, a.the_week, coalesce(sum(b.ro_flag_hours), 0) as labor_hours,
      count(b.ro) as ros_final_closed,
      case
        when count(b.ro) = 0 then 0
        else round(coalesce(sum(b.ro_flag_hours), 0)/count(b.ro), 2)
      end as hours_per_ro
      from sad.writer_weeks a
      left join sad.ros b on a.the_week @> b.final_close_date
      group by a.week_id, a.week_of, a.the_week) ee on aa.week_id = ee.week_id
  left join (
    select a.week_id, a.week_of, a.the_week, coalesce(sum(c.clock_hours), 0) as clock_hours
    from sad.writer_weeks a
    left join sad.writers b on a.writer_name = b.writer_name
    left join arkona.xfm_pypclockin c on b.employee_number = c.employee_number
      and a.the_week @> c.the_date
    group by a.week_id, a.week_of, a.the_week) ff on aa.week_id = ff.week_id) x
on conflict (week_id)
do update
    set(week_of, the_week, ros_opened, labor_sales, parts_sales, discount, gross,
      labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed)
    =
    (excluded.week_of, excluded.the_week, excluded.ros_opened, excluded.labor_sales, 
      excluded.parts_sales, excluded.discount, excluded.gross, excluded.labor_hours, excluded.hours_per_ro, excluded.elr, 
      excluded.clock_hours, excluded.ros_final_closed);    

-- 6. pay period data --------------------------------------------------------------------------------  
-- writers
insert into sad.writers_pay_periodly
select writer_name, pay_period_id, from_date, thru_date, the_pay_period, 
  ros_opened, labor_sales, parts_sales, discount, gross,
  labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed
from (  
  select aa.writer_name, aa.pay_period_id, aa.from_date, aa.thru_date, aa.the_pay_period, 
    aa.ros_opened, bb.labor_sales, cc.parts_sales, dd.discount, 
    bb.labor_sales + cc.parts_sales - bb.labor_cost - cc.parts_cost - dd.discount as gross,
    ee.labor_hours, ee.hours_per_ro,
    case
      when ee.labor_hours = 0 then 0
      else round(bb.labor_sales/ee.labor_hours, 2) 
    end as elr, ff.clock_hours, ee.ros_final_closed 
  from (
    select a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, count(b.ro) as ros_opened
    from sad.writer_pay_periods a
    left join sad.ros b on a.writer_name = b.writer_name
      and a.the_pay_period @> b.open_date
    group by a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) aa
  left join (
    select a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, coalesce(sum(labor_sales), 0) as labor_sales,
      coalesce(sum(labor_cost), 0) as labor_cost
    from sad.writer_pay_periods a
    left join sad.labor_details b on a.writer_name = b.writer_name
      and a.the_pay_period @> b.the_date
    group by a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) bb on aa.writer_name = bb.writer_name
    and aa.pay_period_id = bb.pay_period_id
  left join (
    select a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, coalesce(sum(parts_sales), 0) as parts_sales,
      coalesce(sum(parts_cost), 0) as parts_cost
    from sad.writer_pay_periods a
    left join sad.parts_details b on a.writer_name = b.writer_name
      and a.the_pay_period @> b.the_date
    group by a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) cc on aa.writer_name = cc.writer_name
    and aa.pay_period_id = cc.pay_period_id    
  left join (
    select a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, coalesce(sum(discount), 0) as discount
    from sad.writer_pay_periods a
    left join sad.discount_details b on a.writer_name = b.writer_name
      and a.the_pay_period @> b.the_date
    group by a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) dd on aa.writer_name = dd.writer_name
    and aa.pay_period_id = dd.pay_period_id     
  left join (
    select a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, coalesce(sum(b.ro_flag_hours), 0) as labor_hours,
      count(b.ro) as ros_final_closed,
      case
        when count(b.ro) = 0 then 0
        else round(coalesce(sum(b.ro_flag_hours), 0)/count(b.ro), 2)
      end as hours_per_ro
      from sad.writer_pay_periods a
      left join sad.ros b on a.writer_name = b.writer_name
        and a.the_pay_period @> b.final_close_date
      group by a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) ee on aa.writer_name = ee.writer_name
        and aa.pay_period_id = ee.pay_period_id
  left join (
    select a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, coalesce(sum(c.clock_hours), 0) as clock_hours
    from sad.writer_pay_periods a
    left join sad.writers b on a.writer_name = b.writer_name
    left join arkona.xfm_pypclockin c on b.employee_number = c.employee_number
      and a.the_pay_period @> c.the_date
    group by a.writer_name, a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) ff on aa.writer_name = ff.writer_name
      and aa.pay_period_id = ff.pay_period_id) x     
on conflict (writer_name,pay_period_id)
do update
  set (from_date, thru_date, the_pay_period, 
    ros_opened, labor_sales, parts_sales, discount, gross,
    labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed) 
  =
  (excluded.from_date, excluded.thru_date, excluded.the_pay_period, 
    excluded.ros_opened, excluded.labor_sales, excluded.parts_sales, excluded.discount, excluded.gross,
    excluded.labor_hours, excluded.hours_per_ro, excluded.elr, excluded.clock_hours, excluded.ros_final_closed);

-- shop
insert into sad.shop_pay_periodly
select pay_period_id, from_date, thru_date, the_pay_period, 
  ros_opened, labor_sales, parts_sales, discount, gross,
  labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed
from (  
  select aa.pay_period_id, aa.from_date, aa.thru_date, aa.the_pay_period, 
    aa.ros_opened, bb.labor_sales, cc.parts_sales, dd.discount, 
    bb.labor_sales + cc.parts_sales - bb.labor_cost - cc.parts_cost - dd.discount as gross,
    ee.labor_hours, ee.hours_per_ro,
    case
      when ee.labor_hours = 0 then 0
      else round(bb.labor_sales/ee.labor_hours, 2) 
    end as elr, ff.clock_hours, ee.ros_final_closed 
  from (
    select a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, count(b.ro) as ros_opened
    from sad.writer_pay_periods a
    left join sad.ros b on a.the_pay_period @> b.open_date
    group by a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) aa
  left join (
    select a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, coalesce(sum(labor_sales), 0) as labor_sales,
      coalesce(sum(labor_cost), 0) as labor_cost
    from sad.writer_pay_periods a
    left join sad.labor_details b on a.the_pay_period @> b.the_date
    group by a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) bb on aa.pay_period_id = bb.pay_period_id
  left join (
    select a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, coalesce(sum(parts_sales), 0) as parts_sales,
      coalesce(sum(parts_cost), 0) as parts_cost
    from sad.writer_pay_periods a
    left join sad.parts_details b on a.the_pay_period @> b.the_date
    group by a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) cc on aa.pay_period_id = cc.pay_period_id    
  left join (
    select a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, coalesce(sum(discount), 0) as discount
    from sad.writer_pay_periods a
    left join sad.discount_details b on a.the_pay_period @> b.the_date
    group by a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) dd on aa.pay_period_id = dd.pay_period_id     
  left join (
    select a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, coalesce(sum(b.ro_flag_hours), 0) as labor_hours,
      count(b.ro) as ros_final_closed,
      case
        when count(b.ro) = 0 then 0
        else round(coalesce(sum(b.ro_flag_hours), 0)/count(b.ro), 2)
      end as hours_per_ro
      from sad.writer_pay_periods a
      left join sad.ros b on a.the_pay_period @> b.final_close_date
      group by a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) ee on aa.pay_period_id = ee.pay_period_id
  left join (
    select a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period, coalesce(sum(c.clock_hours), 0) as clock_hours
    from sad.writer_pay_periods a
    left join sad.writers b on a.writer_name = b.writer_name
    left join arkona.xfm_pypclockin c on b.employee_number = c.employee_number
      and a.the_pay_period @> c.the_date
    group by a.pay_period_id, a.from_date, a.thru_date, a.the_pay_period) ff on aa.pay_period_id = ff.pay_period_id) x     
on conflict (pay_period_id)
do update
  set (from_date, thru_date, the_pay_period, 
    ros_opened, labor_sales, parts_sales, discount, gross,
    labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed) 
  =
  (excluded.from_date, excluded.thru_date, excluded.the_pay_period, 
    excluded.ros_opened, excluded.labor_sales, excluded.parts_sales, excluded.discount, excluded.gross,
    excluded.labor_hours, excluded.hours_per_ro, excluded.elr, excluded.clock_hours, excluded.ros_final_closed);    

-- 6. monthly data --------------------------------------------------------------------------------  
-- writers    
insert into sad.writers_monthly  
select writer_name, year_month, the_month, month_name, 
  ros_opened, labor_sales, parts_sales, discount, gross,
  labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed
from (
  select aa.writer_name, aa.year_month, aa.the_month, aa.month_name, aa.ros_opened, bb.labor_sales, cc.parts_sales, dd.discount, 
    bb.labor_sales + cc.parts_sales - bb.labor_cost - cc.parts_cost - dd.discount as gross,
    ee.labor_hours, ee.hours_per_ro,
    case
      when ee.labor_hours = 0 then 0
      else round(bb.labor_sales/ee.labor_hours, 2) 
    end as elr, ff.clock_hours, ee.ros_final_closed 
  from (
    select a.writer_name, a.year_month, a.the_month, a.month_name, count(b.ro) as ros_opened
    from sad.writer_months a
    left join sad.ros b on a.the_month @> b.open_date and a.writer_name = b.writer_name
    group by a.writer_name, a.year_month, a.the_month, a.month_name) aa
  left join (
    select a.writer_name, a.year_month, a.the_month, a.month_name, coalesce(sum(labor_sales), 0) as labor_sales,
      coalesce(sum(labor_cost), 0) as labor_cost
    from sad.writer_months a
    left join sad.labor_details b on a.the_month @> b.the_date and a.writer_name = b.writer_name
    group by a.writer_name, a.year_month, a.the_month, a.month_name) bb on aa.writer_name = bb.writer_name and aa.year_month = bb.year_month
  left join (
    select a.writer_name, a.year_month, a.the_month, a.month_name, coalesce(sum(parts_sales), 0) as parts_sales,
      coalesce(sum(parts_cost), 0) as parts_cost
    from sad.writer_months a
    left join sad.parts_details b on a.the_month @> b.the_date and a.writer_name = b.writer_name
    group by a.writer_name, a.year_month, a.the_month, a.month_name) cc on aa.writer_name = cc.writer_name and aa.year_month = cc.year_month    
  left join (
    select a.writer_name, a.year_month, a.the_month, a.month_name, coalesce(sum(discount), 0) as discount
    from sad.writer_months a
    left join sad.discount_details b on a.the_month @> b.the_date and a.writer_name = b.writer_name
    group by a.writer_name, a.year_month, a.the_month, a.month_name) dd on aa.writer_name = dd.writer_name and aa.year_month = dd.year_month     
  left join (
    select a.writer_name, a.year_month, a.the_month, a.month_name, coalesce(sum(b.ro_flag_hours), 0) as labor_hours,
      count(b.ro) as ros_final_closed,
      case
        when count(b.ro) = 0 then 0
        else round(coalesce(sum(b.ro_flag_hours), 0)/count(b.ro), 2)
      end as hours_per_ro
      from sad.writer_months a
      left join sad.ros b on a.the_month @> b.final_close_date and a.writer_name = b.writer_name
      group by a.writer_name, a.year_month, a.the_month, a.month_name) ee on aa.writer_name = ee.writer_name and aa.year_month = ee.year_month
  left join (
    select a.writer_name, a.year_month, a.the_month, a.month_name, coalesce(sum(c.clock_hours), 0) as clock_hours
    from sad.writer_months a
    left join sad.writers b on a.writer_name = b.writer_name
    left join arkona.xfm_pypclockin c on b.employee_number = c.employee_number
      and a.the_month @> c.the_date
    group by a.writer_name, a.year_month, a.the_month, a.month_name) ff on aa.writer_name = ff.writer_name and aa.year_month = ff.year_month) x
on conflict (year_month,writer_name)
do update
  set (the_month, month_name, 
    ros_opened, labor_sales, parts_sales, discount, gross,
    labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed)
  =    
  (excluded.the_month, excluded.month_name, 
    excluded.ros_opened, excluded.labor_sales, excluded.parts_sales, excluded.discount, excluded.gross,
    excluded.labor_hours, excluded.hours_per_ro, excluded.elr, excluded.clock_hours, excluded.ros_final_closed);

-- shop    
insert into sad.shop_monthly  
select year_month, the_month, month_name, 
    ros_opened, labor_sales, parts_sales, discount, gross,
    labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed
from (
  select aa.year_month, aa.the_month, aa.month_name, aa.ros_opened, bb.labor_sales, cc.parts_sales, dd.discount, 
    bb.labor_sales + cc.parts_sales - bb.labor_cost - cc.parts_cost - dd.discount as gross,
    ee.labor_hours, ee.hours_per_ro,
    case
      when ee.labor_hours = 0 then 0
      else round(bb.labor_sales/ee.labor_hours, 2) 
    end as elr, ff.clock_hours, ee.ros_final_closed 
  from (
    select a.year_month, a.the_month, a.month_name, count(b.ro) as ros_opened
    from sad.writer_months a
    left join sad.ros b on a.the_month @> b.open_date
    group by a.year_month, a.the_month, a.month_name) aa
  left join (
    select a.year_month, a.the_month, a.month_name, coalesce(sum(labor_sales), 0) as labor_sales,
      coalesce(sum(labor_cost), 0) as labor_cost
    from sad.writer_months a
    left join sad.labor_details b on a.the_month @> b.the_date
    group by a.year_month, a.the_month, a.month_name) bb on aa.year_month = bb.year_month
  left join (
    select a.year_month, a.the_month, a.month_name, coalesce(sum(parts_sales), 0) as parts_sales,
      coalesce(sum(parts_cost), 0) as parts_cost
    from sad.writer_months a
    left join sad.parts_details b on a.the_month @> b.the_date
    group by a.year_month, a.the_month, a.month_name) cc on aa.year_month = cc.year_month    
  left join (
    select a.year_month, a.the_month, a.month_name, coalesce(sum(discount), 0) as discount
    from sad.writer_months a
    left join sad.discount_details b on a.the_month @> b.the_date
    group by a.year_month, a.the_month, a.month_name) dd on aa.year_month = dd.year_month     
  left join (
    select a.year_month, a.the_month, a.month_name, coalesce(sum(b.ro_flag_hours), 0) as labor_hours,
      count(b.ro) as ros_final_closed,
      case
        when count(b.ro) = 0 then 0
        else round(coalesce(sum(b.ro_flag_hours), 0)/count(b.ro), 2)
      end as hours_per_ro
      from sad.writer_months a
      left join sad.ros b on a.the_month @> b.final_close_date
      group by a.year_month, a.the_month, a.month_name) ee on aa.year_month = ee.year_month
  left join (
    select a.year_month, a.the_month, a.month_name, coalesce(sum(c.clock_hours), 0) as clock_hours
    from sad.writer_months a
    left join sad.writers b on a.writer_name = b.writer_name
    left join arkona.xfm_pypclockin c on b.employee_number = c.employee_number
      and a.the_month @> c.the_date
    group by a.year_month, a.the_month, a.month_name) ff on aa.year_month = ff.year_month) x
on conflict (year_month)
do update
  set (the_month, month_name, 
    ros_opened, labor_sales, parts_sales, discount, gross,
    labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed)
  =
  (excluded.the_month, excluded.month_name, 
    excluded.ros_opened, excluded.labor_sales, excluded.parts_sales, excluded.discount, excluded.gross,
    excluded.labor_hours, excluded.hours_per_ro, excluded.elr, excluded.clock_hours, excluded.ros_final_closed);

-- 7. wtd --------------------------------------------------------------------------------      
-- writers
insert into sad.writers_wtd
select the_date, week_id, week_of, writer_name, ros_opened, labor_sales, parts_sales, discount, gross,
  labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed
from (    
  select a.the_date, b.week_id, week_of, a.writer_name, 
    sum(a.ros_opened) over (partition by b.week_id, a.writer_name order by a.the_date asc) as ros_opened,
    sum(a.labor_sales) over (partition by b.week_id, a.writer_name order by a.the_date asc) as labor_sales,
    sum(a.parts_sales) over (partition by b.week_id, a.writer_name order by a.the_date asc) as parts_sales,
    sum(a.discount) over (partition by b.week_id, a.writer_name order by a.the_date asc) as discount,
    sum(a.gross) over (partition by b.week_id, a.writer_name order by a.the_date asc) as gross,
    sum(a.labor_hours) over (partition by b.week_id, a.writer_name order by a.the_date asc) as labor_hours,
    case
      when sum(a.ros_final_closed) over (partition by b.week_id, a.writer_name order by a.the_date asc) = 0 then 0
      else 
        round(
          (sum(a.labor_hours) over (partition by b.week_id, a.writer_name order by a.the_date asc)
          /
          sum(a.ros_final_closed) over (partition by b.week_id, a.writer_name order by a.the_date asc)), 2) 
      end as hours_per_ro,
    case
      when sum(a.labor_hours) over (partition by b.week_id, a.writer_name order by a.the_date asc) = 0 then 0
      else
        round(
          (sum(a.labor_sales) over (partition by b.week_id, a.writer_name order by a.the_date asc)
          /
          sum(a.labor_hours) over (partition by b.week_id, a.writer_name order by a.the_date asc)), 2) 
      end as elr,
    sum(a.clock_hours) over (partition by b.week_id, a.writer_name order by a.the_date asc) as clock_hours,
    sum(a.ros_final_closed) over (partition by b.week_id, a.writer_name order by a.the_date asc) as ros_final_closed
  from sad.writers_daily a
  join sad.writer_weeks b on b.the_week @> a.the_date 
    and a.writer_name = b.writer_name
  order by a.writer_name, the_date) x
on conflict(the_date,writer_name)
do update
  set (week_id, week_of, ros_opened, labor_sales, parts_sales, discount, gross,
    labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed)
  =
  (excluded.week_id, excluded.week_of, excluded.ros_opened, excluded.labor_sales, 
    excluded.parts_sales, excluded.discount, excluded.gross,
    excluded.labor_hours, excluded.hours_per_ro, excluded.elr, excluded.clock_hours, excluded.ros_final_closed);

-- shop    
insert into sad.shop_wtd
select the_date, week_id, week_of, ros_opened, labor_sales, parts_sales, discount, gross,
  labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed
from (  
  select a.the_date, b.week_id, week_of,
    sum(a.ros_opened) over (partition by b.week_id order by a.the_date asc) as ros_opened,
    sum(a.labor_sales) over (partition by b.week_id order by a.the_date asc) as labor_sales,
    sum(a.parts_sales) over (partition by b.week_id order by a.the_date asc) as parts_sales,
    sum(a.discount) over (partition by b.week_id order by a.the_date asc) as discount,
    sum(a.gross) over (partition by b.week_id order by a.the_date asc) as gross,
    sum(a.labor_hours) over (partition by b.week_id order by a.the_date asc) as labor_hours,
    case
      when sum(a.ros_final_closed) over (partition by b.week_id order by a.the_date asc) = 0 then 0
      else 
        round(
          (sum(a.labor_hours) over (partition by b.week_id order by a.the_date asc)
          /
          sum(a.ros_final_closed) over (partition by b.week_id order by a.the_date asc)), 2) 
      end as hours_per_ro,
    case
      when sum(a.labor_hours) over (partition by b.week_id order by a.the_date asc) = 0 then 0
      else
        round(
          (sum(a.labor_sales) over (partition by b.week_id order by a.the_date asc)
          /
          sum(a.labor_hours) over (partition by b.week_id order by a.the_date asc)), 2) 
      end as elr,
    sum(a.clock_hours) over (partition by b.week_id order by a.the_date asc) as clock_hours,
    sum(a.ros_final_closed) over (partition by b.week_id order by a.the_date asc) as ros_final_closed
  from sad.shop_daily a
  join (
    select week_id, week_of, the_week 
    from sad.writer_weeks
    group by week_id, week_of, the_week) b on b.the_week @> a.the_date 
  order by the_date) x
on conflict(the_date)
do update
  set (week_id, week_of, ros_opened, labor_sales, parts_sales, discount, gross,
    labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed)
  =
  (excluded.week_id, excluded.week_of, excluded.ros_opened, excluded.labor_sales, 
    excluded.parts_sales, excluded.discount, excluded.gross,
    excluded.labor_hours, excluded.hours_per_ro, excluded.elr, excluded.clock_hours, excluded.ros_final_closed);    

-- 7. pptd --------------------------------------------------------------------------------      
-- writers   
insert into sad.writers_pptd
select the_date, pay_period_id, from_date, thru_date, writer_name, ros_opened, labor_sales, parts_sales, 
  discount, gross, labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed
from (  
  select a.the_date, pay_period_id, from_date, thru_date, a.writer_name, 
    sum(a.ros_opened) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) as ros_opened,
    sum(a.labor_sales) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) as labor_sales,
    sum(a.parts_sales) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) as parts_sales,
    sum(a.discount) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) as discount,
    sum(a.gross) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) as gross,
    sum(a.labor_hours) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) as labor_hours,
    case
      when sum(a.ros_final_closed) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) = 0 then 0
      else 
        round(
          (sum(a.labor_hours) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc)
          /
          sum(a.ros_final_closed) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc)), 2) 
      end as hours_per_ro,
    case
      when sum(a.labor_hours) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) = 0 then 0
      else
        round(
          (sum(a.labor_sales) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc)
          /
          sum(a.labor_hours) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc)), 2) 
      end as elr,
    sum(a.clock_hours) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) as clock_hours,
    sum(a.ros_final_closed) over (partition by b.pay_period_id, a.writer_name order by a.the_date asc) as ros_final_closed
  from sad.writers_daily a
  join sad.writer_pay_periods b on b.the_pay_period @> a.the_date 
    and a.writer_name = b.writer_name
  order by a.writer_name, the_date) x 
on conflict(writer_name,the_date)
do update
  set (pay_period_id, from_date, thru_date, ros_opened, labor_sales, parts_sales, 
    discount, gross, labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed)
  =
  (excluded.pay_period_id, excluded.from_date, excluded.thru_date, excluded.ros_opened, excluded.labor_sales, 
    excluded.parts_sales, excluded.discount, excluded.gross,
    excluded.labor_hours, excluded.hours_per_ro, excluded.elr, excluded.clock_hours, excluded.ros_final_closed);    

-- shop  
insert into sad.shop_pptd
select the_date, pay_period_id, from_date, thru_date, 
  ros_opened, labor_sales, parts_sales, discount, gross,
  labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed
from (  
  select a.the_date, b.pay_period_id, from_date, thru_date,
    sum(a.ros_opened) over (partition by b.pay_period_id order by a.the_date asc) as ros_opened,
    sum(a.labor_sales) over (partition by b.pay_period_id order by a.the_date asc) as labor_sales,
    sum(a.parts_sales) over (partition by b.pay_period_id order by a.the_date asc) as parts_sales,
    sum(a.discount) over (partition by b.pay_period_id order by a.the_date asc) as discount,
    sum(a.gross) over (partition by b.pay_period_id order by a.the_date asc) as gross,
    sum(a.labor_hours) over (partition by b.pay_period_id order by a.the_date asc) as labor_hours,
    case
      when sum(a.ros_final_closed) over (partition by b.pay_period_id order by a.the_date asc) = 0 then 0
      else 
        round(
          (sum(a.labor_hours) over (partition by b.pay_period_id order by a.the_date asc)
          /
          sum(a.ros_final_closed) over (partition by b.pay_period_id order by a.the_date asc)), 2) 
      end as hours_per_ro,
    case
      when sum(a.labor_hours) over (partition by b.pay_period_id order by a.the_date asc) = 0 then 0
      else
        round(
          (sum(a.labor_sales) over (partition by b.pay_period_id order by a.the_date asc)
          /
          sum(a.labor_hours) over (partition by b.pay_period_id order by a.the_date asc)), 2) 
      end as elr,
    sum(a.clock_hours) over (partition by b.pay_period_id order by a.the_date asc) as clock_hours,
    sum(a.ros_final_closed) over (partition by b.pay_period_id order by a.the_date asc) as ros_final_closed
  from sad.shop_daily a
  join (
    select pay_period_id, from_date, thru_date, the_pay_period 
    from sad.writer_pay_periods
    group by pay_period_id, from_date, thru_date, the_pay_period) b on b.the_pay_period @> a.the_date 
  order by a.the_date) x  
on conflict(the_date)
do update  
  set (pay_period_id, from_date, thru_date, ros_opened, labor_sales, parts_sales, 
    discount, gross, labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed)
  =
  (excluded.pay_period_id, excluded.from_date, excluded.thru_date, excluded.ros_opened, excluded.labor_sales, 
    excluded.parts_sales, excluded.discount, excluded.gross,
    excluded.labor_hours, excluded.hours_per_ro, excluded.elr, excluded.clock_hours, excluded.ros_final_closed);    

-- 7. mtd --------------------------------------------------------------------------------      
-- writers             

insert into sad.writers_mtd
select the_date, year_month, month_name, writer_name,
  ros_opened, labor_sales, parts_sales, discount, gross,
  labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed
from (  
  select a.the_date, year_month, month_name, a.writer_name, 
    sum(a.ros_opened) over (partition by b.year_month, a.writer_name order by a.the_date asc) as ros_opened,
    sum(a.labor_sales) over (partition by b.year_month, a.writer_name order by a.the_date asc) as labor_sales,
    sum(a.parts_sales) over (partition by b.year_month, a.writer_name order by a.the_date asc) as parts_sales,
    sum(a.discount) over (partition by b.year_month, a.writer_name order by a.the_date asc) as discount,
    sum(a.gross) over (partition by b.year_month, a.writer_name order by a.the_date asc) as gross,
    sum(a.labor_hours) over (partition by b.year_month, a.writer_name order by a.the_date asc) as labor_hours,
    case
      when sum(a.ros_final_closed) over (partition by b.year_month, a.writer_name order by a.the_date asc) = 0 then 0
      else 
        round(
          (sum(a.labor_hours) over (partition by b.year_month, a.writer_name order by a.the_date asc)
          /
          sum(a.ros_final_closed) over (partition by b.year_month, a.writer_name order by a.the_date asc)), 2) 
      end as hours_per_ro,
    case
      when sum(a.labor_hours) over (partition by b.year_month, a.writer_name order by a.the_date asc) = 0 then 0
      else
        round(
          (sum(a.labor_sales) over (partition by b.year_month, a.writer_name order by a.the_date asc)
          /
          sum(a.labor_hours) over (partition by b.year_month, a.writer_name order by a.the_date asc)), 2) 
      end as elr,
    sum(a.clock_hours) over (partition by b.year_month, a.writer_name order by a.the_date asc) as clock_hours,
    sum(a.ros_final_closed) over (partition by b.year_month, a.writer_name order by a.the_date asc) as ros_final_closed
  from sad.writers_daily a
  join sad.writer_months b on b.the_month @> a.the_date 
    and a.writer_name = b.writer_name
  order by a.writer_name, the_date) x
on conflict(writer_name,the_date)
do update  
  set (year_month, month_name, 
    ros_opened, labor_sales, parts_sales, discount, gross,
    labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed)
  =
  (excluded.year_month, excluded.month_name, 
    excluded.ros_opened, excluded.labor_sales, excluded.parts_sales, excluded.discount, excluded.gross,
    excluded.labor_hours, excluded.hours_per_ro, excluded.elr, excluded.clock_hours, excluded.ros_final_closed); 

-- shop
insert into sad.shop_mtd
select the_date, year_month, month_name, 
  ros_opened, labor_sales, parts_sales, discount, gross,
  labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed
from (    
select a.the_date, b.year_month, b.month_name,
  sum(a.ros_opened) over (partition by b.year_month order by a.the_date asc) as ros_opened,
  sum(a.labor_sales) over (partition by b.year_month order by a.the_date asc) as labor_sales,
  sum(a.parts_sales) over (partition by b.year_month order by a.the_date asc) as parts_sales,
  sum(a.discount) over (partition by b.year_month order by a.the_date asc) as discount,
  sum(a.gross) over (partition by b.year_month order by a.the_date asc) as gross,
  sum(a.labor_hours) over (partition by b.year_month order by a.the_date asc) as labor_hours,
  case
    when sum(a.ros_final_closed) over (partition by b.year_month order by a.the_date asc) = 0 then 0
    else 
      round(
        (sum(a.labor_hours) over (partition by b.year_month order by a.the_date asc)
        /
        sum(a.ros_final_closed) over (partition by b.year_month order by a.the_date asc)), 2) 
    end as hours_per_ro,
  case
    when sum(a.labor_hours) over (partition by b.year_month order by a.the_date asc) = 0 then 0
    else
      round(
        (sum(a.labor_sales) over (partition by b.year_month order by a.the_date asc)
        /
        sum(a.labor_hours) over (partition by b.year_month order by a.the_date asc)), 2) 
    end as elr,
  sum(a.clock_hours) over (partition by b.year_month order by a.the_date asc) as clock_hours,
  sum(a.ros_final_closed) over (partition by b.year_month order by a.the_date asc) as ros_final_closed
from sad.shop_daily a
join (
  select year_month, month_name, the_month
  from sad.writer_months
  group by year_month, month_name, the_month) b on b.the_month @> a.the_date 
order by a.the_date) x
on conflict(the_date)
do update  
  set (year_month, month_name, 
    ros_opened, labor_sales, parts_sales, discount, gross,
    labor_hours, hours_per_ro, elr, clock_hours, ros_final_closed)
  =
  (excluded.year_month, excluded.month_name, 
    excluded.ros_opened, excluded.labor_sales, excluded.parts_sales, excluded.discount, excluded.gross,
    excluded.labor_hours, excluded.hours_per_ro, excluded.elr, excluded.clock_hours, excluded.ros_final_closed);  

    