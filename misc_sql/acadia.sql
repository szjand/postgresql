﻿-- acadia sales from fncl stmt
create temp table acadia_sales as
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201901 and 201905--------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201905 -------------------------------------------------------------------
      and b.page = 10 and b.line = 25
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
      and f.store = 'RY1') d on c.account = d.gl_account
  where a.post_status = 'Y'

select a.control, d.model_year, d.model_code, d.trim_level, d.drive, d.engine, c.color, delivery_date
from acadia_sales a
left join nc.vehicle_sales b on a.control = b.stock_number
join nc.vehicles c on b.vin = c.vin
join nc.vehicle_configurations d on c.configuration_id = d.configuration_id
order by model_code, trim_level, engine, color, delivery_date

select a.stock_number, b.model_year, b.model_code, b.drive, b.trim_level, b.engine, b.color, a.delivery_date
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
  and b.model = 'acadia'
where a.delivery_Date between '01/01/2019' and current_date
order by model_code, trim_level, engine, color, delivery_date

-- 36
select a.inpmast_Stock_number as "Stock #",d.model_year as year, d.model_code, d.drive, d.trim_level, d.engine, c.color, current_Date - e.ground_date as "age on ground"
from arkona.xfm_inpmast a
join nc.vehicles c on a.inpmast_vin = c.vin
join nc.vehicle_configurations d on c.configuration_id = d.configuration_id
join nc.vehicle_acquisitions e on a.inpmast_stock_number = e.stock_number
where a.current_row
  and a.model = 'acadia'
  and a.status = 'I'
  and a.type_n_u = 'N'
order by model_code, trim_level, engine, color



select * from nc.vehicle_configurations where model_year = 2019 and model = 'acadia'
select * from nc.vehicle_acquisitions

select * 
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
  and b.model_year = 2019
  and b.model = 'acadia'
where delivery_date between '03/01/2019' and '05/31/2019'

 
select *
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
  and b.model_year = 2019
  and b.model = 'acadia'
where delivery_date between '03/01/2019' and '05/31/2019'  


select distinct make, model from arkona.ext_inpmast where inventory_account = '123706'
select distinct make, model from arkona.ext_inpmast where inpmast_sale_account = '1423012'
select * from arkona.xfm_inpmast where inpmast_stock_number = 'G36838'



select a.stock_number, b.vin, b.model_year, b.model_code, b.drive, b.trim_level, b.engine, b.color, a.ground_date, a.thru_date
from nc.vehicle_acquisitions a
join nc.vehicles b on a.vin = b.vin
  and b.model = 'acadia'
where a.thru_date > current_Date - 365

select a.the_date, count(*)
from dds.dim_date a
left join (
  select a.stock_number, b.vin, b.model_year, b.model_code, b.drive, b.trim_level, b.engine, b.color, a.ground_date, a.thru_date
  from nc.vehicle_acquisitions a
  join nc.vehicles b on a.vin = b.vin
    and b.model = 'acadia'
  where a.thru_date > current_Date - 365) aa on a.the_date between aa.ground_date and aa.thru_date
where a.the_date between current_date - 365 and current_date
group by a.the_Date


select *
from nc.open_orders
where alloc_group = 'acadia'


select a.the_date, count(*) as on_ground,/*string_agg(distinct aa.stock_number, ',' order by aa.stock_number),*/ string_agg(distinct bb.stock_number || '-' || bb.sale_type, ',') as sold, string_agg(distinct cc.stock_number || '-' || cc.source, ',') as added
from dds.dim_date a
left join (
  select a.stock_number, b.vin, b.model_year, b.model_code, b.drive, b.trim_level, b.engine, b.color, a.ground_date, a.thru_date
  from nc.vehicle_acquisitions a
  join nc.vehicles b on a.vin = b.vin
    and b.model = 'acadia'
  where a.thru_date > current_Date - 365) aa on a.the_date between aa.ground_date and aa.thru_date
left join (
  select *
  from nc.vehicle_sales a
  join nc.vehicles b on a.vin = b.vin
--     and b.model_year = 2019
    and b.model = 'acadia'
  where delivery_date between current_Date - 365 and current_date) bb  on a.the_date = bb.delivery_date
left join (
  select a.stock_number, a.source, b.vin, a.ground_date, a.thru_date
  from nc.vehicle_acquisitions a
  join nc.vehicles b on a.vin = b.vin
    and b.model = 'acadia'
  where a.thru_date > current_Date - 365) cc on a.the_date = cc.ground_date
where a.the_date between current_date - 365 and current_date
group by a.the_Date



-- this generates the model level graph, all from nc.inventory tables
select x.the_date, x.on_ground, y.added, z.sold 
from (-- inventory
  select a.the_date, count(*) as on_ground
  from dds.dim_date a
  left join (
    select a.stock_number, b.vin, b.model_year, b.model_code, b.drive, b.trim_level, b.engine, b.color, a.ground_date, a.thru_date
    from nc.vehicle_acquisitions a
    join nc.vehicles b on a.vin = b.vin
      and b.model = 'acadia'
    where a.thru_date > current_Date - 365) aa on a.the_date between aa.ground_date and aa.thru_date
  where a.the_date between current_date - 365 and current_date
  group by the_date) x
left join (-- acquisition
  select a.the_date, count(*) as added
  from dds.dim_date a
  join (
    select a.stock_number, a.ground_date
    from nc.vehicle_acquisitions a
    join nc.vehicles b on a.vin = b.vin
      and b.model = 'acadia') aa on a.the_date = aa.ground_date 
  where a.the_date between current_date - 365 and current_date
  group by the_date) y on x.the_date = y.the_date
left join (-- sales
  select a.the_date, count(*) as sold
  from dds.dim_date a
  join (
    select a.stock_number, a.delivery_date
    from nc.vehicle_sales a
    join nc.vehicles b on a.vin = b.vin
      and b.model = 'acadia') bb on a.the_date = bb.delivery_date
  where a.the_date between current_date - 365 and current_date
  group by the_date) z on x.the_date = z.the_date

-- denali
select x.the_date, x.on_ground, z.sold 
from (-- inventory
  select a.the_date, count(*) as on_ground
  from dds.dim_date a
  left join (
    select a.stock_number, b.vin, b.model_year, b.model_code, b.drive, b.trim_level, b.engine, b.color, a.ground_date, a.thru_date
    from nc.vehicle_acquisitions a
    join nc.vehicles b on a.vin = b.vin
      and b.model = 'acadia' and trim_level = 'denali'
    where a.thru_date > current_Date - 365) aa on a.the_date between aa.ground_date and aa.thru_date
  where a.the_date between current_date - 365 and current_date
  group by the_date) x
left join (-- acquisition
  select a.the_date, count(*) as added
  from dds.dim_date a
  join (
    select a.stock_number, a.ground_date
    from nc.vehicle_acquisitions a
    join nc.vehicles b on a.vin = b.vin
      and b.model = 'acadia' and trim_level = 'denali') aa on a.the_date = aa.ground_date 
  where a.the_date between current_date - 365 and current_date
  group by the_date) y on x.the_date = y.the_date
left join (-- sales
  select a.the_date, count(*) as sold
  from dds.dim_date a
  join (
    select a.stock_number, a.delivery_date
    from nc.vehicle_sales a
    join nc.vehicles b on a.vin = b.vin
      and b.model = 'acadia' and trim_level = 'denali') bb on a.the_date = bb.delivery_date
  where a.the_date between current_date - 365 and current_date
  group by the_date) z on x.the_date = z.the_date

-- sle
select x.the_date, x.on_ground, y.added, z.sold 
from (-- inventory
  select a.the_date, count(*) as on_ground
  from dds.dim_date a
  left join (
    select a.stock_number, b.vin, b.model_year, b.model_code, b.drive, b.trim_level, b.engine, b.color, a.ground_date, a.thru_date
    from nc.vehicle_acquisitions a
    join nc.vehicles b on a.vin = b.vin
      and b.model = 'acadia' and trim_level = 'SLE'
    where a.thru_date > current_Date - 365) aa on a.the_date between aa.ground_date and aa.thru_date
  where a.the_date between current_date - 365 and current_date
  group by the_date) x
left join (-- acquisition
  select a.the_date, count(*) as added
  from dds.dim_date a
  join (
    select a.stock_number, a.ground_date
    from nc.vehicle_acquisitions a
    join nc.vehicles b on a.vin = b.vin
      and b.model = 'acadia' and trim_level = 'SLE') aa on a.the_date = aa.ground_date 
  where a.the_date between current_date - 365 and current_date
  group by the_date) y on x.the_date = y.the_date
left join (-- sales
  select a.the_date, count(*) as sold
  from dds.dim_date a
  join (
    select a.stock_number, a.delivery_date
    from nc.vehicle_sales a
    join nc.vehicles b on a.vin = b.vin
      and b.model = 'acadia' and trim_level = 'SLE') bb on a.the_date = bb.delivery_date
  where a.the_date between current_date - 365 and current_date
  group by the_date) z on x.the_date = z.the_date



  -- slt
  select x.the_date, x.on_ground, z.sold 
from (-- inventory
  select a.the_date, count(*) as on_ground
  from dds.dim_date a
  left join (
    select a.stock_number, b.vin, b.model_year, b.model_code, b.drive, b.trim_level, b.engine, b.color, a.ground_date, a.thru_date
    from nc.vehicle_acquisitions a
    join nc.vehicles b on a.vin = b.vin
      and b.model = 'acadia' and trim_level = 'SLT'
    where a.thru_date > current_Date - 365) aa on a.the_date between aa.ground_date and aa.thru_date
  where a.the_date between current_date - 365 and current_date
  group by the_date) x
left join (-- acquisition
  select a.the_date, count(*) as added
  from dds.dim_date a
  join (
    select a.stock_number, a.ground_date
    from nc.vehicle_acquisitions a
    join nc.vehicles b on a.vin = b.vin
      and b.model = 'acadia' and trim_level = 'SLT') aa on a.the_date = aa.ground_date 
  where a.the_date between current_date - 365 and current_date
  group by the_date) y on x.the_date = y.the_date
left join (-- sales
  select a.the_date, count(*) as sold
  from dds.dim_date a
  join (
    select a.stock_number, a.delivery_date
    from nc.vehicle_sales a
    join nc.vehicles b on a.vin = b.vin
      and b.model = 'acadia' and trim_level = 'SLT') bb on a.the_date = bb.delivery_date
  where a.the_date between current_date - 365 and current_date
  group by the_date) z on x.the_date = z.the_date