﻿/*
10/06/21
Good morning Jon!

Could you ever tell me who are top 15 whole sale customers are? I am not sure how to pull that report! 

Thank you,
Dayton Marek 

I guess I’m still new to being able to identify wholesale customers but some names of our wholesale guests are Abra Auto body, 
Braatens quality auto body, Ok Automotive, unique auto body, and Knutson autobody. What I mean by the top fifteen would 
be who does the most business with us with sales. Also if I could get it from January first to current date would be awesome. 
No rush on them. Just whenever you can get to it would be great!
*/

extracted pdpthdr 1/1/20 -> 10/06/21

-- sent to dayton

select sort_name, sum(parts_total)::integer
from arkona.ext_pdpthdr
where sale_type = 'W'
  and trans_date between 20210101 and 20211006
  and sort_name not in ('C O D')
group by sort_name
order by sum(parts_total) desc 
limit 20