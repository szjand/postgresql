﻿/*
Good afternoon Jon!

I was wondering if you could help us out. Can you run a report to show how much oil we have used from June 2021 to current date of Dexos 530 oil and Dexos 020 oil? We need to see what we have used so we can get a better number for GM so we don’t continue to battle this oil shortage our PDQ department continues to run into a weekly basis. The labor op we used for Dexos 5-30 is B530DEX and the labor Op we use for Dexos 0-20 is B020dex. Please let me know if you need anything else from me or not. 

Thank you, 

Dayton Marek
*/

select opcode, description, count(*) as "RO's"
from (
	select ro, open_date, opcode, description 
	from dds.fact_repair_order a
	join (
		select *
		from dds.dim_opcode
		where (
			(description like '%dex%' and description like '%020%')
			or
			(opcode in ('PDQD4','PDQD5','PDQD6','PDQD7','PDQD8','PDQD9','PDQD10','LOFD4','LOFD5','LOFD6','LOFD7','LOFD8','LOFD9','LOFD10')))) b on a.opcode_key = b.opcode_key
	where a.open_date between '06/01/2021' and current_date) c
group by opcode, description	


/*
Whoops my apologies Jon I gave you the wrong op codes. You got all the ones for Dex020 but the Op code for 
Dexos 530 Is PDQD4, PDQD5, PDQD6, PDQD7, PDQD8, PDQD9, PDQD10, LOFD4, LOFD5, LOFD6, LOFD7, LOFD8 , LOFD9, and LOFD10. 
Sorry about that. If I could have you do the same for the op codes I gave you as you did in your last email that would be perfect!

Thank you,
*/

select *
from dds.dim_opcode
where opcode in ('PDQD4','PDQD5','PDQD6','PDQD7','PDQD8','PDQD9','PDQD10')