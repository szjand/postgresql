﻿('16250','118010','143180','184610','1144265','185801',
    '1132590','1144310','1114831')

pay periods encompassing may
4/30 ->5/13
5/14 -> 5/27
5/28 -> 6/10

-- hours and hourly pay (inc ot)
select lastname, employeenumber, distcode, hourlyrate, 
  sum(regularhours + vacationhours + ptohours + holidayhours) AS reg_hours,
  sum(overtimehours) AS ot_hours,
  SUM(hourlyrate * (regularhours + vacationhours + ptohours + holidayhours)) AS reg_pay,
  SUM(hourlyrate * 1.5 * overtimehours) AS ot_pay,
  SUM(hourlyrate * (regularhours + vacationhours + ptohours + holidayhours)) + 
    SUM(hourlyrate * 1.5 * overtimehours) AS total_pay
FROM ads.ext_dds_edwclockhoursfact a
INNER JOIN dds.dim_date b on a.datekey = b.date_key
  AND b.year_month = 201705
INNER JOIN ads.ext_dds_edwemployeedim c on a.employeekey = c.employeekey
  AND c.employeenumber IN ('16250','118010','143180','184610','1144265','185801',
    '1132590','1144310','1114831')
GROUP BY lastname, employeenumber, distcode, hourlyrate
order by employeenumber::integer

-- break out hourly by payperiods
-- hours and hourly pay (inc ot)
select lastname, employeenumber, hourlyrate, 
  sum(regularhours + vacationhours + ptohours + holidayhours) AS reg_hours,
  sum(overtimehours) AS ot_hours,
  SUM(hourlyrate * (regularhours + vacationhours + ptohours + holidayhours)) AS reg_pay,
  SUM(hourlyrate * 1.5 * overtimehours) AS ot_pay,
  SUM(hourlyrate * (regularhours + vacationhours + ptohours + holidayhours)) + 
    SUM(hourlyrate * 1.5 * overtimehours) AS total_pay
FROM ads.ext_dds_edwclockhoursfact a
INNER JOIN dds.dim_date b on a.datekey = b.date_key
  AND b.the_date between '04/30/2017' and '06/10/2017'
INNER JOIN ads.ext_dds_edwemployeedim c on a.employeekey = c.employeekey
  AND c.employeenumber IN ('16250','118010','143180','184610','1144265','185801',
    '1132590','1144310','1114831')
GROUP BY lastname, employeenumber, hourlyrate




create table ads.ext_accrual_history (
  year_month integer not null,
  from_date date,
  thru_date date,
  account citext not null,
  control citext not null,
  amount numeric (12,2),
  constraint ext_accrual_history_pkey primary key (year_month, account, control));

-- accrual history detail with days
select a.*, (thru_date - from_date) + 1 as days
from ads.ext_accrual_history a
where control in ('16250','118010','143180','184610','1144265','185801',
    '1132590','1144310','1114831')
  and year_month > 201703
order by year_month

-- accrual total per month/employee
select a.year_month, a.control, sum(a.amount)
from ads.ext_accrual_history a
where control in ('16250','118010','143180','184610','1144265','185801',
    '1132590','1144310','1114831')
  and year_month > 201703
group by a.year_month, a.control


select year_month, sum(thru_date - from_date + 1) as days, control, sum(amount) as amount
from ads.ext_accrual_history
where control in ('16250','118010','143180','184610','1144265','185801',
    '1132590','1144310','1114831')
group by year_month, control
order by year_month, control

select *
from ads.ext_accrual_history



-- fact_gl transactions
select a.control, b.the_date, 
  extract(year from b.the_date) * 100 + extract(month from b.the_date) as year_month, 
  c.description, d.account, d.account_type, d.description, a.amount
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_gl_description c on a.gl_description_key = c.gl_description_key
inner join fin.dim_account d on a.account_key = d.account_key
where a.post_status = 'Y'
  and b.year_month = 201705
  and a.control = '143180'
order by b.the_date, d.account 

-- just the expense accounts
select a.control, b.the_date, 
  extract(year from b.the_date) * 100 + extract(month from b.the_date) as year_month, 
  c.description, d.account, d.account_type, d.description, a.amount
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_gl_description c on a.gl_description_key = c.gl_description_key
inner join fin.dim_account d on a.account_key = d.account_key
where a.post_status = 'Y'
  and b.year_month = 201705
  and a.control = '143180'
  and account_type = 'Expense'
order by b.the_date, d.account 

-- just the expense accounts
-- totaled by date
-- shows higher than payroll total gross  
select b.the_date, sum(amount)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_gl_description c on a.gl_description_key = c.gl_description_key
inner join fin.dim_account d on a.account_key = d.account_key
where a.post_status = 'Y'
  and b.year_month = 201705
  and a.control = '143180'
  and account_type = 'Expense'
group by the_date




select *
from ( 
  select a.control, b.the_date, 
    extract(year from b.the_date) * 100 + extract(month from b.the_date) as year_month, 
    c.description, d.account, d.account_type, d.description, a.amount
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
  inner join fin.dim_gl_description c on a.gl_description_key = c.gl_description_key
  inner join fin.dim_account d on a.account_key = d.account_key
  where a.post_status = 'Y'
    and b.year_month between 201704 and 201705
    and a.control = '190892') aa
left join (
  select a.*, (thru_date - from_date) + 1 as days
  from ads.ext_accrual_history a
  where control in ('16250','118010','143180','184610','1144265','185801',
      '1132590','1144310','1114831','190892')
    and year_month > 201703) bb on aa.control = bb.control and aa.year_month = bb.year_month and aa.account = bb.account


compare hourly calc for same period as accrual    


select *
from ( -- accrual history detail with days
  select year_month, from_date, thru_date, control, (thru_date - from_date) + 1 as days, sum(amount) as amount
  from ads.ext_accrual_history a
  where control in ('16250','118010','143180','184610','1144265','185801',
      '1132590','1144310','1114831')
    and year_month = 201705
  group by year_month, from_date, thru_date, control, (thru_date - from_date)    ) aa
left join (
  select lastname, employeenumber, hourlyrate, 
    SUM(hourlyrate * (regularhours + vacationhours + ptohours + holidayhours)) + 
      SUM(hourlyrate * 1.5 * overtimehours) AS total_pay
  FROM ads.ext_dds_edwclockhoursfact a
  INNER JOIN dds.dim_date b on a.datekey = b.date_key
    AND b.the_Date between '05/14/2017' and '05/31/2017'
  INNER JOIN ads.ext_dds_edwemployeedim c on a.employeekey = c.employeekey
    AND c.employeenumber IN ('16250','118010','143180','184610','1144265','185801',
      '1132590','1144310','1114831')
  GROUP BY lastname, employeenumber, hourlyrate) bb on aa.control = bb.employeenumber


-- 6/20 try to match jeri's numbers for "set up": actual paid - prior month's accrual + current month's accrual
-- from gl
use lud as first example 143180

pay periods     check date   accrual periods
4/30 ->5/13     5/19          4/16 -> 4/30
5/14 -> 5/27    6/2           5/14 -> 5/31
5/28 -> 6/10    6/16

--pyhshdta
select a.payroll_ending_month, a.payroll_ending_day, a.check_month, a.check_day, a.total_gross_pay,
  b.payroll_start_date, b.payroll_end_date
from arkona.ext_pyhshdta a
left join dds.ext_pyptbdta b on a.payroll_run_number = b.payroll_run_number
  and a.payroll_cen_year = b.payroll_cen_year
  and a.company_number = b.company_number
where a.payroll_cen_year = 117
  and a.employee_ = '143180'

-- fact_gl

-- just the expense accounts
select a.control, b.the_date, 
  extract(year from b.the_date) * 100 + extract(month from b.the_date) as year_month, 
  c.description, d.account, d.account_type, d.description, a.amount
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_gl_description c on a.gl_description_key = c.gl_description_key
inner join fin.dim_account d on a.account_key = d.account_key
where a.post_status = 'Y'
  and b.year_month = 201705
  and a.control = '184610'
  and account_type = 'Expense'
order by b.the_date, d.account 

-- exclude accrual, group by payroll batch & account

select a.control, b.the_date, c.description, d.account, sum(amount)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_gl_description c on a.gl_description_key = c.gl_description_key
inner join fin.dim_account d on a.account_key = d.account_key
where a.post_status = 'Y'
  and b.year_month = 201705
  and a.control = '143180'
  and account_type = 'Expense'
group by a.control, b.the_date, c.description, d.account 

-- ok, based on dist codes
dist    gross/ot    abs
PART    12306B     12406
PTSR    12306C     12406
PTDR    12306D     12406

ignore taxes for now
start with 201705 :: 5/14 -> 5/31

select a.*, (thru_date - from_date) + 1 as days, b.*
from ads.ext_accrual_history a
left join (
  select employeenumber, sum(reg_hours) as reg, sum(ot_hours) as ot, 
    sum(reg_pay) as reg_pay, sum(ot_pay) as ot_pay, sum(total_pay) as total_pay
  -- select *
  from clock_detail
  where the_date between '05/14/2017' and '05/31/2017'
  group by employeenumber) b on a.control = b.employeenumber
where control in ('16250','118010','143180','184610','1144265','185801',
    '1132590','1144310','1114831')
  and year_month = 201705
  and account in ('12306B','12306C','12306D','12406')
order by control::integer

select employeenumber, hourlyrate, sum(reg_hours) as reg_hours, sum(vac_hours) as vac_hours,
  sum(pto_hours) as pto_hours, sum(hol_hours) as hol_hours, sum(ot_hours) as ot_hours,
  sum(reg_pay) as reg_pay, sum(ot_pay) as ot_pay, sum(total_pay) as total_pay
-- select *
from clock_detail
where the_date between '05/14/2017' and '05/31/2017'
group by employeenumber, hourlyrate
order by employeenumber::integer


do a clockhour detail table

drop table if exists clock_detail;
create temp table clock_detail as
select lastname, employeenumber, distcode, hourlyrate, the_date,
  sum(regularhours) as reg_hours, sum(vacationhours) as vac_hours, sum(ptohours) as pto_hours,
  sum(holidayhours) as hol_hours,
  sum(overtimehours) AS ot_hours,
  SUM(hourlyrate * (regularhours + vacationhours + ptohours + holidayhours)) AS reg_pay,
  SUM(hourlyrate * 1.5 * overtimehours) AS ot_pay,
  SUM(hourlyrate * (regularhours + vacationhours + ptohours + holidayhours)) + 
    SUM(hourlyrate * 1.5 * overtimehours) AS total_pay
FROM ads.ext_dds_edwclockhoursfact a
INNER JOIN dds.dim_date b on a.datekey = b.date_key
  AND b.year_month between 201704 and 201706
INNER JOIN ads.ext_dds_edwemployeedim c on a.employeekey = c.employeekey
  AND c.employeenumber IN ('16250','118010','143180','184610','1144265','185801',
    '1132590','1144310','1114831')
GROUP BY lastname, employeenumber, distcode, hourlyrate, the_date;

pay periods     check date   accrual periods
4/30 ->5/13     5/19          4/16 -> 4/30
5/14 -> 5/27    6/2           5/14 -> 5/31
5/28 -> 6/10    6/16

-- 2 employees with more than one hourly rate
select employeenumber
from (
  select employeenumber, hourlyrate
  from clock_detail
  group by employeenumber, hourlyrate) z
group by employeenumber
having count(*) > 1

select lastname, employeenumber, hourlyrate, min(the_date), max(the_date)
from clock_detail
where employeenumber in ('185801','118010')
group by lastname, employeenumber, hourlyrate

  
-- clock detail for May
select employeenumber, hourlyrate, sum(reg_hours) as reg_hours, sum(vac_hours) as vac_hours,
  sum(pto_hours) as pto_hours, sum(hol_hours) as hol_hours, sum(ot_hours) as ot_hours,
  sum(reg_pay) as reg_pay, sum(ot_pay) as ot_pay, sum(total_pay) as total_pay
-- select *
from clock_detail
where the_date between '05/01/2017' and '05/31/2017'
group by employeenumber, hourlyrate
order by employeenumber::integer


-- fact_gl
select a.control, b.the_date, 
  extract(year from b.the_date) * 100 + extract(month from b.the_date) as year_month, 
  c.description, d.account, d.description, a.amount
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_gl_description c on a.gl_description_key = c.gl_description_key
inner join fin.dim_account d on a.account_key = d.account_key
where a.post_status = 'Y'
  and b.year_month = 201705
  and a.control IN ('16250','118010','143180','184610','1144265','185801',
    '1132590','1144310','1114831')
  and d.account in ('12306B','12306C','12306D','12406')
order by control::integer

-- fact_gl
-- 201705
-- accounts 12306B,12306C,12306D,12406
-- grouped by control, description
select a.control, c.description, b.the_date, sum(a.amount)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_gl_description c on a.gl_description_key = c.gl_description_key
inner join fin.dim_account d on a.account_key = d.account_key
where a.post_status = 'Y'
--   and b.year_month = 201705
  and b.the_date between '05/05/2017' and '06/16/2017'
  and a.control IN ('16250','118010','143180','184610','1144265','185801',
    '1132590','1144310','1114831')
  and d.account in ('12306B','12306C','12306D','12406')
group by a.control, c.description, b.the_date 
order by control::integer, b.the_date

select *
from ads.ext_accrual_history
where year_month = 201704
  and account in  ('12306B','12306C','12306D','12406')
  and control IN ('16250','118010','143180','184610','1144265','185801',
    '1132590','1144310','1114831')


select *
from ads.ext_accrual_history
where year_month = 201705
--   and account in  ('12306B','12306C','12306D','12406')
  and control = '16250'





select a.control, b.the_date, account_type,
  extract(year from b.the_date) * 100 + extract(month from b.the_date) as year_month, 
  c.description, d.account, d.description, a.amount
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_gl_description c on a.gl_description_key = c.gl_description_key
inner join fin.dim_account d on a.account_key = d.account_key
where a.post_status = 'Y'
  and b.year_month = 201705
  and a.control IN ('16250')
--   and d.account in ('12306B','12306C','12306D','12406')
  and account_type = 'expense'
order by control::integer, the_date, account

-- 6/29/17
-- got clarification from jeri on how she derived her numbers on the sheet
-- bingo account totals match
select account, sum(amount)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_gl_description c on a.gl_description_key = c.gl_description_key
inner join fin.dim_account d on a.account_key = d.account_key
where a.post_status = 'Y'
  and b.year_month = 201705
   and d.account in ('12306B','12306C','12306D')
group by account   

-- bingo, control total match
select a.control, d.account, sum(a.amount) as amount
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_gl_description c on a.gl_description_key = c.gl_description_key
inner join fin.dim_account d on a.account_key = d.account_key
where a.post_status = 'Y'
  and b.year_month = 201705
  and d.account in ('12306B','12306C','12306D')
  and control IN ('16250','118010','143180','184610','1144265','185801',
    '1132590','1144310','1114831')
group by a.control, d.account
order by d.account, length(a.control), a.control    


-- now the detail for one employee
select a.control, b.the_date, account_type,
  extract(year from b.the_date) * 100 + extract(month from b.the_date) as year_month, 
  c.description, d.account, d.description, a.amount
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_gl_description c on a.gl_description_key = c.gl_description_key
inner join fin.dim_account d on a.account_key = d.account_key
where a.post_status = 'Y'
  and b.year_month = 201705
  and a.control IN ('16250')
  and d.account = '12306B'
  and account_type = 'expense'
order by control::integer, the_date, account


-- 6/20
closing in on a way to look a this stuff
thinking need to snapshot a more complete picture each motnh

select * from clock_detail where employeenumber = '16250'

pay periods     check date   accrual periods
4/30 ->5/13     5/19          4/16 -> 4/30
5/14 -> 5/27    6/2           5/14 -> 5/31
5/28 -> 6/10    6/16


select lastname, employeenumber, hourlyrate, min(the_date) as from_date, max(the_date) as thru_date,
  sum(reg_hours + vac_hours + pto_hours + hol_hours) as reg_hours,
  sum(ot_hours) as ot_hours, sum(reg_pay) as reg_pay, sum(ot_pay) as ot_pay
from clock_detail
where the_date between '04/10/2017' and '04/30/17'
group by lastname, employeenumber, hourlyrate  
order by length(employeenumber), employeenumber


select employeenumber, hourlyrate, min(the_date) as from_date, max(the_date) as thru_date,
  sum(reg_hours + vac_hours + pto_hours + hol_hours) as reg_hours,
  sum(ot_hours) as ot_hours, sum(reg_pay) as reg_pay, sum(ot_pay) as ot_pay
from clock_detail
where the_date between '05/14/2017' and '05/31/17'  
group by employeenumber, hourlyrate  
order by length(employeenumber), employeenumber


-- seems like i need to get the fucking paycheck in here
-- what is the difference betwen that and accounting|?

-- base_pay
select *
from arkona.ext_pyhshdta
where payroll_cen_year = 117
  and employee_ = '16250'

select * from dds.dim_date limit 10
-- to get the run number on biweekly paychecks
-- selected_pay_per = 'B'
-- payroll_start_date coincides with dds.dim_date.biweekly_pay_period_start_date
select *
from dds.ext_pyptbdta
where payroll_cen_year = 117
  and company_number = 'ry1'
  and payroll_run_number in (
    select payroll_run_number
    from arkona.ext_pyhshdta
    where payroll_cen_year = 117
      and employee_ = '16250')
order by payroll_Start_date      

-- overtime: code_id = 'OVT'  
select *
from arkona.ext_pyhscdta
where employee_number = '16250'
  and payroll_cen_year = 117
  and payroll_run_number = 224170




select *
from arkona.ext_pyhshdta
where payroll_cen_year = 117
  and employee_ = '16250'


-- payroll run #, check date, actual pay period dates

select company_number, a.payroll_run_number, 
  (select * from dds.db2_integer_to_date(payroll_start_date)) as pay_period_from,
  b.biweekly_pay_period_end_date as pay_period_thru,
  (select * from dds.db2_integer_to_date(check_date)) as check_date
from dds.ext_pyptbdta a
inner join (
  select biweekly_pay_period_start_date, biweekly_pay_period_end_date
  from dds.dim_date b
  group by biweekly_pay_period_start_date, biweekly_pay_period_end_date) b 
    on (select * from dds.db2_integer_to_date(a.payroll_start_date)) = b.biweekly_pay_period_start_date
where a.payroll_cen_year = 117
  and a.company_number = 'ry1'
  and a.payroll_run_number in (
    select payroll_run_number
    from arkona.ext_pyhshdta
    where payroll_cen_year = 117
      and employee_ = '16250')
order by a.payroll_Start_date   


-- 7/13 getting back to this, don't remember where i left off
-- looks like i am still struggling to get the narative
/*
lets try to put it in english
for 201705, danielle (16250), shows 2667.98 in account 12306B, Dan shows, based on clock hours that she earned 2220.40
which is a difference of 447.58
why the difference??

so, let's try to stay focused on these facts (don't worry about payroll info for now)

i'm jumping ahead but i believe i have discovered the discrepancy:
  dan did time clock adjustments on the morning of 5/1
  for daniel it amounted to 38.49 hours total
  dds would not see that until 5/2
yep that was it
but still, need a formalized framework for analysis of accrual (finally)
  
*/

-- the accounting consists of checks issued in the month, current month accrual and reversal of previous month accrual:
select a.control, b.the_date, account_type,
  c.description, d.account, d.description, a.amount
-- select sum(a.amount)  -- 2667.98
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_gl_description c on a.gl_description_key = c.gl_description_key
inner join fin.dim_account d on a.account_key = d.account_key
where a.post_status = 'Y'
  and b.year_month = 201705
  and a.control IN ('1144265')
  and d.account = '12306B'
  and account_type = 'expense'
order by control::integer, the_date, account

-- need to know both accrual periods
select * 
from ads.ext_accrual_history
where year_month in (201705,201704)
  and control = '1144265'
  and account = '12306b'


