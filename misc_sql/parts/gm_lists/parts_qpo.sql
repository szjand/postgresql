﻿general info
to verify counts in dealertrack:
	10: Parts in Inventory
	enter part number
	fx 6: Demand by Month


09/28/21
Jon,
Can you compare this list with what we show in dealer track . Just the parts on hand that show we stock the part.. I would like to see o.h. , o.o. , b.o., and monthly sales
Thanks
Mark
file: air  transmission filters qpo eligible parts listing.xlsx

04/09/21
Jon,

Can you compare this list with our inventory and give me a list showing on hand, on order, and yearly sales?

Mark

filename: lighting qpo eligible part list_non-cpvp_3-22-21.xlsx
single tabe  with 2282 part numbers

/*
04/26/21
Jon,
Can you compare the p/n’s on this list with our inventory and show me qty on hand, on order, and monthly sales?

Thanks
Mark
file: 20210426 - All Air Filters QPO Eligible Parts Listing Version 2

Jon,
Can you compare the p/n’s on this list with our stock and show me on hand, on order , and Monthly sales.

Thanks
Mark
20210426-q2 engine components qpo buyer's guide


*/
/*
4/7/23
Jon,
 Can you compare the attached parts list to our inventory and give me a list of the parts we stock along with a monthly sales history?
 air conditioning qpo eligible parts.xlsx 

Mark
*/

just populate the table from the spreadsheet
04/26/21
no need for multiple tables, this one, with a single field: part_number is fine

alter table jon.parts_lighting_qpo
rename to parts_qpo;

truncate jon.parts_qpo;

select count(*) from jon.parts_qpo;

select * from jon.parts_qpo limit 10

drop table if exists usage;
create temp table usage as
select a.part_number, b.part_description,b.cost, b.qty_on_hand, 
  b.qty_on_order, b.qty_on_back_order,
--   sum(case when d.year_month = 202109 then ptqty else 0 end) as sep,
--   sum(case when d.year_month = 202108 then ptqty else 0 end) as aug,
--   sum(case when d.year_month = 202107 then ptqty else 0 end) as jul,
--   sum(case when d.year_month = 202106 then ptqty else 0 end) as jun,
--   sum(case when d.year_month = 202105 then ptqty else 0 end) as may,
--   sum(case when d.year_month = 202104 then ptqty else 0 end) as apr,
--   sum(case when d.year_month = 202103 then ptqty else 0 end) as mar,
--   sum(case when d.year_month = 202102 then ptqty else 0 end) as feb,
--   sum(case when d.year_month = 202101 then ptqty else 0 end) as jan,
--   sum(case when d.year_month = 202012 then ptqty else 0 end) as dec,
--   sum(case when d.year_month = 202011 then ptqty else 0 end) as nov,
--   sum(case when d.year_month = 202010 then ptqty else 0 end) as oct

  sum(case when d.year_month = 202303 then ptqty else 0 end) as mar,
  sum(case when d.year_month = 202302 then ptqty else 0 end) as feb,
  sum(case when d.year_month = 202301 then ptqty else 0 end) as jan,
  sum(case when d.year_month = 202212 then ptqty else 0 end) as dec,
  sum(case when d.year_month = 202211 then ptqty else 0 end) as nov,
  sum(case when d.year_month = 202210 then ptqty else 0 end) as oct,
  sum(case when d.year_month = 202209 then ptqty else 0 end) as sep,
  sum(case when d.year_month = 202208 then ptqty else 0 end) as aug,
  sum(case when d.year_month = 202207 then ptqty else 0 end) as jul,
  sum(case when d.year_month = 202206 then ptqty else 0 end) as jum,
  sum(case when d.year_month = 202205 then ptqty else 0 end) as may,
  sum(case when d.year_month = 202204 then ptqty else 0 end) as apr
    
from jon.parts_qpo a
left join arkona.ext_pdpmast b on a.part_number = b.part_number
  and b.company_number = 'RY1'
left join arkona.ext_pdptdet c on a.part_number = c.ptpart
  and c.ptco_ = 'RY1'
  and c.ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
  -- 11/23 modified filter on ptsoep
--   and c.ptsoep is null
  and (c.ptsoep is null or c.ptsoep in ('N', 'E'))
inner join dds.dim_date d on (select arkona.db2_integer_to_date_long(c.ptdate)) = d.the_date
where d.year_month between 202204 and 202303 -----------------------------
  and b.status = 'A'  -- per mark's request 05/13/21 If you can just print out the ones that show active stock status, that would help.
group by a.part_number, b.part_description, b.cost, b.qty_on_hand, b.qty_on_order, b.qty_on_back_order;

select * from usage

to test in usage in dealertrack
parts -> 10 -> enter part number -> 6 (demand)
