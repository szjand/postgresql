﻿/*
John,
Can you compare this list to our inventory and show what #’s we stock and monthly sales for the last year if possible.
 Gm is having a sale on these for a couple more weeks.
Thanks

Mark

12/8/20
*/
4 tabs of part numbers, include them all, there may be some overlap



-- drop table if exists jon.parts_brakes_qpo;
-- create table jon.parts_brakes_qpo (
--   part_number citext primary key);

select count(*) from jon.parts_brakes_qpo;  -- 12495


  
insert into jon.parts_brakes_qpo
select *
from jon.parts_brakes_qpo_tmp a
where not exists (
  select 1
  from jon.parts_brakes_qpo
  where part_number = a.part_number);


drop table if exists jon.parts_brakes_qpo_tmp;
create table jon.parts_brakes_qpo_tmp (
  part_number citext);


-- from the file misc_sql/parts/gm_lists/parts_marks_gm_list.sql
-- this returns 233 rows
drop table if exists usage;
create temp table usage as
select a.part_number, b.part_description, b.cost, b.qty_on_hand, 
  b.qty_on_order, b.qty_on_back_order,
  sum(case when d.year_month = 202012 then ptqty else 0 end) as dec,
  sum(case when d.year_month = 202011 then ptqty else 0 end) as nov,
  sum(case when d.year_month = 202010 then ptqty else 0 end) as oct,
  sum(case when d.year_month = 202009 then ptqty else 0 end) as sep,
  sum(case when d.year_month = 202008 then ptqty else 0 end) as aug,
  sum(case when d.year_month = 202007 then ptqty else 0 end) as jul,
  sum(case when d.year_month = 202006 then ptqty else 0 end) as jun,
  sum(case when d.year_month = 202005 then ptqty else 0 end) as may,
  sum(case when d.year_month = 202004 then ptqty else 0 end) as apr,
  sum(case when d.year_month = 202003 then ptqty else 0 end) as mar,
  sum(case when d.year_month = 202002 then ptqty else 0 end) as feb,
  sum(case when d.year_month = 202001 then ptqty else 0 end) as jan
from jon.parts_brakes_qpo a
left join arkona.ext_pdpmast b on a.part_number = b.part_number
  and b.company_number = 'RY1'
left join arkona.ext_pdptdet c on a.part_number = c.ptpart
  and c.ptco_ = 'RY1'
  and c.ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
  -- 11/23 modified filter on ptsoep
--   and c.ptsoep is null
  and (c.ptsoep is null or c.ptsoep in ('N', 'E'))
inner join dds.dim_date d on (select arkona.db2_integer_to_date_long(c.ptdate)) = d.the_date
where d.year_month between 202001 and 202012
group by a.part_number, b.part_description, b.cost, b.qty_on_hand, 
  b.qty_on_order, b.qty_on_back_order;

select *
from usage;


select count(*)
from jon.parts_brakes_qpo a
join arkona.ext_pdpmast b on a.part_number = b.part_number

