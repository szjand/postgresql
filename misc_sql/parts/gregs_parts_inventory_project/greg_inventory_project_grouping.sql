﻿/*
9/18/19
jon:
How would you like me to calculate parts days supply for different groupings?
greg:
Compare against last 60 days cost of sales
jon:
Could you be more explicit?
Maybe actual formulas, I really don’t understand your initial reply.
greg:
You pick a time frame for sales 60 or 90 days. We’ll go with 60.

Sum up the parts sales by category (stocking group, source, etc.) for the last 60 days. Divide that 
sum by 60 to give you a daily sales rate by category.

Use cost in your sum, not sale price, because we value the inventory in accounting at cost.

Now take the current inventory in the category and divide by the above daily sales rate number and 
you have your current days supply in the category (based on 60 days sales history).

*/

select * 
from arkona.ext_pdpsgrp
where company_number = 'RY1'

-- active parts with at least one in stock
drop table if exists parts cascade;
create temp table parts as
select a.company_number, a.manufacturer, a.part_number, a.part_description, a.cost, 
  a.qty_on_hand, a.qty_on_order, a.qty_on_back_order, a.stocking_group, a.group_code,
  b.description, c.stock_code, c.stock_level
from arkona.ext_pdpmast a
join arkona.ext_pdpsgrp b on a.company_number = b.company_number
  and a.stocking_group = b.stock_group
join arkona.ext_pdppmex c on a.company_number = c.company_number -- stock_code 02 = RIM
  and a.manufacturer = c.manufacturer
  and a.part_number = c.part_number
where a.company_number = 'RY1'
  and a.status = 'A' -- Active
  and a.qty_on_hand >= 1;
create index on parts(part_number);  
create index on parts(company_number); 

select count(*) from parts: 12533

select stocking_group, count(*) -- 15 stocking_groups
from parts
group by stocking_group

select stocking_group, description, count(*) 
from parts
group by stocking_group, description

select group_code, count(*) -- 877 group codes
from parts
group by group_code

select stock_code, count(*) -- 9 stock_codes
from parts
group by stock_code



-- Sum up the parts sales by category (stocking group, source, etc.) for the last 60 days. Divide that 
-- sum by 60 to give you a daily sales rate by category.

drop table if exists sales_detail ;
create temp table sales_detail as
select a.ptqty, b.part_number, b.part_description, b.cost, b.stocking_group, 
  b.description as sg_description, b.group_code, b.stock_code
from arkona.ext_pdptdet a
join parts b on a.ptco_ = b.company_number 
  and a.ptpart = b.part_number
where arkona.db2_integer_to_date_long(a.ptdate) between current_date - 61 and current_date -1
  and a.ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR'); 
create index on sales_detail(stocking_group);
create index on sales_detail(sg_description);
create index on sales_detail(group_code);
create index on sales_detail(stock_code);





-- stocking_group
select b.*, a.sales_60, round(qty_on_hand * 1.0/sales_60, 2) as "qty_on_hand/sales_60",
  round((qty_on_hand + qty_on_order)* 1.0/sales_60, 2) as "(qty_on_hand + qty_on_order)/sales_60"
from (
  select stocking_group, sg_description, sum(ptqty * cost)::integer as sales_60
  from sales_detail
  group by stocking_group, sg_description) a
join (
  select stocking_group, description, sum(qty_on_hand * cost)::integer as qty_on_hand,
    sum(qty_on_order * cost)::integer as qty_on_order, sum(qty_on_back_order * cost)::integer as qty_on_back_order
  from parts 
  group by stocking_group, description) b on a.stocking_group = b.stocking_group
order by a.stocking_group

-- stock_code
select b.*, a.sales_60, round(qty_on_hand * 1.0/sales_60, 2) as "qty_on_hand/sales_60",
  round((qty_on_hand + qty_on_order)* 1.0/sales_60, 2) as "(qty_on_hand + qty_on_order)/sales_60"
from (
  select stock_code, sum(ptqty * cost)::integer as sales_60
  from sales_detail
  group by stock_code) a
join (
  select stock_code, sum(qty_on_hand * cost)::integer as qty_on_hand,
    sum(qty_on_order * cost)::integer as qty_on_order, sum(qty_on_back_order * cost)::integer as qty_on_back_order
  from parts 
  group by stock_code) b on a.stock_code = b.stock_code
order by a.stock_code

-- group code
select b.*, a.sales_60, round(qty_on_hand * 1.0/sales_60, 2) as "qty_on_hand/sales_60",
  round((qty_on_hand + qty_on_order)* 1.0/sales_60, 2) as "(qty_on_hand + qty_on_order)/sales_60"
from (
  select group_code, sum(ptqty * cost)::integer as sales_60
  from sales_detail
  group by group_code
  having sum(ptqty * cost)::integer <> 0) a
join (
  select group_code, sum(qty_on_hand * cost)::integer as qty_on_hand,
    sum(qty_on_order * cost)::integer as qty_on_order, sum(qty_on_back_order * cost)::integer as qty_on_back_order
  from parts 
  group by group_code) b on a.group_code = b.group_code
order by a.group_code

-- left 2 of group code
select b.*, a.sales_60, round(qty_on_hand * 1.0/sales_60, 2) as "qty_on_hand/sales_60",
  round((qty_on_hand + qty_on_order)* 1.0/sales_60, 2) as "(qty_on_hand + qty_on_order)/sales_60"
from (
  select left(group_code, 2) as gc, sum(ptqty * cost)::integer as sales_60
  from sales_detail
  group by left(group_code, 2)
  having sum(ptqty * cost)::integer <> 0) a
join (
  select left(group_code, 2) as gc, sum(qty_on_hand * cost)::integer as qty_on_hand,
    sum(qty_on_order * cost)::integer as qty_on_order, sum(qty_on_back_order * cost)::integer as qty_on_back_order
  from parts 
  group by left(group_code, 2)) b on a.gc = b.gc
order by a.gc

-- left 3 of group code
select b.*, a.sales_60, round(qty_on_hand * 1.0/sales_60, 2) as "qty_on_hand/sales_60",
  round((qty_on_hand + qty_on_order)* 1.0/sales_60, 2) as "(qty_on_hand + qty_on_order)/sales_60"
from (
  select left(group_code, 3) as gc, sum(ptqty * cost)::integer as sales_60
  from sales_detail
  group by left(group_code, 3)
  having sum(ptqty * cost)::integer <> 0) a
join (
  select left(group_code, 3) as gc, sum(qty_on_hand * cost)::integer as qty_on_hand,
    sum(qty_on_order * cost)::integer as qty_on_order, sum(qty_on_back_order * cost)::integer as qty_on_back_order
  from parts 
  group by left(group_code, 3)) b on a.gc = b.gc
order by a.gc

/*
interesting conversation with mark 
group_code is very relevant, both in its entirety but perhaps even more so
be the left 2 digits, these are the major parts categories that are the 
same accross all vehicles, with some variation in trucks
*/

select group_code, left(group_code, 2), count(*)
from arkona.ext_pdpmast
group by group_code, left(group_code, 2)
order by group_code, left(group_code, 2)

select * 
from arkona.ext_pdpmast
where left(group_code, 3) = '028'


select sum(qty_on_hand*cost)
from parts
where stocking_group = '502'

select sum(ptqty*cost)
from sales_detail
where stocking_group = '502'



-- 9/19
greg:
  It appears you’re reporting pieces. If you are, run it with dollars please.

  Pieces don’t work for categories. 


-- Sum up the parts sales by category (stocking group, source, etc.) for the last 60 days. Divide that 
-- sum by 60 to give you a daily sales rate by category.
-- Now take the current inventory in the category and divide by the above daily sales rate number and 
-- you have your current days supply in the category (based on 60 days sales history).


-- stocking_group
select b.*, a.daily_sales_rate, 
  round(b.on_hand/a.daily_sales_rate, 1) as "on_hand / daily_sales_rate",
  round((b.on_hand + b.on_order)/a.daily_sales_rate, 1) as "on_hand + on order / daily_sales_rate"
from (
  select stocking_group, sg_description, round(sum(ptqty * cost)/60, 1) as daily_sales_rate
  from sales_detail
  group by stocking_group, sg_description) a
join (
  select stocking_group, description, sum(qty_on_hand * cost)::integer as on_hand,
    sum(qty_on_order * cost)::integer as on_order, sum(qty_on_back_order * cost)::integer as on_back_order
  from parts 
  group by stocking_group, description) b on a.stocking_group = b.stocking_group
order by a.stocking_group  

select sum(qty_on_hand), sum(qty_on_hand * cost) from parts where stocking_group = '502'

select sum(ptqty), sum(ptqty * cost) from sales_detail where stocking_group = '502'

-- stock_code
select b.*, a.daily_sales_rate, 
  round(b.on_hand/a.daily_sales_rate, 1) as "on_hand / daily_sales_rate",
  round((b.on_hand + b.on_order)/a.daily_sales_rate, 1) as "on_hand + on order / daily_sales_rate"
from (
  select stock_code, null as description, round(sum(ptqty * cost)/60, 1) as daily_sales_rate
  from sales_detail
  group by stock_code) a
join (
  select stock_code, null as description, sum(qty_on_hand * cost)::integer as on_hand,
    sum(qty_on_order * cost)::integer as on_order, sum(qty_on_back_order * cost)::integer as on_back_order
  from parts 
  group by stock_code) b on a.stock_code = b.stock_code
order by a.stock_code    


-- left 2 of group code
select b.*, a.daily_sales_rate, 
  round(b.on_hand/a.daily_sales_rate, 1) as "on_hand / daily_sales_rate",
  round((b.on_hand + b.on_order)/a.daily_sales_rate, 1) as "on_hand + on order / daily_sales_rate"
from (
  select left(group_code, 2) as gc, null as description, round(sum(ptqty * cost)/60, 1) as daily_sales_rate
  from sales_detail
  group by left(group_code, 2)) a
join (
  select left(group_code, 2) as gc, null as description, sum(qty_on_hand * cost)::integer as on_hand,
    sum(qty_on_order * cost)::integer as on_order, sum(qty_on_back_order * cost)::integer as on_back_order
  from parts 
  group by left(group_code, 2)) b on a.gc = b.gc
order by a.gc 