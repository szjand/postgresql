﻿/*  PDPTDET */
PTCODE - Transaction Code
AP = Add Part
BO = Back Ordered
CM = Comment
CN = Cancelled
CP = RO Customer Pay Sale
CQ = Converted Quantity
CR = RO Correction
CS = RO Cause
DC = Discounts
DP = Delete Part
FR = Factory Return
GC = Stock Group Change
IA = Manual Inventory Adjustment
IS = RO Internal Sale
LA = Lifo Adjust
LS = Lost Sale
MP = Merged Part
OF = Fees
OR = Ordered
PA = Physical Inventory Adjust
PC = Part count from last physical inventory
PO = Purchase order
RC = Special Order Receipt
RC = Received
RO = Reorder
RS = Restocking charge
RT = Counter Return
SA = Counter Sale
SC = RO Service Contract Sale
SH = Shipping
SL = Sublet
SR = RO Return
TT = RO Tech Timedec
WS = RO Warranty sale
ZA = assign core part
ZR = remove core part

per open track:
SOStatus: ptsoep	Status of sale of the part on the transaction
'Blank or Empty' = Part was sold out of inventory
S = Special Order
E = Emergency Purchase
N = Negative on Hand
H = Hold
X = Part was removed from transaction




---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
gregs inventory project
pdppmex.pmstcd : stock code, 02 = RIM
pdpmast.pmstat : status A = active
pdppmex.pmstkl : RIM level

pdpmrpl

pdpsgrp: stocking groups
select min(ptdate), max(ptdate) from arkona.ext_pdptdet


select b.part_number, b.part_description, b.cost, b.qty_on_hand, 
  b.qty_on_order, b.qty_on_back_order,
  sum(case when d.year_month = 201901 then ptqty else 0 end) as jan,
  sum(case when d.year_month = 201902 then ptqty else 0 end) as feb,
  sum(case when d.year_month = 201903 then ptqty else 0 end) as mar,
  sum(case when d.year_month = 201904 then ptqty else 0 end) as apr,
  sum(case when d.year_month = 201905 then ptqty else 0 end) as may
from arkona.ext_pdpmast b
join arkona.ext_pdptdet c on b.part_number = c.ptpart
  and c.ptco_ = 'RY1'
  and c.ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
  and (c.ptsoep is null or c.ptsoep in ('N', 'E'))
join dds.dim_date d on (select arkona.db2_integer_to_date_long(c.ptdate)) = d.the_date
where d.year_month between 201901 and 201905
  and b.company_number = 'RY1'
  and b.status = 'A' -- Active
  and b.qty_on_hand > 10
group by b.part_number, b.part_description, b.cost, b.qty_on_hand, 
  b.qty_on_order, b.qty_on_back_order
limit 100  

create index on arkona.ext_pdptdet(ptpart);

create index on arkona.ext_pdppmex(company_number);
create index on arkona.ext_pdppmex(manufacturer);
create index on arkona.ext_pdppmex(part_number);

create index on arkona.ext_pdpsgrp(company_number);
create index on arkona.ext_pdpsgrp(stock_group);

select * from dds.ext_qsys2_syscolumns where column_text like '%stock code%'

drop table if exists parts cascade;
create temp table parts as
select a.company_number, a.manufacturer, a.part_number, a.part_description, a.cost, 
  a.qty_on_hand, a.qty_on_order, a.qty_on_back_order, a.stocking_group, 
  b.description, c.stock_code, c.stock_level
from arkona.ext_pdpmast a
join arkona.ext_pdpsgrp b on a.company_number = b.company_number
  and a.stocking_group = b.stock_group
join arkona.ext_pdppmex c on a.company_number = c.company_number
  and a.manufacturer = c.manufacturer
  and a.part_number = c.part_number
where a.company_number = 'RY1'
  and a.status = 'A' -- Active
  and a.qty_on_hand > 5;
create index on parts(part_number);  
create index on parts(company_number); 
create index on parts(manufacturer);
-- with 
--   parts as (
--     select a.part_number, a.part_description, a.cost, a.qty_on_hand, a.qty_on_order, 
--       a.qty_on_back_order, a.stocking_group
--     from arkona.ext_pdpmast a
--     where a.company_number = 'RY1'
--       and a.status = 'A' -- Active
--       and a.qty_on_hand > 10)  
-- this query based on what i used to generate usage for mark on gm lists
-- explain analyze
select b.part_number, b.part_description, b.cost, b.qty_on_hand, 
  b.qty_on_order, b.qty_on_back_order, b.stocking_group, b.description,
  max(case when b.stock_code = '02' then b.stock_level else null end) as rim_stk_level,
  sum(a.ptqty) filter (where a.ptdate between 20190501 and 20190531) as may_19,
  sum(a.ptqty) filter (where a.ptdate between 20190401 and 20190430) as apr_19,
  sum(a.ptqty) filter (where a.ptdate between 20190301 and 20190331) as mar_19,
  sum(a.ptqty) filter (where a.ptdate between 20190201 and 20190228) as feb_19,
  sum(a.ptqty) filter (where a.ptdate between 20190101 and 20190131) as jan_19,
  sum(a.ptqty) filter (where a.ptdate between 20181201 and 20181231) as dec_18,
  sum(a.ptqty) filter (where a.ptdate between 20181101 and 20181130) as nov_18,
  sum(a.ptqty) filter (where a.ptdate between 20181001 and 20181031) as oct_18,
  sum(a.ptqty) filter (where a.ptdate between 20180901 and 20180930) as sep_18,
  sum(a.ptqty) filter (where a.ptdate between 20180801 and 20180831) as aug_18,
  sum(a.ptqty) filter (where a.ptdate between 20180701 and 20180731) as jul_18,
  sum(a.ptqty) filter (where a.ptdate between 20180601 and 20180630) as jun_18
from arkona.ext_pdptdet a
join parts b on a.ptco_ = b.company_number 
  and a.ptpart = b.part_number
where a.ptdate between 20180601 and 20190531
  and a.ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
group by b.part_number, b.part_description, b.cost, b.qty_on_hand, 
  b.qty_on_order, b.qty_on_back_order, b.stocking_group, b.description



  