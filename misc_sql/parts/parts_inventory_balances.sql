﻿drop table if exists parts_accounts;
create temp table parts_accounts as
    select a.fxmpge as page, a.fxmlne::integer as line, b.g_l_acct_number as account
    from arkona.ext_eisglobal_sypffxmst a
    inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
      and a.fxmcyy = b.factory_financial_year
    where a.fxmcyy = 2017
      and trim(a.fxmcde) = 'GM'
      and coalesce(b. consolidation_grp, 'RY1') <> '3'
      and b.factory_code = 'GM'
      and trim(b.factory_account) <> '331A'
      and b.consolidation_grp is null
      and b.g_l_acct_number <> ''
      and a.fxmpge = 1
      and a.fxmlne between 27 and 30
      and a.fxmcol = 3;

-- balances from glpmast
select line, sum(beginning_balance)::integer as beg_bal, sum(jan_balance01)::integer as jan, sum(feb_balance02)::integer as feb,
  sum(mar_balance03)::integer as mar, sum(apr_balance04)::integer as apr, sum(may_balance05)::integer as may, sum(jun_balance06)::integer as jun,
  sum(jul_balance07)::integer as jul, sum(aug_balance08)::integer as aug, sum(sep_balance09)::integer as sep, sum(oct_balance10)::integer as oct,
  sum(nov_balance11)::integer as nov, sum(dec_balance12)::integer as dec
from arkona.ext_glpmast a
inner join parts_Accounts b on a.account_number = b.account
  and year = 2017
group by line  
union
select 99, sum(beginning_balance)::integer as beg_bal, sum(jan_balance01)::integer as jan, sum(feb_balance02)::integer as feb,
  sum(mar_balance03)::integer as mar, sum(apr_balance04)::integer as apr, sum(may_balance05)::integer as may, sum(jun_balance06)::integer as jun,
  sum(jul_balance07)::integer as jul, sum(aug_balance08)::integer as aug, sum(sep_balance09)::integer as sep, sum(oct_balance10)::integer as oct,
  sum(nov_balance11)::integer as nov, sum(dec_balance12)::integer as dec
from arkona.ext_glpmast a
inner join parts_Accounts b on a.account_number = b.account
  and year = 2017
order by line

-- monthly looks good
select x.*,
  sum(bal) over (order by year_month)
from (
  select 0 as year_month, 3419663 as bal
  union
  select b.year_month, sum(amount)::integer
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join parts_accounts d on c.account = d.account
  where post_status = 'Y'
    and b.the_year = 2017
  group by b.year_month) x
order by year_month

-- weekly
select x.*,
  sum(bal) over (order by iso_week)
from (
  select 0 as iso_week, 3419663 as bal
  union
  select b.iso_week, sum(amount)::integer
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join parts_accounts d on c.account = d.account
  where post_status = 'Y'
    and b.the_year = 2017
  group by b.iso_week) x
order by iso_week

select * from dds.dim_date limit 10

-- daily
select x.*,
  sum(bal) over (order by day_of_year)
from (
  select 0 as day_of_year, 3419663 as bal
  union
  select b.day_of_year, sum(amount)::integer
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join parts_accounts d on c.account = d.account
  where post_status = 'Y'
    and b.the_year = 2017
  group by b.day_of_year) x
order by day_of_year