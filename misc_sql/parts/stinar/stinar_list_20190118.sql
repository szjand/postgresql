﻿/*

Jon-

Are you able to take this list of powertrain part numbers and scrub them against our sales history?  We are not interested in WHO, just a 12 month history of that particular part number’s sale quantity.    Column D has the part number, wondering if you can just get me a unit sale quantity for the last 12 months in place it in column E?  

If I was looking for this info, it would be under “Parts in Inventory.”  After that part number is put in the top column, I would press enter and then function 6-showing the 12 month demand.  I can do it individually, but wondering if there is an automated process you can run. 


Example-


Column D             Column E             

19333281                  3
19303201                  0

*/

---------------------------------------------------------------------------------
new list for dan, based on the make query: /home/jon/Desktop/sql/parts/gm_lists/parts_marks_gm_list.sql
---------------------------------------------------------------------------------
fresh extract for arkona tables: E:\python projects\ext_arkona
arkona.ext_pdpmast.py: full scrape
arkona.ext_pdptdet.py
  select max(ptdate) from arkona.ext_pdptdet  -- 20181018
  generates the dates for the extract  
        select the_date 
        from (
          select *, the_year * 10000 + the_month * 100 + the_day as the_date
          from (
          select * from generate_series(2018, 2019, 1) as the_year) a
          cross join (
          select * from generate_series(1, 12, 1) as the_month) b
          cross join (
          select * from generate_series(1, 31, 1) as the_day) c) d
        where the_date between 20181001 and 20190118
        order by the_year,the_month,the_day;  

dans spreadsheet is goofy
a single column of data in column D with empty cells
i am guessing that is what he wants back

create table jon.stinar_201901 (
  part_number citext,
  seq integer);

insert into jon.stinar_201901 values('',1);
insert into jon.stinar_201901 values('',2);
insert into jon.stinar_201901 values('19333281',3);
insert into jon.stinar_201901 values('19303201',4);
...
insert into jon.stinar_201901 values('24283281',235);
insert into jon.stinar_201901 values('24284070',236);


select * from jon.stinar_201901

-- freaked out
-- 19333281 not close compared to arkona monthly demand
-- found out arkona included the old part number which i discovered to be 19417111
-- (in aqt, table pdpmrpl)
-- now closer, but dan wants total demand, so i got rid of the ptcode list
-- and just eliminated codes on an attrition basis to get as close as i could
-- to arkona results (using both part numbers)
-- dan is ok with just doing transactions on the specific part numbers


select *
from arkona.ext_pdptdet
where ptdate between 20181201 and 20181210
and ptpart = '19333281'


select e.*, sum(quantity) over (partition by year_month)
from (
  select d.year_month, c.ptcode, sum(c.ptqty) as quantity
  from jon.stinar_201901 a
  left join arkona.ext_pdpmast b on a.part_number = b.part_number
    and b.company_number = 'RY1'
  left join arkona.ext_pdptdet c on a.part_number = c.ptpart
    and c.ptco_ = 'RY1'
    and c.ptcode not in ('or','dp', 'rc','bo','ia','la','pc')
  --   and c.ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
    and (c.ptsoep is null or c.ptsoep in ('N', 'E'))
  inner join dds.dim_date d on (select arkona.db2_integer_to_date_long(c.ptdate)) = d.the_date  
  -- where d.the_date between '01/15/2018' and '01/15/2019'  
  where d.year_month between 201802 and 201901
    and a.part_number in ('19331650')
  group by d.year_month, c.ptcode) e

select * from arkona.ext_pdptdet where ptpart = '19209698' and ptdate between 20180601 and 20180630

-- this looks good 
-- talked to dan
  select d.year_month, sum(c.ptqty) as quantity
  from jon.stinar_201901 a
  left join arkona.ext_pdpmast b on a.part_number = b.part_number
    and b.company_number = 'RY1'
  left join arkona.ext_pdptdet c on a.part_number = c.ptpart
    and c.ptco_ = 'RY1'
    and c.ptcode not in ('or','dp', 'rc','bo','ia','la','pc')
  --   and c.ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
    and (c.ptsoep is null or c.ptsoep in ('N', 'E'))
  inner join dds.dim_date d on (select arkona.db2_integer_to_date_long(c.ptdate)) = d.the_date  
  -- where d.the_date between '01/15/2018' and '01/15/2019'  
  where d.year_month between 201802 and 201901
    and a.part_number in ('19333281','19417111')
  group by d.year_month

-- lets try a couple more part numbers
-- looks ok
select d.year_month, sum(c.ptqty) as quantity
from jon.stinar_201901 a
left join arkona.ext_pdpmast b on a.part_number = b.part_number
  and b.company_number = 'RY1'
left join arkona.ext_pdptdet c on a.part_number = c.ptpart
  and c.ptco_ = 'RY1'
  and c.ptcode not in ('or','dp', 'rc','bo','ia','la','pc','cn')
--   and c.ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
  and (c.ptsoep is null or c.ptsoep in ('N', 'E'))
inner join dds.dim_date d on (select arkona.db2_integer_to_date_long(c.ptdate)) = d.the_date  
-- where d.the_date between '01/15/2018' and '01/15/2019'  
where d.year_month between 201802 and 201901
  and a.part_number in ('19209698')
group by d.year_month


-- and now the spreadsheet for dan
select aa.part_number, aa.seq, 
  case
    when aa.part_number = '' then null
    else coalesce(bb.quantity, 0)
  end
from jon.stinar_201901 aa
left join (
select a.part_number, sum(c.ptqty) as quantity
from jon.stinar_201901 a
left join arkona.ext_pdpmast b on a.part_number = b.part_number
  and b.company_number = 'RY1'
left join arkona.ext_pdptdet c on a.part_number = c.ptpart
  and c.ptco_ = 'RY1'
  and c.ptcode not in ('or','dp', 'rc','bo','ia','la','pc','pa')
--   and c.ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
  and (c.ptsoep is null or c.ptsoep in ('N', 'E'))
inner join dds.dim_date d on (select arkona.db2_integer_to_date_long(c.ptdate)) = d.the_date  
-- where d.the_date between '01/15/2018' and '01/15/2019'  
where d.year_month between 201802 and 201901
group by a.part_number) bb on aa.part_number = bb.part_number
order by seq









PTCODE - Transaction Code
AP = Add Part
BO = Back Ordered
CM = Comment
CN = Cancelled
CP = RO Customer Pay Sale
CQ = Converted Quantity
CR = RO Correction
CS = RO Cause
DC = Discounts
DP = Delete Part
FR = Factory Return
GC = Stock Group Change
IA = Manual Inventory Adjustment
IS = RO Internal Sale
LA = Lifo Adjust
LS = Lost Sale
MP = Merged Part
OF = Fees
OR = Ordered
PA = Physical Inventory Adjust
PC = Part count from last physical inventory
PO = Purchase order
RC = Special Order Receipt
RC = Received
RO = Reorder
RS = Restocking charge
RT = Counter Return
SA = Counter Sale
SC = RO Service Contract Sale
SH = Shipping
SL = Sublet
SR = RO Return
TT = RO Tech Time
WS = RO Warranty sale
ZA = assign core part
ZR = remove core part