﻿select part_number, group_code, part_description, count(*) past_year
from arkona.ext_pdpmast a
join arkona.ext_pdptdet c on a.part_number = c.ptpart
  and c.ptco_ = 'RY1'
  and c.ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
  and (c.ptsoep is null or c.ptsoep in ('N', 'E'))
inner join dds.dim_date d on (select arkona.db2_integer_to_date_long(c.ptdate)) = d.the_date
-- where d.year_month between 202203 and 202302  
where d.the_date between '06/17/2022' and '06/18/2023'
  and group_code in ( '16005', '10027')
  and status = 'A'
group by company_number, part_number, group_code, part_description  
order by a.part_number