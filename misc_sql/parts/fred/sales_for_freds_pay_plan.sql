﻿/*
12/30/21
Hey John hope you are doing well.  So Randy and I are building a new pay plan for me. 
We are hoping you can get the parts ro sales and internal sales numbers for 2019, 2020, 2021 for each month. 
*/

select a.*
from (
	select 
		extract(year from arkona.db2_integer_to_date(trans_date)) as the_year,
		extract(month from arkona.db2_integer_to_date(trans_date)) as the_month,
		sum(parts_total)::integer as total
	-- select * 
	from arkona.ext_pdpthdr
	where sale_type = 'I'
		and company_number = 'RY1'
		and extract(year from arkona.db2_integer_to_date(trans_date)) between 2019 and 2021
	group by   extract(year from arkona.db2_integer_to_date(trans_date)),
		extract(month from arkona.db2_integer_to_date(trans_date))) a


drop table if exists jon.fred_ros;
create table jon.fred_ros (ro citext primary key);
insert into jon.fred_ros
select ro
from dds.fact_repair_order 
where close_date between '01/01/2019' and '12/31/2021'
	and store_code = 'ry1'
group by ro;

drop table if exists test_1;
create temp table test_1 as
select 
	extract(year from arkona.db2_integer_to_date(a.ptdate)) as the_year,
	extract(month from arkona.db2_integer_to_date(a.ptdate)) as the_month,
	sum(ptqty*ptlist)::integer as total
from arkona.ext_pdptdet a
join jon.fred_ros b on a.ptinv_ = b.ro
where arkona.db2_integer_to_date(a.ptdate) between '01/01/2019' and '12/31/2021'
group by 	extract(year from arkona.db2_integer_to_date(a.ptdate)),
	extract(month from arkona.db2_integer_to_date(a.ptdate))

select b.ro, a.*
from arkona.ext_pdptdet a
left join jon.fred_ros b on a.ptinv_ = b.ro
where arkona.db2_integer_to_date(a.ptdate) between '01/01/2019' and '12/31/2021'	
  and a.ptinv_ like '16%'
limit 200



-- sent to fred on 1/15/22 as ro_and_internal_sales_2019-2021
select a.the_year, c.month_name, a.total as ro_sales, b.total as internal_sales, a.total + b.total as total_sales
from test_1 a
join (
	select a.*
	from (
		select 
			extract(year from arkona.db2_integer_to_date(trans_date)) as the_year,
			extract(month from arkona.db2_integer_to_date(trans_date)) as the_month,
			sum(parts_total)::integer as total
		-- select * 
		from arkona.ext_pdpthdr
		where sale_type = 'I'
			and company_number = 'RY1'
			and extract(year from arkona.db2_integer_to_date(trans_date)) between 2019 and 2021
		group by   extract(year from arkona.db2_integer_to_date(trans_date)),
			extract(month from arkona.db2_integer_to_date(trans_date))) a) b on a.the_year = b.the_year and a.the_month = b.the_month
join (		
	select the_year, month_of_year, month_name
	from dds.dim_date
	where the_year between 2019 and 2021
	group by the_year, month_of_year, month_name) c on a.the_year = c.the_year and a.the_month = c.month_of_year