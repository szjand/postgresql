﻿drop table if exists jon.fred_part_numbers cascade;
create table jon.fred_part_numbers (
  part_number citext);


insert into jon.fred_part_numbers values('88864603');
insert into jon.fred_part_numbers values('88866184');
...

select a.*, b.part_Description, b.cost, b.list_price 
from jon.fred_part_numbers a
left join arkona.ext_pdpmast b on a.part_number = b.part_number
order by b.part_number
  