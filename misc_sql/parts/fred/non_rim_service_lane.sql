﻿general info
to verify counts in dealertrack:
	10: Parts in Inventory
	enter part number
	fx 6: Demand by Month

SL in description = service lane
N: not service lane

pdopmex.stock_code: 02 = RIM
/*
Hi John I was hoping you could help me out with something. I am hoping you  can generate a list of non RIM service lane parts with sales history.  
I am trying to figure out what service lane parts that are not on RIM that we should be stocking. 
If you have any questions just email me back or my phone number is 701-330-2041 for my cell.  
Thank you John for your help I really appreciate it.

Thanks again 
Fred 
*/


select * 
from arkona.ext_pdpmast
where company_number = 'RY1'
  and stocking_group = '506'
limit 1000 

select * from arkona.ext_pdpsgrp order by description

select * from arkona.ext_pdppmex limit 100



select a.company_number, a.manufacturer, a.part_number, a.part_description, a.cost, 
  a.qty_on_hand, a.qty_on_order, a.qty_on_back_order, a.stocking_group, a.group_code,
  b.description, c.stock_code, c.stock_level
from arkona.ext_pdpmast a
join arkona.ext_pdpsgrp b on a.company_number = b.company_number
  and a.stocking_group = b.stock_group
join arkona.ext_pdppmex c on a.company_number = c.company_number -- stock_code 02 = RIM
  and a.manufacturer = c.manufacturer
  and a.part_number = c.part_number
  and c.stock_code <> '02'
where a.company_number = 'RY1'
  and a.status = 'A' -- Active
  and a.part_description like 'SL%'


-- active service lane parts not in RIM, with any sales in the last year
-- sent to fred on 10/04/21
select *
from ( 
	select aa.part_number, aa.part_description,aa.cost, aa.qty_on_hand, aa.qty_on_order, aa.qty_on_back_order,
		sum(case when bb.year_month = 202109 then bb.ptqty else 0 end) as sep,
		sum(case when bb.year_month = 202108 then bb.ptqty else 0 end) as aug,
		sum(case when bb.year_month = 202107 then bb.ptqty else 0 end) as jul,
		sum(case when bb.year_month = 202106 then bb.ptqty else 0 end) as jun,
		sum(case when bb.year_month = 202105 then bb.ptqty else 0 end) as may,
		sum(case when bb.year_month = 202104 then bb.ptqty else 0 end) as apr,
		sum(case when bb.year_month = 202103 then bb.ptqty else 0 end) as mar,
		sum(case when bb.year_month = 202102 then bb.ptqty else 0 end) as feb,
		sum(case when bb.year_month = 202101 then bb.ptqty else 0 end) as jan,
		sum(case when bb.year_month = 202012 then bb.ptqty else 0 end) as dec,
		sum(case when bb.year_month = 202011 then bb.ptqty else 0 end) as nov,
		sum(case when bb.year_month = 202010 then bb.ptqty else 0 end) as oct,
		sum(bb.ptqty) as total
	from ( -- the parts
		select a.part_number, a.part_description,a.cost, a.qty_on_hand, a.qty_on_order, a.qty_on_back_order
		from arkona.ext_pdpmast a 
		join arkona.ext_pdppmex b on a.company_number = b.company_number -- stock_code 02 = RIM
			and a.manufacturer = b.manufacturer
			and a.part_number = b.part_number
			and b.stock_code <> '02'
		where a.status = 'A'
			and a.part_description like 'SL%') aa
	left join ( -- the demand
		select a.ptpart, a.ptqty, b.year_month
		from arkona.ext_pdptdet a
		join dds.dim_date b on (select arkona.db2_integer_to_date_long(a.ptdate)) = b.the_date
			and b.year_month between 202010 and 202109
		where a.ptco_ = 'RY1'
			and a.ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
			and (a.ptsoep is null or a.ptsoep in ('N', 'E'))) bb on aa.part_number = bb.ptpart 
	group by aa.part_number, aa.part_description,aa.cost, aa.qty_on_hand, aa.qty_on_order, aa.qty_on_back_order) cc
where coalesce(total, 0) > 0

