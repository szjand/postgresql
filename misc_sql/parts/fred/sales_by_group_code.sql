﻿/*
12/08/21
Hey John, hope you are doing well. I am wondering if you would be able to help me out. I am trying to find a list that 
shows sales by group numbers if that is possible. So like cabin air filters are in group 9786 and air filters are in group 3410.  
What I am trying to figure out is the most common wiper blades, cabin air filter, and air filters, so we can stock some over f
or midas so we just have to bill them and they can keep stocked up like they do the oil filters.  Let me know if this 
is something you can help me with. The group numbers for the parts are as follow
Cabin air filter  9786
Air filter   3410
Wiper blades 16062 or 10136 depending on the application. 

Thank you John 

Fred 
*/


select b.group_code, b.part_number, b.part_description, count(*)
from arkona.ext_pdptdet a
join arkona.ext_pdpmast b on a.ptpart = b.part_number
  and b.group_code in ('03410','09786','16062','10146')
where ptdate between 20210901 and 20211208
group by b.group_code, b.part_number, b.part_description
order by b.group_code asc, count(*) desc, b.part_number asc
