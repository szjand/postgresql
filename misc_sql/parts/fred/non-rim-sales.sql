﻿-- -- 07/24/2023
-- Hey Jon, hope all is well. Wondering if we can try and run another report for non-rim parts in the GM source. 
-- Lets start with the top 100 sales. We now have a new source for us to put the active non-rim parts in a 
-- certain source so we can run a stock order for them more easily.  Any questions please feel free to 
-- contact me so i can clear up what i am asking for
-- thank you!
-- Fred Van Heste II

-- 06/21/2023
select a.part_number, a.part_description, count(*) past_year_sales
from rydedata.pdpmast a
join rydedata.pdptdet c on a.part_number = c.ptpart
  and c.ptco# = 'RY1'
  and c.ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
  and (c.ptsoep is null or c.ptsoep in ('N', 'E'))
where c.ptdate between 20220724 and 20230723  ------------------------------------------------------------
  and a.status = 'A'
  and not exists (
    select 1
    from rydedata.ry1rimsnd -- assuming this is the definitive RIM list
    where part_number = a.part_number)
group by a.part_number, a.part_description  
order by count(*) desc
-- limit 50
limit 100

select count(*) from rydedata.pdppmex a join rydedata.ry1rimsnd b on a.part_number = b.part_number

turns out i did another query using pdppmex with stock_code = 02 as the rim indicator


select * from rydedata.ry1rimsnd order by part_number