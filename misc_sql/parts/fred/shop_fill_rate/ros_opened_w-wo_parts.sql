﻿/*
Hey John hope you had a good New Years. I am needing help to see if there is a better way of tracking “fill demand”.  
I see in dealertrack there is a demand fill analysis, but it doesn’t give me what I am really looking for and I don’t 
think those numbers are very accutrate to what I am trying to track. I am trying to track of all the ROS that where 
open that day how many needed parts and how many did we have all parts in stock, and which ones did we have to order anything. 
I don’t know if this is going to be possible but I want to try and figure it out with your help. The way they have had 
previous managers track it is just making a “tick” sheet of everytime someone needs to order a part that is a fail fill rate. 
I would like to be able to look into what part had to be ordered on what RO,  so then I can look to see why we didn’t have 
the part in stock and I can either adjust our stock policy or see if it was on B/O. I have looked all over the reports 
and anlysis I can see and nothing breaks it down. I have a feeling to find the true “demand fill rate” we are going to 
have to look at a couple of metrics, such as, how many ROS where open that day, then how many of those ROs needed parts 
and then from that how many ROs how many parts we had to order on those ROs that parts needed to be ordered. 
Thank you John for all your help it is always appreciated. 

01/12/22
  fred thinks a list of ros opened on a given date, with whether it has parts or not is a good starting place
  had to limit by writer, pdq and detail use 16 ros

01/23/2022
Hope you are doing well John. Could I start to get that that service to list from you for every Tuesday. 
The all service Ros opened and closed that Tuesday and then which ones have parts. 
If I could get that email from you by Wednesday morning?  Thanks let me know if you have any questions 

Thanks
Fred 
  
*/

03/02/22
Thanks John, I think you can hold off on sending me this list for now. We are exploring other options and learning new things 


-- 01/24/2022
convert this into a script that runs on wednesday morning generating a spreadsheet and emailing it to fred
found that i was mising a lot of parts that are in pdppdet but not you in pdptdet, makes sense, open ros

drop table if exists ros; 
create temp table ros as  -- 65
select distinct ro, open_date-- , b.writer_name, b.default_service_type, b.writer_number
from dds.fact_repair_order a
join dds.dim_service_writer b on a.service_writer_key = b.service_writer_key
  and b.default_service_type = 'MR'
  and b.writer_number not in ('745')
where open_date = current_date - 1
  and ro like '16%'
-- order by b.writer_name  
order by ro;
create index on ros(ro);


-- ok, this looks like it works for adding the special order attribute
select aaa.*, 
  case
    when bbb.part_numbers is null then null
    else 'has parts'
  end as parts, ccc.ordered
from ros aaa
left join (
	select aa.*,  row_number() over (partition by ro order by arr_len desc) as seq
	from (
		select b.document_number as ro, array_agg(distinct c.part_number) as part_numbers, array_length(array_agg(distinct c.part_number), 1) as arr_len
		from ros a
		join arkona.ext_pdpphdr b on a.ro = b.document_number
		join arkona.ext_pdppdet c on b.pending_key = c.pending_key
			and c.part_number is not null
		group by b.document_number
		union
		select ptinv_, array_agg(distinct b.ptpart) as parts, array_length(array_agg(distinct b.ptpart), 1)
		from ros a
		join arkona.ext_pdptdet b on a.ro = b.ptinv_
		group by ptinv_) aa) bbb on aaa.ro = bbb.ro 
      and bbb.seq = 1	
left join (
	select b.document_number, c.order_type, 'ordered' as ordered
	from ros a
	join arkona.ext_pdpphdr b on a.ro = b.document_number
	join arkona.ext_pdppdet c on b.pending_key = c.pending_key
		and c.part_number is not null
		and c.order_type = 'S'
	union    
	select a.ro, b.ptsoep, 'ordered' as ordered
	from ros a
	join arkona.ext_pdptdet b on a.ro = b.ptinv_
		and b.ptsoep = 'S') ccc on aaa.ro = ccc.document_number
order by aaa.ro

-- 
-- -- looks good could union the 2
-- -- definitely need to include pdppdet
-- select * 
-- from (
-- 	select * 
-- 	from ros aa
-- 	left join (
-- 		select b.document_number, array_agg(distinct c.part_number) -- count(*) as parts
-- 		from ros a
-- 		join arkona.ext_pdpphdr b on a.ro = b.document_number
-- 		join arkona.ext_pdppdet c on b.pending_key = c.pending_key
-- 			and c.part_number is not null
-- 		group by b.document_number) bb on aa.ro = bb.document_number) aaa
-- full outer join (
-- 	select * 
-- 	from ros aa
-- 	left join (
-- 		select ptinv_, array_agg(distinct b.ptpart) as parts
-- 		from ros a
-- 		join arkona.ext_pdptdet b on a.ro = b.ptinv_
-- 		group by ptinv_) bb on aa.ro = bb.ptinv_ ) bbb on aaa.ro = bbb.ro
-- order by aaa.ro	
-- 
-- 
-- 
-- -- lets look at union
-- -- this does it
-- select aaa.*, bbb.part_numbers 
-- from ros aaa
-- left join (
-- 	select aa.*,  row_number() over (partition by ro order by arr_len desc) as seq
-- 	from (
-- 		select b.document_number as ro, array_agg(distinct c.part_number) as part_numbers, array_length(array_agg(distinct c.part_number), 1) as arr_len
-- 		from ros a
-- 		join arkona.ext_pdpphdr b on a.ro = b.document_number
-- 		join arkona.ext_pdppdet c on b.pending_key = c.pending_key
-- 			and c.part_number is not null
-- 		group by b.document_number
-- 		union
-- 		select ptinv_, array_agg(distinct b.ptpart) as parts, array_length(array_agg(distinct b.ptpart), 1)
-- 		from ros a
-- 		join arkona.ext_pdptdet b on a.ro = b.ptinv_
-- 		group by ptinv_) aa) bbb on aaa.ro = bbb.ro 
--       and bbb.seq = 1	
-- order by aaa.ro
-- 
-- 
-- 
-- 
-- --01/25 not satisfied with what i am seeing, send fred the "option 1" same old spread sheet
-- -- ask him for feedback on which if any were ordered
-- -- for now, he just wants ro, open date, has parts, ordered
-- select aaa.*, 
--   case
--     when bbb.part_numbers is null then null
--     else 'has parts'
--   end as parts
-- from ros aaa
-- left join (
-- 	select aa.*,  row_number() over (partition by ro order by arr_len desc) as seq
-- 	from (
-- 		select b.document_number as ro, array_agg(distinct c.part_number) as part_numbers, array_length(array_agg(distinct c.part_number), 1) as arr_len
-- 		from ros a
-- 		join arkona.ext_pdpphdr b on a.ro = b.document_number
-- 		join arkona.ext_pdppdet c on b.pending_key = c.pending_key
-- 			and c.part_number is not null
-- 		group by b.document_number
-- 		union
-- 		select ptinv_, array_agg(distinct b.ptpart) as parts, array_length(array_agg(distinct b.ptpart), 1)
-- 		from ros a
-- 		join arkona.ext_pdptdet b on a.ro = b.ptinv_
-- 		group by ptinv_) aa) bbb on aaa.ro = bbb.ro 
--       and bbb.seq = 1	
-- order by aaa.ro
-- 
-- -- ok, this looks like it works for adding the special order attribute
-- select aaa.*, 
--   case
--     when bbb.part_numbers is null then null
--     else 'has parts'
--   end as parts, ccc.ordered
-- from ros aaa
-- left join (
-- 	select aa.*,  row_number() over (partition by ro order by arr_len desc) as seq
-- 	from (
-- 		select b.document_number as ro, array_agg(distinct c.part_number) as part_numbers, array_length(array_agg(distinct c.part_number), 1) as arr_len
-- 		from ros a
-- 		join arkona.ext_pdpphdr b on a.ro = b.document_number
-- 		join arkona.ext_pdppdet c on b.pending_key = c.pending_key
-- 			and c.part_number is not null
-- 		group by b.document_number
-- 		union
-- 		select ptinv_, array_agg(distinct b.ptpart) as parts, array_length(array_agg(distinct b.ptpart), 1)
-- 		from ros a
-- 		join arkona.ext_pdptdet b on a.ro = b.ptinv_
-- 		group by ptinv_) aa) bbb on aaa.ro = bbb.ro 
--       and bbb.seq = 1	
-- left join (
-- 	select b.document_number, c.order_type, 'ordered' as ordered
-- 	from ros a
-- 	join arkona.ext_pdpphdr b on a.ro = b.document_number
-- 	join arkona.ext_pdppdet c on b.pending_key = c.pending_key
-- 		and c.part_number is not null
-- 		and c.order_type = 'S'
-- 	union    
-- 	select a.ro, b.ptsoep, 'ordered' as ordered
-- 	from ros a
-- 	join arkona.ext_pdptdet b on a.ro = b.ptinv_
-- 		and b.ptsoep = 'S') ccc on aaa.ro = ccc.document_number
-- order by aaa.ro
-- 
-- 
-- 
-- 2/2/22 so far, have been manually updating the spreadsheet from has parts to has parts/ordered
-- based on these 2 queries, in my spare time need to incorporate these queries into the main query
-- -- fred would like to see if the part was ordered, ie, a special order
-- -- pdppdet.order_type: S = special order
--   select b.document_number, c.order_type
--   from ros a
--   join arkona.ext_pdpphdr b on a.ro = b.document_number
--   join arkona.ext_pdppdet c on b.pending_key = c.pending_key
--     and c.part_number is not null
--     and c.order_type = 'S'
-- order by b.document_number, c.order_type   
-- -- pdptdet.ptsoep has 3 values: null, S & N
-- -- S shows as spec ord on the ro (16504190)
-- -- N shows nothing on the ro (16504094)
-- explain analyze --(format json)
-- select a.ro, b.ptsoep
-- from ros a
-- join arkona.ext_pdptdet b on a.ro = b.ptinv_
--   and b.ptsoep = 'S'
-- 
-- 
-- create index on arkona.ext_pdptdet(ptinv_,ptsoep)
-- 
-- select count(*) from arkona.ext_pdptdet  -- 9,298,380
-- 
-- select * from arkona.ext_pdptdet where ptinv_ = '16505699'
-- 
-- select distinct aa.ro , b.order_type
-- from arkona.ext_pdpphdr a
-- join ros aa on a.document_number = aa.ro
-- join arkona.ext_pdppdet b on a.pending_key = b.pending_key
--   and b.order_type = 's'
-- -- where a.document_number = '16504305'
-- order by aa.ro
-- 
-- 
-- 
-- select b.document_number, c.order_type
-- from ros a
-- join arkona.ext_pdpphdr b on a.ro = b.document_number
-- join arkona.ext_pdppdet c on b.pending_key = c.pending_key
-- 	and c.part_number is not null
-- 	and c.order_type = 'S'
-- union    
-- select a.ro, b.ptsoep
-- from ros a
-- join arkona.ext_pdptdet b on a.ro = b.ptinv_
--   and b.ptsoep = 'S'    