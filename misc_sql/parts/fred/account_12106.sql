﻿/*
Hey Job hope you are doing well, I was hoping you could help and get me a pay break down for the months of February, June,  
and October for the years of 2020 and 2022? Looking for a break down of the 12106 and also can your break down the 
Gas Oil and Grease account as well for those months and years? Please email me give me a call if I don’t make sense. 
Thanks Jon
*/
select * 
from fin.dim_Account
where account = '12106'

select coalesce(d.last_name, e.employee_last_name) as last_name, coalesce(d.first_name, e.employee_first_name) as first_name, c.year_month, a.amount--, a.*
from fin.fact_gl a
join fin.dim_Account b on a.account_key = b.account_key
  and b.account = '12106'
join dds.dim_date c on a.date_key = c.date_key
  and c.year_month in (202002,202006,202010,202202,202206,202210)  
left join ukg.employees d on a.control = d.employee_number  
left join arkona.ext_pymast e on a.control = e.pymast_employee_number
where a.post_status = 'Y'  
order by last_name, first_name, year_month


select coalesce(d.last_name, e.employee_last_name) as last_name, coalesce(d.first_name, e.employee_first_name) as first_name, 
  sum(a.amount) filter (where c.year_month = 202002) as feb_2020,
  sum(a.amount) filter (where c.year_month = 202006) as jun_2020,
  sum(a.amount) filter (where c.year_month = 202010) as oct_2020,
  sum(a.amount) filter (where c.year_month = 202202) as feb_2022,
  sum(a.amount) filter (where c.year_month = 202202) as jun_2022,
  sum(a.amount) filter (where c.year_month = 202202) as oct_2022
from fin.fact_gl a
join fin.dim_Account b on a.account_key = b.account_key
  and b.account = '12106'
join dds.dim_date c on a.date_key = c.date_key
  and c.year_month in (202002,202006,202010,202202,202206,202210)  
left join ukg.employees d on a.control = d.employee_number  
left join arkona.ext_pymast e on a.control = e.pymast_employee_number
where a.post_status = 'Y'  
  and last_name is not null
group by  coalesce(d.last_name, e.employee_last_name), coalesce(d.first_name, e.employee_first_name)


select * from arkona.ext_pymast limit 10

/*
The accounts im looking for on the wages are 12306b, 12306c, 1230d, 12306f. I am still looking for the GOG account 
that I want to look at, there are multiple accounts for oil and I want to be sure I am looking at the correct numbers. Thank you Jon
*/

select coalesce(d.last_name, e.employee_last_name) as last_name, coalesce(d.first_name, e.employee_first_name) as first_name, d.cost_center,
  sum(a.amount) filter (where c.year_month = 202002) as feb_2020,
  sum(a.amount) filter (where c.year_month = 202006) as jun_2020,
  sum(a.amount) filter (where c.year_month = 202010) as oct_2020,
  sum(a.amount) filter (where c.year_month = 202202) as feb_2022,
  sum(a.amount) filter (where c.year_month = 202202) as jun_2022,
  sum(a.amount) filter (where c.year_month = 202202) as oct_2022
from fin.fact_gl a
join fin.dim_Account b on a.account_key = b.account_key
  and b.account in ('12306b','12306c','12306d','12306f')
join dds.dim_date c on a.date_key = c.date_key
  and c.year_month in (202002,202006,202010,202202,202206,202210)  
left join ukg.employees d on a.control = d.employee_number  
left join arkona.ext_pymast e on a.control = e.pymast_employee_number
where a.post_status = 'Y'  
  and last_name is not null
group by  coalesce(d.last_name, e.employee_last_name), coalesce(d.first_name, e.employee_first_name), d.cost_center