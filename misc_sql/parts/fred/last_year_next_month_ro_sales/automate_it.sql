﻿                                                                                                                                                                                     select * from sls.months order by seq limit 100
select lower(replace(mmm_yyyy, ' ', '_')) from sls.months where open_closed = 'open'

create schema pts;
comment on schema pts is 'finally, a schema for parts related tables';

select * from dds.dim_Date where the_date = current_date

/*
need to dynamically create the needed tables
no i don't have to do that
create the table with the necessary date attributes
10/06/22 function works, python script works tada
*/

drop table if exists pts.ro_sales_next_month_last_year cascade;
create table pts.ro_sales_next_month_last_year (
  year_month integer not null, -- next month last year, eg on 10/15/22, the year_month is 202111
  ptpart citext not null,
  ptqty integer not null);

select count(*) from pts.ro_sales_next_month_last_year

-- drop function pts.report_next_month_last_year ();
create or replace function pts.report_next_month_last_year ()
	returns table (part_1 citext,description_1 citext,quant_1 bigint,spacer text,part_2 citext,description_2 citext, quant_2 bigint)
	as
$BODY$
/*
select * from  pts.report_next_month_last_year()
*/	
  declare
  _year_month integer := (
    select distinct year_month
    from dds.dim_date
    where the_date = ((current_date + interval '1 month') - interval '1 year')::date);
  _first_of_month date := (
    select distinct first_of_month
    from dds.dim_date
    where year_month = _year_month);
  _last_of_month date := (
    select distinct last_of_month
    from dds.dim_date
    where year_month = _year_month);    
begin
  delete 
  from pts.ro_sales_next_month_last_year
  where year_month = _year_month;

	insert into pts.ro_sales_next_month_last_year
	select _year_month, a.ptpart, a.ptqty
	from arkona.ext_pdptdet a
	join (
		select ro
		from dds.fact_repair_order 
		where open_date between _first_of_month and _last_of_month
			and store_code = 'ry1'
		group by ro) b on a. ptinv_ = b.ro
	where arkona.db2_integer_to_date(ptdate) between _first_of_month and _last_of_month
		and ptsgrp = '501'
		and ptco_ in ('ry1','ry2');  
return query
	select a.part_number, a.part_description, a.quantity, '    '::text, b.part_number, b.part_description, b.quantity
	from (
		select row_number() over(order by quantity desc) as row_num, a.part_number, b.part_description, a.quantity
		from (
			select ptpart as part_number, sum(ptqty) as quantity
			from pts.ro_sales_next_month_last_year
			where year_month = _year_month
			group by ptpart
			having sum(ptqty) > 0) a
		join arkona.ext_pdpmast b on a.part_number = b.part_number
			and b.part_description like 'SL%'
			and b.status = 'A'
		order by quantity desc) a
	left join (
		select row_number() over(order by quantity desc) as row_num, a.part_number, b.part_description, a.quantity
		from (
			select ptpart as part_number, sum(ptqty) as quantity
			from pts.ro_sales_next_month_last_year
			where year_month = _year_month
			group by ptpart
			having sum(ptqty) > 0) a
		join arkona.ext_pdpmast b on a.part_number = b.part_number
			and b.part_description not like 'SL%'
			and b.status = 'A'
		order by quantity desc) b on a.row_num = coalesce(b.row_num, a.row_num);		
end;
$BODY$
language plpgsql;


