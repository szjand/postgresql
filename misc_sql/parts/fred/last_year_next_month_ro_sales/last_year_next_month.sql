﻿/*
12/29/21
Hey John, hope you had a good Christmas! I am wondering if you would be able to help me out. What I am looking for 
is to see if I can get a list of parts sold on Service ROs from the previous year month. So a list of all parts 
sold on ROs in January 2021.  What I am trying to do is find the most common parts that where sold on ROs previous 
year, so I can make sure we have enough stock of the common parts. Example is that the condensers that we sell a 
lot of, if I can figure out the number and what months they are popular moving, my thought is that if I can get 
the next month previous year parts list I can get parts in for the moving parts for next month in stock before 
it becomes a waiting game. T

Thank you for your help as usual!
Fred 
*/
/*
Yes! This is perfect, is there any way to have this list sent to me for the next month like half way 
through the current month. So mid January I get a list for last years numbers from February. 
Or is there somewhere in dealertrack that I can  find this information  

Can I see what that would look like, and I compare it to the list you just gave me and I can see if it 
makes more sense to just have you send me service lane parts or if I need to be looking at both.
 Could you do me a list of service lane parts and non service lane parts? 
 might be good for me to separate them out to see if I should first concentrate on service lane parts, 
 at first then expand it to the non service lane parts.
*/


-- drop table if exists jon.feb_2021_parts cascade;
-- create table jon.feb_2021_parts as
-- select a.ptpart, a.ptqty
-- from arkona.ext_pdptdet a
-- join (
-- 	select ro
-- 	from dds.fact_repair_order 
-- 	where open_date between '02/01/2021' and '02/28/2021'
-- 		and store_code = 'ry1'
-- 	group by ro) b on a. ptinv_ = b.ro
-- where ptdate between 20210201 and 20210228
--   and ptsgrp = '501'
--   and ptco_ in ('ry1','ry2');

-- drop table if exists jon.mar_2021_parts cascade;
-- create table jon.mar_2021_parts as
-- select a.ptpart, a.ptqty
-- from arkona.ext_pdptdet a
-- join (
-- 	select ro
-- 	from dds.fact_repair_order 
-- 	where open_date between '03/01/2021' and '03/31/2021'
-- 		and store_code = 'ry1'
-- 	group by ro) b on a. ptinv_ = b.ro
-- where ptdate between 20210301 and 20210331
--   and ptsgrp = '501'
--   and ptco_ in ('ry1','ry2');

drop table if exists jon.nov_2021_parts cascade; ---------------------------
create table jon.nov_2021_parts as -----------------------------------------
select a.ptpart, a.ptqty
from arkona.ext_pdptdet a
join (
	select ro
	from dds.fact_repair_order 
	where open_date between '11/01/2021' and '11/30/2021'  --------------------
		and store_code = 'ry1'
	group by ro) b on a. ptinv_ = b.ro
where ptdate between 20211101 and 20211130 ----------------------------------
  and ptsgrp = '501'
  and ptco_ in ('ry1','ry2');  
  
select ptpart as part_number, sum(ptqty) as quantity 
from jon.mar_2021_parts
group by ptpart
having sum(ptqty) > 0
order by sum(ptqty) desc

-- service lane
select a.part_number, b.part_description, a.quantity 
from (
	select ptpart as part_number, sum(ptqty) as quantity 
	from jon.mar_2021_parts
	group by ptpart
	having sum(ptqty) > 0) a
join arkona.ext_pdpmast b on a.part_number = b.part_number
  and b.part_description like 'SL%'
  and b.status = 'A'
order by quantity desc

-- not service lane
select a.part_number, b.part_description, a.quantity 
from (
	select ptpart as part_number, sum(ptqty) as quantity 
	from jon.mar_2021_parts
	group by ptpart
	having sum(ptqty) > 0) a
join arkona.ext_pdpmast b on a.part_number = b.part_number
  and b.part_description not like 'SL%'
  and b.status = 'A'
order by quantity desc

select a.part_number, a.part_description, a.quantity, '    ', b.part_number, b.part_description, b.quantity
from (
	select row_number() over(order by quantity desc) as row_num, a.part_number, b.part_description, a.quantity
	from (
		select ptpart as part_number, sum(ptqty) as quantity
		from jon.nov_2021_parts ---------------------------------------
		group by ptpart
		having sum(ptqty) > 0) a
	join arkona.ext_pdpmast b on a.part_number = b.part_number
		and b.part_description like 'SL%'
		and b.status = 'A'
	order by quantity desc) a
left join (
	select row_number() over(order by quantity desc) as row_num, a.part_number, b.part_description, a.quantity
	from (
		select ptpart as part_number, sum(ptqty) as quantity
		from jon.nov_2021_parts --------------------------------------
		group by ptpart
		having sum(ptqty) > 0) a
	join arkona.ext_pdpmast b on a.part_number = b.part_number
		and b.part_description not like 'SL%'
		and b.status = 'A'
	order by quantity desc) b on a.row_num = coalesce(b.row_num, a.row_num)