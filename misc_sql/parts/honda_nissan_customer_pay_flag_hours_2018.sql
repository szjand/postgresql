﻿-- 49971
select d.make, sum(a.flaghours)
from ads.ext_Fact_repair_order a
join dds.dim_date b on a.finalclosedatekey = b.date_key
  and b.the_year = 2018
join ads.ext_dim_payment_type c on a.paymenttypekey = c.paymenttypekey
  and c.paymenttypecode = 'c' 
join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
  and d.make in ('honda','nissan')  
where a.storecode = 'RY2'
group by d.make