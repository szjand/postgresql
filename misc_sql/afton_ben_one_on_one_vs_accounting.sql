﻿--------------------------------------------------------------------------------------------------
drop table if exists board_data cascade;
create temp table board_data as
select a.board_id, a.boarded_ts::date as board_date, d.arkona_store_key, c.last_name as boarded_by, b.board_type, b.board_sub_type,
  e.vehicle_type, a.deal_number, a.stock_number, a.vin, 
  case when a.is_deleted then 'DELETED' else null end as deleted, 
  case when a.is_backed_on then 'BACK_ON' else null end as back_on, 
  e.sale_code, vehicle_make
-- select *
from board.sales_board a
left join board.board_types b on a.board_type_key = b.board_type_key
left join nrv.users c on a.boarded_by = c.user_key
left join onedc.stores d on a.store_key = d.store_key
left join board.daily_board e on a.board_id = e.board_id;  
create index on board_data(stock_number);
create index on board_data(vin);
create index on board_data(board_type);



drop table if exists step_2;
create temp table step_2 as
select store::citext, page, line, control, sum(unit_count) as unit_count
from ( -- h
  select d.store, d.page, d.line, d.g_l_acct_number, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a   
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201712
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account_type = 'Sale'
  inner join ( -- d: fs gm_account page/line/acct description
    select 
      case
        when b.consolidation_grp is null then 'RY1'
        when b.consolidation_grp = '2' then 'RY2'
      end as store,
      a.fxmpge as page, a.fxmlne::integer as line, b.g_l_acct_number
    from arkona.ext_eisglobal_sypffxmst a
    inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
      and a.fxmcyy = b.factory_financial_year
    where a.fxmcyy = 2017
      and trim(a.fxmcde) = 'GM'
      and coalesce(b. consolidation_grp, 'RY1') <> '3'
      and b.factory_code = 'GM'
      and trim(b.factory_account) <> '331A'
      and b.company_number = 'RY1'
      and b.g_l_acct_number <> ''
      and (
        (a.fxmpge between 5 and 15) or
        (a.fxmpge = 16 and a.fxmlne between 1 and 14))) d on c.account = d.g_l_acct_number
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
    and aa.journal_code in ('VSN','VSU')   
  where a.post_status = 'Y' order by control) h
group by store, page, line, control
order by store, page, line;


select * from sls.ext_bopmast_partial limit 100


select a.*, b.bopmast_id, b.vin, b.delivery_date, c.last_name as psc, d.last_name as ssc
from (
  select store::citext, page, line, control, sum(unit_count) as unit_count
  from ( -- h
    select d.store, d.page, d.line, d.g_l_acct_number, 
      a.control, a.amount,
      case when a.amount < 0 then 1 else -1 end as unit_count
    from fin.fact_gl a   
    inner join dds.dim_date b on a.date_key = b.date_key
      and b.year_month = 201712
    inner join fin.dim_account c on a.account_key = c.account_key
      and c.account_type = 'Sale'
    inner join ( -- d: fs gm_account page/line/acct description
      select 
        case
          when b.consolidation_grp is null then 'RY1'
          when b.consolidation_grp = '2' then 'RY2'
        end as store,
        a.fxmpge as page, a.fxmlne::integer as line, b.g_l_acct_number
      from arkona.ext_eisglobal_sypffxmst a
      inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
        and a.fxmcyy = b.factory_financial_year
      where a.fxmcyy = 2017
        and trim(a.fxmcde) = 'GM'
        and coalesce(b. consolidation_grp, 'RY1') <> '3'
        and b.factory_code = 'GM'
        and trim(b.factory_account) <> '331A'
        and b.company_number = 'RY1'
        and b.g_l_acct_number <> ''
        and (
          (a.fxmpge between 5 and 15) or
          (a.fxmpge = 16 and a.fxmlne between 1 and 14))) d on c.account = d.g_l_acct_number
    inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
      and aa.journal_code in ('VSN','VSU')   
    where a.post_status = 'Y' order by control) h
  group by store, page, line, control) a
left join  sls.ext_bopmast_partial b on a.control = b.stock_number
left join sls.personnel c on 
  case 
    when a.store = 'ry1' then b.primary_sc = c.ry1_id
    when a.store = 'ry2' then b.primary_Sc = c.ry2_id
  end
left join sls.personnel d on 
  case 
    when a.store = 'ry1' then b.secondary_sc = d.ry1_id
    when a.store = 'ry2' then b.secondary_sc = d.ry2_id
  end  


-----------------------------------------------------------------------------------------------------
-- counts to date, retail only
-----------------------------------------------------------------------------------------------------
select a.store_code, 
  case
    when a.page = 16 then 'used'
    else 'new'
  end as new_used, a.control as stock_number,
  a.unit_count, b.bopmast_id, b.vin, b.delivery_date, c.last_name as psc, d.last_name as ssc
from (
  select store_code, page, line, control, sum(unit_count) as unit_count
  from ( -- h
    select 
      case
        when department = 'Auto Outlet' then department
        else store_code
      end as store_code,    
      d.page, d.line, d.g_l_acct_number, 
      a.control, a.amount,
      case when a.amount < 0 then 1 else -1 end as unit_count
    from fin.fact_gl a   
    inner join dds.dim_date b on a.date_key = b.date_key
      and b.year_month = 201712
    inner join fin.dim_account c on a.account_key = c.account_key
      and c.account_type = 'Sale'
    inner join ( -- d: fs gm_account page/line/acct description
      select a.fxmpge as page, a.fxmlne::integer as line, b.g_l_acct_number
      from arkona.ext_eisglobal_sypffxmst a
      inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
        and a.fxmcyy = b.factory_financial_year
      where a.fxmcyy = 2017
        and trim(a.fxmcde) = 'GM'
        and coalesce(b. consolidation_grp, 'RY1') <> '3'
        and b.factory_code = 'GM'
        and trim(b.factory_account) <> '331A'
        and b.company_number = 'RY1'
        and b.g_l_acct_number <> ''
        and ( -- retail only
          (a.fxmpge between 5 and 15 and (a.fxmlne between 1 and 19 or a.fxmlne between 25 and 40)) or
          (a.fxmpge = 16 and a.fxmlne between 1 and 5))) d on c.account = d.g_l_acct_number
    inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
      and aa.journal_code in ('VSN','VSU')   
    where a.post_status = 'Y' order by control) h
  group by store_code, page, line, control) a
left join  sls.ext_bopmast_partial b on a.control = b.stock_number
left join sls.personnel c on 
  case 
    when a.store_code = 'ry1' then b.primary_sc = c.ry1_id
    when a.store_code = 'ry2' then b.primary_Sc = c.ry2_id
  end
left join sls.personnel d on 
  case 
    when a.store_code = 'ry1' then b.secondary_sc = d.ry1_id
    when a.store_code = 'ry2' then b.secondary_sc = d.ry2_id
  end  