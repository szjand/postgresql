﻿/*

for a certain level of grouping, jeri wants the reference number 
(she refers to it as invoice, it is the vendors invoice to us)

*/

create table jeri.semi_fixed_line_labels (
  page integer not null,
  line integer primary key,
  line_label citext not null);

insert into jeri.semi_fixed_line_labels
select page, line::integer, line_label
from fin.dim_fs 
where page = 2 
  and year_month = 201801
  and line between 18 and 39
  and col = 1;


drop table if exists jeri.semi_fixed_accounts;
create table jeri.semi_fixed_accounts (
  store_code citext not null, 
  page integer not null,
  line integer not null references jeri.semi_fixed_line_labels(line),
  col integer not null,
  gl_account citext primary key,
  gm_account citext not null);
  
insert into jeri.semi_fixed_accounts
select distinct case coalesce(b.consolidation_grp, '1') when '2' then 'RY2' else 'RY1' end as store_code,
  a.fxmpge as page, a.fxmlne::integer as line, a.fxmcol as col, b.g_l_acct_number as gl_account,
  b.factory_account as gm_account
from arkona.ext_eisglobal_sypffxmst a
inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
where a.fxmcyy = 2018
  and trim(a.fxmcde) = 'GM'
  and coalesce(b.consolidation_grp, '1') <> '3'
  and b.factory_code = 'GM'
  and trim(b.factory_account) <> '331A'
  and b.g_l_acct_number <> ''
  and a.fxmpge between 3 and 4
  and a.fxmlne between 18 and 39;

create table jeri.vendors (
  vendor_number citext primary key,
  vendor_name citext not null);

select count(*) from jeri.vendors -- 2583

insert into jeri.vendors
select vendor_number, search_name
from arkona.ext_glpcust
where active in ('V','B') 
  and vendor_number is not null
on conflict (vendor_number)
do update set vendor_name = excluded.vendor_name;


drop table if exists jeri.vendor_spend_detail cascade;
create table jeri.vendor_spend_detail (
  year_month integer not null,
  the_date date not null,
  store_code citext not null,
  fs_line integer not null,
  fs_line_label citext not null,
  gm_account citext not null,
  gl_account citext not null,
  acct_description citext not null,
  trans_description citext not null,
  dept citext not null,
  vendor citext not null,
  vendor_number citext not null references jeri.vendors(vendor_number),
  amount numeric(12,2) not null,
  trans bigint not null,
  invoice citext not null,
  constraint vendor_spend_detail_pkey primary key (vendor_number,the_date,trans,trans_description,gl_account,invoice));
  
create index on jeri.semi_fixed_accounts(gl_account);
create index on jeri.semi_fixed_accounts(line); 

insert into jeri.vendor_spend_detail
select b.year_month, b.the_date, c.store_code, f.line, 
  f.line_label as fs_line_label, cc.gm_account, cc.gl_account, 
  c.description, d.description, c.department_code,
  e.vendor_name, e.vendor_number, sum(a.amount), a.trans, a.ref
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_date between current_date - interval '3 month' and current_date
inner join fin.dim_account c on a.account_key = c.account_key
inner join jeri.semi_fixed_accounts cc on c.account = cc.gl_account
inner join jeri.vendors e on a.control = e.vendor_number 
inner join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
inner join jeri.semi_fixed_line_labels f on cc.line = f.line
inner join fin.dim_doc_type g on a.doc_type_key = g.doc_type_key
where a.post_status = 'Y'
group by b.year_month, a.trans, b.the_date, c.store_code, f.line, 
  f.line_label, cc.gm_account, cc.gl_account, 
  c.description, d.description, c.department_code,
  e.vendor_name, e.vendor_number, a.trans, a.ref;

select (current_date - interval '6 month')::date
select * from sls.months
select date_trunc('month', (current_date- interval '1 month') )

select * from dds.dim_date where the_date between current_date - interval '1 week' and current_date

select *
from jeri.vendor_spend_detail
where the_date between current_date - interval '3 month' and current_date
order by the_date


select year_month, vendor_number
from rds.dim_date a, jeri.vendors b
where the_date between '01/01/2017' and date_trunc('month', (current_date- interval '1 month') )
  and vendor_number in (
  select distinct vendor_number
  from nrv.vendor_business_rules
  where current_row = true)

  
select current_date - interval '1 week'

select count(*) from jeri.vendor_spend_detail -- 32262

select * from jeri.vendor_spend_detail where gm_Account = '078a'

select vendor_number, vendor, year_month, sum(amount) 
from jeri.vendor_spend_detail
group by vendor_number, vendor, year_month

select year_month, count(*)
from jeri.vendor_spend_Detail
group by year_month

select vendor_number, vendor,  sum(amount) 
from jeri.vendor_spend_detail
group by vendor_number, vendor
having sum(amount) > 0

select *
from (
select vendor_number, vendor, max(the_date) as max_date
from jeri.vendor_spend_detail
group by vendor_number, vendor) s
where max_date < current_date - 365

select current_date - 365


select vendor_number, vendor 
from jeri.vendor_spend_detail
group by vendor_number, vendor

-- vendor numbers that do not begin with 1 or 2
-- mostly cartiva, several misc anomalies
select vendor, vendor_number, count(*)
from jeri.vendor_spend_detail
where left(vendor_number, 1) not in ('1','2')
group by vendor, vendor_number



select invoice, count(*)
from jeri.vendor_spend_Detail
group by invoice
order by count(*) desc


select *
from jeri.vendor_spend_detail
where invoice = '333362'

select *
from jeri.vendor_spend_detail
where invoice = 'STEVE0118'



select *
from jeri.vendor_spend_detail
where invoice = '92147593'

--------------------------------------------------------------------------------------------------------------------------------------------------------

-- 6 previous year_months from today
year month math

select 'hello'

select date_trunc('day', '02/28/2018'::Date - interval '6 month')::date

select extract(month from date_trunc('month', current_date - interval '6 months'))


select a.the_date, a.year_month as (select 'hello'), 
  (select year_month from dds.dim_date where the_date = (select date_trunc('day', a.the_date - interval '6 month')::date)) as six_mos_ago,
  (select year_month from dds.dim_date where the_date = (select date_trunc('day', a.the_date - interval '5 month')::date)) as five_mos_agop,
  (select year_month from dds.dim_date where the_date = (select date_trunc('day', a.the_date - interval '4 month')::date)) as four_mos_ago,
  (select year_month from dds.dim_date where the_date = (select date_trunc('day', a.the_date - interval '3 month')::date)) as three_mos_ago,
  (select year_month from dds.dim_date where the_date = (select date_trunc('day', a.the_date - interval '2 month')::date)) as two_mos_ago,
  (select year_month from dds.dim_date where the_date = (select date_trunc('day', a.the_date - interval '1 month')::date)) as one_mos_ago
from dds.dim_date a
where a.year_month = (select distinct year_month from dds.dim_date where the_date = current_date)
order by a.the_date


-- can't figure out how to use a variable as an alias
do
$$
declare 
  _6_mos_ago text := (select year_month from dds.dim_date where the_date = (select date_trunc('day', current_date - interval '6 month')::date))::text;
begin
    drop table if exists test;
    create temp table test as
    select current_date, 1 as a, 'x' as _6_mos_ago, 2 as b,3 as c,4 as d,5 as e,6 as f
    union
    select a.the_date, a.year_month as current_month, 
    (select year_month from dds.dim_date where the_date = (select date_trunc('day', a.the_date - interval '6 month')::date))::text as _6_mos_ago,
    (select year_month from dds.dim_date where the_date = (select date_trunc('day', a.the_date - interval '5 month')::date)) as five_mos_agop,
    (select year_month from dds.dim_date where the_date = (select date_trunc('day', a.the_date - interval '4 month')::date)) as four_mos_ago,
    (select year_month from dds.dim_date where the_date = (select date_trunc('day', a.the_date - interval '3 month')::date)) as three_mos_ago,
    (select year_month from dds.dim_date where the_date = (select date_trunc('day', a.the_date - interval '2 month')::date)) as two_mos_ago,
    (select year_month from dds.dim_date where the_date = (select date_trunc('day', a.the_date - interval '1 month')::date)) as one_mos_ago
  from dds.dim_date a
  where a.year_month = (select distinct year_month from dds.dim_date where the_date = current_date)
  order by b;  

end
$$;

select * from test

-- this looks pretty good
select vendor_number, vendor, 
  sum(amount) filter (where year_month = (select distinct year_month from dds.dim_date where the_date = current_date)) as current_month,
  sum(amount) filter (where year_month = (select year_month from dds.dim_date where the_date = (select date_trunc('day', current_date - interval '1 month')::date))) as one,
  sum(amount) filter (where year_month = (select year_month from dds.dim_date where the_date = (select date_trunc('day', current_date - interval '2 month')::date))) as two,
  sum(amount) filter (where year_month = (select year_month from dds.dim_date where the_date = (select date_trunc('day', current_date - interval '3 month')::date))) as three,
  sum(amount) filter (where year_month = (select year_month from dds.dim_date where the_date = (select date_trunc('day', current_date - interval '4 month')::date))) as four,
  sum(amount) filter (where year_month = (select year_month from dds.dim_date where the_date = (select date_trunc('day', current_date - interval '5 month')::date))) as five,
  sum(amount) filter (where year_month = (select year_month from dds.dim_date where the_date = (select date_trunc('day', current_date - interval '6 month')::date))) six
from jeri.vendor_spend_detail
group by vendor_number, vendor
order by vendor


-- this also works
select distinct month_name
from dds.dim_date
where year_month = (
select (select extract(year from (current_date - interval '6 month')::date) * 100) + (select extract(month from (current_date - interval '6 month')::date)))


-- 4/9/18
Damnit glcust.company_number is all RY1
vendor table needs to include the store

alter table jeri.vendors
drop column store_Code;

select *
from arkona.ext_glpcust
limit 1000

select vendor_number
from arkona.ext_glpcust
group by vendor_number
having count(*) > 1

create table jeri.vendors (
  vendor_number citext primary key,
  vendor_name citext not null);

insert into jeri.vendors
select vendor_number, search_name
from arkona.ext_glpcust
where active in ('V','B') 
  and vendor_number is not null
on conflict (vendor_number)
do update set vendor_name = excluded.vendor_name;  

alter table jeri.vendors
add column store_code citext;

insert into jeri.vendors
select vendor_number, search_name, company_number
from arkona.ext_glpcust
where active in ('V','B') 
  and vendor_number is not null
on conflict (vendor_number)
do update set vendor_name = excluded.vendor_name, store_code = excluded.store_code; 


update jeri.vendors x
set store_code = (
  select company_number
  from arkona.ext_glpcust
  where vendor_number = x.vendor_number);


select *
from jeri.vendors  


select * from arkona.ext_glpcust where vendor_number = '214420'


select company_number, count(*)
from arkona.ext_glpcust
where active in ('V','B') 
  and vendor_number is not null
group by company_number  

/*
4/10, ah ha, but, glpcust.record_key, which with company_number(which is actually irrelevant) forms
the primary key is a "foreign key" to bopname (bopname_record_key)

*/

-- only 12 rows where company_number is not RY1
select company_number, count(*)
from arkona.ext_glpcust
group by company_number

select * -- none of which are vendors
from arkona.ext_glpcust
where company_number <> 'RY1'

-- thought maybe the company_number in bopname would be relevant, guess not:
select a.company_number, a.record_key, a.active, a.customer_number,
  a.vendor_number, a.search_name, b.*
from arkona.ext_glpcust a
left join arkona.ext_bopname b on a.record_key = b.bopname_record_key
  and a.company_number = b.bopname_company_number
where active in ('V','B') 
  and vendor_number is not null
order by a.vendor_number  


-- 4/13
clean up the stocknumber vendornumber overlap 
(see /jeri_vendor_project/maintenance/vendor_number_vs_stock_number.sql)
also, add doc_type_code, doc_type, document number & journal code to vendor_spend_detail

select * from fin.fact_gl limit 10

select * from jeri.vendor_spend_detail where vendor_number = '26330'

alter table jeri.vendor_spend_detail
add column doc_type citext,
add column doc citext,
add column journal citext;

truncate jeri.vendor_spend_detail;
  insert into jeri.vendor_spend_detail
  select b.year_month, b.the_date, c.store_code, f.line, 
    f.line_label as fs_line_label, cc.gm_account, cc.gl_account, 
    c.description, d.description, c.department_code,
    e.vendor_name, e.vendor_number, sum(a.amount), a.trans, a.ref, 
    g.doc_type, a.doc, h.journal_code
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
--     and b.the_date between current_date - interval '3 month' and current_date
    and b.the_date > '12/31/2016'
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join jeri.semi_fixed_accounts cc on c.account = cc.gl_account
  inner join jeri.vendors e on a.control = e.vendor_number 
  inner join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
  inner join jeri.semi_fixed_line_labels f on cc.line = f.line
  inner join fin.dim_doc_type g on a.doc_type_key = g.doc_type_key
  inner join fin.dim_journal h on a.journal_key = h.journal_key
  where a.post_status = 'Y'
    and g.doc_type_code not in ('p','s')
  group by b.year_month, a.trans, b.the_date, c.store_code, f.line, 
    f.line_label, cc.gm_account, cc.gl_account, 
    c.description, d.description, c.department_code,
    e.vendor_name, e.vendor_number, a.trans, a.ref,
    g.doc_type, a.doc, h.journal_code;


select *
from jeri.vendor_spend_detail
where doc_type in ('parts','service')


