﻿/*
Jon  can I get a report with store, name, department,   job code and distribution code.

Jeri thinks we have some people expenseing to wrong accounts just want to check it out

Thanks Kim

6/7/19

Good Afternoon  Jon  I did some updates to this list would  you be able to run it again?


*/
select a.pymast_company_number, employee_name, pymast_employee_number, 
  a.department_code,  a.distrib_code, c.data
from arkona.xfm_pymast a
left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
left join arkona.ext_pyprjobd c on b.company_number = c.company_number
  and b.job_description = c.job_description 
where a.current_row
  and active_code <> 'T'
  and pymast_company_number in ('RY1','RY2')