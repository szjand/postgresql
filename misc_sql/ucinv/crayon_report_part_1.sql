﻿-- crayon report part 1 --
select * from greg.uc_daily_snapshot_beta_1 
where the_date = current_date
  and price_band = '34-36k'
  and shape = 'pickup'
  and size = 'large'
  and store_code = 'ry1'


select store_code, shape_size, price_band, status, count(*)
from greg.uc_daily_snapshot_beta_1 
where the_date = current_date
  and price_band <> 'not priced'
group by store_code, shape_size, price_band, status
order by store_code, shape_size, price_band desc, status


select count(distinct vin) from greg.uc_daily_snapshot_beta_1 -- 25059

select count(vin) from greg.uc_daily_snapshot_beta_1 -- 1,470,137

select status, count(*) 
from greg.uc_base_Vehicle_statuses
group by status
-- raw
-- avail_fresh
-- avail_aged
-- pulled



select min(the_date)  from greg.uc_daily_snapshot_beta_1 -- 1/2/13

drop table if exists ss;
create temp table ss as
select distinct shape_size
from  greg.uc_daily_snapshot_beta_1
where shape_size is not null   -- 14 vin with null shape size
group by shape_size;
create unique index on ss(shape_size);


-- number of vehicles sold in past 30 days
-- number of vehicles currently in each status
-- for each ss, sold in last 30 days
-- for each ss, avg 30 day sales for last 365

select shape_size, count(*)
from greg.uc_daily_snapshot_beta_1
where sale_date between current_date - 31 and current_date - 1
  and sale_type = 'retail'
  and the_date = sale_date
  and store_code = 'ry1'
group by shape_size  

  

-- for each ss, for each date in past 365, sales
drop table if exists t1;
create temp table t1 as
select the_date, shape_size, count(*) as sales
from greg.uc_daily_snapshot_beta_1
where sale_date between current_date - 396 and current_date - 1
  and sale_type = 'retail'
  and the_date = sale_date
  and store_code = 'ry1'
group by the_date, shape_size;
create unique index on t1(the_date,shape_size); 


drop table if exists ss;
create temp table ss as
select distinct shape_size
from  greg.uc_daily_snapshot_beta_1
where shape_size is not null   -- 14 vin with null shape size
group by shape_size;
create unique index on ss(shape_size);


select shape_size, round(avg(sales_30), 1) as avg_sales_30
from (
  select cc.the_date, cc.shape_size, cc.sales,
    sum(cc.sales) over (partition by cc.shape_size order by cc.the_date rows between 29 preceding and current row) sales_30
  from (  
    select aa.*, coalesce(bb.sales, 0) as sales
    from (
      select a.the_date, b.shape_size
      from dds.dim_date a
      cross join ss b 
      where a.the_date between current_date - 396 and current_date - 1) aa
    left join t1 bb on aa.the_date = bb.the_date
      and aa.shape_size = bb.shape_size) cc) dd
where the_date between current_date -366 and current_date - 1  
group by shape_size


select * from t1 where shape_size = 'Crossover-Large'


select * from ads.ext_make_model_classifications where split_part(vehiclesegment,'_',2) = 'Large' and split_part(vehicletype,'_',2) = 'Crossover'

select *, split_part(vehiclesegment,'_',1) from ads.ext_make_model_classifications

------------------------------------------------------------------------
--< for each ss, avg 30 day sales for last 365
------------------------------------------------------------------------
select shape_size, round(avg(sales_30), 1) as avg_sales_30
from (
  select cc.the_date, cc.shape_size, cc.sales,
    sum(cc.sales) over (partition by cc.shape_size order by cc.the_date rows between 29 preceding and current row) sales_30
  from (  
    select aa.*, coalesce(bb.sales, 0) as sales
    from ( -- 1 row for each ss/day
      select a.the_date, b.shape_size
      from dds.dim_date a
      cross join (
        select distinct shape_size
        from  greg.uc_daily_snapshot_beta_1
        where shape_size is not null   -- 14 vin with null shape size
        group by shape_size) b 
      where a.the_date between current_date - 396 and current_date - 1) aa
    left join ( -- total sales for each ss per day
      select the_date, shape_size, count(*) as sales
      from greg.uc_daily_snapshot_beta_1
      where sale_date between current_date - 396 and current_date - 1
        and sale_type = 'retail'
        and the_date = sale_date
        and store_code = 'ry1'
      group by the_date, shape_size) bb on aa.the_date = bb.the_date
      and aa.shape_size = bb.shape_size) cc) dd
where the_date between current_date -366 and current_date - 1  
group by shape_size

------------------------------------------------------------------------
--/> for each ss, avg 30 day sales for last 365
------------------------------------------------------------------------













-- uhoh, no stock number in veihcle invenory items?

select a.stock_number, a.vin, a.date_acquired, a.sale_date, b.stocknumber, a.status, a.days_in_status
from greg.uc_daily_snapshot_beta_1 a
left join ads.ext_Vehicle_inventory_items b on a.stock_number = b.stocknumber
where a.the_date = current_date
and a.price_band like '%not%' 


select *
from greg.uc_daily_snapshot_beta_1 
where vin = '1G6KD54Y93U265803'
  and the_date = '04/20/2019'


create schema tem;
comment on schema tem is 'schema for jons incessant temp tables';


drop table if exists tem.ads_1;
create table tem.ads_1 as
select a.vin, b.stocknumber as stock_number, b.fromts::date as from_date, b.thruts::date as thru_date
from ads.ext_vehicle_items a
join ads.ext_vehicle_inventory_items b on a.vehicleitemid = b.vehicleitemid;
create unique index on tem.ads_1(vin,stock_number);
create index on tem.ads_1(from_date);
create index on tem.ads_1(thru_date);

-- limit greg to stock/vin combinations from tool
drop table if exists tem.ads_2;
create table tem.ads_2 as
select a.*, b.the_date
from tem.ads_1 a
join greg.uc_daily_snapshot_beta_1 b on a.stock_number = b.stock_number
  and a.vin = b.vin 
  and b.the_date between a.from_date and a.thru_date;

select count(*) from ads_2  -- 1430018

select * 
from tem.ads_2 
where vin = '1G6KD54Y93U265803'

-- vins with multiple instances (different stock numbers) on the same day
select the_date, vin, string_agg(stock_number, ',')
from (
select the_date, vin, stock_number
from tem.ads_2
group by the_date, vin, stock_number) a
group by the_date, vin
having count(*) > 1    