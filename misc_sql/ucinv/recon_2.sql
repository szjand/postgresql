﻿select b.the_date, a.trans, a.seq, a.post_status, a.control, a.doc, c.account, 
  c.account_type, c.description, c.department_code, d.journal_code, a.amount
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join greg.uc_base_vehicles e on a.control = e.stock_number
where a.post_status = 'Y'
  and account in (select gl_account from fin.dim_fs_Account where gm_account in ('240','241'))
--   and b.year_month =  201706
  and d.journal_code = 'POT'
  and exists (
    select 1
    from ads.ext_fact_repair_order
    where ro = a.doc)
order by journal_code  



-- this includes the inv acct pot transactions
-- this gives me the all the ros (fact_gl.doc) which i need  to generate 
-- sales amounts, these are only cogs amounts becuase they are against the UC department
select b.the_date, a.trans, a.seq, a.post_status, a.control, a.doc, c.account, 
  c.account_type, c.description, c.department_code, d.journal_code, a.amount
-- select sum(amount)  -- 744.85
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join greg.uc_base_vehicles e on a.control = e.stock_number
where a.post_status = 'Y'
--   and b.year_month =  201707
  and c.department_code = 'UC'
--   and d.journal_code = 'POT'
  and a.control = '30088xx'
  and exists (
    select 1
    from ads.ext_fact_repair_order
    where ro = a.doc)
order by journal_code, control 


-- this gives me the offsetting entries against cst recon account
-- which, at this point in the game i don't believe i need
-- if the expenditure is during the period of inventory, the journal is VSU
-- if after the sale, the journal is SVI
select b.the_date, a.trans, a.seq, a.post_status, a.control, a.doc, c.account, 
  c.account_type, c.description, c.department_code, d.journal_code, a.amount
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join greg.uc_base_vehicles e on a.control = e.stock_number
where a.post_status = 'Y'
--   and b.year_month =  201707
--   and c.department_code = 'UC'
--   and d.journal_code = 'POT'
--   and account in (select gl_account from fin.dim_fs_account where gm_account in ('647a','647b','651A','651B'))
  and a.control = '29168b'
order by account

need to look at some wholesale deals
what i see with wholesale, is not offestting entries to the recon account, it just gets lumped into
an offsetting entry against the inventory account

-- so the new definition is: ---------------------------------------------------------------------------------------------
-- any transaction against department UC where the doc is an ro
-- need the stock_number to join to vehicles
drop table if exists greg.uc_recon_ros;
create table greg.uc_recon_ros (
  ro citext not null,
  stock_number citext not null references greg.uc_base_vehicles(stock_number) on update cascade);
create index on greg.uc_recon_ros(stock_number);  
insert into greg.uc_recon_ros
select distinct doc as ro, control as stock_number 
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join greg.uc_base_vehicles e on a.control = e.stock_number
where a.post_status = 'Y'
  and c.department_code = 'UC'
  and exists (
    select 1
    from ads.ext_fact_repair_order
    where ro = a.doc);  
    
-- select count(*) from recon_ros; --67774/67480
-- this was a pain in the ass, decided to just discard them
-- when jeri gets back, talk to her about screening for this occurrence
delete 
from greg.uc_recon_ros
where ro in ( -- multiple vehicles per ro, accounting error
  select ro
  from (
    select stock_number, ro
    from greg.uc_recon_ros
    group by stock_number, ro) a
  group by ro
  having count(*) > 1);
  
alter table greg.uc_recon_ros
add primary key(ro);




-- need the transactions where the ro is the control to get sales/cogs/gross
go with a granular recon_detail which easily becomes the basis for any
recon slicing and dicing desired
the question is,
1. the grain: trans/seq/post_status: FK update cascade, this will enable handling void gl transactions
2. attributes: date, stock_number,ro, account_type, department_code, journal_code, amount
3. stock_number FK, 
-- 8/23 include account type expense to include shop supplies as part of recon sales
drop table if exists greg.uc_recon_details;
create table greg.uc_recon_details (
  the_date date not null,
  stock_number citext not null references greg.uc_base_vehicles(stock_number) on update cascade,
  ro citext not null,
  account citext not null,
  account_type citext not null,
  department_code citext not null,
  journal_code citext not null,
  amount numeric(12,2) not null,
  trans integer not null,
  seq integer not null,
  post_status citext not null,
  hash citext not null,
  FOREIGN KEY (trans,seq,post_status) REFERENCES fin.fact_gl(trans,seq,post_status) ON UPDATE CASCADE,
  primary key (trans,seq,post_status));
create index on greg.uc_recon_details(the_date);    
create index on greg.uc_recon_details(stock_number);
create index on greg.uc_recon_details(ro);
create index on greg.uc_recon_details(account_type);
create index on greg.uc_recon_details(account);
create index on greg.uc_recon_details(department_code);
create index on greg.uc_recon_details(journal_code);
create unique index on greg.uc_recon_details(hash);
insert into greg.uc_recon_details 
select b.the_date, aa.stock_number, 
  a.control, c.account, c.account_type, 
  c.department_code, d.journal_code, 
  a.amount, a.trans, a.seq, a.post_status,
  (
    select md5(z::text) as hash
    from (
      select b.the_date, aa.stock_number, 
        a.control, c.account, c.account_type, 
        c.department_code, d.journal_code, 
        a.amount, a.trans, a.seq, a.post_status
      from fin.fact_gl aaa
      inner join greg.uc_recon_ros aa on aaa.control = aa.ro
      inner join dds.dim_date b on aaa.date_key = b.date_key
      inner join fin.dim_account c on aaa.account_key = c.account_key
        and c.account_type in ('sale','cogs','expense') 
      inner join fin.dim_journal d on aaa.journal_key = d.journal_key
      where aaa.trans = a.trans
        and aaa.seq = a.seq
        and aaa.post_status = a.post_status) z)    
from fin.fact_gl a
inner join greg.uc_recon_ros aa on a.control = aa.ro
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account_type in ('sale','cogs','expense') 
inner join fin.dim_journal d on a.journal_key = d.journal_key
where a.post_status = 'Y';


select * from greg.uc_recon_Details limit 10

select the_date, ro,
  sum(case when account_type in ('sale', 'expense') then -1 * amount else 0 end) as sale,
  sum(case when account_type = 'cogs' then amount else 0 end) as cogs,
  sum(-1*amount) as gross
from greg.uc_recon_details
where ro = '18053253'
group by the_date, ro




-- is it possible to include post sale recon in the generation of recon_by_day
-- looks like it works
-- 8/23 include account type expense as part of sales
drop table if exists greg.uc_recon_by_day cascade;
create table greg.uc_recon_by_day (
  stock_number citext not null,
  the_date date not null,  
  recon_sales numeric(12,2) not null default 0,
  recon_cogs numeric(12,2) not null default 0,
  recon_gross numeric(12,2) not null default 0,
  post_sale_recon_sales numeric(12,2) not null default 0,
  post_sale_recon_cogs numeric(12,2) not null default 0,
  post_sale_recon_gross numeric(12,2) not null default 0,
  primary key(the_date,stock_number));
truncate greg.uc_recon_by_day;
insert into greg.uc_recon_by_day  
select a.stock_number, 
  case 
    when a.the_date <= b.sale_date then a.the_date
    else b.sale_date
  end as the_date,
  sum(case when a.account_type in ('sale','expense') and a.the_date  <= b.sale_date then -1 * amount else 0 end) as recon_sales,
  sum(case when a.account_type = 'cogs' and a.the_date  <= b.sale_date then amount else 0 end) as recon_cogs,
  sum(case when a.the_date <= b.sale_date then -1 * amount else 0 end) as recon_gross,
  sum(case when a.account_type in ('sale','expense') and a.the_date  > b.sale_date then -1 * amount else 0 end) as post_sale_recon_sales,
  sum(case when a.account_type = 'cogs' and a.the_date  > b.sale_date then amount else 0 end) as post_sale_recon_cogs,
  sum(case when a.the_date > b.sale_date then -1 * amount else 0 end) as post_sale_recon_gross  
from greg.uc_recon_details a
left join greg.uc_base_vehicles b on a.stock_number = b.stock_number
group by a.stock_number, 
  case 
    when a.the_date <= b.sale_date then a.the_date
    else b.sale_date
  end;


-- 8/23/17 looks like recon should include shop supplies, etc
-- which looks like including expense account type as additional sales
-- need to check body shop as well, yep, looks like that includes it
select b.the_date, aa.stock_number, 
  a.control, c.account, c.account_type, 
  c.department_code, d.journal_code, 
  a.amount, a.trans, a.seq, a.post_status
from fin.fact_gl a
inner join greg.uc_recon_ros aa on a.control = aa.ro
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
--   and c.account_type in ('sale','cogs','expense') 
inner join fin.dim_journal d on a.journal_key = d.journal_key
where a.post_status = 'Y'
  and aa.ro = '18053253'



  