﻿/*
6/10/20
daves request
we have taken a coach car into inventory needing a shitload of recon
UND is claiming normal wear
dave would like to see actual recon spent on the last 10-15 coach cars we have taken in
stocknumber,year,make,model,total recon
*/


-- coach cars have a ...z stocknumber
drop table if exists cars;
create temp table cars as
SELECT a.stocknumber, b.vin, b.yearmodel, b.make, b.model, fromts::date, thruts::date
FROM ads.ext_Vehicle_inventory_items a
JOIN ads.ext_Vehicle_Items b on a.VehicleItemID = b.VehicleItemID
WHERE stocknumber LIKE '%z'
ORDER BY fromts::date DESC; 



-- recon from flightplan
select a.control, sum(a.amount)::integer as recon
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
--   and b.year_month = _year_month
join fin.dim_account c on a.account_key = c.account_key
  and c.account in ('164700','164701','164702','165100','165101','165102')
join fin.dim_journal e on a.journal_key  = e.journal_key   
join cars f on a.control = f.stocknumber
where a.post_status = 'Y'
group by a.control

-- from flightplan\sql\recon.sql
/*
Amounts posted to these accounts from journals other than VSN or VSU 
should identify recon posted after deal is closed.  
It may also be helpful to identify entries going to the 
cost of sale accounts after the vehicle deal is posted (journals SVI, potentially POT, PCA)

GM Store:	
164700,164701,164702,165100,165101,165102	

HGF	
264700,264701,264702,264710,264720,265100	
265101,265102,265110,265120	
*/
select b.the_date, a.*, d.journal_code, c.account, c.description, e.description
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201808
join fin.dim_account c on a.account_key = c.account_key  
  and c.account in ('164700','164701','164702','165100','165101','165102',
    '264700','264701','264702','264710','264720','265100',	
    '265101','265102','265110','265120')
join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code not in ('VSN','VSU')
join fin.dim_gl_description e on a.gl_Description_key = e.gl_description_key  
where a.post_status = 'Y'


-- from ucinv
select *, sum(a.amount) over (partition by a.stock_number)
from greg.uc_recon_Details a
join cars b on a.stock_number = b.stocknumber
where a.account_type = 'sale'
order by a.stock_number

-- this looks good enough, send it to dave 6/11/20
select aa.stocknumber, aa.vin, aa.yearmodel as year, aa.make, aa.model,
  aa.fromts as date_in, aa.thruts as date_sold, bb.total_recon
from cars aa
left join (
  select a.stock_number, sum(-a.amount)::integer as total_recon
  from greg.uc_recon_Details a
  join cars b on a.stock_number = b.stocknumber
  where a.account_type = 'sale'
  group by a.stock_number) bb on aa.stocknumber = bb.stock_number
where aa.thruts between '05/01/2019' and current_date  
order by aa.fromts desc







select e.journal_code, c.account, a.control, f.vin, f.yearmodel, f.make, f.model, f.fromts, sum(a.amount)::integer as recon
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
join fin.dim_account c on a.account_key = c.account_key
--   and c.account in ('164700','164701','164702','165100','165101','165102')
join fin.dim_journal e on a.journal_key  = e.journal_key  
  and e.journal_code = 'svi'
join ( -- this came from flightplan
  SELECT a.stocknumber, b.vin, b.yearmodel, b.make, b.model, fromts::date
  FROM ads.ext_Vehicle_inventory_items a
  JOIN ads.ext_Vehicle_Items b on a.VehicleItemID = b.VehicleItemID
  WHERE stocknumber LIKE '%z'
  ORDER BY fromts::date DESC ) f on a.control = f.stocknumber
where a.post_status = 'Y'
  and a.control = 'G36576XZ'
group by e.journal_code, c.account, a.control, f.vin, f.yearmodel, f.make, f.model, f.fromts
  having sum(a.amount) <> 0
order by a.control 


select * 
from greg.uc_base_vehicles
where stock_number = 'G36576XZ'


select * 
from greg.uc_recon_details
where stock_number = 'G36576XZ'

select * from ads.ext_dim_vehicle where vin = '1G1ZD5STXJF200584'

select b.the_date, a.ro, laborsales
from ads.ext_fact_Repair_order a
join dds.dim_Date b on a.opendatekey = b.date_key
  and b.the_date between '07/23/2019' and '02/27/2020'
where vehiclekey = 188394



select max(open_date) 
from arkona.ext_sdprhdr
limit 10


