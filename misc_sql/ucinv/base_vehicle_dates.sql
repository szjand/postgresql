﻿-- -- -- drop table if exists temp_dates;
-- -- -- create temp table temp_dates as
-- -- -- select *
-- -- -- from (
-- -- --   select a.stocknumber as tl_stock_number, b.vin as tl_vin, 
-- -- --     a.fromts::date as vii_from, a.thruts::date as vii_thru,
-- -- --     c.soldts::date as sold_date, d.soldts::date as sale_cancel_date
-- -- -- -- tool    
-- -- --   from ads.ext_vehicle_inventory_items a
-- -- --   inner join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
-- -- --   left join ads.ext_vehicle_sales c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
-- -- --     and c.status = 'VehicleSale_Sold'
-- -- --   left join ads.ext_vehicle_sales d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
-- -- --     and d.status = 'VehicleSale_SaleCanceled'
-- -- --   where left(stocknumber,1) <> 'C') m 
-- -- -- -- inpmast  
-- -- -- full outer join (
-- -- --   select inpmast_stock_number, inpmast_vin,
-- -- --       case
-- -- --         when a.date_in_invent = 0 then null
-- -- --         else
-- -- --           (left(a.date_in_invent::text,4) || '-' || 
-- -- --             substring(a.date_in_invent::text from 5 for 2) || '-' || 
-- -- --             substring(a.date_in_invent::text from 7 for 2))::date 
-- -- --       end as date_in_invent,
-- -- --       case
-- -- --         when a.date_delivered = 0 then null
-- -- --         else 
-- -- --           (left(a.date_delivered::text,4) || '-' || 
-- -- --             substring(a.date_delivered::text from 5 for 2) || '-' || 
-- -- --             substring(a.date_delivered::text from 7 for 2))::date 
-- -- --       end as date_delivered
-- -- --   from arkona.xfm_inpmast a   
-- -- --   where inpmast_Stock_number is not null
-- -- --     and current_row = true
-- -- --     and left(inpmast_stock_number, 1) <> 'C') n on m.tl_stock_number = n.inpmast_stock_number
-- -- -- -- bopmast    
-- -- -- full outer join (
-- -- --   select stock_number, vin, vehicle_type, sale_type,
-- -- --     buyer_bopname_id, capped_date, delivery_date
-- -- --   from sls.ext_bopmast_partial
-- -- --   where left(stock_number, 1) <> 'C') o on m.tl_stock_number = o.stock_number;
-- -- -- 
-- -- -- 
-- -- -- 22991XXZA is a good example of why the above approach will not work
-- -- -- exists in inpmast, bopmast, but not tool
-- -- -- select * -- shows inpmast only, i don't know why it doesn't show the bopmast data
-- -- -- -- because they are separate rows!!! interesting
-- -- -- from temp_dates
-- -- -- where inpmast_stock_number = '22991XXZA' or stock_number = '22991XXZA'

create temp table stock as
select * --87767
from (
  select stocknumber 
  from ads.ext_vehicle_inventory_items
  union 
  select inpmast_stock_number
  from arkona.xfm_inpmast
  union 
  select stock_number
  from sls.ext_bopmast_partial) a
where left(a.stocknumber, 1) in ('1','2','3','4','5','6','7','8','9','H') -- excludes crookston & junk

drop table if exists temp_dates;
create temp table temp_dates as
select *
from stock a
left join (
  select a.stocknumber as tl_stock_number, b.vin as tl_vin, 
    a.fromts::date as vii_from, a.thruts::date as vii_thru,
    c.soldts::date as sold_date, d.soldts::date as sale_cancel_date
-- tool    
  from ads.ext_vehicle_inventory_items a
  inner join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
  left join ads.ext_vehicle_sales c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
    and c.status = 'VehicleSale_Sold'
  left join ads.ext_vehicle_sales d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
    and d.status = 'VehicleSale_SaleCanceled'
  where left(stocknumber,1) <> 'C') b  on a.stocknumber = b.tl_stock_number
left join (
  select inpmast_stock_number, inpmast_vin,
      case
        when a.date_in_invent = 0 then null
        else
          (left(a.date_in_invent::text,4) || '-' || 
            substring(a.date_in_invent::text from 5 for 2) || '-' || 
            substring(a.date_in_invent::text from 7 for 2))::date 
      end as date_in_invent,
      case
        when a.date_delivered = 0 then null
        else 
          (left(a.date_delivered::text,4) || '-' || 
            substring(a.date_delivered::text from 5 for 2) || '-' || 
            substring(a.date_delivered::text from 7 for 2))::date 
      end as date_delivered
  from arkona.xfm_inpmast a   
  where inpmast_Stock_number is not null
    and current_row = true
    and left(inpmast_stock_number, 1) <> 'C')  c on a.stocknumber = c.inpmast_stock_number
left join (
  select bopmast_id, stock_number, vin, vehicle_type, sale_type,
    buyer_bopname_id, capped_date, delivery_date
  from sls.ext_bopmast_partial
  where left(stock_number, 1) <> 'C') d on a.stocknumber = d.stock_number;    


select *
from temp_dates  
-- where tl_stock_number is null
where right(stocknumber, 1) not in ('1','2','3','4','5','6','7','8','9','0')
  and coalesce(sold_date, '12/31/9999') > '12/31/2011'
  and coalesce(date_delivered, '12/31/9999') > '12/31/2011'
  and coalesce(delivery_date, '12/31/9999') > '12/31/2011'
order by stocknumber


-- no dates, must all be service only vehicles
select *
from (
  select *
  from temp_dates  
  -- where tl_stock_number is null
  where right(stocknumber, 1) not in ('1','2','3','4','5','6','7','8','9','0')
    and coalesce(sold_date, '12/31/9999') > '12/31/2011'
    and coalesce(date_delivered, '12/31/9999') > '12/31/2011'
    and coalesce(delivery_date, '12/31/9999') > '12/31/2011') a
where sold_date is null
  and date_delivered is null
  and delivery_date is null    
  and vii_from is null
  and date_in_invent is null 

-- ok, need accounting dates
-- sale accounts: date sold -----------------------------------------------------------------------------------------------------
select control, the_date, department, account_type, sum(amount)
-- select *
from greg.uc_base_vehicle_accounting a
where page between 16 and 17
  and store_code = 'ry1'
  and exists (
    select 1
    from greg.uc_base_vehicle_accounting
    where control = a.control
      and the_date = '07/15/2017')
group by control, the_date, department, account_type      
order by control, the_date      


select control, count(*)
from (
  select a.control, a.the_date, count(*)
  -- select *
  from greg.uc_base_vehicle_accounting a
  inner join greg.uc_base_vehicles aa on a.control = aa.stock_number
  where a.page between 16 and 17
    and a.store_code = 'ry1'
  group by a.control, the_date) x
group by control
having count(*) > 1

-- the theory is that the date with the most entries is most likely the sale date
select a.*, 
  count(a.the_date) over (partition by a.control, a.the_date) as date_occurrences
from greg.uc_base_vehicle_accounting a
inner join greg.uc_base_vehicles aa on a.control = aa.stock_number
where a.page between 16 and 17
  and a.store_code = 'ry1'
  and aa.sale_date > '01/01/2017'
order by control, the_date


create index on greg.uc_base_vehicle_accounting(control,the_date);

create temp table sales_sale_date as
select b.control, b.the_date as sale_date
from (
  select a.*, -- 33 sec 112864 rows
    count(a.the_date) over (partition by a.control, a.the_date) as date_occurrences
  from greg.uc_base_vehicle_accounting a
  inner join greg.uc_base_vehicles aa on a.control = aa.stock_number
  where a.page between 16 and 17
    and a.store_code = 'ry1') b
inner join (   
  select control, max(date_occurrences) as date_occurrence
  from (
    select a.*, -- 33 sec 112864 rows
      count(a.the_date) over (partition by a.control, a.the_date) as date_occurrences
    from greg.uc_base_vehicle_accounting a
    inner join greg.uc_base_vehicles aa on a.control = aa.stock_number
    where a.page between 16 and 17
      and a.store_code = 'ry1') x
  group by control) c  on b.control = c.control and b.date_occurrences = c.date_occurrence
group by b.control,  b.the_date ; 

create index on sales_sale_date(control);
create index on sales_sale_date(sale_date);

select * --11204
from greg.uc_base_vehicles a
left join sales_sale_date b on a.stock_number = b.control
where a.sale_date = b.sale_date

-- discrepancies: unwind (30785a)
-- 18710A 2 dates with 2 entries, hmmm non sale entries control <> doc
select a.stock_number, a.sale_date, b.sale_Date, abs(a.sale_date - b.sale_date)
from greg.uc_base_vehicles a
left join sales_sale_date b on a.stock_number = b.control
where a.sale_date <> b.sale_date
order by abs(a.sale_date - b.sale_date)


select a.*, -- 33 sec 112864 rows
  count(a.the_date) over (partition by a.control, a.the_date) as date_occurrences
from greg.uc_base_vehicle_accounting a
inner join greg.uc_base_vehicles aa on a.control = aa.stock_number
where a.page between 16 and 17
  and a.store_code = 'ry1'
  and a.control <> a.doc
--   and control = '18710a'
order by control, the_date
limit 3000


-- start over 8/31
select a.*
from greg.uc_base_vehicle_accounting a
inner join greg.uc_base_vehicles aa on a.control = aa.stock_number
where a.page = 16
  and aa.sale_date between '05/01/2017' and '05/31/2017'
  and a.account_type = 'sale'
  and unit_count = 1


select a.store_code, a.line, count(*)
from greg.uc_base_vehicle_accounting a
inner join greg.uc_base_vehicles aa on a.control = aa.stock_number
where a.page = 16
  and aa.sale_date between '05/01/2017' and '05/31/2017'
  and a.account_type = 'sale'
  and a.unit_count = 1
group by a.store_code, a.line  
  





-- this is the count that matches fs
select store, line, sum(unit_count)
from (
  select d.store, d.gl_account, line,
    a.control,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a   
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201707
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201707
      and b.page = 16 and b.line between 1 and 14-- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
    and aa.journal_code ='VSU' 
    where a.post_status = 'Y' ) x
group by store, line    

 
select year_month, store, line, sum(unit_count)
from (
  select b.year_month, d.store, d.gl_account, line,
    a.control,
    case when a.amount < 0 then 1 else -1 end as unit_count, the_date
  from fin.fact_gl a   
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201204
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description, b.year_month
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201204
      and b.page = 16 and b.line between 1 and 14-- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account and b.year_month = d.year_month
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
    and aa.journal_code ='VSU' 
    where a.post_status = 'Y' ) x
group by year_month, store, line    




select z.*, sold_date,date_delivered,delivery_date,inv_sale
from (
select b.year_month, d.store, d.gl_account, line,
  a.control,
  case when a.amount < 0 then 1 else -1 end as unit_count, the_date
from fin.fact_gl a   
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201204
inner join fin.dim_account c on a.account_key = c.account_key
inner join ( -- d: fs gm_account page/line/acct description
  select f.store, d.gl_account, b.page, b.line, b.line_label, e.description, b.year_month
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201204
    and b.page = 16 and b.line between 1 and 14-- used cars
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  inner join fin.dim_account e on d.gl_account = e.account
    and e.account_type_code = '4'
  inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account and b.year_month = d.year_month
inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
  and aa.journal_code ='VSU' 
  where a.post_status = 'Y') z
left join temp_dates zz on z.control = zz.stocknumber  




-- inventory: date acquired & sold -----------------------------------------------------------------------------------------------------------

select *
from greg.uc_base_vehicle_accounting a
inner join greg.uc_base_vehicles aa on a.control = aa.stock_number
where a.page = 1
  and aa.sale_date > '01/01/2017'
  and account_description not like '%PA%' 
  and amount > 0
order by control, the_date

-- where running total = 0, does that = sale date?
-- both dates in one query
select m.control, m.inv_acq, n.inv_sale
from ( -- inv acq: min date 
  select control, min(the_date) as inv_acq
  from (
    select a.*, 
      sum(amount) over (partition by control order by control, the_date) as total
    from greg.uc_base_vehicle_accounting a
    inner join greg.uc_base_vehicles aa on a.control = aa.stock_number
    where a.page = 1) x
  group by control) m
left join ( -- inv sale: running total = 0
  select control, the_date as inv_sale
  from (
    select a.*, 
      sum(amount) over (partition by control order by control, the_date) as total
    from greg.uc_base_vehicle_accounting a
    inner join greg.uc_base_vehicles aa on a.control = aa.stock_number
    where a.page = 1) x
  where total = 0    
  group by control, the_date) n on m.control = n.control


select a.*,
  sum(amount) over (partition by control order by control, the_date)
from greg.uc_base_vehicle_accounting a
inner join greg.uc_base_vehicles aa on a.control = aa.stock_number
where a.control = '30936xxa'  
order by the_Date


select b.the_date, c.account, c.description, d.journal_code, a.amount, e.description, 
  account_type_code, account_type, department
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and control = '30936xxa'
  and journal_code = 'VSU'
  and account_type = 'sale'
  and department = 'used vehicle'  













-- includes
--   tool
--   inpmast
--   bopmast
--   accounting inventory: acq & sold

drop table if exists temp_dates;
create temp table temp_dates as
select *
from stock a
left join (  --tool
  select a.stocknumber as tl_stock_number, b.vin as tl_vin, 
    a.fromts::date as vii_from, a.thruts::date as vii_thru,
    c.soldts::date as sold_date, d.soldts::date as sale_cancel_date
  from ads.ext_vehicle_inventory_items a
  inner join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
  left join ads.ext_vehicle_sales c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
    and c.status = 'VehicleSale_Sold'
  left join ads.ext_vehicle_sales d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
    and d.status = 'VehicleSale_SaleCanceled'
  where left(stocknumber,1) <> 'C') b  on a.stocknumber = b.tl_stock_number
left join ( --inpmast
  select inpmast_stock_number, inpmast_vin,
      case
        when a.date_in_invent = 0 then null
        else
          (left(a.date_in_invent::text,4) || '-' || 
            substring(a.date_in_invent::text from 5 for 2) || '-' || 
            substring(a.date_in_invent::text from 7 for 2))::date 
      end as date_in_invent,
      case
        when a.date_delivered = 0 then null
        else 
          (left(a.date_delivered::text,4) || '-' || 
            substring(a.date_delivered::text from 5 for 2) || '-' || 
            substring(a.date_delivered::text from 7 for 2))::date 
      end as date_delivered
  from arkona.xfm_inpmast a   
  where inpmast_Stock_number is not null
    and current_row = true
    and left(inpmast_stock_number, 1) <> 'C')  c on a.stocknumber = c.inpmast_stock_number
left join ( -- bopmast
  select bopmast_id, stock_number, vin, vehicle_type, sale_type,
    buyer_bopname_id, capped_date, delivery_date
  from sls.ext_bopmast_partial
  where left(stock_number, 1) <> 'C') d on a.stocknumber = d.stock_number
left join ( --accounting inventory  
  select m.control, m.inv_acq, n.inv_sale
  from (
    select control, min(the_date) as inv_acq
    from (
      select a.*, 
        sum(amount) over (partition by control order by control, the_date) as total
      from greg.uc_base_vehicle_accounting a
      inner join greg.uc_base_vehicles aa on a.control = aa.stock_number
      where a.page = 1) x
    group by control) m
  left join ( 
    select control, the_date as inv_sale
    from (
      select a.*, 
        sum(amount) over (partition by control order by control, the_date) as total
      from greg.uc_base_vehicle_accounting a
      inner join greg.uc_base_vehicles aa on a.control = aa.stock_number
      where a.page = 1) x
    where total = 0    
    group by control, the_date) n on m.control = n.control
  group by m.control, m.inv_acq, n.inv_sale) e on a.stocknumber = e.control


select *
from temp_dates
where right(stocknumber, 1) not in ('1','2','3','4','5','6','7','8','9','0','R')
  and coalesce(sold_date, '12/31/9999') > '12/31/2011'
  and coalesce(date_delivered, '12/31/9999') > '12/31/2011'
  and coalesce(delivery_date, '12/31/9999') > '12/31/2011'
  and vii_from = date_in_invent 
  and date_in_invent = inv_acq
order by stocknumber



  