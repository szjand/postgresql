﻿E:\Intranet SQL Scripts\crayon report\availability_2

E:\python projects\greg_availability

-- every vehicle has some status
select *
from greg.uc_base_vehicles a
left join ads.ext_vehicle_inventory_item_statuses b on a. vehicle_inventory_item_id = b.vehicleinventoryitemid
where b.vehicleinventoryitemid is null 


select a.*, b.fromts::date, b.thruts::date, daterange(b.fromts::date, b.thruts::date)
-- select a.*, b.fromts::date, b.thruts::date
from greg.uc_base_vehicles a
left join ads.ext_vehicle_inventory_item_statuses b on a. vehicle_inventory_item_id = b.vehicleinventoryitemid
where b.status = 'RMFlagPulled_Pulled'
  and fromts::date = thruts::date
limit 1000


select a.stock_number, b.fromts::date, b.thruts::date, 
  daterange(b.fromts::date, b.thruts::date), lower_inc(daterange(b.fromts::date, b.thruts::date)),
  daterange(b.fromts::date, b.thruts::date, '[]') as "[] lower_inc upper_inc",
  daterange(b.fromts::date, b.thruts::date, '[)') as "[) lower_inc upper exc",
  daterange(b.fromts::date, b.thruts::date, '()') as "() lower exc upper exc",
  daterange(b.fromts::date, b.thruts::date, '(]') as "(] lower exc upper inc"
-- select a.*, b.fromts::date, b.thruts::date
from greg.uc_base_vehicles a
left join ads.ext_vehicle_inventory_item_statuses b on a. vehicle_inventory_item_id = b.vehicleinventoryitemid
where b.status = 'RMFlagPulled_Pulled'
  and fromts <= thruts
limit 100

-- bogus rows: from > thru
select a.stock_number, a.vehicle_inventory_item_id, b.fromts::date, b.thruts::date
-- select a.*, b.fromts::date, b.thruts::date
from greg.uc_base_vehicles a
left join ads.ext_vehicle_inventory_item_statuses b on a. vehicle_inventory_item_id = b.vehicleinventoryitemid
where fromts > thruts

-- more bogus row(s)
select *
from greg.uc_base_vehicles
where date_acquired > sale_date


select distinct category,status from ads.ext_vehicle_inventory_item_statuses order by category,status

drop table if exists pull_avail;
create temp table pull_avail as
select a.stock_number, daterange(a.date_acquired, a.sale_date, '[]') as inv_range, b.pull_range, c.avail_range
from greg.uc_base_vehicles a
left join (
  select a.stock_number,
    case when status = 'RMFlagPulled_Pulled' then daterange(b.fromts::date, b.thruts::date, '[]') end as pull_range
  from greg.uc_base_vehicles a
  left join ads.ext_vehicle_inventory_item_statuses b on a. vehicle_inventory_item_id = b.vehicleinventoryitemid
    and b.status = 'RMFlagPulled_Pulled'
  where fromts <= thruts) b on a.stock_number = b.stock_number
left join (  
  select a.stock_number,
    case when status = 'RMFlagAV_Available' then daterange(b.fromts::date, b.thruts::date, '[]') end as avail_range
  from greg.uc_base_vehicles a
  left join ads.ext_vehicle_inventory_item_statuses b on a. vehicle_inventory_item_id = b.vehicleinventoryitemid
    and b.status = 'RMFlagAV_Available'
  where fromts <= thruts) c on a.stock_number = c.stock_number
where a.date_acquired <= sale_date  
order by a.stock_number;
create index on pull_avail(stock_number);


select count(distinct a.stock_number) -- 426 vehicles w/multiple instances of pull or avail
select * 
from pull_avail a
inner join (
  select stock_number
  from pull_Avail
  group by stock_number
  having count(*) > 1) b on a.stock_number = b.stock_number


18360a
-- 3 instances of pulled
  select a.stock_number, 
    case when status = 'RMFlagPulled_Pulled' then daterange(b.fromts::date, b.thruts::date) end as pull_range
  from greg.uc_base_vehicles a
  left join ads.ext_vehicle_inventory_item_statuses b on a. vehicle_inventory_item_id = b.vehicleinventoryitemid
    and b.status = 'RMFlagPulled_Pulled'
  where stock_number = '18360a'
-- 3 instances of avail
  select a.stock_number, 
    case when status = 'RMFlagAV_Available' then daterange(b.fromts::date, b.thruts::date) end as pull_range
  from greg.uc_base_vehicles a
  left join ads.ext_vehicle_inventory_item_statuses b on a. vehicle_inventory_item_id = b.vehicleinventoryitemid
    and b.status = 'RMFlagAV_Available'
  where stock_number = '18360a'  


-- no dup stock numbers
-- no adjacent, all overlap
select a.*, a.pull_range -|- a.avail_range as adjacent,
  a.pull_range && a.avail_range as overlap
from pull_avail a
left join (
  select stock_number
  from pull_Avail
  group by stock_number
  having count(*) > 1) b on a.stock_number = b.stock_number  
where b.stock_number is null
order by a.pull_range && a.avail_range


---- 8/10/17 ----------------------------------------------------------------------------------------------
/*
in the previous iteration of this process, (ucinv_tmp...) it was configured as
a single row per vehicle with a single period for each status,, so pull was the last pull and available
was the last (max) avail status
other than making the data smoewhat more manageable, not sure what the point was

this time leaning towards, on a given day, a vehicle has a status, raw/pull/avail(fresh/aged)/sold
and i am thinking the range stuff is pretty much irrelevant
days avail not a problem, if on a given day a vehicle is avail, how long has it been avail?
*/



select x.*, lower(pull_range), upper(pull_range)
from (
select a.stock_number, 
    case when status = 'RMFlagPulled_Pulled' then daterange(b.fromts::date, b.thruts::date) end as pull_range
  from greg.uc_base_vehicles a
  left join ads.ext_vehicle_inventory_item_statuses b on a. vehicle_inventory_item_id = b.vehicleinventoryitemid
    and b.status = 'RMFlagPulled_Pulled'
  where stock_number = '18360a') x

-- date_acq = sale_date: days_owned = 1
select * 
from greg.used_vehicle_daily_snapshot
where from_date = sale_date
limit 100

-- what is the diff between sold from inventory and sold from raw?
-- inventory indicates that on that date, the vehicle was in inventory, not sold
-- feels a bit goofy/contrived
-- base question, does an attribute sold_from_status belong in the daily snapshot
-- i think so, but rather than inventory, N/A might be better
select sold_from_Status, count(*)
from greg.ucinv_tmp_avail_4
group by sold_from_status

-- what is the target?
-- 1 row per vehicle/status/day
-- do i want to jump directly to a zillion rows 1 row for each day in a status,
-- did not do that for price (yet)
-- but base_vehicle_days is a start
-- lets go for it: go for the granularity
-- pulled: 81234 rows
-- pulled & avail: 1025689 rows

THIS IS PREMATURE, NOT READY YET
IF FOR NO OTHER REASON THAN IT DOES NOT HANDLE THE FACT THAT OFTEN THE LAST DAY OF PULLED = 1ST DAY OF AVAIL
A VEHICLE CAN NOT HAVE MULTIPLE STATUSES ON A SINGLE DAY
drop table if exists greg.uc_base_vehicle_statuses;
create table greg.uc_base_vehicle_statuses (
  vehicle_inventory_item_id citext not null references greg.uc_base_vehicles(vehicle_inventory_item_id),
  status citext not null, 
  the_date date not null,
  primary key (vehicle_inventory_item_id, the_date));
create index on greg.uc_base_vehicle_statuses(status);
comment on column greg.uc_base_vehicle_statuses.status is 'from ads::dpsvseries.vehicleinventoryitemstatuses.status'; 
insert into greg.uc_base_vehicle_statuses
select a.vehicle_inventory_item_id, 'pulled'::citext, c.the_date
from greg.uc_base_vehicles a
inner join ads.ext_vehicle_inventory_item_statuses b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
  and b.status = 'RMFlagPulled_Pulled'
inner join dds.dim_date c on c.the_date between b.fromts::date and b.thruts::date  
union
select a.vehicle_inventory_item_id, 'avail'::citext, c.the_date
from greg.uc_base_vehicles a
inner join ads.ext_vehicle_inventory_item_statuses b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
  and b.status = 'RMFlagAV_Available'
inner join dds.dim_date c on c.the_date between b.fromts::date and b.thruts::date    






select a.vehicle_inventory_item_id, 'pulled'::citext as status, c.the_date
from greg.uc_base_vehicles a
inner join ads.ext_vehicle_inventory_item_statuses b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
  and b.status = 'RMFlagPulled_Pulled'
inner join dds.dim_date c on c.the_date between b.fromts::date and b.thruts::date  
where a.vehicle_inventory_item_id = '0007243b-dc6b-4e4e-b67d-a30155b650bb'
union
select a.vehicle_inventory_item_id, 'avail'::citext, c.the_date
from greg.uc_base_vehicles a
inner join ads.ext_vehicle_inventory_item_statuses b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
  and b.status = 'RMFlagAV_Available'
inner join dds.dim_date c on c.the_date between b.fromts::date and b.thruts::date    
where a.vehicle_inventory_item_id = '0007243b-dc6b-4e4e-b67d-a30155b650bb'
order by the_date, status


select *
from (
  select a.stock_number, a.vehicle_inventory_item_id, 'pulled'::citext, c.the_date
  from greg.uc_base_vehicles a
  inner join ads.ext_vehicle_inventory_item_statuses b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
    and b.status = 'RMFlagPulled_Pulled'
  inner join dds.dim_date c on c.the_date between b.fromts::date and b.thruts::date) a  
inner join (
  select a.stock_number, a.vehicle_inventory_item_id, 'avail'::citext, c.the_date
  from greg.uc_base_vehicles a
  inner join ads.ext_vehicle_inventory_item_statuses b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
    and b.status = 'RMFlagAV_Available'
  inner join dds.dim_date c on c.the_date between b.fromts::date and b.thruts::date) b  on a.vehicle_inventory_item_id = b.vehicle_inventory_item_id
  and a.the_date = b.the_date  


/*
what, i think, i want is 1 row per vehicle/day with status on each day
just not sure how to get there
what VIIStatuses gives me are the days when status pulled or avail
don't know whether to do rows or columns
need to create a timeline
not every vehicle will have an instance of every status
trying to mimic what i did before falls apart unless i concede to only 1 row per vehicle, ie, max status instance only
need to create a small subset to play with
*/  


drop table if exists test;
create temp table test as 
select a.vehicle_inventory_item_id, 'pulled'::citext, c.the_date
from greg.uc_base_vehicles a
inner join ads.ext_vehicle_inventory_item_statuses b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
  and b.status = 'RMFlagPulled_Pulled'
inner join dds.dim_date c on c.the_date between b.fromts::date and b.thruts::date  
union
select a.vehicle_inventory_item_id, 'avail'::citext, c.the_date
from greg.uc_base_vehicles a
inner join ads.ext_vehicle_inventory_item_statuses b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
  and b.status = 'RMFlagAV_Available'
inner join dds.dim_date c on c.the_date between b.fromts::date and b.thruts::date    


select stock_number  -- 61 rows some sold, some not
from greg.uc_base_vehicles
where date_acquired between '07/01/2017' and '07/08/2017' 

-- make it 1 row per veh/day
-- this looks promising for a one step 1 row per veh/day generator
select a.stock_number, a.date_acquired, a.sale_date, b.the_date, c.vehicleinventoryitemid as pulled, d.vehicleinventoryitemid as avail
from greg.uc_base_vehicles a
inner join dds.dim_date b on b.the_date between a.date_acquired
  and 
    case
      when a.sale_date > current_date then current_date
      else a.sale_date
    end
left join ads.ext_vehicle_inventory_item_statuses c on a.vehicle_inventory_item_id = c.vehicleinventoryitemid
  and c.status = 'RMFlagPulled_Pulled'  
  and b.the_date between c.from_date and c.thru_date
left join ads.ext_vehicle_inventory_item_statuses d on a.vehicle_inventory_item_id = d.vehicleinventoryitemid
  and d.status = 'RMFlagAV_Available'  
  and b.the_date between d.from_date and d.thru_date
where a.date_acquired between '07/01/2017' and '07/08/2017' 


create index on ads.ext_vehicle_inventory_item_statuses(vehicleinventoryitemid);
create index on ads.ext_vehicle_inventory_item_statuses(fromts);
create index on ads.ext_vehicle_inventory_item_statuses(thruts);
create index on ads.ext_vehicle_inventory_item_statuses(status);


alter table ads.ext_vehicle_inventory_item_statuses
add column from_date date,
add column thru_date date;
update ads.ext_vehicle_inventory_item_statuses
set from_date = fromts::date,
    thru_date = thruts::date;
create index on ads.ext_vehicle_inventory_item_statuses(from_date);
create index on ads.ext_vehicle_inventory_item_statuses(thru_date);    

create index on greg.uc_base_vehicles(date_acquired);
create index on greg.uc_base_vehicles(sale_date);

drop table if exists test;
create temp table test as
select a.stock_number, a.vehicle_inventory_item_id, a.date_acquired, a.sale_date, b.the_date, 
  case
    when c.vehicleinventoryitemid is null and d.vehicleinventoryitemid is null then 'raw' 
    when c.vehicleinventoryitemid is not null and d.vehicleinventoryitemid is null then 'pulled' 
    when d.vehicleinventoryitemid is not null then 
      case 
  end as status
from greg.uc_base_vehicles a
inner join dds.dim_date b on b.the_date between a.date_acquired
  and 
    case
      when a.sale_date > current_date then current_date
      else a.sale_date
    end
left join ads.ext_vehicle_inventory_item_statuses c on a.vehicle_inventory_item_id = c.vehicleinventoryitemid
  and c.status = 'RMFlagPulled_Pulled'  
  and b.the_date between c.from_date and c.thru_date
left join ads.ext_vehicle_inventory_item_statuses d on a.vehicle_inventory_item_id = d.vehicleinventoryitemid
  and d.status = 'RMFlagAV_Available'  
  and b.the_date between d.from_date and d.thru_date;
alter table test alter column status set not null;
create index on test(vehicle_inventory_item_id);
create index on test(the_date);
create index on test(status);

-- 
-- -- ok
-- select * from test where status not in ('raw','pulled','avail')
-- 
-- ok, it is possible for a vehicle to have been avail twice on a day, it is not a problem
-- eg
-- becomes avail at 9AM, at noon a pricing rinse is added which closes the avail, pricing rinse done at 3PM, now it is avail again
-- just do a group, should be one row per vehicle per day
-- select stock_number, the_date from test group by stock_number, the_date having count(*) > 1
-- 

this looks good
check out the anomalies from anomalies_2
shit, seems like they do not matter

-- no vehicles with multiple sale dates
select stock_number
from (
select stock_number, sale_date
from test
group by stock_number, sale_date) a
group by stock_number 
having count(*) > 1




do i want vehicle level data in this table, eg sold from status, 




select a.*,
  (the_date - date_acquired) + 1 as days_owned,
  case 
    when status = 'avail' then
      (the_date + 1) -
        first_value(the_date) over (
            partition by vehicle_inventory_item_id,status 
            order by vehicle_inventory_item_id, the_date) 
    else 0
  end as days_avail,
  case when sale_date = the_date then status end as sold_from_status
from test a
-- where stock_number = '26929xxa'
order by the_date
limit 1000


-- going with the granular base tables approach
-- use them to assemble vehicle and vehicle_days tables
drop table if exists greg.uc_base_vehicle_statuses;
create table greg.uc_base_vehicle_statuses (
  vehicle_inventory_item_id citext not null references greg.uc_base_vehicles(vehicle_inventory_item_id),
  the_date date not null,
  status citext not null,
  primary key (vehicle_inventory_item_id,the_date));
insert into greg.uc_base_vehicle_statuses    
select vehicle_inventory_item_id, the_date, status
from (
  select a.vehicle_inventory_item_id, a.the_date, 
    case
      when b.vehicleinventoryitemid is null and c.vehicleinventoryitemid is null then 'raw' 
      when b.vehicleinventoryitemid is not null and c.vehicleinventoryitemid is null then 'pulled' 
      when c.vehicleinventoryitemid is not null then 'avail'
    end as status
  from greg.uc_base_vehicle_days a
  left join ads.ext_vehicle_inventory_item_statuses b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
    and b.status = 'RMFlagPulled_Pulled'  
    and a.the_date between b.fromts::date and b.thruts::date
  left join ads.ext_vehicle_inventory_item_statuses c on a.vehicle_inventory_item_id = c.vehicleinventoryitemid
    and c.status = 'RMFlagAV_Available'  
    and a.the_date between c.fromts::date and c.thruts::Date) e
group by vehicle_inventory_item_id, the_date, status;
create index on greg.uc_base_vehicle_statuses(status);
comment on column greg.uc_base_vehicle_statuses.status is 'from ads::dpsvseries.vehicleinventoryitemstatuses.status';   

alter table
select count(*) from greg.uc_Base_vehicle_statuses  --1069993



select *
from (
  select vehicle_inventory_item_id, the_date, status
  from (
    select a.vehicle_inventory_item_id, a.the_date, 
      case
        when b.vehicleinventoryitemid is null and c.vehicleinventoryitemid is null then 'raw' 
        when b.vehicleinventoryitemid is not null and c.vehicleinventoryitemid is null then 'pulled' 
        when c.vehicleinventoryitemid is not null then 'avail'
      end as status
    from greg.uc_base_vehicle_days a
    left join ads.ext_vehicle_inventory_item_statuses b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
      and b.status = 'RMFlagPulled_Pulled'  
      and a.the_date between b.fromts::date and b.thruts::date
    left join ads.ext_vehicle_inventory_item_statuses c on a.vehicle_inventory_item_id = c.vehicleinventoryitemid
      and c.status = 'RMFlagAV_Available'  
      and a.the_date between c.fromts::date and c.thruts::Date) e
  group by vehicle_inventory_item_id, the_date, status) x
where not exists (
  select 1
  from greg.uc_base_vehicle_statuses
  where vehicle_inventory_item_id = x.vehicle_inventory_item_id
    and the_date = x.the_date);



select * from ads.ext_vehicle_inventory_item_statuses order by fromts desc limit 500

alter table ads.ext_vehicle_inventory_item_statuses
alter column fromts 

select * from ads.ext_vehicle_pricings limit 100

ads.ext_vehicle_inventory_item_statuses



select a.*,
  case
    when
      (the_date + 1) -
        first_value(the_date) over (
            partition by vehicle_inventory_item_id,status 
            order by vehicle_inventory_item_id, the_date) < 31 then 'avail_fresh'
    else 'avail_aged'
  end as new_status
from greg.uc_Base_vehicle_statuses a
where status = 'avail'
limit 5000


create temp table test as
select *
from greg.uc_Base_vehicle_statuses a
where status = 'avail'


update test z
set status = x.new_status
from (
  select a.*,
    case
      when
        (the_date + 1) -
          first_value(the_date) over (
              partition by vehicle_inventory_item_id,status 
              order by vehicle_inventory_item_id, the_date) < 31 then 'avail_fresh'
      else 'avail_aged'
    end as new_status
  from greg.uc_Base_vehicle_statuses a
  where status = 'avail') x
where z.vehicle_inventory_item_id = x.vehicle_inventory_item_id
  and z.the_date = x.the_date  



select *
from test
where status like 'avail%'
order by vehicle_inventory_item_id, the_date
limit 1000


-- 8/11,
-- going with this, first update all rows where status = avail
-- then replace the update function with this
-- nope not going to work, need the full list of days a vehicle was available to determine fresh/aged
-- add a separate column for fresh/aged: avail_status, and update that column only where the status is avail
-- after insert do the update
select x.vehicle_inventory_item_id, the_date,
  case
    when status = 'avail' then 
      case
        when
          (the_date + 1) -
            first_value(the_date) over (
                partition by vehicle_inventory_item_id,status 
                order by vehicle_inventory_item_id, the_date) < 31 then 'avail_fresh'
        else 'avail_aged'
      end   
    else status
  end    
from (     
  select a.vehicle_inventory_item_id, a.the_date, 
      case
        when b.vehicleinventoryitemid is null and c.vehicleinventoryitemid is null then 'raw' 
        when b.vehicleinventoryitemid is not null and c.vehicleinventoryitemid is null then 'pulled' 
        when c.vehicleinventoryitemid is not null then 'avail'
      end as status
  from greg.uc_base_vehicle_days a
  left join ads.ext_vehicle_inventory_item_statuses b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
    and b.status = 'RMFlagPulled_Pulled'  
    and a.the_date between b.fromts::date and b.thruts::date
  left join ads.ext_vehicle_inventory_item_statuses c on a.vehicle_inventory_item_id = c.vehicleinventoryitemid
    and c.status = 'RMFlagAV_Available'  
    and a.the_date between c.fromts::date and c.thruts::Date) x;


update greg.uc_Base_vehicle_statuses z
set status = x.new_status
from (
  select a.*,
    case
      when
        (the_date + 1) -
          first_value(the_date) over (
              partition by vehicle_inventory_item_id,status 
              order by vehicle_inventory_item_id, the_date) < 31 then 'avail_fresh'
      else 'avail_aged'
    end as new_status
  from greg.uc_Base_vehicle_statuses a
  where status = 'avail') x
where z.vehicle_inventory_item_id = x.vehicle_inventory_item_id
  and z.the_date = x.the_date;      



delete from ops.ads_extract where the_date = current_date and task = 'ext_vehicle_inventory_item_statuses'


delete
-- select *
from greg.uc_base_vehicle_statuses
where the_Date = current_date


select *
from greg.uc_Base_vehicle_statuses
where vehicle_inventory_item_id = 'e3cd49a1-6695-4d04-a047-30f366a9ec09'



select * 
from greg.uc_base_vehicle_statuses
where status like 'avail%'
limit 500


-- 1. all avail_ back to avail
update greg.uc_base_vehicle_statuses
set status = 'avail'
where status like 'avail%';

-- 2. add the attribute
alter table greg.uc_base_vehicle_statuses
add column avail_status citext;

alter table greg.uc_base_vehicle_statuses
rename status to temp_status;

alter table greg.uc_base_vehicle_statuses
rename new_status to status;

-- 3. populate the new attribute
update greg.uc_Base_vehicle_statuses z
set status = x.status
from (
  select a.*,
  case
    when temp_status = 'avail' then 
      case
        when
          (the_date + 1) -
            first_value(the_date) over (
                partition by vehicle_inventory_item_id,temp_status 
                order by vehicle_inventory_item_id, the_date) < 31 then 'avail_fresh'
        else 'avail_aged'
      end   
    else temp_status
  end as status
  from greg.uc_base_vehicle_statuses a) x
where z.vehicle_inventory_item_id = x.vehicle_inventory_item_id
  and z.the_date = x.the_date;   

-- 4. get the indices correct
drop index greg.uc_base_vehicle_statuses_status_idx;
create index on greg.uc_base_vehicle_statuses(temp_status);
create index on greg.uc_base_vehicle_statuses(status);



select *
from greg.uc_Base_vehicle_statuses
order by vehicle_inventory_item_id, the_date
limit 5000 

select count(*)  -- 1070483
from greg.uc_Base_vehicle_statuses

select max(the_date)
from greg.uc_Base_vehicle_statuses

select distinct status
from greg.uc_Base_vehicle_statuses


update greg.uc_base_vehicle_statuses z
set status = x.avail_status
from (
  select a.*,
  case
    when temp_status = 'avail' then 
      case
        when
          (the_date + 1) -
            first_value(the_date) over (
                partition by vehicle_inventory_item_id,temp_status 
                order by vehicle_inventory_item_id, the_date) < 31 then 'avail_fresh'
        else 'avail_aged'
      end   
    else temp_status
  end as avail_status
  from greg.uc_base_vehicle_statuses a) x
where z.vehicle_inventory_item_id = x.vehicle_inventory_item_id
  and z.the_date = x.the_date
  and z.status is null;  


select *
from luigi.luigi_log
order by from_ts desc
limit 100


-- shit, need days_avail too

-- 1. add the new attribute
alter table greg.uc_base_vehicle_statuses
add column days_avail integer;

-- 2. populate the new attribute
update greg.uc_base_vehicle_statuses z
set days_avail = x.days_avail
from (
  select a.vehicle_inventory_item_id, a.the_date,
    (the_date + 1) -
      first_value(the_date) over (
          partition by vehicle_inventory_item_id
          order by vehicle_inventory_item_id, the_date)  as days_avail
  from greg.uc_base_vehicle_statuses a
  where temp_status = 'avail') x
where z.vehicle_inventory_item_id = x.vehicle_inventory_item_id
  and z.the_date = x.the_date
  and z.temp_status = 'avail';   



-------------------------------------------------------------------------------------------------------------------------------------
--</ 9/20 ---------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------
select *
from greg.uc_base_vehicle_statuses
limit 1000

as i try to get back into this flow
what about back ons, status a stocknumber is sold for a period of time and then back into inventory
is this statuses table the place for that, 
it is not currently set up to accomodate that i think
because of the single date structure one row per vehicle per date
as opposed to a from and thru date
raw from 1/2/17 thru 2/1/17
avail from 2/2/17 thru 4/8/17
etc 
do not remember well enuf how i was considering this before, even after reading thru everything above

-- it is not in base_vehicles, sale date is the final sale date
select stock_number
from uc.base_vehicles
group by stock_number
having count(*) > 1

need to look at some real data

select aa.stocknumber, a.* 
from ads.ext_Vehicle_sales a
inner join ads.ext_vehicle_inventory_items aa on a.vehicleinventoryitemid = aa.vehicleinventoryitemid
inner join (
select vehicleinventoryitemid
from ads.ext_Vehicle_sales
group by vehicleinventoryitemid
having count(*) > 1) b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
order by a.vehicleinventoryitemid, a.soldts

here is a good example of an unwind with fucked up data in greg.uc_base_vehicle_statuses
sold and delivered 6/17, 6/23 back in raw, pulled 6/23
but the data 6/18 - 6/22 shows raw with > 260 days in status ???
select * 
from greg.uc_base_vehicle_statuses
where vehicle_inventory_item_id = '77d3a5f8-658e-4ecd-b80f-72858de78e6b'
order by the_Date

-- which makes sense when i look at the query that generates the data:
-- my gut is saying not good enough
  select vehicle_inventory_item_id, the_date, status
  from (
    select a.vehicle_inventory_item_id, a.the_date, 
      case
        when b.vehicleinventoryitemid is null and c.vehicleinventoryitemid is null then 'raw' 
        when b.vehicleinventoryitemid is not null and c.vehicleinventoryitemid is null then 'pulled' 
        when c.vehicleinventoryitemid is not null then 'avail'
      end as status
    from greg.uc_base_vehicle_days a
    left join ads.ext_vehicle_inventory_item_statuses b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
      and b.status = 'RMFlagPulled_Pulled'  
      and a.the_date between b.fromts::date and b.thruts::date
    left join ads.ext_vehicle_inventory_item_statuses c on a.vehicle_inventory_item_id = c.vehicleinventoryitemid
      and c.status = 'RMFlagAV_Available'  
      and a.the_date between c.fromts::date and c.thruts::Date
    where a.vehicle_inventory_item_id = '77d3a5f8-658e-4ecd-b80f-72858de78e6b') e
  group by vehicle_inventory_item_id, the_date, status order by the_Date

-- need a better timeline type of approach

select vehicleinventoryitemid, status, category, fromts::date as from_date, thruts::date as thru_date
from ads.ext_vehicle_inventory_item_statuses
where vehicleinventoryitemid = '77d3a5f8-658e-4ecd-b80f-72858de78e6b'
  and status in ('RawMaterials_RawMaterials','RMFlagPulled_Pulled','RMFlagAV_Available','RawMaterials_Sold','RawMaterials_Delivered')
order by fromts

first question, is the acquisition date the RawMaterials_RawMaterials date or the RMFlagRMP_RawMaterialsPool Date
-- it is RawMaterials_RawMaterials
select a.stock_number, a.date_acquired, e.from_date
from uc.base_vehicles a
left join (
  select d.stocknumber, min(c.fromts::date) as from_date
  from ads.ext_vehicle_inventory_item_statuses c
  inner join ads.ext_vehicle_inventory_items d on c.vehicleInventoryitemid = d.vehicleInventoryitemid
  group by d.stocknumber) e on a.stock_number = e.stocknumber
limit 100

second question
is sale_date RawMaterials_sold or RawMaterialsDelivered
-- does not seem to matter
select a.stock_number, a.sale_date, e.sold_date, f.del_date
from uc.base_vehicles a
left join (
  select d.stocknumber, min(c.fromts::date) as sold_date
  from ads.ext_vehicle_inventory_item_statuses c
  inner join ads.ext_vehicle_inventory_items d on c.vehicleInventoryitemid = d.vehicleInventoryitemid
  where status = 'RawMaterials_Sold'
  group by d.stocknumber) e on a.stock_number = e.stocknumber
left join (
  select d.stocknumber, min(c.fromts::date) as del_date
  from ads.ext_vehicle_inventory_item_statuses c
  inner join ads.ext_vehicle_inventory_items d on c.vehicleInventoryitemid = d.vehicleInventoryitemid
  where status = 'RawMaterials_Delivered'
  group by d.stocknumber) f on a.stock_number = f.stocknumber  
where a.date_acquired between '06/01/2016' and '06/01/2017'
  

are there (historically) vehicles with sold but not delivered 
-- nope
select aa.stocknumber, a.vehicleinventoryitemid, a.status, a.category, a.fromts::date as from_date, a.thruts::date as thru_date
from ads.ext_vehicle_inventory_item_statuses a
inner join ads.ext_vehicle_inventory_items aa on a.vehicleinventoryitemid = aa.vehicleinventoryitemid
where status = 'RawMaterials_Sold'
  and not exists (
    select b.vehicleinventoryitemid
    from ads.ext_vehicle_inventory_item_statuses b
    where b.vehicleinventoryitemid = a.vehicleinventoryitemid
      and status = 'RawMaterials_Delivered')

-- 9/25
where i think i am
look at the array of statuses, which are relevant and form a continuous timeline, no interruptions
maybe the minimum set of statuses to form a continuous status timeline, ie, no gaps
start with 29536xx
acq: 9/27/16
pull: 5/24/17
avail: 5/26/17
sold/delivered: 6/17/17
backon: 6/23/17
pull: 6/23/17
avail: 6/27/17
sold/del: 7/28/17

select status, category, fromts::Date, thruts::date
from ads.ext_vehicle_inventory_item_statuses
where vehicleinventoryitemid = '77d3a5f8-658e-4ecd-b80f-72858de78e6b'
order by fromts

-- -- -- only 74 rows with no RawMaterials_RawMaterials row
-- -- -- of which 71 have no vehicle_inventory_item_id or vehicle_item_id
-- -- select *
-- -- from uc.base_vehicles a
-- -- left join ads.ext_vehicle_inventory_item_statuses b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
-- --   and b.status = 'RawMaterials_RawMaterials'
-- -- where b.vehicleinventoryitemid is null
-- -- order by a.vehicle_item_id


question: are the statuses (from/to) a set of overlapping ranges?

the only thing (i think) i am proposing to do differently is to include the sold status
thinking include sold/delivered, if recon is done after sold but before delivered, then 
it is not post sale recon
i think i am going for each row: stock, status, from, thru

looks like their is a gap in the period between sold and back on
1st thought, thru for delivered should by 12/31/9999 until backed on

select status, category, fromts::Date, thruts::date
from ads.ext_vehicle_inventory_item_statuses
where vehicleinventoryitemid = '77d3a5f8-658e-4ecd-b80f-72858de78e6b'
  and status in ('RawMaterials_RawMaterials','RMFlagPulled_Pulled','RMFlagAV_Available','RawMaterials_Sold','RawMaterials_Delivered')


select aa.stock_number, a.status, a.thruts::date, b.status, b.fromts::date, b.thruts::date
from ads.ext_vehicle_inventory_item_statuses a
inner join uc.base_vehicles aa on a.vehicleinventoryitemid = aa.vehicle_inventory_item_id
inner join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
where a.status = 'RawMaterials_Delivered'
  and b.status = 'RawMaterials_RawMaterials'
  and b.fromts > a.fromts
order by a.vehicleinventoryitemid, a.fromts 


-- goofy, multiple instances of 'RawMaterials_Sold' on the same day, 54 vehicles
-- won't be a problem, grain = day
select a.vehicleinventoryitemid, a.fromts::date, count(*)
from ads.ext_vehicle_inventory_item_statuses a
where a.status = 'RawMaterials_Sold'
group by a.vehicleinventoryitemid, a.fromts::date
having count(*) > 1
order by count(*) desc

-- RawMaterials_Sold fromts <> thruts, think this is delivered after sold
select *
from ads.ext_vehicle_inventory_item_statuses
where status = 'RawMaterials_Sold'
  and date_trunc('minute', fromts) <> date_trunc('minute',thruts)

-- RawMaterials_Delivered fromts always = thruts
select *
from ads.ext_vehicle_inventory_item_statuses
where status = 'RawMaterials_Delivered'
  and date_trunc('minute', fromts) <> date_trunc('minute',thruts)

select vehicleinventoryitemid, count(*) -- 577 rows
from ads.ext_vehicle_inventory_item_statuses
where status = 'RawMaterials_Delivered' 
group by vehicleinventoryitemid 
having count(*) > 1

select vehicleinventoryitemid, count(*) -- 661 rows
from ads.ext_vehicle_inventory_item_statuses
where status = 'RawMaterials_Sold' 
group by vehicleinventoryitemid 
having count(*) > 1

-- sold but not delivered
-- only the current ones
select aa.stock_number, a.*
from ads.ext_vehicle_inventory_item_statuses a
inner join uc.base_vehicles aa on a.vehicleinventoryitemid = aa.vehicle_inventory_item_id
where status = 'RawMaterials_Sold' 
  and not exists (
    select 1
    from ads.ext_vehicle_inventory_item_statuses b
    where b.status = 'RawMaterials_Delivered'
      and b.vehicleinventoryitemid = a.vehicleinventoryitemid)

-- aha, good example, sold, unwound before delivered      
select *
from ads.ext_vehicle_inventory_items
where vehicleinventoryitemid = '1cd99547-1d85-47d1-8b92-f76c00e34f70'

-- need a subset to play with
select aa.stock_number, aa.date_acquired, aa.sale_date, a.fromts::Date, a.thruts::date, a.status
from ads.ext_vehicle_inventory_item_statuses a
inner join uc.base_vehicles aa on a.vehicleinventoryitemid = aa.vehicle_inventory_item_id
where a.status in ('RawMaterials_RawMaterials','RMFlagPulled_Pulled','RMFlagAV_Available','RawMaterials_Sold','RawMaterials_Delivered')
  and aa.date_acquired between '10/01/2016' and '02/01/2017'
  and a.vehicleinventoryitemid in (
    select b.vehicleinventoryitemid
    from ads.ext_vehicle_inventory_item_statuses b
    where b.status = 'RawMaterials_Sold' 
    group by b.vehicleinventoryitemid
    having count(*) > 1)
order by aa.stock_number, a.fromts::date


-- 28712ra mult sales same day, do grouping
drop table if exists test;
create temp table test as
select aa.stock_number, aa.date_acquired, aa.sale_date, a.fromts::Date as from_date, 
  a.thruts::date as thru_date, a.status, fromts
from ads.ext_vehicle_inventory_item_statuses a
inner join uc.base_vehicles aa on a.vehicleinventoryitemid = aa.vehicle_inventory_item_id
where a.status in ('RawMaterials_RawMaterials','RMFlagPulled_Pulled','RMFlagAV_Available','RawMaterials_Sold','RawMaterials_Delivered')
  and aa.date_acquired between '10/01/2016' and '02/01/2017'
  and a.vehicleinventoryitemid in (
    select b.vehicleinventoryitemid
    from ads.ext_vehicle_inventory_item_statuses b
    where b.status = 'RawMaterials_Sold' 
    group by b.vehicleinventoryitemid
    having count(*) > 1)
-- group by aa.stock_number, aa.date_acquired, aa.sale_date, a.fromts::Date, a.thruts::date, a.status    
order by aa.stock_number, a.fromts::date

-- 9/26
maybe a matrix of all transistions from one status to the next

fromts required for correct ordering

select a.* ,
  lead(status) over (partition by stock_number order by fromts),
  first_value (status) over (partition by stock_number order by fromts),
  last_value (status) over (
    partition by stock_number order by fromts
      range between unbounded preceding and unbounded following)
from test a

so

select distinct status, lead(status) over (partition by stock_number order by fromts)
from test

-- hmm for all statuses only 610 combinations
select distinct a.status, lead(a.status) over (partition by aa.stock_number order by fromts)
from ads.ext_vehicle_inventory_item_statuses a
inner join uc.base_vehicles aa on a.vehicleinventoryitemid = aa.vehicle_inventory_item_id

-- just the relevant statuses
-- 17 combinations
select distinct a.status, lead(a.status) over (partition by aa.stock_number order by fromts)
from ads.ext_vehicle_inventory_item_statuses a
inner join uc.base_vehicles aa on a.vehicleinventoryitemid = aa.vehicle_inventory_item_id
where a.status in ('RawMaterials_RawMaterials','RMFlagPulled_Pulled','RMFlagAV_Available','RawMaterials_Sold','RawMaterials_Delivered')
order by a.status

most of the make sense, but not all, eg pulled -> pulled
-- let's make a big temp table
drop table if exists test;
create temp table test as -- 86290
select aa.stock_number, aa.date_acquired, aa.sale_date, a.fromts::Date as from_date, 
  a.thruts::date as thru_date, a.category, a.status, fromts,
  lead(status) over (partition by stock_number order by fromts),
  first_value (status) over (partition by stock_number order by fromts),
  last_value (status) over (
    partition by stock_number order by fromts
      range between unbounded preceding and unbounded following)
from ads.ext_vehicle_inventory_item_statuses a      
inner join uc.base_vehicles aa on a.vehicleinventoryitemid = aa.vehicle_inventory_item_id
where a.status in ('RawMaterials_RawMaterials','RMFlagPulled_Pulled','RMFlagAV_Available','RawMaterials_Sold','RawMaterials_Delivered')      

select *
from test 
where status = 'RMFlagPulled_Pulled'
  and lead = 'RMFlagPulled_Pulled'

26119A pull from & thru on the same day
select *
from ads.ext_vehicle_inventory_item_statuses a      
inner join uc.base_vehicles aa on a.vehicleinventoryitemid = aa.vehicle_inventory_item_id
where a.status in ('RawMaterials_RawMaterials','RMFlagPulled_Pulled','RMFlagAV_Available','RawMaterials_Sold','RawMaterials_Delivered')  
  and stock_number = '26119a'
order by fromts

select count(*) from uc.base_vehicles

select status, lead, count(*)
from test
group by status, lead
order by status

select *
from test limit 100
status                        lead                        count
RawMaterials_Delivered;       RawMaterials_RawMaterials;    529
RawMaterials_Delivered;           ;                       20993
RawMaterials_Delivered;       RMFlagAV_Available;             1
RawMaterials_Delivered;       RawMaterials_Delivered;         3
RawMaterials_RawMaterials;    RawMaterials_RawMaterials;      2
RawMaterials_RawMaterials;    RawMaterials_Sold;           9995
RawMaterials_RawMaterials;    RMFlagAV_Available;            13
RawMaterials_RawMaterials;    RMFlagPulled_Pulled;        11604
RawMaterials_Sold;            RawMaterials_RawMaterials;     82
RawMaterials_Sold;            RawMaterials_Delivered;     21523
RMFlagAV_Available;           RMFlagPulled_Pulled;          167
RMFlagAV_Available;               ;                          11
RMFlagAV_Available;           RawMaterials_Sold;           9542
RMFlagPulled_Pulled;          RMFlagAV_Available;          9706
RMFlagPulled_Pulled;          RMFlagPulled_Pulled;           53
RMFlagPulled_Pulled;               ;                          1
RMFlagPulled_Pulled;          RawMaterials_Sold;           2065

select *
from test
where status = 'RMFlagAV_Available'
  and lead is null

30976xx sold 7/26, unw 8/4, still in inventory, base_vehicle shows sale date
which just spins me out

select *
from test a
where exists (
  select 1
  from test b
  where b.stock_number = a.stock_number
    and b.status = 'RMFlagAV_Available'
    and b.lead = 'RMFlagPulled_Pulled')
order by a.stock_number, a.fromts


27272P
      avail 2/26/16 - 6/6
      pulled  7/5 - 7/6
      gap in between, why ? !! at auction, need to add additional statuses !!
-------------------------------------------------------------------------------------------------------------------------------------
--/> 9/20 ---------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------------------------------------
--< 10/02 ---------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------
damn, it has been a week already
like the idea of a time line
find the gaps, ie, find what additional statuses i need to prevent gaps
temp table test is all rows from ads.ext_vehicle_inventory_item_statuses
where status is in ('RawMaterials_RawMaterials','RMFlagPulled_Pulled','RMFlagAV_Available','RawMaterials_Sold','RawMaterials_Delivered')  

need a query to expose gaps

-- this exposes the gaps, but i don't think it will work on returned backons in 
-- inventory because of excluding RawMaterials, so, just exclude the status, not the category

-- still, don't know how easily it will generalize/scale, seems rather involved (albeit cool)

-- and i still don't know what i am going to do with the gaps

-- this generates the range of dates
drop table if exists the_dates;
create temp table the_dates as
select generate_series::date as range_dates
from (
  select * 
  from generate_series ((
    select min(from_date) as from_date
    from (
      select stock_number, from_Date, thru_Date, category, status, fromts,
        first_value (status) over (partition by stock_number order by fromts),
        last_value (status) over (
          partition by stock_number order by fromts
            range between unbounded preceding and unbounded following)
      from test
      where stock_number = '27272p'
        and status <> 'RawMaterials_RawMaterials') c
    where status = first_value),  
  (
    select max(thru_date) as thru_date
    from (
      select stock_number, from_Date, thru_Date, category, status, fromts,
        first_value (status) over (partition by stock_number order by fromts),
        last_value (status) over (
          partition by stock_number order by fromts
            range between unbounded preceding and unbounded following)
      from test
      where stock_number = '27272p'
        and status <> 'RawMaterials_RawMaterials') c
    where status = last_value), '1 day')) d;

-- and this shows the gap
select *
from the_dates a
left join (
  select *
  from test
  where stock_number = '27272p'
    and status <> 'RawMaterials_RawMaterials') b on a.range_dates between b.from_date and b.thru_date


select b.stock_number, count(*)
from ads.ext_vehicle_inventory_item_statuses a
inner join uc.base_vehicles b on a.vehicleinventoryitemid = b.vehicle_inventory_item_id
where a.status = 'RawMaterials_Sold'
group by b.stock_number
having count(*) > 2

24299xx: sold 3 times, lets see what that looks like

drop table if exists the_dates;
create temp table the_dates as
select generate_series::date as range_dates
from (
  select * 
  from generate_series ((
    select min(from_date) as from_date
    from (
      select stock_number, from_Date, thru_Date, category, status, fromts,
        first_value (status) over (partition by stock_number order by fromts),
        last_value (status) over (
          partition by stock_number order by fromts
            range between unbounded preceding and unbounded following)
      from test
      where stock_number = '24299xx'
        and status <> 'RawMaterials_RawMaterials') c
    where status = first_value),  
  (
    select max(thru_date) as thru_date
    from (
      select stock_number, from_Date, thru_Date, category, status, fromts,
        first_value (status) over (partition by stock_number order by fromts),
        last_value (status) over (
          partition by stock_number order by fromts
            range between unbounded preceding and unbounded following)
      from test
      where stock_number = '24299xx'
        and status <> 'RawMaterials_RawMaterials') c
    where status = last_value), '1 day')) d;

-- and this shows the gap
select *
from the_dates a
left join (
  select *
  from test
  where stock_number = '24299xx'
    and status <> 'RawMaterials_RawMaterials') b on a.range_dates between b.from_date and b.thru_date


-- back to the approach of adding additional statuses to eliminate (at least some) gaps
-- think i need to start with the notion of coalescing overlapping ranges

-- don't know if this will apply, but rather than eliminating statuses, eliminate min and max

-- date grain, have to have some sort of precedence for multiple statuses on same date

-- before i actually do the lead to get the combination of status sequences, need this part, i think

-- fuzziness, what exactly am i trying to do

-- ok, excluded some superfluous statuses,
-- eliminated first row, the date range of the initial RawMaterials_RawMaterials encompasses the
-- entire inventory period for the vehicle, al subsequent date ranges are a subset of that range

select b.stock_number, a.category, a.status, a.fromts::date as from_date, thruts::date as thru_date, fromts 
from ads.ext_vehicle_inventory_item_statuses a
inner join uc.base_vehicles b on a.vehicleinventoryitemid = b.vehicle_inventory_item_id 
where b.stock_number = '27272P'
  and category not like '%recon%'
  and category not in ('RMFlagFLR','RMFlagPB','RMFlagRB','RMFlagWTF')
  and fromts <> (
    select min(fromts)
    from ads.ext_vehicle_inventory_item_statuses e
    where e.vehicleinventoryitemid = a.vehicleinventoryitemid
      and e.status = 'RawMaterials_RawMaterials')
order by a.fromts


-- work thru the example from the postgres temporal book pdf p108

-- do away with RMFlagIP, RMFlagWP, RMFlagRMP, use from_date as  the origin
-- all i am trying to derive are the internal status ranges, we'll see

drop table if exists ranges;
create temp table ranges as
select stock_number, from_date, thru_date, min(fromts) as fromts, 
--   string_agg(category, '|-|') as category, 
  array_agg(distinct category) as category,
--   string_agg(status, '|-|' order by fromts) as status, 
  array_agg(distinct status order by status) as status_array,
  string_agg(distinct status, ',' order by status) as status_string,
  daterange(from_Date, thru_Date, '[]') r
from (
  select b.stock_number, a.category, a.status, a.fromts::date as from_date, thruts::date as thru_date, fromts 
  from ads.ext_vehicle_inventory_item_statuses a
  inner join uc.base_vehicles b on a.vehicleinventoryitemid = b.vehicle_inventory_item_id 
--   where b.stock_number in ('24299xx', '27272P')
--   where stock_number like '3%'
--   where stock_number like '2%'
   where status in ('RawMaterials_RawMaterials','RMFlagPulled_Pulled','RMFlagAV_Available','RawMaterials_Sold','RawMaterials_Delivered')  
  --   and category not like '%recon%'
  --   and category not in ('RMFlagFLR','RMFlagPB','RMFlagRB','RMFlagWTF')
    and fromts <> ( -- eliminate the first row of RawMaterials_RawMaterials
      select min(fromts)
      from ads.ext_vehicle_inventory_item_statuses e
      where e.vehicleinventoryitemid = a.vehicleinventoryitemid
        and e.status = 'RawMaterials_RawMaterials')) x
group by stock_number, from_date, thru_date;

select a.* , array_position(status_array, 'RawMaterials_Delivered'), cardinality(status_array)
from ranges a
limit 1000;

select *
from ranges
where status_string like '%Del%'

-- multiple simoultaneous statuses
select status_array, min(stock_number), max(stock_number), count(*)
from ranges a
where cardinality(status_array) > 1
group by status_array

{RawMaterials_Sold,RMFlagPulled_Pulled}
{RMFlagAV_Available,RMFlagPulled_Pulled}
{RawMaterials_Delivered,RawMaterials_Sold,RMFlagAV_Available}
{RawMaterials_Delivered,RawMaterials_RawMaterials}
{RawMaterials_Delivered,RawMaterials_RawMaterials,RawMaterials_Sold}
{RawMaterials_Delivered,RawMaterials_Sold}
{RawMaterials_Delivered,RMFlagPulled_Pulled}
{RawMaterials_RawMaterials,RawMaterials_Sold}
{RawMaterials_RawMaterials,RMFlagPulled_Pulled}
{RawMaterials_Delivered,RawMaterials_RawMaterials,RawMaterials_Sold,RMFlagPulled_Pulled}
{RawMaterials_RawMaterials,RMFlagAV_Available}
{RawMaterials_Delivered,RawMaterials_RawMaterials,RawMaterials_Sold,RMFlagAV_Available}
{RawMaterials_Delivered,RawMaterials_Sold,RMFlagAV_Available,RMFlagPulled_Pulled}
{RawMaterials_Delivered,RawMaterials_Sold,RMFlagPulled_Pulled}
{RawMaterials_Delivered,RawMaterials_RawMaterials,RawMaterials_Sold,RMFlagAV_Available,RMFlagPulled_Pulled}

15234XXB is  weird on 3/12 sold then back to raw and pulled within 25 minutes on the same day
weird because it is counter to the assumption that simoultaneous statuses that include sold decode to sold
sequence matters

select count(*)
from uc.base_Vehicles
where stock_number like '3%'
/*
frame: specifies the subset or rows in which the windowing funnction will work
  syntax:
    OVER ( 
           [ <PARTITION BY clause> ]
           [ <ORDER BY clause> ] 
           [ <ROW or RANGE clause> ]
          )
      clause syntax:
        [ROWS | RANGE] BETWEEN <Start expr> AND <End expr>
        Where:
          <Start expr> is one of:
              UNBOUNDED PRECEDING: The window starts in the first row of the partition
              CURRENT ROW: The window starts in the current row
              <unsigned integer literal> PRECEDING or FOLLOWING
          <End expr> is one of:
              UNBOUNDED FOLLOWING: The window ends in the last row of the partition
              CURRENT ROW: The window ends in the current row
              <unsigned integer literal> PRECEDING or FOLLOWING
Remember, if I don’t specify the window frame clause, 
then the default is “range between unbounded preceding and current row”



7.7 VALUES Lists
VALUES (1, 'one'), (2, 'two'), (3, 'three')  -- this returns a "constant table" that can be used in a query
-- Each parenthesized list of expressions generates a row in the table, and is equivalent to:
SELECT 1 AS column1, 'one' AS column2
UNION ALL
SELECT 2, 'two'
UNION ALL
SELECT 3, 'three';

select * from (VALUES (1, 'one'), (2, 'two'), (3, 'three')) as t
-- t(num,letters) is a table alias list, provides a column name for the values list generated by the values list
select * from (VALUES (1, 'one'), (2, 'two'), (3, 'three')) as t(num,letters)

all of that to understand the following
from http://tapoueh.org/blog/2013/08/understanding-window-functions/
-- use array_agg to visualze the window frams

select x, array_agg(x) over (order by x)
from generate_series(1, 3) as t(x);

the window definition is over (order by x)

which actually means:
 select x,
        array_agg(x) over (order by x
                           rows between unbounded preceding
                                    and current row)
   from generate_series(1, 3) as t(x);

-- and a different frame specifications
select x,
       array_agg(x) over (rows between current row
                                   and unbounded following)
  from generate_series(1, 3) as t(x);

-- no fram clause used: the whole set of rows
select x,
       array_agg(x) over () as frame,
       sum(x) over () as sum,
       x::float/sum(x) over () as part
  from generate_series(1, 3) as t(x);

*/

-- ok back to p 108 of book, actually page 111: finding gaps in a set of overlapping ranges

select * from ranges

-- uses progressive cte's for each step of processing
-- step 1
-- the cte, a, is just an ordered list of the ranges,  range sorting is  by lower bound first, then upper bound
with
a as (
  select stock_number, r
  from ranges 
  order by stock_number, r)
select * from a;  

select a.*, lower(r), upper(r) from ranges a where lower(r) = upper(r)
-- step 2  
-- 2 possibilities for any range: 
--    it can overlap with one of the precedding ranges or it can start a new none overlapping range
--  the condintion for not overlapping is that its lower bound is greater than the previous
--  maximum uper bound
--  the rows with a non-null value in low are preceeded by a gap in intervals
--  gap: lower is > upper of the preceding row 
with 
a as (
  select stock_number, r from ranges order by stock_number, r),
b as (  
  select stock_number, r,
    case 
      when max(upper(r)) over (partition by stock_number order by stock_number, r
        rows between unbounded preceding and 1 preceding) >=  lower(r) then null
      else lower(r)
    end as low
  from a)
select * from b 

-- step 3  
-- intervals representing actual gaps have both upper and lower bounds set to finite values
with 
a as (
  select stock_number, r from ranges order by stock_number, r),
b as (
  select stock_number, r,
    case 
      when max(upper(r)) over (partition by stock_number order by stock_number, r
        rows between unbounded preceding and 1 preceding) >= lower(r) then null
      else lower(r) end as low
  from a),
c as (
  select stock_number, daterange(lag(upper(r), 1) over (partition by stock_number order by stock_number, r), low) as gap
  from b)
select * from c;


-- step 4
with 
a as (
  select stock_number, r from ranges order by stock_number, r),
b as (
  select stock_number, r,
    case 
      when max(upper(r)) over (partition by stock_number order by stock_number, r
        rows between unbounded preceding and 1 preceding) >= lower(r) then null
      else lower(r) end as low
  from a),
c as (
  select stock_number, daterange(lag(upper(r), 1) over (partition by stock_number order by stock_number, r), low) as gap
  from b)
select stock_number, gap
from c
where not lower_inf(gap) 
  and not upper_inf(gap) 

-------------------------------------------------------------------------------------------------------------------
--</ 10/15 ---------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------  
damn, it has been 2 weeks
best i remember, finally got gaps to work
but not quite sure what to d with them
what am i trying to accomplish
an uninterupted timeline of statuses for each stocknumber
accomodate gaps caused by 
  sale - back on - sale
  too coarse a selection of statuses

running ranges to include all stocknumbers starting with 3, gave me 2713 rows, of which 68 had a gap
set ranges to include all stocknumbers gives me 46758 rows, and the gap process fails at c with:
  ERROR:  range lower bound must be less than or equal to range upper bound
trying to come up with a query to isloate the problem, but am mind fucking hard on doing sequential
elimination and getting nonsensical results
if i limit ranges to t any one of these:
  select left(stock_number,1), count(*) from ranges group by left(stock_number,1)
c fucking passes, can not isolate the failure to a group of stocknumbers
this makes NO sense

AFTER, LITERALLY DAYS OF MIND FUCKING, FIGURED IT OUT, IN THE "b" CTE, IT WAS NECESSARY TO ADD SORTING
AFTER PARTITION BEFORE THE FRAME CLAUSE (ROWS ...)

-- so, 1125 gap
-- 11/10 adding whlsl buffer knocks it down to 558 with a gap
with 
a as (
  select stock_number, r from ranges order by stock_number, r),
b as (
  select stock_number, r,
    case 
      when max(upper(r)) over (partition by stock_number order by stock_number, r
        rows between unbounded preceding and 1 preceding) >= lower(r) then null
      else lower(r) end as low
  from a),
c as (
  select stock_number, daterange(lag(upper(r), 1) over (partition by stock_number order by stock_number, r), low) as gap
  from b)
select stock_number, gap
from c
where not lower_inf(gap) 
  and not upper_inf(gap) 


select * from ranges limit 100

select count(distinct stock_number) from ranges

gap: lower is > upper of the preceding row 
with 
a as (
  select status, stock_number, r from ranges order by stock_number, r),
b as (
  select status, stock_number, r,
    case 
      when max(upper(r)) over (partition by stock_number order by stock_number, r
        rows between unbounded preceding and 1 preceding) >= lower(r) then null
      else lower(r) end as low
  from a)
select * from b  


12227XXA: gap from 1/10 to 1/12: wholesale buffer, at auction

remeber: day grain, accounts for multiple statuses  for daterange in ranges

besides identifying gaps, do i need to identify overlaps?

select stock_number, count(*)
from ranges
group by stock_number
order by count(*) desc

select *
from ranges
where stock_number = '30075x'
gap: 1/1 - 1/2

select a.stock_number, a.date_Acquired, a.sale_date, b.status, b.fromts, b.thruts
from uc.base_vehicles a 
inner join ads.ext_vehicle_inventory_item_statuses b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
where a.stock_number = '30075x'
order by b.fromts, b.status

select b.x::date , a.stock_number, a.date_acquired, a.sale_date, b.x::Date, c.fromts, c.status, c.fromts::date, c.thruts::date
from uc.base_Vehicles a
left join lateral (select * from generate_series (a.date_acquired, a.sale_date, '1 day') as x) b on 1 = 1
left join ads.ext_vehicle_inventory_item_statuses c on a.vehicle_inventory_item_id = c.vehicleinventoryitemid
  and b.x::date between c.fromts::Date and c.thruts::date
  and c.status in ('RawMaterials_RawMaterials','RMFlagPulled_Pulled','RMFlagAV_Available','RawMaterials_Sold','RawMaterials_Delivered')  
where a.stock_number = '30075x'
order by b.x, c.fromts



-------------------------------------------------------------------------------------------------------------------
--< 10/30 ---------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------  
need to get unstuck
for every day that a vehicle is owned, there shall be a status
the range of days owned shall be devined by uc.base_vehicles.date_acquired and .sale_date
trade buffer/in transit => raw materials

a vehicle (Stocknumber) may be sold more than once, eg, back on

-- good test vehicles
-- 15234xxb sold - delivered - pulled - available on the same day
drop table if exists ranges;
create temp table ranges as
select stock_number, from_date, thru_date, min(fromts) as fromts, 
  array_agg(status order by fromts) as status_array,
  string_agg(distinct status, ',' order by status) as status_string,
  daterange(from_Date, thru_Date, '[]') r
from (
  select b.stock_number, a.category, a.status, a.fromts::date as from_date, thruts::date as thru_date, fromts 
  from ads.ext_vehicle_inventory_item_statuses a
  inner join uc.base_vehicles b on a.vehicleinventoryitemid = b.vehicle_inventory_item_id 
   where status in ('RawMaterials_RawMaterials','RMFlagPulled_Pulled','RMFlagAV_Available',
        'RawMaterials_Sold','RawMaterials_Delivered','RMFlagWSB_WholesaleBuffer','RMFlagRMP_RawMaterialsPool')  
    and a.fromts <= a.thruts -- eliminate a couple of goofy RawMaterialsPool rows
    and fromts <> ( -- eliminate the first row of RawMaterials_RawMaterials
      select min(fromts)
      from ads.ext_vehicle_inventory_item_statuses e
      where e.vehicleinventoryitemid = a.vehicleinventoryitemid
        and e.status = 'RawMaterials_RawMaterials')) x
group by stock_number, from_date, thru_date;

-- 11/10 adding whlsl buffer knocks it down to 558 with a gap
-- adding RawMaterialsPool knocks it dow to 400
drop table if exists gaps cascade;
create temp table gaps as
with 
a as (
  select stock_number, r from ranges order by stock_number, r),
b as (
  select stock_number, r,
    case 
      when max(upper(r)) over (partition by stock_number order by stock_number, r
        rows between unbounded preceding and 1 preceding) >= lower(r) then null
      else lower(r) end as low
  from a),
c as (
  select stock_number, daterange(lag(upper(r), 1) over (partition by stock_number order by stock_number, r), low) as gap
  from b)
select stock_number, gap
from c
where not lower_inf(gap) 
  and not upper_inf(gap); 



select * 
from ranges
where stock_number = '15234XXB'
limit 1000


so, need to deal with mult statuses on same day and gaps

this is 29536xx an unwind from way above
but it is goofy, tool shaws acq in 2016, but from_date in base_vehicles is 5/24/17
select * from ranges a inner join uc.base_vehicles b on a.stock_number = b.stock_number and b.vehicle_inventory_item_id = '77d3a5f8-658e-4ecd-b80f-72858de78e6b' order by r


select status_array, min(stock_number), max(stock_number), count(*)
from ranges a
where cardinality(status_array) > 1
group by status_array

select *
from ranges
where stock_number = 'H9272A'

select distinct status from ads.ext_vehicle_inventory_item_statuses

select * from ranges where stock_number = '30506b'
select * from ranges where stock_number = '29119pa' order by r

ok, what i think i want is, the last status prior to the gap becomes the status for the duration of the gap
select *
from gaps a
left join ranges b on a.stock_number = b.stock_number
-- where gap << r
where gap -|- r and gap > r -- gap is adjacent to and > r
-- where gap -|- r
order by a.stock_number, b.r

-- status array because it is ordered
select *, 'gap'
from gaps
union
select a.stock_number, a.r, status_array[array_upper(status_array, 1)] -- last element of the array
from ranges a
inner join gaps b on a.stock_number = b.stock_number
order by stock_number

select c.*, lag(date_range, 1) over(partition by stock_number order by date_range)
from (
  select stock_number, gap as date_range, 'gap' as status
  from gaps
  union
  select a.stock_number, a.r, status_array[array_upper(status_array, 1)] -- last element of the array
  from ranges a
  inner join gaps b on a.stock_number = b.stock_number
  where a.r < gap
  order by stock_number) c
    
select *
from (
  select c.*, lag(date_range, 1) over(partition by stock_number order by date_range)
  from (
    select stock_number, gap as date_range, 'gap' as status
    from gaps
    union
    select a.stock_number, a.r, status_array[array_upper(status_array, 1)] -- last element of the array
    from ranges a
    inner join gaps b on a.stock_number = b.stock_number
    where a.r < gap
    order by stock_number) c) d
where status = 'gap'



-- returns the last element from the array status_array
select *, status_array[array_upper(status_array, 1)]
from ranges
where stock_number = 'H9272A'

select *
from ads.ext_vehicle_inventory_item_Statuses
where status = 'RMFlagRMP_RawMaterialsPool'
  and fromts > thruts


select *
from ads.ext_vehicle_inventory_item_Statuses
where vehicleinventoryitemid in ('5c1698db-9c18-41b2-b9c5-9fd0760370e4','a645ccf3-610d-4dc1-917f-48ea48d70a4a')
order by vehicleinventoryitemid, fromts

select * from uc.base_vehicles where vehicle_inventory_item_id in ('5c1698db-9c18-41b2-b9c5-9fd0760370e4','a645ccf3-610d-4dc1-917f-48ea48d70a4a')


-- 10/2 some pre-angst stuff  
-- -- -- -- first, do all vehicles start with a status of RawMaterials_RawMaterials
-- -- -- -- holy shit can this be true 575 vehicles with no RawMaterials_RawMaterials status
-- -- -- select distinct a.vehicleinventoryitemid
-- -- -- from ads.ext_vehicle_inventory_item_statuses a
-- -- -- where not exists (
-- -- --   select 1
-- -- --   from ads.ext_vehicle_inventory_item_statuses b
-- -- --   where b.vehicleinventoryitemid = a.vehicleinventoryitemid
-- -- --     and status = 'RawMaterials_RawMaterials')
-- -- -- 
-- -- -- 
-- -- -- select d.stocknumber, c.*
-- -- -- from ads.ext_vehicle_inventory_item_statuses c
-- -- -- inner join ads.ext_vehicle_inventory_items d on c.vehicleinventoryitemid = d.vehicleinventoryitemid
-- -- -- where c.vehicleinventoryitemid in (
-- -- --   select distinct a.vehicleinventoryitemid
-- -- --   from ads.ext_vehicle_inventory_item_statuses a
-- -- --   where not exists (
-- -- --     select 1
-- -- --     from ads.ext_vehicle_inventory_item_statuses b
-- -- --     where b.vehicleinventoryitemid = a.vehicleinventoryitemid
-- -- --       and status = 'RawMaterials_RawMaterials'))
-- -- -- order by c.vehicleinventoryitemid, c.fromts
-- -- -- 
-- -- -- -- ok, before i go batshit, how many of these are actually vehicles of concern, ie, in base_Vehicles
-- -- -- -- aha, only 3 of them
-- -- -- select a.*
-- -- -- from uc.base_Vehicles a
-- -- -- where vehicle_inventory_item_id in (
-- -- --   select distinct a.vehicleinventoryitemid
-- -- --   from ads.ext_vehicle_inventory_item_statuses a
-- -- --   where not exists (
-- -- --     select 1
-- -- --     from ads.ext_vehicle_inventory_item_statuses b
-- -- --     where b.vehicleinventoryitemid = a.vehicleinventoryitemid
-- -- --       and status = 'RawMaterials_RawMaterials'))



-- before i actually get anywhere, i am pre-angsting about multiple statuses on the same day 
  

-------------------------------------------------------------------------------------------------------------------------------------
--/> 10/02 --------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------

