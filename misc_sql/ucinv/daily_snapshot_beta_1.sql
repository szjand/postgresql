﻿/*
convert the temp tables in ..../ucinv/uc_daily_snapshot.sql into physical tables
and put them all in a function, greg.update_daily_snapshot_beta_1()
to be called from luigi as part of the used_cars.py module
*/

drop table if exists greg.tmp_vehicles cascade;
create table greg.tmp_vehicles (
  store_code citext not null,
  stock_number citext primary key,
  vin citext not null, 
  date_acquired date not null,
  sale_date date not null,
  make citext not null,
  model citext not null,
  model_year integer not null,
  body_style citext,
  trim_level citext,
  interior_color citext,
  exterior_color citext,
  engine citext,
  transmission citext,
  cab citext,
  drive citext,
  shape citext,
  size citext,
  shape_size citext,
  sale_type citext not null,
  miles integer not null,
  sold_from_status citext,
  vehicle_inventory_item_id citext not null);

truncate greg.tmp_vehicles;
insert into greg.tmp_vehicles
select a.store_code, a.stock_number, a.vin, a.date_acquired, a.sale_date,
  b.make, b.model, b.yearmodel::integer, b.bodystyle, b.trim, b.interiorcolor, 
  b.exteriorcolor, b.engine, b.transmission, 
  CASE
    WHEN BodyStyle LIKE  '%King%'
      OR BodyStyle LIKE '%Access%'
      OR BodyStyle LIKE '%Ext Cab%'
      OR BodyStyle LIKE '%Supercab%'
      or BodyStyle like '%super cab%'
      OR BodyStyle LIKE '%Club%'
      OR BodyStyle LIKE '%xtra%'
      OR BodyStyle LIKE '%ext%'
      OR BodyStyle LIKE '%Double%' THEN 'Extended Cab'
    WHEN BodyStyle LIKE '%Reg%' THEN 'Regular Cab'
    WHEN BodyStyle LIKE '%crew%'
      OR BodyStyle LIKE '%quad%'
      OR BodyStyle LIKE 'mega%' THEN 'Crew Cab'
  END::citext AS Cab,
  CASE
    WHEN BodyStyle LIKE '%AWD%' THEN 'AWD'
    WHEN BodyStyle LIKE '%FWD%' THEN 'FWD'
    WHEN BodyStyle LIKE '%4WD%' OR BodyStyle LIKE '%4X4%' THEN '4WD'
  END::citext AS drive,  
  c.shape, c.size, c.shape_size, 
  coalesce(right(d.typ, length(d.typ) - position('_' in d.typ)), 'N/A') as sale_type,
  f.miles,
  e.status as sold_from_status,
  a.vehicle_inventory_item_id
from greg.uc_base_vehicles a
left join ads.ext_vehicle_items b on a.vehicle_item_id = b.vehicleitemid
left join greg.uc_makes_models c on b.make = c.make
  and b.model = c.model
left join ads.ext_vehicle_sales d on a.vehicle_inventory_item_id = d.vehicleinventoryitemid
  and d.typ = 'VehicleSale_Sold'
left join greg.uc_base_Vehicle_statuses e on a.vehicle_inventory_item_id = e.vehicle_inventory_item_id
  and a.sale_date = e.the_date
left join greg.uc_base_vehicle_miles f on a.vehicle_inventory_item_id = f.vehicle_inventory_item_id;  

drop table if exists greg.tmp_vehicle_days cascade;
create table greg.tmp_vehicle_days (
  sale_date date,
  stock_number citext not null,
  vehicle_inventory_item_id citext not null,
  the_date date not null,
  date_priced date,
  invoice integer,
  best_price integer,
  days_since_priced integer,
  price_band citext,
  days_owned integer,
  status citext,
  days_in_status integer,
  recon_sales numeric(12,2),
  recon_cogs numeric(12,2),
  recon_gross numeric(12,2),
  post_sale_recon_sales numeric(12,2),
  post_sale_recon_cogs numeric(12,2),
  post_sale_recon_gross numeric(12,2),
  inventory_cost numeric(12,2),
  post_sale_cost numeric(12,2),
  primary key (stock_number,the_date));

truncate greg.tmp_vehicle_days;
insert into greg.tmp_vehicle_days
select aa.sale_date, aa.stock_number, a.vehicle_inventory_item_id, a.the_date, 
  coalesce(b.date_priced, '12/31/9999') as date_priced, coalesce(b.invoice, -1)::integer as invoice, 
  coalesce(b.best_price, -1)::integer as best_price, 
  case when date_priced is not null then a.the_date - b.date_priced else 0 end as days_since_priced, 
  coalesce(c.price_band, 'Not Priced') as price_band,
  (a.the_date - aa.date_acquired) + 1 as days_owned,
  d.status, 
  d.days_in_status, 
  e.recon_sales, e.recon_cogs, e.recon_gross,
  e.post_sale_recon_sales, e.post_sale_recon_cogs, e.post_sale_recon_gross,
  f.inventory_cost, f.post_sale_cost
from greg.uc_base_vehicle_days a
inner join greg.uc_base_vehicles aa on a.vehicle_inventory_item_id = aa.vehicle_inventory_item_id
  and aa.date_acquired > '12/31/2012'
left join greg.uc_base_vehicle_prices b on a.vehicle_inventory_item_id = b.vehicle_inventory_item_id
    and a.the_date between b.date_priced and b.priced_thru
left join greg.uc_price_bands c on b.best_price between c.price_from and c.price_thru 
left join greg.uc_base_vehicle_statuses d on a.vehicle_inventory_item_id = d.vehicle_inventory_item_id
  and a.the_date = d.the_date
left join greg.uc_recon_by_day e on aa.stock_number = e.stock_number
  and a.the_date = e.the_date
left join (
  select a.stock_number,
    case 
      when a.the_date < b.date_acquired then b.date_acquired
      when a.the_date <= b.sale_date then a.the_date
      else b.sale_date
    end as the_date,
    sum(case when a.the_date  <= b.sale_date then amount else 0 end) as inventory_cost,
    sum(case when  a.the_date  > b.sale_date then amount else 0 end) as post_sale_cost
  from greg.uc_cost_details a
  left join greg.uc_base_vehicles b on a.stock_number = b.stock_number
  group by a.stock_number, 
    case 
      when a.the_date < b.date_acquired then b.date_acquired
      when a.the_date <= b.sale_date then a.the_date
      else b.sale_date
    end) f on aa.stock_number = f.stock_number
        and a.the_Date = f.the_date;  
  
drop table if exists greg.tmp_running_days cascade;
create table greg.tmp_running_days (
  the_date date,
  store_code citext,
  stock_number citext not null,
  vin citext,
  date_acquired date,
  sale_date date,
  make citext,
  model citext,
  model_year integer,
  body_style citext,
  trim_level citext,
  color citext,
  cab citext,
  drive citext,
  shape citext,
  size citext,
  shape_size citext,
  miles integer,
  date_priced date,
  best_price integer,
  days_since_priced integer,
  price_band citext,
  days_owned integer,
  status citext,
  days_in_status integer,
  recon_sales numeric(12,2),
  recon_cogs numeric(12,2),
  recon_gross numeric(12,2),
  recon_sales_running numeric(12,2),
  recon_cogs_running numeric(12,2),
  recon_gross_running numeric(12,2),
  post_sale_recon_sales numeric(12,2),
  post_sale_recon_cogs numeric(12,2),
  post_sale_reccon_gross numeric(12,2),
  inventory_cost numeric(12,2),
  inventory_cost_running numeric(12,2),
  post_sale_cost numeric(12,2),
  primary key(stock_number,the_date));

truncate greg.tmp_running_days;
insert into greg.tmp_running_days
select b.the_date, a.store_code, a.stock_number, a.vin, a.date_acquired, a.sale_date, a.make, 
  a.model, a.model_year as year, a.body_style,
  a.trim_level, a.exterior_color as color, a.cab, a.drive, a.shape, a.size, a.shape_size, a.miles,
  b.date_priced, b.best_price, b.days_since_priced, b.price_band, b.days_owned, b.status, 
  b.days_in_status,
  coalesce(recon_sales,0) as recon_sales, 
  coalesce(recon_cogs,0) as recon_cogs, 
  coalesce(recon_gross,0) as recon_gross,
  sum(coalesce(1 * recon_sales, 0)) over (partition by a.stock_number order by the_date) as recon_sales_running,
  sum(coalesce(1 * recon_cogs, 0)) over (partition by a.stock_number order by the_date) as recon_cogs_running,
  sum(coalesce(1 * recon_gross, 0)) over (partition by a.stock_number order by the_date) as recon_gross_running,
  coalesce(post_sale_recon_sales,0) as post_sale_recon_sales, 
  coalesce(post_sale_recon_cogs,0) as post_sale_recon_cogs, 
  coalesce(post_sale_recon_gross,0) as post_sale_recon_gross,
  coalesce(inventory_cost, 0) as inventory_cost,
  sum(coalesce(inventory_cost, 0)) over (partition by a.stock_number order by the_date) as inventory_cost_running,
  coalesce(post_sale_cost,0) as post_sale_cost
-- select *
from greg.tmp_vehicles a
inner join greg.tmp_vehicle_days b on a.stock_number = b.stock_number;


drop table if exists greg.uc_daily_snapshot_beta_1 cascade;
create table greg.uc_daily_snapshot_beta_1 (
  the_date date,
  store_code citext,
  stock_number citext,
  vin citext,
  date_acquired date,
  sale_date date,
  make citext,
  model citext,
  year citext,
  bodystyle citext,
  trim citext,
  color citext,
  cab citext,
  drive citext,
  shape citext,
  size citext,
  shape_size citext,
  miles integer,
  date_priced date,
  best_price integer,
  days_since_priced integer,
  price_band citext,
  days_owned integer,
  status citext,
  days_in_status integer,
  recon_sales integer,
  recon_cogs integer,
  recon_gross integer,
  recon_sales_running integer,
  recon_cogs_running integer,
  recon_gross_running integer,
  post_sale_recon_sales integer,
  post_sale_recon_cogs integer,
  post_sale_recon_gross integer,
  inventory_cost integer,
  inventory_cost_running integer,
  post_sale_cost integer,
  sale_type citext,
  pot_lot_gross integer,
  front_gross_at_sale integer,
  front_gross_post_sale integer,
  back_gross_at_sale integer,
  back_gross_post_sale integer,
  primary key(the_date,stock_number));

create index on greg.uc_daily_snapshot_beta_1(vin);
create index on greg.uc_daily_snapshot_beta_1(status);
create index on greg.uc_daily_snapshot_beta_1(shape);
create index on greg.uc_daily_snapshot_beta_1(size);
create index on greg.uc_daily_snapshot_beta_1(make);
create index on greg.uc_daily_snapshot_beta_1(model);
create index on greg.uc_daily_snapshot_beta_1(sale_type);

truncate greg.uc_daily_snapshot_beta_1;
insert into greg.uc_daily_snapshot_beta_1
select a.*, c.sale_type,
  case
    when best_price = -1 then best_price
    when inventory_cost_running = 0 then 0
    else best_price - inventory_cost_running 
  end as pot_lot_gross,
  aa.front_gross_at_sale, aa.front_gross_post_sale, aa.back_gross_at_sale, aa.back_gross_post_sale
from greg.tmp_running_days a
left join ( -- aa
  select a.stock_number, a.sale_date,
    sum(case when b.department_code = 'uc' and b.the_date <= a.sale_date then -1 * amount else 0 end) as front_gross_at_sale,
    sum(case when b.department_code = 'uc' and b.the_date > a.sale_date then amount else 0 end) as front_gross_post_sale,
    sum(case when b.department_code = 'fi' and b.the_date <= a.sale_date then -1 * amount else 0 end) as back_gross_at_sale,
    sum(case when b.department_code = 'fi' and b.the_date > a.sale_date then amount else 0 end) as back_gross_post_sale
  from greg.uc_base_vehicles a
  left join greg.uc_base_vehicle_accounting b on a.stock_number = b.control
  where sale_Date <= current_date
    and left(stock_number, 1) <> 'C'
  group by a.stock_number, a.sale_date) aa on a.stock_number = aa.stock_number and a.the_date = aa.sale_date
left join (
select a.stock_number, substring(b.typ, position('_' in typ) + 1, length(typ) - position('_' in typ)) as sale_type
from greg.uc_base_Vehicles a
left join ads.ext_vehicle_sales b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
  AND b.status = 'VehicleSale_Sold') c on a.stock_number = c.stock_number;

  