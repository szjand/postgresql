﻿drop table if exists ads.vehicleitemmileages;

create TABLE ads.ext_vehicle_item_mileages(
    TableKey citext,
    BasisTable citext,
    VehicleItemID citext,
    Value integer,
    VehicleItemMileageTS timestamp with time zone,
    primary key (vehicleitemid,vehicleitemmileagets));

create table greg.uc_price_bands (
  price_band citext not null primary key,
  price_from integer not null,
  price_thru integer not null);

insert into greg.uc_price_bands values ('00-6k',0,5999);
insert into greg.uc_price_bands values ('06-8k',6000,7999);
insert into greg.uc_price_bands values ('08-10k',8000,9999);
insert into greg.uc_price_bands values ('10-12k',10000,11999);
insert into greg.uc_price_bands values ('12-14k',12000,13999);
insert into greg.uc_price_bands values ('14-16k',14000,15999);
insert into greg.uc_price_bands values ('16-18k',16000,17999);
insert into greg.uc_price_bands values ('18-20k',18000,19999);
insert into greg.uc_price_bands values ('20-22k',20000,21999);
insert into greg.uc_price_bands values ('22-24k',22000,23999);
insert into greg.uc_price_bands values ('24-26k',24000,25999);
insert into greg.uc_price_bands values ('26-28k',26000,27999);
insert into greg.uc_price_bands values ('28-30k',28000,29999);
insert into greg.uc_price_bands values ('30-32k',30000,31999);
insert into greg.uc_price_bands values ('32-34k',32000,33999);
insert into greg.uc_price_bands values ('34-36k',34000,35999);
insert into greg.uc_price_bands values ('36-38k',36000,37999);
insert into greg.uc_price_bands values ('38-40k',38000,39999);
insert into greg.uc_price_bands values ('40k+',40000,999999);
insert into greg.uc_price_bands values ('Not Priced',-1,-1);



select *
from ads.ext_fact_vehicle_sale
limit 100



create TABLE ads.ext_vehicle_sales(
    VehicleSaleID citext primary key,
    VehicleInventoryItemID citext,
    SoldTS timestamp with time zone,
    SoldAmount numeric (12,2),
    Typ citext,
    Status citext,
    SoldBy citext,
    ManagerPartyID citext,
    SoldTo citext,
    FundingTyp citext,
    EstimatedDeliveryDate timestamp with time zone);
create unique index on ads.ext_vehicle_Sales(vehicleinventoryitemid,soldts);  






-- limit to 2012
-- 21381 rows
drop table if exists uc_vehicles;
create temp table uc_vehicles as
select a.stocknumber, a.fromts::date as date_acquired, 
  CASE a.owninglocationid
    WHEN 'B6183892-C79D-4489-A58C-B526DF948B06' THEN 'RY1'
    WHEN '4CD8E72C-DC39-4544-B303-954C99176671' THEN 'RY2'
    WHEN '1B6768C6-03B0-4195-BA3B-767C0CAC40A6' THEN 'RY3'
    ELSE 'XXX'
  END AS store_code,
  b.vin, b.make, b.model, b.yearmodel, b.vinresolved, b.bodystyle, b.trim, b.interiorcolor, b.exteriorcolor,
  b.engine, b.transmission, 
  coalesce((Select max(value) from ads.ext_vehicle_item_mileages where vehicleitemid = b.vehicleitemid and vehicleitemmileagets <= a.thruts), -1) as miles,
  c.shape, c.size, c.shape_size,
  CASE
    WHEN BodyStyle LIKE  '%King%'
      OR BodyStyle LIKE '%Access%'
      OR BodyStyle LIKE '%Ext Cab%'
      OR BodyStyle LIKE '%Supercab%'
      or BodyStyle like '%super cab%'
      OR BodyStyle LIKE '%Club%'
      OR BodyStyle LIKE '%xtra%'
      OR BodyStyle LIKE '%ext%'
      OR BodyStyle LIKE '%Double%' THEN 'Extended Cab'
    WHEN BodyStyle LIKE '%Reg%' THEN 'Regular Cab'
    WHEN BodyStyle LIKE '%crew%'
      OR BodyStyle LIKE '%quad%'
      OR BodyStyle LIKE 'mega%' THEN 'Crew Cab'
  END::citext AS Cab,
  CASE
    WHEN BodyStyle LIKE '%AWD%' THEN 'AWD'
    WHEN BodyStyle LIKE '%FWD%' THEN 'FWD'
    WHEN BodyStyle LIKE '%4WD%' OR BodyStyle LIKE '%4X4%' THEN '4WD'
  END::citext AS drive,
  case
    when extract(year from d.soldts::date) = 1910 then a.thruts::Date
    else coalesce(d.soldts::date, '12/31/9999'::date) 
  end AS sale_date,
  coalesce(substring(d.typ, position('_' IN d.typ) + 1, 9), 'Inventory') AS sale_type,
  a.vehicleinventoryitemid 
-- select *   
from ads.ext_vehicle_inventory_items a
left join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
left join greg.uc_make_model c on b.make = c.make
  and b.model = c.model
left join ads.ext_vehicle_sales d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
  AND d.status = 'VehicleSale_Sold'
where a.thruts::date > '12/31/2011';
create index on ads.ext_vehicle_inventory_items(vehicleitemid);
create index on ads.ext_vehicle_inventory_items(thruts);
create index on ads.ext_vehicle_sales(status);



select * from uc_base_vehicles limit 100




-- 7/29/17 ---------------------------------------------------------------------------
remembering the iffy results for 2011 with financial statements
lets just limit this to 2012 and beyond from the start

select count(*) -- 35993
from ads.ext_vehicle_inventory_items
select count(*) -- 21381
from ads.ext_vehicle_inventory_items
where thruts::date > '12/31/2011'


select count(*) --449943
from ads.ext_vehicle_inventory_item_Statuses a
inner join ads.ext_Vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and b.thruts::date > '12/31/2011'

  
select b.the_date, a.*
from uc_vehicles a
inner join dds.dim_date b on b.the_date between a.date_acquired and a.sale_date
where a.sale_date - a.date_acquired = 3


select b.stocknumber, b.vehicleinventoryitemid, vehiclepricingts::date as price_date
from ads.ext_vehicle_pricings a
inner join uc_vehicles b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and b.sale_date > current_date
inner join ( -- veh with > 2 pricings
  select vehicleinventoryitemid
  from ads.ext_vehicle_pricings
  group by vehicleinventoryitemid
  having count(*) > 2) c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
-- inner join (
--   select vehicleinventoryitemid  
order by stocknumber, vehiclepricingts::date 

-- want the last price per day, need the pricingid of that pricing

-- vehicles with multiple pricings in a day
select a.vehiclepricingid, a.vehicleinventoryitemid, a.vehiclepricingts
from ads.ext_vehicle_pricings a
where a.vehicleinventoryitemid in ('091168ba-074d-4f5c-960e-8e0c5cbb7690',
  'cb2afe6b-65d5-46d9-9d55-5f7f2d1d773b','757a79b0-88a0-42a9-97c4-d4417658bed8',
  '95ac85e2-b048-4684-ae53-8fb7b43092af')
order by a.vehicleinventoryitemid, a.vehiclepricingts


-- vehicles with multiple pricings in a day
-- only the last pricing per day
-- this ain't it
select a.vehiclepricingid, a.vehicleinventoryitemid, a.vehiclepricingts
from ads.ext_vehicle_pricings a
inner join (
  select vehicleinventoryitemid, max(vehiclepricingts) as vehiclepricingts
  from ads.ext_vehicle_pricings
  group by vehicleinventoryitemid) b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
    and a.vehiclepricingts = b.vehiclepricingts
where a.vehicleinventoryitemid in ('091168ba-074d-4f5c-960e-8e0c5cbb7690',
  'cb2afe6b-65d5-46d9-9d55-5f7f2d1d773b','757a79b0-88a0-42a9-97c4-d4417658bed8',
  '95ac85e2-b048-4684-ae53-8fb7b43092af')
order by a.vehicleinventoryitemid, a.vehiclepricingts



select aa.*
from ads.ext_vehicle_pricings aa
inner join (
  select a.vehicleinventoryitemid, a.vehiclepricingts::date as date_priced -- one price per day 3844
  from ads.ext_vehicle_pricings a
  inner join uc_base_vehicles b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
    and sale_date > '12/31/2011'
  group by a.vehicleinventoryitemid, a.vehiclepricingts::date
  having count(*) = 1) bb on aa.vehicleinventoryitemid = bb.vehicleinventoryitemid
    and aa.vehiclepricingts::date = bb.date_priced

 
-- process the subset of veh with mult pricings/per day separately
select a.vehicleinventoryitemid, a.vehiclepricingts::date -- 914
from ads.ext_vehicle_pricings a
inner join uc_base_vehicles b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and b.sale_date > '12/31/2011'
group by a.vehicleinventoryitemid, a.vehiclepricingts::date
having count(*) > 1  

select aa.* -- last price of day for veh with mult pricings in a day
from ads.ext_vehicle_pricings aa
inner join ( -- vehicles with multiple pricings in a day
  select a.vehicleinventoryitemid, a.vehiclepricingts::date as price_date, max(vehiclepricingts) as max_ts
  from ads.ext_vehicle_pricings a
  inner join uc_base_vehicles b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
    and b.sale_date > '12/31/2011'
  group by a.vehicleinventoryitemid, a.vehiclepricingts::date
  having count(*) > 1) bb on aa.vehicleinventoryitemid = bb.vehicleinventoryitemid
    and aa.vehiclepricingts::date = bb.price_date
    and aa.vehiclepricingts = bb.max_ts


drop table if exists uc_pricings;
create temp table uc_pricings as
select cc.vehicleinventoryitemid, cc.vehicleitemid, cc.pricedby, cc.vehiclepricingts::Date as date_priced,
  cc.vehiclepricingstrategy, dd.amount::integer as invoice, ee.amount::integer as best_price
from (
  select aa.* -- veh with single price per day
  from ads.ext_vehicle_pricings aa
  inner join (
    select a.vehicleinventoryitemid, a.vehiclepricingts::date as date_priced -- one price per day 3844
    from ads.ext_vehicle_pricings a
    inner join uc_base_vehicles b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
      and b.sale_date > '12/31/2011'
    group by a.vehicleinventoryitemid, a.vehiclepricingts::date
    having count(*) = 1) bb on aa.vehicleinventoryitemid = bb.vehicleinventoryitemid
      and aa.vehiclepricingts::date = bb.date_priced
  union
  select aa.* -- last price of day for veh with mult pricings in a day
  from ads.ext_vehicle_pricings aa
  inner join ( -- vehicles with multiple pricings in a day
    select a.vehicleinventoryitemid, a.vehiclepricingts::date as price_date, max(vehiclepricingts) as max_ts
    from ads.ext_vehicle_pricings a
    inner join uc_base_vehicles b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
      and b.sale_date > '12/31/2011'
    group by a.vehicleinventoryitemid, a.vehiclepricingts::date
    having count(*) > 1) bb on aa.vehicleinventoryitemid = bb.vehicleinventoryitemid
      and aa.vehiclepricingts::date = bb.price_date
      and aa.vehiclepricingts = bb.max_ts) cc
left join ads.ext_vehicle_pricing_details dd on cc.vehiclepricingid = dd.vehiclepricingid    
  and dd.typ = 'VehiclePricingDetail_Invoice'  
left join ads.ext_vehicle_pricing_details ee on cc.vehiclepricingid = ee.vehiclepricingid    
  and ee.typ = 'VehiclePricingDetail_BestPrice';   
create unique index on uc_pricings(vehicleinventoryitemid,date_priced);

select * from uc_pricings limit 100

-- subset for windowing play
drop table if exists price_windows;
create temp table price_windows as
select b.stocknumber, a.date_priced, a.invoice, a.best_price
from uc_pricings a
inner join uc_base_vehicles b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
inner join ( -- veh with > 2 pricings
  select vehicleinventoryitemid
  from ads.ext_vehicle_pricings
  group by vehicleinventoryitemid
  having count(*) > 2) c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
-- where extract(year from b.sale_Date) = 2016  
order by stocknumber, date_priced
-- limit 1000


select x.stocknumber, x.date_priced,  
  coalesce(lead(x.date_priced, 1) 
    over (partition by x.stocknumber order by x.date_priced), y.sale_date) as priced_thru,
  coalesce(lead(x.date_priced, 1) 
    over (partition by x.stocknumber order by x.date_priced), y.sale_date) - x.date_priced as days_at_price,
  best_price, 
  coalesce(x.best_price  - lag(x.best_price, 1) over (partition by x.stocknumber order by x.date_priced), 0) as best_price_delta,
  invoice, 
  coalesce(x.invoice  - lag(x.invoice, 1) over (partition by x.stocknumber order by x.date_priced), 0) as invoice_delta,
  y.sale_Date
from price_windows x
left join uc_base_vehicles y on x.stocknumber = y.stocknumber
where extract(year from sale_date) = 2017
order by stocknumber, date_priced



select * from uc_base_vehicle_days where stocknumber = '22502xxz'