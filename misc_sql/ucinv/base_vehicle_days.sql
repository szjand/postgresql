﻿--8/9/17: orig included stocknumber, removed it
drop table if exists greg.uc_base_vehicle_days;   
create table greg.uc_base_vehicle_days (
  vehicle_inventory_item_id citext references greg.uc_base_vehicles(vehicle_inventory_item_id),
  the_date date references dds.dim_date(the_date),
  primary key (vehicle_inventory_item_id,the_date));

insert into greg.uc_base_vehicle_days  --1065756 rows 1:52 minutes
select a.vehicle_inventory_item_id, b.the_date
from greg.uc_base_vehicles a
inner join dds.dim_date b on b.the_date < '08/01/2017'
  and b.the_date between a.date_acquired and 
    case
      when a.sale_date > '07/31/2017' then '07/31/2017'
      else a.sale_date
    end;

changed rows: nope
deleted rows: it can happen, but i do not know how to handle it

-- new rows 1399 @ 3.8 sec
select a.vehicle_inventory_item_id, b.the_date 
from greg.uc_base_vehicles a
inner join dds.dim_date b on b.the_date < '08/04/2017'
  and b.the_date between a.date_acquired and 
    case
      when a.sale_date > '08/03/2017' then '08/03/2017'
      else a.sale_date
    end
left join greg.uc_base_vehicle_days c on a.vehicle_inventory_item_id = c.vehicle_inventory_item_id
  and b.the_date = c.the_date    
where c.vehicle_inventory_item_id is null  

-- changed rows
select a.vehicle_inventory_item_id, b.the_date 
from greg.uc_base_vehicles a
inner join dds.dim_date b on b.the_date < '08/04/2017'
  and b.the_date between a.date_acquired and 
    case
      when a.sale_date > '08/03/2017' then '08/03/2017'
      else a.sale_date
    end
inner join  greg.uc_base_vehicle_days c on a.vehicle_inventory_item_id = c.vehicle_inventory_item_id
  and b.the_date = c.the_date
  and a.stock_number <> c.stock_number 

SELECT string_agg('excluded.'||column_name, ',')
FROM information_schema.columns
WHERE table_schema = 'greg'
  AND table_name   = 'uc_base_vehicle_days'
group by table_name  




-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--</ 9/13 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- change PK to stock_number

drop table if exists uc.base_vehicle_days;   
create table uc.base_vehicle_days (
  stock_number citext references uc.base_vehicles(stock_number) on update cascade on delete cascade,
  the_date date );

insert into uc.base_vehicle_days  --1036391 rows 19.5 sec
select a.stock_number, b.the_date
from uc.base_vehicles a
inner join dds.dim_date b on b.the_date < '09/01/2017'
  and b.the_date between a.date_acquired and a.sale_date; 

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--/> 9/13 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



select count(*) from greg.uc_base_vehicle_days  -- 1065756/