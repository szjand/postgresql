﻿need sold amount
sold location -- salee account

select the_date, control, account, account_description, amount, unit_count
from greg.uc_base_vehicle_accounting
where post_status = 'Y'
  and journal_code = 'vsu'
  and account_type = 'sale'
  and department_code = 'uc'
order by control, the_date 
limit 1000

drop table if exists sale_account;
create temp table sale_account as
select store_code, control, account, account_description, department_code
from greg.uc_base_vehicle_accounting
where post_status = 'Y'
  and journal_code = 'vsu'
  and account_type = 'sale'
  and department_code in ('uc','ao')
group by store_code, control, account, account_description, department_code ; 

select aa.year_month, b.store_code, b.department_code, a.price_band, count(*)
from greg.uc_daily_snapshot_beta_1 a
join dds.dim_date aa on a.sale_date = aa.the_date
left join sale_account b on a.stock_number = b.control
where a.the_date = a.sale_date
  and a.sale_type = 'retail'
group by aa.year_month, b.store_code, b.department_code, a.price_band
order by aa.year_month desc 





select count(*)
from greg.uc_daily_snapshot_beta_1

select *
from greg.uc_daily_snapshot_beta_1
where the_date = sale_date
  and best_price = -1

drop table if exists bopmast;
create temp table bopmast as
Select trim(a.bopmast_stock_number) as stock_number, a.retail_price, 
  a.term, a.payment, a.down_payment, date_capped
from arkona.xfm_bopmast a
where a.current_row
  and vehicle_type = 'U'
  and record_status = 'U';


select a.stock_number, a.vin, a.date_acquired, a.sale_date, a.best_price, b.*
from greg.uc_daily_snapshot_beta_1 a
left join bopmast b on a.stock_number = b.stock_number
where a.the_date = sale_date
  and a.sale_type = 'retail'
  and a.best_price <> b.retail_price


-- 
