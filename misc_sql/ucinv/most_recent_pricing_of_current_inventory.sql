﻿/*
Could I get a query that has for every vehicle in the tool (raw & available):
Year, Make, Model, Miles
Age
Days since last reprice

mopst recent pricing of current inventory
*/
select a.stocknumber, c.yearmodel as year, c.make, c.model, d.miles, current_date - a.fromts::date as age,
  current_date - e.last_priced as days_since_priced
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_walks b on a.vehicleinventoryitemid = b.vehicle_inventory_item_id::citext
join ads.ext_vehicle_items c on a.vehicleitemid = c.vehicleitemid
left join (
	select vehicleitemid, miles, seq
	from (
		select aa.vehicleitemid, aa.value as miles, 
			row_number() over (partition by aa.vehicleitemid order by aa.vehicleitemmileagets desc) as seq
		from ads.ext_Vehicle_item_mileages aa
		join ads.ext_vehicle_inventory_items bb on aa.vehicleitemid = bb.vehicleitemid
			and bb.thruts::date > current_date) cc) d on c.vehicleitemid = d.vehicleitemid
			  and d.seq = 1
left join (
	select dd.vehicleinventoryitemid, dd.vehiclepricingts::date as last_priced,
		row_number() over  (partition by dd.vehicleinventoryitemid order by dd.vehiclepricingts desc) as seq
	from ads.ext_vehicle_pricings dd
	join ads.ext_Vehicle_inventory_items ee on dd.vehicleinventoryitemid = ee.vehicleinventoryitemid
		and ee.thruts::date > current_date
	join ads.ext_vehicle_pricing_details ff on dd.vehiclepricingid = ff.vehiclepricingid  
		and ff.typ = 'VehiclePricingDetail_BestPrice') e on a.vehicleinventoryitemid = e.vehicleinventoryitemid
	  and e.seq = 1
where a.thruts::date > current_date	  


select dd.vehicleinventoryitemid, dd.vehiclepricingts::date as last_priced,
  row_number() over  (partition by dd.vehicleinventoryitemid order by dd.vehiclepricingts desc) as seq
from ads.ext_vehicle_pricings dd
join ads.ext_Vehicle_inventory_items ee on dd.vehicleinventoryitemid = ee.vehicleinventoryitemid
  and ee.thruts::date > current_date
join ads.ext_vehicle_pricing_details ff on dd.vehiclepricingid = ff.vehiclepricingid  
  and ff.typ = 'VehiclePricingDetail_BestPrice'

select * from ads.ext_vehicle_pricing_details limit 100

