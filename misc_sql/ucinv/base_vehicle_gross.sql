﻿-- from python projects/sales_pay_plan/financial_statement_gross_per_unit_2.sql
-- instead of relying on fs for an inclusive list of accounts,
-- use arkona.ext_eisglobal_sypffxmst & arkona.ext_ffpxrefdta (from postgres snippets)

-- 8/22/17 add page 1 lines 25,26: inventory accounts
-- 8/29/17 extended to include 2011
drop table if exists greg.uc_accounts_lines cascade;
create table greg.uc_accounts_lines (
  the_year integer not null, 
  store_code citext not null,
  gm_account citext not null,
  gl_account citext not null,
  page integer not null,
  line integer not null,
  col integer not null,
  primary key (the_year,gl_account));
COMMENT ON COLUMN greg.uc_accounts_lines.the_year IS 'arkona.ext_ffpxrefdta.factory_financial_year';
COMMENT ON COLUMN greg.uc_accounts_lines.store_code IS 'arkona.ext_ffpxrefdta.consolidation_grp';
COMMENT ON COLUMN greg.uc_accounts_lines.gm_account IS 'arkona.ext_ffpxrefdta.factory_account';
COMMENT ON COLUMN greg.uc_accounts_lines.gl_account IS 'arkona.ext_ffpxrefdt.g_l_acct_number';
COMMENT ON COLUMN greg.uc_accounts_lines.page IS 'from arkona.ext_eisglobal_sypffxmst.fxmpge';
COMMENT ON COLUMN greg.uc_accounts_lines.line IS 'from arkona.ext_eisglobal_sypffxmst.fxmlne';
COMMENT ON COLUMN greg.uc_accounts_lines.col IS 'from arkona.ext_eisglobal_sypffxmst.fxmcol';

insert into greg.uc_accounts_lines
select b.factory_financial_year as the_year, b.store_code, b.factory_account as gm_account, 
  b.g_l_acct_number as gl_account, a.fxmpge as page, a.fxmlne as line, a.fxmcol as col
from arkona.ext_eisglobal_sypffxmst a
inner join (
  select factory_financial_year, 
  case
    when coalesce(consolidation_grp, '1')  = '1' then 'RY1'::citext
    when coalesce(consolidation_grp, '1')  = '2' then 'RY2'::citext
    else 'XXX'::citext
  end as store_code, g_l_acct_number, factory_account, fact_account_
  from arkona.ext_ffpxrefdta) b on a.fxmcyy = b.factory_financial_year and a.fxmact = b.factory_account
where a.fxmcyy > 2017
  and ((fxmpge = 16 and fxmlne between 1 and 14) 
    or (fxmpge = 17 and fxmlne between 11 and 20)
    or (fxmpge = 1 and fxmlne between 25 and 26));


drop table if exists greg.uc_base_vehicle_accounting;
create table greg.uc_base_vehicle_accounting (
  year_month integer not null,
  the_date date not null,
  control citext not null,
  doc citext,
  trans integer not null,
  seq integer not null,
  post_Status citext not null,
  account citext not null,
  account_description citext not null,
  account_type citext not null,
  gm_account citext not null,
  journal_code citext not null,
  trans_description citext not null,
  store_code citext not null,
  department_code citext not null,
  page integer not null,
  line integer not null,
  col integer not null,
  amount numeric(12,2) not null,
  unit_count integer not null,
  foreign key(trans,seq,post_status) references fin.fact_gl(trans,seq,post_status) on update cascade,
  primary key(trans,seq,post_status));
create index on greg.uc_base_vehicle_accounting(year_month);  
create index on greg.uc_base_vehicle_accounting(the_date);
create index on greg.uc_base_vehicle_accounting(control);
create index on greg.uc_base_vehicle_accounting(account);
create index on greg.uc_base_vehicle_accounting(account_type);
create index on greg.uc_base_vehicle_accounting(store_code);
create index on greg.uc_base_vehicle_accounting(department_code);
create index on greg.uc_base_vehicle_accounting(journal_code);
/*
8/14 uh oh
Luigi FactGL() failed because of the foreign key relationship to greg.uc_base_vehicle_accounting
evey night, void rows in fact_gl get updated (post_status)
initial load into veh_acct was where post_status = Y
options
1.leave the foreign key in, make it cascade on update
then delete from veh_acct where post_status <> Y
i like that, i think

*/


insert into greg.uc_base_vehicle_accounting
select b.year_month, b.the_date, a.control, a.doc, a.trans, a.seq, post_status, 
  c.account, c.description as account_description, c.account_type, cc.gm_account,
  d.journal_code, e.description as trans_description, 
  c.store_code, c.department_code, cc.page, cc.line, cc.col,
  a.amount,
  case 
    when c.account_type = 'sale' and d.journal_code in ('VSN','VSU') then
      case
        when a.amount < 0 then 1
        else
          case
            when a.amount > 0 then -1
            else 0
          end
      end
    else 0
  end as unit_count
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join greg.uc_accounts_lines cc on b.the_year = cc.the_year
  and c.account = cc.gl_account
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and c.store <> 'none'; 


  
-- nov 2013 looks good
select store_code, page, line, col, sum(amount)
from greg.uc_base_vehicle_accounting
where year_month = 201902
  and store_code = 'RY1'
group by store_code, page, line, col
order by store_code, page, line, col
-- gross 
select store_code, page, line, sum(-amount)
from greg.uc_base_vehicle_accounting
where year_month = 201902
  and store_code = 'RY1'
group by store_code, page, line
order by store_code, page, line

-- looks good enuf at a vehicle level
-- do i want to expose sales/cogs/gross?
select a.stock_number, a.sale_date,
  sum(case when b.department_code = 'uc' and b.the_date <= a.sale_date then -1 * amount else 0 end) as gross_front_at_sale,
  sum(case when b.department_code = 'uc' and b.the_date > a.sale_date then amount else 0 end) as gross_front_post_sale,
  sum(case when b.department_code = 'fi' and b.the_date <= a.sale_date then -1 * amount else 0 end) as gross_back_at_sale,
  sum(case when b.department_code = 'fi' and b.the_date > a.sale_date then amount else 0 end) as gross_back_post_sale
from greg.uc_base_vehicles a
left join greg.uc_base_vehicle_accounting b on a.stock_number = b.control
-- where sale_date between '07/01/2017' and '07/31/2017'
--   and b.store_code = 'ry1' 
where sale_Date <= current_date
  and left(stock_number, 1) <> 'C'
group by a.stock_number, a.sale_date
order by sale_date, stock_number


select the_date, sum(amount)
from greg.uc_base_vehicles a
left join greg.uc_base_vehicle_accounting b on a.stock_number = b.control
where a.stock_number = '30501b'
  and department_code = 'uc'
group by the_date

---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------
-- python projects/greg_availability/greg_availability.py
-- potential lot gross
-- intranet scripts/crayon report/postgres/ucinv_potential_gross.sql

-- updated account_lines to include inventory accounts
-- and this is accurate:
select a.control, sum(amount)
from greg.uc_base_vehicle_accounting a
inner join greg.uc_base_vehicles b on a.control = b.stock_number
  and b.sale_date > current_date
where a.store_code = 'ry1'
  and a.account_description not like '%PA%'
group by a.control  
order by sum(amount)

create index on greg.uc_base_vehicle_accounting(journal_code)

select a.the_date, a.journal_code, a.control, department, sum(amount) as amount
from greg.uc_base_vehicle_accounting a
inner join greg.uc_base_vehicles b on a.control = b.stock_number
  and b.sale_date > current_date
where a.store_code = 'ry1'
  and a.account_description not like '%PA%' -- exclude pack
group by a.the_date, a.journal_code, a.control, department
order by a.control, a.the_date

