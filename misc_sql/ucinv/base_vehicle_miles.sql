﻿
select vehicleitemid, the_date, miles
from (
select a.vehicleitemid, a.vehicleitemmileagets::date as the_date,
  last_value(a.value) over (
      partition by vehicleitemid, vehicleitemmileagets::date
      order by vehicleitemid, vehicleitemmileagets
      RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as miles
from ads.ext_vehicle_item_mileages a
where exists (
  select 1
  from greg.uc_base_vehicles 
  where vehicle_item_id = a.vehicleitemid)) x
group by vehicleitemid, the_date, miles

  
-- do it with VII, last mileage reading of VII interval

select b.vehicle_inventory_item_id, a.vehicleitemmileagets::date as the_date,
  last_value(a.value) over (
      partition by b.vehicle_inventory_item_id, a.vehicleitemmileagets::date
      order by b.vehicle_inventory_item_id, a.vehicleitemmileagets
      RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as miles
from ads.ext_vehicle_item_mileages a
inner join greg.uc_base_vehicles b on a.vehicleitemid = b.vehicle_item_id

-- nah, that's not exactly it

-- something goofy going on here
select *
from ads.ext_vehicle_item_mileages a
inner join greg.uc_base_vehicles b on a.vehicleitemid = b.vehicle_item_id
  and b.vehicle_inventory_item_id = '000f28e6-7be3-4d29-8869-afa688703d9a'

believe what i want is to limit the mileages to vehicles in base_vehicles, and miles in date ranges
what about multipe instances of VI

select a.stock_number, a.vin, a.date_acquired, a.sale_date,
  c.*
from greg.uc_base_vehicles a
inner join (
  select vehicle_item_id
  from greg.uc_base_vehicles
  group by vehicle_item_id
  having count(*) > 1) b on a.vehicle_item_id = b.vehicle_item_id
left join ads.ext_vehicle_item_mileages c on a.vehicle_item_id = c.vehicleitemid  
order by  a.vin, a.date_acquired  


-- i think this is the way to go
-- for each row in uc_Base_vehicles, the last mileage recorded during the interval of inventory for that vehicle
-- if there none, miles = -1

-- select count(*) from greg.uc_base_Vehicles  --21461
-- 2135 w/out miles
-- extend from date 3 days: 1868  7 days: 1521
-- most of these fuckers are intra market -- 1187 of them 
select a.stock_number, a.vin, a.vehicle_item_id, a.date_acquired, a.sale_date,
  c.*
from greg.uc_base_vehicles a
left join ads.ext_vehicle_item_mileages c on a.vehicle_item_id = c.vehicleitemid  
  and c.vehicleitemmileagets::Date between a.date_acquired -7 and a.sale_date
where c.vehicleitemid is null  and right(trim(stock_number), 1) in ('X','G')
order by  a.vin, a.date_acquired  

-- ext date_acq 7 days and pass on intra mark date requirement
-- down to 359 nulls
-- bump the date extention to 30 days, down to 233
-- good enuf
-- but what this means is that the mileage is for the vehicle instance NOT THE DATE !!!
select a.stock_number, a.vin, a.vehicle_item_id, a.date_acquired, a.sale_date,
  c.*
from greg.uc_base_vehicles a
left join ads.ext_vehicle_item_mileages c on a.vehicle_item_id = c.vehicleitemid  
  and 
    case
      when right(stock_number, 1) in ('X','G') then 1 = 1
      else c.vehicleitemmileagets::Date between a.date_acquired -30 and a.sale_date
    end
limit 100

select vehicleitemid, count(*) from ads.ext_vehicle_item_mileages group by vehicleitemid having count(*) > 5 order by count(*) desc

select a.stock_number, a.vin, a.vehicle_inventory_item_id, a.date_acquired, a.sale_date, 
  b.vehicleitemmileagets, b.value
from greg.uc_base_vehicles a
left join ads.ext_vehicle_item_mileages b on a.vehicle_item_id = b.vehicleitemid  
  and 
    case
      when right(a.stock_number, 1) in ('X','G') and right(a.stock_number, 2) <> 'XX' then 1 = 1
      else b.vehicleitemmileagets::Date between a.date_acquired -7 and a.sale_date
    end
where a.vehicle_item_id in ('e22bef31-58c1-8640-81dd-c82adb666a92','d56b0b83-540d-0743-8e9d-8b426f4bb33a','8deaa8f4-2a10-a248-9ddd-01fe067f0429','586a571c-af85-2544-88c7-8a38a86ecb19')
order by vin, date_acquired, vehicleitemmileagets



select a.stock_number, a.vin, a.vehicle_inventory_item_id, a.date_acquired, a.sale_date, 
  b.vehicleitemmileagets, b.value
from greg.uc_base_vehicles a
left join ads.ext_vehicle_item_mileages b on a.vehicle_item_id = b.vehicleitemid  
  and 
    case
      when right(a.stock_number, 1) in ('X','G') then 1 = 1
      else b.vehicleitemmileagets::Date between a.date_acquired -30 and a.sale_date
    end
where a.vehicle_inventory_item_id = 'c5d19fdf-a6e4-4ec2-9bbb-16e4f87b5557'



select * 
from ads.ext_vehicle_item_mileages a
inner join 
  (select * from greg.uc_base_vehicles where vin = '3GCRKTE28AG214762') b on a.vehicleitemid = b.vehicle_item_id
order by date_acquired


select *
from greg.uc_base_vehicles
where vin = '3GCRKTE28AG214762'
order by date_acquired


create table greg.uc_base_vehicle_miles (
  vehicle_inventory_item_id citext primary key references greg.uc_base_vehicles(vehicle_inventory_item_id),
  miles integer not null);
comment on column greg.uc_base_vehicle_miles.miles is 'from ads::dpsvseries.vehicleitemmileages.value';

insert into greg.uc_base_vehicle_miles
-- so after a shitload of fucking around, decided to go with this
select vehicle_inventory_item_id, coalesce(last_miles, -1)
from (
  select a.vehicle_inventory_item_id, 
    last_value(value) over (
        partition by vehicle_inventory_item_id
        order by vehicle_inventory_item_id, vehicleitemmileagets
        RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as last_miles              
  from greg.uc_base_vehicles a
  left join ads.ext_vehicle_item_mileages b on a.vehicle_item_id = b.vehicleitemid  
    and 
      case
        when right(a.stock_number, 1) in ('X','G') and right(a.stock_number, 2) <> 'XX' then 1 = 1
        else b.vehicleitemmileagets::Date between a.date_acquired -7 and a.sale_date
      end) x
group by vehicle_inventory_item_id, last_miles;



delete from greg.uc_base_Vehicle_miles
where miles between 10000 and 11000

select count(*) from greg.uc_base_vehicle_miles


select *
from (
  select vehicle_inventory_item_id, coalesce(last_miles, -1)
  from (
    select a.vehicle_inventory_item_id, 
      last_value(value) over (
          partition by vehicle_inventory_item_id
          order by vehicle_inventory_item_id, vehicleitemmileagets
          RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as last_miles              
    from greg.uc_base_vehicles a
    left join ads.ext_vehicle_item_mileages b on a.vehicle_item_id = b.vehicleitemid  
      and 
        case
          when right(a.stock_number, 1) in ('X','G') and right(a.stock_number, 2) <> 'XX' then 1 = 1
          else b.vehicleitemmileagets::Date between a.date_acquired -7 and a.sale_date
        end) x
  group by vehicle_inventory_item_id, last_miles) x
where not exists (
  select 1
  from greg.uc_base_vehicle_miles
  where vehicle_inventory_item_id = x.vehicle_inventory_item_id);



select count(*) from greg.uc_Base_vehicle_miles where miles = -1 -- 362

select count(*) from greg.uc_base_vehicle_miles where miles > 300000 -- 39

select * from greg.uc_base_vehicle_miles order by miles desc limit 100


-------------------------------------------------------------------------------------------------------------------------------------
--</ 9/13 ---------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------
this represents miles for the vehicle instance, not the date,
i row per stocknumber

create table uc.base_vehicle_miles (
  stock_number citext primary key references uc.base_vehicles(stock_number) on update cascade on delete cascade,
  miles integer not null);
comment on column uc.base_vehicle_miles.miles is 'from ads::dpsvseries.vehicleitemmileages.value';

insert into uc.base_vehicle_miles
-- so after a shitload of fucking around, decided to go with this
select stock_number, coalesce(last_miles, -1)
from (
  select a.stock_number, 
    last_value(value) over (
        partition by vehicle_inventory_item_id
        order by vehicle_inventory_item_id, vehicleitemmileagets
        RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as last_miles              
  from uc.base_vehicles a
  left join ads.ext_vehicle_item_mileages b on a.vehicle_item_id = b.vehicleitemid  
    and 
      case
        when right(a.stock_number, 1) in ('X','G') and right(a.stock_number, 2) <> 'XX' then 1 = 1
        else b.vehicleitemmileagets::Date between a.date_acquired -7 and a.sale_date
      end) x
group by stock_number, last_miles;

-------------------------------------------------------------------------------------------------------------------------------------
--/> 9/13 ---------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------




