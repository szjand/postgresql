﻿drop table if exists greg.uc_makes_models;
create table greg.uc_makes_models (
  make citext not null,
  model citext not null,
  shape citext not null,
  size citext not null,
  shape_size citext not null,
  hash citext not null,
  primary key (make,model));
create unique index on greg.uc_makes_models(hash);
create index on greg.uc_makes_models(shape);
create index on greg.uc_makes_models(size);
create index on greg.uc_makes_models(shape_size);
comment on table greg.uc_makes_models is 'all attributes from ads::dpsvseries.makemodelclassifications';

-- initial load
insert into greg.uc_makes_models
select a.make, a.model, 
  right(a.vehicletype, length(a.vehicletype) - position('_' in a.vehicletype)) as shape,
  right(a.vehiclesegment, length(a.vehiclesegment) - position('_' in a.vehiclesegment)) as size, 
  right(a.vehicletype, length(a.vehicletype) - position('_' in a.vehicletype)) 
    ||'-'||right(a.vehiclesegment, length(a.vehiclesegment) - position('_' in a.vehiclesegment)) as shape_size,
  b.hash
from ads.ext_make_model_classifications a
where a.thruts > now() -- current rows only, no history
left join (
  select make, model, md5(x::text) as hash
  from (
    select make, model, 
      right(vehicletype, length(vehicletype) - position('_' in vehicletype)) as shape,
      right(vehiclesegment, length(vehiclesegment) - position('_' in vehiclesegment)) as size, 
      right(vehicletype, length(vehicletype) - position('_' in vehicletype))
         ||'-'||right(vehiclesegment, length(vehiclesegment) - position('_' in vehiclesegment)) as shape_size
    from ads.ext_make_model_classifications
    where thruts > now()) x) b on a.make = b.make and a.model = b.model;

--nightly
insert into greg.uc_makes_models
select a.make, a.model, -- new rows
  right(a.vehicletype, length(a.vehicletype) - position('_' in a.vehicletype)) as shape,
  right(a.vehiclesegment, length(a.vehiclesegment) - position('_' in a.vehiclesegment)) as size, 
  right(a.vehicletype, length(a.vehicletype) - position('_' in a.vehicletype)) 
    ||'-'||right(a.vehiclesegment, 
      length(a.vehiclesegment) - position('_' in a.vehiclesegment)) as shape_size,
  b.hash
from ads.ext_make_model_classifications a
left join ( -- generates hash based on make/model
  select make, model, md5(x::text) as hash
  from (
    select make, model, 
      right(vehicletype, length(vehicletype) - position('_' in vehicletype)) as shape,
      right(vehiclesegment, length(vehiclesegment) - position('_' in vehiclesegment)) as size, 
      right(vehicletype, length(vehicletype) - position('_' in vehicletype)) 
        ||'-'||right(vehiclesegment, 
          length(vehiclesegment) - position('_' in vehiclesegment)) as shape_size
    from ads.ext_make_model_classifications
    where thruts > now()) x) b on a.make = b.make and a.model = b.model
where not exists (
  select 1
  from greg.uc_makes_models
  where make = a.make 
    and model = a.model)  
union
select x.* -- changed rows
from (
  select a.make, a.model, 
    right(a.vehicletype, length(a.vehicletype) - position('_' in a.vehicletype)) as shape,
    right(a.vehiclesegment, length(a.vehiclesegment) - position('_' in a.vehiclesegment)) as size, 
    right(a.vehicletype, length(a.vehicletype) - position('_' in a.vehicletype)) 
      ||'-'||right(a.vehiclesegment, 
        length(a.vehiclesegment) - position('_' in a.vehiclesegment)) as shape_size,
    b.hash
  from ads.ext_make_model_classifications a
  left join ( -- generates hash based on make/model
    select make, model, md5(x::text) as hash
    from (
      select make, model, 
        right(vehicletype, length(vehicletype) - position('_' in vehicletype)) as shape,
        right(vehiclesegment, length(vehiclesegment) - position('_' in vehiclesegment)) as size, 
        right(vehicletype, length(vehicletype) - position('_' in vehicletype)) 
          ||'-'||right(vehiclesegment, 
            length(vehiclesegment) - position('_' in vehiclesegment)) as shape_size
      from ads.ext_make_model_classifications
      where thruts > now()) x) b on a.make = b.make and a.model = b.model
  where a.thruts > now()) x
  inner join greg.uc_makes_models xx on x.make = xx.make
    and x.model = xx.model
    and x.hash <> xx.hash        
on conflict (make,model)
do update 
set (make,model,shape,size,shape_size,hash)
= (excluded.make,excluded.model,excluded.shape,excluded.size,excluded.shape_size,excluded.hash);


-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--</ 9/13 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


drop table if exists uc.makes_models cascade;
create table uc.makes_models (
  make citext not null,
  model citext not null,
  shape citext not null,
  size citext not null,
  shape_size citext not null,
  hash citext not null,
  primary key (make,model));
create unique index on uc.makes_models(hash);
create index on uc.makes_models(shape);
create index on uc.makes_models(size);
create index on uc.makes_models(shape_size);
comment on table uc.makes_models is 'all attributes from ads::dpsvseries.makemodelclassifications';

insert into uc.makes_models
select *
from greg.uc_makes_models;


CREATE TABLE uc.price_bands
(
  price_band citext primary key,
  price_from integer NOT NULL,
  price_thru integer NOT NULL);

insert into uc.price_bands
select *
from greg.uc_price_bands;
  

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--/> 9/13 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------