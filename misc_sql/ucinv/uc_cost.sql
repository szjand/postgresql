﻿select *
from greg.uc_Base_vehicle_accounting 
where page = 1
limit 100

-- wtf 2487 rows, we don't have that much inventory
select control
from greg.uc_Base_vehicle_accounting a
where page = 1
  and exists (
    select 1
    from arkona.xfm_inpmast
    where inpmast_stock_number = a.control)
group by control
having sum(amount) > 0


13887b sold 3/19/2012

select *
-- select sum(amount)
from greg.uc_base_vehicle_accounting
where control = '14591a'
  and page = 1
order by the_Date

-- join instead on base_Vehicles and limit date to sale_date and before

-- ok, this is close: 447 units, tool shows 451
select control, sum(amount)
from greg.uc_Base_vehicle_accounting a
inner join greg.uc_base_vehicles b on a.control = b.stock_number
--   and a.the_date <= b.sale_date
where page = 1
  and account_description not like '%PA%' 
group by control
having sum(amount) > 0

select * from greg.uc_Base_vehicle_accounting limit 100
-- ok, let's go for the detail

select a.the_date, a.control, a.account, a.account_type, c.department_code, 
  a.journal_code, a.amount, a.doc, a.trans, a.seq, a.post_status 
from greg.uc_Base_vehicle_accounting a
inner join greg.uc_base_vehicles b on a.control = b.stock_number
inner join fin.dim_account c on a.account = c.account
  and c.current_row = true
where page = 1
  and account_description not like '%PA%' 
  and b.date_acquired > '07/21/2017'

drop table if exists greg.uc_cost_details;
create table greg.uc_cost_details (
  the_date date not null, 
  stock_number citext not null,
  account citext not null,
  account_type citext not null,
  department_code citext not null,
  journal_code citext not null,
  amount numeric(12,2) not null,
  doc citext not null,
  trans_description citext,
  trans integer not null,
  seq integer not null,
  post_status citext not null,
  hash text not null,
  cost_category citext not null,
  FOREIGN KEY (trans,seq,post_status) REFERENCES fin.fact_gl(trans,seq,post_status) ON UPDATE CASCADE,
  primary key (trans,seq,post_status));
create index on greg.uc_cost_details(the_date);    
create index on greg.uc_cost_details(stock_number);
create index on greg.uc_cost_details(account_type);
create index on greg.uc_cost_details(account);
create index on greg.uc_cost_details(department_code);
create index on greg.uc_cost_details(journal_code);
create unique index on greg.uc_cost_details(hash);
insert into greg.uc_cost_details
select a.the_date, a.control, a.account, a.account_type, c.department_code, 
  a.journal_code, a.amount, a.doc, a.trans_description, a.trans, a.seq, a.post_status,
  (
    select md5(z::text)
    from (
      select the_date, control, aa.account, aa.account_type, department_code, 
        journal_code, amount, aa.doc, aa.trans_description, trans, seq, post_status,
        case
          when journal_code in ('SCA','SVI','AFM','POT') then 'recon'
          when journal_code = 'WTD' or (journal_code = 'GJE' and trans_description like 'WRITE%') then 'write_down'
          when journal_code in ('CRC','PCA') then 'misc'
          else 'purchase'
        end         
      from greg.uc_Base_vehicle_accounting aa
      inner join greg.uc_base_vehicles b on aa.control = b.stock_number
      inner join fin.dim_account c on aa.account = c.account
        and c.current_row = true
      where aa.page = 1
        and aa.account_description not like '%PA%'     
        and aa.trans = a.trans
        and aa.seq = a.seq
        and aa.post_status = a.post_status) z),
  case -- categories of cost: (from previous work in /crayon reports/postgres/ucinv_potential_gross.sql
    when journal_code in ('SCA','SVI','AFM','POT') then 'recon'
    when journal_code = 'WTD' or (journal_code = 'GJE' and trans_description like 'WRITE%') then 'write_down'
    when journal_code in ('CRC','PCA') then 'misc'
    else 'purchase'
  end as cost_category        
from greg.uc_Base_vehicle_accounting a
inner join greg.uc_base_vehicles b on a.control = b.stock_number
inner join fin.dim_account c on a.account = c.account
  and c.current_row = true
where page = 1
  and account_description not like '%PA%';

select * from greg.uc_cost_details where stock_number = '29720a'

select a.*
from greg.uc_cost_details a
where exists (
  select 1
  from greg.uc_cost_details
  where stock_number = a.stock_number
    and cost_category = 'misc')
order by stock_number, the_date    
limit 1000


select *
from greg.uc_cost_details a
where stock_number = '29549XXAA'
order by the_date


-- this is the cost_by_day table
-- fix min acct date < date_acquired here
drop table if exists cost_by_day;
create temp table cost_by_day as
select a.stock_number,
  case 
    when a.the_date < b.date_acquired then b.date_acquired
    when a.the_date <= b.sale_date then a.the_date
    else b.sale_date
  end as the_date,
  sum(case when a.the_date  <= b.sale_date then amount else 0 end) as cogs,
  sum(case when  a.the_date  > b.sale_date then amount else 0 end) as post_sale_cogs
from greg.uc_cost_details a
left join greg.uc_base_vehicles b on a.stock_number = b.stock_number
group by a.stock_number, 
  case 
    when a.the_date < b.date_acquired then b.date_acquired
    when a.the_date <= b.sale_date then a.the_date
    else b.sale_date
  end;


select * from cost_by_day where stock_number = '25594XXZ'

select *
from greg.uc_cost_details 
where stock_number = '25594XXZ'
order by the_date

select *
from greg.uc_base_vehicles
where stock_number = '25594XXZ'  -- 1/12 - 1/24


select *
from greg.uc_Base_vehicle_accounting 
where control = '27716B'
order by the_date

select  *
from arkona.ext_inpmast
where inpmast_stock_number = '25594XXZ'

select stock_number, min(the_Date)
from greg.uc_cost_details
group by stock_number
limit 100




-- first acct entry < than date_acquired
select a.stock_number, a.vin, a.date_acquired, b.min_acct_date, a.sale_date
from greg.uc_base_vehicles a
left join (
  select stock_number, min(the_Date) as min_acct_date
  from greg.uc_cost_details
  group by stock_number) b on a.stock_number = b.stock_number
where b.min_acct_date < a.date_acquired
order by stock_number  


select count(*) from greg.uc_Base_vehicles  --21601
where left(stock_number, 1) = 'C'


create temp table dates as
  select a.stock_number, a.vin, a.date_acquired, a.sale_date, 
    case
      when b.date_in_invent = 0 then '9999-12-31'::date
      else
        (left(b.date_in_invent::text,4) || '-' || 
          substring(b.date_in_invent::text from 5 for 2) || '-' || 
          substring(b.date_in_invent::text from 7 for 2))::date 
    end as date_in_invent,
    case
      when b.date_delivered = 0 then '9999-12-31'::date
      else 
        (left(b.date_delivered::text,4) || '-' || 
          substring(b.date_delivered::text from 5 for 2) || '-' || 
          substring(b.date_delivered::text from 7 for 2))::date 
    end as date_delivered, 
    c.min_acct_date, max_acct_date              
  from greg.uc_base_vehicles a 
  left join arkona.xfm_inpmast b on a.stock_number = b.inpmast_stock_number
    and b.current_row = true
  left join (
    select a.control, min(a.the_date) as min_acct_date, max(a.the_date) as max_acct_date
    from greg.uc_Base_vehicle_accounting a
    inner join greg.uc_base_vehicles aa on a.control = aa.stock_number
    where page = 1
      and account_description not like '%PA%'
    group by a.control) c on a.stock_number = c.control


select  coalesce(abs(date_acquired - min_acct_date), 666), count(*)
from (
  select * from dates a
  except
  select *
  from  dates b
  where date_acquired = min_acct_date
    or date_in_invent = min_acct_date) x
group by coalesce(abs(date_acquired - min_acct_date), 666)
order by count(*) desc

select * -- exclude crookston and 2011, down to 243
from (
  select * from dates a -- within 10 days down to 979 questionable from_dates
  except
  select *
  from  dates b
  where date_acquired = min_acct_date
    or date_in_invent = min_acct_date
    or abs(date_acquired - min_acct_date) < 10
    or abs(date_in_invent - min_acct_date) < 10) x
where left(stock_number, 1) <> 'C'  
  and date_acquired > '12/31/2011'
order by stock_number

-- bad stock numbers in uc_base_vehicles
select a.stock_number, a.vin, a.date_acquired, a.sale_date, 
  c.inpmast_stock_number, c.inpmast_vin, c.date_in_invent, c.date_delivered,
  case
    when exists (
      select 1
      from fin.fact_gl
      where control = c.inpmast_Stock_number) then 'YES'
    else 'NO'
  end
from greg.uc_base_vehicles a
left join greg.uc_Base_vehicle_accounting b on a.stock_number = b.control
left join arkona.xfm_inpmast c on right(a.vin, 6) = right(c.inpmast_vin, 6)
where b.control is null
  and left(a.stock_number, 1) <> 'C'


ok, so, with all the above, thinking of using min_acct_date for date acquired where appropriate
but how am i going to maintain that if uc_base_vehicle_accounting depends on uc_base_vehicles
shit its a circle
oh it does not
only depends on account_lines



select *
from dates
where sale_date <= current_date -- exclude inventory


  select * from dates a where sale_date <= current_date and left(trim(stock_number), 1) <> 'C'
  except
  select *
  from  dates b
  where sale_date = date_delivered
    or date_delivered = max_acct_date
    or sale_date = max_acct_date
    or abs(sale_date - date_delivered) < 10
    or abs(sale_date - max_acct_date) < 10
    or abs(date_delivered - max_acct_date) < 10
    or max_acct_date < sale_date
order by sale_date    

