﻿--8/21/17 added priced_thru
-- 8/22/17 added unique index on vehicle_inventory_item_id,priced_thru

drop table if exists greg.uc_base_vehicle_prices;
create table greg.uc_base_vehicle_prices (
  vehicle_inventory_item_id citext not null references greg.uc_base_vehicles(vehicle_inventory_item_id),
  priced_by citext,
  date_priced date not null,
  invoice numeric(12,2) not null,
  best_price numeric(12,2) not null,
  priced_thru date not null,
  primary key (vehicle_inventory_item_id,date_priced));
comment on column greg.uc_base_vehicle_prices.priced_by is 'from ads::dpsvseries.vehiclepricings.pricedby';
comment on column greg.uc_base_vehicle_prices.date_priced is 'from ads::dpsvseries.vehiclepricings.vehiclepricingts';
comment on column greg.uc_base_vehicle_prices.invoice is 'from ads::dpsvseries.vehiclepricingdetails.amount';
comment on column greg.uc_base_vehicle_prices.best_price is 'from ads::dpsvseries.vehiclepricingdetails.amount';

create unique index on greg.uc_base_vehicle_prices(vehicle_inventory_item_id,priced_thru);
  
insert into greg.uc_base_vehicle_prices
select vehicleinventoryitemid,pricedby,date_priced,invoice,best_price,
  coalesce(lead(date_priced) 
  over (
    partition by vehicleinventoryitemid order by date_priced) - 1, '12/31/9999') as priced_thru
from (
  select a.vehicleinventoryitemid, 
    last_value(a.pricedby) over (
        partition by vehicleinventoryitemid, vehiclepricingts::date 
        order by vehicleinventoryitemid, vehiclepricingts  
        RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as pricedby,
    a.vehiclepricingts::date as date_priced, 
    last_value(b.amount) over (
        partition by vehicleinventoryitemid, vehiclepricingts::date 
        order by vehicleinventoryitemid, vehiclepricingts  
        RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as invoice,
    last_value(c.amount) over (
        partition by vehicleinventoryitemid, vehiclepricingts::date 
        order by vehicleinventoryitemid, vehiclepricingts  
        RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as best_price     
  from ads.ext_vehicle_pricings a
  inner join greg.uc_base_vehicles aa on a.vehicleinventoryitemid = aa.vehicle_inventory_item_id
  left join ads.ext_vehicle_pricing_details b on a.vehiclepricingid = b.vehiclepricingid
    and b.typ = 'VehiclePricingDetail_Invoice'
  left join ads.ext_vehicle_pricing_details c on a.vehiclepricingid = c.vehiclepricingid
    and c.typ = 'VehiclePricingDetail_BestPrice') x
group by vehicleinventoryitemid,pricedby,date_priced,invoice,best_price;


select *
from greg.uc_base_vehicle_prices limit 100

select count(*) from greg.uc_base_vehicle_prices where date_priced = current_Date  - 4

delete from greg.uc_base_vehicle_prices where date_priced = current_Date  - 4

select greg.update_uc_base_vehicle_prices()

select 
insert into greg.uc_base_vehicle_prices
select *
  from (
  select vehicleinventoryitemid,pricedby,date_priced,invoice,best_price,
    coalesce(lead(date_priced) 
  over (
    partition by vehicleinventoryitemid order by date_priced) - 1, '12/31/9999') as priced_thru
  from (
    select a.vehicleinventoryitemid, 
      last_value(a.pricedby) over (
          partition by vehicleinventoryitemid, vehiclepricingts::date 
          order by vehicleinventoryitemid, vehiclepricingts  
          RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as pricedby,
      a.vehiclepricingts::date as date_priced, 
      last_value(b.amount) over (
          partition by vehicleinventoryitemid, vehiclepricingts::date 
          order by vehicleinventoryitemid, vehiclepricingts  
          RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as invoice,
      last_value(c.amount) over (
          partition by vehicleinventoryitemid, vehiclepricingts::date 
          order by vehicleinventoryitemid, vehiclepricingts  
          RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as best_price     
    from ads.ext_vehicle_pricings a
    inner join greg.uc_base_vehicles aa on a.vehicleinventoryitemid = aa.vehicle_inventory_item_id
    left join ads.ext_vehicle_pricing_details b on a.vehiclepricingid = b.vehiclepricingid
      and b.typ = 'VehiclePricingDetail_Invoice'
    left join ads.ext_vehicle_pricing_details c on a.vehiclepricingid = c.vehiclepricingid
      and c.typ = 'VehiclePricingDetail_BestPrice') x
  group by vehicleinventoryitemid,pricedby,date_priced,invoice,best_price) x
  where not exists (
    select 1
    from greg.uc_base_vehicle_prices
    where vehicle_inventory_item_id = x.vehicleinventoryitemid
      and date_priced = x.date_priced);



select vehicleinventoryitemid,pricedby,date_priced,invoice,best_price,
  coalesce(lead(date_priced) 
  over (
    partition by vehicleinventoryitemid order by date_priced) - 1, '12/31/9999') as priced_thru
from (
  select a.vehicleinventoryitemid, 
    last_value(a.pricedby) over (
        partition by vehicleinventoryitemid, vehiclepricingts::date 
        order by vehicleinventoryitemid, vehiclepricingts  
        RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as pricedby,
    a.vehiclepricingts::date as date_priced, 
    last_value(b.amount) over (
        partition by vehicleinventoryitemid, vehiclepricingts::date 
        order by vehicleinventoryitemid, vehiclepricingts  
        RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as invoice,
    last_value(c.amount) over (
        partition by vehicleinventoryitemid, vehiclepricingts::date 
        order by vehicleinventoryitemid, vehiclepricingts  
        RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as best_price     
  from ads.ext_vehicle_pricings a
  inner join greg.uc_base_vehicles aa on a.vehicleinventoryitemid = aa.vehicle_inventory_item_id
  left join ads.ext_vehicle_pricing_details b on a.vehiclepricingid = b.vehiclepricingid
    and b.typ = 'VehiclePricingDetail_Invoice'
  left join ads.ext_vehicle_pricing_details c on a.vehiclepricingid = c.vehiclepricingid
    and c.typ = 'VehiclePricingDetail_BestPrice') x
where vehicleinventoryitemid = 'de57820a-4580-43c9-9259-ac5cbb108614'    
group by vehicleinventoryitemid,pricedby,date_priced,invoice,best_price;




-------------------------------------------------------------------------------------------------------------------------------------
--</ 9/13 ---------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------

drop table if exists uc.base_vehicle_prices cascade;
create table uc.base_vehicle_prices (
  stock_number citext not null references uc.base_vehicles(stock_number) on update cascade on delete cascade,
  priced_by citext,
  date_priced date not null,
  invoice numeric(12,2) not null,
  best_price numeric(12,2) not null,
  priced_thru date not null,
  primary key (stock_number,date_priced));
comment on column uc.base_vehicle_prices.priced_by is 'from ads::dpsvseries.vehiclepricings.pricedby';
comment on column uc.base_vehicle_prices.date_priced is 'from ads::dpsvseries.vehiclepricings.vehiclepricingts';
comment on column uc.base_vehicle_prices.invoice is 'from ads::dpsvseries.vehiclepricingdetails.amount';
comment on column uc.base_vehicle_prices.best_price is 'from ads::dpsvseries.vehiclepricingdetails.amount';

create unique index on uc.base_vehicle_prices(stock_number,priced_thru);
  
insert into uc.base_vehicle_prices
select stock_number,pricedby,date_priced,invoice,best_price,
  coalesce(lead(date_priced) 
  over (
    partition by stock_number order by date_priced) - 1, '12/31/9999') as priced_thru
from (
  select aa.stock_number, 
    last_value(a.pricedby) over (
        partition by stock_number, vehiclepricingts::date 
        order by stock_number, vehiclepricingts  
        RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as pricedby,
    a.vehiclepricingts::date as date_priced, 
    last_value(b.amount) over (
        partition by stock_number, vehiclepricingts::date 
        order by stock_number, vehiclepricingts  
        RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as invoice,
    last_value(c.amount) over (
        partition by stock_number, vehiclepricingts::date 
        order by stock_number, vehiclepricingts  
        RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as best_price     
  from ads.ext_vehicle_pricings a
  inner join uc.base_vehicles aa on a.vehicleinventoryitemid = aa.vehicle_inventory_item_id
  left join ads.ext_vehicle_pricing_details b on a.vehiclepricingid = b.vehiclepricingid
    and b.typ = 'VehiclePricingDetail_Invoice'
  left join ads.ext_vehicle_pricing_details c on a.vehiclepricingid = c.vehiclepricingid
    and c.typ = 'VehiclePricingDetail_BestPrice') x
group by stock_number,pricedby,date_priced,invoice,best_price;

-------------------------------------------------------------------------------------------------------------------------------------
--/> 9/13 ---------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------
