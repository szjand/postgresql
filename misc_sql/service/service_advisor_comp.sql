﻿
/*
Jon, 

Hoping you can offer some assistance! 


Are you able to break out each individual service advisor’s (just from main shop) comp from the last 12 months?   If possible, can you break it down to their hourly, commission, and overtime variance?   If not, just a total dollar amount.   

Rod Troftgruben
Ned Euliss
Cory Stinar
Nick Maro
Justin Rodriguez
Travis Bursinger
Connor Gothberg


We are seeing too much variation of compensation with pretty stable labor sales.  We would like to dig into it more.  


Example-


Rod Troftgruben

May 2018- 
Hourly- $1350
Commission- $7200
OT Var- $530

June 2018 
Hourly- $1600
Commission- $ 6800
OT Var- $475




If you have any questions, please reach out to Jeri or I.  

Thanks Buddy!

Dan

*/
spreadsheet name: service_advisor_comp

select * from arkona.ext_pyhshdta where employee_ = '1140870' and payroll_ending_year = 19

select * from arkona.xfm_pymast where pymast_employee_number = '1140870'

  select company_number, employee_, year_month,
    sum(case when code_id = '79' then amount else 0 end) as commission,
    sum(case when code_id = '78' then amount else 0 end) as draw,
    sum(case when code_id = 'OVT' then amount else 0 end) as overtime,
    sum(case when code_id = '400' then amount else 0 end) as pto_pay_out,
    sum(case when code_id = '500' then amount else 0 end) as lease,
    sum(case when code_id = '74' then amount else 0 end) as vacation,
    sum(case when code_id = '82' then amount else 0 end) as spiffs
  from (-- c: pyhscdta 
    select a.company_number, a.employee_,
      ((2000 + a.check_year) *100) + a.check_month as year_month,
      sum(b.amount) as amount, b.code_id
    from arkona.ext_pyhshdta a
    inner join arkona.ext_pyhscdta b on a.payroll_run_number = b.payroll_run_number
      and a.company_number = b.company_number
      and a.employee_ = b.employee_number
      and b.code_type in ('0','1') -- 0,1: income, 2: deduction
    where trim(a.distrib_code) in ('TEAM','SALE','RAO','SALM')
      and a.payroll_cen_year = (
        select distinct (the_year - 2000) + 100
        from dds.dim_date
        where year_month = (
          select year_month
          from sls.months
          where open_closed = 'open'))      
    group by a.company_number, a.employee_name, a.employee_,
      ((2000 + a.check_year) *100) + a.check_month, b.code_id) c
  group by company_number, employee_, year_month

drop table if exists wtf_1;
create temp table wtf_1 as
select employee_, employee_name, year_month, code_id, sum(amount) as amount
from (
  select c.employee_name, a.employee_,
    ((2000 + a.check_year) *100) + a.check_month as year_month,
    b.amount, b.code_id
  from arkona.ext_pyhshdta a
  inner join arkona.ext_pyhscdta b on a.payroll_run_number = b.payroll_run_number
    and a.company_number = b.company_number
    and a.employee_ = b.employee_number
    and b.code_type in ('0','1') -- 0,1: income, 2: deduction
  left join arkona.xfm_pymast c on a.employee_ =  c.pymast_employee_number
    and c.current_row
  where a.employee_ in (
      select pymast_employee_number
      from arkona.xfm_pymast
      where employee_last_name in (
        'Troftgruben',
        'Euliss',
        'Stinar',
        'Maro',
        'Rodriguez',
        'Bursinger',
        'Gothberg'))  
    and a.employee_ not in ('1117850', '1132595')    
    and ((2000 + a.check_year) *100) + a.check_month between 201806 and 201905) e
group by employee_, employee_name, year_month, code_id

select employee_name, year_month,
  sum(amount) filter (where code_id = '79') as commission,
  sum(amount) filter (where code_id = '70') as tech_xtra_bonus,
  sum(amount) filter (where code_id = 'OVT') as overtime,
  sum(amount) filter (where code_id = '500') as lease,
  sum(amount) filter (where code_id = 'HOL') as holiday,
  sum(amount) filter (where code_id = 'VAC') as vacation,
  sum(amount) filter (where code_id = 'PTO') as pto
from wtf_1
group by employee_name, year_month
order by employee_name, year_month

-- this is the spreadsheet i sent on 6/5/19
select x.*, y.commission, y.tech_xtra_bonus, y.overtime, y.lease, y.holiday, y.vacation, y.pto,
  coalesce(x.base_pay, 0) + coalesce(y.commission, 0) + coalesce(y.overtime, 0) + coalesce(y.lease, 0) + 
    coalesce(y.holiday, 0) + coalesce(y.vacation, 0) + coalesce(y.pto, 0) + coalesce(y.tech_xtra_bonus, 0) as total
from (
  select c.employee_name, 
    ((2000 + a.check_year) *100) + a.check_month as year_month,
    sum(a.base_pay) as base_pay
  from arkona.ext_pyhshdta a
  left join arkona.xfm_pymast c on a.employee_ =  c.pymast_employee_number
    and c.current_row
  where a.employee_ in (
      select pymast_employee_number
      from arkona.xfm_pymast
      where employee_last_name in (
        'Troftgruben',
        'Euliss',
        'Stinar',
        'Maro',
        'Rodriguez',
        'Bursinger',
        'Gothberg'))  
    and a.employee_ not in ('1117850', '1132595')    
    and ((2000 + a.check_year) *100) + a.check_month between 201806 and 201905
  group by c.employee_name, ((2000 + a.check_year) *100) + a.check_month) x
left join (
  select employee_name, year_month,
    sum(amount) filter (where code_id = '79') as commission,
    sum(amount) filter (where code_id = '70') as tech_xtra_bonus,
    sum(amount) filter (where code_id = 'OVT') as overtime,
    sum(amount) filter (where code_id = '500') as lease,
    sum(amount) filter (where code_id = 'HOL') as holiday,
    sum(amount) filter (where code_id = 'VAC') as vacation,
    sum(amount) filter (where code_id = 'PTO') as pto
  from wtf_1
  group by employee_name, year_month) y on x.employee_name = y.employee_name and x.year_month = y.year_month
order by x.employee_name, x.year_month


    
select distinct code_id from wtf_1
select distinct employee_name, employee_ from  wtf_1

79: commission
70: tech xtra bonus
HOL: holiday
OVT: overtime
500: lease
VAC: vacation
PTO: pto

select * from wtf_1 where employee_name like 'trof%' and year_month = 201905

select* from wtf_1 where code_id = 

select employee_name, year_month, 
  case when code_id = 
from wtf_1
group by employee_name, year_month
order by employee_name, year_month



