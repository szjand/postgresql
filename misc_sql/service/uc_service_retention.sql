﻿select a.bopmast_company_number, a.bopmast_stock_number, a.bopmast_vin, a.date_capped, a.bopmast_search_name, count(*), min(arkona.db2_integer_to_date_long(b.open_date)) 
from arkona.ext_bopmast a
left join arkona.ext_sdprhdr b on a.bopmast_vin = b.vin
  and arkona.db2_integer_to_date_long(b.open_date) > a.date_capped
  and b.cust_name not like 'INVENTORY%'
  and b.cust_name <> '*VOIDED REPAIR ORDER*'
where record_status = 'U'
  and date_capped between '01/01/2018' and '12/31/2018'
  and vehicle_type = 'U'
  and sale_type <> 'W'
  and a.bopmast_company_number = 'RY1'
group by a.bopmast_company_number, a.bopmast_stock_number, a.bopmast_vin, a.date_capped, a.bopmast_search_name


-- 2469 vehicles sold
drop table if exists sales;
create temp table sales as
select a.bopmast_vin, max(a.date_capped) as sale_date
from arkona.ext_bopmast a
where record_status = 'U'
  and date_capped between '01/01/2018' and '12/31/2018'
  and vehicle_type = 'U'
  and sale_type <> 'W'
  and a.bopmast_company_number = 'RY1'
group by a.bopmast_vin;

select count(c.bopmast_vin) filter (where ros = 0) as no_show, count(c.bopmast_vin) filter (where ros <> 0) as had_service
from (
  select a.bopmast_vin, count(b.vin) as ros
  from sales a
  left join arkona.ext_sdprhdr b on a.bopmast_vin = b.vin
    and arkona.db2_integer_to_date_long(b.open_date) > a.sale_date
    and b.cust_name not like 'INVENTORY%'
    and b.cust_name <> '*VOIDED REPAIR ORDER*'
  group by a.bopmast_vin) c
