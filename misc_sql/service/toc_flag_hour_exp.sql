﻿select pay_period_start, pay_period_end, last_name, first_name, tech_number, tech_flag_hours_pptd as flag_hours, tech_clock_hours_pptd as clock_hours, round(100 * tech_flag_hours_pptd/tech_clock_hours_pptd, 1) as prof
from tp.data
where the_date in ('08/27/2022','09/10/2022')
  and tech_number in ('577','528','654','658')
order by last_name, the_date  


select last_name, first_name, tech_number, 
  sum(tech_flag_hours_pptd) filter (where pay_period_end = '08/27/2022') as "flag 8-14 -> 8-27",
  sum(tech_flag_hours_pptd) filter (where pay_period_end = '09/10/2022') as "flag 8-28 -> 9-10",
  sum(tech_clock_hours_pptd) filter (where pay_period_end = '08/27/2022') as "clock 8-14 -> 8-27",
  sum(tech_clock_hours_pptd) filter (where pay_period_end = '09/10/2022') as "clock 8-28 -> 9-10",
  sum(round(100 * tech_flag_hours_pptd/tech_clock_hours_pptd, 1)) filter (where pay_period_end = '08/27/2022') as "prof 8-14 -> 8-27",
  sum(round(100 * tech_flag_hours_pptd/tech_clock_hours_pptd, 1)) filter (where pay_period_end = '09/10/2022') as "prof 8-28 -> 9-10"
-- tech_flag_hours_pptd as flag_hours, tech_clock_hours_pptd as clock_hours, round(100 * tech_flag_hours_pptd/tech_clock_hours_pptd, 1) as prof
from tp.data
where the_date in ('08/27/2022','09/10/2022')
  and tech_number in ('577','528','654','658')
group by last_name, first_name, tech_number  


select * from dds.dim_tech where tech_name like '%soberg%'