﻿-- 11/26/19
-- at jeris suggestion, start with ros with AM service type

select a.ro, b.the_date
from ads.ext_Fact_repair_order a
join dds.dim_date b on a.opendatekey =  b.date_key
  and b.the_date > current_date - 730
join ads.ext_dim_service_type c on a.servicetypekey = c.servicetypekey  
  and c.servicetypecode = 'AM'


-- ~ 2000 with multiple lines per ro
select a.ro, count(*)
from ads.ext_Fact_repair_order a
join dds.dim_date b on a.opendatekey =  b.date_key
  and b.the_date > current_date - 730
join ads.ext_dim_service_type c on a.servicetypekey = c.servicetypekey  
  and c.servicetypecode = 'AM'
group by ro
order by count(*) desc


select a.ro, a.line, b.the_date, left(d.correction, 50), e.opcode as cor_code, left(e.description, 25) as cor_descr, 
  f.opcode, left(f.description, 25) as op_desc
from ads.ext_Fact_repair_order a
join dds.dim_date b on a.opendatekey =  b.date_key
  and b.the_date > current_date - 730
join ads.ext_dim_service_type c on a.servicetypekey = c.servicetypekey  
  and c.servicetypecode = 'AM'
join ads.ext_dim_ccc d on a.ccckey = d.ccckey  
join ads.ext_dim_opcode e on a.corcodekey = e.opcodekey
join ads.ext_dim_opcode f on a.opcodekey = f.opcodekey
where a.storecode = 'RY1'
-- where a.ro = '16300978'
-- 
select * from ads.ext_dim_ccc  where ro = '16300978'

select max(ccckey) from ads.ext_dim_ccc

1081889

select a.amount, b.account, b.description
from fin.fact_gl a
join fin.dim_Account b on a.account_key = b.account_key
where a.control = '16300978'


just extract the data from a query in ads

create schema af;
comment on schema af is 'a place to stick tables for the aftermarket project, started with 2 years of ros';

drop table if exists af.ros;
create table af.ros (
  ro citext not null,
  line integer not null,
  the_date date not null,
  customer citext not null,
  correction citext not null,
  op_code citext not null,
  op_code_desc citext not null,
  cor_code citext not null,
  cor_code_desc citext not null,
  writer citext not null);
comment on table af.ros is 'ros extracted from advantage dds where the service type is am';

select * 
from af.ros
where ro = '16296384'


select b.part_description, a.* 
from arkona.ext_pdptdet a
join arkona.ext_pdpmast b on a.ptpart = b.part_number
where ptinv_ = '16296384'



select ro, line
from af.ros
group by ro, line
having count(*) > 1


select *
from af.ros
where ro = '16321000'
  and line = 2

3770 ros
select count(distinct ro) from af.ros




