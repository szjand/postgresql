﻿account 130401 journal PCA Reference is last 6 of vin, document is parts invoice

select a.*
from fin.fact_gl a
join dds.dim_Date b on a.date_key = b.date_key
  and b.the_date > current_date - 60
join fin.dim_account c on a.account_key = c.account_key
  and c.account = '130401'
join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code = 'PCA'   

select * 
from arkona.ext_pdptdet
where ptinv_ = '15470221'

select a.doc, a.ref, a.amount, f.ptpart,f.ptqty, g.*
from fin.fact_gl a
join dds.dim_Date b on a.date_key = b.date_key
  and b.the_date > current_date - 120
join fin.dim_account c on a.account_key = c.account_key
  and c.account = '130401'
join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code = 'PCA'  
left join arkona.ext_pdptdet f on a.doc = f.ptinv_  
left join arkona.ext_pdpmast g on f.ptpart = g.part_number
order by a.ref  


-- 414 rows
select g.part_number, g.part_description, count(*), string_agg(distinct g.stocking_group, ',')
from fin.fact_gl a
join dds.dim_Date b on a.date_key = b.date_key
  and b.the_date > current_date - 90
join fin.dim_account c on a.account_key = c.account_key
  and c.account = '130401'
join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code = 'PCA'  
left join arkona.ext_pdptdet f on a.doc = f.ptinv_  
left join arkona.ext_pdpmast g on f.ptpart = g.part_number
  and g.part_description <>  '1'
group by g.part_number, g.part_description
order by g.part_description


-- last 90 days of gl transactions 130401/PCA
-- 12/2 send to group
select g.part_description, count(*), string_agg(distinct g.stocking_group || '/' || g.part_number, ',')
from fin.fact_gl a
join dds.dim_Date b on a.date_key = b.date_key
  and b.the_date > current_date - 90
join fin.dim_account c on a.account_key = c.account_key
  and c.account = '130401'
join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code = 'PCA'  
left join arkona.ext_pdptdet f on a.doc = f.ptinv_  
left join arkona.ext_pdpmast g on f.ptpart = g.part_number
  and g.part_description <>  '1'
  and trim(g.part_description) <> ''
group by g.part_description
order by count(*) desc



