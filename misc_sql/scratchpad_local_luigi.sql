﻿-- this all started with the luigi_dependency_tree, saw that car_deals.DealsChangedRows() is called by run_all,
-- but in the diagram it shows as a requirement for sc_payroll_201803.DealsByMonth() so it annoys my ocd and
-- i believe should not be called in run_all
-- but i am hesitant to fuck around with production code
-- so
-- i am setting up a local instance of the pg tables i need to run luigi locally where i can make the changes
-- and fuck around
-- turns out making a local copy of the reqd db shit is not simple

-- generate drop schema statements
select string_agg(format('DROP SCHEMA %I CASCADE;', schemaname), E'\n')
from (
select schemaname
from pg_catalog.pg_tables
where schemaname not in ('information_schema','pg_catalog','public')
group by schemaname) X

DROP SCHEMA ads CASCADE;
DROP SCHEMA arkona CASCADE;
DROP SCHEMA bspp CASCADE;
DROP SCHEMA chr CASCADE;
DROP SCHEMA dao CASCADE;
DROP SCHEMA dds CASCADE;
DROP SCHEMA fin CASCADE;
DROP SCHEMA gmgl CASCADE;
DROP SCHEMA jeri CASCADE;
DROP SCHEMA luigi CASCADE;
DROP SCHEMA nc CASCADE;
DROP SCHEMA sls CASCADE;




-- generate backup statement for a bunch of tables
SELECT 'pg_dump ' || ' -h localhost -p 5432 -U postgres  -F c -b -v -f "/pgbak/somedb_keytbls.backup" ' ||
  array_to_string(ARRAY(SELECT '-t ' || table_schema || '.' || table_name   
        FROM information_schema.tables 
        WHERE table_name LIKE '%_notes' AND table_schema NOT IN('pg_catalog','public' )
    ), ' ') || ' somedb';


select '/usr/bin/pg_dump --host 10.130.196.173 --port 5432 --username "postgres" --no-password  
  --format custom --blobs --verbose --file "/home/jon/Desktop/luigi_dds.backup" -a'
  || array_to_string(ARRAY(SELECT '-t ' || table_schema || '.' || table_name


select '/usr/bin/pg_dump --host 10.130.196.173 --port 5432 --username "postgres" --no-password --format custom --blobs --verbose --file "/home/jon/Desktop/luigi_fin.backup" -a ' ||
  string_agg('--table ' || '"' ||table_schema || '.' || table_name||'"' , ' ') || ' ""cartiva"" '
from information_schema.TABLES
where table_schema = 'fin'
  and left(table_name, 1) not in ('z','t','x','e','p')
  and table_type = 'BASE TABLE'

/usr/bin/pg_dump --host 10.130.196.173 --port 5432 --username "postgres" --no-password --format custom --blobs --verbose --file "/home/jon/Desktop/luigi_dds.backup" -a--table fin.budget --table fin.dim_doc_type --table fin.dim_fs --table fin.dim_fs_account --table fin.dim_journal --table fin.dim_gl_description --table fin.dim_fs_org --table fin.fact_gl --table fin.fact_budget_variance --table fin.fact_fs --table fin.fact_gl_change_log --table fin.dim_account cartiva 


ran out of memory doing them all at once
do a separate backup for each file

-- separate back up commands
select '/usr/bin/pg_dump --host 10.130.196.173 --port 5432 --username "postgres" --no-password --format custom --blobs --verbose --file "/home/jon/Desktop/luigi_fin.backup" -a ' ||
  '--table ' || '"' ||table_schema || '.' || table_name||'"' || ' ""cartiva"" '
  ||chr(13)||
  '/usr/bin/pg_restore --host localhost --port 5432 --username "postgres" --dbname "cartiva" --no-password  --verbose "/home/jon/Desktop/luigi_fin.backup"'
from information_schema.TABLES
where table_schema = 'fin'
  and left(table_name, 1) not in ('z','t','x','e','p')
  and table_type = 'BASE TABLE'

-- separate restore commands

fin.fact_gl is too big
lets try fdw
lets experiment with a small table first

create schema prod_fin;
import foreign schema fin limit to (fact_gl)
from server cartiva_173 into prod_fin;

insert into fin.fact_gl
select *
from prod_fin.fact_gl

shit same thing out of shared memory

and trying to fucking query anything from the fdw is forever

so, did a python script, loading by day, done