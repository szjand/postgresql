﻿select pipeline, max(thru_ts::time), min(thru_ts::time), avg(thru_ts::time)
from luigi.luigi_log
where status = 'pass'
  and the_Date > '01/01/2019'
  and thru_ts::time < '03:00:00'
group by pipeline  
order by max(thru_ts::time) desc 


select pipeline, avg(thru_ts - from_ts), min(thru_ts - from_ts), max(thru_ts - from_ts)
from luigi.luigi_log
where status = 'pass'
  and the_Date > '05/01/2019'
  and thru_ts::time < '03:00:00'
  and from_ts::time > '02:00:00'
group by pipeline
order by pipeline


select the_date, min(from_ts), max(thru_ts), max(thru_ts) - min(from_ts)
from luigi.luigi_log
where status = 'pass'
  and the_Date > '05/01/2019'
  and thru_ts::time < '03:00:00'
  and from_ts::time > '02:00:00'
group by the_date
order by the_date


select the_date, count(*)
from luigi.luigi_log
where status = 'pass'
  and the_Date > '05/01/2019'
  and thru_ts::time < '03:00:00'
  and from_ts::time > '02:00:00'
group by the_date
order by the_date