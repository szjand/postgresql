﻿GM Feb 2017 wholesale deals
select the_date,store_code,stock_number,vin,date_acquired,sale_date,make,model,
  year,bodystyle,trim,color,cab,drive,shape,size,shape_size,miles,date_priced,
  best_price,days_since_priced,price_band,days_owned,status,days_in_status,
  recon_sales_running,recon_gross_running,
  inventory_cost_running,front_gross_at_sale, front_gross_post_sale,
  back_gross_at_sale, back_gross_post_sale
from greg.uc_daily_snapshot_beta_1
where the_date = sale_date
  and sale_date between '02/01/2017' and '02/28/2017'
  and sale_type = 'wholesale'
  and store_code = 'ry1'


-- 201401 -> 201708
select sale_date, count(*)
from greg.uc_daily_snapshot_beta_1
where sale_date < current_date
  and sale_date = the_date
group by sale_date
order by sale_date



select a.store_code, b.year_month, a.sale_type, count(*)
from greg.uc_daily_snapshot_beta_1 a
inner join dds.dim_date b on a.sale_date = b.the_date
where a.sale_date between '01/01/2014' and '08/31/2017'
  and a.sale_date = a.the_date
group by a.store_code, b.year_month, a.sale_type
order by a.store_code, b.year_month, a.sale_type

-- inventory on a given date
select store_code, shape, size, price_band, status, count(*)
from greg.uc_daily_snapshot_beta_1 a
where the_date = '08/10/2017'
group by store_code, shape, size, price_band, status
order by store_code, shape, size, price_band


select shape, size, price_band

select *
from greg.uc_daily_snapshot_beta_1 a
where the_date = '08/10/2016'
order by sale_date desc 

select *
from greg.uc_daily_snapshot_beta_1 a
where sale_date = '08/10/2016'
  and the_date = '08/10/2016'


select max(the_date)
from greg.uc_daily_snapshot_beta_1
where sale_date < current_date

select count(distinct stock_number) --17297
from greg.uc_daily_snapshot_beta_1

select count(distinct vin) --15195
from greg.uc_daily_snapshot_beta_1


-- wholesale count and gross is badly off
-- avg gross
select store_code, sale_type, sum(front_gross_At_Sale), count(*)
from greg.uc_daily_snapshot_beta_1 a
inner join dds.dim_date b on a.the_date = b.the_date
where a.sale_date = a.the_date
  and b.year_month = 201711
group by store_code, sale_type  

select *
from greg.uc_base_vehicles
where sale_date between '11/01/2017' and '11/30/2017'
  and store_code = 'ry1'

select a.stock_number, a.sale_date, sale_type, a.best_price, a.status, a.days_owned, front_gross_at_sale, back_gross_at_sale, front_gross_post_sale, back_gross_post_sale, inventory_cost, best_price
-- select a.*
from greg.uc_daily_snapshot_beta_1 a
inner join dds.dim_date b on a.the_date = b.the_date
where a.sale_date = a.the_date
  and b.year_month = 201711
  and a.store_code = 'ry1'
order by front_Gross_at_Sale  

select * from greg.uc_daily_snapshot_beta_1 where stock_number = '29720a'

