﻿-- select stocknumber from (
select d.*, best_price - cost as pot_gross
from (
select a.stocknumber, b.vin, c.year, c.make, c.model, fromts::date as date_acq, c.inpmast_vehicle_cost::integer as cost, -- a.fromts, a.thruts
  (select ads.get_current_best_price_by_stock_number_used(a.stocknumber)) as best_price
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
left join arkona.ext_inpmast c on a.stocknumber = c.inpmast_stock_number
where a.stocknumber like 'h%'
  and a.thruts::date > current_date
  and c.year is not null
  and c.inpmast_vehicle_cost > 0) d
where best_price is not null  
-- ) x group by stocknumber having count(*) > 1
 

-- all stores
select 
  case
    when left(d.stocknumber, 1) = 'G' then 'GM'
    when left (d.stocknumber, 1) = 'H' then 'Honda Nissan'
    when left(d.stocknumber, 1) = 'T' then 'Toyota'
  end as store, d.*, best_price - cost as pot_gross
from (
select a.stocknumber, b.vin, c.year, c.make, c.model, fromts::date as date_acq, c.inpmast_vehicle_cost::integer as cost, -- a.fromts, a.thruts
  (select ads.get_current_best_price_by_stock_number_used(a.stocknumber)) as best_price
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
left join arkona.ext_inpmast c on a.stocknumber = c.inpmast_stock_number
-- where a.stocknumber like 'h%'
where a.thruts::date > current_date
  and c.year is not null
  and c.inpmast_vehicle_cost > 0) d
where best_price is not null  
order by store, stocknumber