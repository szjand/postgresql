﻿drop table if exists pp.greg_new_volume cascade;
create unlogged table pp.greg_new_volume as
		select distinct b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
			a.control, a.amount,
			case when a.amount < 0 then 1 else -1 end as unit_count
		from fin.fact_gl a
		inner join dds.dim_date b on a.date_key = b.date_key
			and b.year_month between 201801 and 202212-----------------------------------------------------------------------------
		inner join fin.dim_account c on a.account_key = c.account_key
			and c.current_row
	-- add journal
		inner join fin.dim_journal aa on a.journal_key = aa.journal_key
			and aa.journal_code in ('VSN','VSU')
		inner join ( -- d: fs gm_account page/line/acct description
			select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
			from fin.fact_fs a
			inner join fin.dim_fs b on a.fs_key = b.fs_key
				and b.year_month between 201801 and 202212  ---------------------------------------------------------------------
				and b.page between 5 and 13 -- exclude pg 14 for gm - fleet 
				and (b.line between 1 and 20 or b.line between 25 and 40) -- new cars, retail only, skip fleet/internal for cars and trucks
			inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
			inner join fin.dim_account e on d.gl_account = e.account
				and e.account_type_code = '4'
				and e.current_row
			inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
			  and f.store = 'ry1') d on c.account = d.gl_account
		where a.post_status = 'Y'
union
		select distinct b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
			a.control, a.amount,
			case when a.amount < 0 then 1 else -1 end as unit_count
		from fin.fact_gl a
		inner join dds.dim_date b on a.date_key = b.date_key
			and b.year_month between 201801 and 202212-----------------------------------------------------------------------------
		inner join fin.dim_account c on a.account_key = c.account_key
			and c.current_row
	-- add journal
		inner join fin.dim_journal aa on a.journal_key = aa.journal_key
			and aa.journal_code in ('VSN','VSU')
		inner join ( -- d: fs gm_account page/line/acct description
			select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
			from fin.fact_fs a
			inner join fin.dim_fs b on a.fs_key = b.fs_key
				and b.year_month between 201801 and 202212  ---------------------------------------------------------------------
				and b.page between 5 and 15 
				and (b.line between 1 and 20 or b.line between 25 and 40) -- new cars, retail only, skip fleet/internal for cars and trucks
			inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
			inner join fin.dim_account e on d.gl_account = e.account
				and e.account_type_code = '4'
				and e.current_row
			inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
			  and f.store = 'ry2') d on c.account = d.gl_account
		where a.post_status = 'Y' ;

select * from pp.greg_new_volume limit 100		
select * from fin.dim_fs_org

select the_year, store, sum(the_count)
from (
	select left(year_month::text, 4) as the_year, year_month, store, sum(unit_count) as the_count
	from pp.greg_new_volume
	group by year_month, store) a
group by store, the_year
order by store, the_year


select store, the_year, make, count(*) as the_count
from (
	select store, left(year_month::text, 4) as the_year, 
		case 
			when store = 'ry1' and page = 5 then 'chev'
			when store = 'ry1' and page = 8 then 'buick'
			when store = 'ry1' and page = 9 then 'cad'
			when store = 'ry1' and page = 10 then 'gmc'
			when store = 'ry2' and page = 7 then 'honda'
			when store = 'ry2' and page = 14 then 'nissan'
		end as make
	from pp.greg_new_volume 
	where store = 'ry1') a
group by store, the_year, make
order by store, make, the_year

select *, sum(the_count) over (partition by store, the_year)
from (
	select store, the_year, make, count(*) as the_count
	from (
		select store, left(year_month::text, 4) as the_year, 
			case 
				when store = 'ry1' and page = 5 then 'chev'
				when store = 'ry1' and page = 8 then 'buick'
				when store = 'ry1' and page = 9 then 'cad'
				when store = 'ry1' and page = 10 then 'gmc'
				when store = 'ry2' and page = 7 then 'honda'
				when store = 'ry2' and page = 14 then 'nissan'
			end as make
		from pp.greg_new_volume 
		where store = 'ry1') a
	group by store, the_year, make
	order by store, make, the_year) b

select *
from pp.greg_new_volume 
where left(year_month::text, 4) = '2018'
  and store = 'ry1'
  and exists (
    select 1
    from pp.greg_new_volume
    where left(year_month::text, 4) = '2018' 
      and store = 'ry1'
      and unit_count < 0)
order by control      


-- fucking backons
select * 
from pp.greg_new_volume a
join (
  select control
  from pp.greg_new_volume
  group by control 
  having count(*) > 1) b on a.control = b.control
order by a.control, a.year_month


---------------------------------------------------------------------------------------------------
-- counts are good
select store_key, extract(year from boarded_date), count(*)
from (
select a.store_key, a.stock_number, a.vin, a.deal_number, a.boarded_date  
from board.sales_board a
-- left join nrv.users b on a.boarded_by = b.user_key
left join board.board_types c on a.board_type_key = c.board_type_key
  and c.board_type_key = 5
left join onedc.stores d on a.store_key = d.store_key
where  a.boarded_date between '01/01/2018' and '12/31/2022'
  and c.board_type = 'Deal'
  and not is_backed_on
  and not is_deleted
--   and a.deal_details->>'VehicleType' = 'N' deal_details didn't start until 2020
  and right(a.stock_number, 1) in ('0','1','2','3','4','5','6','7','8','9','R')
  and a.store_key in (39,40)) x
group by store_key, extract(year from boarded_date)
order by store_key, extract(year from boarded_date) 

select boarded_Date, count(*)
from board.sales_board
where board_type_key = 5
  and not is_backed_on
  and not is_deleted
  and deal_details->>'VehicleType' = 'N'
group by boarded_date
order by boarded_date

select right(trim(arkona_store_key), 1) from onedc.stores

select * from board.sales_board where boarded_date = '01/02/2018'
--9870

drop table if exists greg.horserace_new_car_data cascade;
create unlogged table greg.horserace_new_car_data as
select aa.*, bb.buyer_number, 
  case
    when right(cc.zip_code::text, 4) = '0000' then left(cc.zip_code::text, 5)
    else cc.zip_code::text
  end as zip
from (
	select d.arkona_store_key as store, a.stock_number, a.vin, a.deal_number, a.boarded_date,
		extract(year from a.boarded_date)::integer as boarded_year, e.make, e.model
	from board.sales_board a
	left join board.board_types c on a.board_type_key = c.board_type_key
		and c.board_type_key = 5
	left join onedc.stores d on a.store_key = d.store_key
	left join  arkona.ext_inpmast e on a.vin = e.inpmast_vin
	where  a.boarded_date between '01/01/2018' and '12/31/2022'
		and c.board_type = 'Deal'
		and not is_backed_on
		and not is_deleted
		and right(a.stock_number, 1) in ('0','1','2','3','4','5','6','7','8','9','R')
		and a.store_key in (39,40)) aa
left join arkona.ext_bopmast bb on aa.store = bb.bopmast_company_number
  and	aa.deal_number::integer = bb.record_key
left join  arkona.ext_bopname cc on bb.buyer_number = cc.bopname_record_key
  and cc.company_individ in ('I','C')
  and cc.bopname_company_number = 'RY1'
order by stock_number  

select * 
from greg.horserace_new_car_data;

select store, boarded_year, count(*) 
from greg.horserace_new_car_data
group by store, boarded_year
order by store, boarded_year;

select store, boarded_year, make, count(*) 
from greg.horserace_new_car_data
group by store, make, boarded_year
order by store, make, boarded_year;

select store, boarded_year, make, model, count(*) 
from greg.horserace_new_car_data
group by store, make, model, boarded_year
order by store, make, model, boarded_year;


select store, boarded_year, zip, count(*) 
from greg.horserace_new_car_data
where boarded_year in (2021, 2022)
group by store, boarded_year, zip
order by store, zip, boarded_year;