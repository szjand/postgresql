﻿/*
-- limited to car deals from 201704 -> 201706
drop table if exists tmp_recon_ros;
create temp table tmp_recon_ros as
select control as stock_number, delivery_date, doc as ro, 
--   sum(case when c.account_type = 'sale' then amount else 0 end) as sales,
--   sum(case when c.account_type = 'cogs' then amount else 0 end) as cogs,
  sum(amount) as gross
from fin.fact_gl a
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join (
  select stock_number, delivery_date
  from sls.deals
  where store_code = 'ry1'
    and vehicle_type_code = 'u'
    and year_month between 201704 and 201706
  group by stock_number, delivery_date
  having sum(unit_count) > 0  ) e on a.control = e.stock_number
where a.post_status = 'Y'
  and
    case
      when c.account in ('124000','124001','124100','124101') then d.journal_code in ('SVI','POT')
      when c.account in (
        select gl_account
        from fin.fact_fs a
        inner join fin.dim_fs_account b on a.fs_account_key = b.fs_account_key
        inner join fin.dim_fs c on a.fs_key = c.fs_key
        inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
        inner join fin.dim_account e on b.gl_account = e.account
        where c.page = 16
          and e.department_code = 'uc'
          and e.account_type_code = '5'
          and d.store = 'ry1'
        group by gl_account) then d.journal_code in ('SVI','SCA','POT')
    end
group by control, doc, delivery_date
having sum(amount) <>  0;   
create unique index on tmp_recon_ros(stock_number, ro);

drop table if exists walk_recon;
create temp table walk_recon as
select a.vehicleinventoryitemid, b.stocknumber, 
sum(case when a.typ like 'A%' or a.typ like 'P%' then totalpartsamount + laboramount else 0 end) as walk_detail,
sum(case when a.typ like 'B%' then a.totalpartsamount + a.laboramount else 0 end) as walk_body,
sum(case when a.typ like 'M%' then a.totalpartsamount + a.laboramount else 0 end) as walk_mech,
sum(a.totalpartsamount + a.laboramount) as walk_total
from ads.ext_vehicle_recon_items a
inner join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
where exists (
  select 1
  from ads.ext_authorized_recon_items
  where vehiclereconitemid = a.vehiclereconitemid)
group by a.vehicleinventoryitemid, b.stocknumber;  
create unique index on walk_recon(stocknumber);
create unique index on walk_recon(vehicleinventoryitemid);

drop table if exists eval_recon;
create temp table eval_recon as
select vehicleinventoryitemid, vehicleevaluationts::date as eval_date, sum(coalesce(reconmechanicalamount,0)+coalesce(reconbodyamount,0)+coalesce(reconappearanceamount,0)
  +coalesce(recontireamount,0)+coalesce(reconglassamount,0)+coalesce(reconinspectionamount,0)) as eval_recon
from ads.ext_vehicle_evaluations
group by vehicleinventoryitemid, vehicleevaluationts::date;
create unique index on eval_recon(vehicleinventoryitemid);


drop table if exists recon_detail;
create temp table recon_detail as
select a.stock_number, a.delivery_date, b.control as ro, department, account_type, sum(amount) as amount
from tmp_recon_ros a
inner join fin.fact_gl b on a.ro = b.control
inner join fin.dim_account c on b.account_key = c.account_key
where b.post_status = 'Y'
  and account_type in ('sale','cogs','expense')
group by a.stock_number, a.delivery_date, b.control, department, account_type; 
create unique index on recon_detail(stock_number, delivery_date, ro, department, account_type);

drop table if exists recon_by_vehicle;
create temp table recon_by_vehicle as
select stock_number, delivery_date, 
  sum(case when department = 'service' and account_type in ('sale','expense') then -amount else 0 end) as mech_sales,
  sum(case when department = 'service' and account_type in ('cogs') then amount else 0 end) as mech_cogs,
  sum(case when department = 'service' then -amount else 0 end) as mech_gross,
  sum(case when department = 'body shop' and account_type in ('sale','expense') then -amount else 0 end) as body_sales,
  sum(case when department = 'body shop' and account_type in ('cogs') then amount else 0 end) as body_cogs,
  sum(case when department = 'body shop' then -amount else 0 end) as gody_gross,
  sum(case when department = 'detail' and account_type in ('sale','expense') then -amount else 0 end) as detail_sales,
  sum(case when department = 'detail' and account_type in ('cogs') then amount else 0 end) as detail_cogs,
  sum(case when department = 'detail' then -amount else 0 end) as detail_gross,
  sum(case when department = 'parts' and account_type in ('sale','expense') then -amount else 0 end) as parts_sales,
  sum(case when department = 'parts' and account_type in ('cogs') then amount else 0 end) as parts_cogs,
  sum(case when department = 'parts' then -amount else 0 end) as parts_gross,
  sum(case when account_type in ('sale','expense') then -amount else 0 end) as total_sales,
  sum(case when account_type in ('cogs') then amount else 0 end) as total_cogs,
  sum(-amount) as total_gross
from recon_detail  
group by stock_number, delivery_date;
create unique index on recon_by_vehicle(stock_number);

*/
-- the spreadsheet
select a.stock_number, c.eval_date, a.delivery_date, eval_recon::integer as eval_total, 
  walk_mech::integer, walk_body::integer, walk_detail::integer, walk_total::integer, 
  parts_sales::integer as ark_parts, mech_sales::integer as ark_mech, 
  body_sales::integer as ark_body, detail_sales::integer as ark_detail, 
  total_sales::integer as ark_total, 
  eval_recon::integer - total_sales::integer as eval_diff,
  walk_total::integer - total_sales::integer as walk_diff, total_gross::integer as recon_gross
from recon_by_vehicle a
left join walk_recon b on a.stock_number = b.stocknumber
left join eval_recon c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
where b.stocknumber is not null
order by stock_number

where stock_number = '30568XXA'

select * from recon_detail where stock_number = '29569b'

select account_type, sum(amount) from recon_detail where stock_number = '29569b' group by account_type

select stock_number, total_sales
from recon_by_vehicle
where delivery_date between '06/01/2017' and '06/30/2017'
order by stock_number

select *
from recon_by_vehicle
where stock_number = '29678p'





