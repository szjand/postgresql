﻿-- ctp returned to inventory, but accounting not done yet
drop table if exists r_units;
create temp table r_units as
select inpmast_stock_number as stock_number, inpmast_vin as vin
from arkona.xfm_inpmast a
where right(trim(inpmast_stock_number), 1) = 'R'
  and status = 'i'
  and type_n_u = 'N'
  and current_row
  and inpmast_vehicle_cost < 10000;

select * from r_units 

-- none of these are in gmgl.daily_inventory
select * 
from r_units a 
join gmgl.daily_inventory b on a.inpmast_stock_number = b.stock_number

-- none of these are in nc.vehicle_acquisitions
select * 
from r_units a 
join nc.vehicle_acquisitions b on a.inpmast_stock_number = b.stock_number



-- vehicles with (large) balance in 127703
select b.control, sum(b.amount)
from fin.fact_gl b 
join fin.dim_account c on b.account_key = c.account_key
  and c.account = '127703'
where b.post_status = 'Y'
group by b.control
having sum(b.amount) > 10000


select *
from r_units a
left join (
  select b.control, sum(b.amount) as est_cost
  from fin.fact_gl b 
  join fin.dim_account c on b.account_key = c.account_key
    and c.account = '127703'
  where b.post_status = 'Y'
  group by b.control
  having sum(b.amount) > 10000) b on replace(a.stock_number, 'R','') = b.control

-- estimated cost of r_units based on balance in 127703 
select replace(a.inpmast_stock_number, 'R', '') as stock_number,b.*, c.*
from r_units a
left join fin.fact_gl b on replace(a.inpmast_stock_number, 'R', '') = b.control
join fin.dim_account c on b.account_key = c.account_key
  and c.account = '127703'
where b.post_status = 'Y'
order by a.inpmast_stock_number



with 
  r_units as (
    select inpmast_stock_number as stock_number, inpmast_vin as vin
    from arkona.xfm_inpmast a
    where right(trim(inpmast_stock_number), 1) = 'R'
      and status = 'i'
      and type_n_u = 'N'
      and current_row
      and inpmast_vehicle_cost < 10000),
  est_cost as ( -- vehicles with (large) balance in 127703
    select b.control, sum(b.amount) as amount
    from fin.fact_gl b 
    join fin.dim_account c on b.account_key = c.account_key
      and c.account = '127703'
    where b.post_status = 'Y'
    group by b.control
    having sum(b.amount) > 10000)
select a.stock_number, a.vin, b.amount as cost
from r_units a
left join est_cost b on replace(a.stock_number, 'R', '') = b.control;




select aa.stock_number, aa.vin, bb.amount
from (
    select inpmast_stock_number as stock_number, inpmast_vin as vin
    from arkona.xfm_inpmast a
    where right(trim(inpmast_stock_number), 1) = 'R'
      and status = 'i'
      and type_n_u = 'N'
      and current_row
      and inpmast_vehicle_cost < 10000) aa
join (
    select b.control, sum(b.amount) as amount
    from fin.fact_gl b 
    join fin.dim_account c on b.account_key = c.account_key
      and c.account = '127703'
    where b.post_status = 'Y'
    group by b.control
    having sum(b.amount) > 10000) bb on replace(aa.stock_number, 'R', '') = bb.control;    


create or replace function nc.get_r_units_missing_cost()
  returns table (
    stock_number citext,
    vin citext, 
    est_cost numeric(8,2)) as
$BODY$
/*
select * from nc.get_r_units_missing_cost()
*/
select aa.stock_number, aa.vin, bb.amount
from (
    select inpmast_stock_number as stock_number, inpmast_vin as vin
    from arkona.xfm_inpmast a
    where right(trim(inpmast_stock_number), 1) = 'R'
      and status = 'i'
      and type_n_u = 'N'
      and current_row
      and inpmast_vehicle_cost < 10000) aa
join (
    select b.control, sum(b.amount) as amount
    from fin.fact_gl b 
    join fin.dim_account c on b.account_key = c.account_key
      and c.account = '127703'
    where b.post_status = 'Y'
    group by b.control
    having sum(b.amount) > 10000) bb on replace(aa.stock_number, 'R', '') = bb.control; 
$BODY$
language sql;    

-- this syntax to get the table
select * from nc.get_r_units_missing_cost()

-- this syntax returns record
select nc.get_r_units_missing_cost()


select *
from arkona.xfm_inpmast a
join (select * from nc.get_r_units_missing_cost()) b on a.inpmast_stock_number = b.stock_number


