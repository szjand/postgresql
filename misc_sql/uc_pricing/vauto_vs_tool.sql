﻿select a.*, c.stocknumber, h.amount
from uc.vauto_prices a
left join ads.ext_vehicle_items b on a.vin = b.vin
left join ads.ext_vehicle_inventory_items c on b.vehicleitemid = c.vehicleitemid
  and c.thruts > now()
left join (
   select d.vehiclepricingid, d.vehicleinventoryitemid
   from  ads.ext_vehicle_pricings d 
   inner join ads.ext_vehicle_inventory_items dd on d.vehicleinventoryitemid = dd.vehicleinventoryitemid
     and dd.thruts > now()
   inner join (
     select e.vehicleinventoryitemid, max(e.vehiclepricingts) as vehiclepricingts
     from ads.ext_vehicle_pricings e
     group by e.vehicleinventoryitemid) f on d.vehicleinventoryitemid = f.vehicleinventoryitemid
       and d.vehiclepricingts = f.vehiclepricingts) g on c.vehicleinventoryitemid = g.vehicleinventoryitemid
left join ads.ext_vehicle_pricing_details h on g.vehiclepricingid = h.vehiclepricingid
  and h.typ = 'VehiclePricingDetail_BestPrice'
where a.lookup_date = current_date  
   and a.price <> h.amount



