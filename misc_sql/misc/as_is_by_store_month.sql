﻿drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select year_month, store, page, line, line_label, control, sum(unit_count) as unit_count
from ( -- h
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a   
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201509 and 201710
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201709
      and b.page = 16 and b.line between 1 and 6 -- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
    and aa.journal_code in ('VSN','VSU')   
    where a.post_status = 'Y' order by control) h
group by year_month, store, page, line, line_label, control
order by store, year_month, page, line;
create index on step_1(control);
create index on step_1(unit_count);


drop table if exists step_2;
create temp table step_2 as
select a.year_month, a.store, a.control, c.*
from step_1 a
left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
left join (
  select a.vehicleinventoryitemid, a.typ, a.selectedreconpackagets
  FROM ads.ext_selected_recon_packages a
  inner join (
    select vehicleinventoryitemid, max(selectedreconpackagets) as selectedreconpackagets
    FROM ads.ext_selected_recon_packages 
    where selectedreconpackagets::date > '09/01/2014'
    group by vehicleinventoryitemid) b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
      and a.selectedreconpackagets = b.selectedreconpackagets) c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
where a.unit_count = 1

select year_month, 
  sum(case when store = 'ry1' then 1 else 0 end) as gm,
  sum(case when store = 'ry2' then 1 else 0 end) as hn,
  count(*) as total
from step_2
where typ in ('ReconPackage_AsIs','ReconPackage_AsIsWS')
group by year_month
order by year_month


drop table if exists step_3;
create temp table step_3 as
select a.*, b.locationid, bb.amount
from step_1 a 
left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
left join (
  select a.vehicleinventoryitemid, a.vehiclepricingid
  from ads.ext_vehicle_pricings a
  inner join (
    select vehicleinventoryitemid, max(vehiclepricingts) as vehiclepricingts
    from ads.ext_vehicle_pricings
    where vehiclepricingts::date > '09/01/2014'
    group by vehicleinventoryitemid) b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
      and a.vehiclepricingts = b.vehiclepricingts) aa on b.vehicleinventoryitemid = aa.vehicleinventoryitemid
left join ads.ext_vehicle_pricing_details bb on aa.vehiclepricingid = bb.vehiclepricingid
  and  bb.typ = 'VehiclePricingDetail_Invoice'  
where a.unit_count = 1;

select * from step_3 where control like 'h%' limit 100

select year_month, 
  sum(case when locationid = 'B6183892-C79D-4489-A58C-B526DF948B06' then 1 else 0 end) as gm,
  sum(case when locationid = '4CD8E72C-DC39-4544-B303-954C99176671' then 1 else 0 end) as hn,
  count(*) as total
from step_3  
where amount <= 14000
group by year_month
order by year_month;
  
wait a minute, how can this be more vehicles than packages above?  some cheap cars are nice care, H9856PA, H10110A
select a.*, b.locationid, bb.amount
from step_1 a 
left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
left join (
  select a.vehicleinventoryitemid, a.vehiclepricingid
  from ads.ext_vehicle_pricings a
  inner join (
    select vehicleinventoryitemid, max(vehiclepricingts) as vehiclepricingts
    from ads.ext_vehicle_pricings
    where vehiclepricingts::date > '09/01/2014'
    group by vehicleinventoryitemid) b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
      and a.vehiclepricingts = b.vehiclepricingts) aa on b.vehicleinventoryitemid = aa.vehicleinventoryitemid
left join ads.ext_vehicle_pricing_details bb on aa.vehiclepricingid = bb.vehiclepricingid
  and  bb.typ = 'VehiclePricingDetail_Invoice'  
where a.unit_count = 1
  and year_month = 201705
  and locationid = '4CD8E72C-DC39-4544-B303-954C99176671'
  and amount <= 10000
order by amount  
