﻿select *
from fin.dim_account
where description like '%RAO%'
  and account_type in ('sale','cogs')
order by account  

select *
from ( -- sold units
  select a.control, account, case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201709 and 201710
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.department_code = 'AO'
    and c.account_type = 'sale'
  where a.post_status = 'Y' ) a 
order by a.control  

-- this is ok for avail status, but all the honda units (X stock numbers) don't show any availability
-- import locations and lets see what time at auto outlet looks like compared to avail
drop table if exists step_1;
create temp table step_1 as
select a.*, b.fromts::date as date_acq, b.thruts::date as date_sold, c.category, c.fromts::date as avail_from,
  c.thruts::date as avail_thru
from (
  select a.control, sum(-amount) as gross
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201709 and 201710
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.department_code = 'AO'
    and c.account_type in ('sale', 'cogs')
  inner join fin.dim_journal d on a.journal_key = d.journal_key
    and d.journal_code = 'VSU'    
  where a.post_status = 'Y' 
    and control <> '12865'
  group by a.control) a
left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber  
left join ads.ext_vehicle_inventory_item_Statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
  and c.status = 'RMFlagAV_Available'
order by a.control


create table jon.outlet_days (
  vehicle_inventory_item_id citext,
  from_date date,
  thru_date date); 

select vehicle_inventory_item_id, min(from_date), max(thru_date)
-- select count(*)
from jon.outlet_days
group by vehicle_inventory_item_id

select control, gross, date_acq, date_sold,
  date_sold - date_acq as days_owned,
  coalesce(avail_thru, current_date) - coalesce(avail_from, current_date) as days_avail,
  c.thru_date - c.from_date as days_at_outlet
from (  
  select control, gross,date_acq, date_sold, max(avail_from) as avail_from, max(avail_thru) as avail_thru
  from step_1  
  group by control, gross,date_acq, date_sold) a
left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
left join (
  select vehicle_inventory_item_id, min(from_date) as from_date, max(thru_date) as thru_date
  from jon.outlet_days
  group by vehicle_inventory_item_id) c on b.vehicleinventoryitemid = c.vehicle_inventory_item_id
-- where date_sold - date_acq = 0  
order by a.control

select * from uc.base_vehicles limit 100

-- and here are the source honda vehicles
select a.control, d.comment,
  trim(replace(right(substring(d.comment from 1 for position('to ' in d.comment) - 2), 8), ':', '')) as hn_stock
from step_1 a
left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
left join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
left join arkona.ext_inpcmnt d on c.vin = d.vin
  and d.comment like '%'||a.control||'%'
where right(trim(a.control), 1) = 'X'
order by a.control

--  honda days owned & available
select control, hn_Stock, hn_date_sold - hn_date_Acq as hn_days_owned, 
  coalesce(hn_avail_thru, current_date) - coalesce(hn_avail_from, current_date) as hn_days_avail
from (  
  select a.control, a.hn_stock, b.fromts::date as hn_date_acq, b.thruts::date as hn_date_sold, 
    min(c.fromts::date) as hn_avail_from, max(c.thruts::date) as hn_avail_thru
  from (
    select a.control, 
      trim(replace(right(substring(d.comment from 1 for position('to ' in d.comment) - 2), 8), ':', '')) as hn_stock
    from step_1 a
    left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
    left join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
    left join arkona.ext_inpcmnt d on c.vin = d.vin
      and d.comment like '%'||a.control||'%'
    where right(trim(a.control), 1) = 'X') a
  left join ads.ext_vehicle_inventory_items b on a.hn_stock = b.stocknumber
  left join ads.ext_vehicle_inventory_item_Statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
    and c.status = 'RMFlagAV_Available' 
  group by a.control, a.hn_stock, b.fromts::date, b.thruts::date) d 
 
-- honda outlet days -
select control, hn_stock, coalesce(hn_outlet_thru, current_date) - coalesce(hn_outlet_from, current_date) as hn_outlet_days
from (
  select aa.control, aa.hn_stock, min(c.from_date) as hn_outlet_from, max(c.thru_date) as hn_outlet_thru
  from (
    select a.control, 
      trim(replace(right(substring(d.comment from 1 for position('to ' in d.comment) - 2), 8), ':', '')) as hn_stock
    from step_1 a
    left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
    left join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
    left join arkona.ext_inpcmnt d on c.vin = d.vin
      and d.comment like '%'||a.control||'%'
    where right(trim(a.control), 1) = 'X') aa
  left join ads.ext_vehicle_inventory_items b on aa.hn_stock = b.stocknumber
  left join jon.outlet_days c on b.vehicleinventoryitemid = c.vehicle_inventory_item_id  
  group by aa.control, aa.hn_stock) cc

-- all honda
select ee.*, ff.hn_outlet_days
from (
  select control, hn_Stock, hn_date_sold - hn_date_Acq as hn_days_owned, 
    coalesce(hn_avail_thru, current_date) - coalesce(hn_avail_from, current_date) as hn_days_avail
  from (  
    select a.control, a.hn_stock, b.fromts::date as hn_date_acq, b.thruts::date as hn_date_sold, 
      min(c.fromts::date) as hn_avail_from, max(c.thruts::date) as hn_avail_thru
    from (
      select a.control, 
        trim(replace(right(substring(d.comment from 1 for position('to ' in d.comment) - 2), 8), ':', '')) as hn_stock
      from step_1 a
      left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
      left join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
      left join arkona.ext_inpcmnt d on c.vin = d.vin
        and d.comment like '%'||a.control||'%'
      where right(trim(a.control), 1) = 'X') a
    left join ads.ext_vehicle_inventory_items b on a.hn_stock = b.stocknumber
    left join ads.ext_vehicle_inventory_item_Statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
      and c.status = 'RMFlagAV_Available' 
    group by a.control, a.hn_stock, b.fromts::date, b.thruts::date) d ) ee
left join (
  select control, hn_stock, coalesce(hn_outlet_thru, current_date) - coalesce(hn_outlet_from, current_date) as hn_outlet_days
  from (
    select aa.control, aa.hn_stock, min(c.from_date) as hn_outlet_from, max(c.thru_date) as hn_outlet_thru
    from (
      select a.control, 
        trim(replace(right(substring(d.comment from 1 for position('to ' in d.comment) - 2), 8), ':', '')) as hn_stock
      from step_1 a
      left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
      left join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
      left join arkona.ext_inpcmnt d on c.vin = d.vin
        and d.comment like '%'||a.control||'%'
      where right(trim(a.control), 1) = 'X') aa
    left join ads.ext_vehicle_inventory_items b on aa.hn_stock = b.stocknumber
    left join jon.outlet_days c on b.vehicleinventoryitemid = c.vehicle_inventory_item_id  
    group by aa.control, aa.hn_stock) cc) ff on ee.hn_stock = ff.hn_stock





      
-- and all together, this is what i will send to dave
select aa.control, aa.gross, aa.days_owned, aa.days_avail, aa.days_at_outlet, 
  bb.hn_stock, bb.hn_days_owned, bb.hn_days_avail, bb.hn_outlet_days
from ( -- all w/gm dates
  select control, gross, date_acq, date_sold,
    date_sold - date_acq as days_owned,
    coalesce(avail_thru, current_date) - coalesce(avail_from, current_date) as days_avail,
    c.thru_date - c.from_date as days_at_outlet
  from (  
    select control, gross,date_acq, date_sold, max(avail_from) as avail_from, max(avail_thru) as avail_thru
    from step_1  
    group by control, gross,date_acq, date_sold) a
  left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
  left join (
    select vehicle_inventory_item_id, min(from_date) as from_date, max(thru_date) as thru_date
    from jon.outlet_days
    group by vehicle_inventory_item_id) c on b.vehicleinventoryitemid = c.vehicle_inventory_item_id) aa
left join (-- honda
  select ee.*, ff.hn_outlet_days
  from (
    select control, hn_Stock, hn_date_sold - hn_date_Acq as hn_days_owned, 
      coalesce(hn_avail_thru, current_date) - coalesce(hn_avail_from, current_date) as hn_days_avail
    from (  
      select a.control, a.hn_stock, b.fromts::date as hn_date_acq, b.thruts::date as hn_date_sold, 
        min(c.fromts::date) as hn_avail_from, max(c.thruts::date) as hn_avail_thru
      from (
        select a.control, 
          trim(replace(right(substring(d.comment from 1 for position('to ' in d.comment) - 2), 8), ':', '')) as hn_stock
        from step_1 a
        left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
        left join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
        left join arkona.ext_inpcmnt d on c.vin = d.vin
          and d.comment like '%'||a.control||'%'
        where right(trim(a.control), 1) = 'X') a
      left join ads.ext_vehicle_inventory_items b on a.hn_stock = b.stocknumber
      left join ads.ext_vehicle_inventory_item_Statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
        and c.status = 'RMFlagAV_Available' 
      group by a.control, a.hn_stock, b.fromts::date, b.thruts::date) d ) ee
  left join (
    select control, hn_stock, coalesce(hn_outlet_thru, current_date) - coalesce(hn_outlet_from, current_date) as hn_outlet_days
    from (
      select aa.control, aa.hn_stock, min(c.from_date) as hn_outlet_from, max(c.thru_date) as hn_outlet_thru
      from (
        select a.control, 
          trim(replace(right(substring(d.comment from 1 for position('to ' in d.comment) - 2), 8), ':', '')) as hn_stock
        from step_1 a
        left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
        left join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
        left join arkona.ext_inpcmnt d on c.vin = d.vin
          and d.comment like '%'||a.control||'%'
        where right(trim(a.control), 1) = 'X') aa
      left join ads.ext_vehicle_inventory_items b on aa.hn_stock = b.stocknumber
      left join jon.outlet_days c on b.vehicleinventoryitemid = c.vehicle_inventory_item_id  
      group by aa.control, aa.hn_stock) cc) ff on ee.hn_stock = ff.hn_stock) bb on aa.control = bb.control  
order by hn_stock desc 
