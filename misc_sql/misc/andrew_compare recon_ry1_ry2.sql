﻿drop table if exists sales cascade;
create temp table sales as
SELECT case when left(b.stocknumber, 1) = 'H' then 'Honda' else 'GM' end as store,
  a.VehicleInventoryItemID, b.stocknumber, c.vin, soldts::date AS sale_date, 
  case d.typ
    when 'ReconPackage_Nice' then 'Nice Care'
    when 'ReconPackage_Factory' then 'Certified'
    else 'As Is/As Is WS'
  end as package
FROM ads.ext_vehicle_sales a
INNER JOIN ads.ext_Vehicle_Inventory_Items b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN ads.ext_Vehicle_Items c on b.VehicleItemID = c.VehicleItemID 
INNER JOIN ads.ext_Selected_Recon_Packages d on a.VehicleInventoryItemID = d.VehicleInventoryItemID 
  AND d.SelectedReconPackageTS = (
    SELECT MAX(SelectedReconPackageTS)
	FROM ads.ext_Selected_Recon_Packages 
	WHERE VehicleInventoryItemID = d.VehicleInventoryItemID)
--  AND d.typ IN ('ReconPackage_Nice','ReconPackage_Factory')
WHERE a.typ = 'VehicleSale_Retail'
  AND extract(year from a.soldts) = 2018
  AND a.status = 'VehicleSale_Sold';
  
create index on sales(VehicleInventoryItemID);

create index on ads.ext_vehicle_Recon_Items(VehicleInventoryItemID);
create index on ads.ext_vehicle_Recon_Items(typ);
create index on ads.ext_vehicle_Recon_Items(VehicleReconItemID);
create index on ads.ext_Authorized_Recon_Items(VehicleReconItemID);
create index on ads.ext_Authorized_Recon_Items(typ);


drop table if exists all_amounts;
create temp table all_amounts as
select aa.*, ee.insp, bb.amt as removed, cc.amt as not_auth, dd.amt as due_bill
from sales aa
left join (
  SELECT v.VehicleInventoryItemID, sum(v.TotalPartsAmount + v.LaborAmount) AS amt, 'Removed'
  -- select *
  FROM ads.ext_vehicle_Recon_Items v
  INNER JOIN ads.ext_Authorized_Recon_Items a ON v.VehicleReconItemID = a.VehicleReconItemID
  WHERE v.typ like 'Mechanical%'
    AND a.typ = 'AuthorizedReconItem_Removed'
  group by v.VehicleInventoryItemID) bb on aa.VehicleInventoryItemID = bb.VehicleInventoryItemID
left join (
  SELECT v.VehicleInventoryItemID, sum(v.TotalPartsAmount + v.LaborAmount) AS amt, 'Not Auth'
  -- select *
  FROM ads.ext_vehicle_Recon_Items v
  left JOIN ads.ext_Authorized_Recon_Items a ON v.VehicleReconItemID = a.VehicleReconItemID
  WHERE v.typ like 'Mechanical%'
    AND a.VehicleReconItemID IS NULL
  group by v.VehicleInventoryItemID) cc on aa.VehicleInventoryItemID = cc.VehicleInventoryItemID
left join (
  SELECT v.VehicleInventoryItemID, sum(v.TotalPartsAmount + v.LaborAmount) AS amt, 'Due Bill'
  -- select *
  FROM ads.ext_vehicle_Recon_Items v
  INNER JOIN ads.ext_Authorized_Recon_Items a ON v.VehicleReconItemID = a.VehicleReconItemID
  WHERE v.typ like 'Mechanical%'
    AND a.typ = 'AuthorizedReconItem_TransferredToDueBill'
  group by v.VehicleInventoryItemID) dd on aa.VehicleInventoryItemID = dd.VehicleInventoryItemID   
left join (
  select a.VehicleInventoryItemID, sum(b.TotalPartsAmount + b.LaborAmount) as insp
  from sales a
  join ads.ext_vehicle_Recon_Items b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
    and b.typ like 'Mechanical%'
    and b.basistable = 'VehicleInspections'
    and b.typ <> 'MechanicalReconItem_Inspection'
  group by a.VehicleInventoryItemID) ee on aa.VehicleInventoryItemID = ee.VehicleInventoryItemID;  


select a.*, b.year_month
from all_amounts a
join dds.dim_date b on a.sale_date = b.the_date
where coalesce(a.insp, 0) + coalesce(a.removed, 0) + coalesce(a.not_auth, 0) + coalesce(a.due_bill, 0) <> 0


select a.store, b.year_month, a.package, count(*) as units, sum(insp) as inspection, 
  sum(removed) as removed, sum(not_auth) as not_auth, sum(due_bill) as due_bill
from all_amounts a
join dds.dim_date b on a.sale_date = b.the_date
where coalesce(a.insp, 0) + coalesce(a.removed, 0) + coalesce(a.not_auth, 0) + coalesce(a.due_bill, 0) <> 0
group by a.store, b.year_month, a.package
order by a.store, b.year_month, a.package


select store, the_month, package, round(inspection/units, 0) as recommended, round((removed + not_auth + due_bill)/units, 0) as not_done
from (
  select a.store, left(month_name, 3) as the_month, a.package, count(*) as units, sum(insp) as inspection, 
    sum(coalesce(removed, 0)) as removed, sum(coalesce(not_auth, 0)) as not_auth, sum(coalesce(due_bill, 0)) as due_bill,
    b.year_month
  from all_amounts a
  join dds.dim_date b on a.sale_date = b.the_date
  where coalesce(a.insp, 0) + coalesce(a.removed, 0) + coalesce(a.not_auth, 0) + coalesce(a.due_bill, 0) <> 0
  group by a.store, b.month_name, a.package, b.year_month
  order by a.store, b.year_month, a.package) x


select * from dds.dim_date limit 10
