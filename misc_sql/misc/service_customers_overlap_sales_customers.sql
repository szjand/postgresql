﻿
/*
4/23/19
greg wants to know percentage of service customers who are not sales customers
*/

drop table if exists ros cascade;
create temp table ros as
select b.the_date, c.vin, d.bnkey, d.fullname
from ads.ext_fact_repair_order a
join dds.dim_date b on a.finalclosedatekey = b.date_key
  and b.the_date between current_date - 365 and current_date
join ads.ext_dim_Vehicle c on a.vehiclekey = c.vehiclekey
  and c.vin not like '00%'
  and length(c.vin) = 17
join ads.ext_dim_customer d on a.customerkey = d.customerkey
  and d.fullname not in ('inventory','ADESA','SHOP TIME','CARRY IN','BODY SHOP TIME ADJ','CARRY IN, ANY')
  and d.fullname not like '%RYDELL%'
group by b.the_date, c.vin, d.bnkey, d.fullname
order by d.fullname;

create index on ros(vin);
create index on ros(bnkey);
create index on ros(fullname);

select fullname, count(*), min(the_date), max(the_Date)
from ros
group by fullname
order by count(*) desc;

drop table if exists ros_1 cascade;
create temp table ros_1 as
select bnkey, fullname, vin, min(the_date), max(the_date)
from ros
group by bnkey, fullname, vin
order by count(*) desc;
create unique index on ros_1(fullname, bnkey, vin);
create index on ros_1(vin);
create index on ros_1(bnkey);
create index on ros_1(fullname);

drop table if exists deals cascade;
create temp table deals as
select record_key, bopmast_vin, bopmast_stock_number, buyer_number, co_buyer_number, bopmast_search_name, date_capped
from arkona.xfm_bopmast 
where current_row 
  and record_status = 'U'
  and bopmast_search_name not in ('ADESA','RYDELL AUTO CENTER','T. PARKS','MIDSTATE AUTO AUCTION','RYDELL',
    'SCHWAN, JERRY','SCHWAN, LAVERN MICHAEL','MIDSTATE AUTO ACTION','GELCO CORPORATION','MID STATE AUCTION',
    'HANSEN INVESTMENTS INC.','ADESA FARGO','FLAGSTAFF MOTOR COMPANY','RYDELL NISSAN OF GRAND FORKS',
    'ADESA, FARGO','GELCO FLEET TRUST','RYDELL AUTO CENTER INC','NCR','RYDELL, WESLEY DAVID',
    'HONDA NISSAN OF GRAND FORKS','VOLKSWAGEN OF INVER GROVE','3DESA','NATIONAL','NISSAN HONDA','MID STATE AUTO AUCTION');
create index on deals(bopmast_vin);
create index on deals(buyer_number);
create index on deals(bopmast_search_name);  

select * 
from ros_1 a
left join deals b on a.vin = b.bopmast_vin

select *
from ros_1
where vin in (
select vin
from ros_1
group by vin
having count(*) > 1)
order by vin


select bopmast_Search_name, count(*)
from deals
group by bopmast_Search_name
order by count(*) desc 



select count(distinct vin) -- 34339
from ros_1 a

select 17922/34339.0  -- 0.52191385887766096858

select count(*) --17922
from ros_1 a
where exists (
  select 1
  from deals
  where bopmast_vin = a.vin)

-- only 8 vins not in inpmast
select count(*)
from ros_1 a
where not exists (
  select 1
  from arkona.xfm_inpmast
  where inpmast_vin = a.vin
    and current_row)
  

select *
from ros_1 a
where not exists (
  select 1
  from deals
  where bopmast_vin = a.vin)

  


