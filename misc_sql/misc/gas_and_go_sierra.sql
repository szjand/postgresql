﻿select b.year, b.make, b.model, a.vin, a.stock_number, a.delivery_date, c.bopname_search_name, c.address_1, c.city, c.state_code,c.zip_code, c.phone_number
from sls.deals a 
inner join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
left join arkona.xfm_bopname c on a.buyer_bopname_id = c.bopname_record_key  
where a.delivery_date between current_date - 90 and current_date
  and a.vehicle_type_code = 'N'
  and b.make = 'GMC'
  and b.model like 'sier%'
  and b.body_style like '%den%'
  and b.color like '%white%'