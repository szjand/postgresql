﻿-- 1/15/19 redo RY1 using name join on bopname instead of company

-- sales accounts for main shop and quick lube
drop table if exists service_accounts_ry1;
create temp table service_accounts_ry1 as
select distinct e.account
-- select sum(a.amount)
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201712 and 201811
  and b.page = 16 
  and b.line between 21 and 34
  and b.col in (1,2)
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
  and c.store = 'ry1'
join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
join fin.dim_Account e on d.gl_account = e.account
  and e.department_code in ('SD','QL')
  and e.description not like '%INT%';

-- sales accounts for main shop and quick lube
drop table if exists service_accounts_ry2;
create temp table service_accounts_ry2 as
select distinct e.account
-- select sum(a.amount)
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201712 and 201811
  and b.page = 16 
  and b.line between 21 and 34
  and b.col in (1,2)
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
  and c.store = 'ry2'
join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
join fin.dim_Account e on d.gl_account = e.account
  and e.department_code in ('SD','QL')
  and e.description not like '%INT%';
  
drop table if exists sales_accounts_ry1; -- retail only
create temp table sales_accounts_ry1 as
select distinct e.account
-- select page, sum(a.amount)
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201712 and 201811
  and (
    (b.page in (5, 8, 9, 10) and b.line between 1 and 40) 
    or
    (b.page = 16 and b.line between 1 and 7) 
    or
    (b.page = 17 and b.line between 1 and 20))
  and b.col = 1
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
  and c.store = 'ry1'
join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
join fin.dim_Account e on d.gl_account = e.account;

drop table if exists sales_accounts_ry2; -- retail only
create temp table sales_accounts_ry2 as
select distinct e.account
-- select page, sum(a.amount)
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201712 and 201811
  and (
    (b.page in (5, 8, 9, 10) and b.line between 1 and 40) 
    or
    (b.page = 16 and b.line between 1 and 7) 
    or
    (b.page = 17 and b.line between 1 and 20))
  and b.col = 1
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
  and c.store = 'ry2'
join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
join fin.dim_Account e on d.gl_account = e.account;

  
-- group by page
  
drop table if exists service_sales_ry1;
create temp table service_sales_ry1 as
select a.control, sum(-amount) as sales
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month between 201712 and 201811
join fin.dim_Account c on a.account_key = c.account_key
join service_accounts_ry1 d on c.account = d.account
where a.post_status = 'Y' 
group by a.control ;

drop table if exists service_sales_ry2;
create temp table service_sales_ry2 as
select a.control, sum(-amount) as sales
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month between 201712 and 201811
join fin.dim_Account c on a.account_key = c.account_key
join service_accounts_ry2 d on c.account = d.account
where a.post_status = 'Y' 
group by a.control ;

drop table if exists sales_sales_ry1;
create temp table sales_sales_ry1 as
select a.control, sum(-amount) as sales
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month between 201712 and 201811
join fin.dim_Account c on a.account_key = c.account_key
join sales_accounts_ry1 d on c.account = d.account
where a.post_status = 'Y' 
group by a.control ;

drop table if exists sales_sales_ry2;
create temp table sales_sales_ry2 as
select a.control, sum(-amount) as sales
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month between 201712 and 201811
join fin.dim_Account c on a.account_key = c.account_key
join sales_accounts_ry2 d on c.account = d.account
where a.post_status = 'Y' 
group by a.control ;


-- drop table if exists service_zip;
-- create temp table service_zip as
-- select a.ro, coalesce(c.zip,'none') as zip
-- from ads.ext_fact_repair_order a
-- join service_sales b on a.ro = b.control
-- join ads.ext_dim_customer c on a.customerkey = c.customerkey
-- group by a.ro, c.zip;

drop table if exists service_zip_ry1;
create temp table service_zip_ry1 as
select a.ro_number, coalesce(b.zip_code::citext, 'none') as zip_code
from arkona.ext_sdprhdr a -- limit 10
join service_sales_ry1 aa on a.ro_number = aa.control
join arkona.xfm_bopname b on a.customer_key = b.bopname_record_key
--   and a.company_number = b.bopname_company_number
  and a.cust_name = b.bopname_search_name
  and b.current_row;

drop table if exists service_zip_ry2;
-- something goofy going on with joining bopname to sprhdr by company
create temp table service_zip_ry2 as
select a.ro_number, coalesce(b.zip_code::citext, 'none') as zip_code
-- select a.ro_un
from arkona.ext_sdprhdr a -- limit 10
join service_sales_ry2 aa on a.ro_number = aa.control
join arkona.xfm_bopname b on a.customer_key = b.bopname_record_key
--   and a.company_number = b.bopname_company_number
  and a.cust_name = b.bopname_search_name
  and b.current_row;  


drop table if exists sales_zip_ry1;
create temp table sales_zip_ry1 as
select a.control, coalesce(b.zip_code::citext, 'none') as zip_code
from sales_sales_ry1 a
join sls.ext_Bopmast_partial aa on a.control = aa.stock_number
join arkona.xfm_bopname b on aa.buyer_bopname_id = b.bopname_record_key
  and aa.store = b.bopname_company_number
  and b.current_row

drop table if exists sales_zip_ry2;
create temp table sales_zip_ry2 as
select a.control, coalesce(b.zip_code::citext, 'none') as zip_code
from sales_sales_ry2 a
join sls.ext_Bopmast_partial aa on a.control = aa.stock_number
join arkona.xfm_bopname b on aa.buyer_bopname_id = b.bopname_record_key
  and aa.store = b.bopname_company_number
  and b.current_row  

select zip_code, sum(sales)::integer as sales, count(*) as ros
from service_sales_ry1 a
join service_zip_ry1 b on a.control = b.ro_number
group by zip_code
order by sum(sales) desc

select zip_code, sum(sales)::integer as sales, count(*) as ros
from service_sales_ry2 a
join service_zip_ry2 b on a.control = b.ro_number
group by zip_code
order by sum(sales) desc


-- drop table if exists top_zip_ros;
-- create temp table top_zip_ros as
-- select ro_number
-- from service_zip d
-- join (
--   select c.*, left(zip_code, 5) as zip_5
--   from (
--     select zip_code, sum(sales)::integer as sales, count(*) as ros
--     from service_sales a
--     join service_zip b on a.control = b.ro_number
--     group by zip_code
--     order by sum(sales) desc) c
--   where zip_code <> '0'
--     and ros > 100) e on left(d.zip_code, 5) = e.zip_5;
-- 
-- drop table if exists unique_vins;
-- create temp table unique_vins as
-- select b.vin, c.year, c.make
-- from top_zip_ros a
-- join arkona.ext_sdprhdr b on a.ro_number = b.ro_number
-- join arkona.xfm_inpmast c on b.vin = c.inpmast_vin
-- group by b.vin, c.year, c.make


drop table if exists zip_vin_ry1;
create temp table zip_vin_ry1 as  
select ro_number, zip_5, vin, make
from (
  select a.ro_number, left(a.zip_code, 5) as zip_5, b.vin, 
    case
      when c.make like 'cad%' then 'CADILLAC'
      when c.make like 'chev%' then 'CHEVROLET'
      when c.make like 'GMC%' then 'GMC'
      when c.make like 'sierra%' then 'GMC'
      else c.make
    end as make
  from service_zip_ry1 a
  join arkona.ext_Sdprhdr b on a.ro_number = b.ro_number
  join arkona.xfm_inpmast c on b.vin = c.inpmast_vin
    and c.current_row
    and c.year between 2008 and 2019
  where a.zip_code <> '0') d 
group by ro_number, zip_5, vin, make;



truncate afton.service_unique_vin_in_zip   ;
insert into afton.service_unique_vin_in_zip   
select zip_5::integer, vin, 'ry1' as store,
  max(case
    when make = 'honda' then 'honda'
    when make = 'nissan' then 'nissan'
    when make in ('chevrolet','gmc','buick','cadillac','pontiac','saturn','oldsmobile') then 'gm'
    else 'other'
  end) as make
from zip_vin_ry1
where zip_5 in (
  select left(zip_code, 5) as zip_5
  from (
    select zip_code, sum(sales)::integer as sales, count(*) as ros
    from service_sales_ry1 a
    join service_zip_ry1 b on a.control = b.ro_number
    group by zip_code
    order by sum(sales) desc) c
  where zip_code <> '0'
    and ros > 100)
group by zip_5, vin


drop table if exists zip_vin_ry2;
create temp table zip_vin_ry2 as  
select ro_number, zip_5, vin, make
from (
  select a.ro_number, left(a.zip_code, 5) as zip_5, b.vin, 
    case
      when c.make like 'cad%' then 'CADILLAC'
      when c.make like 'chev%' then 'CHEVROLET'
      when c.make like 'GMC%' then 'GMC'
      when c.make like 'sierra%' then 'GMC'
      else c.make
    end as make
  from service_zip_ry2 a
  join arkona.ext_Sdprhdr b on a.ro_number = b.ro_number
  join arkona.xfm_inpmast c on b.vin = c.inpmast_vin
    and c.current_row
    and c.year between 2008 and 2019
  where a.zip_code <> '0') d 
group by ro_number, zip_5, vin, make;


insert into afton.service_unique_vin_in_zip   
select zip_5::integer, vin, 'ry2' as store,
  max(case
    when make = 'honda' then 'honda'
    when make = 'nissan' then 'nissan'
    when make in ('chevrolet','gmc','buick','cadillac','pontiac','saturn','oldsmobile') then 'gm'
    else 'other'
  end) as make
from zip_vin_ry2
where zip_5 in (
  select left(zip_code, 5) as zip_5
  from (
    select zip_code, sum(sales)::integer as sales, count(*) as ros
    from service_sales_ry2 a
    join service_zip_ry2 b on a.control = b.ro_number
    group by zip_code
    order by sum(sales) desc) c
  where zip_code <> '0'
    and ros > 50)
group by zip_5, vin


select store, make, zip, count(*)
from afton.service_unique_vin_in_zip
group by store, make, zip
order by count(*) desc 


-- 1/17/19
-- new request: any non internal ro for calendary year 2018 (all depts) vehicle 2008 or newer, unique store, vin, zip


drop table if exists service_accounts;
create temp table service_accounts as
select distinct c.store, e.account
-- select sum(a.amount)
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.the_year = 2018
  and b.page = 16 
  and b.line between 21 and 34
  and b.col in (1,2)
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
--   and c.store = 'ry1'
join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
join fin.dim_Account e on d.gl_account = e.account
--   and e.department_code in ('SD','QL')
  and e.description not like '%INT%';

  
drop table if exists service_sales;
create temp table service_sales as
select d.store, a.control, sum(-amount) as sales
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.the_year = 2018
join fin.dim_Account c on a.account_key = c.account_key
join service_accounts d on c.account = d.account
where a.post_status = 'Y' 
group by d.store, a.control ;


drop table if exists service_zip_vin;
create temp table service_zip_vin as
select aa.store, coalesce(left(b.zip_code::citext,5), 'none') as zip_code, a.vin
from arkona.ext_sdprhdr a -- limit 10
join service_sales aa on a.ro_number = aa.control
join arkona.xfm_bopname b on a.customer_key = b.bopname_record_key
--   and a.company_number = b.bopname_company_number
  and a.cust_name = b.bopname_search_name
  and b.zip_code <> 0
  and b.current_row;  

create index on service_zip_vin(vin);
create index on service_zip_vin(zip_code);
create index on service_zip_vin(store);

-- 1/17 this is what i sent to greg
select store, zip_code, vin
from service_zip_vin
-- where store = 'ry1'
-- where store = 'ry2'
group by store, zip_code, vin
order by zip_code desc 





