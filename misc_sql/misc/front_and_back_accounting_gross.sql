﻿
  select distinct case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code,
    a.fxmpge as the_page, a.fxmlne as line, trim(b.g_l_acct_number) as gl_account
  from arkona.ext_eisglobal_sypffxmst a
  inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
    and a.fxmcyy = b.factory_financial_year
  where a.fxmcyy = 2018
    and coalesce(b. consolidation_grp, '1') <> '3'
    and b.g_l_acct_number <> ''
    and (
      (a.fxmpge between 5 and 15) or
      (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
      (a.fxmpge = 17 and a.fxmlne between 1 and 19))


do
$$
declare 
  _stock_number citext := '33073';
begin
drop table if exists wtf;
create temp table wtf as
select 
  coalesce((sum(-a.amount) filter (where c.the_page < 17))::integer, 0) as front_gross,
  coalesce((sum(-a.amount) filter (where c.the_page = 17))::integer, 0) as back_gross,
  coalesce((sum(-a.amount))::integer, 0) as total_gross
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
inner join (
  select distinct case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code,
    a.fxmpge as the_page, a.fxmlne as line, trim(b.g_l_acct_number) as gl_account
  from arkona.ext_eisglobal_sypffxmst a
  inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
    and a.fxmcyy = b.factory_financial_year
  where a.fxmcyy = 2018
    and coalesce(b. consolidation_grp, '1') <> '3'
    and b.g_l_acct_number <> ''
    and (
      (a.fxmpge between 5 and 15) or
      (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
      (a.fxmpge = 17 and a.fxmlne between 1 and 19))) c on b.account = c.gl_account
where a.post_status = 'Y'      
  and a.control = _stock_number;
end
$$;      
select * from wtf;




select a.*, b.account_type, c.*, sum(amount) over (partition by the_page, account_type), sum(amount) over (partition by the_page)
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
inner join (
  select distinct case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code,
    a.fxmpge as the_page, a.fxmlne as line, trim(b.g_l_acct_number) as gl_account
  from arkona.ext_eisglobal_sypffxmst a
  inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
    and a.fxmcyy = b.factory_financial_year
  where a.fxmcyy = 2018
    and coalesce(b. consolidation_grp, '1') <> '3'
    and b.g_l_acct_number <> ''
    and (
      (a.fxmpge between 5 and 15) or
      (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
      (a.fxmpge = 17 and a.fxmlne between 1 and 19))) c on b.account = c.gl_account
where a.post_status = 'Y'      
  and a.control = '32983a'
order by the_page, account_type








  