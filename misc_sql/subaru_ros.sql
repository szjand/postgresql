﻿select count(distinct vin)
-- select ro, vin, array_agg(distinct d.payment_type_code)
from dds.fact_repair_order a
join dds.dim_vehicle b on a. vehicle_key = b.vehicle_key
  and b.make = 'subaru'
join dds.dim_service_type c on a.service_type_key = c.service_type_key
  and c.service_type_code = 'bs'  
join dds.dim_payment_type d on a.payment_type_key = d.payment_type_key  
where open_date > '12/31/2019'
group by ro, vin
