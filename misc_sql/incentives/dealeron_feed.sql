﻿-- based on FUNCTION gmgl.update_incentive_cash()
-- based on FUNCTION gmgl.update_incentive_cash()
-- as started in ...misc_sql/afton/update_incentive_cash().sql
-- goal is to produce a detailed list of incentives for dealeron, in the format defined by dealeron, 
-- to be in compliance with GMs AIS

-- had to add 2020 to gmgl.custom_incentive_rule_distributions
-- insert into gmgl.custom_incentive_rule_distributions values (6,2020,'Cadillac');

 with 
  _current_inventory as ( -- all we need is the vin of current inventory
    select inpmast_vin as vin, make, year
    from arkona.xfm_inpmast a
    where a.current_row = true
      and a.status = 'I'
      and a.type_n_u = 'N'
      and a.make in ('BUICK','CHEVROLET','GMC','CADILLAC')),
  _custom_rules as ( -- cadillac 4%, removed gmgl.clean_gm_vehicles
    select distinct rule_name, case when math_operator = '*' then the_value * second_value::numeric end as custom_incentive, vin
    from (
      select d.rule_name,
        case
          when first_value_type = 'MSRP' 
        then total_msrp 
        end as the_value , math_operator, second_value, a.vin
      from _current_inventory a
--       inner join gmgl.clean_gm_vehicles b on a.vin = b.vin
      inner join gmgl.custom_incentive_rule_distributions c on a.make = c.make and a.year = c.model_year
      inner join gmgl.custom_incentive_rules d on d.rule_key = c.rule_key
      inner join gmgl.vehicle_invoices e on a.vin = e.vin and e.thru_date > now() ) A),
  _programs as (
    select distinct
      a.program_start_date,a.program_end_date,a.major_incentive_key,gg.vin, --a.program_pdf,
      coalesce(a.program_dollar_amount,0) as major_program_dollar_amount,a.program_number as major_program_number,
      a.program_name as major_program_name, coalesce(c.program_dollar_amount,0) as stackable_dollar_amount, 
      c.program_number as stackable_program,c.program_name as stackable_program_name, c.color, 
      ember_id,
      case 
        when a.has_sup_inc = true 
      then d.supinc 
        else 0 end as major_sup_inc,
      case 
        when c.has_sup_inc = true 
      then d.supinc 
        else 0 end as stackable_sup_inc,
            case 
        when a.has_emp_inc = true 
      then d.empinc 
        else 0 end as major_emp_inc,
      case 
        when c.has_emp_inc = true 
      then d.empinc 
        else 0 end as stackable_emp_inc,
        is_best_incentive, 
        a.has_sup_inc as has_major_sup_inc, a.has_emp_inc as has_major_emp_inc,
        c.has_emp_inc as has_stackable_emp_inc,
        c.has_sup_inc as has_stackable_sup_inc, 
      coalesce((select custom_incentive from _custom_rules zz where zz.vin = gg.vin),0) as cadillac_incentive
    from _current_inventory gg
    inner join ( -- major
      select a.*, program_name, program_number, has_sup_inc, has_emp_inc, program_start_date, program_end_date,program_pdf
      from gmgl.major_incentives a
      inner join gmgl.incentive_programs b on a.program_key  = b.program_key
      inner join gmgl.incentive_program_rules c on b.program_key = c.program_key and current_row = true
      where lookup_date = current_date) A on gg.vin = a.vin
    left join ( -- stackable
      select c.*,program_name, program_number, has_sup_inc,has_emp_inc, program_start_date, program_end_date,program_pdf
      from gmgl.stackable_incentives c 
      inner join gmgl.incentive_programs d on c.program_key = d.program_key
      inner join gmgl.incentive_program_rules e on d.program_key = e.program_key 
        and current_row = true
      where lookup_date = current_date) c on a.major_incentive_key = c.major_incentive_key
    inner join gmgl.vehicle_invoices d on gg.vin = d.vin and thru_date > now()
    where gg.vin is not null)      

select * from _programs
where vin = 'KL4CJASB4KB939524'



select vin,major_program_dollar_amount,stackable_dollar_amount,major_sup_inc,stackable_sup_inc,0
  major_emp_inc,stackable_emp_inc,cadillac_incentive
from _programs      
where major_program_dollar_amount <> 0
  or stackable_dollar_amount <> 0
  or major_sup_inc <> 0
  or stackable_sup_inc <> 0
  or major_emp_inc <> 0
  or stackable_emp_inc <> 0
  or cadillac_incentive <> 0 
order by vin  


select *
from gmgl.major_incentives
where vin = '1G1BE5SM0K7142746'
  and lookup_date = current_date


select * 
from gmgl.vin_incentive_cash
where vin = '1G1BE5SM0K7142746'
order by thru_ts desc 


select * 
from gmgl.incentive_programs
where program_key = 1457


for dealeron, the desired format for each vehicle: (all on one row)
3 fields (comma separated): rebates,vin,dealerid
|Name:Consumer Cash Program_Price:2500_Category:Global_Disclaimer:TBD
|Name:Chevrolet Bonus Cash Program_Price:250_Category:Global_Disclaimer:TBD
|Name:Rydell Discount_Price:993_Category:Global_Disclaimer:TBD|,1GCPYFED5KZ425671,5663 

i want to eliminate programs with 0 amount

need to generate Global/??? (black/red)

for what i am doing whether the program is major or stackable does not matter
they are all just programs applicable to a vehicle

select * 
from gmgl.vin_incentives_json 
where vin = '1G1BE5SM0K7142746'
  and lookup_date = current_Date - 1

-- no fucking indexes
create index on gmgl.vin_incentives_json(vin);  
create index on gmgl.vin_incentives_json(lookup_date);
create index on gmgl.stackable_incentives(major_incentive_key);
create index on gmgl.stackable_incentives(lookup_date);



select a.vin,c.thru_ts, a.program_dollar_amount, b.program_name, b.program_number, c.has_sup_inc, c.has_emp_inc, d.major_json->>'contractInd'
from gmgl.major_incentives a
inner join gmgl.incentive_programs b on a.program_key  = b.program_key
inner join gmgl.incentive_program_rules c on b.program_key = c.program_key and current_row = true
left join gmgl.vin_incentives_json d on a.vin = d.vin
  and d.lookup_date = current_date 
  and b.program_number = d.major_json->>'programId'
where a.lookup_date = current_date 
  and a.vin = 'KL4CJASB4KB939524'
  and a.vin = '1G1BE5SM0K7142746'

'KL4CJASB4KB939524' is returning 3 majors  




  select c.program_start_date::timestamp with time zone,c.program_end_date::timestamp with time zone,a.major_incentive_key as id,a.vin as vehicle, c.program_pdf,c.program_number,
    c.program_name, b.is_best_incentive, total_cash,total_stackable as stackable_cash, 
    (total_incentives - cadillac_four_percent) as total_incentives, total_major, sup_inc as supinc, emp_inc as empinc,
    case 
      when sup_inc > 0 
        then true 
      else false 
    end as has_sup_inc,
        case 
      when emp_inc > 0 
        then true 
      else false 
    end as has_emp_inc
  from gmgl.vin_incentive_cash b
  inner join gmgl.major_incentives a on b.major_incentive_key = a.major_incentive_key
  inner join gmgl.incentive_programs c on a.program_key  = c.program_key
  where a.vin ='KL4CJASB4KB939524'
    and b.thru_ts > now()

select * from gmgl.vin_incentive_cash where vin ='KL4CJASB4KB939524' and thru_ts > now() and is_best_incentive

so, this all tells me, for major, need to start with  gmgl.vin_incentive_cash to get the current major


select * from gmgl.vin_incentive_cash where vin ='1G1BE5SM0K7142746' and thru_ts > now() and is_best_incentive

-- stackables
-- from get_vehicles()

 _stackable as (
  select b.ember_id, major_incentive_key,acknowledged
  from gmgl.incentive_program_rules a
  inner join gmgl.stackable_incentives b on a.program_key = b.program_key
    and lookup_date = current_date
  inner join gmgl.incentive_programs d on a.program_key = d.program_key
  inner join _major c on b.major_incentive_key = c.id
  where current_row = true
  group by ember_id, major_incentive_key,d.acknowledged
  order by d.acknowledged ),


select * from gmgl.incentive_program_rules order by program_key -- program_key, from/thru_ts, current_row, has_sup, has_emp

select* from gmgl.incentive_programs where program_number = '19-40GB' order by lookup_ts desc 

select max(snapshot_date) from gmgl.daily_stackable_snapshot -- not used

-------------


i have no idea how the concept of Include Major Rebate is incorporated
per afton, that what controls what goes into incentive_program_rules

-------------

1GCPYCEF3KZ303326

-- working from smart draw model (gm_incentives.sdr)

-- work backwards from gmgl.vin_incentive_cash, that appears to be a consistent
--    representation of the current best incentive for current inventory
--    use it to find the attributes i need: program name, amount, global/
-- don't care where sup/emp inc comes from, like cad 4%, applies to a vehicle or doesn't

this approach assumes the correctness of the process & ETL that generates gmgl.vin_incentive_cash

-- vin is unique
-- 353 rows
select *
from gmgl.vin_incentive_cash a
where a.thru_ts > now()
  and a.is_best_incentive

-- major
-- current rule excludes 18 vehicles
select a.vin, a.total_cash, a.total_incentives, a.total_major, 
  a.total_stackable, a.sup_inc, a.emp_inc, a.cadillac_four_percent, 
  b.program_dollar_amount,
  c.program_key, c.program_number, c.program_name, d.major_json->>'contractInd'
from gmgl.vin_incentive_cash a
join gmgl.major_incentives b on a.major_incentive_key = b.major_incentive_key
join gmgl.incentive_programs c on b.program_key = c.program_key
join gmgl.incentive_program_rules cc on c.program_key = cc.program_key -- only programs for which "rule" is current
left join gmgl.vin_incentives_json d on a.vin = d.vin
  and d.lookup_date = current_date 
  and c.program_number = d.major_json->>'programId'
where a.thru_ts > now()
  and a.is_best_incentive
--   and a.vin = '1GCPYCEF3KZ303326'
order by a.vin 


-- 11/17/19
-- noticed there was a lot of xxxxxxxxxxx for availability, found the determining factor,
-- d.major_json->>'contractInd', was sometimes lowercase, sometimes uppercase
drop table if exists majors;
create temp table majors as

drop table if exists ifd.majors cascade;
create table ifd.majors (
  the_date date not null,
  vin citext not null,
  program_number citext not null,
  revision_number citext not null,
  program_name citext not null,
  program_dollar_amount integer not null,
  availability citext not null,
  primary key (the_date,vin));
comment on table ifd.majors is 'current day major incentives for dealeron feed, initial assumptions is
  one major per vehicle';  

insert into ifd.majors  
select current_date, a.vin, c.program_number, c.revision_number, c.program_name, b.program_dollar_amount, 
  case
    when d.major_json->>'contractInd' in ('false','FALSE') then 'Global'
    when d.major_json->>'contractInd' in ('true','TRUE') then 'Limited'
    else 'xxxxxxxxxxx'
  end as availability -- , d.major_json->>'contractInd'
from gmgl.vin_incentive_cash a
join gmgl.major_incentives b on a.major_incentive_key = b.major_incentive_key
join gmgl.incentive_programs c on b.program_key = c.program_key
join gmgl.incentive_program_rules cc on c.program_key = cc.program_key -- only programs for which "rule" is current
left join gmgl.vin_incentives_json d on a.vin = d.vin
  and d.lookup_date = current_date 
  and c.program_number = d.major_json->>'programId'
where a.thru_ts > now()
  and a.is_best_incentive
  and b.program_dollar_amount <> 0;

-- test for limited or missing availability
select * from ifd.majors where availability <> 'Global'

select the_date, vin
from ifd.majors
group by the_date, vin
having count(*) > 1

-- stackable
-- coincidentally, returns 353 rows
-- need full query for majors, need to include $0 majors with non $0 stackables
select a.vin, a.total_cash, a.total_incentives, a.total_major, 
  a.total_stackable, a.sup_inc, a.emp_inc, a.cadillac_four_percent, 
  b.program_dollar_amount,
  c.program_number, c.revision_number::citext, c.program_name, 
  d.program_dollar_amount,
  e.program_number, e.program_name,
  h.stack ->> 'contractInd',
  h.stack ->> 'programId'
from gmgl.vin_incentive_cash a
join gmgl.major_incentives b on a.major_incentive_key = b.major_incentive_key
join gmgl.incentive_programs c on b.program_key = c.program_key
join gmgl.incentive_program_rules cc on c.program_key = cc.program_key -- only programs for which "rule" is current
join gmgl.stackable_incentives d on a.major_incentive_key = d.major_incentive_key
  and d.lookup_date = current_date
  and d.program_dollar_amount <> 0 -- filter out 0 amount programs
JOIN gmgl.incentive_programs e on d.program_key = e.program_key
join gmgl.incentive_program_rules f on e.program_key = f.program_key -- only programs for which "rule" is current
left join gmgl.vin_incentives_json g on a.vin = g.vin
  and g.lookup_date = current_date
  and c.program_number = g.major_json->>'programId'
left join json_array_elements(g.stackable_json) h(stack) on true
  and e.program_number = (h.stack ->> 'programId')::citext
where a.thru_ts > now()
  and a.is_best_incentive
order by c.program_number


-- 11/17/19
-- noticed there was a lot of xxxxxxxxxxx for availability, found the determining factor,
-- h.stack->>'contractInd', was sometimes lowercase, sometimes uppercase
drop table if exists stackables;
-- only returns row after incentives update in the morning
create temp table stackables as

drop table if exists ifd.stackables cascade;
create table ifd.stackables (
  the_date date not null,
  vin citext not null,
  program_number citext not null,
  revision_number citext not null,
  program_name citext not null,
  program_dollar_amount integer not null,
  availability citext not null);
comment on table ifd.stackables is 'current day stackable incentives for dealeron feed';  
-- dont yet know what the primary key is
insert into ifd.stackables
select current_date, a.vin, e.program_number, e.revision_number, e.program_name, 
  d.program_dollar_amount,
  case
    when h.stack ->> 'contractInd' in ('false','FALSE') then 'Global'
    when h.stack ->> 'contractInd' in ('true','TRUE') then 'Limited'
    else 'xxxxxxxxxxx'
  end as availability
from gmgl.vin_incentive_cash a
join gmgl.major_incentives b on a.major_incentive_key = b.major_incentive_key
join gmgl.incentive_programs c on b.program_key = c.program_key
join gmgl.incentive_program_rules cc on c.program_key = cc.program_key -- only programs for which "rule" is current
join gmgl.stackable_incentives d on a.major_incentive_key = d.major_incentive_key
  and d.lookup_date = current_date
  and d.program_dollar_amount <> 0 -- filter out 0 amount programs
JOIN gmgl.incentive_programs e on d.program_key = e.program_key
join gmgl.incentive_program_rules f on e.program_key = f.program_key -- only programs for which "rule" is current
left join gmgl.vin_incentives_json g on a.vin = g.vin
  and g.lookup_date = current_date
  and c.program_number = g.major_json->>'programId'
left join json_array_elements(g.stackable_json) h(stack) on true
  and e.program_number = (h.stack ->> 'programId')::citext
where a.thru_ts > now()
  and a.is_best_incentive


select max(the_date) from ifd.stackables
  

  
select the_date, vin, program_number, revision_number
from ifd.stackables
group by the_date, vin, program_number, revision_number
having count(*) > 1

select *
from ifd.stackables
where vin = '3GTU9DED2KG311325'
  and program_number = '19-40AEU'

-- test for limited or missing availability
select program_number, revision_number, program_name, program_dollar_amount , array_agg(vin)
from stackables where availability <> 'Global'
group by program_number, revision_number, program_name, program_dollar_amount 


drop table if exists rydell;
create temp table rydell as

drop table if exists ifd.rydell_discount cascade;
create table ifd.rydell_discount (
  the_date date not null,
  vin citext not null,
  program_number citext not null,
  revision_number citext not null,
  program_name citext not null,
  program_dollar_amount integer not null,
  availability citext not null,
  primary key(the_date,vin));
comment on table ifd.rydell_discount is 'the sum of emp_inc, sup_inc and cad 4% all called the rydell discount for dealeron feed';  
insert into ifd.rydell_discount
select current_date, vin, '0'::citext as program_number, 0::integer as revision_number, 
  'Rydell Discount'::citext as program_name, 
  sup_inc + emp_inc + cadillac_four_percent as program_dollar_amount, 'Global'::citext as availability
from gmgl.vin_incentive_cash
where thru_ts > now()
  and is_best_incentive
  and sup_inc + emp_inc + cadillac_four_percent <> 0;

select * from rydell

drop table if exists all_incentives;
create temp table all_incentives as
select 'rydell' as source, a.* from ifd.rydell_discount a
union
select 'major', a.* from ifd.majors a
union 
select 'stackable', a.* from ifd.stackables a;




-- program_number/revision_number unique
select program_number, revision_number
from gmgl.incentive_programs
group by program_number, revision_number
having count(*) > 1;
  
create unique index on gmgl.incentive_programs(program_number, revision_number);

can not create a fk reference to gmgl.incentive_programs for the rydell discount,
therefore, they will have to be added separately, not as part of all_incentives
-- from misc_sql/incentives/cdk_disclaimers_for_dealeron
drop table if exists gmgl.ext_incentive_discaimers_from_cdk;
create table gmgl.ext_incentive_discaimers_from_cdk (
  program_number citext not null,
  revision_number integer not null, 
  disclaimer citext,
  primary key(program_number,revision_number),
  foreign key(program_number,revision_number) references gmgl.incentive_programs(program_number,revision_number));
comment on table gmgl.ext_incentive_discaimers_from_cdk is 'hopefully a temporary repository of gm incentive
  disclaimers scraped for our CDK site, rydellautocenter.com, for feeding to dealeron until they get their
  shit together with GM';

-- don't need to include the program name here as part of the disclaimer
insert into gmgl.ext_incentive_discaimers_from_cdk values('19-40AEC',0,'Take Retail Delivery By 10/31/2019. Excludes L model. Some customers may not qualify. Not available with special financing, lease and some other offers. See dealer for details.');
insert into gmgl.ext_incentive_discaimers_from_cdk values('19-40AEF',2,'Must qualify through GM Financial. Not available with special finance, lease, or some other offers. Take delivery by 10-31-2019. See dealer for details.');
insert into gmgl.ext_incentive_discaimers_from_cdk values('19-40CCT',2,'Take Retail Delivery By 10/31/2019 - Not Compatible With Some Other Offers. Must show proof of current Lease or ownership of a 2005 model year or newer GM vehicle at least 30 days prior to the new vehicle sale. Not available with special financing, lease and some other offers.See dealer for details.');
insert into gmgl.ext_incentive_discaimers_from_cdk values('19-40CO',13,'Take Retail Delivery By 10/31/2019 - Not Compatible With Some Other Offers. Must show proof of current Lease or ownership of a 2005 model year or newer GM vehicle at least 30 days prior to the new vehicle sale. Not available with special financing, lease and some other offers.See dealer for details.');
insert into gmgl.ext_incentive_discaimers_from_cdk values('19-40CCO',1,'Take Retail Delivery By 10/31/2019 - Not Compatible With Some Other Offers. Must show proof of current Lease or ownership of a 2005 model year or newer GM vehicle at least 30 days prior to the new vehicle sale. Not available with special financing, lease and some other offers.See dealer for details.');

11/17/19
insert into gmgl.ext_incentive_discaimers_from_cdk values('19-40CO',14, 'Must show proof of current Lease or ownership of a 2005 model year or newer GM vehicle at least 30 days prior to the new vehicle sale. Not available with special financing, lease and some other offers.See dealer for details.');
insert into gmgl.ext_incentive_discaimers_from_cdk values('19-40AEL',1, 'Excludes L model. Some customers may not qualify. Not available with special financing, lease and some other offers. See dealer for details.');
insert into gmgl.ext_incentive_discaimers_from_cdk values('19-40CCZ',3, 'THIS IS A GENERAL MOTORS LOYALTY INCENTIVE FOR CURRENT OWNERS/LESSEES OF A 2005 OR NEWER GM PASSENGER CAR OR TRUCK WHO PURCHASE ONE OF THE ELIGIBLE NEW AND UNUSED MODELS');
insert into gmgl.ext_incentive_discaimers_from_cdk values('19-40CCO',2, 'Must show proof of current Lease or ownership of a 2005 model year or newer GM vehicle at least 30 days prior to the new vehicle sale. Not available with special financing, lease and some other offers.See dealer for details.');

12/3/19
insert into gmgl.ext_incentive_discaimers_from_cdk values('19-40AFB',0,'Must qualify through GM Financial. Not available with special finance, lease, or some other offers. Take delivery by 01-02-2020. See dealer for details.');
insert into gmgl.ext_incentive_discaimers_from_cdk values('19-40CO',15, 'Must show proof of current Lease or ownership of a 2005 model year or newer GM vehicle at least 30 days prior to the new vehicle sale. Not available with special financing, lease and some other offers.See dealer for details.');
insert into gmgl.ext_incentive_discaimers_from_cdk values('19-40CCO',3, 'Must show proof of current Lease or ownership of a 2005 model year or newer GM vehicle at least 30 days prior to the new vehicle sale. Not available with special financing, lease and some other offers.See dealer for details.');

1/6/2020
insert into gmgl.ext_incentive_discaimers_from_cdk values('20-35C', 0, 'Must show proof of current Lease or ownership of a 2006 model year or newer GM vehicle at least 30 days prior to the new vehicle sale. Not available with special financing, lease and some other offers.See dealer for details.');

insert into gmgl.ext_incentive_discaimers_from_cdk values('20-40AH', 0, 'Excludes L model. Some customers may not qualify. Not available with special financing, lease and some other offers. See dealer for details.');

insert into gmgl.ext_incentive_discaimers_from_cdk values('20-40CF', 0, 'Must show proof of current Lease or ownership of a 2006 model year or newer GM vehicle at least 30 days prior to the new vehicle sale. Not available with special financing, lease and some other offers.See dealer for details.');

2/5/2020
insert into gmgl.ext_incentive_discaimers_from_cdk values('20-35C', 1, 'Must show proof of current Lease or ownership of a 2006 model year or newer GM vehicle at least 30 days prior to the new vehicle sale. Not available with special financing, lease and some other offers.See dealer for details.');

insert into gmgl.ext_incentive_discaimers_from_cdk values('20-40AN', 0,'Must qualify through GM Financial. Not available with special finance, lease, or some other offers. Take delivery by 03-02-2020. See dealer for details.');

insert into gmgl.ext_incentive_discaimers_from_cdk values('20-40CF', 1, 'Program Not available with some other offers. Take delivery by 03-02-2020.');

09/01/2020
insert into gmgl.ext_incentive_discaimers_from_cdk values('20-40CB', 0, 'Must show proof of current Lease or ownership of a 2006 model year or newer GM vehicle at least 30 days prior to the new vehicle sale. Not available with special financing, lease and some other offers. Residency restrictions apply.');

10/01/2020
insert into gmgl.ext_incentive_discaimers_from_cdk values('20-40CBC', 1, 'Must show proof of current Lease or ownership of a 2006 model year or newer Chevrolet, Buick, GMC, or Cadillac vehicle at least 30 days prior to the new vehicle sale. Not available with special financing and some other offers. Residency restrictions apply.');



select * 
from gmgl.ext_incentive_discaimers_from_cdk
update gmgl.ext_incentive_discaimers_from_cdk
set program_number = '20-40CBC'
where program_number = '20-40CB'

-- this is what was sent to dealeron on 10/16
drop table if exists wtf;
create temp table wtf as
-- 
drop table if exists ifd.dealeron_feed;
create table ifd.dealeron_feed (
  rebates citext not null,  
  vin citext not null primary key,
  dealerid citext not null);
insert into ifd.dealeron_feed
select '|'||string_agg('Name:'||program_name||'_Price:'||program_dollar_amount::text||'_Category:'||availability||
  '_Disclaimer:'||
  case availability 
    when 'Global' then 'Available to everyone' 
    else disclaimer 
  end, '|')
  ||'|'||
  max(coalesce(
    case
      when bb.vin is not null then rd 
    end, '')) as rebates,  
  aa.vin, '5663'::text as dealerid
from (  
  select vin, program_name, program_dollar_amount, availability, disclaimer  
  from ifd.majors a
  left join gmgl.ext_incentive_discaimers_from_cdk b on a.program_number = b.program_number
    and a.revision_number::integer = b.revision_number
  where program_name not like '%Cadillac%'
  union
  select vin, program_name, program_dollar_amount, availability, disclaimer
  from ifd.stackables a
  left join gmgl.ext_incentive_discaimers_from_cdk b on a.program_number = b.program_number
    and a.revision_number::integer = b.revision_number
  where program_name not like '%Cadillac%') aa 
left join (
  select vin, 'Name:Rydell Discount'||'_Price:'||program_dollar_amount::text||'_Category:Global'||'_Disclaimer:Available to everyone|' as rd
  from ifd.rydell_discount) bb on  aa.vin = bb.vin 
group by aa.vin order by aa.vin



-- missing disclaimer on stackable
assert 
select count(*)
from (
  select 'stackable', vin, program_name, a.program_number, a.revision_number, program_dollar_amount, availability, disclaimer
  from ifd.stackables a
  left join gmgl.ext_incentive_discaimers_from_cdk b on a.program_number = b.program_number
    and a.revision_number::integer = b.revision_number
  where availability <> 'Global'    
    and program_name not like 'Cad%'
    and (
      disclaimer is null
      or
      disclaimer like 'x%')
  union
  select 'major', vin, program_name, a.program_number, a.revision_number, program_dollar_amount, availability, disclaimer
  from ifd.majors a
  left join gmgl.ext_incentive_discaimers_from_cdk b on a.program_number = b.program_number
    and a.revision_number::integer = b.revision_number
  where availability <> 'Global'    
    and program_name not like 'Cad%'
    and (
      disclaimer is null
      or
      disclaimer like 'x%')) x



    

-- incorporate the addition of the date
select '|'||string_agg('Name:'||program_name||'_Price:'||program_dollar_amount::text||'_Category:'||availability||
  '_Disclaimer:'||
  case availability 
    when 'Global' then 'Available to everyone' 
    else disclaimer 
  end, '|')
  ||'|'||
  max(coalesce(
    case
      when bb.vin is not null then rd 
    end, '')) as rebates,  
  aa.vin, '5663'::text as dealerid
from (  
  select vin, program_name, program_dollar_amount, availability, disclaimer  
  from ifd.majors a
  left join gmgl.ext_incentive_discaimers_from_cdk b on a.program_number = b.program_number
    and a.revision_number::integer = b.revision_number
  where program_name not like '%Cadillac%'
    and the_date = '11/20/2019'
  union
  select vin, program_name, program_dollar_amount, availability, disclaimer
  from ifd.stackables a
  left join gmgl.ext_incentive_discaimers_from_cdk b on a.program_number = b.program_number
    and a.revision_number::integer = b.revision_number
  where program_name not like '%Cadillac%'
    and the_date = '11/20/2019') aa 
left join (
  select vin, 'Name:Rydell Discount'||'_Price:'||program_dollar_amount::text||'_Category:Global'||'_Disclaimer:Available to everyone|' as rd
  from ifd.rydell_discount
  where the_date = '11/20/2019') bb on  aa.vin = bb.vin 
group by aa.vin order by aa.vin



-- creating a python script to do this shit
-- fuck history, this is temporary anyway
-- truncate and fill the tables
-- populate the feed table or just create the feed file
-- test for missing disclaimers
-- ftp to dealeron

create or replace function ifd.create_dealeron_feed()
returns void as
$BODY$
/*
  truncates and populates tables:
    ifd.majors
    ifd.stackables
    ifd.rydell_discount
    ifd.dealeron_feed
  tests for missing disclaimer
  tests for at least 200 vehicles in feed
  select ifd.create_dealeron_feed();
*/
begin

  truncate ifd.majors;
  insert into ifd.majors  
  select current_date, a.vin, c.program_number, c.revision_number, c.program_name, b.program_dollar_amount, 
    case
      when d.major_json->>'contractInd' in ('false','FALSE') then 'Global'
      when d.major_json->>'contractInd' in ('true','TRUE') then 'Limited'
      else 'xxxxxxxxxxx'
    end as availability -- , d.major_json->>'contractInd'
  from gmgl.vin_incentive_cash a
  join gmgl.major_incentives b on a.major_incentive_key = b.major_incentive_key
  join gmgl.incentive_programs c on b.program_key = c.program_key
  join gmgl.incentive_program_rules cc on c.program_key = cc.program_key -- only programs for which "rule" is current
  left join gmgl.vin_incentives_json d on a.vin = d.vin
    and d.lookup_date = current_date 
    and c.program_number = d.major_json->>'programId'
  where a.thru_ts > now()
    and a.is_best_incentive
    and b.program_dollar_amount <> 0;

  truncate ifd.stackables;
  insert into ifd.stackables
  select current_date, a.vin, e.program_number, e.revision_number, e.program_name, 
    d.program_dollar_amount,
    case
      when h.stack ->> 'contractInd' in ('false','FALSE') then 'Global'
      when h.stack ->> 'contractInd' in ('true','TRUE') then 'Limited'
      else 'xxxxxxxxxxx'
    end as availability
  from gmgl.vin_incentive_cash a
  join gmgl.major_incentives b on a.major_incentive_key = b.major_incentive_key
  join gmgl.incentive_programs c on b.program_key = c.program_key
  join gmgl.incentive_program_rules cc on c.program_key = cc.program_key -- only programs for which "rule" is current
  join gmgl.stackable_incentives d on a.major_incentive_key = d.major_incentive_key
    and d.lookup_date = current_date
    and d.program_dollar_amount <> 0 -- filter out 0 amount programs
  JOIN gmgl.incentive_programs e on d.program_key = e.program_key
  join gmgl.incentive_program_rules f on e.program_key = f.program_key -- only programs for which "rule" is current
  left join gmgl.vin_incentives_json g on a.vin = g.vin
    and g.lookup_date = current_date
    and c.program_number = g.major_json->>'programId'
  left join json_array_elements(g.stackable_json) h(stack) on true
    and e.program_number = (h.stack ->> 'programId')::citext
  where a.thru_ts > now()
    and a.is_best_incentive;

  truncate ifd.rydell_discount;
  insert into ifd.rydell_discount
  select current_date, vin, '0'::citext as program_number, 0::integer as revision_number, 
    'Rydell Discount'::citext as program_name, 
    sup_inc + emp_inc + cadillac_four_percent as program_dollar_amount, 'Global'::citext as availability
  from gmgl.vin_incentive_cash
  where thru_ts > now()
    and is_best_incentive
    and sup_inc + emp_inc + cadillac_four_percent <> 0;

  truncate ifd.dealeron_feed;
  insert into ifd.dealeron_feed
  select '|'||string_agg('Name:'||program_name||'_Price:'||program_dollar_amount::text||'_Category:'||availability||
    '_Disclaimer:'||
    case availability 
      when 'Global' then 'Available to everyone' 
      else disclaimer 
    end, '|')
    ||'|'||
    max(coalesce(
      case
        when bb.vin is not null then rd 
      end, '')) as rebates,  
    aa.vin, '5663'::text as dealerid
  from (  
    select vin, program_name, program_dollar_amount, availability, disclaimer  
    from ifd.majors a
    left join gmgl.ext_incentive_discaimers_from_cdk b on a.program_number = b.program_number
      and a.revision_number::integer = b.revision_number
    where program_name not like '%Cadillac%'
    union
    select vin, program_name, program_dollar_amount, availability, disclaimer
    from ifd.stackables a
    left join gmgl.ext_incentive_discaimers_from_cdk b on a.program_number = b.program_number
      and a.revision_number::integer = b.revision_number
    where program_name not like '%Cadillac%') aa 
  left join (
    select vin, 'Name:Rydell Discount'||'_Price:'||program_dollar_amount::text||'_Category:Global'||'_Disclaimer:Available to everyone|' as rd
    from ifd.rydell_discount) bb on  aa.vin = bb.vin 
  group by aa.vin order by aa.vin;

  -- test for empty disclaimers

  assert (
    select count(*)
    from (
      select 'stackable', vin, program_name, a.program_number, a.revision_number, program_dollar_amount, availability, disclaimer
      from ifd.stackables a
      left join gmgl.ext_incentive_discaimers_from_cdk b on a.program_number = b.program_number
        and a.revision_number::integer = b.revision_number
      where availability <> 'Global'    
        and program_name not like 'Cad%'
        and (
          disclaimer is null
          or
          disclaimer like 'x%')
      union
      select 'major', vin, program_name, a.program_number, a.revision_number, program_dollar_amount, availability, disclaimer
      from ifd.majors a
      left join gmgl.ext_incentive_discaimers_from_cdk b on a.program_number = b.program_number
        and a.revision_number::integer = b.revision_number
      where availability <> 'Global'    
        and program_name not like 'Cad%'
        and (
          disclaimer is null
          or
          disclaimer like 'x%')) x) = 0, 'missing disclaimer';    

  assert (
    select count(*)
    from ifd.dealeron_feed) > 200, 'not enough records in ifd.dealeron_feed';
    
end          
$BODY$
  LANGUAGE plpgsql;
comment on function ifd.create_dealeron_feed() is 'based on script in .../misc_sql/incentives/dealeron_feed.sql';

select * from ifd.dealeron_feed