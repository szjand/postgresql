﻿select *
from gmgl.major_incentives
where lookup_date = current_Date - 1

select count(distinct vin)  --413
from gmgl.major_incentives
where lookup_date = current_Date - 1

select vin, count(*)
from gmgl.major_incentives 
where lookup_date = current_Date - 1
group by vin

drop table if exists inc_vins;
create temp table inc_vins as
select vin
from gmgl.major_incentives 
where lookup_date = current_Date - 1
group by vin ;


-- chrome style id , yikes, multiple per vin
select a.vin, r.style ->'attributes'->>'id'
from inc_vins a
left join chr.describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'style') as r(style) on true
order by a.vin

select jsonb_pretty(response) from chr.describe_vehicle where vin = '1GC4YUEY3LF110252'

select *
from gmgl.major_incentives
where lookup_date = current_Date - 1
  and vin = '1GCUYEED7KZ424521'

select * from gmgl.incentive_programs where program_key in (1245,1273)

-- this gives me one row for vin
drop table if exists vin_desc cascade;
create temp table vin_desc as
select a.vin,
  b.response -> 'attributes' ->> 'modelYear' as model_year,
  b.response -> 'attributes' ->> 'bestMakeName' as make,
  b.response -> 'attributes' ->> 'bestTrimName' as trim_level,
  b.response -> 'attributes' ->> 'bestModelName' as model,
  b.response -> 'attributes' ->> 'bestStyleName' as style_name
from inc_vins a
left join chr.describe_vehicle b on a.vin = b.vin;
create unique index on vin_desc(vin);


select *
from vin_desc a
left join (
  select vin, count(*), array_agg(program_key)
  from gmgl.major_incentives 
  where lookup_date = current_Date - 1
  group by vin) b on a.vin = b.vin
order by a.model_year, a.make, a.model, a.trim_level, a.style_name

-- 108 distinct model/trim, compared to 413 vins
select model_year, make, trim_level, model, style_name, count(*)
from vin_desc a
left join (
  select vin, count(*), array_agg(program_key)
  from gmgl.major_incentives 
  where lookup_date = current_Date - 1
  group by vin) b on a.vin = b.vin
group by model_year, make, trim_level, model, style_name  
order by model_year, make, trim_level, model, style_name  