﻿drop table if exists name_1;
create temp table name_1 as
select a.bopname_company_number, a.bopname_Record_key,a.bopname_search_name, a.phone_number, 
  a.zip_code,a.bopname_key, a.row_from_Date, a.row_thru_date,
  row_number() over (partition by bopname_company_number, bopname_record_key order by bopname_key)
from arkona.xfm_bopname a
order by bopname_company_number, bopname_record_key;

select * 
from name_1
where row_from_date = current_date -1 
  and row_number = 1