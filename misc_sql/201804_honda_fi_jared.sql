﻿5/1/18
jared is interested is seeing fi gross by deal type, fi mgr

select *
from sls.deals_gross_by_month
where year_month = 201804


select * 
from sls.deals_by_month
where stock_number = 'h11028'

select sale_type , count(*)
from sls.deals_by_month
group by sale_type

select deal_type_code, count(*)
from sls.deals
-- where stock_number = 'h11028'
group by deal_type_code



select a.*  -- 105
from sls.deals_by_month a
where a.store_code = 'ry2'
  and a.year_month = 201804
  and a.fi_last_name in ('calcaterra','lizakowski')
order by a.stock_number  

select a.stock_number, a.unit_count, a.fi_last_name, b.deal_type_code, c.fi_gross
from sls.deals_by_month a
left join sls.deals b on a.stock_number = b.stock_number
  and a.bopmast_id = b.bopmast_id
  and a.year_month = b.year_month
left join sls.deals_Gross_by_month c on a.stock_number = c.control  
where a.store_code = 'ry2'
  and a.year_month = 201804
  and a.fi_last_name in ('calcaterra','lizakowski')
order by a.stock_number  


select a.stock_number, a.fi_last_name, b.deal_type_code, sum(c.fi_gross) as fi_gross
from sls.deals_by_month a
left join sls.deals b on a.stock_number = b.stock_number
  and a.bopmast_id = b.bopmast_id
  and a.year_month = b.year_month
left join sls.deals_Gross_by_month c on a.stock_number = c.control  
where a.store_code = 'ry2'
  and a.year_month = 201804
  and a.fi_last_name in ('calcaterra','lizakowski')
group by a.stock_number, a.fi_last_name, b.deal_type_code  
order by a.stock_number  


select fi_last_name, deal_type_code, sum(fi_gross) as fi_gross
from (
  select a.stock_number, a.fi_last_name, b.deal_type_code, sum(c.fi_gross) as fi_gross
  from sls.deals_by_month a
  left join sls.deals b on a.stock_number = b.stock_number
    and a.bopmast_id = b.bopmast_id
    and a.year_month = b.year_month
  left join sls.deals_Gross_by_month c on a.stock_number = c.control  
  where a.store_code = 'ry2'
    and a.year_month = 201804
    and a.fi_last_name in ('calcaterra','lizakowski')
  group by a.stock_number, a.fi_last_name, b.deal_type_code ) d
group by fi_last_name, deal_type_code
