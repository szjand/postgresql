﻿drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select store, page, line, line_label, control, sum(unit_count) as unit_count
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 202207-----------------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 202207  ---------------------------------------------------------------------
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
--         or
--         (b.page = 17 and b.line between 1 and 20))-- f/i counts don't make sense
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by store, page, line, line_label, control
order by store, page, line;

select * from step_1

G44666
fs: sales: 55680, cos: 53091  gross: 2589
deal summary: price: 55680 cost: 54985.06
deal recap: price: 55680  cost: 53350.51  holdback 1634.55  total cost: 54985.06

G43223P
fs: sales:27599  cos: 28967  gross: -1368
deal summary: price: 27599  cos: 29009.26
deal recap: price 27599  cost 29235.26  adds to cost -226  total cost 29009.26

select * -- sum(-a.amount)
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
where b.year_month = 202207
  and a.post_status = 'Y'
  and page in (5)
  and line between 25 and 40
  and c.store = 'ry1'

select a.control, c.journal_code, c.journal, sum(a.amount), array_agg(distinct b.account) 
from fin.fact_gl a
join fin.dim_Account b on a.account_key = b.account_key
join fin.dim_journal c on a.journal_key = c.journal_key
where a.control = 'G43223P'
  and a.post_status = 'Y'
group by a.control, c.journal_code, c.journal

select a.control, b.account, b.department_code, b.account_type, c.journal_code, c.journal, sum(a.amount)
from fin.fact_gl a
join fin.dim_Account b on a.account_key = b.account_key
join fin.dim_journal c on a.journal_key = c.journal_key
where a.control = 'G43223P'
  and a.post_status = 'Y'
group by a.control, b.account, b.department_code, b.account_type, c.journal_code, c.journal

select a.control, b.department_code, b.account_type, c.journal_code, c.journal, sum(a.amount), array_agg(distinct b.account), b.description
from fin.fact_gl a
join fin.dim_Account b on a.account_key = b.account_key
join fin.dim_journal c on a.journal_key = c.journal_key
where a.control = 'G43223P'
  and a.post_status = 'Y'
group by a.control, b.department_code, b.account_type, c.journal_code, c.journal, b.description
-- order by journal_code, account_type
order by b.account_type

select * from fin.dim_Account