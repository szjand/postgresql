﻿drop table if exists jon.wf_state_context;
drop table if exists jon.wf_state_hierarchy;
drop table if exists jon.wf_state_type;
drop table if exists jon.wf_level_type;
create table jon.wf_level_type (
  wf_level_type_key text primary key,
  description text not null,
  from_date date not null,
  thru_date date not null default '12/31/9999',
  alt_sequence integer);
comment on table jon.wf_level_type is 'Whether PROCESS, STATE, or OUTCOME. May optionally be QUALIFIER.';
  
insert into jon.wf_level_type (wf_level_type_key,description,from_date)values
('process','high level workflow process',current_date), 
('state','a state in the process',current_date), 
('outcome','how a state ends, its outcome',current_date), 
('qualifier','an optional, more detail qulaifier for an outcome',current_date);

-- although this is called state_type, to me it is more like workflow_items, some are states, some 
-- are outcomes, etc all determined by the FK from wf_level_type
create table jon.wf_state_type (
  wf_state_type_key text primary key,
  wf_level_type_key text not null references jon.wf_level_type(wf_level_type_key),
  from_date date not null,
  thru_date date not null default '12/31/9999',
  alt_sequence integer);
create index on jon.wf_state_type(wf_level_type_key);  
comment on table jon.wf_state_type is 'Workflow State Types are grouped into three or four levels: 
  1. The major, mutually exclusive, PROCESS. This level is never associated with the entity to be managed.
  2. The STATE of the entity to manage within the PROCESS. A STATE can be open-ended, but it must always end with an OUTCOME.
  3. The OUTCOME of the STATE. Only outcomes may have Workflow State Option(s).
There exists an optional fourth level:
  4. The QUALIFIER for an OUTCOME. Gives more detail regarding the OUTCOME.';

insert into jon.wf_state_type (wf_state_type_key,wf_level_type_key, from_date) values
('passed','outcome',current_date),
('failed','outcome',current_date),
('accepted','outcome',current_date),
('declined','outcome',current_date),
('candidate_cancelled','outcome',current_date),
('employer_cancelled','outcome',current_date),
('rejected','outcome',current_date),
('employer_withdrawn','outcome',current_date),
('no_show','outcome',current_date),
('hired','outcome',current_date),
('not_hired','outcome',current_date),
('application_received','state',current_date),
('application_review','state',current_date),
('invited_to_interview','state',current_date),
('interview','state',current_date),
('test_aptitude','state',current_date),
('seek_references','state',current_date),
('make_offer','state',current_date),
('application_closed','state',current_date),
('standard_job_application','process',current_date),
('technical_job_application','process',current_date);

create or replace function jon.wf_is_state(_state text)
returns boolean as
$$
  select 
    case
      when (
        select wf_level_type_key
        from jon.wf_state_type
        where wf_state_type_key = _state) = 'state' then true
      else false
    end;
$$
language sql;

create or replace function jon.wf_is_outcome(_state text)
returns boolean as
$$
  select 
    case
      when (
        select wf_level_type_key
        from jon.wf_state_type
        where wf_state_type_key = _state) = 'outcome' then true
      else false
    end;
$$
language sql;

create table jon.wf_state_hierarchy (
  state_key citext not null references jon.wf_state_type(wf_state_type_key) check(jon.wf_is_state(state_key) = true),
  outcome_key citext not null references jon.wf_state_type(wf_state_type_key) check(jon.wf_is_outcome(outcome_key) = true),
  alt_sequence integer,
  constraint wf_state_hierarchy_pkey primary key(state_key,outcome_key));

insert into jon.wf_state_hierarchy(state_key,outcome_key) values
('application_received','accepted'),
('application_received','rejected'),
('application_received','passed'),
('application_received','failed'),
('invited_to_interview','accepted'),
('invited_to_interview','declined'),
('interview','passed'),
('interview','failed'),
('interview','candidate_cancelled'),
('interview','no_show'),
('make_offer','accepted'),
('make_offer','declined'),
('seek_references','passed'),
('seek_references','failed'),
('application_closed','hired'),
('application_closed','not_hired'),
('test_aptitude','passed'),
('test_aptitude','failed') ;


create or replace function jon.wf_is_process(_state text)
returns boolean as
$$
  select 
    case
      when (
        select wf_level_type_key
        from jon.wf_state_type
        where wf_state_type_key = _state) = 'process' then true
      else false
    end;
$$
language sql;
-- 
-- what ties states to a process, is it context?
-- 
create table jon.wf_state_context (
  process_key citext not null references jon.wf_state_type(wf_state_type_key) check (jon.wf_is_process(process_key) = true),
  state_key citext not null references jon.wf_state_hierarchy(state_key),
  outcome_key citext not null references jon.wf_state_hierarchy(outcome_key),
  child_disabled boolean not null default 'FALSE',
  constraint wf_state_context_pkey primary key(process_key,state_key,outcome_key));

-- where are application_received.passed & application_received.rejected used?
insert into jon.wf_state_context(process_key,state_key,outcome_key) values
('standard_job_application'.'',''),
('technical_job_application','','')








insert into 
select a.wf_level_type_key, b.wf_state_type_key
from jon.wf_level_type a
left join jon.wf_state_type b on a.wf_level_Type_key = b.wf_level_type_key



options

for each state/outcome combination there or 0 or more possible subsequent states (options)


 



