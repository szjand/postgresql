﻿select *
from nc.vehicle_acquisitions a
left join arkona.ext_inpvinv b on a.vin = b.vin
where a.thru_date > current_date


select a.inpmast_stock_number, a.date_in_invent, a.make, a.model, 
  b.year, b.model_code, b.date_ordered, b.list_price, b.vehicle_cost, b.model_desc, b.stock_number,
  count(*) over (),
  count(*) over (partition by b.stock_number)
from arkona.xfm_inpmast a 
left join arkona.ext_inpvinv b on a.inpmast_vin = b.vin
where a.current_row = true
  and a.status = 'I'
  and a.type_n_u = 'N'
  and a.make in ('chevrolet','buick','gmc','cadillac')
order by a.make, a.model  


select * from arkona.xfm_inpmast a where inpmast_vin = 'W04WH3N52HG088507'