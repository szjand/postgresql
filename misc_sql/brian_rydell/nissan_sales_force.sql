﻿-- consultants
drop table if exists consultants;
create temp table consultants as
select aa.*, dds.db2_integer_to_date(bb.hire_date) as hire_date,
  dds.db2_integer_to_date(bb.termination_date) as term_date
from (
  select a.last_name, a.first_name, a.employee_number, a.payplan
  from sls.consultant_payroll a
  where a.year_month between 201910 and 202009
    and left(a.employee_number, 1) = '2'
  group by a.last_name, a.first_name, a.employee_number, a.payplan) aa
left join arkona.ext_pymast bb on aa.employee_number = bb.pymast_employee_number;



-- consultant sales & comp
select last_name, first_name, employee_number, payplan, 
  sum(unit_count) filter (where year_month = 201910) as oct_count,
  sum(total_earned) filter (where year_month = 201910) as oct_comp,
  sum(unit_count) filter (where year_month = 201911) as nov_count,
  sum(total_earned) filter (where year_month = 201911) as nov_comp,
  sum(unit_count) filter (where year_month = 201912) as dec_count,
  sum(total_earned) filter (where year_month = 201912) as dec_comp,
  sum(unit_count) filter (where year_month = 202001) as jan_count,
  sum(total_earned) filter (where year_month = 202001) as jan_comp,
  sum(unit_count) filter (where year_month = 202002) as feb_count,
  sum(total_earned) filter (where year_month = 202002) as feb_comp,
  sum(unit_count) filter (where year_month = 202003) as mar_count,
  sum(total_earned) filter (where year_month = 202003) as mar_comp,
  sum(unit_count) filter (where year_month = 202004) as apr_count,
  sum(total_earned) filter (where year_month = 202004) as apr_comp,
  sum(unit_count) filter (where year_month = 202005) as may_count,
  sum(total_earned) filter (where year_month = 202005) as may_comp,
  sum(unit_count) filter (where year_month = 202006) as jun_count,
  sum(total_earned) filter (where year_month = 202006) as jun_comp,
  sum(unit_count) filter (where year_month = 202007) as jul_count,
  sum(total_earned) filter (where year_month = 202007) as jul_comp,
  sum(unit_count) filter (where year_month = 202008) as aug_count,
  sum(total_earned) filter (where year_month = 202008) as aug_comp,
  sum(unit_count) filter (where year_month = 202009) as sep_count,
  sum(total_earned) filter (where year_month = 202009) as sep_comp               
-- select year_month, last_name, first_name, employee_number, payplan, unit_count, total_earned
from sls.consultant_payroll
where year_month between 201910 and 202009
  and left(employee_number, 1) = '2'
group by last_name, first_name, employee_number, payplan

-- managers
drop table if exists mgrs_1;
create temp table mgrs_1 as
select distinct pymast_employee_number as employee_number, employee_name, a.distrib_code, data as job
-- select a.pymast_company_number, a.pymast_employee_number, employee_name, a.department_code, 
--   dds.db2_integer_to_date(a.hire_date) as hire_date,
--   dds.db2_integer_to_date(a.termination_date) as term_date, distrib_code, c.data
from arkona.xfm_pymast a
left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
left join arkona.ext_pyprjobd c on b.company_number = c.company_number
  and b.job_description = c.job_description 
-- where a.pymast_employee_number = '211232'  
where dds.db2_integer_to_date(a.hire_date) < '09/30/2020'
  and dds.db2_integer_to_date(a.termination_date) > '10/01/2019'
  and a.distrib_code in (/*'SALE',*/'Team','salm','smgr','mgr')
  and a.pymast_company_number = 'RY2'
--   and c.data not like '%consult%'
  and a.department_code in ('01','02')
  and not exists (
    select 1
    from consultants
    where employee_number = a.pymast_employee_number) order by job

select a.*, employeekeyfromdate, employeekeythrudate, termdate
from mgrs_1 a
left join ads.ext_edw_employee_dim b on a.employee_number = b.employeenumber
where a.employee_number not in ('212589','284625','285820')
order by employee_number, employeekey

where employeekeyfromdate < '09/30/2020'
  and employeekeythrudate > '10/01/2019'

bedney: nope
calcaterra  222457, 2016-01-05 - 2019-11-01
tweeten 242460  2018-07-31 - 2019-11-27
longoria 265328 2019-11-05 - 2020-09-30
knudson 279380  10/01/2019 - 
holland 211232  06/05/2019 - 

langenstine nope
lizakowski nope

-- this verifies bedney

select total_gross_pay, (check_month::text ||'-' || check_day::text ||'-' || check_year::text)::Date
from arkona.ext_pyhshdta
where (check_month::text ||'-' || check_day::text ||'-' || check_year::text)::Date between '10/01/2019' and '09/30/2020'
   and employee_ in ('265328')
order by (check_month::text ||'-' || check_day::text ||'-' || check_year::text)::Date

drop table if exists managers;
create temp table managers as
select pymast_employee_number as employee_number, employee_name, data as job, max(row_from_date) as hire_Date, max(row_thru_date) as term_date, d.total_Gross_pay::integer
from arkona.xfm_pymast a
left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
left join arkona.ext_pyprjobd c on b.company_number = c.company_number
  and b.job_description = c.job_description 
left join (
  select employee_, sum(total_gross_pay) as total_Gross_pay
  from arkona.ext_pyhshdta
  where (check_month::text ||'-' || check_day::text ||'-' || check_year::text)::Date between '10/01/2019' and '09/30/2020'
     and employee_ in ('222457','242460','265328','279380','211232')
  group by employee_) d on a.pymast_employee_number = d.employee_  
where a.pymast_employee_number in ('222457','242460','265328','279380','211232')  
group by a.pymast_employee_number, a.employee_name, c.data, d.total_Gross_pay;

select *
from managers

update managers
set job = 'Sales Team Leader', hire_Date = '06/05/2019', term_date = null
where employee_number = '211232';
update managers
set job = 'Sales Team Leader', hire_Date = '07/31/2018', term_date = '11/27/2019'
where employee_number = '242460';
update managers
set job = 'Sales Team Leader', hire_Date = '2016-01-05', term_date = '2019-11-01'
where employee_number = '222457';
update managers
set job = 'Sales Team Leader', hire_Date = '2019-11-05', term_date = '2020-09-30'
where employee_number = '265328';
update managers
set job = 'General Sales Mgr', hire_Date = '10/01/2019', term_date = null
where employee_number = '279380';