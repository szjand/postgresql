﻿
drop table if exists bdc_1;
create temp table bdc_1 as
select employee_last_name, employee_first_name, pymast_employee_number, 
  distrib_code, dds.db2_integer_to_date(hire_date) as hire_Date, 
  dds.db2_integer_to_date(termination_date) as term_Date
from arkona.ext_pymast
where pymast_employee_number in ('187347','159875','256845','157392','147852',
  '157385','165748','145785','175873','164821','185462','159763','148080',
  '166278','163700');



select b.employee_last_name, employee_first_name, a.employee_, b.term_date, 
  (check_month::text ||'-' || check_day::text ||'-' || check_year::text)::Date, 
  sum(total_gross_pay) as comp
from arkona.ext_pyhshdta a
left join bdc_1 b on a.employee_ = b.pymast_employee_number
where (check_month::text ||'-' || check_day::text ||'-' || check_year::text)::Date between '10/01/2019' and '09/30/2020'
   and employee_ in ('187347','159875','256845','157392','147852',
  '157385','165748','145785','175873','164821','185462','159763','148080',
  '166278','163700')
group by b.employee_last_name, employee_first_name, a.employee_, b.term_date, (check_month::text ||'-' || check_day::text ||'-' || check_year::text)::Date  
order by employee_, (check_month::text ||'-' || check_day::text ||'-' || check_year::text)::Date


select b.employee_last_name, employee_first_name, a.employee_, b.term_date, 
  (check_month::text ||'-' || check_day::text ||'-' || check_year::text)::Date, 
  sum(total_gross_pay) as comp
from bdc_1 b  
left join  arkona.ext_pyhshdta a on a.employee_ = b.pymast_employee_number
where (check_month::text ||'-' || check_day::text ||'-' || check_year::text)::Date between '10/01/2019' and '09/30/2020'
   and employee_ in ('187347','159875','256845','157392','147852',
  '157385','165748','145785','175873','164821','185462','159763','148080',
  '166278','163700')
group by b.employee_last_name, employee_first_name, a.employee_, b.term_date, (check_month::text ||'-' || check_day::text ||'-' || check_year::text)::Date  
order by employee_, (check_month::text ||'-' || check_day::text ||'-' || check_year::text)::Date


-- look at this table in edit mode, edit from & thru dates to "select" the appropriate rows
drop table if exists tem.bdc_for_nissan cascade;
create table tem.bdc_for_nissan as
select a.*, b.employeekey, b.distcode, b.distribution, b.pydept, b.rowchangereason, b.active, b.termdate, b.employeekeyfromdate, b.employeekeythrudate, null::date as from_date, null::date as thru_date
from bdc_1 a
left join ads.ext_edw_employee_dim b on a.pymast_employee_number = b.employeenumber
order by employeenumber, employeekey;
alter table tem.bdc_For_nissan add primary key(employeekey);

select employee_last_name, employee_first_name, from_date as hire_date, thru_date as term_Date
from tem.bdc_for_nissan a
where a.from_date is not null


select a.employee_last_name, a.employee_first_name, a.from_date as hire_date, a.thru_date as term_Date, 
  (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date, b.total_gross_pay
from tem.bdc_for_nissan a
left join  arkona.ext_pyhshdta b on a.pymast_employee_number = b.employee_
  and (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date 
    between
      case
        when a.from_date < '10/01/2019' then '10/01/2019'
        else a.from_date
      end
    and  '09/30/2020'
where a.from_date is not null
order by a.pymast_employee_number, (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date 


-- and the spreadsheet for brian


select a.employee_last_name as last_name, a.employee_first_name as first_name, a.from_date as hire_date, a.thru_date as term_Date, 
  sum(b.total_gross_pay) as comp
from tem.bdc_for_nissan a
left join  arkona.ext_pyhshdta b on a.pymast_employee_number = b.employee_
  and (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date 
    between
      case
        when a.from_date < '10/01/2019' then '10/01/2019'
        else a.from_date
      end
    and  '09/30/2020'
where a.from_date is not null
group by a.employee_last_name, a.employee_first_name, a.from_date, a.thru_date
