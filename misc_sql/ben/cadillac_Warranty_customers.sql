﻿select a.ro, b.the_date, c.modelyear, c.model, e.fullname, e.customerkey, f.bopname_Search_name, f.address_1, f.city, f.state_code, f.zip_code, f.phone_number
from ads.ext_Fact_repair_order a
join dds.dim_date b on a.finalclosedatekey = b.date_key
  and b.year_month = 201812
join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey
  and c.make = 'cadillac'  
join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey  
  and d.paymenttypecode = 'W'
join ads.ext_dim_customer e on a.customerkey = e.customerkey  
left join arkona.xfm_bopname f on e.bnkey = f.bopname_record_key
  and f.current_row
group by a.ro, b.the_date, c.modelyear, c.model, e.fullname, e.customerkey, f.bopname_Search_name, f.address_1, f.city, f.state_code, f.zip_code, f.phone_number
order by a.ro


