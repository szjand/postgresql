﻿select * from ads.ext_organizations
select * from ads.ext_vehicle_inspections limit 10
select * from ads.ext_vehicle_inventory_item_statuses where status like '%avail%' limit 20

drop table if exists vehicles cascade;
create temp table vehicles as 
	select a.stocknumber, fromts::date as from_date, thruts::date as thru_date
	from ads.ext_vehicle_inventory_items a
	where a.locationid = 'B6183892-C79D-4489-A58C-B526DF948B06'
		and a.owninglocationid = 'B6183892-C79D-4489-A58C-B526DF948B06'
		and a.thruts::date > '07/31/2022'
		and a.fromts::Date <'09/30/2022'
		
select aa.the_date, count(*) as owned
from (
	select the_date
	from dds.dim_date
	where the_date between '08/01/2022' and '09/30/2022') aa
left join vehicles bb on aa.the_date between bb.from_date and bb.thru_date
group by aa.the_date		

select the_date, count(*) as available
from (
select a.the_date, b.vehicleinventoryitemid, b.stocknumber
from dds.dim_date a
join ads.ext_vehicle_inventory_items b on a.the_date between b.fromts::date and b.thruts::date
  and b.locationid = 'B6183892-C79D-4489-A58C-B526DF948B06'
  and b.owninglocationid = 'B6183892-C79D-4489-A58C-B526DF948B06'
join ads.ext_vehicle_inventory_item_statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
  and c.status = 'RMFlagAV_Available'
  and a.the_date between c.fromts::date and c.thruts::date  
where a.the_date between '08/01/2022' and '09/30/2022') cc
group by the_date
order by the_date


select a.the_date, b.vehicleinventoryitemid, b.stocknumber
from dds.dim_date a
join ads.ext_vehicle_inventory_items b on a.the_date between b.fromts::date and b.thruts::date
  and b.stocknumber = 'G43748A'
  and b.locationid = 'B6183892-C79D-4489-A58C-B526DF948B06'
  and b.owninglocationid = 'B6183892-C79D-4489-A58C-B526DF948B06'
join ads.ext_vehicle_inventory_item_statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
  and c.status = 'RMFlagAV_Available'
  and a.the_date between c.fromts::date and c.thruts::date  
where a.the_date between '08/01/2022' and '09/30/2022'

select a.the_date, b.vehicleinventoryitemid, b.stocknumber
from dds.dim_date a
join ads.ext_vehicle_inventory_items b on a.the_date between b.fromts::date and b.thruts::date
  and b.locationid = 'B6183892-C79D-4489-A58C-B526DF948B06'
  and b.owninglocationid = 'B6183892-C79D-4489-A58C-B526DF948B06'
join ads.ext_vehicle_inventory_item_statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
  and c.status = 'RMFlagAV_Available'
  and a.the_date between c.fromts::date and c.thruts::date  
where a.the_date = '09/30/2022'
order by stocknumber

-- sent to ben 10/3/22
select aaa.the_date, aaa.owned, bbb.available
from (
	select aa.the_date, count(*) as owned
	from (
		select the_date
		from dds.dim_date
		where the_date between '08/01/2022' and '09/30/2022') aa
	left join vehicles bb on aa.the_date between bb.from_date and bb.thru_date
	group by aa.the_date) aaa	
left join (
select the_date, count(*) as available
from (
select a.the_date, b.vehicleinventoryitemid, b.stocknumber
from dds.dim_date a
join ads.ext_vehicle_inventory_items b on a.the_date between b.fromts::date and b.thruts::date
  and b.locationid = 'B6183892-C79D-4489-A58C-B526DF948B06'
  and b.owninglocationid = 'B6183892-C79D-4489-A58C-B526DF948B06'
join ads.ext_vehicle_inventory_item_statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
  and c.status = 'RMFlagAV_Available'
  and a.the_date between c.fromts::date and c.thruts::date  
where a.the_date between '08/01/2022' and '09/30/2022') cc
group by the_date) bbb on aaa.the_date = bbb.the_date	