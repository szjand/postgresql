﻿/*
Jon,

Can you run a report that shows me the folllowing from 8/1/17 - 7/31/18.

How many vehicles were sold with package certified?

Now disregard the above dates but use those VIN’s and show the following IF the odometer was greater than 36k when the RO was written AND any line on the RO is for warranty. 

How many RO’s meet the above criteria?

For the above RO’s what are the total warranty labor sales? Parts sales?

For the above RO’s what are the total CP labor sales? Parts sales?

I will stop back tomorrow and see if this all made sense. 

*/




drop table if exists sales;
create temp table sales as
  select a.control, b.the_date,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201708 and 201807--------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select distinct d.gl_account
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month between 201708 and 201807--- -------------------------------------------------------------------
      and b.page = 16 and b.line in (1,4)-- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
      and f.store = 'RY1') d on c.account = d.gl_account
  where a.post_status = 'Y';



drop table if exists vehicles;
create temp table vehicles as
select a.control, 
  b.bopmast_vin as vin, 
  case 
    when a.the_date > b.date_capped then a.the_date
    else b.date_capped
  end as the_date
from (
  select control, the_Date, sum(unit_count) as unit_count -- 898
  from sales
  group by control, the_Date) a
left join arkona.xfm_bopmast b on a.control = b.bopmast_stock_number
  and b.current_row  
where unit_count = 1;

select a.* , b.ro_number as ro, arkona.db2_integer_to_date_long(open_date) as open_date
from vehicles a
join arkona.ext_sdprhdr b on a.vin = b.vin
  and arkona.db2_integer_to_date_long(open_date) > a.the_date + 7


select * from arkona.ext_sdprhdr limit 100

select b.*, a.line, a.laborsales, c.paymenttypecode, d.ptqty, d.ptnet
-- select *
from ads.ext_fact_Repair_order a
join (
  select a.* , b.ro_number as ro, arkona.db2_integer_to_date_long(open_date) as open_date
  from vehicles a
  join arkona.ext_sdprhdr b on a.vin = b.vin
    and arkona.db2_integer_to_date_long(b.open_date) > a.the_date
    and b.odometer_in > 36000) b on a.ro = b.ro
join ads.ext_dim_payment_type c on a.paymenttypekey = c.paymenttypekey    
  and c.paymenttypecode in ('C','W')
left join arkona.ext_pdptdet d on a.ro = d.ptinv_
  and a.line = d.ptline  


select ptline, sum(ptqty * ptlist), sum(ptqty * ptnet)
from arkona.ext_pdptdet
where ptinv_ = '16340978'
group by ptline



select b.control, a.ro, a.line, c.paymenttypecode, sum(a.laborsales) as labor 
-- select *
from ads.ext_fact_Repair_order a
join (
  select a.* , b.ro_number as ro, arkona.db2_integer_to_date_long(open_date) as open_date
  from vehicles a
  join arkona.ext_sdprhdr b on a.vin = b.vin
    and arkona.db2_integer_to_date_long(b.open_date) > a.the_date
    and b.odometer_in > 36000) b on a.ro = b.ro
join ads.ext_dim_payment_type c on a.paymenttypekey = c.paymenttypekey    
  and c.paymenttypecode in ('C','W')
group by b.control, a.ro, a.line, c.paymenttypecode




select b.control, a.ro, a.line, c.paymenttypecode, sum(a.laborsales) as labor 
-- select *
from ads.ext_fact_Repair_order a
join (
  select a.* , b.ro_number as ro, arkona.db2_integer_to_date_long(open_date) as open_date
  from vehicles a
  join arkona.ext_sdprhdr b on a.vin = b.vin
    and arkona.db2_integer_to_date_long(b.open_date) > a.the_date
    and b.odometer_in > 36000) b on a.ro = b.ro
join ads.ext_dim_payment_type c on a.paymenttypekey = c.paymenttypekey    
  and c.paymenttypecode in ('C','W')
group by b.control, a.ro, a.line, c.paymenttypecode

drop table if exists ros;
create temp table ros as
select b.ro_number as ro
from vehicles a
join arkona.ext_sdprhdr b on a.vin = b.vin
  and arkona.db2_integer_to_date_long(b.open_date) > a.the_date
  and b.odometer_in > 36000
group by ro;    


drop table if exists labor;
create temp table labor as
select a.ro, c.paymenttypecode, sum(b.laborsales) as labor 
from ros a
join ads.ext_fact_repair_order b on a.ro = b.ro
join ads.ext_dim_payment_type c on b.paymenttypekey = c.paymenttypekey    
  and c.paymenttypecode in ('C','W')
group by a.ro, c.paymenttypecode
having sum(b.laborsales) > 0;



drop table if exists parts;
create temp table parts as
select a.ro, d.ptcode, sum(d.ptqty * d.ptnet) as parts
from ros a
join arkona.ext_pdptdet d on a.ro = d.ptinv_
  and d.ptcode in ('CP','WS')
group by a.ro, d.ptcode 
having sum(d.ptqty * d.ptnet) <> 0


select *
from vehicles

select paymenttypecode, sum(labor) from labor group by paymenttypecode

select paymenttypecode, sum(labor) from labor where ro not like '19%' group by paymenttypecode

select ptcode, sum(parts) from parts group by ptcode

select ptcode, sum(parts) from parts where ro not like '19%' group by ptcode


