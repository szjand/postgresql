﻿select pymast_company_number as store, pymast_employee_number as "emp #", employee_name, 
  case a.active_code
		when 'A' then 'full'
		when 'P' then 'part'  
  end as "full/part", b.description as department,
  dds.db2_integer_to_date(hire_date) as hire_date, 
  case
		when dds.db2_integer_to_date(last_raise_date) = '12/31/9999' then null
		else dds.db2_integer_to_date(last_raise_date)
	end as last_raise_date, 
  case a.payroll_class
		when 'H' then 'hourly'
		when 'S' then 'salary'
  end as payroll_class, 
  case a.payroll_class
  	when 'H' then base_hrly_rate
		when 'S' then base_Salary
  end as current_comp
from arkona.ext_pymast a
left join arkona.ext_pypclkctl b on a.pymast_company_number = b.company_number and a.department_code = b.department_code
where active_code <> 'T'
  and payroll_class in ('H','S')
  and pymast_company_number in ('RY1','RY2')