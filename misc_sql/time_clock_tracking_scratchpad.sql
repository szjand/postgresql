﻿-- compare tracking to tracking_daily
select *
from (
  select employee_name, employee_number, sum(ot_hours) as ot_hours
  from jeri.clock_hour_tracking_daily
  where year_month > 201806
    and payroll_class_code = 'H'
    and run_date = '07/23/2018'
  group by employee_name, employee_number
  having sum(ot_comp) <> 0) a
full outer join (
  select employee_name, employee_number, sum(ot_hours) as ot_hours
  from jeri.clock_hour_tracking
  where year_month > 201806
  group by employee_name, employee_number
  having sum(ot_comp) <> 0) b on a.employee_name = b.employee_name 
where a.ot_hours <> b.ot_hours