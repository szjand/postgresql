﻿select *
from (
  select spco_, spsvctyp, splpym, spfran, spsvco, splr1a, md5(a::text) as hash
  from arkona.ext_sdpprice_tmp a
  where spco_ = 'RY1') aa
full outer join (
  select spco_, spsvctyp, splpym, spfran, spsvco, splr1a, md5(a::text) as hash
  from arkona.ext_sdpprice a
  where spco_ = 'RY1') bb on aa.spco_ = bb.spco_
    and aa.spsvctyp = bb.spsvctyp 
    and aa.splpym = bb.splpym 
    and aa.spsvco = bb.spsvco
    and aa.spfran = bb.spfran
where aa.hash <> bb.hash    

same number of rows
only diff is CNA Western National (spsvco = 2) has drpped from 113.74 to 96.68

it looks like the data in arkona.ext_sdpprice was generated on 11/21/2018, check with the 9:30


select 'new' as source, a.* from arkona.ext_sdpprice_tmp a where spco_ = 'RY1' and spsvctyp = 'MN' and splpym = 'S' and spfran = 'GM'
union
select 'old' as source, a.* from arkona.ext_sdpprice a where spco_ = 'RY1' and spsvctyp = 'MN' and splpym = 'S' and spfran = 'GM'
order by spsvco, source


CREATE TABLE arkona.ext_sdpprice_tmp
(
  spco_ citext, -- SPCO# : Company Number
  spsvctyp citext, -- SPSVCTYP : Service Type
  splpym citext, -- SPLPYM : Line Pymt Method
  spfran citext, -- SPFRAN : Franchise Code
  spsvco integer, -- SPSVCO : Serv Cont Comp
  spspct integer, -- SPSPCT : Sublet Markup %
  spslbr citext, -- SPSLBR : Shop Suppl Labor
  spsprt citext, -- SPSPRT : Shop Suppl Parts
  spssbl citext, -- SPSSBL : Shop Suppl Sublt
  spmdy1 integer, -- SPMDY1 : Model Year 1
  spmdy2 integer, -- SPMDY2 : Model Year 2
  spmdy3 integer, -- SPMDY3 : Model Year 3
  spmdy4 integer, -- SPMDY4 : Model Year 4
  splr1a numeric(5,2), -- SPLR1A : Skill 1A Rate
  splr2a numeric(5,2), -- SPLR2A : Skill 2A Rate
  splr3a numeric(5,2), -- SPLR3A : Skill 3A Rate
  splr4a numeric(5,2), -- SPLR4A : Skill 4A Rate
  splr1b numeric(5,2), -- SPLR1B : Skill 1B Rate
  splr2b numeric(5,2), -- SPLR2B : Skill 2B Rate
  splr3b numeric(5,2), -- SPLR3B : Skill 3B Rate
  splr4b numeric(5,2), -- SPLR4B : Skill 4B Rate
  splr1c numeric(5,2), -- SPLR1C : Skill 1C Rate
  splr2c numeric(5,2), -- SPLR2C : Skill 2C Rate
  splr3c numeric(5,2), -- SPLR3C : Skill 3C Rate
  splr4c numeric(5,2), -- SPLR4C : Skill 4C Rate
  splr1d numeric(5,2), -- SPLR1D : Skill 1D Rate
  splr2d numeric(5,2), -- SPLR2D : Skill 2D Rate
  splr3d numeric(5,2), -- SPLR3D : Skill 3D Rate
  splr4d numeric(5,2), -- SPLR4D : Skill 4D Rate
  spplv1 integer, -- SPPLV1 : Price Level 1
  spplv2 integer, -- SPPLV2 : Price Level 1
  spplv3 integer, -- SPPLV3 : Price Level 1
  spplv4 integer, -- SPPLV4 : Price Level 1
  sppsa1 integer, -- SPPSA1 : Prc Strat Assn 1
  sppsa2 integer, -- SPPSA2 : Prc Strat Assn 2
  sppsa3 integer, -- SPPSA3 : Prc Strat Assn 3
  sppsa4 integer, -- SPPSA4 : Prc Strat Assn 4
  spclac citext, -- SPCLAC : Car Labor Acct
  sptlac citext, -- SPTLAC : Truck Labor Acct
  spwrac citext, -- SPWRAC : Warr Recv Account
  spfrac citext, -- SPFRAC : Fact Recv Account
  spcus_ citext, -- SPCUS# : Warr Customer No
  spspctp integer, -- SPSPCTP : Sublet Markup %-P
  splma1 integer, -- SPLMA1 : Labor matrix assignment 1
  splma2 integer, -- SPLMA2 : Labor matrix assignment 2
  splma3 integer, -- SPLMA3 : Labor matrix assignment 3
  splma4 integer -- SPLMA4 : Labor matrix assignment 4
)
WITH (
  OIDS=FALSE
);
ALTER TABLE arkona.ext_sdpprice_tmp
  OWNER TO postgres;
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.spco_ IS 'SPCO# : Company Number';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.spsvctyp IS 'SPSVCTYP : Service Type';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.splpym IS 'SPLPYM : Line Pymt Method';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.spfran IS 'SPFRAN : Franchise Code';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.spsvco IS 'SPSVCO : Serv Cont Comp';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.spspct IS 'SPSPCT : Sublet Markup %';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.spslbr IS 'SPSLBR : Shop Suppl Labor';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.spsprt IS 'SPSPRT : Shop Suppl Parts';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.spssbl IS 'SPSSBL : Shop Suppl Sublt';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.spmdy1 IS 'SPMDY1 : Model Year 1';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.spmdy2 IS 'SPMDY2 : Model Year 2';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.spmdy3 IS 'SPMDY3 : Model Year 3';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.spmdy4 IS 'SPMDY4 : Model Year 4';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.splr1a IS 'SPLR1A : Skill 1A Rate';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.splr2a IS 'SPLR2A : Skill 2A Rate';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.splr3a IS 'SPLR3A : Skill 3A Rate';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.splr4a IS 'SPLR4A : Skill 4A Rate';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.splr1b IS 'SPLR1B : Skill 1B Rate';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.splr2b IS 'SPLR2B : Skill 2B Rate';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.splr3b IS 'SPLR3B : Skill 3B Rate';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.splr4b IS 'SPLR4B : Skill 4B Rate';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.splr1c IS 'SPLR1C : Skill 1C Rate';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.splr2c IS 'SPLR2C : Skill 2C Rate';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.splr3c IS 'SPLR3C : Skill 3C Rate';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.splr4c IS 'SPLR4C : Skill 4C Rate';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.splr1d IS 'SPLR1D : Skill 1D Rate';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.splr2d IS 'SPLR2D : Skill 2D Rate';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.splr3d IS 'SPLR3D : Skill 3D Rate';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.splr4d IS 'SPLR4D : Skill 4D Rate';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.spplv1 IS 'SPPLV1 : Price Level 1';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.spplv2 IS 'SPPLV2 : Price Level 1';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.spplv3 IS 'SPPLV3 : Price Level 1';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.spplv4 IS 'SPPLV4 : Price Level 1';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.sppsa1 IS 'SPPSA1 : Prc Strat Assn 1';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.sppsa2 IS 'SPPSA2 : Prc Strat Assn 2';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.sppsa3 IS 'SPPSA3 : Prc Strat Assn 3';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.sppsa4 IS 'SPPSA4 : Prc Strat Assn 4';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.spclac IS 'SPCLAC : Car Labor Acct';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.sptlac IS 'SPTLAC : Truck Labor Acct';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.spwrac IS 'SPWRAC : Warr Recv Account';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.spfrac IS 'SPFRAC : Fact Recv Account';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.spcus_ IS 'SPCUS# : Warr Customer No';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.spspctp IS 'SPSPCTP : Sublet Markup %-P';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.splma1 IS 'SPLMA1 : Labor matrix assignment 1';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.splma2 IS 'SPLMA2 : Labor matrix assignment 2';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.splma3 IS 'SPLMA3 : Labor matrix assignment 3';
COMMENT ON COLUMN arkona.ext_sdpprice_tmp.splma4 IS 'SPLMA4 : Labor matrix assignment 4';

