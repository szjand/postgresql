﻿
------------------------------------------------------------------------------------
--< jeri's spreadsheets
------------------------------------------------------------------------------------

drop table if exists jon.techs;
create table jon.techs as
select c.employee_name, b.tech_number, b.employee_number,  array_agg(distinct b.tech_key) as tech_key
from dds.dim_date a
left join dds.dim_tech b on a.the_date between row_from_Date and row_thru_date
  and b.active
join arkona.ext_pymast c on b.employee_number = c.pymast_employee_number  
  and c.pymast_employee_number not in ('10568', '1117960')  -- exclude craig rogne and caden aker
where a.the_date between '01/01/2019' and current_date
  and b.store_code = 'RY1'
  and case 
        when b.tech_number = '588' then b.flag_department_code not in ('RE','BS','NA','HS')
        else b.flag_department_code not in ('QL','RE','BS','NA','HS')
      end
  and b.tech_name is not null
group by b.tech_name, b.tech_number, b.employee_number, c.employee_name; 
alter table jon.techs add primary key(tech_number);
CREATE INDEX on jon.techs USING GIN (tech_key);

-- need to include ros with close_date or final_close_date since 1/1/2019
drop table if exists jon.ro_base;
create table jon.ro_base as
select a.ro, a.line, a.open_date, a.close_date, a.final_close_date, b.tech_number, c.service_type_code as service_type, 
  d.payment_type_code as payment_type, sum(flag_hours) as flag_hours, sum(a.labor_sales) as labor_sales
from dds.fact_repair_order a
join jon.techs b on a.tech_key  = any(b.tech_key)
join dds.dim_service_type c on a.service_type_key = c.service_type_key
  and c.service_type_code in ('AM','EM','MN','MR','QS','SV','OT')
join dds.dim_payment_type d on a.payment_type_key = d.payment_type_key
where a.final_close_date between '01/01/2019' and current_date
  or a.close_date between '01/01/2019' and current_date
group by a.ro, a.line, a.open_date, a.close_date, a.final_close_date, b.tech_number, c.service_type_code, d.payment_type_code;
alter table jon.ro_base add primary key(ro,line,tech_number);

drop table if exists jon.ro_details;
create table jon.ro_details as 
select aa.*, coalesce(bb.labor_cost, 0) as labor_cost, coalesce(cc.ro_discount) as discount
from jon.ro_base aa
left join (
  select b.ro, b.line, b.tech_number, sum(a.cost) as labor_cost
  from arkona.ext_sdprdet_history a
  join jon.ro_base b on a.ro_number = b.ro
   and a.line_number = b.line
   and a.technician_id = b.tech_number
  group by b.ro, b.line, b.tech_number) bb on aa.ro = bb.ro
    and aa.line = bb.line
    and aa.tech_number = bb.tech_number
left join (
  select a.ro, a.ro_discount
  from dds.fact_repair_order a
  join jon.ro_base b on a.ro = b.ro
  group by a.ro, a.ro_discount) cc on aa.ro = cc.ro;  
alter table jon.ro_details add primary key(ro,line,tech_number);
create index on jon.ro_details(ro);
create index on jon.ro_details(close_date);
create index on jon.ro_details(service_type);
create index on jon.ro_details(payment_type);

-- cost, segregate OT and non OT
  select b.ro, b.line, b.tech_number, sum(a.cost) as labor_cost
  from arkona.ext_sdprdet_history a
  join jon.ro_base b on a.ro_number = b.ro
   and a.line_number = b.line
   and a.technician_id = b.tech_number
  group by b.ro, b.line, b.tech_number
-- yikes on separate seq than tech number
  select a.*
  from arkona.ext_sdprdet_history a
where ro_number like '163214%'
  and exists (
    select 1
    from arkona.ext_Sdprdet_history
    where ro_number = a.ro_number
      and service_Type = 'OT')
order by ro_number, line_number, sequence_number

-- 4 ros with multiple service types per line
select ro, line
from (
  select b.ro, b.line, a.service_type
  from arkona.ext_sdprdet_history a
  join jon.ro_base b on a.ro_number = b.ro
   and a.line_number = b.line
  where a.service_type is not null
  group by b.ro, b.line, a.service_type) x
group by ro, line having count(*) > 1  

select * from arkona.ext_Sdprdet_history
where (ro_number = '16382295' and line_number = 3) or (ro_number = '16383260' and line_number = 2)


drop table if exists jon.tech_clock_hours;
create table jon.tech_clock_hours as
select b.the_date, b.year_month, a.tech_number, a.employee_number, sum(b.clock_hours) as clock_hours
from jon.techs a
left join arkona.xfm_pypclockin b on a.employee_number = b.employee_number
  and b.the_date between '01/01/2019' and current_date
group by b.the_date, b.year_month, a.tech_number, a.employee_number;
alter table jon.tech_clock_hours add primary key(the_date,tech_number);
create index on jon.tech_clock_hours(tech_number);
create index on jon.tech_clock_hours(employee_number);

drop table if exists jon.tech_comp;
create table jon.tech_comp as
select c.year_month, d.employee_number, d.tech_number, sum(a.amount) as comp
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
  and b.account = '124704'
join dds.dim_date c on a.date_key = c.date_key
  and c.the_date between '01/01/2019' and current_date
join jon.techs d on a.control = d.employee_number
where a.post_status = 'Y'    
group by c.year_month, d.employee_number, d.tech_number;
alter table jon.tech_comp add primary key (year_month,tech_number);
create index on jon.tech_comp(tech_number);


-- i don't know what kind of accounting bullshit is happening, i am assuming jeri
-- is using acct 124704 for these guys as well, but it is not what they are getting paid
drop table if exists jon.non_tech_comp;
create table jon.non_tech_comp as
select c.year_month, d.pymast_employee_number, d.employee_name, sum(a.amount) as comp
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
  and b.account = '124704'
join dds.dim_date c on a.date_key = c.date_key
  and c.the_date between '01/01/2019' and current_date
join (
  select employee_name, pymast_employee_number
  from arkona.ext_pymast
  where employee_name in ('LONGORIA, BEVERLEY A','ROGNE, CRAIG A','ESPELUND, KENNETH M')) d on a.control = d.pymast_employee_number
where a.post_status = 'Y'    
group by c.year_month, d.pymast_employee_number, d.employee_name;
alter table jon.non_tech_comp add primary key (year_month,pymast_employee_number);
create index on jon.non_tech_comp(pymast_employee_number);

-- -- this is the actual gross pay
-- select employee_, check_month, check_year, sum(total_gross_pay) as total_gross
-- from arkona.ext_pyhshdta a
-- join (
--   select employee_name, pymast_employee_number
--   from arkona.ext_pymast
--   where employee_name in ('LONGORIA, BEVERLEY A','ROGNE, CRAIG A','ESPELUND, KENNETH M')) b on a.employee_ = b.pymast_employee_number
-- where check_year in (19,20)
-- group by employee_, check_month, check_year
  


--< Tech Info Mar 2020 with descriptions_with_acl.xlsx -----------------------------
-- need clock hours


select aa.employee_name, aa.tech_number, aa.sales, aa.flag_exc_ot, aa.flag_ot, 
  aa.flag_total, aa.clock_hours, aa.comp, aa.sales - aa.comp as tech_gross,
  (100 * aa.flag_exc_ot/aa.clock_hours)::integer as prof, (aa.comp/aa.flag_exc_ot)::integer as "comp/flag",
  (aa.sales/aa.flag_exc_ot)::integer as elr, bb.flat_hourly
from (
  select e.employee_name, a.tech_number, d.employee_number,
    sum(flag_hours) filter (where service_type <> 'OT') as flag_exc_ot, 
    sum(flag_hours) filter (where service_type = 'OT') as flag_ot,
    sum(flag_hours) as flag_total,
    sum(labor_sales) as sales, 
    sum(labor_cost) as the_cost,
    max(c.clock_hours) as clock_hours,
    max(d.comp) as comp
  from jon.ro_details a
  join dds.dim_date b on a.final_close_date = b.the_date
    and b.year_month = 202003
  left join (
    select tech_number, sum(clock_hours) as clock_hours
    from jon.tech_clock_hours
    where the_date between '03/01/2020' and '03/31/2020'
    group by tech_number) c on a.tech_number = c.tech_number
  left join jon.tech_comp d on a.tech_number = d.tech_number  
    and d.year_month = 202003
  left join jon.techs e on a.tech_number = e.tech_number
  group by e.employee_name, a.tech_number, d.employee_number) aa
left join (
  select b.employee_name, b.tech_number, b.employee_number, string_agg(distinct a.payroll_class,',') flat_hourly
  from arkona.ext_pyhshdta a
  join jon.techs b on a.employee_ = b.employee_number
  where a.check_year = 20
    and check_month = 3
  group by b.employee_name, b.tech_number, b.employee_number) bb on aa.tech_number = bb.tech_number
order by aa.tech_number


-- final_close_date is the closest
march   jeri    final_close_date close_date
sales   497124  500918            505932
  select 
    sum(labor_sales) as sales
  from jon.ro_details a
  join dds.dim_date b on a.final_close_date = b.the_date
    and b.year_month = 202003
  left join (
    select tech_number, sum(clock_hours) as clock_hours
    from jon.tech_clock_hours
    where the_date between '03/01/2020' and '03/31/2020'
    group by tech_number) c on a.tech_number = c.tech_number
  left join jon.tech_comp d on a.tech_number = d.tech_number  
    and d.year_month = 202003




  
--< Tech Info Dec 2019_jeri_v3  --------------------------------------------------------------
defines retention as (labor sales - comp)/labor_sales
-- given the known variations in flag hours, this is pretty damned close to jeri
-- need to add non tech comp (espelund, longoria, rogne) and a total line
select tech_number, sales, comp, round(100*(sales - comp)/sales, 0)
from (
  select a.tech_number, 
    sum(flag_hours) filter (where service_type <> 'OT') as flag_exc_ot, 
    sum(flag_hours) filter (where service_type = 'OT') as flag_ot,
    sum(flag_hours) as flag_total,
    sum(labor_sales) as sales, 
    sum(labor_cost) as the_cost,
    max(c.clock_hours) as clock_hours,
    max(d.comp) as comp
  from jon.ro_details a
  join dds.dim_date b on a.final_close_date = b.the_date
    and b.year_month = 201912
  left join (
    select tech_number, sum(clock_hours) as clock_hours
    from jon.tech_clock_hours
    where the_date between '12/01/2019' and '12/31/2019'
    group by tech_number) c on a.tech_number = c.tech_number
  left join jon.tech_comp d on a.tech_number = d.tech_number  
    and d.year_month = 201912
  group by a.tech_number) aa




-- the first thing i want to see is  total retention over months, to verify the 5% drop may 19 to june 19
-- uh oh, it's not here
select year_month, sales, comp, (100 * (sales - comp)/sales)::integer as retention
from (
  select aa.year_month, sum(sales)::integer as sales, (sum(tech_comp) + max(non_tech_comp))::integer as comp 
  from (
    select b.year_month, a.tech_number, 
      sum(flag_hours) filter (where service_type <> 'OT') as flag_exc_ot, 
      sum(flag_hours) filter (where service_type = 'OT') as flag_ot,
      sum(flag_hours) as flag_total,
      sum(labor_sales) as sales, 
      sum(labor_cost) as the_cost,
      max(d.comp) as tech_comp
    from jon.ro_details a
    join dds.dim_date b on a.final_close_date = b.the_date
      and b.the_year in (2019, 2020)
    left join jon.tech_comp d on a.tech_number = d.tech_number  
      and b.year_month = d.year_month
    group by b.year_month, a.tech_number) aa
  left join (
    select year_month, sum(comp) as non_tech_comp
    from jon.non_tech_comp
    group by year_month) bb on aa.year_month = bb.year_month  
  group by aa.year_month) cc
order by year_month

-- lets add discount
select b.year_month, sum(a.discount)::integer as discount 
from jon.ro_details a
join dds.dim_date b on a.final_close_date = b.the_date
  and b.year_month >= 201901
group by b.year_month
order by b.year_month



select year_month, sales, discount, tech_comp, non_tech_comp,
  (100*(sales - tech_comp)/sales)::integer as ret_1,
  (100*(sales - (tech_comp + non_tech_comp))/sales)::integer as ret_2
--   (100*((sales + discount) - tech_comp)/(sales + discount))::integer as ret_3,
--   (100*((sales + discount) - tech_comp + non_tech_comp)/(sales + discount))::integer as ret_4
from (
  select aa.year_month, sum(sales)::integer as sales, sum(tech_comp)::integer as tech_comp, max(non_tech_comp)::integer as non_tech_comp, max(cc.discount):: integer as discount
  from (
    select b.year_month, a.tech_number, 
      sum(flag_hours) filter (where service_type <> 'OT') as flag_exc_ot, 
      sum(flag_hours) filter (where service_type = 'OT') as flag_ot,
      sum(flag_hours) as flag_total,
      sum(labor_sales) as sales, 
      sum(labor_cost) as the_cost,
      max(d.comp) as tech_comp
    from jon.ro_details a
    join dds.dim_date b on a.final_close_date = b.the_date
      and b.the_year in (2019, 2020)
    left join jon.tech_comp d on a.tech_number = d.tech_number  
      and b.year_month = d.year_month
    group by b.year_month, a.tech_number) aa
  left join (
    select year_month, sum(comp) as non_tech_comp
    from jon.non_tech_comp
    group by year_month) bb on aa.year_month = bb.year_month  
  left join (
    select b.year_month, sum(a.discount)::integer as discount 
    from jon.ro_details a
    join dds.dim_date b on a.final_close_date = b.the_date
      and b.year_month >= 201901
    group by b.year_month) cc on aa.year_month = cc.year_month 
  group by aa.year_month) dd
order by year_month  


-- 10/27/20 for jeri
select year_month, sales, tech_comp, non_tech_comp,
  (100*(sales - tech_comp)/sales)::integer as ret_1,
  (100*(sales - (tech_comp + non_tech_comp))/sales)::integer as ret_2
--   (100*((sales + discount) - tech_comp)/(sales + discount))::integer as ret_3,
--   (100*((sales + discount) - tech_comp + non_tech_comp)/(sales + discount))::integer as ret_4
from (
  select aa.year_month, sum(sales)::integer as sales, sum(tech_comp)::integer as tech_comp, max(non_tech_comp)::integer as non_tech_comp, max(cc.discount):: integer as discount
  from (
    select b.year_month, a.tech_number, 
      sum(flag_hours) filter (where service_type <> 'OT') as flag_exc_ot, 
      sum(flag_hours) filter (where service_type = 'OT') as flag_ot,
      sum(flag_hours) as flag_total,
      sum(labor_sales) as sales, 
      sum(labor_cost) as the_cost,
      max(d.comp) as tech_comp
    from jon.ro_details a
    join dds.dim_date b on a.final_close_date = b.the_date
      and b.the_year in (2019, 2020)
    left join jon.tech_comp d on a.tech_number = d.tech_number  
      and b.year_month = d.year_month
    group by b.year_month, a.tech_number) aa
  left join (
    select year_month, sum(comp) as non_tech_comp
    from jon.non_tech_comp
    group by year_month) bb on aa.year_month = bb.year_month  
  left join (
    select b.year_month, sum(a.discount)::integer as discount 
    from jon.ro_details a
    join dds.dim_date b on a.final_close_date = b.the_date
      and b.year_month >= 201901
    group by b.year_month) cc on aa.year_month = cc.year_month 
  group by aa.year_month) dd
order by year_month  