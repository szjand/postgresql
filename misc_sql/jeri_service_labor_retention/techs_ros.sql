﻿-- techs
-- jeri was working off the labor profit analysis report, selected all techs, then "just knew" which techs were main shop
-- need a daily census from jan 2019

-- not sure if i need a daily census so much as the ability to identify techs appearing on ros for any given date
-----------------------------------------------------------------------------
--< build the tables  10/12 don't remember why i thought i needed a tech_days table, oh well
-- but it does seem a reasonable way to generate a list of techs
-----------------------------------------------------------------------------
drop table if exists tech_days cascade;
create temp table tech_days as
select a.the_date, b.tech_key, b.employee_number, b.tech_name, b.tech_number, b.labor_cost, b.flag_Department_code
from dds.dim_date a
left join dds.dim_tech b on a.the_date between row_from_Date and row_thru_date
  and b.active
where a.the_date between '01/01/2019' and current_date
  and b.store_code = 'RY1'
  and b.flag_department_code not in ('QL','RE','BS','NA','HS')
  and b.tech_name is not null;
create unique index on tech_days(the_date,tech_key);   
create unique index on tech_days(the_date,tech_number); 
create unique index on tech_days(the_date,tech_name);
create unique index on tech_days(the_date,employee_number);


drop table if exists techs cascade;
create temp table techs as 
select tech_key, employee_number, tech_name, tech_number, labor_cost, flag_Department_code
from tech_days
group by tech_key, employee_number, tech_name, tech_number, labor_cost, flag_Department_code;
alter table techs add primary key (tech_key);

-- none of these techs has more than on tech number
select tech_name
from (
  select tech_name, tech_number
  from techs
  group by tech_name, tech_number) a
group by tech_name
having count(*) > 1

-- include service type OT
drop table if exists ro_details cascade;
create temp table ro_details as
select a.ro, a.line, a.close_date, b.tech_key, b.tech_number, c.service_type_code as service_type, d.payment_type_code as payment_type, sum(flag_hours) as flag_hours
from dds.fact_repair_order a
join techs b on a.tech_key = b.tech_key
join dds.dim_service_type c on a.service_type_key = c.service_type_key
  and c.service_type_code in ('AM','EM','MN','MR','QS','SV','OT')
join dds.dim_payment_type d on a.payment_type_key = d.payment_type_key
where a.close_date between '01/01/2019' and current_date
group by a.ro, a.line, a.close_date, b.tech_key, c.service_type_code, d.payment_type_code;
alter table ro_details add primary key(ro,line,tech_key);
create index on ro_details(ro);
create index on ro_details(close_date);
create index on ro_details(service_type);
create index on ro_details(payment_type);
create index on ro_details(tech_number);

drop table if exists ro_numbers cascade;
create temp table ro_numbers as
select distinct ro 
from ro_details;
alter table ro_numbers add primary key(ro);

-----------------------------------------------------------------------------
--< build the tables
-----------------------------------------------------------------------------


-- compare to jeri: tech list is a bit different, but hours look close enough
-- separates out OT hours
select aa.*, sum(aa.flag_hours) over (partition by aa.tech_number, aa.year_month)
from (
  select year_month, c.tech_number, c.tech_name, 
    sum(a.flag_hours) filter (where service_type <> 'OT') as flag_hours,
    sum(a.flag_hours) filter (where service_type = 'OT') as ot_flag_hours,
    sum(a.flag_hours) as total
  from ro_details a
  join dds.dim_date b on a.close_Date = b.the_date
    and b.year_month in (202002, 202003)
  join techs c on a.tech_key = c.tech_key
  group by year_month, c.tech_number, c.tech_name) aa
order by tech_number, year_month


as i should have remembered, no way to allocate labor sales from accounting to the tech level
but, did generated the questions on ro 16390488: sales appeared to be generated incorrectly
need to generate sales from opcodes and pricing table

select a.* 
from ro_details a
join dds.dim_date b on a.close_date = b.the_date
  and b.year_month = 202003

select * from dds.dim_op_code_ext limit 400


select a.ro, a.line, a.close_date, b.tech_key, c.service_type_code as service_type, 
  d.payment_type_code as payment_type, sum(flag_hours) as flag_hours
from dds.fact_repair_order a
join techs b on a.tech_key = b.tech_key
join dds.dim_service_type c on a.service_type_key = c.service_type_key
  and c.service_type_code in ('AM','EM','MN','MR','QS','SV','OT')
join dds.dim_payment_type d on a.payment_type_key = d.payment_type_key
where a.close_date between '01/01/2019' and current_date
group by a.ro, a.line, a.close_date, b.tech_key, c.service_type_code, d.payment_type_code;


select * 
from dds.fact_repair_order a
-- join dds.dim_op_code_ext b on a.opcode_key = b.opcode_key
-- join dds.dim_op_code_ext c on a.cor_code_key = c.opcode_key
where a.ro = '16390488'

-- so, i need the techs labor rate, the opcode & corcode and the retail methods and retail amounts
select *
from ro_details a
left join tech_days b on a.close_date = b.the_date
  and a.tech_key = b.tech_key
where a.close_date between '03/01/2020' and '03/31/2020'

-- 
-- -- wait, can i just use the line labor sales, lets compare that to accounting
-- 
-- select * from (
-- select a.ro, ro_labor_sales as the_ro, sum(labor_sales) the_sum
-- from dds.fact_repair_order a
-- where a.close_date between '03/01/2020' and '03/31/2020'
-- group by a.ro, a.ro_labor_sales
-- ) x where the_ro <> the_sum
-- -- ok, i can use ro_flag_hours
-- 
-- 
-- drop table if exists the_ros;
-- create temp table the_ros as
-- select a.ro, ro_labor_sales as the_ro
-- from dds.fact_repair_order a
-- join techs b on a.tech_key = b.tech_key
-- join dds.dim_service_type c on a.service_type_key = c.service_type_key
--   and c.service_type_code in ('AM','EM','MN','MR','QS','SV')
-- where a.close_date between '03/01/2020' and '03/31/2020'
--   and a.ro_labor_sales <> 0
-- group by a.ro, a.ro_labor_sales;
-- 
-- drop table if exists gl;
-- create temp table gl as
-- select a.control, sum(-amount) as sales
-- from fin.fact_gl a
-- join (
--   select ro
--   from the_ros
--   group by ro) b on a.control = b.ro
-- join sad.labor_accounts c on a.account_key = c.account_key  
--   and c.account_Type = 'sale'
-- where a.post_status = 'Y'  
-- group by a.control;
-- 
-- -- ok, this is "better" 330 rows with differences
-- -- body shop ros are off because only some lines are of the desired service types and gl is not at that grain
-- select *, sales - the_ro
-- from gl a
-- full outer join the_ros b on a.control = b.ro
-- where coalesce(a.sales, 0) <> coalesce(b.the_ro, 0)
--   and a.control not like '18%'
--   and abs(sales - the_ro) > 100
-- order by abs(sales - the_ro) desc nulls last
-- 
-- 16387367 this ro (and lpa) are 816.94 short, june caught and added it to accounting
-- 
-- lets look at 16389289, ro > sales by 449.82
-- looks like warranty reversal
-- 
-- 
-- so what is the deal with a shit load of ros being 45.94 short? PDI, service type RE
-- 35.77 pdq service type QL
-- 
-- select * from ro_details a join techs b on a.tech_key = b.tech_key where ro = '16389289'

------------------------------------------------------------------------------------
--< costing
------------------------------------------------------------------------------------

time to look at costing, jeri pulled it from lpa

-- not in fact repair order
select * from dds.fact_repair_order limit 10

-- don't have the data in sdprdet, but we do have august, do that lpa and check it out
select distinct transaction_date from arkona.ext_sdprdet order by transaction_date

select * from arkona.ext_sdprdet limit 10
-- cost is line_type L & transaction_Code TT

trying to match the labor profit analysis is a rabbit hole, 
so i moved all that stuff to .../rabbit_hole_matching_labor_profit_analysis


now go back and add cost to ro_details using arkona.ext_sdprdet_history -- should actually be in dds.fact_repair_order



drop table if exists ro_details cascade;
create temp table ro_details as
select a.ro, a.line, a.close_date, b.tech_key, c.service_type_code as service_type, d.payment_type_code as payment_type, sum(flag_hours) as flag_hours
from dds.fact_repair_order a
join techs b on a.tech_key = b.tech_key
join dds.dim_service_type c on a.service_type_key = c.service_type_key
  and c.service_type_code in ('AM','EM','MN','MR','QS','SV','OT')
join dds.dim_payment_type d on a.payment_type_key = d.payment_type_key
where a.close_date between '01/01/2019' and current_date
group by a.ro, a.line, a.close_date, b.tech_key, c.service_type_code, d.payment_type_code;
alter table ro_details add primary key(ro,line,tech_key);
create index on ro_details(ro);
create index on ro_details(close_date);
create index on ro_details(service_type);
create index on ro_details(payment_type);

-- need to work out some granularity 
select ro_number, line_number from (
select ro_number, line_number, service_type
from arkona.ext_sdprdet_history a
join ro_numbers b on a.ro_number = b.ro
where a.line_type = 'A'
group by ro_number, line_number, service_type
) x group by ro_number, line_number having count(*) > 1

select *
from arkona.ext_sdprdet_history a
join ro_numbers b on a.ro_number = b.ro

select * from ro_details limit 10

drop table if exists tech_cost;
create temp table tech_cost as
select a.ro_number, a.technician_id, a.line_number, sum(a.cost) as the_cost
from arkona.ext_sdprdet_history a
join ro_numbers b on a.ro_number = b.ro
where a.labor_hours + cost <> 0
  and a.transaction_code = 'TT'
  and a.line_type = 'L'
group by a.ro_number, a.technician_id, a.line_number;

-- added tech_number to ro_details
-- cool , we are good, counts and amounts remain the same
select count(*), sum(flag_hours)
from ro_details a
left join tech_cost b on a.ro = b.ro_number and a.line = b.line_number and a.tech_number = b.technician_id


-------------------------
-- 10/12/20 trying to get back into this
-------------------------

20140502 - 20200916
select min(transaction_Date), max(transaction_date) from arkona.ext_sdprdet_history where transaction_date <> 0 

select extract(year from dds.db2_integer_to_date(transaction_Date)), count(*)
from arkona.ext_sdprdet_history
where transaction_Date <> 0
group by extract(year from dds.db2_integer_to_date(transaction_Date))
order by extract(year from dds.db2_integer_to_date(transaction_Date))

select * from arkona.ext_sdprdet_history limit 100

select * from ro_details limit 100

select * from tech_cost limit 100

-- trying to match labor profit analysis report from dealertrack
-- lets try final close date, hmmm, lets try both
-- none of these 3 match the lpa, close, final_close or both
-- submitted a case
drop table if exists ro_details cascade;
create temp table ro_details as
select a.ro, a.line, a.close_date, a.final_close_date, b.tech_key, c.service_type_code as service_type, d.payment_type_code as payment_type, sum(flag_hours) as flag_hours
from dds.fact_repair_order a
join techs b on a.tech_key = b.tech_key
join dds.dim_service_type c on a.service_type_key = c.service_type_key
  and c.service_type_code in ('AM','EM','MN','MR','QS','SV','OT')
join dds.dim_payment_type d on a.payment_type_key = d.payment_type_key
where (a.final_close_date between '01/01/2019' and current_date or a.close_date between '01/01/2019' and current_date)
group by a.ro, a.line, a.close_date, a.final_close_date, b.tech_key, c.service_type_code, d.payment_type_code;
alter table ro_details add primary key(ro,line,tech_key);
create index on ro_details(ro);
create index on ro_details(close_date);
create index on ro_details(final_close_date);
create index on ro_details(service_type);
create index on ro_details(payment_type);



-----------------------------------------------
-- 10/16/20
-----------------------------------------------
the premise: operational data will simply have to be good enough
costing operational data is not currently persisted in fact.repair_order
get it from sdprdet, not currently persisting history, so i need to do that


all this to convince me to do an sdprdet scrape nightly of 180 days, delete those ros
from arkona.ext_sdprdet_history and replace them with the freshly scraped rows


select extract(year from b.open_date), count(*)
from arkona.ext_sdprdet_history a
join dds.fact_repair_order b on a.ro_number = b.ro
group by extract(year from b.open_date)
order by extract(year from b.open_date)
2015;3496229
2016;3318328
2017;3163071
2018;3383841
2019;3448091
2020;2229598

select left(transaction_date::text, 4), count(*)
from arkona.ext_Sdprdet_history
group by left(transaction_date::text, 4)

so, i am thinking nightly update past 180 days (wimped out, thought of a year, but balked at 1 million rows every night)
scrape into arkona.ext_sdprdet_history_tmp;

select * from arkona.ext_sdprdet_history limit 10

select count(*)  -- 302778
from arkona.ext_sdprdet_history a
join arkona.ext_sdprdet_history_tmp b on a.ro_number = b.ro_number and a.line_number = b.line_number

select count(distinct ro_number) from arkona.ext_sdprdet_history_tmp -- 21820

select ro_number, line_number, sequence_number
from arkona.ext_sdprdet_history_tmp       
order by ro_number, line_number, sequence_number          

select count(distinct ro_number)
from arkona.ext_sdprdet_history a
where ro_number in (
  select ro_number
  from arkona.ext_sdprdet_history_tmp)      

for the ros in ext, is there any data not in tmp

drop table if exists ext;
create temp table ext as
select ro_number, line_number, sequence_number, md5(a::text) as hash
from arkona.ext_sdprdet_history a
where ro_number in (
  select ro_number
  from arkona.ext_sdprdet_history_tmp);

drop table if exists tmp;
create temp table tmp as
select ro_number, line_number, sequence_number, md5(a::text) as hash
from arkona.ext_sdprdet_history_tmp a;

-- for the 5 rows that this returns, none of these currently exists in rydedata.sdprdet
-- ie, deleted data
-- which modestly reassures me that rather than missing data if i replace data by ro, i am actually cleaning up data
-- getting rid of deleted data
select *
from ext a
left join tmp b on a.ro_number = b.ro_number and b.line_number = b.line_number and a.sequence_number = b.sequence_number
where b.line_number is null




create or replace function arkona.ext_sdprdet_history_update()
returns void as
$BODY$
/*
  180 days of sdprdet data (based on ro open_date)
*/

delete 
from arkona.ext_sdprdet_history
where ro_number in (
  select ro_number
  from arkona.ext_sdprdet_history_tmp);

insert into arkona.ext_sdprdet_history (company_number,ro_number,line_number,line_type,
  sequence_number,transaction_code,transaction_date,line_payment_method,service_type,
  policy_adjustment,technician_id,labor_operation_code,correction_code,labor_hours,
  labor_amount,cost,actual_retail_flag,failure_code,sublet_vendor_number,
  sublet_invoice_number,discount_key,discount_basis,vat_code,vat_amount,
  dispatch_priority_1,dispatch_priority_2,dispatch_rank_1,dispatch_rank_2,
  policy_tax_amount,kit_taxable_amount)
select company_number,ro_number,line_number,line_type,
  sequence_number,transaction_code,transaction_date,line_payment_method,service_type,
  policy_adjustment,technician_id,labor_operation_code,correction_code,labor_hours,
  labor_amount,cost,actual_retail_flag,failure_code,sublet_vendor_number,
  sublet_invoice_number,discount_key,discount_basis,vat_code,vat_amount,
  dispatch_priority_1,dispatch_priority_2,dispatch_rank_1,dispatch_rank_2,
  policy_tax_amount,kit_taxable_amount
from arkona.ext_sdprdet_history_tmp;
 
$BODY$
language sql;

comment on function arkona.ext_sdprdet_history_update() is 'run nightly from luigi.ext_arkona.py class ExtSdprdetHistory invoked by
  class MiscArkonaTables invoked by run_all.py';
  

-- lets trim table techs way down, just need to identify the tech on the ro/line
-- 10/19/20 discovered that desiree's default service type had been changed to QL, talked to Andrew, changed it back to MR
--    exclude craig rogne an caden aker
drop table if exists jon.techs;
create table jon.techs as
select c.employee_name, b.tech_number, b.employee_number,  array_agg(distinct b.tech_key) as tech_key
from dds.dim_date a
left join dds.dim_tech b on a.the_date between row_from_Date and row_thru_date
  and b.active
join arkona.ext_pymast c on b.employee_number = c.pymast_employee_number  
  and c.pymast_employee_number not in ('10568', '1117960')  -- exclude craig rogne and caden aker
where a.the_date between '01/01/2019' and current_date
  and b.store_code = 'RY1'
  and case 
        when b.tech_number = '588' then b.flag_department_code not in ('RE','BS','NA','HS')
        else b.flag_department_code not in ('QL','RE','BS','NA','HS')
      end
  and b.tech_name is not null
group by b.tech_name, b.tech_number, b.employee_number, c.employee_name; 
alter table jon.techs add primary key(tech_number);
CREATE INDEX on jon.techs USING GIN (tech_key);

-- add sales and cost
-- 10/18/20 - add min/max acct date, take them out, does not appear to be the answer
--  plus jacked up the other values
-- drop table if exists ro_details cascade;
-- create temp table ro_details as
-- the join on sdprdet is too naive, does not cover a tech with multiple entries (different corr codes) per line, eg, 16424200
-- using min & max acct dates was no help, try labor accounts only
-- does not look promising, include flag date, no help
-- added acct dates based on labor accounts, no help
-- sent an email to Bob Kiholm
drop table if exists jon.ro_base;
create table jon.ro_base as
select a.ro, a.line, a.open_date, a.close_date, a.final_close_date, b.tech_number, c.service_type_code as service_type, 
  d.payment_type_code as payment_type, sum(flag_hours) as flag_hours, sum(a.labor_sales) as labor_sales
from dds.fact_repair_order a
join jon.techs b on a.tech_key  = any(b.tech_key)
join dds.dim_service_type c on a.service_type_key = c.service_type_key
  and c.service_type_code in ('AM','EM','MN','MR','QS','SV','OT')
join dds.dim_payment_type d on a.payment_type_key = d.payment_type_key
-- left join arkona.ext_sdprdet_history e on a.ro = e.ro_number
--   and a.line = e.line_number
--   and b.tech_number = e.technician_id
where a.final_close_date between '01/01/2019' and current_date
group by a.ro, a.line, a.open_date, a.close_date, a.final_close_date, b.tech_number, c.service_type_code, d.payment_type_code;
alter table jon.ro_base add primary key(ro,line,tech_number);
-- create index on jon.ro_base(ro);
-- create index on jon.ro_base(close_date);
-- create index on jon.ro_base(service_type);
-- create index on jon.ro_base(payment_type);

-- select * from jon.ro_base limit 100

-- drop table if exists jon.acct_dates;
-- create table jon.acct_dates as
-- select a.control, array_agg(distinct c.the_date) as acct_date
-- from fin.fact_gl a
-- join fin.dim_Account b on a.account_key = b.account_key
--   and account_type = 'sale'
--   and store_code = 'ry1'
--   and b.department_code = 'sd'
-- join dds.dim_date c on a.date_key = c.date_key  
-- join jon.ro_base d on a.control = d.ro
-- group by a.control;


-- add discounts
drop table if exists jon.ro_details;
create table jon.ro_details as 
select aa.*, coalesce(bb.labor_cost, 0) as labor_cost, coalesce(cc.ro_discount) as discount
from jon.ro_base aa
left join (
  select b.ro, b.line, b.tech_number, sum(a.cost) as labor_cost
  from arkona.ext_sdprdet_history a
  join jon.ro_base b on a.ro_number = b.ro
   and a.line_number = b.line
   and a.technician_id = b.tech_number
  group by b.ro, b.line, b.tech_number) bb on aa.ro = bb.ro
    and aa.line = bb.line
    and aa.tech_number = bb.tech_number
left join (
  select a.ro, a.ro_discount
  from dds.fact_repair_order a
  join jon.ro_base b on a.ro = b.ro
  group by a.ro, a.ro_discount) cc on aa.ro = cc.ro;  
alter table jon.ro_details add primary key(ro,line,tech_number);
create index on jon.ro_details(ro);
create index on jon.ro_details(close_date);
create index on jon.ro_details(service_type);
create index on jon.ro_details(payment_type);

-- select * from jon.ro_details limit 100

select tech_number,payment_type, count(distinct ro) as ros, sum(flag_hours) as flag, sum(labor_sales) as sales, 
sum(labor_cost) as cost, max(close_Date) as close_Date, max(final_close_date) as final_close_Date, max(b.acct_date) as acct_date, max(flag_date)
from jon.ro_Details a
join jon.acct_dates b on a.ro = b.control
  and '10/14/2020' = any(b.acct_date)
group by tech_number, payment_type
order by tech_number, payment_type


-- at the end of 10/18/20, found some old notes that say use final_close_date and exclude the .2 hours for inspection
-- let's check that out
-- this appears to be true, 23I shows up as .2 hours customer pay, with 0 sales or cost in
--    the labor profit analysis, tho the cost does show up in accounting
select a.ro, sum(flag_hours), sum(labor_sales), sum(labor_cost)
from dds.fact_repair_order a
join dds.dim_opcode b on a.opcode_key = b.opcode_key
--   and b.opcode = '23i'
join dds.dim_tech c on a.tech_key = c.tech_key  
  and c.tech_number = '647'
where ro in ('16424500','16424673','16424690')
group by ro

all right, enough of this until i hear back from bob k


------------------------------------------------------------------------------------
--/> costing
------------------------------------------------------------------------------------
