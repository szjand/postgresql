﻿-- -- opcodes from ontrack
-- Elements	    Sample
-- CompanyNumber	The DMS company number
-- OperationCode	The labor operation code
-- Description1	The text description of the labor op code
-- Description2	The second line of the text description
-- Description3	The third line of the text description
-- Description4	The fourth line of the text description
-- MajorCode	    Y/N Indicates whether or not this labor op code prompts for an extended description when add to the repair order
-- MinorCode	    Not used
-- Manufacturer	Manufacturer is the 2 digit code, we use to identify the manufacturer inside the DMS. Make is the Make of the vehicle. Manufacture may be filled in on Correction Codes, Make, Model and Engine code are blank from what I am seeing. If they are filled in it would be from Correction codes for Warranty.
-- Make	        Vehicle make this labor op code applies to. May come from the OEM if we import Labor Op codes for Correction Codes
-- Model	        Vehicle model this labor op code applies to. May come from the OEM if we import Labor Op codes for Correction Codes
-- Engine	      Engine type this labor op code applies to. May come from the OEM if we import Labor Op codes for Correction Codes
-- Year	        Y/N Indicates whether or not this labor op code is active
-- DefLinePaymentMethod	Default line payment method for this labor op code
--                       C Customer pay
--                       CP Customer pay
--                       I Internal
--                       W Warranty
--                       S Service contract
-- CorrectionLOP	Y/N Indicates whether or not this labor op code is a an OEM correction code
-- Type	        Not used
-- ExcludeDiscount	Y/N Indicates whether or not this labor op code should be excluded from discounts.
-- SkillLevel	  A, B, C, or D. This will correspond with the Pricing Table to determine the labor rate level used when this labor operation code is added to a Repair Order.
-- Taxable	      Y/N Indicates whether or not this labor op code is subject to sales tax
-- HazardMaterialCharge	The dollar amount of the hazardous material charge associated with this labor op code
-- ShopSupplyCharge	Y/N Indicates whether or not labor and parts billed to this labor op should be subject to shop supplies
-- RetailMethod	Indicates how to compute the labor charge for this labor op code
--                 A Actual retail amount
--                 B Book
--                 C Other
-- RetailAmount	The flat dollar amount to charge for labor on this labor op code
-- CostMethod	  Indicates how to compute the labor cost for this labor op code
--                 A Actual
--                 B Book
--                 P Percent
-- CostAmount	  The flat dollar amount to charge as the cost of labor on this op code
-- PartsMethod	  Indicates how to compute the parts amount for this labor op code; not currently used
--                 A Actual retail amount
--                 B Book
-- PartsAmount	  The estimated dollar amount of parts that will be applied to this labor op code
-- Estimate	    Y/N Indicates whether or not an estimate is required when this labor op is used on a repair order
-- EstimatedHours	The estimated labor hours for this labor operation code
-- WaiterFlag	  Y/N Indicates whether ornot a repair order with this labor op code can be flagged as a waiter
-- PredefinedCauseDescription	The default cause description for this labor op code.
-- AdditionalDescriptionLine1	The extended description of the labor op code
-- AdditionalDescriptionLine2	The extended description of the labor op code
-- PredefinedComplaintDescription	The default complaint description for this labor op code
-- DefaultServiceType	This field is used to set the default Service Type that will be applied to the line when this Labor Op Code is added to an Appointment or a Repair Order. For example, "MR", "QL", "BS", etc.


drop table if exists dds.dim_op_code_ext cascade;
CREATE TABLE dds.dim_op_code_ext(
  opcode_key bigint NOT NULL references dds.dim_opcode(opcode_key),
  store_code citext,
  opcode citext,
  description citext,
  pdqcat1 citext,
  pdqcat2 citext,
  pdqcat3 citext,
  manufacturer citext, 
  active citext, 
  is_correction citext,
  exclude_discount citext, 
  hazard_material_charge numeric(8,2),
  shop_supply_charge citext,
  retail_method citext,
  retail_amount numeric(8,2),
  cost_method citext,
  cost_amount numeric(8,2),
  parts_method citext,
  parts_amount numeric(8,2),
  default_service_type citext,
  PRIMARY KEY (opcode_key));
create unique index on dds.dim_op_code_ext(store_code,opcode);
comment on table dds.dim_op_code_ext is 'added pricing & costing fields from sdplopc to dds.dim_opcode as part of the labor gross retenion project';  

-- add this query to dds.dim_opcode_update(), 
insert into dds.dim_op_code_ext
select a.opcode_key, store_code, opcode, description, pdqcat1, pdqcat2, pdqcat3, 
  b.manufacturer, b.year as active, b.correction_lop, b.exclude_discount, b.hazard_mater_chg, b.shop_supply_chg, 
  b.retail_method, b.retail_amount, b.cost_method, b.cost_amount, b.parts_method, b.parts_amount, b.default_service_type  
from dds.dim_opcode a
left join arkona.ext_sdplopc b on a.store_code = b.company_number
  and a.opcode = b.operaction_code
where operaction_code not in ('d1999','e9425','j7507','j7921','v9665','3105h4','r06161');


