﻿/*
10/12
pulled this out of the techs_ros.sql file
inconclusive at best
compared the list of ros from labor profit analysis to fact_repair_order (including both closed and final closed dates)
for this one tech (528) for this one month (202008) final close date is the closest
submitted a case to dealertrack, asking for clarification, we will see.
*/

-- from labor profit analysis
drop table if exists lpa_528_august_ros;
create temp table lpa_528_august_ros as
select '16398247' as ro union
select '16403718' as ro union
select '16408207' as ro union
select '16409084' as ro union
select '16410356' as ro union
select '16410392' as ro union
select '16410409' as ro union
select '16410432' as ro union
select '16410600' as ro union
select '16410789' as ro union
select '16410977' as ro union
select '16411004' as ro union
select '16411051' as ro union
select '16411058' as ro union
select '16411114' as ro union
select '16411120' as ro union
select '16411132' as ro union
select '16411147' as ro union
select '16411410' as ro union
select '16411414' as ro union
select '16411430' as ro union
select '16411520' as ro union
select '16411576' as ro union
select '16413373' as ro union
select '16413566' as ro union
select '16413587' as ro union
select '16413598' as ro union
select '16413774' as ro union
select '16413827' as ro union
select '16413831' as ro union
select '16413930' as ro union
select '16414045' as ro union
select '16414104' as ro union
select '16414314' as ro union
select '16414328' as ro union
select '16414409' as ro union
select '16414419' as ro union
select '16414427' as ro union
select '16414438' as ro union
select '16414503' as ro union
select '16414511' as ro union
select '16414689' as ro union
select '16415000' as ro union
select '16415069' as ro union
select '16415097' as ro union
select '16415259' as ro union
select '16415401' as ro union
select '16415618' as ro union
select '16415774' as ro union
select '16415839' as ro;

-- missing from lpa: 16402653
-- close date out of range: 16402653, 16403718, 16410392, 16410356, 16410432, 16410409
-- final close date out of range: 16414409
select * 
from (
select ro, close_Date, final_close_date  
from dds.fact_repair_order a
join dds.dim_tech b on a.tech_key = b.tech_key
  and b.tech_number = '528'
where close_date between '08/01/2020' and '08/31/2020'
  or final_close_date between '08/01/2020' and '08/31/2020'
group by ro, close_Date, final_close_date) aa
full outer join lpa_528_august_ros bb on aa.ro = bb.ro
order by aa.final_close_date

-- 10/18/2020
-- tried using accounting date
-- this query exposes close_date, final_close_date, acct_date, 

-- on the very first tech, 233, LPA shows 1 W RO for 1.7 hours, which matches final_close_date, but final_close date 
--    also includes 1 C RO not in LPA
-- tech 557 LPA 4 C ros for 5.2 hours, matched only by close date
-- tech 566 LPA 3 C ros for 1.8 hours, matched only by close date, but close date also shows 3 W Ros for 7.1 hours not on LPA
select *
from (
  select aa.tech_number, aaa.pay_type, bb.ros as cd_ros, bb.flag as cd_flag,
    cc.ros as fcd_ros, cc.flag as fcd_flag,
    dd.ros as a_ros, cc.flag as a_flag
  from jon.techs aa
  cross join (select 'I' as pay_type union select 'C' union select 'W' union Select 'S') aaa
  left join (
    select tech_number,payment_type, count(distinct ro) as ros, sum(flag_hours) as flag
    from jon.ro_Details a
    WHERE a.close_date = '10/14/2020'
    group by tech_number, payment_type) bb on aa.tech_number = bb.tech_number and aaa.pay_type = bb.payment_type
  left join (
    select tech_number,payment_type, count(distinct ro) as ros, sum(flag_hours) as flag
    from jon.ro_Details a
    where a.final_close_date = '10/14/2020'
    group by tech_number, payment_type) cc on aa.tech_number = cc.tech_number and aaa.pay_type = cc.payment_type
  left join (
    select tech_number,payment_type, count(distinct ro) as ros, sum(flag_hours)
    from jon.ro_Details a
    join jon.acct_dates b on a.ro = b.control
      and '10/14/2020' = any(b.acct_date)
    group by tech_number, payment_type) dd on aa.tech_number = dd.tech_number and aaa.pay_type = dd.payment_type) ee
where coalesce(cd_ros, 0) + coalesce(fcd_ros, 0) + coalesce(a_ros, 0)  <> 0  
order by tech_number, pay_type