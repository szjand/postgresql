﻿/*
create a daily snapshot

CREATE TABLE nrv.fake_ros (
  ro integer,
  customer citext,
  writer citext,
  status citext,
  payment_type citext,
  service_type citext,
  est_labor_hours_sold numeric,
  est_labor_gross numeric,
  est_parts_gross numeric,
  days_open integer,
  days_in_cashier_status integer)

on open ros, i am assuming that what we want is labor/parts that have not yet been entered into accounting
*/

select c.*
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.finalclosedatekey = b.date_key
left join arkona.ext_sdphist c on a.ro = c.repair_order_number
where a.ro = '16314916'
order by updated_Time

limit 1001
offset 5000

select *
from arkona.ext_sdphist
limit 100

create table ads.ext_dim_ro_status (
  ro_status_key integer primary key,
  ro_status_code citext not null,
  ro_status citext not null);

select *
from arkona.ext_pdptdet
where ptinv_ = '16314916'


select a.ro, a.line,
  case a.linestatuskey
    when 2 then 'closed'
    else 'open'
  end as line_status,
  c.the_date as open_date, d.the_date as close_date, b.the_date as final_close_date,
  e.ro_status, f.servicetype, g.paymenttype, h.fullname, a.flaghours, a.roflaghours
-- select a.*  
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.finalclosedatekey = b.date_key
inner join dds.dim_date c on a.opendatekey = c.date_key
inner join dds.dim_date d on a.closedatekey = d.date_key
inner join ads.ext_dim_ro_status e on a.rostatuskey = e.ro_status_key
  and e.ro_status_key = 5 -- cashier
inner join ads.ext_dim_service_type f on a.servicetypekey = f.servicetypekey
inner join ads.ext_dim_payment_type g on a.paymenttypekey = g.paymenttypekey
inner join ads.ext_dim_customer h on a.customerkey = h.customerkey
order by c.the_date
-- where a.ro = '16314916'  


select max(open_date)
from arkona.ext_sdprhdr
where ro_number = '16314916'

select * from ads.ext_dim_ro_status

select *
from arkona.ext_pdptdet
where ptinv_ = '16315901'

select a.ro, a.line,
  case a.linestatuskey
    when 2 then 'closed'
    else 'open'
  end as line_status,
  c.the_date as open_date, d.the_date as close_date, b.the_date as final_close_date,
  e.ro_status, f.servicetype, g.paymenttype, h.fullname, a.flaghours, j.the_date, i.amount, k.account, k.account_type, k.description, k.department
-- select a.*  
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.finalclosedatekey = b.date_key
inner join dds.dim_date c on a.opendatekey = c.date_key
  and c.year_month in (201806, 201807)
inner join dds.dim_date d on a.closedatekey = d.date_key
inner join ads.ext_dim_ro_status e on a.rostatuskey = e.ro_status_key
  and e.ro_status_key = 5 -- cashier
inner join ads.ext_dim_service_type f on a.servicetypekey = f.servicetypekey
inner join ads.ext_dim_payment_type g on a.paymenttypekey = g.paymenttypekey
inner join ads.ext_dim_customer h on a.customerkey = h.customerkey
left join fin.fact_gl i on a.ro = i.control
  and i.post_status = 'Y'
left join dds.dim_date j on i.date_key = j.date_key
left join fin.dim_Account k on i.account_key = k.account_key
--   and k.account_type = 'sale'
-- where a.ro = '16315901'
where h.fullname not in ('shop time','inventory')
order by c.the_date




SELECT c.the_date, a.storecode, a.ro, a.customerkey, a.servicewriterkey,
  a.rostatuskey
FROM ads.ext_fact_repair_order a
INNER JOIN dds.dim_date b on a.finalclosedatekey = b.date_key
  AND b.the_date > current_Date
INNER JOIN dds.dim_date c on a.opendatekey = c.date_key
GROUP BY c.the_date, a.storecode, a.ro, a.customerkey, a.servicewriterkey,
  a.rostatuskey


delete
FROM ads.ext_fact_repair_order
where ro in (
select ro_number
from arkona.ext_sdprhdr
where cust_name = '*VOIDED REPAIR ORDER*')


select max(open_date) from arkona.ext_sdprhdr limit 10


-- 7/9 a little clarity in the 9:30 today, only open lines (ie, not yet in accounting) are relevant

-- need tech
-- ro/line unique
select a.ro, a.line, linestatuskey,
  c.the_date as open_date, d.the_date as close_date, b.the_date as final_close_date,
  e.ro_status, f.servicetype, g.paymenttype, h.fullname, -- sum(a.flaghours) as flaghours
  a.flaghours, i.laborcost, laborsales
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.finalclosedatekey = b.date_key
  and b.the_date > current_date
inner join dds.dim_date c on a.opendatekey = c.date_key
inner join dds.dim_date d on a.closedatekey = d.date_key
inner join ads.ext_dim_ro_status e on a.rostatuskey = e.ro_status_key
inner join ads.ext_dim_service_type f on a.servicetypekey = f.servicetypekey
inner join ads.ext_dim_payment_type g on a.paymenttypekey = g.paymenttypekey
inner join ads.ext_dim_customer h on a.customerkey = h.customerkey
left join ads.ext_dim_tech i on a.techkey = i.techkey
where h.fullname not in ('shop time','inventory')
  and a.linestatuskey = 1
order by ro, line
group by a.ro, a.line,
  c.the_date, d.the_date, b.the_date,
  e.ro_status, f.servicetype, g.paymenttype, h.fullname

-- shows void in accounting, still open in pdpphdr and ui
select * from ads.ext_Fact_repair_order where ro = '2776342'
select * from fin.fact_gl where control = '2776342'
  
select * from ads.ext_dim_ro_status

select * from open_ro a
left join arkona.ext_sdphist b on a.ro = b.repair_order_number


select * from arkona.ext_sdphist limit 100


select * from ads.ext_fact_repair_order where ro = '16315572'



-- menu priced service
select a.*
from ads.ext_Fact_repair_order a
inner join dds.dim_date b on a.finalclosedatekey = b.date_key
  and b.the_date between '06/01/2018' and current_date
inner join ads.ext_dim_service_type c on a.servicetypekey = c.servicetypekey
  and c.servicetype = 'menu priced service'
inner join ads.ext_dim_opcode d on a.opcodekey = d.opcodekey
  and d.opcode = 'ALIGN'



sdpsvct: service types: accounting
sdpprice: labor rates

select * from ads.ext_dim_ro_status
select * from ads.ext_dim_service_type


select * from arkona.ext_pdptdet where ptinv_ = '16313888'
-- mechanical, can't seem to match parts (pdptdet) with ro
select * from ads.ext_fact_repair_order
where ro in (
select a.ro
from ads.ext_Fact_repair_order a
inner join dds.dim_date b on a.finalclosedatekey = b.date_key
  and b.the_date between '06/01/2018' and current_date
inner join ads.ext_dim_service_type c on a.servicetypekey = c.servicetypekey
  and c.servicetype = 'mechanical'
where roStatuskey = 1
  and storecode = 'ry1'
  and ropartssales > 100)
order by ro, line
  

-- what is the deal with pt 19162203 on line 999 ~ $600 ?!? not on ro
-- looking at part history, it was removed, ahh, ptcode = DP, ok, stick to lines, may be ok
select * from arkona.ext_pdptdet where ptinv_ = '16298324'
--ok, we are good now
select ptline, sum(ptqty * ptnet)
from arkona.ext_pdptdet where ptinv_ = '16298324'
group by ptline


-- 7/12/18 fuck it, going with a direct scrape of PDPPHDR/PDPPDET

create table jeri.open_ros (
  the_date date not null,
  ro citext not null,
  customer citext not null,
  writer citext not null,
  status citext not null,
  service_type citext not null,
  payment_type citext not null,
  est_labor_hours_sold numeric(6,2) not null default 0,
  est_labor_gross numeric(6,2) not null default 0,
  est_parts_gross numeric(6,2) not null default 0,
  days_open integer not null default 0,
  days_in_cashier_status integer not null default 0);
comment on column jeri.open_ros.the_date is 'date the row was generated';  
comment on table jeri.open_ros is 'direct daily scrape from arkona, PDPPHDR/PDPPDET';

.