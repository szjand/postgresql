﻿selecT * 
from arkona.xfm_bopname
where bopname_Search_name like '%unassig%'

selecT * 
from arkona.xfm_bopname
where bopname_Search_name like '%longer%'

select *
from arkona.ext_bopvref
where customer_key = 1150338

-------------------------------------------------------------------------------------
--< 07-19 combined unassigned & no longer owned to get a list of all vehicles 
-- where the owner has been modified by the "project"
-------------------------------------------------------------------------------------
select *
from arkona.ext_bopvref
where customer_key in (
  1149275,1149278,1152530,  -- no longer own
  1150338)  -- unassigned

--   S = Buyer on the Deal
--   2 = Co-Buyer on the Deal
--   C = Service Customer
--   T = Trade In

drop table if exists changed_owners cascade;
create temp table changed_owners as
select a.company_number, a.customer_key, a.vin, a.start_date, a.end_date,
  case a.type_code
    when 'S' then 'Buyer on the Deal'
    when '2' then 'Co-Buyer on the Deal'
    when 'C' then 'Service Customer'
    when 'T' then 'Trade In'
  end as type_code,
  c.bopname_search_name
from arkona.ext_bopvref a  
join (
  select *
  from arkona.ext_bopvref
  where customer_key in (
    1149275,1149278,1152530,  -- no longer own
    1150338)) b on a.vin = b.vin  -- unassigned
left join arkona.ext_bopname c on a.customer_key = c.bopname_record_key
  and a.company_number = c.bopname_company_number
  and c.company_individ is not null 
order by a.vin, a.start_date; 
create index on changed_owners(vin);
create index on changed_owners(customer_key); 

select * from changed_owners limit 10


1G1ZE5ST8HF187468: changed to unassigned on 07/06, last ro to owner was on 2/15/20
select * from arkona.xfm_inpmast where inpmast_vin = '1G1ZE5ST8HF187468' order by inpmast_key

select vin, ro_date, customer
from (
  select vin, customer, ro_date, row_number() over (partition by vin order by ro_date desc)
  from (
    select max(a.open_date) as ro_date, b.vin, e.bopname_search_name as customer
    from dds.fact_repair_order a
    join dds.dim_vehicle b on a.vehicle_key = b.vehicle_key
    join changed_owners c on b.vin = c.vin
    join dds.dim_customer d on a.customer_key = d.customer_key
    join arkona.xfm_bopname e on d.bnkey = e.bopname_record_key
      and e.company_individ is not null
  group by b.vin, e.bopname_search_name) x) y
where row_number = 1


    
select aa.*, bb.ro_date as last_ro_on, bb.customer as last_ro_for
from changed_owners aa
left join (
select vin, ro_date, customer
from (
  select vin, customer, ro_date, row_number() over (partition by vin order by ro_date desc)
  from (
    select max(a.open_date) as ro_date, b.vin, e.bopname_search_name as customer
    from dds.fact_repair_order a
    join dds.dim_vehicle b on a.vehicle_key = b.vehicle_key
    join changed_owners c on b.vin = c.vin
    join dds.dim_customer d on a.customer_key = d.customer_key
    join arkona.xfm_bopname e on d.bnkey = e.bopname_record_key
      and e.company_individ is not null
  group by b.vin, e.bopname_search_name) x) y
where row_number = 1) bb on aa.vin = bb.vin
order by aa.vin, aa.start_date    


select * from arkona.xfm_inpmast where inpmast_vin = '1GCVKREC4HZ306441' order by inpmast_key
-------------------------------------------------------------------------------------
--/> 07-19 
-------------------------------------------------------------------------------------


select *
from arkona.ext_bopvref
where vin = '1G1BE5SMXJ7100888'
order by start_date


select a.*
from arkona.ext_bopvref a
join arkona.ext_bopvref b on a.vin = b.vin
  and b.customer_key = 1150338
order by a.vin, a.start_date  


select bopname_company_number, bopname_record_key, bopname_search_name, address_1, phone_number
from arkona.ext_bopname
where bopname_record_key in (1009783, 280236)


select bopname_record_key, bopname_search_name
from arkona.ext_bopname
where bopname_search_name like 'ANDREWS, JON%'


select a.customer_key, a.vin, a.start_date, a.end_Date, a.type_code, a.salesperson, aa.bopname_Search_name, b.year, make, model, c.writer_name
from arkona.ext_Bopvref a
left join arkona.xfm_bopname aa on a.customer_key = aa.bopname_record_key
  and aa.current_row
left join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
left join dds.dim_Service_writer c on a.salesperson = c.writer_number
  and c.current_row  
where customer_key in (
  select bopname_record_key
  from arkona.ext_bopname
  where bopname_search_name like 'ANDREWS, JON%')
  order by start_date

select * from dds.dim_Service_writer where writer_number = '479'

-- bopvref types:
--   S = Buyer on the Deal
--   2 = Co-Buyer on the Deal
--   C = Service Customer
--   T = Trade In
select a.*, aa.bopname_Search_name, b.year, make, model, c.writer_name
from arkona.ext_Bopvref a
left join arkona.xfm_bopname aa on a.customer_key = aa.bopname_record_key
  and aa.current_row
left join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
left join dds.dim_Service_writer c on a.salesperson = c.writer_number
  and c.current_row  
where customer_key in (
  select bopname_record_key
  from arkona.ext_bopname
  where bopname_search_name like 'bruggeman, afton%')
  order by start_date

  
-- new records created
You can search for duplicates by:
Name
Home Phone
Business Phone
Street Address
Street Address & Zip Code



select * from arkona.xfm_pymast where pymast_employee_number = '170854'

/*
please refer to the accompanying file, which is the result set of this query:

select bopname_record_key, bopname_search_name, timestamp_created, 
  created_by_user, timestamp_updated, updated_by_user 
from arkona.ext_bopname 
where phone_number = '2187793664' 
  and updated_by_user <> 0

my question is, how do i identify the user(s) referenced in the fields created_by_user
and updated_by_user?

thanks

Jon Andrews
*/

drop table if exists yesterday cascade;
create temp table yesterday as
select bopname_company_number as store, bopname_record_key as key, bopname_Search_name as name, 
  address_1 as address, zip_code as zip, phone_number as phone,
  row_from_date
from arkona.xfm_bopname
where row_from_date = current_date - 1;
create index on yesterday(key);
create index on yesterday(name);
create index on yesterday(address);
create index on yesterday(phone);
-- -- how do i know these are all initial instances
-- -- this tells me nothing
-- select a.bopname_record_key, count(*)
-- from arkona.xfm_bopname a
-- where a.row_from_date = current_date - 1
-- group by  a.bopname_record_key

-- thats not it, what i need is are there dups based on name, phone, address
  
select a.*, b.bopname_record_key, b.bopname_search_name, b.address_1, b.zip_code, b.phone_number
from yesterday a
join arkona.ext_bopname b on a.name = b.bopname_search_name 
  and a.phone = b.phone_number
  and a.key <> b.bopname_record_key
  and a.name not like 'ALERUS%'
  and b.company_individ is not null
order by a.name  


-- bingo, this is definitelly what we are looking for
select bopname_record_key, bopname_search_name, timestamp_created::timestamp, 
  created_by_user, timestamp_updated::timestamp, updated_by_user 
-- select *  
from arkona.xfm_bopname
where phone_number = '2187792957' 
  and updated_by_user <> 0  

select * from arkona.xfm_bopname where bopname_record_key in( 315381, 224568) and current_row

select arkona.db2_integer_to_date(termination_date) from arkona.xfm_pymast where termination_date <> 0 order by arkona.db2_integer_to_date(termination_date) desc limit 20

select bopname_record_key, bopname_search_name, timestamp_created, 
  created_by_user, timestamp_updated, updated_by_user
from arkona.ext_bopname 
where phone_number = '2187793664' 
  and updated_by_user <> 0
  
-- this only returns ext_bopname & xfm_bopname looking for 101245694 in my records as updated_by_user
-- no luck with 170854 either
-- https://stackoverflow.com/questions/5350088/how-to-search-a-specific-value-in-all-tables-postgresql
-- horse with no names entry: https://dbfiddle.uk/?rdbms=postgres_9.6&fiddle=d36f3d68154864556ba821ccbd8ac93e
with found_rows as (
  select table_name,
         query_to_xml(format($$ select to_jsonb(t) as table_row 
                      from %I.%I as t 
                      where t::text like '%%101245694%%' $$, 
                      table_schema, table_name), true, false, '') as table_rows
  from information_schema.tables 
  where table_schema = 'arkona'
)
select table_name, r.data
from found_rows f
   cross join unnest(xpath('/table/row/table_row/text()', table_rows)) as r(data)   

-- 07/30/2020 its a whole new day, got the data from bob kilholm
drop table if exists arkona.user_cross_reference;
create table arkona.user_cross_reference (
  user_id citext,
  user_key bigint);
comment on table arkona.user_cross_reference is 'user_id: from sepuser, the user login, in cross reference file from 
Bob Kilholm field name is LIDDTC, user_key: hopefully this is the bopname.created_by_user, in the file field name is
USRIDDTC';   

drop table if exists arkona.ext_sepuser; 
create table arkona.ext_sepuser (
  store_code citext,
  user_id citext,
  user_name citext,
  first_name citext,
  last_name citext);
comment on table arkona.ext_sepuser is 'just a few fields from sepuser, when i tried to do a programmatic extract, it
did not bring down the user_names';

select * from arkona.ext_sepuser limit 10

select a.*, b.*, c.employee_first_name, c.employee_last_name
from arkona.user_cross_reference a
left join arkona.ext_sepuser b on a.user_id = b.user_id
left join arkona.ext_pymast c on b.last_name = c.employee_last_name
  and b.first_name = c.employee_first_name
order by a.user_key  
where a.user_key in (219633)

select *
from arkona.user_cross_reference
where user_key in (219633,399277,100448795)

/*
07/31/2020
Hey Jon,

I apologize.  I thought what was pulled was the Fusion User ID, but that was in fact a column called Dealertrack User ID 
number and has no bearing on what you are looking for.  I’ve put together a more comprehensive file to work from.  
The first column in this file is what will relate to BOPNAME.  

Now, one thing to note is that I added the first record to the file manually just so that you will have it, 
but it is not part of your RYDE users. It is a system user.  I will have to get more info on how that is 
specifically used, but that is actually the user that applies to your first example.  The other example you 
listed is found in your regular users  below in the file.

Hopefully I explained that ok.  Let me know if we need to make additional adjustments or if you come across
 any other users that you can’t find in this new list.


Thanks,

Bob Kiholm
RYDE Users.csv
*/

drop table if exists arkona.user_cross_reference cascade;
create table arkona.user_cross_reference (
  user_code bigint,
  user_name citext,
  first_name citext,
  last_name citext,
  user_key bigint);
comment on table arkona.user_cross_reference is 'data from file provided by Bob Kiholm (RYDE Users.csv).  user_code should by the value
referenced in bopname.created_by_user and bopname.updated_by_user';   

select a.*, b.*, c.employee_first_name, c.employee_last_name
from arkona.user_cross_reference a
left join arkona.ext_sepuser b on a.user_name = b.user_id
left join arkona.ext_pymast c on b.last_name = c.employee_last_name
  and b.first_name = c.employee_first_name
order by a.user_key  

select *
from arkona.ext_pymast
where employee_last_name = 'salberg'

select * 
from arkona.ext_sepuser
where user_id = 'gsolber'


drop table if exists yesterday cascade;
create temp table yesterday as
select bopname_company_number as store, bopname_record_key as key, bopname_Search_name as name, 
  address_1 as address, zip_code as zip, phone_number as phone,
  row_from_date
from arkona.xfm_bopname
where row_from_date = current_date - 1;
create index on yesterday(key);
create index on yesterday(name);
create index on yesterday(address);
create index on yesterday(phone);

select a.*, b.bopname_record_key, b.bopname_search_name, b.address_1, b.zip_code, b.phone_number
from yesterday a
join arkona.ext_bopname b on a.name = b.bopname_search_name 
  and a.phone = b.phone_number
  and a.key <> b.bopname_record_key
  and a.name not like 'ALERUS%'
  and b.company_individ is not null
order by a.name  


select a.bopname_record_key, a.bopname_search_name, a.timestamp_created::timestamp, 
  a.created_by_user, a.timestamp_updated::timestamp, a.updated_by_user, b.*, c.* 
-- select *  
from arkona.xfm_bopname a
left join arkona.user_cross_reference b on a.created_by_user = b.user_code
left join arkona.user_cross_reference c on a.updated_by_user = c.user_code
where phone_number = '7017412539' 
  and updated_by_user <> 0  
  and a.current_row

select *
from dds.dim_customer
where bnkey in (  290416,1150814)

select * 
from dds.fact_repair_order
where customer_key = 77152