﻿/*
from Alex in slack 3/14/23
Are you able to pull a .csv with employee name (first and last), employee number, their manager, location & department, job title, and email please?
I heard you’re the man to ask for this style of report. I built a new IT asset tracker on my ubuntu server and want to merge your data with what I currently have in there
*/





10:18
Also side question, are these reports able to be sent automatically? Assuming my initial request is possible




select a.last_name, a.first_name, a.employee_number, a.manager_1, a.store  -- 488
-- select * 
select manager_1, replace(manager_1, ',', ';')
from ukg.employees a
where status = 'active' limit 25

select manager_1, replace(manager_1, ',', ';'),
  substring(manager_1, position(',' in manager_1) + 2, 20) || ' ' ||
  left (manager_1, position(',' in manager_1) - 1)
--   right(manager_1, position(',' in manager_1) - 2)
from ukg.employees a
where status = 'active' limit 25


select * from ukg.get_cost_center_hierarchy()


-- select last_name || first_name from (
-- select a.last_name, a.first_name, a.employee_number, a.manager_1, a.store, b.*  -- 488
-- -- select * 
-- from ukg.employees a
-- left join (select * from ukg.get_cost_center_hierarchy()) b on a.cost_center = b.cost_center
--   and  
--     case
--       when a.store = 'GM' then b.store = 'Rydell GM'
--       when a.store = 'HN' then b.store = 'Rydell Honda Nissan'
--       when a.store = 'Toyota' then b.store = 'Rydell Toyota'
--     end
-- where a.status = 'active' 
--   and last_name <> 'api'
-- ) x group by last_name || first_name having count(*) > 1  
-- order by last_name  
-- 
-- select * from ukg.employees where employee_number = '123100'

-- sent the results of this
select a.last_name, a.first_name, a.employee_number, 
   substring(manager_1, position(',' in manager_1) + 2, 20) || ' ' || left (manager_1, position(',' in manager_1) - 1) as manager,
   a.store, b.department, a.cost_center, a.primary_email
from ukg.employees a
left join (select * from ukg.get_cost_center_hierarchy()) b on a.cost_center = b.cost_center
	and  
		case
			when a.store = 'GM' then b.store = 'Rydell GM'
			when a.store = 'HN' then b.store = 'Rydell Honda Nissan'
			when a.store = 'Toyota' then b.store = 'Rydell Toyota'
		end
	and
		case
			when b.cost_center = 'General Manager' then b.department = 'Corporate' else true
		end
where a.status = 'active' 
	and last_name <> 'api'
order by last_name, first_name  



select a.last_name, a.first_name, a.employee_number, 