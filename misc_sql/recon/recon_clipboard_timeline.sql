SELECT a.stocknumber, 
  max(b.thruts) AS inspected, timestampdiff(sql_tsi_minute, max(b.thruts), max(c.thruts))/60.0 AS hours,
  max(c.thruts) AS walked, timestampdiff(sql_tsi_minute, max(c.thruts), max(d.fromts))/60.0 AS hours,
  max(d.fromts) AS pulled, timestampdiff(sql_tsi_minute, max(d.fromts), max(e.fromts))/60.0 AS hours,
  max(e.fromts) AS pb, timestampdiff(sql_tsi_minute, max(e.fromts), max(f.fromts))/60.0 AS hours,
  max(f.fromts) AS avail
FROM VehicleInventoryItems a
LEFT JOIN VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  AND b.category = 'RMFlagIP'
LEFT JOIN VehicleInventoryItemStatuses c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
  AND c.category = 'RMFlagWP'
LEFT JOIN VehicleInventoryItemStatuses d on a.VehicleInventoryItemID = d.VehicleInventoryItemID 
  AND d.category = 'RMFlagPulled'  
LEFT JOIN VehicleInventoryItemStatuses e on a.VehicleInventoryItemID = e.VehicleInventoryItemID 
  AND e.category = 'RMFlagPB'  
LEFT JOIN VehicleInventoryItemStatuses f on a.VehicleInventoryItemID = f.VehicleInventoryItemID 
  AND f.category = 'RMFlagAV'  
WHERE a.stocknumber IN ('g38994aa','g38428xa','g39722x','g38852a','g34928xz','g38701x','g39097xa',
  'g38095b','g39254x','g38149ra','g39588x','g39660p','g39338a','g39255x','g39406xa')
GROUP BY a.stocknumber
ORDER BY a.stocknumber