﻿select category, status, count(*) 
from ads.ext_vehicle_inventory_item_statuses
where fromts::date > '12/31/2021'
group by category, status

select a.the_date, b.status -- , c.status
from dds.dim_date a
left join ads.ext_vehicle_inventory_item_statuses b on a.the_date = b.fromts::date
  and b.status = 'RMFlagPulled_Pulled'
-- left join ads.ext_vehicle_inventory_item_statuses c on a.the_date = c.fromts::date
--   and c.status = 'RMFlagPB_PricingBuffer'  
where the_date between '02/01/2022' and current_date - 1
group by a.the_date

-- status calendar to start tracking history of outputs
select the_date, coalesce(b.pulled, 0) as pulled, coalesce(c.pricing_buffer, 0) as pricing_buffer,
  coalesce(d.available, 0) as available, coalesce(e.front_line_ready, 0)
from dds.dim_date a
left join (
	select fromts::date, count(*) as pulled
	from ads.ext_vehicle_inventory_item_statuses
	where fromts::date between '02/01/2022' and current_date -1
		and status = 'RMFlagPulled_Pulled'
	group by fromts::date) b on a.the_date = b.fromts
left join (
	select fromts::date, count(*) as pricing_buffer
	from ads.ext_vehicle_inventory_item_statuses
	where fromts::date between '02/01/2022' and current_date
		and status = 'RMFlagPB_PricingBuffer'
	group by fromts::date) c on a.the_date = c.fromts  
left join (
	select fromts::date, count(*) as available
	from ads.ext_vehicle_inventory_item_statuses
	where fromts::date between '02/01/2022' and current_date
		and status = 'RMFlagAV_Available'
	group by fromts::date) d on a.the_date = d.fromts  	
left join (
	select fromts::date, count(*) as front_line_ready
	from ads.ext_vehicle_inventory_item_statuses
	where fromts::date between '02/01/2022' and current_date
		and status = 'RMFlagFLR_FrontLineReady'
	group by fromts::date) e on a.the_date = e.fromts  		
where a.the_date between '02/01/2022' and current_date -1

