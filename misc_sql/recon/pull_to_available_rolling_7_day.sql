﻿
-- this is the query from recon_funnel.sql

-- GM vehicles that have become available in the last 90 days
-- caveats
--   acquisition is iffy using the tool-- caveats
--   acquisition is iffy using the tool
drop table if exists base_1;
create temp table base_1 as
select a.stocknumber, b.yearmodel, b.make, b.model, 
  a.fromts as acq_ts, d.thruts as inspected, f.vehicle_walk_Ts as walked, g.fromts as pulled, e.body_kitted_ts, e.mechanical_kitted_ts, c.fromts as avail_ts
-- select a.stocknumber
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
join ads.ext_vehicle_inventory_item_statuses c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
  and c.category = 'RMFlagAV'
  and c.thruts > now()
  and c.fromts::date > current_date - 60
left join ads.ext_vehicle_inventory_item_statuses g on a.vehicleinventoryitemid = g.vehicleinventoryitemid
  and g.category = 'RMFlagPulled'  
left join ads.ext_vehicle_inventory_item_statuses d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
  and d.category = 'RMFlagIP'
left join rec.pulled_vehicles e on a.vehicleinventoryitemid = e.vehicle_inventory_item_id::text
--   and e.current_row
-- -- limit to vehicles that have been walked, eliminates intra market   
join ads.ext_vehicle_walks f on a.vehicleinventoryitemid::uuid = f.vehicle_inventory_item_id  
where a.stocknumber not like 'H%'
order by c.fromts


essentially no difference between vii fromts and inspection fromts
select stocknumber, acq_ts, insp_from_Ts, extract(epoch from acq_ts - insp_from_Ts)
from base_1
where acq_ts <> insp_from_Ts

-- this is the query from recon_funnel.sql
select pulled::date as pulled_date, stocknumber, yearmodel, make, model, 
--   a.*,
  round((extract(epoch from inspected - acq_ts)/86400)::numeric, 1) acq_to_inspected,
  round((extract(epoch from walked - inspected)/86400)::numeric, 1) inspected_to_walked,
  round((extract(epoch from pulled - walked)/86400)::numeric, 1) walked_to_pulled,
  case
    when body_kitted_ts is null then null::numeric
    else round((extract(epoch from body_kitted_ts - pulled)/86400)::numeric, 1)
  end as pulled_to_body_kitted,
  case
    when body_kitted_ts is null then null::numeric
    else round((extract(epoch from mechanical_kitted_ts - pulled)/86400)::numeric, 1)
  end as pulled_to_mech_kitted,
  round((extract(epoch from avail_ts - pulled)/86400)::numeric, 1) pulled_to_available
-- select *
from base_1 a -- where stocknumber = 'G38330A'
order by pulled::date



now greg wants 2 things: 
  rolling 7 day avg pull to avail
  investigate transactional data combining tool and dealertrack
here i will look at the rolling 7 day

first, the above query was sent out in a spreadsheet 1/5/20 as recon_funnel_v1
both ben and greg questioned only ~40 vehicles in december
so, lets look at that first

-- vehicles pulled per month

ahh, maybe because i limited it to vehicles that were current available
so, lets change that 
this specifies any vehicle that became available from 60 days ago until current date and c.thruts > now()
get rid of the and c.thruts > now()
yep, much better

couple more issues
vehicle can have been pulled multiple times
think i need max avail & max pulled
need a subset of viiid to make these groupings reasonable

-- lets start with a vehicle that became available in the last 90 days
drop table if exists vehicles;
create temp table vehicles as
select vehicleinventoryitemid
from ads.ext_vehicle_inventory_item_statuses
where category = 'RMFlagAV'
  and fromts::date > current_date - 90
group by vehicleinventoryitemid;

  
drop table if exists base_1;
create temp table base_1 as
select a.stocknumber, b.vin, b.yearmodel, b.make, b.model, 
  a.fromts as acq_ts, d.thruts as inspected, f.vehicle_walk_Ts as walked, g.fromts as pulled, 
  e.body_kitted_ts, e.mechanical_kitted_ts, c.fromts as avail_ts,
  h.fromts as FLR
-- select a.stocknumber
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
join (
  select a.vehicleinventoryitemid, max(a.fromts) as fromts
  from ads.ext_vehicle_inventory_item_statuses a
  join vehicles b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  where a.category = 'RMFlagAV'
    and a.fromts::date > current_date - 90
  group by a.vehicleinventoryitemid) c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
join (
  select a.vehicleinventoryitemid, max(fromts) as fromts
  from ads.ext_vehicle_inventory_item_statuses a
  join vehicles b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  where category = 'RMFlagPulled'
  group by a.vehicleinventoryitemid) g on a.vehicleinventoryitemid = g.vehicleinventoryitemid
left join ads.ext_vehicle_inventory_item_statuses d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
  and d.category = 'RMFlagIP'
left join (
  select a.vehicleinventoryitemid, max(a.fromts) as fromts
  from ads.ext_vehicle_inventory_item_statuses a
  join vehicles b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  where a.category = 'RMFlagFLR'
  group by a.vehicleinventoryitemid) h on a.vehicleinventoryitemid = h.vehicleinventoryitemid and h.fromts <= c.fromts and h.fromts >= g.fromts
left join rec.pulled_vehicles e on a.vehicleinventoryitemid = e.vehicle_inventory_item_id::text
--   and e.current_row
-- -- limit to vehicles that have been walked, eliminates intra market   
join ads.ext_vehicle_walks f on a.vehicleinventoryitemid::uuid = f.vehicle_inventory_item_id  
where a.stocknumber not like 'H%'
-- order by a.stocknumber
-- order by c.fromts



-- add the avail date
-- eliminate negative pulled_to_avail (backons, recycles ...)
drop table if exists pull_to_avail cascade;
create temp table pull_to_avail as
select pulled::date as pulled_date, avail_ts::date as date_avail, FLR::date as FLR, stocknumber, vin, yearmodel, make, model, 
--   a.*,
  round((extract(epoch from inspected - acq_ts)/86400)::numeric, 1) acq_to_inspected,
  round((extract(epoch from walked - inspected)/86400)::numeric, 1) inspected_to_walked,
  round((extract(epoch from pulled - walked)/86400)::numeric, 1) walked_to_pulled,
  case
    when body_kitted_ts is null then null::numeric
    else round((extract(epoch from body_kitted_ts - pulled)/86400)::numeric, 1)
  end as pulled_to_body_kitted,
  case
    when body_kitted_ts is null then null::numeric
    else round((extract(epoch from mechanical_kitted_ts - pulled)/86400)::numeric, 1)
  end as pulled_to_mech_kitted,
  round((extract(epoch from avail_ts - pulled)/86400)::numeric, 1) pulled_to_available,
  avail_ts::date - flr::date, avail_ts
-- select *
from base_1 a -- where stocknumber = 'G38330A'
where round((extract(epoch from avail_ts - pulled)/86400)::numeric, 1) >= 0;
create unique index on pull_to_avail(stocknumber);

-- this is too confusing with multiple events per day for both pull and avail
select a.the_date, pulled_date, date_avail, stocknumber, pulled_to_available
-- select *
from dds.dim_Date a
left join pull_to_avail b on a.the_date = b.date_avail
where a.the_date between '10/15/2019' and current_date
order by the_date

-- 1 row per avail_ts

-- this gives me the average over the previous seven day
select stocknumber, pulled_date, date_avail, avail_ts, pulled_to_available
  -- round(avg(pulled_to_available) over(order by avail_ts rows between 6 preceding and current row), 1)  as prev_7_avg
from pull_to_avail
order by avail_ts

-- add the row number to the avg table
select pulled_date, date_avail, stocknumber, pulled_to_available, avail_ts, 
  round(avg(pulled_to_available) over(order by avail_ts rows between 6 preceding and current row), 1) as prev_7_avg, 
  row_number() over ()
from pull_to_avail
order by avail_ts



-- google search: postgresql median of an "array"

-- this gives me an array of the previous 7 days, now just need to get the median of that array
select pulled_date, date_avail, stocknumber, pulled_to_available, avail_ts, 
  array_agg(pulled_to_available) over(order by avail_ts rows between 6 preceding and current row) as prev_7_pull_to_avail,
  row_number() over (order by avail_ts)
from pull_to_avail

-- only those rows with 7 data points
select *
from (
  select pulled_date, date_avail, stocknumber, pulled_to_available, avail_ts, 
    array_agg(pulled_to_available) over(order by avail_ts rows between 6 preceding and current row) as prev_7_pull_to_avail,
    row_number() over (order by avail_ts)
  from pull_to_avail)  x
where cardinality(prev_7_pull_to_avail) = 7


-- unnest, maintains row numbers
select row_number, unnest(prev_7_pull_to_avail)
from (
select pulled_date, date_avail, stocknumber, pulled_to_available, avail_ts, 
  array_agg(pulled_to_available) over(order by avail_ts rows between 6 preceding and current row) as prev_7_pull_to_avail,
  row_number() over (order by avail_ts)
from pull_to_avail) x


-- https://stackoverflow.com/questions/59080697/how-to-calculate-the-median-value-in-postgresql-without-using-window-function
-- in this post, the questioner quotes an example from https://www.compose.com/articles/metrics-maven-meet-in-the-middle-median-in-postgresql/
-- this function is actually taken from the postgresql wiki https://wiki.postgresql.org/wiki/Aggregate_Median
create or replace function jon.median(numeric[])
returns numeric language sql immutable as
$$
    select avg(val)
    from (
        select val
        from unnest($1) val
        order by 1
        limit 2 - mod(array_upper($1, 1), 2)
        offset ceil(array_upper($1, 1) / 2.0) - 1
    ) sub;
$$;

select array_agg(item_count)
from jon.median_test




-- works, but one row at a time
-- aha works with the whole table, just needed to wrap the subselect in double ()
select a.*, round(((select jon.median((prev_7_pull_to_avail)))), 1) as median, 
  round((SELECT SUM(s) FROM UNNEST(prev_7_pull_to_avail) s)/7, 1)  as average,
  (SELECT SUM(s) FROM UNNEST(prev_7_pull_to_avail) s) as sum_of_7
from (
  select *
  from (
    select pulled_date, date_avail, stocknumber, pulled_to_available, avail_ts, 
      array_agg(pulled_to_available) over(order by avail_ts rows between 6 preceding and current row) as prev_7_pull_to_avail
--       row_number() over (order by avail_ts)
    from pull_to_avail)  x
  where cardinality(prev_7_pull_to_avail) = 7
  order by avail_ts) a
-- where row_number = 7  

-- 10 vehicles becoming avail
select a.*, round(((select jon.median((prev_7_pull_to_avail)))), 1) as median, 
  round((SELECT SUM(s) FROM UNNEST(prev_7_pull_to_avail) s)/10, 1)  as average,
  (SELECT SUM(s) FROM UNNEST(prev_7_pull_to_avail) s) as sum_of_7,
  cardinality(prev_7_pull_to_avail) 
from (
  select *
  from (
    select pulled_date, date_avail, stocknumber, pulled_to_available, avail_ts, 
      array_agg(pulled_to_available) over(order by avail_ts rows between 9 preceding and current row) as prev_7_pull_to_avail
--       row_number() over (order by avail_ts)
    from pull_to_avail)  x
  where cardinality(prev_7_pull_to_avail) = 10
  order by avail_ts) a
-- where row_number = 7  


-- 20 vehicles becoming avail
select a.*, round(((select jon.median((prev_7_pull_to_avail)))), 1) as median, 
  round((SELECT SUM(s) FROM UNNEST(prev_7_pull_to_avail) s)/20, 1)  as average,
  (SELECT SUM(s) FROM UNNEST(prev_7_pull_to_avail) s) as sum_of_7,
  cardinality(prev_7_pull_to_avail) 
from (
  select *
  from (
    select pulled_date, date_avail, stocknumber, pulled_to_available, avail_ts, 
      array_agg(pulled_to_available) over(order by avail_ts rows between 19 preceding and current row) as prev_7_pull_to_avail
--       row_number() over (order by avail_ts)
    from pull_to_avail)  x
  where cardinality(prev_7_pull_to_avail) = 20
  order by avail_ts) a
-- where row_number = 7  

-- 1/21 sent spreadsheet   pull_to_avail_with_avg_median

-- -- shit took a piss and thought maybe greg really does want the previous 7 days, avg for however may units
-- -- i emailed greg asking for what he wants, we will see
-- select a.the_date, pulled_date, date_avail, stocknumber, pulled_to_available
-- -- select *
-- from dds.dim_Date a
-- left join pull_to_avail b on a.the_date = b.date_avail
-- where a.the_date between '10/15/2019' and current_date
-- order by the_date
-- 
-- -- so can start with 10/23 to get full previous seven days
-- select a.the_date, pulled_date, date_avail, stocknumber, pulled_to_available
-- -- select *
-- from dds.dim_Date a
-- left join pull_to_avail b on a.the_date = b.date_avail
-- where a.the_date between '10/23/2019' and current_date
-- order by the_date
-- 
-- 
-- select a.the_date, aa.week_id, sum(pulled_to_available), count(*)
-- from dds.dim_Date a
-- join fs.weeks aa on a.the_date between aa.from_date and aa.thru_date
-- left join pull_to_avail b on a.the_date = b.date_avail
-- where a.the_date between '10/23/2019' and current_date
-- group by a.the_date, aa.week_id
-- order by a.the_date