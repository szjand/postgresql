﻿drop table if exists ads.ext_vehicle_walks;
create TABLE ads.ext_vehicle_walks(
    Vehicle_Walk_ID uuid,
    Market_ID citext,
    Vehicle_Walker_ID uuid,
    Intended_Location_ID uuid,
    Walk_Location_ID uuid,
    Fit citext,
    Vehicle_Walk_TS timestamp with time zone,
    Desirability citext,
    Retail_Wholesale_Disposition citext,
    Price_Leader boolean,
    Too_Many boolean,
    Vehicle_Item_ID uuid,
    Vehicle_Inventory_Item_ID uuid,
    Comments citext,
    Wholesale_Method_type citext,
    primary key(vehicle_walk_id));
create index on ads.ext_vehicle_walks(vehicle_item_id);
create index on ads.ext_vehicle_walks(vehicle_inventory_item_id);   


select max(vehicleevaluationts) from ads.ext_vehicle_evaluations


create temp table evals
as select * from ads.ext_vehicle_evaluations

alter table evals
alter column vehicleevaluationid type uuid USING vehicleevaluationid::uuid



select b.vehicle_walk_ts, a.*
from rec.pulled_vehicles a
left join ads.ext_vehicle_walks b on a.vehicle_inventory_item_id = b.vehicle_inventory_item_id
order by b.vehicle_walk_ts


select b.vehicle_walk_ts, a.mechanical_kitted_ts, 
  extract(epoch from a.mechanical_kitted_ts - b.vehicle_walk_ts) as seconds, 
  (extract(epoch from a.mechanical_kitted_ts - b.vehicle_walk_ts))/86400 as days
from rec.pulled_vehicles a
left join ads.ext_vehicle_walks b on a.vehicle_inventory_item_id = b.vehicle_inventory_item_id
where a.stock_number = 'G38960GA'

