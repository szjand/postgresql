﻿select a.stocknumber, b.yearmodel, b.make, b.model, 
  d.fromts as insp_ts, e.body_kitted_ts, e.mechanical_kitted_ts, f.vehicle_walk_Ts, c.fromts as avail_ts
-- select a.stocknumber
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
join ads.ext_vehicle_inventory_item_statuses c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
  and c.category = 'RMFlagAV'
  and c.thruts > now()
  and c.fromts::date > current_date - 90
left join ads.ext_vehicle_inventory_item_statuses d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
  and d.category = 'RMFlagIP'
left join rec.pulled_vehicles e on a.vehicleinventoryitemid = e.vehicle_inventory_item_id::text
--   and e.current_row
-- -- limit to vehicles that have been walked, eliminates intra market   
join ads.ext_vehicle_walks f on a.vehicleinventoryitemid::uuid = f.vehicle_inventory_item_id  
where a.stocknumber not like 'H%'
order by a.stocknumber desc


select *
from ads.ext_vehicle_walks
limit 10

select distinct category
from ads.ext_vehicle_inventory_item_statuses
where category not like '%recon%'
  and thruts::date > current_date - 2
limit 100


select *
from rec.pulled_vehicles a
join (
  select vehicle_inventory_item_id
  from rec.pulled_Vehicles
  group by vehicle_inventory_item_id
  having count(*) > 1) b on a.vehicle_inventory_item_id = b.vehicle_inventory_item_id


select b.vehicle_walk_ts, a.mechanical_kitted_ts, 
  extract(epoch from a.mechanical_kitted_ts - b.vehicle_walk_ts) as seconds, 
  (extract(epoch from a.mechanical_kitted_ts - b.vehicle_walk_ts))/86400 as days
from rec.pulled_vehicles a
left join ads.ext_vehicle_walks b on a.vehicle_inventory_item_id = b.vehicle_inventory_item_id
where a.stock_number = 'G38960GA'


-- 1/5/20
much confusion about this entire process,  generate some initail data simulating bens recon funnel
based on vehicles that become available as a result of having been pulled


-- GM vehicles that have become available in the last 90 days
-- caveats
--   acquisition is iffy using the tool-- caveats
--   acquisition is iffy using the tool
drop table if exists base_1;
create temp table base_1 as
select a.stocknumber, b.yearmodel, b.make, b.model, 
  a.fromts as acq_ts, d.thruts as inspected, f.vehicle_walk_Ts as walked, g.fromts as pulled, e.body_kitted_ts, e.mechanical_kitted_ts, c.fromts as avail_ts
-- select a.stocknumber
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
join ads.ext_vehicle_inventory_item_statuses c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
  and c.category = 'RMFlagAV'
  and c.thruts > now()
  and c.fromts::date > current_date - 60
left join ads.ext_vehicle_inventory_item_statuses g on a.vehicleinventoryitemid = g.vehicleinventoryitemid
  and g.category = 'RMFlagPulled'  
left join ads.ext_vehicle_inventory_item_statuses d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
  and d.category = 'RMFlagIP'
left join rec.pulled_vehicles e on a.vehicleinventoryitemid = e.vehicle_inventory_item_id::text
--   and e.current_row
-- -- limit to vehicles that have been walked, eliminates intra market   
join ads.ext_vehicle_walks f on a.vehicleinventoryitemid::uuid = f.vehicle_inventory_item_id  
where a.stocknumber not like 'H%'
order by c.fromts


essentially no difference between vii fromts and inspection fromts
select stocknumber, acq_ts, insp_from_Ts, extract(epoch from acq_ts - insp_from_Ts)
from base_1
where acq_ts <> insp_from_Ts


select pulled::date as pulled_date, stocknumber, yearmodel, make, model, 
--   a.*,
  round((extract(epoch from inspected - acq_ts)/86400)::numeric, 1) acq_to_inspected,
  round((extract(epoch from walked - inspected)/86400)::numeric, 1) inspected_to_walked,
  round((extract(epoch from pulled - walked)/86400)::numeric, 1) walked_to_pulled,
  case
    when body_kitted_ts is null then null::numeric
    else round((extract(epoch from body_kitted_ts - pulled)/86400)::numeric, 1)
  end as pulled_to_body_kitted,
  case
    when body_kitted_ts is null then null::numeric
    else round((extract(epoch from mechanical_kitted_ts - pulled)/86400)::numeric, 1)
  end as pulled_to_mech_kitted,
  round((extract(epoch from avail_ts - pulled)/86400)::numeric, 1) pulled_to_available
-- select *
from base_1 a -- where stocknumber = 'G38330A'
order by pulled::date










