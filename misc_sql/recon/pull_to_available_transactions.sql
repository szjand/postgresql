﻿
-- lets start with a vehicle that became available in the last 90 days
drop table if exists vehicles;
create temp table vehicles as
select vehicleinventoryitemid
from ads.ext_vehicle_inventory_item_statuses
where category = 'RMFlagAV'
  and fromts::date > current_date - 90
group by vehicleinventoryitemid;

-- Front Line Ready may be kind of flaky, check out pricing buffer instead
  
drop table if exists base_1;
create temp table base_1 as
select a.vehicleinventoryitemid, a.stocknumber, b.vin, b.yearmodel, b.make, b.model, 
  a.fromts as acq_ts, d.thruts as inspected, f.vehicle_walk_Ts as walked, g.fromts as pulled, 
  e.body_kitted_ts, e.mechanical_kitted_ts, c.fromts as avail_ts,
  h.fromts as FLR
-- select a.stocknumber
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
join (
  select a.vehicleinventoryitemid, max(a.fromts) as fromts
  from ads.ext_vehicle_inventory_item_statuses a
  join vehicles b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  where a.category = 'RMFlagAV'
    and a.fromts::date > current_date - 90
  group by a.vehicleinventoryitemid) c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
join (
  select a.vehicleinventoryitemid, max(fromts) as fromts
  from ads.ext_vehicle_inventory_item_statuses a
  join vehicles b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  where category = 'RMFlagPulled'
  group by a.vehicleinventoryitemid) g on a.vehicleinventoryitemid = g.vehicleinventoryitemid
left join ads.ext_vehicle_inventory_item_statuses d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
  and d.category = 'RMFlagIP'
left join (
  select a.vehicleinventoryitemid, max(a.fromts) as fromts
  from ads.ext_vehicle_inventory_item_statuses a
  join vehicles b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  where a.category = 'RMFlagFLR'
  group by a.vehicleinventoryitemid) h on a.vehicleinventoryitemid = h.vehicleinventoryitemid and h.fromts <= c.fromts and h.fromts >= g.fromts
left join rec.pulled_vehicles e on a.vehicleinventoryitemid = e.vehicle_inventory_item_id::text
--   and e.current_row
-- -- limit to vehicles that have been walked, eliminates intra market   
join ads.ext_vehicle_walks f on a.vehicleinventoryitemid::uuid = f.vehicle_inventory_item_id  
where a.stocknumber not like 'H%'
-- order by a.stocknumber
-- order by c.fromts

select * from base_1

-- add the avail date
-- eliminate negative pulled_to_avail (backons, recycles ...)

drop table if exists pull_to_avail cascade;
create temp table pull_to_avail as
select vehicleinventoryitemid, acq_ts::date as acq_date, walked::date as walked, pulled::date as pulled_date, avail_ts::date as date_avail, FLR::date as FLR, stocknumber, vin, yearmodel, make, model, 
--   a.*,
  round((extract(epoch from inspected - acq_ts)/86400)::numeric, 1) acq_to_inspected,
  round((extract(epoch from walked - inspected)/86400)::numeric, 1) inspected_to_walked,
  round((extract(epoch from pulled - walked)/86400)::numeric, 1) walked_to_pulled,
  case
    when body_kitted_ts is null then null::numeric
    else round((extract(epoch from body_kitted_ts - pulled)/86400)::numeric, 1)
  end as pulled_to_body_kitted,
  case
    when body_kitted_ts is null then null::numeric
    else round((extract(epoch from mechanical_kitted_ts - pulled)/86400)::numeric, 1)
  end as pulled_to_mech_kitted,
  round((extract(epoch from avail_ts - pulled)/86400)::numeric, 1) pulled_to_available,
  avail_ts::date - flr::date, avail_ts
-- select *
from base_1 a -- where stocknumber = 'G38330A'
where round((extract(epoch from avail_ts - pulled)/86400)::numeric, 1) >= 0;
create unique index on pull_to_avail(stocknumber);


-- 1 row per avail_ts

select *
from pull_to_avail
order by avail_ts


select e.vin, a.ro, b.the_date as open_date, d.the_date as close_date, array_agg(distinct c.the_date), array_agg(distinct g.opcode)
from ads.ext_fact_repair_order a
join dds.dim_date b on a.opendatekey = b.date_key
join dds.dim_date c on a.flagdatekey = c.date_key
  and c.the_date <> '12/31/9999'
join dds.dim_date d on a.closedatekey = d.date_key
join ads.ext_dim_vehicle e on a.vehiclekey = e.vehiclekey
join pull_to_avail f on e.vin = f.vin
join ads.ext_dim_opcode g on a.opcodekey = g.opcodekey
  and g.opcode not in('PIC','TBW','DLX','ADMIN','NICE','STG','WALK','ASIS','CERT','TCK','CAR','TDW')
where b.the_date >= f.acq_date
group by e.vin, a.ro, b.the_date, d.the_date


-- this is vaguely interesting, but hard to see the usefullness
select *
from pull_to_avail aa
left join (
    select e.vin, array_agg(distinct a.ro || ':' ||b.the_date::text || ':'  || d.the_date::text) 
    from ads.ext_fact_repair_order a
    join dds.dim_date b on a.opendatekey = b.date_key
    -- join dds.dim_date c on a.flagdatekey = c.date_key
    --   and c.the_date <> '12/31/9999'
    join dds.dim_date d on a.closedatekey = d.date_key
    join ads.ext_dim_vehicle e on a.vehiclekey = e.vehiclekey
    join pull_to_avail f on e.vin = f.vin
    join ads.ext_dim_opcode g on a.opcodekey = g.opcodekey
      and g.opcode not in('PIC','TBW','DLX','ADMIN','NICE','STG','WALK','ASIS','CERT','TCK','CAR','TDW')
    where b.the_date >= f.acq_date
    group by e.vin) bb on aa.vin = bb.vin
order by avail_ts


-- after talking to bev, she opens the ros at walk, flagdate doesn't get entered until she books it, therefore is the same as close date
-- if i make sure to eliminat detail ros, the close date for mech ros and for body shop ros might be useful
-- so lets see what the service types on these ros are

select *
from pull_to_avail aa
left join (
    select e.vin, 
      case when left(ro, 2) = '16' then array_agg(distinct a.ro || ':' ||h.servicetype) end as mech,
      case when left(ro, 2) = '18' then array_agg(distinct a.ro || ':' ||h.servicetype) end as bs
    from ads.ext_fact_repair_order a
    join dds.dim_date b on a.opendatekey = b.date_key
    join dds.dim_date d on a.closedatekey = d.date_key
    join ads.ext_dim_vehicle e on a.vehiclekey = e.vehiclekey
    join pull_to_avail f on e.vin = f.vin -- limit the number of vehicles/ros
    join ads.ext_dim_opcode g on a.opcodekey = g.opcodekey
      and g.opcode not in('PIC','TBW','DLX','ADMIN','NICE','STG','WALK','ASIS','CERT','TCK','CAR','TDW')
    join ads.ext_dim_service_type h on a.servicetypekey = h.servicetypekey
    join ads.ext_dim_service_writer i on a.servicewriterkey = i.servicewriterkey -- gets rid of detail ros opened by leslie 
      and i.writernumber not in ('130')
    where b.the_date >= f.acq_date
    group by e.vin, ro) bb on aa.vin = bb.vin
order by avail_ts


16369837
select *
from ads.ext_dim_opcode 
where opcode in('PIC','TBW','DLX','ADMIN','NICE','STG','WALK','ASIS','CERT','TCK','CAR','TDW','CBW')
order by opcode

select *
from ads.ext_dim_service_writer
where name like 'champ%'

-- keep narrowing it down

select *
from pull_to_avail a
limit 100
-- this looks pretty good, excluding detail & inspection ros
    select f.stocknumber, e.vin, a.ro, f.acq_date, ro, f.walked, b.the_date as open_date, d.the_date as close_date, i.name, i.writernumber, array_agg(distinct opcode)
    from ads.ext_fact_repair_order a
    join dds.dim_date b on a.opendatekey = b.date_key
    join dds.dim_date d on a.closedatekey = d.date_key
    join ads.ext_dim_vehicle e on a.vehiclekey = e.vehiclekey
    join pull_to_avail f on e.vin = f.vin -- limit the number of vehicles/ros
    join ads.ext_dim_opcode g on a.opcodekey = g.opcodekey -- inspection & detail ros
--       and g.opcode not in('PIC','TBW','DLX','ADMIN','NICE','STG','WALK','ASIS','CERT','TCK','CAR','TDW')
    join ads.ext_dim_service_type h on a.servicetypekey = h.servicetypekey
    join ads.ext_dim_service_writer i on a.servicewriterkey = i.servicewriterkey -- gets rid of detail ros opened by leslie 
      and i.writernumber not in ('130','574')  --457 with bear 574 exclude
    where b.the_date >= f.acq_date -- exclude ros before acquisition
      and b.the_date >= walked -- ro open date after before the walk date, one way to exclude inspection ros, possible exclude ros written by bear (Check with bev)
      and b.the_date <= f.date_avail -- exclude ros after available
    group by f.stocknumber, e.vin, a.ro, f.acq_date, ro, b.the_date, d.the_date, i.name, i.writernumber, f.walked
    order by f.stocknumber, a.ro


drop table if exists ros;
create temp table ros as
select f.stocknumber, e.vin, 
  max(d.the_date) filter (where h.servicetype = 'Body Shop') as body_ro_done,
  max(d.the_date) filter (where h.servicetype <> 'Body Shop') as mech_ro_done
from ads.ext_fact_repair_order a
join dds.dim_date b on a.opendatekey = b.date_key
join dds.dim_date d on a.closedatekey = d.date_key
join ads.ext_dim_vehicle e on a.vehiclekey = e.vehiclekey
join pull_to_avail f on e.vin = f.vin -- limit the number of vehicles/ros
join ads.ext_dim_service_type h on a.servicetypekey = h.servicetypekey
join ads.ext_dim_service_writer i on a.servicewriterkey = i.servicewriterkey -- gets rid of detail ros opened by leslie 
  and i.writernumber not in ('130','574')  --457 with bear 574 exclude
where b.the_date >= f.acq_date -- exclude ros before acquisition
  and b.the_date >= walked -- ro open date after before the walk date, one way to exclude inspection ros, possible exclude ros written by bear (Check with bev)
  and b.the_date <= f.date_avail -- exclude ros after available
group by f.stocknumber, e.vin;

/*
1/22/20
mind fucking on the notion of a variable time line
there are discrete ordered events: acquired, inspected, walked, pulled, pricing buffer, available
but there are also variable events, mechanical, body & detail

and how do i store the data for display

*/

select * from ros where stocknumber = 'G33587RA'
asdf

select a.*, b.body_ro_done, b.mech_ro_done
from pull_to_avail a
left join ros b on a.stocknumber = b.stocknumber
where a.stocknumber = 'G33587RA'




select b.vehicleinventoryitemid, b.status, b.fromts, b.thruts
from ads.ext_Vehicle_inventory_items a
join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and  b.status in ('MechanicalReconDispatched_Dispatched','MechanicalReconProcess_InProcess','BodyReconDispatched_Dispatched','BodyReconProcess_InProcess')
where a.stocknumber = 'G37885A'


select *
from ads.ext_vehicle_recon_items
where vehicleinventoryitemid = 'a21babe9-6e64-472e-bf3e-630bfacc6f5e'


parts orders with the above statuses look promising

 bev suggested keyper in conjunction with move requests

-- 1/22/20 lets try to get all the timestamps we can

-- try to get more timestamps in here, 
-- recon statuses
-- parts requests  data available
-- wtfs, -- not worth it
-- moves -- data available
drop table if exists base_1;
create temp table base_1 as
select a.stocknumber, b.vin, b.yearmodel, b.make, b.model, 
  a.fromts as acq_ts, d.thruts as inspected, f.vehicle_walk_Ts as walked, g.fromts as pulled, 
  e.body_kitted_ts, e.mechanical_kitted_ts, c.fromts as avail_ts,
  h.fromts as PB
-- select a.stocknumber
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
join (
  select a.vehicleinventoryitemid, max(a.fromts) as fromts
  from ads.ext_vehicle_inventory_item_statuses a
  join vehicles b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  where a.category = 'RMFlagAV'
    and a.fromts::date > current_date - 90
  group by a.vehicleinventoryitemid) c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
join (
  select a.vehicleinventoryitemid, max(fromts) as fromts
  from ads.ext_vehicle_inventory_item_statuses a
  join vehicles b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  where category = 'RMFlagPulled'
  group by a.vehicleinventoryitemid) g on a.vehicleinventoryitemid = g.vehicleinventoryitemid
left join ads.ext_vehicle_inventory_item_statuses d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
  and d.category = 'RMFlagIP'
left join (
  select a.vehicleinventoryitemid, max(a.fromts) as fromts
  from ads.ext_vehicle_inventory_item_statuses a
  join vehicles b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  where a.category = 'RMFlagPB'
  group by a.vehicleinventoryitemid) h on a.vehicleinventoryitemid = h.vehicleinventoryitemid and h.fromts <= c.fromts and h.fromts >= g.fromts
left join rec.pulled_vehicles e on a.vehicleinventoryitemid = e.vehicle_inventory_item_id::text
--   and e.current_row
-- -- limit to vehicles that have been walked, eliminates intra market   
join ads.ext_vehicle_walks f on a.vehicleinventoryitemid::uuid = f.vehicle_inventory_item_id  
where a.stocknumber not like 'H%'
-- order by a.stocknumber
-- order by c.fromts


select a.vehicleinventoryitemid, a.fromts, a.thruts, round((extract(epoch from thruts - fromts)/86400)::numeric, 1)
from ads.ext_vehicles_to_move a
join vehicles b on a.vehicleinventoryitemid = b.vehicleinventoryitemid

-- need to limit move wait to between pull and avail
select * from ads.ext_vehicle_inventory_items where vehicleinventoryitemid = '1ec4e31d-8243-422b-b8c5-26dd51cd9a89'

G38252A

select cc.days_to_move, bb.*
from pull_to_avail bb
join (
select vehicleinventoryitemid, sum(days) as days_to_move
from (
  select b.vehicleinventoryitemid, b.fromts, b.thruts, round((extract(epoch from b.thruts - b.fromts)/86400)::numeric, 1) as days
  from base_1 a
  join ads.ext_vehicles_to_move b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
    and b.fromts < a.avail_ts
    and b.thruts > a.pulled) aa
group by vehicleinventoryitemid) cc on bb.vehicleinventoryitemid = cc.vehicleinventoryitemid 
order by cc.days_to_move desc 

-----------------------------------------------------------------------------------------
-- 1/23/
need to stop fooling around, define my tables
i need 
  vehicles
    from vehicles i need
      tool statuses
      tool parts 
      tool move
      arkona bs & mech ros
      vision mech bs kitting
-----------------------------------------------------------------------------------------
-- gm only
-- vehicles that have become available in the last 90 days
-- only the most recent availability ?
-- limit to vehicles:
--   GM 
--   a single pull 
--   single instance of availability
--   has been walked 
--   eliminates intra market acquisitions, backons, recycles, goofy data 
--   this may leave out some vehicles, but disambiguates the existing data
  

select *
from (
select a.vehicleinventoryitemid, a.stocknumber, b.vin, b.yearmodel, b.make, b.model, max(c.fromts) as avail_ts
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
join ads.ext_vehicle_inventory_item_statuses c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
  and c.category = 'RMFlagAV'
  and c.fromts::date > current_date - 90
where a.stocknumber not like 'H%'  
group by a.vehicleinventoryitemid, a.stocknumber, b.vin, b.yearmodel, b.make, b.model)
where 


drop table if exists vehicles cascade;
create temp table vehicles as
select a.stocknumber, /**/aa.vin,/**/ a.vehicleinventoryitemid, d.vehicle_walk_ts, c.pull_ts, b.avail_ts
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_items aa on a.vehicleitemid = aa.vehicleitemid
inner join (-- vehicles with single avail
  select vehicleinventoryitemid, max(fromts) as avail_ts
  from ads.ext_vehicle_inventory_item_statuses
  where fromts::date > current_date - 120
    and category = 'RMFlagAV'
  group by vehicleinventoryitemid
  having count(*) = 1) b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
inner join (-- single pull
  select vehicleinventoryitemid, max(fromts) as pull_ts
  from ads.ext_vehicle_inventory_item_statuses
  where fromts::date > current_date - 120
    and category = 'RMFlagPulled'
  group by vehicleinventoryitemid
  having count(*) = 1) c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
inner join ads.ext_vehicle_walks d on a.vehicleinventoryitemid = d.vehicle_inventory_item_id::text  
where a.stocknumber not like 'H%'
  and a.stocknumber not in ('G37793XBC','G37439RA','G39036GA','G38589B'); -- unwinds, multiple vins
create unique index on vehicles(vehicleinventoryitemid);
create unique index on vehicles(stocknumber);
create unique index on vehicles(vin);
alter table vehicles add constraint pull_after_walk
check (pull_ts > vehicle_walk_ts);
alter table vehicles add constraint avail_after_pull
check (avail_ts > pull_ts);

select a.* 
from vehicles a


-- -- dispatch doesn't really tell me anything, limit statuses to wip
-- -- need to limit recon work to that done before avail
-- -- and since we are looking at pull to avail, limit to wip in that range
-- -- due to multiple instances of dept wip, looks like recon status will need to be a total duration rather than a range
-- select a.*, b.status, b.fromts, b.thruts
-- from vehicles a
-- left join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
--   and  b.status in ('MechanicalReconProcess_InProcess','BodyReconProcess_InProcess')
--   and b.fromts < a.avail_ts
-- where b.thruts < a.pull_ts  
-- order by a.vehicleinventoryitemid, b.status, b.fromts
-- 
-- -- due to multiple instances of dept wip, looks like recon status will need to be a total duration rather than a range
-- -- vehicles with more than one instance of a status
-- select a.*, b.status, b.fromts, b.thruts
-- from vehicles a
-- join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
--   and  b.status in ('MechanicalReconProcess_InProcess','BodyReconProcess_InProcess')
--   and b.fromts < a.avail_ts
--  where stocknumber in (
--   select stocknumber from (
--   select a.*, b.vehicleinventoryitemid, b.status, b.fromts, b.thruts
--   from vehicles a
--   join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
--     and  b.status in ('MechanicalReconProcess_InProcess','BodyReconProcess_InProcess')
--     and b.fromts < a.avail_ts) x 
--   group by stocknumber, status having count(*) > 1) 
--  order by a.vehicleinventoryitemid, b.status, b.fromts 

-- recon wip
drop table if exists tool_recon_wip cascade;
create temp table tool_recon_wip as
-- limited to recon done in the pull to avail interval
-- only those vehicles with relevant wip time, 194 of 386
select a.stocknumber, a.vehicleinventoryitemid,
  coalesce(sum(round((extract(epoch from thruts - fromts)/86400)::numeric, 1)) filter (where status = 'BodyReconProcess_InProcess'), 0) as body_wip_days,
  coalesce(sum(round((extract(epoch from thruts - fromts)/86400)::numeric, 1)) filter (where status = 'MechanicalReconProcess_InProcess'), 0) as mech_wip_days
from vehicles a
join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and  b.status in ('MechanicalReconProcess_InProcess','BodyReconProcess_InProcess')
  and b.fromts < a.avail_ts 
  and b.thruts > a.pull_ts
 group by a.stocknumber, a.vehicleinventoryitemid;
create unique index on tool_recon_wip(stocknumber);
create unique index on tool_recon_wip(vehicleinventoryitemid);

select a.*, round((extract(epoch from avail_ts - pull_ts)/86400)::numeric, 1) as pull_to_avail_days,
  coalesce(b.body_wip_days, 0) as body_wip_days, coalesce(b.mech_wip_days, 0) as mech_wip_days
from vehicles a
left join tool_recon_wip b on a.stocknumber = b.stocknumber

-- parts
drop table if exists parts cascade;
create temp table parts as
-- requested on or after pull, received before avail
select stocknumber, vehicleinventoryitemid, round((extract(epoch from receivedts - requestedts)/86400)::numeric, 1) as parts_days
from (
  select a.stocknumber, a.vehicleinventoryitemid, c.requestedts, max(c.receivedts) as receivedts
  from vehicles a
  join ads.ext_vehicle_recon_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  join ads.ext_parts_orders c on b.vehiclereconitemid = c.vehiclereconitemid
    and c.cancelledts is null
    and c.receivedts <= a.avail_ts
    and c.requestedts >= a.pull_ts
  group by a.stocknumber, a.vehicleinventoryitemid, c.requestedts) aa


select a.*, round((extract(epoch from avail_ts - pull_ts)/86400)::numeric, 1) as pull_to_avail_days,
  b.body_wip_days, b.mech_wip_days,
  c.parts_days
from vehicles a
left join tool_recon_wip b on a.stocknumber = b.stocknumber
left join parts c on a.stocknumber = c.stocknumber

-- moves
drop table if exists moves cascade;
create temp table moves as
select a.stocknumber, a.vehicleinventoryitemid, sum(round((extract(epoch from b.thruts - b.fromts)/86400)::numeric, 1)) as move_days
from vehicles a
join ads.ext_vehicles_to_move b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and b.thruts < a.avail_ts
  and b.fromts >= a.pull_ts
group by a.stocknumber, a.vehicleinventoryitemid;

select a.*, round((extract(epoch from avail_ts - pull_ts)/86400)::numeric, 1) as pull_to_avail_days,
  b.body_wip_days, b.mech_wip_days,
  c.parts_days, d.move_days
from vehicles a
left join tool_recon_wip b on a.stocknumber = b.stocknumber
left join parts c on a.stocknumber = c.stocknumber
left join moves d on a.stocknumber = d.stocknumber

-- -- pricing buffer
-- -- many 2 pricing buffers: recon done, pricing rinse, use min from and max thru
-- -- pricing buffer includes the time to get the pricing rinse done
-- select a.*, b.fromts, b.thruts
-- from vehicles a
-- join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
--   and b.category = 'RMFlagPB'
--   and b.fromts < a.avail_ts
-- where a.stocknumber in (   
--   select a.stocknumber
--   from vehicles a
--   join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
--     and b.category = 'RMFlagPB'
--     and b.fromts < a.avail_ts
--   group by a.stocknumber
--   having count(*) > 1)  

drop table if exists pricing_buffer;
create temp table pricing_buffer as
select stocknumber, vehicleinventoryitemid, round((extract(epoch from thruts - fromts)/86400)::numeric, 1) as pb_days
from (
  select a.stocknumber, a.vehicleinventoryitemid, min(b.fromts) as fromts, max(b.thruts) as thruts
  from vehicles a
  join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
    and b.category = 'RMFlagPB'
    and b.fromts < a.avail_ts 
    and b.fromts > a.pull_ts
  group by a.stocknumber, a.vehicleinventoryitemid) aa 
where round((extract(epoch from thruts - fromts)/86400)::numeric, 1) < 10;

select a.*, round((extract(epoch from avail_ts - pull_ts)/86400)::numeric, 1) as pull_to_avail_days,
  b.body_wip_days, b.mech_wip_days,
  c.parts_days, d.move_days, e.pb_days
from vehicles a
left join tool_recon_wip b on a.stocknumber = b.stocknumber
left join parts c on a.stocknumber = c.stocknumber
left join moves d on a.stocknumber = d.stocknumber
left join pricing_buffer e on a.stocknumber = e.stocknumber


select a.*, round((extract(epoch from avail_ts - pull_ts)/86400)::numeric, 1) as pull_to_avail_days,
  b.body_wip_days, b.mech_wip_days,
  c.parts_days, d.move_days, e.pb_days,
  round((extract(epoch from avail_ts - pull_ts)/86400)::numeric, 1) 
  - coalesce(b.body_wip_days, 0) 
  - coalesce(b.mech_wip_days, 0) - coalesce( c.parts_days, 0) 
  - coalesce(d.move_days, 0) - coalesce(e.pb_days, 0) as net_days
from vehicles a
left join tool_recon_wip b on a.stocknumber = b.stocknumber
left join parts c on a.stocknumber = c.stocknumber
left join moves d on a.stocknumber = d.stocknumber
left join pricing_buffer e on a.stocknumber = e.stocknumber


drop table if exists ros;
create temp table ros as
select ee.stocknumber, e.vin, 
  max(d.the_date) filter (where h.servicetype = 'Body Shop') as body_ro_done,
  max(d.the_date) filter (where h.servicetype <> 'Body Shop') as mech_ro_done
from ads.ext_fact_repair_order a
join dds.dim_date b on a.opendatekey = b.date_key
join dds.dim_date d on a.closedatekey = d.date_key
join ads.ext_dim_vehicle e on a.vehiclekey = e.vehiclekey
join vehicles ee on e.vin = ee.vin
-- join pull_to_avail f on e.vin = f.vin -- limit the number of vehicles/ros
join ads.ext_dim_service_type h on a.servicetypekey = h.servicetypekey
join ads.ext_dim_service_writer i on a.servicewriterkey = i.servicewriterkey -- gets rid of detail ros opened by leslie 
  and i.writernumber not in ('130','574')  --457 with bear 574 exclude
where b.the_date >= f.acq_date -- exclude ros before acquisition
  and b.the_date >= walked -- ro open date after before the walk date, one way to exclude inspection ros, possible exclude ros written by bear (Check with bev)
  and b.the_date <= f.date_avail -- exclude ros after available
group by ee.stocknumber, e.vin;