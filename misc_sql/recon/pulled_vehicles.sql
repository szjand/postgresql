﻿/*
run against DpsVSeries
first cut at the fully kitted page
*/

-- pulled vehicles AND mech/body status
SELECT a.vehicleinventoryitemid, a.stocknumber,
  c.YearModel, c.Make,  c.Model,
  trim(c.ExteriorColor) + '/' + TRIM(c.InteriorColor) AS Color,
  CASE e.status
        WHEN 'MechanicalReconProcess_NotStarted' THEN 'Not Started'
        WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
        WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
  END AS mechanical,
  CASE f.status
        WHEN 'BodyReconProcess_NotStarted' THEN 'Not Started'
        WHEN 'BodyReconProcess_InProcess' THEN 'WIP'
        WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
  END AS body
FROM VehicleInventoryItems a
INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
  AND b.category = 'RMFlagPulled'
  AND b.ThruTS IS NULL
LEFT JOIN VehicleItems c ON a.VehicleItemID = c.VehicleItemID
LEFT JOIN ReconAuthorizations d on a.VehicleInventoryItemID = d.VehicleInventoryItemID
  AND d.thruts IS NULL
LEFT JOIN VehicleInventoryItemStatuses e on a.VehicleInventoryItemID = e.VehicleInventoryItemID
  AND e.category = 'MechanicalReconProcess'
  AND e.thruts IS null
LEFT JOIN VehicleInventoryItemStatuses f on a.VehicleInventoryItemID = f.VehicleInventoryItemID
  AND f.category = 'BodyReconProcess'
  AND f.thruts IS NULL;
  
  
  -- remove the noise FROM pulled vehicles
  -- exclude those WHERE mech & body are no OPEN items
  -- this changes it top pulled vehicles with OPEN body OR mech recon
DELETE FROM rec_pulled_vehicles_with_open_recon; 
INSERT INTO rec_pulled_vehicles_with_open_recon  
SELECT g.*
FROM (
  SELECT a.vehicleinventoryitemid, a.stocknumber,
    c.YearModel, c.Make,  c.Model,
    trim(c.ExteriorColor) + '/' + TRIM(c.InteriorColor) AS Color,
    CASE e.status
          WHEN 'MechanicalReconProcess_NotStarted' THEN 'Not Started'
          WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
          WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
    END AS mechanical,
    CASE f.status
          WHEN 'BodyReconProcess_NotStarted' THEN 'Not Started'
          WHEN 'BodyReconProcess_InProcess' THEN 'WIP'
          WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
    END AS body,
    a.currentpriority
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
  AND b.category = 'RMFlagPulled'
  AND b.ThruTS IS NULL
  LEFT JOIN VehicleItems c ON a.VehicleItemID = c.VehicleItemID
  LEFT JOIN ReconAuthorizations d on a.VehicleInventoryItemID = d.VehicleInventoryItemID
  AND d.thruts IS NULL
  LEFT JOIN VehicleInventoryItemStatuses e on a.VehicleInventoryItemID = e.VehicleInventoryItemID
  AND e.category = 'MechanicalReconProcess'
  AND e.thruts IS null
  LEFT JOIN VehicleInventoryItemStatuses f on a.VehicleInventoryItemID = f.VehicleInventoryItemID
  AND f.category = 'BodyReconProcess'
  AND f.thruts IS NULL) g
WHERE NOT (mechanical = 'No Open Items' AND body = 'No Open Items');


EXECUTE PROCEDURE rec_update_pulled_vehicles_with_open_recon();

SELECT * FROM rec_pulled_vehicles_with_open_recon;

DROP TABLE rec_pulled_vehicles_with_open_recon
CREATE TABLE rec_pulled_vehicles_with_open_recon (
  vehicle_inventory_item_id Char(38),
  stock_number cichar(20),
  model_year cichar(4),
  make cichar(60),
  model cichar(60),
  color cichar (121),
  mechanical_status cichar(13),
  body_status cichar(13),
  current_priority integer);
  
-- open mech & body recon items

SELECT * FROM rec_pulled_vehicles_with_open_recon ORDER BY stock_number

SELECT a.stock_number, trim(f.description) collate ADS_DEFAULT_CI + ': ' + b.Description AS open_mechanical_items
FROM rec_pulled_vehicles_with_open_recon a
INNER JOIN VehicleReconItems b on a.vehicle_inventory_item_id = b.VehicleInventoryItemID 
INNER JOIN ReconAuthorizations c on a.vehicle_inventory_item_id = c.VehicleInventoryItemID 
  AND c.thruts IS NULL 
INNER JOIN authorizedreconitems d on b.VehicleReconItemId = d.VehicleReconItemID 
  AND c.ReconAuthorizationID = d.ReconAuthorizationID  
  AND d.completets IS null	
INNER JOIN typcategories e on b.typ = e.typ
  AND e.category = 'MechanicalReconItem'
INNER JOIN typdescriptions f on b.typ = f.typ    
ORDER by a.stock_number

SELECT a.stock_number, trim(f.description) collate ADS_DEFAULT_CI + ': ' + b.Description AS open_body_items
FROM rec_pulled_vehicles_with_open_recon a
INNER JOIN VehicleReconItems b on a.vehicle_inventory_item_id = b.VehicleInventoryItemID 
INNER JOIN ReconAuthorizations c on a.vehicle_inventory_item_id = c.VehicleInventoryItemID 
  AND c.thruts IS NULL 
INNER JOIN authorizedreconitems d on b.VehicleReconItemId = d.VehicleReconItemID 
  AND c.ReconAuthorizationID = d.ReconAuthorizationID  
  AND d.completets IS null	
INNER JOIN typcategories e on b.typ = e.typ
  AND e.category = 'BodyReconItem'
INNER JOIN typdescriptions f on b.typ = f.typ    
ORDER by a.stock_number


G37959A IN tool NOT here - it IS no longer pulled

  
SELECT a.stocknumber, 
  trim(f.description) collate ADS_DEFAULT_CI + ': ' + b.Description
FROM VehicleInventoryItems a
INNER JOIN VehicleReconItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN ReconAuthorizations c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
  AND c.thruts IS NULL 
INNER JOIN authorizedreconitems d on b.VehicleReconItemId = d.VehicleReconItemID 
  AND c.ReconAuthorizationID = d.ReconAuthorizationID  
  AND d.completets IS null	
INNER JOIN typcategories e on b.typ = e.typ
  AND e.category = 'MechanicalReconItem'
INNER JOIN typdescriptions f on b.typ = f.typ    
WHERE a.VehicleInventoryItemID = 'd62ca3de-8b2c-462a-9278-97eea30fb9f2'
  
 
SELECT a.stocknumber, 
  trim(f.description) collate ADS_DEFAULT_CI + ': ' + b.Description
FROM VehicleInventoryItems a
INNER JOIN VehicleReconItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN ReconAuthorizations c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
  AND c.thruts IS NULL 
INNER JOIN authorizedreconitems d on b.VehicleReconItemId = d.VehicleReconItemID 
  AND c.ReconAuthorizationID = d.ReconAuthorizationID  
  AND d.completets IS null	
INNER JOIN typcategories e on b.typ = e.typ
  AND e.category = 'BodyReconItem'
INNER JOIN typdescriptions f on b.typ = f.typ    
WHERE a.VehicleInventoryItemID = 'd62ca3de-8b2c-462a-9278-97eea30fb9f2'  
  

  
SELECT a.stock_number, b.notests, b.notes
from rec_pulled_vehicles_with_open_recon a
LEFT JOIN VehicleInventoryItemNotes b on a.vehicle_inventory_item_id = b.VehicleInventoryItemID 
ORDER BY a.stock_number, b.notests



--- base data
SELECT g.*
FROM (
  SELECT a.vehicleinventoryitemid, a.stocknumber,
    c.YearModel, c.Make,  c.Model,
    trim(c.ExteriorColor) + '/' + TRIM(c.InteriorColor) AS Color,
    CASE e.status
          WHEN 'MechanicalReconProcess_NotStarted' THEN 'Not Started'
          WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
          WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
    END AS mechanical,
    CASE f.status
          WHEN 'BodyReconProcess_NotStarted' THEN 'Not Started'
          WHEN 'BodyReconProcess_InProcess' THEN 'WIP'
          WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
    END AS body,
    a.currentpriority
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
  AND b.category = 'RMFlagPulled'
  AND b.ThruTS IS NULL
  LEFT JOIN VehicleItems c ON a.VehicleItemID = c.VehicleItemID
  LEFT JOIN ReconAuthorizations d on a.VehicleInventoryItemID = d.VehicleInventoryItemID
  AND d.thruts IS NULL
  LEFT JOIN VehicleInventoryItemStatuses e on a.VehicleInventoryItemID = e.VehicleInventoryItemID
  AND e.category = 'MechanicalReconProcess'
  AND e.thruts IS null
  LEFT JOIN VehicleInventoryItemStatuses f on a.VehicleInventoryItemID = f.VehicleInventoryItemID
  AND f.category = 'BodyReconProcess'
  AND f.thruts IS NULL) g
WHERE NOT (mechanical = 'No Open Items' AND body = 'No Open Items');


-- mech recon
SELECT aa.vehicleinventoryitemid, trim(f.description) collate ADS_DEFAULT_CI + ': ' + b.Description AS open_mechanical_items
FROM (
  SELECT g.*
  FROM (
    SELECT a.vehicleinventoryitemid, a.stocknumber,
      c.YearModel, c.Make,  c.Model,
      trim(c.ExteriorColor) + '/' + TRIM(c.InteriorColor) AS Color,
      CASE e.status
            WHEN 'MechanicalReconProcess_NotStarted' THEN 'Not Started'
            WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
            WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
      END AS mechanical,
      CASE f.status
            WHEN 'BodyReconProcess_NotStarted' THEN 'Not Started'
            WHEN 'BodyReconProcess_InProcess' THEN 'WIP'
            WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
      END AS body,
      a.currentpriority
    FROM VehicleInventoryItems a
    INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL
    LEFT JOIN VehicleItems c ON a.VehicleItemID = c.VehicleItemID
    LEFT JOIN ReconAuthorizations d on a.VehicleInventoryItemID = d.VehicleInventoryItemID
    AND d.thruts IS NULL
    LEFT JOIN VehicleInventoryItemStatuses e on a.VehicleInventoryItemID = e.VehicleInventoryItemID
    AND e.category = 'MechanicalReconProcess'
    AND e.thruts IS null
    LEFT JOIN VehicleInventoryItemStatuses f on a.VehicleInventoryItemID = f.VehicleInventoryItemID
    AND f.category = 'BodyReconProcess'
    AND f.thruts IS NULL) g
  WHERE NOT (mechanical = 'No Open Items' AND body = 'No Open Items')) aa
INNER JOIN VehicleReconItems b on aa.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN ReconAuthorizations c on aa.VehicleInventoryItemID = c.VehicleInventoryItemID 
  AND c.thruts IS NULL 
INNER JOIN authorizedreconitems d on b.VehicleReconItemId = d.VehicleReconItemID 
  AND c.ReconAuthorizationID = d.ReconAuthorizationID  
  AND d.completets IS null	
INNER JOIN typcategories e on b.typ = e.typ
  AND e.category = 'MechanicalReconItem'
INNER JOIN typdescriptions f on b.typ = f.typ    


-- body recon
SELECT aa.vehicleinventoryitemid, trim(f.description) collate ADS_DEFAULT_CI + ': ' + b.Description AS open_body_items
FROM (
  SELECT g.*
  FROM (
    SELECT a.vehicleinventoryitemid, a.stocknumber,
      c.YearModel, c.Make,  c.Model,
      trim(c.ExteriorColor) + '/' + TRIM(c.InteriorColor) AS Color,
      CASE e.status
            WHEN 'MechanicalReconProcess_NotStarted' THEN 'Not Started'
            WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
            WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
      END AS mechanical,
      CASE f.status
            WHEN 'BodyReconProcess_NotStarted' THEN 'Not Started'
            WHEN 'BodyReconProcess_InProcess' THEN 'WIP'
            WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
      END AS body,
      a.currentpriority
    FROM VehicleInventoryItems a
    INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL
    LEFT JOIN VehicleItems c ON a.VehicleItemID = c.VehicleItemID
    LEFT JOIN ReconAuthorizations d on a.VehicleInventoryItemID = d.VehicleInventoryItemID
    AND d.thruts IS NULL
    LEFT JOIN VehicleInventoryItemStatuses e on a.VehicleInventoryItemID = e.VehicleInventoryItemID
    AND e.category = 'MechanicalReconProcess'
    AND e.thruts IS null
    LEFT JOIN VehicleInventoryItemStatuses f on a.VehicleInventoryItemID = f.VehicleInventoryItemID
    AND f.category = 'BodyReconProcess'
    AND f.thruts IS NULL) g
  WHERE NOT (mechanical = 'No Open Items' AND body = 'No Open Items')) aa
INNER JOIN VehicleReconItems b on aa.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN ReconAuthorizations c on aa.VehicleInventoryItemID = c.VehicleInventoryItemID 
  AND c.thruts IS NULL 
INNER JOIN authorizedreconitems d on b.VehicleReconItemId = d.VehicleReconItemID 
  AND c.ReconAuthorizationID = d.ReconAuthorizationID  
  AND d.completets IS null	
INNER JOIN typcategories e on b.typ = e.typ
  AND e.category = 'BodyReconItem'
INNER JOIN typdescriptions f on b.typ = f.typ    


-- notes
SELECT aa.vehicleinventoryitemid, b.notests, b.notes
FROM (
  SELECT g.*
  FROM (
    SELECT a.vehicleinventoryitemid, a.stocknumber,
      c.YearModel, c.Make,  c.Model,
      trim(c.ExteriorColor) + '/' + TRIM(c.InteriorColor) AS Color,
      CASE e.status
            WHEN 'MechanicalReconProcess_NotStarted' THEN 'Not Started'
            WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
            WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
      END AS mechanical,
      CASE f.status
            WHEN 'BodyReconProcess_NotStarted' THEN 'Not Started'
            WHEN 'BodyReconProcess_InProcess' THEN 'WIP'
            WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
      END AS body,
      a.currentpriority
    FROM VehicleInventoryItems a
    INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL
    LEFT JOIN VehicleItems c ON a.VehicleItemID = c.VehicleItemID
    LEFT JOIN ReconAuthorizations d on a.VehicleInventoryItemID = d.VehicleInventoryItemID
    AND d.thruts IS NULL
    LEFT JOIN VehicleInventoryItemStatuses e on a.VehicleInventoryItemID = e.VehicleInventoryItemID
    AND e.category = 'MechanicalReconProcess'
    AND e.thruts IS null
    LEFT JOIN VehicleInventoryItemStatuses f on a.VehicleInventoryItemID = f.VehicleInventoryItemID
    AND f.category = 'BodyReconProcess'
    AND f.thruts IS NULL) g
  WHERE NOT (mechanical = 'No Open Items' AND body = 'No Open Items')) aa
INNER JOIN VehicleInventoryItemNotes b on aa.VehicleInventoryItemID = b.VehicleInventoryItemID 
ORDER BY aa.vehicleinventoryitemid, b.notests

-- 12/16 ADD key location AND physicallocation to this query
SELECT g.*
FROM (
  SELECT a.vehicleinventoryitemid, a.stocknumber,
    c.YearModel, c.Make,  c.Model,
    trim(c.ExteriorColor) + '/' + TRIM(c.InteriorColor) AS Color,
    CASE e.status
          WHEN 'MechanicalReconProcess_NotStarted' THEN 'Not Started'
          WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
          WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
    END AS mechanical,
    CASE f.status
          WHEN 'BodyReconProcess_NotStarted' THEN 'Not Started'
          WHEN 'BodyReconProcess_InProcess' THEN 'WIP'
          WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
    END AS body,
    a.currentpriority
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
  AND b.category = 'RMFlagPulled'
  AND b.ThruTS IS NULL
  LEFT JOIN VehicleItems c ON a.VehicleItemID = c.VehicleItemID
  LEFT JOIN ReconAuthorizations d on a.VehicleInventoryItemID = d.VehicleInventoryItemID
  AND d.thruts IS NULL
  LEFT JOIN VehicleInventoryItemStatuses e on a.VehicleInventoryItemID = e.VehicleInventoryItemID
  AND e.category = 'MechanicalReconProcess'
  AND e.thruts IS null
  LEFT JOIN VehicleInventoryItemStatuses f on a.VehicleInventoryItemID = f.VehicleInventoryItemID
  AND f.category = 'BodyReconProcess'
  AND f.thruts IS NULL) g
WHERE NOT (mechanical = 'No Open Items' AND body = 'No Open Items');

-- this IS the result
SELECT g.*
FROM (
  SELECT a.vehicleinventoryitemid, a.stocknumber,
    c.YearModel, c.Make,  c.Model,
    trim(c.ExteriorColor) + '/' + TRIM(c.InteriorColor) AS Color,
    CASE e.status
          WHEN 'MechanicalReconProcess_NotStarted' THEN 'Not Started'
          WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
          WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
    END AS mechanical,
    CASE f.status
          WHEN 'BodyReconProcess_NotStarted' THEN 'Not Started'
          WHEN 'BodyReconProcess_InProcess' THEN 'WIP'
          WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
    END AS body,
    a.currentpriority, 
    aa.vehicle_location,
    bb.keystatus  
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
  AND b.category = 'RMFlagPulled'
  AND b.ThruTS IS NULL
  LEFT JOIN VehicleItems c ON a.VehicleItemID = c.VehicleItemID
  LEFT JOIN ReconAuthorizations d on a.VehicleInventoryItemID = d.VehicleInventoryItemID
  AND d.thruts IS NULL
  LEFT JOIN VehicleInventoryItemStatuses e on a.VehicleInventoryItemID = e.VehicleInventoryItemID
  AND e.category = 'MechanicalReconProcess'
  AND e.thruts IS null
  LEFT JOIN VehicleInventoryItemStatuses f on a.VehicleInventoryItemID = f.VehicleInventoryItemID
  AND f.category = 'BodyReconProcess'
  AND f.thruts IS NULL
  LEFT JOIN (
    SELECT a.VehicleInventoryItemID, trim(b.LocationShortName) + ' ' + coalesce(b.locationdescription, '') AS vehicle_location
    FROM VehicleItemPhysicalLocations a
    LEFT JOIN LocationPhysicalVehicleLocations b on a.physicalvehiclelocationid = b.physicalvehiclelocationid
    WHERE a.thruts IS NULL) aa on a.VehicleInventoryItemID = aa.VehicleInventoryItemID
  LEFT JOIN keyperkeystatus bb on a.stocknumber = bb.stocknumber) g
WHERE NOT (mechanical = 'No Open Items' AND body = 'No Open Items');