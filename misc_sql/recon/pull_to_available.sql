﻿drop table if exists step_1;
create temp table step_1 as
select a.stocknumber, aa.vin, aa.yearmodel, aa.make, aa.model, 
  b.category, b.fromts::date, b.thruts::date
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_items aa on a.vehicleitemid = aa.vehicleitemid
left join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and b.category = 'RMFlagPulled'
  and b.fromts::date > '12/31/2018'
left join ads.ext_vehicle_inventory_item_statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
  and c.category = 'RMFlagAV'
where a.stocknumber not like 'H%'  
  and c.category is not null
  and b.thruts::date = c.fromts::date
order by a.stocknumber  

delete 
from step_1
where stocknumber in (
  select stocknumber
  from step_1
  group by stocknumber 
  having count(*) > 1);

select *
from step_1

-- 1158 items
select a.*, thruts - fromts 
from step_1 a

-- 8 days
select avg(thruts - fromts)
from step_1 

select thruts - fromts as days, count(*)
from step_1 
group by thruts - fromts
order by thruts - fromts

-- monthly avg
select b.year_month, count(*) as units, avg(thruts - fromts)::integer as days
from step_1 a
join dds.dim_date b on a.fromts = b.the_date
where year_month < 201912
group by b.year_month
order by year_month

select a.*, thruts - fromts 
from step_1 a
order by thruts - fromts 

-- https://leafo.net/guides/postgresql-calculating-percentile.html
-- percentile as median
select percentile_disc(0.5) within group(order by thruts-fromts)
from step_1

select percentile_cont(0.5) within group(order by thruts-fromts)
from step_1
