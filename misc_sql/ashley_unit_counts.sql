﻿select 
  case
    when sale_group = 'new chev truck' then 1
    when sale_group = 'new chev car' then 2
    when a.make = 'gmc' then 3
    when a.make = 'cadillac' then 4
    when a.make = 'buick' then 5
  end as sort,
  case
    when a.sale_group = 'new chev truck' then 'Chevy Truck'
    when a.sale_group = 'new chev car' then 'Chevy Car'
    else a.make
  end as make, 
  case 
    when a.model = 'SILVERADO 1500' and b.body_style like '%reg%' then '1500 Reg'
    when a.model = 'SILVERADO 1500' and b.body_style like '%double%' then '1500 Double'
    when a.model = 'SILVERADO 1500' and b.body_style like '%crew%' then '1500 Crew'
    when a.model = 'SILVERADO 2500hd' and b.body_style like '%reg%' then '2500 Reg'
    when a.model = 'SILVERADO 2500hd' and b.body_style like '%double%' then '2500 Double'
    when a.model = 'SILVERADO 2500hd' and b.body_style like '%crew%' then '2500 Crew'
    when a.model = 'SILVERADO 3500hd' and b.body_style like '%reg%' then '3500 Reg'
    when a.model = 'SILVERADO 3500hd' and b.body_style like '%double%' then '3500 Double'
    when a.model = 'SILVERADO 3500hd' and b.body_style like '%crew%' then '3500 Crew'    
    when a.model = 'sierra 1500' and b.body_style like '%reg%' then '1500 Reg'
    when a.model = 'sierra 1500' and b.body_style like '%double%' then '1500 Double'
    when a.model = 'sierra 1500' and b.body_style like '%crew%' then '1500 Crew'
    when a.model = 'sierra 2500hd' and b.body_style like '%reg%' then '2500 Reg'
    when a.model = 'sierra 2500hd' and b.body_style like '%double%' then '2500 Double'
    when a.model = 'sierra 2500hd' and b.body_style like '%crew%' then '2500 Crew'
    when a.model = 'sierra 3500hd' and b.body_style like '%reg%' then '3500 Reg'
    when a.model = 'sierra 3500hd' and b.body_style like '%double%' then '3500 Double'
    when a.model = 'sierra 3500hd' and b.body_style like '%crew%' then '3500 Crew'    
    else a.model
  end as model,
  coalesce(sum(unit_count) filter (where year_month = 201805), 0) as may,
  coalesce(sum(unit_count) filter (where year_month = 201806), 0) as june,
  coalesce(sum(unit_count) filter (where year_month = 201807), 0) as july,
  coalesce(sum(unit_count) filter (where year_month = 201808), 0) as august,
  coalesce(sum(unit_count) filter (where year_month = 201809), 0) as september
-- select a.year_month, a.unit_count, a.sale_group, a.make, a.model, b.body_style
from sls.deals_by_month a
left join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row = true
where a.store_code = 'ry1'
  and a.vehicle_type = 'new'
  and a.year_month > 201804
  and a.sale_type not in ('fleet','wholesale')
  and a.unit_count < 0
group by 
  case
    when sale_group = 'new chev truck' then 1
    when sale_group = 'new chev car' then 2
    when a.make = 'gmc' then 3
    when a.make = 'cadillac' then 4
    when a.make = 'buick' then 5
  end ,
  case
    when a.sale_group = 'new chev truck' then 'Chevy Truck'
    when a.sale_group = 'new chev car' then 'Chevy Car'
    else a.make
  end ,
  case 
    when a.model = 'SILVERADO 1500' and b.body_style like '%reg%' then '1500 Reg'
    when a.model = 'SILVERADO 1500' and b.body_style like '%double%' then '1500 Double'
    when a.model = 'SILVERADO 1500' and b.body_style like '%crew%' then '1500 Crew'
    when a.model = 'SILVERADO 2500hd' and b.body_style like '%reg%' then '2500 Reg'
    when a.model = 'SILVERADO 2500hd' and b.body_style like '%double%' then '2500 Double'
    when a.model = 'SILVERADO 2500hd' and b.body_style like '%crew%' then '2500 Crew'
    when a.model = 'SILVERADO 3500hd' and b.body_style like '%reg%' then '3500 Reg'
    when a.model = 'SILVERADO 3500hd' and b.body_style like '%double%' then '3500 Double'
    when a.model = 'SILVERADO 3500hd' and b.body_style like '%crew%' then '3500 Crew'        
    when a.model = 'sierra 1500' and b.body_style like '%reg%' then '1500 Reg'
    when a.model = 'sierra 1500' and b.body_style like '%double%' then '1500 Double'
    when a.model = 'sierra 1500' and b.body_style like '%crew%' then '1500 Crew'
    when a.model = 'sierra 2500hd' and b.body_style like '%reg%' then '2500 Reg'
    when a.model = 'sierra 2500hd' and b.body_style like '%double%' then '2500 Double'
    when a.model = 'sierra 2500hd' and b.body_style like '%crew%' then '2500 Crew'
    when a.model = 'sierra 3500hd' and b.body_style like '%reg%' then '3500 Reg'
    when a.model = 'sierra 3500hd' and b.body_style like '%double%' then '3500 Double'
    when a.model = 'sierra 3500hd' and b.body_style like '%crew%' then '3500 Crew'    
    else a.model
  end  
order by sort,
  case 
    when a.model = 'SILVERADO 1500' and b.body_style like '%reg%' then '1500 Reg'
    when a.model = 'SILVERADO 1500' and b.body_style like '%double%' then '1500 Double'
    when a.model = 'SILVERADO 1500' and b.body_style like '%crew%' then '1500 Crew'
    when a.model = 'SILVERADO 2500hd' and b.body_style like '%reg%' then '2500 Reg'
    when a.model = 'SILVERADO 2500hd' and b.body_style like '%double%' then '2500 Double'
    when a.model = 'SILVERADO 2500hd' and b.body_style like '%crew%' then '2500 Crew'
    when a.model = 'SILVERADO 3500hd' and b.body_style like '%reg%' then '3500 Reg'
    when a.model = 'SILVERADO 3500hd' and b.body_style like '%double%' then '3500 Double'
    when a.model = 'SILVERADO 3500hd' and b.body_style like '%crew%' then '3500 Crew'        
    when a.model = 'sierra 1500' and b.body_style like '%reg%' then '1500 Reg'
    when a.model = 'sierra 1500' and b.body_style like '%double%' then '1500 Double'
    when a.model = 'sierra 1500' and b.body_style like '%crew%' then '1500 Crew'
    when a.model = 'sierra 2500hd' and b.body_style like '%reg%' then '2500 Reg'
    when a.model = 'sierra 2500hd' and b.body_style like '%double%' then '2500 Double'
    when a.model = 'sierra 2500hd' and b.body_style like '%crew%' then '2500 Crew'
    when a.model = 'sierra 3500hd' and b.body_style like '%reg%' then '3500 Reg'
    when a.model = 'sierra 3500hd' and b.body_style like '%double%' then '3500 Double'
    when a.model = 'sierra 3500hd' and b.body_style like '%crew%' then '3500 Crew'    
    else a.model
  end 
