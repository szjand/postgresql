﻿-- report is generated in luigi in the misc.py module
----------------------------------------------------------------------
--< interest credit queries
----------------------------------------------------------------------  
04/19 new rates and days effective 4/21

select * from nc.gm_interest_credit_parameters

update nc.gm_interest_credit_parameters
set thru_date = '04/20/2020';
insert into nc.gm_interest_credit_parameters (make,rate,days,from_date) values 
('buick', 0.0425, 157, '04/21/2020'),
('gmc', 0.0425, 157, '04/21/2020'),
('chevrolet', 0.0425, 157, '04/21/2020'),
('cadillac', 0.0425, 126, '04/21/2020');
alter table nc.gm_interest_credit_parameters
add primary key(make,from_date);

-- vehicles already invoiced  ---------------------------------------------------------------------------------------
-- matched with feb 2020, jan 2020, dec 2019 off by one chev for which we do not have the invoice
-- 3/13 cleaned up where clause
select b.make, a.vin, a.total_invoice, 
  left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date as invoice_date,
  round(((a.total_invoice *.0575)/360) * case when b.make = 'cadillac' then 93 else 120 end, 2),
  sum(round(((a.total_invoice *.0575)/360) * case when b.make = 'CADILLAC' then 93 else 120 end, 2)) over (partition by b.make)
from gmgl.vehicle_invoices a
join jo.vehicle_orders b on a.vin = b.vin
-- exclude fleet orders
join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
  and c.vehicle_order_type not like 'Fleet%'  
where a.thru_date > current_date
  and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between '02/21/2021' and '03/20/2021'
  and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
and ((
  position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
  or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0)) 
order by b.make, substring(a.vin, 10, 2),  substring(a.vin, 12, 6) 

-- total for the time period
select -- b.make, a.vin, a.total_invoice, 
  sum(round(((a.total_invoice *.0575)/360) * case when b.make = 'cadillac' then 93 else 120 end, 2))::integer, count(*)
from gmgl.vehicle_invoices a
join jo.vehicle_orders b on a.vin = b.vin
join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
  and c.vehicle_order_type not like 'Fleet%'  
where a.thru_date > current_date
  and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between '02/21/2021' and '03/20/2021'
  and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
and ((
  position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
  or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0)) 

-- total by make for the time period
select b.make,
  sum(round(((a.total_invoice *.0575)/360) * case when b.make = 'cadillac' then 93 else 120 end, 2))::integer, count(*)
from gmgl.vehicle_invoices a
join jo.vehicle_orders b on a.vin = b.vin
join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
  and c.vehicle_order_type not like 'Fleet%'  
where a.thru_date > current_date
  and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between '02/21/2021' and '03/20/2021'
  and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
and ((
  position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
  or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0)) 
group by b.make        



-- estimates ----------------------------------------------------------------------------------------------------

-- select a.*, d.ground, b.order_type, b.alloc_group, b.vin, b.model_year, b.make, c.model, c.body_style, 
--   case when shipped is null then null else shipped - placed end as order_to_ship_days,
--   case when ground is null then null else ground - shipped end as ship_to_ground_days, b.tpw
-- from (
--   select order_number, 
--     max(case when event_code = '2000' then effective_date end) as placed,
--     max(case when event_code = '3000' then effective_date end) as accepted,
--     max(case when event_code = '4150' then effective_date end) as invoiced,
--     min(case when event_code like '4%' then effective_date end) as shipped
--   from jo.vehicle_order_events
--   group by order_number) a
-- left join jo.vehicle_orders b on a.order_number = b.order_number  
-- left join arkona.xfm_inpmast c on b.vin = c.inpmast_vin
--   and c.current_row
-- left join (
--   select vin, min(ground_date) as ground
--   from nc.vehicle_acquisitions 
--   group by vin) d on b.vin = d.vin  
-- -- where a.order_number = 'XMJMVC'  
-- where model_year = 2020
--   and coalesce(invoiced, '12/31/9999') > '02/20/2020'
-- order by alloc_group, model_year, placed
-- 
-- select * from jo.vehicle_order_types
-- 
-- -- what order types to exclude
-- select a.order_type, min(vin), max(vin), count(*)
-- from jo.vehicle_orders a
-- join jo.vehicle_order_events b on a.order_number = b.order_number
--   and b.event_code = '4150'
--   and b.effective_date between '01/21/2020' and '02/20/2020'  -- for the month of february
-- join jo.vehicle_order_types c on a.order_type = c.vehicle_order_type_key
--   and c.vehicle_order_type not like 'Fleet%'  
-- group by a.order_type  
-- 
-- -- need to estimate order where it is anticipated that they will be invoiced in the relevant date range
-- select a.order_number, a.vin, b.*
-- from jo.vehicle_orders a
-- join jo.vehicle_order_events b on a.order_number = b.order_number
--   and b.event_code = '4150'
--   and b.effective_date between '02/21/2020' and '03/20/2020'  -- for the month of march
-- join jo.vehicle_order_types c on a.order_type = c.vehicle_order_type_key
--   and c.vehicle_order_type not like 'Fleet%'  
-- 
-- -- all invoices for model year 2020 vehicles
-- drop table if exists invoices;
-- create temp table invoices as
-- select vin, total_invoice
-- from gmgl.vehicle_invoices
-- where left(raw_invoice, 4) = '2020'
--   and thru_date > current_date;

-- avg total invoice aount by alloc_group/peg
select alloc_group, peg, 
  case 
    when the_count = 1 then unnest(invoices)
    when avg < median then avg
    else median
  end::integer as peg_invoice
from (  
  select alloc_group, peg, the_count,
    case when the_count = 1 then null else (select min(a) from unnest(invoices) a) end as the_min,
    case when the_count = 1 then null else (select max(a) from unnest(invoices) a) end as the_max,
    case when the_count = 1 then null else (select sum(a) from unnest(invoices) a)/the_count end as avg,
    case when the_count = 1 then null else jon.median((invoices))::integer end as median,
    invoices
  from (  
  select d.alloc_group, d.peg, count(*) as the_count, array_agg(total_invoice) as invoices
  from gmgl.vehicle_invoices a
  join nc.vehicles b on a.vin = b.vin
    and b.model_year = 2020
  join nc.vehicle_configurations c on b.configuration_id = c.configuration_id  
  join jon.configurations d on c.chrome_style_id = d.chrome_Style_id
  group by d.alloc_group, d.peg) x) xx

-- avg total invoice aount by alloc_group
select alloc_group, 
  case 
    when the_count = 1 then unnest(invoices)
    when avg < median then avg
    else median
  end::integer as alloc_invoice
from (  
  select alloc_group, the_count,
    case when the_count = 1 then null else (select min(a) from unnest(invoices) a) end as the_min,
    case when the_count = 1 then null else (select max(a) from unnest(invoices) a) end as the_max,
    case when the_count = 1 then null else (select sum(a) from unnest(invoices) a)/the_count end as avg,
    case when the_count = 1 then null else jon.median((invoices))::integer end as median,
    invoices
  from (  
  select d.alloc_group, count(*) as the_count, array_agg(total_invoice) as invoices
  from gmgl.vehicle_invoices a
  join nc.vehicles b on a.vin = b.vin
    and b.model_year = 2020
  join nc.vehicle_configurations c on b.configuration_id = c.configuration_id  
  join jon.configurations d on c.chrome_style_id = d.chrome_Style_id
  group by d.alloc_group) x) xx  

    
-- average time to invoice by peg
-- going with the lesser value between avg and median
-- this needs to be figured live, each day
drop table if exists est_days_to_invoice_peg cascade;
create temp table est_days_to_invoice_peg as
select alloc_group, peg, 
  case 
    when the_count = 1 then unnest(days)
    when avg < median then avg
    else median
  end as peg_days
from (
  select alloc_group, peg, the_count,
    case when the_count = 1 then null else (select min(a) from unnest(days) a) end as the_min,
    case when the_count = 1 then null else (select max(a) from unnest(days) a) end as the_max,
    case when the_count = 1 then null else (select sum(a) from unnest(days) a)/the_count end as avg,
    case when the_count = 1 then null else jon.median((days))::integer end as median,
    days
  from (  
    select a.alloc_group, a.peg, count(*) as the_count,
      array_agg(c.effective_date - b.effective_date order by c.effective_date - b.effective_date) as days
    from jo.vehicle_orders a
    join jo.vehicle_order_events b on a.order_number = b.order_number
      and b.event_code = '2000'
    join jo.vehicle_order_events c on a.order_number = c.order_number
      and c.event_code = '4150'  
    where a.model_year = 2020
    group by a.alloc_group, a.peg) x) xx;

-- average time to invoice by alloc_group
-- going with the lesser value between avg and median
-- this needs to be figured live, each day
drop table if exists est_days_to_invoice_alloc cascade;
create temp table est_days_to_invoice_alloc as
select alloc_group, 
  case 
    when the_count = 1 then unnest(days)
    when avg < median then avg
    else median
  end as alloc_days
from (
  select alloc_group, the_count,
    case when the_count = 1 then null else (select min(a) from unnest(days) a) end as the_min,
    case when the_count = 1 then null else (select max(a) from unnest(days) a) end as the_max,
    case when the_count = 1 then null else (select sum(a) from unnest(days) a)/the_count end as avg,
    case when the_count = 1 then null else jon.median((days))::integer end as median,
    days
  from (  
    select a.alloc_group, count(*) as the_count,
      array_agg(c.effective_date - b.effective_date order by c.effective_date - b.effective_date) as days
    from jo.vehicle_orders a
    join jo.vehicle_order_events b on a.order_number = b.order_number
      and b.event_code = '2000'
    join jo.vehicle_order_events c on a.order_number = c.order_number
      and c.event_code = '4150'  
    where a.model_year = 2020
    group by a.alloc_group) x) xx;
    

the only 2020 canyon we have seen is a dealer trade


select * from jo.vehicle_orders where order_number in ('XKKS3M','XKKS3N','XMJWKX','	XNJB94','XNJB95')
select * from jo.vehicle_orders where vin = '1GTG6EENXL1146418'

select * from jo.vehicle_order_events where order_number in ('XKKS3M','XKKS3N','XMJWKX','	XNJB94','XNJB95')

select * from nc.vehicles where model_year = 2020 and model = 'canyon'
-------------------------------------------------------------
-- still unclear on how best to determine which order to include in the estimate
-- orders for that don't exist in invoices

-- -- this gives me 278 orders, 69 with a vin
-- select aa.*, bb.event_code, bb.active, bb.effective_date
-- from (
--   select order_number, vin
--   from jo.vehicle_orders
--   where model_year = 2020
--   except 
--   select a.order_number, a.vin
--   from jo.vehicle_orders a
--   join gmgl.vehicle_invoices b on a.vin = b.vin
--   where a.model_year = 2020) aa
-- join jo.vehicle_order_events bb on aa.order_number = bb.order_number
--   and bb.event_code = '2000'
-- order by aa.vin
-- 
-- -- of the above with vins, some of these have 4xxx or above event codes
-- -- so eliminate those, and we should be good enough
-- -- provides 42 rows 
-- select aa.*
-- from (
--   select order_number, vin
--   from jo.vehicle_orders
--   where model_year = 2020
--   except 
--   select a.order_number, a.vin
--   from jo.vehicle_orders a
--   join gmgl.vehicle_invoices b on a.vin = b.vin
--   where a.model_year = 2020) aa
-- join (
--   select order_number, max(event_code) as event_code
--   from jo.vehicle_order_events 
--   group by order_number) bb on aa.order_number = bb.order_number  
-- where vin is not null
--   and left(event_code, 1)::integer < 4

-- so combine the 2
-- for a total of 251 orders
drop table if exists ordered_not_invoiced cascade;
create temp table ordered_not_invoiced as
select aa.*
from (
  select order_number, vin
  from jo.vehicle_orders
  where model_year = 2020
  except 
  select a.order_number, a.vin
  from jo.vehicle_orders a
  join gmgl.vehicle_invoices b on a.vin = b.vin
  where a.model_year = 2020) aa
join jo.vehicle_order_events bb on aa.order_number = bb.order_number
  and bb.event_code = '2000'
where vin is null  
union
select aa.*
from (
  select order_number, vin
  from jo.vehicle_orders
  where model_year = 2020
  except 
  select a.order_number, a.vin
  from jo.vehicle_orders a
  join gmgl.vehicle_invoices b on a.vin = b.vin
  where a.model_year = 2020) aa
join (
  select order_number, max(event_code) as event_code
  from jo.vehicle_order_events 
  group by order_number) bb on aa.order_number = bb.order_number  
where vin is not null
  and left(event_code, 1)::integer < 4;
create unique index on ordered_not_invoiced(order_number);  


-- start with the date, only need to estimate invoice amounts (ie interest credit) for those estimates
-- that fall within the time range
-- need tpw, 2000 date, alloc_group & peg, estimated invoice date
-- canyon is all nulls because the only one we've seen so far is a dealer trade

/*
order of precedence:
  if alloc_group = ENVISN then 2000 + peg
  tpw + 7 days
  2000 + peg_days
  2000 + alloc_days
*/

-- estimated invoice date
select *
from (
  select a.order_number, b.alloc_group, b.peg, b.tpw, c.effective_date as "2000", d.peg_days, e.alloc_days,
    b.tpw + 7 as tpw_7,
    c.effective_date + d.peg_days::integer peg_2000,
    c.effective_date + alloc_days::integer alloc_2000,
    case
      when b.alloc_group in('ENCRGX', 'ENVISN') then c.effective_date + d.peg_days::integer 
      when b.tpw is not null then b.tpw + 7
      when d.peg_days is not null then c.effective_date + d.peg_days::integer
      else c.effective_date + alloc_days::integer 
    end as est_invoice_date
  from ordered_not_invoiced a
  join jo.vehicle_orders b on a.order_number = b.order_number
  join jo.vehicle_order_events c on a.order_number = c.order_number
    and c.event_code = '2000'
  left join est_days_to_invoice_peg d on b.alloc_group = d.alloc_group and b.peg = d.peg  
  left join est_days_to_invoice_alloc e on b.alloc_group = e.alloc_group order by alloc_group, peg) x
where est_invoice_date between '02/21/2020' and '03/20/2020'  
order by alloc_group, peg

select b.tpw, b.model_year, a.order_number, a.event_code, a.effective_Date, a.event_description
from jo.vehicle_order_events a
join jo.vehicle_orders b on a.order_number = b.order_number
  and b.alloc_group = 'envisn'
order by a.order_number, a.event_code  

select a.order_number, a.tpw, b.event_code, b.effective_date, c.event_code, c.effective_date, d.event_code,d.effective_date
from jo.vehicle_orders a
join jo.vehicle_order_events b on a.order_number = b.order_number
  and b.event_code = '2000'
join jo.vehicle_order_events c on a.order_number = c.order_number
  and left(c.event_code, 1) = '4'
join jo.vehicle_order_events d on a.order_number = d.order_number
  and left(d.event_code, 1) = '5'    
where a.alloc_group = 'ENCRGX'

-- -----------------as opposed to this, which just gets orders without vins
-- -- estimated
-- -- orders that have been placed but not yet invoiced
-- -- placed orders, no vin      
-- -- what about tpw date
-- select a.order_number, a.order_type, a.tpw, a.make, a.alloc_group, a.peg, b.event_code, b.effective_date, c.peg_days,
--   b.effective_date + c.peg_days
-- from jo.vehicle_orders a
-- join jo.vehicle_order_events b on a.order_number = b.order_number
--   and b.event_code = '2000'
-- left join (
--   select alloc_group, peg, 
--     case 
--       when the_count = 1 then unnest(days)
--       when avg < median then avg
--       else median
--     end:: integer as peg_days
--   from (
--     select alloc_group, peg, the_count,
--       case when the_count = 1 then null else (select min(a) from unnest(days) a) end as the_min,
--       case when the_count = 1 then null else (select max(a) from unnest(days) a) end as the_max,
--       case when the_count = 1 then null else (select sum(a) from unnest(days) a)/the_count end as avg,
--       case when the_count = 1 then null else jon.median((days))::integer end as median,
--       days
--     from (  
--       select a.alloc_group, a.peg, count(*) as the_count,
--         array_agg(c.effective_date - b.effective_date order by c.effective_date - b.effective_date) as days
--       from jo.vehicle_orders a
--       join jo.vehicle_order_events b on a.order_number = b.order_number
--         and b.event_code = '2000'
--       join jo.vehicle_order_events c on a.order_number = c.order_number
--         and c.event_code = '4150'  
--       where a.model_year = 2020
--       group by a.alloc_group, a.peg) x) xx) c on a.alloc_group = c.alloc_group 
--         and a.peg = c.peg 
-- where a.model_year = 2020
--   and a.vin is null
-- order by b.effective_date  


create table nc.tmp_interest_credits (
  seq integer,
  the_data citext);

select the_data from nc.tmp_interest_credits   

create table nc.gm_interest_credit_parameters (
  make citext not null,
  rate numeric not null,
  days integer not null,
  from_date date,
  thru_date date default '12/31/9999');
create index on nc.gm_interest_credit_parameters(make);
create index on nc.gm_interest_credit_parameters(thru_date);
  
insert into nc.gm_interest_credit_parameters (make,rate,days,from_date) values
('buick', .0575, 120, current_date),
('gmc', .0575, 120, current_date),
('chevrolet', .0575, 120, current_date),
('cadillac', .0575, 93, current_date);

-- change this: 
select -- b.make, a.vin, a.total_invoice, 
  sum(round(((a.total_invoice *.0575)/360) * case when b.make = 'CADILLAC' then 93 else 120 end, 2))::integer, count(*)
from gmgl.vehicle_invoices a
join jo.vehicle_orders b on a.vin = b.vin
join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
  and c.vehicle_order_type not like 'Fleet%'  
where a.thru_date > current_date
  and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between '02/21/2020' and '03/20/2020'
  and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
and ((
  position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
  or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0)) 

-- to this:  
select -- b.make, a.vin, a.total_invoice, 
  sum(round(((a.total_invoice * d.rate)/360) * d.days, 2))::integer, count(*)
from gmgl.vehicle_invoices a
join jo.vehicle_orders b on a.vin = b.vin
join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
  and c.vehicle_order_type not like 'Fleet%'  
join nc.gm_interest_credit_parameters d on upper(b.make) = upper(d.make)  
where a.thru_date > current_date
  and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between '02/21/2020' and '03/20/2020'
  and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
and ((
  position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
  or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0)) 
----------------------------------------------------------------------
--/> interest credit queries
----------------------------------------------------------------------  

