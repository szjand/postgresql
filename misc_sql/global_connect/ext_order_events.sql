﻿-- Table: jon.ext_order_events
-- Table: jon.ext_order_events

create schema jo;
comment on schema jo is 'temporary schema for jon developing vehicle order data model';
drop table if exists jo.ext_vehicle_order_events;
create table jo.ext_vehicle_order_events (
  order_number citext not null references gmgl.vehicle_orders(order_number),
  event_code citext NOT NULL,
  active citext, 
  event_description citext,
  effective_date date,
  event_timestamp timestamptz,
  end_date date,
  system_source citext,
  userid citext) ;


create table jo.ext_vehicle_orders_unavailable (order_number citext not null);


create table jo.vehicle_orders_unavailable (order_number citext primary key);


CREATE OR REPLACE FUNCTION jo.update_vehicle_orders_unavailable()
  RETURNS void AS
$BODY$
/*
select jo.update_vehicle_orders_unavailable();
*/
  insert into jo.vehicle_orders_unavailable
  select distinct order_number
  from jo.ext_vehicle_orders_unavailable a
  where not exists (
    select 1
    from jo.vehicle_orders_unavailable
    where order_number = a.order_number);  

  truncate jo.ext_vehicle_orders_unavailable;    
$BODY$
language sql;

select count(*) from jo.ext_vehicle_orders_unavailable; -- 0
select count(*) from jo.vehicle_orders_unavailable  -- 1086


CREATE TABLE jo.vehicle_order_types (
  vehicle_order_type_key citext NOT NULL,
  vehicle_order_type citext NOT NULL,
  CONSTRAINT vehicle_order_types_pkey PRIMARY KEY (vehicle_order_type_key));

-- insert into jo.vehicle_order_types
-- select * from gmgl.vehicle_order_types;

CREATE TABLE jo.vehicle_orders(
  order_number citext NOT NULL,
  order_type citext NOT NULL,
  msrp_dfc citext,
  tpw date,
  vin citext,
  age_of_inventory integer,
  model_year integer,
  alloc_group citext,
  vehicle_trim text,
  color text,
  make text,
  model_code citext,
  trade citext,
  assigned citext,
  gm_config_id citext,
  dan citext,
  est_delivery_date date,
  secondary_color citext,
  changed citext,
  stock_number citext,
  peg citext,
  primary_fan citext,
  end_user_fan citext,
  po_number citext,
  ship_to_bac citext,
  ship_to_bfc citext,
  request_id citext,
  customer_name citext,
  gm_stored_config_description citext,
  CONSTRAINT vehicle_orders_pkey PRIMARY KEY (order_number),
  CONSTRAINT vehicle_order_type_fk FOREIGN KEY (order_type)
      REFERENCES jo.vehicle_order_types (vehicle_order_type_key) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION);

select count(*) from gmgl.vehicle_orders; --4356
select count(*) from jo.vehicle_orders; -- 4356

-- select string_agg('excluded.' || column_name,',') from information_schema.columns where table_schema = 'jo' and table_name = 'vehicle_orders'

insert into jo.vehicle_orders
select *
from gmgl.vehicle_orders 
on conflict (order_number)
do update
  set (order_type,msrp_dfc,tpw,vin,age_of_inventory,model_year,alloc_group,
        vehicle_trim,color,make,model_code,trade,assigned,gm_config_id,
        dan,est_delivery_date,secondary_color,changed,stock_number,peg,
        primary_fan,end_user_fan,po_number,ship_to_bac,ship_to_bfc,
        request_id,customer_name,gm_stored_config_description)
    = (excluded.order_type,excluded.msrp_dfc,excluded.tpw,excluded.vin,
        excluded.age_of_inventory,excluded.model_year,excluded.alloc_group,
        excluded.vehicle_trim,excluded.color,excluded.make,excluded.model_code,
        excluded.trade,excluded.assigned,excluded.gm_config_id,excluded.dan,
        excluded.est_delivery_date,excluded.secondary_color,excluded.changed,
        excluded.stock_number,excluded.peg,excluded.primary_fan,
        excluded.end_user_fan,excluded.po_number,excluded.ship_to_bac,
        excluded.ship_to_bfc,excluded.request_id,excluded.customer_name,
        excluded.gm_stored_config_description);
    


CREATE TABLE jo.vehicle_order_event_codes (
  code citext NOT NULL,
  description citext,
  category citext,
  CONSTRAINT vehicle_event_pk PRIMARY KEY (code));

insert into jo.vehicle_order_event_codes
select * 
from gmgl.vehicle_event_codes


drop table if exists jo.vehicle_order_events;
create table jo.vehicle_order_events (
  order_number citext not null,
  event_code citext not null references jo.vehicle_order_event_codes(code),
  active citext,
  event_description citext not null,
  effective_date date,
  event_timestamp timestamptz not null,
  end_date date,
  system_source citext,
  userid citext,
  primary key(order_number,event_code,event_description,event_timestamp));

insert into jo.vehicle_order_events
select order_number, event_code, active, 
  event_description, effective_date, event_timestamp, 
  max(end_date) as end_date, system_source, userid
from ( -- adds the appropriate active for the relevant end date
  select b.active, a.*
  from ( -- max(end_date), excludes active
    select order_number, event_code, event_description,
      effective_date, event_timestamp, 
      max(end_date) as end_date,
      system_source, userid
    from jo.ext_vehicle_order_events a
    group by order_number, event_code, event_description,
      effective_date, event_timestamp, system_source, userid) a
  join jo.ext_vehicle_order_events b on a.order_number = b.order_number
    and a.event_code = b.event_code
    and a.event_timestamp = b.event_timestamp
    and a.event_description = b.event_description
    and coalesce(a.end_date, '12/31/9999') = coalesce(b.end_date, '12/31/9999')) c
group by order_number, active, event_code, event_description,
  effective_date, event_timestamp, system_source, userid;


create or replace function jo.update_vehicle_order_events()
  RETURNS void AS
$BODY$
/*
  select jo.update_vehicle_order_events()
*/
  insert into jo.vehicle_order_events
  select order_number, event_code, active, 
    event_description, effective_date, event_timestamp, 
    max(end_date) as end_date, system_source, userid
  from ( -- adds the appropriate active for the relevant end date
    select b.active, a.*
    from ( -- max(end_date), excludes active
      select order_number, event_code, event_description,
        effective_date, event_timestamp, 
        max(end_date) as end_date,
        system_source, userid
      from jo.ext_vehicle_order_events a
      group by order_number, event_code, event_description,
        effective_date, event_timestamp, system_source, userid) a
    join jo.ext_vehicle_order_events b on a.order_number = b.order_number
      and a.event_code = b.event_code
      and a.event_timestamp = b.event_timestamp
      and a.event_description = b.event_description
      and coalesce(a.end_date, '12/31/9999') = coalesce(b.end_date, '12/31/9999')) c
  group by order_number, active, event_code, event_description,
    effective_date, event_timestamp, system_source, userid
  on conflict(order_number,event_code,event_description,event_timestamp)
  do update
    set (active, effective_date,end_date,system_source,userid) 
      =
        (excluded.active, excluded.effective_date, excluded.end_date, excluded.system_source, excluded.userid);

  truncate jo.ext_vehicle_order_events;    
  
$BODY$
language sql;  



------------------------------------------------------------------------------------
--< some data cleanup
------------------------------------------------------------------------------------
-- cancelled orders: indicated by current event = 0000
drop table if exists cancelled;
create temp table cancelled as
select order_number
from jo.vehicle_order_events 
where event_code = '0000'

-- dammit, these are already in jo.vehicle_orders_unavailable from 3/5
-- i did all the deleting then, but they are back on 3/6
-- WWMFCT WWMFCV WWMFCW WWSVQD WWSVQF WWSVQG
select *
from jo.vehicle_orders_unavailable a
join cancelled b on a.order_number = b.order_number
-- do it all again

insert into jo.vehicle_orders_unavailable
select order_number
from cancelled a
on conflict do nothing;

delete 
from jo.vehicle_order_events a
using cancelled b
where a.order_number = b.order_number;

delete 
from jo.vehicle_orders a
using cancelled b
where a.order_number = b.order_number;

                

------------------------------------------------------------------------------------
--< some data cleanup
------------------------------------------------------------------------------------

select *
  from jo.vehicle_orders_unavailable
  where order_number in ('XQWPJH','XQWPMT','XQWPMW')
  
------------------------------------------------------------------------------------
--< while still manual, the order in which things need to be done
------------------------------------------------------------------------------------
1.
gmgl.vehicle_orders complete in luigi

2.
-- update jo.vehicle_orders with data from gmgl.vehicle_orders
/*
modified in an attempt to maintain tpw for the life of the order, i believe that at some
point in time tpw is nulled out, i want to keep the value in jo.vehicle_orders,
eg, if the in gmgl.vehicle_orders the value of tpw is null but the value in jo.vehicle_orders
is not null, the query will maintain the value that already exists in jo.vehicle_orders
using: 
here's a query to expose the situation (i think):
select a.order_number, a.tpw, b.tpw
from gmgl.vehicle_orders a
join jo.vehicle_orders b on a.order_number = b.order_number
  and coalesce(a.tpw, current_date) <> coalesce(b.tpw, current_date)
where a.tpw is null  

eg
  XMJT6M
    on 3/12 tpw = 02/02/03
    on 3/13 tpw is null
select * from jo.vehicle_orders where order_number = 'XMJT6M'    

this clause preserves the tpw date
    = excluded.order_type,excluded.msrp_dfc,
        coalesce(excluded.tpw, vehicle_orders.tpw),



insert into jo.vehicle_orders
select *
from gmgl.vehicle_orders a  
where  not exists (-- exclude orders_unavailable
  select 1
  from jo.vehicle_orders_unavailable
  where order_number = a.order_number)
on conflict (order_number)
do update
  set (order_type,msrp_dfc,tpw,vin,age_of_inventory,model_year,alloc_group,
        vehicle_trim,color,make,model_code,trade,assigned,gm_config_id,
        dan,est_delivery_date,secondary_color,changed,stock_number,peg,
        primary_fan,end_user_fan,po_number,ship_to_bac,ship_to_bfc,
        request_id,customer_name,gm_stored_config_description)
    = (excluded.order_type,excluded.msrp_dfc,
        coalesce(excluded.tpw, vehicle_orders.tpw),
        excluded.vin,
        excluded.age_of_inventory,excluded.model_year,excluded.alloc_group,
        excluded.vehicle_trim,excluded.color,excluded.make,excluded.model_code,
        excluded.trade,excluded.assigned,excluded.gm_config_id,excluded.dan,
        excluded.est_delivery_date,excluded.secondary_color,excluded.changed,
        excluded.stock_number,excluded.peg,excluded.primary_fan,
        excluded.end_user_fan,excluded.po_number,excluded.ship_to_bac,
        excluded.ship_to_bfc,excluded.request_id,excluded.customer_name,
        excluded.gm_stored_config_description);

*/

3.
nc.vehicle_acquisitions -- manual all_nc_inventory_nightly

        
Python:
4.
  Orders1: 
    empties the directories ...global_connect/html_files and global_connect/orders_not_found
    scrapes the event history page for each order into an html file (event history) file or a text file (order not accessible)
5.    
  VehicleOrderEvents:
    populates jo.ext_vehicle_order_events with data from the html files
    populates jo.ext_vehicle_orders_unavailable with data from the text files
    runs select jo.update_vehicle_orders_unavailable():
      inserts order_numbers into jo.vehicle_orders_unavailable from jo.ext_vehicle_orders_unavailable
  select jo.update_vehicle_order_events()
  function jo.update_vehicle_order_events():
    insert into jo.vehicle_order_events from jo.ext_vehicle_order_events
    on conflict do update
    truncate jo.ext_vehicle_order_events


-- 3/21 saturday, looks like no updates
-- no changes from 03/27 thru 3/31
select count(*) from jo.vehicle_orders_unavailable -- 1101/1104/1105/1108/1109/1110/1113/1119/1120/1121/1123

select count(distinct order_number) from jo.vehicle_order_events; -- 2423/2429/2446/2450/2500/2533/2575

select count(*) from jo.vehicle_order_events; -- 37817/37899/37930/38164/38336/38409/38595/38694/38914/39457/39733/40242/41035
------------------------------------------------------------------------------------
--/> while still manual, the order in which things need to be done
------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------
--< best query for luigi
--------------------------------------------------------------------------------------
-- 3/7
-- figure out if this is the "best" query for what is needed
-- looks like it
drop table if exists the_current;
create temp table the_current as
                    select distinct a.order_number
                    from jo.vehicle_orders a
                    left join jo.vehicle_orders_unavailable b on a.order_number = b.order_number
                    left join nc.vehicle_acquisitions c on a.vin = c.vin
                    where b.order_number is null
                      and c.vin is null
                    union
                    select distinct a.order_number
                    from jo.vehicle_orders a
                    left join jo.vehicle_orders_unavailable b on a.order_number = b.order_number
                    where a.order_number in (
                      select distinct order_number 
                      from jo.vehicle_order_events a
                      where not exists (
                        select 1
                        from jo.vehicle_order_events
                        where order_number = a.order_number
                          and left(event_code, 1) in ('6')))
                    and b.order_number is null
order by order_number        

-- 03/05 current query
-- leaving orders without a 6xxx event in, the 6xxx has nothing to do with the timeline placed -> on ground
-- but i can imagine a situation in which someone wants to verify the accuracy of global connect 6xxxx events
drop table if exists the_current;   -- 629
create temp table the_current as

-- 2 part query
--   part 1:
--     orders not in jo.vehicle_orders_unavailable
--     orders not yet in nc.vehicle_acquisitions
select distinct a.order_number  
from jo.vehicle_orders a
left join jo.vehicle_orders_unavailable b on a.order_number = b.order_number
left join nc.vehicle_acquisitions c on a.vin = c.vin
where b.order_number is null
  and c.vin is null
union
-- part 2:
--   orders not in jo.vehicle_orders_unavailable
--   orders that dont yet have a 6xxx event
--   these may already be on the ground, but do not yet have a 6xxx event
select distinct a.order_number  -- 648/623
from jo.vehicle_orders a
left join jo.vehicle_orders_unavailable b on a.order_number = b.order_number 
where a.order_number in (
  select distinct order_number  -- 642
  from jo.vehicle_order_events a
  where not exists (
    select 1
    from jo.vehicle_order_events
    where order_number = a.order_number
      and left(event_code, 1) in ('6')))
and b.order_number is null
order by order_number

--------------------------------------------------------------------------------------
--/> best query for luigi
--------------------------------------------------------------------------------------

----------------------------------------------------------------------
--< effective_date = event_timestamp ?
----------------------------------------------------------------------
-- it appears that effective_date is the intended date, event_timestamp is the actual
-- difference: 6941 of 6987 are 3 days or less, 6339 are 1 day
select event_Timestamp::date - effective_date, a.*
from jo.vehicle_order_events a
where effective_date <> event_Timestamp::date
  and left(event_code, 1)::integer < 5
order by event_Timestamp::date - effective_date

----------------------------------------------------------------------
--/> effective_date = event_timestamp ?
----------------------------------------------------------------------



----------------------------------------------------------------------
--< tpw
----------------------------------------------------------------------  
drop table if exists orders cascade;
-- orders without a 5xxx or a 6xxx event code
create temp table orders as
select a.*
from jo.vehicle_order_events a
join (
  select order_number
  from jo.vehicle_order_events a
  where not exists (
    select 1
    from jo.vehicle_order_events
    where order_number = a.order_number
    and left(event_code, 1) in ('5','6'))
  group by order_number) b on a.order_number = b.order_number  
order by a.order_number; 
create index on orders(order_number);

select a.*
from jo.vehicle_order_events a
join (
  select order_number, max(effective_date) as effective_date,
    max(event_timestamp) as event_timestamp
  from orders
  group by order_number) b on a.order_number = b.order_number
    and a.effective_date = b.effective_date 
    and a.event_timestamp = b.event_timestamp
order by a.event_code

select a.*, b.tpw, b.order_type, b.model_year, b.alloc_group, b.vin, b.make, b.model_code, c.invoice_date, c.shipped_date, c.exp_in_transit_date
from jo.vehicle_order_events a
join (select distinct order_number from orders) aa on a.order_number = aa.order_number
join jo.vehicle_orders b on a.order_number = b.order_number 
left join nc.vehicle_invoices c on b.vin = c.vin
order by order_number, event_code


-- work this one out 2 invoices
select *
from gmgl.vehicle_invoices
where vin = '1G6DF5RK8L0131556'

----------------------------------------------------------------------
--/> tpw
----------------------------------------------------------------------  

----------------------------------------------------------------------
--< invoice date
----------------------------------------------------------------------  
drop table if exists wtf;
create temp table wtf as
select a.vin, split_part(split_part(a.raw_invoice, E'\n', 8), 'INVOICE', 2)::date as split_date, b.invoice_date
from gmgl.vehicle_invoices a
join nc.vehicle_invoices b on a.vin = b.vin
where a.thru_date > current_Date

select *  -- 0
from wtf
where split_date <> invoice_date


-- ok, the conclusion is except for 12 vehicles they are the same
-- for that 12, event_code is for the original invoice, invoice date is 
-- for subsequent invoice issuance
-- so for the purpose of the vehicle time line, event code is best

select b.order_number, a.*, c.*
from wtf a
join jo.vehicle_orders b on a.vin = b.vin
join jo.vehicle_order_events c on b.order_number = c.order_number
  and c.event_code = '4150'
where a.invoice_date <> c.effective_date

----------------------------------------------------------------------
--/> invoice date
----------------------------------------------------------------------  
----------------------------------------------------------------------
--< luigi processing
/*
requirements:
  nc.vehicle_acquisitions 
  vehicle_orders  (gmgl & gmgl to jo)

in luigi:
    module: order_events (probably move these classes to global connect)
    Orders1
    VehicleOrderEvents

*/
----------------------------------------------------------------------

