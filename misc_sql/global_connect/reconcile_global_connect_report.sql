﻿based on work in postgresq\misc_sql_global_connect_interest_credits_v2.sql
seems to be working ok manually, time to automate it
but, need to figure out the monthly update from the global connect report,
thank this is going to have to be manual, so lets go ahead and do that here

1. save the report from global connect in misc_sql/global_connect/wholesale_floorplan_memo september_2021.txt, october_2021.txt, etc
2. open the report in excel as text 
import wizard:
	start import at row 16 (first vin)
	save date field as text
	no need to adjust columns
spreadsheet:	
	search text file for "fleet", be sure to exclude those vehicles
	delete the superfluous rows
	total and verify the amount column
	verify the rate and paid days for each make
-- "insert into gmgl.wfpm_monthly_report values(202110,'"&"09/21/2021'"&",'"&"10/20/2021','"&SUBSTITUTE(A1," ","")&"','"&C1&"',"&SUBSTITUTE(B1,",","")&","&E1&","&0.0425&","&SUBSTITUTE(D1,",","")&");"
  save the file as .xlsx

  
