﻿09/09/21

querying bars by vin will not necessarily reveal all invoices, but by date might

redo interest credits, current only
the objective is a minimal query to return invoices

select *
from jon.august_21_interest_credits a

09/11/21
confusing myself, mixing refactoring the current code to generate gmgl.vehicle_invoices with what i need to do for interest credits
so, get focus back to interest credits
1. global_connect_invoice_list_test.py
		generates a csv file with all invoices (descriptive data only) for the current interest credit period
		and populates gmgl.wfpm_nightly_invoices_ext

first make sure gmgl.vehicle_invoices exist for each invoice in the period (by invoice number)
---------------------------------------------------------------------------------------------------
--< 09/19/21 how time flies
---------------------------------------------------------------------------------------------------
-- have not kept up with this and now it's the end of the fucking period
1. "missing" 12 invoices
	quoted because i have forgotten where i left off in my understanding of the process
	ok, missing invoice numbers based on this query:
		select a.*, a.vin1||a.vin2
		from gmgl.wfpm_nightly_invoices_ext	a
		where not exists (
			select 1
			from gmgl.vehicle_invoices
			where invoice_number = a.invoice_number);	
  missing based on vin: 
		select a.*
		from gmgl.wfpm_nightly_invoices_ext	a
		where not exists (
			select 1
			from gmgl.vehicle_invoices
			where vin = a.vin1||a.vin2);	

	those missing based on vin are straightforward, so simply run the query in global_connect.vehicleInvoices
	of the remaining 9, which have been paid in a previous month
	only 4, of the invoices currently in gmgl.wfpm_monthly_report
	select * 
	from gmgl.wfpm_nightly_invoices_ext a
	join gmgl.wfpm_monthly_report b on a.vin1||a.vin2 = b.vin
	which implies that for 5 vehicles, there are multiple invoices in the current month

struggling with how to model this goofy adjustment data
the nightly_invoices_ext table has invoice metadata only
i need a table of actual invoice data (relevant to interest credits) to process which invoices are relevant

attributes:
month_paid -- assume based on date? back fill based on published report?
vin
invoice number 
invoice date 
total_invoice_amount
rydell invoice boolean
adjustment boolean
order type
entire invoice

which means a separate scraping and parsing process based on date range

select * from gmgl.wfpm_monthly_report limit 1

but, because i have put this off, i am leaning toward shortcutting to just get the correct numbers for september
1. need to add august to gmgl.wfpm_monthly_report
-- "insert into gmgl.wfpm_monthly_report values(202108,'"&"07/21/2021'"&",'"&"08/20/2021','"&SUBSTITUTE(A1," ","")&"','"&C1&"',"&SUBSTITUTE(B1,",","")&","&E1&","&0.0425&","&SUBSTITUTE(D1,",","")&");"
select year_month, from_date, thru_date, count(*)
from gmgl.wfpm_monthly_report
group by year_month, from_date, thru_date

select * from gmgl.wfpm_monthly_report

2. ran gc_invoices_by_date_range.py which populated gmgl.wfpm_nightly_invoices_ext
select * from gmgl.wfpm_nightly_invoices_ext order by vin2

3. gc_invoices_download.py

download invoices by invoice number
	
09/20 on top of arkona, dip, ukg, i need to be accvurate for the september interest creditts
fucking old script ran again last night sending out the bad email
fuck me
so, today 70 invoices in the date range (8/21 - 9/20)
select invoice_number from gmgl.wfpm_nightly_invoices_ext
but here is the trick, only download new invoice numbers
-- only 7 new invoices
select invoice_number 
from gmgl.wfpm_nightly_invoices_ext a
where not exists (  
  select 1
  from gmgl.wfpm_invoices
  where invoice_number = a.invoice_number);

select * from gmgl.wfpm_nightly_invoices_ext
  
4. gc_invoice_parcing.py  (wrongly named)
all this does is to stick the raw invoice into a table
drop table if exists gmgl.wfpm_ext_raw_invoices cascade;
create table gmgl.wfpm_ext_raw_invoices (
  invoice citext);
comment on table gmgl.wfpm_ext_raw_invoices is 'daily extract of raw invoices for the interest credits' ;

select * from gmgl.wfpm_ext_raw_invoices



5. parse the downloaded invoices
what i have to do here is decide what data gets persisted, be specific, data relevant to interest credits

attributes:
month_paid back fill based on published report? sql
vin py
invoice number py
invoice date sql
total_invoice_amount py
rydell invoice boolean sql
adjustment boolean sql
order type sql
entire invoice


!!! all invoices go into this table, interest credits are figured based on a filtered look at the table


drop table if exists gmgl.wfpm_invoices cascade;
create table gmgl.wfpm_invoices (
  invoice_number citext primary key,
  vin citext not null,
  invoice_date date not null,
  total_invoice_amount numeric not null,
  rydell_invoice boolean not null,
  adjustment_invoice boolean not null,
  order_type citext not null,
  invoice citext);
comment on table gmgl.wfpm_invoices is 'invoice details required for interest credits';
comment on column gmgl.wfpm_invoices.rydell_invoice is 'not sure if this process will ever generate a non rydell invoice, check over time';
create index on gmgl.wfpm_invoices(invoice_number);
create index on gmgl.wfpm_invoices(vin);
create index on gmgl.wfpm_invoices(invoice_date);
create index on gmgl.wfpm_invoices(rydell_invoice);
create index on gmgl.wfpm_invoices(adjustment_invoice);
create index on gmgl.wfpm_invoices(order_type);


insert into gmgl.wfpm_invoices  
select 
  left(trim(split_part(a.invoice, 'INVOICE', 2)), 11) as invoice_number,
  replace(trim(left(split_part(a.invoice, 'VIN', 2), 30)), ' ','') as vin,
	left(trim(split_part(a.invoice, 'INVOICE', 3)), 8)::date as invoice_date,
	(substring(split_part(a.invoice, 'TOTAL         ', 2), 30, 12))::numeric as total_invoice_amount,
	case
		when position('RYDELL' in a.invoice) = 0 and position('CADILLAC OF GRAND FORKS' in a.invoice) = 0 then false
		else true
  end as rydell_invoice,
  case 
		when position('ADJUSTMENT' in a.invoice) <> 0 then true
		else false
  end as adjustment_invoice,
  substring(trim(split_part(a.invoice, 'ORDER NO.', 2)), position('/' in trim(split_part(a.invoice, 'ORDER NO.', 2))) + 1, 3) as order_type,
  a.invoice
from gmgl.wfpm_ext_raw_invoices a;	

6. now that i have the data, which invoices are relevant to interest credits
-- exclude invoices paid in previous period
-- exclude non stock order types
-- only those invoices dated in the current period
-- only the most recent invoice for those with multiple invoice numbers in the period
-- and that takes us down to 54 invoices, from 63

select *
from (
	select a.invoice_number, a.vin, a.invoice_date, a.total_invoice_amount,
		row_number() over (partition by a.vin order by invoice_date desc) as seq
	from gmgl.wfpm_invoices a
	where a.order_type in ('SRE','TRE','TSE')
		and not exists (
			select 1
			from gmgl.wfpm_monthly_report
			where vin = a.vin)
		and a.invoice_date between '08/21/2021' and '09/20/2021') b
where seq = 1

-- generate a query for build data
-- and figure interest credits
-- first put this stuff in a table
drop table if exists the_ic cascade;
create temp table the_ic as
select *
from (
	select a.invoice_number, a.vin, a.invoice_date, a.total_invoice_amount,
		row_number() over (partition by a.vin order by invoice_date desc) as seq
	from gmgl.wfpm_invoices a
	where a.order_type in ('SRE','TRE','TSE')
		and not exists (
			select 1
			from gmgl.wfpm_monthly_report
			where vin = a.vin)
		and a.invoice_date between '08/21/2021' and '09/20/2021') b
where seq = 1;

i need the make, lets try build data
-- get the necessary vins
select vin
from (
	select a.invoice_number, a.vin, a.invoice_date, a.total_invoice_amount,
		row_number() over (partition by a.vin order by invoice_date desc) as seq
	from gmgl.wfpm_invoices a
	where a.order_type in ('SRE','TRE','TSE')
		and not exists (
			select 1
			from gmgl.wfpm_monthly_report
			where vin = a.vin)
		and a.invoice_date between '08/21/2021' and '09/20/2021') b
where seq = 1
  and not exists (
    select 1
    from chr.build_data_describe_vehicle
    where vin = b.vin)

-- make sure build data is intact
delete
-- select *
from chr.build_data_describe_vehicle
where coalesce(style_count, 666) <> 1
  or coalesce(source, 'wtf') <> 'build'

-- generate the interest credits
select -- a.*, b.response->'vinDescription'->>'division', c.rate, c.days,
  coalesce((sum(round(((a.total_invoice_amount * c.rate)/360) * c.days, 2))::integer)::text, '0') || ' / ' || coalesce(count(*)::text, '0') 
from the_ic a
left join chr.build_data_describe_vehicle b on a.vin = b.vin
left join nc.gm_interest_credit_parameters c on (b.response->'vinDescription'->>'division')::citext = c.make
  and thru_date > current_date -- make this better, using period dates

select * from nc.gm_interest_credit_parameters


select * from gmgl.wfpm_invoices where invoice_date between '08/21/2021' and '09/20/2021'


select  a.*, b.response->'vinDescription'->>'division', c.rate, c.days,
  round(((a.total_invoice_amount * c.rate)/360) * c.days, 2)
from the_ic a
left join chr.build_data_describe_vehicle b on a.vin = b.vin
left join nc.gm_interest_credit_parameters c on (b.response->'vinDescription'->>'division')::citext = c.make
  and thru_date > current_date -- make this better, using period dates






-- what is wrong with the current process
select *
from (
	select *
	from (
		select a.invoice_number, a.vin, a.invoice_date, a.total_invoice_amount,
			row_number() over (partition by a.vin order by invoice_date desc) as seq
		from gmgl.wfpm_invoices a
		where a.order_type in ('SRE','TRE','TSE')
			and not exists (
				select 1
				from gmgl.wfpm_monthly_report
				where vin = a.vin)
			and a.invoice_date between '08/21/2021' and '09/20/2021') b
	where seq = 1) aa
full outer join ( -- compare to current query
	select a.vin, a.invoice_number
	from gmgl.vehicle_invoices a
	join jo.vehicle_orders b on a.vin = b.vin
	join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
		and c.vehicle_order_type not like 'Fleet%'  
	join nc.gm_interest_credit_parameters d on upper(b.make) = upper(d.make)
		and '09/20/2021' between d.from_date and d.thru_date
	where a.thru_date > current_date
		and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between '08/21/2021' and '09/20/2021'
		and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
	and ((
		position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
		or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0))) bb on aa.vin = bb.vin and aa.invoice_number = bb.invoice_number
order by aa.vin, bb.vin

select * from gmgl.vehicle_invoices where vin in ('1GNSKTKL5MR474773','1GTU9FED8MZ426056','KL79MRSLXNB050386','1GYKPDRS9NZ116973')

!!!!!!!! at least some of the problem is jo.vehicle_orders !!!!!!!!!!!!!!
select * from gmgl.vehicle_orders where vin = '1G1YB2D41M5124964'
-- fucking orders unavailable
select * from jo.vehicle_orders_unavailable where order_number = 'ZBFWTB'

select a.*
from  missing_orders a
join gmgl.vehicle_orders b on a.vin = b.vin
join jo.vehicle_orders_unavailable c on b.order_number = c.order_number


select a.vin, a.invoice_number
from gmgl.vehicle_invoices a
join jo.vehicle_orders b on a.vin = b.vin
-- join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
-- 	and c.vehicle_order_type not like 'Fleet%'  
join nc.gm_interest_credit_parameters d on upper(b.make) = upper(d.make)
	and '09/20/2021' between d.from_date and d.thru_date
where a.thru_date > current_date
	and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between '08/21/2021' and '09/20/2021'
	and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
and ((
	position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
	or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0)) 
and a.vin in ('1G1YB2D41M5124964','KL79MRSL6NB049588','KL79MRSLXNB050386','1GYKPDRS9NZ116973')




fuck it, disable production of interest credits,
do an email in the morning based on the new work
['Subject'] = 'Floorplan Interest Credits'
recipients = ['jandrews@cartiva.com', 'gsorum@cartiva.com', 'bcahalan@rydellcars.com', 'abruggeman@cartiva.com',
							'jschmiess@rydellcars.com', 'tmonson@rydellcars.com', 'myem@rydellcars.com',
							'nshirek@rydellcars.com']
-- -- TSE order type
-- we have 4 order type TSE: Show/Special Events [Stock]
-- select * from gmgl.vehicle_order_types order by vehicle_order_type_key
-- need to see if these have been paid in the past
-- 
-- select a.order_number, a.order_type, a.vin, left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date as invoice_date
-- from gmgl.vehicle_orders a
-- left join gmgl.vehicle_invoices b on a.vin = b.vin
-- where a.order_type = 'TSE'
-- 
-- pulled the report for sep 2019, 1 of the 3 are on it, so, that is inconclusive
-- 
-- 1GCPYCEF3KZ303326
-- 3GCUYGED2KG170129
-- 1GC1KTEYXKF246973
-- 
-- all four from sep 2021 are in dealertrack inventory, so, include them
-- ZMTHHP;TSE;3GCUYGED2MG320646;2021-09-15
-- ZMTHKG;TSE;KL79MSSL4NB025572;2021-09-15
-- ZMTHGV;TSE;1GC4YREY2MF264644;2021-09-15
-- ZPMJKS;TSE;1GCGTEEN4M1286409;2021-09-15















---------------------------------------------------------------------------------------------------
--/> 09/19/21 how time flies
---------------------------------------------------------------------------------------------------

today i, i now have 9 invoice numbers missing, but no vins missing, which looks like
of those that are missing from the current period by invoice
what are the different situations
	1. paid in previous period
	2. paid in current period
KL79MRSLXNB046595: 2 invoices, 8/23 & 9/16

going to start out with a different table
in all invoices will be stored
add date range to monthly_report table

-- 5 missing invoice numbers (1 of which is missing vin as well)
select a.*
from gmgl.wfpm_nightly_invoices_ext	a
where not exists (
  select 1
  from gmgl.vehicle_invoices
  where invoice_number = a.invoice_number)

select aa.*, bb.*
from (
	select a.*
	from gmgl.wfpm_nightly_invoices_ext	a
	where not exists (
		select 1
		from gmgl.vehicle_invoices
		where invoice_number = a.invoice_number)) aa
left join gmgl.vehicle_invoices bb on aa.vin1||aa.vin2 = bb.vin

1GC5YME79NF112103: 2 invoices one dated 08/03/2021 on dated 08/26/2021 invoice adj from 45111.58 to 43711.58 (-1400)
wonder what if any affect this will have on floor plan, guessing none
-- it was paid in august based on invoice of 45111.58
select * from gmgl.wfpm_monthly_report where vin = '1GC5YME79NF112103'
so, thinking the interest credits will be from the barrs date range, minus the adjusted invoices that were initially
issued in a previous period

1GTP9EEL9MZ395883 paid in august
invoiced 07/28/21, 5AD42485982,  for 58448.11  
ADJUSTMENT INVOICE 08/26/21, 5XD47099267, for 49416.21, adjusted down by 9031.90
select * from chr.build_data_describe_vehicle where vin = '1GTP9EEL9MZ395883'

3GTU9DED5MG260454 paid in august
invoiced 08/10/21, 5AD42540232, for 56680.41
ADJUSTMENT INVOICE 08/26/212,  5XD47099747 for 48495.11 adjusted down by 8185.30

1GCPYFED7MZ298585 will this adujstment be paid in september?
invoiced 06/14/21, 1AD67743525 for54101.75 
ADJUSTMENT INVOICE 07/02/21, 1XD42045418 for 45341.35 adjusted down by 8760.40
ADJUSTMENT INVOICE 08/27/21, 1XD42256429 for 54101.75 adusted up by 8760.40











-- 1 missing vin
-- manually ran global_connect.vehicleInvoices for this vin
select a.*
from gmgl.wfpm_nightly_invoices_ext	a
where not exists (
  select 1
  from gmgl.vehicle_invoices
  where vin = a.vin1||a.vin2)

select * from chr.describe_vehicle where vin = 'LRBFZSR44ND015519'

-0-13-21
-- exclude invoices already paid
-- starting with 35 invoices
select a.*
from gmgl.wfpm_nightly_invoices_ext	a
-- only one invoice per vin
select vin2
from gmgl.wfpm_nightly_invoices_ext	a
group by vin2
having count(*) > 1

-- 3 have already been paid
select a.*
from gmgl.wfpm_nightly_invoices_ext	a
where exists (
  select 1
  from gmgl.wfpm_monthly_report
  where vin = a.vin1||a.vin2)

















09/10
so, having figured out how to get daily invoice counts from global
the processing can be done iteratively, each day, compare what i generate with global
so when there is a mismatch, , figure out why

-- would like to understand the difference in the barrs page betweeen vehicle invoice and stock vehicle invoice
-- -- the diff is not based on order type
-- select min(vin), max(vin), right(left(split_part(raw_invoice, 'ORDER NO.', 2), 11), 3) as order_type, min(from_date), max(from_date)
-- from gmgl.vehicle_invoices
-- where left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date > '01/31/2021'
-- group by right(left(split_part(raw_invoice, 'ORDER NO.', 2), 11), 3)
-- -- may be based on dealership ?
-- select vin, from_Date
-- from gmgl.vehicle_invoices
-- where position('RYDELL' in raw_invoice) = 0
--   and position('CADILLAC OF GRAND FORKS' in raw_invoice) = 0
-- order by from_Date desc  
-- -- yep, that appears to be it
-- select * from gmgl.vehicle_invoices where vin = 'KL4MMCSL1MB116049'
-- -- so, we should be good on interest credit verification based solely on barrs document type Vehicle Invoice

do a test on august
tables needed
	wholesale floorplan memo
	daily scrape of vehicle invoices: just vin, date, inv #, ...
	verify daily scrape vs gmgl.vehicle_invoices

select * from  nc.gm_interest_credit_parameters

	
select *, round((invoice_amount * .0425/360) * days, 2)
-- select * 
from jon.august_21_interest_credits a
where amount <> round((invoice_amount * .0425/360) * days, 2)

drop table if exists gmgl.wfpm_monthly_report cascade;
create table gmgl.wfpm_monthly_report (
  year_month integer not null,
  vin citext not null,
  invoice_date date not null,
  invoice_amount numeric(8,2) not null,
  paid_days integer not null,
  rate numeric(4,4) not null,
  amount numeric(6,2) not null,
  primary key(vin));
comment on table gmgl.wfpm_monthly_report is 'from global connect bars main page, the monthly wholesale floor plan memo report, 
	populated manually; wfpm stands for wholesale floor plan memo';

-- 09/19/21 modify structure to include the date range
create temp table wtf as select * from gmgl.wfpm_monthly_report

drop table if exists gmgl.wfpm_monthly_report cascade;
create table gmgl.wfpm_monthly_report (
  year_month integer not null,
  from_date date not null,
  thru_date date not null,
  vin citext not null,
  invoice_date date not null,
  invoice_amount numeric(8,2) not null,
  paid_days integer not null,
  rate numeric(4,4) not null,
  amount numeric(6,2) not null,
  primary key(vin));
comment on table gmgl.wfpm_monthly_report is 'from global connect bars main page, the monthly wholesale floor plan memo report, 
	populated manually; wfpm stands for wholesale floor plan memo';

insert into gmgl.wfpm_monthly_report
select year_month, 
  case 
		when year_month = 202106 then '05/21/2021'::date
		when year_month = 202107 then '06/21/2021'::date
  end,
  case 
		when year_month = 202106 then '06/20/2021'::date
    when year_month = 202107 then '07/20/2021'::date 
  end,
	vin, invoice_date, invoice_amount, paid_days, rate, amount
from wtf;	
	
  
insert into gmgl.wfpm_monthly_report
select 202108, vin, invoice_date, invoice_amount, days, 0.0425, amount
from jon.august_21_interest_credits;

select * from gmgl.wfpm_monthly_report

-- "insert into gmgl.wfpm_monthly_report values(202108,'"&SUBSTITUTE(A1," ","")&"','"&C1&"',"&SUBSTITUTE(B1,",","")&","&E1&","&0.0425&","&SUBSTITUTE(D1,",","")&");"
-- 09/13 loaded the june & july report
select *, sum(amount) over (partition by make), sum(amount) over ()
from (
select a.*, round((invoice_amount * .0425/360) * paid_days, 2), b.response->'vinDescription'->'attributes'->>'division' as make
-- select sum(round((invoice_amount * .0425/360) * paid_days, 2)) 
from gmgl.wfpm_monthly_report a
left join chr.describe_vehicle b on a.vin = b.vin
where year_month = 202106) c
order by make, right(vin,8)


drop table if exists gmgl.wfpm_nightly_invoices_ext cascade;
create unlogged table gmgl.wfpm_nightly_invoices_ext (
  vin2 citext not null,
  vin1 citext not null,
  invoice_number citext not null,
  division citext,
  dealer_code citext,
  invoice_date date not null,
  primary key(invoice_number));
comment on table gmgl.wfpm_nightly_invoices_ext is 'nightly scrape of the global connect bars list of vehicles invoiced for the
	current interest credit period, to be used to verify vehicle invoices for the period, invoice metadata; wfpm stands for wholesale floor plan memo';  

select * from gmgl.wfpm_nightly_invoices_ext


-- here's my august test
-- what i need to do here is match invoice numbers
select * 
from (
	select a.*, b.invoice_number
	from gmgl.wfpm_monthly_report a
	left join gmgl.vehicle_invoices b on a.vin = b.vin
	  and b.thru_date > current_Date
	where a.year_month = 202108) a
full outer join gmgl.wfpm_nightly_invoices_ext b on a.vin = b.vin1||b.vin2
order by a.vin

-- join on invoice number
select * 
from (
	select a.*, b.invoice_number
	from gmgl.wfpm_monthly_report a
	left join gmgl.vehicle_invoices b on a.vin = b.vin
	  and b.thru_date > current_Date
	where a.year_month = 202108) a
full outer join gmgl.wfpm_nightly_invoices_ext b on a.invoice_number = b.invoice_number
-- where right(vin2, 3) = '769'
order by a.vin

-- this gets to confusing results, i think it is because the use of invoice number from gmgl.vehicle_invoices
for the monthly report is not a good idea
the invoice numbers need to be matched on a daily basis
so the daily test, in part consists of comparing invoice number from gmgl.vehicle_invoices to
invoice number from nightly ext
nightly ext currently consists of the date range 07/21 - 08/20

select

select *, row_number() over (partition by vin2 order by invoice_date desc)
from gmgl.wfpm_nightly_invoices_ext
where right(vin2, 3) = '769'
order by vin2

select * from gmgl.vehicle_invoices where right(vin,8) = 'MJ240769'


09/11/21 parse from file
select * from gmgl.vehicle_invoices where invoice_number in ('4OD28665616','1AD68554905','1XD42203686')
1XD42203686 not in table yet, so, this becomes the test subject

drop table if exists jon.vehicle_invoices_tmp cascade;
create table jon.vehicle_invoices_tmp (
	vin citext, 
	invoice_data jsonb,
	invoice citext);


select * from jon.vehicle_invoices_tmp


select vin, invoice_data->>'vin',invoice,invoice_data->>'vehicle_invoice',cast(invoice_data->>'employ' as numeric),
cast(invoice_data->>'supplr' as numeric),
cast(invoice_data->>'empinc' as numeric),cast(invoice_data->>'supinc' as numeric),cast(invoice_data->>'act' as numeric),
cast(invoice_data->>'hb' as numeric),cast(invoice_data->>'adv' as numeric),cast(invoice_data->>'exp' as numeric),
cast(invoice_data->>'total_model_msrp' as numeric),
cast(invoice_data->>'total_model_invoice' as numeric),cast(invoice_data->>'total_msrp' as numeric),
cast(invoice_data->>'total_invoice' as numeric),current_date, '12/31/9999'
from jon.vehicle_invoices_tmp


















-- nightly has 2 invoices for KL79MRSL1NB036229
on the vin line it as ADJUSTMENT, 09/11, turns out adjustment invoices do not match the normal parsing patter,
	will require individual attention
includees RIA dealer installed floor liners for $210    
select * from gmgl.vehicle_invoices where vin = 'KL79MRSL1NB036229'
gmgl.vehicle_invoices has just the original, invoice #1AD68554905
both invoices come up when i interogate bars Vehicle Invoice by vin
so, apparently, the vehicle invoice scraper does not handle the situation where multiple invoices exist
i went through the from & thru dates, nothing recent, that is not doing it
-- FROm all_nc_inventory_nightly.sql
-- -- insert into nc.vehicle_invoices
-- select vin,
--   trim((left(split_part(raw_invoice, E'\n', 1), position('GENERAL' in raw_invoice) -1))::citext) as description,
--   case 
--     when position('RYDELL' in raw_invoice) = 0 and position('CADILLAC OF GRAND FORKS' in raw_invoice) = 0 then 'dealer trade'
--     else 'factory'
--   end as source,  
--   trim((left(split_part(raw_invoice, E'\n', 8), 7))::citext) as model_code,
--   trim((left(split_part(raw_invoice, E'\n', 2), 3))::citext) as color_code,
--   trim((trim(substring(split_part(raw_invoice, E'\n', 2), 6, 31)))::citext) as color,
--   left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date as invoice_date,
--   left(trim(split_part(raw_invoice, 'SHIPPED', 2)), 8)::date as shipped_date,
--   left(trim(split_part(raw_invoice, 'EXP I/T', 2)), 8)::date as exp_in_transit_date
-- from (
--   select vin, replace(raw_invoice, '&amp;', '&') as raw_invoice
--   from gmgl.vehicle_invoices
--   where thru_date > current_date
--     and position('CREDIT FOR INVOICE' in raw_invoice) = 0) a

-- 17
select 2 as seq, 'Actual: $' || coalesce((sum(round(((a.total_invoice * d.rate)/360) * d.days, 2))::integer)::text, '0') || ' / ' || coalesce(count(*)::text, '0')
-- select a.vin
from gmgl.vehicle_invoices a
join jo.vehicle_orders b on a.vin = b.vin
join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
  and c.vehicle_order_type not like 'Fleet%'  
join nc.gm_interest_credit_parameters d on upper(b.make) = upper(d.make)
  and '09/20/2021' between d.from_date and d.thru_date
where a.thru_date > current_date
  and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between '08/21/2021' and  '09/20/2021'
  and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
and ((
  position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
  or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0))

-- vs
-- 25
select a.vin, a.total_invoice,
  position('RYDELL CHEVROLET' in a.raw_invoice),
  position('CADILLAC OF GRAND FORKS' in a.raw_invoice),
  position('CREDIT FOR INVOICE' in a.raw_invoice),
  left(split_part(raw_invoice, 'ORDER NO.', 2), 11) -- order# & order type
from gmgl.vehicle_invoices a
where left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between '08/21/2021' and  '09/20/2021'  
  and right(left(split_part(raw_invoice, 'ORDER NO.', 2), 11), 3) <> 'TRE'

-- vs
-- 24
select a.vin,
  position('RYDELL CHEVROLET' in a.raw_invoice),
  position('CADILLAC OF GRAND FORKS' in a.raw_invoice),
  position('CREDIT FOR INVOICE' in a.raw_invoice)
from gmgl.vehicle_invoices a
where left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between '08/21/2021' and  '09/20/2021'  
  and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
and ((
  position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
  or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0))

select * from jo.vehicle_order_types order by vehicle_order_type_key

select * from nc.vehicle_invoices limit 10

-- all the order types
select *
from (
	select right(left(split_part(raw_invoice, 'ORDER NO.', 2), 11), 3) as order_type, min(left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date) as min_date, 
		max(left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date) as max_date, count(*)
	from gmgl.vehicle_invoices
	group by right(left(split_part(raw_invoice, 'ORDER NO.', 2), 11), 3)) a
left join jo.vehicle_order_types b on a.order_type = b.vehicle_order_type_key




-- 17 vs 25
select * 
from ( -- 17
	select a.vin, right(left(split_part(raw_invoice, 'ORDER NO.', 2), 11), 3) as order_type
	from gmgl.vehicle_invoices a
	join jo.vehicle_orders b on a.vin = b.vin
	join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
		and c.vehicle_order_type not like 'Fleet%'  
	join nc.gm_interest_credit_parameters d on upper(b.make) = upper(d.make)
		and '09/20/2021' between d.from_date and d.thru_date
	where a.thru_date > current_date
		and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between '08/21/2021' and  '09/20/2021'
		and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
	and ((
		position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
		or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0))) aa
full outer join ( -- 25
	select a.vin, a.total_invoice,
		position('RYDELL' in a.raw_invoice),
		position('CADILLAC OF GRAND FORKS' in a.raw_invoice),
		position('CREDIT FOR INVOICE' in a.raw_invoice),
		right(left(split_part(raw_invoice, 'ORDER NO.', 2), 11), 3) as order_type
	from gmgl.vehicle_invoices a
	where left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between '08/21/2021' and  '09/20/2021'
	  and right(left(split_part(raw_invoice, 'ORDER NO.', 2), 11), 3) in ('TRE','SRE')) bb on aa.vin = bb.vin 		

-- the "last" line in invoice to indicate dealer, but i know it is not always the last_line
select TRIM(RIGHT(TRIM(replace(RAW_INVOICE, E'\n', '')), 25)) from gmgl.vehicle_invoices where vin = '1GT49WEY6MF308849'

select vin, TRIM(RIGHT(TRIM(replace(RAW_INVOICE, E'\n', '')), 50)) from gmgl.vehicle_invoices

-- returns BUICK
select trim(replace(RAW_INVOICE, E'\n', '')) from gmgl.vehicle_invoices where vin = '3GNAXUEV5LS514815'

select trim(regexp_replace(raw_invoice, E'\n', '', 'g')) from gmgl.vehicle_invoices where vin = '3GNAXUEV5LS514815'



//*[@id="search-result"]/section[2]/div/table/tbody
//*[@id="search-result"]/section[2]/div/table/tbody/tr[2]
//*[@id="search-result"]/section[2]/div/table/tbody/tr[11]
//*[@id="search-result"]/section[2]/div/table/tbody/tr[1]/td[1]
//*[@id="search-result"]/section[2]/div/table/tbody/tr[1]/td[2]
//*[@id="search-result"]/section[2]/div/table/tbody/tr[1]/td[3]
//*[@id="search-result"]/section[2]/div/table/tbody/tr[1]/td[3]
<td _ngcontent-bfe-c4="" class="text-center">MR468056</td>
-------------------------------------------------------------------------------------------------
--< august 2021 global show 74 invoices
-------------------------------------------------------------------------------------------------
-- this query shows that global counts order type SRE (sold retail order)
select a.vin, a.total_invoice,
  position('RYDELL CHEVROLET' in a.raw_invoice),
  position('CADILLAC OF GRAND FORKS' in a.raw_invoice),
  position('CREDIT FOR INVOICE' in a.raw_invoice),
  left(split_part(raw_invoice, 'ORDER NO.', 2), 11) -- order# & order type
from gmgl.vehicle_invoices a
-- where left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between '08/21/2021' and  '09/20/2021'  
where left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between '07/21/2021' and  '08/20/2021' 
  and right(left(split_part(raw_invoice, 'ORDER NO.', 2), 11), 3) <> 'TRE'  

select *
from jon.august_21_interest_credits  a
full outer join (
select a.vin, a.total_invoice,
  position('RYDELL CHEVROLET' in a.raw_invoice),
  position('CADILLAC OF GRAND FORKS' in a.raw_invoice),
  position('CREDIT FOR INVOICE' in a.raw_invoice),
  left(split_part(raw_invoice, 'ORDER NO.', 2), 11) -- order# & order type
from gmgl.vehicle_invoices a
-- where left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between '08/21/2021' and  '09/20/2021'  
where left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between '07/21/2021' and  '08/20/2021') b on a.vin = b.vin

-- august discrepancies
-- vehicles over 100k missing total invoice
v in global not in invoices
this was the goofy one, orig invoiced to luther (which is what shows in gmgl.vehicle_invoices, which is why
interest credit query did not include it), stolen off transport,
luther didnt want it, we took, it has been re-invoiced to us, but gmgl.vehicle_invoices didnt pick up the new invoice
select * from gmgl.vehicle_invoices where vin = '5GAEVBKW4MJ240769'

3GTU9DED2MG417700 included in gmgl.vehicle_invoices but not global
select * from gmgl.vehicle_invoices where vin = '3GTU9DED2MG417700'
invoiced to smith motors, therefor not included in interest credit query