﻿based on work in postgresq\misc_sql_global_connect_interest_credits_v2.sql
seems to be working ok manually, time to automate it

1. need to scrape global connect 
	A	invoice metadata: interest_credits.py -> InvoiceMetadata
				truncates and populates gmgl.wfpm_nightly_invoices_ext with data from the relevant date range(s)

	  select * from gmgl.wfpm_nightly_invoices_ext
	  
	B  invoices: interest_credits.py -> DownloadInvoices
				based on this query
					select invoice_number 
					from gmgl.wfpm_nightly_invoices_ext a
					where not exists (  
						select 1
						from gmgl.wfpm_invoices
						where invoice_number = a.invoice_number);	
		truncates and populates gmgl.wfpm_ext_raw_invoices
		select * from gmgl.wfpm_ext_raw_invoices


select * from sls.months where year_month = 202111

select * from dds.dim_date where the_date = current_date

select extract(day from current_date - 1)

select extract(month from current_date - 1)

select extract(month from (select first_of_last_month from dds.dim_Date where the_date = current_date - 1))

select extract(month from (select last_of_month + 1 from dds.dim_date where the_date = current_date - 1)) 

select 
	((select extract(month from (select first_of_last_month from dds.dim_date where the_date = current_date - 1))) || '/' || 21 || '/' ||
	(select extract(year from (select last_of_month + 1 from dds.dim_date where the_date = current_date - 1))))::date

select 
	((select extract(month from  current_date - 1)) || '/' || 20 || '/' ||
	(select extract(year from current_date - 1)))::date

select 
	
		
do $$
	declare
		_the_date DATE := current_date - 1;
		_this_month integer := (select extract(month from _the_date));
		_prev_month integer := (select extract(month from (select first_of_last_month from dds.dim_date where the_date _the_date)));
		_next_month integer := (select extract(month from (select last_of_month + 1 from dds.dim_date where the_date = _the_date)));		
		_current_month_from_date date := (
			select 
				((select extract(month from (select first_of_last_month from dds.dim_date where the_date = _the_date))) || '/' || 21 || '/' ||
				(select extract(year from (select last_of_month + 1 from dds.dim_date where the_date = _the_date))))::date);
		_current_month_thru_date date := (				
			select 
				((select extract(month from _the_date)) || '/' || 20 || '/' ||
				(select extract(year from _the_date)))::date);	
		_next_month_from_date date := _current_month_thru_date -1;
		_next_month_thru_date date := 