﻿-- new learning
-- 	09/28/21
--  when there are adjustment invoices, interest credits are paid based on the original only
-- 	TSE order type not included in Wholesale Floor Plan Memo
-- 	If there are multiple invoices in the period, the memo uses the original invoice
--  order type TCS is not a one off anomaly and should be processed as a normal invoice
--  build data not always available on the invoice immediately

------------------------------------------------------------------------
--< 03/21/22 build the functions to automate this
------------------------------------------------------------------------

select * from gmgl.wfpm_monthly_report order by from_date
select * from nc.gm_interest_credit_parameters
select * from gmgl.wfpm_invoices where invoice_date > '03/15/2022'
select * from gmgl.wfpm_nightly_invoices_ext

after scraping and downloading the invoices, need to take data from gmgl.wfpm_ext_raw_invoices and update gmgl.wfpm_invoices

select count(*) from gmgl.wfpm_invoices  -- 829/932

create or replace function gmgl.wfpm_update_invoices ()
returns void as
$BODY$
/*

*/
insert into gmgl.wfpm_invoices  
select 
  left(trim(split_part(a.invoice, 'INVOICE', 2)), 11) as invoice_number,
  replace(trim(left(split_part(a.invoice, 'VIN', 2), 30)), ' ','') as vin,
	left(trim(split_part(a.invoice, 'INVOICE', 3)), 8)::date as invoice_date,
	replace((substring(split_part(a.invoice, 'TOTAL         ', 2), 30, 12)), '-', '')::numeric as total_invoice_amount,
	case
		when position('RYDELL' in a.invoice) = 0 and position('CADILLAC OF GRAND FORKS' in a.invoice) = 0 then false
		else true
  end as rydell_invoice,
  case 
		when position('ADJUSTMENT' in a.invoice) <> 0 then true
		else false
  end as adjustment_invoice,
  substring(trim(split_part(a.invoice, 'ORDER NO.', 2)), position('/' in trim(split_part(a.invoice, 'ORDER NO.', 2))) + 1, 3) as order_type,
  a.invoice, b.make
from gmgl.wfpm_ext_raw_invoices a
left join gmgl.wfpm_makes_models b on -- trim(split_part(a.invoice, ' ', 3)) = b.model;	
  case
    when trim(split_part(a.invoice, ' ', 2)) in ('GMC', 'CHEVROLET') then trim(split_part(a.invoice, ' ', 3))
    else trim(split_part(a.invoice, ' ', 2))
  end = b.model
where position('CREDIT FOR INVOICE' in a.invoice) = 0; -- reversing invoices  ;
$BODY$
language sql;

select * from gmgl.vehicle_orders where order_number = 'ZXFTGG'

-- generate current month text for email
create or replace function gmgl.wfpm_interest_credit_email(
		in _current_month citext,
		in _current_from_date date,
		in _current_thru_date date,
		in _next_month citext,
		in _next_from_date date,
		IN _next_thru_date date)
  returns table(the_data text) as
$BODY$
/*
	select gmgl.wfpm_interest_credit_email('March 2022','02/21/2022','03/20/2022','April 2022','03/21/2022','04/20/2022')
*/
declare
  the_day integer := 21; --(select extract(day from current_date)::integer);
begin  
  if the_day < 21 then
		select distinct _current_month || ': ' || (count(aa.invoice_number) over ())::text || ' vehicles for a total of $' || (sum(round(((aa.total_invoice_amount * c.rate)/360) * c.days, 2)) over ())::text as the_date
		from (
			select *
			from (
				select a.invoice_number, a.vin, a.invoice_date, a.total_invoice_amount, a.make, 
					row_number() over (partition by a.vin order by invoice_date asc) as seq -- first invoice in the period
				from gmgl.wfpm_invoices a
				where a.order_type in ('SRE','TRE','TCS')
					and not exists ( -- exclude vins that have already been paid
						select 1
						from gmgl.wfpm_monthly_report
						where vin = a.vin)
		-- 				  and year_month < 202112) -- add this line if i forget to check against monthly report before inserting into gmgl.wfp_invoices
					and a.invoice_date between _current_from_date and _current_thru_date) b
			where seq = 1) aa
		left join nc.gm_interest_credit_parameters c on aa.make = c.make
			and thru_date > current_date;
	elseif the_day > 20 then 
		select distinct _current_month || ': ' || (count(aa.invoice_number) over ())::text || ' vehicles for a total of $' || (sum(round(((aa.total_invoice_amount * c.rate)/360) * c.days, 2)) over ())::text as the_date
		from (
			select *
			from (
				select a.invoice_number, a.vin, a.invoice_date, a.total_invoice_amount, a.make, 
					row_number() over (partition by a.vin order by invoice_date asc) as seq -- first invoice in the period
				from gmgl.wfpm_invoices a
				where a.order_type in ('SRE','TRE','TCS')
					and not exists ( -- exclude vins that have already been paid
						select 1
						from gmgl.wfpm_monthly_report
						where vin = a.vin)
		-- 				  and year_month < 202112) -- add this line if i forget to check against monthly report before inserting into gmgl.wfp_invoices
					and a.invoice_date between _current_from_date and _current_thru_date) b
			where seq = 1) aa
		left join nc.gm_interest_credit_parameters c on aa.make = c.make
			and thru_date > current_date
		union
		select distinct _next_month || ': ' || (count(aa.invoice_number) over ())::text || ' vehicles for a total of $' || (sum(round(((aa.total_invoice_amount * c.rate)/360) * c.days, 2)) over ())::text
		from (
			select *
			from (
				select a.invoice_number, a.vin, a.invoice_date, a.total_invoice_amount, a.make, 
					row_number() over (partition by a.vin order by invoice_date asc) as seq -- first invoice in the period
				from gmgl.wfpm_invoices a
				where a.order_type in ('SRE','TRE','TCS')
					and not exists ( -- exclude vins that have already been paid
						select 1
						from gmgl.wfpm_monthly_report
						where vin = a.vin)
		-- 				  and year_month < 202112) -- add this line if i forget to check against monthly report before inserting into gmgl.wfp_invoices
					and a.invoice_date between _next_from_date and _next_thru_date) b
			where seq = 1) aa
		left join nc.gm_interest_credit_parameters c on aa.make = c.make
			and thru_date > current_date;
  end if;				
end
$BODY$
language plpgsql;  

------------------------------------------------------------------------
--/> 03/21/22 build the functions to automate this
------------------------------------------------------------------------

branch from misc_sql\global_connect\refactor_interest_credits_again.sql

basing the interest credit report on downloading invoices from bars based on the date range
rather than relying on inventory or gmgl.vehicle_invoices or any jo. tables

seem to have the basics, now i need to automate so that
on sep 22 i both update and display sep invoices (08/21 -> 09/20)
as well as oct invoices (09/21 -> 10/20)

when the current date is between 1 and 20 of current month
	only process and display invoices for the current month: prev_month/21 -> cur_month/20
when the current date is between 21 and eom
	process and display invoices for the prev month: prev_month/21 -> cur_month/20
	and 
	process and display invoices for the next month: cur_month/21 -> next_month/20

current_month: month of todays calendar date
prev & next month based on current_month

current_report_period depends on the date
	if the date is <= current_month/20
		cur_report_period = prev_month/21 -> cur_month/20
	if the date is > current_month/20
		cur_report_period = cur_month/21 -> next_month/20
		prev_report_period = prev_month/21 -> cur_month/20
		
I. Floorplan Credit Memo Report
import into excel parse starting line 16, make data type for the date to be text
REMEMBER to leave out the fleets (at the bottom of the file) before sorting
sort spreadsheet by column A to easily get rid of the junk

this will probably always be a manual job		
change the fucking month
-- "insert into gmgl.wfpm_monthly_report values(202203,'"&"02/21/2022'"&",'"&"03/20/2022','"&SUBSTITUTE(A1," ","")&"','"&C1&"',"&SUBSTITUTE(B1,",","")&","&E1&","&0.0425&","&SUBSTITUTE(D1,",","")&");"

select year_month, from_date, thru_date, count(*), sum(amount)
from gmgl.wfpm_monthly_report
group by year_month, from_date, thru_date
order by year_month

select * from gmgl.wfpm_monthly_report
delete from gmgl.wfpm_monthly_report where year_month = 202109

March 2022: i was i invoice over, invoice 5XD47223420

-- Daily processing
II.gc_invoices_by_date_range.py
		download invoice metadata from bars based on date range, that page is limited to range <= 31 days
		so, when the date > 20th, this will have to be 2 steps, eg 
			09/22/2021 need the range 8/21 -> 9/20 to make sure to pick up and late arrivals for the september report
											and range 9/21 -> 10/20 for the oct report
			this will need to be done until the sep report is published, safe to assume thru 09/30

  gc_invoices_by_date_range.py downloads invoice metadata for the relevant time ranges
  and populates gmgl.wfpm_nightly_invoices_ext

  select * from gmgl.wfpm_nightly_invoices_ext

III. gc_invoices_download.py 
	download the actual invoice by invoice number as determined by the query:
			select invoice_number 
			from gmgl.wfpm_nightly_invoices_ext a
			where not exists (  
				select 1
				from gmgl.wfpm_invoices
				where invoice_number = a.invoice_number); 
	and save them in gmgl.wfpm_ext_raw_invoices


IV. decided to do all needed parsing in sql, populating gmgl.wfpm_invoices

instead of relying on build data on the invoice date, build and maintain a gmgl.wfpm_makes_models table
  and get the make based on the model
alter table gmgl.wfpm_invoices
add column make citext;
alter table gmgl.wfpm_invoices
alter column make set not null;





-- 10/27/21
-- missing make in gmgl.wfpm_makes_models
-- unlike everything else, 1st line: 2022 gmc sierra 1500 limited pro
-- eg, includes the make
-- 10/29 same thing on Chev Spark, plus, spark not in gmgl.wfpm_makes_models
-- insert into gmgl.wfpm_makes_models values('chevrolet','SPARK');
-- 01/07/22  invoice # 1AC48146046 is a credit for in invoice 1AD70075493, exclude in where clause
insert into gmgl.wfpm_invoices  
select 
  left(trim(split_part(a.invoice, 'INVOICE', 2)), 11) as invoice_number,
  replace(trim(left(split_part(a.invoice, 'VIN', 2), 30)), ' ','') as vin,
	left(trim(split_part(a.invoice, 'INVOICE', 3)), 8)::date as invoice_date,
	replace((substring(split_part(a.invoice, 'TOTAL         ', 2), 30, 12)), '-', '')::numeric as total_invoice_amount,
	case
		when position('RYDELL' in a.invoice) = 0 and position('CADILLAC OF GRAND FORKS' in a.invoice) = 0 then false
		else true
  end as rydell_invoice,
  case 
		when position('ADJUSTMENT' in a.invoice) <> 0 then true
		else false
  end as adjustment_invoice,
  substring(trim(split_part(a.invoice, 'ORDER NO.', 2)), position('/' in trim(split_part(a.invoice, 'ORDER NO.', 2))) + 1, 3) as order_type,
  a.invoice, b.make
from gmgl.wfpm_ext_raw_invoices a
left join gmgl.wfpm_makes_models b on -- trim(split_part(a.invoice, ' ', 3)) = b.model;	
  case
    when trim(split_part(a.invoice, ' ', 2)) in ('GMC', 'CHEVROLET') then trim(split_part(a.invoice, ' ', 3))
    else trim(split_part(a.invoice, ' ', 2))
  end = b.model
where position('CREDIT FOR INVOICE' in a.invoice) = 0 -- reversing invoices  ;


-- current period
-- break down october step by step
-- total invoices:  69, 10/12:76,10/13:78,10/14:80,10/15:86, 10/16-17-18:87,10/10:97
-- a week of being sick
-- 10/25 oct:103,nov:9; 10/27 oct:103, nov:17, 10/28 oct:103 nov:20; 10/29 oct:103 nov:29
-- 11/01: 32, 11/02: 33, 11/03:34,  11/04:37, 11/05:39, 11/08:47, 11/09:57, 11/10:60, 11/11:68
-- 11/12:71, 11/15:82,11/16:86,11/17:102,11/18:110, 11/19:115, 11/22:121, 11/23:121
-- 11/24:26/121, 11/25:33/121, 11/29:34/121
-- 12/3:57, 12/6:61, 12/7:80, 12/8:87, 12/9:93, 12/10:97, 12/13:114, 12/14:127,
-- 12/15:130, 12/16:139, 12/17:155, 12/20:160, 12/21:178,12/22:178/7, 12/23: 178/13, 12/24:178/21
-- 12/27:178/23. 12/28:178/31, 12/29:178/39, 12/30:178/44, 12/31:178/51, 1/3:53, 1/5:60
-- 1/6:74, 1/7:82, 1/10/83, 1/11:88, 1/12:95, 1/13:98, 1/14:110, 1/17:116, 1/18:125,
-- 1/19:125, 1/20:137, 1/21:141, 1/24:7-141, 1/25:17-141, 1/26:20-141, 1/27:31-141. 1/28:32-141
-- 1/31:36/141, 2/3:49, 2/4:23, 2/7:56, 2/8:60, 2/9:64,2/10:67, 2/11:72, 2/14:78, 2/15:88
-- 2/16:92,2/17:97,2/17:102, 2/21:108/0, 2/22:108/6, 2/23:108/13, 2/24/:108/13, 2/25:17/108
-- 3/1:21,3/3:38, 3/4:41, 3/7:52, 3/8:57,3/10:61, 3/11:67, 3/14:72,3/15:78, 3/17:93,3/18:96, 3/21:106
-- select a.invoice_number, a.order_type, a.vin, a.invoice_date, a.total_invoice_amount, a.make, adjustment_invoice
-- from gmgl.wfpm_invoices a
-- where a.invoice_date between '09/21/2021' and '10/20/2021'
-- order by vin;
-- -- only relevant order types: 68
-- select a.invoice_number, a.order_type, a.vin, a.invoice_date, a.total_invoice_amount, a.make, adjustment_invoice
-- from gmgl.wfpm_invoices a
-- where a.invoice_date between '09/21/2021' and '10/20/2021'
--   and a.order_type in ('SRE','TRE','TCS');
-- -- not paid in previous month: 62
-- select a.invoice_number, a.order_type, a.vin, a.invoice_date, a.total_invoice_amount, a.make, adjustment_invoice
-- from gmgl.wfpm_invoices a
-- where a.invoice_date between '09/21/2021' and '10/20/2021'
--   and a.order_type in ('SRE','TRE','TCS')
-- 	and not exists ( -- exclude vins that have already been paid
-- 		select 1
-- 		from gmgl.wfpm_monthly_report
-- 		where vin = a.vin)  
-- -- if there are multiple invoices in the current period, use only the original		
-- select *
-- from (
-- 	select a.invoice_number, a.vin, a.invoice_date, a.total_invoice_amount, a.make, 
-- 		row_number() over (partition by a.vin order by invoice_date asc) as seq -- first invoice in the period
-- 	from gmgl.wfpm_invoices a
-- 	where a.order_type in ('SRE','TRE','TCS')
-- 		and not exists ( -- exclude vins that have already been paid
-- 			select 1
-- 			from gmgl.wfpm_monthly_report
-- 			where vin = a.vin)
-- 		and a.invoice_date between '09/21/2021' and '10/20/2021') b
-- where seq = 1;
-- add the calculations



-- march "current month"
select aa.*, c.rate, c.days, 
	count(aa.invoice_number) over (partition by c.make) as count_by_make,
	count(aa.invoice_number) over () as total_count,
	round(((aa.total_invoice_amount * c.rate)/360) * c.days, 2) as interest_credit, total_invoice_amount,
  sum(total_invoice_amount) over (partition by c.make) as total_invoice_make,
  sum(round(((aa.total_invoice_amount * c.rate)/360) * c.days, 2)) over (partition by c.make) as credits_by_make,  
  sum(round(((aa.total_invoice_amount * c.rate)/360) * c.days, 2)) over () as total_interest_credits
from (
	select *
	from (
		select a.invoice_number, a.vin, a.invoice_date, a.total_invoice_amount, a.make, 
			row_number() over (partition by a.vin order by invoice_date asc) as seq -- first invoice in the period
		from gmgl.wfpm_invoices a
		where a.order_type in ('SRE','TRE','TCS')
			and not exists ( -- exclude vins that have already been paid
				select 1
				from gmgl.wfpm_monthly_report
				where vin = a.vin)
-- 				  and year_month < 202112) -- add this line if i forget to check against monthly report before inserting into gmgl.wfp_invoices
			and a.invoice_date between '02/21/2022' and '03/20/2022') b
	where seq = 1) aa
left join nc.gm_interest_credit_parameters c on aa.make = c.make
  and thru_date > current_date  -- make this better, using period dates
-- order by make  
-- order by right(vin, 8)
order by invoice_date


-- april invoices "next month"
select aa.*, c.rate, c.days, 
	count(aa.invoice_number) over (partition by c.make) as count_by_make,
	count(aa.invoice_number) over () as total_count,
	round(((aa.total_invoice_amount * c.rate)/360) * c.days, 2) as interest_credit, total_invoice_amount,
  sum(total_invoice_amount) over (partition by c.make) as total_invoice_make,
  sum(round(((aa.total_invoice_amount * c.rate)/360) * c.days, 2)) over (partition by c.make) as credits_by_make,  
  sum(round(((aa.total_invoice_amount * c.rate)/360) * c.days, 2)) over () as total_interest_credits
from (
	select *
	from (
		select a.invoice_number, a.vin, a.invoice_date, a.total_invoice_amount, a.make, 
			row_number() over (partition by a.vin order by invoice_date asc) as seq -- first invoice in the period
		from gmgl.wfpm_invoices a
		where a.order_type in ('SRE','TRE','TCS')
		  and not adjustment_invoice
			and not exists ( -- exclude vins that have already been paid
				select 1
				from gmgl.wfpm_monthly_report
				where vin = a.vin)
			and a.invoice_date between '03/21/2022' and '04/20/2022') b
	where seq = 1) aa
left join nc.gm_interest_credit_parameters c on aa.make = c.make
  and thru_date > current_date  -- make this better, using period dates
order by invoice_date



-- detail for nick with event description
select distinct aa.*, bb.order_number, cc.event_Description
from (
select aa.invoice_date, aa.invoice_number, aa.vin, aa.make, aa.total_invoice_amount, 
	round(((aa.total_invoice_amount * c.rate)/360) * c.days, 2) as interest_credit
from (
	select *
	from (
		select a.invoice_number, a.vin, a.invoice_date, a.total_invoice_amount, a.make, 
			row_number() over (partition by a.vin order by invoice_date asc) as seq -- first invoice in the period
		from gmgl.wfpm_invoices a
		where a.order_type in ('SRE','TRE','TCS')
			and not exists ( -- exclude vins that have already been paid
				select 1
				from gmgl.wfpm_monthly_report
				where vin = a.vin)
			and a.invoice_date between '02/21/2022' and '03/20/2022') b
	where seq = 1) aa
left join nc.gm_interest_credit_parameters c on aa.make = c.make
  and thru_date > current_date  -- make this better, using period dates
union
select aa.invoice_date, aa.invoice_number, aa.vin, aa.make, aa.total_invoice_amount, 
	round(((aa.total_invoice_amount * c.rate)/360) * c.days, 2) as interest_credit
from (
	select *
	from (
		select a.invoice_number, a.vin, a.invoice_date, a.total_invoice_amount, a.make, 
			row_number() over (partition by a.vin order by invoice_date asc) as seq -- first invoice in the period
		from gmgl.wfpm_invoices a
		where a.order_type in ('SRE','TRE','TCS')
		  and not adjustment_invoice
			and not exists ( -- exclude vins that have already been paid
				select 1
				from gmgl.wfpm_monthly_report
				where vin = a.vin)
			and a.invoice_date between '03/21/2022' and '04/20/2022') b
	where seq = 1) aa
left join nc.gm_interest_credit_parameters c on aa.make = c.make
  and thru_date > current_date ) aa
left join gmgl.vehicle_orders bb on aa.vin = bb.vin  
left join jo.vehicle_order_events cc on bb.order_number = cc.order_number
  and cc.active = 'Y'
  and cc.event_description like '%- MP%'
-- order by order_number
order by invoice_date, order_number



---------------------------------------------------------------------------------------------------------
-- everything above should be exactly what i need for the new version  2
-- everything below is just scratchpad
---------------------------------------------------------------------------------------------------------

-- 10/06/21
-- dont have nearly all the invoices from past months in gmgl.wfpm_invoices
select *
from (
	select a.invoice_number, a.vin, a.invoice_date, a.total_invoice_amount,
		row_number() over (partition by a.vin order by invoice_date desc) as seq
	from gmgl.wfpm_invoices a
	where a.order_type in ('SRE','TRE','TCS')
		and not exists ( -- this eliminates those vins paid on in a previous reporting period
			select 1
			from gmgl.wfpm_monthly_report
			where vin = a.vin
			  and year_month < 202108)
		and a.invoice_date between '07/21/2021' and '08/20/2021') b
where seq = 1

-- do i have them all in gmgl.vehicle_invoices
-- possibly, but that table does not handle adjustments, THAT MAY NOT MATTER
-- insert into gmgl.wfpm_invoices  
drop table if exists aug cascade;
create temp table aug as
select 
  left(trim(split_part(a.raw_invoice, 'INVOICE', 2)), 11) as invoice_number,
  replace(trim(left(split_part(a.raw_invoice, 'VIN', 2), 30)), ' ','') as vin,
	left(trim(split_part(a.raw_invoice, 'INVOICE', 3)), 8)::date as invoice_date,
	(substring(split_part(a.raw_invoice, 'TOTAL         ', 2), 30, 12))::numeric as total_invoice_amount,
	case
		when position('RYDELL' in a.raw_invoice) = 0 and position('CADILLAC OF GRAND FORKS' in a.raw_invoice) = 0 then false
		else true
  end as rydell_invoice,
  case 
		when position('ADJUSTMENT' in a.raw_invoice) <> 0 then true
		else false
  end as adjustment_invoice,
  substring(trim(split_part(a.raw_invoice, 'ORDER NO.', 2)), position('/' in trim(split_part(a.raw_invoice, 'ORDER NO.', 2))) + 1, 3) as order_type,
--   a.invoice, 
  b.make
from gmgl.vehicle_invoices a
left join gmgl.wfpm_makes_models b on trim(split_part(a.raw_invoice, ' ', 2)) = b.model
where left(trim(split_part(a.raw_invoice, 'INVOICE', 3)), 8)::date between '07/21/2021' and '08/20/2021';

select * 
from aug
where adjustment_invoice


-- exclude by vin invoices paid in previous period
-- exclude non stock order types
-- only those invoices dated in the current period
-- only the most recent invoice for those with multiple invoice numbers in the period
-- adjustment invoices
--		eliminate by vin that paid in a previous reporting period
--		use only the most recent when there are multiple invoices in the current period
select *
from (
	select a.invoice_number, a.vin, a.invoice_date, a.total_invoice_amount,
		row_number() over (partition by a.vin order by invoice_date desc) as seq
	from gmgl.wfpm_invoices a
	where a.order_type in ('SRE','TRE','TCS')
		and not exists ( -- this eliminates those vins paid on in a previous reporting period
			select 1
			from gmgl.wfpm_monthly_report
			where vin = a.vin)
		and a.invoice_date between '09/21/2021' and '10/20/2021') b
where seq = 1






-- 09/26 experiencing some mind fuck, the difference between gmgl.wfpm_monthly_report and gmgl.wfpm_invoices
select * from gmgl.wfpm_monthly_report
where invoice_date between '08/18/2021' and '08/25/2021'
order by invoice_Date
-- ok, what i have to keep in mind is that monthly_report is just that, all the other data is about verifying
-- the counts and amounts in the monthly_report 
-- so, i think i was getting distracted by wanting to populatte gmgl.wfpm_invoices with data from old report periods
-- not necessary (at least for now)
-- i have the data i need for the september report verification when it is published
-- and now am collecting the data needed for the october report (09/21 -> 10/20)

-- specifically where i am is that i have populated gmgl.wfpm_invoices invoices for the october report as of a few days ago
-- i beleive i need to separate the nightly processing from the report verifying

select * from gmgl.wfpm_monthly_report
where vin = '1GCUYEED5MZ318801'  

select *
from gmgl.wfpm_monthly_report
limit 10


V. now that i have the data, which invoices are relevant to interest credits
-- exclude invoices paid in previous period
-- exclude non stock order types
-- only those invoices dated in the current period
-- only the most recent invoice for those with multiple invoice numbers in the period
-- and that takes us down to 54 invoices, from 63
-- now that i am on the 22nd of sep


-- this takes care of the september reporting period
select *
from (
	select a.invoice_number, a.vin, a.invoice_date, a.total_invoice_amount,
		row_number() over (partition by a.vin order by invoice_date desc) as seq
	from gmgl.wfpm_invoices a
	where a.order_type in ('SRE','TRE','TSE')
		and not exists ( -- this eliminates those vins paid on in a previous reporting period
			select 1
			from gmgl.wfpm_monthly_report
			where vin = a.vin)
		and a.invoice_date between '08/21/2021' and '09/20/2021') b
where seq = 1

-------------------------------------------------------------------------------
--< 09/28/21
-------------------------------------------------------------------------------

and this query now returns 9 vehicles without a make
updated chr.build_data_describe_vehile
chose the 1st invoice when there are multiple in the period
buick good
cad good
gmc good
chev, i have 4 xtra vehicles
-- holy shit, 1GYS4BKL5MR462615 has 2 invoices in the period, i used the most recent: 84444.93,
-- 		the report used the original: 89238.03
select * 
from gmgl.wfpm_invoices
where vin = '1GYS4BKL5MR462615'

it felt reasonable to include TSE,Show/Special Events [Stock], but they are not in the report
so removing that order type, this query turns into a good test overall
and the result is perfect
from (
	select aa.*, b.response->'vinDescription'->>'division', c.rate, c.days, c.make,
		count(aa.invoice_number) over (partition by c.make) as count_by_make,
		round(((aa.total_invoice_amount * c.rate)/360) * c.days, 2) as interest_credit, 
		sum(total_invoice_amount) over (partition by c.make) as total_invoice_make,
		sum(round(((aa.total_invoice_amount * c.rate)/360) * c.days, 2)) over (partition by make) as credits_by_make,  
		sum(round(((aa.total_invoice_amount * c.rate)/360) * c.days, 2)) over () as total_interest_credits
	from (
		select *
		from (
			select a.invoice_number, a.vin, a.invoice_date, a.total_invoice_amount,
				row_number() over (partition by a.vin order by invoice_date asc) as seq -- first invoice in the period
			from gmgl.wfpm_invoices a
			where a.order_type in ('SRE','TRE')
				and not exists (  -- 
					select 1
					from gmgl.wfpm_monthly_report
					where vin = a.vin
					  and year_month < 202109)
				and a.invoice_date between '08/21/2021' and '09/20/2021') b
		where seq = 1) aa
	left join chr.build_data_describe_vehicle b on aa.vin = b.vin
	left join nc.gm_interest_credit_parameters c on (b.response->'vinDescription'->>'division')::citext = c.make
		and thru_date > current_date) x  -- make this better, using period dates
full outer join (
	select a.*
	from gmgl.wfpm_monthly_report a
	Join chr.build_data_describe_vehicle b on a.vin = b.vin
	where year_month = 202109
		and b.response->'vinDescription'->>'division' = 'Chevrolet') y on x.vin = y.vin
where x.interest_credit <> y.amount

select vin, make, count(vin) over (partition by make) as count_by_make, amount, invoice_amount,
  sum(invoice_amount) over (partition by make) as total_invoice_make,
  sum(amount) over (partition by make) as credits_by_make,
  sum(amount) over () as total_interest_credits
from (  
	select a.*, b.response->'vinDescription'->>'division' as make
	from gmgl.wfpm_monthly_report a
	Join chr.build_data_describe_vehicle b on a.vin = b.vin
	where year_month = 202109) c;

-- prepare some detail for the september report
select aa.*, b.response->'vinDescription'->>'division', c.rate, c.days, c.make,
	count(aa.invoice_number) over (partition by c.make) as count_by_make,
	round(((aa.total_invoice_amount * c.rate)/360) * c.days, 2) as interest_credit, total_invoice_amount,
  sum(total_invoice_amount) over (partition by c.make) as total_invoice_make,
  sum(round(((aa.total_invoice_amount * c.rate)/360) * c.days, 2)) over (partition by make) as credits_by_make,  
  sum(round(((aa.total_invoice_amount * c.rate)/360) * c.days, 2)) over () as total_interest_credits
from (
	select *
	from (
		select a.invoice_number, a.vin, a.invoice_date, a.total_invoice_amount,
			row_number() over (partition by a.vin order by invoice_date asc) as seq -- first invoice in the period
		from gmgl.wfpm_invoices a
		where a.order_type in ('SRE','TRE')
			and not exists ( -- exclude vins that have already been paid
				select 1
				from gmgl.wfpm_monthly_report
				where vin = a.vin
				  and year_month < 202112)
			and a.invoice_date between '11/21/2021' and '12/20/2021') b
	where seq = 1) aa
left join chr.build_data_describe_vehicle b on aa.vin = b.vin
left join nc.gm_interest_credit_parameters c on (b.response->'vinDescription'->>'division')::citext = c.make
  and thru_date > current_date  -- make this better, using period dates


-------------------------------------------------------------------------------
--/> 09/28/21
-------------------------------------------------------------------------------


/*
the daily process should include build data
and today 16 of the 23 are catalog data, some with multiple styles and no MSRPs
try them again tomorrow (deleting the today)
but what that means is that relying on build data for the make will not be up to date
thinking parse the model to make


-- this is all i need to identify the make
drop table if exists models;
create temp table models as
select distinct
  trim(split_part(a.raw_invoice, ' ', 2)) as model
from gmgl.vehicle_invoices a
where left(trim(split_part(a.raw_invoice, 'INVOICE', 3)), 8)::date > current_date - 365

select * from models

drop table if exists gmgl.wfpm_makes_models;
create table gmgl.wfpm_makes_models (
  make citext not null, 
  model citext primary key);

insert into gmgl.wfpm_makes_models
select 
  case 
    when model in ('SILVERADO','BLAZER','MALIBU','TRAILBLAZER','COLORADO','TRAX','EQUINOX','TRAVERSE',
			'TAHOE','SUBURBAN','BOLT EV','BOLT EUV','SPARK','CORVETTE','CAMARO','EXPRESS') then 'chevrolet'
    when model in ('ENCORE','ENVISION','ENCORE GX','ENCLAVE') then 'buick'
    when model in ('SIERRA','ACADIA','CANYON','TERRAIN','YUKON','SAVANA') then 'gmc'
    when model in ('ESCALADE','CT6','XT4','XT5','XT6','CT4','CT5','LYRIQ') then 'cadillac'
  end, model
from models
order by model

update gmgl.wfpm_invoices x
set make = y.make
from (
	select a.vin , trim(split_part(a.invoice, ' ', 2)), b.*
	from gmgl.wfpm_invoices a
	left join gmgl.wfpm_makes_models b on trim(split_part(a.invoice, ' ', 2)) = b.model) y
where trim(split_part(x.invoice, ' ', 2)) = y.model

10/27/21 missing
*/

select * from chr.describe_vehicle where vin = '3GTN9AED1NG111236'