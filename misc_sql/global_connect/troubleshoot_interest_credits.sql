﻿/*
report is generated in luigi in the misc.py module

this first section creates all the temp tables used in the function nc.get_fp_interest_credits()
added the creation of the temp table the_dates which houses all the date variables

the report is available on the BARS Main Age
the Document is : Wholesale Floor Plan Memo
date range: 06 - 01 - 2020, 06 - 26 - 2020

03/30/2021
	haing trouble getting the page to generate the correct report, so i asked jeri what date range she used
	"Typically just the first and last day of the month--but this month's report was available 3/30/21."
	and sure enough, 03  01  2021, 03  30  2021 worked

06/29/2021	
from jeri:
	Actual before fleet credits shows $107,216.20.  This is such a big difference that I wanted to figure out what was going on before I post the invoice. 
	I think the biggest difference is the unit count.  You are showing 61 and I'm counting 113 (possible I counted a unit or two off).  

	For Cadillac:  126 days
	All others:  158 days 

	Rate is .0425
the latest email: 
	June 2021 (credits / units)
	Actual: $55539 / 61
	
 
*/


-----------------------------------------------------------------------------------------------------
--< 08/31/2021	
-----------------------------------------------------------------------------------------------------
barrs: 72163.74
my report: 62318
fuck

drop table if exists jon.august_21_interest_credits cascade;
create unlogged table jon.august_21_interest_credits (
	vin citext primary key,
	invoice_amount numeric,
	invoice_date date,
	amount numeric,
	days integer);

-- all 74 invoices show up in gmgl.vehicle_invoices
select a.*, sum(amount) over (), b.vin, c.*
from jon.august_21_interest_credits	a
left join chr.describe_vehicle b on a.vin = b.vin
left join gmgl.vehicle_invoices c on a.vin = c.vin

select vin
from jon.august_21_interest_credits a
where not exists (
  select 1
  from chr.build_data_describe_vehicle
  where vin = a.vin)


-- cadillacs all missing total_invoice amount  
select a.*, b.response->'vinDescription'->>'source', b.response->>'bestMakeName',
  c.total_invoice, d.invoice_date, bb->'basePrice'->>'invoice', d.*
from jon.august_21_interest_credits	a
left join chr.build_data_describe_vehicle b on a.vin = b.vin
left join jsonb_array_elements(b.response->'style') bb on true
left join gmgl.vehicle_invoices c on a.vin = c.vin
left join nc.vehicle_invoices d on a.vin = d.vin


select * 
from (
	select a.vin, 
		left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date as inv_date, 
		left(trim(split_part(raw_invoice, 'ORDER', 2)), 16)
	from gmgl.vehicle_invoices a
	where left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between '2021-07-21' and '2021-08-20'
		and a.thru_date > current_date
		and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
		and ((
			position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
			or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0))) aa 
full outer join jon.august_21_interest_credits bb on aa.vin = bb.vin		
order by aa.vin
  
select * from gmgl.vehicle_invoices where vin = '5GAEVBKW4MJ240769'


-- orig invoice to Luther, subsq invoice to rydell
-- fuck, but only 1 row in gmgl.vehicle_invoices and in gmgl.vehicle_invoice_log
-- the luther invoice is what is currently in gmgl.vehicle_invoices, the rydell invoice is what i pulled today

-- gmgl.vehicle_invoices is missing the total_invoice amount on cads > 100K
-- the parser decodes the word PAY, cad has PY


select * from gmgl.vehicle_invoice_log where vin = '5GAEVBKW4MJ240769'

select * 
from gmgl.vehicle_invoice_log a
join (
  select vin, count(*)
  from gmgl.vehicle_invoice_log
  group by vin
  having count(*) > 1) b on a.vin = b.vin
order by a.vin, a.lookup_date

select vin, count(*), max(lookup_date)
from gmgl.vehicle_invoice_log
group by vin
having count(*) > 1
order by max(lookup_date)
-- if the invoice already exists, we don't rerun it
-- maybe we need to do a hash type 2 in function gmgl.xfm_vehicle_invoice(citext, json, citext)
-- or just do a separate invoice scrape for interest credits ??? shit, can't do an invoice scrape by date

-- this is the current query for getting invoices from global_connect.py
select inpmast_vin
from arkona.ext_inpmast
where status = 'I'
	and type_n_u = 'N'
	and make in ('Chevrolet','GMC','Buick', 'Cadillac')
	and inpmast_vin not in ('3GCUYGED6MG170976','1GKS2BKD8MR333891')
	and inpmast_vin not in (
		select vin
		from gmgl.vehicle_invoices
		where thru_date > now()) 

select a.vin, from_date, thru_date
from gmgl.vehicle_invoices a 
join (
  select vin
  from gmgl.vehicle_invoices
  group by vin
  having count(*) > 1) b on a.vin = b.vin
order by a.vin


-- what is missing here
-- from the function nc.get_fp_interest_credits()
do $$
declare 
  _current_month integer := (
    select distinct year_month
    from dds.dim_date
    where the_date = current_date);
  _current_month_month_yyyy text := (
    select month_yyyy
    from sls.months
    where year_month = _current_month);  
  _next_month integer := (
    select next_year_month
    from sls.months
    where year_month = _current_month);     
  _next_month_month_yyyy citext := (
    select month_yyyy
    from sls.months
    where year_month = _next_month);   
  _previous_month integer := (
    select previous_year_month
    from sls.months
    where year_month = _current_month);
  _current_from_date date := (
    select the_date
    from dds.dim_date
    where year_month = _previous_month
      and day_of_month = 21);
  _current_thru_date date := (
    select the_date
    from dds.dim_date
    where year_month = _current_month
      and day_of_month = 20);
  _next_from_date date := (
    select the_date
    from dds.dim_date
    where year_month = _current_month
      and day_of_month = 21);
  _next_thru_date date := (
    select the_date
    from dds.dim_date
    where year_month = _next_month
      and day_of_month = 20);      
begin
	drop table if exists wtf;
	create temp table wtf as 
	select _current_from_date, _current_thru_date, 2 as seq, 'Actual: $' || coalesce((sum(round(((a.total_invoice * d.rate)/360) * d.days, 2))::integer)::text, '0') || ' / ' || coalesce(count(*)::text, '0')
	from gmgl.vehicle_invoices a
	join jo.vehicle_orders b on a.vin = b.vin
	join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
		and c.vehicle_order_type not like 'Fleet%'  
	join nc.gm_interest_credit_parameters d on upper(b.make) = upper(d.make)
		and _current_thru_date between d.from_date and d.thru_date
	where a.thru_date > current_date
		and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between _current_from_date and _current_thru_date
		and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
	and ((
		position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
		or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0));
 end $$;
 select * from wtf;

-- from_date = 21st of last month
-- thru_date = 20th of current month
-- missing 3 vehicles for $10K, this makes no sense
-- select _current_from_date, _current_thru_date, 2 as seq, 'Actual: $' || coalesce((sum(round(((a.total_invoice * d.rate)/360) * d.days, 2))::integer)::text, '0') || ' / ' || coalesce(count(*)::text, '0')
select a.vin
from gmgl.vehicle_invoices a
join jo.vehicle_orders b on a.vin = b.vin
join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
	and c.vehicle_order_type not like 'Fleet%'  
join nc.gm_interest_credit_parameters d on upper(b.make) = upper(d.make)
	and _current_thru_date between d.from_date and d.thru_date
where a.thru_date > current_date
	and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between '2021-07-21' and '2021-08-20'
	and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
and ((
	position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
	or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0));

 select * 
-----------------------------------------------------------------------------------------------------
--< 08/31/2021	
-----------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------
--< 06/29/2021	
-----------------------------------------------------------------------------------------------------
from jeri:
	Actual before fleet credits shows $107,216.20.  This is such a big difference that I wanted to figure out what was going on before I post the invoice. 
	I think the biggest difference is the unit count.  You are showing 61 and Im counting 113 (possible I counted a unit or two off).  

	For Cadillac:  126 days
	All others:  158 days 

	Rate is .0425
the latest email: 
	June 2021 (credits / units)
	Actual: $55539 / 61
-------------------------------------------------------------------------------------------------------
again with the shitload of vehicles in jo.vehicle_orders_unavailable
dont know why, but need to put in a test

-- 1 cadillac 1GYS4DKL7MR355206, with no total_invoice value
select b.make, a.vin, a.total_invoice, 
  left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date as invoice_date,
  round(((a.total_invoice *.0425)/360) * case when b.make = 'cadillac' then 126 else 158 end, 2),
  sum(round(((a.total_invoice *.0425)/360) * case when b.make = 'CADILLAC' then 126 else 158 end, 2)) over (partition by b.make),
  count(make) over (partition by make) as make_count
from gmgl.vehicle_invoices a
join jo.vehicle_orders b on a.vin = b.vin
-- exclude fleet orders
join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
  and c.vehicle_order_type not like 'Fleet%'  
where a.thru_date > current_date
  and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between '05/21/2021' and '06/20/2021'
  and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
and ((
  position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
  or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0)) 
--   and a.vin = '1GYS4DKL7MR355206'
order by b.make, substring(a.vin, 10, 2),  substring(a.vin, 12, 6) 

select * from gmgl.vehicle_invoices where vin = '1GYS4DKL7MR355206'

select * from gmgl.vehicle_invoices where vin = '1GYS4BKL9MR368768'

update gmgl.vehicle_invoices
set total_invoice = 106203.42
where vin = '1GYS4DKL7MR355206';

select *
from dds.dim_date
where the_date = current_date

select a.* 
from jo.vehicle_orders_unavailable a
order by order_number

create index on jo.vehicle_orders_unavailable(the_date);

select * 
from gmgl.vehicle_orders
where order_number = 'ZNQJQW'

select * 
from jo.vehicle_orders_unavailable a
join gmgl.vehicle_orders b on a.order_number = b.order_number

fuck me the whole orders_unavailable seems goofy to me now
and whats worse, some of the unavailable i have incomplete event history, 
	ZHPHV4, ZHPHV3 on the order page (inactive) sholws current event as 5000, the last event in in event_codes is 3750 on 5/6
	XQKR4J	5000	3000	3/3/20
  ZNVSN0	6000	5050	6/15/21
  ZBJTH6  6000	5000	11/15/20
	ZCXXD6	6000	5050	2/26/21
	ZDFWBN	6000	5000	12/28/20
	ZGWJ1B 	5000	4000	5/5/21

	
select * from jo.vehicle_order_events
where order_number in ('ZGWJ1B')
order by order_number, event_code


select a.*, b.vin
from jo.vehicle_orders_unavailable a
join jo.vehicle_orders b on a.order_number = b.order_number
where a.order_number like 'z%'
order by a.order_number


select * 
delete
from jo.vehicle_orders_unavailable
where order_number similar to '(ZF|ZG|ZH|ZJ|ZK|ZM|ZN)%'














-- ben wanted a breakdown on these vehicles: 
drop table if exists ben;
create temp table ben as
select d.inpmast_stock_number, a.vin, d.year as model_year, b.make, d. model, d.body_style, e.ground_date, f.delivery_date, f.sale_type, f.sold_in_transit
from gmgl.vehicle_invoices a
join jo.vehicle_orders b on a.vin = b.vin
-- exclude fleet orders
join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
  and c.vehicle_order_type not like 'Fleet%'  
left join arkona.ext_inpmast d on a.vin = d.inpmast_vin  
left join nc.vehicle_acquisitions e on a.vin = e.vin
  and d.inpmast_stock_number = e.stock_number
left join nc.vehicle_sales f on d.inpmast_stock_number = f.stock_number  
where a.thru_date > current_date
  and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between '05/21/2021' and '06/20/2021'
  and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
and ((
  position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
  or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0));


select make, model, count(*),
  count(make) filter (where ground_date is not null) as on_ground,
  count(make) filter (where ground_date is null) as in_transit,
  count(make) filter (where delivery_date is not null) as sold
from ben
group by make, model
order by make,model

-----------------------------------------------------------------------------------------------------
--/> 06/29/2021	
-----------------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------
--< 3/30/21  Jeri: Actual interest credits for March were $84526.98 (excludes fleet).  I show
--							88 units rather than 76.  
--------------------------------------------------------------------------------
-- new % is .0425, days = 158, cad = 126

from jeri:
It changed from 120 and 93(cad) to 157 and 126 (cad) on 4/16/20.  The interest rate went from 5.75% to 4.25%.
Then changed from 157 and 126 (cad) to 158 and 126 (cad) in Jan 2021.  Interest rate stayed at 4.25%
I don’t get a notice that this changes—just the report.

update nc.gm_interest_credit_parameters
set thru_date = '02/28/2021'
where days = 157;
insert into nc.gm_interest_credit_parameters values
('buick',0.0425,158,'03/31/2021','12/31/9999'),
('chevrolet',0.0425,158,'03/31/2021','12/31/9999'),
('gmc',0.0425,158,'03/31/2021','12/31/9999');

all problems were caused by orders getting put into jo.vehicle_orders_unavailable wrongly
not sure how exactly, but this is the same period of time where i fucked up vehicle invoices putting
the wrong query into production, although, offhand, i dont know how invoices affected the generation of
order data, duh, look at nc.get_fp_interest_credits(), the whole thing is based on invoices

update gmgl.vehicle_invoices set total_invoice = 109209.88 where vin = '1GYS4DKL8MR291385'
select * from gmgl.vehicle_invoices where vin in ('1GNSKTKL3MR300779','1GCUYGED6MZ269295','1GCUYDEDXMZ271050','1GCUYDED7MZ271202','1GCUYEED2MZ273767','1GCUYGEL5MZ274429' )
select * from jo.vehicle_orders where vin in ('1GKS2HKD3MR297403' )
not in jo.vehicle_orders
select * from gmgl.vehicle_orders where vin in ('1GKS2HKD3MR297403' )

1GKS2HKD3MR297403



                 select *
                    from gmgl.vehicle_orders a  
                    where  not exists (-- exclude orders_unavailable
                      select 1
                      from jo.vehicle_orders_unavailable
                      where order_number = a.order_number) and vin = '5GAEVBKW7MJ192393'
                      
select * delete from jo.vehicle_orders_unavailable where order_number in ('ZJZM94')
                      
select b.make, a.vin, a.total_invoice, 
  left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date as invoice_date,
  round(((a.total_invoice *.0425)/360) * case when b.make = 'cadillac' then 126 else 158 end, 2),
  sum(round(((a.total_invoice *.0425)/360) * case when b.make = 'CADILLAC' then 126 else 158 end, 2)) over (partition by b.make),
  count(make) over (partition by make) as make_count
from gmgl.vehicle_invoices a
join jo.vehicle_orders b on a.vin = b.vin
-- exclude fleet orders
join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
  and c.vehicle_order_type not like 'Fleet%'  
where a.thru_date > current_date
  and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between '02/21/2021' and '03/20/2021'
  and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
and ((
  position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
  or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0)) 
-- and b.make = 'GMC'  
order by b.make, substring(a.vin, 10, 2),  substring(a.vin, 12, 6) 

select 38369.60 + 13130.85 + 7737.11 + 25289.42
--------------------------------------------------------------------------------
--/> 3/30/21  Jeri: Actual interest credits for March were $84526.98 (excludes fleet).  I show
--							88 units rather than 76.  
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--< 3/21  on 3/21 returning a non 0 estimate for current month (March)
--------------------------------------------------------------------------------
select * from the_dates;

select 3 as seq, 
  case 
    when current_date > (select _current_thru_date from the_dates) then 'Estimated: $0 / 0'
  else
    'Estimated: $'|| coalesce((sum(round(((d.total_invoice * d.rate)/360) * d.days, 2))::integer)::text, '0') || ' / ' ||  coalesce(count(*)::text, '0')
  end 
from (
  select a.order_number, a.make, coalesce(b.peg_invoice, c.alloc_invoice) as total_invoice, aa.rate, aa.days
  from current_month_estimate_by_days a
  join nc.gm_interest_credit_parameters aa on upper(a.make) = upper(aa.make)
    and aa.thru_date > current_date  
  left join est_invoice_by_peg b on a.alloc_group = b.alloc_group
    and a.peg = b.peg
  left join est_invoice_by_alloc c on a.alloc_group = c.alloc_group) d
where total_invoice is not null 

-- here is the problem, est_invoice_date < current_thru_date
-- XKSFNK;CHEVROLET;TRAX;1LT;2020-03-16
select * 
from current_month_estimate_by_days

--------------------------------------------------------------------------------
--/> 3/21  on 3/21 returning a non 0 estimate for current month (March)
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
--< 3/25  march actual changed
--------------------------------------------------------------------------------
-- here is the basic issue
-- invoice "from_date" is based on vehicle being in inpmast
select a.vin, a.from_date, left(trim(split_part(a.raw_invoice, 'INVOICE', 3)), 8)::date as invoice_date, b.order_number, c.effective_date
from gmgl.vehicle_invoices a 
left join gmgl.vehicle_orders b on a.vin = b.vin
left join jo.vehicle_order_events c on b.order_number = c.order_number
  and c.event_code = '4150'
where a.vin in (
  select vin
  from gmgl.vehicle_invoices
  where from_date > '03/20/2020')
order by a.from_date


select *
from jo.vehicle_order_events 
where event_code = '4150' 
  and effective_date between '2020-03-21' and '2020-04-20'



  
gmgl.vehicle_invoices populated based on inpmast:
  select inpmast_vin
  from arkona.ext_inpmast
  where status = 'I'
    and type_n_u = 'N'
    and make in ('Chevrolet','GMC','Buick', 'Cadillac')
    and inpmast_vin not in (
      select vin
      from gmgl.vehicle_invoices
      where thru_date > now())



select *
from gmgl.vehicle_order_events
where order_number = 'WJBC7B'


select * from the_dates

select * 
from gmgl.vehicle_invoices
where vin = '1GNSKCKCXLR308893'


-- current_month actual:
select 2 as seq, 'Actual: $' || coalesce((sum(round(((a.total_invoice * d.rate)/360) * d.days, 2))::integer)::text, '0') || ' / ' || coalesce(count(*)::text, '0')
from gmgl.vehicle_invoices a
join jo.vehicle_orders b on a.vin = b.vin
join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
  and c.vehicle_order_type not like 'Fleet%'  
join nc.gm_interest_credit_parameters d on upper(b.make) = upper(d.make)
  and d.thru_date > current_Date
where a.thru_date > current_date
  and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between (select _current_from_date from the_dates) and (select _current_thru_date from the_dates)
  and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
and ((
  position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
  or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0))

 -- next month actual
select 5 as seq, 'Actual: $' || coalesce((sum(round(((a.total_invoice * d.rate)/360) * d.days, 2))::integer)::text, '0') || ' / ' || coalesce(count(*)::text, '0')
from gmgl.vehicle_invoices a
join jo.vehicle_orders b on a.vin = b.vin
join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
  and c.vehicle_order_type not like 'Fleet%'  
join nc.gm_interest_credit_parameters d on upper(b.make) = upper(d.make)  
  and d.thru_date > current_date
where a.thru_date > current_date
  and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between (select _next_from_date from the_dates) and (select _next_thru_date from the_dates)
  and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
and ((
  position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
  or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0)) 


of course, the issue now is how do i do all this correctly

select a.order_number, a.vin, c.*
from gmgl.vehicle_orders a
left join gmgl.vehicle_invoices b on a.vin = b.vin
left join jo.vehicle_order_events c on a.order_number = c.order_number
  and c.event_code = '4150'
where b.vin is null     
  and a.model_year > 2018 

  -- compare nc.vehicle_invoices to jo.vehicle_order_events

 select vin, 
 from nc.vehicle_invoices 


 -------------------------------------
 -- 3/27/20 

-- 13 invoices with an invoice date of 03/18 showed up on 03/26
select a.vin, a.total_invoice, left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date, from_date
from gmgl.vehicle_invoices a
where a.thru_date > current_date
  and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between '2020-02-21' and '2020-03-20'
  and from_date > '03/25/2020'




  
-- in inpmast on 3/26
select *
from arkona.xfm_inpmast
where inpmast_vin = '3GCUYDED1LG313930'

select * from gmgl.vehicle_invoices where vin = '3GCUYDED1LG313930'

-- gl transactions dated 3/18, actual transaction date id 03/25
select b.the_date, a.*
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
where a.control = 'G39944'

SO WHAT IS THE CORRECT SOURCE FOR INVOICES

-- orders has a vin, has an event code of 4150, does not currently exists in gmgl.vehicle_invoices
select a.order_number, a.order_type, a.tpw, a.vin, a.model_year, a.alloc_group, c.event_code, d.event_code, d.effective_date
from gmgl.vehicle_orders a
left join gmgl.vehicle_invoices b on a.vin = b.vin
left join jo.vehicle_order_events c on a.order_number = c.order_number
  and left(c.event_code, 1)::integer > 3 
left join jo.vehicle_order_events d on a.order_number = d.order_number
  and d.event_code = '4150'
where a.vin is not null
  and b.vin is null
  and a.model_year > 2019


select *
from jo.vehicle_order_events
where order_number = 'XMJZ7N'

-- 2 anomalies, invoice date > ship date
-- has a 4xxx event code, but no 4150
KL79MUSLXMB003621: from the invoice, invoice: 03/26/20, shipped: 03/17/20
3GCUYGED4LG319142: from the invoice, invoice: 03/24/20, shipped: 03/23/20
select distinct order_number
-- select *
from jo.vehicle_order_events a
join gmgl.vehicle_orders b on a.order_number = b.order_number
where exists ( -- has a 4xxx event code
  select 1
  from jo.vehicle_order_events
  where order_number = a.order_number
    and left(event_code, 1)::integer > 3)
and not exists ( -- but no 4150
  select 1
  from jo.vehicle_order_events
  where order_number = a.order_number
    and event_code = '4150')    



-- 03/28
-- do a query all orders with an event code of 4150 that do not exist in vehicle_invoices
-- this returns 2 vins for which an invoice is accessible: 1GTG6CEN2L1231211, 1GKS2HKJXLR305873
select a.order_number, a.vin, b.effective_date 
from gmgl.vehicle_orders a
join jo.vehicle_order_events b on a.order_number = b.order_number
  and b.event_code = '4150'
  and b.effective_date > '01/01/2020'
left join gmgl.vehicle_invoices c on a.vin = c.vin  
  and c.thru_date > current_date
where a.vin is not null
  and c.vin is null
order by b.effective_date  

of course, neither of these vins show up in inpmast or nc.vehicles
select * from nc.vehicles where vin = '1GKS2HKJXLR305873'

select * from gmgl.vehicle_orders where vin in ('1GTG6CEN2L1231211', '1GKS2HKJXLR305873')

select * from jo.vehicle_order_events where order_number in ('XKKS3M', 'XMJWN4') order by order_number, event_code

select * from gmgl.vehicle_invoices where vin in ('1GTG6CEN2L1231211', '1GKS2HKJXLR305873')


select * from arkona.xfm_inpmast
where status = 'I'
  and current_row
  and model in ('canyon', 'yukon xl')
  and type_n_u = 'N'

inventory accounts: 
  canyon: 1428012
  yukon xl: 1432012

select *
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
  and b.account = '123706' 
limit 100 

select *
from nc.stg_availability a
where model = 'yukon xl'



select current_date, f.the_date, f.control
--   g.inpmast_vin, g.year, g.make, g.model, g.model_code, 
--   g.color_code, g.color,
--   case f.journal_code
--     when 'PVI' then 'factory'
--     when 'CDH' then 'dealer trade'
--     when 'GJE' then 'ctp'
--     when 'EFT' then 'dealer trade'
--   end as source
from ( -- accounting journal
  select a.control, max(b.the_date) as the_date, sum(a.amount) as amount
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_date between current_Date - 365 and current_date--'06/22/2018'
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account in ( -- all new vehicle inventory account
      select b.account
      from fin.dim_account b 
      where b.account_type = 'asset'
        and b.department_code = 'nc'
        and b.typical_balance = 'debit'
        and b.current_row
        and b.account <> '126104')   
--   inner join fin.dim_journal d on a.journal_key = d.journal_key
--     and 
--       case
--         when a.control = 'G37876' then d.journal_code = 'CDH'
-- --         when a.control in ('G38401','G38402','G38403','G38404','G38405','G38406','G38407','G38408','G38409') then d.journal_code = 'EFT'
--         else d.journal_code in ('PVI','CDH','GJE','EFT')
--       end
--   inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where a.post_status = 'Y'
  group by a.control -- , d.journal_code
  having sum(a.amount) > 10000) f

where not exists (
  select 1
  from nc.vehicle_acquisitions
  where stock_number = f.control) 
and not exists (
  select 1
  from arkona.xfm_inpmast
  where inpmast_stock_number = f.control
    and current_row)  


04/19/20
-- do a query all orders with an event code of 4150 that do not exist in vehicle_invoices
-- this returns 2 vins for which an invoice is accessible: 1GTG6CEN2L1231211, 1GKS2HKJXLR305873
select a.order_number, a.vin, b.effective_date 
from gmgl.vehicle_orders a
join jo.vehicle_order_events b on a.order_number = b.order_number
  and b.event_code = '4150'
  and b.effective_date > '01/01/2020'
left join gmgl.vehicle_invoices c on a.vin = c.vin  
  and c.thru_date > current_date
where a.vin is not null
  and c.vin is null
order by b.effective_date  

-- returns one row, no invoice, but vehicle is already sold in february
select * from jo.vehicle_order_events where order_number = 'xjgctm' order by event_code

select * from gmgl.vehicle_invoices where vin = '1GT49REY3LF215108'

select * from arkona.xfm_inpmast where inpmast_vin = '1GT49REY3LF215108' order by inpmast_key

select * from gmgl.vehicle_invoices where vin = 'KL7CJPSB3LB333994'

                        select inpmast_vin
                        from arkona.ext_inpmast
                        where status = 'I'
                          and type_n_u = 'N'
                          and make in ('Chevrolet','GMC','Buick', 'Cadillac')
                          and inpmast_vin not in (
                            select vin
                            from gmgl.vehicle_invoices
                            where thru_date > now())

select * from arkona.ext_inpmast where inpmast_vin = 'KL7CJPSB3LB333994'              

select *
from fin.fact_gl 
where control = 'G39994'

              
-------------------------------------------------------------------------
--< reconcile march 2020
-------------------------------------------------------------------------
the diff is Chev: i have 100483.01, the report has, 101,608.48

select 101608.48 - 100483.01

3GCUYGET0LG313622     58,720.33  2020/03/20     1,125.47     120  

select * from gmgl.vehicle_invoices where vin = '3GCUYGET0LG313622'

There are actually 3 invoices for this vehicle, we only picked up the most recent which was a credit

select b.make, a.vin, left(a.vin, 9), right(a.vin, 8), a.total_invoice, 
  left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date as invoice_date,
  round(((a.total_invoice *.0575)/360) * case when b.make = 'cadillac' then 93 else 120 end, 2),
  sum(round(((a.total_invoice *.0575)/360) * case when b.make = 'CADILLAC' then 93 else 120 end, 2)) over (partition by b.make)
from gmgl.vehicle_invoices a
join jo.vehicle_orders b on a.vin = b.vin
-- exclude fleet orders
join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
  and c.vehicle_order_type not like 'Fleet%'  
where a.thru_date > current_date
  and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between '02/21/2020' and '03/20/2020'
  and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
and ((
  position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
  or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0)) 
and b.make = 'CHEVROLET'  
order by b.make, substring(a.vin, 10, 2),  substring(a.vin, 12, 6)   

-------------------------------------------------------------------------
--/> reconcile march 2020
-------------------------------------------------------------------------


-------------------------------------------------------------------------
--< 04/20/20
-------------------------------------------------------------------------
4/19 sunday morning: Actual: $20726 / 23
4/20 monday morning: Actual: $20066 / 23
Why did the $$ change?
on sunday, i updated the parameters in nc.gm_interest_credit_parameters,
but that update has a from_date of 4/21

damnit, the join on nc.gm_interest_credit_parameters
-- well, this query is using the wrong values
-- select 2 as seq, 'Actual: $' || coalesce((sum(round(((a.total_invoice * d.rate)/360) * d.days, 2))::integer)::text, '0') || ' / ' || coalesce(count(*)::text, '0')
select a.total_invoice, d.rate, d.days, d.make
from gmgl.vehicle_invoices a
join jo.vehicle_orders b on a.vin = b.vin
join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
  and c.vehicle_order_type not like 'Fleet%'  
join nc.gm_interest_credit_parameters d on upper(b.make) = upper(d.make)
  and d.thru_date > current_Date
where a.thru_date > current_date
  and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between (select _current_from_date from the_dates) and (select _current_thru_date from the_dates)
  and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
and ((
  position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
  or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0))
  
-- this is not what is needed (running on 4/20, the thru_date for the previous and correct values is 4/20)
-- DUH
select * from nc.gm_interest_credit_parameters where thru_date > current_date  

select * from nc.gm_interest_credit_parameters where current_date between from_Date and thru_date

-- this is correct
select a.total_invoice, d.rate, d.days, d.make
from gmgl.vehicle_invoices a
join jo.vehicle_orders b on a.vin = b.vin
join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
  and c.vehicle_order_type not like 'Fleet%'  
join nc.gm_interest_credit_parameters d on upper(b.make) = upper(d.make)
  and current_date between d.from_date and d.thru_date
where a.thru_date > current_date
  and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between (select _current_from_date from the_dates) and (select _current_thru_date from the_dates)
  and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
and ((
  position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
  or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0))

-- and the actual values now match yesterday
select 2 as seq, 'Actual: $' || coalesce((sum(round(((a.total_invoice * d.rate)/360) * d.days, 2))::integer)::text, '0') || ' / ' || coalesce(count(*)::text, '0')
from gmgl.vehicle_invoices a
join jo.vehicle_orders b on a.vin = b.vin
join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
  and c.vehicle_order_type not like 'Fleet%'  
join nc.gm_interest_credit_parameters d on upper(b.make) = upper(d.make)
  and current_date between d.from_date and d.thru_date
where a.thru_date > current_date
  and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between (select _current_from_date from the_dates) and (select _current_thru_date from the_dates)
  and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
and ((
  position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
  or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0))  

-------------------------------------------------------------------------
--/> 04/20/20
-------------------------------------------------------------------------  


-------------------------------------------------------------------------
--< 04/21/20
-------------------------------------------------------------------------  
well, we have a problem
the current_date is now past the 20th, so the fix from yesterday, is for the current month, april, again
returning the wrong parameters

select * from the_dates

so, i think current month actual will have to have a conditional join on nc.gm_interest_credit_parameter
or, rather than the where clause using current_date, use the _current_thru_date
yes, i think so, a fixed date is what is needed
and it works: (sure hope this is the final fix for this)

select 2 as seq, 'Actual: $' || coalesce((sum(round(((a.total_invoice * d.rate)/360) * d.days, 2))::integer)::text, '0') || ' / ' || coalesce(count(*)::text, '0')
from gmgl.vehicle_invoices a
join jo.vehicle_orders b on a.vin = b.vin
join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
  and c.vehicle_order_type not like 'Fleet%'  
join nc.gm_interest_credit_parameters d on upper(b.make) = upper(d.make)
  and (select _current_thru_date from the_dates) between d.from_date and d.thru_date
where a.thru_date > current_date
  and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between (select _current_from_date from the_dates) and (select _current_thru_date from the_dates)
  and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
and ((
  position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
  or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0))  


but now, next month estimate looks off
the email this morning: Estimated: $17750 / 27
but when i run the function now: Estimated: $18327 / 27

select 6 as seq, 'Estimated: $'|| coalesce((sum(round(((d.total_invoice * d.rate)/360) * d.days, 2))::integer)::text, '0') || ' / ' ||  coalesce(count(*)::text, '0')
from (
  select a.order_number, a.make, coalesce(b.peg_invoice, c.alloc_invoice) as total_invoice, aa.rate, aa.days
  from next_month_estimate_by_days a
  join nc.gm_interest_credit_parameters aa on upper(a.make) = upper(aa.make)
    and (select _next_thru_date from the_dates) between aa.from_date and aa.thru_date
  left join est_invoice_by_peg b on a.alloc_group = b.alloc_group
    and a.peg = b.peg
  left join est_invoice_by_alloc c on a.alloc_group = c.alloc_group) d
where total_invoice is not null    

yikes, i change the join on nc.gm_interest_credit_parameters everywhere
no no no 
for these 2 i want the parameters determined by current month thru_date 
seq 2: current month actual
seq 3: current month estimated
for these 2 i want the parameters determined by next month thru_date
seq 5: next month actual
seq 6: next month estimated

-------------------------------------------------------------------------
--/> 04/21/20
-------------------------------------------------------------------------  


-------------------------------------------------------------------------
--< reconcile april 2020
-------------------------------------------------------------------------
04/29 from jeri: Actual interest credit received was $15690.98
report shows 21229 /24 

jeris update said:
  Chev/Buick/GMC floorplan interest rate is changing to 4.25% for 157 days for vehicles invoices 4/21 and after.
the gm report says: 
              WHOLESALE FINANCE RATE       2020/01/01        .0575              
              WHOLESALE FINANCE RATE       2020/04/16        .0425    

to get the correct report, enter the dates 04/01/2020 and 04/30/2020

the diff is Chev: i have 100483.01, the report has, 101,608.48

select 101608.48 - 100483.01

3GCUYGET0LG313622     58,720.33  2020/03/20     1,125.47     120  

select * from gmgl.vehicle_invoices where vin = '3GCUYGET0LG313622'

There are actually 3 invoices for this vehicle, we only picked up the most recent which was a credit

select b.make, a.vin, left(a.vin, 9), right(a.vin, 8), a.total_invoice, 
  left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date as invoice_date,
  round(((a.total_invoice *.0575)/360) * case when b.make = 'cadillac' then 93 else 120 end, 2) as old_rate,
  round(((a.total_invoice *.0425)/360) * case when b.make = 'cadillac' then 126 else 157 end, 2) as new_rate, 
  sum(round(((a.total_invoice *.0575)/360) * case when b.make = 'CADILLAC' then 93 else 120 end, 2)) over (partition by b.make)
from gmgl.vehicle_invoices a
join jo.vehicle_orders b on a.vin = b.vin
-- exclude fleet orders
join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
  and c.vehicle_order_type not like 'Fleet%'  
where a.thru_date > current_date
  and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between '03/21/2020' and '04/20/2020'
  and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
and ((
  position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
  or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0)) 
-- and b.make = 'CHEVROLET'  
order by b.make, substring(a.vin, 10, 2),  substring(a.vin, 12, 6)   


select ((57077.01 * .0575)/360) * 120  -- 1093.976
select (57077.01 * .0575 * 120)/360 = credits

select 360 * 808.59  -- 291092.40
select 57077.01 * 120

select (360 * 808.59)/(57077.01 * 120)  = .0425

select (360 * 371.54)/(26226.18 * 120)


BUICK;KL4MMESL0LB108583;KL4MMESL0;LB108583;32294.21;2020-04-14;618.97;598.56;3335.59

select (360 * 618.97)/(32294.21 * 120)

-- sent to jeri
The report from global connect, using 04/01 ->04/30 generates the Amount using .0425 as the rate

The formula for the rate would be: (360 * credit amount)/(invoice * 120)

From the report:
KL7CJPSB0 LB326887     26,226.18  2020/04/09       371.54     120
(360 * 371.54)/(26226.18 * 120) =  0.04250028025431076886

3GTU9DED1 LG315979     57,077.01  2020/03/23       808.59     120
(360 * 808.59)/(57077.01 * 120) = 0.04249994875344731618

-------------------------------------------------------------------------
--/> reconcile april 2020
-------------------------------------------------------------------------

-------------------------------------------------------------------------
--/> reconcile june 2020
-------------------------------------------------------------------------

select * from jo.vehicle_orders b limit 10
total from the report (6/26/20):
  select 30981.62 + 6712.06 + 607.34 + 24019.08 = 62320

BUICK;6712
CADILLAC;607
CHEVROLET;30982
GMC;22889

the difference is GMC, erport shows 24019, i have 22889

-- current_month actual:
select b.make, coalesce((sum(round(((a.total_invoice * d.rate)/360) * d.days, 2))::integer)::text, '0')
from gmgl.vehicle_invoices a
join jo.vehicle_orders b on a.vin = b.vin
join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
  and c.vehicle_order_type not like 'Fleet%'  
join nc.gm_interest_credit_parameters d on upper(b.make) = upper(d.make)
  and d.thru_date > current_Date
where a.thru_date > current_date
  and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between (select _current_from_date from the_dates) and (select _current_thru_date from the_dates)
  and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
and ((
  position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
  or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0))
group by b.make  


select b.make, b.vin, round(((a.total_invoice * d.rate)/360) * d.days, 2)
from gmgl.vehicle_invoices a
join jo.vehicle_orders b on a.vin = b.vin
join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
  and c.vehicle_order_type not like 'Fleet%'  
join nc.gm_interest_credit_parameters d on upper(b.make) = upper(d.make)
  and d.thru_date > current_Date
where a.thru_date > current_date
  and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between (select _current_from_date from the_dates) and (select _current_thru_date from the_dates)
  and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
and ((
  position('RYDELL CHEVROLET' in a.raw_invoice) <> 0)
  or (position('CADILLAC OF GRAND FORKS' in a.raw_invoice) <> 0))  
and b.make = 'GMC'
order by b.make, substring(a.vin, 10, 2),  substring(a.vin, 12, 6)   

missing vins: 1GT49NEY5LF260334 : 1129.93 

yep: invoice date 6/15
select * from gmgl.vehicle_invoices where vin = '1GT49NEY5LF260334'
entered: 6/18
select * from arkona.xfm_inpmast where inpmast_vin = '1GT49NEY5LF260334'

select *
from gmgl.vehicle_invoices a
join jo.vehicle_orders b on a.vin = b.vin
join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
--   and c.vehicle_order_type not like 'Fleet%'  
where a.vin = '1GT49NEY5LF260334' 

order type is FNR, should not be included

select * from gmgl.vehicle_orders where vin = '1GT49NEY5LF260334'

select order_type, count(*) from gmgl.vehicle_orders group by order_type

select * from jo.vehicle_order_types
-------------------------------------------------------------------------
--/> reconcile june 2020
-------------------------------------------------------------------------