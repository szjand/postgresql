﻿select * from jo.vehicle_order_types
07/15/21
after last months snafu, thinking about refactoring the interest credits email
get rid of estimates, just show the actuals

no way to check "the report" during the course of the month, it is only available at the end of the period
the only thing i can think of now is to periodically double check against the number of relevant invoices in gmgl.vehicle_invoices

but

if i base the report on that table, shouldnt even have to do that


-- do sold orders show up in interest credits report
-- nope
-- these are all sold retail orders, they (the relevant ones) do not show up on the june report
select vin, left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date from gmgl.vehicle_invoices
where position('SRE' in raw_invoice) <> 0 
order by left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date

-- credit invoices, not very many
select vin, left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date
from gmgl.vehicle_invoices a
where position('CREDIT FOR INVOICE' in a.raw_invoice) <> 0
order by left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date




select a.vin
from gmgl.vehicle_invoices a
join jo.vehicle_orders b on a.vin = b.vin
join jo.vehicle_order_types c on b.order_type = c.vehicle_order_type_key
  and c.vehicle_order_type not like 'Fleet%'  
-- join nc.gm_interest_credit_parameters d on upper(b.make) = upper(d.make)
--   and '07/21/2021' between d.from_date and d.thru_date
where a.thru_date > current_date
  and left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date between '06/21/2021' and current_date
  and position('CREDIT FOR INVOICE' in a.raw_invoice) = 0 -- reversing invoices
  and position('TRE' in a.raw_invoice) <> 0 -- exclude fleet orders