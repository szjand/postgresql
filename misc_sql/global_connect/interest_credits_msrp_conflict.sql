﻿/*
11/11/21
from ben knudson via afton
G43514 is showing the wrong MSRP. Shows in vision as $42,965 but is actually $40,070. Looks like an 
adjustment invoice hit after the fact and not sure how we can get vision to recognize that if it’s even possible
*/

select inpmast_stock_number, inpmast_vin, list_price, b.invoice_number, b.invoice_date, b.vin, b.total_invoice_amount,
  trim((substring(split_part(b.invoice, 'TOTAL         ', 2), 21, 9)))::numeric as invoice_msrp
from arkona.ext_inpmast a
left join gmgl.wfpm_invoices b on a.inpmast_vin = b.vin
where status = 'I'
  and a.make not in ('honda','nissan')
  and type_n_u = 'N'
order by a.inpmast_vin

-- dup vins - multiple invoices
select inpmast_vin
from (
select inpmast_stock_number, inpmast_vin, list_price, b.invoice_number, b.invoice_date, b.vin, b.total_invoice_amount,
  trim((substring(split_part(b.invoice, 'TOTAL         ', 2), 21, 9)))::numeric as invoice_msrp
from arkona.ext_inpmast a
left join gmgl.wfpm_invoices b on a.inpmast_vin = b.vin
where status = 'I'
  and a.make not in ('honda','nissan')
  and type_n_u = 'N') aa 
group by inpmast_vin
having count(*) > 1

-- msrp <> invoice msrp
select inpmast_stock_number, inpmast_vin, list_price, b.invoice_number, b.invoice_date, b.vin, b.total_invoice_amount,
  trim((substring(split_part(b.invoice, 'TOTAL         ', 2), 21, 9)))::numeric as invoice_msrp
from arkona.ext_inpmast a
left join gmgl.wfpm_invoices b on a.inpmast_vin = b.vin
where status = 'I'
  and a.make not in ('honda','nissan')
  and type_n_u = 'N'
  and a.list_price <> trim((substring(split_part(b.invoice, 'TOTAL         ', 2), 21, 9)))::numeric

-- no invoice
select inpmast_stock_number, inpmast_vin, list_price, b.invoice_number, b.invoice_date, b.vin, b.total_invoice_amount,
  trim((substring(split_part(b.invoice, 'TOTAL         ', 2), 21, 9)))::numeric as invoice_msrp
from arkona.ext_inpmast a
left join gmgl.wfpm_invoices b on a.inpmast_vin = b.vin
where status = 'I'
  and a.make not in ('honda','nissan')
  and type_n_u = 'N'
  and b.vin is null
  -- these 6 vins have no invoice in wfpm_invoices, but they each have only a single invoice
  and a.inpmast_vin not in ('1GCUYDED1MZ384417','1GTU9DEL4MZ244690','1GTU9DEL9MZ244183','1GYKPHRS6MZ210072','3GCUYEEDXMG308118','3GCUYGED6MG394765')


-- lets  refine this query
-- -- tomorrow, too tired tonight
-- 
-- select inpmast_stock_number, inpmast_vin, list_price, c.invoice_number, c.invoice_date, c.vin, c.total_invoice_amount,
--   trim((substring(split_part(c.invoice, 'TOTAL         ', 2), 21, 9)))::numeric as invoice_msrp
-- from arkona.ext_inpmast a
-- left join ( -- join to only the most recent invoid
-- 	select *
-- 	from (
-- 		select a.invoice_number, a.vin, a.invoice_date, a.total_invoice_amount, a.make, a.invoice,
-- 			row_number() over (partition by a.vin order by invoice_date asc) as seq -- first invoice in the period
-- 		from gmgl.wfpm_invoices a
-- 		where a.order_type in ('SRE','TRE','TCS')) b
-- 	-- 		and not exists ( -- exclude vins that have already been paid
-- 	-- 			select 1
-- 	-- 			from gmgl.wfpm_monthly_report
-- 	-- 			where vin = a.vin)
-- 	-- 		and a.invoice_date between '09/21/2021' and '10/20/2021') b
-- 	where seq = 1) c on a.inpmast_vin = c.vin
-- where status = 'I'
--   and a.make not in ('honda','nissan')
--   and type_n_u = 'N'
--   and c.vin is null
--   -- these 6 vins have no invoice in wfpm_invoices, but they each have only a single invoice
--   and a.inpmast_vin not in ('1GCUYDED1MZ384417','1GTU9DEL4MZ244690','1GTU9DEL9MZ244183','1GYKPHRS6MZ210072','3GCUYEEDXMG308118','3GCUYGED6MG394765')


-- all current inventory, joined to the most recent invoice in gmgl.wfpm_invoices
-- returns row where msrp <> wfpm_invoices.msrp or no invoice in wfpm_invoices
select inpmast_stock_number, inpmast_vin, list_price, c.invoice_number, c.invoice_date, c.vin, c.total_invoice_amount,
  trim((substring(split_part(c.invoice, 'TOTAL         ', 2), 21, 9)))::numeric as invoice_msrp, c.seq
from arkona.ext_inpmast a
left join ( -- join to only the most recent invoid
	select *
	from (
		select a.invoice_number, a.vin, a.invoice_date, a.total_invoice_amount, a.make, a.invoice,
			row_number() over (partition by a.vin order by invoice_date desc) as seq -- most recent invoice first
		from gmgl.wfpm_invoices a
		where a.order_type in ('SRE','TRE','TCS')) b
	where seq = 1) c on a.inpmast_vin = c.vin
where status = 'I'
  and a.make not in ('honda','nissan')
  and type_n_u = 'N'
  -- these 6 vins have no invoice in wfpm_invoices, but they each have only a single invoice
  and a.inpmast_vin not in ('1GCUYDED1MZ384417','1GTU9DEL4MZ244690','1GTU9DEL9MZ244183','1GYKPHRS6MZ210072','3GCUYEEDXMG308118','3GCUYGED6MG394765')
  and (
		a.list_price <> trim((substring(split_part(c.invoice, 'TOTAL         ', 2), 21, 9)))::numeric
		or 
		c.vin is null)

