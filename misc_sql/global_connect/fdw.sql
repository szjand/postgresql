﻿jon@ubuntu-1804:~/projects/global_connect_ordering/csv_dir$ scp ext_global_connect_orders.csv rydell@10.130.196.173:/home/rydell/files/


create server global_connect foreign data wrapper file_fdw;

CREATE foreign table ext_ordered_vehicles_csv
(
  row_number citext,
  field_action citext,
  order_number citext,
  trade citext,
  assigned citext,
  gm_config_id citext,
  order_type citext,
  msrp_dfc citext,
  dan citext,
  model citext,
  tpw citext,
  est_delivery_date citext,
  vin citext,
  age_of_inventory citext,
  primary_color citext,
  secondary_color citext,
  trim_value citext,
  changed citext,
  stock_number citext,
  peg citext,
  model_year citext,
  alloc_group citext,
  primary_fan citext,
  end_user_fan citext,
  po_num citext,
  ship_to_bac citext,
  ship_to_bfc citext,
  request_id citext,
  current_event citext,
  current_event_date citext,
  ordered_options citext,
  customer_name citext,
  gm_stored_config_description citext)
server global_connect
options(filename '/home/rydell/files/ext_global_connect_orders.csv', format 'csv');


select * from ext_ordered_vehicles_csv