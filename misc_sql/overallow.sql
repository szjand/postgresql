﻿-- overallow for knudson

-- select deal from (
select distinct a.date_capped, a.vehicle_type as new_used, bopmast_stock_number as deal, c.year, c.make, c.model, 
  b.stock_ as trade, d.year, d.make, d.model, b.trade_allowance::integer, b.acv::integer, b.trade_allowance::integer - b.acv::integer as over_allow
from arkona.ext_bopmast a
join arkona.ext_boptrad b on a.record_key = b.key
  and a.bopmast_company_number = b.company_number
left join arkona.ext_inpmast c on a.bopmast_vin = c.inpmast_vin
  and 
    case
      when a.bopmast_stock_number like 'H%' then c.inpmast_company_number = 'RY1'
      else a.bopmast_company_number = c.inpmast_company_number
    end
left join arkona.ext_inpmast d on b.vin = d.inpmast_vin
  and
    case
      when a.bopmast_stock_number like 'H%' then d.inpmast_company_number = 'RY1'
      else b.company_number = d.inpmast_company_number
    end
where date_capped > '12/31/2022'
  and record_status = 'U'
--   and a.trade_allowance <> a.trade_acv
  and b.trade_allowance::integer - b.acv::integer > 0
order by deal  
--   ) x group by deal having count(*) > 1 order by deal
-- order by a.date_capped 


G44781P
G46326


