﻿do
$$
declare
  _year_month integer := 201712;
begin
  drop table if exists mtd_retail_accounting_count;
  create temp table mtd_retail_accounting_count as
  select a.store_code, 
    case
      when a.page = 16 then 'used'
      else 'new'
    end as new_used, a.control as stock_number,
    a.unit_count, b.bopmast_id, b.vin, b.delivery_date, c.last_name as psc, d.last_name as ssc
  from (
    select store_code, page, line, control, sum(unit_count) as unit_count
    from ( -- h
      select 
        case
          when department = 'Auto Outlet' then department
          else store_code
        end as store_code,    
        d.page, d.line, d.g_l_acct_number, 
        a.control, a.amount,
        case when a.amount < 0 then 1 else -1 end as unit_count
      from fin.fact_gl a   
      inner join dds.dim_date b on a.date_key = b.date_key
        and b.year_month = _year_month
      inner join fin.dim_account c on a.account_key = c.account_key
        and c.account_type = 'Sale'
      inner join ( -- d: fs gm_account page/line/acct description
        select a.fxmpge as page, a.fxmlne::integer as line, b.g_l_acct_number
        from arkona.ext_eisglobal_sypffxmst a
        inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
          and a.fxmcyy = b.factory_financial_year
        where a.fxmcyy = 2017
          and trim(a.fxmcde) = 'GM'
          and coalesce(b. consolidation_grp, 'RY1') <> '3'
          and b.factory_code = 'GM'
          and trim(b.factory_account) <> '331A'
          and b.company_number = 'RY1'
          and b.g_l_acct_number <> ''
          and ( -- retail only
            (a.fxmpge between 5 and 15 and (a.fxmlne between 1 and 19 or a.fxmlne between 25 and 40)) or
            (a.fxmpge = 16 and a.fxmlne between 1 and 5))) d on c.account = d.g_l_acct_number
      inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
        and aa.journal_code in ('VSN','VSU')   
      where a.post_status = 'Y' order by control) h
    group by store_code, page, line, control) a
  left join  sls.ext_bopmast_partial b on a.control = b.stock_number
  left join sls.personnel c on 
    case 
      when a.store_code = 'ry1' then b.primary_sc = c.ry1_id
      when a.store_code = 'ry2' then b.primary_Sc = c.ry2_id
    end
  left join sls.personnel d on 
    case 
      when a.store_code = 'ry1' then b.secondary_sc = d.ry1_id
      when a.store_code = 'ry2' then b.secondary_sc = d.ry2_id
    end;
end
$$;

select *
from mtd_retail_accounting_count;


select store_code, new_used, sum(unit_count)
from mtd_retail_accounting_count
group by store_code, new_used