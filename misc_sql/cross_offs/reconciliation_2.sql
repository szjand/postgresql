﻿--------------------------------------------------------------------------------------------------
-- < base tables 
--------------------------------------------------------------------------------------------------
drop table if exists board_data cascade;
create temp table board_data as
select a.board_id, a.boarded_ts::date as board_date, d.arkona_store_key, c.last_name as boarded_by, b.board_type, b.board_sub_type,
  e.vehicle_type, a.deal_number, a.stock_number, a.vin, 
  case when a.is_deleted then 'DELETED' else null end as deleted, 
  case when a.is_backed_on then 'BACK_ON' else null end as back_on, 
  e.sale_code, vehicle_make
-- select *
from board.sales_board a
left join board.board_types b on a.board_type_key = b.board_type_key
left join nrv.users c on a.boarded_by = c.user_key
left join onedc.stores d on a.store_key = d.store_key
left join (
  select vehicle_type, board_id, vehicle_make, customer_name, sale_code
  from board.daily_board 
  group by vehicle_type, board_id, vehicle_make, customer_name, sale_code) e on a.board_id = e.board_id; 
create index on board_data(stock_number);
create index on board_data(vin);
create index on board_data(board_type);

drop table if exists inpmast cascade;
create temp table inpmast as
select a.inpmast_stock_number, a.inpmast_vin, a.status, a.type_n_u, 
  a.year, a.make, a.model, a.body_style, a.color, a.date_in_invent, a.date_delivered
from arkona.xfm_inpmast a
where a.current_row = true
  and a.inpmast_Stock_number not in ('31560B', '31615CC','30812XXA','31122A','32154AA'); -- motorcycles
create index on inpmast(inpmast_stock_number);
create index on inpmast(inpmast_vin);

-- all evals is needed for is status on acquisitions that have not yet been booked into 
-- the tool, expose incomplete evals
drop table if exists evals cascade;
create temp table evals as
select b.vin, a.vehicleevaluationts::date as eval_date, -- a.vehicleevaluationts as eval_ts, 
  substring(a.currentstatus, position('_' in a.currentstatus) + 1, 12) as eval_status
from ads.ext_vehicle_evaluations a
inner join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
-- left join ads.ext_vehicle_inventory_items d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
where a.currentstatus <> 'VehicleEvaluation_Booked' 
  and a.vehicleevaluationts::date > current_date - 180;
create unique index on evals(vin, eval_date);


drop table if exists acqs cascade;
create temp table acqs as
select b.vin, a.fromts::date, substring(c.typ, position('_' in c.typ) + 1, 20) as source, a.stocknumber
from ads.ext_Vehicle_inventory_items a
inner join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
inner join ads.ext_Vehicle_acquisitions c on a.vehicleinventoryitemid = c.vehicleinventoryitemid;
create index on acqs(vin);
create index on acqs(stocknumber);

-- select *
-- from ads.ext_Vehicle_inventory_items a
-- where not exists (
--  select 1
--  from ads.ext_vehicle_acquisitions b
--  where b.vehicleinventoryitemid = a.vehicleinventoryitemid)
--------------------------------------------------------------------------------------------------
-- /> base tables 
--------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------
-- < tests
--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------
--< 1. BOARD: bad stock number in board data: inventoried vehicles board_data matches vin & date, different stock number
--------------------------------------------------------------------------------------------------------
-- drop table if exists board.trade_stock_number_corrections;
-- create table board.trade_stock_number_corrections (
--   board_id integer not null primary key,
--   vin citext not null,
--   inpmast_stock_number citext not null,
--   stock_number citext not null,
--   board_type citext not null, 
--   board_sub_type citext not null,
--   boarded_by citext not null,
--   board_date date not null, 
--   date_in_inventory date not null);  
-- comment on table board.trade_stock_number_corrections is 'a log of stock numbers corrected in board.sales_board';  

-- alter table board.trade_stock_number_corrections
-- add column date_corrected date;
-- update board.trade_stock_number_corrections
-- set date_corrected = board_date + 1;

insert into board.trade_stock_number_corrections
select 
  b.board_id, a.inpmast_vin, a.inpmast_Stock_number, b.stock_number, b.board_type, b.board_sub_type,
  b.boarded_by, b.board_date, (select arkona.db2_integer_to_date_long(a.date_in_invent)) as inp_date_in_inv, current_date
from inpmast a
left join (
  select a.board_id, a.boarded_ts::date as board_date, c.last_name as boarded_by, b.board_type, b.board_sub_type,
    a.stock_number, a.vin
  from board.sales_board a
  left join board.board_types b on a.board_type_key = b.board_type_key
  left join nrv.users c on a.boarded_by = c.user_key) b on a.inpmast_vin = b.vin
where a.status = 'I'
  and a.inpmast_Stock_number not in ('31560B', '31615CC') -- motorcycles
  and a.inpmast_stock_number not in ('32835X') -- intra market ws was never boarded
  and a.date_in_invent > 20171100
  and a.inpmast_stock_number <> b.stock_number
  and abs((select arkona.db2_integer_to_date_long(a.date_in_invent)) - b.board_date) < 4;


update board.sales_board z
set stock_number = x.inpmast_stock_number
from (
  select *
  from board.trade_stock_number_corrections) x
where z.board_id = x.board_id;

-- select * from board.trade_stock_number_corrections
select * from board.trade_stock_number_corrections
select * from board_data where vin = '1GTHK63K89F136772'
--------------------------------------------------------------------------------------------------------
--/> 1. BOARD: bad stock number in board data: inventoried vehicles board_data matches vin & date, different stock number
--------------------------------------------------------------------------------------------------------

select * from board_data where board_date = current_date - 1 order by boarded_by

select * from board_data  where stock_number = '32417l'
--------------------------------------------------------------------------------------------------------
--< 2. TOOL: inventoried used vehicles not in tool (in inpmast, not in tool)
--------------------------------------------------------------------------------------------------------
select inpmast_stock_number as stock_number, inpmast_vin as vin, year, make, model,
  arkona.db2_integer_to_date_long(date_in_invent) as date_acquired, d.*
-- select *
from inpmast a
left join acqs b on a.inpmast_stock_number = b.stocknumber
left join board_data c on a.inpmast_Stock_number = c.stock_number
left join evals d on a.inpmast_vin = d.vin
where a.status = 'I'
  and a.type_n_u = 'U'
  and b.vin is null
  and a.inpmast_Stock_number not in ('31560B', '31615CC') -- motorcycles
  and d.vin is null -- no eval last 90 days
order by date_in_invent;

/*
update ads data
13 tables: make_model_classifications,vehicle_inventory_items,vehicle_items,vehicle_pricing_details,vehicle_pricings
           vehicle_inventory_item_statuses,vehicle_item_mileages, vehicle_sales, vehicle_evaluations, 
           vehicle_acquisitions, dim_sales_person, people, vehicleitemstatusees
 
select *
-- delete
from ops.ads_extract
where the_date = current_date
  and task like 'uc%';
  
scheduled task to populate tables:   
  10.130.196.22\luigi\ads_for_luigi_uc_inventory
*/

--------------------------------------------------------------------------------------------------------
--/> 2. TOOL: inventoried used vehicles not in tool (in inpmast, not in tool)
--------------------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------------------
--< 3. TOOL: inpmast sold vehicles not sold in tool
--------------------------------------------------------------------------------------------------------
-- status RawMaterials_Sold: when thruts > now() status is sold and not delivered
-- believe i should be looking for the presence of the the status RawMaterials_Sold
-- 12/27, 31464A outlet, not capped yet, not included in email
select a.inpmast_Stock_number, inpmast_vin, year, make, model,
  case 
    when d.sale_type_code = 'r' then 'retail'
    when d.sale_type_code = 'w' then 'wholesale'
    else 'xxx'
  end 
  || ' to ' || replace(e.fullname, ',', '')
  || ' on ' || extract(month from arkona.db2_integer_to_date_long(a.date_delivered))::text
  || '-' || extract(day from arkona.db2_integer_to_date_long(a.date_delivered))::text
-- select a.*, e.fullname,d.*
from inpmast a
left join ads.ext_vehicle_inventory_items b on a.inpmast_stock_number = b.stocknumber
left join ads.ext_vehicle_inventory_item_statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
  and c.status = 'RawMaterials_Sold'
left join ( -- 32094a, this does ot pick up the post unwind sale, new bopmast_id, new sequence, try max(run_date)
--   select aa.stock_number, aa.sale_type_code, aa.buyer_bopname_id 
--   from sls.deals aa
--   inner join (
--     select max(cc.seq) as seq, cc.stock_number
--     from sls.deals cc
--     group by cc.stock_number) bb on aa.stock_number = bb.stock_number
--       and aa.seq = bb.seq) d on a.inpmast_stock_number = d.stock_number
  select aa.stock_number, aa.sale_type_code, aa.buyer_bopname_id 
  from sls.deals aa
  inner join (
    select max(cc.run_date) as run_date, cc.stock_number
    from sls.deals cc
    group by cc.stock_number) bb on aa.stock_number = bb.stock_number
      and aa.run_date = bb.run_date) d on a.inpmast_stock_number = d.stock_number
left join ads.ext_dim_customer e on d.buyer_bopname_id = e.bnkey
where a.type_n_u = 'U'
  and a.status = 'C'
  and a.date_delivered between 20180000 and 20190400
  and c.status is null
  and a.inpmast_stock_number not in ('29704B','H10186A','H9191C') -- misc outliers to ignore
  and a.inpmast_stock_number not in ('30057','30637') -- miscategorized new vehicles
  and a.inpmast_stock_number not in ('30710RB','31527A','32271X') -- not in tool at all
order by a.inpmast_stock_number

--------------------------------------------------------------------------------------------------------
--/> 3. TOOL: inpmast sold vehicles not sold in tool
--------------------------------------------------------------------------------------------------------  

--------------------------------------------------------------------------------------------------------
--< 4. BOARD: inventoried used vehicles not boarded, in inpmast as status I, not on board
--------------------------------------------------------------------------------------------------------  
select a.inpmast_stock_number as stock_number, inpmast_vin as vin, year, make, model, 
  arkona.db2_integer_to_date_long(a.date_in_invent) as date_acquired
-- select a.*, b.board_id, c.*
from inpmast a
left join board_data b on a.inpmast_Stock_number = b.stock_number or a.inpmast_vin = b.vin
left join acqs c on a.inpmast_stock_number = c.stocknumber
where a.status = 'I'
  and a.type_n_u = 'U'
  and a.inpmast_Stock_number not in ('31560B', '31615CC') -- motorcycles
  and a.date_in_invent > 20171200
  and b.board_id is null
order by date_in_invent  

--------------------------------------------------------------------------------------------------------
--/> 4. BOARD: inventoried used vehicles not boarded
--------------------------------------------------------------------------------------------------------  

--------------------------------------------------------------------------------------------------------
--< 5. BOARD: wholesale sales not on board
--------------------------------------------------------------------------------------------------------  
select *
from (
  select b.the_date, a.*, c.account, d.journal_code, e.description
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month >201800
  inner join fin.dim_Account c on a.account_key = c.account_key
    and c.account in ('145200','144800','244800','245200')
  inner join fin.dim_journal d on a.journal_key = d.journal_key
    and d.journal_code in ('VSN','VSU')  
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key) f   
left join board_data e on f.control = e.stock_number
  and e.board_type = 'deal'
--   and e.board_sub_type in ('wholesale','Intercompany Wholesale')
  and e.deleted is null
where board_id is null  
order by control  

--------------------------------------------------------------------------------------------------------
--/> 5. BOARD: inventoried used vehicles not boarded
--------------------------------------------------------------------------------------------------------  

--------------------------------------------------------------------------------------------------------
--< 6. BOARD: retail sales not on board
--------------------------------------------------------------------------------------------------------  
select *
from sls.deals
where year_month = 201712

select *
from sls.deals_by_month
where year_month = 201712

next step
use the statement, which lists everything except dealer trades
left join sls.deals (or deals_by_month)
--------------------------------------------------------------------------------------------------------
--/> 6. BOARD: inventoried used vehicles not boarded
--------------------------------------------------------------------------------------------------------  

--------------------------------------------------------------------------------------------------------
--</ 7. BOARD: fleet, internal, dealertrade sales not boarded
--------------------------------------------------------------------------------------------------------  

--------------------------------------------------------------------------------------------------------
--</ 8. BOARD: daily count differences
--------------------------------------------------------------------------------------------------------  
--------------------------------------------------------------------------------------------------
-- /> tests
--------------------------------------------------------------------------------------------------  

-- sold in inpmast, not boarded
select a.inpmast_stock_number, inpmast_vin, year, make, model, arkona.db2_integer_to_date_long(a.date_in_invent)
-- select a.*, b.board_id, c.*
from inpmast a
left join board_data b on a.inpmast_Stock_number = b.stock_number or a.inpmast_vin = b.vin
left join acqs c on a.inpmast_stock_number = c.stocknumber
where a.status = 'C'
--   and a.type_n_u = 'U'
  and a.inpmast_Stock_number not in ('31560B', '31615CC') -- motorcycles
  and a.date_delivered between 20171200 and 20171231
  and b.board_id is null

select * from board_data

-- sold in sls.deals, not sold in board

select * -- 151
from sls.deals_by_month a
where year_month = 201712
  and stock_number = '32058a'


select a.stock_number, a.bopmast_id, a.vin, a.sale_type, a.buyer, b.*
from sls.deals_by_month a
left join board_Data b on a.stock_number = b.stock_number
  and b.board_date > '11/30/2017'
  and b.board_type = 'deal'
--   and b.deleted is null  
where a.stock_number = '32058A' -- year_month = 201712
  and b.stock_number is null
  

select * 
from board_data
where stock_number in ('32094a','32003a')

-- board_data with back_ons
select a.*, b.board_date, b.board_type
from board_data a
left join board_data b on a.stock_number = b.stock_number
  and b.board_date > a.board_date
where a.board_date > '11/30/2017'
--   and a.arkona_store_key = 'RY3'
  and a.board_type = 'deal'
--   and a.stock_number = '32058a'
order by a.stock_number  


-- this matches michelle's wholesale to honda missing from board
select *
from (
  select b.the_date, a.*, c.account, d.journal_code, e.description
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201712
  inner join fin.dim_Account c on a.account_key = c.account_key
    and c.account in ('145200','144800','244800','245200')
  inner join fin.dim_journal d on a.journal_key = d.journal_key
    and d.journal_code in ('VSN','VSU')  
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key) f   
left join board_data e on f.control = e.stock_number
  and e.board_type = 'deal'
--   and e.board_sub_type in ('wholesale','Intercompany Wholesale')
  and e.deleted is null
where board_id is null  
order by control  


select * from board_Data where vin like '%317144'


select * from board_data where stock_number in ('31091A','32208B','32226A','32385XXA')


select * from board.sales_board where stock_number = '32527a'
















-- inventoried used vehicles not booked
select *
from inpmast a
left join acqs b on a.inpmast_stock_number = b.stocknumber
left join board_data c on a.inpmast_Stock_number = c.stock_number
left join evals d on a.inpmast_vin = d.vin
where a.status = 'I'
  and a.type_n_u = 'U'
  and b.vin is null
  and a.inpmast_Stock_number not in ('31560B', '31615CC') -- motorcycles
  and d.vin is not null -- no eval last 90 days

/* inpmast vs board */
-- inventoried new vehicles not boarded
-- new cars, don't know if this is useful, now way to distinguish which new cars were
-- acquired from dealer trades, and normal new car inventory is not boarded
select *
from inpmast a
left join board_data b on a.inpmast_Stock_number = b.stock_number or a.inpmast_vin = b.vin
where a.status = 'I'
  and a.type_n_u = 'N'
  and a.inpmast_Stock_number not in ('31560B', '31615CC') -- motorcycles
  and a.date_in_invent > 20171100
  and b.board_id is null







select *
from ads.ext_Vehicle_inventory_items
where vehicleitemid in (
  select vehicleitemid
  from ads.ext_vehicle_items
  where vin = '1C4RJFBG1EC482064')
  


-- issues -------------------------
-- this seems like it should be all the acquisition data, but where are the purchases?
select *
from board_data
where board_date > '10/31/2017'
  and vehicle_type = 'U'
  and board_type = 'Addition'


select * from board_Data where stock_number = '32494'


select arkona_store_key, board_type, board_sub_type, vehicle_make, vehicle_type, count(*)
-- select *
from board_data
where board_Date > '11/30/2017'
group by arkona_store_key, board_type, board_sub_type, vehicle_make, vehicle_type
order by arkona_store_key, board_type, board_sub_type


select arkona_store_key, board_type, board_sub_type, 
  case 
    when vehicle_make in ('Chevrolet','Buick','Cadillac','GMC','Honda','Nissan') and vehicle_type = 'N' then vehicle_make
    else 'USED'
  end, vehicle_type, count(*)
-- select *
from board_data
where board_Date > '11/30/2017'
group by arkona_store_key, board_type, board_sub_type, 
  case 
    when vehicle_make in ('Chevrolet','Buick','Cadillac','GMC','Honda','Nissan') and vehicle_type = 'N' then vehicle_make
    else 'USED'
  end, vehicle_type
order by arkona_store_key,   case 
    when vehicle_make in ('Chevrolet','Buick','Cadillac','GMC','Honda','Nissan') and vehicle_type = 'N' then vehicle_make
    else 'USED'
  end

select board_type, board_sub_type,
  count(*) filter (where board_date < '12/01/2017' and arkona_store_key = 'RY1') as ry1_nov,
  count(*) filter (where board_date < '12/01/2017' and arkona_store_key = 'RY2') as ry2_nov,
  count(*) filter (where board_date < '12/01/2017' and arkona_store_key = 'RY3') as ry3_nov,
  count(*) filter (where board_date >= '12/01/2017' and arkona_store_key = 'ry1') as ry1_dec,
  count(*) filter (where board_date >= '12/01/2017' and arkona_store_key = 'ry2') as ry2_dec,
  count(*) filter (where board_date >= '12/01/2017' and arkona_store_key = 'ry3') as ry3_dec,
  count(*) filter (where arkona_store_key in ('ry1','ry2', 'ry3')) as total
from board_data 
group by board_type, board_sub_type  
order by board_type, board_sub_type

--- michelle -----------------------------
select *
from board_data
where stock_number in ('32226a','32058a','31956pb','32094a','31091a','32385xxa')
order by stock_number, board_date


select *
from sls.deals
where stock_number = '32058a'


select *
from sls.deals_by_month
where stock_number = '32058a'


select *
from board.sales_board
where stock_number = '32094A'



select * from board_data where stock_number = '32526'

select * from arkona.xfm_inpmast where status = 'I' and type_n_u = 'N' and current_row = true

select * from board.board_types

select * from board_data where board_sub_type = 'fleet'


select sale_type_code, count(*)
from sls.deals
-- where stock_number = '32117'
group by sale_type_code


select *
from sls.deals a
inner join (
  select bopmast_id
  from sls.deals
  group by bopmast_id
  having count(*) > 1) b on a.bopmast_id = b.bopmast_id
order by a.bopmast_id, a.run_Date  


select bopmast_id
from (
select bopmast_id, sale_type_code
from sls.deals
group by bopmast_id, sale_type_code) a
group by bopmast_id having count(*) > 1


select *
from sls.xfm_deals
where stock_number in ('32526','32512','32527')

select c.the_date, b.account, b.account_type, a.*
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
inner join dds.dim_date c on a.date_key = c.date_key
where a.control = '32512'
order by account_type, account



select * from board_data where stock_number = '32740l'

select *
from board.sales_board a
left join board.daily_board b on a.board_id = b.board_id
where stock_number = '32740l'

update board.daily_board
set vehicle_type = 'U'
where board_id = 1170



select *
from board_data
where board_date >= '12/01/2017'
order by arkona_store_key, board_type, board_sub_type

select *
from board_data
where board_sub_type = 'courtesy delivery'

select *
from sls.deals_by_month
where year_month = 201712


-- courtesy delivery
-- they are given a stock number but never in inventory,
-- no entries in inventory or sale acccounts
-- best i can do is a row in bopvref where customer_key = 1122815 (courtesy delivery in bopname)
select b.the_date,a.trans, a.seq, a.control, a.doc, a.ref, f.doc_type, a.amount, c.account, c.account_type, d.journal_code, e.description, f.*
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
inner join fin.dim_doc_type f on a.doc_type_key = f.doc_type_key
where a.post_status = 'Y'
  and control in ('32605','32420','32535')

select *
from arkona.xfm_inpmast a
left join arkona.ext_inpcmnt b on a.inpmast_vin = b.vin
where a.inpmast_stock_number in ('32605','32420','32535')
  and a.current_row = true

select

select * from board_data
where stock_number = 'H10845L'



-- board sale_group does not equal current inpmast bus_off_fran_code
select a.*, c.bus_off_fran_code
from board_data a
-- inner join sls.sale_groups b on a.sale_code = b.code and a.arkona_store_key = b.store_code
inner join arkona.xfm_inpmast c on a.stock_number = c.inpmast_stock_number
  and c.current_row = true
where a.sale_code <> c.bus_off_Fran_code
  and board_date >= '12/01/2017'
  and a.arkona_Store_key <> 'ry3'
  and a.deleted is null
  
-- but board vehicle_type is equal to current inpmast bus_off_fran_code decoded to new/used
select a.*, c.bus_off_fran_code
from board_data a
-- inner join sls.sale_groups b on a.sale_code = b.code and a.arkona_store_key = b.store_code
inner join arkona.xfm_inpmast c on a.stock_number = c.inpmast_stock_number
  and c.current_row = true
inner join sls.sale_Groups d on c.bus_off_fran_code = d.sale_group and a.arkona_store_key = d.store_code  
where a.vehicle_type <> d.new_used
  and board_date >= '12/01/2017'
  and a.arkona_Store_key <> 'ry3'
  and a.deleted is null  



-- correlation between deal_type and sale_group

select a.store_code, 
  case a.sale_type_code
    when 'R' then 'retail'
    when 'L' then 'lease'
    when 'X' then 'dlr_xfr'
    when 'F' then 'fleet'
    when 'W' then 'whlsl'
    else 'unknown'
  end as sale_type, b.sale_group, count(*)
from sls.deals a
left join sls.sale_Groups b on a.sale_group_code = b.code
where a.year_month = 201712
  and a.store_code = 'ry1'
group by a.store_code, 
  case a.sale_type_code
    when 'R' then 'retail'
    when 'L' then 'lease'
    when 'X' then 'dlr_xfr'
    when 'F' then 'fleet'
    when 'W' then 'whlsl'
    else 'unknown'
  end, b.sale_group  



