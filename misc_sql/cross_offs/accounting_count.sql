﻿drop table if exists step_1;
drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select store, page, line, line_label, control, sum(unit_count) as unit_count
from ( -- h
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a   
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201711
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201711
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
    and aa.journal_code in ('VSN','VSU')   
    where a.post_status = 'Y' order by control) h
group by store, page, line, line_label, control
order by store, page, line;


select a.*, sum(the_count) over (partition by store, page) as page_count
from (
select store, page, line, sum(unit_count) as the_count
from step_1
group by store, page, line)a
order by store, page, line


what i want is to be able to match this from fin.fact_gl for a month in process

-- from fin_data_mart/sql/ben_and_greg_fin_history_project/accounts_lines_gross.sql
drop table if exists accounts_lines cascade;
create temp table accounts_lines as
-- from sales_consultant_payroll.py AccountsRoutes
-- accounts/page/line/store/year
select distinct a.fxmcyy as the_year,
  case coalesce(b.consolidation_grp, '1') when '2' then 'RY2' else 'RY1' end::citext as store_code,
  a.fxmpge as page, a.fxmlne::integer as line, trim(b.g_l_acct_number) as gl_account
from arkona.ext_eisglobal_sypffxmst a
inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
where a.fxmcyy between 2012 and 2017
  and trim(a.fxmcde) = 'GM'
  and coalesce(b.consolidation_grp, '1') <> '3'
  and b.factory_code = 'GM'
  and trim(b.factory_account) <> '331A'
  and b.company_number = 'RY1'
  and b.g_l_acct_number <> ''
  and (
    (a.fxmpge = 3 and a.fxmlne = 67) or -- doc fees
    (a.fxmpge between 5 and 15) or
    (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
    (a.fxmpge = 17 and a.fxmlne between 1 and 19));
create unique index on accounts_lines(the_year, store_code, gl_account);   
create index on accounts_lines(the_year);
create index on accounts_lines(gl_account);

select * from accounts_lines

-- from luigi/sales_consultant_payroll.py
-- populates sls.deals_accounts_routes
                    select distinct case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code,
                      a.fxmpge as the_page, INTEGER(a.fxmlne) as line, trim(b.g_l_acct_number) as gl_account
                    from eisglobal.sypffxmst a
                    inner join rydedata.ffpxrefdta b on a.fxmact = b.factory_account
                      and a.fxmcyy = b.factory_financial_year
                    where a.fxmcyy = 2017
                      and trim(a.fxmcde) = 'GM'
                      and b. consolidation_grp <> '3'
                      and b.factory_code = 'GM'
                      and trim(b.factory_account) <> '331A'
                      and b.company_number = 'RY1'
                      and b.g_l_acct_number <> ''
                      and (
                        (a.fxmpge between 5 and 15) or
                        (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
                        -- exclude fin comp & chargebacks
                        (a.fxmpge = 17 and a.fxmlne in (1,2,6,7,11,12,16,17)))


                        
select *
from sls.deals_accounts_routes


accounts_lines looks good, need to add label

select a.the_year, a.store_code, a.page, a.line, a.gl_account, 
  b.line_label, c.department_code, c.account_type, c.description,
  case
    when a.store_code = 'RY1' and a.page = 5 then 'Chevrolet'
    when a.store_code = 'RY1' and a.page = 8 then 'Buick'
    when a.store_code = 'RY1' and a.page = 9 then 'Cadillac'
    when a.store_code = 'RY1' and a.page = 10 then 'GMC'
    when a.store_code = 'RY2' and a.page = 7 then 'Honda'
    when a.store_code = 'RY2' and a.page = 14 then 'Nissan'
  end as make
from accounts_lines a
left join (
  select the_year, page, line, line_label
  from fin.dim_Fs
  group by the_year, page, line, line_label) b on a.the_year = b.the_year and a.page = b.page and a.line = b.line
left join fin.dim_account c on a.gl_account = c.account  
  and c.current_row = true
where a.the_year = 2017
  and a.page > 3 -- don't need doc fees for this
order by a.store_code, a.page, a.line

select *
from accounts_lines a
inn

-- generate count from accounts_lines/fact_gl
-- select a.control, a.amount, c.account, d.*
select e.*, sum(unit_count) over (partition by account)
from (
select c.store_code, d.page, d.line, a.control,  c.account,
  sum(case when a.amount < 0 then 1 else -1 end) as unit_count
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join accounts_lines d on c.account = d.gl_account
where a.post_status = 'Y'
  and b.year_month = 201711
  and d.the_year = 2017
  and d.page between 5 and 15
  and c.account_type = 'Sale'
group by c.store_code, d.page, d.line, a.control, c.account) e
order by store_code, page, line, account, control

-- check detail for goofy result from count
select b.the_date, a.*, d.*
-- select *
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join accounts_lines d on c.account = d.gl_account
where a.post_status = 'Y'
  and b.year_month = 201711
  and d.the_year = 2017
  and d.page between 5 and 15
  and c.account_type = 'Sale'
  and a.control = '32011'


select * from fin.dim_fs_account limit 1000

select * from fin.dim_fs limit 1000

pages 5-15
  line 1 - 19: car retail
  line 22: car fleet
  line 23: car internal
  line 25 - 40 truck retail
  line 43: truck fleet
  line 44: truck internal
  line 47: aftermarket
page 16
  line 1: retail car certified
  line 2: retail car other
  line 4: retail truck certified
  line 5: retail truck other
  line 8: whlsl car
  line 10: whlsl truck
page 17 (the overlap in lines is different columns)
  line 3: new charge back
  line 13: used charge back
  line 9: new comp
  line 19: used comp
  line 1 - 8: fin income
  line 11 - 18: fin income



select bopmast_id, deal_status_code, vehicle_type_code, sale_type_code, 
  sale_group_code, stock_number, unit_count,
  count(sale_group_code) over (partition by sale_group_code)
from sls.deals where year_month = 201711 
  and store_code = 'RY1'
order by sale_group_code  

-- dealer trade 
-- 32494 acquired by dealer trade, subsequently sold
-- 31830 sold by dealer trade, no sale_account transaction
select b.the_Date, a.*, c.account, c.account_type, c.description, c.department_code, d.journal_code, e.description,
  sum(amount) over (partition by account)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account_type in ('Asset','Sale')
  and c.department_code = 'nc'
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_Description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and a.control in ('32494', '31830')
order by control, account



select * from arkona.xfm_inpmast where inpmast_stock_number = '32015'

select * from arkona.xfm_inpmast where inpmast_vin = '3GCUKREC2JG151647'

select * from arkona.ext_inpcmnt where vin = '3GCUKREC2JG151647'

-- drop table if exists board_data cascade;
-- create temp table board_data as
-- select a.board_id, a.boarded_ts::date as board_date, d.arkona_store_key, c.last_name as boarded_by, b.board_type, b.board_sub_type,
--   e.vehicle_type, a.deal_number, a.stock_number, a.vin, 
--   case when a.is_deleted then 'DELETED' else null end as deleted, 
--   case when a.is_backed_on then 'BACK_ON' else null end as back_on, 
--   e.sale_code
-- -- select *
-- from board.sales_board a
-- left join board.board_types b on a.board_type_key = b.board_type_key
-- left join nrv.users c on a.boarded_by = c.user_key
-- left join onedc.stores d on a.store_key = d.store_key
-- left join board.daily_board e on a.board_id = e.board_id;  
-- create index on board_data(stock_number);
-- create index on board_data(vin);
-- create index on board_data(board_type);

select *
from board_data a
left join arkona.xfm_inpmast b on a.stock_number = b.inpmast_stock_number
  and b.current_row = true
where a.board_sub_type = 'dealer trade'
order by a.vin

---------------------------------------------------------------------------------------------------------------------------
-- 12/8 convert step_1 to work without fact_fs
---------------------------------------------------------------------------------------------------------------------------

drop table if exists step_2;
create temp table step_2 as
select store::citext, page, line, control, department_code, g_l_acct_number, sum(unit_count) as unit_count
from ( -- h
  select d.store, d.page, d.line, d.g_l_acct_number, 
    a.control, a.amount, c.department_code, 
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a   
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201712
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account_type = 'Sale'
  inner join ( -- d: fs gm_account page/line/acct description
    select 
      case
        when b.consolidation_grp is null then 'RY1'
        when b.consolidation_grp = '2' then 'RY2'
      end as store,
      a.fxmpge as page, a.fxmlne::integer as line, b.g_l_acct_number
    from arkona.ext_eisglobal_sypffxmst a
    inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
      and a.fxmcyy = b.factory_financial_year
    where a.fxmcyy = 2017
      and trim(a.fxmcde) = 'GM'
      and coalesce(b. consolidation_grp, 'RY1') <> '3'
      and b.factory_code = 'GM'
      and trim(b.factory_account) <> '331A'
      and b.company_number = 'RY1'
      and b.g_l_acct_number <> ''
      and (
        (a.fxmpge between 5 and 15) or
        (a.fxmpge = 16 and a.fxmlne between 1 and 14))) d on c.account = d.g_l_acct_number
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
    and aa.journal_code in ('VSN','VSU')   
  where a.post_status = 'Y' order by control) h
group by store, page, line, control, department_code, g_l_acct_number
order by store, page, line;

select store, page, line, sum(unit_count) from step_2 group by store, page, line
order by store, page, line

select b.line_label, a.* ,
  sum(unit_count) over (partition by a.page, b.line_label) as by_page_line_label
from step_2 a
left join (
  select page, line::integer, line_label
  from fin.dim_Fs
  where (page in (5,8,9,10) or (page = 16 and line in (1,2,4,5,8,10)))
    and line_label is not null 
    and the_year = 2017
    and line_label <> 'do not use'
  group by page, line, line_label) b on a.page = b.page and a.line = b.line 
where store = 'ry1'
order by a.page, a.line

  

select sum
-- matches for november total
select * from (
select a.*, sum(the_count) over (partition by store, page) as page_count
from (
select store, page::integer, line::integer, sum(unit_count) as the_count
from step_1
group by store, page, line) a) x
full outer join (
select a.*, sum(the_count) over (partition by store, page) as page_count
from (
select store, page, line, sum(unit_count) as the_count
from step_2
group by store, page, line) a) y on x.store = y.store and x.page = y.page and x.line = y.line


select * 
from step_2 a
left join sls.deals b on a.control = b.stock_number
where b.delivery_date between '12/01/2017' and '12/10/2017'

-- add the date, do a daily count
-- what are the categories to group to 
-- ?? dealer trades don't show on fs? - maybe inventory
-- this is sales only but should include wholesale !!!!!!!!!
-- shit need to break out outlet, generate store from dim_account, not ffpxrefdta
-- hmmm, add some categorization, 
drop table if exists dec;
create temp table dec as
select store::citext, 
  page, line, control, the_date, sum(unit_count) as unit_count
from ( -- h
  select 
    case
      when department = 'Auto Outlet' then department
      else store_code
    end as store,
    d.page, d.line, d.g_l_acct_number, 
    a.control, aa.the_date, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a   
  inner join dds.dim_date aa on a.date_key = aa.date_key
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201712
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account_type = 'Sale'
  inner join ( -- d: fs gm_account page/line
    select a.fxmpge as page, a.fxmlne::integer as line, b.g_l_acct_number
    from arkona.ext_eisglobal_sypffxmst a
    inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
      and a.fxmcyy = b.factory_financial_year
    where a.fxmcyy = 2017
      and trim(a.fxmcde) = 'GM'
      and coalesce(b. consolidation_grp, 'RY1') <> '3'
      and b.factory_code = 'GM'
      and trim(b.factory_account) <> '331A'
      and b.company_number = 'RY1'
      and b.g_l_acct_number <> ''
      and (
        (a.fxmpge between 5 and 15) or
        (a.fxmpge = 16 and a.fxmlne between 1 and 14))) d on c.account = d.g_l_acct_number
  inner join fin.dim_journal e on a.journal_key = e.journal_key 
    and e.journal_code in ('VSN','VSU')   
  where a.post_status = 'Y' order by control) h
group by store, page, line, control, the_date
order by store, page, line;


-- hmmm, add some categorization, 
select store, make, deal_type, new_used, shape, sum(unit_count)
from (
select *,
  case page when 16 then 'used' else 'new' end as new_used,
  case 
    when ((page = 16 and line in (1,2)) or (page between 5 and 15 and line between 1 and 23)) then 'car' 
    else 'truck'
  end as shape,
  case
    when page = 5 then 'Chevrolet'
    when page = 8 then 'Buick'
    when page = 9 then 'Cadillac'
    when page = 10 then 'GMC'
    when page = 7 then 'Honda'
    when page = 14 then 'Nissan'
  end as make,
  case
    when page between 5 and 15 and (line between 1 and 19 or line between 25 and 40) then 'retail'
    when page = 16 and line between 1 and 5 then 'retail'
    when page = 16 and line between 8 and 10 then 'wholesale'
    when line in (22, 43)   then 'fleet'
    when line in (23, 44) then 'internal'
  end as deal_type
from dec
where the_date > '11/30/2017') x
group by store, deal_type, new_used, shape, make
order by store, make, deal_type, shape

select * from dec order by unit_count

select * from dec where store = 'auto outlet' and the_date > '11/30/2017'

select * from sls.deals where stock_number = '29700b'


select * from board_Data where stock_number = '32253'

pages 5-15
  line 1 - 19: car retail
  line 22: car fleet
  line 23: car internal
  line 25 - 40 truck retail
  line 43: truck fleet
  line 44: truck internal
  line 47: aftermarket
page 16
  line 1: retail car certified
  line 2: retail car other
  line 4: retail truck certified
  line 5: retail truck other
  line 8: whlsl car
  line 10: whlsl truck

-- 12/9 count comparisons
-- don't yet know how to verify additions in accounting
outlet: board shows 11, acct = 12
  acct does not show the latest back on of 29700B
                              board    acct
                      page    query    query
outlet deal retail     11       12       13
outlet add  trade       2        2           

select *
from dec a
where exists (
  select 1
  from dec 
  where control = a.control
    and unit_count < 0)

select * from dec where control = '29700b'    

select * from dec where page = 8

select * from fin.dim_account where account = '1406004'

    select a.fxmpge as page, a.fxmlne::integer as line, b.g_l_acct_number
    from arkona.ext_eisglobal_sypffxmst a
    inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
      and a.fxmcyy = b.factory_financial_year
    where a.fxmcyy = 2017
      and trim(a.fxmcde) = 'GM'
      and coalesce(b. consolidation_grp, 'RY1') <> '3'
      and b.factory_code = 'GM'
      and trim(b.factory_account) <> '331A'
      and b.company_number = 'RY1'
      and b.g_l_acct_number <> ''
      and a.fxmpge = 8
order by g_l_acct_number     

select * from arkona.ext_ffpxrefdta where g_l_acct_number in('1406004','1425004')

select * from arkona.ext_ffpxrefdta where g_l_acct_number in('1606004','1406004')

select * from arkona.ext_ffpxrefdta where factory_account in('406D','606D')

select * from fin.dim_account where account in ('1406104', '1406004')

select * from arkona.ext_ffpxrefdta where g_l_acct_number in('1406104', '1406004')


-- intramarket wholesales

select * from dec

 
---------------------------------------------------------------------------------------------------------------------------
-- 12/8 convert step_1 to work without fact_fs
---------------------------------------------------------------------------------------------------------------------------

-- inventory
select *
from fin.fact_gl a
inner join fin.dim_Account b on a.account_key = b.account_key
where control = '31064b'


select control, account_type, sum(amount)
from fin.fact_gl a
inner join fin.dim_Account b on a.account_key = b.account_key
where control in ( '31064b','31567B', '31625a')
group by control, account_type
order by control

select control, sum(amount)
from fin.fact_gl a
inner join fin.dim_Account b on a.account_key = b.account_key
where control in ( '31064b','31567B', '31625a')
group by control


select type_n_u, bus_off_fran_code, year, make, model, inpmast_sale_account, inventory_account
from arkona.xfm_inpmast
where current_row = true
  and status = 'I'
group by type_n_u, bus_off_fran_code, year, make, model, inpmast_sale_account, inventory_account

-- inventory acccounts?
select *
from fin.dim_account
where account_type = 'Asset'
  and department_code in ('NC','UC')
  and description like '%INV%'

/*
looks like trades shit i don't know how to interpret, 31384XXA
*/
select b.the_date, a.*, c.account, c.description, d.journal_code
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join (
  select *
  from fin.dim_account
  where account_type = 'Asset'
    and department_code in ('NC','UC')
    and description like '%INV%') c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key    
where b.the_date > '11/30/2017'
-- and control = '31384XXA'
order by control  

-- fs inventory
-- inventory acccounts?
select *
from fin.dim_account
where account_type = 'Asset'
  and department_code in ('NC','UC')
  and description like '%INV%'

life cycle of a new vehicle  

select control, b.the_date, a.amount, c.account, c.department_code, c.account_type, c.description, d.journal_code, e.description
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where post_status = 'Y'
  and control in ( '32159', '31728a')
order by a.control, b.the_date


select control, b.the_date, a.amount, c.account, c.department_code, c.account_type, c.description, d.journal_code, e.description
-- select sum(amount)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where post_status = 'Y'
  and control in ( '29086ra')
  and c.account in (
    select account
    from fin.dim_account
    where account_type = 'Asset'
      and department_code in ('NC','UC')
      and description like '%INV%')  
order by a.control, b.the_date



select c.store_code, a.control, c.department_code, sum(a.amount), count(*) over (partition by c.store_code, c.department_code)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
where post_status = 'Y'
-- and control in ( '29086ra')
and c.account in (
  select account
  from fin.dim_account
  where account_type = 'Asset'
    and department_code in ('NC','UC')
    and description like '%INV%')
and b.the_date < '12/01/2017'        
group by c.store_code, a.control, c.department_code
having sum(a.amount) > 0
order by c.store_code, c.department_code



select * from dec where page = 5 and the_date > '11/30/2017'

select *
from board_data



select *
from step_2  
where store = 'ry2'


select *
from 