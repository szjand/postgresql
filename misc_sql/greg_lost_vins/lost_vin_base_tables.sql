﻿--------------------------------------------------------------------------------------------------------
-------------------- run this entire block to create the necessary tables ------------------------------ 
--------------------------------it only takes 40 seconds -----------------------------------------------
-- create index on ads.ext_fact_repair_order(opendatekey);
-- vins visited any dept between 5 and 2 years ago 
-- drop table if exists visits; 
-- create temp table visits as 

drop table if exists abby.visits cascade;
create table abby.visits (
  ro citext primary key,
  the_date date not null,
  vin citext not null);
insert into abby.visits  
select a.ro, b.the_date, c.vin 
from ads.ext_fact_repair_order a 
inner join dds.dim_date b on a.opendatekey = b.date_key 
inner join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey 
inner join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey
  and d.paymenttype <> 'Internal'
where b.the_date between current_date - 5*365 and current_date - 2*365
--   and a.storecode = 'RY1'
  and c.vin not like '0%'
  and length(c.vin) = 17  
group by b.the_date, a.ro, c.vin;
create index on abby.visits(the_date);
create index on abby.visits(vin);
comment on table abby.visits is 'ro, vin, open_date, all market ros excluding internal opened between 5 and 2 years ago';


-- all vins visited any dept
-- drop table if exists ros;
-- create temp table ros as
drop table if exists abby.ros cascade;
create table abby.ros (
  ro citext primary key,
  the_date date not null,
  vin citext not null);
insert into abby.ros
select a.ro, b.the_date, c.vin
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.opendatekey = b.date_key 
inner join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey 
inner join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey
  and d.paymenttype <> 'Internal'
-- where a.storecode = 'RY1'
where c.vin not like '0%'
  and length(c.vin) = 17
  and b.the_date >= current_Date - 5*365
group by b.the_date, a.ro, c.vin;
create index on abby.ros(the_date);
create index on abby.ros(vin);
comment on table abby.ros is 'ro, vin, open_date, all market ros, excluding internal, opened within the past 5 years';

-- 
-- single visit vins from more than 2 years ago 
-- drop table if exists single_visit_vins; 
-- create temp table single_visit_vins as 
drop table if exists abby.single_visit_vins cascade;
create table abby.single_visit_vins (
  ro citext primary key,
  the_date date not null,
  vin citext not null);
insert into abby.single_visit_vins  
select a.* 
from abby.ros a 
inner join (
  select vin
  from abby.ros
  where the_date < current_date -730
  group by vin
  having count(vin) = 1) b on a.vin = b.vin;  
create index on abby.single_visit_vins(ro);
create index on abby.single_visit_vins(vin);
create index on abby.single_visit_vins(the_date);
comment on table abby.single_visit_vins is 'ro, vin, open_date, all market ros opened more than 2 years ago, for with there is only one ro for the vin';  

-- 
-- drop table if exists prev_dates;
-- -- visits and their previous visit date
-- create temp table prev_dates as
-- select a.*,
--   lag(a.the_date, 1) over (partition by a.vin order by a.the_date) as prev_date,
--   a.the_date - coalesce(lag(a.the_date, 1) over (partition by a.vin order by a.the_date), a.the_date) as date_diff 
-- from ros a where exists (
--   select 1
--   from visits
--   where vin = a.vin)
-- and not exists (
--   select 1
--   from single_visit_vins
--   where vin  = a.vin);
-- create index on prev_dates(the_date);
-- create index on prev_dates(vin);
-- create index on prev_dates(prev_date);
-- create index on prev_dates(date_diff);

-- drop table if exists next_dates;
-- -- visits and the next visit date -- i like this one better than prev_dates 
-- create temp table next_dates as 

drop table if exists abby.next_dates cascade;
create table abby.next_dates (
  the_date date not null,
  ro citext not null, 
  vin citext not null, 
  next_date date,
  date_diff integer,
  constraint next_dates_pkey primary key (ro,vin));
create index on abby.next_dates(the_date);
create index on abby.next_dates(vin);
create index on abby.next_dates(next_date);
create index on abby.next_dates(date_diff);
comment on table abby.next_dates is 'from abby.visits, those ros/vins that don''t exists in abby.single_visit_vins, showing the next visit date';

insert into abby.next_dates 
select the_date, ro, vin, next_date,
  case
    when next_date is null then current_date - b.the_date
    else date_diff
  end as date_diff
from (
  select a.*,
    lead(a.the_date, 1) over (partition by a.vin order by a.the_date) as next_Date,
    lead(a.the_date, 1) over (partition by a.vin order by a.the_date) - a.the_date as date_diff
  from abby.ros a
  where exists (
    select 1
    from abby.visits
    where vin = a.vin)
  and not exists (
    select 1 
    from abby.single_visit_vins 
    where vin  = a.vin)) b;

--------------------------------------------------------------------------------------------------

-- 6/9/18
-- abby's notes
select *
from abby.next_dates 
where the_Date between '05/21/2016' and '05/21/2017'
  and next_Date is null
order by vin, the_Date
limit 1000  


select *
from abby.visits
where vin = '19XFA1F80AE026956'

select * from abby.ros where ro = '16287344'

select * 
from arkona.ext_bopvref a
left join ads.ext_dim_customer b on a.customer_key = b.bnkey
where vin = '19XFA1F80AE026956'


-- rethink the whole ro deal, include both stores, don't exclude internal pay type but do exclude inventory

-- drop table if exists visits; -- 486861 -> 374962
-- create temp table visits as

drop table if exists abby.visits cascade;
create table abby.visits (
  ro citext primary key,
  the_date date not null,
  vin citext not null,
  fullname citext not null);
  
insert into abby.visits  
select a.ro, b.the_date, c.vin, e.fullname
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.opendatekey = b.date_key 
inner join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey 
inner join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey
inner join ads.ext_dim_customer e on a.customerkey = e.customerkey
  and e.fullname not in ( 'INVENTORY', 'RYDELL AUTO CENTER', 'CARWASH, CARWASH') -- 110000 rows
where c.vin not like '0%'
  and c.vin not like '111%'
  and length(c.vin) = 17
  and b.the_date >= current_Date - 5*365
group by b.the_date, a.ro, c.vin, e.fullname;

create index on abby.visits(the_date);
create index on abby.visits(vin);
create index on abby.visits(fullname);
comment on table abby.visits is 'all market ros from the past 5 years, excluding inventory, loaners, etc';


-- think i will ignore single visit vins, at least for now

drop table if exists abby.next_dates cascade;
create table abby.next_dates (
  the_date date not null,
  ro citext not null, 
  vin citext not null, 
  fullname citext not null,
  next_date date,
  date_diff integer,
  constraint next_dates_pkey primary key (ro,vin));
  
create index on abby.next_dates(the_date);
create index on abby.next_dates(vin);
create index on abby.next_dates(next_date);
create index on abby.next_dates(fullname);
create index on abby.next_dates(date_diff);
comment on table abby.next_dates is 'from abby.visits showing the next visit date';
-- drop table if exists next_dates;
-- create temp table next_dates as
insert into abby.next_dates
select the_date, ro, vin, fullname, next_date,
  case
    when next_date is null then current_date - b.the_date
    else date_diff
  end as date_diff
from (
  select a.*,
    lead(a.the_date, 1) over (partition by a.vin order by a.the_date) as next_Date,
    lead(a.the_date, 1) over (partition by a.vin order by a.the_date) - a.the_date as date_diff
  from visits a) b;

select *
from next_dates
where the_date between '06/13/2016' and '06/13/2017'   
  and next_date is null
order by ro

select * from abby.next_dates where vin = '19XFB2F8XCE023715' order by the_date

drop table abby.ros cascade;
drop table abby.single_visit_vins cascade;  

select vin, count(*)
from (
select vin, fullname
from abby.visits
group by vin, fullname) a
group by vin
order by count(*) desc


select *
from next_dates
where vin = '1GKS2JKJXFR151015'
order by the_date

