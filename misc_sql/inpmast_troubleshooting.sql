﻿8/31/17

work_in_process went from
  8/29: 0
  8/30: 205.04
  8/31 0
resulting in hash for 8/29 = hash for 8/30, causing update to fail due to unique constraint "xfm_inpmast_hash_idx"
both rows are valid, leading me to believe that the unique constraint is not appropriate

drop index arkona.xfm_inpmast_hash_idx;
create index on arkona.xfm_inpmast(hash);

select inpmast_company_number,inpmast_vin,vin_last_6,inpmast_stock_number,inpmast_document_number,
  status,g_l_applied,type_n_u,bus_off_fran_code,service_fran_code,manufacturer_code,vehicle_code,
  year,make,model_code,model,body_style,color,trim,fuel_type,mpg,cylinders,truck,wheel_drive4wd,
  turbo,color_code,engine_code,transmission_code,ignition_key_code,trunk_key_code,keyless_code,
  radio_code,wheel_lock_code,dealer_code,location,odometer,date_in_invent,date_in_service,
  date_delivered,date_ordered,inpmast_sale_account,inventory_account,demo_name,warranty_months,
  warranty_miles,warranty_deduct,list_price,inpmast_vehicle_cost,option_package,license_number,
  gross_weight,work_in_process,inspection_month,odometer_actual,bopname_key,
  key_to_cap_explosion_data,co2_emission_code2,registration_date1,funding_expiration_date2,
  inspection_date3,drivers_side,free_flooring_period,ordered_status,publish_vehicle_info_to_web,
  sale,certified_used_car,last_service_date4,next_service_date5,dealer_code_imdlrcd,
  common_vehicle_id,chrome_style_id,created_user_code,updated_user_code,created_timestamp,
  updated_timestamp,stocked_company,engine_hours,
  -- skip inpmast_key: serial
  current_date as row_from_date,
  -- skip row_thru_date: default value
  true as current_row,
    (
      select md5(z::text) as hash
      from (
        select inpmast_company_number,inpmast_vin,vin_last_6,inpmast_stock_number,inpmast_document_number,
          status,g_l_applied,type_n_u,bus_off_fran_code,service_fran_code,manufacturer_code,vehicle_code,
          year,make,model_code,model,body_style,color,trim,fuel_type,mpg,cylinders,truck,wheel_drive4wd,
          turbo,color_code,engine_code,transmission_code,ignition_key_code,trunk_key_code,keyless_code,
          radio_code,wheel_lock_code,dealer_code,location,odometer,date_in_invent,date_in_service,
          date_delivered,date_ordered,inpmast_sale_account,inventory_account,demo_name,warranty_months,
          warranty_miles,warranty_deduct,list_price,inpmast_vehicle_cost,option_package,license_number,
          gross_weight,work_in_process,inspection_month,odometer_actual,bopname_key,
          key_to_cap_explosion_data,co2_emission_code2,registration_date1,funding_expiration_date2,
          inspection_date3,drivers_side,free_flooring_period,ordered_status,publish_vehicle_info_to_web,
          sale,certified_used_car,last_service_date4,next_service_date5,dealer_code_imdlrcd,
          common_vehicle_id,chrome_style_id,created_user_code,updated_user_code,created_timestamp,
          updated_timestamp,stocked_company,engine_hours
        from arkona.ext_inpmast
        where inpmast_company_number = a.inpmast_company_number
          and inpmast_vin = a.inpmast_vin) z)
from arkona.ext_inpmast a
where a.inpmast_vin = '3GTU2WEJXFG229015'

union 
select inpmast_company_number,inpmast_vin,vin_last_6,inpmast_stock_number,inpmast_document_number,
  status,g_l_applied,type_n_u,bus_off_fran_code,service_fran_code,manufacturer_code,vehicle_code,
  year,make,model_code,model,body_style,color,trim,fuel_type,mpg,cylinders,truck,wheel_drive4wd,
  turbo,color_code,engine_code,transmission_code,ignition_key_code,trunk_key_code,keyless_code,
  radio_code,wheel_lock_code,dealer_code,location,odometer,date_in_invent,date_in_service,
  date_delivered,date_ordered,inpmast_sale_account,inventory_account,demo_name,warranty_months,
  warranty_miles,warranty_deduct,list_price,inpmast_vehicle_cost,option_package,license_number,
  gross_weight,work_in_process,inspection_month,odometer_actual,bopname_key,
  key_to_cap_explosion_data,co2_emission_code2,registration_date1,funding_expiration_date2,
  inspection_date3,drivers_side,free_flooring_period,ordered_status,publish_vehicle_info_to_web,
  sale,certified_used_car,last_service_date4,next_service_date5,dealer_code_imdlrcd,
  common_vehicle_id,chrome_style_id,created_user_code,updated_user_code,created_timestamp,
  updated_timestamp,stocked_company,engine_hours,
  -- skip inpmast_key: serial
  row_from_date, current_row, hash
from arkona.xfm_inpmast
where inpmast_vin = '3GTU2WEJXFG229015'
order by row_from_date