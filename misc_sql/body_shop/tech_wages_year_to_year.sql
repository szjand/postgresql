﻿/*
09/02/2021
Jon would you be able to run me a report showing the wages for the body shop in 2020 and then the wages for the body shop up to this point in 2021 please.
 
Thank you
*/


select a.employee_name,
  sum(b.total_gross_pay) filter (where check_year = 20) as "2020 total",
  sum(b.total_gross_pay) filter (where (check_year = 20 and check_month between 1 and 8) or (check_year = 20 and check_month = 9 and check_day = 4)) as "2020 thru 9/4",
  sum(b.total_gross_pay) filter (where check_year = 21) as "2021 thru 9/3"
--  select a.employee_name, b.check_year, b.check_month, check_day, b.total_gross_pay
from arkona.ext_pymast a
left join arkona.ext_pyhshdta b on a.pymast_employee_number = b.employee_
  and check_year > 19
where a.employee_name in ('ADAM, PATRICK',
	'BIES II, DAVID',
	'BUCHANAN, KYLE',
	'DRISCOLL, TERRANCE',
	'EBERLE, MARCUS M',
	'GARDNER, CHAD A',
	'JACOBSON, PETER A',
	'JOHNSON, BENJAMIN R',
	'LAMONT, BRANDON',
	'MAVITY, ROBERT',
	'OLSON, CODEY',
	'OLSON, JUSTIN S',
	'ORTIZ, GUADALUPE',
	'PETERSON, BRIAN D',
	'ROSE, CORY',
	'SEVIGNY, SCOTT',
	'WALDEN, CHRIS W',
	'WALTON, JOSHUA')
-- group by a.employee_name	
-- order by a.employee_name	
order by b.check_year, b.check_month, check_day	

-- Do you happen to know how many payroll cycles we have gone through so far?
select distinct b.check_year, b.check_month, check_day
from arkona.ext_pymast a
left join arkona.ext_pyhshdta b on a.pymast_employee_number = b.employee_
  and check_year > 19
where a.employee_name in ('ADAM, PATRICK',
	'BIES II, DAVID',
	'BUCHANAN, KYLE',
	'DRISCOLL, TERRANCE',
	'EBERLE, MARCUS M',
	'GARDNER, CHAD A',
	'JACOBSON, PETER A',
	'JOHNSON, BENJAMIN R',
	'LAMONT, BRANDON',
	'MAVITY, ROBERT',
	'OLSON, CODEY',
	'OLSON, JUSTIN S',
	'ORTIZ, GUADALUPE',
	'PETERSON, BRIAN D',
	'ROSE, CORY',
	'SEVIGNY, SCOTT',
	'WALDEN, CHRIS W',
	'WALTON, JOSHUA')
and check_year = 21	
order by b.check_year, b.check_month, check_day		


select string_agg(''''||a.pymast_employee_number||'''', ',')
from arkona.ext_pymast a
where a.employee_name in ('ADAM, PATRICK',
	'BIES II, DAVID',
	'BUCHANAN, KYLE',
	'DRISCOLL, TERRANCE',
	'EBERLE, MARCUS M',
	'GARDNER, CHAD A',
	'JACOBSON, PETER A',
	'JOHNSON, BENJAMIN R',
	'LAMONT, BRANDON',
	'MAVITY, ROBERT',
	'OLSON, CODEY',
	'OLSON, JUSTIN S',
-- 	'ORTIZ, GUADALUPE',
	'PETERSON, BRIAN D',
	'ROSE, CORY',
	'SEVIGNY, SCOTT',
	'WALDEN, CHRIS W',
	'WALTON, JOSHUA')


