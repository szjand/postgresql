﻿drop table if exists jon.bs_tech_clock_flag_hours_history cascade;
create table jon.bs_tech_clock_flag_hours_history(
  the_date date not null,
  employee_number citext not null,
  first_name citext not null,
  last_name citext not null,
  clock_hours numeric not null,
  flag_hours numeric not null,
  primary key(the_date,employee_number));

comment on table jon.bs_tech_clock_flag_hours_history is 'culled from tpdata and bsflatratedata, all clock and flag data 1/1/2018 - 9/3/2021'


select * from jon.bs_tech_clock_flag_hours_history limit 10

select extract(year from the_date), first_name, last_name, count(*)
from jon.bs_tech_clock_flag_hours_history 
group by extract(year from the_date), first_name, last_name
order by first_name, last_name, extract(year from the_date)

select last_name || ', ' || first_name, 
  sum(clock_hours) filter (where extract(year from the_date) = 2018) as "2018 clock",
  sum(clock_hours) filter (where extract(year from the_date) = 2019) as "2019 clock",
  sum(clock_hours) filter (where extract(year from the_date) = 2020) as "2020 clock",
  sum(clock_hours) filter (where extract(year from the_date) = 2021) as "2021 clock",
  sum(flag_hours) filter (where extract(year from the_date) = 2018) as "2018 flag",
  sum(flag_hours) filter (where extract(year from the_date) = 2019) as "2019 flag",
  sum(flag_hours) filter (where extract(year from the_date) = 2020) as "2020 flag",
  sum(flag_hours) filter (where extract(year from the_date) = 2021) as "2021 flag"
from jon.bs_tech_clock_flag_hours_history 
group by last_name || ', ' || first_name
order by last_name || ', ' || first_name



/*
the advantage sql to generate the data

SELECT thedate, employeenumber, firstname, lastname, techclockhoursday, techflaghoursday FROM tpdata

SELECT  MIN(thedate), MAX(thedate)
--FROM tpdata_archive_thru_2018
FROM tpdata_archive_thru_2019
--FROM tpdata_archive_thru_2020
WHERE employeenumber IN ('135770','150105','171055',
			'191350','1106400','1110425','1118722','1125565',
			'1146991','1147061','184620','1100625','184614',
			'11660','137101','145789','157953')
and thedate > '12/31/2017'	

			
SELECT DISTINCT(lastname + ' ' + firstname)
FROM tpdata_archive_thru_2018
WHERE employeenumber IN ('135770','150105','171055',
			'191350','1106400','1110425','1118722','1125565',
			'1146991','1147061','184620','1100625','184614',
			'11660','137101','145789','157953')			
and thedate > '12/31/2017'			
UNION
SELECT DISTINCT(lastname + ' ' + firstname)
FROM tpdata_archive_thru_2019
WHERE employeenumber IN ('135770','150105','171055',
			'191350','1106400','1110425','1118722','1125565',
			'1146991','1147061','184620','1100625','184614',
			'11660','137101','145789','157953')			
UNION
SELECT DISTINCT(lastname + ' ' + firstname)
FROM tpdata_archive_thru_2020
WHERE employeenumber IN ('135770','150105','171055',
			'191350','1106400','1110425','1118722','1125565',
			'1146991','1147061','184620','1100625','184614',
			'11660','137101','145789','157953')					
UNION
SELECT DISTINCT(lastname + ' ' + firstname)
FROM tpdata
WHERE employeenumber IN ('135770','150105','171055',
			'191350','1106400','1110425','1118722','1125565',
			'1146991','1147061','184620','1100625','184614',
			'11660','137101','145789','157953')		
	
				
SELECT thedate, employeenumber, firstname, lastname, techclockhoursday, techflaghoursday
FROM tpdata_archive_thru_2018
WHERE employeenumber IN ('135770','150105','171055',
			'191350','1106400','1110425','1118722','1125565',
			'1146991','1147061','184620','1100625','184614',
			'11660','137101','145789','157953')			
and thedate > '12/31/2017'			
UNION
SELECT thedate, employeenumber, firstname, lastname, techclockhoursday, techflaghoursday 
FROM tpdata_archive_thru_2019
WHERE employeenumber IN ('135770','150105','171055',
			'191350','1106400','1110425','1118722','1125565',
			'1146991','1147061','184620','1100625','184614',
			'11660','137101','145789','157953')			
UNION
SELECT thedate, employeenumber, firstname, lastname, techclockhoursday, techflaghoursday 
FROM tpdata_archive_thru_2020
WHERE employeenumber IN ('135770','150105','171055',
			'191350','1106400','1110425','1118722','1125565',
			'1146991','1147061','184620','1100625','184614',
			'11660','137101','145789','157953')					
UNION
SELECT thedate, employeenumber, firstname, lastname, techclockhoursday, techflaghoursday 
FROM tpdata
WHERE employeenumber IN ('135770','150105','171055',
			'191350','1106400','1110425','1118722','1125565',
			'1146991','1147061','184620','1100625','184614',
			'11660','137101','145789','157953')		
UNION 			
SELECT thedate, employeenumber, 'Brian', 'Peterson', techclockhoursday, techpdrflaghoursday + techmetalflaghoursday
FROM bsflatratedata			
WHERE thedate > '12/31/2017'
*/