﻿/*
I have a request from Ashley that you could probably do much faster

She would like for June 2022 & July 2022 (MTD) Body Shop UC (Internal) Flag Hours & Customer Flag Hours
She said she can't get this on her own because Honda units are lumped in as customer
*/
select ro, b.full_name, a.flag_hours, c.payment_type, d.service_type
-- select a.*
from dds.fact_Repair_order a
join dds.dim_customer b on a.customer_key = b.customer_key
join dds.dim_payment_type c on a.payment_type_key = c.payment_type_key
join dds.dim_Service_type d on a.service_type_key = d.service_type_key
join dds.dim_vehicle e on a.vehicle_key = e.vehicle_key
--   and e.vin = '2FMPK4G91LBB23325'
where a.ro like '18%' 
  and a.open_date > '05/31/2022'
--   and full_name = 'inventory'
--   and payment_type = 'customer pay'
  and a.flag_hours > 0


select b.full_name, c.payment_type, d.service_type, sum(a.flag_hours), count(*), array_agg(distinct ro)
from dds.fact_Repair_order a
join dds.dim_customer b on a.customer_key = b.customer_key
join dds.dim_payment_type c on a.payment_type_key = c.payment_type_key
join dds.dim_Service_type d on a.service_type_key = d.service_type_key
join dds.dim_vehicle e on a.vehicle_key = e.vehicle_key
where a.ro like '18%' 
  and a.open_date > '05/31/2022'
  and a.flag_hours > 0
group by b.full_name, c.payment_type, d.service_type
order by count(*) desc

-- these are the number io sent to ashley
-- should have used flag date
select 
  sum(a.flag_hours) filter (where b.full_name = 'BODY SHOP TIME ADJ') as "shop time",
  sum(a.flag_hours) filter (where b.full_name = 'inventory') as "used cars",
  sum(a.flag_hours) filter (where b.full_name <> 'inventory' and c.payment_type = 'customer pay') as "cust pay",
  sum(a.flag_hours) filter (where b.full_name <> 'inventory' and c.payment_type = 'internal') as "other internal"
from dds.fact_repair_order a
join dds.dim_customer b on a.customer_key = b.customer_key
join dds.dim_payment_type c on a.payment_type_key = c.payment_type_key
join dds.dim_Service_type d on a.service_type_key = d.service_type_key
join dds.dim_vehicle e on a.vehicle_key = e.vehicle_key
where a.ro like '18%' 
  and a.open_date > '05/31/2022'
  and a.flag_hours > 0


-- add by tech, sent this csv as well
select f.tech_number, f.tech_name,
  sum(a.flag_hours) filter (where b.full_name = 'BODY SHOP TIME ADJ') as "shop time",
  sum(a.flag_hours) filter (where b.full_name = 'inventory') as "used cars",
  sum(a.flag_hours) filter (where b.full_name <> 'inventory' and c.payment_type = 'customer pay') as "cust pay",
  sum(a.flag_hours) filter (where b.full_name <> 'inventory' and c.payment_type = 'internal') as "other internal"
from dds.fact_repair_order a
join dds.dim_customer b on a.customer_key = b.customer_key
join dds.dim_payment_type c on a.payment_type_key = c.payment_type_key
join dds.dim_Service_type d on a.service_type_key = d.service_type_key
join dds.dim_vehicle e on a.vehicle_key = e.vehicle_key
join dds.dim_tech f on a.tech_key = f.tech_key
  and tech_name is not null
where a.ro like '18%' 
  and a.open_date > '05/31/2022'
  and a.flag_hours > 0
group by f.tech_number, f.tech_name