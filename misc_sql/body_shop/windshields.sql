﻿/*
The parts department is looking to stock windshields.  I am unable to give them a good idea of what the most common windshields are. 
Would you be able to run a report based off part number for GM vehicles only that would give us totals for windshields we did in 2019.

Example:
Silverado windshields we did 150 of this part number X
Sierra windshields we did 75 for part number Y

There is a number of different part numbers for each model.

DW 1658 GBY  OLDER P/U W/O RAIN SENSOR 
DW 1659GBY OLDER W/ RAIN SENSOR
DW 2040GTY NEWER W/O LANE DEPT
DW 2041GTY NEWER W/ LANE DEPT

Here are the aftermarket part numbers, these are some of the windshields we are looking to stop doing aftermarket glass and begin stocking OEM glass for.

*/

select *
from arkona.ext_pdpmast
where part_description like 'WIND%'
limit 100

-- uh oh i didnt read it closely enough, GM only is what he asked for
drop table if exists windshields;
create temp table windshields as
select a.ptmanf, c.the_date, c.year_month, c.the_year, b.part_number, b.part_description, a.ptco_, a.ptinv_, a.ptcode, d.model_year, d.make, d.model
from arkona.ext_pdptdet a
join arkona.ext_pdpmast b on a.ptpart = b.part_number
  and b.part_description = 'windshield'
join dds.dim_date c on dds.db2_integer_to_date(a.ptdate) = c.the_date
join (
  select ro, c.model_year, c.make, c.model
  from dds.dim_date a 
  join dds.fact_repair_order b on a.the_date = b.open_date
    and b.ro like '18%'
  join dds.dim_vehicle c on b.vehicle_key = c.vehicle_key  
    and c.make in ('GMC','CHEVROLET','CADILLAC','PONTIAC','BUICK')
  group by ro, c.model_year, c.make, c.model) d on a.ptinv_ = d.ro
where dds.db2_integer_to_date(a.ptdate) between '01/01/2019' and '12/31/2020'  
  and a.ptinv_ like '18%'
  and a.ptcode <> 'DP';

select ptcode, count(*) 
from windshields
group by ptcode

select the_year, part_number, string_agg(distinct model, ','), count(*)
from windshields
group by the_year, part_number
order by the_year, part_number

select distinct make from windshields

select * from arkona.ext_pdptdet where ptpart in ('DW1528GTYOEE','DW1531GTYOEE')
