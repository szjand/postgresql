﻿drop table if exists accounting cascade;
create temp table accounting as
select b.year_month, a.control, c.account, sum(a.amount) as amount
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month between 202101 and 202107
join fin.dim_account c on a.account_key = c.account_key
  and c.account in ('147900','167900')
where a.post_Status = 'Y' 
group by b.year_month, a.control, c.account


select count(*) from accounting  732 total, 366 ros

select * from accounting order by control

-- limit to ros with internal pay type
select aa.year_month,
  sum(-aa.amount) filter (where account = '147900') as "147900",
  sum(aa.amount) filter (where account = '167900') as "167900",
  sum(-aa.amount) as diff
from accounting aa
join (
	select distinct a.control
	from accounting a
	join dds.fact_repair_order b on a.control = b.ro
	join dds.dim_payment_type c on b.payment_type_key = c.payment_type_key
		and c.payment_type = 'internal') bb on aa.control = bb.control
group by aa.year_month