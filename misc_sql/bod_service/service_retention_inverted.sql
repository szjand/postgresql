﻿of the main shop ros written in june, how man of those vehicles were sold here in the last 5 years

drop table if exists service_vins;
create temp table service_vins as
select distinct vin
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.opendatekey = b.date_key
inner join ads.ext_dim_customer c on a.customerkey = c.customerkey
left join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
where left(a.ro, 2) = '16'
  and b.year_month = 201706
  and c.fullname not in ('inventory', 'shop time')
  and length(d.vin) = 17;


 select count(*),
   sum(case when b.bopmast_vin is not null then 1 else 0) end as sold_here


select a.vin, a.fullname, b.bopmast_stock_number, b.bopmast_Search_name, b.delivery_date, b.date_capped   
-- select *
from service_vins a
left join arkona.ext_bopmast b on a.vin = b.bopmast_vin
  and b.delivery_date > '07/01/2012'


select count(*),
  sum(case when b.bopmast_vin is not null then 1 else 0 end) as sold_here
  
from service_vins a
left join arkona.ext_bopmast b on a.vin = b.bopmast_vin
  and b.delivery_date > '07/01/2012'
