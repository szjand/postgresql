﻿--------------------------------------------------------------------------------------------------------
-------------------- run this entire block to create the necessary tables ------------------------------
--------------------------------it only takes 40 seconds -----------------------------------------------
-- create index on ads.ext_fact_repair_order(opendatekey);
-- vins visited any dept between 5 and 2 years ago
drop table if exists visits;
create temp table visits as
select b.the_date, a.ro, c.vin
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.opendatekey = b.date_key
inner join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey
inner join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey
  and d.paymenttype <> 'Internal'
where b.the_date between current_date - 5*365 and current_date - 2*365
  and a.storecode = 'RY1'
  and c.vin not like '0%'
  and length(c.vin) = 17 
group by b.the_date, a.ro, c.vin;
create index on visits(the_date);
create index on visits(ro);
create index on visits(vin);

-- all vins visited any dept
drop table if exists ros;
create temp table ros as
select b.the_date, a.ro, c.vin
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.opendatekey = b.date_key
inner join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey
inner join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey
  and d.paymenttype <> 'Internal'
where a.storecode = 'RY1'
  and c.vin not like '0%'
  and length(c.vin) = 17 
group by b.the_date, a.ro, c.vin;
create index on ros(the_date);
create index on ros(ro);
create index on ros(vin);

-- single visit vins from more than 2 years ago
drop table if exists single_visit_vins;
create temp table single_visit_vins as
select a.*
from ros a
inner join (
  select vin
  from ros
  where the_date < current_date -730
  group by vin
  having count(vin) = 1) b on a.vin = b.vin
order by a.vin, a.the_date;  

drop table if exists prev_dates;
-- visits and their previous visit date
create temp table prev_dates as
select a.*,
  lag(a.the_date, 1) over (partition by a.vin order by a.the_date) as prev_date,
  a.the_date - coalesce(lag(a.the_date, 1) over (partition by a.vin order by a.the_date), a.the_date) as date_diff
from ros a
where exists (
  select 1
  from visits
  where vin = a.vin) 
and not exists (
  select 1 
  from single_visit_vins 
  where vin  = a.vin);
create index on prev_dates(the_date);
create index on prev_dates(vin);
create index on prev_dates(prev_date);
create index on prev_dates(date_diff);

drop table if exists next_dates;
-- visits and the next visit date -- i like this one better than prev_dates
create temp table next_dates as
select the_date, ro, vin, next_date,
  case
    when next_date is null then current_date - b.the_date
    else date_diff
  end as date_diff
from (  
  select a.*,
    lead(a.the_date, 1) over (partition by a.vin order by a.the_date) as next_Date,
    lead(a.the_date, 1) over (partition by a.vin order by a.the_date) - a.the_date as date_diff
  from ros a
  where exists (
    select 1
    from visits
    where vin = a.vin) 
  and not exists (
    select 1 
    from single_visit_vins 
    where vin  = a.vin)) b;
create index on next_dates(the_date);
create index on next_dates(vin);
create index on next_dates(next_date);
create index on next_dates(date_diff);

--------------------------------------------------------------------------------------------------------
-------------------- run this entire block to create the necessary tables ------------------------------
--------------------------------------------------------------------------------------------------------

select *
from next_dates
order by vin, the_date
limit 1000

select *
from next_dates a
where not exists (
  select 1
  from next_dates
  where vin = a.vin
    and date_diff < 730)



select count(distinct vin)  --47865
from visits; 

select count(distinct a.vin) -- 19386
from single_visit_vins a
inner join visits b on a.vin = b.vin;


select count(distinct vin) -- 13507
from next_dates 
where date_diff > 730  

for a total of 32893 lost vins

select 100 * 32893/47865 = 68% lost 

-- 8/19
-- curious about single visit vins, look at pay type
-- all vins visited any dept, all time

drop table if exists ros;
create temp table ros as
select b.the_date, a.ro, c.vin, string_agg(distinct d.paymenttype, '-') as payment_type
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.opendatekey = b.date_key
inner join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey
  and c.vin not like '0%'
  and length(c.vin) = 17 
  and c.vin not like '111%'
inner join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey
where storecode = 'ry1'
-- inner join (
--   select vin
--   from ads.ext_fact_repair_order m
--   inner join ads.ext_dim_vehicle n on m.vehiclekey = n.vehiclekey
--   group by vin
--   having count(*) = 1) e on c.vin = e.vin
group by b.the_date, a.ro, c.vin

select count(*) from ros  -- 369164

select a.*
from ros a
inner join (
  select vin
  from ros
  group by vin 
  having count(*) = 1) b on a.vin = b.vin
limit 1000  

select payment_type, count(*) 
from (
  select a.*
  from ros a
  inner join (
    select vin
    from ros
    group by vin 
    having count(*) = 1) b on a.vin = b.vin) a
group by payment_type    
order by count(*) desc

select extract(year from the_date), count(*)
from ros
group by extract(year from the_date)

select 
  case dept
    when '16' then 'main shop'
    when '18' then 'body shop'
    when '19' then 'pdq'
  end as dept, payment_type, count
from (  
  select left(ro,2) as dept, payment_type, count(*) 
  from (
    select a.*
    from ros a
    inner join (
      select vin
      from ros
      group by vin 
      having count(*) = 1) b on a.vin = b.vin) a
  where left(ro,2) in ('16','18','19')    
  group by left(ro, 2), payment_type) x    
order by dept asc, count desc


----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------
/*
august BOD mtg in grand forks exposed a different way of looking  at lost vins, 
from sioux falls ford

start with all unique vins seen in service : 173,955
then
total number of those vehicles that have not been in for service 
  in the past year: 150100
  in the past 2 year: 139232
  in the past 3 years: 129312
  in the past 4 years=: 119664
  in the past 5 years 109663

conclusion: we stop seeing 10000 unique vehicles every year
*/  

/*
select min(the_date)  -- 7/11/2011
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.opendatekey = b.date_key

select b.the_year, count(*)
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.opendatekey = b.date_key
group by b.the_year
order by b.the_year

8/17 redo it using a deep extract from arkona, back thru 2005: arkona.ads.ext
*/
-- unique vins ever seen in service at RY1, including interal
drop table if exists ry1_vins cascade;
create temp table ry1_vins as
select c.vin
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.opendatekey = b.date_key
inner join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey
-- inner join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey
--   and d.paymenttype <> 'Internal'
-- where b.the_date between current_date - 5*365 and current_date
where a.storecode = 'RY1'
  and c.vin not like '0%'
  and length(c.vin) = 17 
  and left(c.vin, 4) <> '1111'
group by c.vin;
create unique index on ry1_vins(vin);

select count(*) from ry1_vins; -- 74581;

-- unique vins ever seen in service at RY2, including interal
drop table if exists ry2_vins;
create temp table ry2_vins as
select c.vin
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.opendatekey = b.date_key
inner join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey
-- inner join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey
--   and d.paymenttype <> 'Internal'
-- where b.the_date between current_date - 5*365 and current_date
where a.storecode = 'RY2'
  and c.vin not like '0%'
  and length(c.vin) = 17 
  and left(c.vin, 4) <> '1111'
group by c.vin;
create unique index on ry2_vins(vin);

select count(*) from ry2_vins; -- 22300


-- all vins visited any dept
drop table if exists ros;
create temp table ros as
select b.the_date, a.storecode, a.ro, c.vin
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.opendatekey = b.date_key
inner join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey
-- inner join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey
--   and d.paymenttype <> 'Internal'
-- where a.storecode = 'RY1'
where c.vin not like '0%'
  and length(c.vin) = 17 
  and left(c.vin, 4) <> '1111'
group by a.storecode, b.the_date, a.ro, c.vin;
create index on ros(the_date);
create index on ros(ro);
create index on ros(vin);


drop table if exists past_years;
create temp table past_years as
select a.vin,
  case 
    when exists (
      select 1
      from ros
      where vin = a.vin
        and the_date between current_date - 365 and current_Date) then 1 else 0 end as past_year,     
  case 
    when exists (
      select 1
      from ros
      where vin = a.vin
        and the_date between current_date - (2*365) and current_Date -365) then 1 else 0 end as past_2_year,   
  case 
    when exists (
      select 1
      from ros
      where vin = a.vin
        and the_date between current_date - (3*365) and current_Date - (2*365)) then 1 else 0 end as past_3_year,   
  case 
    when exists (
      select 1
      from ros
      where vin = a.vin
        and the_date between current_date - (4*365) and current_Date - (3*365)) then 1 else 0 end as past_4_year,   
  case 
    when exists (
      select 1
      from ros
      where vin = a.vin
        and the_date between current_date - (5*365) and current_Date - (4*365)) then 1 else 0 end as past_5_year                                   
from ry1_vins a;
create unique index on past_years(vin);
create index on past_years(past_year);
create index on past_years(past_2_year);
create index on past_years(past_3_year);
create index on past_years(past_4_year);
create index on past_years(past_5_year);

select *
from past_years
limit 1000



-- 41605;28131;16784;7113;4
select 
  sum(case when past_year = 0 then 1 else 0 end) as past_year,
  sum(case when past_year = 0 and past_2_year = 0 then 1 else 0 end) as past_2_year,
  sum(case when past_year = 0 and past_2_year = 0 and past_3_year = 0 then 1 else 0 end) as past_3_year,
  sum(case when past_year = 0 and past_2_year = 0 and past_3_year = 0 and past_4_year = 0 then 1 else 0 end) as past_4_year,
  sum(case when past_year = 0 and past_2_year = 0 and past_3_year = 0 and past_4_year = 0 and past_5_year = 0 then 1 else 0 end) as past_5_year
from past_years;  

--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
-- 8/17 redo it using a deep extract from arkona, back thru 2005: arkona.ads.ext
-- unique vins ever seen in service, including interal

8/28/17 scrape failed on ro '16046015' there is a ' 16046015' ro as well
drop index arkona.ext_sdprhdr_history_ro_idx;

delete  
from arkona.ext_sdprhdr_history
where trim(ro) in (
  select trim(ro)
  from arkona.ext_sdprhdr_history
  group by trim(ro)
  having count(*) > 1);


drop table if exists greg.vins cascade;
create table greg.vins (
  vin citext primary key);
insert into greg.vins  
select a.vin
from arkona.ext_sdprhdr_history a
where a.vin not like '0%'
  and length(a.vin) = 17 
  and left(a.vin, 4) <> '1111'
group by a.vin;

select count(*) from arkona.ext_sdprhdr_history -- 1131008
select count(*) from greg.vins; -- 159176
select count(*) from greg.ros; -- 1126274

select max(open_date) from arkona.ext_sdprhdr_history

-- all vins visited any dept
drop table if exists greg.ros;
create table greg.ros (
  store_code citext not null,
  ro citext primary key,
  customer_key integer not null,
  customer_name citext not null,
  open_date date not null,
  vin citext not null,
  zip_code citext not null);
insert into greg.ros
select a.store_code, a.ro, a.customer_key, a.customer_name,
  a.open_date, a.vin, coalesce(a.zip_code, 'UNKNOWN')
from arkona.ext_sdprhdr_history a
inner join greg.vins b on a.vin = b.vin
where a.customer_name is not null
  and a.customer_key is not null;
create index on greg.ros(store_code);  
create index on greg.ros(ro);
create index on greg.ros(customer_key);
create index on greg.ros(customer_name);
create index on greg.ros(zip_code);
create index on greg.ros(left(ro,2));

alter table greg.vins
add column first_visit date,
add column single_visit boolean;

update greg.vins z
set first_visit = x.first_visit,
    single_visit = x.single_visit
from (    
  select a.vin, b.first_visit, 
    case
      when c.vin is not null then true
      else false
    end as single_visit
  from greg.vins a
  left join greg.first_visits b on a.vin = b.vin
  left join greg.single_visits c on a.vin = c.vin) x
where z.vin = x.vin;

alter table greg.vins
alter column first_visit set not null,
alter column single_visit set not null;




-- a few misc anomalies
delete from greg.vins where vin in ('3G5DB03E64S588009','1GCEK14H0GF318102','3FAHP07Z17R207435','1GKET16P736189873','1G1ZT54865F290883');
select count(*) from greg.ros -- 1126274
select store_code, count(*) from greg.ros group by store_code -- 897007/229267
select left(ro,2), count(*) from greg.ros where store_code = 'ry1' group by left(ro,2)

select max(open_date) from greg.ros where store_code = 'ry1' and ro not in ('13030485','320481') and left(ro,2) not in ('16','19', '18')
select * from greg.ros where open_date = '07/02/2010'

select * from greg.ros where extract(year from open_date) = 2007 limit 1000

--  first visits -------------------------------
drop table if exists greg.first_visits;
create table greg.first_visits (
  vin citext primary key,
  first_visit date not null);
create index on greg.first_visits(first_visit);

insert into greg.first_visits
select vin, min(open_date) as first_visit
from greg.ros
group by vin;

select extract(year from first_visit), count(*)
from greg.first_visits
group by extract(year from first_visit)
order by extract(year from first_visit);
--------------------------------------------------
-- single visits ---------------------------------
drop table if exists greg.single_visits;
create table greg.single_visits (
  vin citext primary key);
insert into greg.single_visits
select vin
from greg.ros
group by vin
having count(*) = 1;  

select count(*) from greg.single_visits; --44218
--------------------------------------------------



drop table if exists past_years cascade;
create temp table past_years as
select a.vin,
  case 
    when exists (
      select 1
      from greg.ros
      where vin = a.vin
        and store_code = 'ry2'
        and open_date between current_date - 365 and current_Date) then 1 else 0 end as past_year,     
  case 
    when exists (
      select 1
      from greg.ros
      where vin = a.vin
        and store_code = 'ry2'
        and open_date between current_date - (2*365) and current_Date -365) then 1 else 0 end as past_2_year,   
  case 
    when exists (
      select 1
      from greg.ros
      where vin = a.vin
        and store_code = 'ry2'
        and open_date between current_date - (3*365) and current_Date - (2*365)) then 1 else 0 end as past_3_year,   
  case 
    when exists (
      select 1
      from greg.ros
      where vin = a.vin
        and store_code = 'ry2'
        and open_date between current_date - (4*365) and current_Date - (3*365)) then 1 else 0 end as past_4_year,   
  case 
    when exists (
      select 1
      from greg.ros
      where vin = a.vin
        and store_code = 'ry2'
        and open_date between current_date - (5*365) and current_Date - (4*365)) then 1 else 0 end as past_5_year                                   
from greg.vins a;
  
create unique index on past_years(vin);
create index on past_years(past_year);
create index on past_years(past_2_year);
create index on past_years(past_3_year);
create index on past_years(past_4_year);
create index on past_years(past_5_year);

select *
from past_years
limit 1000

-- Market: 
-- RY1: 128619;114614;102571; 92335; 82236
-- RY2: 150270;146218;142686;139248;136126
select 
  sum(case when past_year = 0 then 1 else 0 end) as past_year,
  sum(case when past_year = 0 and past_2_year = 0 then 1 else 0 end) as past_2_year,
  sum(case when past_year = 0 and past_2_year = 0 and past_3_year = 0 then 1 else 0 end) as past_3_year,
  sum(case when past_year = 0 and past_2_year = 0 and past_3_year = 0 and past_4_year = 0 then 1 else 0 end) as past_4_year,
  sum(case when past_year = 0 and past_2_year = 0 and past_3_year = 0 and past_4_year = 0 and past_5_year = 0 then 1 else 0 end) as past_5_year
from past_years;  

----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
-- 8/18 filling out greg's "bad apple to bad apple" spreadsheet
-- the doubtful method other stores used for identifying lost vins
select count(distinct vin) -- 43534
from greg.ros
where store_code = 'ry2'

select count(distinct vin) -- 137836
from greg.ros
where store_code = 'ry1'

drop table if exists past_years cascade;
create temp table past_years as
select a.vin,
  case 
    when exists (
      select 1
      from greg.ros
      where vin = a.vin
        and store_code = 'ry1'
        and open_date between current_date - 365 and current_Date) then 1 else 0 end as past_year,     
  case 
    when exists (
      select 1
      from greg.ros
      where vin = a.vin
        and store_code = 'ry1'
        and open_date between current_date - (2*365) and current_Date -365) then 1 else 0 end as past_2_year,   
  case 
    when exists (
      select 1
      from greg.ros
      where vin = a.vin
        and store_code = 'ry1'
        and open_date between current_date - (3*365) and current_Date - (2*365)) then 1 else 0 end as past_3_year,   
  case 
    when exists (
      select 1
      from greg.ros
      where vin = a.vin
        and store_code = 'ry1'
        and open_date between current_date - (4*365) and current_Date - (3*365)) then 1 else 0 end as past_4_year,   
  case 
    when exists (
      select 1
      from greg.ros
      where vin = a.vin
        and store_code = 'ry1'
        and open_date between current_date - (5*365) and current_Date - (4*365)) then 1 else 0 end as past_5_year                                   
from greg.vins a
where exists (
  select 1
  from greg.ros
  where vin = a.vin
    and store_code = 'ry1')

select 
  sum(case when past_year = 0 then 1 else 0 end) as past_year,
  sum(case when past_year = 0 and past_2_year = 0 then 1 else 0 end) as past_2_year,
  sum(case when past_year = 0 and past_2_year = 0 and past_3_year = 0 then 1 else 0 end) as past_3_year,
  sum(case when past_year = 0 and past_2_year = 0 and past_3_year = 0 and past_4_year = 0 then 1 else 0 end) as past_4_year,
  sum(case when past_year = 0 and past_2_year = 0 and past_3_year = 0 and past_4_year = 0 and past_5_year = 0 then 1 else 0 end) as past_5_year
from past_years;  


select 
  sum(case when past_year = 1 then 1 else 0 end) as past_year,
  sum(case when past_year = 1 or past_2_year = 1 then 1 else 0 end) as past_2_year,
  sum(case when past_year = 1 or past_2_year = 1 or past_3_year = 1 then 1 else 0 end) as past_3_year,
  sum(case when past_year = 1 or past_2_year = 1 or past_3_year = 1 or past_4_year = 1 then 1 else 0 end) as past_4_year,
  sum(case when past_year = 1 or past_2_year = 1 or past_3_year = 1 or past_4_year = 1 or past_5_year = 1 then 1 else 0 end) as past_5_year
from past_years;  

----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
--  used base_ros and base_vins
drop table if exists past_years cascade;
create temp table past_years as
select a.vin,
  case 
    when exists (
      select 1
      from base_ros
      where vin = a.vin
        and left(ro,2) = '18'
        and open_date between current_date - 365 and current_Date) then 1 else 0 end as past_year,     
  case 
    when exists (
      select 1
      from base_ros
      where vin = a.vin
        and left(ro,2) = '18'
        and open_date between current_date - (2*365) and current_Date -365) then 1 else 0 end as past_2_year,   
  case 
    when exists (
      select 1
      from base_ros
      where vin = a.vin
        and left(ro,2) = '18'
        and open_date between current_date - (3*365) and current_Date - (2*365)) then 1 else 0 end as past_3_year,   
  case 
    when exists (
      select 1
      from base_ros
      where vin = a.vin
        and left(ro,2) = '18'
        and open_date between current_date - (4*365) and current_Date - (3*365)) then 1 else 0 end as past_4_year,   
  case 
    when exists (
      select 1
      from base_ros
      where vin = a.vin        
        and left(ro,2) = '18'
        and open_date between current_date - (5*365) and current_Date - (4*365)) then 1 else 0 end as past_5_year                                   
from base_vins a;


-- using base_vin/base_ros, 104865 unique vins, 8/1/2009 -> current_date
-- body shop: 99770;95204;91267;87538;83855
-- pdq:       84607;76447;69626;62226;55218
-- main shop: 87848;76605;66270;59034;51421
-- all ros:   74264;60262;48223;38025;27915

-- inverted
-- body shop:  5095; 9661;13598;17327;21010
-- pdq:       20258;28418;35239;42639;49647
-- main shop: 17017;28260;38595;45831;53444 
-- all ros:   30601;44603;56642;66840;76951
select 
  sum(case when past_year = 0 then 1 else 0 end) as past_year,
  sum(case when past_year = 0 and past_2_year = 0 then 1 else 0 end) as past_2_year,
  sum(case when past_year = 0 and past_2_year = 0 and past_3_year = 0 then 1 else 0 end) as past_3_year,
  sum(case when past_year = 0 and past_2_year = 0 and past_3_year = 0 and past_4_year = 0 then 1 else 0 end) as past_4_year,
  sum(case when past_year = 0 and past_2_year = 0 and past_3_year = 0 and past_4_year = 0 and past_5_year = 0 then 1 else 0 end) as past_5_year
from past_years;  

select 
  sum(case when past_year = 1 then 1 else 0 end) as past_year,
  sum(case when past_year = 1 or past_2_year = 1 then 1 else 0 end) as past_2_year,
  sum(case when past_year = 1 or past_2_year = 1 or past_3_year = 1 then 1 else 0 end) as past_3_year,
  sum(case when past_year = 1 or past_2_year = 1 or past_3_year = 1 or past_4_year = 1 then 1 else 0 end) as past_4_year,
  sum(case when past_year = 1 or past_2_year = 1 or past_3_year = 1 or past_4_year = 1 or past_5_year = 1 then 1 else 0 end) as past_5_year
from past_years;  

drop table if exists base_vins;
create temp table base_vins as 
select distinct vin
from greg.ros
where store_code = 'ry1'
  and open_date > '07/31/2009'
  and left(ro,2) in ('16','18','19');
create unique index on base_vins(vin);

drop table if exists base_ros;
create temp table base_ros as
select *
from greg.ros
where store_code = 'ry1'
  and open_date > '07/31/2009'
  and left(ro,2) in ('16','18','19');
create index on base_ros(vin);
create index on base_ros(ro);
create index on base_ros(left(ro,2));
create index on base_ros(open_date);


-- single visit by year/dept
select extract(year from open_date), left(ro,2), count(*) 
from greg.ros a
inner join greg.vins b on a.vin = b.vin
  and b.single_visit = true
where left(a.ro, 2) in ('16','18','19')  
group by extract(year from open_date), left(ro,2)

-- main shop, 2015 single visit vins
select *
from greg.ros a
inner join greg.vins b on a.vin = b.vin
  and b.single_visit = true
where extract(year from open_date) = 2015
  and left(ro,2) = '16'  

-- new vins each year
-- 10821;11750;11807;10876;10988
select 
  sum(case when b.first_visit between current_date - 365 and current_date then 1 else 0 end) as new_past_year,
  sum(case when b.first_visit between current_date - (2*365) and current_Date -365 then 1 else 0 end) as new_past_2_year,
  sum(case when b.first_visit between current_date - (3*365) and current_Date - (2*365) then 1 else 0 end) as new_past_3_year,
  sum(case when b.first_visit between current_date - (4*365) and current_Date - (3*365) then 1 else 0 end) as new_past_4_year,
  sum(case when b.first_visit between current_date - (5*365) and current_Date - (4*365) then 1 else 0 end) as new_past_5_year
from greg.vins b
 
 
-- new vins each year by dept
-- main shop: 6425;7347;7427;4954;5456
-- pdq:       2874;3122;3257;3934;3916  
-- body shop:  584; 578; 495; 541; 754
select 
  sum(case when b.first_visit between current_date - 365 and current_date then 1 else 0 end) as new_past_year,
  sum(case when b.first_visit between current_date - (2*365) and current_Date -365 then 1 else 0 end) as new_past_2_year,
  sum(case when b.first_visit between current_date - (3*365) and current_Date - (2*365) then 1 else 0 end) as new_past_3_year,
  sum(case when b.first_visit between current_date - (4*365) and current_Date - (3*365) then 1 else 0 end) as new_past_4_year,
  sum(case when b.first_visit between current_date - (5*365) and current_Date - (4*365) then 1 else 0 end) as new_past_5_year
from greg.ros a
inner join greg.vins b on a.vin = b.vin and a.open_date = b.first_visit  
where left(a.ro,2) = '18'


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

-- for August greg wants the number of new vins per month in 2017
select left(ro,2), extract(month from b.first_visit), count(*)
from greg.ros a
inner join greg.vins b on a.vin = b.vin 
  and a.open_date = b.first_visit
where extract(year from b.first_visit) = 2017
  and a.store_code = 'ry1'
group by left(ro,2), extract(month from b.first_visit)
order by left(ro,2), extract(month from b.first_visit)

-- these are the numbers i sent to greg
select c.month_name, count(*)
from greg.ros a
inner join greg.vins b on a.vin = b.vin 
  and a.open_date = b.first_visit
inner join dds.dim_date c on b.first_visit = c.the_date  
where extract(year from b.first_visit) = 2017
  and a.store_code = 'ry1'
group by c.month_name, c.year_month
order by c.year_month

-- 9/1 he wants to see it broken down by dept
select month_name, 
  case 
    when dept = '16' then 'main_shop'
    when dept = '18' then 'body_shop'
    when dept = '19' then 'pdq'
  end, vins
 from (
  select c.month_name, left(ro,2) as dept, count(*) as vins
  from greg.ros a
  inner join greg.vins b on a.vin = b.vin 
    and a.open_date = b.first_visit
  inner join dds.dim_date c on b.first_visit = c.the_date  
  where extract(year from b.first_visit) = 2017
    and a.store_code = 'ry1'
  group by c.month_name, c.year_month, left(ro,2)
  order by c.year_month, left(ro,2)) x

-- 9/13 ry2
-- these are the numbers i sent to greg
select c.month_name, count(*)
from greg.ros a
inner join greg.vins b on a.vin = b.vin 
  and a.open_date = b.first_visit
inner join dds.dim_date c on b.first_visit = c.the_date  
where extract(year from b.first_visit) = 2017
  and a.store_code = 'ry2'
group by c.month_name, c.year_month
order by c.year_month