﻿-- 6/20 
-- main shop/pdq customer pay parts/service accounts
-- 1 row per account
drop table if exists cp_accounts cascade;
create temp table cp_accounts as
select b.g_l_acct_number as account
from arkona.ext_eisglobal_sypffxmst a
inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
inner join fin.dim_account c on b.g_l_acct_number = c.account  
where a.fxmcyy = 2017
  and c.account_type = 'sale'
  and a.fxmpge = 16 and a.fxmlne in (21,23, 46,47,48) -- add parts accounts
  and c.department_code in ('SD', 'PD', 'QL'); -- excludes detail accounts
create unique index on cp_accounts(account);  

-- drop table if exists ros;
-- create temp table ros as
-- select a.storecode as store, a.ro, c.vin, a.rocreatedts::date as ro_date, 
--   string_agg(distinct 
--     case
--       when servicetypekey = 8 then 'MR'::citext
--       when servicetypekey = 9 then 'QL'::citext
--       when servicetypekey = 13 then 'MN'::citext
--     end, '|') as service_type
-- from ads.ext_fact_repair_order a
-- inner join dds.dim_date b on a.opendatekey = b.date_key
-- inner join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey
-- where a.paymenttypekey = 2  -- customer pay
--   and a.servicetypekey in (8,9,13) -- MR, QL, MN
--   and a.opcodekey not in (10492, 10494, 10495, 10496) -- exclude multipoint inspection, shows as customer pay
--   and left(ro, 1) = '1'
-- group by a.storecode, c.vin, a.ro, a.rocreatedts::date;

-- 6/20 add ro sales amount (parts & labor)
drop table if exists ros cascade;
create temp table ros as  
select aa.*, coalesce(bb.amount, 0) as amount
from (
  select a.storecode as store, a.ro, c.vin, a.rocreatedts::date as ro_date, 
    string_agg(distinct 
      case
        when servicetypekey = 8 then 'MR'::citext
        when servicetypekey = 9 then 'QL'::citext
        when servicetypekey = 13 then 'MN'::citext
      end, '|') as service_type
  from ads.ext_fact_repair_order a
  inner join dds.dim_date b on a.opendatekey = b.date_key
  inner join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey
      -- exclude multipoint/pdq inspection, drot, thank you opcodes, shows as customer pay
  inner join ads.ext_dim_opcode d on a.opcodekey = d.opcodekey
    and d.opcodekey not in (10492,10494,10495,10496,10497,2835,14826,24221,24222,24223,24224,24225,24226,24227,
      24228,24828,25341,25497,26630,2830,14821 ) 
    and d.description not like 'BASED%'
  where a.paymenttypekey = 2  -- customer pay
    and a.servicetypekey in (8,9,13) -- MR, QL, MN
    and left(ro, 1) = '1'
  group by a.storecode, c.vin, a.ro, a.rocreatedts::date) aa
left join (
  select control, sum(-amount) as amount
  from fin.fact_gl a
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join cp_accounts f on c.account = f.account
  inner join dds.dim_date g on a.date_key = g.date_key
    and g.year_month > 201112
  where a.post_status = 'Y'
  group by control) bb on aa.ro = bb.control; 
create unique index on ros(ro);

drop table if exists recon_package cascade;
create temp table recon_package as
select a.vehicleinventoryitemid, right(typ, length(typ) - position('_' in typ)) as recon_package
from ads.ext_selected_recon_packages a
inner join (
  select vehicleinventoryitemid, max(selectedreconpackagets) as selectedreconpackagets
  from ads.ext_selected_recon_packages
  where typ is not null 
    and vehicleinventoryitemid is not null
    and vehicleinventoryitemid not in ('8D99D07C-8B1E-4466-ACCF-0D45AFAC6477',
      '74F366D8-AF69-4E5D-A53A-C44293A58B6E','598F683B-0782-4058-A4DD-5A5C83A7D019',
      'A627DFA8-7D09-4AEC-84C8-B608B99B7B61','45E0FBDE-10C0-4C25-A15E-67275D2E1685',
      '352A1BDE-D7E9-4BA9-96D1-6A7543AD38DB')
  group by vehicleinventoryitemid) b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
    and a.selectedreconpackagets = b.selectedreconpackagets
group by a.vehicleinventoryitemid, right(typ, length(typ) - position('_' in typ));
create unique index on recon_package(vehicleinventoryitemid);    

drop table if exists sales cascade; 
create temp table sales as
/*
sales from 2012 -> current
6/20: need zipcode: get it from ads.ext_dim_customer
*/
-- select a.vin, b.stocknumber, b.fromts::date, b.thruts::date, c.vehicleprice, 
--   d.the_date as sale_date, e.saletype, f.package
select 
  case 
    when b.owninglocationid = '4CD8E72C-DC39-4544-B303-954C99176671' then 'RY2'
    when b.owninglocationid = 'B6183892-C79D-4489-A58C-B526DF948B06' then 'RY1'
  end as store,
  vin, c.stocknumber as stock_number, vehicleprice::integer as price, saletype as sale_type, 
  recon_package, d.the_date as sale_date, 
  coalesce(lead(fromts, 1) over (partition by vin order by b.fromts), '12/31/9999')::date as thru_date,
  g.zip 
from ads.ext_vehicle_items a
inner join ads.ext_vehicle_inventory_items b on a.vehicleitemid = b.vehicleitemid
inner join ads.ext_fact_vehicle_sale c on b.stocknumber = c.stocknumber
inner join dds.dim_date d on c.cappeddatekey = d.date_key
inner join ads.ext_dim_car_deal_info e on c.cardealinfokey = e.cardealinfokey
inner join recon_package f on b.vehicleinventoryitemid = f.vehicleinventoryitemid
left join ads.ext_dim_customer g on c.buyerkey = g.customerkey
where b.owninglocationid in ('B6183892-C79D-4489-A58C-B526DF948B06','4CD8E72C-DC39-4544-B303-954C99176671')
  and d.the_date > '12/31/2011';
create unique index on sales(stock_number,sale_date);
create unique index on sales(vin,sale_date);

select * from sales limit 1000

drop table if exists bod_uc_service_retention cascade;
create temp table bod_uc_service_retention as
select a.*, b.ro, b.amount as sales, service_type, b.ro_date, b.ro_date - a.sale_date as days_from_sale
from sales a
left join ros b on a.vin = b.vin
  and b.ro_date between a.sale_date and a.thru_date;
create unique index on bod_uc_service_retention(stock_number,sale_date,coalesce(ro,'none'));
create unique index on bod_uc_service_retention(vin,sale_date,coalesce(ro,'none'));  

create index on sales(vin);
create index on sales(sale_date);
create index on sales(thru_date);
create index on sales(store);
create index on sales(zip);
create index on ros(vin);
create index on ros(ro_date);
create index on ros(ro);
create index on ros(store);

create index on bod_uc_service_retention(store);
create index on bod_uc_service_retention(vin);
create index on bod_uc_service_retention(stock_number);
create index on bod_uc_service_retention(sale_date);

----------------------------------------------------------------------------------------------------------------------------------------  
----------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------



-- service (main shop & pdq) customer pay sale accounts (FS: P6 L21-23)
select 
  case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code,
  a.fxmpge as the_page, a.fxmlne::integer as line, trim(b.g_l_acct_number) as gl_account, fxmact, c.account_type
from arkona.ext_eisglobal_sypffxmst a
inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
inner join fin.dim_account c on b.g_l_acct_number = c.account  
where a.fxmcyy = 2017
--   and trim(a.fxmcde) = 'GM'
--   and coalesce(b.consolidation_grp, 'RY1') in('2','RY1')
--   and b.factory_code = 'GM'
--   and trim(b.factory_account) <> '331A'
  and c.account_type = 'sale'
  and a.fxmpge = 16 and a.fxmlne in (21,23)
order by line, case b.consolidation_grp when '2' then 'RY2' else 'RY1' end, fxmact, gl_Account



  
-- vehicles with 3 or more inventory instances
SELECT stocknumber, fromts, thruts, vin
FROM ads.ext_vehicle_inventory_items a
INNER JOIN ads.ext_vehicle_items b on a.VehicleItemID = b.VehicleItemID
INNER JOIN (
  SELECT VehicleItemID
  FROM ads.ext_vehicle_inventory_items 
  where left(stocknumber, 1) not in ('C', 'H')
  GROUP BY VehicleItemID 
  HAVING COUNT(*) >= 3) c on a.VehicleItemID = c.VehicleItemID 
ORDER BY vin, fromts  

-- the last recon package per vehicleinventoryitem
select vehicleinventoryitemid, right(typ, length(typ) - position('_' in typ))
-- select *
from ads.ext_selected_recon_packages a
where typ is not null
  and vehicleinventoryitemid is not null 
  and selectedreconpackagets = (
    select max(selectedreconpackagets)
    from ads.ext_selected_recon_packages
    where vehicleinventoryitemid = a.vehicleinventoryitemid)
limit 100


-- VIN 3GSCL33P38S690673 --------------------------------------------------------------------------
/* BOPVREF */
BVTYPE (TYPE_CODE) Customer/Vehicle Reference
S = Buyer on the Deal
2 = Co-Buyer on the Deal
C = Service Customer
T = Trade In
4 = ?? (Of the 212409 rows in BOPVREF table, only 59 are type 4)

select type_code, count(*)
from arkona.ext_bopvref
group by type_code;


select a.customer_key, a.vin, a.start_date, a.end_date, a.type_code,
  (select arkona.db2_integer_to_date_long(a.start_date)), 
  (select arkona.db2_integer_to_date_long(a.end_date))
from arkona.ext_bopvref a
where vin = '3GSCL33P38S690673'
order by start_date


select a.customer_key, a.vin, a.start_date, a.end_date, a.type_code,
  (select arkona.db2_integer_to_date_long(a.start_date)), 
  (select arkona.db2_integer_to_date_long(a.end_date)),
  b.*
from arkona.ext_bopvref a
left join arkona.ext_bopname b on a.customer_key = b.bopname_record_key
where a.vin = '3GSCL33P38S690673'
order by a.start_date


select b.the_date, c.account, a.control, a.doc, c.description, d.journal_code, 
  a.amount, e.description, account_type
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and journal_code = 'VSU'
  and account_type in ('asset','sale')
  and control = '24857b'


select b.the_date, c.account, a.control, a.doc, c.description, d.journal_code, 
  a.amount, e.description, account_type, 
  a.*
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and journal_code = 'VSU'
  and account_type in ('asset','sale')
  and control = '29032xxa'

-- VIN 3GSCL33P38S690673 --------------------------------------------------------------------------

-- used car inventory accounts
select *
from fin.dim_Account
where account_type = 'asset'
  and description like '%inv%'
  and department_code = 'UC'
order by account

-- it appears the trouble with accounting is that there is no reference to the vin


select e.the_date, b.account, d.journal_key, a.*, f.description
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
inner join cp_accounts c on b.account = c.account
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join dds.dim_date e on a.date_key = e.date_key
inner join fin.dim_gl_description f on a.gl_description_key = f.gl_description_key
where e.the_date > '12/31/2016'


select min(date_capped), max(date_capped) from arkona.ext_bopmast_tmp where date_capped > '01/01/0001'

-- excludable dups
select bopmast_stock_number, bopmast_vin, count(*)
from arkona.ext_bopmast
where bopmast_stock_number is not null
  and bopmast_vin is not null
  and record_status = 'U'
  and date_capped > '12/31/2010'
  and vehicle_Type = 'U'
group by bopmast_stock_number, bopmast_vin
having count(*) > 1


select bopmast_vin, count(*)
from (
  select bopmast_stock_number, bopmast_vin
  from arkona.ext_bopmast
  where bopmast_stock_number is not null
    and bopmast_vin is not null
    and record_status = 'U'
    and date_capped > '12/31/2010'
    and vehicle_Type = 'U'
  group by bopmast_stock_number, bopmast_vin) a
group by bopmast_vin
having count(*) > 1
order by bopmast_vin

select bopmast_stock_number, bopmast_vin, bopmast_Search_name, record_status, date_capped, vehicle_type
from arkona.ext_bopmast
where bopmast_vin = '3GSCL33P38S690673'



select *
from arkona.ext_bopvref
where vin = '1G1ZE5ST0GF275929'





      
select a.vin, b.stocknumber, b.fromts::date, b.thruts::date, c.vehicleprice, 
  d.the_date as sale_date, e.saletype, f.package
from ads.ext_vehicle_items a
inner join ads.ext_vehicle_inventory_items b on a.vehicleitemid = b.vehicleitemid
inner join ads.ext_fact_vehicle_sale c on b.stocknumber = c.stocknumber
inner join dds.dim_date d on c.cappeddatekey = d.date_key
inner join ads.ext_dim_car_deal_info e on c.cardealinfokey = e.cardealinfokey
inner join recon_package f on b.vehicleinventoryitemid = f.vehicleinventoryitemid
where vin in ('3GSCL33P38S690673','1G4HD57227U155790','1G2ZG57N584115977')
order by vin, fromts

select min(b.the_date), max(the_date)
from ads.ext_fact_vehicle_sale a
inner join dds.dim_date b on a.cappeddatekey = b.date_key
where b.the_date < current_date
limit 10


select * from ads.ext_vehicle_inventory_items limit 10


  
select * 
from sales a
where exists (
  select 1
  from sales
  where vin = a.vin 
    and thru_date < current_Date)
order by vin, sale_date    

select min(b.the_date), max(b.the_date)  -- 2/17/12 -> 6/19/17
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.opendatekey = b.date_key





select * from bod_uc_service_retention where left(stock_number, 1) <> 'H' and left(ro, 1) = '1'


-- multiple instances
select a.*, b.ro, b.ro_date, b.ro_date - a.sale_date
from sales a
left join ros b on a.vin = b.vin
  and b.ro_date between a.sale_date and a.thru_date
where sale_date > '12/31/2011'  
  and a.vin in (
    select vin
    from (
      select vin, stock_number
      from sales
      group by vin, stock_number) a
    group by vin having count(*) > 2)
order by a.vin, sale_date, ro_date

-- maximum ownership instances
select stock_number, count(*) -- fuck, 33
from bod_uc_service_retention
group by stock_number
order by count(*) desc 


select *
from bod_uc_service_retention
where ro is not null
order by vin, ro_date


select store, extract(year from sale_date), count(distinct stock_number)
from bod_uc_service_retention
group by store, extract(year from sale_date)

select *
from bod_uc_service_retention
where extract(year from sale_date) = 9999

select *
from bod_uc_service_retention
where vin = '1C3BC2FG1BN571992'


select *
from bod_uc_service_retention
where store = 'RY1'
  and recon_package = 'Nice'
  and sale_type = 'retail'
order by vin, ro_date  


-- 6/20/17
/*
1 row per ownership add columns for years

*/

select store, vin, stock_number, recon_package, sale_date, thru_date, ro_date, sales, 
  case 
    when thru_date > current_date then current_date - sale_date
    else thru_date - sale_date
  end as days_owned,
  case when ro_date between sale_date and  (sale_date + interval '1 year')::Date then 'X' else '' end as year_one,
  case when ro_date between (sale_date + interval '1 year')::Date and  (sale_date + interval '2 year')::Date then 'X' else '' end as year_two,
  case when ro_date between (sale_date + interval '2 year')::Date and  (sale_date + interval '3 year')::Date then 'X' else '' end as year_three,
  case when ro_date between (sale_date + interval '3 year')::Date and  (sale_date + interval '4 year')::Date then 'X' else '' end as year_four,
  case when ro_date between (sale_date + interval '4 year')::Date and '9999-12-31'::Date then 'X' else '' end as beyond
from bod_uc_service_retention
where store = 'RY1'
  and sale_type = 'retail'
  and vin = '1G11H5SA3DU138428'
order by vin, stock_number, ro_date  
limit 1000

-- now try to group it
-- V2 spreadsheet
drop table if exists v2 cascade;
create table v2 as
select store, zip, vin, stock_number, recon_package, sale_type, sale_date, thru_date, 
  round(case 
    when thru_date > current_date then current_date - sale_date
    else thru_date - sale_date
  end/365.0, 1)as years_owned,
  sum(case when ro_date between sale_date and  (sale_date + interval '1 year')::Date then 1 else null end) as year_one,
  sum(case when ro_date between (sale_date + interval '1 year')::Date and  (sale_date + interval '2 year')::Date then 1 else null end) as year_two,
  sum(case when ro_date between (sale_date + interval '2 year')::Date and  (sale_date + interval '3 year')::Date then 1 else null end) as year_three,
  sum(case when ro_date between (sale_date + interval '4 year')::Date and '9999-12-31'::Date then 1 else null end) as beyond,
  sum(case when ro_date is not null then 1 else null end) as overall,
  sum(sales) as sales
from bod_uc_service_retention
where store = 'RY1'
  and sale_date <> '9999-12-31'
group by store, zip, vin, stock_number, recon_package, sale_type, sale_date, thru_date,
  case 
    when thru_date > current_date then current_date - sale_date
    else thru_date - sale_date
  end  
order by vin, sale_date


-- 6/22 report specs

select * 
from v2
where sale_type = 'retail'
  and extract(year from sale_date) = 2012
order by years_owned

select extract(year from sale_date) as sold_in, recon_package, 
  count(*) as total_sold,
  sum(case when year_one is not null then 1 end) as year_one_retained,
  sum(case when years_owned > 1 and year_two is not null then 1 end) as year_two_retained,
  sum(case when years_owned > 2 and year_three is not null then 1 end) as year_three_retained,
  sum(case when years_owned > 3 and beyond is not null then 1 end) as beyond_retained
from v2
where sale_type = 'retail'
group by recon_package, extract(year from sale_date) 
order by extract(year from sale_date), recon_package

--V3 spreadsheet
select sold_in, recon_package, total_sold, 
  year_one_retained, round(100*year_one_retained/total_sold, 1) as perc_ret_1,
  year_two_retained, round(100*year_two_retained/total_sold, 1) as perc_ret_2,
  year_three_retained, round(100*year_three_retained/total_sold, 1) as perc_ret_3,
  beyond_retained, round(100*beyond_retained/total_sold, 1) as perc_ret_4
from (
  select extract(year from sale_date) as sold_in, recon_package, 
    count(*) as total_sold,
    sum(case when year_one is not null then 1 end) as year_one_retained,
    sum(case when years_owned > 1 and year_two is not null then 1 end) as year_two_retained,
    sum(case when years_owned > 2 and year_three is not null then 1 end) as year_three_retained,
    sum(case when years_owned > 3 and beyond is not null then 1 end) as beyond_retained
  from v2
  where sale_type = 'retail' 
  group by recon_package, extract(year from sale_date)) a
order by sold_in, recon_package  
-- order by perc_ret_1

--V4 spreadsheet
select sold_in, 'Nice Care' as recon_package,
  round(100*year_one_retained/total_sold, 1) as perc_ret_1,
  round(100*year_two_retained/total_sold, 1) as perc_ret_2,
  round(100*year_three_retained/total_sold, 1) as perc_ret_3,
  round(100*beyond_retained/total_sold, 1) as perc_ret_4
from (
  select extract(year from sale_date) as sold_in, recon_package, 
    count(*) as total_sold,
    sum(case when year_one is not null then 1 end) as year_one_retained,
    sum(case when years_owned > 1 and year_two is not null then 1 end) as year_two_retained,
    sum(case when years_owned > 2 and year_three is not null then 1 end) as year_three_retained,
    sum(case when years_owned > 3 and beyond is not null then 1 end) as beyond_retained
  from v2
  where sale_type = 'retail' 
    and recon_package = 'Nice'
  group by recon_package, extract(year from sale_date)) a
union all
select sold_in, 'All Other', 
  round(100*year_one_retained/total_sold, 1) as perc_ret_1,
  round(100*year_two_retained/total_sold, 1) as perc_ret_2,
  round(100*year_three_retained/total_sold, 1) as perc_ret_3,
  round(100*beyond_retained/total_sold, 1) as perc_ret_4
from (
  select extract(year from sale_date) as sold_in,
    count(*) as total_sold,
    sum(case when year_one is not null then 1 end) as year_one_retained,
    sum(case when years_owned > 1 and year_two is not null then 1 end) as year_two_retained,
    sum(case when years_owned > 2 and year_three is not null then 1 end) as year_three_retained,
    sum(case when years_owned > 3 and beyond is not null then 1 end) as beyond_retained
  from v2
  where sale_type = 'retail' 
    and recon_package <> 'Nice'
  group by extract(year from sale_date)) a
order by sold_in, recon_package desc


-- 7/30/17
/*
The information that Greg is looking for at the BOD is the amount of customer pay 
work a Nice Care customer spends at our store the first year vs. what a non-Nice Care 
customer spends at our store the first year. Then the same thing for
 the second year of ownership. Let me know what you need
 */

-- sounds like redoing the V2 query with $$ spent rather than perc retained

drop table if exists cp_sales_per_year cascade;
create table cp_sales_per_year as
select store, zip, vin, stock_number, recon_package, sale_type, sale_date, thru_date, 
  round(case 
    when thru_date > current_date then current_date - sale_date
    else thru_date - sale_date
  end/365.0, 1)as years_owned,
  sum(case when ro_date between sale_date and  (sale_date + interval '1 year')::Date then sales else null end) as year_one_sales,
  sum(case when ro_date between (sale_date + interval '1 year')::Date and  (sale_date + interval '2 year')::Date then sales else null end) as year_two_sales,
  sum(case when ro_date between (sale_date + interval '2 year')::Date and  (sale_date + interval '3 year')::Date then sales else null end) as year_three_sales,
  sum(case when ro_date between (sale_date + interval '4 year')::Date and '9999-12-31'::Date then sales else null end) as beyond_sales,
  sum(case when ro_date is not null then 1 else null end) as overall,
  sum(sales) as sales
from bod_uc_service_retention
where store = 'RY1'
  and sale_date <> '9999-12-31'
group by store, zip, vin, stock_number, recon_package, sale_type, sale_date, thru_date,
  case 
    when thru_date > current_date then current_date - sale_date
    else thru_date - sale_date
  end;  
create unique index on cp_sales_per_year(stock_number,sale_date);

-- and then a modified version of V4 spreadsheet query to do sales
-- V5: cp_sales_by_recon_package
select extract(year from sale_date) as year_sold, recon_package, 
  count(*) as total_sold,
  sum(year_one_sales)::integer as  year_one_cp_sales,
  sum(year_two_sales)::integer as  year_two_cp_sales,
  sum(year_three_sales)::integer as  year_three_cp_sales,
  sum(beyond_sales)::integer as  beyond_cp_sales
from cp_sales_per_year
where sale_type = 'retail' 
  and recon_package = 'Nice'
group by recon_package, extract(year from sale_date)
union
select extract(year from sale_date) as year_sold, 'All Other', 
  count(*) as total_sold,
  sum(year_one_sales)::integer as  year_one_cp_sales,
  sum(year_two_sales)::integer as  year_two_cp_sales,
  sum(year_three_sales)::integer as  year_three_cp_sales,
  sum(beyond_sales)::integer as  beyond_cp_sales
from cp_sales_per_year
where sale_type = 'retail' 
  and recon_package <> 'Nice'
group by extract(year from sale_date)
order by year_sold, recon_package desc


-- 7/31/ greg says: per vin
-- bod_service_cp_sales_by_recon_package_V2
select extract(year from sale_date) as year_sold, vin, recon_package, 
  coalesce(sum(year_one_sales), 0)::integer as  year_one_cp_sales,
  coalesce(sum(year_two_sales), 0)::integer as  year_two_cp_sales,
  coalesce(sum(year_three_sales), 0)::integer as  year_three_cp_sales,
  coalesce(sum(beyond_sales), 0)::integer as  beyond_cp_sales
from cp_sales_per_year
where sale_type = 'retail' 
  and recon_package = 'Nice'
group by recon_package, vin, extract(year from sale_date)
union
select extract(year from sale_date) as year_sold, vin, 'All Other',
  coalesce(sum(year_one_sales), 0)::integer as  year_one_cp_sales,
  coalesce(sum(year_two_sales), 0)::integer as  year_two_cp_sales,
  coalesce(sum(year_three_sales), 0)::integer as  year_three_cp_sales,
  coalesce(sum(beyond_sales), 0)::integer as  beyond_cp_sales
from cp_sales_per_year
where sale_type = 'retail' 
  and recon_package <> 'Nice'
group by vin, extract(year from sale_date)
order by year_sold, recon_package desc


-- 8/3
what he really wants is avg spend per vin nice vs other
drop table if exists spend;
create temp table spend as
select extract(year from sale_date) as year_sold, vin, recon_package, 
  coalesce(sum(year_one_sales), 0)::integer as  year_one_cp_sales,
  coalesce(sum(year_two_sales), 0)::integer as  year_two_cp_sales,
  coalesce(sum(year_three_sales), 0)::integer as  year_three_cp_sales,
  coalesce(sum(beyond_sales), 0)::integer as  beyond_cp_sales
from cp_sales_per_year
where sale_type = 'retail' 
  and recon_package = 'Nice'
group by recon_package, vin, extract(year from sale_date)
union
select extract(year from sale_date) as year_sold, vin, 'All Other',
  coalesce(sum(year_one_sales), 0)::integer as  year_one_cp_sales,
  coalesce(sum(year_two_sales), 0)::integer as  year_two_cp_sales,
  coalesce(sum(year_three_sales), 0)::integer as  year_three_cp_sales,
  coalesce(sum(beyond_sales), 0)::integer as  beyond_cp_sales
from cp_sales_per_year
where sale_type = 'retail' 
  and recon_package <> 'Nice'
group by vin, extract(year from sale_date)
order by year_sold, recon_package desc;

select 'year_one' as the_year, recon_package, count(*) as vehicles, sum(year_one_cp_sales) as cp_sales, 
  round(sum(year_one_cp_sales)/count(*), 0) as avg_cp_sales_per_vehicle
from (
  select vin, recon_package, year_one_cp_sales
  from spend
  where year_one_cp_sales <> 0) a
group by recon_package  
union
select 'year_two', recon_package, count(*), sum(year_two_cp_sales), round(sum(year_two_cp_sales)/count(*), 0) 
from (
  select vin, recon_package, year_two_cp_sales
  from spend
  where year_two_cp_sales <> 0) a
group by recon_package  
order by the_year, recon_package desc