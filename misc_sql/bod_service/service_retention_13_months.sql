﻿/*
4/14/2018
vehicles sold in jan/feb 2017 for which a non internal ro has been opened within 13 months of sale
eg
Jan 17 RY1 New 150 Sold / 46 retained

*/


drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select store, page, line, line_label, control, sum(unit_count) as unit_count, max(the_date) as the_date
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count,
    b.the_date
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201701--------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201701 -------------------------------------------------------------------
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
--         or
--         (b.page = 17 and b.line between 1 and 20))-- f/i
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by store, page, line, line_label, control
order by store, page, line;

select * from step_1 order by the_date

drop table if exists sales_201701;
create temp table sales_201701 as
select 201701 as year_month, a.*, b.vin, b.vehicle_type
from step_1 a
inner join sls.ext_bopmast_partial b on a.control = b.stock_number
where unit_count = 1
  and ((a.page between 5 and 14) and ((a.line between 1 and 19) or (a.line between 25 and 40)))
  or (page = 16 and line between 1 and 6);

drop table if exists sales;
create temp table sales as
select *
from sales_201701
union all
select *
from sales_201702;

select * from sales

drop table if exists ros;
create temp table ros as
select a.ro, b.the_date, d.vin, c.paymenttype
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.opendatekey = b.date_key
  and b.the_date > '12/31/2016'
inner join ads.ext_dim_payment_type c on a.paymenttypekey = c.paymenttypekey
  and c.paymenttype <> 'internal'  
inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
group by a.ro, b.the_date, d.vin, c.paymenttype

alter table sales
rename column "?column?" to year_month


-- Retained: non internal RO within 13 months of sale
select store, year_month, 
  case vehicle_type
    when 'N' then 'New'
    when 'U' then 'Used'
  end as new_used,
  count(*) as sold, 
  count(1) filter (where max_serv_date is not null) as retained
from (  
  select a.year_month, a.store, a.control, a.the_date as sale_date, a.vehicle_type, max(b.the_date) as max_serv_date, count(*) - 1 as ros
  from sales a
  left join ros b on a.vin = b.vin
    and b.the_date > a.the_date
    and b.the_date between a.the_date + 1 and a.the_date + interval '13 months'
  group by a.year_month, a.store, a.control, a.the_date, a.vehicle_type ) x
group by year_month, store, vehicle_type  
order by store, year_month, vehicle_type

---------------------------------------------------------------------------------------------------------------------------------------
-- Can you run it with retained defined as:
-- 
-- Non internal RO within 12 months of sale with oil change or rotate
need to narrow rows down to oil change/rotate

select * 
from ads.ext_dim_opcode
where description like '%rotat%'
'FREEROT','FREE10B','M0021','M1','M2','PDQDP','Q9','ROT','ROTD','ROTEMP','ROTP','Z2263','Z2334','Z2342','Z2357','0040072',
'15KC','15KD','15KT','30KC','30KD','30KT','6ROT','9','FROT','P71203','ROT','ROTATE','YW15AA','24G','ROTF','ROTEMP','WROT','110009A'

select * 
from ads.ext_dim_opcode
where description like '%qts%'

select *
from ads.ext_dim_opcode
where pdqcat1 <> 'n/a'
  or pdqcat2 <> 'n/a'
  or pdqcat3 <> 'n/a'


select a.ro, b.opcode, c.the_date
from ads.ext_fact_repair_order a
inner join ads.ext_dim_opcode b on a.opcodekey = b.opcodekey
  and b.description like '%qts%' 
inner join dds.dim_date c on a.opendatekey = c.date_key
  and c.the_date > '12/31/2016'  
group by a.ro, b.opcode, c.the_date


-- at least 1 ro for each opcode !
select aa.opcode, count(*)
from (
  select * 
  from ads.ext_dim_opcode
  where description like '%qts%') aa
left join (
  select a.ro, b.opcode, c.the_date
  from ads.ext_fact_repair_order a
  inner join ads.ext_dim_opcode b on a.opcodekey = b.opcodekey
    and b.description like '%qts%' 
  inner join dds.dim_date c on a.opendatekey = c.date_key
    and c.the_date > '12/31/2016'  
  group by a.ro, b.opcode, c.the_date) bb on aa.opcode = bb.opcode  
group by aa.opcode  


select *
from ads.ext_dim_opcode 
where opcode in ('FREEROT','FREE10B','M0021','M1','M2','PDQDP','Q9',
  'ROT','ROTD','ROTEMP','ROTP','Z2263','Z2334','Z2342','Z2357',
  '0040072','15KC','15KD','15KT','30KC','30KD','30KT','6ROT',
  '9','FROT','P71203','ROT','ROTATE','YW15AA','24G','ROTF',
  'ROTEMP','WROT','110009A')
order by opcode  

select aa.storecode, aa.opcode, count(*)
from (
  select * 
  from ads.ext_dim_opcode
  where opcode in ('FREEROT','FREE10B','M0021','M1','M2','PDQDP','Q9',
    'ROT','ROTD','ROTEMP','ROTP','Z2263','Z2334','Z2342','Z2357',
    '0040072','15KC','15KD','15KT','30KC','30KD','30KT','6ROT',
    '9','FROT','P71203','ROT','ROTATE','YW15AA','24G','ROTF',
    'ROTEMP','WROT','110009A')) aa 
left join (
  select a.storecode, a.ro, b.opcode, c.the_date
  from ads.ext_fact_repair_order a
  inner join ads.ext_dim_opcode b on a.opcodekey = b.opcodekey and a.storecode = b.storecode
    and b.opcode in ('FREEROT','FREE10B','M0021','M1','M2','PDQDP','Q9',
    'ROT','ROTD','ROTEMP','ROTP','Z2263','Z2334','Z2342','Z2357',
    '0040072','15KC','15KD','15KT','30KC','30KD','30KT','6ROT',
    '9','FROT','P71203','ROT','ROTATE','YW15AA','24G','ROTF',
    'ROTEMP','WROT','110009A')
  inner join dds.dim_date c on a.opendatekey = c.date_key
    and c.the_date > '12/31/2016'  
  group by a.storecode, a.ro, b.opcode, c.the_date) bb on aa.opcode = bb.opcode and aa.storecode = bb.storecode
group by aa.storecode, aa.opcode  




drop table if exists ros;
create temp table ros as
select a.ro, b.the_date, d.vin, c.paymenttype
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.opendatekey = b.date_key
  and b.the_date > '12/31/2016'
inner join ads.ext_dim_payment_type c on a.paymenttypekey = c.paymenttypekey
  and c.paymenttype <> 'internal'  
inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
inner join ads.ext_dim_opcode e on a.opcodekey = e.opcodekey 
  and a.storecode = e.storecode
  and (
    e.opcode in ('FREEROT','FREE10B','M0021','M1','M2','PDQDP','Q9',
      'ROT','ROTD','ROTEMP','ROTP','Z2263','Z2334','Z2342','Z2357',
      '0040072','15KC','15KD','15KT','30KC','30KD','30KT','6ROT',
      '9','FROT','P71203','ROT','ROTATE','YW15AA','24G','ROTF',
      'ROTEMP','WROT','110009A')
    or e.description like '%qts%')
group by a.ro, b.the_date, d.vin, c.paymenttype

select *
from ros



select store, year_month, 
  case vehicle_type
    when 'N' then 'New'
    when 'U' then 'Used'
  end as new_used,
  count(*) as sold, 
  count(1) filter (where max_serv_date is not null) as retained
from (  
  select a.year_month, a.store, a.control, a.the_date as sale_date, a.vehicle_type, max(b.the_date) as max_serv_date, count(*) - 1 as ros
  from sales a
  left join ros b on a.vin = b.vin
    and b.the_date between a.the_date + 1 and a.the_date + interval '12 months'
  group by a.year_month, a.store, a.control, a.the_date, a.vehicle_type ) x
group by year_month, store, vehicle_type  
order by store, year_month, vehicle_type


