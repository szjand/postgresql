﻿/*
somehow, greg thinks that he can derive some information from looking at detailed ro data
for first visit vins and lost vins
he is confounded by "losing so many vins" and hopes to find some potential explanations
*/

select max(b.the_date)
from ads.ext_Fact_repair_order a
inner join dds.dim_date b on a.opendatekey = b.date_key

select min(b.the_date) as first_date, max(b.the_date) as last_date, c.vin
from ads.ext_Fact_repair_order a
inner join dds.dim_date b on a.opendatekey = b.date_key
inner join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey
where c.vin not like '0%'
  and c.vin not like '111%'
  and length(c.vin) = 17
group by c.vin

-- first visit of any vehicle to any dept at RY1 (not internal) that occurred in 2017
-- need to exclude ry2 HDEL ros, and ry1 pdi, 120, nitropdi
select * from ads.ext_dim_opcode where opcode in ('hdel','pdi','120','nitropdi')

select * from ads.ext_dim_opcode where description like ('%PDI%')

drop table if exists first_visit_1 cascade;
create temp table first_visit_1 as -- 7094
-- group on vin only, eliminate multiple "household" first visits
select min(b.the_date) as first_date, c.vin
from ads.ext_Fact_repair_order a
inner join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
  and aa.description not like '%PDI%'
  and aa.opcode not in ('HDEL','HDLX','DT','HTDW','OTH','PIC','BABT')
inner join dds.dim_date b on a.opendatekey = b.date_key
inner join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey
inner join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey
  and d.paymenttype <> 'Internal'
where a.storecode = 'RY1'
  and c.vin not like '0%'
  and c.vin not like '111%'
  and length(c.vin) = 17
  and a.customerkey <> 2 -- exclude inventory
group by c.vin
having min(b.the_date) >= '01/01/2017';
create unique index on first_visit_1(vin);
create index on first_visit_1(first_date);

drop table if exists first_visit_2 cascade;
-- shit multiple ros on the same vehicle opened on the same day, max(ro)
-- 8 rows with multiple customers for same vehicle same day
create temp table first_visit_2 as 
-- 7281 rows
select max(a.ro) as ro, max(a.customerkey) as customerkey, e.first_date, e.vin
from ads.ext_fact_repair_order a
inner join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
  and aa.description not like '%PDI%'
  and aa.opcode not in ('HDEL','HDLX','DT','HTDW','OTH','PIC','BABT')
inner join dds.dim_date b on a.opendatekey = b.date_key
inner join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey
inner join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey
  and d.paymenttype <> 'Internal'
inner join first_visit_1 e on b.the_date = e.first_date and c.vin = e.vin
where a.storecode = 'RY1'
  and c.vin not like '0%'
  and c.vin not like '111%'
  and length(c.vin) = 17
  and a.customerkey <> 2 
group by e.first_date, e.vin
having min(b.the_date) >= '01/01/2017';
create index on first_visit_2(ro);
create index on first_visit_2(customerkey);
create index on first_visit_2(first_date);
create index on first_visit_2(vin);

select * from first_visit_2 limit 100

what even are some relevant questions to ask about first visit vins
was the vehicle purchased here by the ro customer?
has that customer been here with another vehicle ?
has that vehicle/customer been to honda?
earlier service visit to honda
has that vehicle been here with another customer
10/24 seems like all these questions fade except purchase, lets move on to lost vins

select c.fullname, c.customerkey, c.bnkey, a.*, b.bopmast_search_name, buyer_number, b.bopmast_vin, b.delivery_date
from first_visit_2 a
left join arkona.ext_bopmast b on a.vin = b.bopmast_vin
left join ads.ext_dim_customer c on a.customerkey = c.customerkey
  and c.storecode = 'RY1'
  

/*
select * from ads.ext_dim_customer where fullname like '%eide%' limit 10

select * from ads.ext_dim_customer where bnkey = 190

select *
from ads.ext_fact_repair_order
where ro = '16267070'

select * from arkona.ext_bopname where bopname_record_key = 190

WTF arkona id 190  sdprhdr = eide motrs, bopname = sarah martin

there are a shitload of them
SELECT a.ptckey, a.ptcnam, b.*
FROM stgarkonasdprhdr a
LEFT JOIN dimcustomer b on a.ptckey = b.bnkey
WHERE a.ptcnam <> b.fullname
*/


-- drop table if exists honda_ros;
-- create temp table honda_ros as
-- select string_agg(a.ro, ',') as ros, min(b.the_date) as min_date, max(b.the_date) as max_date, c.vin, count(*)
-- from ads.ext_fact_repair_order a
-- inner join dds.dim_date b on a.opendatekey = b.date_key
-- inner join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey
-- inner join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey
--   and d.paymenttype <> 'Internal'
-- where a.storecode = 'RY2'
--   and c.vin not like '0%'
--   and c.vin not like '111%'
--   and length(c.vin) = 17
-- group by c.vin;
-- create unique index on honda_ros(vin);

-- think i want honda_ros with detail, not grouped

drop table if exists honda_ros cascade;
create temp table honda_ros as
select a.ro, b.the_date, c.vin, e.fullname
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.opendatekey = b.date_key
inner join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey
inner join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey
  and d.paymenttype <> 'Internal'
inner join ads.ext_dim_customer e on a.customerkey = e.customerkey  
where a.storecode = 'RY2'
  and c.vin not like '0%'
  and c.vin not like '111%'
  and length(c.vin) = 17 
group by a.ro, b.the_date, c.vin,e.fullname;
create unique index on honda_ros(ro,the_date,vin,fullname);



-- couple of lease purchases: ro prior to sale, eg, ro on new car, subsequently bought as used car
-- # of previous ros @ honda
-- 

select a.*, b.bnkey, b.fullname, 
  c.bopmast_search_name, c.delivery_date, c.buyer_number, c.bopmast_vin, 
  c.bopmast_stock_number, c.bopmast_company_number,
  case 
    when c.bopmast_company_number = 'RY1' then 'GM'
    when c.bopmast_company_number = 'RY2' then 'Honda Nissan'
    else 'Unknown'
  end as purchased_from,
  first_date - delivery_date as days_since_purch,
  d.*
from first_visit_2 a
left join ads.ext_dim_customer b on a.customerkey = b.customerkey
  and b.storecode = 'RY1'
left join arkona.ext_bopmast c on a.vin = c.bopmast_vin  and b.bnkey = c.buyer_number
left join honda_ros d on a.vin = d.vin
where a.vin not in ('1HGCR2F59EA247442','2HKRM4H76EH668688','2HKRW2H52HH667152') -- lease purchases
order by first_date - delivery_date

-- add zip & distance from gf
-- dealership lat/lon: 47.898521, -97.053180
drop table if exists first_visit_3 cascade;
create temp table first_visit_3 as
select a.ro, 
  case
    when left(a.ro, 2) = '16' then 'service'
    when left(a.ro, 2) = '18' then 'body shop'
    when left(a.ro, 2) = '19' then 'pdq'
  end as dept,
  g.payment_type,
  a.first_date as first_visit_date, a.vin, b.bnkey, b.customerkey, replace(lastname, ',', ' ') as lastname, 
  b.firstname,  b.city, b.state, b.zip,
  case
    when f.zip is null then - 1
    else (point(f.lon,f.lat) <@> (point(-97.053180,47.898521)))::integer
  end as miles_from_dlr,
  case 
    when c.bopmast_company_number = 'RY1' then 'GM'::citext
    when c.bopmast_company_number = 'RY2' then 'Honda Nissan'::citext
    else 'Unknown'::citext
  end as purchased_from,
  c.delivery_date as purch_date,
  a.first_date - c.delivery_date as days_since_purch 
from first_visit_2 a
left join ads.ext_dim_customer b on a.customerkey = b.customerkey
  and b.storecode = 'RY1'
left join arkona.ext_bopmast c on a.vin = c.bopmast_vin  and b.bnkey = c.buyer_number  
left join jon.lat_long_zip f on left(b.zip, 5)::integer = f.zip
left join (
  select aa.ro, string_agg(distinct c.paymenttype,'-') as payment_type
  from ads.ext_fact_repair_order aa
  inner join ads.ext_fact_repair_order b on aa.ro = b.ro
  inner join ads.ext_dim_payment_type c on b.paymenttypekey = c.paymenttypekey
  group by aa.ro) g on a.ro = g.ro
where a.vin not in ('5Y2SL638X5Z406410','2GKFLTEK0D6127605','1HGCR2F59EA247442','2HGFA1F51AH580597','2HKRM4H76EH668688','5J6RM4H75EL045181','5J6RM4H79DL083110');
create index on first_visit_3(vin);

-- do count of honda ros before and after first visit
-- send this to greg for first visits
select a.*, b.honda_ros_before_first_visit
from first_visit_3 a
left join (
  select a.vin, count(*) as honda_ros_before_first_visit
  from first_visit_3 a
  inner join honda_ros b on a.vin = b.vin
    and b.the_date < a.first_visit_date  
  group by a.vin) b on a.vin = b.vin

--ehh, not so interesting
select count(distinct a.fullname) 
-- 170 first visit customers who bought a different car from us
from first_visit_3 a
inner join  sls.ext_bopmast_partial b on a.bnkey = b.buyer_bopname_id
  and b.store = 'ry2'
where a.purchased_from = 'unknown'
  and a.fullname not like '%RYDELL%'
  and a.fullname not like '%ALTRU%'
  and a.fullname not like '%NATIONAL%'
  and a.fullname not like '%CROWN%'
order by a.fullname  


-- 11-8: on to lost vins
select * from first_visit_3 limit 100

select lastname, replace(lastname, ',', ' ') from first_visit_3 where position(',' in lastname) <> 0



--------------------------------------------------------------------------------------------------------------
-- 10/24/17 lost vins -------------------
--------------------------------------------------------------------------------------------------------------
defined as not seen in 2 years
last seen between '11/08/2014' and '11/08/2015'

-- drop table if exists lost_1 cascade;
-- create temp table lost_1 as -- 
-- -- group on vin only, eliminate multiple "household" first visits
-- select max(b.the_date) as last_date, c.vin, max(a.ro) as ro
-- from ads.ext_Fact_repair_order a
-- inner join dds.dim_date b on a.opendatekey = b.date_key
-- inner join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey
-- inner join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey
--   and d.paymenttype <> 'Internal'
-- where a.storecode = 'RY1'
--   and c.vin not like '0%'
--   and c.vin not like '111%'
--   and length(c.vin) = 17
--   and a.customerkey <> 2 -- exclude inventory
-- group by c.vin
-- having max(b.the_date) < current_date - 730;
-- -- having max(b.the_date) between '11/08/2014' and '11/08/2015';
-- create unique index on lost_1(vin);
-- create index on lost_1(last_date);
-- 
-- 
-- max ro is not the way to go

drop table if exists lost_1 cascade;
create temp table lost_1 as 
select max(b.the_date) as last_date, c.vin
from ads.ext_Fact_repair_order a
inner join dds.dim_date b on a.opendatekey = b.date_key
inner join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey
inner join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey
  and d.paymenttype <> 'Internal'
where a.storecode = 'RY1'
  and c.vin not like '0%'
  and c.vin not like '111%'
  and length(c.vin) = 17
  and a.customerkey <> 2 -- exclude inventory
group by c.vin
having max(b.the_date) < current_date - 730;
create unique index on lost_1(vin);
create index on lost_1(last_date);



select min(last_date), max(last_date)
from lost_1 a
where last_date between current_date - 1095 and current_date - 730

-- trying to clean up multiple ros on the same day for the same vehicle
drop table if exists lost_2;
create temp table lost_2 as
select ro, the_date, vin
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.opendatekey = b.date_key
  and b.the_date between current_date - 1095 and current_date - 730
inner join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey  
inner join ads.ext_dim_payment_type d on a.paymenttypekey = d.paymenttypekey
  and d.paymenttype <> 'Internal'
inner join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
  and aa.description not like '%PDI%'
  and aa.opcode not in ('HDEL','HDLX','DT','HTDW','OTH','PIC','BABT')  
where a.storecode = 'RY1'
  and c.vin not like '0%'
  and c.vin not like '111%'
  and length(c.vin) = 17
group by ro, the_date, vin  


drop table if exists exceptions;
create temp table exceptions as
select max(c.ro) as ro, c.the_date, c.vin
from lost_2 c
inner join (
  select a.vin, a.last_date
  from lost_1 a
  left join lost_2 b on a.vin = b.vin
    and a.last_date = b.the_date
  where last_date between current_date - 1095 and current_date - 730
  group by a.vin, a.last_date
  having count(*) > 1) d on c.vin = d.vin and c.the_date = d.last_date
where c.ro not like '19%'  
group by c.the_date, c.vin;
create unique index on exceptions(the_date, vin);


drop table if exists lost_3;
create temp table lost_3 as
select c.*
from (
  select b.ro, a.last_date, a.vin
  from lost_1 a
  inner join lost_2 b on a.last_date = b.the_date and a.vin = b.vin
  where a.vin not in ('1G4HD57217U129617','1G1PC5SH1C7261227','2C4RC1BG5DR620174','WDBBA45C7FA022529','1GCRKSE78CZ132989','1NXBR30E18Z939489')) c
left join exceptions d on c.last_date = d.the_date and c.vin = d.vin
where d.ro is null;
create unique index on lost_3(ro);
create unique index on lost_3(vin);

insert into lost_3
select *
from exceptions;

select current_date - 1095 
union
select current_date - 730
-- 10218 vins that visited between 11/11/2014 and 11/11/2015 and have not been back since
select *
from lost_3

drop table if exists lost_4;
create temp table lost_4 as
select aa.ro, a.customerkey, b.bnkey, aa.last_date, 
  case left(aa.ro, 2)
    when '16' then 'service'
    when '18' then 'body shop'
    when '19' then 'pdq'
  end as dept,
  aa.vin, replace(b.lastname, ',', ' ') as lastname, b.firstname, b.zip, 
  string_agg(distinct c.paymenttype, '-') as payment_type,
  d.modelyear, d.make, d.model, a.miles as odometer
from ads.ext_fact_repair_order a
inner join lost_3 aa on a.ro = aa.ro
inner join ads.ext_dim_customer b on a.customerkey = b.customerkey
inner join ads.ext_dim_payment_type c on a.paymenttypekey = c.paymenttypekey 
inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
group by aa.ro, aa.last_date, aa.vin, a.customerkey, b.bnkey, replace(b.lastname, ',', ' '), b.firstname, b.zip, 
  d.modelyear, d.make, d.model, a.miles;
create unique index on lost_4(ro);
create unique index on lost_4(vin);

drop table if exists lost_5;
create temp table lost_5 as
select a.* , b.city, b.state,
  case
    when b.zip is null then - 1
    else (point(b.lon,b.lat) <@> (point(-97.053180,47.898521)))::integer
  end as miles_from_dlr
from lost_4 a
left join jon.lat_long_zip b on left(a.zip, 5)::integer = b.zip;
create unique index on lost_5(ro);
create unique index on lost_5(vin);

select * from lost_5 limit 10

-- 789 have been going to honda
select *
from lost_5 aa
left join (
  select a.vin, count(*) as honda_ros_after_last_visit
  from lost_5 a
  inner join honda_ros b on a.vin = b.vin
    and a.last_date < b.the_date
  group by a.vin) bb on aa.vin = bb.vin

select * from arkona.ext_bopmast limit 10

-- 714 have been sold by us, 214 wholesale, 514 retail
select a.*, 
  case 
    when b.sale_type = 'W' and bopmast_company_number = 'RY1' then 'gm_whlsl'
    when b.sale_type = 'W' and bopmast_company_number = 'RY2' then 'hn_whlsl'
    when b.sale_type = 'r' and bopmast_company_number = 'RY1' then 'gm_retail'
    when b.sale_type = 'r' and bopmast_company_number = 'RY2' then 'hn_retail'
  end as sold_after_last_visit
from lost_5 a
inner join arkona.ext_bopmast b on a.vin = b.bopmast_vin
  and a.last_date < b.delivery_Date


select a.*, 
  case 
    when b.sale_type = 'W' and bopmast_company_number = 'RY1' then 'gm_whlsl'
    when b.sale_type = 'W' and bopmast_company_number = 'RY2' then 'hn_whlsl'
    when b.sale_type = 'r' and bopmast_company_number = 'RY1' then 'gm_retail'
    when b.sale_type = 'r' and bopmast_company_number = 'RY2' then 'hn_retail'
  end as sold_after_last_visit,
  c.honda_ros_after_last_visit
from lost_5 a
left join arkona.ext_bopmast b on a.vin = b.bopmast_vin
  and a.last_date < b.delivery_Date
left join (
  select a.vin, count(*) as honda_ros_after_last_visit
  from lost_5 a
  inner join honda_ros b on a.vin = b.vin
    and a.last_date < b.the_date
  group by a.vin) c on a.vin = c.vin  





--------------------------------------------------------------------------------------------------------------
-- 10/24/17 lost vins -------------------
--------------------------------------------------------------------------------------------------------------