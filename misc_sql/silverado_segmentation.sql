﻿!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

see /Desktop/sql/new_car_inventory_vins.sql

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
select *
from arkona.xfm_inpmast a
where current_row = true
  and year > 2016
  and make = 'chevrolet'
  and model = 'silverado 1500'
limit 100

select inpmast_vin, year, make, model_code, model, body_Style, color, color_code, chrome_style_id,
  b.style_name, b.trim_level, b.cf_style_name, b.cf_drive_train, b.cf_body_type, c.*
from arkona.xfm_inpmast a
-- left join chr.styles b on a.chrome_style_id = b.style_id
left join chr.styles b on a.model_code = b.full_style_code and a.chrome_style_id = b.style_id
left join lateral (select * from chr.decode_vin(a.inpmast_vin)) c on 1 = 1
where a.current_row = true
  and a.year > 2016
  and a.make = 'chevrolet'
  and a.model = 'silverado 1500'
order by right(inpmast_vin, 6)  
limit 1000


-- trim down these fields, add yearmakemdelstyle
-- looks like year_make_model_style.style_name has what i need
select inpmast_vin, year, model_code, model, body_Style, color, 
  b.style_name, 
  c.vin_Style_name, 
  e.style_name, a.chrome_style_id, e.chrome_Style_id
from arkona.xfm_inpmast a
-- left join chr.styles b on a.chrome_style_id = b.style_id
left join chr.styles b on a.model_code = b.full_style_code and a.chrome_style_id = b.style_id
left join lateral (select * from chr.decode_vin(a.inpmast_vin)) c on 1 = 1
left join chr.vin_Pattern_Style_Mapping d on c.vin_pattern_id = d.vin_pattern_id
left join chr.year_make_model_style e on d.chrome_style_id = e.chrome_style_id
where a.current_row = true
  and a.year > 2016
  and a.make = 'chevrolet'
  and a.model = 'silverado 1500'
order by right(inpmast_vin, 6)  
limit 1000



select a.stock_number, b.*
from sls.deals_by_month a
left join (
  select inpmast_vin, year, model_code, model, body_Style, color, 
    b.style_name, 
    c.vin_Style_name, 
    e.style_name, a.chrome_style_id, e.chrome_Style_id
  from arkona.xfm_inpmast a
  -- left join chr.styles b on a.chrome_style_id = b.style_id
  left join chr.styles b on a.model_code = b.full_style_code and a.chrome_style_id = b.style_id
  left join lateral (select * from chr.decode_vin(a.inpmast_vin)) c on 1 = 1
  left join chr.vin_Pattern_Style_Mapping d on c.vin_pattern_id = d.vin_pattern_id
  left join chr.year_make_model_style e on d.chrome_style_id = e.chrome_style_id
  where a.current_row = true
    and a.year > 2016
    and a.make = 'chevrolet'
    and a.model = 'silverado 1500') b on a.vin = b.inpmast_vin
where a.model = 'silverado 1500'
  and a.vehicle_type = 'new'
  and a.unit_count > 0
order by year_month  

select * from sls.deals_by_month where vin = '3GCUKREC3HG217374'


ok, need to sort this out immediately, vin decodes to multiple chrome_style_ids
select *
from (  select inpmast_vin, year, model_code, model, body_Style, color, 
    b.style_name, 
    c.vin_Style_name, 
    e.style_name, a.chrome_style_id, e.chrome_Style_id
  from arkona.xfm_inpmast a
  -- left join chr.styles b on a.chrome_style_id = b.style_id
  left join chr.styles b on a.model_code = b.full_style_code and a.chrome_style_id = b.style_id
  left join lateral (select * from chr.decode_vin(a.inpmast_vin)) c on 1 = 1
  left join chr.vin_Pattern_Style_Mapping d on c.vin_pattern_id = d.vin_pattern_id
  left join chr.year_make_model_style e on d.chrome_style_id = e.chrome_style_id and a.chrome_Style_id = e.chrome_style_id
  where a.current_row = true
    and a.year > 2016
    and a.make = 'chevrolet'
    and a.model = 'silverado 1500') x where x.inpmast_vin = '3GCUKREC3HG217374'



select inpmast_vin, year, model_code, model, body_Style, color
--     b.style_name, 
--     c.vin_Style_name, 
--     e.style_name, a.chrome_style_id, e.chrome_Style_id
-- -- select c.*, e.*
  from arkona.xfm_inpmast a
--   left join chr.styles b on a.model_code = b.full_style_code and a.chrome_style_id = b.style_id
--   left join lateral (select * from chr.decode_vin(a.inpmast_vin)) c on 1 = 1
--   left join chr.vin_Pattern_Style_Mapping d on c.vin_pattern_id = d.vin_pattern_id
  left join chr.year_make_model_style e on a.chrome_style_id = e.chrome_style_id
  where a.current_row = true
    and a.year > 2016
    and a.make = 'chevrolet'
    and a.model = 'silverado 1500'
    and a.inpmast_vin = '3GCUKREC3HG217374'


select * from chr.decode_vin('3GCUKREC3HG217374')    

select * from chr.vin_pattern_style_mapping where vin_pattern_id = 3507032

-- trick might be join inpmast.chrome_style_id directly to year_make_model_style.chrome_style_id
-- pretty good, 142 of 457 don't decode to silverados though
-- for the ones that do, year_make_mode_style.style_name gives the full trim: lt1, lt2, lz1, lz2
-- limit to crew cab only
select a.stock_number, a.model_year, a.model,
  b.inpmast_vin, b.model_code, b.body_style, b.color, b.chrome_style_id, 
  c.*
from sls.deals_by_month a
inner join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row = true
inner join chr.year_make_model_style c on b.chrome_style_id = c.chrome_style_id  
where a.model = 'silverado 1500'
  and a.vehicle_type = 'New'
  and b.body_style like '%CREW%'
  and c.model_name = 'silverado 1500'

-- these are the ones that apparently have the wrong chrome_stlye_id in inpmast
-- add vin decoding to get vin pattern
-- and from vin_pattern_style_mapping the chrome_style_id(s)
-- vin_pattern.vin_style_name does not give full resolution on trim: 4WD Crew Cab LT, 4WD Double Cab 143.5"" LT, 4WD Crew Cab LTZ, etc
-- yep, what i was afraid of, each vin_pattern_id yields multiple chrome_style_id's, shit
select a.stock_number, a.model_year, a.model,
  b.inpmast_vin, b.model_code, b.body_style, b.color, b.chrome_style_id, 
  c.division_name, c.model_name, 
  string_agg(distinct e.chrome_style_id::text, ',')
from sls.deals_by_month a
inner join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row = true
inner join chr.year_make_model_style c on b.chrome_style_id = c.chrome_style_id  
left join lateral (select * from chr.decode_vin(b.inpmast_vin)) d on 1 = 1
left join chr.vin_pattern_style_mapping e on d.vin_pattern_id = e.vin_pattern_id
where a.model = 'silverado 1500'
  and a.vehicle_type = 'New'
  and c.model_name <> 'silverado 1500'
group by a.stock_number, a.model_year, a.model,
  b.inpmast_vin, b.model_code, b.body_style, b.color, b.chrome_style_id, 
  c.division_name, c.model_name
  
select * 
from chr.year_make_model_style
where style_name like '%Z71 4WD LT DOUBLE CAB%'  

-- body style: Z71 4WD LT DOUBLE CAB  chrome: isuzu trooper
select * from chr.decode_vin('1GCVKREC7HZ288596')  
select * from chr.vin_pattern_style_mapping where vin_pattern_id = 3506997
select * from chr.year_make_model_style where chrome_style_id in (383672, 383673)
select * from chr.styles where style_id in (383672, 383673)
select * from arkona.xfm_inpmast where inpmast_vin = '1GCVKREC7HZ288596'
select * from fin.dim_Account where account = '1429001'

-- yet another way arkona data is fucked up
select distinct body_style from arkona.xfm_inpmast where model_code = 'ck15543'

select distinct style_name from chr.year_make_model_style where mfr_style_code = 'ck15753'
select distinct style_name from chr.year_make_model_style where mfr_style_code = 'ck15543'


ok, maybe some hope
  first eliminate the double cabs (verify that double cab = extended cab NOT CREW)
  then, styles.cf_style_name distinguishes between LT and LT Z71, which corresponds to 1LT and 2LT in styles.style_name

-- get the vin_pattern_id for the "non silverados"
-- add on the styles.cf_style_name
-- the assumption is that the inpmast.body_style correctly identifies Z71 vs not Z71
ok, was getting hung up on bed size, kelly clarified, model code 15543 is short bed, 15743 is long (standard) bed
ah, thats where i was fucked up, model code correlates to bed length, style_code(-A, -B) correlates to trim
ok where i am at, inpmast (correctly or not) identifies z71 vs non z71
select a.stock_number, a.model_year, a.model,
  b.inpmast_vin, b.model_code, b.body_style, b.color, b.chrome_style_id, 
  c.division_name, c.model_name, 
  e.chrome_style_id,
  f.style_name, f.cf_style_name,
  f.*
from sls.deals_by_month a
inner join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row = true
inner join chr.year_make_model_style c on b.chrome_style_id = c.chrome_style_id  
left join lateral (select * from chr.decode_vin(b.inpmast_vin)) d on 1 = 1
left join chr.vin_pattern_style_mapping e on d.vin_pattern_id = e.vin_pattern_id
left join chr.styles f on e.chrome_style_id = f.style_id
where a.model = 'silverado 1500'
  and a.vehicle_type = 'New'
  and c.model_name <> 'silverado 1500'
  and b.body_style like '%CREW%'
order by inpmast_vin, f.style_name


select * from arkona.xfm_inpmast where inpmast_vin = '3GCUKREC0HG364736'

select * from arkona.xfm_inpmast where model_code = 'CK15743' and current_row = true and date_delivered between 20170000 and 20171231 limit 100


-- ok, first, the ones with correct chrome_style_id 
-- what fields do i actually need
select a.year_month, a.vin, 
  b.color, b.make, b.model_code, b.body_style, b.chrome_style_id,
  c.style_name
from sls.deals_by_month a -- 457
inner join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row = true
left join chr.styles c on b.chrome_style_id = c.style_id  
where a.model = 'silverado 1500'
  and vehicle_type = 'New'
  and b.body_style like '%CREW%' -- 309
  -- only the correct chrome_style_id
  and c.cf_model_name = 'silverado 1500' --143

-- not sure i want to start with chr.styles, there are many that do not decode the inpmast.chrome_style_id
select a.year_month, a.vin, 
  b.color, b.make, b.model_code, b.body_style, b.chrome_style_id,
  c.style_name
-- select *  
from sls.deals_by_month a -- 457
inner join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row = true
left join chr.styles c on b.chrome_style_id = c.style_id  
where a.model = 'silverado 1500'
  and vehicle_type = 'New'
  and b.body_style like '%CREW%' -- 309
  -- only the correct chrome_style_id
  and c.cf_model_name <> 'silverado 1500' --143  

-- this is what is working -----------------------------------------------------------------------------------------------
-- try chr.year_make_model_style
-- ok, this is different, all are chevrolet, 88 of the 309 are express cargo van based on inpmast.chrome_style_id, & 1 that does not decode
select a.year_month, a.vin, 
  b.color, b.make, b.model_code, b.body_style, b.chrome_style_id,
  c.division_name, c.model_name, c.style_name
-- just the fields needed
select a.year_month, b.color, c.style_name -- 220
from sls.deals_by_month a -- 457
inner join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row = true
left join chr.year_make_model_style c on b.chrome_style_id = c.chrome_style_id  
where a.model = 'silverado 1500'
  and vehicle_type = 'New'
  and b.body_style like '%CREW%' -- 309
  -- those that decode correctly
  and c.model_name = 'silverado 1500' -- 220
and a.unit_count > 0  
order by c.style_name

-- now, for those that do not decode correctly,
-- decode the vin and generate new chrome_style_id(s)
select e.year_month, e.vin, e.color, e.model_code, e.body_style,
  h.*
-- just the fields i need 
select e.year_month, e.color, h.style_name  
from (
  select a.year_month, a.vin, 
    b.color, b.make, b.model_code, b.body_style, b.chrome_style_id,
    c.division_name, c.model_name, c.style_name
  -- select *  
  from sls.deals_by_month a -- 457
  inner join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
    and b.current_row = true
  left join chr.year_make_model_style c on b.chrome_style_id = c.chrome_style_id  
  where a.model = 'silverado 1500'
    and vehicle_type = 'New'
    and b.body_style like '%CREW%' -- 309
    and coalesce(c.model_name, 'xxx') <> 'silverado 1500'
  and a.unit_count > 0) e
left join lateral (select * from chr.decode_vin(e.vin)) f on 1 = 1    
left join chr.vin_pattern_style_mapping g on f.vin_pattern_id = g.vin_pattern_id
left join chr.styles h on g.chrome_style_id = h.style_id
  and e.model_code = h.full_style_code
  and
    case 
      when e.body_style like '%Z71%' then h.cf_style_name like '%Z71%'
      when e.body_style not like '%Z71%' then h.cf_style_name not like '%Z71%'
    end
where h.style_id is not null   


-- and all together now

  case
    when color like '%iri%' then 'IRIDESCENT PEARL TRICOAT'
    when color like '%ocean%' then 'DEEP OCEAN BLUE METALLIC'
  END AS color_fixed,
  
select * from colors
-- drop table if exists colors;
-- create temp table colors as
-- select color, count(*) from (

drop table if exists silverados;
create temp table silverados as
select a.year_month, b.color, 
  c.style_name -- 220
from sls.deals_by_month a -- 457
inner join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row = true
left join chr.year_make_model_style c on b.chrome_style_id = c.chrome_style_id  
where a.model = 'silverado 1500'
  and a.vehicle_type = 'New'
  and b.body_style like '%CREW%' -- 309
  -- those that decode correctly
  and c.model_name = 'silverado 1500' -- 220
  and a.unit_count > 0
union all
select e.year_month, e.color, 
  h.style_name  
from (
  select a.year_month, a.vin, 
    b.color, b.make, b.model_code, b.body_style, b.chrome_style_id,
    c.division_name, c.model_name, c.style_name
  -- select *  
  from sls.deals_by_month a -- 457
  inner join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
    and b.current_row = true
  left join chr.year_make_model_style c on b.chrome_style_id = c.chrome_style_id  
  where a.model = 'silverado 1500'
    and vehicle_type = 'New'
    and b.body_style like '%CREW%' -- 309
    and coalesce(c.model_name, 'xxx') <> 'silverado 1500'
    and a.unit_count > 0) e
left join lateral (select * from chr.decode_vin(e.vin)) f on 1 = 1    
left join chr.vin_pattern_style_mapping g on f.vin_pattern_id = g.vin_pattern_id
left join chr.styles h on g.chrome_style_id = h.style_id
  and e.model_code = h.full_style_code
  and
    case 
      when e.body_style like '%Z71%' then h.cf_style_name like '%Z71%'
      when e.body_style not like '%Z71%' then h.cf_style_name not like '%Z71%'
    end
where h.style_id is not null   
order by color;

select distinct style_name from silverados 1LT, 2LT, 1LZ, 2LZ

-- select 'SILVERADO CREW CAB LT1', null::integer as "BLACK",null::integer as "DEEP OCEAN BLUE METALLIC",
--   null::integer as "GRAPHITE METALLIC",null::integer as "IRIDESCENT PEARL TRICOAT",null::integer as "PEPPERDUST METALLIC",
--   null::integer as "RED HOT",null::integer as "SILVER ICE METALLIC",null::integer as "SIREN RED TINTCOAT",
--   null::integer as "SUMMIT WHITE",null::integer as "UNRIPENED GREEN"
-- union all  
select b.month_name, 
  count(b.year_month) filter (where color_fixed = 'BLACK') as "BLACK",
  count(b.year_month) filter (where color_fixed = 'DEEP OCEAN BLUE METALLIC') as "DEEP OCEAN BLUE METALLIC",
  count(b.year_month) filter (where color_fixed = 'CAJUN RED TINTCOAT') as "CAJUN RED TINTCOAT",
  count(b.year_month) filter (where color_fixed = 'GRAPHITE METALLIC') as "GRAPHITE METALLIC",
  count(b.year_month) filter (where color_fixed = 'IRIDESCENT PEARL TRICOAT') as "IRIDESCENT PEARL TRICOAT",
  count(b.year_month) filter (where color_fixed = 'PEPPERDUST METALLIC') as "PEPPERDUST METALLIC",
  count(b.year_month) filter (where color_fixed = 'RED HOT') as "RED HOT",
  count(b.year_month) filter (where color_fixed = 'SILVER ICE METALLIC') as "SILVER ICE METALLIC",
  count(b.year_month) filter (where color_fixed = 'SIREN RED TINTCOAT') as "SIREN RED TINTCOAT",
  count(b.year_month) filter (where color_fixed = 'SUMMIT WHITE') as "SUMMIT WHITE",
  count(b.year_month) filter (where color_fixed = 'UNRIPENED GREEN') as "UNRIPENED GREEN"
from (
  select year_month, month_name
  from dds.dim_date 
  where year_month between 201702 and 201712
  group by year_month, month_name) b
left join (
  select year_month, color, 
    case
      when color like '%black%' then 'BLACK'      
      when color like '%ocean%' then 'DEEP OCEAN BLUE METALLIC'
      when color = 'GRAPHITE METALLIC' then 'GRAPHITE METALLIC'
      when color like '%iri%' then 'IRIDESCENT PEARL TRICOAT'
      when color like 'pep%' then 'PEPPERDUST METALLIC'
      when color = 'RED HOT' then 'RED HOT'
      when color like 'CAJUN RED%' then 'CAJUN RED TINTCOAT'
      when color = 'SILVER ICE METALLIC' then 'SILVER ICE METALLIC'
      when color like 'siren%' then 'SIREN RED TINTCOAT'
      when color like 'sum%' then 'SUMMIT WHITE'
      when color = 'UNRIPENED GREEN' then 'UNRIPENED GREEN'
      else '******************************'
    END AS color_fixed,
    style_name
  from silverados --) a on b.year_month = a.year_month  
  where style_name like '%1LT%') a on b.year_month = a.year_month  
group by b.year_month, b.month_name
order by b.year_month
  
------------------------------------------------------------------------------------
-- include stock_number, vin
drop table if exists silverados;
create temp table silverados as
select a.stock_number, a.vin, a.year_month, b.color, 
  c.style_name -- 220
from sls.deals_by_month a -- 457
inner join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row = true
left join chr.year_make_model_style c on b.chrome_style_id = c.chrome_style_id  
where a.model = 'silverado 1500'
  and a.vehicle_type = 'New'
  and b.body_style like '%CREW%' -- 309
  -- those that decode correctly
  and c.model_name = 'silverado 1500' -- 220
  and a.unit_count > 0
union all
select e.stock_number, e.vin, e.year_month, e.color, 
  h.style_name  
from (
  select a.year_month, a.stock_number, a.vin, 
    b.color, b.make, b.model_code, b.body_style, b.chrome_style_id,
    c.division_name, c.model_name, c.style_name
  -- select *  
  from sls.deals_by_month a -- 457
  inner join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
    and b.current_row = true
  left join chr.year_make_model_style c on b.chrome_style_id = c.chrome_style_id  
  where a.model = 'silverado 1500'
    and vehicle_type = 'New'
    and b.body_style like '%CREW%' -- 309
    and coalesce(c.model_name, 'xxx') <> 'silverado 1500'
    and a.unit_count > 0) e
left join lateral (select * from chr.decode_vin(e.vin)) f on 1 = 1    
left join chr.vin_pattern_style_mapping g on f.vin_pattern_id = g.vin_pattern_id
left join chr.styles h on g.chrome_style_id = h.style_id
  and e.model_code = h.full_style_code
  and
    case 
      when e.body_style like '%Z71%' then h.cf_style_name like '%Z71%'
      when e.body_style not like '%Z71%' then h.cf_style_name not like '%Z71%'
    end
where h.style_id is not null   
order by color;


select *
from silverados
order by color




