﻿-- vision -----------------------------------------------------------------------------
-- this is the query from swMetricDataYesterday that generates labor sales

    SELECT d.userName, d.storecode, d.fullname, d.lastname, d.firstname,
      d.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
      b.dayOfMonth, b.sundayToSaturdayWeek, 'Labor Sales', 5 AS seq, 
      round(cast(abs(a.laborsales) AS sql_double), 0) AS today,
      0, 
      trim(cast(coalesce(abs(round(CAST(a.laborsales AS sql_double), 0)), 0) AS sql_char)) AS todayDisplay, 
      CAST(NULL AS sql_char)
    FROM (   
-- *a*	     
--      SELECT a.ServiceWriterKey, SUM(RoLaborSales) AS LaborSales
	  SELECT a.ServiceWriterKey, SUM(laborSales) AS LaborSales
      FROM dds.factRepairOrder a
      INNER JOIN dds.day b on a.FinalCloseDateKey = b.datekey
        AND b.thedate = curdate() - 1
      GROUP BY a.ServiceWriterKey) a 
    INNER JOIN dds.day b on b.thedate = curdate() - 1
    INNER JOIN dds.dimServiceWriter c on a.ServiceWriterKey = c.ServiceWriterKey
      AND c.CensusDept = 'MR'
    INNER JOIN ServiceWriters d on c.StoreCode = d.StoreCode
      and c.WriterNumber = d.WriterNumber; 
----

select sum(laborsales) from (
create temp table vision as
select ro, sum(laborsales) as laborsales
from ads.ext_Fact_repair_order a
inner join dds.dim_date b on a.finalclosedatekey = b.date_key
  and b.year_month = 201709
inner join ads.ext_dim_service_writer c on a.servicewriterkey = c.servicewriterkey
  and c.writernumber = '714'
group by ro
) x


select bb.the_date as cd, b.the_date as fcd, a.*
from ads.ext_Fact_repair_order a
inner join dds.dim_date b on a.finalclosedatekey = b.date_key
  and b.year_month = 201709
left join dds.dim_date bb on a.closedatekey = bb.date_key
--   and b.year_month = 201709  
inner join ads.ext_dim_service_writer c on a.servicewriterkey = c.servicewriterkey
  and c.writernumber = '714'
where a.ro = '16276132'  

select aa.the_date, a.*, b.*
from fin.fact_gl a
inner join dds.dim_date aa on a.date_key = aa.date_key
inner join fin.dim_account b on a.account_key = b.account_key
  and b.account_type = 'sale'
  and b.department_code = 'sd'
where a.control = '16276132'  


  
-- vision -----------------------------------------------------------------------------




select aa.the_date, a.*, b.*
from fin.fact_gl a
inner join dds.dim_date aa on a.date_key = aa.date_key
inner join fin.dim_account b on a.account_key = b.account_key
  and b.account_type = 'sale'
  and b.department_code = 'sd'
inner join (
  select ro
  from ads.ext_fact_repair_order a
  inner join ads.ext_dim_service_writer b on a.servicewriterkey = b.servicewriterkey
    and b.writernumber = '714'
  inner join dds.dim_date c on a.finalclosedatekey = c.date_key
    and c.year_month = 201709  
  group by ro) c on a.control = c.ro
where a.post_status = 'Y'  
  and aa.year_month = 201709
  and a.control = '16287023'
order by a.control


select * from fin.dim_Account
where account_type = 'sale'
  and department_code = 'sd'
  and store_code = 'ry1'


select *
from fin.dim_account
where account in ('148004','149104')

select *
from ads.ext_fact_repair_order
where ro = '16287023'


  
select a.control, sum(amount)
-- select sum(amount)
-- select aa.the_date, a.*
from fin.fact_gl a
inner join dds.dim_date aa on a.date_key = aa.date_key
inner join fin.dim_account b on a.account_key = b.account_key
  and b.account_type = 'sale'
  and b.department_code = 'sd'
inner join (
  select ro
  from ads.ext_fact_repair_order a
  inner join ads.ext_dim_service_writer b on a.servicewriterkey = b.servicewriterkey
    and b.writernumber = '714'
  inner join dds.dim_date c on a.finalclosedatekey = c.date_key
    and c.year_month = 201709  
  group by ro) c on a.control = c.ro
where a.post_status = 'Y'  
  and aa.year_month = 201709
group by a.control  
order by a.control



create table jon.rods_ros (
  ro citext,
  amount numeric(12,2));

select sum(amount) from jon.rods_ros  

select sum(laborsales) from vision

select *, abs(coalesce(a.amount, 0) - coalesce(b.laborsales, 0)) as diff
from jon.rods_ros a
full outer join vision b on a.ro = b.ro
where coalesce(a.amount, 0) <> coalesce(b.laborsales, 0)
order by abs(coalesce(a.amount, 0) - coalesce(b.laborsales, 0)) desc










delete from jon.rods_ros
-- -- -- 
-- -- -- insert into jon.rods_ros values('16287067',9);
-- -- -- insert into jon.rods_ros values('16287660',15);
-- -- -- insert into jon.rods_ros values('16289666',15);
-- -- -- insert into jon.rods_ros values('16289883',15);
-- -- -- insert into jon.rods_ros values('16288370',19.95);
-- -- -- insert into jon.rods_ros values('16288148',21.21);
-- -- -- insert into jon.rods_ros values('16286669',22.75);
-- -- -- insert into jon.rods_ros values('16286950',22.75);
-- -- -- insert into jon.rods_ros values('16287714',22.75);
-- -- -- insert into jon.rods_ros values('16289105',22.75);
-- -- -- insert into jon.rods_ros values('16289259',22.75);
-- -- -- insert into jon.rods_ros values('16290036',22.75);
-- -- -- insert into jon.rods_ros values('16289671',23.58);
-- -- -- insert into jon.rods_ros values('16288185',24.95);
-- -- -- insert into jon.rods_ros values('16288876',24.95);
-- -- -- insert into jon.rods_ros values('16288932',24.95);
-- -- -- insert into jon.rods_ros values('16288953',24.95);
-- -- -- insert into jon.rods_ros values('16289786',24.95);
-- -- -- insert into jon.rods_ros values('16289941',24.95);
-- -- -- insert into jon.rods_ros values('16290172',24.95);
-- -- -- insert into jon.rods_ros values('16290173',24.95);
-- -- -- insert into jon.rods_ros values('16288373',25.42);
-- -- -- insert into jon.rods_ros values('16288598',25.42);
-- -- -- insert into jon.rods_ros values('16281961',27);
-- -- -- insert into jon.rods_ros values('16290177',27.5);
-- -- -- insert into jon.rods_ros values('16276132',30);
-- -- -- insert into jon.rods_ros values('16285714',30);
-- -- -- insert into jon.rods_ros values('16287069',30);
-- -- -- insert into jon.rods_ros values('16287910',30);
-- -- -- insert into jon.rods_ros values('16288674',30);
-- -- -- insert into jon.rods_ros values('16287447',34.12);
-- -- -- insert into jon.rods_ros values('16287622',34.12);
-- -- -- insert into jon.rods_ros values('16287731',34.12);
-- -- -- insert into jon.rods_ros values('16287909',34.12);
-- -- -- insert into jon.rods_ros values('16288948',34.12);
-- -- -- insert into jon.rods_ros values('16289132',34.12);
-- -- -- insert into jon.rods_ros values('16289641',34.12);
-- -- -- insert into jon.rods_ros values('16289769',34.12);
-- -- -- insert into jon.rods_ros values('16289812',34.12);
-- -- -- insert into jon.rods_ros values('16289364',38.13);
-- -- -- insert into jon.rods_ros values('16289428',38.13);
-- -- -- insert into jon.rods_ros values('16289383',40.21);
-- -- -- insert into jon.rods_ros values('16288803',41.96);
-- -- -- insert into jon.rods_ros values('16288950',44);
-- -- -- insert into jon.rods_ros values('16287730',45.5);
-- -- -- insert into jon.rods_ros values('16288489',45.5);
-- -- -- insert into jon.rods_ros values('16288491',45.5);
-- -- -- insert into jon.rods_ros values('16288874',45.5);
-- -- -- insert into jon.rods_ros values('16289661',45.5);
-- -- -- insert into jon.rods_ros values('16288406',49.95);
-- -- -- insert into jon.rods_ros values('16288487',49.95);
-- -- -- insert into jon.rods_ros values('16288877',49.95);
-- -- -- insert into jon.rods_ros values('16288987',50.84);
-- -- -- insert into jon.rods_ros values('16285370',56.87);
-- -- -- insert into jon.rods_ros values('16287907',56.87);
-- -- -- insert into jon.rods_ros values('16289772',56.87);
-- -- -- insert into jon.rods_ros values('16287261',57.43);
-- -- -- insert into jon.rods_ros values('16287350',60);
-- -- -- insert into jon.rods_ros values('16287586',60);
-- -- -- insert into jon.rods_ros values('16287786',60);
-- -- -- insert into jon.rods_ros values('16287986',60);
-- -- -- insert into jon.rods_ros values('16288423',60);
-- -- -- insert into jon.rods_ros values('16288473',60);
-- -- -- insert into jon.rods_ros values('16288845',60);
-- -- -- insert into jon.rods_ros values('16289825',60);
-- -- -- insert into jon.rods_ros values('16289829',60);
-- -- -- insert into jon.rods_ros values('16290097',60);
-- -- -- insert into jon.rods_ros values('16288681',61.48);
-- -- -- insert into jon.rods_ros values('16287725',63.55);
-- -- -- insert into jon.rods_ros values('16288145',63.55);
-- -- -- insert into jon.rods_ros values('16288264',63.55);
-- -- -- insert into jon.rods_ros values('16288324',63.55);
-- -- -- insert into jon.rods_ros values('16288680',63.55);
-- -- -- insert into jon.rods_ros values('16288870',63.55);
-- -- -- insert into jon.rods_ros values('16289893',63.55);
-- -- -- insert into jon.rods_ros values('16287724',68.24);
-- -- -- insert into jon.rods_ros values('16288425',68.24);
-- -- -- insert into jon.rods_ros values('16288669',68.24);
-- -- -- insert into jon.rods_ros values('16288943',68.24);
-- -- -- insert into jon.rods_ros values('16289203',69.97);
-- -- -- insert into jon.rods_ros values('16288973',70);
-- -- -- insert into jon.rods_ros values('16287919',71.94);
-- -- -- insert into jon.rods_ros values('16286340',79.62);
-- -- -- insert into jon.rods_ros values('16287606',79.62);
-- -- -- insert into jon.rods_ros values('16288270',81.82);
-- -- -- insert into jon.rods_ros values('16289770',83.24);
-- -- -- insert into jon.rods_ros values('16289528',83.58);
-- -- -- insert into jon.rods_ros values('16285027',83.96);
-- -- -- insert into jon.rods_ros values('16290026',87.8);
-- -- -- insert into jon.rods_ros values('16287614',88.97);
-- -- -- insert into jon.rods_ros values('16287663',89.8);
-- -- -- insert into jon.rods_ros values('16283518',90);
-- -- -- insert into jon.rods_ros values('16289192',90.99);
-- -- -- insert into jon.rods_ros values('16289221',91.93);
-- -- -- insert into jon.rods_ros values('16288571',92.26);
-- -- -- insert into jon.rods_ros values('16288564',93.5);
-- -- -- insert into jon.rods_ros values('16289518',93.55);
-- -- -- insert into jon.rods_ros values('16288796',95.6);
-- -- -- insert into jon.rods_ros values('16289533',98.96);
-- -- -- insert into jon.rods_ros values('16287278',99.8);
-- -- -- insert into jon.rods_ros values('16288967',99.8);
-- -- -- insert into jon.rods_ros values('16289136',99.8);
-- -- -- insert into jon.rods_ros values('16289972',99.8);
-- -- -- insert into jon.rods_ros values('16289412',99.95);
-- -- -- insert into jon.rods_ros values('16290175',99.95);
-- -- -- insert into jon.rods_ros values('16290179',99.95);
-- -- -- insert into jon.rods_ros values('16288116',100.35);
-- -- -- insert into jon.rods_ros values('16288672',102);
-- -- -- insert into jon.rods_ros values('16289381',103.48);
-- -- -- insert into jon.rods_ros values('16289115',110.76);
-- -- -- insert into jon.rods_ros values('16288658',120.42);
-- -- -- insert into jon.rods_ros values('16287726',123.37);
-- -- -- insert into jon.rods_ros values('16288691',125.11);
-- -- -- insert into jon.rods_ros values('16286880',125.57);
-- -- -- insert into jon.rods_ros values('16289539',126.5);
-- -- -- insert into jon.rods_ros values('16288480',127.1);
-- -- -- insert into jon.rods_ros values('16288713',127.1);
-- -- -- insert into jon.rods_ros values('16289067',127.1);
-- -- -- insert into jon.rods_ros values('16289707',127.1);
-- -- -- insert into jon.rods_ros values('16290065',127.1);
-- -- -- insert into jon.rods_ros values('16290130',127.1);
-- -- -- insert into jon.rods_ros values('16288263',128.39);
-- -- -- insert into jon.rods_ros values('16288657',132);
-- -- -- insert into jon.rods_ros values('16288619',132.99);
-- -- -- insert into jon.rods_ros values('16288831',133.75);
-- -- -- insert into jon.rods_ros values('16287605',136.49);
-- -- -- insert into jon.rods_ros values('16289055',136.49);
-- -- -- insert into jon.rods_ros values('16289416',144.75);
-- -- -- insert into jon.rods_ros values('16288933',145.23);
-- -- -- insert into jon.rods_ros values('16289213',145.34);
-- -- -- insert into jon.rods_ros values('16287304',146.11);
-- -- -- insert into jon.rods_ros values('16286517',159.23);
-- -- -- insert into jon.rods_ros values('16287727',159.23);
-- -- -- insert into jon.rods_ros values('16289360',159.71);
-- -- -- insert into jon.rods_ros values('16287126',159.95);
-- -- -- insert into jon.rods_ros values('16287321',159.95);
-- -- -- insert into jon.rods_ros values('16287584',159.95);
-- -- -- insert into jon.rods_ros values('16287609',159.95);
-- -- -- insert into jon.rods_ros values('16288498',159.95);
-- -- -- insert into jon.rods_ros values('16287023',160.96);
-- -- -- insert into jon.rods_ros values('16288182',165.23);
-- -- -- insert into jon.rods_ros values('16289238',169.12);
-- -- -- insert into jon.rods_ros values('16289311',173.24);
-- -- -- insert into jon.rods_ros values('16287273',177.04);
-- -- -- insert into jon.rods_ros values('16287189',177.81);
-- -- -- insert into jon.rods_ros values('16290174',177.94);
-- -- -- insert into jon.rods_ros values('16289557',178.49);
-- -- -- insert into jon.rods_ros values('16288281',179.07);
-- -- -- insert into jon.rods_ros values('16289415',180.23);
-- -- -- insert into jon.rods_ros values('16287595',181.1);
-- -- -- insert into jon.rods_ros values('16287585',181.98);
-- -- -- insert into jon.rods_ros values('16289585',190.65);
-- -- -- insert into jon.rods_ros values('16288824',193.65);
-- -- -- insert into jon.rods_ros values('16288146',195.09);
-- -- -- insert into jon.rods_ros values('16288620',196.05);
-- -- -- insert into jon.rods_ros values('16287443',196.79);
-- -- -- insert into jon.rods_ros values('16287807',199.75);
-- -- -- insert into jon.rods_ros values('16287850',199.75);
-- -- -- insert into jon.rods_ros values('16288692',199.75);
-- -- -- insert into jon.rods_ros values('16289012',199.75);
-- -- -- insert into jon.rods_ros values('16289164',199.75);
-- -- -- insert into jon.rods_ros values('16289419',199.75);
-- -- -- insert into jon.rods_ros values('16289580',199.75);
-- -- -- insert into jon.rods_ros values('16287034',203.36);
-- -- -- insert into jon.rods_ros values('16289787',203.38);
-- -- -- insert into jon.rods_ros values('16289193',204.73);
-- -- -- insert into jon.rods_ros values('16289665',204.73);
-- -- -- insert into jon.rods_ros values('16289662',216.07);
-- -- -- insert into jon.rods_ros values('16288410',223.61);
-- -- -- insert into jon.rods_ros values('16289908',223.98);
-- -- -- insert into jon.rods_ros values('16288129',227.48);
-- -- -- insert into jon.rods_ros values('16288493',227.89);
-- -- -- insert into jon.rods_ros values('16287737',235.5);
-- -- -- insert into jon.rods_ros values('16288429',235.6);
-- -- -- insert into jon.rods_ros values('16287590',238.85);
-- -- -- insert into jon.rods_ros values('16287808',238.85);
-- -- -- insert into jon.rods_ros values('16287759',248.48);
-- -- -- insert into jon.rods_ros values('16287715',254.2);
-- -- -- insert into jon.rods_ros values('16289065',254.2);
-- -- -- insert into jon.rods_ros values('16289349',254.2);
-- -- -- insert into jon.rods_ros values('16286257',261.6);
-- -- -- insert into jon.rods_ros values('16286901',266.26);
-- -- -- insert into jon.rods_ros values('16289588',276.01);
-- -- -- insert into jon.rods_ros values('16288814',284.35);
-- -- -- insert into jon.rods_ros values('16289080',284.35);
-- -- -- insert into jon.rods_ros values('16287868',299.9);
-- -- -- insert into jon.rods_ros values('16287455',305.04);
-- -- -- insert into jon.rods_ros values('16286855',307.1);
-- -- -- insert into jon.rods_ros values('16287867',316.86);
-- -- -- insert into jon.rods_ros values('16287474',317.75);
-- -- -- insert into jon.rods_ros values('16288725',317.75);
-- -- -- insert into jon.rods_ros values('16287754',318.47);
-- -- -- insert into jon.rods_ros values('16289358',338.8);
-- -- -- insert into jon.rods_ros values('16287431',343.17);
-- -- -- insert into jon.rods_ros values('16287876',343.17);
-- -- -- insert into jon.rods_ros values('16289063',343.17);
-- -- -- insert into jon.rods_ros values('16287442',353.48);
-- -- -- insert into jon.rods_ros values('16287738',361.39);
-- -- -- insert into jon.rods_ros values('16287781',363.97);
-- -- -- insert into jon.rods_ros values('16289575',366.86);
-- -- -- insert into jon.rods_ros values('16288438',368.59);
-- -- -- insert into jon.rods_ros values('16289354',381.3);
-- -- -- insert into jon.rods_ros values('16288112',398.09);
-- -- -- insert into jon.rods_ros values('16287616',409.19);
-- -- -- insert into jon.rods_ros values('16289076',420);
-- -- -- insert into jon.rods_ros values('16288661',436.27);
-- -- -- insert into jon.rods_ros values('16287799',459.67);
-- -- -- insert into jon.rods_ros values('16288481',475.67);
-- -- -- insert into jon.rods_ros values('16288016',480.21);
-- -- -- insert into jon.rods_ros values('16289652',508.4);
-- -- -- insert into jon.rods_ros values('16289520',534.58);
-- -- -- insert into jon.rods_ros values('16288931',554.62);
-- -- -- insert into jon.rods_ros values('16289525',555.35);
-- -- -- insert into jon.rods_ros values('16287576',558.2);
-- -- -- insert into jon.rods_ros values('16289644',584.8);
-- -- -- insert into jon.rods_ros values('16287881',591.45);
-- -- -- insert into jon.rods_ros values('16288937',594.8);
-- -- -- insert into jon.rods_ros values('16288559',597.84);
-- -- -- insert into jon.rods_ros values('16289165',597.84);
-- -- -- insert into jon.rods_ros values('16288552',622.79);
-- -- -- insert into jon.rods_ros values('16287444',625);
-- -- -- insert into jon.rods_ros values('16288963',684.06);
-- -- -- insert into jon.rods_ros values('16287430',686.33);
-- -- -- insert into jon.rods_ros values('16289767',693.81);
-- -- -- insert into jon.rods_ros values('16288411',697.32);
-- -- -- insert into jon.rods_ros values('16288142',722.74);
-- -- -- insert into jon.rods_ros values('16288613',758.56);
-- -- -- insert into jon.rods_ros values('16287625',771.89);
-- -- -- insert into jon.rods_ros values('16289889',772.69);
-- -- -- insert into jon.rods_ros values('16287264',797.49);
-- -- -- insert into jon.rods_ros values('16288502',818.93);
-- -- -- insert into jon.rods_ros values('16287600',869.38);
-- -- -- insert into jon.rods_ros values('16288267',883.06);
-- -- -- insert into jon.rods_ros values('16287029',924.62);
-- -- -- insert into jon.rods_ros values('16288292',977.75);
-- -- -- insert into jon.rods_ros values('16287604',1056);
-- -- -- insert into jon.rods_ros values('16289801',1517.12);
-- -- -- insert into jon.rods_ros values('16289196',1625.15);
-- -- -- insert into jon.rods_ros values('16286262',1630.4);
-- -- -- insert into jon.rods_ros values('16287457',2029.91);
-- -- -- insert into jon.rods_ros values('16287022',2147.99);
-- -- -- insert into jon.rods_ros values('16289220',2319.01);
-- -- -- insert into jon.rods_ros values('16287849',2604.64);
