﻿select * from sls.deals where stock_number = 'G35143R'

select * from nc.vehicles where vin = '1GCUYDED5KZ148849'

select distinct color from nc.vehicles where chrome_Style_id = '399780'  -- Black

select * from jon.configurations where chrome_Style_id = '399780'

graph shows 15 black on ground on 8/21/19 (14 factory 1 dt)

select * from nc.vehicle_configurations where configuration_id = 270

select *
from nc.vehicles
where configuration_id = 270
  and color = 'black'


select *
from nc.vehicles a
join nc.vehicle_acquisitions b on a.vin = b.vin
join nc.vehicle_sales c on b.stock_number = c.stock_number
  and b.ground_date = c.ground_date
where a.configuration_id = 270
  and a.color = 'black'
 order by a.vin 

create temp table we_have_them as
select b.stock_number, a.vin, b.ground_date, b.thru_date
from nc.vehicles a
join nc.vehicle_acquisitions b on a.vin = b.vin
where a.configuration_id = 270
  and a.color = 'black'
  and '08/21/2019'::Date between b.ground_date and b.thru_date

-- only gives changing prices, no sense of sale date
select * 
from nc.pricing_history
where vin =  '3GCUYDED2KG132589'
-- no help
select * 
from gmgl.vehicle_vis
where vin =  '3GCUYDED2KG132589'

select a.*, c.order_number, c.event_code, c.from_date, c.thru_date
from we_have_them a
left join gmgl.vehicle_orders b on a.vin = b.vin
left join gmgl.vehicle_order_events c on b.order_number = c.order_number
  and left(c.event_code, 1)::integer > 4
order by a.stock_number, c.from_Date


select a.control,
  min(d.the_date) filter (where c.journal_code = 'SVI') as serv_date,
  min(d.the_date) filter (where c.journal_code = 'VSN') as sale_date
from fin.fact_gl a
join fin.dim_Account b on a.account_key = b.account_key
  and b.account = '123700'
join fin.dim_journal c on a.journal_key = c.journal_key
  and c.journal_code in ('SVI','VSN')  
join dds.dim_date d on a.date_key = d.date_key  
where a.post_status = 'Y'
  and a.control in (
    select b.stock_number
    from nc.vehicles a
    join nc.vehicle_acquisitions b on a.vin = b.vin
    where a.configuration_id = 270
      and a.color = 'black'
      and '08/21/2019'::Date between b.ground_date and b.thru_date)  
group by a.control 