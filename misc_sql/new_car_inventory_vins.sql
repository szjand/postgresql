﻿-- fact_gl : control/account/store
select b.the_date, a.control, a.doc, a.ref, c.account, c.description, d.journal_code, e.description, c.account_type, a.amount
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and control = '29707'
  and c.account in (
    select account
    from fin.dim_account  
    where department_code = 'NC'
      and account_type = 'cogs'
      and store_code = 'ry1')
      and journal_code = 'PVI')

select *
from fin.dim_Account
where department_code = 'nc'
  and store_code = 'ry1'
  and account_type in ('asset','liability')

  
-- fact_gl : control/account/store
select b.the_date, a.control, a.doc, a.ref, c.account, c.description, d.journal_code, e.description, c.account_type --, amount--, sum(amount)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
--   and control = '13022'
  and c.account in (
    select account
    from fin.dim_account  
    where department_code = 'NC'
--       and account_type = 'cogs'
      and store_code = 'ry1')
  and journal_code = 'PVI'
group by b.the_date, a.control, a.doc, a.ref, c.account, c.description, d.journal_code, e.description, c.account_type
having sum(amount) > 0  



select sum(amount)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and control = '29075'


  
select inpmast_vin --315
from arkona.ext_inpmast
where status = 'I'
  and type_n_u = 'N'
  and manufacturer_code = 'GM'




-- this actually seems to work, sum of inventory account > 0 indicates in inventory
select control, sum(amount) 
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
  and b.account = '123105'
where post_Status = 'Y'
group by control
having sum(amount) > 1  

so, now, how to identify the inventory accounts

select *
from fin.dim_account
where store_code = 'ry1'
  and department_code = 'nc'
  and account_type = 'asset'
  and account <> '126104' -- factory receivables



select a.control
from fin.fact_gl a
inner join fin.dim_account c on a.account_key = c.account_key
where a.post_status = 'Y'
  and account in (
  select account
  from fin.dim_account
  where store_code = 'ry1'
    and department_code = 'nc'
    and account_type = 'asset'
    and account <> '126104') -- factory receivables)
group by a.control
having sum(amount) > 0


select count(*)  --169623
from arkona.ext_inpmast

select max(updated_timestamp)  -- 2016-08-22 13:57:48.92177-05
from arkona.ext_inpmast 
limit 1000

----------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------
-- -- -- 
-- -- -- as part of the silverado segmentation, we know what trim/color silverados sold in each 
-- -- -- month, need to know how many were in inventory each month
-- -- -- before i go to far, probably ought to do daily inventory, that can be aggregated *********
-- -- -- 
-- -- -- -- new silverado inventory account is 123700
-- -- -- select inventory_account, model, count(*)
-- -- -- from arkona.xfm_inpmast
-- -- -- where current_row = true
-- -- --   and make = 'chevrolet'
-- -- --   and type_n_u = 'n'
-- -- -- group by inventory_account, model  
-- -- -- order by model
-- -- -- 
-- -- -- -- inventory transactions are controlled by stock number, get vin from inpmast
-- -- -- drop table if exists inpmast;
-- -- -- create temp table inpmast as
-- -- -- select inpmast_stock_number, inpmast_vin, model, body_style, color, chrome_style_id
-- -- -- from arkona.xfm_inpmast
-- -- -- group by inpmast_stock_number, inpmast_vin, model, body_style, color, chrome_style_id;
-- -- -- 
-- -- -- -- so, i am looking, by stock number, for days on which the balance of 123700 > 0
-- -- -- 
-- -- -- drop table if exists inventory;
-- -- -- create temp table inventory as
-- -- -- select b.the_date, a.control, a.amount, -- d.inpmast_vin,
-- -- --   sum(amount)  over (partition by a.control order by b.the_date) as balance
-- -- -- from fin.fact_gl a
-- -- -- inner join dds.dim_date b on a.date_key = b.date_key
-- -- -- inner join fin.dim_account c on a.account_key = c.account_key
-- -- -- -- left join inpmast d on a.control = d.inpmast_stock_number
-- -- -- -- left join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
-- -- -- where post_Status = 'Y'
-- -- --   and c.account = '123700';
-- -- -- --   and b.the_year > 2014
-- -- -- -- order by a.control, b.the_date
-- -- -- -- limit 1000
-- -- -- 
-- -- -- -- not unique
-- -- -- select control, the_date
-- -- -- from inventory
-- -- -- group by control, the_date
-- -- -- having count(*) > 1
-- -- -- 
-- -- -- -- not quite unique
-- -- -- select control, the_date, amount
-- -- -- from inventory
-- -- -- group by control, the_date, amount
-- -- -- having count(*) > 1
-- -- -- 
-- -- -- select * from inventory where control = '32601'
-- -- -- 
-- -- -- select * 
-- -- -- from inventory
-- -- -- where control = '31387'
-- -- --   and the_date = '06/22/2017'
-- -- -- 
-- -- -- 
-- -- -- select * from inventory order by control, the_date
-- -- -- 
-- -- -- 
-- -- -- -- multiple days with 0 bal
-- -- -- select *
-- -- -- from inventory
-- -- -- where control in (
-- -- --   select control
-- -- --   from (
-- -- --     select control, the_date
-- -- --     from inventory
-- -- --     where balance = 0
-- -- --     group by control, the_date) a
-- -- --   group by control
-- -- --   having count(*) > 1 ) 
-- -- -- 
-- -- -- -- thinking limit to transactions < +- 10K
-- -- -- select * from inventory order by abs(amount)
-- -- -- 
-- -- -- select * from inventory where control = '32145'
-- -- -- 
-- -- -- select min(the_date) from fin.fact_gl a inner join dds.dim_date b on a.date_key = b.date_key
-- -- -- !!! fact_gl starts with 01/01/2011
-- -- -- 
-- -- -- -- minimum list price = 21557.14
-- -- -- select inpmast_stock_number, inpmast_vin, date_in_invent, date_delivered, list_price
-- -- -- from arkona.xfm_inpmast 
-- -- -- where model = 'silverado 1500'
-- -- --   and type_n_u = 'n'
-- -- -- order by list_price
-- -- -- 
-- -- -- -- limit to transactions > 12000
-- -- -- -- results in many with bal <> 0, but may be good enuf
-- -- -- drop table if exists inventory;
-- -- -- create temp table inventory as
-- -- -- select b.the_date, a.control, a.amount, -- d.inpmast_vin,
-- -- --   sum(amount)  over (partition by a.control order by b.the_date) as balance
-- -- -- from fin.fact_gl a
-- -- -- inner join dds.dim_date b on a.date_key = b.date_key
-- -- -- inner join fin.dim_account c on a.account_key = c.account_key
-- -- -- where post_Status = 'Y'
-- -- --   and c.account = '123700'
-- -- --   and abs(a.amount) > 12000
-- -- -- 
-- -- -- select * 
-- -- -- from inventory  
-- -- -- 
-- -- -- -- of the 7047, only 92 have more than 2 transactions
-- -- -- select control, count(*)
-- -- -- from inventory
-- -- -- group by control
-- -- -- order by count(*) desc
-- -- -- 
-- -- -- select *
-- -- -- from inventory
-- -- -- where control in (
-- -- --   select control
-- -- --   from inventory
-- -- --   group by control
-- -- --   having count(*) > 2)
-- -- -- order by control, the_date  
-- -- -- 
-- -- -- -- not liking this, maybe, rather it's in the analysis of transactions
-- -- -- -- something like if balance > 12k then it is in inventory
-- -- -- -- add the journal
-- -- -- drop table if exists inventory;
-- -- -- create temp table inventory as
-- -- -- select b.the_date, a.control, d.journal_code, a.amount, -- d.inpmast_vin,
-- -- --   sum(amount)  over (partition by a.control order by b.the_date) as balance
-- -- -- from fin.fact_gl a
-- -- -- inner join dds.dim_date b on a.date_key = b.date_key
-- -- -- inner join fin.dim_account c on a.account_key = c.account_key
-- -- -- inner join fin.dim_journal d on a.journal_key = d.journal_key
-- -- -- where post_Status = 'Y'
-- -- --   and c.account = '123700';
-- -- -- -- clean up some garbage
-- -- -- delete from inventory where control in ('3378','BZ258632','CENTER','CG169537','CZ180455','REF042913');
-- -- -- 
-- -- -- -- trying to sort out backons
-- -- -- -- of the 7000 rows, 92 have more than 2 transactions > 12000
-- -- -- select control, count(*)
-- -- -- from inventory  
-- -- -- where abs(amount) > 12000
-- -- -- group by control
-- -- -- order by count(*) desc 
-- -- -- 
-- -- -- select *
-- -- -- from inventory
-- -- -- where control in (
-- -- --   select control
-- -- --   from inventory  
-- -- --   where abs(amount) > 12000
-- -- --   group by control
-- -- --   having count(*) > 2) 
-- -- -- order by control, the_date
-- -- -- 
-- -- -- 
-- -- -- select * from arkona.xfm_inpmast where inpmast_vin = '3GCUKRECXJG206250' order by inpmast_key
-- -- -- 
-- -- -- select * from sls.ext_bopmast_partial where vin = '3GCUKRECXJG206250'
-- -- -- 
-- -- -- select * from sls.xfm_deals where vin = '3GCUKRECXJG206250'
-- -- -- 
-- -- -- select * 
-- -- -- from sls.xfm_deals a
-- -- -- where vehicle_type = 'N'
-- -- --   and exists (
-- -- --     select 1
-- -- --     from sls.xfm_deals
-- -- --     where vin = a.vin
-- -- --       and gl_count = -1)
-- -- -- order by vin, bopmast_id, seq   
-- -- -- 
-- -- -- -- shit was hoping all inventory acquisitions were PVI, these 1106 are not
-- -- -- select a.*
-- -- -- from inventory a
-- -- -- inner join (
-- -- --   select control, min(the_date) as the_date
-- -- --   from inventory
-- -- --   group by control) b on a.control = b.control and a.the_date = b.the_date
-- -- -- where a.journal_code <> 'PVI'  
-- -- -- order by control  

-- all the above kind of panned out
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-- inventory transactions are controlled by stock number, get vin from inpmast
drop table if exists inpmast;
create temp table inpmast as
select inpmast_stock_number, inpmast_vin, model, model_code, body_style, color, chrome_style_id
from arkona.xfm_inpmast
group by inpmast_stock_number, inpmast_vin, model, model_code, body_style, color, chrome_style_id;

-- not sure how this will play out, group by date, control, 

drop table if exists inventory;
create temp table inventory as
select the_Date, control, amount,
  sum(amount)  over (partition by control order by the_date) as balance
from (  
  select b.the_date, a.control, sum(a.amount) as amount
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
  inner join fin.dim_account c on a.account_key = c.account_key
  where post_Status = 'Y'
    and c.account = '123700'
  group by b.the_date, a.control) c;
delete from inventory where control in ('3378','BZ258632','CENTER','CG169537','CZ180455','REF042913');

min date where bal > 12k
min date where bal  = 0

-- crude approximation, does not account for back ons, good enuf for now
drop table if exists inventory_2;
create temp table inventory_2 as
select a.control, a.from_date, coalesce(b.thru_date, '12/31/9999') as thru_date
from ( -- into inventory
  select control, min(the_date) as from_date
  from inventory 
  where balance > 12000
  group by control) a
left join ( -- out of inventory
  select control, min(the_date) as thru_date
  from inventory
  where balance = 0 group by control) b on a.control = b.control;



-- current inventory
select *
from inventory_2
where from_date <= current_date
  and thru_date >= current_date
  
-- inventory 201703 --213
select *
from inventory_2
where from_date <= '01/31/2017'
  and thru_date >= '03/01/2017'

-- inventory 2017, with vins
select *
from inventory_2 a --1176
inner join inpmast b on a.control = b.inpmast_stock_number
  and b.model = 'silverado 1500'
where from_date <= '12/31/2017'
  and thru_date >= '01/01/2017'
order by from_date  

-- inventory 201702, with vins
select a.*, b.*, count(*) over (partition by color)
from inventory_2 a --1176
inner join inpmast b on a.control = b.inpmast_stock_number
  and b.model = 'silverado 1500'
where from_date <= '02/28/2017'
  and thru_date >= '02/01/2017'



-- current inventory
-- inventory 201702, with vins
select a.*, b.*, count(*) over (partition by color)
from inventory_2 a --1176
inner join inpmast b on a.control = b.inpmast_stock_number
  and b.model = 'silverado 1500'
where from_date <= current_date
  and thru_date >= current_date


select * from arkona.xfm_inpmast limit 10

select year, make, model_code, model, body_style, inventory_account, chrome_style_id, count(*)
from arkona.xfm_inpmast
where current_row = true
  and type_n_u = 'n'
  and year between 2016 and 2018
  and make = 'chevrolet'
--   and model = 'silverado 1500'
--   and body_style like '%CREW%'
group by year, make, model_code, model, body_style, inventory_account, chrome_style_id
order by inventory_account, model



select year, inpmast_stock_number, make, model_code, model, body_style, inventory_account, chrome_style_id
from arkona.xfm_inpmast
where current_row = true
  and type_n_u = 'n'
  and year between 2016 and 2018
  and make = 'chevrolet'
  and model = 'silverado 1500'



-- inventory 2017, with vins
-- good inpast.chrome_style_id: trim comes from chr.year_make_model_style.style_name
select a.*, b.inpmast_vin, b.color, c.style_name
from inventory_2 a --1176
inner join inpmast b on a.control = b.inpmast_stock_number
  and b.model = 'silverado 1500'
  and b.body_style like '%CREW%'
left join chr.year_make_model_style c on b.chrome_style_id = c.chrome_style_id     
where a.from_date <= '12/31/2017'
  and a.thru_date >= '01/01/2017'
  and c.model_name = 'silverado 1500' -- good inpast.chrome_style_id

-- bad inpmast.chrome_Style_id
select e.control, e.from_Date, e.thru_date, e.inpmast_vin, e.color, h.style_name
from (
  select a.*, b.*
  from inventory_2 a --1176
  inner join inpmast b on a.control = b.inpmast_stock_number
    and b.model = 'silverado 1500'
    and b.body_style like '%CREW%'
  left join chr.year_make_model_style c on b.chrome_style_id = c.chrome_style_id     
  where a.from_date <= '12/31/2017'
    and a.thru_date >= '01/01/2017'
    and c.model_name <> 'silverado 1500') e -- good inpast.chrome_style_id  
left join lateral (select * from chr.decode_vin(e.inpmast_vin)) f on 1 = 1    
left join chr.vin_pattern_style_mapping g on f.vin_pattern_id = g.vin_pattern_id
left join chr.styles h on g.chrome_style_id = h.style_id
  and e.model_code = h.full_style_code
  and
    case 
      when e.body_style like '%Z71%' then h.cf_style_name like '%Z71%'
      when e.body_style not like '%Z71%' then h.cf_style_name not like '%Z71%'
    end    
where h.style_id is not null

-- put them both together for 2017 inventory  

drop table if exists inventory_2017;
create temp table inventory_2017 as
select a.*, b.inpmast_vin, b.color, c.style_name
from inventory_2 a --1176
inner join inpmast b on a.control = b.inpmast_stock_number
  and b.model = 'silverado 1500'
  and b.body_style like '%CREW%'
left join chr.year_make_model_style c on b.chrome_style_id = c.chrome_style_id     
where a.from_date <= '12/31/2017'
  and a.thru_date >= '01/01/2017'
  and c.model_name = 'silverado 1500'-- good inpast.chrome_style_id
union
select e.control, e.from_Date, e.thru_date, e.inpmast_vin, e.color, h.style_name
from (
  select a.*, b.*
  from inventory_2 a --1176
  inner join inpmast b on a.control = b.inpmast_stock_number
    and b.model = 'silverado 1500'
    and b.body_style like '%CREW%'
  left join chr.year_make_model_style c on b.chrome_style_id = c.chrome_style_id     
  where a.from_date <= '12/31/2017'
    and a.thru_date >= '01/01/2017'
    and c.model_name <> 'silverado 1500') e -- good inpast.chrome_style_id  
left join lateral (select * from chr.decode_vin(e.inpmast_vin)) f on 1 = 1    
left join chr.vin_pattern_style_mapping g on f.vin_pattern_id = g.vin_pattern_id
left join chr.styles h on g.chrome_style_id = h.style_id
  and e.model_code = h.full_style_code
  and
    case 
      when e.body_style like '%Z71%' then h.cf_style_name like '%Z71%'
      when e.body_style not like '%Z71%' then h.cf_style_name not like '%Z71%'
    end    
where h.style_id is not null;   

create index on inventory_2017(from_date);
create index on inventory_2017(thru_date);

select *
from inventory_2017
order by color

update inventory_2017
set color = 
  case 
    when color like '%BLACK%' then 'BLACK' 
    when color like '%ocean%' then 'DEEP OCEAN BLUE METALLIC'
    when color like '%iri%' then 'IRIDESCENT PEARL TRICOAT'
    when color like '%ocean%' then 'DEEP OCEAN BLUE METALLIC'
    when color like '%iri%' then 'IRIDESCENT PEARL TRICOAT'
    when color like 'pep%' then 'PEPPERDUST METALLIC'
    when color like 'siren%' then 'SIREN RED TINTCOAT'
    when color like 'sum%' then 'SUMMIT WHITE'
    else color
  end;



 select b.month_name, 
  count(b.year_month) filter (where color = 'BLACK') as "BLACK",
  count(b.year_month) filter (where color = 'DEEP OCEAN BLUE METALLIC') as "DEEP OCEAN BLUE METALLIC",
  count(b.year_month) filter (where color = 'CAJUN RED TINTCOAT') as "CAJUN RED TINTCOAT",
  count(b.year_month) filter (where color = 'GRAPHITE METALLIC') as "GRAPHITE METALLIC",
  count(b.year_month) filter (where color = 'IRIDESCENT PEARL TRICOAT') as "IRIDESCENT PEARL TRICOAT",
  count(b.year_month) filter (where color = 'PEPPERDUST METALLIC') as "PEPPERDUST METALLIC",
  count(b.year_month) filter (where color = 'RED HOT') as "RED HOT",
  count(b.year_month) filter (where color = 'SILVER ICE METALLIC') as "SILVER ICE METALLIC",
  count(b.year_month) filter (where color = 'SIREN RED TINTCOAT') as "SIREN RED TINTCOAT",
  count(b.year_month) filter (where color = 'SUMMIT WHITE') as "SUMMIT WHITE",
  count(b.year_month) filter (where color = 'UNRIPENED GREEN') as "UNRIPENED GREEN"
from (
  select year_month, month_name, the_date
  from dds.dim_date 
  where year_month between 201702 and 201712) b
left join (
  select from_Date, thru_date, color,style_name
  from inventory_2017
  where style_name like '%1LT%') a on b.the_date between a.from_date and a.thru_date
group by b.year_month, b.month_name


select a.from_Date, a.thru_date, color,style_name,
  case when exists (select 1 from dds.dim_date where the_date between a.from_date and a.thru_date and year_month = 201702) then 1 end as february,
  case when exists (select 1 from dds.dim_date where the_date between a.from_date and a.thru_date and year_month = 201703) then 1 end as mar,
  case when exists (select 1 from dds.dim_date where the_date between a.from_date and a.thru_date and year_month = 201704) then 1 end as april,
  case when exists (select 1 from dds.dim_date where the_date between a.from_date and a.thru_date and year_month = 201705) then 1 end as may
from inventory_2017 a
where a.style_name like '%1LT%'


select b.year_month, a.*
from inventory_2017 a
left join dds.dim_date b on b.the_date between a.from_date and a.thru_date
where style_name like '%1LT%'    


select year_month, month_name, the_date
from dds.dim_date 
where year_month between 201702 and 201712

one row for each vehicle for each month it was in inventory

select *
from inventory_2017

-- starting to look like a good case for date range
-- 
-- look for overlapping date ranges
-- range 1: year_month begin date -> end date
-- range 2: inventory from_date -> thru_date
-- 
-- which year months does the inventory period include
-- i want one row of the vehicle for each month
-- 
-- select * from dds.dim_date where the_date = current_date
-- 
-- alter table dds.dim_date
-- add column last_of_month date;
-- 
-- update dds.dim_date x
-- set last_of_month = y.the_date
-- from (
--   select year_month, the_date
--   from dds.dim_date
--   where last_day_of_month) y
-- where x.year_month = y.year_month;  
-- 
-- create index on dds.dim_date(first_of_month);
-- create index on dds.dim_date(last_of_month);

select year_month, daterange(first_of_month, last_of_month)
from dds.dim_date
where year_month between 201702 and 201712
group by year_month, first_of_month, last_of_month


select distinct from_date, thru_date -- 360
from inventory_2017


select distinct year_month
from dds.dim_date
where the_date between '10/28/2017' and '04/21/2017'

select * 
from inventory_2017
limit 10

-- one row for each day vehicle is in inventory
drop table if exists inventory_days;
create temp table inventory_days as
select *
from (
  select the_date, year_month
  from dds.dim_date
  where year_month between 201702 and 201712) a
left join (
  select * 
  from inventory_2017) b on a.the_date between b.from_date and b.thru_date;

select year_month, color, style_name
from inventory_days

-- i think this is it
select b.month_name, 
  count(b.year_month) filter (where color = 'BLACK') as "BLACK",
  count(b.year_month) filter (where color = 'DEEP OCEAN BLUE METALLIC') as "DEEP OCEAN BLUE METALLIC",
  count(b.year_month) filter (where color = 'CAJUN RED TINTCOAT') as "CAJUN RED TINTCOAT",
  count(b.year_month) filter (where color = 'GRAPHITE METALLIC') as "GRAPHITE METALLIC",
  count(b.year_month) filter (where color = 'IRIDESCENT PEARL TRICOAT') as "IRIDESCENT PEARL TRICOAT",
  count(b.year_month) filter (where color = 'PEPPERDUST METALLIC') as "PEPPERDUST METALLIC",
  count(b.year_month) filter (where color = 'RED HOT') as "RED HOT",
  count(b.year_month) filter (where color = 'SILVER ICE METALLIC') as "SILVER ICE METALLIC",
  count(b.year_month) filter (where color = 'SIREN RED TINTCOAT') as "SIREN RED TINTCOAT",
  count(b.year_month) filter (where color = 'SUMMIT WHITE') as "SUMMIT WHITE",
  count(b.year_month) filter (where color = 'UNRIPENED GREEN') as "UNRIPENED GREEN"
from (
  select year_month, month_name
  from dds.dim_date 
  where year_month between 201702 and 201712
  group by year_month, month_name) b
left join (
  select year_month, control, color, style_name
  from inventory_days
  where style_name like '%1LT%'
  group by year_month, control, color, style_name) a on b.year_month = a.year_month  
group by b.year_month, b.month_name
order by b.year_month

select *
from inventory_days
where color like '%OCEAN%'
  and style_name like '%1LT%'
order by control  



  select year_month, control, color, style_name
  from inventory_days
  where year_month = 201712
  group by year_month, control, color, style_name

comparing to sales (silverado_segmentation.sql)  
find a way to include sales/inventory in same output


select count(distinct control) from inventory_days where color = 'black' and from_date <= '10/31/2017' and thru_date >= '10/01/2017'       


------------------------------------------------------------------------------------------------------------------------------
-- all constituent parts
------------------------------------------------------------------------------------------------------------------------------
-- 2017 sales of silverados
-- include stock_number, vin
-- 1-27 exclude wholesale deals
drop table if exists silverados;
create temp table silverados as
select a.stock_number, a.vin, a.year_month, b.color, 
  c.style_name -- 220
from sls.deals_by_month a -- 457
inner join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row = true
left join chr.year_make_model_style c on b.chrome_style_id = c.chrome_style_id  
where a.model = 'silverado 1500'
  and a.sale_type <> 'Wholesale'
  and a.vehicle_type = 'New'
  and b.body_style like '%CREW%' -- 309
  -- those that decode correctly
  and c.model_name = 'silverado 1500' -- 220
  and a.unit_count > 0
union all
select e.stock_number, e.vin, e.year_month, e.color, 
  h.style_name  
from (
  select a.year_month, a.stock_number, a.vin, 
    b.color, b.make, b.model_code, b.body_style, b.chrome_style_id,
    c.division_name, c.model_name, c.style_name
  -- select *  
  from sls.deals_by_month a -- 457
  inner join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
    and b.current_row = true
  left join chr.year_make_model_style c on b.chrome_style_id = c.chrome_style_id  
  where a.model = 'silverado 1500'
    and a.sale_type <> 'Wholesale'
    and vehicle_type = 'New'
    and b.body_style like '%CREW%' -- 309
    and coalesce(c.model_name, 'xxx') <> 'silverado 1500'
    and a.unit_count > 0) e
left join lateral (select * from chr.decode_vin(e.vin)) f on 1 = 1    
left join chr.vin_pattern_style_mapping g on f.vin_pattern_id = g.vin_pattern_id
left join chr.styles h on g.chrome_style_id = h.style_id
  and e.model_code = h.full_style_code
  and
    case 
      when e.body_style like '%Z71%' then h.cf_style_name like '%Z71%'
      when e.body_style not like '%Z71%' then h.cf_style_name not like '%Z71%'
    end
where h.style_id is not null   
order by color;


-- inventory transactions are controlled by stock number, get vin from inpmast
drop table if exists inpmast;
create temp table inpmast as
select inpmast_stock_number, inpmast_vin, model, model_code, body_style, color, chrome_style_id
from arkona.xfm_inpmast
group by inpmast_stock_number, inpmast_vin, model, model_code, body_style, color, chrome_style_id;

-- not sure how this will play out, group by date, control, 
drop table if exists inventory;
create temp table inventory as
select the_Date, control, amount,
  sum(amount)  over (partition by control order by the_date) as balance
from (  
  select b.the_date, a.control, sum(a.amount) as amount
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
  inner join fin.dim_account c on a.account_key = c.account_key
  where post_Status = 'Y'
    and c.account = '123700'
  group by b.the_date, a.control) c;
delete from inventory where control in ('3378','BZ258632','CENTER','CG169537','CZ180455','REF042913');

-- crude approximation, does not account for back ons, good enuf for now
drop table if exists inventory_2;
create temp table inventory_2 as
select a.control, a.from_date, coalesce(b.thru_date, '12/31/9999') as thru_date
from ( -- into inventory
  select control, min(the_date) as from_date
  from inventory 
  where balance > 12000
  group by control) a
left join ( -- out of inventory
  select control, min(the_date) as thru_date
  from inventory
  where balance = 0 group by control) b on a.control = b.control;


-- put them both together for 2017 inventory  
drop table if exists inventory_2017;
create temp table inventory_2017 as
select a.*, b.inpmast_vin, b.color, c.style_name
from inventory_2 a --1176
inner join inpmast b on a.control = b.inpmast_stock_number
  and b.model = 'silverado 1500'
  and b.body_style like '%CREW%'
left join chr.year_make_model_style c on b.chrome_style_id = c.chrome_style_id     
where a.from_date <= '12/31/2017'
  and a.thru_date >= '01/01/2017'
  and c.model_name = 'silverado 1500'-- good inpast.chrome_style_id
union
select e.control, e.from_Date, e.thru_date, e.inpmast_vin, e.color, h.style_name
from (
  select a.*, b.*
  from inventory_2 a --1176
  inner join inpmast b on a.control = b.inpmast_stock_number
    and b.model = 'silverado 1500'
    and b.body_style like '%CREW%'
  left join chr.year_make_model_style c on b.chrome_style_id = c.chrome_style_id     
  where a.from_date <= '12/31/2017'
    and a.thru_date >= '01/01/2017'
    and c.model_name <> 'silverado 1500') e -- good inpast.chrome_style_id  
left join lateral (select * from chr.decode_vin(e.inpmast_vin)) f on 1 = 1    
left join chr.vin_pattern_style_mapping g on f.vin_pattern_id = g.vin_pattern_id
left join chr.styles h on g.chrome_style_id = h.style_id
  and e.model_code = h.full_style_code
  and
    case 
      when e.body_style like '%Z71%' then h.cf_style_name like '%Z71%'
      when e.body_style not like '%Z71%' then h.cf_style_name not like '%Z71%'
    end    
where h.style_id is not null;   

update inventory_2017
set color = 
  case 
    when color like '%BLACK%' then 'BLACK' 
    when color like '%ocean%' then 'DEEP OCEAN BLUE METALLIC'
    when color like '%iri%' then 'IRIDESCENT PEARL TRICOAT'
    when color like '%ocean%' then 'DEEP OCEAN BLUE METALLIC'
    when color like '%iri%' then 'IRIDESCENT PEARL TRICOAT'
    when color like 'pep%' then 'PEPPERDUST METALLIC'
    when color like 'siren%' then 'SIREN RED TINTCOAT'
    when color like 'sum%' then 'SUMMIT WHITE'
    else color
  end;

-- one row for each day vehicle is in inventory
drop table if exists inventory_days;
create temp table inventory_days as
select *
from (
  select the_date, year_month
  from dds.dim_date
  where year_month between 201702 and 201712) a
left join (
  select * 
  from inventory_2017) b on a.the_date between b.from_date and b.thru_date;


-- select distinct style_name from silverados 1LT, 2LT, 1LZ, 2LZ
-- ok, got rid of the anomalies caused by wholesales
select x.month_name, x."BLACK", '  ', y."BLACK", '  ' , x."DEEP OCEAN BLUE METALLIC",'  ' , y."DEEP OCEAN BLUE METALLIC",'  ' , 
  x."CAJUN RED TINTCOAT", '  ', y."CAJUN RED TINTCOAT", '  ' , x."GRAPHITE METALLIC",'  ' , y."GRAPHITE METALLIC",'  ' , 
  x."IRIDESCENT PEARL TRICOAT", '  ', y."IRIDESCENT PEARL TRICOAT", '  ' , x."PEPPERDUST METALLIC",'  ' , y."PEPPERDUST METALLIC",'  ' , 
  x."RED HOT", '  ', y."RED HOT",'  ' ,  x."SILVER ICE METALLIC",'  ' , y."SILVER ICE METALLIC",'  ' , 
  x."SIREN RED TINTCOAT", '  ', y."SIREN RED TINTCOAT", '  ' , x."SUMMIT WHITE",'  ' , y."SUMMIT WHITE",'  ' , 
  x."UNRIPENED GREEN", '  ', y."UNRIPENED GREEN"
from (
  select b.year_month, b.month_name, 
    count(b.year_month) filter (where color = 'BLACK') as "BLACK",
    count(b.year_month) filter (where color = 'DEEP OCEAN BLUE METALLIC') as "DEEP OCEAN BLUE METALLIC",
    count(b.year_month) filter (where color = 'CAJUN RED TINTCOAT') as "CAJUN RED TINTCOAT",
    count(b.year_month) filter (where color = 'GRAPHITE METALLIC') as "GRAPHITE METALLIC",
    count(b.year_month) filter (where color = 'IRIDESCENT PEARL TRICOAT') as "IRIDESCENT PEARL TRICOAT",
    count(b.year_month) filter (where color = 'PEPPERDUST METALLIC') as "PEPPERDUST METALLIC",
    count(b.year_month) filter (where color = 'RED HOT') as "RED HOT",
    count(b.year_month) filter (where color = 'SILVER ICE METALLIC') as "SILVER ICE METALLIC",
    count(b.year_month) filter (where color = 'SIREN RED TINTCOAT') as "SIREN RED TINTCOAT",
    count(b.year_month) filter (where color = 'SUMMIT WHITE') as "SUMMIT WHITE",
    count(b.year_month) filter (where color = 'UNRIPENED GREEN') as "UNRIPENED GREEN"
  from (
    select year_month, month_name
    from dds.dim_date 
    where year_month between 201702 and 201712
    group by year_month, month_name) b
  left join (
    select year_month, control, color, style_name
    from inventory_days
    where style_name like '%2LZ%'
    group by year_month, control, color, style_name) a on b.year_month = a.year_month  
  group by b.year_month, b.month_name) x
left join (
  select b.year_month, b.month_name, 
    count(b.year_month) filter (where color = 'BLACK') as "BLACK",
    count(b.year_month) filter (where color = 'DEEP OCEAN BLUE METALLIC') as "DEEP OCEAN BLUE METALLIC",
    count(b.year_month) filter (where color = 'CAJUN RED TINTCOAT') as "CAJUN RED TINTCOAT",
    count(b.year_month) filter (where color = 'GRAPHITE METALLIC') as "GRAPHITE METALLIC",
    count(b.year_month) filter (where color = 'IRIDESCENT PEARL TRICOAT') as "IRIDESCENT PEARL TRICOAT",
    count(b.year_month) filter (where color = 'PEPPERDUST METALLIC') as "PEPPERDUST METALLIC",
    count(b.year_month) filter (where color = 'RED HOT') as "RED HOT",
    count(b.year_month) filter (where color = 'SILVER ICE METALLIC') as "SILVER ICE METALLIC",
    count(b.year_month) filter (where color = 'SIREN RED TINTCOAT') as "SIREN RED TINTCOAT",
    count(b.year_month) filter (where color = 'SUMMIT WHITE') as "SUMMIT WHITE",
    count(b.year_month) filter (where color = 'UNRIPENED GREEN') as "UNRIPENED GREEN"
  from (
    select year_month, month_name
    from dds.dim_date 
    where year_month between 201702 and 201712
    group by year_month, month_name) b
  left join (
    select year_month, 
      case
        when color like '%black%' then 'BLACK'      
        when color like '%ocean%' then 'DEEP OCEAN BLUE METALLIC'
        when color = 'GRAPHITE METALLIC' then 'GRAPHITE METALLIC'
        when color like '%iri%' then 'IRIDESCENT PEARL TRICOAT'
        when color like 'pep%' then 'PEPPERDUST METALLIC'
        when color = 'RED HOT' then 'RED HOT'
        when color like 'CAJUN RED%' then 'CAJUN RED TINTCOAT'
        when color = 'SILVER ICE METALLIC' then 'SILVER ICE METALLIC'
        when color like 'siren%' then 'SIREN RED TINTCOAT'
        when color like 'sum%' then 'SUMMIT WHITE'
        when color = 'UNRIPENED GREEN' then 'UNRIPENED GREEN'
        else '******************************'
      END AS color,
      style_name
    from silverados --) a on b.year_month = a.year_month  
    where style_name like '%2LZ%') a on b.year_month = a.year_month  
  group by b.year_month, b.month_name) y on x.month_name = y.month_name
  order by y.year_month

  
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------

-- taylors project
-- need to add engine size, add trims: LS, WT
-- 4/9/18
select * from silverados

