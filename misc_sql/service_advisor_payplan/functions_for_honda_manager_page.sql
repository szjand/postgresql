﻿create or replace function sap.get_ros_by_pay_period(_pay_period_indicator integer)
returns setof json as
$BODY$
/*
select sap.get_ros_by_pay_period(0)
*/
begin
return query
	select row_to_json(a)
	from (
		select array_to_json(array_agg(row_to_json(b))) as ro
		from (
			select ro,sum(amount) filter (where category = 'labor') as labor_sales, 
				sum(amount) filter (where category = 'discount') as discount_sales, 
				sum(amount) filter (where category = 'road hazard') as road_hazard, 
				sum(amount) filter (where category = 'sublet') as sublet,
				sum(amount) filter (where category = 'parts') as parts, 
				sum(amount) as ro_total
			from sap.sales_details
			where pay_period_id = (
				select distinct biweekly_pay_period_sequence + _pay_period_indicator
				from dds.dim_date
				where the_date = current_date)
				and store = 'RY2'
			group by ro) b) a;
end
$BODY$
language plpgsql;
comment on  function sap.get_ros_by_pay_period(integer) is 'allow honda manager to view all the ros contributing to sales for service advisor payroll for a given pay period'

create or replace function sap.get_ro(_ro citext)
returns setof json as
$BODY$
/*
select sap.get_ro('2840145')
*/
begin
return query
	select row_to_json(a)
	from (
		select array_to_json(array_agg(row_to_json(b))) as ro
		from (
			select the_date, ro,sum(amount) filter (where category = 'labor') as labor_sales, 
				sum(amount) filter (where category = 'discount') as discount_sales, 
				sum(amount) filter (where category = 'road hazard') as road_hazard, 
				sum(amount) filter (where category = 'sublet') as sublet,
				sum(amount) filter (where category = 'parts') as parts, 
				sum(amount) as ro_total
			from sap.sales_details
			where ro = _ro
			group by the_date, ro) b) a;
end
$BODY$
language plpgsql;

comment on function sap.get_ro(citext) is 'allow manager to see the entire history of an ro as it applies to advisor payroll';