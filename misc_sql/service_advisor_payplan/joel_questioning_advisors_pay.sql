﻿/*
My advisors should be paid the same way that the chevy advisors get paid.  
On total sales but it looks like some sales are omitted for some reason.  
It also shows on their page that they lose sales periodically on repair orders.  
Their last payroll showed 86K and change.  
I print sales report and sales are 93,458.

Some op codes I see that are a problem are 4nt, 4nto, Lof0205, Lofd, Lof,  
many repairs that we have to hit F6 and input customer concern.  
Please let me know.

*/
-- sent this to joel
-- pay period 07-04 -> 07-17
select ro, account, category, amount
from sap.sales_details
where store = 'ry2'
  and pay_period_id = 328


/* 
7/21/21 from joel  
I have highlighted some discrepancies that the guys have found.  This is mainly with parts but a few with labor also.  There is a stack more that they sent to Randy.  It looks like the tire opcodes and the oil change op codes are triggering this for some reason.

Repair Order                      Labor on Ro        Labor on Report               Discount              Parts on Ro         Parts on Report
2841475                                224.95                   224.95                                   178.00                   712.00                   0
2841494                                224.95                   224.95                                   191.00                   2610.68                 1846.68
2841554                                224.95                   224.95                                   169.00                   676.00                   0
2841729                                224.95                   224.95                                   163.00                   652.00                   0
2841820                                224.95                   224.95                                   152.50                   610.00                   0
2841880                                250.96                   224.95                                   186.00                   766.25                   7                              
*/


select *
from sap.sales_details
where ro = '2841475'

select c.*, a.*
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
join sap.sale_accounts c on b.account = c.account
where a.post_Status = 'Y'
  and a.control = '2841475'
order by b.account 

select * from fin.dim_account where account = '249000'

select * from sap.sale_accounts where store = 'RY2' order by account

sap.sale_accounts is missing AT LEAST 249000
sent the info to jeri

07/27/21
jeri added 249000 to the pay pln
updated sap.sale_accounts
FUNCTION sap.update_sales_details() will handle updating the current pay period 7/18/21 - 7/31/21
need to generate a back pay statement for 249000 from the start thru 7/17/21


-- from FUNCTION sap.update_sales_details()
select d.the_date, d.biweekly_pay_period_sequence, a.control as ro, b.account, 
  b.category, sum(-a.amount) as amount, b.store
from fin.fact_gl a
join sap.sale_accounts b on a.account_key = b.account_key
  and b.category = 'parts' and b.account = '249000'
join fin.dim_journal c on a.journal_key = c.journal_key
  and c.journal_code in ('SCA','SVI','SWA')  
join dds.dim_date d on a.date_key = d.date_key
where a.post_status = 'Y'
  and d.the_date between '07/18/2021' and current_date
group by d.the_date, d.biweekly_pay_period_sequence, a.control, b.account, b.category;


modify it for the relevant period

select min(the_date) from sap.sales_details where account like '2%'  -- 2/15/21

select * from sap.sales_details where account like '2%' order by the_date limit 200

select * from sap.payroll_submittals where store = 'ry2'

select min(the_date) from sap.clock_hours where employee_number like '2%'

select * from sap.clock_hours where employee_number like '2%' order by the_date limit 100

looks like 2/15/21 as the start date

actuall, start date is 02/28, the 2/15 data was for dev

-- for fraser &  pederson
select sum(-amount)  -- 112379.22
from fin.fact_gl a
join sap.sale_accounts b on a.account_key = b.account_key
  and b.account = '249000'
join fin.dim_journal c on a.journal_key = c.journal_key
  and c.journal_code in ('SCA','SVI','SWA')  
join dds.dim_date d on a.date_key = d.date_key
where a.post_status = 'Y'
  and d.the_date between '02/28/2021' and '07/17/2021'

-- for durand  
select sum(-amount)  -- 52833.63
from fin.fact_gl a
join sap.sale_accounts b on a.account_key = b.account_key
  and b.account = '249000'
join fin.dim_journal c on a.journal_key = c.journal_key
  and c.journal_code in ('SCA','SVI','SWA')  
join dds.dim_date d on a.date_key = d.date_key
where a.post_status = 'Y'
  and d.the_date between '05/09/2021' and '07/17/2021'

-- back pay
select a.full_name, a.employee_number, a.from_Date, b.commission_percentage, 
  case 
		when a.employee_number in ('247000','2106410') then 112379.22
		else  52833.63
	end as sales,
  case
    when a.employee_number in ('247000','2106410') then round(b.commission_percentage * 112379.22, 2)
    else round(b.commission_percentage * 52833.63, 2)
  end as back_pay
from sap.personnel a  
left join sap.personnel_pay_plan_details b on a.employee_number = b.employee_number
where a.store = 'ry2'
  and a.thru_date > current_date

-- ros
select d.the_date, a.control as ro, sum(-a.amount) as amount
from fin.fact_gl a
join sap.sale_accounts b on a.account_key = b.account_key
  and b.account = '249000'
join fin.dim_journal c on a.journal_key = c.journal_key
  and c.journal_code in ('SCA','SVI','SWA')  
join dds.dim_date d on a.date_key = d.date_key
where a.post_status = 'Y'
  and d.the_date between '02/28/2021' and '07/17/2021'
group by d.the_date, a.control;

sent to jeri, randy & joel on 7/28/21