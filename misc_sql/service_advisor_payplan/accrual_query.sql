﻿-- select * from dds.dim_date where the_date = current_Date
12/01/20
looks like nick is still sending jeri everything in the accrual doc,
so, this is close
but not needed yet


do $$
declare

_current_pay_period integer := (select biweekly_pay_period_sequence from dds.dim_Date where the_date = current_date);

begin 
drop table if exists wtf;
create temp table wtf as
with 
clock_hours as (
  select a.employee_number, coalesce(sum(regular_hours),0) as total_regular_hours, 
    coalesce(sum(overtime_hours),0) as total_overtime_hours, 
    coalesce(sum(pto_hours),0) + coalesce(sum(hol_hours),0) as total_pto_hours,
    coalesce(sum(regular_hours),0) * hourly_rate as total_regular_hourly_pay, 
    coalesce(sum(overtime_hours),0) * overtime_rate as total_overtime_pay, 
    coalesce(sum(pto_hours),0) * pto_rate  as total_pto_pay,
    (coalesce(sum(regular_hours),0) * hourly_rate) + (coalesce(sum(overtime_hours),0) * overtime_rate) + 
      (coalesce(sum(pto_hours),0) * pto_rate) as total_hourly_pay
  from sap.personnel a
  left join sap.clock_hours b on a.employee_number = b.employee_number and pay_period_id = _current_pay_period
  inner join sap.personnel_pay_plan_details c on a.employee_number = c.employee_number 
    and c.from_date <= (select  distinct biweekly_pay_period_end_date from dds.dim_date where biweekly_pay_period_sequence = _current_pay_period )
     and c.thru_date >= (select  distinct biweekly_pay_period_end_date from dds.dim_date where biweekly_pay_period_sequence = _current_pay_period )
  where a.from_date <= (select  distinct biweekly_pay_period_end_date from dds.dim_date where biweekly_pay_period_sequence = _current_pay_period )
    and a.thru_date >=  (select  distinct biweekly_pay_period_end_date from dds.dim_date where biweekly_pay_period_sequence = _current_pay_period )
  group by a.employee_number, hourly_rate, overtime_rate, pto_rate
), 

total_sales as (
  select ro,sum(amount) filter (where category = 'labor') as labor_sales, 
    sum(amount) filter (where category = 'discount') as discount_sales, 
    sum(amount) filter (where category = 'road hazard') as road_hazard, 
    sum(amount) filter (where category = 'sublet') as sublet,
    sum(amount) filter (where category = 'parts') as parts, 
    sum(amount) as ro_total
  from sap.sales_details
  where pay_period_id = _current_pay_period
  group by ro
), 

adjustments as (
  select employee_number, sum(amount) as adjustment_amount,
    json_agg(json_build_object('comment', comment, 'amount', amount)) as adjustment_details
  from sap.adjustments
  where pay_period_id = _current_pay_period
  group by employee_number, pay_period_id
), 
working_days as (
  select employee_number, wd_in_biweekly_pay_period as total_working_days,
  count(wd_of_biweekly_pay_period) filter (where wd_of_biweekly_pay_period <> 0) as working_days,
  wd_in_biweekly_pay_period - count(wd_of_biweekly_pay_period) filter (where wd_of_biweekly_pay_period <> 0) as non_working_days, 
  case when wd_in_biweekly_pay_period = 0 then 0 else round((count(wd_of_biweekly_pay_period) filter (where wd_of_biweekly_pay_period <> 0))::numeric / wd_in_biweekly_pay_period::numeric ,2) end as percentage
  from sap.personnel a
  inner join dds.working_days b on department = 'service' 
  and the_date between from_date and case when end_date <> '12/31/9999' then end_date else thru_date end
  where biweekly_pay_period_sequence = _current_pay_period
 group by employee_number, wd_in_biweekly_pay_period, from_date),

biweekly_overtime_var as (
 -- biweekly ot var calc
select  a.employee_number, 
case when total_regular_hours + total_overtime_hours = 0 then 0 else round(((((total_sales *  percentage) * commission_percentage) + coalesce(b.adjustment_amount,0)) / (total_regular_hours + total_overtime_hours)) * total_overtime_hours * .5,2) end as biweekly_overtime_var
from clock_hours a
left join adjustments b on a.employee_number = b.employee_number
inner join sap.personnel_pay_plan_details c on a.employee_number = c.employee_number 
  and c.from_date <= (select  distinct biweekly_pay_period_end_date from dds.dim_date where biweekly_pay_period_sequence = _current_pay_period )
  and c.thru_date >= (select  distinct biweekly_pay_period_end_date from dds.dim_date where biweekly_pay_period_sequence = _current_pay_period )
inner join working_days e on a.employee_number = e.employee_number
cross join ( -- g
  select sum(ro_total) as total_sales,sum(labor_sales) as total_labor_sales, sum(discount_sales) as total_discount_sales,
    sum(road_hazard) as total_road_hazard, sum(sublet) as total_sublet, sum(parts) as total_parts 
  from total_sales
  ) d
)

    select a.employee_number as id,
--      user_key as user, 
     full_name, 
      biweekly_overtime_var,
      ((total_sales *  percentage) * commission_percentage) as commission_amount
    from sap.personnel a 
    inner join nrv.users b on a.employee_number = b.employee_number
    left join clock_hours c on a.employee_number = c.employee_number
    left join adjustments d on a.employee_number = d.employee_number
    left join biweekly_overtime_var i on a.employee_number = i.employee_number
    inner join sap.personnel_pay_plan_details f on a.employee_number = f.employee_number 
    and f.from_date <= (select  distinct biweekly_pay_period_end_date from dds.dim_date where biweekly_pay_period_sequence = _current_pay_period )
    and f.thru_date >= (select  distinct biweekly_pay_period_end_date from dds.dim_date where biweekly_pay_period_sequence = _current_pay_period )
    cross join ( -- g
      select sum(ro_total) as total_sales,sum(labor_sales) as total_labor_sales, sum(discount_sales) as total_discount_sales,
        sum(road_hazard) as total_road_hazard, sum(sublet) as total_sublet, sum(parts) as total_parts 
      from total_sales
      ) g
    inner join working_days h on a.employee_number = h.employee_number
  where a.from_date <= (select  distinct biweekly_pay_period_end_date from dds.dim_date where biweekly_pay_period_sequence = _current_pay_period )
    and a.thru_date >=  (select  distinct biweekly_pay_period_end_date from dds.dim_date where biweekly_pay_period_sequence = _current_pay_period );
--   ) z
--   group by total_sales;

end $$;

select * from wtf;