﻿updating to accommodate adding RY2 advisors

---------------------------------------------------------------------------
--< schema
---------------------------------------------------------------------------
create schema sap;
comment on schema sap is 'tables to support the main shop service advisor pay plan';

---------------------------------------------------------------------------
--/> schema
---------------------------------------------------------------------------



---------------------------------------------------------------------------
--< functions
---------------------------------------------------------------------------
-- comment on function sap.get_pay_periods() is 'returns current and 7 previous pay periods for the display of service advisor pay data';
CREATE OR REPLACE FUNCTION sap.get_pay_periods()
  RETURNS SETOF json AS
$BODY$
/*
  same function as used in schemas hs & bspp, just a different starting date
  select sap.get_pay_periods();
*/
declare
  _thru_pay_period_seq integer := (
    select distinct biweekly_pay_period_sequence
    from dds.dim_date
    where the_date = current_date);
  _from_pay_period_seq integer := _thru_pay_period_seq - 7; 
begin  
return query

select row_to_json(A)
from (
select array_to_json(array_agg(row_to_json(x))) as pay_periods
from (
  select distinct to_char(biweekly_pay_period_start_date, 'Mon') 
    || ' ' || to_char(biweekly_pay_period_start_date, 'DD')
    || ' - ' 
    || to_char(biweekly_pay_period_end_date, 'Mon') 
    || ' ' || to_char(biweekly_pay_period_end_date, 'DD')
    || ', ' || to_char(biweekly_pay_period_end_date, 'YYYY') as pay_period_display,
    biweekly_pay_period_sequence - _thru_pay_period_seq as pay_period_indicator
  from dds.dim_date 
  where biweekly_pay_period_sequence between _from_pay_period_seq and _thru_pay_period_seq
    and the_date >= '09/13/2020' -- '10/11/2020' 
  order by biweekly_pay_period_sequence - _thru_pay_period_seq desc) x) A;
  
end 
$BODY$
LANGUAGE plpgsql

--------------------------------------------------------------------------------------------------------------------------

-- comment on function sap.update_sales_details() is 'updates sale data for the service advisor pay plan, refreshes for the 
--   current pay period nightly via luigi';  
CREATE OR REPLACE FUNCTION sap.update_sales_details()
  RETURNS void AS
$BODY$ 
/*
select sap.update_sales_details()
*/
declare
  _from_date date := (
    select biweekly_pay_period_start_date
    from dds.dim_date
    where the_date = current_Date - 1);
  _thru_date date := (
    select biweekly_pay_period_end_date
    from dds.dim_date
    where the_date = current_Date - 1); 
begin

delete
from sap.sales_details
where the_date between _from_date and _thru_date;

insert into sap.sales_details
select c.the_date, c.biweekly_pay_period_sequence, a.control as ro, b.account, b.category, sum(-a.amount) as amount
from fin.fact_gl a
join sap.sale_accounts b on a.account_key = b.account_key
  and b.category <> 'parts'
join dds.dim_date c on a.date_key = c.date_key
where a.post_status = 'Y'
  and c.the_date between _from_date and _thru_date
group by c.the_date, c.biweekly_pay_period_sequence, a.control, b.account, b.category;

insert into sap.sales_details
select d.the_date, d.biweekly_pay_period_sequence, a.control as ro, b.account, b.category, sum(-a.amount) as amount
from fin.fact_gl a
join sap.sale_accounts b on a.account_key = b.account_key
  and b.category = 'parts'
join fin.dim_journal c on a.journal_key = c.journal_key
  and c.journal_code in ('SCA','SVI','SWA')  
join dds.dim_date d on a.date_key = d.date_key
where a.post_status = 'Y'
  and d.the_date between _from_date and _thru_date
group by d.the_date, d.biweekly_pay_period_sequence, a.control, b.account, b.category;

end
$BODY$
LANGUAGE plpgsql;

--------------------------------------------------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION sap.update_clock_hours()
  RETURNS void AS
$BODY$ 
/*
select sap.update_clock_hours()
*/
declare
  _from_date date := (
    select biweekly_pay_period_start_date
    from dds.dim_date
    where the_date = current_Date - 1);
  _thru_date date := (
    select biweekly_pay_period_end_date
    from dds.dim_date
    where the_date = current_Date - 1); 
begin

delete
from sap.clock_hours
where the_date between _from_date and _thru_date;

insert into sap.clock_hours
select a.employee_number, b.the_date, c.biweekly_pay_period_sequence,
  reg_hours, ot_hours,vac_hours + pto_hours + hol_hours
from sap.personnel a
join arkona.xfm_pypclockin b on a.employee_number = b.employee_number
  and b.the_date between _from_date and _thru_date
join dds.dim_date c on b.the_date = c.the_date;

end
$BODY$
LANGUAGE plpgsql;
comment on function sap.update_clock_hours() is 'updates time clock data for the service advisor pay plan, refreshes for the 
  current pay period nightly via luigi';  



---------------------------------------------------------------------------
--/> functions
---------------------------------------------------------------------------




---------------------------------------------------------------------------
--< tables
---------------------------------------------------------------------------

drop table if exists sap.sale_accounts cascade;
create table sap.sale_accounts (
  account citext primary key,
  account_key integer not null references fin.dim_account(account_key),
  store citext not null,
  description citext not null,
  category citext not null);
create index on sap.sale_accounts(category);
comment on table sap.sale_accounts is 'the list of all sale accounts on which writers are paid, from jeri''s pay plan document,
  these accounts control what data populates sad.sales_details and subsequently total sales';
insert into sap.sale_accounts (select account, account_key, 'RY1', description, 'labor' from fin.dim_account where account = '146000' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY1', description, 'labor' from fin.dim_account where account = '146004' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY1', description, 'labor' from fin.dim_account where account = '146100' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY1', description, 'labor' from fin.dim_account where account = '146001' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY1', description, 'labor' from fin.dim_account where account = '146201' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY1', description, 'labor' from fin.dim_account where account = '146204' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY1', description, 'labor' from fin.dim_account where account = '146007' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY1', description, 'labor' from fin.dim_account where account = '146301' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY1', description, 'labor' from fin.dim_account where account = '146304' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY1', description, 'labor' from fin.dim_account where account = '146305' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY1', description, 'labor' from fin.dim_account where account = '146305' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY1', description, 'labor' from fin.dim_account where account = '146401' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY1', description, 'road hazard' from fin.dim_account where account = '146901' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY1', description, 'sublet' from fin.dim_account where account = '146604' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY1', description, 'parts' from fin.dim_account where account = '146700' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY1', description, 'parts' from fin.dim_account where account = '148104' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY1', description, 'parts' from fin.dim_account where account = '148904' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY1', description, 'parts' from fin.dim_account where account = '146707' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY1', description, 'parts' from fin.dim_account where account = '148004' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY1', description, 'parts' from fin.dim_account where account = '149004' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY1', description, 'discount' from fin.dim_account where account = '146000D' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY1', description, 'discount' from fin.dim_account where account = '146700D' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY1', description, 'discount' from fin.dim_account where account = '149004D' and current_row);

-- ry2 mod
select account,description,department from fin.dim_Account where account in (
  '245700','245800','246000','246100','245900',
  '245901','246200','246301','246300','246302',
  '246305','246310','246320','246600','246700',
  '246705','246709','246800','246809','248000',
  '248010','248100','248101','248105','248110',
  '248120','246000D','246700D')
and current_row order by department desc, account asc
  
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'labor' from fin.dim_account where account = '245700' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'labor' from fin.dim_account where account = '245800' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'labor' from fin.dim_account where account = '245900' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'labor' from fin.dim_account where account = '245901' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'labor' from fin.dim_account where account = '246000' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'labor' from fin.dim_account where account = '246100' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'labor' from fin.dim_account where account = '246200' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'labor' from fin.dim_account where account = '246300' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'labor' from fin.dim_account where account = '246301' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'labor' from fin.dim_account where account = '246302' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'labor' from fin.dim_account where account = '246305' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'labor' from fin.dim_account where account = '246310' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'labor' from fin.dim_account where account = '246320' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'sublet' from fin.dim_account where account = '246600' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'parts' from fin.dim_account where account = '246700' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'parts' from fin.dim_account where account = '246705' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'parts' from fin.dim_account where account = '246709' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'parts' from fin.dim_account where account = '246800' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'parts' from fin.dim_account where account = '246809' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'parts' from fin.dim_account where account = '248000' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'parts' from fin.dim_account where account = '248010' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'parts' from fin.dim_account where account = '248100' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'parts' from fin.dim_account where account = '248101' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'parts' from fin.dim_account where account = '248105' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'parts' from fin.dim_account where account = '248110' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'parts' from fin.dim_account where account = '248120' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'discount' from fin.dim_account where account = '246000D' and current_row);
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'discount' from fin.dim_account where account = '246700D' and current_row);

-- 07/27/21
-- add account 249000 tires
insert into sap.sale_accounts (select account, account_key, 'RY2', description, 'parts' from fin.dim_account where account = '249000' and current_row);

select a.*, count(*) over (partition by category) from sap.sale_accounts a order by category, account;

--------------------------------------------------------------------------------------------------------------------------

drop table if exists sap.personnel cascade;
create table sap.personnel (
  employee_number citext primary key,
  full_name citext not null,
  last_name citext not null,
  first_name citext not null,
  hire_date date not null);
comment on table sap.personnel is 'main shop writers covered by this pay plan';

insert into sap.personnel
select a.employee_number, b.employee_name, b.employee_last_name, b.employee_first_name, dds.db2_integer_to_date(b.hire_date) as hire_date
from sad.writers a
join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number;

select * from arkona.ext_pymast where pymast_employee_number = '1140870'

-- ry2 mod
alter table sap.personnel
add column store citext;
update sap.personnel
set store = 'RY1';
alter table sap.personnel
alter column store set not null;
--------------------------------------------------------------------------------------------------------------------------
drop table if exists sap.pay_plans cascade;
create table sap.pay_plans (
  employee_number citext not null references sap.personnel(employee_number),
  hourly_rate numeric not null,
  pto_rate numeric not null,
  overtime_rate numeric not null,
  commission_percentage numeric not null,
  from_date date not null,
  thru_date date not null default '12/31/9999',
  primary key (employee_number,thru_date));
comment on table sap.pay_plans is 'details of pay plan for each writer';  

insert into sap.pay_plans(employee_number,hourly_Rate,pto_rate,overtime_rate,commission_percentage,from_date)
select a.employee_number, base_hrly_rate as hourly_rate, base_hrly_rate as pto_rate, 1.5 * base_hrly_Rate as overtime_rate,
  .002 as commission_percentage, '09/13/2020' as from_date
from sad.writers a
join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number;

update sap.pay_plans set commission_percentage = .003 where employee_number = '1140870';  
update sap.pay_plans set commission_percentage = .0025 where employee_number = '191060';  
update sap.pay_plans set commission_percentage = .0025 where employee_number = '141060';
update sap.pay_plans set commission_percentage = 0 where employee_number = '123525';    

select * from sap.pay_plans
--------------------------------------------------------------------------------------------------------------------------

drop table if exists sap.clock_hours cascade;
create table sap.clock_hours (
  employee_number citext not null references sap.personnel(employee_number),
  the_date date not null references dds.dim_date(the_date),
  pay_period_id integer not null,
  regular_hours numeric not null,
  overtime_hours numeric not null,
  pto_hours numeric not null,
  primary key (employee_number,the_date));
comment on table sap.clock_hours is 'writer clock hours refreshed nightly for the current pay period';


--------------------------------------------------------------------------------------------------------------------------
drop table if exists sap.sales_details cascade;
create table sap.sales_details (
  the_date date not null,
  pay_period_id integer not null,
  ro citext not null,
  account citext not null,
  category citext not null,
  amount numeric(8,2) not null,
  primary key (the_date,ro,account));
create index on sap.sales_details(pay_period_id);
comment on table sap.sales_details is 'nightly updated with sales data from accounting for the current pay period, this is the data that
serves as the basis for the service advisor pay plan. it is limited to accounts in sap.sale_accounts';  

-- ry2 mod
alter table sap.sales_details
add column store citext;
update sap.sales_details
set store = 'RY1';
alter table sap.sales_details
alter column store set not null;

select * from sap.sales_details a
where not exists (select 1 from dds.fact_Repair_order where ro = a.ro)


-- non discount negative transactions
-- trivial amounts, labor adjustments
select * from sap.sales_details where amount < 0 and category <> 'discount'

-- non ro transactions
-- conclusion, trivial amounts not a concern
1. 
select * from fin.fact_gl where control = '1679100'  -- these are reversals of a control typo

select * from fin.fact_gl where control = '16379100'  

2. 
select * from fin.fact_gl where control = '1110550' 

select category, count(*) from sap.sales_Details group by category

select * from sap.sales_details where pay_period_id = 308

select ro, 
  sum(amount) filter (where category = 'labor') as labor, 
  sum(amount) filter (where category = 'parts') as parts, 
  sum(amount) filter (where category = 'sublet') as sublet,
  sum(amount) filter (where category = 'road hazard') as "road hazard", 
  sum(amount) filter (where category = 'discount') as discount, string_agg(account, ',') filter (where category = 'discount')
from sap.sales_details
where pay_period_id = 307
group by ro

--------------------------------------------------------------------------------------------------------------------------

drop table if exists sap.adjustments cascade;
create table sap.adjustments (
  employee_number citext not null references sap.personnel(employee_number),
  pay_period_id integer not null,
  comment citext,
  amount numeric not null,
  primary key(employee_number,pay_period_id,comment));

--------------------------------------------------------------------------------------------------------------------------

drop table if exists sap.csi_bonus cascade;
create table sap.csi_bonus (
  employee_number citext not null references sap.personnel(employee_number),
  pay_period_id integer not null,
  comment citext,
  amount numeric not null,
  primary key(employee_number,pay_period_id));
  
--------------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists sap.payroll_submittals;
CREATE TABLE sap.payroll_submittals
(
  employee_number citext NOT NULL,
  full_name citext NOT NULL,
  total_pay numeric NOT NULL,
  commission_pay numeric NOT NULL,
  adjustments numeric NOT NULL,
  csi_bonus numeric NOT NULL,
  overtime_pay numeric NOT NULL,
  hourly_pay numeric NOT NULL,
  pto_pay numeric NOT NULL,
  holiday_pay numeric NOT NULL,
  pay_period_id integer NOT NULL,
  date_submitted date NOT NULL,
  ot_variance numeric NOT NULL,
  store citext NOT NULL,
  CONSTRAINT payroll_submittals_pk PRIMARY KEY (employee_number, pay_period_id));
COMMENT ON TABLE sap.payroll_submittals
  IS 'Every payroll submital from GM & HN Service management ';
alter table sap.payroll_submittals add foreign key (employee_number) references sap.personnel(employee_number)


/*
this is all my looking into hourly employees getting shorted due to not clocking out
once again i am letting it go, small occurence, timeclock/payroll system will be changing soon
-- what happens to "overwritten?" yicode = I
select c.employee_name, a.pymast_employee_number, a.yiclkind, a.yiclkint, a.yicode, b.yiclkind, b.yiclkint, b.yicode
from (
  select * 
  from arkona.ext_pypclockin
  where yicode = 'I' ) a
join (
  select *
  from arkona.ext_pypclockin
  where yicode <> 'I' ) b on a.pymast_employee_number = b.pymast_employee_number and a.yiclkind = b.yiclkind
join arkona.ext_pymast c on a.pymast_employee_number = c.pymast_employee_number
order by a.yiclkind desc
limit 100

select * from arkona.ext_pypclockin where pymast_employee_number = '184614' and yiclkind = '09/22/2020'

select * from arkona.xfm_pypclockin where employee_number = '184614' and the_date = '09/22/2020'



-- what happens to "abaondoned?" yicode = I
select c.employee_name, a.pymast_employee_number, a.yiclkind, a.yiclkint, a.yicode
from (
  select * 
  from arkona.ext_pypclockin
  where yicode = 'I' ) a
join arkona.ext_pymast c on a.pymast_employee_number = c.pymast_employee_number
where not exists (
  select 1
  from arkona.ext_pypclockin
  where yicode <> 'I' 
    and pymast_employee_number = a.pymast_employee_number
    and yiclkind = a.yiclkind) 
order by a.yiclkind desc
limit 100

select * from arkona.ext_pypclockin where pymast_employee_number = '195975' order by  yiclkind desc limit 20

select * from arkona.ext_pypclockin where pymast_employee_number = '159875' and yiclkind = '08/18/2020'

select * from arkona.ext_pypclockin where pymast_employee_number = '159875' order by yiclkind desc limit 50
paycheck shows 42.54 hours
select distinct biweekly_pay_period_start_date,biweekly_pay_period_end_date
from dds.dim_Date
where '08/18/2020' between biweekly_pay_period_start_date and biweekly_pay_period_end_date

select sum(clock_hours)  -- 42.54
from arkona.xfm_pypclockin
where employee_number = '159875'
  and the_date between '08/16/2020' and '08/29/2020'

this is an example of an employee that got shorted 
just talked to kim, she explained that when an employee does not clock out, the next time they got
to clockin the system displays an error, the employee then has to go to their manager,
if that error is not resolved on that day, it does not show up again

so i need to detect outstanding unresolved clockins that have a subsequent clockin
at this point in the game i have no stomach for resolving all wrongs in history, 
so i am going to limit the search to current pay period

select b.active_code, b.employee_name, b.payroll_class, a.pymast_employee_number, a.yiclkind, a.yiclkint, c.*
from arkona.ext_pypclockin a
join arkona.ext_pymast b on a.pymast_employee_number = b.pymast_employee_number
  and b.payroll_class in ('C','H')
left join arkona.xfm_pypclockin c on a.pymast_employee_number = c.employee_number
  and a.yiclkind = c.the_date  
where yicode = 'I'
  and yiclkind between 
    (
      select distinct biweekly_pay_period_start_date
      from dds.dim_Date
      where current_Date between biweekly_pay_period_start_date and biweekly_pay_period_end_date)
    and (
      select distinct biweekly_pay_period_end_date
      from dds.dim_Date
      where current_Date between biweekly_pay_period_start_date and biweekly_pay_period_end_date)    
  and exists ( -- there is a clockin after the missing clockout
    select 1
    from arkona.ext_pypclockin
    where pymast_employee_number = a.pymast_employee_number
      and yiclkind > a.yiclkind)   
  and not exists ( -- sometimes there is a clockin with no clockout, then a clockin and out for the same day, within a few minutes, not say, after lunch)
  so i guess what i want is the time diff between the suspect clockin and the next clockin, that should be greater than an hour say
  but that does not pick clocked in in the morning and clocked out in the afternoon but also clocked in after lunch
select * from arkona.ext_pymast limit 30      
*/


---------------------------------------------------------------------------
--/> tables
---------------------------------------------------------------------------
