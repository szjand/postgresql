﻿drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select year_month, store, page, line, line_label, control, sum(unit_count) as unit_count
from (
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
--     and b.year_month = 201708
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select b.year_month, f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month > 201701
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
--         or
--         (b.page = 17 and b.line between 1 and 20))-- f/i
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by year_month, store, page, line, line_label, control
order by store, page, line;


select a.*, b.deal_status, deal_type, sale_type, sale_group, capped_date, delivery_date
from step_1 a
left join sls.ext_bopmast_partial b on a.control = b.stock_number
order by store, sale_type


select  *
from fin.dim_Fs
limit 100


select *
from sls.deals
where year_month = 201708
  and store_code = 'ry1'


select *
from sls.deals_by_month
where year_month = 201708
  and store_code = 'ry1'


  

select *
from ads.ext_dim_car_deal_info

select a.store_code, a.bopmast_id, a.stock_number, a.unit_count, c.vehicletype, c.dealtype, c.saletype, 
  b.gap, b.service_contract, b.total_care, 
  sum(case when h.field_Descrip = 'WindshieldProtection' then amo_fee_amount end) as glass_sale,
--   sum(case when h.field_Descrip = 'WindshieldProtection' then cost end) as glass_cost,
  sum(case when h.field_Descrip = 'RoadVantage 4 in 1' then amo_fee_amount end) as "4_1_sale",
--   sum(case when h.field_Descrip = 'RoadVantage 4 in 1' then cost end) as "4_1_cost",
  sum(case when h.field_Descrip = 'Resistall' then amo_fee_amount end) as protection_sale,
--   sum(case when h.field_Descrip = 'Resistall' then cost end) as protection_Cost
  b.fi_manager,
  sum(a.unit_count) over (partition by fi_manager)
--  select *
from ( -- a: one row per deal (bopmast_id/store) per year_month
  select store_code, bopmast_id, stock_number, year_month, sum(unit_count)::integer as unit_count
  from sls.deals
  where year_month = 201708 -- (select year_month from sls.months where open_closed = 'open')
  group by store_code, bopmast_id, stock_number, year_month
  having sum(unit_count) <> 0) a
-- for the given month, the sls.deals data for the bopmast_id row with the max seq
left join sls.deals b on a.store_code = b.store_code
  and a.bopmast_id = b.bopmast_id
  and a.year_month = b.year_month
    and b.seq = (
      select max(seq)
      from sls.deals
      where store_code = b.store_code and bopmast_id = b.bopmast_id and year_month = b.year_month)
left join (
  select distinct saletypecode,saletype, vehicletypecode, vehicletype, dealtypecode, dealtype
  from ads.ext_dim_car_deal_info) c on b.sale_type_code = c.saletypecode
    and b.vehicle_type_code = c.vehicletypecode and b.deal_type_code = c.dealtypecode and b.vehicle_type_code = c.vehicletypecode
-- left join ads.ext_dim_vehicle d on b.vin = d.vin
-- left join ads.ext_dim_customer e on b.buyer_bopname_id = e.bnkey
-- left join ads.ext_dim_customer f on b.cobuyer_bopname_id = f.bnkey
--   and f.bnkey <> 0
-- left join sls.sale_groups g on b.store_code = g.store_code and b.sale_group_code = g.code
left join arkona.ext_bopopts h on a.bopmast_id = h.contract_key
  and h.field_descrip in ('WindshieldProtection','RoadVantage 4 in 1','ResistALL')
where saletype <> 'wholesale'  
  and a.store_code = 'ry1'
group by a.store_code, a.bopmast_id, a.stock_number, a.unit_count, c.dealtype, c.saletype, c.vehicletype,
  b.gap, b.service_contract, b.total_care, b.fi_manager
order by b.fi_manager  

select * from arkona.ext_bopopts where contract_key = 43409

select store_code, bopmast_id
from (
select store_code, bopmast_id, deal_type_code
from sls.deals
group by store_code, bopmast_id, deal_type_code) x
group by store_code, bopmast_id
having count(*) > 1


select *
from sls.deals
where bopmast_id in (40982,42943)
order by bopmast_id, seq

select *
from sls.deals_by_month
where year_month = 201708
  and fi_last_name = 'aubol'
  and unit_count = '1'


select 750 + 499 + 699 - 417 - 245 - 159


select b.the_date, c.account, c.description, d.journal_code, a.amount, e.description, 
  account_type_code, account_type, department
-- select department, account_type, sum(amount)  
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and control = '30525'
  and journal_code = 'VSN'
  and account_type in ('sale', 'cogs')
  and department = 'finance' 
-- group by department, account_type  


  
select 
from sls.deals_by_month a
left join arkona.ext_bopopts b on a.bopmast_id = b.contract_key
where a.store_code = 'ry1'
  and a.year_month = 201708
  and vehicle_type = 'new'

select *
from sls.ext_bopmast_partial a
left join arkona.ext_bopopts b on a.bopmast_id = b.contract_key
where stock_number = '31313'


----------------------------------------------------------------------------------------
-- 9/16/17
----------------------------------------------------------------------------------------
-- from /luigi/projects/luigi/sales_consultant_payroll.py class DealsByMonth
-- 1 row per store/bopmast_id/year_month
-- trim it down a bit to just what i need, add the fi product crap,
-- and dim_car_deal_info.\

is there a way to correlate a specific product sale to accounting?

this is actually probably better done with the partitioning by rather group by fi/new_used/

need to distinguish between lease and finance

drop table if exists fi_test;
create temp table fi_test as
select a.store_code, a.bopmast_id, a.stock_number, a.unit_count, c.vehicletype, c.dealtype, c.saletype, 
  b.gap, b.service_contract, b.total_care, 
  sum(case when h.field_Descrip = 'WindshieldProtection' then amo_fee_amount end) as glass_sale,
--   sum(case when h.field_Descrip = 'WindshieldProtection' then cost end) as glass_cost,
  sum(case when h.field_Descrip = 'RoadVantage 4 in 1' then amo_fee_amount end) as "4_1_sale",
--   sum(case when h.field_Descrip = 'RoadVantage 4 in 1' then cost end) as "4_1_cost",
  sum(case when h.field_Descrip = 'Resistall' then amo_fee_amount end) as protection_sale,
--   sum(case when h.field_Descrip = 'Resistall' then cost end) as protection_Cost
  b.fi_manager,
  sum(a.unit_count) over (partition by fi_manager) as mgr_unit_count,
  sum(case when service_contract > 0 then 1 else 0 end) over (partition by fi_manager) as mgr_SVC,
  sum(case when gap > 0 then 1 else 0 end) over (partition by fi_manager) as mgr_gap
--  select *
from ( -- a: one row per deal (bopmast_id/store) per year_month
  select store_code, bopmast_id, stock_number, year_month, sum(unit_count)::integer as unit_count
  from sls.deals
  where year_month = 201708 -- (select year_month from sls.months where open_closed = 'open')
  group by store_code, bopmast_id, stock_number, year_month
  having sum(unit_count) <> 0) a
-- for the given month, the sls.deals data for the bopmast_id row with the max seq
left join sls.deals b on a.store_code = b.store_code
  and a.bopmast_id = b.bopmast_id
  and a.year_month = b.year_month
    and b.seq = (
      select max(seq)
      from sls.deals
      where store_code = b.store_code and bopmast_id = b.bopmast_id and year_month = b.year_month)
left join (
  select distinct saletypecode,saletype, vehicletypecode, vehicletype, dealtypecode, dealtype
  from ads.ext_dim_car_deal_info) c on b.sale_type_code = c.saletypecode
    and b.vehicle_type_code = c.vehicletypecode and b.deal_type_code = c.dealtypecode and b.vehicle_type_code = c.vehicletypecode
left join arkona.ext_bopopts h on a.bopmast_id = h.contract_key
  and h.field_descrip in ('WindshieldProtection','RoadVantage 4 in 1','ResistALL')
where saletype <> 'wholesale'  
  and a.store_code = 'ry1'
group by a.store_code, a.bopmast_id, a.stock_number, a.unit_count, c.dealtype, c.saletype, c.vehicletype,
  b.gap, b.service_contract, b.total_care, b.fi_manager
order by b.fi_manager  


select vehicletype, dealtype,saletype, count(*)
from fi_test
group by vehicletype, dealtype,saletype
order by vehicletype, dealtype,saletype



select *
from fi_test
where gap > 0
  and service_contract > 0
  and total_care > 0
  and glass_Sale > 0
  and "4_1_sale" > 0
  and protection_sale > 0



select b.the_date, c.account, cc.gm_Account, c.description, d.journal_code, a.amount, e.description, 
  account_type_code, account_type, department
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
left join fin.dim_fs_account cc on c.account = cc.gl_account
  and cc.gm_account not like '<%'
  and cc.gm_account not like 'P%'
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and control = '31313'
  and journal_code = 'VSN'
  and account_type in ('sale', 'cogs')
order by department, account_type, account  



select *
from fin.dim_fs_account a
left join fin.dim_account b on a.gl_account = b.account
where gm_account in ('806','807','444')
  and gm_account not like '<%'
  and gm_account not like 'P%'
  and left(gl_account, 1) = '1'


    select distinct g_l_acct_number
    from arkona.ext_eisglobal_sypffxmst a
    inner join (
      select factory_financial_year, g_l_acct_number, factory_account, fact_account_
      from arkona.ext_ffpxrefdta) b on a.fxmcyy = b.factory_financial_year and a.fxmact = b.factory_account
    where a.fxmcyy = 2017
      and fxmpge = 17
      and fxmlne between 1 and 14



select * from sls.deals limit 10

-- 9/17/17  try to focus on jeri's budgeting project
figure out why step_2 is different than sls.deals

-- this says 73 deals
drop table if exists sls;
create temp table sls as 
select a.*
from ( -- a: one row per deal (bopmast_id/store) per year_month
  select store_code, bopmast_id, stock_number, year_month, sum(unit_count)::integer as unit_count
  from sls.deals
  where year_month = 201708 and store_code = 'ry2' and vehicle_type_code = 'N'
  group by store_code, bopmast_id, stock_number, year_month
  having sum(unit_count) <> 0) a
-- for the given month, the sls.deals data for the bopmast_id row with the max seq
inner join sls.deals b on a.store_code = b.store_code
  and a.bopmast_id = b.bopmast_id
  and a.year_month = b.year_month
    and b.seq = (
      select max(seq)
      from sls.deals
      where store_code = b.store_code and bopmast_id = b.bopmast_id and year_month = b.year_month)
where a.store_code = 'RY2'
  and b.vehicle_type_code = 'N'    

-- this gives me 71 rows which agrees with the statement

select year_month, sum(unit_count)
from step_1
where store = 'ry2'
  and page < 16
group by year_month

select control, unit_count
from step_1 
where store = 'ry2'
  and page < 16
  and year_month = 201702
  and unit_count <> 0
order by control

select * from step_1 where control = 'h9825'

drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select year_month, store, page, line, line_label, control, sum(unit_count) as unit_count
from (
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201708 -- between 201702 and 201709
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select b.year_month, f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month > 201701
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
--         or
--         (b.page = 17 and b.line between 1 and 20))-- f/i
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account and b.year_month = d.year_month
  where a.post_status = 'Y') h
group by year_month, store, page, line, line_label, control
order by store, page, line;


-- fact_gl : control/account/store
select b.the_date, c.account, c.description, d.journal_code, a.amount, e.description, account_type
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and control = 'h9825'
order by account_type, the_date  


-- for comparing
select year_month, sum(unit_count)
from sls.deals_by_month
where store_code = 'ry2'
  and sale_group like 'new%'
group by year_month


select stock_number, unit_count
from sls.deals_by_month
where store_code = 'ry2'
  and sale_group like 'new%'
  and year_month = 201708 
  and unit_count <> 0
order by stock_number  
