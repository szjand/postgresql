﻿

create extension cube schema public;
create extension earthdistance schema public;

-- http://tapoueh.org/blog/2013/08/how-far-is-the-nearest-pub/
 select round((point(-0.1216925,51.5185189) <@> point(-0.12,51.516))::numeric, 3)
  
-- distance grand forks to crookston
 select round((point(-96.412,47.40508) <@> point(-97.4315,47.9041))::numeric, 2)

drop table if exists jon.lat_long_zip;
create table jon.lat_long_zip (
  zip integer,
  lat numeric (9,6), 
  lon numeric (9,6),
  city citext,
  state citext,
  county citext);


delete from jon.lat_long_zip where lat is null or lon is null 

select * from jon.lat_long_zip limit 10

select * from jon.lat_long_zip where city in ('minneapolis')

select * from jon.lat_long_zip where city = 'grand forks'


select 
  (
  (select point(lon,lat) from jon.lat_long_zip where zip = '58201') <@>
  (select point(lon,lat) from jon.lat_long_zip where city = 'crookston' and state = 'MN')
  ) as distance_miles



select *
from jon.lat_long_zip
where state in ('ND','MN')


select 
  (
  (select point(lon,lat) from jon.lat_long_zip where zip = '58201') <@>
  (select point(lon,lat) from jon.lat_long_zip where city = 'crookston' and state = 'MN')
  ) as distance_miles



-- distance from dealership to crookston
select (
  (select point(-97.053180,47.898521)) <@>
  (select point(lon,lat) from jon.lat_long_zip where city = 'crookston' and state = 'MN')
  )::integer as distance_miles

