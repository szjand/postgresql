﻿  select a.control, d.journal_code, max(b.the_date) as the_date, sum(a.amount) as amount
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_date between '06/01/2018' and current_date--'06/22/2018'
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account in  ('123700','16102','16101','190500')
  inner join fin.dim_journal d on a.journal_key = d.journal_key
    and d.journal_code in ('PVI','CDH','GJE')
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where a.post_status = 'Y'
  group by a.control, d.journal_code
  having sum(a.amount) > 10000


select a.control, d.journal_code, c.account, max(b.the_date) as the_date, sum(a.amount) as amount, 
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_date between '05/01/2018' and current_date
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account in  ('123700','16102','16101','190500')
inner join fin.dim_journal d on a.journal_key = d.journal_key
--   and d.journal_code in ('PVI','CDH','GJE')
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
inner join nc.vehicle_acquisitions f on a.control = f.stock_number
where a.post_status = 'Y'
group by a.control, d.journal_code, c.account
order by account


select b.the_date, c.account, d.journal_code, a.*, e.*
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join nc.vehicle_acquisitions e on a.control = e.stock_number
where a.post_status = 'Y'
  and c.account in ('190500','126101','126102')
  and b.the_year = 2018
order by e.source, a.control


drop table if exists wtf;
create temp table wtf as
select a.trans, a.seq, b.the_date, c.account, d.journal_code, a.control, a.doc, a.amount, dd.description, ee.make, ee.model--, e.*
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description dd on a.gl_description_key = dd.gl_description_key
left join arkona.xfm_inpmast ee on a.control = ee.inpmast_stock_number
  and ee.current_row = true
-- inner join nc.vehicle_acquisitions e on a.control = e.stock_number
where a.post_status = 'Y'
  and d.journal_code = 'PVI'
  and c.account = '126102'
  and b.the_year = 2018
order by doc

select *
from wtf

select *
from nc.vehicle_acquisitions
inner join wtf on stock_number = doc
order by stock_number


if their is no PVI/126102 transaction it is a dealer trade


drop table if exists avail_1;
create temp table avail_1 as
select f.control as stock_number, f.journal_code, f.the_date, f.amount, 
  case f.journal_code
    when 'PVI' then 'factory'
    when 'CDH' then 'dealer trade'
    when 'GJE' then 'ctp'
  end as acquisition_type,
  g.inpmast_vin as vin, g.year as model_year, g.model_code, g.color as color, g.color_code
from ( -- accounting journal
  select a.control, d.journal_code, max(b.the_date) as the_date, sum(a.amount) as amount
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_date between '06/01/2018' and current_date--'06/22/2018'
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account = '123700'
  inner join fin.dim_journal d on a.journal_key = d.journal_key
    and d.journal_code in ('PVI','CDH','GJE')
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where a.post_status = 'Y'
  group by a.control, d.journal_code
  having sum(a.amount) > 10000) f
inner join arkona.xfm_inpmast g on f.control = g.inpmast_stock_number
  and g.model = 'silverado 1500'
  and g.current_row = true;


select a.stock_number, a.vin, a.ground, a.delivery_Date,  a.acq_type, b.*
from jon.test_base a
left join (
  -- select a.trans, a.seq, b.the_date, c.account, d.journal_code, a.control, a.doc, a.amount, dd.description, ee.make, ee.model--, e.*
  select b.the_date, c.account, d.journal_code, a.doc, sum(a.amount)
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join fin.dim_journal d on a.journal_key = d.journal_key
  inner join fin.dim_gl_description dd on a.gl_description_key = dd.gl_description_key
  left join arkona.xfm_inpmast ee on a.control = ee.inpmast_stock_number
    and ee.current_row = true
  -- inner join nc.vehicle_acquisitions e on a.control = e.stock_number
  where a.post_status = 'Y'
    and d.journal_code = 'PVI'
    and c.account = '126102'
    and b.the_year > 2016
  group by b.the_date, c.account, d.journal_code, a.doc) b on a.stock_number = b.doc 
-- where b.the_date is not null  
order by stock_number


select *
from gmgl.vehicle_orders
where vin in ('1GCVKREC0JZ332489')


select a.stock_number, a.vin, a.ground, a.delivery_Date,  a.acq_type, b.*
from jon.test_base a
left join gmgl.vehicle_orders b on a.vin = b.vin
order by ground

G34118 accounting shows as a dealer trade
vehicle_orders shows an order


-- 6/23
ok, dealer trades show up as orders, afton has not been populating the ship-to-BAC field
so based on no 126102 transactions these should be dealer trades (that i currently do not show as dealer trades in jon.test_base:
G33999  3GCUKREC8JG106180  5/15  order no longer in gmgl
G34229  3GCUKSEC0JG383431  6/13  VZPKW0 dealer trade
G34263  3GCUKSECXJG345155  6/13  VXBK4M dealer trade
G34291  1GCVKREC0JZ332489  6/15  VXMHPQ dealer trade


select *
from gmgl.vehicle_orders
where vin in ('3GCUKREC8JG106180','3GCUKSEC0JG383431','3GCUKSECXJG345155','1GCVKREC0JZ332489')









