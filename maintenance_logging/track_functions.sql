﻿look at this: https://www.cybertec-postgresql.com/en/avoiding-unnecessary-stored-procedure-calls-in-postgresql/

08/06/21

enabled track_functions in postgresql.conf
added to luigi and made a cron job to update a snapshot at 12 mintes past the hour 24/7

show track_functions

select sap.get_ros_by_pay_period(0)
SELECT * FROM pg_stat_user_functions where schemaname <> 'public' order by schemaname;

SELECT * FROM pg_stat_user_functions where schemaname <> 'public' order by calls desc;

drop table if exists jon.function_statistics cascade;
create table jon.function_statistics (
	funcid bigint,
  schema_name citext,
  function_name citext,
  calls integer,
  total_time numeric,
  self_time numeric,
  ts timestamp);

alter table jon.function_statistics
rename to function_statistics_snapshots;
alter table jon.function_statistics_snapshots
alter column calls type bigint;

insert into jon.function_statistics_snapshots
select funcid,schemaname,funcname,calls,total_time,self_time, now()
from pg_stat_user_functions


select * 
from jon.function_statistics_snapshots
where schema_name <> 'public'
order by function_name, ts


select * 
from jon.function_statistics_snapshots
where schema_name <> 'public'
order by ts desc 


select * 
from jon.function_statistics_snapshots
where schema_name <> 'public'
order by calls desc 

select * 
from luigi.luigi_log
order by from_ts desc
limit 100

select * 
from luigi.luigi_log
where pipeline = 'postgresql_statistics'

select schema_name, function_name, calls, min(ts) as min_ts, max(ts) as max_ts
from jon.function_statistics_snapshots
where schema_name <> 'public' 
group by schema_name, function_name, calls
order by schema_name, function_name, min(ts)


https://dba.stackexchange.com/questions/154525/find-which-functions-are-used

-- all functions
select n.nspname as function_schema,
       p.proname as function_name,
       l.lanname as function_language,
       case when l.lanname = 'internal' then p.prosrc
            else pg_get_functiondef(p.oid)
            end as definition,
       pg_get_function_arguments(p.oid) as function_arguments,
       t.typname as return_type
from pg_proc p
left join pg_namespace n on p.pronamespace = n.oid
left join pg_language l on p.prolang = l.oid
left join pg_type t on t.oid = p.prorettype 
where n.nspname not in ('pg_catalog', 'information_schema')
order by function_schema,
         function_name;


select * 
from ( -- all functions
	select n.nspname as function_schema,
				 p.proname as function_name,
				 l.lanname as function_language
	from pg_proc p
	left join pg_namespace n on p.pronamespace = n.oid
	left join pg_language l on p.prolang = l.oid
	left join pg_type t on t.oid = p.prorettype 
	where n.nspname not in ('pg_catalog', 'information_schema', 'public')) a
full outer join ( -- function statistics
	select schema_name, function_name, max(calls) as calls
	from jon.function_statistics_snapshots
	where schema_name <> 'public'
	group by schema_name, function_name) b on a.function_schema = b.schema_name 
		and a.function_name = b.function_name	
order by a.function_schema, a.function_name  



	select n.nspname as function_schema,
       p.proname as function_name,
       l.lanname as function_language,
       case when l.lanname = 'internal' then p.prosrc
            else pg_get_functiondef(p.oid)
            end as definition,
       pg_get_function_arguments(p.oid) as function_arguments,
       t.typname as return_type
	from pg_proc p
	left join pg_namespace n on p.pronamespace = n.oid
	left join pg_language l on p.prolang = l.oid
	left join pg_type t on t.oid = p.prorettype 
	where n.nspname not in ('pg_catalog', 'information_schema', 'public')
	  and p.proname = 'insert_nissan_vehicle_details' -- 2 actual "identical" versions of this query
	order by function_schema, function_name



select * from pg_stat_database
select pg_stat_get_snapshot_timestamp()


SELECT viewname
FROM pg_views
WHERE viewname LIKE 'pg_stat%';