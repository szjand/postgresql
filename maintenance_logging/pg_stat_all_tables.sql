﻿feel like i fucked up by resetting tables
should just go with snapshots

-- https://dataegret.com/2017/04/deep-dive-into-postgres-stats-pg_stat_all_tables/
select * 
from pg_stat_all_tables 
where schemaname not in ('pg_toast','pg_catalog')
order by seq_Scan desc
order by n_dead_tup desc

select * from pg_stat_statements


select * from pg_stat_all_tables where relname = 'stores' and schemaname = 'onedc'
select pg_stat_reset_single_table_counters(3178527);

select 'select pg_stat_reset_single_table_counters('||relid||');' 
from pg_stat_all_tables 
where relname = 'stores'

select pg_stat_reset_single_table_counters(469369);
select pg_stat_reset_single_table_counters(3178527);

select * from pg_stat_all_tables where relname = 'stores'

08/09/21
reset all table statistics, run query into csv, past csv into pgadmin
and create the an hourly snapshot



select 'select pg_stat_reset_single_table_counters('||relid||');'
from pg_stat_all_tables 
where schemaname not in ('pg_toast','pg_catalog','information_schema')

-- it appears that only the seq_scan gets reset, the idx_scan still has the counts & tuples
select *
from pg_stat_user_tables

select schemaname, count(*)
from pg_stat_user_tables
group by schemaname
order by schemaname

select schemaname, count(*)
from pg_stat_all_tables
group by schemaname
order by schemaname

-- from mastering_postgresql_96, lots of queries on stat tables
SELECT schemaname, relname, seq_scan, seq_tup_read,
idx_scan, seq_tup_read / seq_scan AS avg
FROM pg_stat_user_tables
WHERE seq_scan > 0
ORDER BY seq_tup_read DESC


-- unused indexes
-- from https://www.cybertec-postgresql.com/en/get-rid-of-your-unused-indexes/
-- WOW, some HUGE indexes never used
SELECT s.schemaname,
       s.relname AS tablename,
       s.indexrelname AS indexname,
       pg_relation_size(s.indexrelid) AS index_size
FROM pg_catalog.pg_stat_user_indexes s
   JOIN pg_catalog.pg_index i ON s.indexrelid = i.indexrelid
WHERE s.idx_scan = 0      -- has never been scanned
  AND 0 <>ALL (i.indkey)  -- no index column is an expression
  AND NOT i.indisunique   -- is not a UNIQUE index
  AND NOT EXISTS          -- does not enforce a constraint
         (SELECT 1 FROM pg_catalog.pg_constraint c
          WHERE c.conindid = s.indexrelid)
ORDER BY pg_relation_size(s.indexrelid) DESC;


select * from pg_stat_user_tables order by seq_scan desc

select count(*) from pg_stat_activity where not pid = pg_backend_pid()

drop table if exists jon.table_statistics_snapshots cascade;
create table jon.table_statistics_snapshots as
select current_timestamp as ts, a.*
from pg_stat_user_tables a;


select *
from jon.table_statistics_snapshots a
order by schemaname,relname, ts desc



select *
from jon.table_statistics_snapshots a
where exists (
  select 1
  from jon.table_statistics_snapshots
  where relid = a.relid
    and seq_scan <> 0)
order by schemaname,relname, ts desc