﻿-- currently in service per ontrac report  34 units
select * from ontrac.active_vehicles_report where active and current_row

-- accounting for an in service vehicle
-- account 127703 SERV. LOANER INVENTORY 
select b.account, b.description, a.amount
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
where a.control = 'g42118'
order by abs(amount) desc 


-- 2 bogus vehicles, 27096 & 31522, leaving 36 units
select a.control, sum(a.amount)
from fin.fact_gl a
join fin.dim_Account b on a.account_key = b.account_key
  and b.account = '127703'
where a.post_Status = 'Y'  
group by a.control  
having sum(a.amount) > 10000

-- total inventory in service 1096661
select sum(a.amount)::integer
from fin.fact_gl a
join fin.dim_Account b on a.account_key = b.account_key
  and b.account = '127703'
where a.post_Status = 'Y'  
  and control not in ('27096','31522')
having sum(a.amount) > 10000


the page is populated by jeri.get_inventory_budgets()
	yearly: jeri.inventory_balances
					jeri.inventory_accounts


select * from jeri.inventory_accounts
insert into jeri.inventory_accounts values('RY1','NC','127703', '285', 'CTP Inventory');


select * from fin.dim_fs_account where gl_Account = '127703'  -- 285

current balance $4,087,126\

select 5703330 - 4087126