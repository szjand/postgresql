﻿-- create schema ontrac;
-- comment on schema ontrac is 'for ontrac/ctp files as we start the ctp project';

drop table if exists ontrac.ext_active_vehicles_report cascade;
create unlogged table ontrac.ext_active_vehicles_report (
  vin citext not null,
  make citext not null,
  model citext,
  model_year integer,
  stock_number citext,
  plate_number citext,
  loan_status citext not null,
  onstar_status citext not null,
  days_of_ctp_service integer not null, 
  total_qualified_miles integer not null,
  odometer integer not null,
  primary key(vin));


select * from ontrac.ext_active_vehicles_report  

-- 06/04/21
-- taylor is backing off the scrape once a day position, so
-- do a type 2 with timestamp

drop table if exists ontrac.active_vehicles_report cascade;
create table ontrac.active_vehicles_report (
  vin citext not null,
  make citext not null,
  model citext,
  model_year integer,
  stock_number citext,
  plate_number citext,
  loan_status citext not null,
  onstar_status citext not null,
  days_of_ctp_service integer not null, 
  total_qualified_miles integer not null,
  odometer integer not null,
  row_from_ts timestamptz,
  row_thru_ts timestamp default '12/31/9999 12:01:01.0' check,
  current_row boolean,
  primary key(vin,row_from_ts));
alter table ontrac.active_vehicles_report
add constraint thru_ts_from_ts
check (row_thru_ts > row_from_ts);

alter table ontrac.active_vehicles_report
add column active boolean;
update ontrac.active_vehicles_report set active = true;
alter table ontrac.active_vehicles_report
alter column active set not null;

drop table if exists ontrac.active_vehicles_report_changed_rows cascade;
create table ontrac.active_vehicles_report_changed_rows (
	vin citext primary key);


select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ontrac'
and table_name = 'ext_active_vehicles_report'

select * from ontrac.active_vehicles_report order by vin, row_from_ts

select row_from_ts, count(*) from ontrac.active_vehicles_report group by row_from_ts order by row_from_ts

select max(row_from_ts) from ontrac.active_vehicles_report

select * from ontrac.active_vehicles_report where row_from_ts::date = current_date order by row_from_ts

select * from ontrac.active_vehicles_report where not active

select * from ontrac.active_vehicles_report where active order by vin, row_from_ts

create temp table ext_6_6 as
select 'extr' as source, vin, loan_Status, onstar_Status, days_of_ctp_service, total_qualified_miles, odometer
from ontrac.ext_active_vehicles_report  
union
create temp table prod_6_6 as
select 'prod' as source, vin, loan_Status, onstar_Status, days_of_ctp_service, total_qualified_miles, odometer
from ontrac.active_vehicles_report  
where current_row
order by vin, source

select a.vin, a.odometer, b.odometer
from ext_6_6 a
join  prod_6_6 b on a.vin = b.vin and a.odometer <> b.odometer

do $$
declare
	_now_ts timestamptz := (select now());
	_thru_ts timestamp := _now_ts - interval '1 second';
begin	
	-- insert row for new vehicle(s)
	insert into ontrac.active_vehicles_report (vin,make,model,model_year,stock_number,
		plate_number,loan_status,onstar_status,days_of_ctp_service,total_qualified_miles,
		odometer,row_from_ts,current_row,active)
	select vin,make,model,model_year,stock_number,plate_number,loan_status,onstar_status,
		days_of_ctp_service,total_qualified_miles,odometer, 
		_now_ts, true, true
	from ontrac.ext_active_vehicles_report a
	where not exists (
		select 1
		from ontrac.active_vehicles_report
		where vin = a.vin);

	-- changed row(s)
	truncate ontrac.active_vehicles_report_changed_rows;
	insert into ontrac.active_vehicles_report_changed_rows (vin)
	select c.vin
	from (
		select aa.vin,
			( -- generate a hash for ontrac.ext_active_vehicles_report
				select md5(z::text) as hash
				from (
					select vin,make,model,model_year,stock_number,plate_number,loan_status,onstar_status,
						days_of_ctp_service,total_qualified_miles,odometer
					from ontrac.ext_active_vehicles_report
					where vin = aa.vin) z)
		from ontrac.ext_active_vehicles_report aa) c
	join (
		select bb.vin,
			( -- generate a hash for ontrac.active_vehicles_report
				select md5(z::text) as hash
				from (
					select vin,make,model,model_year,stock_number,plate_number,loan_status,onstar_status,
						days_of_ctp_service,total_qualified_miles,odometer
					from ontrac.active_vehicles_report
					where vin = bb.vin
						and current_row) z)
		from ontrac.active_vehicles_report bb
		where current_row) d on c.vin = d.vin
			and c.hash <> d.hash;

	-- update current version of changed row(s)
	update ontrac.active_vehicles_report x
	set current_row = false,
			row_thru_ts = _thru_ts
	from ontrac.active_vehicles_report_changed_rows z
	where x.vin = z.vin
		and x.current_row;
			
	-- add new row for each changed row
	insert into ontrac.active_vehicles_report (vin,make,model,model_year,stock_number,
		plate_number,loan_status,onstar_status,days_of_ctp_service,total_qualified_miles,
		odometer,row_from_ts, current_row, active)
	select a.vin,make,model,model_year,stock_number,plate_number,loan_status,onstar_status,
		days_of_ctp_service,total_qualified_miles,odometer, 
		_now_ts, true, true
	from ontrac.ext_active_vehicles_report a
	join ontrac.active_vehicles_report_changed_rows b on a.vin = b.vin;

	-- update the active attribute for vehicles no longer in service
	update ontrac.active_vehicles_report
	set active = false
	where vin in (
	select distinct a.vin
	from ontrac.active_vehicles_report a
	where not exists (
	  select 1
	  from ontrac.ext_active_vehicles_report
	  where vin = a.vin));
	  
end $$;
