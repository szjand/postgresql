﻿select * from gmgl.service_make_csi_ext

delete from gmgl.service_make_csi_ext



select * from gmgl.service_make_csi where report_date = current_date

select * from luigi.luigi_log where the_date = current_date and pipeline = 'gm_csi' and task like 'servicemake%'

delete from luigi.luigi_log where the_date = current_date and pipeline = 'gm_csi' and task like 'servicemake%'

select * from arkona.ext_pymast where employee_last_name = 'kuester'

select * from nrv.users where last_name = 'kuester'

update nrv.users
set is_active = true
where last_name = 'kuester'

select * from chr.build_data_describe_vehicle where vin in ('3GNAXUEV9KS568522','2GNAXKEV2K6184312','3GNAXUEV9KL217944','2GNAXVEX8K6160566')
delete from chr.build_data_describe_vehicle where vin in ('3GNAXUEV9KS568522','2GNAXKEV2K6184312','3GNAXUEV9KL217944','2GNAXVEX8K6160566')

select vin, response->'vinDescription'->>'source'
from chr.build_data_describe_vehicle where vin in ('3GNAXUEV9KS568522','2GNAXKEV2K6184312','3GNAXUEV9KL217944','2GNAXVEX8K6160566')

select vin, response->'vinDescription'->>'division', 
	response->'vinDescription'->>'modelName',
	response->'vinDescription'->>'modelYear',
	response->'vinDescription'->>'source'
from chr.build_data_describe_vehicle
where response->'vinDescription'->>'source' = 'Build'

delete
-- select vin, response->'vinDescription'->>'source' 
from chr.build_data_describe_vehicle
where response->'vinDescription'->>'source' = 'Catalog'



-- equinox_pricing_analysis_1.csv
select *
from (
	select dealership_name, vin, miles, miles_per_year, mileage_categories as mileage_cat, days_scraped_from_website as age, price::integer
	-- 	select * 
	from scrapes.get_used_vehicle_competitors_table()
	where model = 'equinox'
	and trim_level = 'LT'
	and model_year = 2019) aa
join (
select avg(a.price)::integer as mean_price, avg(miles)::integer as mean_miles, avg(miles_per_year)::integer as mean_miles_per_year  -- 23342
from (
	select dealership_name, vin, miles, miles_per_year, mileage_categories as mileage_cat, days_scraped_from_website as age, price::integer
-- 	select * 
	from scrapes.get_used_vehicle_competitors_table()
	where model = 'equinox'
	and trim_level = 'LT'
	and model_year = 2019) a) bb on true
join (
select jon.median(price_array)::integer as median_price, jon.median(miles_array)::integer as median_miles,
	jon.median(miles_per_year_array)::integer as median_miles_per_year-- 23449
from (
	select array_agg(price::numeric order by price) as price_array, array_agg(miles::numeric order by miles) as miles_array,
	  array_agg(miles_per_year::numeric order by miles) as miles_per_year_array
	from (
		select dealership_name, vin, miles, miles_per_year, mileage_categories as mileage_cat, days_scraped_from_website as age, price::integer
	-- 	select * 
		from scrapes.get_used_vehicle_competitors_table()
		where model = 'equinox'
		and trim_level = 'LT'
		and model_year = 2019) a) b) cc on true


drop table if exists equinox cascade;
create temp table equinox as
select dealership_name as dealer, vin, miles, miles_per_year, mileage_categories as mileage_cat, days_scraped_from_website as age, price::integer
-- 	select * 
from scrapes.get_used_vehicle_competitors_table()
where model = 'equinox'
and trim_level = 'LT'
and model_year = 2019

-- z: (price - mean)/stddev
select dealer, vin, color, miles, a.price, 
	round((a.price::numeric(8,3) - b.mean_price)/stddev_price, 3) as z_price,
	round((a.miles::numeric(8,3) - b.mean_miles)/stddev_miles, 3) as z_miles
from equinox a
join (
	select stddev(price)::integer as stddev_price, stddev(miles)::integer as stddev_miles, 
		jon.median(array_agg(price::numeric order by price))::integer as median_price, 
		jon.median(array_agg(miles::numeric order by miles))::integer as median_miles,
		avg(price)::integer as mean_price, avg(miles)::integer as mean_miles, av
		max(price) - min(price) as range_price, max(miles) - min(miles) as range_miles
	from equinox) b on true
order by (a.price::numeric(8,3) - b.mean_price)/stddev_price desc





select * from equinox
where vin not in ('3GNAXUEV9KS568522','2GNAXKEV2K6184312','3GNAXUEV9KL217944','2GNAXVEX8K6160566')

drop table if exists equinox cascade;
create temp table equinox as
select dealership_name as dealer, vin, color, miles, price 
from (
  -- competitors
	select lookup_date, dealership_name, trim_level,
		 vin, 
		color, miles, price,
		row_number() over (partition by vin order by lookup_date desc) as seq
	from scrapes.competitor_websites_data a
	inner join scrapes.competitor_websites b on a.scraping_url = b.scraping_url
	where lookup_date > current_date -31
		and model_year = '2019'
		and model = 'equinox'
		and trim_level = 'LT'
		and position ('FWD' in upper(raw_data)) = 0
	) x where  seq = 1
union
-- rydell
select 'Rydell', vin, exteriorcolor, miles, best_price
from (
	select a.*, b.website_url
	from (
		select 'Rydell', current_date - fromts::date as age, b.vin, b.yearmodel::integer as model_year, b.make, b.model, 
		 case when body_style like '%crew%' then 'crew' when body_style like '%double%' then 'double' else 'unknown' end as cab,
		round(e.value/round((current_date - concat('09/01/',b.yearmodel::citext)::date)::numeric / 365, 2)) as miles_per_year,
		 b.trim, b.exteriorcolor, e.value as miles, d.amount as best_price, 'used'
		from ads.ext_vehicle_inventory_items a
		join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
		--   and b.model = 'silverado 1500'
		--   and b.yearmodel::integer between 2017 and 2020
		left join (
			select vehicleinventoryitemid, vehiclepricingid
			from ads.ext_vehicle_pricings aa
			where vehiclepricingts = (
				select max(vehiclepricingts)
				from ads.ext_Vehicle_pricings
				where vehicleinventoryitemid = aa.vehicleinventoryitemid)) c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
		left join ads.ext_vehicle_pricing_details d on c.vehiclepricingid = d.vehiclepricingid
			and d.typ = 'VehiclePricingDetail_BestPrice'
		left join (
			select vehicleitemid, value
			from ads.ext_vehicle_item_mileages bb
			where vehicleitemmileagets = (
				select max(vehicleitemmileagets)
				from ads.ext_vehicle_item_mileages
				where vehicleitemid = bb.vehicleitemid)) e on b.vehicleitemid = e.vehicleitemid
		inner join arkona.xfm_inpmast f on b.vin = f.inpmast_vin and current_row = true
		where a.thruts > now()) a
	inner join scrapes.most_recent_rydell_scrape b on a.vin = b.vin
		and website_title = 'rydellcars'
		and best_price is not null
	where model = 'equinox'
	  and model_year = 2019
	  and trim = 'lt') b;

-- 2019 equinox table	for generating build data (finally)
-- 60 days back

drop table if exists jon.equinox_pricing_analysis cascade;
create table jon.equinox_pricing_analysis as
select aa.*, ((bb.response->'vinDescription'->>'builtMSRP')::numeric)::integer as  msrp,
	cc->>'drivetrain' as drivetrain
from (
	select dealership_name as dealer, vin, color, miles, price 
	from (
		-- competitors
		select lookup_date, dealership_name, trim_level,
			 vin, 
			color, miles, price,
			row_number() over (partition by vin order by lookup_date desc) as seq
		from scrapes.competitor_websites_data a
		inner join scrapes.competitor_websites b on a.scraping_url = b.scraping_url
		where lookup_date > current_date -61
			and model_year = '2019'
			and model = 'equinox'
			and trim_level = 'LT'
-- 			and position ('FWD' in upper(raw_data)) = 0
		) x where  seq = 1
	union
	-- rydell
	select 'Rydell', vin, exteriorcolor, miles, best_price
	from (
		select a.*, b.website_url
		from (
			select 'Rydell', current_date - fromts::date as age, b.vin, b.yearmodel::integer as model_year, b.make, b.model, 
			 case when body_style like '%crew%' then 'crew' when body_style like '%double%' then 'double' else 'unknown' end as cab,
			round(e.value/round((current_date - concat('09/01/',b.yearmodel::citext)::date)::numeric / 365, 2)) as miles_per_year,
			 b.trim, b.exteriorcolor, e.value as miles, d.amount as best_price, 'used'
			from ads.ext_vehicle_inventory_items a
			join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
			--   and b.model = 'silverado 1500'
			--   and b.yearmodel::integer between 2017 and 2020
			left join (
				select vehicleinventoryitemid, vehiclepricingid
				from ads.ext_vehicle_pricings aa
				where vehiclepricingts = (
					select max(vehiclepricingts)
					from ads.ext_Vehicle_pricings
					where vehicleinventoryitemid = aa.vehicleinventoryitemid)) c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
			left join ads.ext_vehicle_pricing_details d on c.vehiclepricingid = d.vehiclepricingid
				and d.typ = 'VehiclePricingDetail_BestPrice'
			left join (
				select vehicleitemid, value
				from ads.ext_vehicle_item_mileages bb
				where vehicleitemmileagets = (
					select max(vehicleitemmileagets)
					from ads.ext_vehicle_item_mileages
					where vehicleitemid = bb.vehicleitemid)) e on b.vehicleitemid = e.vehicleitemid
			inner join arkona.xfm_inpmast f on b.vin = f.inpmast_vin and current_row = true
			where a.thruts > now()) a
		inner join scrapes.most_recent_rydell_scrape b on a.vin = b.vin
			and website_title = 'rydellcars'
			and best_price is not null
		where model = 'equinox'
			and model_year = 2019
			and trim = 'lt') b) aa
left join chr.build_data_describe_vehicle bb on aa.vin = bb.vin			
left join jsonb_array_elements(bb.response->'style') cc on true
where aa.price <> 0
order by cc->>'drivetrain'

select distinct vin
from jon.equinox_pricing_analysis	a
where not exists (
	select 1
	from chr.build_data_describe_vehicle
	where vin = a.vin)

select a.vin, response->'vinDescription'->>'division', 
	response->'vinDescription'->>'modelName',
	response->'vinDescription'->>'modelYear',
	response->'vinDescription'->>'builtMSRP',
	response->'vinDescription'->>'source', b.vin
from chr.build_data_describe_vehicle a
left join gmgl.vehicle_invoices b on a.vin = b.vin
where response->'vinDescription'->>'source' = 'Build'	
  and response->'vinDescription'->>'modelName'= 'Equinox'

select * from gmgl.vehicle_invoices where vin = '3GNAXUEV9KS568522' 

select * from jon.equinox_pricing_analysis

select dealer, vin, color, miles, a.price, a.msrp,
	round((a.price::numeric(8,3) - b.mean_price)/stddev_price, 3) as z_price,
	round((a.miles::numeric(8,3) - b.mean_miles)/stddev_miles, 3) as z_miles,
	round((a.msrp::numeric(8,3) - b.mean_msrp)/stddev_msrp, 3) as z_msrp
from jon.equinox_pricing_analysis a	
join (
	select count(*), stddev(price)::integer as stddev_price, stddev(miles)::integer as stddev_miles, stddev(msrp)::integer as stddev_msrp,
		jon.median(array_agg(price::numeric order by price))::integer as median_price, 
		jon.median(array_agg(miles::numeric order by miles))::integer as median_miles,
		avg(price)::integer as mean_price, avg(miles)::integer as mean_miles, avg(msrp)::integer as mean_msrp,
		max(price) - min(price) as range_price, max(miles) - min(miles) as range_miles
	from jon.equinox_pricing_analysis
	where drivetrain = 'Front Wheel Drive') b on true
where a.price <> 0	-- one fargo vehicle has 0 price
  AND  a.drivetrain = 'Front Wheel Drive'
-- order by round((a.msrp::numeric(8,3) - b.mean_msrp)/stddev_msrp, 3)	desc
order by (a.price::numeric(8,3) - b.mean_price)/stddev_price desc




 -- 2019 silverado 1500 4WD, Crew, LT
-- trim down the queries, get as much as possible from build data
select dealership_name as dealer, vin, color, miles, price, model,  position('4WD' in upper(raw_data)), position('CREW' in upper(raw_data))
from scrapes.competitor_websites_data a
join scrapes.competitor_websites b on a.scraping_url = b.scraping_url
where model like 'silverado%'
  and model_year = '2019'
  and trim_level = 'LT'
  and lookup_date > current_date -31

select raw_data from scrapes.competitor_websites_data where vin = '1GCUYDED0KZ101860' and lookup_date = current_date


drop table if exists jon.equinox_pricing_analysis cascade;
create table jon.equinox_pricing_analysis as
select aa.*, ((bb.response->'vinDescription'->>'builtMSRP')::numeric)::integer as  msrp
from (
	select dealership_name as dealer, vin, color, miles, price
	from (
		-- competitors
		select lookup_date, dealership_name, model, trim_level,
			 vin, 
			color, miles, price,
			row_number() over (partition by vin order by lookup_date desc) as seq
		from scrapes.competitor_websites_data a
		inner join scrapes.competitor_websites b on a.scraping_url = b.scraping_url
		where lookup_date > current_date -31
			and model_year = '2018'
			and model like 'silverado 1%'
			and trim_level = 'LT'
		) x where  seq = 1
	union
	-- rydell
	select 'Rydell', vin, exteriorcolor, miles, best_price
	from (
		select a.*, b.website_url
		from (
			select 'Rydell', current_date - fromts::date as age, b.vin, b.yearmodel::integer as model_year, b.make, b.model, 
			 case when body_style like '%crew%' then 'crew' when body_style like '%double%' then 'double' else 'unknown' end as cab,
			round(e.value/round((current_date - concat('09/01/',b.yearmodel::citext)::date)::numeric / 365, 2)) as miles_per_year,
			 b.trim, b.exteriorcolor, e.value as miles, d.amount as best_price, 'used'
			from ads.ext_vehicle_inventory_items a
			join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
			--   and b.model = 'silverado 1500'
			--   and b.yearmodel::integer between 2017 and 2020
			left join (
				select vehicleinventoryitemid, vehiclepricingid
				from ads.ext_vehicle_pricings aa
				where vehiclepricingts = (
					select max(vehiclepricingts)
					from ads.ext_Vehicle_pricings
					where vehicleinventoryitemid = aa.vehicleinventoryitemid)) c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
			left join ads.ext_vehicle_pricing_details d on c.vehiclepricingid = d.vehiclepricingid
				and d.typ = 'VehiclePricingDetail_BestPrice'
			left join (
				select vehicleitemid, value
				from ads.ext_vehicle_item_mileages bb
				where vehicleitemmileagets = (
					select max(vehicleitemmileagets)
					from ads.ext_vehicle_item_mileages
					where vehicleitemid = bb.vehicleitemid)) e on b.vehicleitemid = e.vehicleitemid
			inner join arkona.xfm_inpmast f on b.vin = f.inpmast_vin and current_row = true
			where a.thruts > now()) a
		inner join scrapes.most_recent_rydell_scrape b on a.vin = b.vin
			and website_title = 'rydellcars'
			and best_price is not null
		where model like 'silverado%'
			and model_year = 2019
			and trim = 'lt') b) aa
left join chr.build_data_describe_vehicle bb on aa.vin = bb.vin			


select inpmast_vin, year, make, model 
from arkona.ext_inpmast 
where status = 'I'
  and year = 2019
  
order by model  
  

select a.dealer, a.vin, 
  c->>'colorName', replace(e->>'_value_1','Bed','') as bed, replace(replace(f->>'_value_1', 'Pickup', ''),'Cab','') as cab,
  d->>'drivetrain' as drive,
  g->>'cylinders' as cylinders, replace(g->'fuelType'->>'_value_1','Fuel','') as fuel, h->>'_value_1' as displacement,
  a.price, a.miles, ((b.response->'vinDescription'->>'builtMSRP')::numeric)::integer as  msrp
from jon.equinox_pricing_analysis a
join chr.build_data_describe_vehicle b on a.vin = b.vin
  and b.response->'vinDescription'->>'source' = 'Build'
join jsonb_array_elements(b.response->'exteriorColor') c on true
  and c->'installed'->>'cause' = 'RelatedColor'
join jsonb_array_elements(b.response->'style') d on true
join jsonb_array_elements(d->'bodyType') e on true
  and e->>'primary' = 'false'
join jsonb_array_elements(d->'bodyType') f on true
  and f->>'primary' = 'true'
join jsonb_array_elements(b.response->'engine') g on true
  and g->'installed'->>'cause' = 'OptionCodeBuild'
join jsonb_array_elements(g->'displacement'->'value') h on true
  and h->>'unit' = 'liters'
order by a.dealer, a.vin




-- ***********************************************************************
update chr.build_data_describe_vehicle a
set source = b.source
from (
  select vin, response->'vinDescription'->>'source' as source
  from chr.build_data_describe_vehicle 
  where source is null) b
where a.vin = b.vin    
-- ***********************************************************************


