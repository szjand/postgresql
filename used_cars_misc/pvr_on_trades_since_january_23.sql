﻿drop table if exists evals;  -- 448
create temp table evals as
select b.stocknumber as stock_number, d.vin, vehicleevaluationts::date as eval_date, c.fullname as evaluator, a.vehicleinventoryitemid, d.vehicleitemid -- 441
-- select a.*
from ads.ext_vehicle_evaluations a
join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid  = b.vehicleinventoryitemid
join ads.ext_people c on a.vehicleevaluatorid = c.partyid
join ads.ext_vehicle_items d on b.vehicleitemid = d.vehicleitemid
where vehicleevaluationts::date > '01/18/2023';

select * from evals where right(stock_number, 1) in ('G','H','T')
-- the problem with the join on stocknumber, after a vehicle has been intramarket ws'd the stocknumber is not in evals, so it is excluded
-- i need a way to know if a vehicle that was eval'd was subsequently intramarket ws'd
drop table if exists boarded;   --204
create temp table boarded as
select d.arkona_store_key as store,
	a.board_type_key, a.stock_number, 
  case
	  when b.board_sub_type = 'Intercompany Wholesale' then (select * from ads.get_intra_market_wholesale_new_stock_number(a.stock_number)) 
		else null
	end as imws_stock_number,
	a.vin, a.boarded_date, b.board_type, b.board_sub_type as disposition,
	c.vehicle_model_year as model_year, c.vehicle_make as make, c.vehicle_model as model, 
	e.trim, aa.vehicleinventoryitemid, aa.vehicleitemid, evaluator
from board.sales_board a
join evals aa on a.stock_number = aa.stock_number
join board.board_types b on a.board_type_key = b.board_type_key
	and b.board_type = 'Deal'
join board.daily_board c on a.board_id = c.board_id
	and c.vehicle_make not in ('yamaha','kawasaki','HARLEY','HARLEY DAVIDSON','HARLEY-DAVIDSON','SUZUKI')
	and c.vehicle_type = 'U'
join onedc.stores d on a.store_key = d.store_key  
left join ads.ext_vehicle_items e on a.vin = e.vin
where a.boarded_date between '01/19/2023' and current_date
	and not a.is_deleted
	and not is_backed_on
	and right(a.stock_number, 1) <> 'L'
	and a.vin <> 'CB3602006227'  

select * from boarded	order by disposition -- 14 were intramarket ws'd


drop table if exists imws; -- 13
create temp table imws as
select d.arkona_store_key as store,
	a.board_type_key, a.stock_number, 
	a.vin, a.boarded_date, b.board_type, b.board_sub_type as disposition,
	c.vehicle_model_year as model_year, c.vehicle_make as make, c.vehicle_model as model, e.trim
from board.sales_board a
join (
	select imws_stock_number 
	from boarded	
	where disposition = 'Intercompany Wholesale'
		and imws_stock_number is not null)  aa on a.stock_number = aa.imws_stock_number
join board.board_types b on a.board_type_key = b.board_type_key
	and b.board_type = 'Deal'
join board.daily_board c on a.board_id = c.board_id
	and c.vehicle_make not in ('yamaha','kawasaki','HARLEY','HARLEY DAVIDSON','HARLEY-DAVIDSON','SUZUKI')
	and c.vehicle_type = 'U'
join onedc.stores d on a.store_key = d.store_key  
left join ads.ext_vehicle_items e on a.vin = e.vin
where a.boarded_date between '01/19/2023' and current_date
	and not a.is_deleted
	and not is_backed_on
	and right(a.stock_number, 1) <> 'L'
	and a.vin <> 'CB3602006227'; 

-- accounting cost and sales

select * from fin.dim_account limit 10
-- from marketing_bdc/morgan
--
select sum(gross)/106 as avg_pvr
from (
	select a.stock_number, sum(-b.amount) filter (where c.account_type = 'COGS') as cost,
		sum(-b.amount) filter (where c.account_type = 'Sale') as sales,
		sum(-b.amount) as gross, a.disposition
	from boarded a
	join fin.fact_gl b on a.stock_number = b.control
		and b.post_status = 'Y'
	join fin.dim_account c on b.account_key = c.account_key
		and c.account in (
			select b.g_l_acct_number::citext as account
			from arkona.ext_eisglobal_sypffxmst a
			inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
				and b.factory_financial_year  = (select max(factory_financial_year) from arkona.ext_ffpxrefdta)
			where a.fxmcyy = (select max(fxmcyy) from arkona.ext_eisglobal_sypffxmst)
				and coalesce(b. consolidation_grp, '1') <> '3'
				and b.g_l_acct_number <> ''
				and a.fxmpge = 16 
				and a.fxmlne between 1 and 14
				and b.g_l_acct_number not in ('144502','164502')) 
	where disposition = 'Retail'      
	group by a.stock_number, a.disposition) d


-- all together for afton
select sum(gross)/106 as avg_pvr
from (
	select a.stock_number, sum(-b.amount) filter (where c.account_type = 'COGS') as cost,
		sum(-b.amount) filter (where c.account_type = 'Sale') as sales,
		sum(-b.amount) as gross, a.disposition
	from ( -- boarded deals
		select d.arkona_store_key as store,
			a.board_type_key, a.stock_number, 
			case
				when b.board_sub_type = 'Intercompany Wholesale' then (select * from ads.get_intra_market_wholesale_new_stock_number(a.stock_number)) 
				else null
			end as imws_stock_number,
			a.vin, a.boarded_date, b.board_type, b.board_sub_type as disposition,
			c.vehicle_model_year as model_year, c.vehicle_make as make, c.vehicle_model as model, e.trim, aa.vehicleinventoryitemid, aa.vehicleitemid
		from board.sales_board a
		join (	
			select b.stocknumber as stock_number, d.vin, vehicleevaluationts::date as eval_date, c.fullname as evaluator, a.vehicleinventoryitemid, d.vehicleitemid -- 441
			from ads.ext_vehicle_evaluations a
			join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid  = b.vehicleinventoryitemid
			join ads.ext_people c on a.vehicleevaluatorid = c.partyid
			join ads.ext_vehicle_items d on b.vehicleitemid = d.vehicleitemid
			where vehicleevaluationts::date > '01/18/2023') aa on a.stock_number = aa.stock_number
		join board.board_types b on a.board_type_key = b.board_type_key
			and b.board_type = 'Deal'
		join board.daily_board c on a.board_id = c.board_id
			and c.vehicle_make not in ('yamaha','kawasaki','HARLEY','HARLEY DAVIDSON','HARLEY-DAVIDSON','SUZUKI')
			and c.vehicle_type = 'U'
		join onedc.stores d on a.store_key = d.store_key  
		left join ads.ext_vehicle_items e on a.vin = e.vin
		where a.boarded_date between '01/19/2023' and current_date
			and not a.is_deleted
			and not is_backed_on
			and right(a.stock_number, 1) <> 'L'
			and a.vin <> 'CB3602006227') a
	join fin.fact_gl b on a.stock_number = b.control
		and b.post_status = 'Y'
	join fin.dim_account c on b.account_key = c.account_key
		and c.account in (
			select b.g_l_acct_number::citext as account
			from arkona.ext_eisglobal_sypffxmst a
			inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
				and b.factory_financial_year  = (select max(factory_financial_year) from arkona.ext_ffpxrefdta)
			where a.fxmcyy = (select max(fxmcyy) from arkona.ext_eisglobal_sypffxmst)
				and coalesce(b. consolidation_grp, '1') <> '3'
				and b.g_l_acct_number <> ''
				and a.fxmpge = 16 
				and a.fxmlne between 1 and 14
				and b.g_l_acct_number not in ('144502','164502')) 
	where disposition = 'Retail'      
	group by a.stock_number, a.disposition) d

-- gross by evaluator for greg
-- select disposition, sum(evals) from ( -- 106/81
select evaluator, disposition, count(*) as evals, sum(gross)::integer as total_gross, (sum(gross)/count(*))::integer as avg_gross
from (
	select a.stock_number, evaluator, disposition, sum(b.amount) filter (where c.account_type = 'COGS') as cost,
		sum(-b.amount) filter (where c.account_type = 'Sale') as sales,
		sum(-b.amount) as gross
	from boarded a
	join fin.fact_gl b on a.stock_number = b.control
		and b.post_status = 'Y'
	join fin.dim_account c on b.account_key = c.account_key
		and c.account in (
			select b.g_l_acct_number::citext as account
			from arkona.ext_eisglobal_sypffxmst a
			inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
				and b.factory_financial_year  = (select max(factory_financial_year) from arkona.ext_ffpxrefdta)
			where a.fxmcyy = (select max(fxmcyy) from arkona.ext_eisglobal_sypffxmst)
				and coalesce(b. consolidation_grp, '1') <> '3'
				and b.g_l_acct_number <> ''
				and a.fxmpge = 16 
				and a.fxmlne between 1 and 14
				and b.g_l_acct_number not in ('144502','164502')) 
-- 	where disposition = 'Retail'      
	group by a.stock_number, evaluator, disposition) d
where disposition <> 'Intercompany Wholesale'	
group by evaluator, disposition
-- ) x group by disposition
order by evaluator, disposition

-- current inventory

select stocknumber, eval_date, evaluator, cost::integer, best_price, best_price - cost::integer as plg 
from (
	select a.stocknumber, b.vehicleevaluationts::date as eval_date, c.fullname as evaluator, sum(d.amount) as cost,
		(select ads.get_current_best_price_by_stock_number_used(a.stocknumber)) as best_price
	from ads.ext_vehicle_inventory_items a
	join ads.ext_vehicle_evaluations b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
	join ads.ext_people c on b.vehicleevaluatorid = c.partyid
	join fin.fact_gl d on a.stocknumber = d.control
		and d.post_status = 'Y'
	join fin.dim_account e on d.account_key = e.account_key
	join jeri.inventory_accounts f on e.account = f.account
		and f.department = 'uc'
		and f.account not in ('124101','124001','224002','224102')
	where a.thruts::date > current_date 
	  and a.stocknumber <> 'G46596B'
	group by a.stocknumber, b.vehicleevaluationts::date, c.fullname) g
where coalesce(best_price,0) > 0
  and cost > 0
  and best_price < 100000
order by evaluator
  
select evaluator, count(*), sum(cost) as cost, sum(best_price) as total_price, (sum(best_price - cost)/count(*))::integer as avg_plg
from (
select stocknumber, eval_date, evaluator, cost::integer, best_price, best_price - cost::integer as plg 
from (
	select a.stocknumber, b.vehicleevaluationts::date as eval_date, c.fullname as evaluator, sum(d.amount) as cost,
		(select ads.get_current_best_price_by_stock_number_used(a.stocknumber)) as best_price
	from ads.ext_vehicle_inventory_items a
	join ads.ext_vehicle_evaluations b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
	join ads.ext_people c on b.vehicleevaluatorid = c.partyid
	join fin.fact_gl d on a.stocknumber = d.control
		and d.post_status = 'Y'
	join fin.dim_account e on d.account_key = e.account_key
	join jeri.inventory_accounts f on e.account = f.account
		and f.department = 'uc'
		and f.account not in ('124101','124001','224002','224102')
	where a.thruts::date > current_date 
	  and a.stocknumber <> 'G46596B'
	group by a.stocknumber, b.vehicleevaluationts::date, c.fullname) g
where coalesce(best_price,0) > 0
  and cost > 0
  and best_price < 100000) h
group by evaluator  



select a.*, b.description
from jeri.inventory_accounts a
join fin.dim_account b on a.account = b.account
where a.department = 'uc'

