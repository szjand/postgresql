﻿09/21/21
think ben f has derived some good analysis
the task today is make a list for wilkie of 2018 & 2019 silverad LT and equinox LT
he picks out the exceptions
we compare that to the analytics



drop table if exists jon.rydell cascade;
create table jon.rydell as
with 
	pricings as (
		select a.vehicleinventoryitemid, b.amount as best_price,
			row_number() over (partition by a.vehicleinventoryitemid order by vehiclepricingts desc) as seq
		from ads.ext_vehicle_pricings a
		join ads.ext_vehicle_pricing_details b on a.vehiclepricingid = b.vehiclepricingid
			and b.typ = 'VehiclePricingDetail_BestPrice'
		join ads.ext_vehicle_inventory_items c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
			and c.thruts > now()
		join ads.ext_vehicle_items d on c.vehicleitemid = d.vehicleitemid
			and d.model in ('silverado 1500', 'equinox')
			and d.yearmodel::integer between 2018 and 2019
			and d.trim = 'LT'),
	odo as (
		select a.vehicleitemid, a.value as miles,
			row_number() over (partition by a.vehicleitemid order by vehicleitemmileagets desc) as seq
		from ads.ext_vehicle_item_mileages a
		join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
			and b.model in ('silverado 1500', 'equinox')
			and b.yearmodel::integer between 2018 and 2019
			and b.trim = 'LT'
		join ads.ext_vehicle_inventory_items c on b.vehicleitemid = c.vehicleitemid
			and c.thruts > now()) 			   
select 'Rydell'::citext as dealer, b.vin, d.best_price, e.miles, round(e.miles/round((current_date - concat('09/01/',b.yearmodel::citext)::date)::numeric / 365, 2)) as miles_per_year
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
  and b.model in( 'silverado 1500', 'equinox')
  and b.yearmodel::integer between 2018 and 2019
  and b.trim = 'LT'
left join pricings d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
  and d.seq = 1
left join odo e on a.vehicleitemid = e.vehicleitemid
  and e.seq = 1  
where a.thruts > now()
  and best_price is not null; 

drop table if exists jon.competitors cascade;
create table jon.competitors as
select dealership_name as dealer, vin, price, miles,
	round(miles/round((current_date - concat('09/01/',model_year::citext)::date)::numeric / 365, 2)) as miles_per_year
from (
	-- competitors
	select lookup_date, dealership_name, model, trim_level,
		 vin, model_year,
		color, miles, price,
		row_number() over (partition by vin order by lookup_date desc) as seq
	from scrapes.competitor_websites_data a
	inner join scrapes.competitor_websites b on a.scraping_url = b.scraping_url
	where lookup_date > current_date - 31
		and (
			(model_year = '2018' and model like 'silverado 1%' and trim_level = 'LT') 
			or
			(model_year = '2019' and model like 'silverado 1%' and trim_level = 'LT')
			or
			(model_year = '2018' and model like 'equinox' and trim_level = 'LT')
			or
			(model_year = '2019' and model like 'equinox' and trim_level = 'LT'))) x
where  seq = 1
	and dealership_name not in ('echo park','Finley Motors','Ozark Chevrolet','Sioux Falls Luxury Auto Mall','Sioux Falls Ford');


-- chrome build data
select * 
from (
	select vin from jon.competitors
	union
	select vin from jon.rydell) a	
left join chr.build_data_describe_Vehicle b on a.vin = b.vin	

select * 
from (
	select vin from jon.competitors
	union
	select vin from jon.rydell) a	
where not exists (
  select 1
  from chr.build_data_describe_vehicle
  where vin = a.vin)

-- hmm, 2 without build data, emailed chrome support
select a.*, response->'vinDescription'->>'division', response->'vinDescription'->>'modelYear', response->'vinDescription'->>'modelName'
from chr.build_Data_describe_Vehicle a
where coalesce(style_count, 666) <> 0
  and coalesce(source, 'XXX') <> 'build' 

select vin, source, style_count
-- delete
from chr.build_Data_describe_Vehicle 
where coalesce(style_count, 666) <> 0
  and coalesce(source, 'XXX') <> 'build'   


-- the output
-- 09/21/21 sent out as competitor_pricing_V2.xlsx 
drop table if exists the_temp cascade;
create temp table the_temp as
select x.*
from (
	select a.*, (r.style->>'modelYear')::integer as year,
		(r.style ->'division'->>'_value_1')::citext as make,
		(r.style ->'model'->>'_value_1'::citext)::citext as model,
	--   (r.style ->>'mfrModelCode')::citext as chr_model_code,
		case
			when r.style ->>'drivetrain' like 'Rear%' then 'RWD'
			when r.style ->>'drivetrain' like 'Front%' then 'FWD'
			when r.style ->>'drivetrain' like 'Four%' then '4WD'
			when r.style ->>'drivetrain' like 'All%' then 'AWD'
			else 'XXX'
		end as drive,
		coalesce(r.style ->>'trim', 'none')::citext as trim_level,
		case
			when u.cab->>'_value_1' like 'Crew%' then 'Crew' 
			when u.cab->>'_value_1' like 'Extended%' then 'Double'  
			when u.cab->>'_value_1' like 'Regular%' then 'Reg'  
			else null
		end as cab,
		case
			when v.bed->>'_value_1' like '%Bed' then substring(bed->>'_value_1', 1, position(' ' in bed->>'_value_1') - 1)
			else null
		end as bed,   
		trim(t->>'_value_1') || ' ' || left(s->'fuelType'->>'_value_1', 3) as engine, 
		aa->>'colorName', (left(b.response->'vinDescription'->>'builtMSRP', position('.' in b.response->'vinDescription'->>'builtMSRP') -1))::integer as msrp
	from (
		select * from jon.competitors
		union
		select * from jon.rydell) a	
	left join chr.build_data_describe_vehicle b on a.vin = b.vin	
	left join jsonb_array_elements(b.response->'style') as r(style) on true
	left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
		and u.ordinality =  2
	left join jsonb_array_elements(r.style->'bodyType') with ordinality as v(bed) on true
		and v.ordinality =  1    
	left join jsonb_array_elements(b.response->'engine') as s(engine) on true  
		and s.engine->'installed'->>'cause' = 'OptionCodeBuild'
	left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
		and t.displacement ->>'unit' = 'liters' 
	left join jsonb_array_elements(b.response->'exteriorColor') as aa(color) on true
			and aa->'installed'->>'cause' = 'RelatedColor'
	where b.vin is not null) x 
where case when model = 'equinox' then drive = 'AWD' else 1 = 1 end	
  and price > 0
order by year, model;

-- 09/22/21 add the analytics

select a.* 
from the_temp a
order by year, model

select c.*, abs_z_dep - abs_z_miles
from (
	select b.*, 
		abs(round((miles::numeric(9,3) - mean_miles)/stddev_miles, 2)) as abs_z_miles,
		round(abs((dep_x - mean_dep)/stddev_dep), 2) as abs_z_dep
	from (
		select a.*, 
			(avg(miles) over (partition by year, left(model, 8)))::integer as mean_miles,
			(stddev(miles) over (partition by year, left(model, 8)))::integer as stddev_miles,
			round(abs(price/msrp -1), 3) as dep_x, 
			round(avg(abs(price/msrp -1)) over (partition by year, left(model,8)), 3) mean_dep,
			round(stddev(abs(price/msrp -1)) over (partition by year, left(model, 8)), 3) as stddev_dep
		from the_temp a) b) c
order by year, left(model, 8), abs_z_dep - abs_z_miles desc