﻿select a.vehicleitemid, a.vehicleinventoryitemid, a.stocknumber, b.vin, b.yearmodel, b.make, b.model
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
where right(a.stocknumber,1) in ('G','H','T')
  and a.thruts::date > current_date - 31

select distinct typ from ads.ext_vehicle_Sales limit 10  

select * from ads.ext_vehicle_Sales limit 10  

select * from ads.ext_vehicle_sales where typ = 'VehicleSale_Wholesale' and soldto is not null limit 20

select * from ads.ext_organizations

create index on ads.ext_vehicle_sales(soldto);

select a.vehicleinventoryitemid, c.vehicleitemid, c.stocknumber, d. vin, b.fullname, b.name, a.soldto, a.soldts
-- select distinct b.name, b.fullname
from ads.ext_vehicle_sales a 
join ads.ext_organizations b on a.soldto = b.partyid
  and b.fullname in ('GF-Honda Cartiva','GF-Rydell Auto Outlet','GF-Rydells','GF-Toyota')
join ads.ext_vehicle_inventory_items c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
join ads.ext_vehicle_items d on c.vehicleitemid = d.vehicleitemid
where typ = 'VehicleSale_Wholesale'
  and a.soldts::date > current_Date - 31


-- this generates 3 duplicates 1FBZX2ZG9HKB07082, 1GNFLGEK7FZ127361, 4S4WX93DX94403803
select * 
from (
	select a.vehicleitemid, a.vehicleinventoryitemid, a.stocknumber, b.vin, b.yearmodel, b.make, b.model
	from ads.ext_vehicle_inventory_items a
	join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
	where right(a.stocknumber,1) in ('G','H','T')
		and a.thruts::date > current_date - 31) aa
left join (
	select a.vehicleinventoryitemid, c.vehicleitemid, c.stocknumber, d. vin, b.fullname, b.name, a.soldto, a.soldts
	-- select distinct b.name, b.fullname
	from ads.ext_vehicle_sales a 
	join ads.ext_organizations b on a.soldto = b.partyid
		and b.fullname in ('GF-Honda Cartiva','GF-Rydell Auto Outlet','GF-Rydells','GF-Toyota')
	join ads.ext_vehicle_inventory_items c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
	join ads.ext_vehicle_items d on c.vehicleitemid = d.vehicleitemid
	where typ = 'VehicleSale_Wholesale') bb on aa.vin = bb.vin 
order by aa.vin


-- statuses on the new viiid
select * 
from ads.ext_vehicle_inventory_item_statuses
where vehicleinventoryitemid = 'aad3d558-66dd-4414-82ab-8cb8e16cf46c'
order by fromts

-- then new vii gets a status of RawMaterials_BookingPending
-- from ads SP GetBookPendingVehicleInfo
-- with an input of the new viiid, get the old stocknumber
-- pg doesn't do top, but limit and offset
  SELECT top 1 start at 2 stocknumber, VehicleInventoryItemID  
  select *
  FROM ads.ext_Vehicle_Inventory_Items
  WHERE VehicleItemID = (
    SELECT VehicleItemID
    FROM ads.ext_Vehicle_Inventory_Items 
    WHERE VehicleInventoryItemID = 'aad3d558-66dd-4414-82ab-8cb8e16cf46c')
  ORDER BY fromts DESC
  limit 1 offset 1
  
-- then new vii gets a status of RawMaterials_BookingPending
select a.fromts, a.vehicleinventoryitemid, b.stocknumber as new_stk
from ads.ext_vehicle_inventory_item_statuses a
join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
where a.status = 'RawMaterials_BookingPending'
	and a.fromts::date > current_date - interval '4 years'

-- then new vii gets a status of RawMaterials_BookingPending
-- eh this got messy, just do a function
select a.fromts, a.vehicleinventoryitemid, b.stocknumber as new_stk, c.thruts, c.stocknumber
from ads.ext_vehicle_inventory_item_statuses a
join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
left join ads.ext_vehicle_inventory_items c on b.vehicleitemid = c.vehicleitemid
where a.status = 'RawMaterials_BookingPending'
	and a.fromts::date > current_date - interval '4 years'
limit 100


create or replace function ads.get_intra_market_wholesale_prior_stock_number(_vii_id citext)
returns text as
$BODY$
/*
select * from ads.get_intra_market_wholesale_prior_stock_number('aad3d558-66dd-4414-82ab-8cb8e16cf46c')
*/
select stocknumber
FROM ads.ext_Vehicle_Inventory_Items
WHERE VehicleItemID = (
	SELECT VehicleItemID
	FROM ads.ext_Vehicle_Inventory_Items 
	WHERE VehicleInventoryItemID = _vii_id)
ORDER BY fromts DESC
limit 1 offset 1;
$BODY$
language sql;



select b.stocknumber as new_stk,
  (select * from ads.get_intra_market_wholesale_prior_stock_number(a.vehicleinventoryitemid)) as old_stk
from ads.ext_vehicle_inventory_item_statuses a
join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
where a.status = 'RawMaterials_BookingPending'
	and a.fromts::date > current_date - interval '4 years'  