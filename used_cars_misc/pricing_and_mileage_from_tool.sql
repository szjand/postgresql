﻿insert into sls.payroll_adjustment_reasons values
('RY1', 'Street Purchases'),
('RY2', 'Street Purchases');

select * 
from sls.payroll_adjustments
order by year_month desc 


update sls.payroll_adjustment_reasons
set reason = 'Street Purchase'
where reason = 'Street Purchases'

select * from sls.payroll_adjustment_reasons


 select inpmast_vin as vin, inpmast_stock_number as stock_number, model, "year" as model_year, make, color, odometer, list_price
  from arkona.xfm_inpmast a
  where a.inventory_account is not null
    and a.status = 'I'
    and a.type_n_u = 'U'
    and a.current_row = true
    and "year" between 2017 and 2020
    and model = 'Silverado 1500'


select * from ads.xfm_vehicle_pricings     limit 100


select a.stocknumber, b.vin, b.make, b.model, b.trim, b.yearmodel as model_year, b.exteriorcolor, d.amount as best_price
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
  and b.model = 'silverado 1500'
  and b.yearmodel::integer between 2017 and 2020
inner join (
  select vehicleinventoryitemid, vehiclepricingid, max(vehiclepricingts) as vehiclepricingts
  from ads.ext_vehicle_pricings
  group by vehicleinventoryitemid, vehiclepricingid) c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
left join ads.ext_vehicle_pricing_details d on c.vehiclepricingid = d.vehiclepricingid
  and d.typ = 'VehiclePricingDetail_BestPrice'
where a.thruts > now()
order by stocknumber

-- sent to afton 6/28/21
select a.stocknumber, b.vin, b.make, b.model, b.trim, b.yearmodel as model_year, b.exteriorcolor, e.value as miles, d.amount as best_price
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
  and b.model = 'silverado 1500'
  and b.yearmodel::integer between 2017 and 2020
left join (
	select vehicleinventoryitemid, vehiclepricingid
	from ads.ext_vehicle_pricings aa
	where vehiclepricingts = (
		select max(vehiclepricingts)
		from ads.ext_Vehicle_pricings
		where vehicleinventoryitemid = aa.vehicleinventoryitemid)) c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
left join ads.ext_vehicle_pricing_details d on c.vehiclepricingid = d.vehiclepricingid
  and d.typ = 'VehiclePricingDetail_BestPrice'
left join (
	select vehicleitemid, value
	from ads.ext_vehicle_item_mileages bb
	where vehicleitemmileagets = (
		select max(vehicleitemmileagets)
		from ads.ext_vehicle_item_mileages
		where vehicleitemid = bb.vehicleitemid)) e on b.vehicleitemid = e.vehicleitemid
where a.thruts > now()
order by stocknumber  




-- https://stackoverflow.com/questions/3800551/select-first-row-in-each-group-by-group/7630564#7630564
-- erwin's answer
-- not working as expected, returns multiple rows for G41775A
select distinct on (a.vehiclepricingid)
  c.stocknumber, a.vehicleinventoryitemid, b.amount, a.vehiclepricingid, a.vehiclepricingts
from ads.ext_vehicle_pricings a
join ads.ext_vehicle_pricing_details b on a.vehiclepricingid = b.vehiclepricingid
  and b.typ = 'VehiclePricingDetail_BestPrice'
join ads.ext_vehicle_inventory_items c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
  and c.thruts> now()
join ads.ext_vehicle_items d on c.vehicleitemid = d.vehicleitemid
  and d.model = 'silverado 1500'
  and d.yearmodel::integer between 2017 and 2020    
order by a.vehiclepricingid, vehiclepricingts desc 

with 
	pricings as (
		select a.vehicleinventoryitemid, b.amount as best_price,
			row_number() over (partition by a.vehicleinventoryitemid order by vehiclepricingts desc) as seq
		from ads.ext_vehicle_pricings a
		join ads.ext_vehicle_pricing_details b on a.vehiclepricingid = b.vehiclepricingid
			and b.typ = 'VehiclePricingDetail_BestPrice'
		join ads.ext_vehicle_inventory_items c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
			and c.thruts > now()
		join ads.ext_vehicle_items d on c.vehicleitemid = d.vehicleitemid
			and d.model = 'silverado 1500'
			and d.yearmodel::integer between 2017 and 2020),
	odo as (
		select a.vehicleitemid, a.value as miles,
			row_number() over (partition by a.vehicleitemid order by vehicleitemmileagets desc) as seq
		from ads.ext_vehicle_item_mileages a
		join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
			and b.model = 'silverado 1500'
			and b.yearmodel::integer between 2017 and 2020
		join ads.ext_vehicle_inventory_items c on b.vehicleitemid = c.vehicleitemid
			and c.thruts > now()) 			   
select a.stocknumber, b.vin, b.make, b.model, b.trim, b.yearmodel as model_year, b.exteriorcolor, d.best_price, e.miles
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
  and b.model = 'silverado 1500'
  and b.yearmodel::integer between 2017 and 2020
left join pricings d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
  and d.seq = 1
left join odo e on a.vehicleitemid = e.vehicleitemid
  and e.seq = 1  
where a.thruts > now()
order by stocknumber  			



		select a.vehicleinventoryitemid, b.amount as best_price,
			row_number() over (partition by a.vehicleinventoryitemid order by vehiclepricingts desc) as seq
select distinct on (a.vehiclepricingts::Date, c.stocknumber)		
  c.stocknumber, d.vin, b.amount, a.vehiclepricingts::Date
		from ads.ext_vehicle_pricings a
		join ads.ext_vehicle_pricing_details b on a.vehiclepricingid = b.vehiclepricingid
			and b.typ = 'VehiclePricingDetail_BestPrice'
		join ads.ext_vehicle_inventory_items c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
			and c.thruts > now()
		join ads.ext_vehicle_items d on c.vehicleitemid = d.vehicleitemid
			and d.model = 'silverado 1500'
			and d.yearmodel::integer between 2017 and 2020
order by a.vehiclepricingts::date, c.stocknumber desc 


G46555T,3FA6P0K90DR215469