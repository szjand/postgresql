﻿-- gross
-- 07/04/23  
-- update for June 23
-- monthly update of rec.uc_front_gross_detail
-- delete from cu.uc_front_gross_detail where year_month = 202306;
-- 202307, 202308, 202309, 202310,202311
insert into cu.uc_front_gross_detail (store,year_month,description,control,doc,amount,line,col,
  line_label,account,account_type,journal_code)
select 
	case e.store_code 
	  when 'RY1' then 'GM'::citext
	  when 'RY2' then 'HN'::citext
	  when 'RY8' then 'TY'::citext
	  else 'XXX'::citext
	end as store, b.year_month, d.description, a.control, a.doc, a.amount, e.line, e.col, e.line_label, e.account, e.account_type, journal_code
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 202312 --------------------------------------------------------------
join fin.dim_account c on a.account_key = c.account_key
  and c.current_row
join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key  
join cu.uc_front_gross_accounts e on c.account = e.account
join fin.dim_journal f on a.journal_key = f.journal_key
where a.post_status = 'Y';


-- just compare gross at store level fs vs accounting
select *
from (
	select store, sum(gross) as statement
	from cu.financial_statement_used_counts 
	where year_month = 202312 -----------------------------------------------
	group by store) a
full outer join (
	select store, sum(-amount)::integer as acct
	from cu.uc_front_gross_detail 
	where year_month = 202312 ------------------------------------------------
	group by store) b on a.store = b.store

	
-- these are the page 6 totals
select store, line, sum(-amount) from cu.uc_front_gross_detail where year_month = 202309 group by store, line

=========================================================================================================================================================
-- 7/4 toyota, query is twice the fs for lines 8 & 10, keep forgetting, dim_account is type 2, need current row
--     added current row to queries for cu.uc_front_gross_detail & cu.uc_front_gross_accounts


-- 5/26 fixed the adjustment issue, the adjustment entries needed to be added into wholesale only
--  original filter clauses were causing the adjustments to be included in retail
-- this compares the gross from fs with fact_fs and is good
-- 06/02/23 May looks good
-- 07/04/23 need to run reconcie_actual_fs_counts_with_step_1 to populate cu.used_count_detail before running this query
-- 10/04/23 202309 looks good
-- this compares all months
select *, a.gross - d.gross
from cu.financial_statement_used_counts   a
full outer join (
select 
  case c.store
    when 'RY1' then 'GM'
    when 'RY2' then 'HN'
    when 'RY8' then 'TY'
    else 'XXX'
  end as store, b.year_month, 'retail'::citext as sale_type,
  sum(-a.amount) filter (where line_label like '%retail%' and line_label not like 'new%') as gross -- accomodate line 11
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
where b.year_month between 201801 and 202309 -----------------------------------------------------------------
  and page = 16
  and line between 1 and 14
group by c.store, b.year_month
union 
select 
  case c.store
    when 'RY1' then 'GM'
    when 'RY2' then 'HN'
    when 'RY8' then 'TY'
    else 'XXX'
  end as store, b.year_month, 'wholesale'::citext as sale_type,
  sum(-a.amount) filter (where line_label like '%wholesale%' or line_label like 'new%') as gross -- accomodate line 11
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
where b.year_month between 201801 and 202309 ----------------------------------------------------------------
  and page = 16
  and line between 1 and 14
group by c.store, b.year_month) d on a.store = d.store
  and a.year_month = d.year_month
  and a.sale_type = d.sale_type
where abs(a.gross - d.gross) > 0  
order by a.store, a.year_month, a.sale_type




select * from cu.financial_Statement_used_counts where year_month = 202308
-- fix the data entry errors in rec.financial_statement_used_counts
update cu.financial_statement_used_counts
set gross = -24178
where store = 'HN'
  and year_month = 202306
  and sale_type = 'wholesale';


-- 5/27  --------------------------------------------------------------------------------------------
-- what i want is sales/cogs/gross by vehicle from fs


-- here are all the front end accounts for used
drop table if exists cu.uc_front_gross_accounts;
create table cu.uc_front_gross_Accounts as
select distinct g.store_code, d.gl_account as account, b.line, b.col, b.line_label, e.description, g.account_type
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.page = 16
  and b.line between 1 and 14
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
  and e.current_row
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
join fin.dim_Account g on d.gl_Account = g.account
order by g.store_code, b.line ;
comment on table cu.uc_front_gross_accounts is 'used car front gross accounts based on account routing from financial statements';



-----------------------------------------------------------------------------------------------
--< 06/02/23 update tables to cu schema
-----------------------------------------------------------------------------------------------
-- uc_front_gross_accounts --------------------------------
CREATE TABLE cu.uc_front_gross_accounts
(
  store_code citext,
  account citext,
  line numeric(4,1),
  col integer,
  line_label citext,
  description citext,
  account_type citext
);
COMMENT ON TABLE cu.uc_front_gross_accounts
  IS 'monthly accounting gross by stock number for used cars sales';

insert into cu.uc_front_gross_accounts
select * from rec.uc_front_gross_accounts;

select * from cu.uc_front_gross_accounts

drop table if exists rec.uc_front_gross_accounts;

-- uc_front_gross_detail ----------------------------------------
CREATE TABLE cu.uc_front_gross_detail
(
  store citext,
  year_month integer,
  description citext,
  control citext,
  doc citext,
  amount numeric(12,2),
  line numeric(4,1),
  col integer,
  line_label citext,
  account citext,
  account_type citext,
  journal_code citext
);
comment on table cu.uc_front_gross_detail is 'monthly accounting gross by stock number for used cars sales, at this level of detail there is no combination
  of attributes that is guaranteeably unique';

insert into cu.uc_front_gross_detail
select * from rec.uc_front_gross_detail;
-----------------------------------------------------------------------------------------------
--/> 06/02/23 update tables to cu schema
-----------------------------------------------------------------------------------------------
