﻿-- this picks up where i left off
-- chrome data has been input to cu.tmp_vehicles for vehicles that resolve with a style count = 1

------------------------------------------------------------------------------------------
--< the bad vins
------------------------------------------------------------------------------------------
-- 7 where vin <> 17
select distinct vin
from cu.used_3
where length(vin) <> 17

-- 2 that fail check sum
select distinct vin
from cu.used_3
where length(vin) = 17
  and not cra.vin_check_sum(vin)

-- conditionally successful
ZACCJBAH0FPB66556: in tool 2015 Jeep Renegade sport, same in chrome
ZFBCFYDT0GP336691: in tool 2016 Fiat 500 lounge, 2016 Fiat 500X lounge in chrome
select distinct a.vin, b.style_count, b.source, d->>'_value_1' as fail_reason
from cu.used_3 a
left join chr.build_data_describe_vehicle b on a.vin = b.vin
left join chr.describe_vehicle c on a.vin = c.vin
left join jsonb_array_elements(b.response->'responseStatus'->'status') d on true
where length(a.vin) <> 17
  or not cra.vin_check_sum(a.vin)  

select *
from chr.build_Data_describe_vehicle
where vin in ('ZACCJBAH0FPB66556','ZFBCFYDT0GP336691')  

-- so, of the 9 bad vins, 

------------------------------------------------------------------------------------------
--/> the bad vins
------------------------------------------------------------------------------------------

-- 2274 vins not yet in tmp_vehicles
-- this is up to 2535 after july statement
select distinct a.vin 
from cu.used_3 a
where not exists (
  select 1
  from cu.tmp_vehicles
  where vin = a.vin)

select distinct a.vin, b.style_count, b.source
from cu.used_3 a
join chr.build_data_describe_vehicle b on a.vin = b.vin
where not exists (
  select 1
  from cu.tmp_vehicles
  where vin = a.vin)  

-- 1G3NL52M9WM319554: though it resolves in build_data, very little info, no style, model_year, color, etc. 
-- it is a Oldsmobile Achieva SL
select * from chr.build_data_describe_vehicle
where vin = '1G3NL52M9WM319554'  



select distinct a.vin, b.style_count, d.model_code, c ->'attributes'->>'mfrModelCode' as chr_model_code,
  (c ->'attributes'->>'id')::citext as chrome_style_id,
  d.chrome_style_id as inpmast_chrome_style_id,
  d.key_to_cap_explosion_data as inpmast_chrome_style_id_2,
  (c->'attributes'->>'modelYear')::integer as model_year,
  (c->'division'->>'$value')::citext as make,
  (c->'subdivision'->>'$value')::citext as subdivision,
  (c->'model'->>'$value'::citext)::citext as model,
  coalesce(c->'attributes'->>'trim', 'none')::citext as trim_level,
  d.body_style,
  e.trim
from cu.used_3 a
join chr.describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'style') c on true
left join arkona.ext_inpmast d on a.vin = d.inpmast_vin
left join ads.ext_vehicle_items e on a.vin = e.vin
where not exists (
  select 1
  from cu.tmp_vehicles
  where vin = a.vin)  
  and d.model_code  = (c ->'attributes'->>'mfrModelCode')::citext
  and e.trim = coalesce(c->'attributes'->>'trim', 'none')::citext
  and ((c ->'attributes'->>'id')::bigint = d.chrome_style_id or (c ->'attributes'->>'id')::citext = d.key_to_cap_explosion_data)
  and c->'division'->>'$value' <> 'Ford'
order by a.vin

-- the problem with this one, the inpmast body_style is cut off due to attribute size limit,
-- has 4WD CREW CAB 143.5" LT W/ should be 4WD CREW CAB 143.5" LT W/ plus 1LT or 2LT
-- but it has the chrome style id in key_to_cap_explosion_data
select * from arkona.ext_inpmast where inpmast_vin = '3GCUKREC5GG283777'


-- select vin from (
select distinct a.vin, b.style_count, d.model_code, c ->'attributes'->>'mfrModelCode' as chr_model_code,
  (c ->'attributes'->>'id')::citext as chrome_style_id,
  d.chrome_style_id as inpmast_chrome_style_id,
  d.key_to_cap_explosion_data as inpmast_chrome_style_id_2,
  (c->'attributes'->>'modelYear')::integer as model_year,
  (c->'division'->>'$value')::citext as make,
  (c->'subdivision'->>'$value')::citext as subdivision,
  (c->'model'->>'$value'::citext)::citext as model,
  coalesce(c->'attributes'->>'trim', 'none')::citext as trim_level,
  d.body_style,
  e.trim
-- select distinct a.vin,  (c ->'attributes'->>'id')::bigint
from cu.used_3 a
join chr.describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'style') c on true
left join arkona.ext_inpmast d on a.vin = d.inpmast_vin
left join ads.ext_vehicle_items e on a.vin = e.vin
where not exists (
  select 1
  from cu.tmp_vehicles
  where vin = a.vin)  
--   and d.model_code  = (c ->'attributes'->>'mfrModelCode')::citext
--   and e.trim = coalesce(c->'attributes'->>'trim', 'none')::citext
  and ((c ->'attributes'->>'id')::bigint = d.chrome_style_id or (c ->'attributes'->>'id')::citext = d.key_to_cap_explosion_data)
  and c->'division'->>'$value' <> 'Ford'
  and d.chrome_style_id <> 0 -- gets rid of the dups
-- ) x  group by vin having count(*) > 1
order by a.vin

-- still returns some dup vins
2HKRM4H74FH658789
2HKRW2H89HH669085
5FNRL6H74KB087196
5TDJZRFH2JS823636
5TDJZRFH8HS359293
JTEBU5JR9C5096074
NMTKHMBX9JR006864


-- this gives me an additional 245 vehicles based on matching the chrome_style_id 
-- with data in inpmast (chrome_style_id, key_to_cap_explosion_data
select a.vin,  (c ->'attributes'->>'id')::bigint as style_id
from cu.used_3 a
join chr.describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'style') c on true
left join arkona.ext_inpmast d on a.vin = d.inpmast_vin
where not exists (
  select 1
  from cu.tmp_vehicles
  where vin = a.vin)  
  and ((c ->'attributes'->>'id')::bigint = d.chrome_style_id or (c ->'attributes'->>'id')::citext = d.key_to_cap_explosion_data)
  and c->'division'->>'$value' <> 'Ford'
  and d.chrome_style_id <> 0 -- gets rid of the dups
group by a.vin,  (c ->'attributes'->>'id')::bigint

-- actually, this looks like there are more than a few that should have build data coverage, GM, Dodge, Nissan
-- for the Nissans, looks like there is build data, but style count > 1, BUT looking at the comparison
-- betweeen the data available from build vs non build, there is more data (ext color, trans, msrp, interior)  in build
-- which i can narrow down to a single chrome style id using inpmast
-- so, first do this against build data (excluding Ford)
-- 08/12/23 the build data is already done, these 232 can be inserted into cu.tmp_vehicles
insert into cu.tmp_vehicles
select a.vin, 
  (c->'attributes'->>'modelYear')::integer as model_year,
  (c->'division'->>'$value')::citext as make,
  (c->'subdivision'->>'$value')::citext as subdivision,
  (c->'model'->>'$value'::citext)::citext as model,
  coalesce(c->'attributes'->>'trim', 'none')::citext as trim,
  case
    when c->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
    when c->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
    when c->'attributes'->>'drivetrain' like 'Four%' then '4WD'
    when c->'attributes'->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drive,
  (c->'attributes'->>'fleetOnly'::citext)::boolean as fleet_only,
  (c->'attributes'->>'modelFleet'::citext)::boolean as model_fleet,  
  (c->'attributes'->>'mfrModelCode')::citext as chr_model_code,  
  case
    when d.cab->>'$value' like 'Crew%' then 'crew' 
    when d.cab->>'$value' like 'Extended%' then 'double'  
    when d.cab->>'$value' like 'Regular%' then 'reg'  
    else 'n/a'   
  end as cab,    
  case
    when e.bed->>'$value' like '%Bed' then substring(e.bed->>'$value', 1, position(' ' in e.bed->>'$value') - 1)
    else 'n/a'
  end as box_size,      
  coalesce(g.displacement->>'$value', 'n/a') as engine_displacement,
  f->>'cylinders' as cylinders, f->'fuelType'->>'$value' as fuel,
  null as transmission, null as ext_color, null as interior,
  c->'attributes'->>'passDoors' as pass_doors,
  null as msrp,
  c->'marketClass'->>'$value' as market_class,
  b.response->'vinDescription'->'attributes'->>'bodyType' as body_type,
  c->'attributes'->>'altBodyType' as alt_body_type,
  b.response->'vinDescription'->'attributes'->>'styleName' as style_name,
  null as alt_style_name, 
  c->'attributes'->>'nameWoTrim' as name_wo_trim,
  b.response->'attributes'->>'bestMakeName' as best_make_name,
  b.response->'attributes'->>'bestModelName' as best_model_name,
  b.response->'attributes'->>'bestTrimName' as best_trim_name,
  b.response->'attributes'->>'bestStyleName' as best_style_name,
  c->'stockImage'->'attributes'->>'url' as pic_url,
  'Catalog' as source, (c ->'attributes'->>'id')::citext as chrome_style_id
-- select a.vin 
from ( -- select '2HKRW2H54NH626260'::citext as vin) a
	select a.vin,  (c ->'attributes'->>'id')::citext as style_id
	from cu.used_3 a
	join chr.describe_vehicle b on a.vin = b.vin
	join jsonb_array_elements(b.response->'style') c on true
	left join arkona.ext_inpmast d on a.vin = d.inpmast_vin
	where not exists (
		select 1
		from cu.tmp_vehicles
		where vin = a.vin)  
		and ((c ->'attributes'->>'id')::bigint = d.chrome_style_id or (c ->'attributes'->>'id')::citext = d.key_to_cap_explosion_data)
		and c->'division'->>'$value' <> 'Ford'
		and d.chrome_style_id <> 0 -- gets rid of the dups
	group by a.vin,  (c ->'attributes'->>'id')::citext) a
join chr.describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'style') c on true
left join jsonb_array_elements(c->'bodyType') with ordinality as d(cab) on true
  and d.ordinality =  2
left join jsonb_array_elements(c->'bodyType') with ordinality as e(bed) on true
  and e.ordinality =  1    
left join jsonb_array_elements(b.response->'engine') as f(engine) on true  
  and f.engine ? 'installed'
left join jsonb_array_elements(f.engine->'displacement'->'value') as g(displacement) on true
  and g.displacement->'attributes'->>'unit' = 'liters'  
where a.style_id = (c ->'attributes'->>'id')::citext
order by c->'division'->>'$value', c->'model'->>'$value', a.vin
-- order by a.vin


-- the build data version (excluding Ford)
-- gives me 46 vehicles, 21 of which are actually build data, but even when source is catalog, returns more data than non build
select a.vin, 
  (c ->>'modelYear')::integer as model_year,
  (c ->'division'->>'_value_1')::citext as make,
  (c ->'subdivision'->>'_value_1')::citext as subdivision,
  (c ->'model'->>'_value_1'::citext)::citext as model,
  coalesce(c ->>'trim', 'none')::citext as trim,
  case
    when c ->>'drivetrain' like 'Rear%' then 'RWD'
    when c->>'drivetrain' like 'Front%' then 'FWD'
    when c ->>'drivetrain' like 'Four%' then '4WD'
    when c ->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drive,  
  (c ->>'fleetOnly'::citext)::boolean as fleet_only,
  (c ->>'modelFleet'::citext)::boolean as model_fleet,
  (c ->>'mfrModelCode')::citext as model_code,
--   b.response->'vinDescription'->>'drivingWheels' as driving_wheels, -- adds nothing interesting, often null
  case
    when d.cab->>'_value_1' like 'Crew%' then 'crew' 
    when d.cab->>'_value_1' like 'Extended%' then 'double'  
    when d.cab->>'_value_1' like 'Regular%' then 'reg'  
    else 'n/a'   
  end as cab,
  case
    when e.bed->>'_value_1' like '%Bed' then substring(bed->>'_value_1', 1, position(' ' in bed->>'_value_1') - 1)
    else 'n/a'
  end as box_size,  
  case
    when (c ->'model'->>'_value_1'::citext)::citext in ('bolt', 'bolt ev') then 'n/a'
    else g->>'_value_1' 
  end as engine_displacement,
  f->>'cylinders' as cylinders, f->'fuelType'->>'_value_1' as fuel,
  case
    when  (c ->'division'->>'_value_1')::citext = 'Nissan' then 'automatic'
    when i->>0 is null then 'unknown'
    when i->>0 like '%SPEED MANUAL%' then 'manual'
    else 'automatic'
  end as transmission,  
  j->>'colorName' as ext_color, k->>'colorName' as interior,
  c->>'passDoors' as pass_doors,
  ((coalesce(b.response->'vinDescription'->>'builtMSRP', c->'basePrice'->>'msrp'))::numeric)::integer as msrp,
--   m->>'_value_1' as market_class,
  c->'marketClass'->>'_value_1' as market_class,
  b.response->'vinDescription'->>'bodyType' as body_type, c->>'altBodyType' as alt_body_type, 
  b.response->'vinDescription'->>'styleName' as style_name, c->>'altStyleName' as alt_style_name,
  (c->>'nameWoTrim'::citext)::citext as name_wo_trim,
  b.response->>'bestMakeName' as best_make_name,
  b.response->>'bestModelName' as best_model_name,
  b.response->>'bestTrimName' as best_trim_name,
  b.response->>'bestStyleName' as best_style_name,
  c->'stockImage'->>'url' as pic_url,
  b.source, (c ->>'id')::citext as chrome_style_id   , b.the_date, b.style_count
-- select a.vin  
-- select ((coalesce(b.response->'vinDescription'->>'builtMSRP', c->'basePrice'->>'msrp'))::numeric)::integer as msrp
from ( --select '1GB4KZC8XDF220206'::citext as vin) a
	select a.vin,  (c ->>'id')::citext as style_id
	from cu.used_3 a
	join chr.build_data_describe_vehicle b on a.vin = b.vin
	join jsonb_array_elements(b.response->'style') c on true
	left join arkona.ext_inpmast d on a.vin = d.inpmast_vin
	where not exists (
		select 1
		from cu.tmp_vehicles
		where vin = a.vin)  
		and ((c ->>'id')::bigint = d.chrome_style_id or (c ->>'id')::citext = d.key_to_cap_explosion_data)
		and c ->'division'->>'_value_1' <> 'Ford'
		and d.chrome_style_id <> 0 -- gets rid of the dups
	group by a.vin,  (c ->>'id')::citext) a
join chr.build_data_describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'style') c on true
left join jsonb_array_elements(c->'bodyType') with ordinality as d(cab) on true
  and d.ordinality =  2
left join jsonb_array_elements(c->'bodyType') with ordinality as e(bed) on true
  and e.ordinality =  1    
left join jsonb_array_elements(b.response->'engine') as f on true  
  and (f->'installed'->>'cause' = 'OptionCodeBuild' or coalesce(f->'installed'->>'cause', 'xxx') = 'VIN')
left join jsonb_array_elements(f->'displacement'->'value') as g on true
  and g ->>'unit' = 'liters'
left join jsonb_array_elements(b.response->'factoryOption') h on true
  and h->'header'->>'_value_1' = 'TRANSMISSION'
  and h->'installed'->>'cause' in ('VIN','OptionCodeBuild')
left join jsonb_array_elements(h->'description') i on true
left join jsonb_array_elements(b.response->'exteriorColor') as j on true
  and j->'installed'->>'cause' in ('RelatedColor','ExteriorColorBuild', 'OptionCodeBuild','InteriorColorBuild')    
left join jsonb_array_elements(b.response->'interiorColor') as k on true --interior
  and k->'installed'->>'cause' in ('RelatedColor','ExteriorColorBuild', 'OptionCodeBuild','InteriorColorBuild')
left join jsonb_array_elements(k->'description') as l on true
-- left join jsonb_array_elements(b.response->'vinDescription'->'marketClass') m on true 
-- where b.source = 'build'  
where a.style_id = (c ->>'id')::citext
order by b.source --c->'division'->>'_value_1', c->'model'->>'_value_1', a.vin

select count(*) from cu.tmp_vehicles  --15920

select a.vin  --2130
from cu.used_3 a
where not exists (
  select 1
  from cu.tmp_vehicles
  where vin = a.vin)