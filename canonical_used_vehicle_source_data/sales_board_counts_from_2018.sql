﻿select year_month, store, sale_type, sum(unit_count) 
from (
    select distinct
    case when store_key in (39,41) then 'GM' when store_key = 40 then 'Honda/Nissan'
    when store_key = 42 then 'Toyota' end as store,
    case when board_sub_type like '%wholesale%' 
        or board_sub_type in ('Coaches','Drivers Training') then 'Wholesale' else 'Retail' end as sale_type,
    a.stock_number, c.board_type, board_sub_type, is_backed_on, boarded_date, 
    case when board_type = 'Deal' then 1 else -1 end as unit_count, year_month
    -- a.stock_number, c.board_type, board_sub_type, is_backed_on, boarded_date, is_backed_on,
    -- vehicle_model_year, vehicle_make, 
    -- vehicle_model, vehicle_color, days_in_inventory, used_vehicle_package, vin
    from board.sales_board a
    inner join board.daily_board b on a.board_id = b.board_id and vehicle_type = 'U'
    inner join board.board_types c on a.board_type_key = c.board_type_key and board_type in ('Deal', 'Back-on')
    inner join dds.dim_date d on a.boarded_date = d.the_date
    where boarded_date >= '06/01/2018'
        and is_deleted = false
        and store_key in (39,41,40, 42) 
) A
group by year_month, store, sale_type
order by store, year_month, sale_type




    select distinct
			case 
				when store_key in (39,41) then 'RY1' 
				when store_key = 40 then 'RY2'
				when store_key = 42 then 'RY8' 
			end as store,
			e.record_key as bopmast_id, a.stock_number, boarded_date as delivery_date, bopmast_search_name as customer
    from board.sales_board a
    join board.daily_board b on a.board_id = b.board_id 
			and vehicle_type = 'U'
    join board.board_types c on a.board_type_key = c.board_type_key 
      and c.board_type ='Deal'
      and c.board_sub_type = 'Retail'
    join dds.dim_date d on a.boarded_date = d.the_date
    join arkona.ext_bopmast e on a.stock_number = e.bopmast_stock_number
    where boarded_date >= '01/01/2024'
        and is_deleted = false
        and store_key in (39,41,40, 42) 
















