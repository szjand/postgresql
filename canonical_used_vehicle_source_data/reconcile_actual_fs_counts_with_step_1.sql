﻿-- -- this modification of step_1, adding year_month, allows creating data for multiple months at a time
-- -- drop table if exists step_1;
-- -- create temp table step_1 as
-- -- 5/31/23 change store from ry1,ry2,ry8 to gm,hn,ty
-- drop table if exists cu.used_count_detail;
-- create table if not exists cu.used_count_detail as
-- each  month, insert the data for the month just passed
-- 07/04/23 202306 entered
-- 08/03/23 202307 entered
-- 09/02/23 202308 entered
-- 10/04/23 202309 entered
-- 11/04/23 202310 entered
insert into cu.used_count_detail (year_month, store, line, line_label, control, unit_count)
select year_month, 
  case store
		when 'RY1' then 'GM'::citext
		when 'RY2' then 'HN'::citext
		when 'RY8' then 'TY'::citext
		else 'XXX'::citext
  end as store, line, line_label, control, unit_count 
from (
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 202312 -- between 201801 and 202304 -----------------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description, limits transactions to the relevant sale accounts
    select b.year_month, f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 202312 -- between 201801 and 202304 ---------------------------------------------------------------------
      and b.page = 16 and b.line between 1 and 14 -- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4' -- sale accounts only, does not factor in cogs
      and e.current_row
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account and b.year_month = d.year_month
  where a.post_status = 'Y') h;
-- comment on table cu.used_count_detail is 'stock number level detail on monthly unit counts derived from accounting data';

select store, year_month, count(*) from cu.used_count_detail group by store, year_month order by store, year_month

-- need to convert one or the other to the same format as the other
-- and no differences, cu.financial_statement_used_count_detail matches counts from cu.financial_statement_used_counts
select *
from cu.financial_statement_used_counts   a
full outer join (
	select store, year_month, 'retail'::citext as sale_type,
		sum(unit_count) filter (where line_label like '%retail%') as the_count
	from (
		select *
		from cu.used_count_detail) a
	group by store, year_month
	union all
	select store, year_month, 'wholesale'::citext as sale_type,
		sum(unit_count) filter (where line_label like '%wholesale%') as the_count
	from (
		select *
		from cu.used_count_detail) a
	group by store, year_month) c  on a.store = c.store 
		and a.year_month = c.year_month 
		and a.sale_type = c.sale_type
-- where a.the_count <> c.the_count
where a.year_month = 202312 --------------------------------------------------------------------
order by a.store, a.year_month, a.sale_type	


===========================================================================================================================================================


----------------------------------------------------------------------------------------------------------------
--< WHY FS COUNTS ARE NOT THE NUMBER OF VEHICLES ACTUALLY SOLD ----------------------------------------------
----------------------------------------------------------------------------------------------------------------
-- the FS count for ry1 201906 is 289
-- this generates 296 rows for ry1
select store, control
from cu.used_count_detail
where year_month = 201906 and store = 'GM'

-- so, 296 - 7 = 289 (-2 for each -1 and -1 for the 0)
G36539GA,-1
G37016B, -1
G37123G, -1
G36391A,  0

select *
from cu.used_count_detail
where year_month = 201906 and store = 'GM'
  and control in ('G36539GA','G37016B','G37123G','G36391A')
order by control

201906,ry1,5.0,USED TRKS   RETAIL OTHER,G36391A,	 0
201906,ry1,5.0,USED TRKS   RETAIL OTHER,G36539GA,	-1
201906,ry1,5.0,USED TRKS   RETAIL OTHER,G37016B,	-1
201906,ry1,2.0,USED CARS   RETAIL OTHER,G37123G,	-1
201906,ry1,8.0,USED CARS   WHOLESALE,		G37123G,	 1


-- this returns 291 rows  
-- because it excludes all 5 of the above rows, so 296 - 5 = 291
select store, control
from cu.used_count_detail
where year_month = 201906 and store = 'GM'
group by store, control
having sum(unit_count) > 0
order by control

-- and this returns a count of 289
select store, year_month, sum(unit_count) as the_count
from (
	select *
	from cu.used_count_detail
	where store = 'GM' and year_month = 201906) a
group by store, year_month

THEREFORE
the count is not a reliable representation of the vehicles sold,eg, 37123G backed on from a previous month,
but was actually subsequently wholesaled in the month of June and should count as a sale.  

----------------------------------------------------------------------------------------------------------------
--/> WHY FS COUNTS ARE NOT THE NUMBER OF VEHICLES ACTUALLY SOLD ----------------------------------------------
----------------------------------------------------------------------------------------------------------------

select store, count(*)
from cu.used_count_detail
group by store

