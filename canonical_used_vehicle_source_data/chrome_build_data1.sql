﻿
-- select vin from (
-- 6/28 added (b.response->'vinDescription'->>'bodyType' & styleName
-- added passDoors, subdivision, modelFleet, fleetOnly, nameWoTrim, stockImage
-- add all the "best"
-- select  distinct fleet_only, model_fleet from (
-- changed marketClass to come from style, vinDescription lists 2 values for marketClass for vin 1GB4KZC8XDF220206
insert into cu.tmp_vehicles
select a.vin, 
  (c ->>'modelYear')::integer as model_year,
  (c ->'division'->>'_value_1')::citext as make,
  (c ->'subdivision'->>'_value_1')::citext as subdivision,
  (c ->'model'->>'_value_1'::citext)::citext as model,
  coalesce(c ->>'trim', 'none')::citext as trim,
  case
    when c ->>'drivetrain' like 'Rear%' then 'RWD'
    when c->>'drivetrain' like 'Front%' then 'FWD'
    when c ->>'drivetrain' like 'Four%' then '4WD'
    when c ->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drive,  
  (c ->>'fleetOnly'::citext)::boolean as fleet_only,
  (c ->>'modelFleet'::citext)::boolean as model_fleet,
  (c ->>'mfrModelCode')::citext as model_code,
--   b.response->'vinDescription'->>'drivingWheels' as driving_wheels, -- adds nothing interesting, often null
  case
    when d.cab->>'_value_1' like 'Crew%' then 'crew' 
    when d.cab->>'_value_1' like 'Extended%' then 'double'  
    when d.cab->>'_value_1' like 'Regular%' then 'reg'  
    else 'n/a'   
  end as cab,
  case
    when e.bed->>'_value_1' like '%Bed' then substring(bed->>'_value_1', 1, position(' ' in bed->>'_value_1') - 1)
    else 'n/a'
  end as box_size,  
  case
    when (c ->'model'->>'_value_1'::citext)::citext in ('bolt', 'bolt ev') then 'n/a'
    else g->>'_value_1' 
  end as engine_displacement,
  f->>'cylinders' as cylinders, f->'fuelType'->>'_value_1' as fuel,
  case
    when  (c ->'division'->>'_value_1')::citext = 'Nissan' then 'automatic'
    when i->>0 is null then 'unknown'
    when i->>0 like '%SPEED MANUAL%' then 'manual'
    else 'automatic'
  end as transmission,  
  j->>'colorName' as ext_color, k->>'colorName' as interior,
  c->>'passDoors' as pass_doors,
  ((coalesce(b.response->'vinDescription'->>'builtMSRP', c->'basePrice'->>'msrp'))::numeric)::integer as msrp,
--   m->>'_value_1' as market_class,
  c->'marketClass'->>'_value_1' as market_class,
  b.response->'vinDescription'->>'bodyType' as body_type, c->>'altBodyType' as alt_body_type, 
  b.response->'vinDescription'->>'styleName' as style_name, c->>'altStyleName' as alt_style_name,
  (c->>'nameWoTrim'::citext)::citext as name_wo_trim,
  b.response->>'bestMakeName' as best_make_name,
  b.response->>'bestModelName' as best_model_name,
  b.response->>'bestTrimName' as best_trim_name,
  b.response->>'bestStyleName' as best_style_name,
  c->'stockImage'->>'url' as pic_url,
  b.source, (c ->>'id')::citext as chrome_style_id   
-- select a.vin  
-- select ((coalesce(b.response->'vinDescription'->>'builtMSRP', c->'basePrice'->>'msrp'))::numeric)::integer as msrp
from ( --select '1GB4KZC8XDF220206'::citext as vin) a
	select distinct vin
	from cu.used_2) a
join chr.build_data_describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'style') c on true
left join jsonb_array_elements(c->'bodyType') with ordinality as d(cab) on true
  and d.ordinality =  2
left join jsonb_array_elements(c->'bodyType') with ordinality as e(bed) on true
  and e.ordinality =  1    
left join jsonb_array_elements(b.response->'engine') as f on true  
  and (f->'installed'->>'cause' = 'OptionCodeBuild' or coalesce(f->'installed'->>'cause', 'xxx') = 'VIN')
left join jsonb_array_elements(f->'displacement'->'value') as g on true
  and g ->>'unit' = 'liters'
left join jsonb_array_elements(b.response->'factoryOption') h on true
  and h->'header'->>'_value_1' = 'TRANSMISSION'
  and h->'installed'->>'cause' in ('VIN','OptionCodeBuild')
left join jsonb_array_elements(h->'description') i on true
left join jsonb_array_elements(b.response->'exteriorColor') as j on true
  and j->'installed'->>'cause' in ('RelatedColor','ExteriorColorBuild', 'OptionCodeBuild','InteriorColorBuild')    
left join jsonb_array_elements(b.response->'interiorColor') as k on true --interior
  and k->'installed'->>'cause' in ('RelatedColor','ExteriorColorBuild', 'OptionCodeBuild','InteriorColorBuild')
left join jsonb_array_elements(k->'description') as l on true
-- left join jsonb_array_elements(b.response->'vinDescription'->'marketClass') m on true 
where b.source = 'build'  
  and style_count = 1
-- ) x
order by c ->'model'->>'_value_1', c ->>'modelYear'


select * from chr.build_data_describe_vehicle where vin = '1GCVKSEC6GZ112760'

-- bunch of hyundais with unknown trans
select * from chr.build_Data_describe_vehicle where vin = '1GB4KZC8XDF220206'
-- 2 options under standard, both with installed = BaseEquipment
-- Transmission: 6-Speed Automatic w/SHIFTRONIC & Transmission w/Driver Selectable Mode

select source, count(*)
from chr.build_data_describe_vehicle
group by source


drop table if exists cu.inpmast_year_make_vin_stock;
create table cu.inpmast_year_make_vin_stock as
select year, make, inpmast_stock_number, inpmast_vin
from arkona.xfm_inpmast
where inpmast_stock_number is not null
  and length(inpmast_stock_number) > 4
  and length(inpmast_vin) = 17
group by year, make, inpmast_stock_number, inpmast_vin;
-- alter table cu.inpmast_year_make_vin_stock add primary key (inpmast_vin,inpmast_stock_number);
comment on table cu.inpmast_year_make_vin_stock is 'unique combination of year, make, vin, stock_number from arkona.xfm_inpmast, used for
  verifying which vins are eligible for build data based on make and model year (for joining to chr.build_data_coverage, this will need
  to be updated at least daily';
create index on cu.inpmast_year_make_vin_stock (year);
create index on cu.inpmast_year_make_vin_stock (make);
create index on cu.inpmast_year_make_vin_stock (inpmast_stock_number);
create index on cu.inpmast_year_make_vin_stock (inpmast_vin);

select * from cu.inpmast_year_make_vin_stock


06/24/23  ran chrome on these 1020
-- eligible for build data, but don't have build data yet
-- an additional 2394  prior to checking for eligiblity
-- an additional 1020 vins
select distinct b.inpmast_vin as vin
from cu.used_1 a
join cu.inpmast_year_make_vin_stock b on a.stock_number = b.inpmast_stock_number
join chr.build_data_coverage c on b.make = c.division
  and b.year between c.from_year and c.thru_year
where not exists (
  select 1
  from chr.build_data_describe_vehicle
  where vin = b.inpmast_vin
    and source = 'build'
    and style_count = 1)

select source, count(*) from chr.build_data_describe_vehicle where the_date = '06/24/2023' group by source
      ,2
Sparse,3
Catalog,72
Build,998

06/25/23 get rid of non build data

delete 
from chr.build_data_describe_vehicle
where source <> 'build'

-- 06/27/23 ----------------------------------------------------------------------------------------------------------------------------
updated cu.inpmast_year_make_vin_stock

thinking i might have fucked up by deleting non source = build data from chr.build_data_describe_vehicle
dont know yet

-- this returns 302 vins that should be eligible for build data, what bothers me, this is what i ran on 6/24
-- so why now is it returning so many vins ????
-- run these then see what i have
drop table if exists pot_build_vins;
create temp table pot_build_vins as
select distinct b.inpmast_vin as vin
from cu.used_1 a
join cu.inpmast_year_make_vin_stock b on a.stock_number = b.inpmast_stock_number
join chr.build_data_coverage c on b.make = c.division
  and b.year between c.from_year and c.thru_year
where not exists (
  select 1
  from chr.build_data_describe_vehicle
  where vin = b.inpmast_vin
    and source = 'build'
    and style_count = 1)

yep, that is what i was afraid of, they are mostly all fucking catalog or sparse source data

Build,1
Catalog,203
Sparse,4

select source, count(*) from (
select a.vin, a.source, a.style_count, a.the_date
from chr.build_data_describe_vehicle a
join pot_build_vins b on a.vin = b.vin                                                                           
where the_date = current_date
) x group by source

havent thought it through yet, but thinking i dont need to delete anything, just filter on source
--7/6/23
-- so, i am thinking i should be able to generate a query to run describe_vehicle on vins that dont exists in build_data with source = build

ALSO
CHECK ON THE DATA DIFFERENCES (KEYS AS WELL AS VALUES) BETWEEN BUILD DATA WITH A SOURCE OF CATALOG AND DESCRIBE VEHICLE

-- 160 in build_data_describe_vehicle with a source of catalog that also exist in describe_vehicle --------------------------------------
select a.vin, a.source, a.style_count, a.the_date
from chr.build_data_describe_vehicle a
join pot_build_vins b on a.vin = b.vin                                                                           
where the_date = current_date
  and source = 'catalog'
  and exists (
    select 1
    from chr.describe_vehicle
    where vin = a.vin)

-- it is as i suspected, catalog data in build_data is laid out differently than it is in describe_vehicle
-- and the keys are different, eg build: "_value_1": "Sierra 1500"  describe: "$value": "Sierra 1500",
select * from chr.build_data_describe_vehicle where vin = '3GTU2UEC4FG161297'    
select * from chr.describe_vehicle where vin = '3GTU2UEC4FG161297'  
----------------------------------------------------------------------------------------------------------------------------------------


-- describe vehicle, about 2/3 have catalog as source under vinDescription->attributes, -------------------------
-- it appears that source is not even a field in most of the rest
select coalesce(response->'vinDescription'->'attributes'->>'source', 'NULL'), count(*)
from chr.describe_vehicle
group by coalesce(response->'vinDescription'->'attributes'->>'source', 'NULL')
where vin = '3GTU2UEC4FG161297'

NULL,14279
Catalog,23875
Sparse,28

select *
from chr.describe_vehicle 
where response->'vinDescription'->'attributes'->>'source' is null
limit 10
----------------------------------------------------------------------------------------------------------------

select * from chr.build_data_describe_vehicle where vin = '3GNTKGE72DG156443'







