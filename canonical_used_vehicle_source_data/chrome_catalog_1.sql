﻿-- order this like the buid data query

select a.vin, 
  (c->'attributes'->>'modelYear')::integer as model_year,
  (c->'division'->>'$value')::citext as make,
  (c->'subdivision'->>'$value')::citext as subdivision,
  (c->'model'->>'$value'::citext)::citext as model,
  coalesce(c->'attributes'->>'trim', 'none')::citext as trim,
  case
    when c->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
    when c->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
    when c->'attributes'->>'drivetrain' like 'Four%' then '4WD'
    when c->'attributes'->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drive,
  (c->'attributes'->>'fleetOnly'::citext)::citext as fleet_only,
  (c->'attributes'->>'modelFleet'::citext)::citext as model_fleet,  
  (c->'attributes'->>'mfrModelCode')::citext as chr_model_code,  
  case
    when d.cab->>'$value' like 'Crew%' then 'crew' 
    when d.cab->>'$value' like 'Extended%' then 'double'  
    when d.cab->>'$value' like 'Regular%' then 'reg'  
    else 'n/a'   
  end as cab,    
  case
    when e.bed->>'$value' like '%Bed' then substring(e.bed->>'$value', 1, position(' ' in e.bed->>'$value') - 1)
    else 'n/a'
  end as box_size,      
  coalesce(g.displacement->>'$value', 'n/a') as engine_displacement,
  f->>'cylinders' as cylinders, f->'fuelType'->>'$value' as fuel,
  null as transmission, null as ext_color, null as interior,
  c->'attributes'->>'passDoors' as pass_doors,
  null as msrp,
  c->'marketClass'->>'$value' as market_class,
  b.response->'vinDescription'->'attributes'->>'bodyType' as body_type,
  c->'attributes'->>'altBodyType' as alt_body_type,
  b.response->'vinDescription'->'attributes'->>'styleName' as style_name,
  null as alt_style_name, 
  c->'attributes'->>'nameWoTrim' as name_wo_trim,
  b.response->'attributes'->>'bestMakeName' as best_make_name,
  b.response->'attributes'->>'bestModelName' as best_model_name,
  b.response->'attributes'->>'bestTrimName' as best_trim_name,
  b.response->'attributes'->>'bestStyleName' as best_style_name,
  c->'stockImage'->'attributes'->>'url' as pic_url,
  'Catalog' as source, (c ->'attributes'->>'id')::citext as chrome_style_id    
-- select a.vin 
from ( -- select '2HKRW2H54NH626260'::citext as vin) a
	select distinct a.vin-- 2322
	from cu.used_2 a
	where not exists (
		select 1
		from chr.build_Data_describe_vehicle
		where vin = a.vin
		  and source = 'build'
		  and style_count = 1)) a
join chr.describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'style') c on true
left join jsonb_array_elements(c->'bodyType') with ordinality as d(cab) on true
  and d.ordinality =  2
left join jsonb_array_elements(c->'bodyType') with ordinality as e(bed) on true
  and e.ordinality =  1    
left join jsonb_array_elements(b.response->'engine') as f(engine) on true  
  and f.engine ? 'installed'
left join jsonb_array_elements(f.engine->'displacement'->'value') as g(displacement) on true
  and g.displacement ->'attributes'->>'unit' = 'liters'  
where b.style_count = 1  
order by a.vin

----------------------------------------------------------------------------------------------------------------------------
i was nervous about engine, is the presence of 'installed' really enough
ran 1GCHC39R5TE230038, has 3 engine options, only one has the 'installed' attribute:
      "installed": {
        "attributes": {
          "cause": "VIN"
        }
      },
so, ok, guess i did due diligence way back when
----------------------------------------------------------------------------------------------------------------------------   
under vinDescription->attributes i see bodyType, dont recall seeing that in build data, in the chrome_build_data1. query
there is no bodyType attribute,  BUT, there is vinDescription->bodyType in json, add it to query, right before alt_body_type
----------------------------------------------------------------------------------------------------------------------------   

select a.*, b.*, c.style_count, d.model_code
select distinct b.inpmast_vin
from cu.used_1 a
join cu.inpmast_year_make_vin_stock b on a.stock_number = b.inpmast_stock_number
join chr.describe_vehicle c on b.inpmast_vin = c.vin
left join arkona.ext_inpmast d on c.vin = d.inpmast_vin
where not exists (
  select 1
  from chr.build_Data_describe_vehicle
  where vin = b.inpmast_vin)
and c.style_count = 1  


select * from chr.describe_vehicle where vin = '1GCHC39R5TE230038'

select * from chr.describe_vehicle where vin = '2HKRW2H54NH626260'