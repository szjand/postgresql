﻿-- 08/12/23  
-- continue with getting all vins from cu.used_3 into cu.tmp_vehicles
select *  -- 2130
from cu.used_3 a
where not exists (
  select 1
  from cu.tmp_vehicles
  where vin = a.vin)
------------------------------------------------------------------------------------------------------------------
--< exclude ford, look for match on model code
------------------------------------------------------------------------------------------------------------------
select a.vin, d.make, b.source, b.style_count as build_style_ct, c.style_count, 
  d.model_code, (cc->'attributes'->>'mfrModelCode')::citext as chr_model_code, (bb ->>'mfrModelCode')::citext as build_model_code
from cu.used_3 a
left join chr.build_data_describe_vehicle b on a.vin = b.vin
left join jsonb_array_elements(b.response->'style') bb on true
left join chr.describe_vehicle c on a.vin = c.vin
left join jsonb_array_elements(c.response->'style') cc on true
left join arkona.ext_inpmast d on a.vin = d.inpmast_vin
  and d.model_code is not null
where not exists (
  select 1
  from cu.tmp_vehicles
  where vin = a.vin)
and d.make <> 'ford'
and coalesce((bb ->>'mfrModelCode')::citext, (cc->'attributes'->>'mfrModelCode')::citext) = d.model_code

-- separate queries for build and non build
-- build  32 vins
select vin
from (
	select a.vin, d.make, b.source, b.style_count, 
		d.model_code,(bb ->>'mfrModelCode')::citext as build_model_code,
		count(a.vin) over (partition by a.vin) as vin_count
	from cu.used_3 a
	join chr.build_data_describe_vehicle b on a.vin = b.vin
	join jsonb_array_elements(b.response->'style') bb on true
	left join arkona.ext_inpmast d on a.vin = d.inpmast_vin
		and d.model_code is not null
		and d.inpmast_company_number = 'RY1'
	where not exists (
		select 1
		from cu.tmp_vehicles
		where vin = a.vin)
	and d.make <> 'ford'
	and (bb ->>'mfrModelCode')::citext = d.model_code) e
where vin_count = 1
union
select vin
from (
	select a.vin, d.make, b.source, b.style_count, 
		d.model_code,(bb ->>'mfrModelCode')::citext as build_model_code,
		count(a.vin) over (partition by a.vin) as vin_count
	from cu.used_3 a
	join chr.build_data_describe_vehicle b on a.vin = b.vin
	join jsonb_array_elements(b.response->'style') bb on true
	left join arkona.ext_inpmast d on a.vin = d.inpmast_vin
		and d.model_code is not null
		and d.inpmast_company_number = 'RY8'
	where not exists (
		select 1
		from cu.tmp_vehicles
		where vin = a.vin)
	and d.make <> 'ford'
	and (bb ->>'mfrModelCode')::citext = d.model_code) e
where vin_count = 1

-- non build  373 vins 
select distinct vin 
from (
	select a.vin, d.make, c.style_count, 
		d.model_code, (cc->'attributes'->>'mfrModelCode')::citext as chr_model_code,
		count(a.vin) over (partition by a.vin) as vin_count
	from cu.used_3 a
	join chr.describe_vehicle c on a.vin = c.vin
	join jsonb_array_elements(c.response->'style') cc on true
	left join arkona.ext_inpmast d on a.vin = d.inpmast_vin
		and d.model_code is not null
		and d.inpmast_company_number = 'RY1'
	where not exists (
		select 1
		from cu.tmp_vehicles
		where vin = a.vin)
	and d.make <> 'ford'
	and (cc->'attributes'->>'mfrModelCode')::citext = d.model_code) e
where vin_count = 1
union
select distinct vin 
from (
	select a.vin, d.make, c.style_count, 
		d.model_code, (cc->'attributes'->>'mfrModelCode')::citext as chr_model_code,
		count(a.vin) over (partition by a.vin) as vin_count
	from cu.used_3 a
	join chr.describe_vehicle c on a.vin = c.vin
	join jsonb_array_elements(c.response->'style') cc on true
	left join arkona.ext_inpmast d on a.vin = d.inpmast_vin
		and d.model_code is not null
		and d.inpmast_company_number = 'RY8'
	where not exists (
		select 1
		from cu.tmp_vehicles
		where vin = a.vin)
	and d.make <> 'ford'
	and (cc->'attributes'->>'mfrModelCode')::citext = d.model_code) e
where vin_count = 1


-- non-build insert statement from E:\sql\postgresql\canonical_used_vehicle_source_data\chrome_step_2.sql

this got rid of the dups
-- select vin from (
insert into cu.tmp_vehicles  -- 08/13 RY1 inserted 368 ry8 inserted 5
select aa.vin, 
  (c->'attributes'->>'modelYear')::integer as model_year,
  (c->'division'->>'$value')::citext as make,
  (c->'subdivision'->>'$value')::citext as subdivision,
  (c->'model'->>'$value'::citext)::citext as model,
  coalesce(c->'attributes'->>'trim', 'none')::citext as trim,
  case
    when c->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
    when c->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
    when c->'attributes'->>'drivetrain' like 'Four%' then '4WD'
    when c->'attributes'->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drive,
  (c->'attributes'->>'fleetOnly'::citext)::boolean as fleet_only,
  (c->'attributes'->>'modelFleet'::citext)::boolean as model_fleet,  
  (c->'attributes'->>'mfrModelCode')::citext as chr_model_code,  
  case
    when d.cab->>'$value' like 'Crew%' then 'crew' 
    when d.cab->>'$value' like 'Extended%' then 'double'  
    when d.cab->>'$value' like 'Regular%' then 'reg'  
    else 'n/a'   
  end as cab,    
  case
    when e.bed->>'$value' like '%Bed' then substring(e.bed->>'$value', 1, position(' ' in e.bed->>'$value') - 1)
    else 'n/a'
  end as box_size,      
  coalesce(g.displacement->>'$value', 'n/a') as engine_displacement,
  f->>'cylinders' as cylinders, f->'fuelType'->>'$value' as fuel,
  null as transmission, null as ext_color, null as interior,
  c->'attributes'->>'passDoors' as pass_doors,
  null as msrp,
  c->'marketClass'->>'$value' as market_class,
  b.response->'vinDescription'->'attributes'->>'bodyType' as body_type,
  c->'attributes'->>'altBodyType' as alt_body_type,
  b.response->'vinDescription'->'attributes'->>'styleName' as style_name,
  null as alt_style_name, 
  c->'attributes'->>'nameWoTrim' as name_wo_trim,
  b.response->'attributes'->>'bestMakeName' as best_make_name,
  b.response->'attributes'->>'bestModelName' as best_model_name,
  b.response->'attributes'->>'bestTrimName' as best_trim_name,
  b.response->'attributes'->>'bestStyleName' as best_style_name,
  c->'stockImage'->'attributes'->>'url' as pic_url,
  'Catalog' as source, (c ->'attributes'->>'id')::citext as chrome_style_id
from ( -- aa
		select a.vin, (cc->'attributes'->>'id')::citext as chrome_style_id,
			d.model_code, (cc->'attributes'->>'mfrModelCode')::citext as chr_model_code,
			count(a.vin) over (partition by a.vin) as vin_count
		from cu.used_3 a
		join chr.describe_vehicle c on a.vin = c.vin
		join jsonb_array_elements(c.response->'style') cc on true
		left join arkona.ext_inpmast d on a.vin = d.inpmast_vin
			and d.model_code is not null
-- 			and d.inpmast_company_number = 'RY1'
			and d.inpmast_company_number = 'RY8'
		where not exists (
			select 1
			from cu.tmp_vehicles
			where vin = a.vin)
		and d.make <> 'ford'
		and (cc->'attributes'->>'mfrModelCode')::citext = d.model_code) aa
join chr.describe_vehicle b on aa.vin = b.vin
join jsonb_array_elements(b.response->'style') c on true		
  and aa.chrome_style_id = (c ->'attributes'->>'id')::citext
left join jsonb_array_elements(c->'bodyType') with ordinality as d(cab) on true
  and d.ordinality =  2
left join jsonb_array_elements(c->'bodyType') with ordinality as e(bed) on true
  and e.ordinality =  1    
left join jsonb_array_elements(b.response->'engine') as f(engine) on true  
  and f.engine ? 'installed'
left join jsonb_array_elements(f.engine->'displacement'->'value') as g(displacement) on true
  and g.displacement->'attributes'->>'unit' = 'liters'    
where vin_count = 1	
-- ) x group by vin having count(*) > 1	

-- build data version ------------------------------------------------------------------------------------------------
insert into cu.tmp_vehicles  -- 08/13 RY1 insert 11 rows, RY8 1 row
select aa.vin, 
  (c ->>'modelYear')::integer as model_year,
  (c ->'division'->>'_value_1')::citext as make,
  (c ->'subdivision'->>'_value_1')::citext as subdivision,
  (c ->'model'->>'_value_1'::citext)::citext as model,
  coalesce(c ->>'trim', 'none')::citext as trim,
  case
    when c ->>'drivetrain' like 'Rear%' then 'RWD'
    when c->>'drivetrain' like 'Front%' then 'FWD'
    when c ->>'drivetrain' like 'Four%' then '4WD'
    when c ->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drive,  
  (c ->>'fleetOnly'::citext)::boolean as fleet_only,
  (c ->>'modelFleet'::citext)::boolean as model_fleet,
  (c ->>'mfrModelCode')::citext as model_code,
--   b.response->'vinDescription'->>'drivingWheels' as driving_wheels, -- adds nothing interesting, often null
  case
    when d.cab->>'_value_1' like 'Crew%' then 'crew' 
    when d.cab->>'_value_1' like 'Extended%' then 'double'  
    when d.cab->>'_value_1' like 'Regular%' then 'reg'  
    else 'n/a'   
  end as cab,
  case
    when e.bed->>'_value_1' like '%Bed' then substring(bed->>'_value_1', 1, position(' ' in bed->>'_value_1') - 1)
    else 'n/a'
  end as box_size,  
  case
    when (c ->'model'->>'_value_1'::citext)::citext in ('bolt', 'bolt ev') then 'n/a'
    else g->>'_value_1' 
  end as engine_displacement,
  f->>'cylinders' as cylinders, f->'fuelType'->>'_value_1' as fuel,
  case
    when  (c ->'division'->>'_value_1')::citext = 'Nissan' then 'automatic'
    when i->>0 is null then 'unknown'
    when i->>0 like '%SPEED MANUAL%' then 'manual'
    else 'automatic'
  end as transmission,  
  j->>'colorName' as ext_color, k->>'colorName' as interior,
  c->>'passDoors' as pass_doors,
  ((coalesce(b.response->'vinDescription'->>'builtMSRP', c->'basePrice'->>'msrp'))::numeric)::integer as msrp,
--   m->>'_value_1' as market_class,
  c->'marketClass'->>'_value_1' as market_class,
  b.response->'vinDescription'->>'bodyType' as body_type, c->>'altBodyType' as alt_body_type, 
  b.response->'vinDescription'->>'styleName' as style_name, c->>'altStyleName' as alt_style_name,
  (c->>'nameWoTrim'::citext)::citext as name_wo_trim,
  b.response->>'bestMakeName' as best_make_name,
  b.response->>'bestModelName' as best_model_name,
  b.response->>'bestTrimName' as best_trim_name,
  b.response->>'bestStyleName' as best_style_name,
  c->'stockImage'->>'url' as pic_url,
  b.source, (c ->>'id')::citext as chrome_style_id
from ( -- aa
	select a.vin, b.source, (bb->>'id')::citext as chrome_style_id,
	d.make, b.source, b.style_count, 
		d.model_code,(bb ->>'mfrModelCode')::citext as build_model_code,
		count(a.vin) over (partition by a.vin) as vin_count
	from cu.used_3 a
	join chr.build_data_describe_vehicle b on a.vin = b.vin
	join jsonb_array_elements(b.response->'style') bb on true
	left join arkona.ext_inpmast d on a.vin = d.inpmast_vin
		and d.model_code is not null
-- 		and d.inpmast_company_number = 'RY1'
		and d.inpmast_company_number = 'RY8'
	where not exists (
		select 1
		from cu.tmp_vehicles
		where vin = a.vin)
	and d.make <> 'ford'
	and (bb ->>'mfrModelCode')::citext = d.model_code) aa
join chr.build_data_describe_vehicle b on aa.vin = b.vin
join jsonb_array_elements(b.response->'style') c on true
left join jsonb_array_elements(c->'bodyType') with ordinality as d(cab) on true
  and d.ordinality =  2
left join jsonb_array_elements(c->'bodyType') with ordinality as e(bed) on true
  and e.ordinality =  1    
left join jsonb_array_elements(b.response->'engine') as f on true  
  and (f->'installed'->>'cause' = 'OptionCodeBuild' or coalesce(f->'installed'->>'cause', 'xxx') = 'VIN')
left join jsonb_array_elements(f->'displacement'->'value') as g on true
  and g ->>'unit' = 'liters'
left join jsonb_array_elements(b.response->'factoryOption') h on true
  and h->'header'->>'_value_1' = 'TRANSMISSION'
  and h->'installed'->>'cause' in ('VIN','OptionCodeBuild')
left join jsonb_array_elements(h->'description') i on true
left join jsonb_array_elements(b.response->'exteriorColor') as j on true
  and j->'installed'->>'cause' in ('RelatedColor','ExteriorColorBuild', 'OptionCodeBuild','InteriorColorBuild')    
left join jsonb_array_elements(b.response->'interiorColor') as k on true --interior
  and k->'installed'->>'cause' in ('RelatedColor','ExteriorColorBuild', 'OptionCodeBuild','InteriorColorBuild')
left join jsonb_array_elements(k->'description') as l on true
where aa.vin_count = 1	
  and aa.chrome_style_id = (c ->>'id')::citext
------------------------------------------------------------------------------------------------------------------
--/> exclude ford, look for match on model code
------------------------------------------------------------------------------------------------------------------