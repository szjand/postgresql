﻿select year_month, 
  case store
		when 'RY1' then 'GM'::citext
		when 'RY2' then 'HN'::citext
		when 'RY8' then 'TY'::citext
		else 'XXX'::citext
  end as store, line, line_label, control, unit_count 
from (
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 202306 -- between 201801 and 202304 -----------------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description, limits transactions to the relevant sale accounts
    select b.year_month, f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 202305 -- between 201801 and 202304 ---------------------------------------------------------------------
      and b.page = 16 and b.line between 1 and 14 -- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4' -- sale accounts only, does not factor in cogs
      and e.current_row
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account and b.year_month = d.year_month
  where a.post_status = 'Y') h
where control like 'H%'  
order by control  
-- Honda "L deals" that Longoria asks me to zap out of the tool, show up in the fs count?
-- sale account is not included in the transaction for H16648L and H16662L
-- so they will not be included in the fs count
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
select a.*, b.the_date   
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 202306 -- between 201801 and 202304 -----------------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
where a.control like 'H%'
  and length(a.control) = 6
order by control

select * from fin.dim_account where account_key = 78  
  
select * from fin.dim_account where account in ('220200','224100','222002')