﻿no acq date?

so, what does an IMWS look like
it is one instance of a vehicle in inventory with multiple stock numbers
so acq date will be the orig acquisition
and delivery will be the the final delivery
eg
H15877G  1C4RJFBT6JC220547
acq 8/25/22 as G45391GA
avail on 9/2/22
imws on 9/10/22
del on 9/10/22

--------------------------------------------------------------------------------------------------------------------------------------------
-- not in tool, 
select a.*, c.bopmast_Stock_number,  c.bopmast_vin, c.bopmast_search_name, c.delivery_Date, c.date_capped
from cu.used_2 a
inner join ads.ext_vehicle_items b on a.vin = b.vin
left join arkona.ext_bopmast c on a.stock_number = c.bopmast_stock_number
where status_at_time_of_sale = 'unknown'
  and sale_subtype <> 'lease buyout'


select * from arkona.ext_bopmast where bopmast_stock_number = 'G36737G'

select status,  inpmast_stock_number, inpmast_vin, arkona.db2_integer_to_date(date_in_invent) as date_acq,
  arkona.db2_integer_to_date(date_delivered) as date_del, row_from_date, row_thru_date
from arkona.xfm_inpmast
where inpmast_vin = '1FAHP35N78W249947'
order by row_From_date


select record_key, record_status, record_type, sale_type, bopmast_Stock_number, bopmast_vin, date_capped, delivery_date, row_from_Date, row_thru_date, a.buyer_number, b.bopname_search_name 
from arkona.xfm_bopmast a
left join arkona.ext_bopname b on a.buyer_number = b.bopname_record_key
  and a.bopmast_company_number = b.bopname_company_number
where bopmast_vin = '1FAHP35N78W249947'














select * from cu.used_2 limit 1000

select status_at_time_of_sale, count(*)
from cu.used_2
where sale_subtype <> 'lease buyout'
group by status_at_time_of_sale;

select status_at_time_of_sale, left(delivery_year_month::text,4):: integer, count(*)
from cu.used_2
where sale_subtype <> 'lease buyout'
group by status_at_time_of_sale, left(delivery_year_month::text,4):: integer
order by left(delivery_year_month::text,4):: integer, status_at_time_of_sale


-- 153 rows with status at sale = unknown, none of which are in tool by stock number
-- but 107 of the vins are in the tool
select * 
from cu.used_2 a
inner join ads.ext_vehicle_items b on a.vin = b.vin
-- inner join ads.ext_Vehicle_inventory_items b on a.stock_number = b.stocknumber
where status_at_time_of_sale = 'unknown'
  and sale_subtype <> 'lease buyout'

select * from cu.used_2 where sale_subtype = 'lease buyout'  

select sale_subtype, count(*)
from cu.used_2
group by sale_subtype

create index on cu.used_2(vin)

select distinct vin from cu.used_2  --17107

select vin  --7
from cu.used_2 a
where length(vin) <> 17

select * 
from cu.used_2 
where vin in (
select vin
from cu.used_2
where not cra.vin_check_sum(vin)
  or length(vin) <> 17)

------------------------------------------------------------------------------------------------------------------------
select distinct vin  --2407
from cu.used_2 a
where not exists (
  select 1
  from cu.tmp_vehicles
  where vin = a.vin)
and cra.vin_check_sum(a.vin)
and length(vin) = 17


select distinct a.vin  -- 18
from cu.used_2 a
join chr.build_data_describe_vehicle b on a.vin = b.vin
  and b.source <> 'build'
  and b.style_count = 1
where not exists (
  select 1
  from cu.tmp_vehicles
  where vin = a.vin)  


-- inserted into cu.tmp_vehicles
select distinct b.inpmast_vin as vin -- 148
from cu.used_1 a
join cu.inpmast_year_make_vin_stock b on a.stock_number = b.inpmast_stock_number
join chr.build_data_coverage c on b.make = c.division
  and b.year between c.from_year and c.thru_year
where not exists (
  select 1
  from chr.build_data_describe_vehicle
  where vin = b.inpmast_vin)
and not exists (
  select 1
  from cu.tmp_vehicles
  where vin = b.inpmast_vin)  

  select count(*) from chr.build_data_describe_vehicle where the_date = current_date 