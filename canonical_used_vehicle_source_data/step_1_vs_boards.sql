-- drop table if exists step_1;
-- create temp table step_1 as
-- select store, page, line, line_label, control, sum(unit_count) as unit_count
-- from (
--   select d.store, d.page, d.line, d.line_label, d.description, d.gl_account,
--     a.control, a.amount,
--     case when a.amount < 0 then 1 else -1 end as unit_count
--   from fin.fact_gl a
--   inner join dds.dim_date b on a.date_key = b.date_key
--     and b.year_month = 201811 -----------------------------------------------------------------------------
--   inner join fin.dim_account c on a.account_key = c.account_key
--     and c.current_row
-- -- add journal
--   inner join fin.dim_journal aa on a.journal_key = aa.journal_key
--     and aa.journal_code in ('VSN','VSU')
--   inner join ( -- d: fs gm_account page/line/acct description
--     select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
--     from fin.fact_fs a
--     inner join fin.dim_fs b on a.fs_key = b.fs_key
--       and b.year_month = 201811 ---------------------------------------------------------------------
--       and b.page = 16 and b.line between 1 and 14 -- used cars
-- --         or
-- --         (b.page = 17 and b.line between 1 and 20))-- f/i counts don't make sense
--     inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
--     inner join fin.dim_account e on d.gl_account = e.account
--       and e.account_type_code = '4'
--       and e.current_row
--     inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
--   where a.post_status = 'Y') h
-- group by store, page, line, line_label, control
-- order by store, page, line;

/*
-- TOTALS
select store,-- page, line, line_label, sum(unit_count),
  sum(unit_count) filter (where line_label like '%retail%'),
  sum(unit_count) filter (where line_label like '%whole%')
from (
  select *
  from step_1) a
group by store
*/


-- FULL OUTER JOIN
select * 
from ( -- AA
    select distinct a.stock_number, c.board_type, board_sub_type, is_backed_on, boarded_date, case when board_type = 'Deal' then 1 else -1 end as unit_count
    from board.sales_board a
    inner join board.daily_board b on a.board_id = b.board_id and vehicle_type = 'U'
    inner join board.board_types c on a.board_type_key = c.board_type_key and board_type in ('Deal', 'Back-on')
    inner join dds.dim_date d on a.boarded_date = d.the_date and year_month = 201812
    where is_deleted = false
        and store_key in (39,41)  ) AA
        -- 39, 41 (GM & OUTLET)
        -- 40 (Honda/Nissan)
        -- 42 (Toyota)
full outer join (
    select * 
    from step_1
    where store = 'ry1') BB on AA.stock_number = BB.control 
-- where coalesce(AA.stock_number, 'HH') <>  coalesce(BB.control,'JJ') 
order by AA.stock_number