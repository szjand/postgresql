﻿drop table if exists avail_sales;
create temp table avail_sales as
select * from cu.used_3
where delivery_date between current_date - 365 and current_date
  and days_available > 0;
create index on avail_sales(vin);  

select * from avail_Sales


select * from avail_Sales a
left join (
select replace(cust_name, 'INVENTORY: ','') as stock_number, vin, ro_number
from arkona.ext_sdprhdr where cust_name like 'INVENTORY%') b on a.stock_number = b.stock_number::citext  



select a.stock_number, a.vin, b.ro_number from avail_Sales a
left join arkona.ext_sdprhdr b on a.vin = b.vin


select min(open_date), max(open_date) from arkona.ext_sdprhdr_history



select vin 
from cu.used_3
where delivery_date between '06/01/2022' and  current_date
  and days_available > 0;

-- 07/13/23 populated in python_projects/ext_arkona/ext_sdprhdr_for_used_car_recon.py
drop table if exists cu.recon_ro_numbers;
create table cu.recon_ro_numbers (
  stock_number citext,
  vin citext,
  ro citext,
  primary key(stock_number,ro));
comment on table cu.recon_ro_numbers is 'one time scrape of rydedata.sdprhdr to get the stock number for recon ros,
  scraped with python_projects/ext_arkona/ext_sdprhdr_for_used_car_recon.py';  

create temp table wtf_1 as
select * from cu.recon_ro_numbers            


select a.stock_number, a.vin, b.*, replace(b.cust_name, 'INVENTORY: ',''), c.*
from cu.used_3 a
left join cu.recon_ro_numbers b on a.stock_number = replace(b.cust_name, 'INVENTORY: ','')::citext
left join dds.fact_repair_order c on b.ro = c.ro
where a.stock_number in ('H15507C','G44622P')
order by a.stock_number

create temp table wtf as 
select replace(cust_name, 'INVENTORY: ','')::citext as stock_number, vin, ro
from cu.recon_ro_numbers
group by replace(cust_name, 'INVENTORY: ',''), vin, ro

select stock_number, ro
from wtf
group by stock_number, ro
having count(*) > 1

insert into cu.recon_ro_numbers
select * from wtf