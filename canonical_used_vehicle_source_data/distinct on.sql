﻿-- https://stackoverflow.com/questions/46566602/what-does-distinct-on-expression-do
-- https://stackoverflow.com/questions/3800551/select-first-row-in-each-group-by-group/7630564#7630564 (erwin)

select inpmast_vin, inpmast_stock_number, count(*)
from (
select inpmast_stock_number, inpmast_vin, row_from_date
from arkona.xfm_inpmast
where inpmast_Stock_number is not null
  and length(inpmast_vin) = 17) a
group by inpmast_vin, inpmast_stock_number
having count(*) = 5
order by count(*) desc   
limit 5


select inpmast_stock_number, inpmast_vin, row_from_date
from arkona.xfm_inpmast
where inpmast_vin in ('19XFA16539E024538','19XFA16999E006340','19XFA1F57AE046403','19XFB2F53DE100027','19UUA56623A066346')
order by inpmast_vin, row_from_date desc

H4007B,19UUA56623A066346,2021-04-02
H4007B,19UUA56623A066346,2021-03-19
H4007B,19UUA56623A066346,2021-03-17
H4007B,19UUA56623A066346,2021-03-02
H4007B,19UUA56623A066346,2017-08-29
9987,19XFA16539E024538,2022-01-21
9987,19XFA16539E024538,2022-01-16
9987,19XFA16539E024538,2018-08-14
9987,19XFA16539E024538,2018-01-16
9987,19XFA16539E024538,2017-08-29
H1339,19XFA16999E006340,2022-09-01
H1339,19XFA16999E006340,2019-02-07
H1339,19XFA16999E006340,2018-12-12
H1339,19XFA16999E006340,2017-11-26
H1339,19XFA16999E006340,2017-08-29
H2105,19XFA1F57AE046403,2021-08-26
H2105,19XFA1F57AE046403,2021-08-20
H2105,19XFA1F57AE046403,2021-02-18
H2105,19XFA1F57AE046403,2021-02-17
H2105,19XFA1F57AE046403,2017-08-29
H10647A,19XFB2F53DE100027,2019-02-19
H10647A,19XFB2F53DE100027,2018-09-25
H10647A,19XFB2F53DE100027,2018-03-24
H6480,19XFB2F53DE100027,2018-02-10
H6480,19XFB2F53DE100027,2017-08-29

select distinct on (inpmast_vin) inpmast_stock_number, inpmast_vin, row_from_date 
from (
select inpmast_stock_number, inpmast_vin, row_from_date
from arkona.xfm_inpmast
where inpmast_vin in ('19XFA16539E024538','19XFA16999E006340','19XFA1F57AE046403','19XFB2F53DE100027','19UUA56623A066346')) a
order by inpmast_vin, row_from_date desc


drop table if exists inpmast_1;
create temp table inpmast_1 as
select inpmast_Stock_number, inpmast_vin
from arkona.xfm_inpmast
where inpmast_Stock_number is not null
  and length(inpmast_stock_number) > 5
  and length(inpmast_vin) = 17
group by inpmast_Stock_number, inpmast_vin  ;


select inpmast_vin, count(*)
from inpmast_1
group by inpmast_vin
having count(*) >  2


select * from inpmast_1 where inpmast_vin = '2GKALPEK8H6162898'

select inpmast_stock_number, row_from_date, row_thru_date, date_in_invent from arkona.xfm_inpmast where inpmast_vin = '2GKALPEK8H6162898' order by inpmast_stock_number, row_thru_date desc


-- either from_date or thru_date work the same
select distinct on (inpmast_stock_number)inpmast_stock_number, inpmast_vin, date_in_invent
from arkona.xfm_inpmast 
where inpmast_vin = '2GKALPEK8H6162898'
order by inpmast_stock_number, row_from_date desc

G34478X,2GKALPEK8H6162898,20180711
G43230G,2GKALPEK8H6162898,20210504
H14460A,2GKALPEK8H6162898,20210504
H14475A,2GKALPEK8H6162898,20210503


-- this returns the same results
select distinct inpmast_stock_number, inpmast_vin, date_in_invent
from arkona.xfm_inpmast 
where inpmast_vin = '2GKALPEK8H6162898'

-- so, can i just do this
drop table if exists inpmast_2;
create temp table inpmast_2 as  --90162, 69340 with non 0 date_in_ 22891 when limit to cu.used_1
select distinct inpmast_stock_number, inpmast_vin, date_in_invent
from arkona.xfm_inpmast a
where inpmast_Stock_number is not null
  and length(inpmast_stock_number) > 5
  and length(inpmast_vin) = 17
  and date_in_invent <> 0
  and exists (
    select 1 
    from cu.used_1
    where stock_number = a.inpmast_stock_number)
order by inpmast_vin, date_in_invent
create index on inpmast_2(inpmast_stock_number);

select * from inpmast_2  

date_in_invent is not reliable (not consistently updated): 1GCVKREC1JZ233566

-- this is an ugly anomaly
select inpmast_stock_number, inpmast_vin, date_in_invent, row_from_Date, row_thru_date
from arkona.xfm_inpmast
where inpmast_vin = '1GCVKREC1JZ233566'
order by row_from_Date
-- wtf
select * from arkona.xfm_inpmast where inpmast_stock_number = 'G40936A' order by row_from_Date
vin 3GCUYDED8KG104442 in tool

-- 3 stk w/same date_in
select inpmast_stock_number, inpmast_vin, date_in_invent, row_from_Date, row_thru_date
from arkona.xfm_inpmast
where inpmast_vin = '1GCVKREC5HZ177321'
order by row_from_Date
-- looks like the 2 honda stocknumbers never actually happened ? at least not sold
select record_key, record_status, record_type, sale_type, bopmast_Stock_number, bopmast_vin, date_capped, row_from_Date, row_thru_date, a.buyer_number, b.bopname_search_name 
from arkona.xfm_bopmast a
left join arkona.ext_bopname b on a.buyer_number = b.bopname_record_key
  and a.bopmast_company_number = b.bopname_company_number
where bopmast_stock_number in ('30156R','H10278A','H10659B')




drop table if exists bopmast_1 cascade;
create temp table bopmast_1 as  --18913
select bopmast_stock_number, bopmast_vin, delivery_date
from arkona.xfm_bopmast a
where record_status = 'U'
and exists (
  select 1
  from cu.used_1
  where stock_number = a.bopmast_stock_number)
group by bopmast_stock_number, bopmast_vin, delivery_date  
order by bopmast_vin, delivery_date;
create index on bopmast_1(bopmast_Stock_number);
19XFC1F98GE006966: tool shows 3 stk#s, bopmast only shows 2, does not show the stk that was imws to GM


select *
from cu.used_1 a
left join inpmast_2 b on a.stock_number = b.inpmast_stock_number
left join bopmast_1 c on a.stock_number = c.bopmast_stock_number
order by a.stock_number

-- how to distinguish between these 3 G41347RA