﻿select a.vin, a.model_year, a.make, a.model, r.style ->'attributes'->>'name',
  array_agg(t.tech ->> 'titleId' order by t.tech ->> 'titleId')
-- select *
from nc.vehicles a
join chr.describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'style') as r(style) on true
join jsonb_array_elements(b.response->'technicalSpecification') as t(tech) on true
where cab <> 'n/a'
  and make = 'chevrolet'
  and model_year = 2020
  and model like '%1500%'
group by a.vin, a.model_year, a.make, a.model, r.style ->'attributes'->>'name'
limit 10  

select aa.*, bb.shape, bb.size
from (
  select distinct make , model, unnest((replace(wheelbase, '{,', '{'))::numeric[]) as wheelbase
  from ( -- remove comma immediately after first bracket
    select make, model, replace(wheelbase, ',}', '}') as wheelbase
    from ( -- remove trailing comma before bracket
      select make, model, '{' || replace(wheelbase, ',,', ',')  ||'}' as wheelbase
      from ( -- remove - TBD -
        select make, model, replace(wheelbase, '- TBD -', '') as wheelbase
        from ( -- remove none
          select make, model, replace(wheelbase, 'none', '') as wheelbase
          from veh.vehicle_types) a) b) c) d) aa
left join veh.shape_size_classifications bb on aa.make = bb.make and aa.model = bb.model          
order by wheelbase


    select make, model, replace(wheelbase, ',}', '}') as wheelbase
    from ( -- remove trailing comma before bracket
      select make, model, '{' || replace(wheelbase, ',,', ',')  ||'}' as wheelbase
      from ( -- remove - TBD -
        select make, model, replace(wheelbase, '- TBD -', '') as wheelbase
        from ( -- remove none
          select make, model, replace(wheelbase, 'none', '') as wheelbase
          from veh.vehicle_types) a) b) c




left join ( -- 2005: multiple peg for some models
  select style_id, string_agg(distinct oem_code, ',') as peg
  from chr2.option
  where header_name = 'PREFERRED EQUIPMENT GROUP'
  group by style_id) f on a.style_id = f.style_id
left join ( -- wheelbase 
  select style_id, string_agg(the_value, ',') as wheelbase
  from chr2.tech_specs 
  where title_id = 301
  group by style_id) g on a.style_id = g.style_id
left join ( -- overall_length 
  select style_id, string_agg(the_value, ',') as overall_length
  from chr2.tech_specs 
  where title_id = 304
  group by style_id) h on a.style_id = h.style_id
left join chr2.image i on a.style_id = i.style_id
left join chr2.tech_specs j on a.style_id = j.style_id
  and j.title_id = 1113  -- segment
left join chr2.tech_specs k on a.style_id = k.style_id
  and k.title_id = 1114  -- vehicle_type
left join (
  select style_id, string_agg(distinct the_value, ',') as passenger_capacity
  from chr2.tech_specs 
  where title_name = 'passenger capacity'
  group by style_id) l on a.style_id = l.style_id           