﻿create index on cu.used_1(stock_number);

-- 15 with no match in inpmast
select * 
from cu.used_1 a
left join (
  select inpmast_stock_number, inpmast_vin
  from arkona.xfm_inpmast
  where inpmast_stock_number is not null
  group by inpmast_stock_number, inpmast_vin) b on a.stock_number = b.inpmast_stock_number
where b.inpmast_vin is null  


-- 925 with no match in tool
-- 739 are L vehicles
select right(a.stock_number, 1), count(*)
from cu.used_1 a
left join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
where b.stocknumber is null
group by right(a.stock_number, 1)


attributes of a lease buyout:
	inpcmnt: a comment that says lease 
	acquistion date = sale date
	consultant is HSE
-- include inpcmnt on those with no tool data	
drop table if exists cu.tmp_lease_buyout;
create unlogged table cu.tmp_lease_buyout as
select a.*, b.inpmast_vin, b.acq_date, e.primary_salespers, d.comment 
from cu.used_1 a
left join (
  select inpmast_stock_number, inpmast_vin, arkona.db2_integer_to_date(date_in_invent) as acq_date
  from arkona.xfm_inpmast
  where inpmast_stock_number is not null
  group by inpmast_stock_number, inpmast_vin, arkona.db2_integer_to_date(date_in_invent)) b on a.stock_number = b.inpmast_stock_number
left join ads.ext_vehicle_inventory_items c on a.stock_number = c.stocknumber  
left join arkona.ext_inpcmnt d on b.inpmast_vin = d.vin
  and comment like '%BUY%'
left join arkona.ext_bopmast e on a.stock_number = e.bopmast_stock_number  
where right(a.stock_number, 1) = 'L'
  and c.stocknumber is null;
comment on table cu.tmp_lease_buyout is 'looking at identifying lease purchases (lessee buys out his own lease) that do not exist in the tool';



select *, delivery_date - acq_date from cu.tmp_lease_buyout order by delivery_date - acq_date

select * from arkona.xfm_inpmast where inpmast_stock_number = 'G43520L'
select * from arkona.xfm_inpmast where inpmast_vin = '1C6SRFJT8KN674002' order by row_from_date

select * from ads.ext_vehicle_sales limit 10

select typ, count(*) from ads.ext_vehicle_sales group by typ

where in the tool, in and out is registered
there is a table in advantage, InAndOuts, with 2 fields: VehicleSaleID and Typ for which there are 3 values: InAndOut_BuyBid, InAndOut_InAndOut, InAndOut_LeasePurchase

-- advantage query
-- in and outs show now statuses except raw, sold & delivered
SELECT c.stocknumber, a.VehicleInventoryItemID, CAST(a.soldts AS sql_date) AS sale_Date, a.typ AS sale_type, b.typ AS in_and_out_type
-- SELECT COUNT(*) -- 579 / 577 w/VehicleInventoryItems  / 62 past 5/31/2018
FROM vehiclesales a
JOIN inandouts b on a.vehiclesaleid = b.vehiclesaleid
JOIN VehicleInventoryItems c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
WHERE CAST(a.soldts AS sql_date) > '05/31/2018'
ORDER BY CAST(a.soldts AS sql_date) desc





select * from ads.ext_vehicle_evaluations where vehicleevaluationts::date >'05/31/2023'
select typ, count(*) from ads.ext_vehicle_evaluations group by typ
 
update cu.sale_types
set sale_subtype = 'Lease Purchase'
where sale_type = 'Wholesale'
  and sale_subtype = 'Lease Buyout';


select distinct on (inpmast_stock_number) inpmast_vin, row_thru_date
from arkona.xfm_inpmast 
where inpmast_stock_number = 'G45190B'
order by inpmast_stock_number, row_thru_date desc

---------------------------------------------------------------------------------------------------------------------------------------------
-- the purpose of this is to change the sale_sub_type in cu.used_1 and commit to excluded those stock numbers that are undecipherable
-- starting with 764 rows

-- get consensus with afton on difference between lease buyout and lease purchase

select *, delivery_date - acq_date 
from cu.tmp_lease_buyout 
order by delivery_date - acq_date

-- all with a comment are retail
select comment, sale_type, count(*), min(delivery_date - acq_date), max(delivery_date - acq_date)
-- select count(*) -- 316 rows
from cu.tmp_lease_buyout 
where comment is not null 
group by comment, sale_type

-- the ddl picked up the incorrect date_in_invent, s/b 07/01/2022
select * from cu.tmp_lease_buyout where delivery_date - acq_date = 1080
select * from arkona.xfm_inpmast where inpmast_vin = '1GCUYGED4KZ268563' order by row_from_date

-- assumption #1, any row with a comment is a valid lease buyout

-- those with no comment  -------------------------------------------------------
select *, delivery_date - acq_date
-- select count(*) -- 448 rows
from cu.tmp_lease_buyout 
where comment is null 
order by delivery_date - acq_date

-- those with no comment and cons - HSE ------------------------------------------
select *, delivery_date - acq_date
-- select count(*) -- 407 rows
from cu.tmp_lease_buyout 
where comment is null 
  and primary_salespers = 'hse'
  and sale_type = 'retail'
order by delivery_date - acq_date


-- 9 rows where inpmast_vin is null -----------------------------------------------
select *, delivery_date - acq_date  
from cu.tmp_lease_buyout
where inpmast_vin is null

-- get the vin from bopmast?  yep, except for the wholesale
select a.*, b.bopmast_vin, b.date_capped
from cu.tmp_lease_buyout a
left join arkona.ext_bopmast b on a.stock_number = b.bopmast_stock_number
where a.inpmast_vin is null


H15799L (5FNYF6H73LB007178) was a lease purchase that was retailed as G45353G
select * from board.sales_board where stock_number = 'H15799L'


select inpmast_vin, inpmast_Stock_number, row_from_date, date_in_invent
from arkona.xfm_inpmast a
where a.inpmast_vin in ('3C6UR5CJ7JG144643','5FNYF6H5XHB086421','1C6SRFFTXKN816472','2C4RC1EG5JR352377','5FPYK3F55KB007717','5TFDY5F17LX902891','1C6SRFJT8KN674002','5TDDZRBH8LS049446','5FNYF6H73LB007178')
order by inpmast_vin, row_from_date


-- why am i focusing on vehicles not in the tool
-- the puurpose of identifying lease buyouts is to exclude them from sale stats (for scope 1 at least), so whether they are in the tool or not, they need to be identified
-- 1129 L vehicles
select * from cu.used_1 where right(stock_number, 1) = 'L'

-- so what i need, is acquistion date (to compare to delivery date), consultant, comment
-- 6/19/23 add days owned
drop table if exists cu.tmp_lease_buyout;
create unlogged table cu.tmp_lease_buyout as
select g.*, delivery_date - acq_date as days_owned
from (
	select a.stock_number,a.store,a.outlet_sale,max(a.delivery_date) as delivery_date,max(a.delivery_year_month) as delivery_year_month,a.sale_type,a.sale_subtype,max(bopmast_id) as bopmast_id, b.inpmast_vin, 
		max(acq_date) as acq_date, c.stocknumber as tool_stock, d.comment, e.bopmast_vin, 
		e.primary_salespers, e.retail_price, e.vehicle_cost, e.retail_price - e.vehicle_cost as front_gross, 
		sum(coalesce(f.days_avail, 0)) as days_avail,
		c.thruts::date - c.fromts::date as days_owned_tool
	from cu.used_1 a
	left join (
		select inpmast_stock_number, inpmast_vin, arkona.db2_integer_to_date(date_in_invent) as acq_date
		from arkona.xfm_inpmast
		where inpmast_stock_number is not null
			and
				case
					when inpmast_stock_number = 'G44614L' then inpmast_vin = '1GTP9EEL4KZ341209'
					when inpmast_stock_number = 'G44613L' then inpmast_vin = '3GTP9EELXKG194698'
					else true
				end
		group by inpmast_stock_number, inpmast_vin, arkona.db2_integer_to_date(date_in_invent)) b on a.stock_number = b.inpmast_stock_number
	left join ads.ext_vehicle_inventory_items c on a.stock_number = c.stocknumber  
	left join arkona.ext_inpcmnt d on b.inpmast_vin = d.vin
		and d.comment like '%BUY%'
		and d.comment <> 'NO GUARANTEES NO BUY BACK'
	left join arkona.ext_bopmast e on a.stock_number = e.bopmast_stock_number  
	left join (
		select a.stock_number, c.thruts::date - c.fromts::date as days_avail
		from cu.used_1 a
		join ads.ext_Vehicle_inventory_items b on a.stock_number = b.stocknumber
		join ads.ext_vehicle_inventory_item_statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
			and c.status = 'RMFlagAV_Available'
		where right(a.stock_number, 1) = 'L') f on a.stock_number = f.stock_number
	where right(a.stock_number, 1) = 'L'
	group by  a.stock_number,a.store,a.outlet_sale,a.sale_type,a.sale_subtype,
		b.inpmast_vin,c.stocknumber,d.comment,e.bopmast_vin,e.primary_salespers,e.retail_price,e.vehicle_cost, e.retail_price - e.vehicle_cost, c.thruts::date - c.fromts::date) g;
comment on table cu.tmp_lease_buyout is 'looking at identifying lease buyouts (lessee buys out their own lease) and lease purchases';

-- no dups
select stock_number from cu.tmp_lease_buyout group by stock_number having count(*) > 1

select * from cu.tmp_lease_buyout where comment is not null

select * from cu.tmp_lease_buyout order by sale_subtype

-- any dup rows left? nope
select a.*
from cu.tmp_lease_buyout a
join (
	select stock_number 
	from cu.tmp_lease_buyout
	group by stock_number 
	having count(*) > 1) b on a.stock_number = b.stock_number
order by a.stock_number

--< but missing 1 vehicle -------------------------------------------------------------------------
-- but this doesn't find it, funny 1129 in cu.used_1 but 1128 in cu.tmp_lease_buyout, but no missing stock_number
select * 
from cu.used_1 a
where right(stock_number, 1) = 'L'
  and not exists (
    select 1
    from cu.tmp_lease_buyout
    where stock_number = a.stock_number)
    
-- this doesn't help, a bunch of unwinds
-- here is why, G43556L a lease purchase that unwound
select * from cu.used_1 a
join (
select stock_number from cu.used_1 group by stock_number having count(*) > 1) b on a.stock_number = b.stock_number
where right(a.stock_number, 1) = 'L'
order by a.stock_number
-- and the grouping eliminated one row
select * from cu.tmp_lease_buyout where stock_number = 'G43556L'
--/> but missing 1 vehicle -------------------------------------------------------------------------



-- is there anything in bopmast that identifies a lease buyout
-- price = cost:  retail_price, vehicle_cost
select * from arkona.ext_bopmast where bopmast_stock_number = 'G45328L'


select * from ads.ext_vehicle_inventory_items where stocknumber = 'G44567L'
select * from ads.ext_vehicle_inventory_item_statuses where vehicleinventoryitemid = 'b91d1446-62d8-4232-bc73-237e1464aacc'


select a.stock_number, c.thruts::date - c.fromts::date
from cu.used_1 a
join ads.ext_Vehicle_inventory_items b on a.stock_number = b.stocknumber
join ads.ext_vehicle_inventory_item_statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
  and c.status = 'RMFlagAV_Available'
where right(a.stock_number, 1) = 'L'  

-- 06/14/23 -----------------------------------------------------------------------------------

--------------------------------------------------------------------------------------
--< L vehicles, sale_subtype: IMWS
--------------------------------------------------------------------------------------
select * from cu.tmp_lease_buyout where sale_subtype = 'Intracompany Wholesale'

-- sale_subtype: IMWS, what is the new stocknumber

-- ads.get_intra_market_wholesale_new_stock_number(citext)
with
  history as (
		select stocknumber, row_number() over (order by fromts) as seq 
		from ads.ext_vehicle_inventory_items
		where vehicleitemid = (
			select vehicleitemid
			from ads.ext_vehicle_inventory_items
			where stocknumber = 'H16164L')
		order by fromts) 
select stocknumber
from history
where seq = (
  select seq + 1
  from history
  where stocknumber = 'H16164L');

-- ads.get_intra_market_wholesale_old_stock_number(citext)
with
  history as (
		select stocknumber, row_number() over (order by fromts) as seq 
		from ads.ext_vehicle_inventory_items
		where vehicleitemid = (
			select vehicleitemid
			from ads.ext_vehicle_inventory_items
			where stocknumber = 'H15799L')
		order by fromts desc) 
select stocknumber
from history
where seq = (
  select seq - 1
  from history
  where stocknumber = 'H15799L');

1N4BL4BW0KC260043 in tool as H16164L, in inpmast as H12905
select status,  inpmast_stock_number, inpmast_vin, arkona.db2_integer_to_date(date_in_invent),
  arkona.db2_integer_to_date(date_delivered), row_from_date, row_thru_date
from arkona.xfm_inpmast
where inpmast_vin = '1N4BL4BW0KC260043'
order by row_From_date

-- add H15799L to cu.exclude
insert into cu.excluded_stock_numbers values('RY2','H15799L',202208,'Wholesale', 'stocknumber does not exist in tool or inpmnast, so unable to derive a VIN');

--------------------------------------------------------------------------------------
--/> L vehicles, sale_subtype: IMWS
--------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------
--< L vehicles, sale_type Retail identify sale_subtype of Lease Buyout
--------------------------------------------------------------------------------------
select * from cu.tmp_lease_buyout where comment is not null -- and sale_type <> 'Retail'  -- all are retail

update cu.used_1 x
set sale_subtype = 'Lease Buyout'
where stock_number in ( -- 331 rows
  select stock_number 
  from cu.tmp_lease_buyout
  where comment is not null);


-- only 1 fit this, G35296L/1GCVKREC1FZ336123
select * from cu.tmp_lease_buyout where comment is null and sale_type = 'retail' and bopmast_vin is null
G35296L does not exist in bopmast or tool but does exist in DT UI as a sale in 201810, unwound and sold retail in 201811, 
        old record of vin in xfm_bopmast sold to Warweg, DT shows sold to Warweg, so, this is a lease buyout
update cu.used_1
set sale_subtype = 'Lease Buyout'
-- select * from cu.used_1
where stock_number = 'G35296L';        


-- so, i am concerned on these where the inpmast_vin is null, though there are onbly 8 of them
select * from cu.tmp_lease_buyout where comment is null and sale_type = 'retail' and primary_salespers = 'HSE' and inpmast_vin is null
-- so, what does this mean, i am leaning towards, no inpmast means it was a lease purchase rather than a lease buyout
-- hard to tell


select * from cu.tmp_lease_buyout where comment is null and sale_type = 'retail' and primary_salespers = 'HSE' and inpmast_vin is not null




-- pin down the lease buyouts
-- these all show 0 days available and minimal front gross if any
select * from cu.tmp_lease_buyout where comment is null and sale_type = 'retail' and primary_salespers = 'HSE'

select * from cu.used_1 where sale_subtype = 'lease buyout'

select * from cu.tmp_lease_buyout where stock_number = 'H15151L'
select record_status,  bopmast_Stock_number, bopmast_vin, date_capped, row_from_Date, row_thru_date from arkona.xfm_bopmast where bopmast_stock_number = 'H15151L'
select record_key, record_status, record_type, sale_type, bopmast_Stock_number, bopmast_vin, date_capped, row_from_Date, row_thru_date, a.buyer_number, b.bopname_search_name 
from arkona.xfm_bopmast a
left join arkona.ext_bopname b on a.buyer_number = b.bopname_record_key
  and a.bopmast_company_number = b.bopname_company_number
where bopmast_vin = '1C6SRFFTXKN816472'

select record_key, record_status, record_type, sale_type, bopmast_Stock_number, bopmast_vin, date_capped, row_from_Date, row_thru_date, a.buyer_number, b.bopname_search_name 
from arkona.xfm_bopmast a
left join arkona.ext_bopname b on a.buyer_number = b.bopname_record_key
  and a.bopmast_company_number = b.bopname_company_number
where record_key = '5095'

select record_key, record_status, record_type, sale_type, bopmast_Stock_number, bopmast_vin, date_capped, row_from_Date, row_thru_date, a.buyer_number, b.bopname_search_name 
from arkona.xfm_bopmast a
left join arkona.ext_bopname b on a.buyer_number = b.bopname_record_key
  and a.bopmast_company_number = b.bopname_company_number
where bopmast_stock_number = '32628A' 

select record_key, record_status, record_type, sale_type, bopmast_Stock_number, bopmast_vin, date_capped, a.row_from_Date, a.row_thru_date, a.buyer_number, b.bopname_search_name 
from arkona.xfm_bopmast a
left join arkona.xfm_bopname b on a.buyer_number = b.bopname_record_key
  and a.bopmast_company_number = b.bopname_company_number
where bopmast_stock_number = '32628A' 

select record_key, record_status, record_type, sale_type, bopmast_Stock_number, bopmast_vin, date_capped, a.row_from_Date, a.row_thru_date, a.buyer_number, b.bopname_search_name 
from arkona.xfm_bopmast a
left join arkona.xfm_bopname b on a.buyer_number = b.bopname_record_key
  and a.bopmast_company_number = b.bopname_company_number
where record_key in (51595, 51157)

select status,  inpmast_stock_number, inpmast_vin, arkona.db2_integer_to_date(date_in_invent) as date_acq,
  arkona.db2_integer_to_date(date_delivered) as date_del, row_from_date, row_thru_date
from arkona.xfm_inpmast
where inpmast_vin = '1GCVKREC1FZ336123'
order by row_From_date

select status,  inpmast_stock_number, inpmast_vin, arkona.db2_integer_to_date(date_in_invent) as date_acq,
  arkona.db2_integer_to_date(date_delivered) as date_del, row_from_date, row_thru_date
from arkona.xfm_inpmast
where inpmast_stock_number = 'H15151L'

select * from arkona.ext_bopname where bopname_search_name like 'rude, m%'

select * from arkona.xfm_bopname where bopname_search_name like 'rude, m%'
--------------------------------------------------------------------------------------
--/> L vehicles, sale_type Retail identify sale_subtype of Lease Buyout
--------------------------------------------------------------------------------------



--------------------------------------------------------------------------------------
--< board_id (from sales_board) vs record_key (bopmast)
--------------------------------------------------------------------------------------
-- 14851 retail sales, 121 that don't match
select a.*, b.record_key 
from cu.used_1 a 
left join arkona.ext_bopmast b on a.stock_number = b.bopmast_stock_number
where  a.bopmast_id <> b.record_key::citext
  and a.sale_type = 'retail'

select record_key, record_status, record_type, sale_type, bopmast_Stock_number, bopmast_vin, date_capped, row_from_Date, row_thru_date, a.buyer_number, b.bopname_search_name 
from arkona.xfm_bopmast a
left join arkona.ext_bopname b on a.buyer_number = b.bopname_record_key
  and a.bopmast_company_number = b.bopname_company_number
where record_key in (49007,49178)



select distinct a.*, d.customer_name, b.record_key, e.bopname_search_name, b.record_Status
from cu.used_1 a 
left join arkona.ext_bopmast b on a.stock_number = b.bopmast_stock_number
left join board.sales_board c on a.stock_number = c.stock_number
  and c.board_type_key = 5
  and not c.is_Deleted
left join board.daily_board d on c.board_id = d.board_id
left join arkona.ext_bopname e on b.bopmast_company_number = e.bopname_company_number
  and b.buyer_number = e.bopname_record_key
where  a.bopmast_id <> b.record_key::citext
  and a.sale_type = 'retail'
order by a.stock_number  

-- all the above stuff gets too complicated, this is more of a bottom line
-- only 19 where deal_number does not exists in xfm_bopmast
select * 
from cu.used_1 a
where sale_type = 'Retail'
	and not exists (
		select 1
		from arkona.xfm_bopmast
		where record_key = a.bopmast_id::integer)


  		
--------------------------------------------------------------------------------------
--/> board_id (from sales_board) vs record_key (bopmast)
--------------------------------------------------------------------------------------

select * from cu.used_1 where right(stock_number, 1) = 'L' and sale_subtype = 'retail'

-- all these remaining (409) have 0 days avail
select * from cu.tmp_lease_buyout where comment is null and sale_type = 'retail' and primary_salespers = 'HSE' and days_avail = 0 and front_gross <> 0

select b.*
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
where a.stocknumber = 'h16378l'

-- this is a fucking mess
select record_key, record_status, record_type, sale_type, bopmast_Stock_number, bopmast_vin, date_capped, a.row_from_Date, a.row_thru_date, a.buyer_number, b.bopname_search_name 
from arkona.xfm_bopmast a
left join arkona.xfm_bopname b on a.buyer_number = b.bopname_record_key
  and a.bopmast_company_number = b.bopname_company_number
where bopmast_vin in (select bopmast_vin from cu.tmp_lease_buyout where comment is null and sale_type = 'retail' and primary_salespers = 'HSE' and days_avail = 0 and front_gross <> 0)
order by bopmast_vin, row_from_date

--no inpmast_vin
select status,  inpmast_stock_number, inpmast_vin, arkona.db2_integer_to_date(date_in_invent) as date_acq,
  arkona.db2_integer_to_date(date_delivered) as date_del, row_from_date, row_thru_date
from arkona.xfm_inpmast
-- where inpmast_vin = '1C6SRFFTXKN816472' -- g42331l G42208AA in inpmast
where inpmast_vin = '1C6SRFJT8KN674002' -- G43520L  g443374A
order by row_From_date

select record_key, record_status, record_type, sale_type, bopmast_Stock_number, bopmast_vin, date_capped, a.row_from_Date, a.row_thru_date, a.buyer_number, b.bopname_search_name 
from arkona.xfm_bopmast a
left join arkona.xfm_bopname b on a.buyer_number = b.bopname_record_key
  and a.bopmast_company_number = b.bopname_company_number
where bopmast_vin  = '3GCUKREC3JG423705'
order by bopmast_vin, row_from_date

select * from cu.used_1 where stock_number = 'G42208AA'

-- just make them lease buyouts, somewhat less offensive than exclude

update cu.used_1 x
set sale_subtype = 'Lease Buyout'
where stock_number in ( -- 409 rows
  select stock_number 
  from cu.tmp_lease_buyout
  where comment is null 
    and sale_type = 'retail' 
		and primary_salespers = 'HSE'
		and sale_subtype = 'Retail');
-----------------------------------------------------------------------------------------------
--< WTF
-----------------------------------------------------------------------------------------------



-----------------------------------------------------------------------------------------------
--/> WTF
-----------------------------------------------------------------------------------------------