﻿-- ---------------------------------------------------------------------------
-- --< DDL
-- ---------------------------------------------------------------------------
-- 6/20/23
-- Greg:  spreadsheet is in files (Category Snapshot 2023-06-08.xlsx)
-- Ben is going to follow this email with a more detailed request but this should give you the gist of it.
-- 
-- The attached spreadsheet has a tab labeled “NewCategories”.
-- 
-- On the “NewCategories” sheet are cells with notes (red triangle in the corner).
-- 
-- We would like you to show the last six month’s sales (retail) for the models listed in the notes (separate query/sheet/whatever for each note).
-- 
-- @Ben — I’m assuming it’s sales for the market and you’re not worried about which store sold them. Yes?

-- 6/21
-- Ben:
-- On the NewCategories tab for each cell containing an R we need to know in the last six months what 
-- sales did we have from any of our stores that are a model in that category. 
-- 
-- One extra filter that needs to be applied is package. As-Is sales will all fall under the As-Is 
-- category under the Outlet column. Nice Care and Certified packages will be broke out under all 
-- the other categories we have on that tab. Below is a sample of what I need. The below example 
-- would be under the GM column for the row SUV-sm. The columns do not represent a single store. 
-- They represent the manufacturer so we are looking for the sales of that manufacturer over the 
-- last six months for those models that we are classifying in that category.
-- 
-- Date	Vehicle
-- 1/15/23	2015 Equinox
-- 1/18/23	2018 Terrain
-- 2/2/23	2021 Trax


----------------------------------------------------------------------------------------
-- drop table if exists cu.tmp_new_categories;
-- create table cu.tmp_new_categories (
--   category citext not null,
--   store citext not null,
--   model citext primary key);
-- comment on table cu.tmp_new_category_sales is 'table to store the new category information provided by greg/ben on spreadsheet Category Snapshot 2023-06-08.xlsx.
--       includes opportunistic models when they are listed, but the focus is on replenishment';  

-- alter table cu.tmp_new_categories
-- rename column store to manufacturer;
-- 
-- rename it so i can do a table of just the categories of interest with a seq
-- alter table cu.tmp_new_categories
-- rename to tmp_new_category_sales;

-- insert into cu.tmp_new_category_sales(category,store,model) values
-- ('SUV-SM','GM','trax'),
-- ('SUV-SM','GM','blazer'),
-- ('SUV-SM','GM','trailblazer'),
-- ('SUV-SM','GM','equinox'),
-- ('SUV-SM','GM','terrain'),
-- ('SUV-SM','GM','encore'),
-- ('SUV-SM','GM','envision'),
-- ('SUV-SM','GM','encore gx'),
-- ('SUV-SM','GM','captiva'),
-- ('SUV-SM','HN','hrv'),
-- ('SUV-SM','HN','rogue sport'),
-- ('SUV-SM','HN','kicks'),
-- ('SUV-SM','HN','murano'),
-- ('SUV-SM','TY','rav4'),
-- ('SUV-M','GM','traverse'),
-- ('SUV-M','GM','acadia'),
-- ('SUV-M','GM','enclave'),
-- ('SUV-M','HN','crv'),
-- ('SUV-M','HN','rogue'),
-- ('SUV-M','HN','passport'),
-- ('SUV-M','HN','pilot'),
-- ('SUV-M','TY','4runner'),
-- ('SUV-M','TY','land cruiser'),
-- ('SUV-M','TY','highlander'),
-- ('SUV-L/XL','GM','yukon'),
-- ('SUV-L/XL','GM','yukon xl'),
-- ('SUV-L/XL','GM','suburban'),
-- ('SUV-L/XL','GM','tahoe'),
-- ('SUV-L/XL','HN','armada'),
-- ('SUV-L/XL','TY','grand highlander'),
-- ('SUV-L/XL','TY','sequoia'),
-- ('Truck-M','GM','canyon'),
-- ('Truck-M','GM','colorado'),
-- ('Truck-M','HN','frontier'),
-- ('Truck-M','HN','ridgeline'),
-- ('Truck-M','TY','tacoma'),
-- ('Truck-L','GM','silverado 1500'),
-- ('Truck-L','GM','sierra 1500'),
-- ('Truck-L','HN','titan'),
-- ('Truck-L','TY','tundra'),
-- ('Truck-HD','GM','silverado 2500'),
-- ('Truck-HD','GM','silverado 3500'),
-- ('Truck-HD','GM','sierra 2500'),
-- ('Truck-HD','GM','sierra 3500'),
-- ('Sport','GM','corvette'),
-- ('Sport','GM','camaro'),
-- ('Sport','GM','ats-v'),
-- ('Sport','GM','cts-v'),
-- ('Sport','HN','350z'),
-- ('Sport','HN','370z'),
-- ('Sport','HN','s2000'),
-- ('Sport','HN','civic type r'),
-- ('Sport','TY','supra'),
-- ('Sport','TY','g86'),
-- ('XT5/6(SUV)','GM','xt5'),
-- ('XT5/6(SUV)','GM','xt6'),
-- ('Escalade(SUV)','GM','escalade'),
-- ('Escalade(SUV)','GM','escalade esv'),
-- ('Escalade(SUV)','GM','escalade ext');

07/07/2023 adding the additional models overlooked the first time

insert into cu.tmp_new_category_sales(category,manufacturer,model) values
('SUV-SM','GM','ENVISION (2.0L)'),
('SUV-SM','GM','ENVISION (2.5L)'),
('Truck-L','GM','SILVERADO 1500 DIESEL'),
('Truck-L','GM','SILVERADO 1500 LD'),
('Truck-L','GM','SILVERADO 1500 LIMITED'),
('Truck-L','GM','SILVERADO 1500 LTD'),
('Truck-L','GMC','SIERRA'),
('Truck-L','GMC','SIERRA 1500 DIESEL'),
('Truck-L','GMC','SIERRA 1500 LIMITED'),
('Truck-HD','GM','SILVERADO 2500HD'),
('Truck-HD','GM','SILVERADO 2500HD DIESEL'),
('Truck-HD','GM','SILVERADO 3500HD DIESEL'),
('Truck-HD','GM','SILVERADO 3500HD'),
('Truck-HD','GMC','SIERRA 2500HD'),
('Truck-HD','GMC','SIERRA 2500HD DIESEL'),
('Truck-HD','GMC','SIERRA 3500HD'),
('Truck-HD','GMC','SIERRA 3500HD DIESEL'),
('SUV-L/XL','GMC','YUKON DENALI'),
('SUV-M','HN','CR-V'),
('SUV-M','HN','CR-V HYBRID'),
('SUV-M','GMC','ACADIA LIMITED'),
('SUV-M','HN','ROGUE SELECT'),
('SUV-M','HN','ROGUE SV'),
('SUV-M','TY','4RUNNER (4.0L)'),
('SUV-SM','TY','RAV4 (2.5L)'),
('SUV-SM','TY','RAV4 HYBRID'),
('Truck-M','TY','TACOMA (3.5L)'),
('Truck-M','TY','TACOMA 4WD'),
('Truck-M','TY','TACOMA SR5'),
('Truck-M','TY','TACOMA TRD SPORT'),
('Truck-L','TY','TUNDRA (3.5L)'),
('Truck-L','TY','TUNDRA 4WD'),
('Truck-L','TY','TUNDRA 4WD TRUCK'),
('Truck-L','TY','TUNDRA (5.7L)'),
('Truck-L','TY','TUNDRA FFV (5.7L)');



drop table if exists cu.tmp_new_categories;
create table cu.tmp_new_categories (
  category citext primary key,
  seq integer );
comment on table cu.tmp_new_categories is 'partial list of the new categories with a seq for assembling greg/ben''s data request';

insert into cu.tmp_new_categories(category)
select distinct category from cu.tmp_new_category_sales;

select * from cu.tmp_new_categories

insert into cu.tmp_new_categories values('As-Is', 10);
-- ---------------------------------------------------------------------------
-- --/> DDL
-- ---------------------------------------------------------------------------

select * 
from cu.tmp_new_category_sales

39: GM
40: HN
41: RAO
42: TY

/*
ok, i f'd up, instead of starting over for take 2, i just modified a bunch of the queries
oh well
the issue now that has me spinning, where should a tundra (man TY) sold at GM show up
does not matter where it was sold, it should show up under Toyota

select * from cu.tmp_jon_1 where model = 'tundra'

so, only 6 tundras show up under toyota because 2 are as-is
*/

-- these queries are now configured to produce new_categories_sale_2.xlsx
drop table if exists cu.tmp_jon_1;
create table cu.tmp_jon_1 as
select bb.seq, aa.category, aa.manufacturer, aa.model, a.boarded_date, b.vehicle_model_year as model_year, 
	case a.store_key
		when 39 then 'GM'
		when 40 then 'HN'
		when 41 then 'RAO'
		when 42 then 'TY'
	end as sold_at, 
	used_vehicle_package, stock_number
from board.sales_board a
join board.daily_board b on a.board_id = b.board_id 
	and vehicle_type = 'U'
join cu.tmp_new_category_sales aa on b.vehicle_model = aa.model
join cu.tmp_new_categories bb on aa.category = bb.category
join board.board_types c on a.board_type_key = c.board_type_key 
	and c.board_sub_type = 'retail'
where boarded_date between '06/22/2022' and '06/21/2023'
    and not a.is_backed_on
		and c.board_sub_type = 'retail'
		and a.is_deleted = false
		and 
		  case 
		    when a.used_vehicle_package = 'none' then right(a.stock_number, 1) <> 'L' -- exclude lease buyouts
		    else true
		  end
group by bb.seq, aa.category, aa.manufacturer, aa.model, a.boarded_date, b.vehicle_model_year, store_key, used_vehicle_package, stock_number		  
order by bb.seq, aa.manufacturer, aa.model, a.boarded_date;      
comment on table cu.tmp_jon_1 is 'first shot at putting data together for greg and ben';

-- ok now
select stock_number 
from cu.tmp_jon_1
group by stock_number
having count(*) > 1


-- update package on IMWS vehicles
update cu.tmp_jon_1 x
set used_Vehicle_package = y.package
from (
	select a.*, split_part(c.typ, '_', 2) as package
	from cu.tmp_jon_1 a
	left join ads.ext_Vehicle_inventory_items b on a.stock_number = b.stocknumber
	left join ads.ext_Selected_recon_packages c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
	where used_vehicle_package = 'none') y
where x.stock_number = y.stock_number;

select used_Vehicle_package, count(*) from cu.tmp_jon_1 group by used_Vehicle_package

for OEMs: 'Nice Care','Nice','Factory Certified'
for As-is: 'As-Is','AsIs','MechanicsSpecial','Mechanics Special'


-- gm_1.csv
select a.seq, a.category, b.model, b.model_year, b.boarded_date
--   count(*) over (partition by b.model) as model_count,
--   count(*) over (partition by a.category) as category_count
from (
	select * 
	from cu.tmp_new_categories
	where seq between 1 and 9
	order by seq) a
left join (
  select * 
  from cu.tmp_jon_1
  where manufacturer = 'GM'
    and used_vehicle_package in ('Nice Care','Nice','Factory Certified')) b on a.category = b.category 
order by a.seq, b.model, b.boarded_date

-- gm_2.csv
select a.seq, a.category, b.model, count(*)
from (
	select * 
	from cu.tmp_new_categories
	where seq between 1 and 9
	order by seq) a
left join (
  select * 
  from cu.tmp_jon_1
  where manufacturer = 'GM'
    and used_vehicle_package in ('Nice Care','Nice','Factory Certified')) b on a.category = b.category 
group by a.seq, a.category, b.model  
order by a.seq, b.model

-- gm_3.csv
select a.seq, a.category, count(*)
from (
	select * 
	from cu.tmp_new_categories
	where seq between 1 and 9
	order by seq) a
left join (
  select * 
  from cu.tmp_jon_1
  where manufacturer = 'GM'
    and used_vehicle_package in ('Nice Care','Nice','Factory Certified')) b on a.category = b.category 
group by a.seq,a.category  
order by a.seq

-- hn_1.csv
select a.seq, a.category, b.model, b.model_year, b.boarded_date
--   count(*) over (partition by b.model) as model_count,
--   count(*) over (partition by a.category) as category_count
from (
	select * 
	from cu.tmp_new_categories
	where seq between 1 and 9
	order by seq) a
left join (
  select * 
  from cu.tmp_jon_1
  where manufacturer = 'HN'
    and used_vehicle_package in ('Nice Care','Nice','Factory Certified')) b on a.category = b.category 
order by a.seq, b.model, b.boarded_date

-- hn_2.csv
select a.seq, a.category, b.model, count(*)
from (
	select * 
	from cu.tmp_new_categories
	where seq between 1 and 9
	order by seq) a
left join (
  select * 
  from cu.tmp_jon_1
  where manufacturer = 'HN'
    and used_vehicle_package in ('Nice Care','Nice','Factory Certified')) b on a.category = b.category
group by a.seq, a.category, b.model  
order by a.seq, b.model

select * from cu.tmp_jon_1 where model in ('kicks','370z')

-- hn_3.csv
select a.seq, a.category, count(*)
from (
	select * 
	from cu.tmp_new_categories
	where seq between 1 and 9
	order by seq) a
left join (
  select * 
  from cu.tmp_jon_1
  where manufacturer = 'HN'
    and used_vehicle_package in ('Nice Care','Nice','Factory Certified')) b on a.category = b.category
group by a.seq,a.category  
order by a.seq

-- ty_1.csv
select a.seq, a.category, b.model, b.model_year, b.boarded_date
--   case
--     when model is null then null
--     else count(*) over (partition by b.model) 
--   end as model_count,
--   case
--     when model is null then null
--     else count(*) over (partition by a.category) 
--   end as category_count
from (
	select * 
	from cu.tmp_new_categories
	where seq between 1 and 9
	order by seq) a
left join (
  select * 
  from cu.tmp_jon_1
  where manufacturer = 'TY'
    and used_vehicle_package in ('Nice Care','Nice','Factory Certified')) b on a.category = b.category
order by a.seq, b.model, b.boarded_date       

-- ty2.csv
select a.seq, a.category, b.model, 
  case
    when model is null then null
    else count(*)
  end
from (
	select * 
	from cu.tmp_new_categories
	where seq between 1 and 9
	order by seq) a
left join (
  select * 
  from cu.tmp_jon_1
  where manufacturer = 'TY'
    and used_vehicle_package in ('Nice Care','Nice','Factory Certified')) b on a.category = b.category
group by a.seq, a.category, b.model  
order by a.seq, b.model

-- ty3.csv
select a.seq, a.category, count(*)
from (
	select * 
	from cu.tmp_new_categories
	where seq between 1 and 9
	order by seq) a
left join (
  select * 
  from cu.tmp_jon_1
  where manufacturer = 'TY'
    and used_vehicle_package in ('Nice Care','Nice','Factory Certified')) b on a.category = b.category
group by a.seq,a.category  
having count(*) > 1
order by a.seq

select * from cu.tmp_jon_1 where manufacturer = 'TY' and category =  'SUV-L/XL'


-- AS-IS
-- 06/23/23
-- jon: 
-- Ben 
-- I have come up with a question
-- As-Is
-- Include only replenishment models 
-- OR 
-- ALL As-Is vehicles sold retail in the last 6 months ?
-- 
-- Ben:
-- The package As-Is is what we are replenishing. 
-- There are no models we are replenishing in that category but rather a recon package. 
-- 
-- So yes ALL As-Is vehicles sold retail in the last 6 months

-- need a different base data set for as-is, no categories, recon package only
drop table if exists cu.tmp_jon_2;
create table cu.tmp_jon_2 as
select b.vehicle_make, b.vehicle_model, b.vehicle_model_year as model_year, 
	a.used_vehicle_package, a.stock_number, a.boarded_date 
from board.sales_board a
join board.daily_board b on a.board_id = b.board_id 
	and vehicle_type = 'U'
join board.board_types c on a.board_type_key = c.board_type_key 
	and c.board_sub_type = 'retail'
where boarded_date between '06/22/2022' and '06/21/2023'
    and a.used_vehicle_package not in ('nice care', 'factory certified','Pre Auction','Wholesale')
    and not a.is_backed_on
		and c.board_sub_type = 'retail'
		and a.is_deleted = false
		and 
		  case 
		    when a.used_vehicle_package = 'none' then right(a.stock_number, 1) <> 'L' -- exclude lease buyouts
		    else true
		  end		  
group by b.vehicle_make, b.vehicle_model, b.vehicle_model_year, a.used_vehicle_package, a.stock_number, a.boarded_date ;	  
-- order by a.used_vehicle_package 
comment on table cu.tmp_jon_2 is 'first shot at putting as-is data together for greg and ben';

select * from cu.tmp_jon_2

-- update package on IMWS vehicles
update cu.tmp_jon_2 x
set used_Vehicle_package = y.package
from (
	select a.*, split_part(c.typ, '_', 2) as package
	from cu.tmp_jon_2 a
	join ads.ext_Vehicle_inventory_items b on a.stock_number = b.stocknumber
	join ads.ext_Selected_recon_packages c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
	where used_vehicle_package = 'none' order by package) y
where x.stock_number = y.stock_number;

select used_vehicle_package, count(*) from cu.tmp_jon_2 group by used_Vehicle_package

packages: 'MechanicsSpecial','Mechanics Special','AsIs','As-Is'

-- asis_1.csv
select vehicle_make as make, vehicle_model as model, model_year, boarded_date
from cu.tmp_jon_2 
where used_vehicle_package in ('MechanicsSpecial','Mechanics Special','AsIs','As-Is')
order by vehicle_make, vehicle_model, boarded_date

-- asis_2.csv  model count
select vehicle_make as make, vehicle_model as model, count(*)
from cu.tmp_jon_2 
where used_vehicle_package in ('MechanicsSpecial','Mechanics Special','AsIs','As-Is')
group by make, model
order by count(*) desc



-- 06/22/23  sent new_categories_sale_1.xlsx to greg and ben
-- jon:
-- Gentlemen
-- look this over, please
-- For now, it is just GM
-- I am looking for feedback on presentation
-- Is this essentially what you need to see?
-- Greg, I know that you want more detail on the individual models, that will be coming, preferably with the canonical data
-- 
-- PS
-- Ben, if i understand correctly, anything sold at the Outlet should be moved to the As-is category? 
-- That was not done on this data, yet.  Not sure how that would fit in on a Manufacturer level report, 
-- maybe just exclude them from this? Or list AS-Is separately?
-- ?
-- 
-- Ben:
-- Yes the report works for me. The manufacturer category report should include the recon package Nice Care and Certified only. 
-- As-Is is a new category, but its probably the easiest one as I simply need this same spreadsheet but 
-- with all the As-Is vehicles. Store or location is not anything that should be considered in either of these spreadsheets. 
-- Does that make sense?

so,  remove all the as-is data from each manufacturer, create an As-is category and populate it (make, model, model_year, boarded_date * counts)

-- 07/07/23
Hey Jon! 
Can you send this again but this time is just one category and that category is for the whole market 
and it' all the sales that weren't included previously. So if I add those additional sales to the 
ones you already sent it would equal the total sold for that same period. 
Thanks!
Ben 

Jon: Sorry Ben, i dont understand at all

Ben:
The spreadsheet info that you sent had the sales in it based on the defined categories we had set. 
I am looking now for every other sale that wasnt covered in the new categories. 
So all of the Fords, Dodges, Fiats, BMWs every other sale that wasnt listed in the data you sent.


----------------------------------------------------------------------------------------------------------------
-- 07/06/2023
-- fuck me, doing these "others" revealed a bunch of models that got left out above, eg silverado 1500 ld, silverado 2500HD, etc
-- corrected above
----------------------------------------------------------------------------------------------------------------
drop table if exists cu.tmp_jon_3;
create table cu.tmp_jon_3 as
select -- bb.seq, aa.category, aa.manufacturer, aa.model, 
  b.vehicle_make as make, b.vehicle_model as model, b.vehicle_model_year as model_year, a.boarded_date
-- 	case a.store_key
-- 		when 39 then 'GM'
-- 		when 40 then 'HN'
-- 		when 41 then 'RAO'
-- 		when 42 then 'TY'
-- 	end as sold_at, 
-- 	used_vehicle_package, stock_number, 
-- select b.vehicle_make, b.vehicle_model, count(*)
from board.sales_board a
join board.daily_board b on a.board_id = b.board_id 
	and vehicle_type = 'U'
-- join cu.tmp_new_category_sales aa on b.vehicle_model = aa.model
-- join cu.tmp_new_categories bb on aa.category = bb.category
join board.board_types c on a.board_type_key = c.board_type_key 
	and c.board_sub_type = 'retail'
where boarded_date between '06/22/2022' and '06/21/2023'
	and not a.is_backed_on
	and c.board_sub_type = 'retail'
	and a.is_deleted = false
	and 
		case 
			when a.used_vehicle_package = 'none' then right(a.stock_number, 1) <> 'L' -- exclude lease buyouts
			else true
		end
  and not exists (
    select 1
    from cu.tmp_jon_1
    where stock_number = a.stock_number)
  and not exists (
    select 1
    from cu.tmp_jon_2
    where stock_number = a.stock_number)
group by a.boarded_date, b.vehicle_model_year, b.vehicle_make, b.vehicle_model, used_vehicle_package, stock_number		
order by make, model, boarded_date;     
comment on table cu.tmp_jon_3 is 'Bens request for all the remaining sales for the past year that were not categorized with the new categories';

-- other_1.csv
select * from cu.tmp_jon_3

-- other_2.csv  model count
select make, model, count(*)
from cu.tmp_jon_3 
group by make, model
order by count(*) desc