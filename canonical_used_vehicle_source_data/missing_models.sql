﻿E:\python_projects\vehicles\back_to_classifications.docx

-- running this query on model only, returns, the Jeep Wrangler 4xe
-- but including make returns: all except the jeep are pre 2000
-- 1999 GMC,Suburban
-- 1994 Plymouth,Voyager
-- 1998 Plymouth,Voyager
-- 1985 Toyota,Pickup
-- 2023 Jeep,Wrangler 4xe

select *  from cu.tmp_vehicles where make = 'gmc' and model = 'suburban'
select * 
from cu.tmp_vehicles a
where not exists (
  select 1
  from veh.Shape_size_classifications
  where model = a.model and make = a.make)

select * from veh.shape_size_classifications where model = 'voyager'

missing model Jeep Wrangler 4xe

select * from chr.divisions

select model
from chr2.models a
where division_id = 21
  and not exists (
    select 1
    from veh.shape_size_classifications
    where model = a.model)
group by a.model
order by a.model  

------------------------------------------------------------------------------------------------------------------------

select * 
from veh.makes_models a
join (
  select model, count(*)
  from veh.makes_models
  group by model
  having count(*) > 1) b on a.model = b.model

select * from chr2.model_years

select division, array_agg(model_year order by model_year) from chr2.divisions group by division order by division

------------------------------------------------------------------------------------------------------------------------
-- 11/11/23
select * from chr2.divisions order by division, model_year

-- ran this in ads_base_tables.py models()
select model_year, division_id
from chr2.divisions
where (
	(division = 'GMC' and model_year in (1999)) or
	(division = 'Plymouth' and model_year in (1994, 1998)) or
	(division = 'toyota' and model_year = 1985) or
	(division = 'jeep' and model_year in (2023)));

-- ran this in ads_base_tables.py style_ids()
select *
from chr2.models a
where not exists (
	select 1 
	from chr2.style_ids
	where model_id = a.model_id)       

------------------------------------------------------------------------------------------------------------------------------
11/13/23
remove GMC Forward Control Chassis, GMC Savana Camper Special, GMC Savana Special, GMC School Bus Chassis
from whatever turns out to be the appropriate tables in chr2, dont want to make nate classifiy these


-- can't delete from veh.models due to FK in veh.makes_models
-- but this worked 
-- and removes it from the Not Completed list in Vision Vehicle Classifications
delete 
-- select *
from veh.makes_models
where model in ('Forward control chassis','Savana Camper Special','Savana Special','School Bus Chassis')

select * from cu.tmp_vehicles where make = 'plymouth'

select * 
from veh.makes_models 
where make = 'plymouth'
