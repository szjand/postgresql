﻿01/07/24
step 5 is now done in E:\sql\postgresql\chrome_cvd\sql\parse_tmp_vehicles_2.sql

-- month end, afton has updated cu.used_3, i now need to update cu.tmp_vehicles
-- here are the candidates for build data
select a.stock_number, a.vin, b.make, b.model, b.year 
from cu.used_3 a
join arkona.ext_inpmast b on a.stock_number = b.inpmast_stock_number
join chr.build_data_coverage c on b.make = c.division
  and b.year between c.from_year and c.thru_year
where not exists (
  select 1
  from cu.tmp_vehicles
  where vin = a.vin)
and not exists (
  select 1
  from chr.build_data_describe_vehicle
  where vin = a.vin)  

-- and configured for the chrome query (currently using zeep_build_data_query.py)
-- 11/6/23: 144 vins, ran this first in zeep_build_data_query.py
select distinct a.vin  -- 131 vins
from cu.used_3 a
join arkona.ext_inpmast b on a.stock_number = b.inpmast_stock_number
join chr.build_data_coverage c on b.make = c.division
  and b.year between c.from_year and c.thru_year
where not exists (
  select 1
  from cu.tmp_vehicles
  where vin = a.vin)
and not exists (
  select 1
  from chr.build_data_describe_vehicle
  where vin = a.vin) 

-- limit to build and style_count = 1
-- this is the base query for the script just below
select distinct a.vin, d.source, d.style_count, d.the_date
from cu.used_3 a
join arkona.ext_inpmast b on a.stock_number = b.inpmast_stock_number
join chr.build_data_coverage c on b.make = c.division
  and b.year between c.from_year and c.thru_year
join chr.build_data_describe_vehicle d on a.vin = d.vin  
  and d.source = 'build'
  and d.style_count = 1
where not exists (
  select 1
  from cu.tmp_vehicles
  where vin = a.vin)
------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------
-- from .../canonical.../chrome_build_data1.sql
-- base table changed to the above
-- not sure how this happened, somehow went from 131 vins (above) to 187 vins, oh well, they are unique
-- 09/05/23 inserted 49 rows
-- 10/04/23 inserted 295 rows
-- 11/6/23 inserted 188 rows
-- E:\python_projects\chrome\zeep_build_data_query.py
insert into cu.tmp_vehicles
-- select vin from (
select a.vin, 
  (c ->>'modelYear')::integer as model_year,
  (c ->'division'->>'_value_1')::citext as make,
  (c ->'subdivision'->>'_value_1')::citext as subdivision,
  (c ->'model'->>'_value_1'::citext)::citext as model,
  coalesce(c ->>'trim', 'none')::citext as trim,
  case
    when c ->>'drivetrain' like 'Rear%' then 'RWD'
    when c->>'drivetrain' like 'Front%' then 'FWD'
    when c ->>'drivetrain' like 'Four%' then '4WD'
    when c ->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drive,  
  (c ->>'fleetOnly'::citext)::boolean as fleet_only,
  (c ->>'modelFleet'::citext)::boolean as model_fleet,
  (c ->>'mfrModelCode')::citext as model_code,
--   b.response->'vinDescription'->>'drivingWheels' as driving_wheels, -- adds nothing interesting, often null
  case
    when d.cab->>'_value_1' like 'Crew%' then 'crew' 
    when d.cab->>'_value_1' like 'Extended%' then 'double'  
    when d.cab->>'_value_1' like 'Regular%' then 'reg'  
    else 'n/a'   
  end as cab,
  case
    when e.bed->>'_value_1' like '%Bed' then substring(bed->>'_value_1', 1, position(' ' in bed->>'_value_1') - 1)
    else 'n/a'
  end as box_size,  
  case
    when (c ->'model'->>'_value_1'::citext)::citext in ('bolt', 'bolt ev') then 'n/a'
    else g->>'_value_1' 
  end as engine_displacement,
  f->>'cylinders' as cylinders, f->'fuelType'->>'_value_1' as fuel,
  case
    when  (c ->'division'->>'_value_1')::citext = 'Nissan' then 'automatic'
    when i->>0 is null then 'unknown'
    when i->>0 like '%SPEED MANUAL%' then 'manual'
    else 'automatic'
  end as transmission,  
  j->>'colorName' as ext_color, k->>'colorName' as interior,
  c->>'passDoors' as pass_doors,
  ((coalesce(b.response->'vinDescription'->>'builtMSRP', c->'basePrice'->>'msrp'))::numeric)::integer as msrp,
--   m->>'_value_1' as market_class,
  c->'marketClass'->>'_value_1' as market_class,
  b.response->'vinDescription'->>'bodyType' as body_type, c->>'altBodyType' as alt_body_type, 
  b.response->'vinDescription'->>'styleName' as style_name, c->>'altStyleName' as alt_style_name,
  (c->>'nameWoTrim'::citext)::citext as name_wo_trim,
  b.response->>'bestMakeName' as best_make_name,
  b.response->>'bestModelName' as best_model_name,
  b.response->>'bestTrimName' as best_trim_name,
  b.response->>'bestStyleName' as best_style_name,
  c->'stockImage'->>'url' as pic_url,
  b.source, (c ->>'id')::citext as chrome_style_id   
-- select a.vin  
-- select ((coalesce(b.response->'vinDescription'->>'builtMSRP', c->'basePrice'->>'msrp'))::numeric)::integer as msrp
from ( --select '1GB4KZC8XDF220206'::citext as vin) a
-- 	select distinct vin
-- 	from cu.used_2) a
	select distinct a.vin, d.source, d.style_count, d.the_date
	from cu.used_3 a
	join arkona.ext_inpmast b on a.stock_number = b.inpmast_stock_number
	join chr.build_data_coverage c on b.make = c.division
		and b.year between c.from_year and c.thru_year
	join chr.build_data_describe_vehicle d on a.vin = d.vin  
		and d.source = 'build'
		and d.style_count = 1
	where not exists (
		select 1
		from cu.tmp_vehicles
		where vin = a.vin)) a
join chr.build_data_describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'style') c on true
left join jsonb_array_elements(c->'bodyType') with ordinality as d(cab) on true
  and d.ordinality =  2
left join jsonb_array_elements(c->'bodyType') with ordinality as e(bed) on true
  and e.ordinality =  1    
left join jsonb_array_elements(b.response->'engine') as f on true  
  and (f->'installed'->>'cause' = 'OptionCodeBuild' or coalesce(f->'installed'->>'cause', 'xxx') = 'VIN')
left join jsonb_array_elements(f->'displacement'->'value') as g on true
  and g ->>'unit' = 'liters'
left join jsonb_array_elements(b.response->'factoryOption') h on true
  and h->'header'->>'_value_1' = 'TRANSMISSION'
  and h->'installed'->>'cause' in ('VIN','OptionCodeBuild')
left join jsonb_array_elements(h->'description') i on true
left join jsonb_array_elements(b.response->'exteriorColor') as j on true
  and j->'installed'->>'cause' in ('RelatedColor','ExteriorColorBuild', 'OptionCodeBuild','InteriorColorBuild')    
left join jsonb_array_elements(b.response->'interiorColor') as k on true --interior
  and k->'installed'->>'cause' in ('RelatedColor','ExteriorColorBuild', 'OptionCodeBuild','InteriorColorBuild')
left join jsonb_array_elements(k->'description') as l on true
-- left join jsonb_array_elements(b.response->'vinDescription'->'marketClass') m on true 
where b.source = 'build'  
  and b.style_count = 1
  and not exists (
		select 1
		from cu.tmp_vehicles
		where vin = a.vin)
-- ) x group by vin having count(*) > 1
order by c ->'model'->>'_value_1', c ->>'modelYear'

------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------

-- from .../canonical.../chrome_step_2.sql
-- again from build data, but this time including those that show source = catalog
-- the difference here is filtered on chrome_Style_id matching inpmast
-- the build data version (excluding Ford)
-- 09/5/23 no rows inserted
-- 10/04/23 3 rows inserted
-- 11/6/23 5 rows inserted
-- E:\python_projects\chrome\zeep_build_data_query.py
insert into cu.tmp_vehicles
select a.vin, 
  (c ->>'modelYear')::integer as model_year,
  (c ->'division'->>'_value_1')::citext as make,
  (c ->'subdivision'->>'_value_1')::citext as subdivision,
  (c ->'model'->>'_value_1'::citext)::citext as model,
  coalesce(c ->>'trim', 'none')::citext as trim,
  case
    when c ->>'drivetrain' like 'Rear%' then 'RWD'
    when c->>'drivetrain' like 'Front%' then 'FWD'
    when c ->>'drivetrain' like 'Four%' then '4WD'
    when c ->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drive,  
  (c ->>'fleetOnly'::citext)::boolean as fleet_only,
  (c ->>'modelFleet'::citext)::boolean as model_fleet,
  (c ->>'mfrModelCode')::citext as model_code,
--   b.response->'vinDescription'->>'drivingWheels' as driving_wheels, -- adds nothing interesting, often null
  case
    when d.cab->>'_value_1' like 'Crew%' then 'crew' 
    when d.cab->>'_value_1' like 'Extended%' then 'double'  
    when d.cab->>'_value_1' like 'Regular%' then 'reg'  
    else 'n/a'   
  end as cab,
  case
    when e.bed->>'_value_1' like '%Bed' then substring(bed->>'_value_1', 1, position(' ' in bed->>'_value_1') - 1)
    else 'n/a'
  end as box_size,  
  case
    when (c ->'model'->>'_value_1'::citext)::citext in ('bolt', 'bolt ev') then 'n/a'
    else g->>'_value_1' 
  end as engine_displacement,
  f->>'cylinders' as cylinders, f->'fuelType'->>'_value_1' as fuel,
  case
    when  (c ->'division'->>'_value_1')::citext = 'Nissan' then 'automatic'
    when i->>0 is null then 'unknown'
    when i->>0 like '%SPEED MANUAL%' then 'manual'
    else 'automatic'
  end as transmission,  
  j->>'colorName' as ext_color, k->>'colorName' as interior,
  c->>'passDoors' as pass_doors,
  ((coalesce(b.response->'vinDescription'->>'builtMSRP', c->'basePrice'->>'msrp'))::numeric)::integer as msrp,
--   m->>'_value_1' as market_class,
  c->'marketClass'->>'_value_1' as market_class,
  b.response->'vinDescription'->>'bodyType' as body_type, c->>'altBodyType' as alt_body_type, 
  b.response->'vinDescription'->>'styleName' as style_name, c->>'altStyleName' as alt_style_name,
  (c->>'nameWoTrim'::citext)::citext as name_wo_trim,
  b.response->>'bestMakeName' as best_make_name,
  b.response->>'bestModelName' as best_model_name,
  b.response->>'bestTrimName' as best_trim_name,
  b.response->>'bestStyleName' as best_style_name,
  c->'stockImage'->>'url' as pic_url,
  b.source, (c ->>'id')::citext as chrome_style_id
-- select a.vin  
-- select ((coalesce(b.response->'vinDescription'->>'builtMSRP', c->'basePrice'->>'msrp'))::numeric)::integer as msrp
from ( --select '1GB4KZC8XDF220206'::citext as vin) a
	select a.vin,  (c ->>'id')::citext as style_id
	from cu.used_3 a
	join chr.build_data_describe_vehicle b on a.vin = b.vin
	join jsonb_array_elements(b.response->'style') c on true
	left join arkona.ext_inpmast d on a.vin = d.inpmast_vin
	where not exists (
		select 1
		from cu.tmp_vehicles
		where vin = a.vin)  
		and ((c ->>'id')::bigint = d.chrome_style_id or (c ->>'id')::citext = d.key_to_cap_explosion_data)
		and c ->'division'->>'_value_1' <> 'Ford'
		and d.chrome_style_id <> 0 -- gets rid of the dups
	group by a.vin,  (c ->>'id')::citext) a
join chr.build_data_describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'style') c on true
left join jsonb_array_elements(c->'bodyType') with ordinality as d(cab) on true
  and d.ordinality =  2
left join jsonb_array_elements(c->'bodyType') with ordinality as e(bed) on true
  and e.ordinality =  1    
left join jsonb_array_elements(b.response->'engine') as f on true  
  and (f->'installed'->>'cause' = 'OptionCodeBuild' or coalesce(f->'installed'->>'cause', 'xxx') = 'VIN')
left join jsonb_array_elements(f->'displacement'->'value') as g on true
  and g ->>'unit' = 'liters'
left join jsonb_array_elements(b.response->'factoryOption') h on true
  and h->'header'->>'_value_1' = 'TRANSMISSION'
  and h->'installed'->>'cause' in ('VIN','OptionCodeBuild')
left join jsonb_array_elements(h->'description') i on true
left join jsonb_array_elements(b.response->'exteriorColor') as j on true
  and j->'installed'->>'cause' in ('RelatedColor','ExteriorColorBuild', 'OptionCodeBuild','InteriorColorBuild')    
left join jsonb_array_elements(b.response->'interiorColor') as k on true --interior
  and k->'installed'->>'cause' in ('RelatedColor','ExteriorColorBuild', 'OptionCodeBuild','InteriorColorBuild')
left join jsonb_array_elements(k->'description') as l on true
-- left join jsonb_array_elements(b.response->'vinDescription'->'marketClass') m on true 
-- where b.source = 'build'  
where a.style_id = (c ->>'id')::citext

------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------
-- 11/6/23 that leaves 66 vehicles in cu.used_3 not in cu.tmp_vehicles and not in chr.build or chr.describe
select a.stock_number, a.vin, b.make, b.model, b.year
from cu.used_3 a
join arkona.ext_inpmast b on a.stock_number = b.inpmast_stock_number
-- join chr.describe_vehicle c on a.vin = c.vin
where not exists (
  select 1
  from cu.tmp_vehicles
  where vin = a.vin)
and not exists (
  select 1
  from chr.build_data_describe_vehicle
  where vin = a.vin) 
and not exists (
  select 1
  from chr.describe_vehicle
  where vin = a.vin) 
order by make  

------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------
-- need to populate chr.describe_vehicle for non build data candidate

select a.stock_number, a.vin, b.make, b.model, b.year 
from cu.used_3 a
join arkona.ext_inpmast b on a.stock_number = b.inpmast_stock_number
left join chr.build_data_coverage c on b.make = c.division
  and b.year between c.from_year and c.thru_year
where not exists (
  select 1
  from cu.tmp_vehicles
  where vin = a.vin)
and c.division is null -- exclude candidates for build data  
and not exists (
  select 1
  from chr.describe_vehicle
  where vin = a.vin) 
and make not in ('CHEVORLET','KAWASAKI','HARLEY','HARLEY-DAVIDSON','YAMAHA', 'HARLEY DAVIDSON')
order by b.make  
  
-- and configured for the chrome query (currently using "E:\python_projects\chrome\describe_vehicle.py")
-- 11/6/23 48 rows
select distinct a.vin  -- 483 vins
from cu.used_3 a
join arkona.ext_inpmast b on a.stock_number = b.inpmast_stock_number
left join chr.build_data_coverage c on b.make = c.division
  and b.year between c.from_year and c.thru_year
where not exists (
  select 1
  from cu.tmp_vehicles
  where vin = a.vin)
and c.division is null -- exclude candidates for build data
and not exists (
  select 1
  from chr.describe_vehicle
  where vin = a.vin) 
and make not in ('CHEVORLET','KAWASAKI','HARLEY','HARLEY-DAVIDSON','YAMAHA','HARLEY DAVIDSON')
and a.vin not in ('1HD1KED12HB640756','1HD1KHM18FB688096','JS1GT76A252103098')



------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------
  

-- query from .../canonical/.../chrome_catalog_1.sql
-- 08/04/23 inserted 292 rows based on style_count = 1
-- lots more work to unravel those with multiple styles, but for now, back to make model classifications
-- 09/05/23 only 23 inserts from this one
-- 10/04/23 115 rows inserted
-- 11/6/23 53 rows inserted
-- E:\python_projects\chrome\describe_vehicle.py
insert into cu.tmp_vehicles
select a.vin, 
  (c->'attributes'->>'modelYear')::integer as model_year,
  (c->'division'->>'$value')::citext as make,
  (c->'subdivision'->>'$value')::citext as subdivision,
  (c->'model'->>'$value'::citext)::citext as model,
  coalesce(c->'attributes'->>'trim', 'none')::citext as trim,
  case
    when c->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
    when c->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
    when c->'attributes'->>'drivetrain' like 'Four%' then '4WD'
    when c->'attributes'->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drive,
  (c->'attributes'->>'fleetOnly'::citext)::boolean as fleet_only,
  (c->'attributes'->>'modelFleet'::citext)::boolean as model_fleet,  
  (c->'attributes'->>'mfrModelCode')::citext as chr_model_code,  
  case
    when d.cab->>'$value' like 'Crew%' then 'crew' 
    when d.cab->>'$value' like 'Extended%' then 'double'  
    when d.cab->>'$value' like 'Regular%' then 'reg'  
    else 'n/a'   
  end as cab,    
  case
    when e.bed->>'$value' like '%Bed' then substring(e.bed->>'$value', 1, position(' ' in e.bed->>'$value') - 1)
    else 'n/a'
  end as box_size,      
  coalesce(g.displacement->>'$value', 'n/a') as engine_displacement,
  f->>'cylinders' as cylinders, f->'fuelType'->>'$value' as fuel,
  null as transmission, null as ext_color, null as interior,
  c->'attributes'->>'passDoors' as pass_doors,
  null as msrp,
  c->'marketClass'->>'$value' as market_class,
  b.response->'vinDescription'->'attributes'->>'bodyType' as body_type,
  c->'attributes'->>'altBodyType' as alt_body_type,
  b.response->'vinDescription'->'attributes'->>'styleName' as style_name,
  null as alt_style_name, 
  c->'attributes'->>'nameWoTrim' as name_wo_trim,
  b.response->'attributes'->>'bestMakeName' as best_make_name,
  b.response->'attributes'->>'bestModelName' as best_model_name,
  b.response->'attributes'->>'bestTrimName' as best_trim_name,
  b.response->'attributes'->>'bestStyleName' as best_style_name,
  c->'stockImage'->'attributes'->>'url' as pic_url,
  'Catalog' as source, (c ->'attributes'->>'id')::citext as chrome_style_id
-- select a.vin 
from ( -- select '2HKRW2H54NH626260'::citext as vin) a
	select distinct a.vin-- 2322
	from cu.used_3 a
	where not exists (
		select 1
		from chr.build_Data_describe_vehicle
		where vin = a.vin)) a
join chr.describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'style') c on true
left join jsonb_array_elements(c->'bodyType') with ordinality as d(cab) on true
  and d.ordinality =  2
left join jsonb_array_elements(c->'bodyType') with ordinality as e(bed) on true
  and e.ordinality =  1    
left join jsonb_array_elements(b.response->'engine') as f(engine) on true  
  and f.engine ? 'installed'
left join jsonb_array_elements(f.engine->'displacement'->'value') as g(displacement) on true
  and g.displacement ->'attributes'->>'unit' = 'liters'  
where b.style_count = 1  
  and not exists (
    select 1
    from cu.tmp_vehicles
    where vin = a.vin)


-- select * 
-- delete
-- from cu.tmp_vehicles where make = 'maserati'

-- try to get some more decipherable data using inpmast with model code or key_to_cap_explosion_data (chrome style_id)
select a.vin, b.year, b.make, b.model, b.model_code, b.chrome_style_id, b.key_to_cap_explosion_data
from cu.used_3 a
left join arkona.ext_inpmast b on a.vin = b.inpmast_vin
WHERE NOT exists ( 
  select 1
  from cu.tmp_vehicles
  where vin = a.vin)
and vin = '1HGFA16807L031382'  

select * from chr.describe_vehicle where vin = '1HGFA16807L031382' 

select a.vin, b.year, b.make, b.model, b.model_code, b.chrome_style_id, b.key_to_cap_explosion_data
from cu.used_3 a
left join arkona.ext_inpmast b on a.vin = b.inpmast_vin
WHERE NOT exists ( 
  select 1
  from cu.tmp_vehicles
  where vin = a.vin)
and b.model_code is not null  
and b.make <> 'ford'  -- model code does not disambiguate fords
order by make, model
-- and b.key_to_cap_explosion_data = '316892'

select * from chr2.style_ids where style_id = '316892'
select * from chr.describe_vehicle where vin = '1FTEX1E84AKB46591'

select a.vin, b.year, b.make, b.model, b.model_code, b.chrome_style_id, b.key_to_cap_explosion_data
from cu.used_3 a
-- join chr.describe_vehicle aa on a.vin = aa.vin
left join arkona.ext_inpmast b on a.vin = b.inpmast_vin
WHERE NOT exists ( 
  select 1
  from cu.tmp_vehicles
  where vin = a.vin)
and b.key_to_cap_explosion_data is not null  