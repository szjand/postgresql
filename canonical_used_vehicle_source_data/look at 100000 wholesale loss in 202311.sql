﻿drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select store, page, line, line_label, control, sum(unit_count) as unit_count, description
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 202311-----------------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 202311 ---------------------------------------------------------------------
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
--         or
--         (b.page = 17 and b.line between 1 and 20))-- f/i counts don't make sense
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by store, page, line, line_label, control, description
order by store, page, line;

-- 174 units
select * from step_1
where page = 16
  and line > 5


select control, 
  (sum(-amount) filter (where col = 1))::integer as sale,
  (sum(amount) filter (where col = 3))::integer as cogs,
  sum(-amount)::integer as front_gross
from cu.uc_front_gross_detail 
where year_month = 202311 
  and line > 5 
group by control 
having sum(-amount) < 0
order by control

-- base query from "E:\sql\postgresql\canonical_used_vehicle_source_data\reconcile_fs_gross_with_query.sql"
drop table if exists gross cascade;
create temp table gross as
select * -- sum(front_gross)
from (
	select store, control, description,
		(sum(-amount) filter (where col = 1))::integer as sale,
		(sum(amount) filter (where col = 3))::integer as cogs,
		sum(-amount)::integer as front_gross
	from (
		select 
			case e.store_code 
				when 'RY1' then 'GM'::citext
				when 'RY2' then 'HN'::citext
				when 'RY8' then 'TY'::citext
				else 'XXX'::citext
			end as store, b.year_month, d.description, a.control, a.doc, a.amount, e.line, e.col, e.line_label, e.account, e.account_type, journal_code
		from fin.fact_gl a
		join dds.dim_date b on a.date_key = b.date_key
			and b.year_month = 202311 --------------------------------------------------------------
		join fin.dim_account c on a.account_key = c.account_key
			and c.current_row
		join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key  
		join cu.uc_front_gross_accounts e on c.account = e.account
			and e.line > 5
		join fin.dim_journal f on a.journal_key = f.journal_key
		where a.post_status = 'Y') x
	group by store, control, description) x
where front_gross < -100
  and sale is not null
order by description;
create unique index on gross(control);

select fullname, sum(front_gross), count(*), sum(front_gross)/count(*) from (
select a.*, c.vehicleevaluatorid, d.fullname 
from gross a
left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
left join ads.ext_vehicle_evaluations c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
left join ads.ext_people d on c.vehicleevaluatorid = d.partyid
-- order by d.fullname, a.front_gross
) x group by fullname