﻿select distinct a.vin, a.source, source
from chr.build_data_describe_vehicle a
join cu.used_2 b on a.vin = b.vin
left join chr.describe_vehicle c on a.vin = c.vin
where a.source <> 'build'
  and a.style_count = 1

select response->'responseStatus'->>'description', count(*)
from chr.build_data_describe_Vehicle 
group by response->'responseStatus'->>'description'
Unsuccessful,14
Successful,25613
ConditionallySuccessful,116

select vin, response->'responseStatus'-->>'description'
from chr.build_data_describe_Vehicle 
where response->'responseStatus'->>'description' = 'ConditionallySuccessful'

select response->'responseStatus'->'attributes'->>'description', count(*)
from chr.describe_Vehicle 
group by response->'responseStatus'->'attributes'->>'description'
Unsuccessful,32
Successful,38281
ConditionallySuccessful,28

select *,response->'responseStatus'->'attributes'->>'description'
from chr.describe_Vehicle 
where response->'responseStatus'->'attributes'->>'description' <> 'Successful'


-- compare catalog data from build data vs describe vehicle
select * from chr.build_data_describe_vehicle where vin = '3GTP2VE73CG155217'
select * from chr.describe_vehicle where vin = '3GTP2VE73CG155217'

select 'build' as source, a.vin, 
  (c ->>'modelYear')::integer as model_year,
  (c ->'division'->>'_value_1')::citext as make,
  (c ->'subdivision'->>'_value_1')::citext as subdivision,
  (c ->'model'->>'_value_1'::citext)::citext as model,
  coalesce(c ->>'trim', 'none')::citext as trim,
  case
    when c ->>'drivetrain' like 'Rear%' then 'RWD'
    when c->>'drivetrain' like 'Front%' then 'FWD'
    when c ->>'drivetrain' like 'Four%' then '4WD'
    when c ->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drive,  
  (c ->>'fleetOnly'::citext)::boolean as fleet_only,
  (c ->>'modelFleet'::citext)::boolean as model_fleet,
  (c ->>'mfrModelCode')::citext as model_code,
--   b.response->'vinDescription'->>'drivingWheels' as driving_wheels, -- adds nothing interesting, often null
  case
    when d.cab->>'_value_1' like 'Crew%' then 'crew' 
    when d.cab->>'_value_1' like 'Extended%' then 'double'  
    when d.cab->>'_value_1' like 'Regular%' then 'reg'  
    else 'n/a'   
  end as cab,
  case
    when e.bed->>'_value_1' like '%Bed' then substring(bed->>'_value_1', 1, position(' ' in bed->>'_value_1') - 1)
    else 'n/a'
  end as box_size,  
  case
    when (c ->'model'->>'_value_1'::citext)::citext in ('bolt', 'bolt ev') then 'n/a'
    else g->>'_value_1' 
  end as engine_displacement,
  f->>'cylinders' as cylinders, f->'fuelType'->>'_value_1' as fuel,
  case
    when  (c ->'division'->>'_value_1')::citext = 'Nissan' then 'automatic'
    when i->>0 is null then 'unknown'
    when i->>0 like '%SPEED MANUAL%' then 'manual'
    else 'automatic'
  end as transmission,  
  j->>'colorName' as ext_color, k->>'colorName' as interior,
  c->>'passDoors' as pass_doors,
  ((coalesce(b.response->'vinDescription'->>'builtMSRP', c->'basePrice'->>'msrp'))::numeric)::integer as msrp,
--   m->>'_value_1' as market_class,
  c->'marketClass'->>'_value_1' as market_class,
  b.response->'vinDescription'->>'bodyType' as body_type, c->>'altBodyType' as alt_body_type, 
  b.response->'vinDescription'->>'styleName' as style_name, c->>'altStyleName' as alt_style_name,
  (c->>'nameWoTrim'::citext)::citext as name_wo_trim,
  b.response->>'bestMakeName' as best_make_name,
  b.response->>'bestModelName' as best_model_name,
  b.response->>'bestTrimName' as best_trim_name,
  b.response->>'bestStyleName' as best_style_name,
  c->'stockImage'->>'url' as pic_url,
  b.source, (c ->>'id')::citext as chrome_style_id   
-- select a.vin  
-- select ((coalesce(b.response->'vinDescription'->>'builtMSRP', c->'basePrice'->>'msrp'))::numeric)::integer as msrp
from (select '3GTP2VE73CG155217'::citext as vin) a
-- 	select distinct vin
-- 	from cu.used_2) a
join chr.build_data_describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'style') c on true
left join jsonb_array_elements(c->'bodyType') with ordinality as d(cab) on true
  and d.ordinality =  2
left join jsonb_array_elements(c->'bodyType') with ordinality as e(bed) on true
  and e.ordinality =  1    
left join jsonb_array_elements(b.response->'engine') as f on true  
  and (f->'installed'->>'cause' = 'OptionCodeBuild' or coalesce(f->'installed'->>'cause', 'xxx') = 'VIN')
left join jsonb_array_elements(f->'displacement'->'value') as g on true
  and g ->>'unit' = 'liters'
left join jsonb_array_elements(b.response->'factoryOption') h on true
  and h->'header'->>'_value_1' = 'TRANSMISSION'
  and h->'installed'->>'cause' in ('VIN','OptionCodeBuild')
left join jsonb_array_elements(h->'description') i on true
left join jsonb_array_elements(b.response->'exteriorColor') as j on true
  and j->'installed'->>'cause' in ('RelatedColor','ExteriorColorBuild', 'OptionCodeBuild','InteriorColorBuild')    
left join jsonb_array_elements(b.response->'interiorColor') as k on true --interior
  and k->'installed'->>'cause' in ('RelatedColor','ExteriorColorBuild', 'OptionCodeBuild','InteriorColorBuild')
left join jsonb_array_elements(k->'description') as l on true
-- left join jsonb_array_elements(b.response->'vinDescription'->'marketClass') m on true 
-- where b.source = 'build'  
--   and style_count = 1
union
select 'non-build', a.vin, 
  (c->'attributes'->>'modelYear')::integer as model_year,
  (c->'division'->>'$value')::citext as make,
  (c->'subdivision'->>'$value')::citext as subdivision,
  (c->'model'->>'$value'::citext)::citext as model,
  coalesce(c->'attributes'->>'trim', 'none')::citext as trim,
  case
    when c->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
    when c->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
    when c->'attributes'->>'drivetrain' like 'Four%' then '4WD'
    when c->'attributes'->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drive,
  (c->'attributes'->>'fleetOnly'::citext)::boolean as fleet_only,
  (c->'attributes'->>'modelFleet'::citext)::boolean as model_fleet,  
  (c->'attributes'->>'mfrModelCode')::citext as chr_model_code,  
  case
    when d.cab->>'$value' like 'Crew%' then 'crew' 
    when d.cab->>'$value' like 'Extended%' then 'double'  
    when d.cab->>'$value' like 'Regular%' then 'reg'  
    else 'n/a'   
  end as cab,    
  case
    when e.bed->>'$value' like '%Bed' then substring(e.bed->>'$value', 1, position(' ' in e.bed->>'$value') - 1)
    else 'n/a'
  end as box_size,      
  coalesce(g.displacement->>'$value', 'n/a') as engine_displacement,
  f->>'cylinders' as cylinders, f->'fuelType'->>'$value' as fuel,
  null as transmission, null as ext_color, null as interior,
  c->'attributes'->>'passDoors' as pass_doors,
  null as msrp,
  c->'marketClass'->>'$value' as market_class,
  b.response->'vinDescription'->'attributes'->>'bodyType' as body_type,
  c->'attributes'->>'altBodyType' as alt_body_type,
  b.response->'vinDescription'->'attributes'->>'styleName' as style_name,
  null as alt_style_name, 
  c->'attributes'->>'nameWoTrim' as name_wo_trim,
  b.response->'attributes'->>'bestMakeName' as best_make_name,
  b.response->'attributes'->>'bestModelName' as best_model_name,
  b.response->'attributes'->>'bestTrimName' as best_trim_name,
  b.response->'attributes'->>'bestStyleName' as best_style_name,
  c->'stockImage'->'attributes'->>'url' as pic_url,
  'Catalog' as source, (c ->'attributes'->>'id')::citext as chrome_style_id    
-- select a.vin 
from ( select '3GTP2VE73CG155217'::citext as vin) a
-- 	select distinct a.vin-- 2322
-- 	from cu.used_2 a
-- 	where not exists (
-- 		select 1
-- 		from chr.build_Data_describe_vehicle
-- 		where vin = a.vin
-- 		  and source = 'build'
-- 		  and style_count = 1)) a
join chr.describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'style') c on true
left join jsonb_array_elements(c->'bodyType') with ordinality as d(cab) on true
  and d.ordinality =  2
left join jsonb_array_elements(c->'bodyType') with ordinality as e(bed) on true
  and e.ordinality =  1    
left join jsonb_array_elements(b.response->'engine') as f(engine) on true  
  and f.engine ? 'installed'
left join jsonb_array_elements(f.engine->'displacement'->'value') as g(displacement) on true
  and g.displacement ->'attributes'->>'unit' = 'liters'  
where b.style_count = 1   