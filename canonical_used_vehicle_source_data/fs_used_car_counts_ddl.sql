﻿/*
finally, put the damned counts in a table
*/

drop table if exists cu.financial_statement_used_counts;
create table if not exists cu.financial_statement_used_counts(
  store citext not null,
  year_month integer not null,
  sale_type citext not null,
  the_count integer not null,
  gross integer not null,
  primary key(store,year_month,sale_type));
comment on table cu.financial_statement_used_counts is 'transcribed used car counts and gross from the financial statements, 201801 thru 202304, Toyota starts with 202208';  

select * from cu.financial_statement_used_counts where year_month = 202310

-- 06/02/23 change stores to GM, HN & TY
update cu.financial_statement_used_counts
set store = 'GM'
where store = 'RY1';
update cu.financial_statement_used_counts
set store = 'HN'
where store = 'RY2';
update cu.financial_statement_used_counts
set store = 'TY'
where store = 'RY8';

-- insert 202312 values
insert into cu.financial_statement_used_counts values
('GM',202312,'retail',148,-71881),
('GM',202312,'wholesale',90,-105190),
('HN',202312,'retail',45,31314),
('HN',202312,'wholesale',45,-36680),
('TY',202312,'retail',37,35796),
('TY',202312,'wholesale',26,-18565);

-- insert 202311 values
insert into cu.financial_statement_used_counts values
('GM',202311,'retail',179,142984),
('GM',202311,'wholesale',93,-62122),
('HN',202311,'retail',49,15516),
('HN',202311,'wholesale',44,-18888),
('TY',202311,'retail',31,25621),
('TY',202311,'wholesale',37,-21999);

-- insert 202310 values
insert into cu.financial_statement_used_counts values
('GM',202310,'retail',146,235711),
('GM',202310,'wholesale',59,-8170),
('HN',202310,'retail',52,65811),
('HN',202310,'wholesale',33,2596),
('TY',202310,'retail',35,60527),
('TY',202310,'wholesale',20,-6361);

-- insert 202309 values
insert into cu.financial_statement_used_counts values
('GM',202309,'retail',156,153176),
('GM',202309,'wholesale',62,-26454),
('HN',202309,'retail',47,36450),
('HN',202309,'wholesale',28,-9868),
('TY',202309,'retail',48,69672),
('TY',202309,'wholesale',10,-13211);

-- insert 202308 values
insert into cu.financial_statement_used_counts values
('GM',202308,'retail',191,243570),
('GM',202308,'wholesale',63,-97),
('HN',202308,'retail',42,47974),
('HN',202308,'wholesale',49,4950),
('TY',202308,'retail',52,68258),
('TY',202308,'wholesale',24,-11527);

-- insert 202307 values
insert into cu.financial_statement_used_counts values
('GM',202307,'retail',159,231284),
('GM',202307,'wholesale',69,5312),
('HN',202307,'retail',39,65519),
('HN',202307,'wholesale',26,-1819),
('TY',202307,'retail',40,72017),
('TY',202307,'wholesale',17,18895);

-- insert 202306 values
insert into cu.financial_statement_used_counts values
('GM',202306,'retail',165,190637),
('GM',202306,'wholesale',92,-20937),
('HN',202306,'retail',47,52143),
('HN',202306,'wholesale',56,-241789),
('TY',202306,'retail',49,101248),
('TY',202306,'wholesale',27,-8012);

-- insert 202305 values
insert into cu.financial_statement_used_counts values
('GM',202305,'retail',152,268780),
('GM',202305,'wholesale',88,-4631),
('HN',202305,'retail',59,96888),
('HN',202305,'wholesale',42,-22838),
('TY',202305,'retail',56,59952),
('TY',202305,'wholesale',25,-3758);

select * 
from cu.financial_statement_used_counts
order by store, year_month, sale_type
























