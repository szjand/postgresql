﻿select d.gl_account, b.line, e.description
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201801  and b.page = 16
  and b.line between 1 and 14
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
  and e.account_type_code = '4'
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store = 'ry1'  


select store, year_month,
  sum(-amount) filter (where b.line_label like '%retail%') as retail,
  sum(-amount) filter (where b.line_label like '%wholesale%') as ws
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
where b.year_month between 201801 and 202304
  and page =16
  and line between 1 and 14
--   and c.store = 'ry1'
group by store, year_month