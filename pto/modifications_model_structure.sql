﻿08/27/20
rebecca risberg changes from ry2 receptionist to ry1 detail technician
select * from arkona.xfm_pymast where employee_last_name = 'risberg' order by pymast_key
employee_number changes from 258964 to 158964

transfer between stores is not unusual
leaning toward making action on the employee_number FK in use_pto, request, employee_pto_allocation
& position_fulfillment be on upate cascade

that would handle
  xfr between stores
  typos


alter table pto.requests  
drop constraint requests_employee_number_fkey,
add constraint requests_employee_number_fkey 
  foreign key(employee_number) references pto.employees(employee_number) on update cascade;

alter table pto.used_pto  
drop constraint used_pto_employee_number_fkey,
add constraint used_pto_employee_number_fkey 
  foreign key(employee_number) references pto.employees(employee_number) on update cascade;

alter table pto.employee_pto_allocation  
drop constraint employee_pto_allocation_employee_number_fkey,
add constraint employee_pto_allocation_employee_number_fkey 
  foreign key(employee_number) references pto.employees(employee_number) on update cascade;  

alter table pto.position_fulfillment  
drop constraint position_fulfillment_department_fkey,
add constraint position_fulfillment_department_fkey 
  foreign key(employee_number) references pto.employees(employee_number) on update cascade;    

update pto.employees
set employee_number = '158964'
where employee_number = '258964';

update pto.position_fulfillment
set department = 'Detail', the_position = 'Detail Technician'
where employee_number = '158964';
