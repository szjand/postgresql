﻿/*
Good morning Jon have a question on some honda employees for their pto rates:
Cory batzer 11-6-17
Josh olson 11-15-21 
Elijah Goetz 3-24-20
Zak louden 5.29-19

On Cory and Josh I thought they would be on the vision page but I do not see them?
I do not have any rates in the pto fields in UKG for these 4 so would you be able to let me know what their rates should be, and I will add them?

Kim

--cory   20.30
update pto.flat_rate_employees
set most_recent_anniv = '11/06/2021'
where employee_number = '24665';
--elizj  --15.07
update pto.flat_rate_employees
set most_recent_anniv = '03/24/2021'
where employee_number = '269854';
--zakery  -- 21.24
update pto.flat_rate_employees
set most_recent_anniv = '05/21/2021'
where employee_number = '275342';
--joshua  28.88
update pto.flat_rate_employees
set most_recent_anniv = '11/15/2021'
where employee_number = '264673';

select * 
from pto.flat_rate_employee_dates
where employee_number like '2%'

what i need to do is to reformat FUNCTION pto.flat_rate_pto_rate_recalc() for each employee so 
that it is as if i am running in in the pay period during with their anniversary occrus

select biweekly_pay_period_start_date, biweekly_pay_period_end_date, biweekly_pay_period_sequence from dds.dim_date where the_date = '05/21/2022'
-- turns out  cory and joshua are in the same pay period
-- and all 4 are already in pto.flat_rate_employees
select * from pto.flat_rate_employees where store = 'Rydell Honda Nissan'

*/
do $$
declare
	_first_of_pp date := '05/08/2022'; 
	_last_of_pp date := '05/21/2022';
	_pay_period_seq integer := 350;
begin	
drop table if exists employee_dates;
create temp table employee_dates as	
		select distinct _pay_period_seq as pay_period_seq, ----------------------------------------------
			_first_of_pp as first_of_pp, _last_of_pp as last_of_pp, aa.employee_number, aa.most_recent_anniv, aa.next_anniv, aa.min_check_date, 
			aa.max_check_date,c.biweekly_pay_period_start_date clock_from_date, e.biweekly_pay_period_end_date as clock_thru_date
		from (
			select a.employee_number, a.most_recent_anniv, a.next_anniv, 
			  case  -- the first biweekly check in ukg had a pay date of 1/7/22, anything before that requires dealertrack payroll data
			    when a.most_recent_anniv < '01/07/2022' then min((b.check_month || '-' || b.check_day || '-' || (2000 + b. check_year))::date) 
			    else min(c.pay_date)
			  end as min_check_date,
				max(c.pay_date) as max_check_date
			from (
				select *, (most_recent_anniv + interval '1 year')::date as next_anniv 
				from pto.flat_rate_employees
				where pay_calc_profile = 'Full-Time Flat Rate'
					and (most_recent_anniv + interval '1 year')::date between _first_of_pp and _last_of_pp) a 	
			join arkona.ext_pyhshdta b on a.employee_number = b.employee_
				and (b.check_month || '-' || b.check_day || '-' || (2000 + b. check_year))::date between a.most_recent_anniv and a.next_anniv - 1
				and b.total_gross_pay <> 0
			join ukg.payrolls c on true	
				and c.payroll_name like '%bi-weekly%'
				and c.pay_date between a.most_recent_anniv and a.next_anniv - 1
			where a.pay_calc_profile = 'Full-Time flat Rate'
			group by a.employee_number, a.most_recent_anniv, a.next_anniv) aa	
		left join dds.dim_date b on aa.min_check_date = b.the_date
		-- the pay period previous to the one that includes the min_check_date
		left join dds.dim_date c on b.biweekly_pay_period_sequence = c.biweekly_pay_period_sequence + 1	
		left join dds.dim_date d on aa.max_check_date = d.the_date
		-- the pay period previous to the one that includes the max check date
		left join dds.dim_date e on d.biweekly_pay_period_sequence = e.biweekly_pay_period_sequence + 1;

drop table if exists earnings;		
create temp table earnings as
		select pay_period_seq, employee_number, earnings_code, sum(earnings) as earnings
		from (
			select b.pay_period_seq, a.employee_number, 
				(c.check_month || '-' || c.check_day || '-' || (2000 + c. check_year))::date as check_date,
				coalesce(d.code_id || ' ' || d.description, 'base pay') as earnings_code, coalesce(c.base_pay, 0) + coalesce(d.amount, 0) as earnings
			from pto.flat_rate_employees a
			join employee_dates b on a.employee_number = b.employee_number ----------------------------------------
			  and b.pay_period_seq = _pay_period_seq --------------------------------------------------------------
			join arkona.ext_pyhshdta c on a.employee_number = c.employee_
				and (c.check_month || '-' || c.check_day || '-' || (2000 + c. check_year))::date between b.min_check_date and b.max_check_date
				and c.total_gross_pay <> 0
			left join arkona.ext_pyhscdta d on c.company_number = d.company_number -- does not exists for everyone, so left join
				and c.payroll_run_number = d.payroll_run_number 
				and a.employee_number = d.employee_number
				and d.code_type = '1'
				and d.code_id in ('79','C19','cov','75','87','70')	
			union all
			select aa.pay_period_seq,a.employee_number, c.pay_date, d.earnings_code, b.ee_amount as earnings
			from pto.flat_rate_employees a
			join employee_dates aa on a.employee_number = aa.employee_number -----------------------------------
			  and aa.pay_period_seq = _pay_period_seq
			join ukg.pay_statement_earnings b on a.employee_id = b.employee_account_id
				and coalesce(b.ee_amount, 0) <> 0
			join ukg.payrolls c on b.payroll_id = c.payroll_id
				and c.pay_date between aa.min_check_date and aa.max_check_date
			join pto.earnings_codes d on b.earning_code	= d.earnings_code
				and d.include_in_pto_rate_calc) e
		group by pay_period_seq, employee_number, earnings_code
		having sum(earnings) > 0;

drop table if exists clock_hours;
create temp table clock_hours as
		select _pay_period_seq as pay_period_seq, 
			employee_number, sum(clock_hours) as clock_hours, sum(pto_hours) as pto_hours, sum(hol_hours) as hol_hours
		from (
			select a.employee_number, c.the_date, c.clock_hours, c.pto_hours, c.hol_hours
			from pto.flat_rate_employees a
			join employee_dates b on a.employee_number = b.employee_number  --------------------------------------
				and b.pay_period_seq = _pay_period_seq
			join ukg.clock_hours c on a.employee_number = c.employee_number
				and c.the_date between b.clock_from_date and b.clock_thru_date
			where c.the_date > '12/18/2021'  
			union
			select a.employee_number, c.the_date, c.clock_hours, c.vac_hours + c.pto_hours as pto_hours, c.hol_hours
			from pto.flat_rate_employees a
			join employee_dates b on a.employee_number = b.employee_number --------------------------------------------
				and b.pay_period_seq = _pay_period_seq
			join arkona.xfm_pypclockin c on a.employee_number = c.employee_number
				and c.the_date between b.clock_from_date and b.clock_thru_date
			where c.the_date < '12/19/2021') d
		group by employee_number;	

drop table if exists summary;
create temp table summary as
		select b.pay_period_seq, b.first_of_pp, b.last_of_pp, a.last_name, a.first_name, a.employee_number, a.store, a.department, a.cost_center, 
			a.manager_1, a.manager_2, a.seniority_date, b.next_anniv, d.earnings, c.clock_hours, a.current_pto_rate, 
			round(earnings/clock_hours, 2) as new_pto_rate,
			case
				when round(d.earnings/c.clock_hours, 2) > a.current_pto_rate then round(d.earnings/c.clock_hours, 2)
				else a.current_pto_rate
			end as effective_pto_rate,
			case
				when round(d.earnings/c.clock_hours, 2) < a.current_pto_rate then 0
				else round(d.earnings/c.clock_hours, 2) - a.current_pto_rate
			end as diff
		from pto.flat_rate_employees a
		join employee_dates b on a.employee_number = b.employee_number ----------------------------------------
			and b.pay_period_seq = _pay_period_seq
		join clock_hours c on a.employee_number = c.employee_number -------------------------------------------
			and b.pay_period_seq = c.pay_period_seq
		join (
			select pay_period_seq, employee_number, sum(earnings) as earnings
			from earnings ----------------------------------------------------------------------------------------
			where pay_period_seq = _pay_period_seq
			group by pay_period_seq, employee_number) d on a.employee_number = d.employee_number
			and b.pay_period_seq = d.pay_period_seq;				
end $$;
select * from summary


                                                                                    