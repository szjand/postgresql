﻿-- these are the stored procs called in data.phph on old vision
-- these are the stored procs called in data.phph on old vision

--   EXECUTE PROCEDURE pto_get_new_employees();
  execute procedure pto_search_for_employee( ? )
  execute procedure pto_get_departments()
  execute procedure pto_get_positions_for_department( ? )
  execute procedure pto_get_administrator_for_position( ?, ? )
  execute procedure pto_get_employee_pto( ? )
  execute procedure pto_get_manager_employees( ? )
  execute procedure pto_get_new_employee( ? )
  execute procedure pto_get_employee_policy( ? )  -- does not seem to exist
  execute procedure pto_get_employee_pto_used( ? )
  execute procedure pto_insert_new_employee( ?, ?, ?, ?, ? )
  execute procedure pto_update_employee_pto( ?, ?, ?, ?, ? )

!!!!!!!!!!!!!
 the only people that need search are laura and kim, laura should have edit capacity, but only laura
 managers have access to their employees on their admin page

check for pto payout & email


compli data
  advantage
    pto_update_compli_users()
    pto_update_requests()
  postgresql
    pto.compli_users_update()
    pto.requests_update()
    pto.duplicate_request_email()

timeclock data
  advantage
    pto_update_pto_used()
  postgresql
    pto.used_pto_update()

employee page
  advantage
    pto_get_employee_pto(employeenumber)
    pto_get_employee_pto_used(employeenumber)
  postgresql
    pto.get_employee_pto(_emp citext)
    pto.get_employee_used_pto(_emp citext)

pto manager PTO Admin page (repsonsibility for tracking pto: dayton marek, )
  advantage
    pto_get_manager_employees(employeeNumber)
  postgresql
    pto.get_manager_employees(_emp citext)

pto manager View    
  advantage
    pto_get_employee_pto(employeenumber)
    pto_get_employee_pto_used(employeenumber)
  postgresql
    pto.get_employee_pto(_emp citext)
    pto.get_employee_used_pto(_emp citext)    

pto manager PTO Summary
    pto.get_employee_pto(_emp citext)
    pto.get_employee_used_pto(_emp citext)    
    
pto admin New Employees
  advantage
    pto_get_new_employees()
  postgresql
    pto.get_new_employees()
      
pto admin search    
  advantage
    pto_search_for_employee(search_string)
  postgresql
    pto.search_for_employee(_search citext)    

pto admin Edit search result 
  advantage
    pto_get_departments()
    pto_get_positions_for_department(department)
    pto_get_employee_pto(employeenumber)
    pto_get_administrator_for_position(department,position)
    pto_update_employee_pto( ?, ?, ?, ?, ? )
  postgresql
    pto.get_departments()
    pto.get_positions_for_department(_department)
    pto.get_employee_pto(_emp)
    pto.get_administrator_for_position(_department, _position)
    pto.update_employee(_dept,_position,_anniv,_hours_earned)
    
pto admin edit new employee
  advantage
    pto_get_departments()     
    pto_get_new_employee(employeenumber)
    pto_get_positions_for_department(department)
    pto_get_administrator_for_position(department,position)
    pto_insert_new_employee( ?, ?, ?, ?, ? )
  postgresql
    pto.get_departments()
    pto.get_new_employee(_emp)
    pto.get_positions_for_department(_department)
    pto.get_administrator_for_position(_department, _position)




CREATE OR REPLACE FUNCTION pto.insert_employee(
    _emp citext,
    _dept citext,
    _position citext,
    _anniv_date date,
    _hours_earned numeric)
  RETURNS void AS
$BODY$

/*
  select pto.insert_employee('169645','RY2 Sales','Sales Consultant', '	08-03-2020', 22);
*/   
declare
  _anniv_type citext := (
    select 
      case 
        when _anniv_date = dds.db2_integer_to_date(hire_date) then 'Latest'
        When _anniv_date = dds.db2_integer_to_date(org_hire_date) then 'Original'
        else 'Other'
      end
    from arkona.ext_pymast
    where pymast_employee_number = _emp);
  _user_name citext := (
    select lower(email)
    from pto.compli_users
    where user_id = _emp);
begin
  if not exists (
    select 1
    from arkona.ext_pymast
    where pymast_employee_number = _emp) then
      raise exception 'Employee Number "%" does not exist in Dealertrack Payroll', _emp;
  elsif ( -- test for new position already existing as a pto admin
    select count(*)
    from pto.position_fulfillment a
    where a.department = _dept
      and a.the_position = _position
      and exists (
        select 1
        from pto.authorization a
        where a.auth_by_department = _dept
          and a.auth_by_position =_position)) <> 0
    then
      raise exception 'The position of % is already filled by someone else', _new_position;
  elsif _anniv_date > current_date
    then
      raise exception 'Sorry, pto anniversary can not be set to a future date';
  else    
    insert into pto.employees (employee_number, pto_anniversary, user_name, anniversary_type,active)
    values(_emp, _anniv_date, _user_name, _anniv_type, true);
    insert into pto.position_fulfillment (department,the_position,employee_number)
    values(_dept,_position,_emp);
    insert into pto.employee_pto_allocation (employee_number,from_date,thru_date,hours)
    SELECT c.employee_number, 
      (pto_anniversary + interval '1 year' * c.n)::date AS from_date,
      CASE c.n
        WHEN 20 THEN '12/31/9999'::date
        else (((pto_anniversary + interval '1 year' * (c.n::integer + 1))::date) - interval '1 day')::date
      END AS thruDate,
      d.pto_hours  
    FROM (
      SELECT a.employee_number, a.pto_Anniversary, b.*
      FROM pto.employees a,
        (
          SELECT n
          FROM dds.tally
          WHERE n BETWEEN 0 AND 20) b  
      WHERE a.employee_number = _emp) c   
    LEFT JOIN pto.categories d on c.n BETWEEN d.from_years_tenure AND d.thru_years_tenure;    
  end if;
  if _hours_earned > 0 then
    update pto.employee_pto_allocation
    set hours = _hours_earned
    where employee_number = _emp
      and current_date between from_date and thru_date;
  end if;
end    
$BODY$
LANGUAGE plpgsql;

select * from pto.adjustments limit 10

insert into pto.adjustments(employee_number,from_date,hours,reason,notes)
select _emp, from_date, _hours_earned, 'Adjustment', 'PTO hours added by HR for first year of employment'
from pto.employee_pto_allocation
where employee_number = _emp
  and current_Date between from_date and thru_date

select *
from (
  select * 
  from pto.employee_pto_allocation) a
join (
  select * 
  from pto.employee_pto_allocation) b on a.employee_number = b.employee_number 
  and daterange(a.from_date, a.thru_date) <> daterange(b.from_date, b.thru_date)
  and daterange(a.from_date, a.thru_date) && daterange(b.from_date, b.thru_date)


select * from pto.employee_pto_allocation where from_Date > thru_date
select * from pto.employees where employee_number = '155214'

select * from pto.employee_pto_allocation where employee_number = '155214'

update pto.employee_pto_allocation
set from_date = '08/01/2039'
-- select * from pto.employee_pto_allocation
where employee_number = '155214'
  and thru_Date = '12/31/9999'


-- 09/14/2020 afton needds a function to determine if an employee is an authorizer,
-- for limiting a simple edit of department/position
select 
  case
    when exists (
select 1
from (
  select auth_by_department, auth_by_position
  from pto.authorization 
  group by auth_by_department, auth_by_position) a
join pto.position_fulfillment b on a.auth_by_department = b.department
  and a.auth_by_position = b.the_position
where employee_number = '1102195')  then true
else false
end

select *
from pto.position_fulfillment a
join pto.authorization b on a.department = b.auth_by_department
join pto.authorization c on a.the_position = c.auth_by_position
where employee_number = '1102195'

create or replace function pto.employee_is_authorizer (_emp citext)
returns boolean as
$BODY$
/*
select pto.employee_is_authorizer('1102195')
*/
select 
  case
    when exists (
      select 1
      from (
        select auth_by_department, auth_by_position
        from pto.authorization 
        group by auth_by_department, auth_by_position) a
      join pto.position_fulfillment b on a.auth_by_department = b.department
        and a.auth_by_position = b.the_position
      where employee_number = _emp)  then true
    else false
  end;

$BODY$
language sql;