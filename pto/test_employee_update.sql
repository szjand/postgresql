﻿do $$
declare
  _anniv date;
  _from_date date;
  _thru_date date;
  _emp citext;
begin  
  _emp := '194675';
  _anniv := (
    select pto_anniversary
    from pto.employees
    where employee_number = _emp);
  _from_date := (
    select
      case
        when a.thru_date > (select current_date + interval '100 years') then
          (select the_date 
            from dds.dim_date aa
            where extract(month from aa.the_date) = extract(month from b.pto_anniversary)
              and extract(day from aa.the_date) = extract(day from b.pto_anniversary)
              and aa.the_date <= current_date
            order by aa.the_date desc
            limit 1)
        else a.from_date
      end
    from pto.employee_pto_allocation a
    join pto.employees b on a.employee_number = b.employee_number
    where a.employee_number = _emp
      and current_date between a.from_date and a.thru_date);   
  _thru_date := (
    select
      case
        when a.thru_date > (select current_date + interval '100 years') then
          (_from_date + interval '1 year')::date - 1
        else a.thru_date
      end
    from pto.employee_pto_allocation a
    where a.employee_number = _emp
      and current_date between a.from_date and a.thru_date);      
drop table if exists wtf;
create temp table wtf as      
      select 
        h.employee_number, h.name, 
        h.department, h.the_position, 
        h.pto_admin, h.pto_anniversary, h.hours + coalesce(n.pto_adjustments, 0) as pto_hours_earned,
        coalesce(i.pto_used, 0) as pto_used,
        h.hours + coalesce(n.pto_adjustments, 0) - coalesce(i.pto_used, 0) - coalesce(km.pto_hours_requested, 0) 
          - coalesce(km.pto_hours_approved, 0) as pto_hours_remaining,
        h.department || ':' ||h.the_position as title, _from_date as pto_from_date, _thru_date as pto_thru_date, 
        coalesce(km.pto_hours_approved,0) AS pto_hours_approved, 
        coalesce(km.pto_hours_requested,0) AS pto_hours_requested,
    --     (_thru_date - _from_date) + 1 AS totalDaysInPeriod,
        _thru_date - current_date AS remainingDaysInPeriod     
      from (
        select a.employee_number, a.pto_anniversary, b.employee_first_name || ' ' || b.employee_last_name as name,
          b.pymast_company_number as storecode, 
          b.active_code, c.department, c.the_position,
          f.employee_first_name || ' ' || f.employee_last_name as pto_admin,
          g.hours, 
          case when b.pymast_company_number = 'RY1' then 'RY1 - GM' else 'RY2 - Honda' end as store,
          dds.db2_integer_to_date(b.org_hire_date) as original_hire_date, 
          dds.db2_integer_to_date(b.hire_date) as latest_hire_date
        from pto.employees a
        join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
        join pto.position_fulfillment c on a.employee_number = c.employee_number
        join pto.authorization d on c.department = d.auth_for_department
          and c.the_position = d.auth_for_position
        join pto.position_fulfillment e on d.auth_by_department = e.department
          and d.auth_by_position = e.the_position
        join arkona.ext_pymast f on e.employee_number = f.pymast_employee_number  
        join pto.employee_pto_allocation g on a.employee_number = g.employee_number
          and current_Date between g.from_date and g.thru_date
        where a.employee_number = _emp) h
      left join ( -- may not be any used
        select employee_number, sum(hours) as pto_used
        from pto.used_pto
        where trim(employee_number) = _emp
          -- limit to curdate(), some managers enter pto time IN advance
          and the_date between _from_date and current_date
        group by employee_number) i on h.employee_number = i.employee_number
      left join pto.department_positions j on h.department = j.department
        and h.the_position = j.the_position
      left join ( -- limit requested & approved to current period
        SELECT employee_number, 
          SUM(hours) filter (where request_status = 'Approved') AS pto_hours_approved,
          SUM(hours) filter (where request_status = 'Pending') AS pto_hours_requested
        FROM pto.requests  
        WHERE request_type = 'paid time off'
          and the_date between current_date and _thru_date
          and employee_number = _emp
        group by employee_number) km on h.employee_number = km.employee_number
      left join (
        select employee_number, from_date, sum(hours) as pto_adjustments
        from pto.adjustments
        group by employee_number, from_date) n on h.employee_number = n.employee_number
          and n.from_date = _from_date;
end $$;       
select * from wtf;   