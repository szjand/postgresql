﻿1. term
1a. reverse term
2. change store and position
2a. change store same position
3. change position within store
4. rehire, completely fresh restart
5. from full to part time with payout
6. rehire with name change (samantha arnold -> samantha casserly), fresh restart
7. get payout
8. heirarchy editing
9. allocation editing
10. change pto anniversary
11. new position
12. authorizer change 
13. manually enter a new employee
14. large authorization change
15. donate to cares fund

/*
general query
select a.user_name, a.pto_anniversary, a.active, b.department, b.the_position, string_agg(c.from_date::text ||'->'|| c.thru_date::text||'::'||(c.hours::integer)::text,',')
from pto.employees a
left join pto.position_fulfillment b on a.employee_number = b.employee_number
left join pto.employee_pto_allocation c on a.employee_number = c.employee_number
where a.employee_number = '259875'
group by a.user_name, a.pto_anniversary, a.active, b.department, b.the_position
*/

/* 

saved this in E:\sql\advantage\tool\ToolCleanUp\term_from_pg_pto.sql

only putting it here because it is part of the laura deal
this is for tool users
db = dpsvseries

SELECT * FROM users WHERE username = 'lreeb';

DECLARE @PartyID string;
DECLARE @NowTS timestamp;

@PartyID = (SELECT PartyID FROM users WHERE username = 'lreeb');
@NowTS = (SELECT now() FROM system.iota);

BEGIN TRANSACTION;
TRY 
  UPDATE contactmechanisms
  SET ThruTS = @NowTS
  WHERE PartyID = @PartyID
  AND ThruTS IS NULL;
  
  UPDATE users
  SET Active = False
  WHERE partyid = @PartyID;
  
  UPDATE ApplicationUsers
  SET ThruTS = @NowTS
  WHERE partyid = @PartyID
  AND ThruTS IS NULL;
  
  UPDATE PartyRelationships
  SET ThruTS = @NowTS
  WHERE partyid2 = @PartyID
  AND ThruTS IS NULL;
  
  UPDATE PartyPrivileges
  SET ThruTS = @NowTS
  WHERE PartyID = @PartyID
  AND ThruTS IS NULL;
  
  COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END;   
*/

pto.position_fulfillment does not get "cleand up", ie, there are beaucoup inactive employee numbers in this table
THEREFOR 
limit queries on pto.position_fulfillment with a join to pto.employees where active
------------------------------------------------------------
--< 1. term
------------------------------------------------------------

1. 
is this person an authorizer?
select * from pto.employees where user_name like '%sbergh%' and active
select * from pto.position_fulfillment where employee_number = '113085'
select * from pto.authorization where auth_by_department = 'Appearance' and auth_by_position = 'Director'
yep
at the time of term, it looks like steve bergh no longer authorized anyone,
all the positions had had the department changed
select * 
from pto.position_fulfillment a
where the_position in ('Team Leader','Maintenance Technician','Talent Manager')

fuck, hanging up on he doesnt show up in vision search, wydat?
BECAUSE THE ACTIVE_CODE IN PYMAST = P, SEARCH QUERY SPECIFIES an A
update pto.employees set active = false where employee_number = '267531';
delete from pto.position_fulfillment where employee_number = '267531';
delete from pto.employee_pto_allocation where employee_number = '267531';

select * from pto.used_pto where employee_number in ('112589','212589') and the_date between '06/05/2020' and '06/04/2021'

-- 1.  get the picture from pymast
select pymast_employee_number, employee_name, active_code, hire_date, termination_date, row_from_date
from arkona.xfm_pymast 
where employee_last_name like 'keller%'-- and employee_first_name = 'brandon'
--   and employee_first_name = 'jaxon'
order by pymast_employee_number, pymast_key

-- 2. the relevant pto tables
select a.active  as pto_active, b.active_code as pymast_active, b.employee_name as pymast_name, 
  a.employee_number, a.pto_anniversary, 
  c.department, c.the_position, d.hours as pto_due, coalesce(e.hours, 0) as adj,
  coalesce(f.used, 0) as used, d.from_date as alloc_from, d.thru_date as alloc_thru
from pto.employees a
left join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
left join pto.position_fulfillment c on a.employee_number = c.employee_number
left join pto.employee_pto_allocation d on a.employee_number = d.employee_number
  and current_Date between d.from_Date and d.thru_date
left join pto.adjustments e on a.employee_number = e.employee_number
  and e.from_date = d.from_date 
left join (
  select m.employee_number, sum(o.hours) as used
  from pto.employees m
  join pto.employee_pto_allocation n on m.employee_number = n.employee_number
    and current_Date between n.from_date and n.thru_date 
  join pto.used_pto o on m.employee_number = o.employee_number
    and o.the_date between n.from_date and n.thru_date
  where m.employee_number = '295148'
  group by m.employee_number) f on a.employee_number = f.employee_number
where a.employee_number = '295148';

3. make the necessary changes

delete from pto.position_fulfillment where employee_number = '295148';
update pto.employees set active = false where employee_number = '295148';

select * from arkona.xfm_pymast where pymast_employee_number = '135987' order by row_from_date

select * 
from pto.employees a
join pto.position_fulfillment b on a.employee_number = b.employee_number
and b.the_position = 'customer care sales specialist'

------------------------------------------------------------
--/> 1. term
------------------------------------------------------------
------------------------------------------------------------
--< 1a. reverse term
------------------------------------------------------------
-- justin brunk
-- laura: He’s returning to the same role so let’s just make it look like he never left.  So his PTO anniversary is 2/26/2018.
select * from arkona.ext_pymast where employee_last_name = 'brunk' and employee_first_name = 'justin'
SELECT * FROM PTO.USED_PTO WHERE employee_number = '258763'
select * from arkona.xfm_pypclockin where employee_number = '258763'

select *
from pto.employees
where employee_number = '258763'

select * from pto.position_fulfillment where department = 'ry2 sales'

update pto.employees
set active = true = true
where employee_number = '258763';

insert into pto.position_fulfillment values('RY2 Sales','Sales Consultant','258763');
------------------------------------------------------------
--/> 1a. reverse term
------------------------------------------------------------


------------------------------------------------------------
--< 2. change store and position
------------------------------------------------------------
/*
08/02/21
Hey Jon-

Could you add Sam Foster into Vision under Honda Sales Manager?  It wouldn’t let me do it.

Thanks!

Laura Roth 
*/
select * from pto.employees where user_name like 'sfoster%'  -- 148080

select * from pto.employee_pto_allocation where employee_number in ('148080','248080') order by from_date
select * from pto.position_fulfillment where employee_number = '248080'
aha
RY2 Sales;Sales Manager;148080

this query also updates pto.position_fulfillment & pto.employee_pto_allocation
update pto.employees
set employee_number = '248080'
where employee_number = '148080'




/*
3/11/21
Lucas Keifer switched to Honda as a technican but I also don’t know where 
his PTO hours were at.  Could you look into that for me?  His actual PTO anniversary date is 5/19/2014.
*/
select * from arkona.ext_pymast where employee_last_name = 'kiefer'
178050
select * from pto.position_fulfillment where employee_number = '178050'
select * from pto.employee_pto_allocation where employee_number = '178050'

update pto.employees
set employee_number = '278050'
where employee_number = '178050';

select * from pto.used_pto where 
employee_number in ('278050','178050')
and the_date between '05/19/2020' and '05/18/2021'

select * from arkona.xfm_pypclockin 
where the_date between '05/19/2020' and '05/18/2021'
  and employee_number in ('278050','178050')
  and pto_hours <> 0

add the 8 hours used at ry1 to his ry2 emp#

insert into pto.used_pto values
('278050','09/03/2020','Time Clock', 4),
('278050','09/21/2020','Time Clock', 4);

/*
3/11/21
I was trying to add Ethan Farley as a Detail Technician at Honda and it would not let me do that.  
Could you add him in as that and then I guess we will need to add that to the list of jobs at 
the Honda location.  That was not on my previous list that I sent to you and Afton.
*/
select * from arkona.ext_pymast where employee_last_name = 'farley'
269846

select * from pto.position_fulfillment where employee_number = '169846'
select * from pto.department_positions where the_position like '%detail%'
select * 
from pto.authorization a
join pto.position_fulfillment b on a.auth_for_department = b.department
  and a.auth_for_position = b.the_position
where b.the_position like '%detail%'

update pto.employees
set employee_number = '269846'
where employee_number = '169846';

update pto.position_fulfillment
set department = 'Detail', the_position = 'Detail Technician'
where employee_number = '269846';


-- 08/27/20
Rebecca Risberg from RY2 Receptionist to RY1 Detail Technician
update pto.employees
set employee_number = '158964'
where employee_number = '258964';

update pto.position_fulfillment
set department = 'Detail', the_position = 'Detail Technician'
where employee_number = '158964';


select * 
from pto.employees a
left join pto.position_fulfillment b on a.employee_number = b.employee_number
where a.employee_number in ('165328','265328','212589','112589')

select *
from pto.department_positions
where department = 'RY1 Sales'
order by the_position

/*
10/09
Mike Longoria is moving back to the Rydell GM store from Honda but I noticed that he was not in Vision.  
Could you please put him in there under Sales Floor Manager at GM with the PTO anniversary of 7/30/2018?
*/
select a.* 
from pto.employees a
join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
where b.employee_name = 'longoria, michael';
-- his honda emp# not in position fulfillment
select * from pto.position_fulfillment where employee_number = '265328';
select * from pto.positions order by the_position
select * from pto.position_fulfillment where employee_number = '1126300' -- nick shirek
select * from pto.employee_pto_allocation where employee_number in ('165328','265328')
select * from pto.requests where employee_number in ('165328','265328')

update pto.employees
set employee_number = '165328'
where employee_number = '265328';
insert into pto.positions values ('Floor Manager');
insert into pto.department_positions values ('RY1 Sales','Floor Manager');
insert into pto.authorization values('RY1 Sales','General Sales Manager','RY1 Sales','Floor Manager');
insert into pto.position_fulfillment values('RY1 Sales','Floor Manager','165328');

------------------------------------------------------------
--/> 2. change store and position
------------------------------------------------------------

------------------------------------------------------------
--< 2a. change store same position
------------------------------------------------------------


-- 04/14/21 mike longoria has moved back to honda, assume the same postion
select * from pto.employees where user_name like '%longoria%'

update pto.employees
set employee_number = '265328'
where employee_number = '165328';

and nik holand has moved back to rydell
update pto.employees
set employee_number = '111232'
where employee_number = '211232';

select * 
from pto.employee_pto_allocation
where employee_number in ('111232','211232')
and from_date = '03/27/2021'



-- 3/11/21 ben bina, moving from honda to gm , detail tech
-- oops
ERROR:  update or delete on table "employees" violates foreign key constraint "current_pto_periods_employee_number_fkey" on table "current_pto_periods"
DETAIL:  Key (employee_number)=(254976) is still referenced from table "current_pto_periods".

alter table pto.current_pto_periods
drop constraint current_pto_periods_employee_number_fkey;
alter table pto.current_pto_periods
add foreign key(employee_number)
REFERENCES pto.employees (employee_number)
  on update cascade;

-- cascade updates takes care of employee_number FK tables
update pto.employees
set employee_number = '154976'
where employee_number = '254976';

for this case, that is all that needs to be done,
he will no longer show up on the admin page as a new employee after the first
compli update (runs every 20 minutes) 



/*
The system won’t let me edit Tyler Bedney.  He needs to be moved back over to the Honda location with the same title.
10/09
Laura Roth 
*/
-- cascade updates takes care of employee_number FK tables
update pto.employees
set employee_number = '212589'
where employee_number = '112589';


insert into pto.authorization values('RY1 Sales','General Sales Manager','RY2 Sales','Sales Manager');

-- Bradley Schumacher consultant moved from honda to gm
select * from pto.employees where employee_number in ('165470','265478')
select * from arkona.xfm_pymast where pymast_employee_number in ('165470','265470') order by pymast_key
select * from arkona.xfm_pymast where employee_first_name = 'bradley' and employee_last_name = 'schumacher' order by pymast_key
select * from pto.position_fulfillment where employee_number = '265478'

update pto.employees
set employee_number = '165470'
where employee_number = '265478';

-- 11/20/2020
/*
Hi Jon-
Vision won’t let me edit Dylan to move him over to Honda.  Could you please help me with that?  
He will be a Sales Manager at Honda now starting 11/22. 
*/

need to wait for dealertrack to be updated

select *
from pto.employees a
left join pto.position_fulfillment b on a.employee_number = b.employee_number
where user_name like 'dhaley%'

select * from arkona.xfm_pymast where pymast_employee_number like '%63700' order by pymast_key

-- 11/25/20, he has been changed in dealertrack
update pto.employees
set employee_number = '263700'
where employee_number = '163700';

-- hmmm details in vision blank
update pto.position_fulfillment
set department = 'RY2 Sales', the_position = 'Sales Manager'
-- select * from pto.position_fulfillment 
where employee_number = '263700';

select * from pto.employee_pto_allocation where employee_number = '263700'

select * from pto.authorization where auth_for_department = 'ry2 sales' and auth_for_position =  'sales manager'


------------------------------------------------------------
--/> 2a. change store same position
------------------------------------------------------------

------------------------------------------------------------
--< 3. change position within store
------------------------------------------------------------
08/27/20
Tyler Magnuson from detail:technician to Building Maintenance:Technician
select * from pto.employees where employee_number = '190600'
select * from pto.position_fulfillment where employee_number = '190600'

update pto.position_fulfillment
set department = 'Building Maintenance', the_position = 'Technician'
where employee_number = '190600';

08/27/20
Taner Smith from detail:support specialist to detail:cashier
select * from arkona.xfm_pymast where employee_first_name = 'Taner' order by pymast_key
select * from pto.employees where employee_number = '176430'
select * from pto.position_fulfillment where employee_number = '176430'
select * from pto.department_positions where department = 'detail'

update pto.position_fulfillment
set the_position = 'Cashier'
where employee_number = '176430';

09/02/2020
Hi Jon-
Can you please move Braden Fraser, Hunter Zeman and Tim Durand over to Joel in Vision?  Hunter is an Express Service Representative and Braden and Tim are Service Advisors.
Thanks,
Laura Roth 

Tim Durand is a rehire, completely fresh restart:

Hunter Zeman: 
joel: Hunter is now a Service Advisor.
laura: 
The last Compli form that we have on file says 50/50 PDQ/Main Shop.  If he is a full time Service Advisor now then please send in a new Compli form stating that.

Braden Fraser 247000
update pto.position_fulfillment
set department = 'RY2 Main Shop',
    the_position = 'Advisor'
-- select * from pto.position_fulfillment
where employee_number = '247000';

--< 10/13/2020 --------------------------------------------------------------
Anthony Erickson can go to ryan shroyers vision please
Can I get Myron mason and Hunter Zeman under mine also.
Thanks,
Joel T. Dangerfield

-- anthony, currently advisor, change him to RY2 PDQ:Express Service Representative
select *
from pto.position_fulfillment
where employee_number = '295435'

select * 
from pto.authorization
where auth_by_department = 'ry2 pdq'
  and auth_by_position = 'manager'

update pto.position_fulfillment
set department = 'RY2 PDQ', the_position = 'Express Service Representative'
where employee_number = '295435';  
-- hunter
-- currently is ry2 pdq:advisor, which does not exist in authorization
select *
from pto.authorization
where auth_for_department = 'ry2 pdq'
  and auth_for_position = 'advisor'
-- so change him to ry2 main shop:advisor , that will put him under joiel
select * 
from pto.authorization
where auth_by_department = 'ry2 main shop'
  and auth_by_position = 'manager'

update pto.position_fulfillment
set department = 'RY2 Main Shop'
where employee_number = '211568'; 

-- myron change from RY2 PDQ:Express Service Representative to ry2 main shop:advisor 
update pto.position_fulfillment
set department = 'RY2 Main Shop', the_position = 'Advisor'
where employee_number = '298432';

--/> 10/13/2020 --------------------------------------------------------------


------------------------------------------------------------
--/> 3. change position within store
------------------------------------------------------------

------------------------------------------------------------
--> 4. rehire, completely fresh restart
------------------------------------------------------------
Tim Durand is a rehire, completely fresh restart:
jon: 
Timothy Durand was originally hired on 09/17/18,  termed on 08/02/19 and rehired on 07/27/20.
In terms of PTO, is he essentially a new hire, meaning, his PTO anniversary is 07/27/20 and he does not become eligible for 72 hours pto until 07/27/21?
laura:
That is correct.  We brought him back as a brand new employee.  

so that means an entire new config, need to remove the old allocation, used, requests, etc.

delete from pto.requests where employee_number = '265421';
delete from pto.used_pto where employee_number = '265421';
delete from pto.employee_pto_allocation where employee_number = '265421';
delete from pto.adjustments where employee_number = '265421';

update pto.employees
set pto_anniversary = '07/27/2020',
    active = true
-- select * from pto.employees 
where employee_number = '265421';

update pto.position_fulfillment
set department = 'RY2 Main Shop',
    the_position = 'Advisor'
-- select * from pto.position_fulfillment
where employee_number = '265421';

insert into pto.employee_pto_allocation (employee_number,from_date,thru_date,hours)
SELECT c.employee_number, 
  (pto_anniversary + interval '1 year' * c.n)::date AS from_date,
  CASE c.n
    WHEN 20 THEN '12/31/9999'::date
    else (((pto_anniversary + interval '1 year' * (c.n::integer + 1))::date) - interval '1 day')::date
  END AS thruDate,
  d.pto_hours  
FROM (
  SELECT a.employee_number, a.pto_Anniversary, b.*
  FROM pto.employees a,
    (
      SELECT n
      FROM dds.tally
      WHERE n BETWEEN 0 AND 20) b  
  WHERE a.employee_number = '265421') c   
LEFT JOIN pto.categories d on c.n BETWEEN d.from_years_tenure AND d.thru_years_tenure;  

    




------------------------------------------------------------
--/> 4. rehire, completely fresh restart
------------------------------------------------------------
------------------------------------------------------------
--< 5. from full to part time with payout
------------------------------------------------------------
5. 

select * from arkona.ext_pymast where employee_last_name = 'budeau'
select * from pto.employees where employee_number in ('175873','265873')
select * from pto.employee_pto_allocation where employee_number = '175873'   5/19/20 -> 21  72 hours
select * from pto.used_pto where employee_number = '175873'

-- 10/20/20
-- Hey Jon-
-- Frances Johnson at Honda went to Part Time 10/16.  Could you remove her from accruing vacation in Vision?
-- Thanks!
-- Laura Roth 

-- not done in dealertrack yet
select * from arkona.xfm_pymast where employee_last_name = 'johnson' and employee_first_name = 'frances' order by pymast_key;
select * from pto.employees where employee_number = '259756';
select * from pto.employee_pto_allocation where employee_number = '259756' and current_date between from_Date and thru_date;
no pto due, still in first year of employment
-- remove from position
select *
-- delete 
from pto.position_fulfillment where employee_number = '259756';
-- deactive pto.employees
update pto.employees
set active = false
where employee_number = '259756';
------------------------------------------------------------
--/> 5. from full to part time with payout
------------------------------------------------------------



select * from pto.department_positions order by department, the_position

select * from pto.positions order by the_position

select * from pto.department_positions where the_position = 'Maintenance Technician'

select * from pto.departments order by department

select employee_name, active_code, department_code, hire_date, org_hire_date, termination_date, row_from_date, row_thru_Date 
-- select *
from arkona.xfm_pymast where employee_last_name = 'durand'

select * from pto.employees where employee_number = '265421'

select * from pto.employee_pto_allocation where employee_number = '265421'



------------------------------------------------------------
--< 6. rehire with name change (samantha arnold -> samantha casserly) fresh restart
------------------------------------------------------------
hire: 04/29/19
term: 01/30/20
rehire:: 09/08/20

select * 
from arkona.xfm_pymast
where pymast_employee_number = '164821'
order by pymast_key

select * 
from arkona.ext_pymast
where pymast_employee_number = '164821'


select * from pto.employee_pto_allocation where employee_number = '164821'

select * from pto.position_fulfillment where employee_number = '164821'

select * from pto.department_positions order by department, the_position

select * from pto.employees where employee_number = '164821'

-- ok, i think the best thing to do is to delete the existing samantha employees record, the create a new one

delete from pto.employees where employee_number = '164821'
-- i did not implement on DELTE CASCASDE
-- so, for now, do it manually
delete from pto.adjustments where employee_number = '164821';
delete from pto.employee_pto_allocation where employee_number = '164821';
delete from pto.used_pto where employee_number = '164821';
delete from pto.requests where employee_number = '164821';
delete from pto.position_fulfillment where employee_number = '164821';
delete from pto.employees where employee_number = '164821';
-- and create the new records
select pto.insert_employee('164821','Customer Care Center','Customer Care Sales Specialist', '09/08/2020', 0);

select * from pto.employees where employee_number = '164821';
------------------------------------------------------------
--/> 6. rehire with name change (samantha arnold -> samantha casserly) fresh restart
------------------------------------------------------------





------------------------------------------------------------
--< 7. get payout
------------------------------------------------------------
/*
03/22/21
Can you tell me how much unsued PTO Brian Peterson had at his reset date please.

John Gardner
*/
/*  11/04/2020
I never received a PTO pay out request for Scott Sevigny.  His PTO expiration was on 9/29, he believes he had some left over PTO 
but I am unsure of how to look into whether he did or did not.  If you could please let me know whether he had left over PTO 
that needs to be paid out I would appreciate it.
Thank you
John Gardner
*/
select * from arkona.ext_pymast where employee_last_name in ('jacobson') and employee_first_name = 'peter'
171055
select * from pto.employee_pto_allocation where employee_number in ('171055') order by from_date desc;  -- 152 hours
select sum(hours) from  pto.used_pto where employee_number = '171055' and the_date between '2020-12-13' and '2021-12-12' --64 hours used
select * from pto.adjustments where employee_number in ('171055')  -- 0
select 112 - 64  balance of 48 hours

/*
Good Afternoon John,

Could you send me Paul Leavy and Eric Davis's remaining PTO? I know it had reset and I need to submit the forms into Compli. Thank you
*/

/*
09/14/20
Hey Jon,
I was told to just send you an email and give you a heads up. My PTO reset last week and I still had 4 days that I didn't use that need to be cashed out. Let me know if you need anything from me.

Thanks,
Tiffany Noreikis
*/

select * from arkona.ext_pymast where employee_last_name in ('pederson')
david pederson 2106410
select * from pto.employee_pto_allocation where employee_number in ('2106410') order by from_date desc;  -- 112 hrs
select sum(hours) from  pto.used_pto where employee_number = '2106410' and the_date between '2019-12-16' and '2020-12-15'   80 hours used
select * from pto.adjustments where employee_number in ('2106410')  -- 0


eric davis 131315
paul leavy 184806

select * from pto.employee_pto_allocation where employee_number in ('131315','184806')

select * from pto.adjustments where employee_number in ('131315','184806')

eric davis  2019-08-11;2020-08-10  112 hours
paul leavy 2019-08-25;2020-08-24 112 hours

select sum(hours) from  pto.used_pto where employee_number = '131315' and the_date between '08/11/2019' and '08/10/2020'   64 hours

select sum(hours) from  pto.used_pto where employee_number = '184806' and the_date between '08/25/2019' and '08/24/2020'  120 hours

select * from  pto.used_pto where employee_number = '184806' and the_date between '08/25/2019' and '08/24/2020'  120 hours

select * from arkona.xfm_pypclockin
where employee_number = '184806' and the_date between '08/25/2019' and '08/24/2020' and pto_hours <> 0
order by the_date
-------------------
select * from arkona.ext_pymast where employee_last_name in ('noreikis')  -- 187651
select * from pto.employee_pto_allocation where employee_number in ('187651')  -- 9/10/19 -> 9/9/20 72 hrs
select * from pto.adjustments where employee_number in ('187651')  -- 0

select * from  pto.used_pto where employee_number = '187651' and the_date between '09/10/2019' and '09/09/2020'  -- 40 hours

/*
10/09/20
Can you check pto……….do I owe kenny 3 days pto payout for this past year?   You fixed it for me but it updated before I checked.

Thanks

Joel T. Dangerfield
*/
select * from arkona.ext_pymast where employee_last_name in ('knudson')  -- 279630
select * from pto.employee_pto_allocation where employee_number in ('279630')  -- 192 hrs
select * from pto.adjustments where employee_number in ('279630')  -- 0
select * from pto.employees where employee_number = '279630'  -- anniv 10/6
select sum(hours) from pto.used_pto where employee_number = '279630' and the_Date between '10/06/2019' and '10/05/2020'  -- 168 hours
select 192 - 168 = 24 hours = 3 days

/*
11/20/20
Can you tell me if Terry Driscoll had any leftover PTO from his anniversary on 11/4 please and thank  you.h
John Gardner
*/
select * from arkona.ext_pymast where employee_last_name in ('driscoll')  -- 135770
select * from pto.employee_pto_allocation where employee_number in ('135770')  -- 152 hrs
select * from pto.adjustments where employee_number in ('135770')  -- 0
select * from pto.employees where employee_number = '135770'  -- anniv 11/04
select sum(hours) from pto.used_pto where employee_number = '135770' and the_Date between '11/04/2019' and '11/03/2020'  -- 120 hours
select 152 - 120 = 32 hours = 4 days

/*
11/30/20
Could you let me know how many hours Bo had left before his PTO restarted on 11/23?
Laura Roth 
*/
select * from arkona.ext_pymast where employee_last_name in ('brorby')  -- 121063
select * from pto.employee_pto_allocation where employee_number in ('121063')  -- 112 hrs
select * from pto.adjustments where employee_number in ('121063')  -- 0
select * from pto.employees where employee_number = '121063'  -- anniv 11/04
select sum(hours) from pto.used_pto where employee_number = '121063' and the_Date between '11/23/2019' and '11/22/2020'  -- 72 hours
select 112 - 72 = 40 hours = 5 days

/*
02/08/21
Good Morning Jon, 

It has come to the time of year again where my Work Anniversary has occurred.  Last I looked I still had 56 Hours of unused PTO.  Could you please confirm and forward this information on to the people that need it.  Thanks as Always.


John Olderbak 
*/


select * from arkona.ext_pymast where employee_last_name in ('olderbak')  -- 1106225
select * from pto.employee_pto_allocation where employee_number in ('1106225')  -- 152 hrs
select * from pto.adjustments where employee_number in ('1106225')  -- 0
select * from pto.employees where employee_number = '1106225'  -- anniv 02/07/05
select sum(hours) from pto.used_pto where employee_number = '1106225' and the_Date between '2020-02-07' and '2021-02-06'  -- 96 hours
select 152 - 96 = 56 hours = 7 days

/*
04/27/21
Pat’s PTO turnover was on 4/25, can you please let me know how much PTO he had left.

John Gardner
*/
select * from arkona.ext_pymast where employee_last_name in ('adam')  -- 11660
select * from pto.employee_pto_allocation where employee_number in ('11660') order by from_date -- 112 hrs
select * from pto.adjustments where employee_number in ('11660')  -- 0
select * from pto.employees where employee_number = '11660'  -- anniv 04/25/16
select sum(hours) from pto.used_pto where employee_number = '11660' and the_Date between '2020-04-25' and '2021-04-24'  -- 72 hours
select 112 - 72 = 40 hours
------------------------------------------------------------
--/> 7. get payout
------------------------------------------------------------


------------------------------------------------------------
--< 8. heirarchy admin
------------------------------------------------------------
--< 06/23/21 -------------------------------------------------------------------------------------------
laura:
I tried to move Preston Close into the Customer Care Sales Manager position but noticed in Vision that 
it’s called a “Lead” instead.  Could you update it to say Manager?  
Sam Foster is still in the role but is there a way for you to add both of them into that role for now?
jon:
In the current system, a position that is responsible for authorizing the pto of other positions can only be held by a single person.
I can change sam’s position to manager, perhaps make preston  the lead (authorized by the manager) for the meantime?
laura:
You can move Sam out of it and put him as the lead so that Preston has all of the functionality 
that he’s suppose to have.  Sam is going to start working in Foster’s Training so 
he’ll be classified as whatever Ben Foster is in there.

1.create a new position
select * from pto.department_positions order by department, the_position

select a.auth_by_department, a.auth_by_position, d.employee_number, e.employee_name,
  a.auth_for_department, a.auth_for_position, b.employee_number, c.employee_name
from pto.authorization a
join pto.position_fulfillment b on a.auth_for_position = b.the_position 
join arkona.ext_pymast c on b.employee_number = c.pymast_employee_number
join pto.position_fulfillment d on a.auth_by_position = d.the_position
join arkona.ext_pymast e on d.employee_number = e.pymast_employee_number
where a.auth_for_department = 'Customer Care Center'

select * from pto.department_positions where department = 'Customer Care Center'
insert into pto.positions values ('Customer Care Sales Manager');
insert into pto.department_positions values('Customer Care Center','Customer Care Sales Manager',4,'bdc_policy_html');

2. put preston in the new position
select * from pto.employees where user_name like 'pclose%'  -- 162983
select * from pto.position_fulfillment where employee_number = '162983'

update pto.position_fulfillment 
set department = 'Customer Care Center', the_position = 'Customer Care Sales Manager'
where employee_number = '162983'

3. change authorization from lead to manager
select * from pto.authorization where auth_by_position = 'Customer Care Sales Lead'

select * from pto.authorization where auth_for_position = 'Customer Care Sales Lead'

update pto.authorization
set auth_by_position = 'Customer Care Sales Manager'
where auth_by_position = 'Customer Care Sales Lead';

insert into pto.authorization values ('Market','Marketing Manager','Customer Care Center','Customer Care Sales Manager',4,4);

--/> 06/23/21 -------------------------------------------------------------------------------------------

09/16/20
Hi Jon-

Could you please add Steve Bergh as the PTO Admin for the Car Wash Assistant Manager position?  
And then Brittany Haarstad and Narumi Dyer should both be listed as Assistant Managers.  
And then could you change the Detail Cashier PTO Admin to Tyler Hagen?

Thanks!

Laura Roth 




-- ok, none of the current car wash positions are an authorizer
select *,
  (select pto.employee_is_authorizer(b.employee_number))
from pto.department_positions a
left join  pto.position_fulfillment b on a.department = b.department and a.the_position = b.the_position
join pto.employees c on b.employee_number = c.employee_number
  and c.active
where a.department = 'car wash'



-- Could you please add Steve Bergh as the PTO Admin for the Car Wash Assistant Manager position?  
select a.employee_name, a.pymast_employee_number, b.department, b.the_position
from arkona.ext_pymast a
join pto.position_fulfillment b on a.pymast_employee_number = b.employee_number
where a.employee_last_name = 'bergh'

select *
from pto.authorization 
where auth_by_department = 'appearance'
  and auth_by_position = 'director'

insert into pto.authorization values('Appearance','Director','Car Wash','Assistant Manager',3,4);

-- And then Brittany Haarstad and Narumi Dyer should both be listed as Assistant Managers.  
select a.employee_name, c.*
from arkona.ext_pymast a
join pto.employees b on a.pymast_employee_number = b.employee_number
  and b.active
left join pto.position_fulfillment c on b.employee_number = c.employee_number  
where employee_last_name in ('kobayashi-dyer','haarstad')

update pto.position_fulfillment
set the_position = 'Assistant Manager'
where employee_number = '145982';

-- And then could you change the Detail Cashier PTO Admin to Tyler Hagen?
-- tanner smith is the current detail cashier, authorized by steve bergh
select * 
from pto.position_fulfillment a
join pto.employees b on a.employee_number = b.employee_number
  and b.active
where a.department = 'detail'
  and a.the_position =  'cashier'

select *
from pto.position_fulfillment a
join pto.employees b on a.employee_number = b.employee_number
  and b.active
where a.department = 'detail'
  and a.the_position = 'manager' 

select * 
from pto.authorization
where auth_for_Department = 'detail'
  and auth_for_position = 'cashier'

update pto.authorization
set auth_by_Department = 'Detail', auth_by_position = 'Manager'
where auth_for_department = 'Detail' and auth_for_position = 'Cashier';
    

-- holy shit tyler hagen's personal pto page is blank

This all lead to realizing that Detail:Manager had no authorizer, turns out it is randy sattler but he is 
Still defined as body shop manager, so, randy is market : Fixed Operations Manager over Joel, Andrew
John Gardner and  Tyler

select * from pto.department_positions where the_position like '%director%'
-- all these directors, anybody doing it?
select b.*, c.employee_name 
from pto.department_positions a
left join pto.position_fulfillment b on a.department = b.department
  and a.the_position = b.the_position
left join arkona.ext_pymast c on b.employee_number = c.pymast_employee_number  
where a.the_position = 'director'

1. change randys department to market position to fixed operations director
2. is randy authorizing anyone?
select * 
from pto.authorization 
where auth_by_department = 'body shop' and auth_by_position = 'directory'
3. nope, go ahead and change is department & position
4. does the position exists ?
select * from pto.positions where the_position like '%director%'
5. nope, there is an unused position of Fixed Director, lets just change that
update pto.positions
set the_position = 'Fixed Operations Director'
where the_position = 'Fixed Director';
6. add it to department_positions
insert into pto.department_positions values ('Market','Fixed Operations Director',2,'no_policy_html')
7. no change randy
update pto.position_fulfillment
set department = 'Market', the_position = 'Fixed Operations Director'
where employee_number = '1122342';

8.fix tyler  195975
insert into pto.authorization
values('Market','Fixed Operations Director','Detail','Manager',2,3);

9. joel  230601
select * from pto.employees where user_name like 'jdang%'
select * from pto.position_fulfillment where employee_number = '230601'
insert into pto.authorization
values('Market','Fixed Operations Director','RY2 Main Shop','Manager',2,3);

10. john g 150120
select * from pto.employees where user_name like 'jgard%'
select * from pto.position_fulfillment where employee_number = '150120'
insert into pto.authorization
values('Market','Fixed Operations Director','Body Shop','Manager',2,3);

delete from pto.authorization where auth_for_department = 'body shop' and auth_for_position = 'manager' and auth_by_position = 'general manager'

11. andrew 1102195
select * from pto.employees where user_name like 'aneum%'
select * from pto.position_fulfillment where employee_number = '1102195'

update pto.authorization
set auth_by_department = 'Market', auth_by_position = 'Fixed Operations Director'
where auth_for_department = 'RY1 Service' and auth_for_position = 'Director'


select a.employee_name, c.*, d.*
from arkona.ext_pymast a
join pto.employees b on a.pymast_employee_number = b.employee_number
join pto.position_fulfillment c on b.employee_number = c.employee_number
left join pto.authorization d on c.department = d.auth_for_department
  and c.the_position = d.auth_for_position
where a.employee_last_name = 'hagen'
  and a.employee_first_name = 'tyler'

------------------------------------------------------------
--/> 8. heirarchy admin
------------------------------------------------------------


------------------------------------------------------------
--< 9. allocation editing
------------------------------------------------------------
-- 09/21/20
-- When we hired Dave Bies in 2019 as part of his job offer, he was given 3 weeks of PTO.  Currently the vision page shows he has 
-- only earned 72 for this year.  I need it to show that Dave Bies has 120 hours of PTO as per our contract.  
-- His PTO expiration of 1/28/2019 will remain the same.
-- Thank  you
-- John Gardner

select * from pto.employee_pto_Allocation where employee_number = '157953' order by from_date

update pto.employee_pto_allocation
set hours = 120
where employee_number = '157953'
  and from_date in ('2020-01-28','2021-01-28','2022-01-28','2023-01-28','2024-01-28','2025-01-28','2026-01-28','2027-01-28','2028-01-28');




------------------------------------------------------------
--/> 9. allocation editing
------------------------------------------------------------


------------------------------------------------------------
--< 10. change pto anniversary
------------------------------------------------------------
-------------------------
--< 11/09/21
-------------------------
Ben Johnsons PTO date on Vision and compli do not match up, can you look at see if he has any PTO for me please.
John Gardner

He went from part time to full time on 1/23/2017 so that is his PTO anniversary even though he started in 2014.
Laura Roth 

Vision is currently showing 1/20/2017 as his anniversary, so i will change it to 1/23/17

select * from pto.employees where employee_number = '184620'
1.
update pto.employees
set pto_anniversary = '01/23/2017', anniversary_type ='Latest'
where employee_number = '184620';

select * 
from pto.employee_pto_allocation
where employee_number = '184620'

update pto.employee_pto_allocation
set from_date = from_date + 3, thru_date = thru_date + 3
where employee_number = '184620';

-------------------------
--/> 11/09/21
-------------------------

Hi Jon-

Isaiah Johnson’s PTO Anniversary date should be 10/1/2020.  Could you please correct that for me?

Thanks!
10/7/20
Laura Roth 

select * from arkona.ext_pymast where employee_last_name = 'johnson' and employee_first_name = 'isaiah'
hire_date = 6/29/20

select * from pto.employees where employee_number = '195675'

1. update pto.employees
update pto.employees
set pto_anniversary = '10/01/2020', anniversary_type = 'Other'
where employee_number = '195675';

select * 
from pto.employee_pto_allocation
where employee_number = '195675'

2. remove allocation data
delete 
from pto.employee_pto_allocation
where employee_number = '195675';

3. create new allocation data

    insert into pto.employee_pto_allocation (employee_number,from_date,thru_date,hours)
    SELECT c.employee_number, 
      (pto_anniversary + interval '1 year' * c.n)::date AS from_date,
      CASE c.n
        WHEN 20 THEN '12/31/9999'::date
        else (((pto_anniversary + interval '1 year' * (c.n::integer + 1))::date) - interval '1 day')::date
      END AS thruDate,
      d.pto_hours  
    FROM (
      SELECT a.employee_number, a.pto_Anniversary, b.*
      FROM pto.employees a,
        (
          SELECT n
          FROM dds.tally
          WHERE n BETWEEN 0 AND 20) b  
      WHERE a.employee_number = '195675') c   
    LEFT JOIN pto.categories d on c.n BETWEEN d.from_years_tenure AND d.thru_years_tenure;  

2/1/21
Could you please correct Sara Roger’s PTO anniversary date to 6/2/2019?  She should show that she used 52 hours of PTO so far this year and that she has 20 left.
Thanks!
Laura Roth   

select * from pto.employees where employee_number = '165780'  
-- 1. update pto.employees
update pto.employees
set pto_anniversary = '06/02/2019', anniversary_type = 'Other'
where employee_number = '165780';

-- 2. remove allocation data
delete 
from pto.employee_pto_allocation
where employee_number = '165780';

-- 3. create new allocation data
insert into pto.employee_pto_allocation (employee_number,from_date,thru_date,hours)
SELECT c.employee_number, 
  (pto_anniversary + interval '1 year' * c.n)::date AS from_date,
  CASE c.n
    WHEN 20 THEN '12/31/9999'::date
    else (((pto_anniversary + interval '1 year' * (c.n::integer + 1))::date) - interval '1 day')::date
  END AS thruDate,
  d.pto_hours  
FROM (
  SELECT a.employee_number, a.pto_Anniversary, b.*
  FROM pto.employees a,
    (
      SELECT n
      FROM dds.tally
      WHERE n BETWEEN 0 AND 20) b  
  WHERE a.employee_number = '165780') c   
LEFT JOIN pto.categories d on c.n BETWEEN d.from_years_tenure AND d.thru_years_tenure;  
------------------------------------------------------------
--/> 10. change pto anniversary
------------------------------------------------------------


------------------------------------------------------------
--< 11. new position
------------------------------------------------------------
new postion: Market:Global Inventory Manager
authorized by Market:General Manager

select * from pto.department_positions order by department, the_position

insert into pto.positions values('Global Inventory Manager');
insert into pto.department_positions values('Market','Global Inventory Manager', 4, 'no_policy_html');
insert into pto.authorization values('Market','General Manager','Market','Global Inventory Manager',2,4);

----------------------------------------------------
new postion: RY2 Main Shop:Service Drive Manager
authorized by RY2 Main Shop:manager

select * from pto.positions order by the_position
select * from pto.department_positions order by department, the_position

insert into pto.positions values('Service Drive Manager');
insert into pto.department_positions values('RY2 Main Shop','Service Drive Manager',5, 'no_policy_html');
insert into pto.authorization values('RY2 Main Shop','Manager','RY2 Main Shop','Service Drive Manager',4,5);

new postion: Detail:Photo Specialist
authorized by Detail:manager

select * from pto.positions order by the_position
select * from pto.department_positions where department = 'detail' order by the_position
select * from pto.authorization where auth_by_department = 'detail'

insert into pto.positions values('Photo Specialist');
insert into pto.department_positions values('Detail','Photo Specialist',5, 'detail_policy_html');
insert into pto.authorization values('Detail','Manager','Detail','Photo Specialist',4,5);


new postion: Parts:Counterperson
authorized by Detail:manager

select * from pto.positions order by the_position
select * from pto.department_positions where department = 'ry1 parts' order by the_position
select * from pto.authorization where auth_by_department = 'ry1 parts'

insert into pto.positions values('Counterperson');
insert into pto.department_positions values('RY1 Parts','Counterperson',4, 'parts_policy_html');
insert into pto.authorization values('RY1 Parts','Assistant Manager','RY1 Parts','Counterperson',3,4);

select * from pto.position_fulfillment where department = 'ry1 parts' and the_position = 'assistant manager'
select employee_name from arkona.ext_pymast where pymast_employee_number = '168351'
select * From pto.authorization where auth_by_department = 'ry1 parts'

new position: body shop:manager trainee
authorized by body shop: manager
insert into pto.positions values('Manager Trainee');
insert into pto.department_positions values('Body Shop','Manager Trainee',4, 'collision_center_policy_html');
insert into pto.authorization values('Body Shop','Manager Trainee','Body Shop','Manager',3,4);






------------------------------------------------------------
--/> 11. new position
------------------------------------------------------------

------------------------------------------------------------
--< 12.  authorizer change 
------------------------------------------------------------
--< 9/1/21 -----------------------------------------------------------------------------------------
Hey Jon-

Effective 8/29/2021 – Tyler Hagen is taking over PDQ Manager and will be PTO Admin for that role.  
Hannah Appleby will be Detail Manager and taking PTO Manager for that role.

Thanks!

i asked what is to become of dayton marek
He’s going to be the Service Drive Manager at Honda.  
I just created a new username # for him under Honda and switched him in Compli.  That # is 290910.

so i can go ahead am make the hannah and tyler moves, then create a new position and assign darek

select *
from pto.authorization
where auth_by_department in ('ry1 pdq','detail')
order by auth_by_department

select a.*, b.employee_name
from pto.position_fulfillment a
-- join pto.employees b on a.employee_number = b.employee_number
join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
where a.department in ('detail','ry1 pdq')
  and a.the_position = 'manager'

tyler: 195975
select pymast_employee_number, employee_name from arkona.ext_pymast where employee_last_name = 'appleby'
137842

select * from pto.position_fulfillment
-- pdq
update pto.position_fulfillment
set employee_number = '195975'
-- select * from pto.position_fulfillment
where department = 'ry1 pdq'
  and the_position = 'manager';
--detail
update pto.position_fulfillment
set employee_number = '137842'
where department = 'detail'
  and the_position = 'manager';
  
--/> 9/1/21 -----------------------------------------------------------------------------------------

--< 10/13/2020 Laura --------------------------------------------------------------
Could you please move Sam Foster to Customer Care Sales Lead in Vision?  

Dylanger Haley is currently in that position.
Since that position is responsible for authorizing pto for the customer car sales specialist position, 
only one person can be in the position of Customer Care Sales Lead.
So, we either need to change Sam or Dylan’s position or change the pto authorization hierarchy.

Hey Jon – Dylan is going to be moving back to Sales at the GM store as a Sales Manager so you can move him there.  
He’s going to train with Sam for a few weeks first.  Thanks!

If you don’t mind, I think I will hold off on this for now.  Sam training for a few weeks does not feel like a sure thing to me.

He’s for sure moving in to that role, he’s been promoted.  He just needs to train with Dylan for a couple weeks before Dylan moves.  
So you can go ahead and move him because I have already processed the transfer and won’t remember to send another email about it.


select distinct the_position
from pto.department_positions
where department = 'RY1 Sales'
order by the_position;

select b.employee_name, a.* from pto.position_fulfillment a
join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
and the_position = 'general sales manager'

-- currently no position RY1 Sales:Manager
insert into pto.department_positions values('RY1 Sales','Manager',4,'no_policy_html');
-- change dylan
update pto.position_fulfillment
set department = 'RY1 Sales', the_position = 'Manager'
where employee_number = '163700';
-- change sam
update pto.position_fulfillment
set department = 'Customer Care Center', the_position = 'Customer Care Sales Lead'
where employee_number = '148080';

*** need to give sam PTO Tracking and PTO Search in Vision ***



--/> 10/13/2020 Laura --------------------------------------------------------------

------------------------------------------------------------
--/> 12.  authorizer change 
------------------------------------------------------------

------------------------------------------------------------
--< 13. manually enter a new employee
------------------------------------------------------------

-- extracted from FUNCTION pto.insert_employee(citext, citext, citext, date, numeric);

do $$
declare
  _emp citext := '265258';
  _dept citext := 'RY2 PDQ';
  _position citext := 'Technician';
  _anniv_date date := '09/21/2020';
  _hours_eanred numeric := 0;
  _user_name citext := (
    select lower(email)
    from pto.compli_users
    where user_id = _emp);  
  _anniv_type citext := (
    select 
      case 
        when _anniv_date = dds.db2_integer_to_date(hire_date) then 'Latest'
        When _anniv_date = dds.db2_integer_to_date(org_hire_date) then 'Original'
        else 'Other'
      end
    from arkona.ext_pymast
    where pymast_employee_number = _emp);    
begin  
  insert into pto.employees (employee_number, pto_anniversary, user_name, anniversary_type,active)
  values(_emp, _anniv_date, _user_name, _anniv_type, true);
  insert into pto.position_fulfillment (department,the_position,employee_number)
  values(_dept,_position,_emp);
  insert into pto.employee_pto_allocation (employee_number,from_date,thru_date,hours)  
  SELECT c.employee_number, 
    (pto_anniversary + interval '1 year' * c.n)::date AS from_date,
    CASE c.n
      WHEN 20 THEN '12/31/9999'::date
      else (((pto_anniversary + interval '1 year' * (c.n::integer + 1))::date) - interval '1 day')::date
    END AS thruDate,
    d.pto_hours  
  FROM (
    SELECT a.employee_number, a.pto_Anniversary, b.*
    FROM pto.employees a,
      (
        SELECT n
        FROM dds.tally
        WHERE n BETWEEN 0 AND 20) b  
    WHERE a.employee_number = '259875') c   
  LEFT JOIN pto.categories d on c.n BETWEEN d.from_years_tenure AND d.thru_years_tenure; 
end $$;    
------------------------------------------------------------
--/> 13. manually enter a new employee
------------------------------------------------------------

------------------------------------------------------------
--/> 14. large authorization change
------------------------------------------------------------
-- 11/09/2020
-- Hey Jon –
-- Could you please help us out with this one?  Everyone should be put under Brittany except Roger.
-- Thanks!
-- Laura Roth 
-- 
-- Good Morning and Happy Saturday Laura!  Car Wash management team had Zoom meeting with Ben yesterday going over what 
-- each manager’s “lane” of responsibility is moving forward.  Brittany is in charge of anything dealing with team members 
-- moving forward.  Could you do what needs to be done to put all Car Wash team members PTO administration under Brittany 
-- in Rydell Vision?  The only one to remain under my administration would be Roger.  Your help is greatly appreciated.

-- jon:
-- Currently, both Brittany and Narumi Kobayashi-Dyer are assigned the position of Car Wash Assistant Manager.
-- In the current system, only one person can fill any position responsible for authorizing PTO.
-- So, for Brittany to assume the responsibility of authorizing PTO for Car Wash employees, either Brittany or Narumi 
-- need to be classified as something other than Car Wash Assistant Manager.
-- 
-- laura: 
-- Okay we might need to create a new title for Brittany.  She’s going to be the Assistant Manager who is mainly 
-- responsible for people/talent so maybe we create a title in there called Car Wash Talent Manager and have her under that role?

select a.auth_by_department, a.auth_by_position, a.auth_for_position, b.employee_number, c.active, d.employee_name, d.termination_date
from pto.authorization a
left join pto.position_fulfillment b on a.auth_for_department = b.department
  and a.auth_for_position = b.the_position
left join pto.employees c on b.employee_number = c.employee_number  
left join arkona.ext_pymast d on c.employee_number = d.pymast_employee_number
where auth_for_department = 'car wash'
order by a.auth_for_position

select *
from pto.department_positions
where department = 'car wash'
-- create the new position
insert into pto.positions values ('Talent Manager');
insert into pto.department_positions values ('Car Wash','Talent Manager',3, 'car_wash_policy_html');
-- assign it to brittany
update pto.position_fulfillment
set the_position = 'Talent Manager'
where employee_number = '145982';
-- assign steve as her authorizer
insert into pto.authorization(auth_by_department,auth_by_position,auth_for_department,auth_for_position)
values ('Appearance','Director','Car Wash','Talent Manager');
-- make her the authorizer of all other car wash positions, except roger (Maintenance Technician)
update pto.authorization
set auth_by_department = 'Car Wash',
    auth_by_position = 'Talent Manager'
where auth_for_department = 'Car Wash'
  and auth_for_position in ('Cashier','Team Member','Manager','Assistant Manager');
------------------------------------------------------------
--/> 14. large authorization change
------------------------------------------------------------



------------------------------------------------------------
--< 15. donate to cares fund
------------------------------------------------------------
/*
Could you also take 8 hours out of Rudy Robles account?  He donated that to the Cares Fund.
Laura Roth 
11/30/20
*/
select * from pto.adjustments;
select * from pto.employees where user_name like '%robles%'; -- 1117600
select * from pto.employee_pto_allocation where employee_number = '1117600' and current_date between from_date and thru_date; -- 7/12/2020
insert into pto.adjustments values ('1117600','07/12/2020',-8,'Donation','Cares Fund');

------------------------------------------------------------
--/> 15. donate to cares fund
------------------------------------------------------------


