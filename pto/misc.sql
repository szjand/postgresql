﻿/* advantage
select a.appname, a.appcode, a.approle, b.username
FROM applicationmetadata a
LEFT JOIN employeeappauthorization b on a.appname = b.appname 
  AND a.appcode = b.appcode
  AND a.approle = b.approle
WHERE a.appname = 'pto'

select a.appname, a.appcode, a.approle, COUNT(*)
FROM applicationmetadata a
LEFT JOIN employeeappauthorization b on a.appname = b.appname 
  AND a.appcode = b.appcode
  AND a.approle = b.approle
WHERE a.appname = 'pto'
GROUP BY a.appname, a.appcode, a.approle
*/


-- authorization

select a.auth_by_department, a.auth_by_position, b.employee_number, c.employee_last_name, c.employee_first_name
from (
  select auth_by_department, auth_by_position
  from pto.authorization 
  group by auth_by_department, auth_by_position) a
join pto.position_fulfillment b on a.auth_by_department = b.department
  and a.auth_by_position = b.the_position
join arkona.ext_pymast c on b.employee_number = c.pymast_employee_number  
order by a.auth_by_department, a.auth_by_position

-- these folks need manager access
select b.employee_number, c.employee_last_name, c.employee_first_name
from (
  select auth_by_department, auth_by_position
  from pto.authorization 
  group by auth_by_department, auth_by_position) a
join pto.position_fulfillment b on a.auth_by_department = b.department
  and a.auth_by_position = b.the_position
join arkona.ext_pymast c on b.employee_number = c.pymast_employee_number  
order by c.employee_first_name


-- matching allocation hours to calculated hours based on anniv
select * from (
select a.employee_number, aa.employee_last_name, aa.employee_first_name, a.pto_anniversary, b.hours, age(current_date, a.pto_anniversary),
  b.from_date, b.thru_date, 
  date_part('year',age(current_date, a.pto_anniversary))::integer, -- extract the year from the age() function
  c.pto_hours,
  (a.pto_anniversary + date_part('year',age(current_date, a.pto_anniversary))::integer*'1 year'::interval)::date -- add the year from the age() function to pto_anniversary
from pto.employees a
join arkona.ext_pymast aa on a.employee_number = aa.pymast_employee_number
join pto.employee_pto_allocation b on a.employee_number = b.employee_number
  and current_date between b.from_date and b.thru_date
join pto.categories c on  date_part('year',age(current_date, a.pto_anniversary))::integer between c.from_years_tenure and c.thru_years_tenure
where a.active
order by age(current_date, a.pto_anniversary) desc 
) x where hours <> pto_hours  --where employee_number = '275342' --






 -- a query to determine an employees pto allocation for the current period without pto.employee_pto_allocation 
-- start with zak louden, 40 hours adj in first year
select a.employee_number, aa.employee_last_name, aa.employee_first_name, a.pto_anniversary, c.*
from pto.employees a
join arkona.ext_pymast aa on a.employee_number = aa.pymast_employee_number
join pto.categories c on  extract('year' from age(current_date, a.pto_anniversary))::integer between c.from_years_tenure and c.thru_years_tenure 
where a.active
order by employee_last_name


select * from pto.adjustments where employee_number = '1124875'

select * from pto.adjustments where extract(year from  from_date ) = 2020

select * from pto.employee_pto_allocation where employee_number = '1124875' order by from_date



-- calculate years employed since pto anniv, from and thru date of current pto period, all based on pto anniv
select * from (
select a.employee_number, current_date, pto_anniversary, extract('year' from age(current_date, a.pto_anniversary))::integer as years,
  (pto_anniversary + interval '1 year' * extract('year' from age(current_date, a.pto_anniversary))::integer)::date as calc_from,
  (((pto_anniversary + interval '1 year' * (extract('year' from age(current_date, a.pto_anniversary))::integer + 1))::Date)
      - interval '1 day')::date as calc_thru,
  b.from_date, b.thru_date
from pto.employees a
join pto.employee_pto_allocation b on a.employee_number = b.employee_number
  and current_date between b.from_date and b.thru_date  
) x where calc_from <> from_date or calc_thru <> thru_date


-------------------------------------------------------------------------------------------------
--< leap year
-------------------------------------------------------------------------------------------------
-- 1 active, 2 inactive
select * 
from pto.employees
where active
  and extract(month from pto_anniversary) = 2 
  and extract(day from pto_anniversary) = 29

select * from pto.employee_pto_allocation where employee_number in('1109819','1109836','173489') order by employee_number, from_date

-- the effect of someone being hired on 02/29 is that, each year the from date alternates between 02/29 and 03/01

-- only old timers (+20) with an anniv of 03/01
select * 
from pto.employees
where active
  and extract(month from pto_anniversary) = 3 
  and extract(day from pto_anniversary) = 1
  
select * from pto.employee_pto_allocation where employee_number in('1147250','179775','116185') order by employee_number, from_date

-- the effect of being hired on 03/01 is every 4th year the thru is 02/29 vs 02/28 
    SELECT 
      (pto_anniversary + interval '1 year' * c.n)::date AS from_date,
      CASE c.n
        WHEN 20 THEN '12/31/9999'::date
        else (((pto_anniversary + interval '1 year' * (c.n::integer + 1))::date) - interval '1 day')::date
      END AS thruDate,
      d.pto_hours  
    FROM (
          SELECT '03/01/2016'::date as pto_anniversary, n
          FROM dds.tally
          WHERE n BETWEEN 0 AND 20) c   
    LEFT JOIN pto.categories d on c.n BETWEEN d.from_years_tenure AND d.thru_years_tenure;


      

select '02/29/2020'::date + interval '1 year'
select '02/28/2021':: date - '02/29/2020'::date 

select extract(year from age('02/28/2021':: date, '02/29/2020'::date)) 




-------------------------------------------------------------------------------------------------
--/> leap year
-------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------
--< transition time adjustments
--------------------------------------------------------------------------------------
-- allocation from date does not match anniversary date

select a.active, a.employee_number, b.employee_first_name, b.employee_last_name ,
   a.pto_anniversary, c.from_date, age(c.from_date,a.pto_anniversary)
from pto.employees a
join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
join (
  select employee_number, max(from_date) as from_date
  from pto.employee_pto_allocation
  group by employee_number) c on a.employee_number = c.employee_number
where a.active
  and (
    extract('day' from age(c.from_date,a.pto_anniversary))::integer <> 0
    or
    extract('month' from age(c.from_date,a.pto_anniversary))::integer <> 0) 

------------------------------------------------------------------
-- zakery louden, 275342, HR gave him 40 hours in first year, change it from pto.employee_pto_allocation to pto.adjustments 
INSERT INTO pto.adjustments values('275342','05/10/2020',40,'Adjustment','PTO hours added by HR for first year of employment');
UPDATE pto.employee_pto_allocation
SET hours = 0
WHERE employee_number = '275342' 
  AND from_date = '05/10/2020';

------------------------------------------------------------------
-- anastasia gagnon
UPDATE pto.employees
SET pto_anniversary = '08/01/2019'
-- select * FROM ptoemployees 
WHERE employee_number = '155214' 
----------------------------------------------------------------
dylan haley 163700
orig hire date 9/26/16
pto anniversary is 6/5/17
select active_code, department_code, distrib_code, row_from_date from arkona.xfm_pymast where pymast_employee_number = '163700' order by pymast_key

select * from pto.employee_pto_allocation where employee_number = '163700' order by from_date

select * from ads.ext_edw_employee_dim where employeenumber = '163700' order by employeekey

update pto.employees
set pto_anniversary = '09/26/2016'
where employee_number = '163700'
----------------------------------------------------------------
dayton marek 190910
pto anniv 09/25/2015
orig hire 6/9/11
last hire 5/5/17
became full time 05/09/2017

select active_code, department_code, distrib_code, hire_date, org_hire_date, termination_date, row_from_date from arkona.xfm_pymast where pymast_employee_number = '190910' order by pymast_key

select * from ads.ext_edw_employee_dim where employeenumber = '190910' order by employeekey

select * from pto.employee_pto_allocation where employee_number = '190910' order by from_date

select * from pto.categories

update pto.employees
set pto_anniversary = '05/05/2017'
where employee_number = '190910'

----------------------------------------------------------------

braden fraser 247000
pto anniv 07/17/2017
orig hire 10/05/2017
last hire 07/17/2019

select active_code, department_code, distrib_code, hire_date, org_hire_date, termination_date, row_from_date from arkona.xfm_pymast where pymast_employee_number = '247000' order by pymast_key

select * from ads.ext_edw_employee_dim where employeenumber = '247000' order by employeekey

select * from pto.employee_pto_allocation where employee_number = '247000' order by from_date

delete from pto.employee_pto_allocation where employee_number = '247000';
insert into pto.employee_pto_allocation (employee_number,from_date,thru_date,hours)
    SELECT c.employee_number, 
      (pto_anniversary + interval '1 year' * c.n)::date AS from_date,
      CASE c.n
        WHEN 20 THEN '12/31/9999'::date
        else (((pto_anniversary + interval '1 year' * (c.n::integer + 1))::date) - interval '1 day')::date
      END AS thruDate,
      d.pto_hours  
    FROM (
      SELECT a.employee_number, a.pto_Anniversary, b.*
      FROM pto.employees a,
        (
          SELECT n
          FROM dds.tally
          WHERE n BETWEEN 0 AND 20) b  
      WHERE a.employee_number = '247000') c   
    LEFT JOIN pto.categories d on c.n BETWEEN d.from_years_tenure AND d.thru_years_tenure;

    
----------------------------------------------------------------
-- matt paschke
/*
As we work on moving the PTO admin into new vision, we’re trying to clean up stuff as we go.

Matt Paschke was rehired on 8/26/2019, but his PTO anniversary was set as 08/26/2018, and he was granted 80 hours of pto for 08/26/2019 thru 08/26/2020
What should his pto allocation be for the period starting on 08/26/2020?
My guess is that it should be 112 hours and remain there until 08/26/28, at which time it would become 152.

Let me know what it should be please and I will update the program accordingly

Thanks

jon

Hi Jon-

You are correct in your guess below.  Let me know if you need anything else from me.

Thanks!
*/

anniv: 08/26/2018
select * from pto.employees where employee_number = '1109700'

select * from pto.employee_pto_allocation where employee_number = '1109700' order by from_date

-- reset his allocation, add 8hrs adjustment for period of 08/26/2019
delete from pto.employee_pto_allocation where employee_number = '1109700'
insert into pto.employee_pto_allocation (employee_number,from_date,thru_date,hours)
SELECT c.employee_number, 
  (pto_anniversary + interval '1 year' * c.n)::date AS from_date,
  CASE c.n
    WHEN 20 THEN '12/31/9999'::date
    else (((pto_anniversary + interval '1 year' * (c.n::integer + 1))::date) - interval '1 day')::date
  END AS thruDate,
  d.pto_hours  
FROM (
  SELECT a.employee_number, a.pto_Anniversary, b.*
  FROM pto.employees a,
    (
      SELECT n
      FROM dds.tally
      WHERE n BETWEEN 0 AND 20) b  
  WHERE a.employee_number = '1109700') c   
LEFT JOIN pto.categories d on c.n BETWEEN d.from_years_tenure AND d.thru_years_tenure;

insert into pto.adjustments values('1109700','08/26/2019',8,'adjustment','per HR on rehire');
--------------------------------------------------------------------------------------
--/> transition time adjustments
--------------------------------------------------------------------------------------  

--------------------------------------------------------------------------------------
--< pto employees that should not be active
--------------------------------------------------------------------------------------  
-- update pto.employees to not active based on pymast
update pto.employees
set active = false
where employee_number in (
  select a.employee_number
  from pto.employees a
  join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
    and b.active_code = 'T'
  where a.active);

--------------------------------------------------------------------------------------
--/> pto employees that should not be active
-------------------------------------------------------------------------------------- 
--------------------------------------------------------------------------------------
--< active employees with no position_fulfillment
--------------------------------------------------------------------------------------  

select a.*, c.active_code
from pto.employees a
left join pto.position_fulfillment b on a.employee_number = b.employee_number
left join arkona.ext_pymast c on a.employee_number = c.pymast_employee_number
where a.active
  and b.employee_number is null
  and not exists (
    select 1
    from pto.exclude_employees
    where employee_number = a.employee_number)

-- these 8 are termed and should not be active
update pto.employees
set active = false
where employee_number in ('2120492','156845','173800','115676','267934','134976','245987','165289');

-- these are the active employees from above, no position fulfillment
08/15/20 all fixed
select pymast_employee_number, employee_first_name, employee_last_name, active_code, department_code, 
  distrib_code, dds.db2_integer_to_date(hire_date) as hire_date, 
  dds.db2_integer_to_date(org_hire_date) as orig_hire_date, dds.db2_integer_to_date(termination_date) as term_date, 
  b.pto_anniversary, c.*
from arkona.ext_pymast a
left join pto.employees b on a.pymast_employee_number = b.employee_number
left join pto.employee_pto_allocation c on b.employee_number = c.employee_number
  and current_Date between c.from_date and c.thru_date
where pymast_employee_number in ('15646')
order by employee_last_name


insert into pto.position_fulfillment values('Detail','Detail Technician','254976');
insert into pto.position_fulfillment values('Detail','Support Specialist','184605');
insert into pto.position_fulfillment values('RY1 PDQ','Express Service Representative','15668');
insert into pto.position_fulfillment values('HR','Manager','12236');
insert into pto.position_fulfillment values('RY1 Sales','Sales Consultant','195231');
insert into pto.position_fulfillment values('RY1 Parts','Sales Consultant','15646');


-- add michael longoria to exclude, ry2 sales floor manager
insert into pto.exclude_employees values('265328');
--------------------------------------------------------------------------------------
--/> active employees with no position_fulfillment
--------------------------------------------------------------------------------------  

--------------------------------------------------------------------------------------
--< position fulfillment on inactive employees
--------------------------------------------------------------------------------------  
-- need to have a discussion about maintaining history before i zap any of this stuff
select c.employee_name, a.employee_number, a.department, a.the_position,  b.active, c.active_code, b.pto_anniversary, 
  dds.db2_integer_to_date(termination_date) as term_date, d.from_date, d.thru_date, d.hours
from pto.position_fulfillment a
left join pto.employees b on a.employee_number = b.employee_number
left join arkona.ext_pymast c on a.employee_number = c.pymast_employee_number
left join pto.employee_pto_allocation d on a.employee_number = d.employee_number
  and dds.db2_integer_to_date(termination_date) between d.from_date and d.thru_date
where b.active = false and c.active_code = 'T'
order by dds.db2_integer_to_date(termination_date)
--------------------------------------------------------------------------------------
--/> position fulfillment on inactive employees
--------------------------------------------------------------------------------------  

--------------------------------------------------------------------------------------
--< current full time employees in pto?
--------------------------------------------------------------------------------------  
-- perfect, only the new employees on the PTO Admin page show up
select a.pymast_employee_number, a.employee_name, a.payroll_class, c.employee_number as excluded
from arkona.ext_pymast a
left join pto.employees b on a.pymast_employee_number = b.employee_number
left join pto.exclude_employees c on a.pymast_employee_number = c.employee_number
where a.active_code = 'A'
  and a.pymast_company_number in ('RY1','RY2')
  and b.employee_number is null

select * from pto.compli_users where last_name = 'winzer'

--------------------------------------------------------------------------------------
--/> current full time employees in pto?
--------------------------------------------------------------------------------------  


--------------------------------------------------------------------------------------
--< compli term report
--------------------------------------------------------------------------------------  
drop table if exists pto.compli_departures;
create table pto.compli_departures (
  employee_number citext not null,
  first_name citext not null,
  last_name citext not null,
  reason citext,
  termination_date date not null,
  store_code citext,
  title citext,
  eligible_for_rehire citext);


select * 
from pto.compli_departures

--------------------------------------------------------------------------------------
--/> compli term report
--------------------------------------------------------------------------------------  

--------------------------------------------------------------------------------------
--< 20 year vets
--------------------------------------------------------------------------------------  

select c.employee_name, a.employee_number, count(*)
from pto.employee_pto_allocation a
join  pto.employees b on a.employee_number = b.employee_number
  and b.active = true
join arkona.ext_pymast c on a.employee_number = c.pymast_employee_number  
where exists (
  select 1
  from pto.employee_pto_allocation
  where employee_number = a.employee_number
    and thru_date = '12/31/9999')  
and not exists (
  select 1
  from pto.exclude_employees
  where employee_number = a.employee_number)
group by c.employee_name, a.employee_number    
order by count(*), c.employee_name

select c.employee_name, a.employee_number, b.pto_anniversary, a.from_date
from pto.employee_pto_allocation a
join  pto.employees b on a.employee_number = b.employee_number
  and b.active = true
join arkona.ext_pymast c on a.employee_number = c.pymast_employee_number  
where a.thru_date = '12/31/9999' 
and not exists (
  select 1
  from pto.exclude_employees
  where employee_number = a.employee_number)
order by a.from_date



--------------------------------------------------------------------------------------
--/> 20 year vets
--------------------------------------------------------------------------------------  

--------------------------------------------------------------------------------------
--< termed employees still active in pto.employees
--------------------------------------------------------------------------------------  
select b.employee_name, a.*, dds.db2_integer_to_date(b.termination_date)
from pto.employees a
join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
where a.active 
  and b.active_code = 'T'

select * 
from pto.position_fulfillment a
join pto.employees b on a.employee_number = b.employee_number
  and not b.active  
--------------------------------------------------------------------------------------
--/> termed employees still active in pto.employees
--------------------------------------------------------------------------------------  


--------------------------------------------------------------------------------------
--< so many questions
--------------------------------------------------------------------------------------  


still need to do

02/29 hire dates



all full time employees in pto.employees?

clarification on ATO

20 year vets just add the rows (i think seeba is an exception)

what to do when a person goes from full time to part time

what to d with allocation when someone is termed (frequent need to determine remaining pto after term)

transfer between stores

add admin function to adjust current period pto

snapshot pto @ term to have record of pto due at term

editing
what about when there is a new department/position
almost need an edit menu
same thing for new employees
  first time employed
  rehire
  xfr between stores
  term
  change from full to part time
  change depart/position
  create an adustment(+ or -) for current period

edit the authorization hierarchy

need pg version of payout function/email  

do managers need an overview view of their dept by week/month/quarter/year?  

how to maintain when jobs are in dealertrack, compli and here and never the twain shall meet
and of course, managers do not coincide either
look at benjamin bina 254976

should laura be ATO?

does this system need to maintain pto history?

no system currently tells me the active roster
  DT


afton todo
meeting agenda
    include Kim
    edit How PTO Works page
    review the PTO policies pages
    who is ato
    who is available for payout
    payout email
    larry stadstad, PT consultant, PTO?
immediate problems:
    ann ostlund 1108200, shows deficit of -56 hours, that is incorrect, the approved hours does not
        recognize the pto period and the current date
        select * from pto.used_pto where employee_number = '1108200' and the_date >= '01/17/2020' order by the_date
        current date is 08/20, the approved 40 hours in august 20 is for the 10th - 14th, which she used
        approved for august should show 0 
processes:
  term
  change full to part
  change part to full
  new hire
  rehire
  xfr store
  new department/position
  authorizer leaves position
  change hierarchy
  Kayla Budeau: hired 3/27/18, anniv: 5/20/19, part: 8/14/20 earned: 72, used 21 i she entitled to payout
      
      
select * from arkona.xfm_pymast where pymast_employee_number = '175873' order by pymast_key   
--------------------------------------------------------------------------------------
--.> so many questions
--------------------------------------------------------------------------------------  



--------------------------------------------------------------------------------------
--> need an org chart
--------------------------------------------------------------------------------------  

select aa.*, bb.auth_for_department, bb.auth_for_position, bb.employee_name, bb.pymast_employee_number, bb.active_code
from (
  select distinct a.auth_by_department, a.auth_by_position, c.employee_name, c.pymast_employee_number 
  from pto.authorization a
  left join pto.position_fulfillment b on a.auth_by_department = b.department
    and a.auth_by_position = b.the_position
  left join arkona.ext_pymast c on b.employee_number = c.pymast_employee_number  
  join pto.employees d on b.employee_number = d.employee_number  -- eliminate terms
    and d.active) aa
left join (
  select a.*, c.pymast_employee_number, c.employee_name, c.active_code
  from pto.authorization a
  left join pto.position_fulfillment b on a.auth_for_department = b.department
    and a.auth_for_position = b.the_position
  left join arkona.ext_pymast c on b.employee_number = c.pymast_employee_number  
  join pto.employees d on b.employee_number = d.employee_number  -- eliminate terms
    and d.active) bb on aa.auth_by_department = bb.auth_by_department
    and aa.auth_by_position = bb.auth_by_position
order by aa.auth_by_department, aa.auth_by_position, bb.auth_for_department, bb.auth_for_position

-- format for spreadsheet
select aa.auth_by_department || ':' || aa.auth_by_position as "dept:position", 
  aa.employee_name as name, aa.pymast_employee_number as "emp #",
  bb.auth_for_department || ':' || bb.auth_for_position as "dept:position",
  bb.employee_name as name, bb.pymast_employee_number as "emp #"
from (
  select distinct a.auth_by_department, a.auth_by_position, c.employee_name, c.pymast_employee_number 
  from pto.authorization a
  left join pto.position_fulfillment b on a.auth_by_department = b.department
    and a.auth_by_position = b.the_position
  left join arkona.ext_pymast c on b.employee_number = c.pymast_employee_number  
  join pto.employees d on b.employee_number = d.employee_number  -- eliminate terms
    and d.active) aa
left join (
  select a.*, c.pymast_employee_number, c.employee_name, c.active_code
  from pto.authorization a
  left join pto.position_fulfillment b on a.auth_for_department = b.department
    and a.auth_for_position = b.the_position
  left join arkona.ext_pymast c on b.employee_number = c.pymast_employee_number  
  join pto.employees d on b.employee_number = d.employee_number  -- eliminate terms
    and d.active) bb on aa.auth_by_department = bb.auth_by_department
    and aa.auth_by_position = bb.auth_by_position
order by aa.auth_by_department, aa.auth_by_position, bb.auth_for_department, bb.auth_for_position, bb.employee_name



select a.department, a.the_position, count(b.employee_number)--, string_agg(b.employee_number, ',')
from pto.department_positions a
left join pto.position_fulfillment b on a.department = b.department
  and a.the_position = b.the_position
  and exists (
    select 1
    from pto.employees
    where employee_number = b.employee_number
      and active)
group by a.department, a.the_position
order by a.department, a.the_position


--------------------------------------------------------------------------------------
--/> need an org chart
--------------------------------------------------------------------------------------  

select * from pto.employees

select *
from arkona.ext_pymast where employee_last_name = 'greer'