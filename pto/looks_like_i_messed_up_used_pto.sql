﻿ah fuck, looks like i fucked up pto used

-------------------------------------------------------------------
09/10
need a query that shows me the pto period, used pto (from pto and arkona) for that period for each employee
need the current period

-- 304 employees
select aa.employee_name, a.employee_number, c.from_date, c.thru_Date, c.hours
  case
    when c.thru_date = '12/31/9999' then
from pto.employees a
join arkona.ext_pymast aa on a.employee_number = aa.pymast_employee_number
left join pto.exclude_employees b on a.employee_number = b.employee_number
left join pto.employee_pto_allocation c on a.employee_number = c.employee_number
  and current_date between c.from_date and c.thru_date
where b.employee_number is null
  and a.active

-- finally a functioning query to return current period
select aa.*, 
  case when from_Date = f_date then null else 'FROM DATE' end,
  case when thru_date = t_date then null else 'THRU DATE' end
from (
  select a.*,
    case
      when thru_date = '12/31/9999' then
        (
          select the_date
          from dds.dim_date
          where month_of_year = extract(month from from_date)
            and day_of_month = extract(day from from_date)
            and the_date <= current_date
          order by the_date desc
          limit 1)
      else a.from_date
    end as f_date,
    case 
      when thru_date = '12/31/9999' then
        (
          select (the_date + interval '1 year')::date - 1
          from dds.dim_date
          where month_of_year = extract(month from from_date)
            and day_of_month = extract(day from from_date)
            and the_date <= current_date
          order by the_date desc
          limit 1)    
      else a.thru_date
    end as t_date
  from pto.employee_pto_allocation a
  join pto.employees b on a.employee_number = b.employee_number
    and b.active
  left join pto.exclude_employees c on b.employee_number = c.employee_number
  -- where employee_number = '1146991'
  WHERE current_date between from_date and thru_date
    and c.employee_number is null) aa
order by hours  

-- put it in a function
DROP FUNCTION pto.get_employee_current_period(citext);
create or replace function pto.get_employee_current_period (IN _emp citext, out employee_number citext, out from_date date, out thru_date date)
as
$BODY$
/*
select * from pto.get_employee_current_period('1146991');
*/
select a.employee_number,
  case
    when thru_date = '12/31/9999' then
      (
        select the_date
        from dds.dim_date
        where month_of_year = extract(month from from_date)
          and day_of_month = extract(day from from_date)
          and the_date <= current_date
        order by the_date desc
        limit 1)
    else a.from_date
  end as f_date,
  case 
    when thru_date = '12/31/9999' then
      (
        select (the_date + interval '1 year')::date - 1
        from dds.dim_date
        where month_of_year = extract(month from from_date)
          and day_of_month = extract(day from from_date)
          and the_date <= current_date
        order by the_date desc
        limit 1)    
    else a.thru_date
  end as t_date
from pto.employee_pto_allocation a
join pto.employees b on a.employee_number = b.employee_number
  and b.active
left join pto.exclude_employees c on b.employee_number = c.employee_number
WHERE a.employee_number = _emp
  and current_date between from_date and thru_date
  and c.employee_number is null;
$BODY$  
language sql;  

-- now, back to comparing current period used pto vs arkona

select * from arkona.xfm_pypclockin limit 10



drop table if exists table_1 cascade;
create temp table table_1 as 
select a.employee_number, b.employee_name, d.from_date, d.thru_date
from pto.employees a
join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
left join pto.exclude_employees c on a.employee_number = c.employee_number
left join pto.get_employee_current_period(a.employee_number) d on true
where a.active
  and c.employee_number is null

-- ok, not as bad as i thought, only 4 folks short on used pto
select aa.*, bb.arkona_pto_hours
from (
  select a.employee_number, a.employee_name, a.from_Date, a.thru_date, coalesce(sum(b.hours), 0) as pto_pto_hours
  from table_1 a
  left join pto.used_pto b on a.employee_number = b.employee_number
    and b.the_date between a.from_date and a.thru_date
  group by a.employee_number, a.employee_name, a.from_Date, a.thru_date) aa
join (
  select a.employee_number, a.employee_name, a.from_Date, a.thru_date, coalesce(sum(c.pto_hours+ c.vac_hours), 0) as arkona_pto_hours
  from table_1 a
  left join arkona.xfm_pypclockin c on a.employee_number = c.employee_number
    and c.the_date between a.from_date and a.thru_date
  group by a.employee_number, a.employee_name, a.from_Date, a.thru_date) bb on aa.employee_number = bb.employee_number
where aa.pto_pto_hours <> bb.arkona_pto_hours

turns out it is no problem at all, all the diffs are due to future clocking in arkona

-- taylor  clocked in advance, 
select * from arkona.xfm_pypclockin where employee_number = '118030' and the_date >= '06/30/2020' and pto_hours + vac_hours <> 0
-- rod  clocked in advance
select * from arkona.xfm_pypclockin where employee_number = '1140870' and the_date >= '08/03/2020' and pto_hours + vac_hours <> 0
-- terry  clocked in advance
select * from arkona.xfm_pypclockin where employee_number = '162428' and the_date >= '04/21/2020' and pto_hours + vac_hours <> 0
-- bev  clocked in advance
select * from arkona.xfm_pypclockin where employee_number = '187675' and the_date >= '05/21/2020' and pto_hours + vac_hours <> 0