﻿-- -- these are the stored procs called in data.phph on old vision
-- - execute procedure pto_get_new_employees()
-- - execute procedure pto_search_for_employee( ? )
-- - execute procedure pto_get_departments()
-- - execute procedure pto_get_positions_for_department( ? )
-- - execute procedure pto_get_administrator_for_position( ?, ? )
-- - execute procedure pto_get_employee_pto( ? )
-- - execute procedure pto_get_manager_employees( ? )
-- - execute procedure pto_get_new_employee( ? )
-- - execute procedure pto_get_employee_policy( ? )  -- does not seem to exist
-- - execute procedure pto_get_employee_pto_used( ? )
-- - execute procedure pto_insert_new_employee( ?, ?, ?, ?, ? )
-- execute procedure pto_update_employee_pto( ?, ?, ?, ?, ? )
-- 
-- -- tables required by these stored procs
-- -- other tables
--     dds.edwEmployeeDim
--     dds.stgArkonaPymast 
--     dds.tally
-- -- pto employees
--     ptoEmployees  
--     pto_employee_pto_allocation
--     pto_adjustments 
--     pto_used_sources  
--     pto_used
--     pto_requests
-- -- positions/authorization
--     ptoDepartments
--     ptoPositions
--     ptoDepartmentPositions
--     ptoPositionFulfillment
--     ptoAuthorization    
-- -- no RI
--     pto_exclude    
--     pto_compli_users
--     pto_categories   
--     pto_tmp_compli_timeoff 
--     pto_tmp_compli_users
-- -- may not be needed
--     employeeAppAuthorization
--     applicationMetadata
-- -- ?    
--     pto_tmp_manager_employees
-- 
-- create schema pto;
-- comment on schema pto is 'schema for the migration of pto data from Advantage';    

drop table if exists pto.employees cascade;
create TABLE pto.employees(
    employee_number citext primary key,
    pto_anniversary date,
    user_name citext NOT NULL,
    anniversary_type citext NOT NULL,
    active boolean not null DEFAULT true,
    unique(user_name));
comment on table pto.employees is 'basic employee pto information , in advantage was ptoEmployees';

drop table if exists pto.employee_pto_allocation cascade;
create TABLE pto.employee_pto_allocation(
    employee_number citext NOT NULL references pto.employees(employee_number),
    from_date date NOT NULL,
    thru_date date NOT NULL,
    hours numeric (12,4),
    PRIMARY KEY (employee_number,from_date));
comment on table pto.employee_pto_allocation is 'pto allocated by employee/year, in advantage was pto_employee_pto_allocation';

drop table if exists pto.adjustments cascade;
create TABLE pto.adjustments(
    employee_number citext NOT NULL,
    from_date date NOT NULL,
    hours numeric (12,4) NOT NULL,
    reason citext NOT NULL,
    notes citext,
    foreign KEY (employee_number,from_date) references pto.employee_pto_allocation(employee_number,from_date));
comment on table pto.adjustments is 'adjustments made to an employees pto allocation for a particular  year, in advantage was pto_adjustments';

-- drop table if exists pto.used_pto_sources cascade;
-- create TABLE pto.used_pto_sources(
--     source citext PRIMARY KEY);
-- comment on table pto.used_pto_sources is 'sources of used pto, eg time clock, donate, etc, in advantage was pto_used_sources';

drop table if exists pto.used_pto cascade;
create TABLE pto.used_pto(
    employee_number citext NOT NULL references pto.employees(employee_number),
    the_date date NOT NULL,
    source citext NOT NULL references pto.used_pto_sources(source),
    hours numeric (9,2),
    PRIMARY KEY (employee_number,the_date,source));
comment on table pto.used_pto is 'record of used pto by employee/date, in advantage was pto_used';

drop table if exists pto.requests cascade;
create TABLE pto.requests(
    employee_number citext NOT NULL references pto.employees(employee_number),
    the_date date NOT NULL,
    hours numeric (6,2) NOT NULL,
    request_status citext NOT NULL,
    request_type citext,
    PRIMARY KEY (employee_number,the_date,request_type));
comment on table pto.requests is 'pto request scraped from compli, in advantage was pto_requests';

drop table if exists pto.departments cascade;
create TABLE pto.departments(
    department citext PRIMARY KEY);
comment on table pto.departments is 'departments, part of position definitions, in advantage was ptoDepartments';

drop table if exists pto.positions cascade;
create TABLE pto.positions(
    the_position citext PRIMARY KEY); 
comment on table pto.positions is 'positions, part of position definitions, in advantage was ptoPositions';

drop table if exists pto.department_positions cascade;
create TABLE pto.department_positions(
    department citext NOT NULL references pto.departments(department),
    the_position citext NOT NULL references pto.positions(the_position),
    the_level integer,
    pto_policy_html citext,
    PRIMARY KEY (department,the_position));
comment on table pto.department_positions is 'the full position comprised of a department and a position, in advantage was ptoDepartmentPositions';

drop table if exists pto.position_fulfillment cascade;
create TABLE pto.position_fulfillment(
    department citext NOT NULL,
    the_position citext NOT NULL,
    employee_number citext,
    foreign key(department,the_position) references pto.department_positions(department,the_position),
    PRIMARY KEY (department,the_position,employee_number)); 
comment on table pto.compli_users is 'an employee can ever only be in one position at a time, in advantage was ptoPositionFulfillment';

drop table if exists pto.authorization cascade;
create TABLE pto.authorization(
    auth_by_department citext NOT NULL,
    auth_by_position citext NOT NULL,
    auth_for_department citext NOT NULL,
    auth_for_position citext NOT NULL,
    auth_by_level integer,
    auth_for_level integer,
    foreign key(auth_by_department,auth_by_position) references pto.department_positions(department,the_position),
    foreign key(auth_for_department,auth_for_position) references pto.department_positions(department,the_position),
    PRIMARY KEY (auth_by_department,auth_by_position,auth_for_department,auth_for_position));
comment on table pto.authorization is 'authorization for pto approval by departmenet & position, levels are deprecated, in advantage was ptoAuthorization';

drop table if exists pto.exclude_employees cascade;
create TABLE pto.exclude_employees(
    employee_number citext primary key);
comment on table pto.exclude_employees is 'employees excluded from pto, in advantage was pto_exclude';          

-- -- this table is populated in real time from luigi module compli.py, don't need data from advantage, don't need to
-- -- reinitialize the table
-- drop table if exists pto.compli_users cascade;
-- create TABLE pto.compli_users(
--     user_id citext primary key,
--     first_name citext,
--     last_name citext,
--     email citext,
--     supervisor_id citext,
--     supervisor_name citext,
--     location_id citext,
--     location_name citext,
--     date_of_hire date,
--     title citext,
--     status citext,
--     policy_assigned citext,
--     policy_signed citext,
--     training_assigned citext,
--     training_completed citext,
--     forms_assigned citext,
--     forms_completed citext,
--     forms_in_notification citext,
--     exclusionary citext);
-- comment on table pto.compli_users is 'raw user information scraped from compli, in advantage was pto_compli_users';

-- drop table if exists pto.categories cascade;
-- create TABLE pto.categories(
--     pto_category integer NOT NULL,
--     pto_hours integer NOT NULL,
--     from_years_tenure integer NOT NULL,
--     thru_years_tenure integer,
--     PRIMARY KEY (pto_category));
-- comment on table pto.categories is 'categorize amount of pto earned by tenure, in advantage was pto_categories';

-- -- this table is populated in real time from luigi module compli.py, don't need data from advantage, don't need to
-- -- reinitialize the table
-- drop table if exists pto.tmp_compli_timeoff cascade;
-- create TABLE pto.tmp_compli_timeoff(
--     last_name citext,
--     first_name citext,
--     user_id citext,
--     hire_date citext,
--     request_status citext,
--     scheduled_date citext,
--     hours citext,
--     allocation_type citext,
--     currently_with citext,
--     started_on citext,
--     last_sent citext,
--     filed_on citext,
--     from_title citext,
--     the_view citext,
--     field_1 citext,
--     field_2 citext);
-- comment on table pto.tmp_compli_timeoff is 'staging table for raw scrape of compli pto request data, in advantage was pto_tmp_compli_timeoff';

-- drop table if exists pto.tmp_compli_users cascade;
-- create TABLE pto.tmp_compli_users(
--     user_id citext,
--     first_name citext,
--     last_name citext,
--     email citext,
--     supervisor_id citext,
--     supervisor_name citext,
--     location_id citext,
--     location_name citext,
--     date_of_hire citext,
--     title citext,
--     status citext,
--     policy_assigned citext,
--     policy_signed citext,
--     training_assigned citext,
--     training_completed citext,
--     forms_assigned citext,
--     forms_completed citext,
--     forms_in_notification citext,
--     exclusionary citext,
--     drivers_license_state citext,
--     drivers_license_id citext);
-- comment on table pto.tmp_compli_timeoff is 'staging table for raw scrpae of compli user data, in advantage was pto_tmp_compli_users';


-- drop table if exists dds.tally cascade;
-- create TABLE dds.tally (
--   n integer primary key);
-- 
-- insert into dds.tally
-- select *
-- from generate_series (0,100000);

