﻿-- -- 1st step is to verify with ben & laura the position entitled to a payout
-- 
-- from th 11/22/2019 employee handbook Section 5c.
The Company understands that certain commissioned employees chose to forego taking earned and awarded 
PTO because they prefer to work and earn commission.  Therefore, employees who work as service advisors, 
sales consultants, body shop estimators, main shop technicians, detail technicians, body shop technicians, 
and aftermarket sales consultants are eligible to have any unused, earned PTO at the end of their 
anniversary year of allocation paid out to them in accordance with the Company’s practices. 
ie: service advisors, sales consultants, body shop estimators, main shop technicians, detail technicians, body shop technicians, and aftermarket sales consultants


drop table if exists pto.payout_employees cascade;
create table pto.payout_employees (
  employee_number citext primary key,
  source citext not null);
insert into pto.payout_employees  
select * 
from (
  select a.employee_number, 'bs est' as source from bspp.personnel a 
  join bspp.personnel_roles b on a.user_name = b.user_name and b.thru_date > current_date 
  join pto.employees c on a.employee_number = c.employee_number and c.active
  union
  select a.employee_number, 'ry1 adv' from sap.personnel a 
  join pto.employees c on a.employee_number = c.employee_number and c.active 
  where a.thru_date > current_date
  union
  select a.employee_number, 'sc' from sls.personnel a
  join pto.employees c on a.employee_number = c.employee_number and c.active 
  where a.end_Date > current_Date and a.employee_number <> 'HSE'
  union
  select a.employee_number, 'ry2 tech' from hs.main_shop_flat_rate_techs a
  join pto.employees c on a.employee_number = c.employee_number and c.active 
  where a.thru_date > current_date
  union
  select a.pymast_employee_number, 'ry2 adv'
  from arkona.ext_pymast a
  join pto.employees c on a.pymast_employee_number = c.employee_number and c.active 
  where a.employee_last_name = 'pederson' and a.employee_first_name = 'david'
  union
  select a.pymast_employee_number, 'ry1 tech' -- ry1 main shop techs 
  from arkona.ext_pymast a
  join pto.employees c on a.pymast_employee_number = c.employee_number and c.active 
  where a.distrib_code = 'stec'
    and a.payroll_class = 'c'
    and a.active_code = 'a'
  union
  select a.pymast_employee_number, 'bs tech' -- body shop techs
  from arkona.ext_pymast a
  join pto.employees c on a.pymast_employee_number = c.employee_number and c.active 
  where a.distrib_code = 'btec'
    and a.payroll_class = 'c'
    and a.active_code = 'a'
  union  
  select a.pymast_employee_number, 'detail' -- detail techs
  from arkona.ext_pymast a
  join pto.employees c on a.pymast_employee_number = c.employee_number and c.active 
  where a.distrib_code = 'wtec'
    and a.payroll_class = 'c'
    and a.active_code = 'a') a
where a.employee_number not in ('17534','195460','1126300','1132700','212589');  -- exclude aubol, michael, shirek, stout, bedney


-- looks ok i believe
select a.*, b.employee_name, c.department, c.the_position
from pto.payout_employees a
left join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
left join pto.position_fulfillment c on a.employee_number = c.employee_number
order by source, employee_name


-- skipping the authorizer's authorizer
drop table if exists pto.payout_elibible;
CREATE TABLE pto.payout_elibible ( 
  employee_number citext primary key,
  employee_name citext,
  employee_email citext,
  anniversary_date Date,
  current_period_from Date,
  current_period_thru Date,
  pto_hours numeric,
  pto_used numeric,
  adjustments numeric,
  pto_unused numeric,
  authorizer citext,
  authorizer_email citext);

-- put this into luigi and refresh daily
-- leave this as all active employees
-- from FUNCTION pto.get_employee_current_period(citext)
drop table if exists pto.current_pto_periods cascade;
create table pto.current_pto_periods (
  employee_number citext primary key references pto.employees(employee_number),
  from_date date not null,
  thru_date date not null);
insert into pto.current_pto_periods  
select a.employee_number,
case
  when thru_date = '12/31/9999' then
    (
      select the_date
      from dds.dim_date
      where month_of_year = extract(month from from_date)
        and day_of_month = extract(day from from_date)
        and the_date <= current_date
      order by the_date desc
      limit 1)
  else a.from_date
end as from_date,
case 
  when thru_date = '12/31/9999' then
    (
      select (the_date + interval '1 year')::date - 1
      from dds.dim_date
      where month_of_year = extract(month from from_date)
        and day_of_month = extract(day from from_date)
        and the_date <= current_date
      order by the_date desc
      limit 1)    
  else a.thru_date
end as thru_date
from pto.employee_pto_allocation a
join pto.employees b on a.employee_number = b.employee_number
  and b.active
left join pto.exclude_employees c on b.employee_number = c.employee_number
WHERE current_date between from_date and thru_date
  and c.employee_number is null;
create index on pto.current_pto_periods(from_date);
create index on pto.current_pto_periods(thru_date);


-- this runs on monday morning  
-- so, i need employees where the current period expires between monday and sunday
select a.employee_number, b.employee_name, d.user_name, d.pto_anniversary, c.from_date, c.thru_date,
  coalesce(e.hours, 0) as pto, coalesce(f.used, 0) as used, coalesce(g.adjustments, 0) as adjustments,
  coalesce(e.hours, 0) - coalesce(f.used, 0) - coalesce(g.adjustments, 0) as remaining,
  k.employee_name as authorizer, l.user_name as auth_email
from pto.payout_employees a 
left join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
left join pto.current_pto_periods c on a.employee_number = c.employee_number
join pto.employees d on a.employee_number = d.employee_number    
join pto.employee_pto_allocation e on  a.employee_number = e.employee_number 
  and c.thru_date between e.from_date and e.thru_date
left join lateral(
  select employee_number, sum(hours) as used
  from pto.used_pto
  where the_date between c.from_date and c.thru_date
  group by employee_number) f on a.employee_number = f.employee_number
left join lateral(
  select employee_number, sum(hours) as adjustments
  from pto.adjustments
  where from_date between c.from_date and c.thru_date
  group by employee_number) g on a.employee_number = g.employee_number  
join  pto.position_fulfillment h on a.employee_number = h.employee_number  
join pto.authorization i on h.department = i.auth_for_department and h.the_position = i.auth_for_position
join pto.position_fulfillment j on i.auth_by_department = j.department and i.auth_by_position = j.the_position
join arkona.ext_pymast k on j.employee_number = k.pymast_employee_number
join pto.employees l on j.employee_number = l.employee_number
where c.thru_date between '12/28/2020' and '12/31/2020':: date + 6
  and coalesce(e.hours, 0) - coalesce(f.used, 0) - coalesce(g.adjustments, 0) <> 0

-- list for ben and laura
select d.employee_name as employee, b.department, a.employee_number as "emp#", -- b.the_position, c.title, f.department, f.the_position, 
  g.employee_name as pto_authorizer
from payout_employees a
join pto.position_fulfillment b on a.employee_number = b.employee_number
join pto.compli_users c on a.employee_number = c.user_id
join arkona.ext_pymast d on a.employee_number = d.pymast_employee_number
join pto.authorization e on b.department = e.auth_for_department and b.the_position = e.auth_for_position
join pto.position_fulfillment f on e.auth_by_department = f.department and e.auth_by_position = f.the_position
join arkona.ext_pymast g on f.employee_number = g.pymast_employee_number
where a.employee_number not in ('17534','195460','1126300','1132700','212589') -- exclude aubol, michael, shirek, stout, bedney
order by b.department, d.employee_name



