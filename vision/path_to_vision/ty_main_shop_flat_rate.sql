﻿-- vision
vision -> payroll -> ukg import -> toyota main shop flat rate ukg
https://beta.rydellvision.com:8888/vision/ty-main-shop-flat-rate-payrolls
const query = 'select * from  ukg.get_ty_main_shop_flat_rate_payroll($1) as result';
from ts.get_ty_flat_rate_managers_payroll_approval_data(_pay_period_ind) 
from ts.ty_main_shop_flat_rate_payroll_data
Function: ts.update_ty_main_shop_flat_rate_payroll_data()


-- tech page


-- ok these 2 match
select * from ts.ty_main_shop_flat_rate_payroll_data

select employee_number, sum(flag_hours) 
from ts.pay_period_ros
where from_date = '10/23/2022'
group by employee_number

select * from ts.main_shop_flat_rate_techs

select * from arkona.ext_sdpxtim where company_number = 'RY8'

-- ok what i am struggling with
currently vision ukg imports is populated by ts.update_ty_main_shop_flat_rate_payroll_data(), which like honda
uses a series of CTEs to generate clock hours, flag hours and adjustments (just like honda)
i dont like ctes from a troubleshooting perspective
i also have separte functions that update separate tables:ts.pay_period_adjustments, ts.pay_period_clock_hours & ts.pay_period_ros
which could be updated nightly and substituted in function ts.update_ty_main_shop_flat_rate_payroll_data() in place of the ctes

i checked, for current payt period the flag hours generated are the same, as i would hope

the advantage is no ctes, have the flag hours and ros generated from the same table

if i do it, i should update honda to do the same

-- 11/02 going with the separate tables
first step is to get the individual tables updating daily

select * from ts.pay_period_ros order by pay_period_seq, employee_number, the_date

select ro
from ts.pay_period_ros
group by ro
having count(*) > 1

select *
from ts.pay_period_ros
where ro = '86000725'

  select employee_number, the_date,
		sum(flag_hours) over (partition by employee_number, pay_period_seq order by the_date) as running_flag_hours
  from ts.pay_period_ros

 -- 11/07/22 struggling with ros, think i need to tables ?? one for ros one for flag hour
 -- at least that's where i left off, now this has been assigned a high priority by greg
 -- and as i think about that, it doesn't make any sense
 -- as i get started on this now, keep in mind it is all about the pay period
  -- oh yeah, now i remember, it's about trying to include the running flag hours in the same table
  -- for now, i say bad idea, separate queries for pay page and ro page

  select * from ts.pay_period_ros

  select * from ts.pay_period_clock_hours
  same thing with clock hours & adjustments, no need for running totals on a daily basis
  alter table ts.pay_period_adjustments
  drop column pay_period_adjustments;

  alter table ts.pay_period_clock_hours
  drop column pay_period_clock_hours;  

  alter table ts.pay_period_ros  
  drop column pay_period_flag_hours;


 select * from ts.get_flat_rate_tech_guarantee() 

 --------------------------------------------------------------------
 -- this is a version of FUNCTION ts.update_ty_main_shop_flat_rate_payroll_data() using tables instead of cte
 -- this will work for the vision tech commission page, have to talk to afton about how she wants this data
 --------------------------------------------------------------------
do $$
declare 
	_pay_period_seq integer :=  (
	  select distinct biweekly_pay_period_sequence
	  from dds.dim_date
	  where the_date = current_date - 1);
	_thru_date date := (
	  select distinct biweekly_pay_period_end_date
	  from dds.dim_date 
	  where biweekly_pay_period_sequence = _pay_period_seq);	
        
begin 		
	drop table if exists the_tables;
	create temp table the_tables as
		select 
			_pay_period_seq as pay_period_seq,
			aa.last_name, aa.first_name, a.employee_number, f.user_key,
			c.flag_hours, coalesce(d.flag_hours, 0) as adjustments,
			c.flag_hours + coalesce(d.flag_hours,0) as total_flag_hours,
			b.clock_hours,
			case
				when b.clock_hours = 0 then 0
				else coalesce(c.flag_hours + coalesce(d.flag_hours,0), 0) / b.clock_hours 
			end as prof,
			a.flat_rate
		from ts.main_shop_flat_rate_techs a
		join ukg.employees aa on a.employee_number = aa.employee_number
		left join (
		  select employee_number, sum(clock_hours) as clock_hours
		  from ts.pay_period_clock_hours 
		  where pay_period_seq = _pay_period_seq
		  group by employee_number) b on a.employee_number = b.employee_number
		left join (
		  select employee_number, sum(flag_hours) as flag_hours
		  from ts.pay_period_ros
		  where pay_period_seq = _pay_period_seq
		  group by employee_number) c on a.employee_number = c.employee_number
		left join (
		  select employee_number, sum(adjustments) as flag_hours
		  from ts.pay_period_adjustments
		  where pay_period_seq = _pay_period_seq
		  group by employee_number) d on a.employee_number = d.employee_number
		left join ts.get_flat_rate_tech_guarantee() e on a.employee_number = e.employee_number
		left join nrv.users f on a.employee_number = f.employee_number and f.is_active
		where a.thru_date > _thru_date; --) aa;
end $$;

select * from the_tables;	

  
/*
zimmer, termed in compli, tech  number deactivated, still current in ukg
select * from ukg.employees where last_name = 'zimmer'

select * from dds.dim_tech where tech_name like 'zimmer%'

select ts.update_pay_period_adjustments();
select ts.update_pay_period_clock_hours();
select ts.update_pay_period_ros();



*/

 --------------------------------------------------------------------
 -- this will work for the vision tech ros page ros, have to talk to afton about how she wants this data
 --------------------------------------------------------------------
do $$
declare 
	_pay_period_seq integer :=  (
	  select distinct biweekly_pay_period_sequence
	  from dds.dim_date
	  where the_date = current_date - 1);
	_thru_date date := (
	  select distinct biweekly_pay_period_end_date
	  from dds.dim_date 
	  where biweekly_pay_period_sequence = _pay_period_seq);	
        
begin 		
	drop table if exists the_ros;
	create temp table the_ros as
	
	select _pay_period_seq, c.the_date, b.first_name, b.last_name, b.employee_number,
	  c.ro, sum(coalesce(c.flag_hours, 0)) as flag_hours
	from ts.main_shop_flat_rate_techs a
	join ukg.employees b on a.employee_number = b.employee_number	
	left join ts.pay_period_ros c on b.employee_number = c.employee_number
	  and c.pay_period_seq = _pay_period_seq
	where a.thru_date > _thru_date
	group by c.the_date, b.first_name, b.last_name, b.employee_number, c.ro
	union
	select _pay_period_seq, null::date, null::citext, null::citext, b.employee_number,
	  'TOTAL', sum(coalesce(c.flag_hours, 0)) as total_flag_hours
	from ts.main_shop_flat_rate_techs a
	join ukg.employees b on a.employee_number = b.employee_number	
	left join ts.pay_period_ros c on b.employee_number = c.employee_number
	  and c.pay_period_seq = _pay_period_seq
	where a.thru_date > _thru_date
	group by b.employee_number
	order by employee_number, the_date, ro;	
end $$;

select * from the_ros;	

 --------------------------------------------------------------------
 -- this will work for the vision tech ros page adjustments, have to talk to afton about how she wants this data
 --------------------------------------------------------------------

do $$
declare 
	_pay_period_seq integer :=  (
	  select distinct biweekly_pay_period_sequence
	  from dds.dim_date
	  where the_date = current_date - 1);
	_thru_date date := (
	  select distinct biweekly_pay_period_end_date
	  from dds.dim_date 
	  where biweekly_pay_period_sequence = _pay_period_seq);	
        
begin 		
	drop table if exists the_adjustments;
	create temp table the_adjustments as
	
	select _pay_period_seq, b.employee_number, c.ro, sum(coalesce(c.adjustments, 0)) as adjustments
	from ts.main_shop_flat_rate_techs a
	join ukg.employees b on a.employee_number = b.employee_number	
	left join ts.pay_period_adjustments c on b.employee_number = c.employee_number
	  and c.pay_period_seq = _pay_period_seq
	where a.thru_date > _thru_date
	group by b.employee_number, c.ro
	union
	select _pay_period_seq, b.employee_number, 'TOTAL', sum(coalesce(c.adjustments, 0)) as total_adjustments
	from ts.main_shop_flat_rate_techs a
	join ukg.employees b on a.employee_number = b.employee_number	
	left join ts.pay_period_adjustments c on b.employee_number = c.employee_number
	  and c.pay_period_seq = _pay_period_seq
	where a.thru_date > _thru_date
	group by b.employee_number
	order by employee_number, ro;	
end $$;

select * from the_adjustments;	


-----------------------------------------------------------------------------------------------
-- base this on FUNCTION hs.get_honda_main_shop_flat_rate_tech(text, integer) which is used to generate
-- both the commission and ros page for honda techs
-- replace ctes with base tables
-----------------------------------------------------------------------------------------------
create or replace function ts.get_toyota_main_shop_flat_rate_tech(_id text, _pay_period_indicator integer)
returns setof json as
$BODY$
/*
a copy of hs.get_honda_main_shop_flat_rate_tech(text, integer) modified for toyota,
no bonus, commission only

select * from ts.get_toyota_main_shop_flat_rate_tech((select user_key::text from nrv.users where last_name = 'moon' and first_name = 'eric'), 0);

*/
declare
  _from_date date;
  _thru_date date;
  _current_pay_period_seq integer;
  _employee_number citext;
begin
  _current_pay_period_seq = (
    select biweekly_pay_period_sequence
    from dds.dim_date
    where the_Date = current_date);  
  _from_date = (
    select distinct biweekly_pay_period_start_date
    from dds.dim_date
    where biweekly_pay_period_sequence = _current_pay_period_seq + _pay_period_indicator);
  _thru_date = (
    select distinct biweekly_pay_period_end_date
    from dds.dim_date
    where biweekly_pay_period_sequence = _current_pay_period_seq + _pay_period_indicator);    
  _employee_number = (
    select employee_number
    from nrv.users
    where user_key::text = _id);     
return query 
with 
  clock_hours as (
    select a.the_date, d.employee_number, sum(clock_hours) as clock_hours
    from dds.dim_date a
    left join ukg.clock_hours b on a.the_date = b.the_date
    inner join ts.main_shop_flat_rate_techs d on b.employee_number = d.employee_number 
      and d.thru_date > _thru_date
    where a.the_date between _from_date and _thru_date
    group by a.the_date, d.employee_number
    order by a.the_date),  
  ros as (
    select a.the_date, d.employee_number, b.ro, sum(b.flag_hours) as flag_hours
    from dds.dim_date a
    left join dds.fact_repair_order b on a.the_date = b.flag_date
    inner join dds.dim_tech c on b.tech_key = c.tech_key
    inner join ts.main_shop_flat_rate_techs d on c.employee_number = d.employee_number 
      and d.thru_date > _thru_date
    where a.the_date between _from_date and _thru_date
      and d.employee_number = _employee_number
    group by a.the_date, d.employee_number, b.ro),
  production as (
    select a.the_date as date, coalesce(b.clock_hours, 0) as clock_hours, coalesce(c.flag_hours, 0) as flag_hours
    from dds.dim_date a
    left join clock_hours b on a.the_date = b.the_date
      and b.employee_number = _employee_number
    left join (
      select the_date, employee_number, sum(flag_hours) as flag_hours
      from ros
      where employee_number = _employee_number
      group by the_date, employee_number) c on a.the_date = c.the_date
    where a.the_date between _from_date and _thru_date),
  adjustments as (
    select a.tech_number, arkona.db2_integer_to_date_long(b.trans_date) as date, 
      b.ro_number as ro, 'Adjust after Close' as type, sum(labor_hours) as flag_hours
    from ts.main_shop_flat_rate_techs a
    inner join arkona.ext_sdpxtim b on a.tech_number = b.technician_id
      and b.company_number = 'RY2'
    where arkona.db2_integer_to_date_long(b.trans_date) between _from_date and _thru_date
      and a.employee_number = _employee_number
      and a.thru_date > _thru_date
    group by a.tech_number, arkona.db2_integer_to_date_long(b.trans_date), b.ro_number
    having sum(labor_hours) <> 0)
select row_to_json(z)
from (
  select row_to_json(y) as toyota_main_shop_flat_rate_tech
  from (
    select bb.*,_id as employee --total_commission_pay 
    from ( -- bb: added a level to generate total pay
      select aa.*, 
        case 
          when aa.total_clock_hours = 0 then 0
          else round(aa.total_flag_hours/aa.total_clock_hours, 2) 
        end as proficiency,
        aa.flat_rate * aa.total_flag_hours as total_commission_pay,
        ( -- array of json objects, one row per day, clock hours and flag hours
          select coalesce(array_to_json(array_agg(row_to_json(bb))), '[]')
          from (
            select *
            from production) bb) as production,
        ( -- array of json objects, one row per ro
          select coalesce(array_to_json(array_agg(row_to_json(dd))), '[]')
          from (
            select the_date as date, ro, flag_hours
            from ros) dd) as ros,
        ( -- array of json objects, one row per ro
          select coalesce(array_to_json(array_agg(row_to_json(ee))), '[]')
          from (
            select date, ro, type, flag_hours
            from adjustments) ee) as adjustments  
      from ( -- aa top level tech data
        select _id as id, _pay_period_indicator as pay_period_indicator, a.flat_rate, --a.pto_rate, 
--           2 * a.flat_rate as bonus_rate,
          (select sum(clock_hours) from clock_hours where employee_number = _employee_number) as total_clock_hours,
          coalesce((select sum(flag_hours) from ros where employee_number = _employee_number),0) 
          + 
          coalesce((select sum(flag_hours) from adjustments), 0) as total_flag_hours,
--           coalesce((select sum(pto_hours) from pto_hours where employee_number = _employee_number), 0) as total_pto_hours,
          coalesce((select sum(flag_hours) from adjustments), 0) as total_adjustments
        from ts.main_shop_flat_rate_techs a
        inner join nrv.users b on a.employee_number = b.employee_number
        where b.user_key::text = _id
          and a.thru_date > _from_date) aa) bb) y) z;
end;    
$BODY$
  LANGUAGE plpgsql;
          