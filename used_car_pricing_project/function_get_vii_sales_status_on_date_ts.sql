﻿-- DROP FUNCTION ads.get_vii_sales_status_on_date(citext, timestamp);

CREATE OR REPLACE FUNCTION ads.get_vii_sales_status_on_date(_viiid citext, _the_date timestamp)
  RETURNS text AS
$BODY$
/*
select * from ads.get_vii_sales_status_on_date('130ec1f5-8c49-4748-a414-4f03657ec3ff', '12/12/2022 20:00:00') as status  -- sold & not del
select * from ads.get_vii_sales_status_on_date('7cd63d96-1746-4e3a-9c17-43bad04dac23', '11/16/2022 20:00:00') as status  -- at auction
select * from ads.get_vii_sales_status_on_date('414b40ac-f360-45ca-ba0c-c8d1e4de5ca4', '12/16/2022 20:00:00') as status  -- sales buffer
select * from ads.get_vii_sales_status_on_date('02e9c107-10ef-4277-b86a-c98fa22386b9', '12/14/2022 20:00:00') as status  -- raw material
select * from ads.get_vii_sales_status_on_date('b1170286-02bf-4547-b782-b20534fe1937', '12/16/2022 20:00:00') as status -- was in trade buffer


select * from ads.ext_vehicle_sales where vehicleinventoryitemid = '92755d26-4894-490d-88ea-4c583fa0d392'

select * from ads.ext_vehicle_inventory_items limit 20

-- convert to no timezone
select fromts, fromts::timestamp as no_tz from ads.ext_Vehicle_inventory_item_statuses order by fromts desc limit 20

12/17/22
	error: more than one row returned by a subquery used as an expression
	i think that is because of multiple statuses on a date, change function to determine status at 8PM on the date

	once i get the tool sales status, transate those into replinishment statuses
	acq  acq on grd not on lot  on lot
*/
    SELECT 
        CASE
          WHEN veh.Wholesaled = 1 THEN 'Wholesaled'
          WHEN veh.Delivered = 1 THEN 'Delivered'
          WHEN veh.Sold = 1 THEN 'Sold and Not Delivered'
          WHEN veh.AtAuction = 1 THEN 'At Auction'
          WHEN veh.SalesBuffer = 1 THEN 'Sales Buffer'
          WHEN veh.WholesaleBuffer = 1 THEN 'WS Buffer'
          WHEN veh.InTransit = 1 THEN 'In Transit'
          WHEN veh.TNA = 1 THEN 'Trade Buffer'  
          WHEN veh.InspPending = 1 THEN 'Inspection Pending'
          WHEN veh.WalkPending = 1 THEN 'Walk Pending'
          WHEN veh.RawMaterials = 1 THEN 'Raw Materials'          
          WHEN veh.Available = 1 THEN 'Available'
        	WHEN veh.PricingBuffer = 1 THEN 'Pricing Buffer'
          WHEN veh.Pulled = 1 THEN 'Pulled'
          WHEN veh.BookingPending = 1 THEN 'Booking Pending'
          ELSE ''
        END AS Status
      FROM (   
        SELECT --a.stocknumber,
          ( -- Delivered
						SELECT 1
            FROM ads.ext_Vehicle_Inventory_Items
            WHERE VehicleInventoryItemID = _viiid
              AND ThruTS <= _the_date) AS Delivered, 
          ( -- Sold
            SELECT 1
              FROM  ads.ext_Vehicle_Sales vs 
							WHERE vs.VehicleInventoryItemID = _viiid
								AND vs.Status = 'VehicleSale_Sold'
								AND vs.Typ = 'VehicleSale_Retail'
								and vs.soldts <= _the_date) AS Sold,
          ( -- Wholesaled
						SELECT 1
            FROM ads.ext_Vehicle_Sales vs
            WHERE vs.VehicleInventoryItemID = _viiid
               AND vs.Status = 'VehicleSale_Sold'
               AND vs.Typ = 'VehicleSale_Wholesale'
               and vs.soldts <= _the_date) AS WholeSaled,
          ( -- AtAuction  
            SELECT 1
            FROM ads.ext_vehicle_Inventory_Item_Statuses
            WHERE VehicleInventoryItemID = _viiid
							AND status = 'RMFlagAtAuction_AtAuction'
							and fromts <= _the_date
							AND ThruTS > _the_date) AS AtAuction,    
          ( -- SalesBuffer
            SELECT 1
            FROM ads.ext_vehicle_Inventory_Item_Statuses
            WHERE VehicleInventoryItemID = _viiid
							AND status = 'RMFlagSB_SalesBuffer'
							and fromts <= _the_date
							AND ThruTS > _the_date) AS SalesBuffer,  
          ( -- WholesaleBuffer
            SELECT 1
            FROM ads.ext_vehicle_Inventory_Item_Statuses
            WHERE VehicleInventoryItemID = _viiid
              AND status = 'RMFlagWSB_WholesaleBuffer'
							and fromts <= _the_date
							AND ThruTS > _the_date) AS WholesaleBuffer,       
          ( -- IN Transit
            SELECT 1
            FROM ads.ext_vehicle_Inventory_Item_Statuses
            WHERE VehicleInventoryItemID = _viiid
							AND status = 'RMFlagPIT_PurchaseInTransit'
							and fromts <= _the_date
							AND ThruTS > _the_date) AS InTransit,   
          ( -- TNA
            SELECT 1
            FROM ads.ext_vehicle_Inventory_Item_Statuses
            WHERE VehicleInventoryItemID = _viiid
              AND status = 'RMFlagTNA_TradeNotAvailable'
							and fromts <= _the_date
							AND ThruTS > _the_date) AS TNA, 
          ( -- Insp Pending
            SELECT 1
            FROM ads.ext_vehicle_Inventory_Item_Statuses
            WHERE VehicleInventoryItemID = _viiid
              AND status = 'RMFlagIP_InspectionPending'
							and fromts <= _the_date
							AND ThruTS > _the_date) AS InspPending, 
          ( -- Walk Pending
            SELECT 1
            FROM ads.ext_vehicle_Inventory_Item_Statuses
            WHERE VehicleInventoryItemID = _viiid
              AND status = 'RMFlagWP_WalkPending'
							and fromts <= _the_date
							AND ThruTS > _the_date) AS WalkPending,                         
          ( -- Raw Materials
            SELECT 1
            FROM ads.ext_vehicle_Inventory_Item_Statuses
            WHERE VehicleInventoryItemID = _viiid
              AND status = 'RMFlagRMP_RawMaterialsPool'
							and fromts <= _the_date
							AND ThruTS > _the_date) AS RawMaterials,
          ( -- Available
            SELECT 1
            FROM ads.ext_vehicle_Inventory_Item_Statuses
            WHERE VehicleInventoryItemID = _viiid
              AND status = 'RMFlagAV_Available'
							and fromts <= _the_date
							AND ThruTS > _the_date) AS Available,
          ( -- Pricing Buffer
            SELECT 1
            FROM ads.ext_vehicle_Inventory_Item_Statuses
            WHERE VehicleInventoryItemID = _viiid
              AND status = 'RMFlagPB_PricingBuffer'
							and fromts <= _the_date
							AND ThruTS > _the_date) AS PricingBuffer,	  
          ( -- Pulled
            SELECT 1
            FROM ads.ext_vehicle_Inventory_Item_Statuses
            WHERE VehicleInventoryItemID = _viiid
              AND status = 'RMFlagPulled_Pulled'
							and fromts <= _the_date
							AND ThruTS > _the_date) AS Pulled, 
          ( -- Booking Pending
            SELECT 1
            FROM ads.ext_vehicle_Inventory_Item_Statuses
            WHERE VehicleInventoryItemID = _viiid
              AND status = 'RawMaterials_BookingPending'
							and fromts <= _the_date
							AND ThruTS > _the_date) AS BookingPending) veh;                  
      
        
$BODY$
  LANGUAGE sql VOLATILE
  COST 100;
ALTER FUNCTION ads.get_vii_sales_status(citext)
  OWNER TO rydell;
