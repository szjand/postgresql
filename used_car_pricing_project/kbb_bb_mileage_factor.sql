﻿-- bb
-- select * from pp.kelley_black_book a
-- delete from pp.kelley_black_book where vin = '3GCUYGEDXKG149416'  crew ltz 5.3

select distinct a.vin, (c->>'range_begin')::integer as range_begin, c->>'range_end' as range_end, 
	c->>'avg' as avg, c->>'clean' as clean, c->>'rough' as rough, c->>'finadv' as finadv, c->>'xclean' as xclean
from pp.kelley_black_book a
join jsonb_array_elements(a.response->'bb') as b on true
join jsonb_Array_elements(b->'mileage_list') as c on true
where a.vin = '3GNAXUEVXLS576033'
order by (c->>'range_begin')::integer


select distinct a.vin, b->>'mileage_cat', (c->>'range_begin')::integer as range_begin, c->>'range_end' as range_end, 
	c->>'avg' as avg, c->>'clean' as clean, c->>'rough' as rough, c->>'finadv' as finadv, c->>'xclean' as xclean
from pp.kelley_black_book a
join jsonb_array_elements(a.response->'bb') as b on true
join jsonb_Array_elements(b->'mileage_list') as c on true
where a.vin = '3GCUYGEDXKG149416'
order by (c->>'range_begin')::integer

select * from chr.build_data_describe_Vehicle limit 10

drop table if exists pp.equinox_built_in_2018 cascade;
create table pp.equinox_built_in_2018 as
select age(current_date, (c.response->'vinDescription'->>'built')::date) as age,
  c.response->'vinDescription'->>'vin' as vin, (c.response->'vinDescription'->>'built')::date as built, 
	(r.style ->>'id')::citext as style_id,
  (r.style->>'modelYear')::integer as model_year,
  (r.style ->'division'->>'_value_1')::citext as make,
  (r.style ->'model'->>'_value_1')::citext as model,
  (r.style ->>'mfrModelCode')::citext as model_code,
  case
    when r.style ->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drive,
  case
    when u.cab->>'_value_1' like 'Crew%' then 'crew' 
    when u.cab->>'_value_1' like 'Extended%' then 'double'  
    when u.cab->>'_value_1' like 'Regular%' then 'reg'  
    else 'n/a'   
  end as cab,
  coalesce(r.style ->>'trim', 'none')::citext as chr_trim,
  t->>'_value_1' as engine_displacement,
  case
    when v.bed->>'_value_1' like '%Bed' then substring(bed->>'_value_1', 1, position(' ' in bed->>'_value_1') - 1)
    else 'n/a'
  end as box_size   
from chr.build_data_describe_Vehicle c
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(r.style->'bodyType') with ordinality as v(bed) on true
  and v.ordinality =  1    
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
  and (s.engine->'installed'->>'cause' = 'OptionCodeBuild' or coalesce(s.engine->'installed'->>'cause', 'xxx') = 'VIN')
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->>'unit' = 'liters'  
where c.source = 'build'
  and c.style_count = 1
  and (r.style ->'model'->>'_value_1')::citext = 'equinox'
  and extract('year' from (c.response->'vinDescription'->>'built')::date) = 2018;
alter table pp.equinox_built_in_2018
add primary key (vin);
comment on table pp.equinox_built_in_2018 is 'build data extract for equinoxes built in 2018, 234 rows';

select vin from pp.equinox_built_in_2018  

select distinct a.vin, b->>'mileage_cat', (c->>'range_begin')::integer as range_begin, c->>'range_end' as range_end, 
	c->>'avg' as avg, c->>'clean' as clean, c->>'rough' as rough, c->>'finadv' as finadv, c->>'xclean' as xclean, 
	aa.*
from pp.kelley_black_book a
join pp.equinox_built_in_2018   aa on a.vin = aa.vin
join jsonb_array_elements(a.response->'bb') as b on true
join jsonb_Array_elements(b->'mileage_list') as c on true
-- where (c->>'range_begin')::integer = 1
-- order by built
-- where a.vin = '3GNAXUEU8JS616131' order by (c->>'range_begin')::integer  -- 2018
where a.vin = '3GNAXUEV0KL221929' order by (c->>'range_begin')::integer  -- 2019

select a.* 
from pp.equinox_built_in_2018 a
join ads.ext_vehicle_items b on a.vin  = b.vin -- 157
join ads.ext_vehicle_inventory_items c on b.vehicleitemid = c.vehicleitemid
order by a.built