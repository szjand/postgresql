﻿create index on scrapes.competitor_websites_data(lookup_date);
create index on scrapes.competitor_websites_data(vin);

select * from scrapes.competitor_websites_data limit 100

select count(*) from scrapes.competitor_websites_data where lookup_date = current_date

select count(*) from scrapes.competitor_websites_data where lookup_date = current_date

select * from scrapes.competitor_websites_data where lookup_date = current_date and new_used = 'used' limit 1000

select * from scrapes.competitor_websites_data where vin = '3FMCR9B60NRE19462'

select * from scrapes.competitor_websites_data where lookup_date = current_date and scraping_url = 'https://www.siouxfallsford.com/new-inventory/index.htm'

select * from scrapes.competitor_websites

select * 
from chr.describe_vehicle c
join jsonb_array_elements(c.response->'style') as r(style) on true
where (r.style ->'attributes'->>'id')::citext = '111698'

-- create schema pp;
-- comment on schema  pp is 'used car pricing project';
-- need the dealer
drop table if exists pp.comp_uc_today cascade;
create table pp.comp_uc_today as  -- 1203
select current_date, b.dealership_name as dealer, vin, model_year, make, model, 
	trim_level, miles, color, model_code, msrp::integer as msrp, price::integer as price 
from scrapes.competitor_websites_data a
join scrapes.competitor_websites b on a.scraping_url = b.scraping_url
where lookup_date = current_date 
	and new_used = 'used';
create index on pp.comp_uc_today(vin);


drop table if exists pp.comp_uc_today_chrome cascade;
create table pp.comp_uc_today_chrome as
-- !!!! this query is for style_count = 1 ONLY
select distinct a.dealer, a.vin, a.price, a.miles, --a.*, 
	(r.style ->>'id')::citext as chr_style_id,
  (r.style->>'modelYear')::integer as chr_model_year,
  (r.style ->'division'->>'_value_1')::citext as chr_make,
  (r.style ->'model'->>'_value_1'::citext)::citext as chr_model,
  (r.style ->>'mfrModelCode')::citext as chr_model_code,
  case
    when r.style ->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as chr_drive,
  case
    when u.cab->>'_value_1' like 'Crew%' then 'crew' 
    when u.cab->>'_value_1' like 'Extended%' then 'double'  
    when u.cab->>'_value_1' like 'Regular%' then 'reg'  
    else 'n/a'   
  end as chr_cab,
  coalesce(r.style ->>'trim', 'none')::citext as chr_trim,
  t->>'_value_1' as engine_displacement,
  s->'fuelType'->>'_value_1' as fuel,
  a.color, aa->>'colorName' as chr_color,
  (coalesce(c.response->'vinDescription'->>'builtMSRP', r.style->'basePrice'->>'msrp'))::numeric::integer as chr_msrp,
  case
    when v.bed->>'_value_1' like '%Bed' then substring(bed->>'_value_1', 1, position(' ' in bed->>'_value_1') - 1)
    else 'n/a'
  end as box_size, 
  d->>'oemCode' as peg,
  c.source, c.style_count 
from pp.comp_uc_today a
join chr.build_data_describe_vehicle c on a.vin = c.vin
  and c.style_count = 1
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(r.style->'bodyType') with ordinality as v(bed) on true
  and v.ordinality =  1    
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
  and (s.engine->'installed'->>'cause' = 'OptionCodeBuild' or coalesce(s.engine->'installed'->>'cause', 'xxx') = 'VIN')
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->>'unit' = 'liters'  
left join jsonb_array_elements(c.response->'exteriorColor') as aa(color) on true
		and aa->'installed'->>'cause' in ('RelatedColor','ExteriorColorBuild', 'OptionCodeBuild')
left join jsonb_array_elements(c.response->'factoryOption') as d(factory_options) on true
  and d.factory_options->'header'->>'_value_1' = 'PREFERRED EQUIPMENT GROUP'
  and d.factory_options->'installed'->>'cause' = 'OptionCodeBuild'
order by (r.style ->'division'->>'_value_1')::citext;
-- where a.vin = '2GC4YREY6N1243742'  
-- order by a.make, a.model		
create unique index on pp.comp_uc_today_chrome(vin);






-- now i need our current inventory chrome data
drop table if exists pp.inv_uc_today cascade;
create table pp.inv_uc_today as
with 
	pricings as (
		select a.vehicleinventoryitemid, b.amount as best_price,
			row_number() over (partition by a.vehicleinventoryitemid order by vehiclepricingts desc) as seq
		from ads.ext_vehicle_pricings a
		join ads.ext_vehicle_pricing_details b on a.vehiclepricingid = b.vehiclepricingid
			and b.typ = 'VehiclePricingDetail_BestPrice'
		join ads.ext_vehicle_inventory_items c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
			and c.thruts > now()
		join ads.ext_vehicle_items d on c.vehicleitemid = d.vehicleitemid),
	odo as (
		select a.vehicleitemid, a.value as miles,
			row_number() over (partition by a.vehicleitemid order by vehicleitemmileagets desc) as seq
		from ads.ext_vehicle_item_mileages a
		join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
		join ads.ext_vehicle_inventory_items c on b.vehicleitemid = c.vehicleitemid
			and c.thruts > now()) 			   
select a.stocknumber, b.vin, b.make, b.model, b.trim, b.yearmodel as model_year, b.exteriorcolor as color, d.best_price, e.miles
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
left join pricings d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
  and d.seq = 1
left join odo e on a.vehicleitemid = e.vehicleitemid
  and e.seq = 1  
where a.thruts > now(); 
create unique index on pp.inv_uc_today(vin);			



drop table if exists pp.inv_uc_today_chrome cascade;
create table pp.inv_uc_today_chrome as
-- !!!! this query is for style_count = 1 ONLY
select distinct a.stocknumber, a.vin, a.best_price, a.miles, --a.*, 
	(r.style ->>'id')::citext as chr_style_id,
  (r.style->>'modelYear')::integer as chr_model_year,
  (r.style ->'division'->>'_value_1')::citext as chr_make,
  (r.style ->'model'->>'_value_1'::citext)::citext as chr_model,
  (r.style ->>'mfrModelCode')::citext as chr_model_code,
  case
    when r.style ->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as chr_drive,
  case
    when u.cab->>'_value_1' like 'Crew%' then 'crew' 
    when u.cab->>'_value_1' like 'Extended%' then 'double'  
    when u.cab->>'_value_1' like 'Regular%' then 'reg'  
    else 'n/a'   
  end as chr_cab,
  coalesce(r.style ->>'trim', 'none')::citext as chr_trim,
  t->>'_value_1' as engine_displacement,
  s->'fuelType'->>'_value_1' as fuel,
  a.color, aa->>'colorName' as chr_color,
  (coalesce(c.response->'vinDescription'->>'builtMSRP', r.style->'basePrice'->>'msrp'))::numeric::integer as chr_msrp,
  case
    when v.bed->>'_value_1' like '%Bed' then substring(bed->>'_value_1', 1, position(' ' in bed->>'_value_1') - 1)
    else 'n/a'
  end as box_size, 
  d->>'oemCode' as peg,
  c.source, c.style_count 
from pp.inv_uc_today a
join chr.build_data_describe_vehicle c on a.vin = c.vin
  and c.style_count = 1
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(r.style->'bodyType') with ordinality as v(bed) on true
  and v.ordinality =  1    
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
  and (s.engine->'installed'->>'cause' = 'OptionCodeBuild' or coalesce(s.engine->'installed'->>'cause', 'xxx') = 'VIN')
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->>'unit' = 'liters'  
left join jsonb_array_elements(c.response->'exteriorColor') as aa(color) on true
		and aa->'installed'->>'cause' in ('RelatedColor','ExteriorColorBuild', 'OptionCodeBuild')
left join jsonb_array_elements(c.response->'factoryOption') as d(factory_options) on true
  and d.factory_options->'header'->>'_value_1' = 'PREFERRED EQUIPMENT GROUP'
  and d.factory_options->'installed'->>'cause' = 'OptionCodeBuild'
order by (r.style ->'division'->>'_value_1')::citext;
-- where a.vin = '2GC4YREY6N1243742'  
-- order by a.make, a.model		
create unique index on pp.inv_uc_today_chrome(vin);


-- 2 views
-- 1 is our inventory with matching competitors
-- 1 is multiple instances in our own inventory

-- 1
select string_agg('a.' || column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'pp'
  and table_name = 'inv_uc_today_chrome'

sent to slack as competitor_matching_on_chr_style_id_v1.xlsx 10/8/22
select a.chr_style_id,
	a.chr_style_id::citext as chrome,
	'Rydell'::citext as store,
	'aaa' as dealer,a.chr_msrp::text,a.best_price as price,a.miles,coalesce(a.chr_color, a.color) as color,a.stocknumber,a.vin,a.chr_model_year::citext,a.chr_make,a.chr_model,a.chr_model_code,a.chr_drive,a.chr_cab,a.chr_trim,a.engine_displacement,a.fuel,a.box_size,a.peg
from pp.inv_uc_today_chrome a
join pp.comp_uc_today_chrome b on a.chr_style_id = b.chr_style_id
union 
select b.chr_style_id,
	'',
	b.dealer,
	b.dealer,''::text,b.price,b.miles,coalesce(b.chr_color, b.color),''::citext, ''::citext,''::citext,''::citext,''::citext,''::citext,''::citext,''::citext,''::citext,''::citext,''::citext,''::citext,''::citext
from pp.inv_uc_today_chrome a
join pp.comp_uc_today_chrome b on a.chr_style_id = b.chr_style_id
order by chr_style_id, dealer


order by a.chr_style_id, a.vin, b.vin




select b.the_count, a.* 
from pp.inv_uc_today_chrome a
join (
  select chr_style_id, count(*) as the_count
  from pp.inv_uc_today_chrome
  group by chr_style_id
  having count(*) > 1) b on a.chr_style_id = b.chr_style_id
order by b.the_count desc, a.chr_style_id














select distinct b.vin  -- 410/128/57  355
from ads.ext_vehicle_inventory_items a
join ads.ext_Vehicle_items b on a.vehicleitemid = b.vehicleitemid
where a.thruts::date > current_date
	and not exists (
		select 1
		from chr.build_data_describe_Vehicle
		where vin = b.vin)


look at used_car_Crayon_report\make_model_classification.sql

-- -- exterior color from build data
-- select distinct aa->'installed'->>'cause'
-- from pp.uc_today a
-- join chr.build_data_describe_vehicle c on a.vin = c.vin
--   and c.style_count = 1
-- left join jsonb_array_elements(c.response->'exteriorColor') as aa(color) on true
-- 		and aa->'installed'->>'cause' in ( 'RelatedColor','ExteriorColorBuild', 'OptionCodeBuild')  
-- 
-- select count(*) filter (where aa->'installed'->>'cause' = 'RelatedColor'),
--   count(*) filter (where aa->'installed'->>'cause' = 'ExteriorColorBuild'),
--   count(*) filter (where aa->'installed'->>'cause' = 'OptionCodeBuild')
-- from pp.uc_today a
-- join chr.build_data_describe_vehicle c on a.vin = c.vin
--   and c.style_count = 1
-- left join jsonb_array_elements(c.response->'exteriorColor') as aa(color) on true
