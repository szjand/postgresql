﻿select current_date, b.dealership_name as dealer, vin, model_year, make, model, 
	trim_level, miles, color, model_code, msrp::integer as msrp, price::integer as price 
from scrapes.competitor_websites_data a
join scrapes.competitor_websites b on a.scraping_url = b.scraping_url
where lookup_date = current_date 
	and new_used = 'used';


-- 8/20/20 -> current
select min(lookup_date), max(lookup_date) from scrapes.competitor_websites_data	

select b.dealership_name as dealer, a.vin, count(*)
from scrapes.competitor_websites_data a
join scrapes.competitor_websites b on a.scraping_url = b.scraping_url
where new_used = 'used'
  and exists (
    select 1
    from scrapes.competitor_websites_data
    where scraping_url = a.scraping_url
      and vin = a.vin
      and lookup_date = current_date)
group by b.dealership_name, a.vin	
order by b.dealership_name, a.vin	

-- 45 dealers on 6/2, 18 on 10/30
select a.lookup_date, b.dealership_name, count(*)
from scrapes.competitor_websites_data a
join scrapes.competitor_websites b on a.scraping_url = b.scraping_url
where new_used = 'used'
  and a.lookup_date > '05/31/2022'
group by a.lookup_date, b.dealership_name
order by a.lookup_date

-- the number of dealers scraped each day since june
select lookup_date, count(*)
from (
	select a.lookup_date, b.dealership_name, count(*)
	from scrapes.competitor_websites_data a
	join scrapes.competitor_websites b on a.scraping_url = b.scraping_url
	where new_used = 'used'
		and a.lookup_date > '05/31/2022'
	group by a.lookup_date, b.dealership_name) aa
group by lookup_date
order by lookup_date

-- the last date each dealer was scraped
select dealership_name, max(lookup_date) as last_scraped
from (
	select a.lookup_date, b.dealership_name, count(*)
	from scrapes.competitor_websites_data a
	join scrapes.competitor_websites b on a.scraping_url = b.scraping_url
	where new_used = 'used'
		and a.lookup_date between '06/01/2022' and current_date - 1
	group by a.lookup_date, b.dealership_name) aa
group by dealership_name
order by last_scraped, dealership_name
	