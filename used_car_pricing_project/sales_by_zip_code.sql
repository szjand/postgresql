﻿drop table if exists wtf;
create temp table wtf as  
select a.bopmast_company_number  as store, a.bopmast_vin as vin, a.vehicle_type as new_used, 
	 left(b.zip_code::text, 5)  as zip_code, 
	 a.date_capped, a.retail_price::integer, c.year, c.make, c.model, c.body_style
from arkona.ext_bopmast a
join arkona.ext_bopname b on a.buyer_number = b.bopname_record_key
  and b.company_individ is not null
  and b.bopname_company_number in ('RY1','RY2')
  and b.zip_code is not null
  and length(b.zip_code::text) > 4
join arkona.ext_inpmast c on a.bopmast_vin = c.inpmast_vin  
where a.record_status = 'U'
  and a.sale_type <> 'W'
  and a.date_capped between '01/01/2017' and current_date - 1
  and bopmast_company_number in ('RY1','RY2')
  and similarity(a.bopmast_search_name, b.bopname_search_name) > .44;




select *
from wtf
limit 100


select make
from wtf
group by make

select * from arkona.ext_bopmast where bopmast_vin = '1GC5YME79NF112103'

select * 
from (
select distinct zip_code,
  count(bopmast_vin) over (partition by zip_code) as sales_per_zip
from wtf) a
order by sales_per_zip desc


select * 
from (
select distinct zip_code,
  count(bopmast_vin) over (partition by zip_code) as sales_per_zip
from wtf
where vehicle_type = 'U') a
order by sales_per_zip desc

select * 
from (
select distinct zip_code,
  count(bopmast_vin) over (partition by zip_code) as sales_per_zip
from wtf
where vehicle_type = 'N') a
order by sales_per_zip desc

with 
  used_sales as (
		select * 
		from (
		select distinct zip_code,
			count(bopmast_vin) over (partition by zip_code) as sales_per_zip
		from wtf
		where vehicle_type = 'U') a) 
select *
from wtf a
join used_sales b on a.zip_code = b.zip_code
where a.vehicle_type = 'U'

select "<10K","10K-20K","20K-30K",
  lower("<10K"),lower("10K-20K"),lower("20K-30K"),
  upper("<10K"),upper("10K-20K"),upper("20K-30K")
from (  
select int4range (0,10000) as "<10K",
  int4range(10000, 20000) AS "10K-20K",
  int4range(20000,30000) as "20K-30K") a

select "<10K" @> 10000
from (
select int4range (0,10000) as "<10K",
  int4range(10000, 20000) AS "10K-20K",
  int4range(20000,30000) as "20K-30K") a 


zipcode  new 2016 used 2016 total 2016 ... new 2021  used 2021  total 2021  total new total used total
1234
2345
3456
4567


select count(vin) over (partition by zip_code)
from wtf 
where extract(year from date_capped) = 2016
  and new_used = 'N'

select zip_code, 
--   count(vin) filter (where extract(year from date_capped) = 2016 and new_used = 'N') as new_2016,
--   count(vin) filter (where extract(year from date_capped) = 2016 and new_used = 'U') as used_2016,
--   count(vin) filter (where extract(year from date_capped) = 2016) as total_2016,
  count(vin) filter (where extract(year from date_capped) = 2017 and new_used = 'N') as new_2017,
  count(vin) filter (where extract(year from date_capped) = 2017 and new_used = 'U') as used_2017,
  count(vin) filter (where extract(year from date_capped) = 2017) as total_2017,  
  count(vin) filter (where extract(year from date_capped) = 2018 and new_used = 'N') as new_2018,
  count(vin) filter (where extract(year from date_capped) = 2018 and new_used = 'U') as used_2018,
  count(vin) filter (where extract(year from date_capped) = 2018) as total_2018,
  count(vin) filter (where extract(year from date_capped) = 2019 and new_used = 'N') as new_2019,
  count(vin) filter (where extract(year from date_capped) = 2019 and new_used = 'U') as used_2019,
  count(vin) filter (where extract(year from date_capped) = 2019) as total_2019,
  count(vin) filter (where extract(year from date_capped) = 2020 and new_used = 'N') as new_2020,
  count(vin) filter (where extract(year from date_capped) = 2020 and new_used = 'U') as used_2020,
  count(vin) filter (where extract(year from date_capped) = 2020) as total_2020,
  count(vin) filter (where extract(year from date_capped) = 2021 and new_used = 'N') as new_2021,
  count(vin) filter (where extract(year from date_capped) = 2021 and new_used = 'U') as used_2021,
  count(vin) filter (where extract(year from date_capped) = 2021) as total_2021,   
  count(vin) filter (where extract(year from date_capped) = 2022 and new_used = 'N') as new_2022,
  count(vin) filter (where extract(year from date_capped) = 2022 and new_used = 'U') as used_2022,
  count(vin) filter (where extract(year from date_capped) = 2022) as total_2022,   
  count(vin) as total
from wtf
group by zip_code  
having count(vin) > 20
order by total desc
order by zip_code