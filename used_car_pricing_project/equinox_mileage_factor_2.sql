﻿/*
12/24/22
continuation of equinox_mileage_factor.sql

use what i learned in the toc stuff to get solid fs data (used_car_pricing_project\toc_lisa_dec_22\fs_data.sql)

include wholesale - they will ear analysis as to why, but clearly mark them

be explicit about intra market from the outset, life of the vehicle is from orig acquisition -> retail, 
  the intramarket wholesale is irrelevant

*/
-- 2 years of fs data yields 541 equinoxes
-- expand it to 4 years of data, now 991
drop table if exists pp.equinox_mf_step_1 cascade;
create unlogged table pp.equinox_mf_step_1 as
		select distinct b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
			a.control as stock_number, a.amount,
			case when a.amount < 0 then 1 else -1 end as unit_count
		from fin.fact_gl a
		inner join dds.dim_date b on a.date_key = b.date_key
			and b.year_month between 201812 and 202211-----------------------------------------------------------------------------
		inner join fin.dim_account c on a.account_key = c.account_key
			and c.current_row
	-- add journal
		inner join fin.dim_journal aa on a.journal_key = aa.journal_key
			and aa.journal_code in ('VSN','VSU')
		inner join ( -- d: fs gm_account page/line/acct description
			select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
			from fin.fact_fs a
			inner join fin.dim_fs b on a.fs_key = b.fs_key
				and b.year_month between 201812 and 202211  ---------------------------------------------------------------------
				and b.page = 16 and b.line between 1 and 12 -- retail & wholesale
			inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
			inner join fin.dim_account e on d.gl_account = e.account
				and e.account_type_code = '4'
				and e.current_row
			inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
		where a.post_status = 'Y'
		  and a.control not in ('32696XXB');
create index on pp.equinox_mf_step_1(stock_number);
comment on table pp.equinox_mf_step_1 is 'first cut of fs data for 2 years of equinox sales, retail and wholesale,
  for building the mileage factor data set';
  
select count(*) from pp.equinox_mf_step_1  --8230/16404

-- equinox only
drop table if exists pp.equinox_mf_step_2 cascade;
create unlogged table pp.equinox_mf_step_2 as
-- 991 rows
select * 
from pp.equinox_mf_step_1 a
where exists(
  select 1
  from arkona.xfm_inpmast
  where inpmast_stock_number = a.stock_number
    and model = 'equinox');
comment on table pp.equinox_mf_step_2 is 'narrow pp.equinox_mf_step_1 down to equinoxes only';

-- 12 dups
select stock_number, count(*)
from  pp.equinox_mf_step_2
group by stock_number
having count(*) > 1

select a.year_month, a.stock_number, a.unit_count, a.gl_account, a.line_label, a.unit_count, b.* 
from pp.equinox_mf_step_1 a
join (
	select stock_number, count(*)
	from  pp.equinox_mf_step_2
	group by stock_number
	having count(*) > 1) b on a.stock_number = b.stock_number
order by a.stock_number, a.year_month	
/*
32696XXB in dealertrack retail to lafontaine, in bopmast & tool ws to adesa wtf
select * from arkona.ext_bopmast where bopmast_stock_number = '32696XXB'

select * from arkona.xfm_inpmast where inpmast_stock_number = '32696XXB'

the fucking vin was changed
13 rows as 2LMDU88C28BJ19066

2 rows as  2GNALDEK9C6281988
*/

-- narrow data down to stock number, year_month, sale_price
drop table if exists pp.equinox_mf_step_3 cascade;
create unlogged table pp.equinox_mf_step_3 as
select stock_number, max(year_month) filter (where amount < 0) as year_month, 
	(min(-amount::integer) filter (where amount < 0))  as sale_price
from (
	select a.*, row_number() over (partition by a.stock_number order by a.year_month desc) as seq
	from pp.equinox_mf_step_2 a
	join (
		select stock_number
		from pp.equinox_mf_step_2
		group by stock_number
		having count(*) > 1) b on a.stock_number = b.stock_number		
	order by a.stock_number, a.year_month) aa
group by stock_number having count(*) > 2
union
select a.stock_number as stock_number, a.year_month, -a.amount::integer as sale_price
from pp.equinox_mf_step_2 a
join (
  select stock_number
  from pp.equinox_mf_step_2
  group by stock_number
  having count(*) = 1) b on a.stock_number = b.stock_number;
alter table pp.equinox_mf_step_3
add primary key (stock_number);
comment on table pp.equinox_mf_step_3 is 'narrow pp.equinox_mf_step_2 down to stock number, year_month, sale_price, eliminate backons';

select count(*) from pp.equinox_mf_step_3; -- 964

-- only 872 in vii, missing 72
select a.*
from pp.equinox_mf_step_3 a
join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber

-- most are L stock numbers, lease purchases, 85 of the 92
select right(a.stock_number,1), count(*)
from pp.equinox_mf_step_3 a
left join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
where b.stocknumber is null
group by right(a.stock_number,1)

-- so, eliminate the L's, 824 remaining
select a.*
from pp.equinox_mf_step_3 a
join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
where right(a.stock_number, 1) <> 'L'

-- vins/build_data, most already have build data
select a.*, d.vin, d.style_count
from pp.equinox_mf_step_3 a
join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
left join chr.build_data_describe_vehicle d on c.vin = d.vin
where right(a.stock_number, 1) <> 'L'

-- those that don't, only 10
select a.*, c.vin, d.*, c.vehicleitemid, b.vehicleinventoryitemid
from pp.equinox_mf_step_3 a
join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
left join chr.build_data_describe_vehicle d on c.vin = d.vin
where right(a.stock_number, 1) <> 'L'
  and d.vin is null

drop table if exists pp.equinox_mf_step_4 cascade;
create unlogged table pp.equinox_mf_step_4 as
-- add vin, prior stock, sale date, sale type, viid, viiid, model_year
select e.stock_number, e.vin, e.yearmodel as model_year, e.prior_stock, e.year_month, e.from_date, e.sale_date, e.sale_price, e.sale_type, 
	e.vehicleitemid, e.vehicleinventoryitemid
from (
	select a.stock_number, c.vin, c.yearmodel,
		( -- prior stock number on intra market
			select stocknumber
			FROM ads.ext_Vehicle_Inventory_Items
			WHERE VehicleItemID = (
				select vehicleitemid
				from ads.ext_vehicle_items
				where vin = c.vin
					and right(a.stock_number, 1) in ('G','T','H'))
			order by fromts desc
			limit 1 offset 1) as prior_stock,
		a.year_month, b.fromts::date as from_date, d.soldts::date as sale_date, a.sale_price, 
		split_part(d.typ, '_', 2) as sale_type, c.vehicleitemid, b.vehicleinventoryitemid
	from pp.equinox_mf_step_3 a
	join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
	join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
	left join ads.ext_vehicle_sales d on b.vehicleinventoryitemid = d.vehicleinventoryitemid
	 and d.status <> 'VehicleSale_SaleCanceled') e
left join ads.ext_vehicle_inventory_items f on e.prior_stock = f.stocknumber;
alter table pp.equinox_mf_step_4
add primary key(stock_number);
comment on table pp.equinox_mf_step_4 is 'add vin, prior stock, sale date, sale type, viid, viiid';

-- here are some prior stock anomalies, the query assigned prior stock as the same as the stock #
-- the problem is when there are other transactions on the vehicleitem
select * from pp.equinox_mf_step_4 where vin in ('2GNFLGEK8C6272836','2GNFLNEK3D6118556','2GNAXSEV1J6152577','2CNFLGEC0B6467924','3GNAXVEX1KL370504','2GNFLFEK4G6176945') order by vin, from_date
-- need to just do it manually for now
delete from pp.equinox_mf_step_4 where vin in ('3GNAXVEX1KL370504','2CNFLNEC9B6384738','1GNFLGEK7FZ127361');
update pp.equinox_mf_step_4 set prior_stock = 'H15428C' where vin = '2CNFLGEC0B6467924' and prior_stock is not null;
update pp.equinox_mf_step_4 set prior_stock = 'G39684X' where vin = '2GNAXSEV1J6152577' and prior_stock is not null;
update pp.equinox_mf_step_4 set prior_stock = 'H15653B' where vin = '2GNFLFEK4G6176945' and prior_stock is not null;
update pp.equinox_mf_step_4 set prior_stock = 'H10808RA' where vin = '2GNFLGEK8C6272836' and prior_stock is not null;
update pp.equinox_mf_step_4 set prior_stock = 'G38022RA' where vin = '2GNFLNEK3D6118556' and prior_stock is not null;

-- delete the rows for the intra market wholesale transaction 53 rows
delete
from pp.equinox_mf_step_4
where stock_number in (
	select a.stock_number 
	from pp.equinox_mf_step_4 a
	join (
		select prior_stock
		from pp.equinox_mf_step_4
		where prior_stock is not null) b on a.stock_number = b.prior_stock);

-- update from_date for intra market, 56 rows
update pp.equinox_mf_step_4 a
set from_date = b.prior_from_date
from (
	select stock_number, from_date, prior_stock,
		(
			select fromts::date
			from ads.ext_vehicle_inventory_items
			where stocknumber = a.prior_stock) as prior_from_date
	from pp.equinox_mf_step_4 a
	where prior_stock is not null) b
where a.stock_number = b.stock_number;

select * from pp.equinox_mf_step_4 where from_date > sale_date

-- add days_to_sell, source, miles, mileage_cat, days_avail, msrp(chrome)
drop table if exists pp.equinox_mf_step_5 cascade;
create unlogged table pp.equinox_mf_step_5 as
select stock_number, vin, model_year, source, from_date, sale_date, days_to_sell, days_avail,
  sale_price, sale_type, miles, 
	case
-- 		when (dd.miles = 0) or (Length(Trim(dd.model_year)) <> 4) or (position('.' in dd.model_year) <> 0) or (position('C' in dd.model_year) <> 0) or (position('/' in dd.model_year) <> 0) or ((dd.model_year < '1901') OR (dd.model_year > '2050')) then Null
		when 
			dd.miles /
				case
					when round((from_date - to_date('09'||'01'||(model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
					else round((from_date - to_date('09'||'01'||(model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
				end <= 4000 then 'Freaky Low'
		when 
			dd.miles /
				case
					when round((from_date - to_date('09'||'01'||(model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
					else round((from_date - to_date('09'||'01'||(model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
				end <= 8000 then 'Very Low'
		when 
			dd.miles /
				case
					when round((from_date - to_date('09'||'01'||(model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
					else round((from_date - to_date('09'||'01'||(model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
				end <= 12000 then 'Low'
		when 
			dd.miles /
				case
					when round((from_date - to_date('09'||'01'||(model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
					else round((from_date - to_date('09'||'01'||(model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
				end <= 15000 then 'Average'
		when 
			dd.miles /
				case
					when round((from_date - to_date('09'||'01'||(model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
					else round((from_date - to_date('09'||'01'||(model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
				end <= 18000 then 'High'
		when 
			dd.miles /
				case
					when round((from_date - to_date('09'||'01'||(model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
					else round((from_date - to_date('09'||'01'||(model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
				end <= 22000 then 'Very High'
		when 
			Dd.miles /
				case
					when round((from_date - to_date('09'||'01'||(model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
					else round((from_date - to_date('09'||'01'||(model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
				end <= 999999 then 'Freaky High'
		else
			'Unknown'																		
	end::citext as miles_cat
from ( -- dd
	select aa.stock_number, vin, model_year::integer, prior_stock, 
		case
			when prior_stock is not null then
				case
					when right(prior_stock, 1) in ('A','B','C','D','E','F') then 'trade'
					when right(prior_stock, 1) in ('M','X') then 'auction'
					when right(prior_stock, 1)  = 'P' then 'street'
					when right(prior_stock, 1)  = 'L' then 'lease'
				end
			else
				case
					when right(aa.stock_number, 1) in ('A','B','C','D','E','F') then 'trade'
					when right(aa.stock_number, 1) in ('M','X') then 'auction'
					when right(aa.stock_number, 1)  = 'P' then 'street' 
					when right(aa.stock_number, 1)  = 'L' then 'lease'    
				end
		end as source,
		from_date, sale_date, sale_date - from_date as days_to_sell, days_avail,
		sale_price, sale_type, bb.miles, aa.vehicleitemid, aa.vehicleinventoryitemid 
	from pp.equinox_mf_step_4 aa
	join ( -- extended miles date range to pick up miles only collected at eval
		select a.stock_number, max(b.value) as miles
		from pp.equinox_mf_step_4 a
		left join ads.ext_vehicle_item_mileages b on a.vehicleitemid = b.vehicleitemid
			and b.vehicleitemmileagets::date between a.from_date - 5 and a.sale_date
		group by a.stock_number) bb on aa.stock_number = bb.stock_number
	join ( -- days avail
		select a.vehicleinventoryitemid, coalesce(max(b.thruts::date) - min(b.fromts::date), 0) as days_avail
		from pp.equinox_mf_step_4 a
		left join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
			and status = 'RMFlagAV_Available'
		group by a.vehicleinventoryitemid) cc on aa.vehicleinventoryitemid = cc.vehicleinventoryitemid) dd;
alter table pp.equinox_mf_step_5
add primary key(stock_number);
comment on table pp.equinox_mf_step_5 is 'add days_to_sell, source, miles, mileage_cat, days_avail';

-- chrome time
-- oops, there were some fuckups, non equinox vins. now dow to 783
-- interior, color, package
drop table if exists pp.equinox_mf_step_6 cascade;
create unlogged table pp.equinox_mf_step_6 as
SELECT a.*,
	(c->>'id')::citext as chrome_style_id,
  (c ->'division'->>'_value_1')::citext as make,
  (c ->'model'->>'_value_1'::citext)::citext as model,
  (c->>'trim')::citext as trim_level,
  (b.response->>'bestStyleName')::citext as style_name,
  (c ->>'mfrModelCode')::citext as model_code,
  case
    when c ->>'drivetrain' like 'Rear%' then 'RWD'
    when c ->>'drivetrain' like 'Front%' then 'FWD'
    when c ->>'drivetrain' like 'Four%' then '4WD'
    when c ->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drive,
  (coalesce(b.response->'vinDescription'->>'builtMSRP', c->'basePrice'->>'msrp'))::numeric::integer as msrp,
  d->>'cylinders' as eng_cylinders, e->>'_value_1' as eng_displacement,
  f->>'oemCode' as peg,
  i->>'colorName' as ext_color,
  h->>0 as interior
FROM pp.equinox_mf_step_5 a
join chr.build_data_describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'style') as c on true
left join jsonb_array_elements(b.response->'engine') as d on true  
  and (d->'installed'->>'cause' = 'OptionCodeBuild' or coalesce(d->'installed'->>'cause', 'xxx') = 'VIN')
left join jsonb_array_elements(d->'displacement'->'value') as e on true
  and e ->>'unit' = 'liters' 
left join jsonb_array_elements(b.response->'factoryOption') as f on true --peg
  and f->'header'->>'_value_1' = 'PREFERRED EQUIPMENT GROUP'
  and f->'installed'->>'cause' = 'OptionCodeBuild'
left join jsonb_array_elements(b.response->'factoryOption') as g on true --interior
  and g->'header'->>'_value_1' = 'SEAT TRIM'
  and g->'installed'->>'cause' in ('RelatedColor','ExteriorColorBuild', 'OptionCodeBuild')
left join jsonb_array_elements(g->'description') as h on true
left join jsonb_array_elements(b.response->'exteriorColor') as i on true
  and i->'installed'->>'cause' in ('RelatedColor','ExteriorColorBuild', 'OptionCodeBuild')
where (c ->'model'->>'_value_1'::citext)::citext = 'equinox'
  and b.style_count = 1;
alter table pp.equinox_mf_step_6
add primary key(stock_number);
comment on table pp.equinox_mf_step_6 is 'add some chrome data';  

select * from pp.equinox_mf_step_6


select a.stock_number, a.model_year, f->>'oemCode', f->'description'
FROM pp.equinox_mf_step_5 a
join chr.build_data_describe_vehicle b on a.vin = b.vin
left join jsonb_array_elements(b.response->'factoryOption') as f on true
  and f->'installed'->>'cause' = 'OptionCodeBuild'
  and f->'header'->>'_value_1' = 'ADDITIONAL EQUIPMENT'

select f->>'oemCode', f->'description', count(*), array_agg(distinct model_year)
FROM pp.equinox_mf_step_5 a
join chr.build_data_describe_vehicle b on a.vin = b.vin
left join jsonb_array_elements(b.response->'factoryOption') as f on true
  and f->'installed'->>'cause' = 'OptionCodeBuild'
  and f->'header'->>'_value_1' = 'ADDITIONAL EQUIPMENT'
group by f->>'oemCode', f->'description'
order by f->>'oemCode'


select * from chr.build_data_describe_vehicle where vin = '2CNDL037396210948'













select distinct status from ads.ext_vehicle_inventory_item_statuses where status like '%avail%'


-- verify my thoughts on deleting the prior stock number transaction
-- before i do that, i need the age of the vehicle in inventory, vehicle source


			
-- intra market strategy: get the prior stock number delete the rows where stock_number =  prior_stock


-- 1023 rows from xfm_inpmast
select a.stock_number, a.year_month, a.sale_price, b.inpmast_vin
from pp.equinox_mf_step_3 a
join arkona.xfm_inpmast b on a.stock_number = b.inpmast_stock_number
group by a.stock_number, a.year_month, a.sale_price, b.inpmast_vin
order by b.inpmast_vin  