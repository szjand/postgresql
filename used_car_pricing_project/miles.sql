﻿-- from customer_evaluation/evaluation_comparison_data_from_tool.sql
	case round(
				 aa.miles/ case 
										 when round((the_date - to_date('09'||'01'||(aa.yearmodel::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
										 else round((the_date - to_date('09'||'01'||(aa.yearmodel::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
									 end, 0) 
	  when < 5000 then 35
	  when between 5000 and 9999 then 25
	  when between 10000 and 14999 then 15
	  when between 15000 and 19999 then -5
	  when between 20000 and 24999 then -15
	  when between 25000 and 999999 then -30
	end

		miles_per_year, 
		case
			when miles_per_year < 5000 then 35
			when miles_per_year between 5000 and 9999 then 25
			when miles_per_year between 10000 and 14999 then 15
			when miles_per_year between 15000 and 19999 then -5
			when miles_per_year between 20000 and 24999 then -15
			when miles_per_year between 25000 and 999999 then -30
		end as mileage_score	


-- from lisa_toc/query_history.sql
select store_code as store, year, make, model, trim, shape, shape_size, best_price, status, days_owned, days_in_status, sale_type, sale_date, 
   case 
     when miles/(2021 - year::integer)  < 5000 then 'freaky low'
     when miles/(2021 - year::integer)  between 5000 and 10000 then '5 - 10'
     when miles/(2021 - year::integer) between 10001 and 15000 then '10-15'
     when miles/(2021 - year::integer) between 15001 and 20000 then '15-20'
     when miles/(2021 - year::integer) between 20001 and 25000 then '20-25'
     when miles/(2021 - year::integer) > 25000 then 'freaky hi'
   end as mile_cat
from greg.uc_daily_snapshot_beta_1
where sale_date between current_date - 180 and current_date
  and the_Date = sale_date
  and best_price between 15000 and 20000
  and sale_type = 'retail'
--   and status &lt;&gt; 'avail_aged' -- in ('raw', 'avail_fresh')
order by shape,shape_size, make, model


-- from misc_sql/ucinv/base_vehicle_miles.sql
-- most recent miles from tool
  select vehicle_inventory_item_id, coalesce(last_miles, -1)
  from (
    select a.vehicle_inventory_item_id, 
      last_value(value) over (
          partition by vehicle_inventory_item_id
          order by vehicle_inventory_item_id, vehicleitemmileagets
          RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as last_miles              
    from greg.uc_base_vehicles a
    left join ads.ext_vehicle_item_mileages b on a.vehicle_item_id = b.vehicleitemid  
      and 
        case
          when right(a.stock_number, 1) in ('X','G') and right(a.stock_number, 2) <> 'XX' then 1 = 1
          else b.vehicleitemmileagets::Date between a.date_acquired -7 and a.sale_date
        end) x
  group by vehicle_inventory_item_id, last_mile

-- from used_cars_misc/competitor_pricing_v2.sql
-- some foster analytics

 select c.*, abs_z_dep - abs_z_miles
from (
	select b.*, 
		abs(round((miles::numeric(9,3) - mean_miles)/stddev_miles, 2)) as abs_z_miles,
		round(abs((dep_x - mean_dep)/stddev_dep), 2) as abs_z_dep
	from (
		select a.*, 
			(avg(miles) over (partition by year, left(model, 8)))::integer as mean_miles,
			(stddev(miles) over (partition by year, left(model, 8)))::integer as stddev_miles,
			round(abs(price/msrp -1), 3) as dep_x, 
			round(avg(abs(price/msrp -1)) over (partition by year, left(model,8)), 3) mean_dep,
			round(stddev(abs(price/msrp -1)) over (partition by year, left(model, 8)), 3) as stddev_dep
		from the_temp a) b) c
order by year, left(model, 8), abs_z_dep - abs_z_miles desc 


-- from used_cars_misc/competitor_pricing.sql
-- z: (price - mean)/stddev
select dealer, vin, color, miles, a.price, 
	round((a.price::numeric(8,3) - b.mean_price)/stddev_price, 3) as z_price,
	round((a.miles::numeric(8,3) - b.mean_miles)/stddev_miles, 3) as z_miles
from equinox a
join (
	select stddev(price)::integer as stddev_price, stddev(miles)::integer as stddev_miles, 
		jon.median(array_agg(price::numeric order by price))::integer as median_price, 
		jon.median(array_agg(miles::numeric order by miles))::integer as median_miles,
		avg(price)::integer as mean_price, avg(miles)::integer as mean_miles, av
		max(price) - min(price) as range_price, max(miles) - min(miles) as range_miles
	from equinox) b on true
order by (a.price::numeric(8,3) - b.mean_price)/stddev_price desc