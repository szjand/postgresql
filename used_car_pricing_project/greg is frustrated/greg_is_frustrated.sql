﻿
/*

I'm frustrated about the market's used car performance in January. Really frustrated with myself that we don't have a used car measurement system 
in place that would have raised the warnings months ago. Oh well we can only deal with today. I need some queries (I'm sure I've asked for them 
before but I didn't save them where I can find them) around used car sales/wholesale/purchasing/inventory.
I'd like to see used vehicle retail sales for January. Attributes -- date, stock #, VIN, year make model trim, miles, date purchased, 
date available, days to sell purchased, days to sell available, last best price, sales gross, F&I gross, 
recon dollars, recon gross (recon dollars * 0.5), purchase source (trade, auction, street...), 
purchaser, salesperson.

We pissed away $400k in market in used car gross

Our expectation is to gross $2,000 per unit on average. Very doable -- have stores doing it and then some. I don't think $2,000 is an unrealistic expectation. We thought $1,500 was a good job 25 years ago.


We currently talk about used cars in total and reflect on our performance by month. I'm interested in reporting/discussing performance weekly. Treat each week's purchases as a separate "Week Class" using Sun or Mon date as a name. I'd like to use KBB as our benchmark right now for purchase performance, pricing and sales performance. As we identify data-based replicable shortcomings with KBB for a specific model we can come up with a better measure.

Once Afton has the booking page up we can start slicing the data with our benchmark.

I want to also report on purchase performance based on purchaser and use this info for assessment and coaching. I'm done with "we don't want to scare, hurt their feelings" or whatever. If someone is paying too much or too little they need to know about it and coached.

I want to create information thay changes daily actions to improve our performance.

As part of this project, we will be documenting the organization's processes for purchasing, pricing, reconditioning, selling, wholesaling, merchandising, marketing, etc. used vehicles.

To think we will want to lay off people if our performance doesn't improve makes me sick to my stomach. This stuff truly isn't rocket science. It takes process, execution, discussion, coaching, etc. Transparent POOGI.

Thought I mentioned that, but yes. Need wholesale too. What I want to get rid of is the month-over-month comparison to assess our performance. From where we're starting we can show month-over-month improvement for many months and still piss away millions over that same period. We can show purchase amount to KBB, "Potential Log Gross", sales gross, wholesale gross by Week Class instead waiting for the financial statements. We can also show "portfolio performance" for each of our purchasers too, e.g. if you show that Eric's purchases don't make money then coach him on how to improve.

The transaction counts are ridiculously small on a daily basis. We can figure out a POOGI process pretty quickly
*/


-- select *
-- from fin.fact_fs a
-- join fin.dim_fs b on a.fs_key = b.fs_key
--   and b.year_month = 202301
-- join fin.dim_fs_account c on a.fs_Account_key = c.fs_account_key  
--   and c.gl_account in ('164700','164701','164702','165100','165101','165102','264700','264701','264702','264710','264720','265100','265101','265102','265110')
-- 
-- 
-- select *
-- from fin.fact_fs a
-- join fin.dim_fs b on a.fs_key = b.fs_key
--   and b.year_month = 202301
-- join fin.dim_fs_account c on a.fs_Account_key = c.fs_account_key    
-- 
-- 
-- drop table if exists board;
-- create temp table board as
-- select store,stock_number, boarded_Date, board_type, board_sub_type, model_year, make, model  
-- from (
-- 	select case d.arkona_store_key when 'RY3' then 'RY1' else d.arkona_store_key end as store, 
-- 		a.stock_number, a.vin, a.boarded_date, b.board_type, b.board_sub_type, 
-- 		c.vehicle_model_year as model_year, c.vehicle_make as make, c.vehicle_model as model
-- 	-- select a.*
-- 	from board.sales_board a
-- 	join board.board_types b on a.board_type_key = b.board_type_key
-- 		and b.board_type = 'Deal'
-- 		and b.board_type_key not in (16)
-- 	join board.daily_board c on a.board_id = c.board_id
-- 		and c.vehicle_make not in ('yamaha','kawasaki','HARLEY','HARLEY DAVIDSON','HARLEY-DAVIDSON','SUZUKI')
-- 		and c.vehicle_type = 'U'
-- 	join onedc.stores d on a.store_key = d.store_key  
-- 	left join ads.ext_vehicle_items e on a.vin = e.vin
-- 	where a.boarded_date between '01/01/2023' and '01/31/2023'
-- 		and not a.is_deleted
-- 		and a.vin <> 'CB3602006227') e
-- group by store,stock_number, vin, boarded_Date, board_type, board_sub_type, model_year, make, model  
-- order by store, board_sub_type;  
-- 
-- 
drop table if exists fs;
create temp table fs as
-- include stocknumber on each row
select store, page, line, line_label, control, sum(unit_count) as unit_count
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 202301-----------------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 202301 ---------------------------------------------------------------------
      and b.page = 16 and b.line between 1 and 14 -- used cars

    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y'
  and right(control, 1) <> 'L') h
group by store, page, line, line_label, control
order by store, page, line;

select c.vehicleinventoryitemid, a.*, b.* 
from fs a
left join board b on a.control = b.stock_number
left join ads.ext_vehicle_inventory_items c on a.control = c.stocknumber
where a.unit_count = 1
order by a.control

select * from fs

select * from ads.get_intra_market_wholesale_prior_stock_number((select vehicleinventoryitemid from ads.ext_vehicle_inventory_items where stocknumber = 'G46199P'))
------- go with board ---------------------------------------------------------------------------------------------

drop table if exists board;
create temp table board as
select board_type_key, store,stock_number, vin, boarded_Date, board_type, board_sub_type, model_year, make, model, trim  
from (
	select d.arkona_store_key as store,
-- 	  case d.arkona_store_key when 'RY3' then 'RY1' else d.arkona_store_key end as store, 
		a.board_type_key, a.stock_number, a.vin, a.boarded_date, b.board_type, b.board_sub_type, 
		c.vehicle_model_year as model_year, c.vehicle_make as make, c.vehicle_model as model, e.trim
	-- select a.*
	from board.sales_board a
	join board.board_types b on a.board_type_key = b.board_type_key
		and b.board_type = 'Deal'
		and b.board_type_key not in (16)
	join board.daily_board c on a.board_id = c.board_id
		and c.vehicle_make not in ('yamaha','kawasaki','HARLEY','HARLEY DAVIDSON','HARLEY-DAVIDSON','SUZUKI')
		and c.vehicle_type = 'U'
	join onedc.stores d on a.store_key = d.store_key  
	left join ads.ext_vehicle_items e on a.vin = e.vin
	where a.boarded_date between '01/01/2023' and '01/31/2023'
		and not a.is_deleted
		and a.vin <> 'CB3602006227') z
group by board_type_key, store,stock_number, vin, boarded_Date, board_type, board_sub_type, model_year, make, model, trim 
order by store, board_sub_type;  

-- addition board_types are useless

-- select aa.*, bb.boarded_date, bb.board_type, bb.board_sub_type
-- from board aa
-- left join (
-- 	select a.*, b.*
-- 	from board.sales_board a
-- 	join board.board_types b on a.board_type_key = b.board_type_key
-- 		and b.board_type = 'Addition'
-- 	where not a.is_deleted) bb on aa.stock_number = bb.stock_number
-- order by right(aa.stock_number, 1), aa.stock_number		
-- 
-- 
-- select aa.*, bb.boarded_date, bb.board_type, bb.board_sub_type
-- from board aa
-- left join (
-- 	select a.*, b.*
-- 	from board.sales_board a
-- 	join board.board_types b on a.board_type_key = b.board_type_key
-- 		and b.board_type = 'Addition'
-- 	where not a.is_deleted) bb on aa.vin = bb.vin
-- order by right(aa.stock_number, 1), aa.stock_number		


select * 
from board a
left join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
where b.stocknumber is null


select * from ads.get_intra_market_wholesale_prior_stock_number('4a85bf54-3043-4b90-8bf1-4bf9358bfe6c')


select * 
from board a
left join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
order by vin

------ 02/09/23 -----------------------------------------------------------------------------------------------------
well, it turns out the board addition types are useles, neither does the board record intra market wholesales
and it turns out intra market sales are now in acounting, rather than just changing stocknumbers

drop table if exists wholesales cascade;
create temp table wholesales as
select a.vehicleinventoryitemid, a.soldamount, a.soldts::date as date_sold, b.fullname
from ads.ext_vehicle_sales a
join ads.ext_organizations b on a.soldto = b.partyid
where soldts::date > '12/31/2022';


select c.vehicleinventoryitemid, a.control, a.line_label, b.vin, b.boarded_date, b.board_type, b.board_sub_type, d.soldamount, d.date_sold, d.fullname, e.vin
-- select a.*
from fs a
left join board b on a.control = b.stock_number
left join ads.ext_vehicle_inventory_items c on a.control = c.stocknumber
left join wholesales d on c.vehicleinventoryitemid = d.vehicleinventoryitemid
left join ads.ext_vehicle_items e on c.vehicleitemid = e.vehicleitemid
where a.unit_count = 1
  and a.control not in ('G45595A','G45835A','G45869X','G45956X','G45870X',
		'G46056X','G45869X','G46057X','G46072X','G46203HB','G46386X','H16087A',
		'T10357GB', 'T10367B')
  and not exists ( -- exclude intra market wholesale
    select 1
    from wholesales
    where vehicleinventoryitemid = c.vehicleinventoryitemid
      and fullname in ('GF-Honda Cartiva','GF-Rydells','GF-Toyota'))
order by a.control



	

           


  G46554H	W04WH3N50HG039533	
    board retail 1/16
    tool not in tool, but retailed as H16087A at GM  ** change the stock# in vii
    dt ry1 retailed to Amiot

  			 
  these 5
	G45956X
	G45870X
	G46056X
	G45869X
	G46057X
					board wholesaled (not intramarket)
					dt wholesaled by gm to toyota
					tool the gm stocknumbers were never entered into the tool;
					none of them have been sold yet

  G46039AB  & G46039ABB whats up with the stocknumbering		

  G46521X  tool  ws to honda 2/1
		       dt does not exist in inventory, does exist in accounting
		       
*** so, not all intramarket deals are done as wholesale deals in dt ***

  G46569T 1GKFK16Z83J196871	
    dt ws toyota to gm as T10357GB on 1/21
    tool retailed as T10357GB at GM to Janich on 1/21
    board retail to janich on 1/21 
    board ws on 1/21 (not intramarket)   

  G45595A  5YFBURHE9HP636869
	         tool stock# not in tool, vin is in tool as T10403G	consigned to toyota    
				   board ws 1/11/23
					 dt RY8 ws to GM as T10403G on 2/8/23
					 dt ry1 pending deal as G46700T 2/9/23

  G45835A	5TFDY5F1XMX990255 
    board ws 1/11/23
    tool sold at GM as T10392G on 2/8/23
    dt RY8 ws to GM as T10392G on 2/7/23
    dt RY1 deal to ??? 1/11 & sold to gm as G46669T an 2/7/23

  G45869X  2T3P1RFV6MW142325
    board ws 1/11/23
    tool ws to Adesa on 2/9/23 as T10397G
    dt RY1 ws to toyota 1/11/23
		dt RY8 T10397G in inventory

  G45870X 2T3P1RFVXMW172735
    board ws 1/11/23
    tool consigned to toyota as T10399G in inventory 
		dt ry8 T10399G in inventory
		dt ry1 ws to toyota on 1/11/23

*** these consigned vehicles with toyota stocknumbers should have location changed in the tool ***

  G46072X  JTMA1RFV3KD503820
    board ws 1/11
    tool consigned to toyota as T10394G in inventory
    dt ry8 in inventory as T10394G
    dt ry1 ws to toyota 1/11/23

  G46203HB 4T1B11HK3JU054092
    board ws 1/11
    tool consigned to toyota s T10402G in inventory
    dt ry8 in inventory as T10402G
    dt ry1 ws to toyota (sort of, shows in ui as service customer, comments show capped 1/11)
    
  G46386X 5TDGZRBH0MS528765
    board ws 1/11
    tool owned by toyota as T10404G
    dt ry1 ws to toyota (sort of, shows in ui as service customer, comments show capped 1/11)

  G46554H W04WH3N50HG039533
    board retail 1/16
    tool retail at GM as H16087A to Amiot, no record of G46554H
    dt RY1 retailed at GM on 1/16 as G46554H to Amiot
	  *** change stocknumber in tool from H16087A to G46554H ***
-- UPDATE ads.ext_Vehicle_Inventory_Items
-- SET stocknumber = 'G46554H'
-- -- select * from ads.ext_Vehicle_Inventory_Items
-- WHERE VehicleInventoryItemID = '6bf453fb-476c-4679-b8d2-d8a49b41b960'

	G46569T  1GKFK16Z83J196871
    board retail 1/21
    tool sold retail at GM to Janich on 1/21 as T10357GB
		dt RY1 retailed to Janich on 1/21
    dt ry8 wholesaled to GM
-- 	  *** change stocknumber in tool from T10357GB to G46569T ***    
-- UPDATE ads.ext_Vehicle_Inventory_Items
-- SET stocknumber = 'G46569T'
-- -- select * from ads.ext_Vehicle_Inventory_Items
-- WHERE VehicleInventoryItemID = '9c0e38d8-7743-47d4-ac2d-e1e01f87f720'

    H16087A  
    boarded as intercompany on 1/16  
	  dt stocknumber does not exist , but retailed at GM on 1/16 as G46554H to Amiot
  	tool retailed on 1/17/23 to Amiot  vin W04WH3N50HG039533
  	inpmast, use to exist, no longer does (not in ext, but 2 rows in xfm)
					 
    H16173P  4S3BH665227662003
    board boarded only as a purchase
    tool wholesaled at Honda to schwan
    dt ry1 ws to schwan 1/25

    H16219G  1N6AA1ED1LN502510
    board retailed at honda on 1/6
    tool stock number does not exists
         ws as G43567A to honda on
         in tool as H16210G
    dt ry1 changed stocknumber and retailed on 1/6
-- 	  *** change stocknumber in tool from H16210G to H16219G ***    
-- UPDATE ads.ext_Vehicle_Inventory_Items
-- SET stocknumber = 'H16219G'
-- -- select * from ads.ext_Vehicle_Inventory_Items
-- WHERE VehicleInventoryItemID = '7b7a16f4-3618-4b80-aa07-68af0b57c90c'

	  T10355X  5TDKRKEC0MS046220
	  board not boarded
	  tool and dt show retail in jan
    dt ry8 retail to Duarte on 1/31

    T10357GB  1GKFK16Z83J196871
    board ws 1/21
    tool stocknumber does not exist
         vin exists as G46569T retailed at GMto Jaich
    dt ry8 wholesale to GM 
         
		T10367B
		board interco ws
		tool see G46555T/G46555TT
		dt ry8 wholesale to GM 1/16, vin :3FA6P0K90DR215469
		dt RY1 wholeslae to toyota









    
             		 
select * from board.sales_board where stock_number = 'G43567A'	

select * from arkona.xfm_inpmast where inpmast_stock_number = 'H16087A'
select * from arkona.ext_inpmast where inpmast_stock_number = 'H16087A'

select * from arkona.ext_inpmast where inpmast_stock_number in ('G46056X','G45869X','G46057X','G45956X','G45870X')