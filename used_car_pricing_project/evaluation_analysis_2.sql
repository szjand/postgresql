﻿/* 
10/30/22
continuation of used_car_pricing_prooject\evaluation_analysis.sql
added black book values, acv, finished, writeback
sent as evals_scoring_2.xlsx
limited to 45 days of evaluations, booked only and chrome_style count = 1
included a second worksheet with chrome mapping for bb & kbb
*/
select a.store, a.stock_number, a.vin, miles, a.eval_date, a.consultant, a.evaluator, a.mech, a.body, a.appearance, a.tires, a.inspection,
	a.mech+a.body+a.appearance+a.tires+a.inspection as total_eval_recon_est, e.amount::integer as eval_acv, f.amount::integer as eval_finished, 
	j.cleanbase as clean_base, j.cleanmileageadjustment as clean_adj,j.averagebase as avg_base, j.averagemileageadjustment as avg_adj, j.roughbase as rough_base,j.roughmileageadjustment as rough_adj, 
	j.regionaladjustment as region_adj, j.addsdeductstotal as add_ded,
	j.cleanbase+j.cleanmileageadjustment+j.regionaladjustment+j.addsdeductstotal as clean_total,
	j.averagebase+j.averagemileageadjustment+j.regionaladjustment+j.addsdeductstotal as avg_total,
	j.roughbase+j.roughmileageadjustment+j.addsdeductstotal as rough_total,
	a.manheim,
	a.walk_date, g.amount::integer as walk_acv, h.amount::integer as walk_finished, i.amount::integer as write_back, b.best_price as first_price, c.best_price as last_price, 
	a.sales_status, coalesce(d.act_insp, 0) as act_insp, coalesce(d.act_mech, 0) as act_mech, coalesce(d.act_bs, 0) as act_bs, coalesce(d.act_detail, 0) as act_detail,
	coalesce(d.act_insp, 0)+coalesce(d.act_mech, 0)+coalesce(d.act_bs, 0)+coalesce(d.act_detail, 0) as total_recon
from pp.evals_1 a
join chr.build_data_describe_vehicle aa on a.vin = aa.vin
left join ( -- first price
    select a.vehicleinventoryitemid, b.amount::integer as best_price, vehiclepricingts,
        row_number() over (partition by a.vehicleinventoryitemid order by vehiclepricingts asc) as seq
    from ads.ext_vehicle_pricings a
    join ads.ext_vehicle_pricing_details b on a.vehiclepricingid = b.vehiclepricingid
        and b.typ = 'VehiclePricingDetail_BestPrice'
    join pp.evals_1 c on a.vehicleinventoryitemid = c.vehicleinventoryitemid) b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
        and b.seq = 1
left join ( -- last price
    select a.vehicleinventoryitemid, b.amount::integer as best_price, vehiclepricingts,
        row_number() over (partition by a.vehicleinventoryitemid order by vehiclepricingts desc) as seq
    from ads.ext_vehicle_pricings a
    join ads.ext_vehicle_pricing_details b on a.vehiclepricingid = b.vehiclepricingid
        and b.typ = 'VehiclePricingDetail_BestPrice'
    join pp.evals_1 c on a.vehicleinventoryitemid = c.vehicleinventoryitemid) c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
        and c.seq = 1
left join (
    select vehicleinventoryitemid, 
        sum(amount) filter (where department = 'Inspection')::integer as act_insp,
        sum(amount) filter (where department = 'Mech')::integer as act_mech,
        sum(amount) filter (where department = 'Body Shop')::integer as act_bs,
        sum(amount) filter (where department = 'Detail')::integer as act_detail
    from pp.recon_ros_2
    group by vehicleinventoryitemid) d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
left join ads.ext_vehicle_price_components e on a.vehicleevaluationid = e.tablekey
  and e.typ = 'VehiclePriceComponent_ACV'
  and e.thruts is null
left join ads.ext_vehicle_price_components f on a.vehicleevaluationid = f.tablekey
  and f.typ = 'VehiclePriceComponent_Finished'  
  and f.thruts is null
left join ads.ext_vehicle_price_components g on a.vehicle_walk_id::citext = g.tablekey
  and g.typ = 'VehiclePriceComponent_ACV'
  and g.thruts is null
left join ads.ext_vehicle_price_components h on a.vehicle_walk_id::citext = h.tablekey
  and h.typ = 'VehiclePriceComponent_Finished'  
  and h.thruts is null  
left join ads.ext_vehicle_price_components i on a.vehicle_walk_id::citext = i.tablekey
  and i.typ = 'VehiclePriceComponent_WriteBack'  
  and i.thruts is null  
left join ads.ext_black_book_values j on a.vehicleevaluationid = j.tablekey  
where eval_status = 'booked'    
  and aa.style_count = 1
order by a.vin -- a.eval_status, a.sales_status

-- 
-- mapping data
select distinct a.vin, -- b.source, b.style_count,
	c->>'id' as chrome_style_id, d.uvc, e.option_code, e.uoc, e.add_deduct, f.vehicle_id, g.vehicle_option_id, g.option_code, g.add_deduct, g.option_display_name
from pp.evals_1 a
join chr.build_data_describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'style') as c on true
left join chr.black_book_vehicle_map d on c->>'id' = d.style_id
left join chr.black_book_required_options e on d.mapping_id = e.mapping_id
left join chr.kelley_blue_book_vehicle_map f on c->>'id' = f.style_id
left join chr.kelley_blue_book_required_options g on d.mapping_id = g.mapping_id
where a.eval_status = 'booked'	
  and b.style_count = 1
order by a.vin  

select a.vin, f.*, g.* 
from pp.evals_1 a
join chr.build_data_describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'style') as c on true
-- left join chr.black_book_vehicle_map d on c->>'id' = d.style_id
-- left join chr.black_book_required_options e on d.mapping_id = e.mapping_id
left join chr.kelley_blue_book_vehicle_map f on c->>'id' = f.style_id
left join chr.kelley_blue_book_required_options g on f.mapping_id = g.mapping_id
where a.vin = '3FA6P0H75FR144214'
