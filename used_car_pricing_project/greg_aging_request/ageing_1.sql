﻿Lisa is concerned about aged vehicles blocking flow. 
At the same time Ben K is wanting to reclassify an aged unit from 30 days to 60 days available. 
@jon (they/them)
 can you generate the data that shows how many used cars we sell in time buckets and their gross please? 
 Time buckets 
	for available: 0-30, 30-60, 60-90, over 90 
	for retail sales. 
	Would also want a bucket for we made a vehicle available but ultimately wholesaled it. 
	Would also like to report based on days owned: 0-30, 30-60, 60-90, 90+.   
	Also need stats on vehicles sold before available. Last 365 days for the data period.



drop table if exists board;
create temp table board as
select board_type_key, store,stock_number, vin, boarded_Date, board_type, board_sub_type, model_year, make, model, trim
from (
	select -- d.arkona_store_key as store,
	  case d.arkona_store_key when 'RY3' then 'RY1' else d.arkona_store_key end as store, -- RY3 is outlet
		a.board_type_key, a.stock_number, a.vin, a.boarded_date, b.board_type, b.board_sub_type,
		c.vehicle_model_year as model_year, c.vehicle_make as make, c.vehicle_model as model, e.trim
	-- select a.*
	from board.sales_board a
	join board.board_types b on a.board_type_key = b.board_type_key
		and b.board_type = 'Deal'
-- 		and b.board_type_key not in (16)  -- Intercompany Wholesale
	join board.daily_board c on a.board_id = c.board_id
		and c.vehicle_make not in ('yamaha','kawasaki','HARLEY','HARLEY DAVIDSON','HARLEY-DAVIDSON','SUZUKI')
		and c.vehicle_type = 'U'
	join onedc.stores d on a.store_key = d.store_key
	left join ads.ext_vehicle_items e on a.vin = e.vin
	where a.boarded_date between '05/01/2022' and '04/30/2023'
		and not a.is_deleted
		and a.vin <> 'CB3602006227') z
group by board_type_key, store,stock_number, vin, boarded_Date, board_type, board_sub_type, model_year, make, model, trim
order by store, board_sub_type;


select * from board.board_types order by board_type_key

select * from onedc.stores


select store, year_month, board_sub_type, count(*)
from (
	select *, (select distinct year_month from dds.dim_date where the_date = a.boarded_Date) as year_month
	from board a) b
group by store, year_month, board_sub_type
order by year_month, store, board_sub_type


---------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------

drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
insert into step_1
select year_month, store, page, line, line_label, control, sum(unit_count) as unit_count
from (
  select b.year_month, --------------------------
    d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 202304-----------------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 202304 ---------------------------------------------------------------------
      and b.page = 16 and b.line between 1 and 14 -- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by year_month, store, page, line, line_label, control
order by year_month, store, page, line;


-- by store, page
-- total numbers are right on
select store, year_month, sum(unit_count)
from (
  select *
  from step_1) a
group by store, year_month
order by store, year_month

select * from step_1 where store = 'ry1'

!!!!!!!!!!!!!!!!!  exclude L vehicles !!!!!!!!!!!!!!!!!!!!!!!!!

select store, year_month, control
from step_1
where store = 'ry1'
group by store, year_month, control
having sum(unit_count) > 0


select year_month, count(*)
from (
	select store, year_month, control
	from step_1
	where store = 'ry1'
	group by store, year_month, control
	having sum(unit_count) = 1) a
group by year_month

select * from step_1 where store = 'ry1' and year_month = 202205 and unit_count <> 1

select * from step_1 where store = 'ry1' and year_month = 202205
and control in ( 
'G42335PD',
'G44519G',
'G43984A',
'G44601P',
'G43716A',
'G44292A',
'G44374P')
order by control


select * from step_1 where control = 'G44601P'

select * from step_1 where right(control, 1) = 'L' and store = 'ry1' and year_month = 202205


board: G43987C, G44415PA, G44461L, G44539GA sold & unwound in may
       G44292A, G43716A boarded in april

select * 
from (
	select * 
	from (
		select *, (select distinct year_month from dds.dim_date where the_date = a.boarded_Date) as year_month
		from board a) b
	where year_month = 202205  
		and store = 'ry1') aa
full outer join (
  select * 
  from step_1
  where store = 'ry1' 
    and year_month = 202205) bb on aa.stock_number = bb.control
	