
RETURN (  
SELECT 
  CASE
    WHEN MechStatus = 'No Open Items' AND BodyStatus = 'No Open Items' AND AppearanceStatus = 'No Open Items' THEN 
      'All Recon Complete'
    ELSE 
      '<B>Mech:</B> ' + TRIM(MechStatus)+ '</br>   <B>Body:</B> ' + TRIM(BodyStatus) + '</br>   <B>Appearance:</B> ' + TRIM(AppearanceStatus)
    END   
  FROM (  
    SELECT  
      (SELECT 
        CASE status
          WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
          WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
          WHEN 'MechanicalReconProcess_NotStarted' THEN --'Not Started'
            CASE 
              WHEN EXISTS (
                SELECT 1 
                FROM VehicleInventoryItemStatuses 
                WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
                AND category = 'MechanicalReconDispatched'
                AND ThruTS IS NULL) THEN 'Dispatched'
              ELSE 'Not Started'
            END 
        END         
        FROM VehicleInventoryItemStatuses
        WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
        AND category = 'MechanicalReconProcess'
        AND ThruTS IS NULL) AS MechStatus,
      (SELECT 
        CASE status
          WHEN 'BodyReconProcess_InProcess' THEN 'WIP'
          WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
          WHEN 'BodyReconProcess_NotStarted' THEN --'Not Started'
            CASE 
              WHEN EXISTS (
                SELECT 1 
                FROM VehicleInventoryItemStatuses 
                WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
                AND category = 'BodyReconDispatched'
                AND ThruTS IS NULL) THEN 'Dispatched'
              ELSE 'Not Started'
            END           
        END         
        FROM VehicleInventoryItemStatuses
        WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
        AND category = 'BodyReconProcess'
        AND ThruTS IS NULL) AS BodyStatus,
      (SELECT 
        CASE status
          WHEN 'AppearanceReconProcess_InProcess' THEN 'WIP'
          WHEN 'AppearanceReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
          WHEN 'AppearanceReconProcess_NotStarted' THEN --'Not Started'
            CASE 
              WHEN EXISTS (
                SELECT 1 
                FROM VehicleInventoryItemStatuses 
                WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
                AND category = 'AppearanceReconDispatched'
                AND ThruTS IS NULL) THEN 'Dispatched'
              ELSE 'Not Started'
            END           
        END         
        FROM VehicleInventoryItemStatuses
        WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
        AND category = 'AppearanceReconProcess'
        AND ThruTS IS NULL) AS AppearanceStatus
    FROM VehicleInventoryItems viix
    WHERE VehicleInventoryItemID = @VehicleInventoryItemID) wtf);
  
  

  