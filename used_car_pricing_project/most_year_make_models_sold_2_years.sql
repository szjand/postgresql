﻿drop table if exists pp.stock_numbers_2_years cascade;
create table pp.stock_numbers_2_years as  -- 6118
select year_month, control as stock_number
from (
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 202010 and 202210-----------------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month between 202010 and 202210  ---------------------------------------------------------------------
      and b.page = 16 and b.line between 1 and 6 -- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by year_month, control;
create index on pp.stock_numbers_2_years(stock_number);

select * from pp.stock_numbers_2_years limit 10

drop table if exists pp.vehicles_sold_2_years cascade;
create table pp.vehicles_sold_2_years as
select a.year_month, c.yearmodel, c.make, c.model
from pp.stock_numbers_2_years a
join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid;

-- need to figure an age for vehicles at the month sold
select * 
from pp.vehicles_sold_2_years

i am doing some mileage playing.
i want to establish the age of vehicles when sold
so, if we sell a 2018 eqiuinox in september of 2022, what is the age of that vehicle at the time sold?
formula please?

birthdate is september of the year prior to model_year

select make, model, age_in_mos, count(*)
from (
	select *, round(age_in_days/365.0, 1) as age_in_yrs, (age_in_days/30.417)::integer as age_in_mos
	from (
		select a.*, 
			to_date(lpad((select distinct month_of_year from dds.dim_date where year_month = a.year_month)::text, '2', '0')
				||'01'||((select distinct the_year from dds.dim_date where year_month = a.year_month)::text),'MMDDYYYY')
			-
			to_date('09'||'01'||(a.yearmodel::integer - 1)::text,'MMDDYYYY') as age_in_days
		from pp.vehicles_sold_2_years a) b) c
group by make, model, age_in_mos
order by count(*) desc		

-- a total of 1464 make/model/age_in_years
select make, model, age_in_yrs::integer, array_agg(distinct age_in_mos) as age_in_months, 
  count(*) as the_count, 
  round(100 * count(*)/5727.0, 2) as perc_of_total,
  row_number() over(order by count(*) desc) as seq
from (
	select *, round(age_in_days/365.0, 1) as age_in_yrs, (age_in_days/30.417)::integer as age_in_mos
	from (
		select a.*, 
			to_date(lpad((select distinct month_of_year from dds.dim_date where year_month = a.year_month)::text, '2', '0')
				||'01'||((select distinct the_year from dds.dim_date where year_month = a.year_month)::text),'MMDDYYYY')
			-
			to_date('09'||'01'||(a.yearmodel::integer - 1)::text,'MMDDYYYY') as age_in_days
		from pp.vehicles_sold_2_years a) b) c
group by make, model, age_in_yrs::integer
limit 20


	
select round(100 * (sum(the_count) filter (where seq < 11))/sum(the_count), 1) as top_ten_perc, 
  round(100 * (sum(the_count) filter (where seq < 21))/sum(the_count), 1) as top_twenty_perc,
  sum(the_count) filter (where seq < 11) as top_ten,
  sum(the_count) filter (where seq < 21) as top_twenty, sum(the_count) as total
from (
	select make, model, age_in_yrs::integer, array_agg(distinct age_in_mos), count(*) as the_count, row_number() over(order by count(*) desc) as seq
	from (
		select *, round(age_in_days/365.0, 1) as age_in_yrs, (age_in_days/30.417)::integer as age_in_mos
		from (
			select a.*, 
				to_date(lpad((select distinct month_of_year from dds.dim_date where year_month = a.year_month)::text, '2', '0')
					||'01'||((select distinct the_year from dds.dim_date where year_month = a.year_month)::text),'MMDDYYYY')
				-
				to_date('09'||'01'||(a.yearmodel::integer - 1)::text,'MMDDYYYY') as age_in_days
			from pp.vehicles_sold_2_years a) b) c
	group by make, model, age_in_yrs::integer) d


-- percentage of total sales

select * from pp.vehicles_sold_2_years limit 10

select count(*) from pp.vehicles_sold_2_years


-- 11/16/22 lisa 

select * from ads.ext_vehicle_items where yearmodel::integer > 2019 limit 50

select * from ads.ext_vehicle_inventory_items limit 100

select * from pp.vehicles_sold_2_years_lisa limit 10

drop table if exists pp.vehicles_sold_2_years_lisa cascade;
create table pp.vehicles_sold_2_years_lisa as
select * 
from (
	select b.vehicleinventoryitemid, c.vehicleitemid, b.stocknumber,
		case
			when b.stocknumber like 'G%' then 'GM'
			when b.stocknumber like 'H%' then 'HN'
			when b.stocknumber like 'T%' then 'TOY'
		end as store, a.year_month, c.yearmodel, c.make, c.model, c.bodystyle, c.trim,
		b.fromts::date as from_date, b.thruts::date as thru_date
	from pp.stock_numbers_2_years a
	join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
		and right(trim(b.stocknumber), 1) <> 'G'
	join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid) d
where store is not null

drop table if exists pp.vehicles_sold_2_years_lisa_2 cascade;
create table pp.vehicles_sold_2_years_lisa_2 as
select a.*, d.amount as price, e.value as miles
from pp.vehicles_sold_2_years_lisa a
left join (
	select vehicleinventoryitemid, vehiclepricingid
	from ads.ext_vehicle_pricings aa
	where vehiclepricingts = (
		select max(vehiclepricingts)
		from ads.ext_Vehicle_pricings
		where vehicleinventoryitemid = aa.vehicleinventoryitemid)) c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
left join ads.ext_vehicle_pricing_details d on c.vehiclepricingid = d.vehiclepricingid
  and d.typ = 'VehiclePricingDetail_BestPrice'
left join (
	select vehicleitemid, value
	from ads.ext_vehicle_item_mileages bb
	where vehicleitemmileagets = (
		select max(vehicleitemmileagets)
		from ads.ext_vehicle_item_mileages
		where vehicleitemid = bb.vehicleitemid)) e on a.vehicleitemid = e.vehicleitemid		


select * from  pp.vehicles_sold_2_years_lisa_2 limi		


select a.bopmast_stock_number, sale_type, a.date_capped, bopmast_vin, b.make, b.year, b.model 
from arkona.ext_bopmast a
join arkona.ext_inpmast b on a.bopmast_vin = b.inpmast_vin
  and b.model = 'equinox'
  and b.year > '2017'
where a.date_capped > '08/31/2022'  
  and a.vehicle_type = 'U'
  and a.sale_type <> 'W'
order by date_capped


select *, sum(sales) over (order by the_date rows between 14 preceding and current row)
from (
	select  a.the_date, coalesce(sales, 0) as sales
	from dds.dim_date a
	left join (
	select thru_date, count(stocknumber) as sales
	from pp.vehicles_sold_2_years_lisa_2 a 
	where thru_date between current_date - ('6 months')::interval and current_date
	and yearmodel in ('2018','2019', '2020', '2021')
	and model = 'equinox'
	group by thru_date
	) b on a.the_date = b.thru_date
	where the_date between current_date - ('6 months')::interval and current_date
	and day_of_week <> 1) x
order by the_date asc



select a.stock_number, the_date, status, b.make, b.model, sale_date
-- select distinct c.status
from greg.uc_base_vehicles a
join ads.ext_vehicle_items b on a.vehicle_item_id = b.vehicleitemid
  and b.yearmodel in ('2020','2021','2022')
  and b.make = 'chevrolet'
  and b.model like 'silverado 15%'
join greg.uc_base_vehicle_statuses c on a.vehicle_inventory_item_id = c.vehicle_inventory_item_id
where the_date > current_date - 180
order by the_date

select a.*, b.shape, b.size
from  pp.vehicles_sold_2_years_lisa_2 a 
join veh.shape_size_classifications b on a.make = b.make and a.model = b.model
where shape = 'pickup'
  and size = 'large'
  and price > 15000
  and thru_date > '07/31/2022'
  and a.make in ('chevrolet','gmc','buick','cadillac')
order by price  


select distinct status from ads.ext_vehicle_inventory_item_statuses where status like '%avail%'
-- from afton
select a.*, b.shape, b.size, thru_date - from_date as age, c.fromts::date as on_lot
from  pp.vehicles_sold_2_years_lisa_2 a 
join veh.shape_size_classifications b on a.make = b.make and a.model = b.model
left join ads.ext_vehicle_inventory_item_statuses c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
  and status = 'RMFlagAV_Available'
where shape = 'pickup'
  and size = 'large'
  and price > 15000
  and thru_date > '07/31/2022'
  and a.make in ('Chevrolet','Buick', 'GMC', 'Cadillac')
order by price