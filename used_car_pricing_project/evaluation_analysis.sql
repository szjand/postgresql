﻿-- evaluation analysis --
select * from ads.ext_vehicle_evaluations limit 2
select * from ads.ext_people limit 2
select distinct typ from ads.ext_vehicle_third_party_values
VehicleThirdPartyValue_Manheim
VehicleThirdPartyValue_BBACVAppeal
VehicleThirdPartyValue_BBACVDesirability
VehicleThirdPartyValue_KelleyBlueBookFair
VehicleThirdPartyValue_KelleyBlueBookGood

select * from ads.ext_Vehicle_recon_items limit 100

create index on ads.ext_vehicle_third_party_values(typ);
create index on ads.ext_black_book_values(tablekey);


select * from ads.ext_black_book_values limit 20


-- first and most recent price
-- current status
-- current recon status
-- actual recon
-- vehicle attributes:  coalesce(build_data, describe_vehicle, vehicle_items)
-- do a separate table for vehicle attributes
-- vehicle stuff is a distraction right now, need the basic evaluation scoring elements,
-- so, prices, recon statuses, recon, sales status
-- walk feedback


drop table if exists pp.evals_1 cascade;
create table pp.evals_1 as
select a.vehicleevaluationid, m.vehicle_walk_id, d.vehicleinventoryitemid, c.vehicleitemid, a.vehicleevaluationts::date as eval_date, 
    e.description as eval_status, m.vehicle_walk_ts::date as walk_date,
    case b.fullname
        when 'GF-Rydells' then 'GM'
        when 'GF-Honda Cartiva' then 'Honda Nissan'
        when 'GF-Toyota' THen 'Toyota'
        else 'Bumfuck Egypt'
    end as store, c.vin, d.stocknumber as stock_number,
    f.description as acq_type, g.lastname || ', ' || g.firstname as consultant, 
    h.lastname || ', ' || h.firstname as evaluator, 
    a.reconmechanicalamount::integer as mech, reconbodyamount::integer as body, reconappearanceamount::integer appearance,
    recontireamount::integer as tires, reconinspectionamount::integer as inspection, amountshown::integer as shown,
    i.value as miles,
    l.amount as manheim,
    (select * from ads.get_vii_sales_status(d.vehicleinventoryitemid)) as sales_status,
    (select mechanical from ads.get_current_recon_status_by_vehicleinventoryitemid(d.vehicleinventoryitemid)) as mech_status,
    (select body from ads.get_current_recon_status_by_vehicleinventoryitemid(d.vehicleinventoryitemid)) as body_status,
    (select appearance from ads.get_current_recon_status_by_vehicleinventoryitemid(d.vehicleinventoryitemid)) as appear_status,
    o.notes as walk_feedback,  n.fullname as walker
--   j.source, j.style_count as build_count, k.style_count as desc_count
from ads.ext_vehicle_evaluations a
join ads.ext_organizations b on a.locationid = b.partyid
left join ads.ext_vehicle_items c on a.vehicleitemid = c.vehicleitemid
left join ads.ext_vehicle_inventory_items d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
left join ads.ext_status_descriptions e on a.currentstatus = e.status
left join ads.ext_typ_descriptions f on a.typ = f.typ
left join ads.ext_people g on a.salesconsultantid = g.partyid
left join ads.ext_people h on a.vehicleevaluatorid = h.partyid
left join ads.ext_vehicle_item_mileages i on a.vehicleevaluationid = i.tablekey
left join ads.ext_vehicle_third_party_values l on a.vehicleevaluationid = l.tablekey
    and l.typ = 'VehicleThirdPartyValue_Manheim'
left join ads.ext_vehicle_walks m on d.vehicleinventoryitemid = m.vehicle_inventory_item_id::citext 
left join ads.ext_people n on m.vehicle_walker_id::citext = n.partyid
left join ads.ext_vehicle_inventory_item_notes o on d.vehicleinventoryitemid = o.vehicleinventoryitemid
    and o.subcategory = 'VehicleWalk_EvaluatorFeedback'
where vehicleevaluationts::date > current_date - interval '45 days' -- repopulated 10/29 with this date range
order by eval_status, sales_status;
create index on pp.evals_1(vehicleinventoryitemid);
comment on table pp.evals_1 is 'base table for evaluations for scoring evaluations as part of the pricing project';


select * 
from pp.evals_1 aa


-- add prices (1st and most recent)

select a.*, b.best_price as first_price, c.best_price as last_price
from pp.evals_1 a
left join ( -- first price
    select a.vehicleinventoryitemid, b.amount::integer as best_price, vehiclepricingts,
        row_number() over (partition by a.vehicleinventoryitemid order by vehiclepricingts asc) as seq
    from ads.ext_vehicle_pricings a
    join ads.ext_vehicle_pricing_details b on a.vehiclepricingid = b.vehiclepricingid
        and b.typ = 'VehiclePricingDetail_BestPrice'
    join pp.evals_1 c on a.vehicleinventoryitemid = c.vehicleinventoryitemid) b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
        and b.seq = 1
left join ( -- last price
    select a.vehicleinventoryitemid, b.amount::integer as best_price, vehiclepricingts,
        row_number() over (partition by a.vehicleinventoryitemid order by vehiclepricingts desc) as seq
    from ads.ext_vehicle_pricings a
    join ads.ext_vehicle_pricing_details b on a.vehiclepricingid = b.vehiclepricingid
        and b.typ = 'VehiclePricingDetail_BestPrice'
    join pp.evals_1 c on a.vehicleinventoryitemid = c.vehicleinventoryitemid) c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
        and c.seq = 1
order by a.eval_status, a.sales_status        


-- actual recon
-- this gets me stock#, ro & total $$ per ro
-- store from account doesn't work, eg h15856a, derive store from stock_number later if needed
-- shit, i believe this gives me labor only, wrong, looks like it includes parts
drop table if exists pp.recon_ros_1 cascade;
create table pp.recon_ros_1 as
select a.control as stock_number, b.vehicleinventoryitemid, a.ref as ro, sum(a.amount) as amount
from fin.fact_gl a
join pp.evals_1 b on a.control = b.stock_number
join fin.dim_journal c on a.journal_key = c.journal_key
  and c.journal_code in ('SVI','SWA','SCA','POT')
join fin.dim_account d on a.account_key = d.account_key
where a.post_status = 'Y'
    and exists (-- eliminate non ro transactions (eg move coaches car to uc)
        select 1
        from dds.fact_repair_order
        where ro = a.ref)
group by a.control, b.vehicleinventoryitemid, a.ref;    
comment on table pp.recon_ros_1 is 'derive recon ros from accounting transactions';
create unique index on pp.recon_ros_1(ro);

-- now that we have the ro, need to categorize the actual ros to dept
-- 
drop table if exists  pp.recon_ros_2 cascade;
create table pp.recon_ros_2 as
select 
    case left(a.stock_number, 1)
      when 'G' then 'RY1'
      when 'H' then 'RY2'
      when 'T' then 'RY8'
    end as store, a.stock_number, a.vehicleinventoryitemid, a.ro, a.amount, 
    /*b.ro_labor_sales, b.ro_parts_sales, b.ro_shop_supplies, b.ro_paint_materials,*/ d.writer_name, 
    array_agg(distinct c.opcode order by c.opcode) as opcode, array_agg(distinct c.description) as opcode_desc,
    case
      when left(a.ro, 2) = '18' and length(a.ro) = 8 then 'Body Shop'::citext
      when d.writer_name = 'BEAR, JEFF' then 'Inspection'
      when d.writer_name = 'Detail Department' then 'Detail'
      else null::citext
    end as department
from pp.recon_ros_1 a
join dds.fact_repair_order b on a.ro = b.ro 
join dds.dim_opcode c on b.opcode_key = c.opcode_key
join dds.dim_service_writer d on b.service_Writer_key = d.service_writer_key
group by a.stock_number, a.vehicleinventoryitemid, a.ro, a.amount, b.ro_labor_sales, b.ro_parts_sales, b.ro_shop_supplies, b.ro_paint_materials, d.writer_name;
comment on table pp.recon_ros_2 is 'categorization of recon ros';
create unique index on pp.recon_ros_2(ro);

update pp.recon_ros_2
set department = 'Inspection'
where department is null
  and 'ADMIN'::citext = any(opcode);
  
update pp.recon_ros_2
set department = 'Inspection'
where department is null
    and (
        'ASIS'::citext = any(opcode)
        or
        'NICE'::citext = any(opcode)
        or
        'CERT'::citext = any(opcode));
        
update pp.recon_ros_2
set department = 'Mech'
where department is null;

select * from pp.recon_ros_2

select vehicleinventoryitemid, department, sum(amount)::integer
from pp.recon_ros_2
group by vehicleinventoryitemid, department


-- ok, first wide spreadsheet to submit for greg to consider: eval_scoring_1.xlsx
select distinct department from pp.recon_ros_2

select a.*, b.best_price as first_price, c.best_price as last_price, d.act_insp, d.act_mech, d.act_bs, d.act_detail
from pp.evals_1 a
left join ( -- first price
    select a.vehicleinventoryitemid, b.amount::integer as best_price, vehiclepricingts,
        row_number() over (partition by a.vehicleinventoryitemid order by vehiclepricingts asc) as seq
    from ads.ext_vehicle_pricings a
    join ads.ext_vehicle_pricing_details b on a.vehiclepricingid = b.vehiclepricingid
        and b.typ = 'VehiclePricingDetail_BestPrice'
    join pp.evals_1 c on a.vehicleinventoryitemid = c.vehicleinventoryitemid) b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
        and b.seq = 1
left join ( -- last price
    select a.vehicleinventoryitemid, b.amount::integer as best_price, vehiclepricingts,
        row_number() over (partition by a.vehicleinventoryitemid order by vehiclepricingts desc) as seq
    from ads.ext_vehicle_pricings a
    join ads.ext_vehicle_pricing_details b on a.vehiclepricingid = b.vehiclepricingid
        and b.typ = 'VehiclePricingDetail_BestPrice'
    join pp.evals_1 c on a.vehicleinventoryitemid = c.vehicleinventoryitemid) c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
        and c.seq = 1
left join (
    select vehicleinventoryitemid, 
        sum(amount) filter (where department = 'Inspection') as act_insp,
        sum(amount) filter (where department = 'Mech') as act_mech,
        sum(amount) filter (where department = 'Body Shop') as act_bs,
        sum(amount) filter (where department = 'Detail') as act_detail
    from pp.recon_ros_2
    group by vehicleinventoryitemid) d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
order by a.evaluator, a.eval_status, a.sales_status


---------------------------------------------------------------------------------------------------------------
--< 10/20/22  request bb and manheim values
---------------------------------------------------------------------------------------------------------------
-- price components
select * 
from ads.ext_vehicle_price_components
where fromts::date > current_date - 30
  and thruts is null
  and basistable = 'VehicleWalks'
  and vehicleinventoryitemid is not null
limit 200

--< chrome -----------------------------------------------------------------------------------
i think first, i want to add a chrome_style_id, and then incorporate the bb & kbb mapping

CREATE TABLE ads.ext_black_book_resolver ( 
      vehicleitemid citext primary key,
      groupnumber citext,
      vin8 citext,
      vinyear citext,
      uvc citext,
      expiredts timestamp);
      
select * from pp.evals_1
-- get rid of the non build records NO, these are used cars, if build data was available, we would already have it
-- 175 
select distinct a.vin
from pp.evals_1 a
where a.eval_status = 'booked'
	and not exists (
		select 1 
		from chr.build_data_describe_vehicle 
		where vin = a.vin)
and a.stock_number is null	

select * from chr.black_book_vehicle_map limit 20
select * from chr.black_book_required_options limit 20

-- 375 rows
-- black book mapping
select a.vin, b.source, b.style_count,
	c->>'id' as chrome_style_id, d.*, e.*, f.groupnumber, f.uvc
from pp.evals_1 a
join chr.build_data_describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'style') as c on true
left join chr.black_book_vehicle_map d on c->>'id' = d.style_id
left join chr.black_book_required_options e on d.mapping_id = e.mapping_id
left join ads.ext_black_book_resolver f on a.vehicleitemid = f.vehicleitemid -- not all tool vehicles bb resolved
where a.eval_status = 'booked'	
  and b.style_count = 1
order by a.vin  

-- kbb mapping
select a.vin, b.source, b.style_count,
	c->>'id' as chrome_style_id, d.*, e.*
from pp.evals_1 a
join chr.build_data_describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'style') as c on true
left join chr.kelley_blue_book_vehicle_map d on c->>'id' = d.style_id
left join chr.kelley_blue_book_required_options e on d.mapping_id = e.mapping_id
where a.vin = '1GCVKREC6EZ180630'
where a.eval_status = 'booked'	
  and b.style_count = 1
order by a.vin    
  
--/> chrome -----------------------------------------------------------------------------------

--< black book -----------------------------------------------------------------------------------




with all these values, limit this dataset to booked evals

wtf, only up till 5/13/22, actually, only run that once, so i updated the table
select * from ads.ext_black_book_values order by blackbookvaluets desc limit 100

select min(blackbookvaluets), max(blackbookvaluets) from ads.ext_black_book_values


select task, min(the_date), max(the_date) from ops.ads_extract group by task order by task


select a.vehicleitemid, count(*)
from pp.evals_1 a
join ads.ext_vehicle_inventory_items b on a.vehicleitemid = b.vehicleitemid
group by a.vehicleitemid
order by count(*) desc


select * from ads.ext_black_book_values where vehicleitemid = '0f3d7b22-feac-c140-a6fc-4e508ceb8150'

select count(*) from pp.evals_1 where eval_status = 'booked'  --427, 394 w/bb

select a.vin, 
	b.cleanbase as c_base, b.averagebase as a_base, b.roughbase as r_base, 
	b.cleanmileageadjustment as c_adj,
			b.averagemileageadjustment as a_adj, b.roughmileageadjustment as r_adj,
	b.regionaladjustment as region_adj, b.addsdeductstotal as add_ded, b.mileage as miles
from pp.evals_1 a
left join ads.ext_black_book_values b on a.vehicleevaluationid = b.tablekey
where a.eval_Status = 'booked'    


--/> black book -----------------------------------------------------------------------------------


--< 3rd party -----------------------------------------------------------------------------------
manheim, kbb

select * from pp.evals_1 where vin = '1GCVKREC6EZ180630'

select * from chr.build_data_describe_vehicle where vin = '1GCVKREC6EZ180630'

select distinct typ from ads.ext_vehicle_third_party_values

select a.stock_number, a.vin, b.amount as eval_manheim, c.amount as walk_manheim, d.amount as kbb_fair, e.amount as kbb_good
from pp.evals_1 a
left join ads.ext_vehicle_third_party_values b on a.vehicleevaluationid = b.tablekey
  and b.typ = 'VehicleThirdPartyValue_Manheim'
left join ads.ext_vehicle_third_party_values c on a.vehicle_walk_id::citext = c.tablekey
  and c.typ = 'VehicleThirdPartyValue_Manheim'  
left join ads.ext_vehicle_third_party_values d on a.vehicleevaluationid = d.tablekey
  and d.typ = 'VehicleThirdPartyValue_KelleyBlueBookFair'  
left join ads.ext_vehicle_third_party_values e on a.vehicleevaluationid = e.tablekey
  and e.typ = 'VehicleThirdPartyValue_KelleyBlueBookGood'  
where a.eval_status = 'booked'


--/> 3rd party -----------------------------------------------------------------------------------



---------------------------------------------------------------------------------------------------------------
--/> 10/20/22  request bb and manheim values
---------------------------------------------------------------------------------------------------------------




/*  this is something that will need to be cleaned up
select store, left(stock_number, 1), count(*)
from pp.evals_1
group by store, left(stock_number, 1)


select * from pp.evals_1 where store = 'GM' and left(Stock_number, 1) = 'T'
*/