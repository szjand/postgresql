﻿-- sent to the group on 11/10/2022
drop table if exists pp.competitors cascade;
create table pp.competitors (
  dealer_name citext);
insert into pp.competitors  
select distinct 
  case
    when dealership_name like 'Aitkin%' then 'Aitkin Motor Company'
    when dealership_name like 'Brost%' then 'Brost Chevrolet, Inc.'
    when dealership_name like 'Cartiva%' then 'Cartiva of Minot'
    when dealership_name like '%an Brothers Ford%' then 'Christian Brothers Ford, Inc.'
    when dealership_name like 'Christian Chrysler%' then 'Christian Chrysler Dodge Jeep Ram'
    when dealership_name = 'Corwin' then 'Corwin Automotive Group'
    when dealership_name like 'Corwin Chrysler%' then 'Corwin Chrysler Dodge Jeep Ram'
    when dealership_name like 'Corwin Toyota%' then 'Corwin Toyota of Fargo'
    when dealership_name like 'Dave Hahler Automotive%' then 'Dave Hahler Automotive, Inc.'
    when dealership_name = 'D&B Motors' then 'D & B Motors Incorporated'
    when dealership_name like 'Diamond Buick GMC%' then 'Diamond Buick GMC of Alexandria'
    when dealership_name like '%Eide Ford Lincoln%' then 'Eide Ford Lincoln'
    when dealership_name like 'Eide Ford-Lincoln Inc%' then 'Eide Ford Lincoln'
    when dealership_name like 'Elite Motors%' then 'Elite Motors Auto Sales'
    when dealership_name like 'Finley%' then 'Finley Motors, Incorporated'
    when dealership_name like 'Grand Forks Subaru%' then 'Grand Forks Subaru Kia'
    when dealership_name like 'Juettner %' then 'Juettner Ford Lincoln'
    when upper(dealership_name) like 'KIA%' then 'Kia Of Fargo'
    when dealership_name like 'Lake Chevrolet%' then 'Lake Chevrolet GM Auto Center'
    when dealership_name like 'Lithia Chrysler%' then 'Lithia Chrysler Jeep Dodge of Grand Forkss'
    when dealership_name like 'Lunde%' then 'Lunde Kia Lincoln Mazda'
    when dealership_name like 'Mills Ford%' then 'Mills Ford Lincoln'
    when dealership_name like '%Mills GM%' then 'Mills GM'
    when dealership_name like 'Minnesota Motor%' then 'Minnesota Motor Co'
    when dealership_name like 'Minot%' then 'Minot Chrysler and Toyota Center'
    when dealership_name like 'M J McGuire%' then 'MJ McGuire Company'
    when dealership_name = 'Muscatell ' then 'Muscatell Burns Ford'
    when dealership_name like 'Nereson%' then 'Nereson Chevrolet Cadillac'
    when dealership_name like 'Nick%' then 'Nick''s Auto Sales'
    when dealership_name like 'Norseman Motors%' then 'Norseman Motors, Inc.'
    when dealership_name like 'Northern Motors%' then 'Northern Motors, Inc.'
    when dealership_name like 'Nyhus Family Sales%' then 'Nyhus Family Sales, Inc.'
    when dealership_name like 'Pfeifle%' then 'Pfeifle Chevrolet Buick'
    when dealership_name like 'Pierson Ford%' then 'Pierson Ford Lincoln, Inc.'
    when dealership_name like 'Puklich%' then 'Puklich Chevrolet Buick GMC'
    when dealership_name = 'R & G Subaru' then 'R&G Subaru'
    when dealership_name like 'Ryan Chevrolet%' then 'Ryan Chevrolet of Minot'
    when dealership_name like 'Ryan GMC%' then 'Ryan GMC Buick Cadillac'
    when dealership_name like 'Tanner Auto%' then 'Tanner Motors'
    when dealership_name like 'Thielen Motors%' then 'Thielen Motors Inc'
    when dealership_name like 'Valu%' then 'Valu Ford & Chryslers'
    when dealership_name like 'Veracity Motors%' then 'Veracity Motors Inc'
    when dealership_name like 'Westside Motors%' then 'Westside Motors of TRF, Inc.'
    when dealership_name like 'Wolfe Ford%' then 'Wolfe Ford, Inc.'
--     when dealership_name like 'Elite Motors%' then 'Elite Motors Auto Sales'
    else dealership_name
  end
from (
	select distinct dealership_name 
	from scrapes.competitor_websites 
	where is_current --order by dealership_name
	union
	select dealer --, similarity(dealer, next_dealer) as sim_next, similarity(dealer, previous_dealer) as sim_prev
	from (
		select dealer, lead(dealer, 1) over (order by dealer) as next_dealer,
			lag(dealer, 1) over (order by dealer) as previous_dealer
		from (
			select distinct 
					case
						when dealer like '%&amp;%' then replace(dealer, 'amp;','')
						else dealer
					end as dealer
			from scrapes.carsdotcom_daily
			where dealer <> 'N/A'
			union
			select distinct 
					case
						when dealer like '%&amp;%' then replace(dealer, 'amp;','')
						else dealer
					end as dealer
			from scrapes.cargurus_dealers_daily
			where dealer <> 'N/A'
			union
			select distinct 
					case
						when dealer like '%&amp;%' then replace(dealer, 'amp;','')
						else dealer
					end as dealer
			from scrapes.autotrader_daily
			where dealer <> 'N/A') a) b) c
where dealership_name not in ('Lithia Toyota')	
order by dealership_name



-- select * from scrapes.competitor_websites where dealership_name = 'Corwin'
