﻿/*
in the pricing/eval meeting today
decided to add certain options to figuring the mileage factor
contribute to ben fosters calculation of the z number

referencing used_car_pricing_project\most_year_make_models_sold_2_years.sql

per Wilkie and Knudson, the right configuration for an equinox is LT with Confidence & Convenience Pkg
which includes:
Dual Zone Automatic Climate
Heated Front Seat
Rmove Vehicle Start
Programmable Power Reear Liftgate
Wilkie: these are very improtant to customers versus without
*/
-- 4 years of financial statement data
-- 12/9/22 why did i exclude wholesale?  put them back in fuck, leave wholesale out for now
drop table if exists pp.equinox_mf_step_1 cascade;
create unlogged table pp.equinox_mf_step_1 as
select year_month, control as stock_number, gl_account, -amount::integer as sale_price
from (
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201810 and 202211-----------------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month between 201810 and 202211  ---------------------------------------------------------------------
      and b.page = 16 and b.line between 1 and 6 --12 -- used cars include wholesale
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by year_month, control, gl_account, -amount::integer;
comment on table pp.equinox_mf_step_1 is 'base data for trying to generate a mileage factor using equinoxes from the last 4 years';

select * from pp.equinox_mf_step_1 limit 100

select count(*) from pp.equinox_mf_step_1  -- 12576

-- multiple instances of the same stock_number
select a.* 
from pp.equinox_mf_step_1 a
join (
  select stock_number
  from pp.equinox_mf_step_1
  group by stock_number 
  having count(*) > 1) b on a.stock_number = b.stock_number
order by a.stock_number


-- inner join: 11548
-- 687 missing, 592 of which are L vehicles
select a.*, b.vehicleitemid
-- select right(stock_number, 1), count(*)
from (
	select stock_number, sum(sale_price) as sale_price  -- 12235
	from pp.equinox_mf_step_1
	group by stock_number
	having sum(sale_price) > 0) a
left join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
where b.stocknumber is null
group by right(stock_number, 1)

-- so, i am ok excluding those that don't have a record in vehicleinventoryitems
select * from ads.ext_vehicle_sales limit 100
select typ, split_part(typ, '_', 2) from ads.ext_vehicle_sales limit 100

-- 11548, only lose 6 with inner join to vehiclesales
-- head so far up the ass, actually 72 equinoxes
-- need miles, no miles on intra market vehicles
drop table if exists pp.equinox_mf_step_2 cascade;
create unlogged table pp.equinox_mf_step_2 as
select a.*, c.vin, max(d.soldts::date) as sale_date, -- most recent sale date only
	b.fromts::date as from_date, b.thruts::date as thru_date, split_part(typ, '_', 2) as sale_type, 
	b.vehicleitemid, b.vehicleinventoryitemid
-- select c.model	
from (
	select stock_number, sum(sale_price) as sale_price  -- 12235
	from pp.equinox_mf_step_1
	group by stock_number
	having sum(sale_price) > 0) a
join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
  and c.model = 'equinox'
join ads.ext_vehicle_sales d on b.vehicleinventoryitemid = d.vehicleinventoryitemid
left join ads.ext_vehicle_item_mileages e on c.vehicleitemid = e.vehicleitemid
  and e.vehicleitemmileagets between b.fromts and b.thruts
-- where right(a.stock_number, 1) in ('G','H','T')  
group by a.stock_number, a.sale_price, c.vin, split_part(typ, '_', 2), b.vehicleitemid, b.vehicleinventoryitemid
order by a.stock_number;
comment on table pp.equinox_mf_step_2 is 'base data for trying to generate a mileage factor using equinoxes from the last 4 years,
  this step adds the vin and sale date ';

select count(*) from pp.equinox_mf_step_2
select * from pp.equinox_mf_step_2 limit 10

-- miles, not done  yet
select a.vehicleitemid, a.from_date, a.thru_date, b.vehicleitemmileagets, b.value
from pp.equinox_mf_step_2 a
join ads.ext_vehicle_item_mileages b on a.vehicleitemid = b.vehicleitemid
  and b.vehicleitemmileagets::date between a.from_Date and a.thru_date
order by a.vehicleitemid

select * from fin.dim_Account where account in ('145000','145002','145001','144601','245200')  
-- exceptions to deal with
-- 32181RA gm car: converted to UC from drivers training
-- intra market:  xxxG, xxxH, xxxT
-- lease purchases: xxxL
-- uh oh, now 

-- already have 8085 in build data
select distinct a.vin
from pp.equinox_mf_step_2 a
join chr.build_data_describe_vehicle b on a.vin = b.vins

-- get build data for the rest
select distinct vin
from pp.equinox_mf_step_2 a
where not exists (
  select 1
  from chr.build_data_describe_vehicle
  where vin = a.vin)

-- !!!!!!!!!!!!!!!!! 12-10-22 oh shit, i have never limited to equinox !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- based on nc.stg_vehicle query from \nc_inventory\all_nc_inventory_nightly.sql

select * from chr.build_data_describe_vehicle where vin = '3GNAXXEV5MS164525'

-- need miles, miles/year
select a.vin, (r.style ->>'id')::citext as chrome_style_id,
  (r.style->>'modelYear')::integer as model_year,
  (r.style ->'division'->>'_value_1')::citext as make,
  (r.style ->'model'->>'_value_1'::citext)::citext as model,
  (r.style ->>'mfrModelCode')::citext as model_code,
  case
    when r.style ->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drive,
  coalesce(r.style ->>'trim', 'none')::citext as trim_level,
  t->>'_value_1' as engine_displacement, 
  replace((c.response->'vinDescription'->>'builtMSRP'),'.0', '')::integer as msrp,
  c.response->'vinDescription'->>'styleName' as style_name
from pp.equinox_mf_step_2 a
join chr.build_data_describe_vehicle c on a.vin = c.vin
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
  and (s.engine->'installed'->>'cause' = 'OptionCodeBuild' or coalesce(s.engine->'installed'->>'cause', 'xxx') = 'VIN')
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->>'unit' = 'liters'  
where a.vin = '2GNAXUEV2L6171119'
order by (r.style->>'modelYear')::integer desc
limit 100


-- equinoxen that were invoiced here
select * from gmgl.vehicle_invoices a
where exists (
  select 1
  from pp.equinox_mf_step_2
  where vin = a.vin)

 select * from chr.build_data_describe_vehicle where vin = '2GNAXUEV2L6171119' 

select 
	h->'header'->>'_value_1' as header, h->>'oemCode' as oem_code, h.*
from pp.equinox_mf_step_2 a
join chr.build_data_describe_vehicle c on a.vin = c.vin
left join jsonb_array_elements(c.response->'factoryOption') h on true
  and h-> 'installed' <> 'null'
where a.vin = '2GNAXUEV2L6171119'

select * from gmgl.vehicle_vis where vin = '2GNAXUEV2L6171119' 

select* from gmgl.vehicle_option_codes where vin = '2GNAXUEV2L6171119' 

select * from gmgl.get_vehicle_build_data('2GNAXUEV2L6171119')