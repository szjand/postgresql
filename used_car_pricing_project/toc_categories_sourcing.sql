﻿-- 4 years of financial statement data

drop table if exists pp.category_sourcing_1 cascade;
create table pp.category_sourcing_1 as
select year_month, store, page, line, line_label, control, sum(unit_count) as unit_count, sum(amount) as sale_price
from (
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 202010 --between 202010 and 202210-----------------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 202010 -- between 202010 and 202210  ---------------------------------------------------------------------
      and b.page = 16 and b.line between 1 and 12 -- used cars, retail & wholesale
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by year_month, store, page, line, line_label, control;
comment on table pp.category_sourcing_1 is 'raw data from account for past 2 years of used sales, retail & wholesale, to do some 
toc category sourcing analysis';

-- need price (for price band) couldn't quickly figure out how to do it all at once, so i did it by month
do $$
	declare
	  _year_month integer := 202210;
begin	  
	insert into pp.category_sourcing_1
	select year_month, store, page, line, line_label, control, sum(unit_count) as unit_count, sum(amount) as sale_price
	from (
		select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
			a.control, a.amount,
			case when a.amount < 0 then 1 else -1 end as unit_count
		from fin.fact_gl a
		inner join dds.dim_date b on a.date_key = b.date_key
			and b.year_month = _year_month --between 202010 and 202210-----------------------------------------------------------------------------
		inner join fin.dim_account c on a.account_key = c.account_key
			and c.current_row
	-- add journal
		inner join fin.dim_journal aa on a.journal_key = aa.journal_key
			and aa.journal_code in ('VSN','VSU')
		inner join ( -- d: fs gm_account page/line/acct description
			select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
			from fin.fact_fs a
			inner join fin.dim_fs b on a.fs_key = b.fs_key
				and b.year_month = _year_month -- between 202010 and 202210  ---------------------------------------------------------------------
				and b.page = 16 and b.line between 1 and 12 -- used cars, retail & wholesale
			inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
			inner join fin.dim_account e on d.gl_account = e.account
				and e.account_type_code = '4'
				and e.current_row
			inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
		where a.post_status = 'Y') h
	group by year_month, store, page, line, line_label, control;
end $$;	


select * from pp.category_sourcing_1 limit 10-- 8412

--8337
select control, b.fromts::date as from_date, b.thruts::date as thru_Date,
  c.yearmodel, c.make, c.model, d.vehiclesegment, d.vehicletype 
from pp.category_sourcing_1 a
left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
join ads.ext_make_model_classifications d on c.make = d.make and c.model = d.model
  and d.vehiclesegment in ('VehicleSegment_Large')

-- anbd the source is overwhelmingly trades
select category, 
  count(*) filter (where source = 'trade') as trade,
  count(*) filter (where source = 'street') as street,
  count(*) filter (where source = 'auction') as auction
from (
	select a.control, -a.sale_price::integer as sale_price, a.year_month, b.fromts::date as from_date, b.thruts::date as thru_date,
		c.make, c.model, -- d.vehiclesegment, d.vehicletype
		case
			when c.make in ('chevrolet','gmc','buick','cadillac') and d.vehiclesegment = 'VehicleSegment_Large' and d.vehicletype = 'VehicleType_Pickup' and -a.sale_price between 15001 and 25000 then 'gm_large_pk_15-25'
			when c.make in ('chevrolet','gmc','buick','cadillac') and d.vehiclesegment = 'VehicleSegment_Large' and d.vehicletype = 'VehicleType_Pickup' and -a.sale_price between 25001 and 35000 then 'gm_large_pk_25-35'
			when c.make in ('chevrolet','gmc','buick','cadillac') and d.vehiclesegment = 'VehicleSegment_Large' and d.vehicletype = 'VehicleType_Pickup' and -a.sale_price between 35001 and 45000 then 'gm_large_pk_35-45'
			when c.make in ('chevrolet','gmc','buick','cadillac') and d.vehiclesegment = 'VehicleSegment_Large' and d.vehicletype = 'VehicleType_Pickup' and -a.sale_price between 45001 and 145000 then 'gm_large_pk_45+'
			when c.make in ('chevrolet','gmc','buick','cadillac') and d.vehiclesegment = 'VehicleSegment_Small' and d.vehicletype = 'VehicleType_SUV' and -a.sale_price between 15001 and 25000 then 'gm_small_suv_15-25'
			when c.make in ('chevrolet','gmc','buick','cadillac') and d.vehiclesegment = 'VehicleSegment_Small' and d.vehicletype = 'VehicleType_SUV' and -a.sale_price between 25001 and 125000 then 'gm_small_suv_25+'
			when c.make in ('toyota','honda','nissan') and d.vehiclesegment = 'VehicleSegment_Small' and d.vehicletype = 'VehicleType_SUV' and -a.sale_price between 15001 and 25000 then 'import_small_suv_15-25'
			when c.make in ('toyota','honda','nissan') and d.vehiclesegment = 'VehicleSegment_Small' and d.vehicletype = 'VehicleType_SUV' and -a.sale_price between 25001 and 125000 then 'import_small_suv_25+'   
			when -a.sale_price < 15000 then 'outlet'   
		end as category,
		case
			when right(control, 1) = 'X' then 'auction'
			when right(control, 1) = 'P' then 'street'
			else 'trade'
		end as source
	from pp.category_sourcing_1 a
	join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
	join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
	join ads.ext_make_model_classifications d on c.make = d.make and c.model = d.model
	where right(a.control, 1) <> 'G' -- and a.line < 7
	and (
		(c.make in ('chevrolet','gmc','buick','cadillac') and d.vehiclesegment = 'VehicleSegment_Large' and d.vehicletype = 'VehicleType_Pickup' and -a.sale_price between 15001 and 25000)
		or
		(c.make in ('chevrolet','gmc','buick','cadillac') and d.vehiclesegment = 'VehicleSegment_Large' and d.vehicletype = 'VehicleType_Pickup' and -a.sale_price between 25001 and 35000)
		OR
		(c.make in ('chevrolet','gmc','buick','cadillac') and d.vehiclesegment = 'VehicleSegment_Large' and d.vehicletype = 'VehicleType_Pickup' and -a.sale_price between 35001 and 45000)
		OR
		(c.make in ('chevrolet','gmc','buick','cadillac') and d.vehiclesegment = 'VehicleSegment_Large' and d.vehicletype = 'VehicleType_Pickup' and -a.sale_price between 45001 and 145000)
		or
		(c.make in ('chevrolet','gmc','buick','cadillac') and d.vehiclesegment = 'VehicleSegment_Small' and d.vehicletype = 'VehicleType_SUV' and -a.sale_price between 15001 and 25000)
		or
		(c.make in ('chevrolet','gmc','buick','cadillac') and d.vehiclesegment = 'VehicleSegment_Small' and d.vehicletype = 'VehicleType_SUV' and -a.sale_price between 25001 and 125000)
		or
		(c.make in ('toyota','honda','nissan') and d.vehiclesegment = 'VehicleSegment_Small' and d.vehicletype = 'VehicleType_SUV' and -a.sale_price between 15001 and 25000)
		or
		(c.make in ('toyota','honda','nissan') and d.vehiclesegment = 'VehicleSegment_Small' and d.vehicletype = 'VehicleType_SUV' and -a.sale_price between 25001 and 125000)
		or
		(-a.sale_price < 15000))
	) x
group by category

-- missed trades of category vehicles in the last 6 months
select a.amountshown, b.* 
from ads.ext_vehicle_evaluations a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
join ads.ext_make_model_classifications c on b.make = c.make and b.model = c.model
where vehicleevaluationts::date > current_date - 180
  and currentstatus = 'VehicleEvaluation_Finished'
	and (
		(b.make in ('chevrolet','gmc','buick','cadillac') and c.vehiclesegment = 'VehicleSegment_Large' and c.vehicletype = 'VehicleType_Pickup' and a.amountshown between 15001 and 25000)
		or
		(b.make in ('chevrolet','gmc','buick','cadillac') and c.vehiclesegment = 'VehicleSegment_Large' and c.vehicletype = 'VehicleType_Pickup' and a.amountshown between 25001 and 35000)
		OR
		(b.make in ('chevrolet','gmc','buick','cadillac') and c.vehiclesegment = 'VehicleSegment_Large' and c.vehicletype = 'VehicleType_Pickup' and a.amountshown between 35001 and 45000)
		OR
		(b.make in ('chevrolet','gmc','buick','cadillac') and c.vehiclesegment = 'VehicleSegment_Large' and c.vehicletype = 'VehicleType_Pickup' and a.amountshown between 45001 and 145000)
		or
		(b.make in ('chevrolet','gmc','buick','cadillac') and c.vehiclesegment = 'VehicleSegment_Small' and c.vehicletype = 'VehicleType_SUV' and a.amountshown between 15001 and 25000)
		or
		(b.make in ('chevrolet','gmc','buick','cadillac') and c.vehiclesegment = 'VehicleSegment_Small' and c.vehicletype = 'VehicleType_SUV' and a.amountshown between 25001 and 125000)
		or
		(b.make in ('toyota','honda','nissan') and c.vehiclesegment = 'VehicleSegment_Small' and c.vehicletype = 'VehicleType_SUV' and a.amountshown between 15001 and 25000)
		or
		(b.make in ('toyota','honda','nissan') and c.vehiclesegment = 'VehicleSegment_Small' and c.vehicletype = 'VehicleType_SUV' and a.amountshown between 25001 and 125000)
		or
		(a.amountshown < 15000))  
 