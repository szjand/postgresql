﻿/*
01/20/23
Nate has repriced 57 vehicles based on kbb
want to track these vehicles
*/

create unlogged table ads.ext_kbb_toc_repricings (
  vehicleinventoryitemid citext);
alter table ads.ext_kbb_toc_repricings
add primary key(vehicleinventoryitemid);


create temp table wtf as
select vehicleinventoryitemid, row_number() over (partition by vehicleinventoryitemid) as seq
from ads.ext_kbb_toc_repricings;

delete from wtf where seq = 2;

drop table if exists ads.ext_kbb_toc_repricings;
create unlogged table ads.ext_kbb_toc_repricings (
  vehicleinventoryitemid citext);
insert into ads.ext_kbb_toc_repricings
select vehicleinventoryitemid from wtf;  
alter table ads.ext_kbb_toc_repricings
add primary key(vehicleinventoryitemid);
select * from ads.ext_kbb_toc_repricings;

select * from ads.ext_vehicle_inventory_item_notes limit 100
select * from ads.ext_vehicle_pricings limit 50
select * from ads.ext_vehicle_pricing_details limit 50
select * from ads.ext_people limit 50
select * from arkona.ext_inpmast limit 50

-- select count(distinct stocknumber) from (
select b.stocknumber, c.vin, c.yearmodel as model_year, c.make, c.trim, b.fromts::date as date_acquired, 
	e.vehiclepricingts::date as date_priced, 
	regexp_replace(d.notes, E'[\n\r]', ' ', 'g') as pricing_note, f.amount::integer as best_price, 
	h.inpmast_vehicle_cost::integer as adj_cost,
	(select * from ads.get_vii_sales_status_on_date(a.vehicleinventoryitemid, e.vehiclepricingts::timestamp)) as sales_status_at_repricing,
	g.fullname as priced_by,
	( select * from ads.get_vii_sales_status(a.vehicleinventoryitemid)) as current_status
from ads.ext_kbb_toc_repricings  a
join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
join ads.ext_vehicle_inventory_item_notes d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
  and d.category = 'vehiclepricing'
join ads.ext_vehicle_pricings e on d.tablekey = e.vehiclepricingid
join ads.ext_vehicle_pricing_details f on e.vehiclepricingid = f.vehiclepricingid
  and f.typ = 'VehiclePricingDetail_BestPrice'
join ads.ext_people g on e.pricedby = g.partyid
left join arkona.ext_inpmast h on b.stocknumber = h.inpmast_stock_number
-- ) x  
order by b.stocknumber, e.vehiclepricingts

-- get the initial price from walk
-- 2 missing, no walk
select * from ads.ext_kbb_toc_repricings aa
left join (
	select distinct a.vehicleinventoryitemid, c.vehiclepricingts::date as date_priced, d.amount as best_price
	from ads.ext_kbb_toc_repricings a
	join ads.ext_vehicle_walks b on 
		case 
			when a.vehicleinventoryitemid = '7540233d-0690-43e5-bbf3-25ea298e062b' then b.vehicle_inventory_item_id::citext = 'b88dbf6e-2802-4b33-8b02-52bb92431438'
			when a.vehicleinventoryitemid = '772d2f38-7651-468c-806f-ec5b4e773070' then b.vehicle_inventory_item_id::citext = '788d25b6-4c89-407b-8c0a-3812c4cae0ca'
			else a.vehicleinventoryitemid = b.vehicle_inventory_item_id::citext
		end
	join ads.ext_vehicle_pricings c on b.vehicle_walk_id::citext = c.tablekey
	join ads.ext_vehicle_pricing_details d on c.vehiclepricingid = d.vehiclepricingid
		and d.typ = 'VehiclePricingDetail_BestPrice') bb on aa.vehicleinventoryitemid = bb.vehicleinventoryitemid
  
-- they are both intramarket wholesales
select * from ads.ext_vehicle_inventory_items where vehicleinventoryitemid in ('772d2f38-7651-468c-806f-ec5b4e773070','7540233d-0690-43e5-bbf3-25ea298e062b')
select * from ads.get_intra_market_wholesale_prior_stock_number('7540233d-0690-43e5-bbf3-25ea298e062b');  -- H15933P  b88dbf6e-2802-4b33-8b02-52bb92431438
select * from ads.get_intra_market_wholesale_prior_stock_number('772d2f38-7651-468c-806f-ec5b4e773070'); -- T10238A 788d25b6-4c89-407b-8c0a-3812c4cae0ca
select stocknumber, vehicleinventoryitemid from ads.ext_vehicle_inventory_items where stocknumber in ('H15933P','T10238A')


select 	d.notes, 
  regexp_replace(d.notes, E'[\n\r]', ' ', 'g')
-- replace(d.notes, char(13) || char(10),'')
from ads.ext_kbb_toc_repricings  a
join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
join ads.ext_vehicle_inventory_item_notes d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
  and d.category = 'vehiclepricing'
join ads.ext_vehicle_pricings e on d.tablekey = e.vehiclepricingid
join ads.ext_vehicle_pricing_details f on e.vehiclepricingid = f.vehiclepricingid
  and f.typ = 'VehiclePricingDetail_BestPrice'
join ads.ext_people g on e.pricedby = g.partyid
-- ) x  
order by b.stocknumber, e.vehiclepricingts


-- are you able to update the repriced vehicles spreadsheet to include current status and if sold or wholesaled the associated gross?
-- progress reporting
-- get rid of the pricing history, one row per vehicle
select ( select * from ads.get_vii_sales_status(a.vehicleinventoryitemid))
from ads.ext_kbb_toc_repricings a
join ads.ext_vehicle_inventory_items b on a.
  
select x.*, 
  (select ads.get_current_best_price_by_stock_number_used(x.stocknumber)) as best_price,
  y.gross::integer as gross
from (
	select b.stocknumber, c.vin, c.yearmodel as model_year, c.make, c.trim, b.fromts::date as date_acquired, 
	-- 	e.vehiclepricingts::date as date_priced, 
	-- 	regexp_replace(d.notes, E'[\n\r]', ' ', 'g') as pricing_note, f.amount::integer as best_price, 
		coalesce(h.inpmast_vehicle_cost::integer, i.inpmast_vehicle_cost::integer) as adj_cost,
	-- 	(select * from ads.get_vii_sales_status_on_date(a.vehicleinventoryitemid, e.vehiclepricingts::timestamp)) as sales_status_at_repricing,
	-- 	g.fullname as priced_by,
		( select * from ads.get_vii_sales_status(a.vehicleinventoryitemid)) as current_status
	from ads.ext_kbb_toc_repricings  a
	join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
	join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
	-- join ads.ext_vehicle_inventory_item_notes d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
	--   and d.category = 'vehiclepricing'
	-- join ads.ext_vehicle_pricings e on d.tablekey = e.vehiclepricingid
	-- join ads.ext_vehicle_pricing_details f on e.vehiclepricingid = f.vehiclepricingid
	--   and f.typ = 'VehiclePricingDetail_BestPrice'
	-- join ads.ext_people g on e.pricedby = g.partyid
	left join arkona.ext_inpmast h on b.stocknumber = h.inpmast_stock_number
		and h.inpmast_company_number = 'RY1'
	left join arkona.ext_inpmast i on b.stocknumber = i.inpmast_stock_number
		and i.inpmast_company_number = 'RY8') x
-- where current_status in ('Delivered','Wholesaled')  
left join (
	select a.stocknumber, sum(-b.amount) as gross
	-- from jon.morgan_deal_gross a
	from (
		select *
		from (
				select b.stocknumber, 
					( select * from ads.get_vii_sales_status(a.vehicleinventoryitemid)) as current_status
				from ads.ext_kbb_toc_repricings  a
				join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid) xx
			where current_status in ('Delivered','Wholesaled')) a
		join fin.fact_gl b on a.stocknumber = b.control
			and b.post_status = 'Y'
		join fin.dim_account c on b.account_key = c.account_key
			and c.account in (
				select b.g_l_acct_number::citext as account
				from arkona.ext_eisglobal_sypffxmst a
				inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
					and b.factory_financial_year  = (select max(factory_financial_year) from arkona.ext_ffpxrefdta)
				where a.fxmcyy = (select max(fxmcyy) from arkona.ext_eisglobal_sypffxmst)
					and coalesce(b. consolidation_grp, '1') <> '3'
					and b.g_l_acct_number <> ''
					and (
						(a.fxmpge between 5 and 15) or
						(a.fxmpge = 16 and a.fxmlne between 1 and 14) or
						(a.fxmpge = 17 and a.fxmlne between 1 and 19))
					and b.g_l_acct_number not in ('144502','164502')) 
		group by a.stocknumber) y on x.stocknumber = y.stocknumber
    