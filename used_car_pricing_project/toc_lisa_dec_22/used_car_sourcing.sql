﻿
----------------------------------------------------------------------------------------------------------------------------------------------------
--< and then some used car sourcing data, used this for spreadsheet 12/19
----------------------------------------------------------------------------------------------------------------------------------------------------
-- first the november dataset we are working with
select source, count(*) as vehicles 
from pp.toc_category_analysis_detail_1
where source <> 'unknown'
group by source
order by count(*) desc

select category, 
  count(*) filter (where source = 'trade') as trade,
  count(*) filter (where source = 'auction') as auction,
  count(*) filter (where source = 'street') as street,
  count(*) filter (where source = 'lease') as lease
from pp.toc_category_analysis_detail_1
where source <> 'unknown'
group by category
union
select 'TOTAL'::citext, 
  count(*) filter (where source = 'trade') as trade,
  count(*) filter (where source = 'auction') as auction,
  count(*) filter (where source = 'street') as street,
  count(*) filter (where source = 'lease') as lease
from pp.toc_category_analysis_detail_1
where source <> 'unknown'
order by category


-- 2 years of retail sales
-- this comes from today's scratchpad

select category, 
  count(*) filter (where source = 'trade') as trade,
  count(*) filter (where source = 'auction') as auction,
  count(*) filter (where source = 'street') as street,
  count(*) filter (where source = 'lease') as lease
--   count(*) filter (where source = 'unknown') as unknown
from (
	select * ,
		case
			when sale_price is null then 'not priced'
			when make in ('chevrolet','gmc','buick','cadillac') and size = 'Large' and shape = 'Pickup' and sale_price between 15000 and 24999 then 'gm_large_pk_15-25'
			when make in ('chevrolet','gmc','buick','cadillac') and size = 'Large' and shape = 'Pickup' and sale_price between 25000 and 34999 then 'gm_large_pk_25-35'
			when make in ('chevrolet','gmc','buick','cadillac') and size = 'Large' and shape = 'Pickup' and sale_price between 35000 and 44999 then 'gm_large_pk_35-45'
			when make in ('chevrolet','gmc','buick','cadillac') and size = 'Large' and shape = 'Pickup' and sale_price between 45000 and 145000 then 'gm_large_pk_45+'
			when make in ('chevrolet','gmc','buick','cadillac') and size = 'Small' and shape = 'SUV' and sale_price between 15000 and 24999 then 'gm_small_suv_15-25'
			when make in ('chevrolet','gmc','buick','cadillac') and size = 'Small' and shape = 'SUV' and sale_price between 25000 and 125000 then 'gm_small_suv_25+'
			when make in ('toyota','honda','nissan') and size = 'Small' and shape = 'SUV' and sale_price between 15000 and 24999 then 'import_small_suv_15-25'
			when make in ('toyota','honda','nissan') and size = 'Small' and shape = 'SUV' and sale_price between 25000 and 125000 then 'import_small_suv_25+'   
			when sale_price < 15000 then 'outlet'   
		end::citext as category	
	from (	
		select *,
			split_part(vehicletype, '_', 2)::citext as shape, 
			case
				when split_part(vehiclesegment, '_', 2) = 'Extra' then 'Extra_Large'
				else split_part(vehiclesegment, '_', 2) 
			end::citext as size,	
			coalesce(
				case
					when prior_stock is null then
						case
							when right(stock_number, 1) in ('A','B','C','D','E') then 'trade'
							when right(stock_number, 1) = 'L' then 'lease'
							when right(stock_number, 1) in ('X','M') then 'auction'
							when right(stock_number, 1) = 'P' then 'street'
						end
					else
						case
							when right(prior_stock, 1) in ('A','B','C','D','E') then 'trade'
							when right(prior_stock, 1) = 'L' then 'lease'
							when right(prior_stock, 1) in ('X','M') then 'auction'
							when right(prior_stock, 1) = 'P' then 'street'		
						end
				end::citext, 'unknown') as source	 
		from pp.toc_new_categories_4) a) b
group by category		
union
select 'TOTAL'::citext, 
  count(*) filter (where source = 'trade') as trade,
  count(*) filter (where source = 'auction') as auction,
  count(*) filter (where source = 'street') as street,
  count(*) filter (where source = 'lease') as lease
--   count(*) filter (where source = 'unknown') as unknown
from (
	select * ,
		case
			when sale_price is null then 'not priced'
			when make in ('chevrolet','gmc','buick','cadillac') and size = 'Large' and shape = 'Pickup' and sale_price between 15000 and 24999 then 'gm_large_pk_15-25'
			when make in ('chevrolet','gmc','buick','cadillac') and size = 'Large' and shape = 'Pickup' and sale_price between 25000 and 34999 then 'gm_large_pk_25-35'
			when make in ('chevrolet','gmc','buick','cadillac') and size = 'Large' and shape = 'Pickup' and sale_price between 35000 and 44999 then 'gm_large_pk_35-45'
			when make in ('chevrolet','gmc','buick','cadillac') and size = 'Large' and shape = 'Pickup' and sale_price between 45000 and 145000 then 'gm_large_pk_45+'
			when make in ('chevrolet','gmc','buick','cadillac') and size = 'Small' and shape = 'SUV' and sale_price between 15000 and 24999 then 'gm_small_suv_15-25'
			when make in ('chevrolet','gmc','buick','cadillac') and size = 'Small' and shape = 'SUV' and sale_price between 25000 and 125000 then 'gm_small_suv_25+'
			when make in ('toyota','honda','nissan') and size = 'Small' and shape = 'SUV' and sale_price between 15000 and 24999 then 'import_small_suv_15-25'
			when make in ('toyota','honda','nissan') and size = 'Small' and shape = 'SUV' and sale_price between 25000 and 125000 then 'import_small_suv_25+'   
			when sale_price < 15000 then 'outlet'   
		end::citext as category	
	from (	
		select *,
			split_part(vehicletype, '_', 2)::citext as shape, 
			case
				when split_part(vehiclesegment, '_', 2) = 'Extra' then 'Extra_Large'
				else split_part(vehiclesegment, '_', 2) 
			end::citext as size,	
			coalesce(
				case
					when prior_stock is null then
						case
							when right(stock_number, 1) in ('A','B','C','D','E') then 'trade'
							when right(stock_number, 1) = 'L' then 'lease'
							when right(stock_number, 1) in ('X','M') then 'auction'
							when right(stock_number, 1) = 'P' then 'street'
						end
					else
						case
							when right(prior_stock, 1) in ('A','B','C','D','E') then 'trade'
							when right(prior_stock, 1) = 'L' then 'lease'
							when right(prior_stock, 1) in ('X','M') then 'auction'
							when right(prior_stock, 1) = 'P' then 'street'		
						end
				end::citext, 'unknown') as source	 
		from pp.toc_new_categories_4) a) b
order by category

----------------------------------------------------------------------------------------------------------------------------------------------------
--< and then some used car sourcing data
----------------------------------------------------------------------------------------------------------------------------------------------------