﻿-- 2/13/23
-- found the sight unseen notes on unbooked evals: vehicle_price_components
SELECT VehicleEvaluationts::date as eval_date, c.yearmodel, c.make, c.model, b.notes
FROM ads.ext_Vehicle_Evaluations a
JOIN ads.ext_vehicle_price_components b on a.VehicleEvaluationid = b.tablekey	
JOIN ads.ext_Vehicle_Items c on a.VehicleItemID = c.VehicleItemID
WHERE VehicleEvaluationts::date > '01/18/2023'
  AND a.VehicleInventoryItemID IS NULL
	AND b.notes IS NOT null