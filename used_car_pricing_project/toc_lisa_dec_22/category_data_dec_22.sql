﻿/*
-- issues for greg/afton
--		mileage category on old vehicles with high mileage, a 2004 with 218000 miles is categorized as low, based on miles/year
				seems like after a certain age, the categorization doesn't make sense
-- age
-- availability
-- mileage category
-- number of vehicle sold without becoming available
select * from ads.ext_make_model_classifications where model = 'yukon xl'
select * from ads.ext_vehicle_inventory_item_statuses order by fromts desc limit 100

select * from ads.ext_vehicle_inventory_items where stocknumber in ('H15988A','G45865H')

select * from ads.ext_make_model_classifications where model = 'equinox'
acq not on lot: not available except ws buf, sales buf, sold & not delivered

-- where i am at - multiple records for available

12/202/22  
if a vehicle has a prior stock number, that should determine the from date
separate stat gathering: inventory vs statuses, that would remove the need for a status of "acquired not on the lot new"
look into seq_1 when a vehicle has multiple instances of a status
*/
drop table if exists pp.toc_category_analysis_detail_1a cascade;
create unlogged table pp.toc_category_analysis_detail_1a as
select stock_number, vin, model_year, make, model, price, prior_stock, 
	case
		when prior_stock is null then aa.from_date
		else (select fromts::date from ads.ext_vehicle_inventory_items where stocknumber = aa.prior_stock)
	end as from_date,
	sale_date, 
  case
    when prior_stock is null then
			case
				when sale_date < current_date then sale_date - from_date 
				else null
			end
	  else
	    case
	      when sale_date < current_date then
					(select sale_date - (select fromts::date from ads.ext_Vehicle_inventory_items where stocknumber = aa.prior_stock))
			  else null
			end
  end as days_to_sell,
	shape, size, 
	case
		when price is null then 'not priced'
		when make in ('chevrolet','gmc','buick','cadillac') and size = 'Large' and shape = 'Pickup' and price between 15001 and 25000 then 'gm_large_pk_15-25'
		when make in ('chevrolet','gmc','buick','cadillac') and size = 'Large' and shape = 'Pickup' and price between 25001 and 35000 then 'gm_large_pk_25-35'
		when make in ('chevrolet','gmc','buick','cadillac') and size = 'Large' and shape = 'Pickup' and price between 35001 and 45000 then 'gm_large_pk_35-45'
		when make in ('chevrolet','gmc','buick','cadillac') and size = 'Large' and shape = 'Pickup' and price between 45001 and 145000 then 'gm_large_pk_45+'
		when make in ('chevrolet','gmc','buick','cadillac') and size = 'Small' and shape = 'SUV' and price between 15001 and 25000 then 'gm_small_suv_15-25'
		when make in ('chevrolet','gmc','buick','cadillac') and size = 'Small' and shape = 'SUV' and price between 25001 and 125000 then 'gm_small_suv_25+'
		when make in ('toyota','honda','nissan') and size = 'Small' and shape = 'SUV' and price between 15001 and 25000 then 'import_small_suv_15-25'
		when make in ('toyota','honda','nissan') and size = 'Small' and shape = 'SUV' and price between 25001 and 125000 then 'import_small_suv_25+'   
		when price < 15000 then 'outlet'   
	end::citext as category,
	(select * from ads.get_vii_sales_status(vehicleinventoryitemid))::citext as sales_status,
	case
		when prior_stock is null then
		  case
		    when right(stock_number, 1) in ('A','B','C','D','E') then 'trade'
		    when right(stock_number, 1) = 'L' then 'lease'
		    when right(stock_number, 1) in ('X','M') then 'auction'
		    when right(stock_number, 1) = 'P' then 'street'
		  end
		else
		  case
		    when right(prior_stock, 1) in ('A','B','C','D','E') then 'trade'
		    when right(prior_stock, 1) = 'L' then 'lease'
		    when right(prior_stock, 1) in ('X','M') then 'auction'
		    when right(prior_stock, 1) = 'P' then 'street'		
		  end
	end::citext as source,
	vehicleitemid, vehicleinventoryitemid		
from (
	select a.stocknumber as stock_number, b.vin, b.yearmodel as model_year, b.make, b.model, 
		(select ads.get_current_best_price_by_stock_number_used(a.stocknumber)) as price,
		case
			when right(a.stocknumber, 1) in ('G','H','T') then
				(select * from ads.get_intra_market_wholesale_prior_stock_number(a.vehicleinventoryitemid)) 
			else null
		end::citext as prior_stock,
		a.fromts::date as from_date,
		a.thruts::date as sale_date, 
		split_part(c.vehicletype, '_', 2)::citext as shape, 
		case
		  when split_part(c.vehiclesegment, '_', 2) = 'Extra' then 'Extra_Large'
		  else split_part(c.vehiclesegment, '_', 2) 
		end::citext as size,		
		a.vehicleinventoryitemid, a.vehicleitemid
	from ads.ext_vehicle_inventory_items a
	join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
	left join ads.ext_make_model_classifications c on b.make = c.make
		and b.model = c.model
	where a.fromts::date < current_date 
	  and a.thruts::date > '11/16/2022' -- nothing sold before 11/17
	  and length(b.vin) = 17) aa
where (
		(make in ('chevrolet','gmc','buick','cadillac') and size = 'Large' and shape = 'Pickup') -- and -a.sale_price between 15001 and 25000)
		or
		(make in ('chevrolet','gmc','buick','cadillac') and size = 'Small' and shape = 'SUV') -- and -a.sale_price between 15001 and 25000)
		or
		(make in ('toyota','honda','nissan') and size = 'Small' and shape = 'SUV') -- and -a.sale_price between 15001 and 25000))
    OR coalesce(price, 10) < 15000);

alter table pp.toc_category_analysis_detail_1a
add primary key(stock_number);
alter table pp.toc_category_analysis_detail_1a
add foreign key(category) references pp.toc_categories(category);
comment on table pp.toc_category_analysis_detail_1a is '1 row per stock_number, basic toc categorized vehicles from VehicleInventoryItems where vehicle was in inventory on or after 11/17/2022';

-- remove the prior stock number transaction on intra market wholesale vehicles  
delete 
from pp.toc_category_analysis_detail_1a
where stock_number in (
  select prior_stock
  from pp.toc_category_analysis_detail_1a);


select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'pp'
  and table_name = 'toc_category_analysis_detail_1'

drop table if exists pp.toc_category_analysis_detail_1 cascade;
create unlogged table pp.toc_category_analysis_detail_1 as
select stock_number,vin,model_year,make,model,price,prior_stock,from_date,
	sale_date,age,days_to_sell,shape,size,category,sales_status,source,miles, 
	case
		when (e.miles = 0) or (Length(Trim(e.model_year)) <> 4) or (position('.' in e.model_year) <> 0) or (position('C' in e.model_year) <> 0) or (position('/' in e.model_year) <> 0) or ((e.model_year < '1901') OR (e.model_year > '2050')) then Null
		when 
			e.miles /
				case
					when round((from_date - to_date('09'||'01'||(model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
					else round((from_date - to_date('09'||'01'||(e.model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
				end <= 4000 then 'Freaky Low'
		when 
			e.miles /
				case
					when round((from_date - to_date('09'||'01'||(e.model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
					else round((from_date - to_date('09'||'01'||(e.model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
				end <= 8000 then 'Very Low'
		when 
			e.miles /
				case
					when round((from_date - to_date('09'||'01'||(e.model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
					else round((from_date - to_date('09'||'01'||(e.model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
				end <= 12000 then 'Low'
		when 
			e.miles /
				case
					when round((from_date - to_date('09'||'01'||(e.model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
					else round((from_date - to_date('09'||'01'||(e.model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
				end <= 15000 then 'Average'
		when 
			e.miles /
				case
					when round((from_date - to_date('09'||'01'||(e.model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
					else round((from_date - to_date('09'||'01'||(e.model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
				end <= 18000 then 'High'
		when 
			e.miles /
				case
					when round((from_date - to_date('09'||'01'||(e.model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
					else round((from_date - to_date('09'||'01'||(e.model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
				end <= 22000 then 'Very High'
		when 
			e.miles /
				case
					when round((from_date - to_date('09'||'01'||(e.model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
					else round((from_date - to_date('09'||'01'||(e.model_year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
				end <= 999999 then 'Freaky High'
		else
			'Unknown'																		
	end::citext as mileage_category,
  vehicleitemid,vehicleinventoryitemid			
from (  
	select stock_number,vin,model_year,make,model,price,prior_stock, 
		from_date, sale_date,
		case
			when prior_stock is null then
				case
					when sale_date < current_date then sale_date - from_date 
					else current_date - from_date
				end
			else
				case
					when sale_date < current_date then
						(select sale_date - (select fromts::date from ads.ext_Vehicle_inventory_items where stocknumber = a.prior_stock))
					else (select current_date - (select fromts::date from ads.ext_Vehicle_inventory_items where stocknumber = a.prior_stock))
				end
		end as age,
		days_to_sell,shape,size,category,sales_status,
		case
		  when source is not null then source
		  else 'unknown'
		end::citext as source,aa.miles, a.vehicleitemid,a.vehicleinventoryitemid 
	-- select *
	from pp.toc_category_analysis_detail_1a a
	left join (
		select * 
		from (
			select a.vehicleitemid, a.vehicleitemmileagets, a.value as miles,
				row_number() over (partition by a.vehicleitemid order by vehicleitemmileagets desc) as seq
			from ads.ext_vehicle_item_mileages a
			join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
			join pp.toc_category_analysis_detail_1a c on b.vehicleitemid = c.vehicleitemid) d
		where seq = 1) aa on a.vehicleitemid = aa.vehicleitemid) e;
alter table pp.toc_category_analysis_detail_1
add primary key(stock_number);
alter table pp.toc_category_analysis_detail_1
add foreign key(category) references pp.toc_categories(category);
comment on table pp.toc_category_analysis_detail_1 is '1 row per stock_number, basic toc categorized vehicles from VehicleInventoryItems where vehicle was in inventory on or after 11/17/2022
  added additional attributes: source, days_to _sell, age, miles, mileage_category, figured dates using the prior stocknumber on intra market acquisitions, removed the wholesale transactions';


select * from pp.toc_category_analysis_detail_1


-- 12/17/22
drop table if exists pp.toc_category_analysis_detail_2 cascade;
create unlogged table pp.toc_category_analysis_detail_2 as
-- select d.*, row_number() over (partition by category, stock_number, replen_status order by the_date) as seq_1
-- from ( -- d
-- 	select c.*,
-- 		case 
-- 			when sales_status in ('At Auction','Wholesaled','Sales Buffer','Sold and Not Delivered','Delivered', 'WS Buffer') then 'outflow'
-- 			when sales_status in ('In Transit','Trade Buffer') then 'on the way'
-- 			when sales_status in ('Walk Pending','Raw Materials','Pricing Buffer','Inspection Pending','Pulled') then 'acquired not on the lot'
-- 			when sales_status = 'Available' then 'on lot'
-- 		end::citext as replen_status
-- 	from ( -- c
-- 		select a.the_date,
-- 			b.stock_number, b.from_date, b.sale_date, b.category::citext,
-- 				(select * from ads.get_vii_sales_status_on_date(b.vehicleinventoryitemid, (a.the_date || ' 20:00:00')::timestamp))::citext as sales_status
-- 		from dds.dim_date a
-- 		join pp.toc_category_analysis_detail_1 b on true
-- 			and b.sale_date >= a.the_date -- only show thru sale date
-- 			and a.the_date >= b.from_date  -- don't show blank rows on days prior to acq
-- 		where a.the_date between '11/17/2022' and current_date - 1) c) d
-- order by category, stock_number, the_date;
-- 12/18/22 intra market vehicles had null sales status prior to transfer, generated statuses for prior_stock
-- added seq 2 to disambiguate when a vehicle first shows up in replen_statuses, eg, acq on lot, did that come from on the way?
select d.*, row_number() over (partition by category, stock_number, replen_status order by the_date) as seq_1,
  row_number() over (partition by stock_number order by the_date) as seq_2
from ( -- d
	select c.*,
		case 
			when coalesce(sales_status, sales_status_prior) in ('At Auction','Wholesaled','Sales Buffer','Sold and Not Delivered','Delivered', 'WS Buffer') then 'outflow'
			when coalesce(sales_status, sales_status_prior) in ('In Transit','Trade Buffer') then 'on the way'
			when coalesce(sales_status, sales_status_prior) in ('Walk Pending','Raw Materials','Pricing Buffer','Inspection Pending','Pulled') then 'acquired not on the lot'
			when coalesce(sales_status, sales_status_prior) = 'Available' then 'on lot'
		end::citext as replen_status
	from ( -- c
		select a.the_date,
			b.stock_number, b.from_date, b.sale_date, b.category::citext,
				(select * from ads.get_vii_sales_status_on_date(b.vehicleinventoryitemid, (a.the_date || ' 20:00:00')::timestamp))::citext as sales_status,
				(select * from ads.get_vii_sales_status_on_date((
				  select vehicleinventoryitemid
				  from ads.ext_vehicle_inventory_items
				  where stocknumber = b.prior_stock), (a.the_date || ' 20:00:00')::timestamp))::citext as sales_status_prior
		from dds.dim_date a
		join pp.toc_category_analysis_detail_1 b on true
			and b.sale_date >= a.the_date -- only show thru sale date
			and a.the_date >= b.from_date  -- don't show blank rows on days prior to acq
		where a.the_date between '11/17/2022' and current_date - 1) c) d
order by category, stock_number, the_date;


alter table pp.toc_category_analysis_detail_2
add primary key(the_date, stock_number);
alter table pp.toc_category_analysis_detail_2
add foreign key(category) references pp.toc_categories(category);
alter table pp.toc_category_analysis_detail_2
add foreign key(replen_status) references pp.toc_replenishment_statuses(replen_status);
comment on table pp.toc_category_analysis_detail_2 is '1 row for each vehicle for each day owned, translates tool sales status into replenishment
  statuses, seq_1 attribute numbers the days the vehicle is in a particular status in order by the day, therefore seq_1 = 1 is the first day
  that that vehicle is in that status';


select * from pp.toc_category_analysis_detail_2 where replen_status is null

this should show me how many went into a status on each day
select a.the_date, b.category, b.replen_status, count(*)
from dds.dim_date a
left join pp.toc_category_analysis_detail_2 b on a.the_date = b.the_date
  and b.seq_1 = 1
where a.the_date between '11/17/2022' and current_date - 1  
group by a.the_date, b.category, b.replen_status
order by a.the_date

drop table if exists pp.toc_categories cascade;
create unlogged table pp.toc_categories as
select distinct category::citext from pp.toc_category_analysis_detail_1;
alter table pp.toc_categories add primary key(category);


drop table if exists pp.toc_replenishment_statuses cascade;
create unlogged table pp.toc_replenishment_statuses as
select distinct replen_status::citext from pp.toc_category_analysis_detail_2;
alter table pp.toc_replenishment_statuses add primary key(replen_status);


-- need to look at this with the charts
-- also probably need some context like, the total in each status on each day
-- pk 35-45 on the lot 12/9
-- seq_1 = 1 :: the day in which the vehicle entered the status

-- select * from (  -- this works
select aa.category, aa.the_date, 
  max(bb.the_count) filter (where aa.replen_status = 'on the way') as "on the way",
  max(bb.the_count) filter (where aa.replen_status = 'acquired not on the lot') as "acq not on lot",
  max(bb.the_count) filter (where aa.replen_status = 'on lot') as "on lot",
  max(bb.the_count) filter (where aa.replen_status = 'outflow') as "outflow"
from (
	select * 
	from pp.toc_categories, 
		pp.toc_replenishment_statuses, 
		(SELECT day::date as the_date FROM generate_series(timestamp '2022-11-17', '2022-12-16', '1 day') day) a) aa
left join (
	select a.the_date, b.category::citext, b.replen_status::citext, coalesce(count(*), 0) as the_count
	from dds.dim_date a
	left join pp.toc_category_analysis_detail_2 b on a.the_date = b.the_date
		and b.seq_1 = 1
	where a.the_date between '11/17/2022' and current_date - 1  
	group by a.the_date, b.category, b.replen_status) bb on aa.the_date = bb.the_date
	  and aa.category = bb.category
	  and aa.replen_status = bb.replen_status
group by aa.the_date, aa.category
-- where coalesce("on the way", 0) + coalesce("acq not on lot", 0) + coalesce("on lot", 0) + coalesce("outflow", 0) > 0
order by category, the_date


-- also probably need some context like, the total in each status on each day
-- retail sales
select category, sale_date, count(*)
from (
	select stock_number, category, sale_date
	from pp.toc_category_analysis_detail_2
	where sale_date < current_date
		and sales_status = 'Delivered'
	group by stock_number, category, sale_date) a
group by category,sale_date
order by sale_date, category

select * from pp.toc_category_analysis_detail_2 where category = 'outlet' and the_date = '12/01/2022' order by sale_date

select distinct the_date from pp.toc_category_analysis_detail_2 order by the_date

select the_date, category, replen_status, count(*)
from pp.toc_category_analysis_detail_2
where
  case
    when replen_status = 'outflow' then seq_1 = 1
    else true
  end
group by the_date, category, replen_status
order by the_date, category, replen_status

select * 
from pp.toc_category_analysis_detail_2
where the_date = '11/18/2022'
  and category = 'gm_large_pk_15-25'
order by replen_status  


-- put them altogether
-- reconciling this is turning into a bitch
G44375RB on 12/15/22 shows a retail sale, but no outflow, because it had been in sales buffer since 12/05/22
gm_large_pk_15-25 on 12/5 1 acq on lot, was that a newly added vehicle or /// removed subtracting outflow from the total, total is just the sum of the other statuses
gm_large_pk_15-25 on 11/22  G45471A 1st day it becomes acq nmot on lot BUUUT seq_2 = 6, so it is not a new vehicle added to the total
  only becomes an addition to total if it is the 1st appearance of the vehicle, this one was in on the way for 5 days
adding 'acquired not on the lot new' as using that to calculate change took care of 11/22
select * 
from pp.toc_replenishment_statuses
insert into pp.toc_replenishment_statuses values ('acquired not on the lot new')

gm_large_pk_15-25 12/8 total on the way shows 1, none in on the way  - typo

gm_large_pk_25-35 12/2  left shows only added 1 on lot   right shows total acq not on lot 3->2, total on lot 4->6, total 10 increase of 1 since 12/1
		a vehicle went from outflow (sales buffer) to on lot 

need someway to identify vehicles new to the system, that would be seq_2

select *, 
  coalesce("on the way", 0) + coalesce("acq not on lot new", 0) - coalesce("outflow", 0) as the_change,
  coalesce("total on the way", 0) + coalesce("total acq not on lot", 0)  + coalesce("total on lot", 0) as the_total -- coalesce("total outflow", 0) as the_total,
-- coalesce("outflow", 0) + coalesce("on lot", 0) + coalesce("acq not on lot", 0) + coalesce("on the way", 0)
from (
	select aaa.*, bbb.retail_sales, ccc."total on the way", ccc."total acq not on lot", ccc."total on lot", ccc."total outflow"
	from ( -- aaa by day/category, the number of vehicles that become each status
		select aa.category, aa.the_date, 
			max(bb.the_count) filter (where aa.replen_status = 'on the way') as "on the way",
			max(bb.the_count) filter (where aa.replen_status = 'acquired not on the lot') as "acq not on lot",
			max(bb.the_count) filter (where aa.replen_status = 'acquired not on the lot new') as "acq not on lot new",
			max(bb.the_count) filter (where aa.replen_status = 'on lot') as "on lot",
			max(bb.the_count) filter (where aa.replen_status = 'outflow') as "outflow"
		from (
			select * 
			from pp.toc_categories, 
				pp.toc_replenishment_statuses, 
				(SELECT day::date as the_date FROM generate_series(timestamp '2022-11-17', '2022-12-16', '1 day') day) a) aa
		left join (
			select x.the_date, x.category::citext, x.replen_status::citext, coalesce(count(*), 0) as the_count
			from (
				select a.the_date, b.category::citext, 
					case
						when seq_2 = 1 and b.replen_status = 'acquired not on the lot' then (b.replen_status || ' new')::citext
						else b.replen_status::citext
					end as replen_status
				from dds.dim_date a
				left join pp.toc_category_analysis_detail_2 b on a.the_date = b.the_date
					and b.seq_1 = 1
				where a.the_date between '11/17/2022' and current_date - 1) x 
			group by x.the_date, x.category, x.replen_status) bb on aa.the_date = bb.the_date
				and aa.category = bb.category
				and aa.replen_status = bb.replen_status
		group by aa.the_date, aa.category) aaa
		
	left join ( -- retail sales
		select category, sale_date, count(*) as retail_sales
		from (
			select stock_number, category, sale_date
			from pp.toc_category_analysis_detail_2
			where sale_date < current_date
				and sales_status = 'Delivered'
			group by stock_number, category, sale_date) a
		group by category,sale_date) bbb on aaa.the_date = bbb.sale_date
			and aaa.category = bbb.category	
	left join ( -- totals
		select the_date, category, 
			count(*) filter (where replen_status = 'on the way') as "total on the way",
			count(*) filter (where replen_status = 'acquired not on the lot') as "total acq not on lot",
			count(*) filter (where replen_status = 'on lot') as "total on lot",
			count(*) filter (where replen_status = 'outflow') as "total outflow"
		from pp.toc_category_analysis_detail_2
		where
			case
				when replen_status = 'outflow' then seq_1 = 1
				else true
			end
		group by the_date, category) ccc on aaa.the_date = ccc.the_date
			and aaa.category = ccc.category		
) x
-- where coalesce("outflow", 0) + coalesce("on lot", 0) + coalesce("acq not on lot", 0) + 
-- 		coalesce("on the way", 0) <> 0 
-- coalesce("total outflow", 0) + coalesce("total on lot", 0) + 
-- 		coalesce("total acq not on lot", 0) + coalesce("total on the way", 0) <> 0 	  
order by category,the_date	


-- greg liked this query for looking at details
select * 
from pp.toc_category_analysis_detail_2
where category = 'gm_large_pk_15-25'
  and the_date between '11/17/2022' and '12/09/2022'
order by the_date, stock_number, replen_status


-- this may have potential
select category, the_date, 
	array_agg(stock_number order by stock_number) filter (where replen_status = 'on the way') as "on the way",
	array_agg(stock_number order by stock_number) filter (where replen_status = 'acquired not on the lot') as "acq not on lot",
	array_agg(stock_number order by stock_number) filter (where replen_status = 'on lot') as "on lot",
	array_agg(stock_number order by stock_number) filter (where replen_status = 'outflow') as "outflow"
from pp.toc_category_analysis_detail_2
group by category, the_date 
order by category, the_date


*********** narrow it down to lot and retail sales

select * from pp.toc_category_analysis_detail_1
select * from pp.toc_category_analysis_detail_2











-- 12/18
look at this


and try to make the math work for the above statuses query



time line

