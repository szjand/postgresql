﻿

select a.stocknumber, a.vehicleinventoryitemid, b.make, b.model, a.fromts::date, a.thruts::date, c.*
from ads.ext_vehicle_inventory_items a
join  ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
  and make in ('buick','cadillac','chevrolet','gmc')
join ads.ext_make_model_classifications c on b.make = c.make and b.model = c.model
  and c.vehiclesegment = 'VehicleSegment_Large'
  and c.vehicletype = 'VehicleType_Pickup'
where a.fromts::date < '10/01/2022'
  and a.thruts::date > '09/30/2022'
 limit 30 


select the_date aa, count(*)
from dds.dim_date aa
left join (
select a.stocknumber, a.vehicleinventoryitemid, b.make, b.model, a.fromts::date as from_Date, a.thruts::date as thru_date,
  ads.get_current_best_price_by_stock_number_used(a.stocknumber) as price
from ads.ext_vehicle_inventory_items a
join  ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
  and make in ('buick','cadillac','chevrolet','gmc')
join ads.ext_make_model_classifications c on b.make = c.make and b.model = c.model
  and c.vehiclesegment = 'VehicleSegment_Large'
  and c.vehicletype = 'VehicleType_Pickup'
where a.fromts::date < '10/01/2022'
  and a.thruts::date > '09/30/2022') bb on aa.the_date between bb.from_date and bb.thru_date
where aa.year_month = 202209 
  and bb.price between 15000 and 25000
group by aa.the_date



select the_date aa, count(*)
from dds.dim_date aa
left join (
select a.stocknumber, a.vehicleinventoryitemid, b.make, b.model, a.fromts::date as from_Date, a.thruts::date as thru_date,
  ads.get_current_best_price_by_stock_number_used(a.stocknumber) as price

from ads.ext_vehicle_inventory_items a
join  ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
  and make in ('buick','cadillac','chevrolet','gmc')
join ads.ext_make_model_classifications c on b.make = c.make and b.model = c.model
  and c.vehiclesegment = 'VehicleSegment_Large'
  and c.vehicletype = 'VehicleType_Pickup'
where a.fromts::date < '10/01/2022'
  and a.thruts::date > '09/30/2022') bb on aa.the_date between bb.from_date and bb.thru_date
where aa.year_month = 202209 
  and bb.price between 15000 and 25000
group by aa.the_date




		select distinct c.amount::integer as best_price
		from ads.ext_vehicle_inventory_items a
		join ads.ext_vehicle_pricings b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
		join ads.ext_vehicle_pricing_details c on b.vehiclepricingid = c.vehiclepricingid
			and c.typ = 'VehiclePricingDetail_BestPrice'
		where stocknumber = 'G45265A'
		  and vehiclepricingts between a.fromts and a.thruts  
--  giving me ERROR:  more than one row returned by a subquery used as an expression
select a.stocknumber, a.vehicleinventoryitemid, b.make, b.model, a.fromts::date as from_Date, a.thruts::date as thru_date,
  ads.get_current_best_price_by_stock_number_used(a.stocknumber) as price,
(
		select distinct o.amount::integer as best_price
		from ads.ext_vehicle_inventory_items m
		join ads.ext_vehicle_pricings n on m.vehicleinventoryitemid = n.vehicleinventoryitemid
		join ads.ext_vehicle_pricing_details o on n.vehiclepricingid = o.vehiclepricingid
			and o.typ = 'VehiclePricingDetail_BestPrice'
		where m.stocknumber = a.stocknumber
		  and n.vehiclepricingts between m.fromts and m.thruts)  

from ads.ext_vehicle_inventory_items a
join  ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
  and make in ('buick','cadillac','chevrolet','gmc')
join ads.ext_make_model_classifications c on b.make = c.make and b.model = c.model
  and c.vehiclesegment = 'VehicleSegment_Large'
  and c.vehicletype = 'VehicleType_Pickup'
where a.fromts::date < '10/01/2022'
  and a.thruts::date > '09/30/2022'		 


select a.stocknumber, a.vehicleinventoryitemid, b.make, b.model, a.fromts::date as from_Date, a.thruts::date as thru_date,
  ads.get_current_best_price_by_stock_number_used(a.stocknumber) as price, d.best_price
from ads.ext_vehicle_inventory_items a
join  ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
  and make in ('buick','cadillac','chevrolet','gmc')
join ads.ext_make_model_classifications c on b.make = c.make and b.model = c.model
  and c.vehiclesegment = 'VehicleSegment_Large'
  and c.vehicletype = 'VehicleType_Pickup'
left join lateral(  
		select distinct m.stocknumber, o.amount::integer as best_price
		from ads.ext_vehicle_inventory_items m
		join ads.ext_vehicle_pricings n on m.vehicleinventoryitemid = n.vehicleinventoryitemid
		join ads.ext_vehicle_pricing_details o on n.vehiclepricingid = o.vehiclepricingid
			and o.typ = 'VehiclePricingDetail_BestPrice'
		where m.stocknumber = a.stocknumber
		  and n.vehiclepricingts between m.fromts and m.thruts) d on a.stocknumber = d.stocknumber
where a.fromts::date < '10/01/2022'
  and a.thruts::date > '09/30/2022'	 


		select distinct o.amount::integer as best_price
		from ads.ext_vehicle_inventory_items m
		join ads.ext_vehicle_pricings n on m.vehicleinventoryitemid = n.vehicleinventoryitemid
		join ads.ext_vehicle_pricing_details o on n.vehiclepricingid = o.vehiclepricingid
			and o.typ = 'VehiclePricingDetail_BestPrice'
		where m.stocknumber = a.stocknumber
		  and n.vehiclepricingts between m.fromts and m.thruts   