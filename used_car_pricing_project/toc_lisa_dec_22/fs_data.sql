﻿feeling pretty good about this as sales data from accounting


drop table if exists pp.toc_new_categories_1 cascade;
create unlogged table pp.toc_new_categories_1 as
		select distinct b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
			a.control, a.amount,
			case when a.amount < 0 then 1 else -1 end as unit_count
		from fin.fact_gl a
		inner join dds.dim_date b on a.date_key = b.date_key
			and b.year_month between 202012 and 202211-----------------------------------------------------------------------------
		inner join fin.dim_account c on a.account_key = c.account_key
			and c.current_row
	-- add journal
		inner join fin.dim_journal aa on a.journal_key = aa.journal_key
			and aa.journal_code in ('VSN','VSU')
		inner join ( -- d: fs gm_account page/line/acct description
			select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
			from fin.fact_fs a
			inner join fin.dim_fs b on a.fs_key = b.fs_key
				and b.year_month between 202012 and 202211  ---------------------------------------------------------------------
				and b.page = 16 and b.line between 1 and 5 -- used cars, retail only
			inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
			inner join fin.dim_account e on d.gl_account = e.account
				and e.account_type_code = '4'
				and e.current_row
			inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
		where a.post_status = 'Y';



-- this takes care of those with more than 2 rows per control
-- only 19 with 2 rows, ignoring those
drop table if exists pp.toc_new_categories_2 cascade;
create unlogged table pp.toc_new_categories_2 as
select control as stock_number, max(year_month) filter (where amount < 0) as year_month, 
(min(-amount::integer) filter (where amount < 0))  as sale_price
from (
	select a.*, row_number() over (partition by a.control order by a.year_month desc) as seq
	from wtf a
	join (
		select control
		from pp.toc_new_categories_1
		group by control
		having count(*) > 1) b on a.control = b.control		
	order by a.control, a.year_month) aa
group by control having count(*) > 2
union
select a.control as stock_number, a.year_month, -a.amount::integer as sale_price
from wtf a
join (
  select control
  from pp.toc_new_categories_1
  group by control
  having count(*) = 1) b on a.control = b.control;
alter table pp.toc_new_categories_2
add primary key (stock_number);


-- only 5400
select a.stock_number
from pp.toc_new_categories_2
join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber

select a.stock_number  -- 5692
from pp.toc_new_categories_2 a
join arkona.ext_inpmast b on a.stock_number = b.inpmast_stock_number

-- a few dup stock numbers, a few missing vins, but close
-- can clean these up salvage some vins
-- 1 inner join
select distinct a.*, b.inpmast_vin
from pp.toc_new_categories_2 a
join arkona.xfm_inpmast b on a.stock_number = b.inpmast_stock_number  
where not exists (
  select 1
  from arkona.ext_inpmast 
  where inpmast_stock_number = a.stock_number)
and b.inpmast_vin not in ('3GTP9EEL3KG203600','3GYFK62877G268294','1C4RJFBG5JC294853','2HKRW2H58JH649440')

-- used these vins above
select stock_number
from (
select distinct a.*, b.inpmast_vin
from pp.toc_new_categories_2 a
join arkona.xfm_inpmast b on a.stock_number = b.inpmast_stock_number  
where not exists (
  select 1
  from arkona.ext_inpmast 
  where inpmast_stock_number = a.stock_number)
and b.inpmast_vin not in ('3GTP9EEL3KG203600')) aa
group by stock_number having count(*) > 1
select control from pp.new_categories_2 group by control having count(*) > 1

-- so, now, put them together
drop table if exists pp.toc_new_categories_3 cascade;
create unlogged table pp.toc_new_categories_3 as
select a.*, b.inpmast_vin as vin  --5692
from pp.toc_new_categories_2 a
join arkona.ext_inpmast b on a.stock_number = b.inpmast_stock_number
union
select distinct a.*, b.inpmast_vin as vin  -- 200
from pp.toc_new_categories_2 a
join arkona.xfm_inpmast b on a.stock_number = b.inpmast_stock_number  
where not exists (
  select 1
  from arkona.ext_inpmast 
  where inpmast_stock_number = a.stock_number)
and b.inpmast_vin not in ('3GTP9EEL3KG203600','3GYFK62877G268294','1C4RJFBG5JC294853','2HKRW2H58JH649440');
alter table pp.toc_new_categories_3
add primary key(stock_number);  

-- ******** time for make & model ***************
-- 344 not in vi
-- add chr.describe_vehicle and we are down to 16, good enough
-- limits to categorized vehicles, used this for sourcing
drop table if exists pp.toc_new_categories_4 cascade;
create unlogged table pp.toc_new_categories_4 as
select e.stock_number, e.year_month, e.sale_price, e.vin, coalesce(e.make, e.chr_make, 'unkown') as make, f.vehicletype, f.vehiclesegment,
	( -- prior stock number on intra market
		select stocknumber
		FROM ads.ext_Vehicle_Inventory_Items
		WHERE VehicleItemID = (
			select vehicleitemid
			from ads.ext_vehicle_items
			where vin = e.vin
			  and right(e.stock_number, 1) in ('G','T','H'))
	  order by fromts desc
	  limit 1 offset 1) as prior_stock
from (
	select distinct a.*, b.make, b.model, 
		(d->'division'->>'$value')::citext as chr_make,
		(d->'model'->>'$value'::citext)::citext as chr_model
	from pp.toc_new_categories_3 a
	left join ads.ext_vehicle_items b on a.vin = b.vin
	left join chr.describe_vehicle c on a.vin = c.vin
	left join jsonb_array_elements(c.response->'style') as d on true) e
left join ads.ext_make_model_classifications f on coalesce(e.make, e.chr_make, 'xxx') = f.make
  and coalesce(e.model, e.chr_model, 'xxx') = f.model
where (
		( coalesce(e.make, e.chr_make, 'xxx') in ('chevrolet','gmc','buick','cadillac') and f.vehiclesegment = 'VehicleSegment_Large' and f.vehicletype = 'VehicleType_Pickup' and e.sale_price between 15001 and 25000)
		or
		( coalesce(e.make, e.chr_make, 'xxx') in ('chevrolet','gmc','buick','cadillac') and f.vehiclesegment = 'VehicleSegment_Large' and f.vehicletype = 'VehicleType_Pickup' and e.sale_price between 25001 and 35000)
		OR
		( coalesce(e.make, e.chr_make, 'xxx') in ('chevrolet','gmc','buick','cadillac') and f.vehiclesegment = 'VehicleSegment_Large' and f.vehicletype = 'VehicleType_Pickup' and e.sale_price between 35001 and 45000)
		OR
		( coalesce(e.make, e.chr_make, 'xxx') in ('chevrolet','gmc','buick','cadillac') and f.vehiclesegment = 'VehicleSegment_Large' and f.vehicletype = 'VehicleType_Pickup' and e.sale_price between 45001 and 145000)
		or
		( coalesce(e.make, e.chr_make, 'xxx') in ('chevrolet','gmc','buick','cadillac') and f.vehiclesegment = 'VehicleSegment_Small' and f.vehicletype = 'VehicleType_SUV' and e.sale_price between 15001 and 25000)
		or
		( coalesce(e.make, e.chr_make, 'xxx') in ('chevrolet','gmc','buick','cadillac') and f.vehiclesegment = 'VehicleSegment_Small' and f.vehicletype = 'VehicleType_SUV' and e.sale_price between 25001 and 125000)
		or
		( coalesce(e.make, e.chr_make, 'xxx') in ('toyota','honda','nissan') and f.vehiclesegment = 'VehicleSegment_Small' and f.vehicletype = 'VehicleType_SUV' and e.sale_price between 15001 and 25000)
		or
		( coalesce(e.make, e.chr_make, 'xxx') in ('toyota','honda','nissan') and f.vehiclesegment = 'VehicleSegment_Small' and f.vehicletype = 'VehicleType_SUV' and e.sale_price between 25001 and 125000)
		or
		(e.sale_price < 15000))
and f.vehicletype is not null;

select * from pp.toc_new_categories_4


---------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------


drop table if exists pp.toc_new_categories_4 cascade;
create unlogged table pp.toc_new_categories_4 as
select e.stock_number, e.year_month, e.sale_price, e.vin, coalesce(e.make, e.chr_make, 'unkown') as make, f.vehicletype, f.vehiclesegment,
	( -- prior stock number on intra market
		select stocknumber
		FROM ads.ext_Vehicle_Inventory_Items
		WHERE VehicleItemID = (
			select vehicleitemid
			from ads.ext_vehicle_items
			where vin = e.vin
			  and right(e.stock_number, 1) in ('G','T','H'))
	  order by fromts desc
	  limit 1 offset 1) as prior_stock
from (
	select distinct a.*, b.make, b.model, 
		(d->'division'->>'$value')::citext as chr_make,
		(d->'model'->>'$value'::citext)::citext as chr_model
	from pp.toc_new_categories_3 a
	left join ads.ext_vehicle_items b on a.vin = b.vin
	left join chr.describe_vehicle c on a.vin = c.vin
	left join jsonb_array_elements(c.response->'style') as d on true) e
left join ads.ext_make_model_classifications f on coalesce(e.make, e.chr_make, 'xxx') = f.make
  and coalesce(e.model, e.chr_model, 'xxx') = f.model
where (
		( coalesce(e.make, e.chr_make, 'xxx') in ('chevrolet','gmc','buick','cadillac') and f.vehiclesegment = 'VehicleSegment_Large' and f.vehicletype = 'VehicleType_Pickup' and e.sale_price between 15001 and 25000)
		or
		( coalesce(e.make, e.chr_make, 'xxx') in ('chevrolet','gmc','buick','cadillac') and f.vehiclesegment = 'VehicleSegment_Large' and f.vehicletype = 'VehicleType_Pickup' and e.sale_price between 25001 and 35000)
		OR
		( coalesce(e.make, e.chr_make, 'xxx') in ('chevrolet','gmc','buick','cadillac') and f.vehiclesegment = 'VehicleSegment_Large' and f.vehicletype = 'VehicleType_Pickup' and e.sale_price between 35001 and 45000)
		OR
		( coalesce(e.make, e.chr_make, 'xxx') in ('chevrolet','gmc','buick','cadillac') and f.vehiclesegment = 'VehicleSegment_Large' and f.vehicletype = 'VehicleType_Pickup' and e.sale_price between 45001 and 145000)
		or
		( coalesce(e.make, e.chr_make, 'xxx') in ('chevrolet','gmc','buick','cadillac') and f.vehiclesegment = 'VehicleSegment_Small' and f.vehicletype = 'VehicleType_SUV' and e.sale_price between 15001 and 25000)
		or
		( coalesce(e.make, e.chr_make, 'xxx') in ('chevrolet','gmc','buick','cadillac') and f.vehiclesegment = 'VehicleSegment_Small' and f.vehicletype = 'VehicleType_SUV' and e.sale_price between 25001 and 125000)
		or
		( coalesce(e.make, e.chr_make, 'xxx') in ('toyota','honda','nissan') and f.vehiclesegment = 'VehicleSegment_Small' and f.vehicletype = 'VehicleType_SUV' and e.sale_price between 15001 and 25000)
		or
		( coalesce(e.make, e.chr_make, 'xxx') in ('toyota','honda','nissan') and f.vehiclesegment = 'VehicleSegment_Small' and f.vehicletype = 'VehicleType_SUV' and e.sale_price between 25001 and 125000)
		or
		(e.sale_price < 15000))
and f.vehicletype is not null;

-- 3016
select * from pp.toc_new_categories_4  limit 100


-- 2837 rows total
-- currently non categorized vehicles
-- this is an interim table not an end product
-- this is good, 16 bad rows
-- dec 2020 thru nov 2022
-- total 2819
-- left out L stocknumbers, count now 2628
drop table if exists pp.toc_new_categories_5 cascade;
create unlogged table pp.toc_new_categories_5 as
select e.stock_number, e.year_month, e.sale_price, e.vin, e.make, e.chr_make, 
  e.model, e.chr_model, 
	f.vehicletype, f.vehiclesegment,
	( -- prior stock number on intra market
		select stocknumber
		FROM ads.ext_Vehicle_Inventory_Items
		WHERE VehicleItemID = (
			select vehicleitemid
			from ads.ext_vehicle_items
			where vin = e.vin
			  and right(e.stock_number, 1) in ('G','T','H'))
	  order by fromts desc
	  limit 1 offset 1) as prior_stock,
	coalesce(g.shape, h.shape) as shape, coalesce(g.size, h.size) as size
from (
	select distinct a.*, b.make, b.model, 
		(d->'division'->>'$value')::citext as chr_make,
		(d->'model'->>'$value'::citext)::citext as chr_model
	from pp.toc_new_categories_3 a
	left join ads.ext_vehicle_items b on a.vin = b.vin
	left join chr.describe_vehicle c on a.vin = c.vin
	left join jsonb_array_elements(c.response->'style') as d on true) e
left join ads.ext_make_model_classifications f on coalesce(e.make, e.chr_make, 'xxx') = f.make
  and coalesce(e.model, e.chr_model, 'xxx') = f.model
left join veh.shape_size_classifications g on e.make = g.make
  and e.model = g.model
left join veh.shape_size_classifications h on e.chr_make = h.make
  and e.chr_model = h.model
where (
		not ( coalesce(e.make, e.chr_make, 'xxx') in ('chevrolet','gmc','buick','cadillac') and f.vehiclesegment = 'VehicleSegment_Large' and f.vehicletype = 'VehicleType_Pickup' and e.sale_price between 15001 and 25000)
		and
		not ( coalesce(e.make, e.chr_make, 'xxx') in ('chevrolet','gmc','buick','cadillac') and f.vehiclesegment = 'VehicleSegment_Large' and f.vehicletype = 'VehicleType_Pickup' and e.sale_price between 25001 and 35000)
		and
		not ( coalesce(e.make, e.chr_make, 'xxx') in ('chevrolet','gmc','buick','cadillac') and f.vehiclesegment = 'VehicleSegment_Large' and f.vehicletype = 'VehicleType_Pickup' and e.sale_price between 35001 and 45000)
		and
		not ( coalesce(e.make, e.chr_make, 'xxx') in ('chevrolet','gmc','buick','cadillac') and f.vehiclesegment = 'VehicleSegment_Large' and f.vehicletype = 'VehicleType_Pickup' and e.sale_price between 45001 and 145000)
		and
		not ( coalesce(e.make, e.chr_make, 'xxx') in ('chevrolet','gmc','buick','cadillac') and f.vehiclesegment = 'VehicleSegment_Small' and f.vehicletype = 'VehicleType_SUV' and e.sale_price between 15001 and 25000)
		and
		not ( coalesce(e.make, e.chr_make, 'xxx') in ('chevrolet','gmc','buick','cadillac') and f.vehiclesegment = 'VehicleSegment_Small' and f.vehicletype = 'VehicleType_SUV' and e.sale_price between 25001 and 125000)
		and
		not ( coalesce(e.make, e.chr_make, 'xxx') in ('toyota','honda','nissan') and f.vehiclesegment = 'VehicleSegment_Small' and f.vehicletype = 'VehicleType_SUV' and e.sale_price between 15001 and 25000)
		and
		not ( coalesce(e.make, e.chr_make, 'xxx') in ('toyota','honda','nissan') and f.vehiclesegment = 'VehicleSegment_Small' and f.vehicletype = 'VehicleType_SUV' and e.sale_price between 25001 and 125000)
    and e.sale_price > 15000)
  and vin not in ('1GC2KVEG8FZ139657')
  and coalesce(g.shape, h.shape) is not null
  and right(stock_number, 1) <> 'L';
alter table pp.toc_new_categories_5
add primary key(stock_number);


select stock_number, vin, coalesce(make, chr_make) as make,
  coalesce(model, chr_model) as model, shape, size, sale_price, year_month
from pp.toc_new_categories_5
order by shape, sale_price

-- categories
select stock_number, vin, coalesce(make, chr_make) as make,
  coalesce(model, chr_model) as model, shape, size, sale_price, year_month,
  case
    when shape = 'car' and sale_price between 15000 and 19999 then 'car 15-20'
    when shape = 'car' and sale_price between 20000 and 24999 then 'car 15-25'
    when shape = 'car' and sale_price >= 25000 then 'car 25+'
    when shape = 'van' then 'minivans'
    when shape = 'pickup' then 'other pickups'
    when shape = 'suv' then 'other SUVs'
  end as category
from pp.toc_new_categories_5
order by shape, sale_price;

-- category sales by month
select category, 
  count(stock_number) filter (where year_month = 202012) as "dec 20",
  count(stock_number) filter (where year_month = 202101) as "jan 21",
  count(stock_number) filter (where year_month = 202102) as "feb 21",
  count(stock_number) filter (where year_month = 202103) as "mar 21",
  count(stock_number) filter (where year_month = 202104) as "apr 21",
  count(stock_number) filter (where year_month = 202105) as "may 21",
  count(stock_number) filter (where year_month = 202106) as "jun 21",
  count(stock_number) filter (where year_month = 202107) as "jul 21",
  count(stock_number) filter (where year_month = 202108) as "aug 21",
  count(stock_number) filter (where year_month = 202109) as "sep 21",
  count(stock_number) filter (where year_month = 202110) as "oct 21",
  count(stock_number) filter (where year_month = 202111) as "nov 21",
  count(stock_number) filter (where year_month = 202112) as "dec 21",
  count(stock_number) filter (where year_month = 202201) as "jan 22",
  count(stock_number) filter (where year_month = 202202) as "feb 22",
  count(stock_number) filter (where year_month = 202203) as "mar 22",
  count(stock_number) filter (where year_month = 202204) as "apr 22",
  count(stock_number) filter (where year_month = 202205) as "may 22",
  count(stock_number) filter (where year_month = 202206) as "jun 22",
  count(stock_number) filter (where year_month = 202207) as "jul 22",
  count(stock_number) filter (where year_month = 202208) as "aug 22",
  count(stock_number) filter (where year_month = 202209) as "sep 22",
  count(stock_number) filter (where year_month = 202210) as "oct 22",
  count(stock_number) filter (where year_month = 202211) as "nov 22",
  count(stock_number) as total
from (  
	select stock_number, vin, coalesce(make, chr_make) as make,
		coalesce(model, chr_model) as model, shape, size, sale_price, year_month,
		case
			when shape = 'car' and sale_price between 15000 and 19999 then 'car 15-20'
			when shape = 'car' and sale_price between 20000 and 24999 then 'car 15-25'
			when shape = 'car' and sale_price >= 25000 then 'car 25+'
			when shape = 'van' then 'minivans'
			when shape = 'pickup' then 'other pickups'
			when shape = 'suv' then 'other SUVs'
		end as category
	from pp.toc_new_categories_5) a
group by category	
order by category;


-- suv's
select coalesce(make, chr_make) as make,
  coalesce(model, chr_model) as model, count(*)
from pp.toc_new_categories_5
where shape = 'suv'  
group by coalesce(make, chr_make), coalesce(model, chr_model)
order by count(*) desc;

-- minivans
select coalesce(make, chr_make) as make,
  coalesce(model, chr_model) as model, count(*)
from pp.toc_new_categories_5
where shape = 'van'  
group by coalesce(make, chr_make), coalesce(model, chr_model)
order by count(*) desc;

-- other pickups
select coalesce(make, chr_make) as make,
  coalesce(model, chr_model) as model, count(*)
from pp.toc_new_categories_5
where shape = 'pickup'  
group by coalesce(make, chr_make), coalesce(model, chr_model)
order by count(*) desc;

-- cars
select coalesce(make, chr_make) as make,
  coalesce(model, chr_model) as model, count(*)
from pp.toc_new_categories_5
where shape = 'car'  
group by coalesce(make, chr_make), coalesce(model, chr_model)
order by count(*) desc;


select avg(sale_price) 
from pp.toc_new_categories_5
where shape = 'pickup'

select percentile_cont(0.5) within group (order by sale_price) as median
from pp.toc_new_categories_5
where shape = 'pickup'

select *
from pp.toc_new_categories_5
where model = 'murano'

-- 
-- select 
-- 	category, count(*)
-- from (
-- select c.stocknumber, a.make, a.model, a.shape, a.size, category,
--   ads.get_current_best_price_by_stock_number_used(c.stocknumber) as price
-- from (
-- 	select coalesce(make, chr_make) as make, coalesce(model, chr_model) as model, shape, size,
-- 		max(case
-- 			when shape = 'car' and coalesce(sale_price, 0) between 15000 and 19999 then 'car 15-20'
-- 			when shape = 'car' and coalesce(sale_price, 0) between 20000 and 24999 then 'car 15-25'
-- 			when shape = 'car' and coalesce(sale_price, 0) >= 25000 then 'car 25+'
-- 			when shape = 'van' then 'minivans'
-- 			when shape = 'pickup' then 'other pickups'
-- 			when shape = 'suv' then 'other SUVs'
-- 		end) as category
-- 	from pp.toc_new_categories_5
-- 	group by coalesce(make, chr_make), coalesce(model, chr_model), shape, size) a
-- join ads.ext_vehicle_items b on a.make = b.make
--   and a.model = b.model
-- join ads.ext_vehicle_inventory_items c on b.vehicleitemid = c.vehicleitemid    
--   and c.fromts::date < current_date
--   and c.thruts::date > current_date) a 
--  group by category
-- 
-- 
-- 
-- select c.stocknumber, a.make, a.model, a.shape, a.size, 
--   ads.get_current_best_price_by_stock_number_used(c.stocknumber) as price
-- from (
-- 	select coalesce(make, chr_make) as make, coalesce(model, chr_model) as model, shape, size
-- 	from pp.toc_new_categories_5
-- 	group by coalesce(make, chr_make), coalesce(model, chr_model), shape, size) a
-- join ads.ext_vehicle_items b on a.make = b.make
--   and a.model = b.model
-- join ads.ext_vehicle_inventory_items c on b.vehicleitemid = c.vehicleitemid    
--   and c.fromts::date < current_date
--   and c.thruts::date > current_date 


-- this is a better shot at current inventory
select category, count(*) 
from (
select *,
  case
    when shape = 'car' and sale_price between 15000 and 19999 then 'car 15-20'
    when shape = 'car' and sale_price between 20000 and 24999 then 'car 20-25'
    when shape = 'car' and sale_price >= 25000 then 'car 25+'
    when shape = 'van' then 'minivans'
    when shape = 'pickup' then 'other pickups'
    when shape = 'suv' then 'other SUVs'
  end as category
from (  
	select e.stocknumber, e.vin, e.make, e.chr_make, 
		e.model, e.chr_model, 
		f.vehicletype, f.vehiclesegment,
		coalesce(g.shape, h.shape) as shape, coalesce(g.size, h.size) as size, sale_price
	from (
		select distinct a.stocknumber, b.make, b.model, 
			(d->'division'->>'$value')::citext as chr_make,
			(d->'model'->>'$value'::citext)::citext as chr_model, b.vin,
			ads.get_current_best_price_by_stock_number_used(a.stocknumber) as sale_price
		from ads.ext_vehicle_inventory_items a
		left join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
		left join chr.describe_vehicle c on b.vin = c.vin
		left join jsonb_array_elements(c.response->'style') as d on true
		where a.fromts::date < current_date
			and a.thruts::date > current_date) e

	left join ads.ext_make_model_classifications f on coalesce(e.make, e.chr_make, 'xxx') = f.make
		and coalesce(e.model, e.chr_model, 'xxx') = f.model
	left join veh.shape_size_classifications g on e.make = g.make
		and e.model = g.model
	left join veh.shape_size_classifications h on e.chr_make = h.make
		and e.chr_model = h.model
	where (
			not ( coalesce(e.make, e.chr_make, 'xxx') in ('chevrolet','gmc','buick','cadillac') and f.vehiclesegment = 'VehicleSegment_Large' and f.vehicletype = 'VehicleType_Pickup' and e.sale_price between 15001 and 25000)
			and
			not ( coalesce(e.make, e.chr_make, 'xxx') in ('chevrolet','gmc','buick','cadillac') and f.vehiclesegment = 'VehicleSegment_Large' and f.vehicletype = 'VehicleType_Pickup' and e.sale_price between 25001 and 35000)
			and
			not ( coalesce(e.make, e.chr_make, 'xxx') in ('chevrolet','gmc','buick','cadillac') and f.vehiclesegment = 'VehicleSegment_Large' and f.vehicletype = 'VehicleType_Pickup' and e.sale_price between 35001 and 45000)
			and
			not ( coalesce(e.make, e.chr_make, 'xxx') in ('chevrolet','gmc','buick','cadillac') and f.vehiclesegment = 'VehicleSegment_Large' and f.vehicletype = 'VehicleType_Pickup' and e.sale_price between 45001 and 145000)
			and
			not ( coalesce(e.make, e.chr_make, 'xxx') in ('chevrolet','gmc','buick','cadillac') and f.vehiclesegment = 'VehicleSegment_Small' and f.vehicletype = 'VehicleType_SUV' and e.sale_price between 15001 and 25000)
			and
			not ( coalesce(e.make, e.chr_make, 'xxx') in ('chevrolet','gmc','buick','cadillac') and f.vehiclesegment = 'VehicleSegment_Small' and f.vehicletype = 'VehicleType_SUV' and e.sale_price between 25001 and 125000)
			and
			not ( coalesce(e.make, e.chr_make, 'xxx') in ('toyota','honda','nissan') and f.vehiclesegment = 'VehicleSegment_Small' and f.vehicletype = 'VehicleType_SUV' and e.sale_price between 15001 and 25000)
			and
			not ( coalesce(e.make, e.chr_make, 'xxx') in ('toyota','honda','nissan') and f.vehiclesegment = 'VehicleSegment_Small' and f.vehicletype = 'VehicleType_SUV' and e.sale_price between 25001 and 125000)
			and e.sale_price > 15000)
		and vin not in ('1GC2KVEG8FZ139657')
		and coalesce(g.shape, h.shape) is not null
		and right(stocknumber, 1) <> 'L') x order by category, stocknumber
		) y
group by category   