﻿select * from pp.toc_category_analysis_detail_2 limit 10

select * 
from pp.toc_category_analysis_detail_2
where from_date >= '11/17/2022'
order by stock_number, the_date


select a.stock_number, a.vehicleinventoryitemid, a.from_date, a.sale_date, b.status, b.fromts, b.thruts 
from pp.toc_category_analysis_detail_1 a
left join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and b.status in ('BodyReconProcess_InProcess','MechanicalReconProcess_InProcess')
order by a.stock_number, b.fromts desc  

select a.stock_number, a.vehicleinventoryitemid, min(b.fromts), max(b.thruts)
from pp.toc_category_analysis_detail_1 a
left join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and b.status in ('BodyReconProcess_InProcess','MechanicalReconProcess_InProcess')
group by a.stock_number, a.vehicleinventoryitemid  
order by a.stock_number, b.fromts desc  


-- thru_date is suspect, G45726A  trade buffer at 11AM 11/30, closed at 10AM 12/01, but thru date as calculated
-- as the max date at 8PM for that status, shows 11/30
-- a better thru date will probably be the next from date
select *, row_number() over (partition by stock_number order by from_date) as seq 
from (
	select a.stock_number, coalesce(a.sales_status_prior, a.sales_status), min(a.the_date) as from_date, max(a.the_date) as thru_date
	from pp.toc_category_analysis_detail_2 a
	where a.from_date > '11/17/2022'
-- 	  and stock_number = 'G45762A'
	group by a.stock_number, coalesce(a.sales_status_prior, a.sales_status)) b
order by b.stock_number, b.from_Date


so i am thinkg a 2 line time line one line is a combo of sales and repoen statuses, the other is recon statuses
each instance of mech/body in wip

select * from pp.toc_category_analysis_detail_2 where stock_number = 'G43355a'

select a.stock_number, a.vehicleinventoryitemid, min(b.fromts), max(b.thruts)
from pp.toc_category_analysis_detail_1 a
left join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and b.status in ('BodyReconProcess_InProcess','MechanicalReconProcess_InProcess')
where a.stock_number = 'G45762A'
group by a.stock_number, a.vehicleinventoryitemid  
order by a.stock_number

-- without the grouping
select a.stock_number, a.vehicleinventoryitemid, b.status, b.fromts, b.thruts
from pp.toc_category_analysis_detail_1 a
left join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and b.status in ('BodyReconProcess_InProcess','MechanicalReconProcess_InProcess')
where a.stock_number = 'G45762A'
-- group by a.stock_number, a.vehicleinventoryitemid  
order by a.stock_number