﻿select * from pp.toc_category_analysis_detail_1

-- sold since 11/17
select category, stock_number, model_year, make, model, miles, mileage_category, days_to_sell,
  count(stock_number) over (partition by category) as sales
from pp.toc_category_analysis_detail_1
where sale_date < current_date
  and category = 'gm_large_pk_15-25'
order by days_to_sell  
select category, stock_number, model_year, make, model, miles, mileage_category, days_to_sell,
  count(stock_number) over (partition by category) as sales
from pp.toc_category_analysis_detail_1
where sale_date < current_date
  and category = 'gm_large_pk_25-35'
order by days_to_sell  
order by category, from_date

-- current inventory
select category, stock_number, model_year, make, model, miles, mileage_category, age,
  count(stock_number) over (partition by category) as inventory
from pp.toc_category_analysis_detail_1
where sale_date > current_date
order by category, from_date