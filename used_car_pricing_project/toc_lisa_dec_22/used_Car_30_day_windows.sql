﻿select 1  as seq, 'All Used Car Retail' as category, avg(the_count)::integer as "30_day_avg", max(the_count) as  most_in_30, max(the_count) filter (where the_date = current_date) as last_30
from ( -- C
	select a.the_date, sum(total_sold) over (order by the_date rows between 30 preceding and current row) as the_count
	from dds.dim_date a
	left join (
		select *
		from ( -- A
				select count(*) as total_sold, boarded_date
				from board.sales_board a
				inner join board.daily_board b on a.board_id = b.board_id
				inner join veh.shape_size_classifications c on b.vehicle_make = c.make and b.vehicle_model = c.model
				where board_type_key = 5
						and boarded_date between (current_date - interval '13 months')::date and current_date
						and b.vehicle_type = 'u'
						and is_deleted = false
						and stock_number not like '%L'
			 group by boarded_date) A
		order by boarded_date ) b on a.the_date = b.boarded_date
	where the_date between (current_date - interval '1 year')::date and current_date) C
where the_date > (current_date - interval '1 year')::date + 30 
union
select 2, 'GM Large Pickup 15-25', avg(the_count)::integer , max(the_count), max(the_count) filter (where the_date = current_date)
from ( -- C
	select a.the_date, b.stock_number, bp, count(stock_number) over (order by the_date rows between 30 preceding and current row) as the_count
	from dds.dim_date a
	 left join (
	select *
	from ( -- A
			select a.stock_number, boarded_date, ads.get_current_best_price_by_stock_number_used(a.stock_number) as bp
			from board.sales_board a 
			inner join board.daily_board b on a.board_id = b.board_id
			inner join veh.shape_size_classifications c on b.vehicle_make = c.make and b.vehicle_model = c.model
			  and c.make in ('buick','cadillac','chevrolet','gmc')
			where board_type_key = 5
					and boarded_date between (current_date - interval '13 months')::date and current_date
					and shape = 'Pickup'
					and size = 'Large'
					and b.vehicle_type = 'u'
					and is_deleted = false
					and stock_number not like '%L%') A
	where bp between 15000 and 24999
	order by boarded_date ) b on a.the_date = b.boarded_date
	where the_date between (current_date - interval '1 year')::date and current_date) C
where the_date > (current_date - interval '1 year')::date + 30 
union
select 3, 'GM Large Pickup 25-35', avg(the_count)::integer , max(the_count), max(the_count) filter (where the_date = current_date)
from ( -- C
	select a.the_date, b.stock_number, bp, count(stock_number) over (order by the_date rows between 30 preceding and current row) as the_count
	from dds.dim_date a
	 left join (
	select *
	from ( -- A
			select a.stock_number, boarded_date, ads.get_current_best_price_by_stock_number_used(a.stock_number) as bp
			from board.sales_board a 
			inner join board.daily_board b on a.board_id = b.board_id
			inner join veh.shape_size_classifications c on b.vehicle_make = c.make and b.vehicle_model = c.model
			  and c.make in ('buick','cadillac','chevrolet','gmc')
			where board_type_key = 5
					and boarded_date between (current_date - interval '13 months')::date and current_date
					and shape = 'Pickup'
					and size = 'Large'
					and b.vehicle_type = 'u'
					and is_deleted = false
					and stock_number not like '%L%') A
	where bp between 25000 and 44999
	order by boarded_date ) b on a.the_date = b.boarded_date
	where the_date between (current_date - interval '1 year')::date and current_date) C
where the_date > (current_date - interval '1 year')::date + 30 
union
select 4, 'GM Large Pickup 35-45', avg(the_count)::integer , max(the_count), max(the_count) filter (where the_date = current_date)
from ( -- C
	select a.the_date, b.stock_number, bp, count(stock_number) over (order by the_date rows between 30 preceding and current row) as the_count
	from dds.dim_date a
	 left join (
	select *
	from ( -- A
			select a.stock_number, boarded_date, ads.get_current_best_price_by_stock_number_used(a.stock_number) as bp
			from board.sales_board a 
			inner join board.daily_board b on a.board_id = b.board_id
			inner join veh.shape_size_classifications c on b.vehicle_make = c.make and b.vehicle_model = c.model
			  and c.make in ('buick','cadillac','chevrolet','gmc')
			where board_type_key = 5
					and boarded_date between (current_date - interval '13 months')::date and current_date
					and shape = 'Pickup'
					and size = 'Large'
					and b.vehicle_type = 'u'
					and is_deleted = false
					and stock_number not like '%L%') A
	where bp between 35000 and 44999
	order by boarded_date ) b on a.the_date = b.boarded_date
	where the_date between (current_date - interval '1 year')::date and current_date) C
where the_date > (current_date - interval '1 year')::date + 30 
union
select 5, 'GM Large Pickup 45+', avg(the_count)::integer , max(the_count), max(the_count) filter (where the_date = current_date)
from ( -- C
	select a.the_date, b.stock_number, bp, count(stock_number) over (order by the_date rows between 30 preceding and current row) as the_count
	from dds.dim_date a
	 left join (
	select *
	from ( -- A
			select a.stock_number, boarded_date, ads.get_current_best_price_by_stock_number_used(a.stock_number) as bp
			from board.sales_board a 
			inner join board.daily_board b on a.board_id = b.board_id
			inner join veh.shape_size_classifications c on b.vehicle_make = c.make and b.vehicle_model = c.model
			  and c.make in ('buick','cadillac','chevrolet','gmc')
			where board_type_key = 5
					and boarded_date between (current_date - interval '13 months')::date and current_date
					and shape = 'Pickup'
					and size = 'Large'
					and b.vehicle_type = 'u'
					and is_deleted = false
					and stock_number not like '%L%') A
	where bp >= 45000
	order by boarded_date ) b on a.the_date = b.boarded_date
	where the_date between (current_date - interval '1 year')::date and current_date) C
where the_date > (current_date - interval '1 year')::date + 30 
union
select 6, 'GM Small SUV 15-25', avg(the_count)::integer , max(the_count), max(the_count) filter (where the_date = current_date)
from ( -- C
	select a.the_date, b.stock_number, bp, count(stock_number) over (order by the_date rows between 30 preceding and current row) as the_count
	from dds.dim_date a
	 left join (
	select *
	from ( -- A
			select a.stock_number, boarded_date, ads.get_current_best_price_by_stock_number_used(a.stock_number) as bp
			from board.sales_board a 
			inner join board.daily_board b on a.board_id = b.board_id
			inner join veh.shape_size_classifications c on b.vehicle_make = c.make and b.vehicle_model = c.model
			  and c.make in ('buick','cadillac','chevrolet','gmc')
			where board_type_key = 5
					and boarded_date between (current_date - interval '13 months')::date and current_date
					and shape = 'SUV'
					and size = 'Small'
					and b.vehicle_type = 'u'
					and is_deleted = false
					and stock_number not like '%L%') A
	where bp between 15000 and 24999
	order by boarded_date ) b on a.the_date = b.boarded_date
	where the_date between (current_date - interval '1 year')::date and current_date) C
where the_date > (current_date - interval '1 year')::date + 30 
union
select 7, 'GM Small SUV 25+', avg(the_count)::integer , max(the_count), max(the_count) filter (where the_date = current_date)
from ( -- C
	select a.the_date, b.stock_number, bp, count(stock_number) over (order by the_date rows between 30 preceding and current row) as the_count
	from dds.dim_date a
	 left join (
	select *
	from ( -- A
			select a.stock_number, boarded_date, ads.get_current_best_price_by_stock_number_used(a.stock_number) as bp
			from board.sales_board a 
			inner join board.daily_board b on a.board_id = b.board_id
			inner join veh.shape_size_classifications c on b.vehicle_make = c.make and b.vehicle_model = c.model
			  and c.make in ('buick','cadillac','chevrolet','gmc')
			where board_type_key = 5
					and boarded_date between (current_date - interval '13 months')::date and current_date
					and shape = 'SUV'
					and size = 'Small'
					and b.vehicle_type = 'u'
					and is_deleted = false
					and stock_number not like '%L%') A
	where bp >= 25000
	order by boarded_date ) b on a.the_date = b.boarded_date
	where the_date between (current_date - interval '1 year')::date and current_date) C
where the_date > (current_date - interval '1 year')::date + 30 
union
select 8, 'Import Small SUV 15-25', avg(the_count)::integer , max(the_count), max(the_count) filter (where the_date = current_date)
from ( -- C
	select a.the_date, b.stock_number, bp, count(stock_number) over (order by the_date rows between 30 preceding and current row) as the_count
	from dds.dim_date a
	 left join (
	select *
	from ( -- A
			select a.stock_number, boarded_date, ads.get_current_best_price_by_stock_number_used(a.stock_number) as bp
			from board.sales_board a 
			inner join board.daily_board b on a.board_id = b.board_id
			inner join veh.shape_size_classifications c on b.vehicle_make = c.make and b.vehicle_model = c.model
			  and c.make in ('toyota','nissan','honda')
			where board_type_key = 5
					and boarded_date between (current_date - interval '13 months')::date and current_date
					and shape = 'SUV'
					and size = 'Small'
					and b.vehicle_type = 'u'
					and is_deleted = false
					and stock_number not like '%L%') A
	where bp between 15000 and 24999
	order by boarded_date ) b on a.the_date = b.boarded_date
	where the_date between (current_date - interval '1 year')::date and current_date) C
where the_date > (current_date - interval '1 year')::date + 30 
union
select 9, 'Import Small SUV 25+', avg(the_count)::integer , max(the_count), max(the_count) filter (where the_date = current_date)
from ( -- C
	select a.the_date, b.stock_number, bp, count(stock_number) over (order by the_date rows between 30 preceding and current row) as the_count
	from dds.dim_date a
	 left join (
	select *
	from ( -- A
			select a.stock_number, boarded_date, ads.get_current_best_price_by_stock_number_used(a.stock_number) as bp
			from board.sales_board a 
			inner join board.daily_board b on a.board_id = b.board_id
			inner join veh.shape_size_classifications c on b.vehicle_make = c.make and b.vehicle_model = c.model
			  and c.make in ('toyota','nissan','honda')
			where board_type_key = 5
					and boarded_date between (current_date - interval '13 months')::date and current_date
					and shape = 'SUV'
					and size = 'Small'
					and b.vehicle_type = 'u'
					and is_deleted = false
					and stock_number not like '%L%') A
	where bp >= 25000
	order by boarded_date ) b on a.the_date = b.boarded_date
	where the_date between (current_date - interval '1 year')::date and current_date) C
where the_date > (current_date - interval '1 year')::date + 30 
union
select 10, 'Outlet', avg(the_count)::integer , max(the_count), max(the_count) filter (where the_date = current_date)
from ( -- C
	select a.the_date, b.stock_number, bp, count(stock_number) over (order by the_date rows between 30 preceding and current row) as the_count
	from dds.dim_date a
	 left join (
	select *
	from ( -- A
			select a.stock_number, boarded_date, ads.get_current_best_price_by_stock_number_used(a.stock_number) as bp
			from board.sales_board a 
			inner join board.daily_board b on a.board_id = b.board_id
			inner join veh.shape_size_classifications c on b.vehicle_make = c.make and b.vehicle_model = c.model
			where board_type_key = 5
					and boarded_date between (current_date - interval '13 months')::date and current_date
					and b.vehicle_type = 'u'
					and is_deleted = false
					and stock_number not like '%L%') A
	where bp < 15000
	order by boarded_date ) b on a.the_date = b.boarded_date
	where the_date between (current_date - interval '1 year')::date and current_date) C
where the_date > (current_date - interval '1 year')::date + 30 
order by seq
