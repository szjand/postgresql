﻿
/*
Hi,

I’ll be leaving for airport in about an hour. Would be great to have some data to look at while en route.
I understand you’ve got something, Jon, that you reviewed with Greg yesterday.
If there’s any chance I can see for at least each of the 9 categories that we’ve been replenishing this past month:

•	Sales per day
•	Receipts into “the lot” per day
•	Trades received per day and other received (that we acquired from elsewhere) per day

That would be super helpful.  If unable to provide, we’ll talk tomorrow. Or, I’ll be spending bunches of time at airports today before and between flights and will have internet on the flight between PHX and MSP.

Thanks,


Lisa

all charts

*/select * from pp.toc_category_analysis_detail_2

select distinct coalesce(sales_Status, sales_status_prior) from pp.toc_category_analysis_detail_2

select * from pp.toc_category_analysis_detail_2 where sales_status is null

select * from pp.toc_category_analysis_detail_2 where coalesce(sales_Status, sales_status_prior) in ('Delivered','Wholesaled')

-- sales
select b.category, the_date, a.stock_number, a.make, a.model, a.model_year, miles, a.days_to_sell
-- select *
from pp.toc_category_analysis_detail_1 a
join pp.toc_category_analysis_detail_2 b on a.stock_number = b.stock_number
where b.sales_status in ('Delivered','Wholesaled')
order by a.category, b.the_date

-- on the lot
select b.category, b.the_date, a.stock_number, a.make, a.model, a.model_year, miles
-- select *
from pp.toc_category_analysis_detail_1 a
join pp.toc_category_analysis_detail_2 b on a.stock_number = b.stock_number
where b.replen_status = 'on lot'
  and b.seq_1 = 1
order by a.category, b.the_date

-- acquired
select b.category, b.the_date, a.stock_number, a.make, a.model, a.model_year, miles, b.replen_status, a.source
-- select *
from pp.toc_category_analysis_detail_1 a
join pp.toc_category_analysis_detail_2 b on a.stock_number = b.stock_number
where b.seq_2 = 1
  and b.the_date > '11/17/2022'
order by a.category, b.the_date

select * from pp.toc_category_analysis_detail_2
where stock_number = 'G45484a'  


select b.category, the_date, a.stock_number, a.make, a.model, a.model_year, miles, a.days_to_sell, b.sales_status
-- select *
from pp.toc_category_analysis_detail_1 a
join pp.toc_category_analysis_detail_2 b on a.stock_number = b.stock_number
where b.sales_status in ('Delivered','Wholesaled')
  and b.category = 'outlet'
order by b.sales_status