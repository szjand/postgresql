﻿/*
https://alexklibisz.com/2022/02/18/optimizing-postgres-trigram-search

start out limiting competitor data to vehicles that match year make model to pp.evals_1

*/
select *
from pp.evals_1

--7837 distinct vins
drop table if exists vins cascade;
create temp table vins as
select distinct vin  -- 2721
from scrapes.cargurus_dealers_daily
union
select distinct vin -- 6188
from scrapes.carsdotcom_daily
union
select distinct vin -- 1144
from scrapes.autotrader_daily;
create unique index on vins(vin)

-- 144 distinct dealers  quite a few dups: Dave Hahler Automotive vs Dave Hahler Automotive, Inc.
select distinct dealer  -- 30
from scrapes.cargurus_dealers_daily
union
select distinct dealer -- 87
from scrapes.carsdotcom_daily
union
select distinct dealer -- 59
from scrapes.autotrader_daily
order by dealer

select dealership_name  -- 99 dealers
from scrapes.competitor_websites
where is_current


select a.*  -- 646
from vins a
join chr.build_data_describe_vehicle b on a.vin = b.vin
where style_count = 1

select a.*  -- 123
from vins a
join chr.describe_vehicle b on a.vin = b.vin
where style_count = 1


select vin, model_year, make, model 
from scrapes.carsdotcom_daily
union
select vin, model_year, make, model 
from scrapes.cargurus_dealers_daily
union
select vin, model_year, make, model 
from scrapes.autotrader_daily



select a.vin -- 346
from pp.evals_1 a
join chr.build_data_describe_vehicle b on  a.vin = b.vin
  and b.source = 'build'
  and b.style_count = 1
union
select a.vin -- 81
from pp.evals_1 a
join chr.describe_vehicle b on a.vin = b.vin
where not exists (
  select 1
  from chr.build_data_describe_vehicle where vin = a.vin)
  and b.style_count = 1

-- total of 1826 vins
select distinct bb.vin  --1384
from (
	select a.vin, -- 346
		(r.style ->>'id')::citext as chr_style_id,
		(r.style->>'modelYear')::integer as model_year,
		(r.style ->'division'->>'_value_1')::citext as make,
		(r.style ->'model'->>'_value_1'::citext)::citext as model  
	from pp.evals_1 a
	join chr.build_data_describe_vehicle b on  a.vin = b.vin
		and b.source = 'build'
		and b.style_count = 1
	join jsonb_array_elements(b.response->'style') as r(style) on true) aa  
join  scrapes.carsdotcom_daily bb on aa.model_year::citext = bb.model_year 
  and aa.make = bb.make 
  and aa.model = bb.model
union
-- select * from scrapes.carsdotcom_daily where vin = '1GKKVRED9CJ413970'  

select distinct bb.vin  -- 670
from (
	select a.vin, -- 346
		(r.style ->>'id')::citext as chr_style_id,
		(r.style->>'modelYear')::integer as model_year,
		(r.style ->'division'->>'_value_1')::citext as make,
		(r.style ->'model'->>'_value_1'::citext)::citext as model  
	from pp.evals_1 a
	join chr.build_data_describe_vehicle b on  a.vin = b.vin
		and b.source = 'build'
		and b.style_count = 1
	join jsonb_array_elements(b.response->'style') as r(style) on true) aa  
join  scrapes.cargurus_dealers_daily bb on aa.model_year::citext = bb.model_year 
  and aa.make = bb.make 
  and aa.model = bb.model
  
-- select * from scrapes.cargurus_dealers_daily where vin = '3GNAXSEV7JS634052'
union
select distinct bb.vin  -- 180
from (
	select a.vin, -- 346
		(r.style ->>'id')::citext as chr_style_id,
		(r.style->>'modelYear')::integer as model_year,
		(r.style ->'division'->>'_value_1')::citext as make,
		(r.style ->'model'->>'_value_1'::citext)::citext as model  
	from pp.evals_1 a
	join chr.build_data_describe_vehicle b on  a.vin = b.vin
		and b.source = 'build'
		and b.style_count = 1
	join jsonb_array_elements(b.response->'style') as r(style) on true) aa  
join  scrapes.autotrader_daily bb on aa.model_year::citext = bb.model_year 
  and aa.make = bb.make 
  and aa.model = bb.model

select * from scrapes.autotrader_daily where vin = '2GNAXUEV7L6223716'  

-- run these vins thru chrome
--  from pp.evals these are matching year/make/model
-- total of 1542 vins
drop table if exists pp.the_vins cascade;
create unlogged table pp.the_vins as
select distinct vin
from (
	select distinct bb.vin  --1384
	from (
		select a.vin, -- 346
			(r.style ->>'id')::citext as chr_style_id,
			(r.style->>'modelYear')::integer as model_year,
			(r.style ->'division'->>'_value_1')::citext as make,
			(r.style ->'model'->>'_value_1'::citext)::citext as model  
		from pp.evals_1 a
		join chr.build_data_describe_vehicle b on  a.vin = b.vin
			and b.source = 'build'
			and b.style_count = 1
		join jsonb_array_elements(b.response->'style') as r(style) on true) aa  
	join  scrapes.carsdotcom_daily bb on aa.model_year::citext = bb.model_year 
		and aa.make = bb.make 
		and aa.model = bb.model
	union
	select distinct bb.vin  -- 670
	from (
		select a.vin, -- 346
			(r.style ->>'id')::citext as chr_style_id,
			(r.style->>'modelYear')::integer as model_year,
			(r.style ->'division'->>'_value_1')::citext as make,
			(r.style ->'model'->>'_value_1'::citext)::citext as model  
		from pp.evals_1 a
		join chr.build_data_describe_vehicle b on  a.vin = b.vin
			and b.source = 'build'
			and b.style_count = 1
		join jsonb_array_elements(b.response->'style') as r(style) on true) aa  
	join  scrapes.cargurus_dealers_daily bb on aa.model_year::citext = bb.model_year 
		and aa.make = bb.make 
		and aa.model = bb.model
	union
	select distinct bb.vin  -- 180
	from (
		select a.vin, -- 346
			(r.style ->>'id')::citext as chr_style_id,
			(r.style->>'modelYear')::integer as model_year,
			(r.style ->'division'->>'_value_1')::citext as make,
			(r.style ->'model'->>'_value_1'::citext)::citext as model  
		from pp.evals_1 a
		join chr.build_data_describe_vehicle b on  a.vin = b.vin
			and b.source = 'build'
			and b.style_count = 1
		join jsonb_array_elements(b.response->'style') as r(style) on true) aa  
	join  scrapes.autotrader_daily bb on aa.model_year::citext = bb.model_year 
		and aa.make = bb.make 
		and aa.model = bb.model) x
where not exists ( -- exclude rydell vehicles
  select 1 
  from pp.evals_1
  where vin = x.vin);
-- where not exists ( -- this was for getting chrome
--   select 1
--   from chr.build_data_describe_vehicle
--   where vin = x.vin);
create unique index on pp.the_vins(vin);  

-- the fucking dealers are not normalized at all
drop table if exists pp.dealer_vins_1 cascade;
create unlogged table pp.dealer_vins_1 as
select vin, array_agg(distinct dealer) as dealer, count(*) as the_count
from (
	select a.vin, dealer
	from pp.the_vins a
	join scrapes.carsdotcom_daily  b on a.vin = b.vin
	union
	select a.vin, dealer
	from pp.the_vins a
	join scrapes.cargurus_dealers_daily  b on a.vin = b.vin
	union
	select a.vin, dealer
	from pp.the_vins a
	join scrapes.autotrader_daily  b on a.vin = b.vin) x
group by vin
order by vin;

select * from pp.dealer_vins_1 where the_count > 1

select * from the_vins where vin = '1C4AJWAG8HL728367'

-- 1113 rows
select a.vin, (c->>'id')::citext as style_id, d.*
from pp.evals_1 a
join chr.build_data_describe_vehicle b on a.vin = b.vin
  and b.source = 'build'
  and b.style_count = 1
join jsonb_array_elements(b.response->'style') as c on true
join (
	select a.*, (r.style ->>'id')::citext as style_id
	from pp.dealer_vins_1 a
	join chr.build_data_describe_vehicle b on a.vin = b.vin
	join jsonb_array_elements(b.response->'style') as r(style) on true) d on (c->>'id')::citext = d.style_id

do 3 separate tables, 1 for each source
select aa.*, bb.source, bb.mileage, bb.price, bb.dealer, cc.source, cc.mileage, cc.price, cc.dealer,
  dd.source, dd.mileage, dd.price, dd.dealer
from (
	select (c->>'id') as style_id, a.stock_number, a.vin, c->>'modelYear' as model_year, 
		c->'division'->>'_value_1' as make, c->'model'->>'_value_1' as model, c->>'trim' as trim_level,
		case
			when c ->>'drivetrain' like 'Rear%' then 'RWD'
			when c ->>'drivetrain' like 'Front%' then 'FWD'
			when c ->>'drivetrain' like 'Four%' then '4WD'
			when c ->>'drivetrain' like 'All%' then 'AWD'
			else 'XXX'
		end as chr_drive,
		case
			when c->>'_value_1' like 'Crew%' then 'crew' 
			when c->>'_value_1' like 'Extended%' then 'double'  
			when c->>'_value_1' like 'Regular%' then 'reg'  
			else 'n/a'  
		end as chr_cab,
		a.miles,
		a.shown as amount_shown, a.manheim, d.best_price
	from pp.evals_1 a
	join chr.build_data_describe_vehicle b on a.vin = b.vin
		and b.source = 'build'
		and b.style_count = 1
	join jsonb_array_elements(b.response->'style') as c on true 
	left join ( -- last price
			select a.vehicleinventoryitemid, b.amount::integer as best_price, vehiclepricingts,
					row_number() over (partition by a.vehicleinventoryitemid order by vehiclepricingts desc) as seq
			from ads.ext_vehicle_pricings a
			join ads.ext_vehicle_pricing_details b on a.vehiclepricingid = b.vehiclepricingid
					and b.typ = 'VehiclePricingDetail_BestPrice'
			join pp.evals_1 c on a.vehicleinventoryitemid = c.vehicleinventoryitemid) d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
					and d.seq = 1	
	where a.eval_status = 'booked') aa
left join (
  select 'carsdotcom' as source, c->>'id' as style_id, a.mileage, a.price, a.dealer
  from scrapes.carsdotcom_daily a
  join chr.build_data_describe_vehicle b on a.vin = b.vin
  join jsonb_array_elements(b.response->'style') as c on true) bb on aa.style_id = bb.style_id
left join (
  select 'cargurus' as source, c->>'id' as style_id, a.mileage, a.price, a.dealer
  from scrapes.cargurus_dealers_daily a
  join chr.build_data_describe_vehicle b on a.vin = b.vin
  join jsonb_array_elements(b.response->'style') as c on true) cc on aa.style_id = cc.style_id  
left join (
  select 'autotrader' as source, c->>'id' as style_id, a.mileage, a.price, a.dealer
  from scrapes.autotrader_daily a
  join chr.build_data_describe_vehicle b on a.vin = b.vin
  join jsonb_array_elements(b.response->'style') as c on true) dd on aa.style_id = dd.style_id  


-- do a separate table for rydell;
drop table if exists pp.rydell cascade;
create table pp.rydell as
	select (c->>'id') as style_id, a.stock_number, a.vin, (c->>'modelYear')::citext as model_year, 
		(c->'division'->>'_value_1')::citext as make, (c->'model'->>'_value_1')::citext as model, (c->>'trim')::citext as trim_level,
		case
			when c ->>'drivetrain' like 'Rear%' then 'RWD'::citext
			when c ->>'drivetrain' like 'Front%' then 'FWD'::citext
			when c ->>'drivetrain' like 'Four%' then '4WD'::citext
			when c ->>'drivetrain' like 'All%' then 'AWD'::citext
			else 'XXX'
		end as chr_drive,
		case
			when u.cab->>'_value_1' like 'Crew%' then 'crew'::citext 
			when u.cab->>'_value_1' like 'Extended%' then 'double'::citext
			when u.cab->>'_value_1' like 'Regular%' then 'reg'::citext  
			else 'n/a'  
		end as chr_cab,
		a.miles,
		a.shown as amount_shown, a.manheim, d.best_price
	from pp.evals_1 a
	join chr.build_data_describe_vehicle b on a.vin = b.vin
		and b.source = 'build'
		and b.style_count = 1
	join jsonb_array_elements(b.response->'style') as c on true 
	left join jsonb_array_elements(c->'bodyType') with ordinality as u(cab) on true
		and u.ordinality =  2	
	left join ( -- last price
			select a.vehicleinventoryitemid, b.amount::integer as best_price, vehiclepricingts,
					row_number() over (partition by a.vehicleinventoryitemid order by vehiclepricingts desc) as seq
			from ads.ext_vehicle_pricings a
			join ads.ext_vehicle_pricing_details b on a.vehiclepricingid = b.vehiclepricingid
					and b.typ = 'VehiclePricingDetail_BestPrice'
			join pp.evals_1 c on a.vehicleinventoryitemid = c.vehicleinventoryitemid) d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
					and d.seq = 1	
	where a.eval_status = 'booked';
create index on pp.rydell(style_id);
create unique index on pp.rydell(vin);

	
-- add deal classification, cargurus: dealRating, autotrader: isHot (?), carsdotcom: badges
-- carsdotcom
drop table if exists pp.carsdotcom cascade;
create table pp.carsdotcom as -- 483
select distinct bb.* --aa.*, bb.source, bb.mileage, bb.price, bb.dealer, bb.deal
from pp.rydell aa
join (
  select 'carsdotcom'::text as source, c->>'id' as style_id, a.vin, a.mileage, a.price, 
    case
      when a.dealer like '%&amp;%' then replace(a.dealer, 'amp;','')
      else a.dealer
    end as dealer , replace(d::text, '"', '') as deal_rating, ''::text as days_on_market
  from scrapes.carsdotcom_daily a
  join chr.build_data_describe_vehicle b on a.vin = b.vin
    and b.style_count = 1
  join jsonb_array_elements(b.response->'style') as c on true
  left join json_array_elements(a.json_data->'badges') as d on true
		and d::text like '%deal%'
	where a.dealer <> 'N/A'
	  and not exists (
	    select 1 
	    from pp.rydell
	    where vin = a.vin)) bb on aa.style_id = bb.style_id
order by bb.style_id;
create index on pp.carsdotcom(style_id);
create unique index on pp.carsdotcom(vin);

drop table if exists pp.cargurus cascade;
create table pp.cargurus as -- 674
select distinct bb.* --aa.*, bb.source, bb.mileage, bb.price, bb.dealer, bb.deal
from pp.rydell aa
join (
	select 'cargurus'::text as source, c->>'id' as style_id, a.vin, a.mileage, a.price,
		case
			when a.dealer like '%&amp;%' then replace(a.dealer, 'amp;','')
			else a.dealer
		end as dealer, json_data->>'dealRating' as deal_rating, json_data->>'daysOnMarket' as days_on_market
	from scrapes.cargurus_dealers_daily a
	join chr.build_data_describe_vehicle b on a.vin = b.vin
		and b.style_count = 1
	join jsonb_array_elements(b.response->'style') as c on true
	where a.lookup_date = '11/09/2022'
	  and a.dealer <> 'N/A'
	  and not exists (
	    select 1 
	    from pp.rydell
	    where vin = a.vin)) bb on aa.style_id = bb.style_id
order by bb.style_id;
create index on pp.cargurus(style_id);
create unique index on pp.cargurus(vin);	

-- autotrader
drop table if exists pp.autotrader cascade;
create table pp.autotrader as -- 44
select distinct bb.* --aa.*, bb.source, bb.mileage, bb.price, bb.dealer, bb.deal
from pp.rydell aa
join (
	select 'cargurus'::text as source, c->>'id' as style_id, a.vin, a.mileage, a.price,
		case
			when a.dealer like '%&amp;%' then replace(a.dealer, 'amp;','')
			else a.dealer
		end as dealer, json_data->>'isHot' as deal_rating, json_data->>'daysOnMarket' as days_on_market
	from scrapes.autotrader_daily a
	join chr.build_data_describe_vehicle b on a.vin = b.vin
		and b.style_count = 1
	join jsonb_array_elements(b.response->'style') as c on true
	where a.dealer <> 'N/A'
	  and lookup_date = '11/09/2022'
	  and not exists (
	    select 1 
	    from pp.rydell
	    where vin = a.vin)) bb on aa.style_id = bb.style_id
order by bb.style_id;
create index on pp.autotrader(style_id);
create unique index on pp.autotrader(vin);	

select * 
from pp.rydell
where style_id = '406427'
order by miles

select source, dealer, vin, mileage, price::integer, deal_rating, days_on_market from pp.carsdotcom where style_id = '406427' 
union
select source, dealer, vin, mileage, price::integer, deal_rating, days_on_market from pp.cargurus where style_id = '406427' 
union
select source, dealer, vin, mileage, price::integer, deal_rating, days_on_market from pp.autotrader where style_id = '406427' order by mileage


----------------------------------------------------------------------------------
--< kelley & black book
----------------------------------------------------------------------------------
drop table if exists pp.kelley_black_book cascade;
create table pp.kelley_black_book (
  vin citext,
  response jsonb);

select * from pp.kelley_black_book
  
delete from pp.kelley_black_book where vin = '1GNSKSKLXNR155445'
