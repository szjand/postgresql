﻿/*
9-14
Ray Lee looking for a list of all parts customers and their pricing level
eg Northwest Autobody: PL-6
*/

/*
10-08
Charlie's info
*/
 
select ppkey, ppdesc, ppmeth from pdppric where ppco# = 'RY1'

Select * from rydedata.PDPPSAS where psco# = 'RY1'

Select distinct length(trim(psrplev)) from rydedata.PDPPSAS

Select distinct length(trim(psrplev)) from rydedata.PDPPSAS where psco# = 'RY1'

select pssgrp, 
  substr(trim(psrplev), 1, 3) as "1",
  substr(trim(psrplev), 4, 3) as "2",
  substr(trim(psrplev), 7, 3) as "3",
  substr(trim(psrplev), 10, 3) as "4",
  substr(trim(psrplev), 13, 3) as "5",
  substr(trim(psrplev), 16, 3) as "6",
  substr(trim(psrplev), 19, 3) as "7",
  substr(trim(psrplev), 22, 3) as "8",
  substr(trim(psrplev), 25, 3) as "9",
  substr(trim(psrplev), 28, 3) as "10",
  substr(trim(psrplev), 31, 3) as "11",
  substr(trim(psrplev), 34, 3) as "12",
  substr(trim(psrplev), 37, 3) as "13",
  substr(trim(psrplev), 40, 3) as "14",
  substr(trim(psrplev), 43, 3) as "15",
  substr(trim(psrplev), 46, 3) as "16",
  substr(trim(psrplev), 49, 3) as "17",
  substr(trim(psrplev), 52, 3) as "18",
  substr(trim(psrplev), 55, 3) as "19",
  substr(trim(psrplev), 58, 3) as "20"
from rydedata.PDPPSAS 
where psco# = 'RY1'
and pssgrp = ''

-- 6/9/2011

/* 
attempting to get the data available from
Parts -> 30
goto Advanced Collision Center (124240)
F7: shows a Parts Price Level (Counter) of PL-6
query shows PDPCUST.PJPLVL = 10
wtf?
*/

--6/23
-- right column is what shows in UI
-- left column is the value from pdpcust = column header from pdppsas
-- just the fucking Pricing Stragegy Assignments screen, numbered from top to bottom
-- ie, the 10th row on that page says PL-6, and the 10 column in pdppsas has a value of 17
-- that (17) is the pdppric.ppkey value (list less 30%)
1:  Retail
2:  Service Customer Pay
3.  Interanl
4.  Cores
5.  PL-1
6.  PL-2
7.  PL-3
8.  PL-4
9.  PL-5
10. PL-6
11. PL-7
12. PL-8
13. PL-9
14. PL-10
15. PL-11
16. PL-12
17. PL-13
18. PL-14
19. PL-15
20. PL-16

select * from rydedata.pdppsas


-- here are the customer pricing levels
-- the column header is the price level from pdpcust
-- the values are the pdppric key values
select  
  cast(substr(trim(psrplev), 1, 3) as integer) as "1",
  cast(substr(trim(psrplev), 4, 3) as integer) as "2",
  cast(substr(trim(psrplev), 7, 3) as integer) as "3",
  cast(substr(trim(psrplev), 10, 3) as integer) as "4",
  cast(substr(trim(psrplev), 13, 3) as integer) as "5",
  cast(substr(trim(psrplev), 16, 3) as integer) as "6",
  cast(substr(trim(psrplev), 19, 3) as integer) as "7",
  cast(substr(trim(psrplev), 22, 3) as integer) as "8",
  cast(substr(trim(psrplev), 25, 3) as integer) as "9",
  cast(substr(trim(psrplev), 28, 3) as integer) as "10",
  cast(substr(trim(psrplev), 31, 3) as integer) as "11",
  cast(substr(trim(psrplev), 34, 3) as integer) as "12",
  cast(substr(trim(psrplev), 37, 3) as integer) as "13",
  cast(substr(trim(psrplev), 40, 3) as integer) as "14",
  cast(substr(trim(psrplev), 43, 3) as integer) as "15",
  cast(substr(trim(psrplev), 46, 3) as integer) as "16",
  cast(substr(trim(psrplev), 49, 3) as integer) as "17",
  cast(substr(trim(psrplev), 52, 3) as integer) as "18",
  cast(substr(trim(psrplev), 55, 3) as integer) as "19",
  cast(substr(trim(psrplev), 58, 3) as integer) as "20"
from rydedata.PDPPSAS 
where psco# = 'RY1'
and pssgrp = ''

-- 6/24, this finally is it

--02/04/2024
well, i got this to run, but the results dont come close to the pl assignments in dealertrack


select distinct g.search_name as name, trim(g.customer_number) as "Cust No", p.parts_price_level, --, b.bncity as City, b.bnstcd as state,
  case p.parts_price_level
  when 1 then 'Retail'
  when 2 then 'Service Customer Pay'
  when 3 then 'Internal'
  when 4 then 'Cores'
  when 5 then 'PL-1'
  when 6 then 'PL-2'
  when 7 then 'PL-3'
  when 8 then 'PL-4'
  when 9 then 'PL-5'
  when 10 then 'PL-6'
  when 11 then 'PL-7'
  when 12 then 'PL-8'
  when 13 then 'PL-9'
  when 14 then 'PL-10'
  when 15 then 'PL-11'
  when 16 then 'PL-12'
  when 17 then 'PL-13'
  when 18 then 'PL-14'
  when 19 then 'PL-15'
  when 20 then 'PL-16'
  end as "Price Level",
  case p.parts_price_level
  when 1 then (select description from arkona.ext_pdppric where record_key = 22 and company_number = 'RY1')
  when 2 then (select description from arkona.ext_pdppric where record_key = 1 and company_number = 'RY1')
  when 3 then (select description from arkona.ext_pdppric where record_key = 1 and company_number = 'RY1')
  when 4 then (select description from arkona.ext_pdppric where record_key = 2 and company_number = 'RY1')
  when 5 then (select description from arkona.ext_pdppric where record_key = 6 and company_number = 'RY1')
  when 6 then (select description from arkona.ext_pdppric where record_key = 4 and company_number = 'RY1')
  when 7 then (select description from arkona.ext_pdppric where record_key = 6 and company_number = 'RY1')
  when 8 then (select description from arkona.ext_pdppric where record_key = 3 and company_number = 'RY1')
  when 9 then (select description from arkona.ext_pdppric where record_key = 1 and company_number = 'RY1')
  when 10 then (select description from arkona.ext_pdppric where record_key = 17 and company_number = 'RY1')
  when 11 then (select description from arkona.ext_pdppric where record_key = 19 and company_number = 'RY1')
  when 12 then (select description from arkona.ext_pdppric where record_key = 20 and company_number = 'RY1')
  when 13 then (select description from arkona.ext_pdppric where record_key = 14 and company_number = 'RY1')
  when 14 then (select description from arkona.ext_pdppric where record_key = 7 and company_number = 'RY1')
  when 15 then (select description from arkona.ext_pdppric where record_key = 2 and company_number = 'RY1')
  when 16 then (select description from arkona.ext_pdppric where record_key = 10 and company_number = 'RY1')
  when 17 then (select description from arkona.ext_pdppric where record_key = 5 and company_number = 'RY1')
  when 18 then (select description from arkona.ext_pdppric where record_key = 22 and company_number = 'RY1')
  when 19 then (select description from arkona.ext_pdppric where record_key = 21 and company_number = 'RY1')
  when 20 then (select description from arkona.ext_pdppric where record_key = 23 and company_number = 'RY1')
  end as Description
from arkona.ext_pdpcust p
left join arkona.ext_glpcust g on g.record_key = p.KEY_TO_BOPNAME -- for customer name
left join arkona.ext_bopname b on trim(g.search_name) = trim(b.bopname_search_name) -- for cust # & address
  and b.bopname_company_number = 'RY1'
where p.parts_price_level between 1 and 23
order by p.parts_price_level
-- order by g.gcsnam, b.bncity


select * from arkona.ext_glpcust where customer_number = '1980'
---------------------------------------------------------------------------------

/*
  see the word doc from charlie kaser in python projects/ext_arkona/docs/
*/
-- all the above was from june 2011
-- 10/22/18 from dan stinar:
/*

Good Morning Sir!  Hoping you can do me a favor?   Long long time ago, you were able to get me a report of every A/R charge account report with each customer number along with their pricing level in our DMS.

I am hoping you can do the same for me for our Honda Nissan accounts. 

When you are able to ..can you make me 
  a list of all of our accounts on the Honda Nissan side
  pricing level we have set up for each account
*/


select distinct g.gcsnam as name, trim(g.gccus#) as "Cust No", b.bncity as City, b.bnstcd as state,
  case p.pjplvl
  when 1 then 'Retail'
  when 2 then 'Service Customer Pay'
  when 3 then 'Internal'
  when 4 then 'Cores'
  when 5 then 'PL-1'
  when 6 then 'PL-2'
  when 7 then 'PL-3'
  when 8 then 'PL-4'
  when 9 then 'PL-5'
  when 10 then 'PL-6'
  when 11 then 'PL-7'
  when 12 then 'PL-8'
  when 13 then 'PL-9'
  when 14 then 'PL-10'
  when 15 then 'PL-11'
  when 16 then 'PL-12'
  when 17 then 'PL-13'
  when 18 then 'PL-14'
  when 19 then 'PL-15'
  when 20 then 'PL-16'
  end as "Price Level",
  case p.pjplvl
  when 1 then (select ppdesc from rydedata.pdppric where ppkey = 22 and ppco# = 'RY2')
  when 2 then (select ppdesc from rydedata.pdppric where ppkey = 1 and ppco# = 'RY2')
  when 3 then (select ppdesc from rydedata.pdppric where ppkey = 1 and ppco# = 'RY2')
  when 4 then (select ppdesc from rydedata.pdppric where ppkey = 2 and ppco# = 'RY2')
  when 5 then (select ppdesc from rydedata.pdppric where ppkey = 6 and ppco# = 'RY2')
  when 6 then (select ppdesc from rydedata.pdppric where ppkey = 4 and ppco# = 'RY2')
  when 7 then (select ppdesc from rydedata.pdppric where ppkey = 6 and ppco# = 'RY2')
  when 8 then (select ppdesc from rydedata.pdppric where ppkey = 3 and ppco# = 'RY2')
  when 9 then (select ppdesc from rydedata.pdppric where ppkey = 1 and ppco# = 'RY2')
  when 10 then (select ppdesc from rydedata.pdppric where ppkey = 17 and ppco# = 'RY2')
  when 11 then (select ppdesc from rydedata.pdppric where ppkey = 19 and ppco# = 'RY2')
  when 12 then (select ppdesc from rydedata.pdppric where ppkey = 20 and ppco# = 'RY2')
  when 13 then (select ppdesc from rydedata.pdppric where ppkey = 14 and ppco# = 'RY2')
  when 14 then (select ppdesc from rydedata.pdppric where ppkey = 7 and ppco# = 'RY2')
  when 15 then (select ppdesc from rydedata.pdppric where ppkey = 2 and ppco# = 'RY2')
  when 16 then (select ppdesc from rydedata.pdppric where ppkey = 10 and ppco# = 'RY2')
  when 17 then (select ppdesc from rydedata.pdppric where ppkey = 5 and ppco# = 'RY2')
  when 18 then (select ppdesc from rydedata.pdppric where ppkey = 22 and ppco# = 'RY2')
  when 19 then (select ppdesc from rydedata.pdppric where ppkey = 21 and ppco# = 'RY2')
  when 20 then (select ppdesc from rydedata.pdppric where ppkey = 23 and ppco# = 'RY2')
  end as Description
from rydedata.pdpcust p
left join rydedata.glpcust g on g.gckey = p.pjkey -- for customer name
left join rydedata.bopname b on trim(g.gcsnam) = trim(b.bnsnam) -- for cust # & address
  and b.bnco# = 'RY2'
where p.pjplvl between 1 and 23
  and p.company_number = 'RY2'
order by g.gcsnam, b.bncity



select pjplvl, count(*) from rydedata.pdpcust group by pjplvl
select * from rydedata.pdpcust where company_number = 'RY2' limit 100

select company_number, count(*) from rydedata.pdpcust group by company_number





select * from rydedata.pdpcust where company_number = 'RY2' limit 100



select * From rydedata.glpcust where record_key in (200073,217512,217534,217571)

mindfuckville


select * from rydedata.glpcust where trim(search_name) = 'ABERDEEN CHRYSLER CENTER'

select * from rydedata.bopname where trim(bopname_search_name) = 'ABERDEEN CHRYSLER CENTER'

pdpcust PK: company_number, key_to_bopname
glpcust PK: company_number, record_key
bopname PK: bopname_company_number, bopname_record_key

select  bopname_Search_name, b.*
-- select count(*) --152
from rydedata.bopname a
inner join rydedata.pdpcust b on a.bopname_Record_key = b.key_to_bopname
where a.bopname_company_number = 'RY2'
order by bopname_Search_name


-- 10/22 this might be the right path

select a.key_to_bopname, a.parts_price_level, b.customer_number, b.vendor_number, b.active, b.search_name 
-- select count(*) -- 4917
from rydedata.pdpcust a
inner join rydedata.glpcust b on a.key_to_bopname = b.record_key
  -- and b.company_number = 'RY2'
where a.company_number = 'RY2'
order by b.search_name

-- add in bopname for address

select b.search_name, trim(b.customer_number) as cust#, c.city, c.state_code,
  case a.pjplvl
  when 1 then 'Retail'
  when 2 then 'Service Customer Pay'
  when 3 then 'Internal'
  when 4 then 'Cores'
  when 5 then 'PL-1'
  when 6 then 'PL-2'
  when 7 then 'PL-3'
  when 8 then 'PL-4'
  when 9 then 'PL-5'
  when 10 then 'PL-6'
  when 11 then 'PL-7'
  when 12 then 'PL-8'
  when 13 then 'PL-9'
  when 14 then 'PL-10'
  when 15 then 'PL-11'
  when 16 then 'PL-12'
  when 17 then 'PL-13'
  when 18 then 'PL-14'
  when 19 then 'PL-15'
  when 20 then 'PL-16'
  end as "Price Level",
  case a.pjplvl
  when 1 then (select ppdesc from rydedata.pdppric where ppkey = 22 and ppco# = 'RY2')
  when 2 then (select ppdesc from rydedata.pdppric where ppkey = 1 and ppco# = 'RY2')
  when 3 then (select ppdesc from rydedata.pdppric where ppkey = 1 and ppco# = 'RY2')
  when 4 then (select ppdesc from rydedata.pdppric where ppkey = 2 and ppco# = 'RY2')
  when 5 then (select ppdesc from rydedata.pdppric where ppkey = 6 and ppco# = 'RY2')
  when 6 then (select ppdesc from rydedata.pdppric where ppkey = 4 and ppco# = 'RY2')
  when 7 then (select ppdesc from rydedata.pdppric where ppkey = 6 and ppco# = 'RY2')
  when 8 then (select ppdesc from rydedata.pdppric where ppkey = 3 and ppco# = 'RY2')
  when 9 then (select ppdesc from rydedata.pdppric where ppkey = 1 and ppco# = 'RY2')
  when 10 then (select ppdesc from rydedata.pdppric where ppkey = 17 and ppco# = 'RY2')
  when 11 then (select ppdesc from rydedata.pdppric where ppkey = 19 and ppco# = 'RY2')
  when 12 then (select ppdesc from rydedata.pdppric where ppkey = 20 and ppco# = 'RY2')
  when 13 then (select ppdesc from rydedata.pdppric where ppkey = 14 and ppco# = 'RY2')
  when 14 then (select ppdesc from rydedata.pdppric where ppkey = 7 and ppco# = 'RY2')
  when 15 then (select ppdesc from rydedata.pdppric where ppkey = 2 and ppco# = 'RY2')
  when 16 then (select ppdesc from rydedata.pdppric where ppkey = 10 and ppco# = 'RY2')
  when 17 then (select ppdesc from rydedata.pdppric where ppkey = 5 and ppco# = 'RY2')
  when 18 then (select ppdesc from rydedata.pdppric where ppkey = 22 and ppco# = 'RY2')
  when 19 then (select ppdesc from rydedata.pdppric where ppkey = 21 and ppco# = 'RY2')
  when 20 then (select ppdesc from rydedata.pdppric where ppkey = 23 and ppco# = 'RY2')
  end as Description
from rydedata.pdpcust a
inner join rydedata.glpcust b on a.key_to_bopname = b.record_key
left join rydedata.bopname c on a.key_to_bopname = c.bopname_Record_key
where a.company_number = 'RY2'
  and b.search_name = c.bopname_Search_name
  and b.customer_number <> ''
order by b.search_name

































