﻿select * from arkona.ext_pdpthdr where company_number = 'RY1' and sort_name = 'MINOT''S FINEST COLLISION CENTE' order by trans_date desc limit 100

create table pts.gm_wholesale_customers (
  customer_key integer,
  customer_name citext,
  address citext);
comment on table pts.gm_wholesale_customers is 'populated from Dealertrack report Option 22 Wholesale Analysis which generates a spreadsheet';


select * from arkona.ext_pdppric
select company_number, record_key, description, pricing_method, net_price_percent from arkona.ext_pdppric where company_number = 'RY1'


-- cust key 284548
select company_number, invoice_number, trans_date, customer_key, sort_name, price_level
from arkona.ext_pdpthdr
where price_level = 26
  and sort_name = 'lake chevrolet'
order by trans_date
limit 100

-- wtf, cust_key 182300  this also shows as the control in gl (31) for the AR line
-- wtf why is the AR to SPIRIT LAK, and the other lines are to Lake Chevrolet
select * from pts.gm_wholesale_customers where customer_name = 'lake chevrolet'
*P*148304            -989.07 15592166LAKE CHEVROLET           
*P*122000             989.07   182300Invoice for PO SPIRIT LAK

-- this returns no rows
select company_number, invoice_number, trans_date, customer_key, sort_name, price_level
from arkona.ext_pdpthdr
where customer_key = 182300

-- 284548 is one of 9 different record keys !!
select * from arkona.ext_bopname where bopname_search_name = 'lake chevrolet'


select company_number, record_key, active, customer_number, vendor_number, search_name, customer_type 
-- select * 
from arkona.ext_glpcust where search_name = 'lake chevrolet'

/*
GLPCUST.GCACTIVE
B: Both Charge Customer & Vendor 
C: Charge Customer (GCVND# = '')
N: Inactive
V: Vendor


customer types are in glpctyp
RY2 Wholesale is customer_type 6

select a.search_name, a.customer_number, b.tax_exmpt_no_ as tax_exmpt_no
from arkona.ext_glpcust a
left join arkona.xfm_bopname b on a.record_key = b.bopname_record_key
  and b.current_row
where a.company_number in ('RY1','RY2')
  and a.active  = 'C'
  and a.customer_type = 6

-- and put them both together:

select 
  case
    when a.customer_type = 2 then 'RY1'
    when a.customer_type = 6 then 'RY2'
  end as store,
  a.search_name, a.customer_number, b.tax_exmpt_no_ as tax_exmpt_no
from arkona.ext_glpcust a
left join arkona.xfm_bopname b on a.record_key = b.bopname_record_key
  and b.current_row
where a.company_number in ('RY1','RY2')
  and a.active  = 'C'
  and a.customer_type = 2
union
select
  case
    when a.customer_type = 2 then 'RY1'
    when a.customer_type = 6 then 'RY2'
  end as store,
    a.search_name, a.customer_number, b.tax_exmpt_no_ as tax_exmpt_no
from arkona.ext_glpcust a
left join arkona.xfm_bopname b on a.record_key = b.bopname_record_key
  and b.current_row
where a.company_number in ('RY1','RY2')
  and a.active  = 'C'
  and a.customer_type = 6  
order by store, search_name  
*/


select count(*) from arkona.ext_glpcust where active = 'C'  -- 24394


RY1,296220,B,283000,212750,LAKE CHEVROLET
RY1,284548,C,182300,,LAKE CHEVROLET
RY1,36,V,,112750,LAKE CHEVROLET


select *
from fin.dim_account
where account in ('148304','168304','124200','122000')
  and current_row

-- 02/03/2024 try to match up with Dealertrack::Application Environment::Wholesale Pricing ASsignments

select * from pts.gm_wholesale_customers order by customer_name

select price_level, count(*)
from arkona.ext_pdpthdr
where trans_date > 20230930
  and company_number = 'RY1'
group by price_level
order by count(*) desc


select distinct sort_name
from arkona.ext_pdpthdr
where trans_date > 20230930
  and company_number = 'RY1'
  and price_level = 10 --(cost + 40%)
order by sort_name  

!!!!!!!!!!!!!!!!!!!!!!
found in aqt, the charlie file, see if i can make it make sense


select * from arkona.ext_pdppsas

-- looking at this output
-- a price level of 10 (10th column) for customer assignment refers to pricing strategy "017"
--  17 is the record key in pdppric and is described as "List less 30%"
select  
  substr(trim(REG_PRICE_LEVELS), 1, 3) as "1",
  substr(trim(REG_PRICE_LEVELS), 4, 3) as "2",
  substr(trim(REG_PRICE_LEVELS), 7, 3) as "3",
  substr(trim(REG_PRICE_LEVELS), 10, 3) as "4",
  substr(trim(REG_PRICE_LEVELS), 13, 3) as "5",
  substr(trim(REG_PRICE_LEVELS), 16, 3) as "6",
  substr(trim(REG_PRICE_LEVELS), 19, 3) as "7",
  substr(trim(REG_PRICE_LEVELS), 22, 3) as "8",
  substr(trim(REG_PRICE_LEVELS), 25, 3) as "9",
  substr(trim(REG_PRICE_LEVELS), 28, 3) as "10",
  substr(trim(REG_PRICE_LEVELS), 31, 3) as "11",
  substr(trim(REG_PRICE_LEVELS), 34, 3) as "12",
  substr(trim(REG_PRICE_LEVELS), 37, 3) as "13",
  substr(trim(REG_PRICE_LEVELS), 40, 3) as "14",
  substr(trim(REG_PRICE_LEVELS), 43, 3) as "15",
  substr(trim(REG_PRICE_LEVELS), 46, 3) as "16",
  substr(trim(REG_PRICE_LEVELS), 49, 3) as "17",
  substr(trim(REG_PRICE_LEVELS), 52, 3) as "18",
  substr(trim(REG_PRICE_LEVELS), 55, 3) as "19",
  substr(trim(REG_PRICE_LEVELS), 58, 3) as "20",
  substr(trim(REG_PRICE_LEVELS), 61, 3) as "21",
  substr(trim(REG_PRICE_LEVELS), 64, 3) as "22",
  substr(trim(REG_PRICE_LEVELS), 67, 3) as "23",
  substr(trim(REG_PRICE_LEVELS), 70, 3) as "24", 
  substr(trim(REG_PRICE_LEVELS), 73, 3) as "25", 
  substr(trim(REG_PRICE_LEVELS), 76, 3) as "26", 
  substr(trim(REG_PRICE_LEVELS), 79, 3) as "27"
from arkona.ext_PDPPSAS 
where company_number = 'RY1'
and stock_group is null


ok, let me go thru an example, instead of going round and round and round
in Dealertrack Parts->30 (charge_customers)
select change for A & J PAINT & BODY
the customer number on this screen (1980) is customer_number attribute of glpcust
select pricing (F7)g
the shows A & J has been assigned PL-6: List less 30%
The problem with that is, in the above query results
column 6 (which is suppose to be the price level) is for pricing key 4 which is Cost + 10%
List less 30% is pricing level 17

and of course, all that "understanding" from charlies doc is blown out the window,
looking below, PL-6 is List less 30% according to the parts specification report


-- from the parts specification report RY1
-- this list matches the list that comes up in Application Environment->Wholesale Pricing Assignments
 ================================================================================================================================
 PRICING LEVEL                ASSIGNMENTS
 ================================================================================================================================
 Retail                       Matrix
 Service Customer Pay         List
 Internal                     List
 Cores                        Cost
 Pricing Level  1             Cost + 20%
 Pricing Level  2             Cost + 10%
 Pricing Level  3             Cost + 20%
 Pricing Level  4             Trade
 Pricing Level  5             List
 Pricing Level  6             List less 30%
 Pricing Level  7             List less 32%
 Pricing Level  8             List less 34%
 Pricing Level  9             List less 15%
 Pricing Level  10            Cost + 25%
 Pricing Level  11            Cost
 Pricing Level  12            Cost + 40%
 Pricing Level  13            Cost + 15%
 Pricing Level  14            Matrix
 Pricing Level  15            Cost + 200%
 Pricing Level  16            LIST LESS 33
 Pricing Level  17            List less 35%
 Pricing Level  18            List less 20%
 Pricing Level  19            COST + 76%
 Pricing Level  20            TRADE 2.0
 Pricing Level  21            COST + 5%
 Pricing Level  22            RYDELL PARTS PROGRAM
 Pricing Level  23            List less 38%


so, if i decide to forego charlies doc, and go with the spec report levels,
where am i going to get the PL assigned to the customer




-- DROP TABLE IF EXISTS arkona.ext_PDPPSAS;
-- CREATE TABLE IF NOT EXISTS arkona.ext_PDPPSAS(
--     COMPANY_NUMBER CITEXT,
--     STOCK_GROUP CITEXT,
--     FACT_PRICE_SYMBOL CITEXT,
--     DESCRIPTION CITEXT,
--     REG_PRICE_LEVELS CITEXT,
--     REG_DEFAULTS CITEXT,
--     COMP_PRICE_LEVELS CITEXT,
--     COMP_DEFAULTS CITEXT)
-- WITH (OIDS=FALSE);
-- COMMENT ON COLUMN arkona.ext_PDPPSAS.COMPANY_NUMBER IS 'PSCO# : Company Number';
-- COMMENT ON COLUMN arkona.ext_PDPPSAS.STOCK_GROUP IS 'PSSGRP : Stock Group';
-- COMMENT ON COLUMN arkona.ext_PDPPSAS.FACT_PRICE_SYMBOL IS 'PSPSYM : Fact Price Symbol';
-- COMMENT ON COLUMN arkona.ext_PDPPSAS.DESCRIPTION IS 'PSDESC : Description';
-- COMMENT ON COLUMN arkona.ext_PDPPSAS.REG_PRICE_LEVELS IS 'PSRPLEV : Reg Price Levels';
-- COMMENT ON COLUMN arkona.ext_PDPPSAS.REG_DEFAULTS IS 'PSRDFLT : Reg Defaults';
-- COMMENT ON COLUMN arkona.ext_PDPPSAS.COMP_PRICE_LEVELS IS 'PSCPLEV : Comp Price Levels';
-- COMMENT ON COLUMN arkona.ext_PDPPSAS.COMP_DEFAULTS IS 'PSCDFLT : Comp Defaults';
-- -- {TRIM(COMPANY_NUMBER),TRIM(STOCK_GROUP),TRIM(FACT_PRICE_SYMBOL),TRIM(DESCRIPTION),TRIM(REG_PRICE_LEVELS),TRIM(REG_DEFAULTS),TRIM(COMP_PRICE_LEVELS),TRIM(COMP_DEFAULTS)}



-- DROP TABLE IF EXISTS arkona.ext_PDPCUST;
-- CREATE TABLE IF NOT EXISTS arkona.ext_PDPCUST(
--     COMPANY_NUMBER CITEXT,
--     KEY_TO_BOPNAME INTEGER,
--     PARTS_PRICE_LEVEL INTEGER,
--     SERV_PRICE_LEVEL INTEGER,
--     SERV_PRICE_ASSIGN INTEGER,
--     SPEC_ORD_DEP_PCT INTEGER,
--     LABOR_RATE NUMERIC (5,2),
--     SERV_SALES_LEVEL INTEGER,
--     WHOLESALE_COMP_ CITEXT,
--     WHLS_PRT_COMP_TYPE CITEXT,
--     WHLS_COMP_PRC_LEV INTEGER,
--     SALE_TYPE CITEXT,
--     WHLS_SRV_COMP_TYPE CITEXT,
--     CODE_1 CITEXT,
--     CODE_2 CITEXT,
--     RESTOCKING_AMT$ NUMERIC (7,2),
--     RESTOCKING_PCT_ NUMERIC (5,3),
--     RESTOCKING_LIMIT NUMERIC (7,2),
--     INCLUDE CITEXT,
--     INCLUDE_CORE CITEXT,
--     TAX_GROUP INTEGER,
--     COST_STRATEGY INTEGER,
--     SALES_SUB_TYPE CITEXT)
-- WITH (OIDS=FALSE);
-- COMMENT ON COLUMN arkona.ext_PDPCUST.COMPANY_NUMBER IS 'PJCO# : Company Number';
-- COMMENT ON COLUMN arkona.ext_PDPCUST.KEY_TO_BOPNAME IS 'PJKEY : Key to BOPNAME';
-- COMMENT ON COLUMN arkona.ext_PDPCUST.PARTS_PRICE_LEVEL IS 'PJPLVL : Parts Price Level';
-- COMMENT ON COLUMN arkona.ext_PDPCUST.SERV_PRICE_LEVEL IS 'PJSPLV : Serv Price Level';
-- COMMENT ON COLUMN arkona.ext_PDPCUST.SERV_PRICE_ASSIGN IS 'PJSPSA : Serv Price Assign';
-- COMMENT ON COLUMN arkona.ext_PDPCUST.SPEC_ORD_DEP_PCT IS 'PJSPDP : Spec Ord Dep Pct';
-- COMMENT ON COLUMN arkona.ext_PDPCUST.LABOR_RATE IS 'PJLRATE : Labor Rate';
-- COMMENT ON COLUMN arkona.ext_PDPCUST.SERV_SALES_LEVEL IS 'PJSSLEV : Serv Sales Level';
-- COMMENT ON COLUMN arkona.ext_PDPCUST.WHOLESALE_COMP_ IS 'PJWHC# : Wholesale Comp #';
-- COMMENT ON COLUMN arkona.ext_PDPCUST.WHLS_PRT_COMP_TYPE IS 'PJWHCT : Whls Prt Comp Type';
-- COMMENT ON COLUMN arkona.ext_PDPCUST.WHLS_COMP_PRC_LEV IS 'PJWHPL : Whls Comp Prc Lev';
-- COMMENT ON COLUMN arkona.ext_PDPCUST.SALE_TYPE IS 'PJSTYP : Sale Type';
-- COMMENT ON COLUMN arkona.ext_PDPCUST.WHLS_SRV_COMP_TYPE IS 'PJSRVWHCT : Whls Srv Comp Type';
-- COMMENT ON COLUMN arkona.ext_PDPCUST.CODE_1 IS 'PJCODE1 : Code 1';
-- COMMENT ON COLUMN arkona.ext_PDPCUST.CODE_2 IS 'PJCODE2 : Code 2';
-- COMMENT ON COLUMN arkona.ext_PDPCUST.RESTOCKING_AMT$ IS 'PJRESTK$ : Restocking Amt$';
-- COMMENT ON COLUMN arkona.ext_PDPCUST.RESTOCKING_PCT_ IS 'PJRESTKPCT : Restocking Pct%';
-- COMMENT ON COLUMN arkona.ext_PDPCUST.RESTOCKING_LIMIT IS 'PJRESTKLMT : Restocking Limit';
-- COMMENT ON COLUMN arkona.ext_PDPCUST.INCLUDE IS 'PJINCL : Include';
-- COMMENT ON COLUMN arkona.ext_PDPCUST.INCLUDE_CORE IS 'PJINCLC : Include Core';
-- COMMENT ON COLUMN arkona.ext_PDPCUST.TAX_GROUP IS 'PJTAXG : Tax Group';
-- COMMENT ON COLUMN arkona.ext_PDPCUST.COST_STRATEGY IS 'PJCSTS : Cost Strategy';
-- COMMENT ON COLUMN arkona.ext_PDPCUST.SALES_SUB_TYPE IS 'PJSLSBTP : Sales Sub Type';
-- {TRIM(COMPANY_NUMBER),KEY_TO_BOPNAME,PARTS_PRICE_LEVEL,SERV_PRICE_LEVEL,SERV_PRICE_ASSIGN,SPEC_ORD_DEP_PCT,LABOR_RATE,SERV_SALES_LEVEL,TRIM(WHOLESALE_COMP_),TRIM(WHLS_PRT_COMP_TYPE),WHLS_COMP_PRC_LEV,TRIM(SALE_TYPE),TRIM(WHLS_SRV_COMP_TYPE),TRIM(CODE_1),TRIM(CODE_2),RESTOCKING_AMT$,RESTOCKING_PCT_,RESTOCKING_LIMIT,TRIM(INCLUDE),TRIM(INCLUDE_CORE),TAX_GROUP,COST_STRATEGY,TRIM(SALES_SUB_TYPE)}


select * from arkona.ext_pdpcust where key_to_bopname = 285631

select * from arkona.ext_bopname where bopname_Search_name = 'A & J PAINT & BODY'

select count(*) from pts.gm_wholesale_customers  -- 283

select count(*) from arkona.ext_pdpcust where sale_type = 'W' -- 11022

select * 
from pts.gm_wholesale_customers a
left join arkona.ext_pdpcust b on a.customer_key = b.key_to_bopname
