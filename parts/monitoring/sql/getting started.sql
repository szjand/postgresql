﻿-- from fred
10/19/23
With the strike going on I am hoping you can help me create a daily report of cabin air filters, air filter, 
and oil filters of what we have in stock and sales history. We are still able to place orders for these items 
and should get them within the week.  Hoping once we get this figured out we could get it emailed to me and 
mark and donny so we can take a look at is and place orders through out the week so we can keep a better stock 
or overstock of these maintenance items to keep PDQ open and servicing guests. Thank you
Fred Van Heste II

10/20/23
Hey Jon I wanted yoiu to see this list as well. This is all the direct ship filters and blades that GM is 
telling us we can order ship direct. Once you get this figured out we can moniter these easier, i am hoping 
we can do a list for brake parts and some other parts that GM has told us we can order direct from the 
supplier. Any questions let me know, and thank you Jon
Fred Van Heste II

10/20/23
Qty on hand
Qty on order
And sales history for the last month i think is a good start 
sorry for the delay
Fred Van Heste II

--------------------------------------------------------------------------------------------------------------

select status, count(*)  A/N/C/R
from arkona.ext_pdpmast
group by status


status,  qty onhand, qty on order, qty on spe order, date_last_sold, cost, list_price, bin location, 
recent_demand (sold in last 90), prior_demand(sold in past year)

drop table if exists pts.parts;
create table pts.parts (
  part_number citext primary key,
  source citext);
 comment on table pts.parts is 'first take at the list of part numbers fred would like to monitor, source is where i got the part numbers from'; 


drop table if exists pts.tmp_parts;
create table pts.tmp_parts (
  part_number citext primary key,
  source citext); 
comment on table pts.tmp_parts is 'intermediate table, insert part numbers from the list(s) we get from Fred, enabling installing only parts into pts.parts that don''t yet exist in that table';



insert into pts.parts
select * 
from pts.tmp_parts a
where not exists (
  select 1
  from pts.parts
  where part_number = a.part_number)

select * from pts.parts order by part_number 

select a.*
from arkona.ext_pdpmast a
join pts.parts b on a.part_number = b.part_number
where a.company_number = 'RY1'

-- all those missing from pdpmast are from the gm list
select * 
from pts.parts a
where not exists (
  select 1
  from arkona.ext_pdpmast
  where part_number = a.part_number)

select a.part_number, b.part_description, b.qty_on_hand, b.qty_on_order
from pts.parts a
join arkona.ext_pdpmast b on a.part_number = b.part_number
  and b.company_number = 'RY1'

select * from arkona.ext_pdpmast where part_number = '19236616'

-- added sales part from "E:\sql\postgresql\misc_sql\parts\gm_lists\parts_qpo.sql"

select a.part_number, b.part_description, b.qty_on_hand, b.qty_on_order, 
  sum(c.ptqty) filter (where d.year_month = 202308) as aug_sales,
  sum(c.ptqty) filter (where d.year_month = 202309) as sep_sales,
  sum(c.ptqty) filter (where d.year_month = 202310) as oct_sales
from pts.parts a
join arkona.ext_pdpmast b on a.part_number = b.part_number
  and b.company_number = 'RY1'
left join arkona.ext_pdptdet c on a.part_number = c.ptpart
  and c.ptco_ = 'RY1'
  and c.ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
  -- 11/23 modified filter on ptsoep
--   and c.ptsoep is null
  and (c.ptsoep is null or c.ptsoep in ('N', 'E'))
left join dds.dim_date d on (select arkona.db2_integer_to_date_long(c.ptdate)) = d.the_date
where d.year_month between 202308 and 202310 -----------------------------
--   and b.status = 'A'  -- per mark's request 05/13/21 If you can just print out the ones that show active stock status, that would help.
group by a.part_number, b.part_description, b.qty_on_hand, b.qty_on_order;  

to test sales in usage in dealertrack
parts -> 10 -> enter part number -> 6 (demand)