﻿employee list for comply

select a.employee_number, a.first_name, a.last_name, a.primary_email, 
  case
    when a.store = 'GM' then 'Rydell Auto Center, Inc.'
    when a.store = 'HN' then 'H.G.F., Inc.'
    when a.store = 'Toyota' then 'Rydell Toyota of Grand Forks'
  end as company, c.department, a.cost_center as title, a.manager_1, coalesce(a.manager_2, '') as manager_2
from ukg.employees a
left join ukg.ext_employee_details b on a.employee_number = b.employee_number
left join (select * from ukg.get_cost_center_hierarchy()) c on b.cost_center_id = c.id
where a.status = 'active'
  and a.last_name <> 'api'
order by last_name

-- ok
select employee_number from (
select a.employee_number, a.first_name, a.last_name, a.primary_email, 
  case
    when a.store = 'GM' then 'Rydell Auto Center, Inc.'
    when a.store = 'HN' then 'H.G.F., Inc.'
    when a.store = 'Toyota' then 'Rydell Toyota of Grand Forks'
  end as company, c.department, a.cost_center as title, a.manager_1, coalesce(a.manager_2, '') as manager_2
from ukg.employees a
left join ukg.ext_employee_details b on a.employee_number = b.employee_number
left join (select * from ukg.get_cost_center_hierarchy()) c on b.cost_center_id = c.id
where a.status = 'active'
  and a.last_name <> 'api'
) x group by employee_number having count(*) > 1  


