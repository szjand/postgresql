﻿https://dba.stackexchange.com/questions/222900/jsonb-array-elements-in-the-select-seems-to-turn-a-left-join-into-an-inner-joi
my concern is the jsonb_array_elements in the select clause

the value array:
  the json [{[],{}}]
  [
    {
      "styleId": [
        398652,
        398662
      ],
      "attributes": {
        "value": "5",
        "condition": "LS"
      }
    },
    {
      "styleId": [
        398654,
        398664
      ],
      "attributes": {
        "value": "6",
        "condition": "6B-0"
      }
    }
  ]
-- this is what i sent to afton
select t.tech_specs ->> 'titleId' as title_id, t.tech_specs ->> 'value' as the_array
from chr2.describe_vehicle_by_vin a
join jsonb_array_elements(a.response->'technicalSpecification') as t(tech_specs) on true
where vin = '1FT7W2B66KEG20920'
  and t ->> 'titleId' = '8'

-- get rid of everything except the json object
-- the t.tech_specs consists of an object containing an object named range and an array named value
select t.tech_specs, t.tech_specs->'value' as value_array
from chr2.describe_vehicle_by_vin a
left join jsonb_array_elements(a.response->'technicalSpecification') as t(tech_specs) on true
where vin = '1FT7W2B66KEG20920'
  and t ->> 'titleId' = '8'

-- just the value array
select t.tech_specs->'value' as value_array
from chr2.describe_vehicle_by_vin a
join jsonb_array_elements(a.response->'technicalSpecification') as t(tech_specs) on true
-- join jsonb_array_elements(t.tech_specs->'value') as u(the_values) on true
where vin = '1FT7W2B66KEG20920'
  and t ->> 'titleId' = '8'

-- the issue here is i want to take the jsonb_array_element out of the select clause in the afton solution
-- think i figured it out in chr2.option
-- try that here

-- base
select t.tech_specs ->> 'titleId' as title_id, t.tech_specs -> 'value' as the_array
from chr2.describe_vehicle_by_vin a
join jsonb_array_elements(a.response->'technicalSpecification') as t(tech_specs) on true
where vin = '1FT7W2B66KEG20920'
  and t ->> 'titleId' = '8'

select t.tech_specs ->> 'titleId' as title_id, t.tech_specs -> 'value' as the_array
from chr2.describe_vehicle_by_vin a
join jsonb_array_elements(a.response->'technicalSpecification') as t(tech_specs) on true
where vin = '1FT7W2B66KEG20920'
  and t ->> 'titleId' = '8'

  
select t ->> 'titleId' as title_id, u.*, t.*
from chr2.describe_vehicle_by_vin a
join jsonb_array_elements(a.response->'technicalSpecification'->'value') as t on true
left join lateral jsonb_array_elements(t->'styleId') as u on true
where vin = '1FT7W2B66KEG20920'
  and t ->> 'titleId' = '8'
      
  
-- aftons response
with test_table 
  as ( 
    select t.tech_specs ->> 'titleId' as title_id, t.tech_specs -> 'value' as the_array
    from chr2.describe_vehicle_by_vin a
    join jsonb_array_elements(a.response->'technicalSpecification') as t(tech_specs) on true
    where vin = '1FT7W2B66KEG20920'
      and t ->> 'titleId' = '8') 
select title_id, jsonb_array_elements_text(styles->'styleId') as style_id, styles->'attributes'->>'value' as value, styles->'attributes'->>'condition' as condition
from test_table
join  jsonb_array_elements(the_array) as styles on true

select * from chr2.technical_specification_definition where title_id = 8
-- first challenge, rewrite afton's query removing jsonb_array_elements from select clause


this is what worked for chr.option, essentially unnest on an array of style ids


select 
  (c->'header'->'attributes'->>'id')::integer as header_id,
  c->'header'->>'$value' as header_name,
  (d.value->>0)::integer as style_id,
  de->>0 as descriptions
from chr2.describe_vehicle_by_vin a
join jsonb_array_elements(a.response->'factoryOption') as c on true
join jsonb_array_elements(c->'styleId') as d on true
join jsonb_array_elements(c->'description') as de on true
where a.vin = '1FT7W2B66KEG20920'
  and c->'header'->'attributes'->>'id' = '1160'
  and c->'attributes'->>'oemCode' = '99T' 