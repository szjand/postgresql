﻿do
$$
declare
  _stock_number citext := 'H11535';
begin  
drop table if exists gross_detail;
create temp table gross_detail as
select b.the_date, a.control,d.gl_account,
  e.journal_code, f.description as trans_desc, -a.amount as amount,
  sum(-amount) over (order by the_date rows between unbounded preceding and current row) as gross
--   sum(-amount) over (partition by journal_code)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
--   and b.year_month = (select year_month from sls.months where open_closed = 'open')
inner join fin.dim_account c on a.account_key = c.account_key
inner join (
    select distinct (case b.consolidation_grp when '2' then 'RY2' else 'RY1' end)::citext as store,
      a.fxmpge as page, a.fxmlne::integer as line, b.g_l_acct_number as gl_account
    from arkona.ext_Eisglobal_sypffxmst a 
    inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
      and a.fxmcyy = b.factory_financial_year
    where (
      a.fxmcyy = (select the_year from dds.dim_date where the_date = current_date)
      or
      a.fxmcyy = (select the_year - 1 from dds.dim_date where the_date = current_date))
      and (
        (a.fxmpge in (5,7,8,9,10,14) and (a.fxmlne between 1 and 19 or a.fxmlne between 25 and 40))  or
        (a.fxmpge = 16 and a.fxmlne between 1 and 6))) d on c.account = d.gl_account --b.year_month = d.year_month
inner join fin.dim_journal e on a.journal_key  = e.journal_key    
left join fin.dim_gl_description f on a.gl_description_key = f.gl_description_key
  and b.the_date between f.row_from_date and f.row_thru_date
where a.post_status = 'Y'
  and a.control = _stock_number;

end
$$;

select * from gross_detail;  



select the_date, control, gl_account, journal_code, sum(amount)
from gross_detail
group by the_date, control, gl_account, journal_code



-- Function: board.daily_deal_accounting_gross_pop()
why does this query include 271.68 against 164701 (ro 16315402) and the above query does not
it is the date

select aa.control, (sum(-aa.amount))::integer as the_sum,
coalesce((sum(-aa.amount) filter (where c.the_page < 17))::integer, 0) as front_gross,
coalesce((sum(-aa.amount) filter (where c.the_page = 17))::integer, 0) as back_gross
-- select *
from fin.fact_gl aa
inner join fin.dim_account b on aa.account_key = b.account_key
inner join (
  select distinct case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code,
  a.fxmpge as the_page, a.fxmlne as line, trim(b.g_l_acct_number) as gl_account
  from arkona.ext_eisglobal_sypffxmst a
  inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
    and a.fxmcyy = b.factory_financial_year
  where a.fxmcyy = 2018
    and coalesce(b. consolidation_grp, '1') <> '3'
    and b.g_l_acct_number <> ''
    and (
      (a.fxmpge between 5 and 15) or
      (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
      (a.fxmpge = 17 and a.fxmlne between 1 and 19))) c on b.account = c.gl_account
where aa.post_status = 'Y'
  and control = '29691C' order by account
group by aa.control  