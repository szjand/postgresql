﻿-- DO you know why Alexis Tureskis does not show stats? Her writer number is 518.  
-- Ryan Shroyer

so, apparently alexis tureskis was termed for part of August


so, we have a bunch of ros going back to the end of august where alexis was the writer but the data shows unknown
because dim_service_writer has her termed
so, first, since there are no ros under the termed key, fix dim_service_writer, get rid of the termed record
select * from dds.dim_service_Writer where writer_name like 'tur%' 
delete from dds.dim_service_writer
where service_writer_key = 1168;
update dds.dim_service_writer
set current_row = true, row_change_reason  = null, row_thru_date = '12/31/9999'
where service_writer_key = 1073;


-- oh fuck me i deleted 1068 by mistake
-- guess i am ok
-- select ro from dds.fact_repair_order where service_writer_key = 1068 limit 10

update dds.fact_repair_order
set service_writer_key = 1073
where ro in (
select distinct a.ro_number
from arkona.ext_sdprhdr a
join dds.fact_repair_order b on a.ro_number = b.ro
where service_writer_id = '518')


insert into pdq.ros  
  select a.store_code, a.open_date, a.ro, a.line, d.opcode, e.category, 
    c.writer_number, c.employee_number, c.last_name || ', ' || c.first_name
  from dds.fact_repair_order a
  join pdq.writers c on a.service_writer_key = c.writer_key
    and c.active
    and c.writer_key = 1073
  join dds.dim_opcode d on a.opcode_key = d.opcode_key
  join pdq.opcodes e on a.store_code = e.store
    and d.opcode = e.opcode
  where a.open_date > '08/31/2022' -- between _from_date and _thru_date
  group by a.store_code, a.open_date, a.ro, a.line, d.opcode, e.category,
    c.last_name || ', ' || c.first_name, c.writer_number, c.employee_number
  on conflict(ro,line) do nothing; 

select * from pdq.get_writer_stats();  