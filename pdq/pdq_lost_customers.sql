﻿I need your help please. For both the GM Store and the Honda / Nissan store, could you put together a 
list of roughly 8 to 12 VIN numbers of guests, that had a PDQ oil change done between 04/01/22 and 04/01/21. 
Then, from that list, could you determine how many of those guests have not returned for an oil 
change within the last 12 months, 04/01/22 to 04/01/23. 

Ultimately what I am looking for is around 6 to 8 customers from both stores who 
had an oil change done prior to the past 12 months, but within the last 24 months, 
and have not returned.   
 
What I am going to do is call 6 to 8 customers from that list, visit with them on why 
hey have not been back in with in the past 12 months.  

The goal is to try to determine why our PDQ volume is slipping. Maybe we can learn from 
these customers why they have not returned. 

select * from dds.dim_Service_type limit 1
select * from dds.fact_Repair_order limit 1
select * from dds.dim_customer limit 1

select a.ro, a.line, a.open_date, b.opcode, b.description, d.full_name, e.vin
from dds.fact_repair_order a
join dds.dim_opcode b on a.opcode_key = b.opcode_key
join dds.dim_service_type c on a.service_type_key = c.service_type_key
  and c.service_type_code = 'QL'
join dds.dim_customer d on a.customer_key = d.customer_key  
join dds.dim_vehicle e on a.vehicle_key = e.vehicle_key
where a.open_date between '04/01/2021' and '03/31/2022'
limit 100

-- select count(*)
drop table if exists jon.pdq_oil_changes_1;
create table if not exists jon.pdq_oil_changes_1 (
  store citext not null,
  the_date date not null,
  ro citext primary key);
comment on table jon.pdq_oil_changes_1 is 'pdq oil changes done between 4/1/21 and 3/31/22';
insert into jon.pdq_oil_changes_1
select max(a.store) as store, max(a.the_date) as the_date, a.ro 
from pdq.ros a
where a.store in ('ry1','ry2')
  and a.the_date between '04/01/2021' and '03/31/2022'
  and a.category = 'LOF'
group by ro -- 7 ros out of 33289 have multiple oil change opcodes on the ro  


drop table if exists jon.pdq_oil_changes_1a;
create table if not exists jon.pdq_oil_changes_1a (
  store citext not null,
  the_date date not null,
  ro citext primary key,
  vin citext not null,
  bnkey integer not null,
  full_name citext not null);
comment on table jon.pdq_oil_changes_1a is 'add vin and customer to pdq oil changes done between 4/1/21 and 3/31/22';
insert into jon.pdq_oil_changes_1a
select a.store, a.the_date, a.ro, c.vin, d.bnkey, d.full_name
from jon.pdq_oil_changes_1 a
join dds.fact_repair_order b on a.ro = b.ro
join dds.dim_vehicle c on b.vehicle_key = c.vehicle_key
join dds.dim_customer d on b.customer_key = d.customer_key
group by a.store, a.the_date, a.ro, c.vin, d.bnkey, d.full_name;


drop table if exists jon.pdq_oil_changes_2;
create table if not exists jon.pdq_oil_changes_2 (
  store citext not null,
  the_date date not null,
  ro citext primary key);
comment on table jon.pdq_oil_changes_2 is 'pdq oil changes done between 4/1/22 and 5/1/23';
insert into jon.pdq_oil_changes_2
select max(a.store) as store, max(a.the_date) as the_date, a.ro 
-- select a.*
from pdq.ros a
where a.store in ('ry1','ry2')
  and a.the_date between '04/01/2022' and '05/01/2023'
  and a.category = 'LOF'
group by ro;


drop table if exists jon.pdq_oil_changes_2a;
create table if not exists jon.pdq_oil_changes_2a (
  store citext not null,
  the_date date not null,
  ro citext primary key,
  vin citext not null,
  bnkey integer not null,
  full_name citext not null);
comment on table jon.pdq_oil_changes_2a is 'add vin and customer to pdq oil changes done between 4/1/22 and 5/1/23';
insert into jon.pdq_oil_changes_2a
select a.store, a.the_date, a.ro, c.vin, d.bnkey, d.full_name
from jon.pdq_oil_changes_2 a
join dds.fact_repair_order b on a.ro = b.ro
join dds.dim_vehicle c on b.vehicle_key = c.vehicle_key
join dds.dim_customer d on b.customer_key = d.customer_key
group by a.store, a.the_date, a.ro, c.vin, d.bnkey, d.full_name;

drop table if exists jon.pdq_lost_customers_1;
create table if not exists jon.pdq_lost_customers_1 (
  store citext not null,
  the_date date not null,
  ro citext primary key,
  vin citext not null,
  bnkey integer not null,
  full_name citext not null);  
comment on table jon.pdq_lost_customers_1 is 'first cut of potentially lost pdq customers, vins that appear in pdq_oil_changes_1a but not pdq_oil_changes 2a';

insert into jon.pdq_lost_customers_1  -- 11831
select a.* 
from jon.pdq_oil_changes_1a a
left join jon.pdq_oil_changes_2a b on a.vin = b.vin
where b.vin is null;


select bnkey, vin, array_agg(ro), count(*)
from jon.pdq_lost_customers_1
group by bnkey, vin

drop table if exists jon.pdq_lost_customers_2;
create table if not exists jon.pdq_lost_customers_2 (
  store citext not null,
  the_date date not null,
  ro citext primary key,
  vin citext not null,
  bnkey integer not null,
  full_name citext not null);  
comment on table jon.pdq_lost_customers_2 is 'narrow the list of potentially lost pdq customers, to those, that at least in dealertrack, still own the vehicle';
insert into jon.pdq_lost_customers_2  -- 9714
select distinct a.*
from jon.pdq_lost_customers_1 a
join arkona.ext_bopvref b on a.vin = b.vin
  and b.end_date = 0
  and a.bnkey = b.customer_key

select bnkey, vin, array_agg(ro), count(*)
from jon.pdq_lost_customers_2
group by bnkey, vin

-- i know we are looking for pdq business, but i would feel bad calling a customer from this list
-- that had been back with that vehicle for service other than pdq

drop table if exists jon.all_ros;
create table if not exists jon.all_ros (
  open_date date,
  ro citext,
  vin citext,
  bnkey integer);
comment on table jon.all_ros is 'all ros since 3/31/2022, to check against potentially lost pdq customers';  
insert into jon.all_ros
select a.open_date, a.ro, b.vin, c.bnkey
from dds.fact_repair_order a
join dds.dim_vehicle b on a.vehicle_key = b.vehicle_key
join dds.dim_customer c on a.customer_key = c.customer_key
where a.open_date > '03/31/2022'
group by a.open_date, a.ro, b.vin, c.bnkey;
create index on jon.all_ros(vin);
create index on jon.all_ros(bnkey);

select * 
from jon.pdq_lost_customers_2 a
join jon.all_ros b on a.vin = b.vin
  and a.bnkey = b.bnkey

select *
from (
select bnkey, vin, array_agg(ro), count(*)
from jon.pdq_lost_customers_2
group by bnkey, vin) a
join jon.all_ros b on a.vin = b.vin
  and a.bnkey = b.bnkey

drop table if exists jon.pdq_lost_customers_3;
create table if not exists jon.pdq_lost_customers_3 (
  store citext not null,
  the_date date not null,
  ro citext primary key,
  vin citext not null,
  bnkey integer not null,
  full_name citext not null);  
comment on table jon.pdq_lost_customers_3 is 'narrow the list of potentially lost pdq customers further, eliminate any customer that had any ro past 3/31/2022';
insert into jon.pdq_lost_customers_3  -- 7777
select a.*
from jon.pdq_lost_customers_2 a
where not exists (
  select 1
  from jon.all_ros 
  where bnkey = a.bnkey)  

drop table if exists jon.pdq_lost_customers_4;
create table if not exists jon.pdq_lost_customers_4 (
  store citext not null,
  the_date date not null,
  ro citext primary key,
  vin citext not null,
  bnkey integer not null,
  full_name citext not null,
  ros integer);  
comment on table jon.pdq_lost_customers_4 is 'narrow the list of potentially lost pdq customers further, to those with multiple ros before 3/31/2022';
insert into jon.pdq_lost_customers_4
select a.*,  b.ros
from jon.pdq_lost_customers_3 a
join (
	select bnkey, vin, count(*) as ros
	from jon.pdq_lost_customers_3
	group by bnkey, vin
	having count(*) > 1) b on a.vin = b.vin
  and a.bnkey = b.bnkey; 



select * from jon.pdq_lost_customers_4 where store = 'RY2' and ros > 2 order by vin
RY2,2021-11-04,2847077,19XFC2F62KE030989,1150198,NAJIMOV, KIRNIECE,3
RY2,2022-03-21,2853445,1FMJK2AT9LEA44326,300048,JUNDT, RYAN MICHAEL,3
RY2,2022-01-21,2850799,1GNKVGKD2DJ131649,1031261,DOESCHER JR, DONALD,3
RY2,2022-02-24,2852254,2HKRL18552H576354,1019472,GUDVANGEN, JAMES EARL,3
RY2,2022-03-02,2852590,2HKRW2H84LH698969,282474,BELL, CORRINE,4
RY2,2021-04-05,2836927,2T3JFREV9JW760579,236285,FINCHER, AMANDA,3
RY2,2021-06-08,2840035,5FNRL5H69DB062296,274886,BALMER, DON,5
RY2,2021-11-20,2847852,5FNYF4H50DB047318,1165103,CAVANAUGH, MARY,3
RY2,2022-02-05,2851426,5FNYF6H51LB063358,294646,WEST, TERRY JAMES,5
RY2,2022-01-18,2850569,5J6RM4H73DL037904,1154786,FRY, KAYCDE,5
RY2,2022-02-21,2852064,5N1AR1NB4BC609261,1123171,ARROYO, LORENA,3
RY2,2022-03-04,2852709,2C4RDGCG7KR771996,1159923,RINDE, PAUL,4







select * from jon.pdq_lost_customers_4 where store = 'RY1' and ros > 2 order by vin
RY1,2022-02-14,16507048,3GKALVEV5ML302748,1159537,BROBERG, JANICE,5
RY1,2022-02-26,16508923,1GKDT13W4Y2188685,1029714,TOENYAN, STEVE,3
RY1,2022-03-30,16514256,1GT12UEY3HF102423,1125999,FORDE, JOSHUA ALLAN,4
RY1,2022-02-02,16505639,1C4PJMCX5KD306280,1159918,DOBMEIER, EMILY ANN,3
RY1,2022-03-23,16512905,1C6SRFCT8KN665118,1064799,EVENSON, ANDREW,3
RY1,2022-02-19,19365522,1FMCU9GX2FUB94858,222093,SOUDER, DEANNE,3
RY1,2022-03-15,16511513,1FMDK01195GA79455,265876,LIGGETT, JENNY,3
RY1,2022-02-17,16507821,1FTPW14V87FA19976,1158340,ANDRUSKI, DUSTIN,3
RY1,2022-03-21,16512394,1G1ZD5ST4KF176638,275424,CARPENTER, RENAE,4
RY1,2022-03-17,16512011,1GC1KVE84GF259592,1110352,KUGLIN, STEVE,3
RY1,2022-03-19,16512253,1GC2KVEG8FZ139657,1090940,SCHULZ, ETHAN DOUGLAS,4
RY1,2022-03-12,16511185,1GCEK19BX5E208231,1131903,DEMERS, WYATT,3

-- do a final check on bnkey in ros after 4/1/22


select 
from jon.all_ros where bnkey in (1159923,257550,222329,1120770,294646,277823,274886,239120,282474,1154953,226900,1127556) order by bnkey


-- ry2
select bopname_company_number, bopname_search_name, phone_number, business_phone, cell_phone
from arkona.ext_bopname a
where bopname_record_key in (1150198,300048,1031261,1019472,282474,236285,274886,1165103,294646,1154786,1123171,1159923)

-- ry1
select bopname_company_number, bopname_search_name, phone_number, business_phone, cell_phone 
from arkona.ext_bopname
where bopname_record_key in (1159537,1029714,1125999,1159918,1064799,222093,265876,1158340,275424,1110352,1090940,1131903)


-- 05/10/22 this is what i sent to randy
select 'Honda Nissan' as store, a.full_name, a.vin, b.phone_number, b.business_phone, b.cell_phone
from (
	select store, full_name, vin, bnkey
	from jon.pdq_lost_customers_4 
	where store = 'RY2' 
		and bnkey in (1150198,300048,1031261,1019472,282474,236285,274886,1165103,294646,1154786,1123171,1159923)
	group by store, full_name, vin, bnkey) a
left join arkona.ext_bopname b on a.bnkey = b.bopname_record_key

union

select 'GM' as store, a.full_name, a.vin, b.phone_number, b.business_phone, b.cell_phone
from (
	select store, full_name, vin, bnkey
	from jon.pdq_lost_customers_4 
	where store = 'RY1' 
		and bnkey in (1159537,1029714,1125999,1159918,1064799,222093,265876,1158340,275424,1110352,1090940,1131903)
		and vin <> '1G11B5SA5DF323845'
	group by store, full_name, vin, bnkey) a
left join arkona.ext_bopname b on a.bnkey = b.bopname_record_key
order by store, full_name