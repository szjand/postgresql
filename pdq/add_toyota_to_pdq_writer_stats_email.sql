﻿-- Daytons response to what is needed to include toyota: 
-- The writers that should be included are David Lacy (401) Jeff Stubb (402) Samual Koss (403) Conner Gothberg (404) and Charles Hosek (801).
-- 
-- The list of opp codes are ;
-- Rotates: RT 
-- Cabin Filter: Cabin
-- Engine Filter: AF
-- Wiper blades: WB
-- Bulbs: Bulb
-- Battery: BAT
-- 
-- OIL CHANGE OPP CODES;
-- LOFP
-- ILIFE
-- ILIFED
-- ILIFES
-- ILIFESD
-- 
-- Relavant sales accounts are;
-- 
-- Express Lube Toyota – 4435
-- c/s Express lube Toyota – 6435.
-- 
-- I think to start lets have Chuck, Randy, Jeff Stubb and myself to receive the emails for now if possible. 


--------------------------------------------------------------------------------------
--< step 1 pdq.get_ros()
--------------------------------------------------------------------------------------
requires the opcodes:
select * from pdq.opcodes
select distinct category from pdq.opcodes
insert into pdq.opcodes (store,category,opcode)values
('RY8','Rotate','RT'),
('RY8','Cabin Filters','Cabin'),
('RY8','Air Filters','AF'),
('RY8','Wipers','WB'),
('RY8','Bulbs','Bulb'),
('RY8','Batteries','BAT'),
('RY8','LOF','LOFP'),
('RY8','LOF','ILIFE'),
('RY8','LOF','ILIFED'),
('RY8','LOF','ILIFES'),
('RY8','LOF','ILIFESD');

requires the writers
select * from pdq.writers where active
-- have to remove this unique index on writer_number, same writer number across stores, 401
DROP INDEX pdq.writers_writer_number_idx;
-- except it is a FK to pdq.ros
ALTER TABLE pdq.ros DROP CONSTRAINT ros_writer_number_fkey;

insert into pdq.writers (first_name,last_name,employee_number,writer_number,writer_key,store,active) values
('David','Lacy','8100214','401',1152,'RY8',true),
('Jeff','Stubb','8100228','402',1153,'RY8',true),
('Samual','Koss','8100213','403',1154,'RY8',true),
('Conner','Gothberg','8100233','404',1176,'RY8',true),
('Charles','Hosek','8100209','801',1155,'RY8',true);

function pdq.get_ros() now includes toyota ros

comment on TABLE pdq.ros is 'updated nightly from luigi pdq.py call to pdq.get_ros()';

select * from pdq.ros where store = 'ry8'
--------------------------------------------------------------------------------------
--/> step 1 pdq.get_ros()
--------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------
--< step 2 pdq.udpate_ro_sales()
--------------------------------------------------------------------------------------
select * from pdq.sale_accounts

never mind, from jeri:
We don’t currently have separate PDQ parts accounts.

4435 the only one—4435D is the offsetting discount account.  Given the way their pdq is structured, I am not treating this as a department separate from service.

so, no sales numbers for toyota

insert into pdq.sale_accounts(account,account_key,department_code,store,description)
select account, account_key, department_code, 'Toyota', description
from fin.dim_account
where account = '4435';

delete from pdq.sale_Accounts
where account = '4435'
  and store = 'toyota';

if not from the accounts, maybe from the ros

select * 
from pdq.ros 
where the_date between '09/25/2022' and '10/08/2022'

from dayton:
So here is one way I have to help isolate and tabulate pdq sales. I created a report in dealertrack that I can pull to see what 
we have for numbers. Not sure if its 100% accurate but it at least gives me a baseline. Typically when I run this report, I go 
into the service tab, go to service reports, click on the tab that says sales by labor operation. Once im in there, I will 
select the month or  days I want. For the service type I select everything besides the bodyshop option to make sure I am pulling 
from everywhere with in the service department that I can. Next I will click the labor op codes and select the one I built 
labeled “quick lane”. Then I select my service advisors and include Conner gothberg and Chuck in there as well. 
They I will generate the report. 

my concern is here is a bunch of pdq ros with opcodes that are not pdq opcodes
select aa.*
from (
	select a.ro, c.opcode
	from dds.fact_repair_order a
	join pdq.ros b on a.ro = b.ro
	join dds.dim_opcode c on a.opcode_key = c.opcode_key
	where a.store_code = 'ry8'
	group by a.ro, c.opcode) aa
where not exists (
  select 1
  from pdq.opcodes
  where store = 'ry8'
    and opcode = aa.opcode)

maybe exclude internal work

-- eek, validity of opcodes, eg rotate vs rt
select aa.*, bb.*
from (
	select a.ro, c.opcode
	from dds.fact_repair_order a
	join pdq.ros b on a.ro = b.ro
	join dds.dim_opcode c on a.opcode_key = c.opcode_key
	where a.store_code = 'ry8'
	group by a.ro, c.opcode) aa
left join dds.dim_opcode bb on aa.opcode = bb.opcode	
  and bb.store_code = 'ry8'
where not exists (
  select 1
  from pdq.opcodes
  where store = 'ry8'
    and opcode = aa.opcode)

select * from pdq.ros
where store = 'ry8'
limit 10

select opcode, count(*) 
from pdq.ros
where store = 'ry8'
group by opcode

-- holy shit 240 rotate opcodes
select * 
from dds.dim_opcode
where store_code = 'ry8'
  and description like '%ROTAT%'    
--------------------------------------------------------------------------------------
--< step 2 pdq.udpate_ro_sales()
--------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------
--< step 3 pdq.get_writer_stats()
--------------------------------------------------------------------------------------
-- no changes req'd
select b.*, -- b.lof * c.wd_in_biweekly_pay_period/c.wd_of_biweekly_pay_period as "lof pacing"
  case
    when c.wd_of_biweekly_pay_period = 0 then 0 else
    b.lof * c.wd_in_biweekly_pay_period/c.wd_of_biweekly_pay_period 
  end as "lof pacing"
from (
  select a.store, a.writer_number, (a.last_name || ', ' || a.first_name)::citext as writer, a.employee_number,
    count(*) filter (where b.category = 'LOF') as LOF,
    (count(*) filter (where b.category = 'Rotate'))::text || ' / ' || 
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Rotate')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as rotate,
    (count(*) filter (where b.category = 'Air Filters'))::text || ' / ' ||  
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Air Filters')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as "air filters",
    (count(*) filter (where b.category = 'Cabin Filters'))::text || ' / ' ||  
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Cabin Filters')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as "cabin filters",
    (count(*) filter (where b.category = 'Batteries'))::text || ' / ' ||  
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Batteries')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as batteries,    
    (count(*) filter (where b.category = 'Bulbs'))::text || ' / ' ||  
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Bulbs')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as bulbs,
    (count(*) filter (where b.category = 'Wipers'))::text || ' / ' || 
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Wipers')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as wipers  
  from pdq.writers a  
  left join pdq.ros b on a.employee_number = b.employee_number
    and b.the_date between '09/25/2022' and '10/08/2022'
  where a.active
  group by a.store, a.writer_number, a.last_name || ', ' || a.first_name, a.employee_number) b 
join dds.working_days c on c.department = 'pdq'
  and c.the_date = current_date - 1  
union
select b.store, b.writer_number, b.writer, b.employee_number,
  b.lof, 
  case when b.lof = 0 then '0' else (round(100.0 * b.rotate/b.lof, 1))::text end || '%',
  case when b.lof = 0 then '0' else round(100.0 * b."air filters"/b.lof, 1)::text end || '%', 
  case when b.lof = 0 then '0' else round(100.0 * b."cabin filters"/b.lof, 1)::text end || '%',
  case when b.lof = 0 then '0' else round(100.0 * b.batteries/b.lof, 1)::text end || '%', 
  case when b.lof = 0 then '0' else round(100.0 * b.bulbs/b.lof, 1)::text end || '%', 
  case when b.lof = 0 then '0' else round(100.0 * b.wipers/b.lof, 1)::text end || '%',
  case
    when c.wd_of_biweekly_pay_period = 0 then 0 else
    b.lof * c.wd_in_biweekly_pay_period/c.wd_of_biweekly_pay_period 
  end as "lof pacing"
  from (
    select a.store, 'Total LOF & Penetration'::citext as writer_number,
      null::citext as writer ,null::citext as employee_number,
      count(*) filter (where a.category = 'LOF') as LOF,
      count(*) filter (where a.category = 'Rotate') as rotate,
      count(*) filter (where a.category = 'Air Filters') as "air filters",
      count(*) filter (where a.category = 'Cabin Filters') as "cabin filters",
      count(*) filter (where a.category = 'Batteries') as batteries,
      count(*) filter (where a.category = 'Bulbs') as bulbs,
      count(*) filter (where a.category = 'Wipers') as wipers
    from pdq.ros a
    where a.the_date between '09/25/2022' and '10/08/2022'
    group by a.store) b
  join dds.working_days c on c.department = 'pdq'
    and c.the_date = current_date - 1
order by store, writer nulls last;

--------------------------------------------------------------------------------------
--/> step 3 pdq.get_writer_stats()
--------------------------------------------------------------------------------------