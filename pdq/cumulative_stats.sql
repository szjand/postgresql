﻿/*
occasionally, darek & ryan ask for cumulative data

Good morning Jon! 

I was wondering if you could help me out with something. Do you remember last year when I asked if you could 
send me a report with all the penetration numbers on it for the PDQ writers? I was wondering if you could do 
it again for both the GM store and Honda Store but for the first quarter of 2021? Let me know If you have any questions!

Thank you,
Dayton Marek 

i just past the csv into the email body

this is just an altered version of function  pdq.get_writer_stats()

Also is there a way you could set up at the end of every month I get a total number and percent that we were 
running for the month? Just makes it easier for me to keep track how we do month to month and what we all sell! 
Let me know if that works! 

create a function based on this script to do this
*/

select *  from dds.dim_date where the_date = current_date

do $$
declare
  _from_date date := '01/01/2021';
  _thru_date date := current_date;  
begin
drop table if exists wtf;
create temp table wtf as
  select 
    case a.store
			when 'RY1' THen 'GM'
			when 'RY2' THEN 'Honda Nissan'
	  end as store,
    count(*) filter (where b.category = 'LOF') as LOF,
    (count(*) filter (where b.category = 'Rotate'))::text || ' / ' || 
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Rotate')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as rotate,
    (count(*) filter (where b.category = 'Air Filters'))::text || ' / ' ||  
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Air Filters')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as "air filters",
    (count(*) filter (where b.category = 'Cabin Filters'))::text || ' / ' ||  
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Cabin Filters')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as "cabin filters",
    (count(*) filter (where b.category = 'Batteries'))::text || ' / ' ||  
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Batteries')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as batteries,    
    (count(*) filter (where b.category = 'Bulbs'))::text || ' / ' ||  
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Bulbs')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as bulbs,
    (count(*) filter (where b.category = 'Wipers'))::text || ' / ' || 
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Wipers')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as wipers  
  from pdq.writers a  
  left join pdq.ros b on a.employee_number = b.employee_number
    and b.the_date between _from_date and _thru_date
--   where a.active
  group by a.store;
end $$;

select * from wtf order by store;
