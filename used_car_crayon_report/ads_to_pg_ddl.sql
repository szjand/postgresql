﻿create TABLE ads.ext_party_relationships(
    PartyID1 citext,
    PartyID2 citext,
    Typ citext,
    FromTS timestamp with time zone,
    ThruTS timestamp with time zone);

drop table if exists ads.ext_typ_descriptions;
create TABLE ads.ext_typ_descriptions(
    Typ citext,
    FromTS timestamp with time zone,
    ThruTS timestamp with time zone,
    Description citext,
    Sequence numeric);

 
create TABLE ads.ext_vehicle_item_options(
    VehicleItemID citext,
    VehicleOptions citext,
    VehicleWheels citext,
    PreviousBodyDamage boolean,
    PreviousBodyDamageAmount integer,
    EmissionSystemIntact boolean,
    ExtendedServiceContract boolean,
    WindshieldGlass citext,
    WindshieldGlassAmount integer,
    ActualMileage boolean,
    AdditionalEquipment citext,
    VehicleItemOptionTS timestamp with time zone);

create TABLE ads.ext_gross_log_table(
    Year integer,
    Month integer,
    Week integer,
    day integer,
    TheDate date,
    StockNumber citext,
    Make citext,
    Model citext,
    VehicleSegment citext,
    VehicleType citext,
    LocationID citext,
    SalesStatus citext,
    SoldAmount numeric (12,2),
    CostOfSale numeric (12,2),
    CostPostSale numeric (12,2),
    TotalCost numeric (12,2),
    DealGross numeric (12,2),
    TotalGross numeric (12,2));

drop table if exists ads.ext_glump_limits;
create TABLE ads.ext_glump_limits(
    LocationID citext,
    VehicleTyp citext,
    GlumpTyp citext,
    LowerLimit numeric,
    UpperLimit numeric,
    Description citext,
    ShortVehicleTyp citext);

create TABLE ads.ext_black_book_values(
    TableKey citext,
    BasisTable citext,
    VehicleItemID citext,
    ExtraCleanBase integer,
    CleanBase integer,
    AverageBase integer,
    RoughBase integer,
    RegionalAdjustment integer,
    ExtraCleanMileageAdjustment integer,
    CleanMileageAdjustment integer,
    AverageMileageAdjustment integer,
    RoughMileageAdjustment integer,
    AddsDeductsTotal integer,
    Mileage integer,
    BlackBookPubDate date,
    BlackBookValueTS timestamp with time zone);
