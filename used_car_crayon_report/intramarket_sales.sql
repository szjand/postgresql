﻿--------------------------------------------------------------------------
--< intramarket G stocknumbers 
need to adjust days to sell, combination of orig store + retail store
--------------------------------------------------------------------------

select * 
from cra.temp1
where right(stock_number, 1) = 'G'
order by date_sold


select a.stocknumber, a.vin,daystosell,daysonlot,daysowned,datesold,price,disposition
-- select *
from cra.temp1 a
where vin in (
	select vin 
	from cra.temp1
	where right(stocknumber, 1) = 'G')
order by a.vin, a.location

select typ, count(*) from ads.ext_vehicle_sales group by typ

select * 
from ads.ext_Vehicle_sales a
join ads.ext_organizations b on a.soldto = b.partyid 
where soldto is not null and length(soldto) > 25 limit 100


select b.fullname, count(*)
from ads.ext_Vehicle_sales a
join ads.ext_organizations b on a.soldto = b.partyid 
  and b.fullname in ('GF-Honda Cartiva','GF-Rydells','GF-Rydell Auto Outlet')
group by b.fullname  

select b.fullname, a.vehicleinventoryitemid
from ads.ext_Vehicle_sales a
join ads.ext_organizations b on a.soldto = b.partyid 
  and b.fullname in ('GF-Honda Cartiva','GF-Rydells','GF-Rydell Auto Outlet')

-- select vin from ( -- 15 where the vin is sold more than twice
select a.stocknumber, a.vin,daystosell,daysonlot,daysowned,datesold,price,disposition, aa.fullname
from cra.temp1 a
left join ( -- sales to one of the 3 locations
	select b.fullname, a.vehicleinventoryitemid
	from ads.ext_Vehicle_sales a
	join ads.ext_organizations b on a.soldto = b.partyid 
		and b.fullname in ('GF-Honda Cartiva','GF-Rydells','GF-Rydell Auto Outlet')) aa on a.vehicleinventoryitemid = aa.vehicleinventoryitemid
where vin in ( -- vin of G stocknumbers
	select vin 
	from cra.temp1
	where right(stocknumber, 1) = 'G')
-- ) x group by vin having count(*) > 2	 -- 15 where the vin is sold more than twice
order by a.vin, a.location  
--------------------------------------------------------------------------
--/> intramarket G stocknumbers
--------------------------------------------------------------------------

-- separate intramarket from regular wholesale

select * from ads.ext_vehicle_sales where vehicleinventoryitemid in ('fe1ac79c-145d-45ef-afc7-caa4500c4c30','1c7d37a5-c862-4d40-945b-d22f80f31f7f','5fa9c811-daad-428a-a202-36008f7dc834')


select a.vehicleinventoryitemid,
  coalesce(b.fullname, a.soldto)
from ads.ext_vehicle_sales a
left join ads.ext_organizations b on a.soldto = b.partyid
where a.vehicleinventoryitemid in ('fe1ac79c-145d-45ef-afc7-caa4500c4c30','1c7d37a5-c862-4d40-945b-d22f80f31f7f','5fa9c811-daad-428a-a202-36008f7dc834')

select coalesce(b.fullname, a.soldto), count(*), min(soldts), max(soldts)
from ads.ext_vehicle_sales a
left join ads.ext_organizations b on a.soldto = b.partyid
where a.typ = 'VehicleSale_Wholesale'
  and soldts::date > '05/01/2017'
group by coalesce(b.fullname, a.soldto)
order by count(*) desc

-- candidate for updating a new field in base tables
select a.stock_number, a.vin, a.date_sold, aa.ws_to
from cra.temp1 a
left join (
	select a.vehicleinventoryitemid,
		case
			when coalesce(b.fullname, a.soldto) in ('GF-Rydells','GF-Honda Cartiva') then 'intramarket'
			when coalesce(b.fullname, a.soldto) in ('adesa fargo','midstate','other auction','adesa minneapolis') then 'auction'
			else 'other'
		end as ws_to
	from ads.ext_vehicle_sales a
	left join ads.ext_organizations b on a.soldto = b.partyid
	where a.typ = 'VehicleSale_Wholesale'
		and soldts::date > '05/01/2017') aa on a.vehicle_inventory_item_id = aa.vehicleinventoryitemid
where a.sales_status = 'wholesaled'		



select a.stock_number, a.vin, a.date_sold, aa.ws_to
from cra.five_years a
left join (
	select a.vehicleinventoryitemid,
		case
			when coalesce(b.fullname, a.soldto) in ('GF-Rydells','GF-Honda Cartiva') then 'intramarket'
			when coalesce(b.fullname, a.soldto) in ('adesa fargo','midstate','other auction','adesa minneapolis') then 'auction'
			else 'other'
		end as ws_to, c.typ
	from ads.ext_vehicle_sales a
	left join ads.ext_organizations b on a.soldto = b.partyid
	where a.typ = 'VehicleSale_Wholesale'
		and soldts::date > '05/01/2017') aa on a.vehicle_inventory_item_id = aa.vehicleinventoryitemid
where a.sales_status = 'wholesaled'		

five_years.source does not show -- intramarket
select a.stock_number, a.vin, b.typ, date_acquired, a.source
from cra.five_years a
left join ads.ext_vehicle_acquisitions b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
where right(stock_number, 1) in ('G','L')
order by a.date_acquired desc

select source, b.typ, right(a.stock_number, 1), count(*) 
from cra.five_years a
left join ads.ext_vehicle_acquisitions b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
where right(stock_number, 1) in ('G','L')
group by source, b.typ, right(a.stock_number, 1)

select a.*, b.*
from ( --1704
	select stock_number, vin, date_acquired, date_sold 
	from cra.five_years
	where right(stock_number, 1) = 'G') a
left join (
  select stock_number, vin, date_acquired, date_sold
  from cra.five_years 
  where right(stock_number, 1) <> 'G') b on a.vin = b.vin
where b.date_sold <> a.date_acquired

select a.stock_number, a.vin, a.source
from cra.five_years a

