﻿
drop table if exists cra.base_1 cascade;
create table cra.base_1 (
  the_date date not null,
  shape citext not null,
  size citext not null,
  price_bands_5k citext not null,
  primary key(the_date,shape,size,price_bands_5k));
create index on cra.base_1(shape);
create index on cra.base_1(size);
create index on cra.base_1(price_bands_5k);

insert into cra.base_1
-- select a.the_date, c.shape, c.size, b.price_bands_5k
-- from dds.dim_date a
-- join dds.working_days aa on a.the_date = aa.the_date
--   and aa.wd_of_year > 0
--   and aa.department = 'sales'
select a.the_date, c.shape, c.size, b.price_bands_5k
from (
	select the_date
	from dds.dim_date
	where day_of_week between 2 and 7
		and the_date between '05/15/2021' and '05/14/2022'  
		and 
			case  -- exclude holidays except memorial day and 4th july
				when month_name not in ('may','july') then not holiday
				else true
			end) a   
cross join (
	select distinct price_bands_5k
	from cra.temp1 
	where price_bands_5k is not null
	order by price_bands_5k) b
cross join (
	select distinct vehicle_type as shape, segment as size
	from cra.temp1
	where vehicle_type is not null
		and segment is not null) c
where a.the_date between '05/15/2021' and '05/14/2022'
order by a.the_date, c.shape, c.size, b.price_bands_5k;

drop table if exists cra.base_2 cascade;
create table cra.base_2 (
  the_date date not null,
  shape citext not null,
  size citext not null,
  price_bands_5k citext not null,
  sales integer not null,
  primary key(the_date,shape,size,price_bands_5k));
create index on cra.base_2(shape);
create index on cra.base_2(size);
create index on cra.base_2(price_bands_5k);

insert into cra.base_2
select a.*, coalesce(aa.sales, 0) as sales
from cra.base_1 a
left join (
	select date_sold, price_bands_5k, vehicle_type as shape, segment as size, count(*) as sales
	from cra.temp1
	where sales_status = 'sold'
	group by date_sold, price_bands_5k, vehicle_type, segment) aa on a.the_date = aa.date_sold
		and a.price_bands_5k = aa.price_bands_5k
		and a.shape = aa.shape
    and a.size = aa.size
order by a.the_date, a.shape, a.size, a.price_bands_5k;    



-- this is it
select b.*, 
  sum(sales) over (partition by b.shape, b.size, b.price_bands_5k order by b.the_date rows between 29 preceding and current row) as prev_30
from cra.base_1 a
left join cra.base_2 b on a.the_date = b.the_date
  and a.shape = b.shape
  and a.size = b.size
  and a.price_bands_5k = b.price_bands_5k
order by b.shape, b.size, b.price_bands_5k, b.the_date

-- only rows where sales <> 0
select * 
from (
select b.*, 
  sum(sales) over (partition by b.shape, b.size, b.price_bands_5k order by b.the_date rows between 29 preceding and current row) as prev_30
from cra.base_1 a
left join cra.base_2 b on a.the_date = b.the_date
  and a.shape = b.shape
  and a.size = b.size
  and a.price_bands_5k = b.price_bands_5k) c
where sales <> 0  
order by shape, size, price_bands_5k, the_date


select shape, size, price_bands_5k, avg(prev_30)::integer avg_prev_30
from (
	select b.*, 
		sum(sales) over (partition by b.shape, b.size, b.price_bands_5k order by b.the_date rows between 29 preceding and current row) as prev_30
	from cra.base_1 a
	left join cra.base_2 b on a.the_date = b.the_date
		and a.shape = b.shape
		and a.size = b.size
		and a.price_bands_5k = b.price_bands_5k) c
group by shape, size, price_bands_5k		
order by shape, size, price_bands_5k

---------------------------------------------------------------------------------
-- do the same thing with 2k price bands

drop table if exists cra.base_1_2k_pb cascade;
create table cra.base_1_2k_pb (
  the_date date not null,
  shape citext not null,
  size citext not null,
  price_bands_2k citext not null,
  primary key(the_date,shape,size,price_bands_2k));
create index on cra.base_1_2k_pb(shape);
create index on cra.base_1_2k_pb(size);
create index on cra.base_1_2k_pb(price_bands_2k);

insert into cra.base_1_2k_pb
-- select a.the_date, c.shape, c.size, b.price_bands_5k
-- from dds.dim_date a
-- join dds.working_days aa on a.the_date = aa.the_date
--   and aa.wd_of_year > 0
--   and aa.department = 'sales'
select a.the_date, c.shape, c.size, b.price_band_desc
from (
	select the_date
	from dds.dim_date
	where day_of_week between 2 and 7
		and the_date between '05/15/2021' and '05/14/2022'  
		and 
			case  -- exclude holidays except memorial day and 4th july
				when month_name not in ('may','july') then not holiday
				else true
			end) a   
cross join (
	select distinct price_band_desc
	from cra.temp1 
	where price_band_desc is not null
	order by price_band_desc) b
cross join (
	select distinct vehicle_type as shape, segment as size
	from cra.temp1
	where vehicle_type is not null
		and segment is not null) c
where a.the_date between '05/15/2021' and '05/14/2022'
order by a.the_date, c.shape, c.size, b.price_band_desc;

drop table if exists cra.base_2_2k_pb cascade;
create table cra.base_2_2k_pb (
  the_date date not null,
  shape citext not null,
  size citext not null,
  price_bands_2k citext not null,
  sales integer not null,
  primary key(the_date,shape,size,price_bands_2k));
create index on cra.base_2_2k_pb(shape);
create index on cra.base_2_2k_pb(size);
create index on cra.base_2_2k_pb(price_bands_2k);

insert into cra.base_2_2k_pb
select a.*, coalesce(aa.sales, 0) as sales
from cra.base_1_2k_pb a
left join (
	select date_sold, price_band_desc, vehicle_type as shape, segment as size, count(*) as sales
	from cra.temp1
	where sales_status = 'sold'
	group by date_sold, price_band_desc, vehicle_type, segment) aa on a.the_date = aa.date_sold
		and a.price_bands_2k = aa.price_band_desc
		and a.shape = aa.shape
    and a.size = aa.size
order by a.the_date, a.shape, a.size, a.price_bands_2k;    


select shape, size, price_bands_2k, round(avg(prev_30), 1)
from (
	select b.*, 
		sum(sales) over (partition by b.shape, b.size, b.price_bands_2k order by b.the_date rows between 29 preceding and current row) as prev_30
	from cra.base_1_2k_pb a
	left join cra.base_2_2k_pb b on a.the_date = b.the_date
		and a.shape = b.shape
		and a.size = b.size
		and a.price_bands_2k = b.price_bands_2k) c
group by shape, size, price_bands_2k		
order by shape, size, price_bands_2k