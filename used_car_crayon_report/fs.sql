﻿drop table if exists cra.fs_detail cascade;
create table cra.fs_detail as
  select distinct b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201706 and 202205
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code = 'VSU' 
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month between 201706 and 202205
      and b.page = 16
      and b.line between 1 and 14
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y';

alter table cra.fs_detail
add primary key(year_month, control, amount, gl_account);

create index on cra.fs_detail(control);
create index on cra.fs_detail(year_month);

-- 132 missing vehilces
select a.*
from cra.fs_detail a
left join cra.five_years_2 b on a.control = b.stock_number
where a.line between 1 and 6
  and b.stock_number is null
  and right(a.control, 1) <> 'L'
  and a.year_month < 202205

-- tool missing 127 retail sales 
drop table if exists missing;
create temp table missing as
select a.*
from (
	select year_month, control, sum(unit_count)
	from cra.fs_detail
	where line between 1 and 6
	group by year_month, control 
	having sum(unit_count) > 0) a
left join cra.five_years b on a.control = b.stock_number
where b.stock_number is null
  and right(a.control, 1) <> 'L'
  and a.year_month < 202205
order by year_month;  

/*
06/09/22
turns out that using xmf_inpmast for this gives me a LOT of noise
*/
drop table if exists cra.inpmast cascade;
create table cra.inpmast as
select inpmast_stock_number as stock_number, inpmast_vin as vin
from arkona.xfm_inpmast
where inpmast_stock_number is not null
group by inpmast_stock_number, inpmast_vin;

alter table cra.inpmast
add primary key(stock_number, vin);
create index on cra.inpmast(vin);

-- join missing to inpmast, then by vin to five_years, maybe bad stocknumber in five_years
select a.* , b.vin, c.stock_number
from missing a
left join cra.inpmast b on a.control = b.stock_number
left join cra.five_years c on b.vin = c.vin




-- 06/08 perhapes unioning these 2 tables i can get a rational representation
-- 06/16/22 this did not seem to go anywhere
select * from (
select 'sale'::citext, b.stocknumber, c.vin, a.vehicleinventoryitemid, a.soldts as acq_sold_ts, split_part(a.typ,'_',2), coalesce(d.fullname, a.soldto) as acq_from_sold_to
-- select a.*
from ads.ext_vehicle_sales a
join cra.five_years_2 aa on a.vehicleinventoryitemid = aa.vehicle_inventory_item_id
join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
--   and right(b.stocknumber, 1) in ('G','L')
join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
--   and exists (
--     select 1 
--     from ads.ext_vehicle_inventory_items
--     where vehicleinventoryitemid = a.vehicleinventoryitemid
--       and vehicleitemid = c.vehicleitemid
--       and stocknumber in ('G','L'))
left join ads.ext_organizations d on a.soldto = d.partyid
where a.status <> 'VehicleSale_SaleCanceled'
limit 100) A
union
select * from (
select 'acquisition'::citext, b.stocknumber, c.vin, a.vehicleinventoryitemid, b.fromts, split_part(a.typ,'_',2), coalesce(d.fullname, a.acquiredfrompartyid)
from ads.ext_vehicle_acquisitions a
join cra.five_years_2 aa on a.vehicleinventoryitemid = aa.vehicle_inventory_item_id
join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
--   and right(b.stocknumber, 1) in ('G','L')
join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
--   and exists (
--     select 1 
--     from ads.ext_vehicle_inventory_items
--     where vehicleinventoryitemid = a.vehicleinventoryitemid
--       and vehicleitemid = c.vehicleitemid
--       and stocknumber in ('G','L'))
left join ads.ext_organizations d on a.acquiredfrompartyid = d.partyid
limit 100) b
order by vin, acq_sold_ts

select distinct status from ads.ext_Vehicle_sales where vehicleinventoryitemid = '000166e3-0f75-4f34-9d1a-f7cc103a4e5a'
select distinct split_part(a.typ,'_',2) from ads.ext_vehicle_sales a

select * from ads.ext_vehicle_acquisitions where vehicleinventoryitemid = '00080f85-2b14-478a-839d-6b24d4f441d2'


select 'sale'::citext, b.stocknumber, c.vin, a.vehicleinventoryitemid, a.soldts as acq_sold_ts, split_part(a.typ,'_',2), coalesce(d.fullname, a.soldto) as acq_from_sold_to
-- select a.*
from ads.ext_vehicle_sales a
join cra.five_years_2 aa on a.vehicleinventoryitemid = aa.vehicle_inventory_item_id
join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and right(b.stocknumber, 1) in ('G','L')
join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
left join ads.ext_organizations d on a.soldto = d.partyid
where a.status <> 'VehicleSale_SaleCanceled'
  and a.soldts::date > '01/01/2022'
--   and exists (
--     select 1 
--     from ads.ext_vehicle_inventory_items
--     where vehicleinventoryitemid = a.vehicleinventoryitemid
--       and vehicleitemid = c.vehicleitemid
--       and stocknumber in ('G','L'))
limit 100


-- 06/16/22 start over
-- lets try a specific vehicle, then generalize from there, step at a time
G44126G,5GAKVBKD6HJ133102 sold retail on 4/5/22  ?? the only acquisition is a trade?
G44233G,3CZRU6H37LM720937,1ead4045-255c-47d4-9de2-dd00f0b9fa77 ok, but orig stocknumber is H15155L
H15296G,5FNYF6H51JB045259,8d4f4eb9-b567-420a-9891-3ae82db0553e

select a.vehicleinventoryitemid, a.soldts::date, a.typ, a.soldto, b.stocknumber, c.vin 
from ads.ext_vehicle_sales a
join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
where a.status <> 'VehicleSale_SaleCanceled'
  and a.vehicleinventoryitemid = '8d4f4eb9-b567-420a-9891-3ae82db0553e'

-- find all instances of vin in sales * acqs
-- sales, why no intramarket sale?
select a.vehicleinventoryitemid, a.soldts::date, a.typ, a.soldto, b.stocknumber, c.vin 
from ads.ext_vehicle_sales a
join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
  and c.vin = '5FNYF6H51JB045259'
where a.status <> 'VehicleSale_SaleCanceled'

select a.*, b.stocknumber 
from ads.ext_vehicle_acquisitions a
join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
  and c.vin = '5FNYF6H51JB045259'

-- so it is looking like, start with acquisition with typ VehicleAcquisition_IntraMarketPurchase
-- so, this gives me the vins of the G stocknumbers, although prior to 2018 a lot of them were X stocknumbers
select a.vehicleinventoryitemid, b.stocknumber, c.vin, b.fromts::Date, b.thruts::date 
from ads.ext_vehicle_acquisitions a
join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
where a.typ = 'VehicleAcquisition_IntraMarketPurchase'








-- show stock number types
select year_month, count(*), array_agg(right(control, 1) order by right(control, 1))
from (
	select a.*
	from (
		select year_month, control, sum(unit_count)
		from cra.fs_detail
		where line between 1 and 6
		group by year_month, control 
		having sum(unit_count) > 0) a
	left join cra.five_years_1 b on a.control = b.stock_number
	where b.stock_number is null
		and right(a.control, 1) <> 'L'
		and a.year_month < 202205) b
group by  year_month		 
order by year_month
  



