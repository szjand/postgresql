
DECLARE @Market String;
DECLARE @MarketID String;
DECLARE @AsOfDate Date;
DECLARE @FromDate Date;
DECLARE @TempTableName String;
DECLARE @LoadTableSQL String;
DECLARE @T1 Timestamp;
DECLARE @T2 Timestamp;
DECLARE @T3 Timestamp;
DECLARE @T4 Timestamp;
@Market = 'GF-Grand Forks';
@AsOfDate = curdate();
@FromDate = CAST(TimeStampAdd(SQL_TSI_YEAR, -1 ,@AsOfDate) AS SQL_DATE);

//@Market = 'GF-Grand Forks';
//@AsOfDate = '2011-04-20';
@TempTableName = (SELECT '#' + NewIDString() FROM system.iota);
@MarketID = (SELECT PartyID FROM Organizations WHERE FullName = @Market);


SELECT Utilities.DropTablesIfExist('#totalavailablehoursX') FROM system.iota;
SELECT Utilities.DropTablesIfExist('#salesbufferrecordsXxX') FROM system.iota;
SELECT Utilities.DropTablesIfExist('#totalsbadjustmentX') FROM system.iota;
SELECT Utilities.DropTablesIfExist('#availablehoursadjustedX') FROM system.iota;
SELECT Utilities.DropTablesIfExist('#temp') FROM system.iota;
SELECT Utilities.DropTablesIfExist('#temp1') FROM system.iota;
SELECT Utilities.DropTablesIfExist(' #statusesX') FROM system.iota;


@T1 = (SELECT Current_Timestamp() FROM system.iota);
//Select all sales buffer statuses for vehicles of interest
select viis.VehicleInventoryItemID,
  case when viis.FromTS is null then CAST(@AsOfDate as SQL_TIMESTAMP) else viis.FromTS end as FromTS,
  case when viis.ThruTS is null then CAST(@AsOfDate as SQL_TIMESTAMP) else viis.ThruTS end as ThruTS 
into  #salesbufferrecordsXxX
from vehicleinventoryitemStatuses viis
inner join vehicleinventoryitems vii on vii.vehicleinventoryitemid = viis.vehicleinventoryitemid
where viis.Status = 'RMFlagSB_SalesBuffer'
  and vii.LocationID IN (SELECT PartyID2
                           FROM PartyRelationships 
                           WHERE PartyID1 =  @MarketID 
                             AND Typ = 'PartyRelationship_MarketLocations')  
  AND CASE
        WHEN vii.ThruTS is null then TRUE    //in inventory
        ELSE
          CASE WHEN CAST(vii.ThruTS AS SQL_DATE) > @FromDate
                 AND CAST(vii.ThruTS AS SQL_DATE) <= @AsOfDate THEN TRUE  //Vehicle sold within last year
          ELSE FALSE END
        END;
		
				
				
//Calculate the hours available for each vehicle for each time in RMFlagAV_Available status
select viis.VehicleInventoryItemID, 
  case
    when viis.ThruTS is null then TimeStampDiff(SQL_TSI_HOUR, viis.FromTS, @AsOfDate)
	else TimeStampDiff(SQL_TSI_HOUR, viis.FromTS, viis.ThruTS)
	end as Hours,
  case when viis.FromTS is null then CAST(@AsOfDate as SQL_TIMESTAMP) else viis.FromTS end as FromTS,
  case when viis.ThruTS is null then CAST(@AsOfDate as SQL_TIMESTAMP) else viis.ThruTS end as ThruTS 
into #totalavailablehoursX
from vehicleinventoryitemStatuses viis
inner join vehicleinventoryitems vii on vii.vehicleinventoryitemid = viis.vehicleinventoryitemid
where viis.Status = 'RMFlagAV_Available'
and vii.LocationID IN (SELECT PartyID2
                                   FROM PartyRelationships 
                                  WHERE PartyID1 =  @MarketID 
                                    AND Typ = 'PartyRelationship_MarketLocations')  
  AND CASE
        WHEN vii.ThruTS is null then TRUE    //in inventory
        ELSE
          CASE WHEN CAST(vii.ThruTS AS SQL_DATE) > @FromDate
                 AND CAST(vii.ThruTS AS SQL_DATE) <= @AsOfDate THEN TRUE  //Vehicle sold within last year
          ELSE FALSE END
        END;






//Calculate total hours available for each vehicle, insert into #availablehoursadjustedX (adjustment comes later)
select VehicleinventoryItemID, sum(hours) as AvailableHours
  into #availablehoursadjustedX
  FROM #totalavailablehoursX
  where hours is not null
  group by 1;

//Calculate adjustments for SalesBuffer hours for vehicles that are available
select s.VehicleInventoryItemID, sum(s.Adjustment) as Adjustment
  into #totalsbadjustmentX
  from
    (select
       t1.vehicleinventoryitemid, //t1.FromTS, t1.thruTS, t2.FromTS, t2.ThruTS,
       case
         when ( //inner sb
           t1.FromTS <= t2.FromTS
           and 
	       t1.ThruTS >= t2.FromTS
	       and not t2.ThruTS > t1.ThruTS
		  ) then TimeStampDiff(SQL_TSI_HOUR, t2.FromTS, t2.ThruTS)
         when  ( //overlap from before
	       t2.FromTS <= t1.FromTS
           and
	       t2.ThruTS >= t1.FromTS
		   and not t2.ThruTS > t1.ThruTS
		   ) then TimeStampDiff(SQL_TSI_HOUR, t1.FromTS, t2.ThruTS)
         when  (//overlap after
           t1.FromTS <= t2.FromTS
           and 
	       t1.ThruTS >= t2.FromTS
	      ) then TimeStampDiff(SQL_TSI_HOUR, t2.FromTS, t2.ThruTS) - TimeStampDiff(SQL_TSI_HOUR, t1.ThruTS, t2.ThruTS)
         else //overlap all
	       TimestampDiff(SQL_TSI_HOUR, t1.FromTS, t1.ThruTS) 
       end as Adjustment
       from #totalavailablehoursX t1
       inner join #salesbufferrecordsXxX t2 
       on t1.vehicleInventoryItemID = t2.VehicleInventoryItemID  
       where (
              t1.FromTS <= t2.FromTS
              and 
	          t1.ThruTS >= t2.FromTS
	          )
         or   (t2.FromTS <= t1.FromTS
         and
	       t2.ThruTS > t1.FromTS
		  )
    ) s		
  group by 1;

//Update the #availablehoursadjustedX  
Update t11
 set [AvailableHours] = [AvailableHours] - t22.Adjustment
from #availablehoursadjustedX t11, #totalsbadjustmentX t22
where t11.VehicleInventoryItemID = t22.VehicleInventoryItemID;



//Create core statuses table. Contains SoldDate and flags for statuses
//Joins current inventory and sold vehicle for time period
select vii.VehicleInventoryItemID,
 (Select 'X' from VehicleInventoryItemStatuses v
    where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
      AND  v.Status = 'RMFlagPIT_PurchaseInTransit' 
      AND  CAST(v.FromTS AS SQL_DATE) <= @AsOfDate 
      AND (CAST(v.ThruTS AS SQL_DATE) > @AsOfDate 
           OR 
           v.ThruTS IS NULL
           )
  ) as PurchaseInTransit,
 (Select 'X' from VehicleInventoryItemStatuses v
    where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
      AND  v.Status = 'RMFlagTNA_TradeNotAvailable' 
      AND  CAST(v.FromTS AS SQL_DATE) <= @AsOfDate 
      AND (CAST(v.ThruTS AS SQL_DATE) > @AsOfDate 
           OR 
           v.ThruTS IS NULL
           )
  ) as TradeNotAvailable,
 (Select 'X' from VehicleInventoryItemStatuses v
    where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
      AND  v.Status = 'RMFlagRMP_RawMaterialsPool' 
      AND  CAST(v.FromTS AS SQL_DATE) <= @AsOfDate 
      AND (CAST(v.ThruTS AS SQL_DATE) > @AsOfDate 
           OR 
           v.ThruTS IS NULL
           )
  ) as RawMaterialsPool,
 (Select 'X' from VehicleInventoryItemStatuses v
    where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
      AND  v.Status = 'RMFlagPulled_Pulled' 
      AND  CAST(v.FromTS AS SQL_DATE) <= @AsOfDate 
      AND (CAST(v.ThruTS AS SQL_DATE) > @AsOfDate 
           OR 
           v.ThruTS IS NULL
           )
  ) as Pulled,
 (Select 'X' from VehicleInventoryItemStatuses v
    where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
      AND  v.Status = 'AppearanceReconProcess_InProcess' 
      AND  CAST(v.FromTS AS SQL_DATE) <= @AsOfDate 
      AND (CAST(v.ThruTS AS SQL_DATE) > @AsOfDate 
           OR 
           v.ThruTS IS NULL
           )
  ) as AppearanceWIP,
 (Select 'X' from VehicleInventoryItemStatuses v
    where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
      AND  v.Status = 'MechanicalReconProcess_InProcess' 
      AND  CAST(v.FromTS AS SQL_DATE) <= @AsOfDate 
      AND (CAST(v.ThruTS AS SQL_DATE) > @AsOfDate 
           OR 
           v.ThruTS IS NULL
           )
  ) as MechanicalWIP,
 (Select 'X' from VehicleInventoryItemStatuses v
    where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
      AND  v.Status = 'BodyReconProcess_InProcess' 
      AND  CAST(v.FromTS AS SQL_DATE) <= @AsOfDate 
      AND (CAST(v.ThruTS AS SQL_DATE) > @AsOfDate 
           OR 
           v.ThruTS IS NULL
           )
  ) as BodyWIP,
 (Select 'X' from VehicleInventoryItemStatuses v
    where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
      AND  v.Status = 'RMFlagPB_PricingBuffer' 
      AND  CAST(v.FromTS AS SQL_DATE) <= @AsOfDate 
      AND (CAST(v.ThruTS AS SQL_DATE) > @AsOfDate 
           OR 
           v.ThruTS IS NULL
           )
  ) as PricingBuffer,
 (Select 'X' from VehicleInventoryItemStatuses v
    where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
      AND  v.Status = 'RMFlagAV_Available' 
      AND  CAST(v.FromTS AS SQL_DATE) <= @AsOfDate 
      AND (CAST(v.ThruTS AS SQL_DATE) > @AsOfDate 
           OR 
           v.ThruTS IS NULL
           )
  ) as AvailableNow,
 (Select 'X' from VehicleInventoryItemStatuses v
    where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
      AND  v.Status = 'RMFlagSB_SalesBuffer' 
      AND  CAST(v.FromTS AS SQL_DATE) <= @AsOfDate 
      AND (CAST(v.ThruTS AS SQL_DATE) > @AsOfDate 
           OR 
           v.ThruTS IS NULL
           )
  ) as SalesBuffer,
 (Select 'X' from VehicleInventoryItemStatuses v
    where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
      AND  v.Status = 'RMFlagWSB_WholesaleBuffer' 
      AND  CAST(v.FromTS AS SQL_DATE) <= @AsOfDate 
      AND (CAST(v.ThruTS AS SQL_DATE) > @AsOfDate 
           OR 
           v.ThruTS IS NULL
           )
  ) as WholesaleBuffer,
 (Select 'X' from VehicleInventoryItemStatuses v
    where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
      AND  v.Status = 'RawMaterials_Sold' 
      AND  CAST(v.FromTS AS SQL_DATE) <= @AsOfDate 
      AND (CAST(v.ThruTS AS SQL_DATE) > @AsOfDate 
           OR 
           v.ThruTS IS NULL
           )
  ) as SoldNotDelivered,    
  CAST(null as SQL_CHAR(1)) as SoldRetail,
  CAST(null as SQL_CHAR(1)) as SoldWholesale,
  CAST(null AS SQL_DATE) as SoldDate,
  CAST(null as SQL_CHAR(38)) as SoldBy,
  CAST(null as SQL_INTEGER) as DaysAvailable,
  CAST(null AS SQL_CHAR(30)) as Status  
into #statusesX  
from VehicleInventoryItems vii
where vii.LocationID IN (SELECT PartyID2
                            FROM PartyRelationships 
                            WHERE PartyID1 =  @MarketID 
                              AND Typ = 'PartyRelationship_MarketLocations')  
  AND CAST(vii.FromTS AS SQL_DATE) <= @AsOfDate 
  AND vii.ThruTS IS NULL

UNION

SELECT vii.VehicleInventoryItemID,
 CAST(null as SQL_CHAR(1)) as PurchaseInTransit,
 CAST(null as SQL_CHAR(1)) as TradeNotAvailable,
 CAST(null as SQL_CHAR(1)) as RawMaterialsPool,
 CAST(null as SQL_CHAR(1)) as Pulled,
 CAST(null as SQL_CHAR(1)) as AppearanceWIP,
 CAST(null as SQL_CHAR(1)) as MechanicalWIP,
 CAST(null as SQL_CHAR(1)) as BodyWIP,
 CAST(null as SQL_CHAR(1)) as PricingBuffer,
 CAST(null as SQL_CHAR(1)) as AvailableNow,
 CAST(null as SQL_CHAR(1)) as SalesBuffer,
 CAST(null as SQL_CHAR(1)) as WholesaleBuffer,
 CAST(null as SQL_CHAR(1)) as SoldNotDelivered,     
 (Select 'X' from VehicleSales v
    where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
      AND  v.Typ = 'VehicleSale_Retail' 
      AND CAST(v.SoldTS AS SQL_DATE) <= @AsOfDate
      AND v.Status = 'VehicleSale_Sold'
  ) as SoldRetail,
 (Select 'X' from VehicleSales v
    where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
      AND  v.Typ = 'VehicleSale_Wholesale' 
      AND CAST(v.SoldTS AS SQL_DATE) <= @AsOfDate
      AND v.Status = 'VehicleSale_Sold'
  ) as SoldWholesale,
 (Select CAST(v.SoldTS as SQL_DATE) from VehicleSales v
    where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
      AND CAST(v.SoldTS AS SQL_DATE) <= @AsOfDate
      AND v.Status = 'VehicleSale_Sold'
  ) as SoldDate,
 (Select SoldBy from VehicleSales v
    where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
      AND CAST(v.SoldTS AS SQL_DATE) <= @AsOfDate
      AND v.Status = 'VehicleSale_Sold'
  ) as SoldBy,  
  CAST(null as SQL_INTEGER) as DaysAvailable,
  CAST(null AS SQL_CHAR(30)) as Status
FROM VehicleInventoryItems vii
WHERE vii.LocationID IN (SELECT PartyID2
                            FROM PartyRelationships 
                            WHERE PartyID1 =  @MarketID 
                              AND Typ = 'PartyRelationship_MarketLocations')  
  AND CAST(vii.FromTS AS SQL_DATE) <= @AsOfDate   
  AND CAST(vii.ThruTS AS SQL_DATE) > @FromDate
  AND CAST(vii.ThruTS AS SQL_DATE) <= @AsOfDate;  
  		
//select * from #statusesX where vehicleinventoryitemid = '6b2b7e3a-f9f4-4a5f-af20-6cf3e550f4d2'

UPDATE s
  SET [DaysAvailable] = ROUND( d.AvailableHours / 24, 0)
FROM #statusesX s, #availablehoursadjustedX d
WHERE s.VehicleInventoryItemID = d.VehicleInventoryItemID;

//do some cleanup
-- drop table #totalavailablehoursX;drop table #salesbufferrecordsXxX; drop table #totalsbadjustmentX;drop table #availablehoursadjustedX;

//Calculate the current status value
UPDATE s
  SET [Status] = CASE
                  WHEN s.SoldRetail IS NOT NULL THEN 'Sold'
                  WHEN s.SoldNotDelivered is not null then 'Sold'
                  WHEN s.SoldWholesale IS NOT NULL THEN 'Wholesaled'
                  WHEN s.SalesBuffer IS NOT NULL THEN 'Sales Buffer'
                  WHEN s.WholesaleBuffer IS NOT NULL THEN 'Wholesale Buffer'
                  //ok, we have handled all of the sold, wholesales, sales buffer, AND wholesale buffer vehicles.
                  //We DO NOT have to consider these flags any more
                  WHEN s.AvailableNow IS NOT NULL THEN 'Available'
                  WHEN (s.PurchaseInTransit IS NOT NULL) 
                    OR (s.TradeNotAvailable IS NOT NULL) THEN 'In Transit/Trade Buffer'
                  WHEN s.PricingBuffer IS NOT NULL THEN 'Pricing Buffer'
                  WHEN s.Pulled IS NOT NULL THEN
                    CASE 
                    WHEN s.BodyWIP IS NOT NULL
                      OR s.MechanicalWIP IS NOT NULL
                      OR s.AppearanceWIP IS NOT NULL THEN 'Recon WIP'
                    ELSE 'Recon Buffer'
                    END  
                  ELSE 'Raw Materials'
                  END
FROM #statusesX s;                  

//Preliminaries done. 

SELECT 
  vii.VehicleInventoryItemID, 
  CAST(@Market AS SQL_CHAR(100)) AS Market, 
--  (SELECT Trim(FullName) from Organizations where PartyID = vii.OwningLocationID) AS Location,
-- *a*
  (SELECT Trim(FullName) from Organizations where PartyID = vii.LocationID) AS Location,  
  Trim(vii.StockNumber) AS StockNumber, 
  Trim(vi.VIN) AS VIN, 
  Trim(vi.YearModel) AS Year, 
  Trim(vi.Make) AS Make, 
  Trim(vi.Model) AS Model, 
  Trim(vi.Trim) AS TrimLevel, 
  Trim(vi.ExteriorColor) AS Color, 
  Trim(vi.InteriorColor) AS InteriorColor,
  Trim(vi.BodyStyle) AS BodyStyle, 
  CASE
    WHEN s.SoldDate IS not NULL THEN	
      (SELECT Top 1 Value 
         FROM VehicleItemMileages vim 
         WHERE vim.VehicleItemID = vii.VehicleItemID
		   AND CAST(vim.VehicleItemMileageTS AS SQL_DATE) <= s.SoldDate
         ORDER BY vim.VehicleItemMileageTS DESC)
    ELSE
      (SELECT Top 1 Value 
         FROM VehicleItemMileages vim 
         WHERE vim.VehicleItemID = vii.VehicleItemID
         ORDER BY vim.VehicleItemMileageTS DESC)
  END AS Mileage,

  Trim(vi.Engine) AS Engine, 
  Trim(vi.Transmission) AS Transmission,
  Trim(vio.VehicleOptions) AS Options, 
  Trim(vio.AdditionalEquipment) as OptionsExploded,

  CAST(vii.FromTS AS SQL_DATE) as DateAcquired,
  case
    when s.DaysAvailable is null then 0
    else s.DaysAvailable
  end as DaysToSell,
  case
    when s.DaysAvailable is null then 0
    else s.DaysAvailable
  end as DaysOnLot,
  case
    when s.SoldDate is null then @AsOfDate - CAST(vii.FromTS AS SQL_DATE)
    else s.SoldDate - CAST(vii.FromTS as SQL_DATE)
  end as DaysOwned,
  s.SoldDate as DateSold,

  (SELECT TOP 1 vpd.Amount
    FROM VehiclePricings vp
    inner join VehiclePricingDetails vpd 
	on vpd.VehiclePricingID = vp.VehiclePricingID and vpd.Typ = 'VehiclePricingDetail_Invoice' 
    WHERE vp.VehicleInventoryItemID = vii.VehicleInventoryItemID
    ORDER BY VehiclePricingTS DESC) as MCRV,	 

  (SELECT TOP 1 vpd.Amount
    FROM VehiclePricings vp
    inner join VehiclePricingDetails vpd 
	on vpd.VehiclePricingID = vp.VehiclePricingID and vpd.Typ = 'VehiclePricingDetail_BestPrice' 
    WHERE vp.VehicleInventoryItemID = vii.VehicleInventoryItemID
    ORDER BY VehiclePricingTS DESC) as Price,	   

  Coalesce(glt.TotalCost, 0) AS Net,	   
  Coalesce(glt.SoldAmount, 0) as SoldAmount,
//  Coalesce(xxx.SoldAmount, 0) as SoldAmount,
  Coalesce(glt.SoldAmount, 0) - Coalesce(glt.TotalCost, 0) as Gross,   	   	
  case
    when s.SoldRetail = 'X' then 'VehicleSale_Retail'
    when s.SoldWholesale = 'X' then 'VehicleSale_Wholesale'
    else null
  end as Disposition,    
  s.SoldBy as SoldBy,
  (SELECT p.FullName FROM People p 
     WHERE p.PartyID = ve.VehicleEvaluatorID 
	   AND p.ThruTS IS NULL) AS AppraisedBy, 

  s.Status as SalesStatus,
  (SELECT td.Description FROM TypDescriptions td 
     WHERE td.typ = ve.Typ AND td.ThruTS IS null) AS Source,
  (SELECT td.Description FROM TypDescriptions td 
     WHERE td.typ = mmc.VehicleType AND td.ThruTS IS null) AS VehicleType,
  (SELECT td.Description FROM TypDescriptions td 
     WHERE td.typ = mmc.VehicleSegment AND td.ThruTS IS null) AS Segment,   
  mmc.Luxury AS Luxury,
  mmc.Sport AS Sport,
  mmc.Commercial AS Commercial
  INTO #temp 
  FROM VehicleInventoryItems vii
  INNER JOIN VehicleItems vi ON vi.VehicleItemID = vii.VehicleItemID
  //this inner join restricts query to vehicles of interest
  INNER JOIN #statusesX s on vii.VehicleInventoryItemID = s.VehicleInventoryItemID
  inner JOIN MakeModelClassifications mmc ON  mmc.Make = vi.Make AND mmc.Model = vi.Model	    	      
  LEFT OUTER JOIN GrossLogTable glt ON vii.StockNumber = glt.StockNumber 
  LEFT outer JOIN VehicleItemOptions vio ON vio.VehicleItemID = vii.VehicleItemID
  LEFT OUTER JOIN VehicleEvaluations ve ON ve.VehicleInventoryItemID = vii.VehicleInventoryItemID;
// added VehicleSales to get SoldAmount, GrossLogTable IS no longer being generated
//  LEFT JOIN VehicleSales xxx ON vii.VehicleInventoryItemID = xxx.VehicleInventoryItemID;    


@T2 = (SELECT Current_Timestamp() FROM system.iota);	
   
SELECT
  vii.*,

  //without the top 1 the following generates more than one record 
 (SELECT top 1 gl.Description 
    FROM GlumpLimits gl 
    WHERE gl.ShortVehicleTyp = vii.VehicleType 
      AND (vii.Price >= gl.LowerLimit 
      AND vii.Price <= gl.UpperLimit)) AS Glump,  

  case
    when vii.DateSold IS NULL then NULL
    WHEN vii.DaysOnLot IS NULL THEN null
    when TRUNCATE(vii.DaysOnLot / 7.01, 0) > 8 then 8
    else TRUNCATE(vii.DaysOnLot / 7.01, 0)
/*    
    when (vii.DateSold - vii.DateOnLot) >=  8 and (vii.DateSold - vii.DateOnLot) <= 14 then 1
    when (vii.DateSold - vii.DateOnLot) >= 15 and (vii.DateSold - vii.DateOnLot) <= 21 then 2
    when (vii.DateSold - vii.DateOnLot) >= 22 and (vii.DateSold - vii.DateOnLot) <= 28 then 3
    when (vii.DateSold - vii.DateOnLot) >= 29 and (vii.DateSold - vii.DateOnLot) <= 35 then 4
    when (vii.DateSold - vii.DateOnLot) >= 36 and (vii.DateSold - vii.DateOnLot) <= 42 then 5
    when (vii.DateSold - vii.DateOnLot) >= 43 and (vii.DateSold - vii.DateOnLot) <= 49 then 6
    when (vii.DateSold - vii.DateOnLot) >= 50 and (vii.DateSold - vii.DateOnLot) <= 56 then 7
    when (vii.DateSold - vii.DateOnLot) > 56 then 8 */
  end as "SellingWeek",    
  case
    when (vii.Mileage = 0) or (Length(Trim(vii.Year)) <> 4) or (Locate('.', vii.Year) <> 0) or (Locate('C', vii.Year) <> 0) or (Locate('/', vii.Year) <> 0) or ((vii.Year < '1901') OR (vii.Year > '2050')) then Null
    when vii.Mileage / TimeStampDiff(SQL_TSI_MONTH, Convert(Trim(Convert(Convert(vii.Year, SQL_INTEGER) - 1, SQL_CHAR)) + '-07-31', SQL_DATE), @AsOfDate) * 12 <= 4000 then 'Freaky Low'
    when vii.Mileage / TimeStampDiff(SQL_TSI_MONTH, Convert(Trim(Convert(Convert(vii.Year, SQL_INTEGER) - 1, SQL_CHAR)) + '-07-31', SQL_DATE), @AsOfDate) * 12 <= 8000 then 'Very Low'
    when vii.Mileage / TimeStampDiff(SQL_TSI_MONTH, Convert(Trim(Convert(Convert(vii.Year, SQL_INTEGER) - 1, SQL_CHAR)) + '-07-31', SQL_DATE), @AsOfDate) * 12 <= 12000 then 'Low'
    when vii.Mileage / TimeStampDiff(SQL_TSI_MONTH, Convert(Trim(Convert(Convert(vii.Year, SQL_INTEGER) - 1, SQL_CHAR)) + '-07-31', SQL_DATE), @AsOfDate) * 12 <= 15000 then 'Average'
    when vii.Mileage / TimeStampDiff(SQL_TSI_MONTH, Convert(Trim(Convert(Convert(vii.Year, SQL_INTEGER) - 1, SQL_CHAR)) + '-07-31', SQL_DATE), @AsOfDate) * 12 <= 18000 then 'High'
    when vii.Mileage / TimeStampDiff(SQL_TSI_MONTH, Convert(Trim(Convert(Convert(vii.Year, SQL_INTEGER) - 1, SQL_CHAR)) + '-07-31', SQL_DATE), @AsOfDate) * 12 <= 22000 then 'Very High'
    when vii.Mileage / TimeStampDiff(SQL_TSI_MONTH, Convert(Trim(Convert(Convert(vii.Year, SQL_INTEGER) - 1, SQL_CHAR)) + '-07-31', SQL_DATE), @AsOfDate) * 12 <= 999999 then 'Freaky High'
    else
      'Unknown'
  end as "MileageCategory",

  case
    when DateSold = '1900-01-01' then null
    when vii.Disposition = 'VehicleSale_Retail' and vii.DaysOnLot BETWEEN  0 and 30 then 'Fresh'
    when vii.Disposition = 'VehicleSale_Retail' and vii.DaysOnLot > 30 then 'Aged'
    when vii.Disposition = 'VehicleSale_Wholesale' and vii.DaysOnLot is null then 'Pure W/S'
    when vii.Disposition = 'VehicleSale_Wholesale' and vii.DaysOnLot is not null then 'Aged W/S'
      else null
  end as "SalesCategory",
  case
    when vii.Price = 0 then NULL
    WHEN vii.Price IS NULL THEN NULL
    when vii.Price >     0 and vii.Price <=  6000 then  0
    when vii.Price >  6000 and vii.Price <=  8000 then  1
    when vii.Price >  8000 and vii.Price <= 10000 then  2
    when vii.Price > 10000 and vii.Price <= 12000 then  3
    when vii.Price > 12000 and vii.Price <= 14000 then  4
    when vii.Price > 14000 and vii.Price <= 16000 then  5
    when vii.Price > 16000 and vii.Price <= 18000 then  6
    when vii.Price > 18000 and vii.Price <= 20000 then  7
    when vii.Price > 20000 and vii.Price <= 22000 then 8
    when vii.Price > 22000 and vii.Price <= 24000 then 9
    when vii.Price > 24000 and vii.Price <= 26000 then 10
    when vii.Price > 26000 and vii.Price <= 28000 then 11
    when vii.Price > 28000 and vii.Price <= 30000 then 12
    when vii.Price > 30000 and vii.Price <= 32000 then 13
    when vii.Price > 32000 and vii.Price <= 34000 then 14
    when vii.Price > 34000 and vii.Price <= 36000 then 15
    when vii.Price > 36000 and vii.Price <= 38000 then 16
    when vii.Price > 38000 and vii.Price <= 40000 then 17
    else
      18
  end as "PriceBand",
  case
    when vii.Price = 0 then NULL
    WHEN vii.Price IS NULL THEN NULL
    when vii.Price >     0 and vii.Price <=  6000 then '0-6k'
    when vii.Price >  6000 and vii.Price <=  8000 then '6-8k'
    when vii.Price >  8000 and vii.Price <= 10000 then '8-10k'
    when vii.Price > 10000 and vii.Price <= 12000 then '10-12k'
    when vii.Price > 12000 and vii.Price <= 14000 then '12-14k'
    when vii.Price > 14000 and vii.Price <= 16000 then '14-16k'
    when vii.Price > 16000 and vii.Price <= 18000 then '16-18k'
    when vii.Price > 18000 and vii.Price <= 20000 then '18-20k'
    when vii.Price > 20000 and vii.Price <= 22000 then '20-22k'
    when vii.Price > 22000 and vii.Price <= 24000 then '22-24k'
    when vii.Price > 24000 and vii.Price <= 26000 then '24-26k'
    when vii.Price > 26000 and vii.Price <= 28000 then '26-28k'
    when vii.Price > 28000 and vii.Price <= 30000 then '28-30k'
    when vii.Price > 30000 and vii.Price <= 32000 then '20-32k'
    when vii.Price > 32000 and vii.Price <= 34000 then '32-24k'
    when vii.Price > 34000 and vii.Price <= 36000 then '34-36k'
    when vii.Price > 36000 and vii.Price <= 38000 then '36-38k'
    when vii.Price > 38000 and vii.Price <= 40000 then '38-40k'
    else
      '40k+'
  end as "PriceBandDesc"

  INTO #temp1
  FROM #temp vii;
	
//----------------------------------------------------------------------------//	
//----------------------------------------------------------------------------//	

SELECT vehicleinventoryitemid
FROM #temp1
GROUP BY vehicleinventoryitemid
HAVING COUNT(*) > 1

SELECT * FROM #statusesx

SELECT * FROM #temp1;	

SELECT COUNT(*) FROM #temp1  -- 4121


-- drop table #temp; drop table #temp1;drop table #statusesX;

-- drop table #totalavailablehoursX;drop table #salesbufferrecordsXxX; drop table #totalsbadjustmentX;drop table #availablehoursadjustedX;


SELECT stocknumber,vin, mileage,mileagecategory  FROM #temp1
WHERE stocknumber LIKE 'g444%' ORDER BY stocknumber

SELECT stocknumber,vin, mileage,mileagecategory  FROM #temp1 WHERE year = '2022'
SELECT COUNT(*) FROM #temp1

SELECT options, optionsexploded FROM #temp1 WHERE stocknumber = 'H13553X'


select COUNT(*) 
from #temp1
where datesold between curdate() - 31 and curdate()