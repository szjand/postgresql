﻿-- create schema cra;
-- comment on schema cra is 'a schema for all the fucking temp tables in the generation of the crayon report';

do $$
	declare
		_market citext := 'GF-Grand Forks';
		_market_id citext := (select partyid from ads.ext_organizations where fullname = _market);
		_as_of_date date := current_date;
		_from_date date := (select _as_of_date - interval '1 year');
		_temp_table_name citext := 'the_temp_table';
		_load_table_sql citext;
		_t1 timestamp := (select now());
		_t2 timestamp;
		_t3 timestamp;
		_t4 timestamp;

  begin
-- Select all sales buffer statuses for vehicles of interest  
    truncate cra.salesbufferrecordsXxX;
		insert into cra.salesbufferrecordsXxX 
		select viis.VehicleInventoryItemID,
			case when viis.FromTS > now() then _as_of_date::TIMESTAMP else viis.FromTS end as FromTS,
			case when viis.ThruTS > now() then _as_of_date::TIMESTAMP else viis.ThruTS end as ThruTS
-- into  #salesbufferrecordsXxX
		from ads.ext_vehicle_inventory_item_statuses viis
		inner join ads.ext_vehicle_inventory_items vii on vii.vehicleinventoryitemid = viis.vehicleinventoryitemid
		where viis.Status = 'RMFlagSB_SalesBuffer'
			and vii.LocationID IN (
				SELECT PartyID2
				FROM ads.ext_party_relationships
				WHERE PartyID1 =  _Market_ID 
				  AND Typ = 'PartyRelationship_MarketLocations') 
			AND 
				CASE
					WHEN vii.ThruTS > now() then TRUE   --in inventory
					ELSE
						CASE 
							WHEN vii.ThruTS::date > _From_Date AND vii.ThruTS::date <= _as_of_date THEN TRUE  --Vehicle sold within last year
							ELSE FALSE 
						END
					END;
-- Calculate the hours available for each vehicle for each time in RMFlagAV_Available status
    truncate cra.totalavailablehoursX;
    insert into cra.totalavailablehoursX
		select viis.VehicleInventoryItemID, 
			case
				when viis.ThruTS > now() then extract(epoch from  _as_of_date - viis.fromts)/3600
				else extract(epoch from viis.thruts - viis.fromts)/ 3600
			end as Hours,
			case when viis.FromTS > now() then _as_of_date::TIMESTAMP else viis.FromTS end as FromTS,
			case when viis.ThruTS > now() then _as_of_date::TIMESTAMP else viis.ThruTS end as ThruTS, stocknumber
-- 		into #totalavailablehoursX
		from ads.ext_vehicle_inventory_item_statuses viis
		inner join ads.ext_vehicle_inventory_items vii on vii.vehicleinventoryitemid = viis.vehicleinventoryitemid
		where viis.Status = 'RMFlagAV_Available'
			and vii.LocationID IN (
				SELECT PartyID2
				FROM ads.ext_party_relationships
				WHERE PartyID1 =  _Market_ID 
				  AND Typ = 'PartyRelationship_MarketLocations') 
			AND 
				CASE
					WHEN vii.ThruTS > now() then TRUE   --in inventory
					ELSE
						CASE 
							WHEN vii.ThruTS::date > _From_Date AND vii.ThruTS::date <= _as_of_date THEN TRUE  --Vehicle sold within last year
							ELSE FALSE 
						END
					END;			

--Calculate total hours available for each vehicle, insert into #availablehoursadjustedX (adjustment comes later)
    truncate cra.availablehoursadjustedX;
		insert into cra.availablehoursadjustedX 
		select VehicleinventoryItemID, sum(hours) as AvailableHours
		FROM cra.totalavailablehoursX
		where hours is not null
		group by 1;		

-- Calculate adjustments for SalesBuffer hours for vehicles that are available
    truncate cra.totalsbadjustmentX;
    insert into cra.totalsbadjustmentX 
		select s.VehicleInventoryItemID, sum(s.Adjustment) as Adjustment
		from (
			select
				t1.vehicleinventoryitemid, --t1.FromTS, t1.thruTS, t2.FromTS, t2.ThruTS,
				case
					when ( --inner sb
							t1.FromTS <= t2.FromTS
							and 
							t1.ThruTS >= t2.FromTS
							and not t2.ThruTS > t1.ThruTS) then extract(epoch from t2.ThruTS - t2.FromTS)/ 3600
					when  ( --overlap from before
						t2.FromTS <= t1.FromTS
						and
						t2.ThruTS >= t1.FromTS
					  and not t2.ThruTS > t1.ThruTS) then extract(epoch from t2.ThruTS - t1.FromTS)/ 3600 
					when  (--overlap after
						t1.FromTS <= t2.FromTS
						and 
						t1.ThruTS >= t2.FromTS) then extract(epoch from t2.ThruTS - t2.FromTS)/ 3600 - extract(epoch from  t2.ThruTS - t1.ThruTS)/ 3600 
					else --overlap all
						 extract(epoch from t1.ThruTS - t1.FromTS)/ 3600  
				end as Adjustment
			from cra.totalavailablehoursX t1
			join cra.salesbufferrecordsXxX t2 on t1.vehicleInventoryItemID = t2.VehicleInventoryItemID  
			where (
				t1.FromTS <= t2.FromTS
				and 
				t1.ThruTS >= t2.FromTS)
			or (
				t2.FromTS <= t1.FromTS
				and
				t2.ThruTS > t1.FromTS)) s		
			group by 1;				

-- Update the #availablehoursadjustedX  
		update cra.availablehoursadjustedX x
		set availablehours = x.availablehours - y.adjustment
		from (
			select VehicleInventoryItemID, adjustment
			from cra.totalsbadjustmentX) y
		where x.VehicleInventoryItemID = y.VehicleInventoryItemID;

-- Create core statuses table. Contains SoldDate and flags for statuses
-- Joins current inventory and sold vehicle for time period
    truncate cra.statusesX;
    insert into cra.statusesX
		select vii.VehicleInventoryItemID,
			(Select 'X'::text from ads.ext_Vehicle_Inventory_Item_Statuses v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Status = 'RMFlagPIT_PurchaseInTransit' 
					AND  v.FromTS::date <= _as_of_date
					AND (
						v.ThruTS::date > _as_of_date
						OR v.ThruTS > now())) as PurchaseInTransit,
						
		 (Select 'X'::text from ads.ext_Vehicle_Inventory_Item_Statuses v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Status = 'RMFlagTNA_TradeNotAvailable' 
					AND  v.FromTS::date <= _as_of_date
					AND (
						v.ThruTS::date > _as_of_date
						OR v.ThruTS > now()))  as TradeNotAvailable,
						
		 (Select 'X'::text from ads.ext_Vehicle_Inventory_Item_Statuses v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Status = 'RMFlagRMP_RawMaterialsPool' 
					AND  v.FromTS::date <= _as_of_date
					AND (
						v.ThruTS::date > _as_of_date
						OR v.ThruTS > now()))  as RawMaterialsPool,

		 (Select 'X'::text from ads.ext_Vehicle_Inventory_Item_Statuses v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Status = 'RMFlagPulled_Pulled' 
					AND  v.FromTS::date <= _as_of_date
					AND (
						v.ThruTS::date > _as_of_date
						OR v.ThruTS > now()))  as Pulled,

		 (Select 'X'::text from ads.ext_Vehicle_Inventory_Item_Statuses v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Status = 'AppearanceReconProcess_InProcess' 
					AND  v.FromTS::date <= _as_of_date
					AND (
						v.ThruTS::date > _as_of_date
						OR v.ThruTS > now()))  as AppearanceWIP,

		 (Select 'X'::text from ads.ext_Vehicle_Inventory_Item_Statuses v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Status = 'MechanicalReconProcess_InProcess' 
					AND  v.FromTS::date <= _as_of_date
					AND (
						v.ThruTS::date > _as_of_date
						OR v.ThruTS > now()))  as MechanicalWIP,

		 (Select 'X'::text from ads.ext_Vehicle_Inventory_Item_Statuses v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Status = 'BodyReconProcess_InProcess' 
					AND  v.FromTS::date <= _as_of_date
					AND (
						v.ThruTS::date > _as_of_date
						OR v.ThruTS > now()))  as BodyWIP,

		 (Select 'X'::text from ads.ext_Vehicle_Inventory_Item_Statuses v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Status = 'RMFlagPB_PricingBuffer' 
					AND  v.FromTS::date <= _as_of_date
					AND (
						v.ThruTS::date > _as_of_date
						OR v.ThruTS > now()))  as PricingBuffer,

		 (Select 'X'::text from ads.ext_Vehicle_Inventory_Item_Statuses v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Status = 'RMFlagAV_Available' 
					AND  v.FromTS::date <= _as_of_date
					AND (
						v.ThruTS::date > _as_of_date
						OR v.ThruTS > now()))  as AvailableNow,

		 (Select 'X'::text from ads.ext_Vehicle_Inventory_Item_Statuses v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Status = 'RMFlagSB_SalesBuffer' 
					AND  v.FromTS::date <= _as_of_date
					AND (
						v.ThruTS::date > _as_of_date
						OR v.ThruTS > now()))  as SalesBuffer,

		 (Select 'X'::text from ads.ext_Vehicle_Inventory_Item_Statuses v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Status = 'RMFlagWSB_WholesaleBuffer' 
					AND  v.FromTS::date <= _as_of_date
					AND (
						v.ThruTS::date > _as_of_date
						OR v.ThruTS > now()))  as WholesaleBuffer,

		 (Select 'X'::text from ads.ext_Vehicle_Inventory_Item_Statuses v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Status = 'RawMaterials_Sold' 
					AND  v.FromTS::date <= _as_of_date
					AND (
						v.ThruTS::date > _as_of_date
						OR v.ThruTS > now()))  as SoldNotDelivered,
		 
			null::text as SoldRetail,
			null::text as SoldWholesale,
			null::date as SoldDate,
			null::text as SoldBy,
			null::integer as DaysAvailable,
			null::text as Status  
		from ads.ext_vehicle_inventory_items vii
		where vii.LocationID IN (
			SELECT PartyID2
			FROM ads.ext_party_relationships
			WHERE PartyID1 =  _Market_ID 
				AND Typ = 'PartyRelationship_MarketLocations') 
			AND vii.FromTS::date <= _as_of_date
			AND vii.ThruTS > now()

		UNION

		SELECT vii.VehicleInventoryItemID,
		 null as PurchaseInTransit,
		 null as TradeNotAvailable,
		 null as RawMaterialsPool,
		 null as Pulled,
		 null as AppearanceWIP,
		 null as MechanicalWIP,
		 null as BodyWIP,
		 null as PricingBuffer,
		 null as AvailableNow,
		 null as SalesBuffer,
		 null as WholesaleBuffer,
		 null as SoldNotDelivered,     
		 (Select 'X'::text from ads.ext_vehicle_sales v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Typ = 'VehicleSale_Retail' 
					AND v.SoldTS::date <= _as_of_date
					AND v.Status = 'VehicleSale_Sold'
			) as SoldRetail,
		 (Select 'X'::text from ads.ext_vehicle_sales v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Typ = 'VehicleSale_Wholesale' 
					AND v.SoldTS::date <= _as_of_date
					AND v.Status = 'VehicleSale_Sold'
			) as SoldWholesale,
		 (Select v.SoldTS::date from ads.ext_vehicle_sales v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND v.SoldTS::date <= _as_of_date
					AND v.Status = 'VehicleSale_Sold'
			) as SoldDate,
		 (Select SoldBy from ads.ext_vehicle_sales v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND v.SoldTS::date <= _as_of_date
					AND v.Status = 'VehicleSale_Sold'
			) as SoldBy,  
			null::integer as DaysAvailable,
			null::text AS Status
		FROM ads.ext_vehicle_inventory_items vii
		WHERE vii.LocationID IN (
			SELECT PartyID2
			FROM ads.ext_party_relationships
			WHERE PartyID1 =  _Market_ID 
				AND Typ = 'PartyRelationship_MarketLocations') 
			AND vii.FromTS::date <= _as_of_date 
			AND vii.ThruTS::date >= _from_date
			and vii.ThruTS < now();  -- 05/15/2022 sold section only, this ensures that		

		update cra.statusesX x
		set daysavailable = y.days
		from (
			select VehicleInventoryItemID, (availablehours/24)::integer as days
			from cra.availablehoursadjustedX) y
		where x.VehicleInventoryItemID = y.VehicleInventoryItemID;

-- Calculate the current status value
	UPDATE cra.statusesX
	SET Status = 
		CASE
			WHEN SoldRetail IS NOT NULL THEN 'Sold'
			WHEN SoldNotDelivered is not null then 'Sold'
			WHEN SoldWholesale IS NOT NULL THEN 'Wholesaled'
			WHEN SalesBuffer IS NOT NULL THEN 'Sales Buffer'
			WHEN WholesaleBuffer IS NOT NULL THEN 'Wholesale Buffer'
			-- ok, we have handled all of the sold, wholesales, sales buffer, AND wholesale buffer vehicles.
			-- We DO NOT have to consider these flags any more
			WHEN AvailableNow IS NOT NULL THEN 'Available'
			WHEN (PurchaseInTransit IS NOT NULL) 
				OR (TradeNotAvailable IS NOT NULL) THEN 'In Transit/Trade Buffer'
			WHEN PricingBuffer IS NOT NULL THEN 'Pricing Buffer'
			WHEN Pulled IS NOT NULL THEN
				CASE 
					WHEN BodyWIP IS NOT NULL
						OR MechanicalWIP IS NOT NULL
						OR AppearanceWIP IS NOT NULL THEN 'Recon WIP'
					ELSE 'Recon Buffer'
				END  
			ELSE 'Raw Materials'
		END;                

-- Preliminaries done. 

		truncate cra.temp;
		insert into cra.temp 
		SELECT 
			vii.VehicleInventoryItemID, 
			_Market AS Market, 
			(SELECT Trim(FullName) from ads.ext_Organizations where PartyID = vii.LocationID) AS Location,  
			Trim(vii.StockNumber) AS StockNumber, 
			Trim(vi.VIN) AS VIN, 
			Trim(vi.YearModel) AS Year, 
			Trim(vi.Make) AS Make, 
			Trim(vi.Model) AS Model, 
			Trim(vi.Trim) AS TrimLevel, 
			Trim(vi.ExteriorColor) AS Color, 
			Trim(vi.InteriorColor) AS InteriorColor,
			Trim(vi.BodyStyle) AS BodyStyle, 
			CASE
				WHEN s.SoldDate IS not NULL THEN (
					SELECT Value 
					FROM ads.ext_Vehicle_Item_Mileages vim 
					WHERE vim.VehicleItemID = vii.VehicleItemID
						AND vim.VehicleItemMileageTS::date <= s.SoldDate
					ORDER BY vim.VehicleItemMileageTS DESC
					limit 1)
				ELSE (
					SELECT Value 
					FROM ads.ext_Vehicle_Item_Mileages vim 
					WHERE vim.VehicleItemID = vii.VehicleItemID
					ORDER BY vim.VehicleItemMileageTS DESC
					limit 1)
			END AS Mileage,

			Trim(vi.Engine) AS Engine, 
			Trim(vi.Transmission) AS Transmission,
			Trim(vio.VehicleOptions) AS Options, 
			Trim(vio.AdditionalEquipment) as OptionsExploded, -- actually additional equipment

			vii.FromTS::date as DateAcquired,
			case
				when s.DaysAvailable is null then 0
				else s.DaysAvailable
			end as DaysToSell,
			case
				when s.DaysAvailable is null then 0
				else s.DaysAvailable
			end as DaysOnLot,
			case
				when s.SoldDate is null then _as_of_date - vii.FromTS::date
				else s.SoldDate - vii.FromTS::date
			end as DaysOwned,
			s.SoldDate as DateSold,

			(
				SELECT vpd.Amount
				FROM ads.ext_Vehicle_Pricings vp
				inner join ads.ext_Vehicle_Pricing_Details vpd on vpd.VehiclePricingID = vp.VehiclePricingID 
					and vpd.Typ = 'VehiclePricingDetail_Invoice' 
				WHERE vp.VehicleInventoryItemID = vii.VehicleInventoryItemID
				ORDER BY VehiclePricingTS DESC
				limit 1) as MCRV,	 

			(
				SELECT vpd.Amount
				FROM ads.ext_Vehicle_Pricings vp
				inner join ads.ext_Vehicle_Pricing_Details vpd on vpd.VehiclePricingID = vp.VehiclePricingID 
					and vpd.Typ = 'VehiclePricingDetail_BestPrice' 
				WHERE vp.VehicleInventoryItemID = vii.VehicleInventoryItemID
				ORDER BY VehiclePricingTS DESC
				limit 1) as Price,	   

			Coalesce(glt.TotalCost, 0) AS Net,	   
			Coalesce(glt.SoldAmount, 0) as SoldAmount,
			Coalesce(glt.SoldAmount, 0) - Coalesce(glt.TotalCost, 0) as Gross,   	   	
			case
				when s.SoldRetail = 'X' then 'VehicleSale_Retail'
				when s.SoldWholesale = 'X' then 'VehicleSale_Wholesale'
				else null
			end as Disposition,    
			s.SoldBy as SoldBy,
			(
				SELECT p.FullName 
				FROM ads.ext_people p 
				WHERE p.PartyID = ve.VehicleEvaluatorID 
				 AND p.ThruTS IS NULL) AS AppraisedBy, 

			s.Status as SalesStatus,
			(
				SELECT td.Description 
				FROM ads.ext_Typ_Descriptions td 
				WHERE td.typ = ve.Typ AND td.ThruTS IS null) AS Source,
			(
				SELECT td.Description 
				FROM ads.ext_Typ_Descriptions td 
				WHERE td.typ = mmc.VehicleType AND td.ThruTS IS null) AS VehicleType,
			(
				SELECT td.Description 
				FROM ads.ext_Typ_Descriptions td 
				WHERE td.typ = mmc.VehicleSegment AND td.ThruTS IS null) AS Segment,   
			mmc.Luxury AS Luxury,
			mmc.Sport AS Sport,
			mmc.Commercial AS Commercial
			FROM ads.ext_Vehicle_Inventory_Items vii
			INNER JOIN ads.ext_Vehicle_Items vi ON vi.VehicleItemID = vii.VehicleItemID
			--  this inner join restricts query to vehicles of interest
			INNER JOIN cra.statusesX s on vii.VehicleInventoryItemID = s.VehicleInventoryItemID
			inner JOIN ads.ext_Make_Model_Classifications mmc ON  mmc.Make = vi.Make AND mmc.Model = vi.Model	    	      
			LEFT OUTER JOIN ads.ext_gross_log_table glt ON vii.StockNumber = glt.StockNumber 
			LEFT outer JOIN ads.ext_Vehicle_Item_Options vio ON vio.VehicleItemID = vii.VehicleItemID
			LEFT OUTER JOIN ads.ext_Vehicle_Evaluations ve ON ve.VehicleInventoryItemID = vii.VehicleInventoryItemID;

			
		truncate cra.temp1;
		insert into cra.temp1 
		SELECT
			vii.*,
			-- without the top 1 the following generates more than one record 
		 (
			SELECT gl.Description 
			FROM ads.ext_Glump_Limits gl 
			WHERE gl.ShortVehicleTyp = vii.VehicleType 
				AND (vii.Price >= gl.LowerLimit 
				AND vii.Price <= gl.UpperLimit)
			limit 1) AS Glump,  

			case
				when vii.DateSold IS NULL then NULL
				WHEN vii.DaysOnLot IS NULL THEN null
				when (vii.daysonlot/7.01)::integer > 8 then 8
				else (vii.DaysOnLot / 7.01)::integer

			end as "SellingWeek",    
			case
				when (vii.Mileage = 0) or (Length(Trim(vii.Year)) <> 4) or (position('.' in vii.Year) <> 0) or (position('C' in vii.Year) <> 0) or (position('/' in vii.Year) <> 0) or ((vii.Year < '1901') OR (vii.Year > '2050')) then Null
				when 
					mileage /
						case
							when round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
							else round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
						end <= 4000 then 'Freaky Low'
				when 
					mileage /
						case
							when round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
							else round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
						end <= 8000 then 'Very Low'
				when 
					mileage /
						case
							when round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
							else round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
						end <= 12000 then 'Low'
				when 
					mileage /
						case
							when round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
							else round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
						end <= 15000 then 'Average'
				when 
					mileage /
						case
							when round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
							else round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
						end <= 18000 then 'High'
				when 
					mileage /
						case
							when round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
							else round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
						end <= 22000 then 'Very High'
				when 
					mileage /
						case
							when round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
							else round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
						end <= 999999 then 'Freaky High'
				else
					'Unknown'																		
			end as mileagecategory,						
			case
				when DateSold = '1900-01-01' then null
				when vii.Disposition = 'VehicleSale_Retail' and vii.DaysOnLot BETWEEN  0 and 30 then 'Fresh'
				when vii.Disposition = 'VehicleSale_Retail' and vii.DaysOnLot > 30 then 'Aged'
				when vii.Disposition = 'VehicleSale_Wholesale' and vii.DaysOnLot is null then 'Pure W/S'
				when vii.Disposition = 'VehicleSale_Wholesale' and vii.DaysOnLot is not null then 'Aged W/S'
					else null
			end as "SalesCategory",
			case
				when vii.Price = 0 then NULL
				WHEN vii.Price IS NULL THEN NULL
				when vii.Price >     0 and vii.Price <=  6000 then  0
				when vii.Price >  6000 and vii.Price <=  8000 then  1
				when vii.Price >  8000 and vii.Price <= 10000 then  2
				when vii.Price > 10000 and vii.Price <= 12000 then  3
				when vii.Price > 12000 and vii.Price <= 14000 then  4
				when vii.Price > 14000 and vii.Price <= 16000 then  5
				when vii.Price > 16000 and vii.Price <= 18000 then  6
				when vii.Price > 18000 and vii.Price <= 20000 then  7
				when vii.Price > 20000 and vii.Price <= 22000 then 8
				when vii.Price > 22000 and vii.Price <= 24000 then 9
				when vii.Price > 24000 and vii.Price <= 26000 then 10
				when vii.Price > 26000 and vii.Price <= 28000 then 11
				when vii.Price > 28000 and vii.Price <= 30000 then 12
				when vii.Price > 30000 and vii.Price <= 32000 then 13
				when vii.Price > 32000 and vii.Price <= 34000 then 14
				when vii.Price > 34000 and vii.Price <= 36000 then 15
				when vii.Price > 36000 and vii.Price <= 38000 then 16
				when vii.Price > 38000 and vii.Price <= 40000 then 17
				else
					18
			end as "PriceBand",
			case
				when vii.Price = 0 then NULL
				WHEN vii.Price IS NULL THEN NULL
				when vii.Price >     0 and vii.Price <=  6000 then '0-6k'
				when vii.Price >  6000 and vii.Price <=  8000 then '6-8k'
				when vii.Price >  8000 and vii.Price <= 10000 then '8-10k'
				when vii.Price > 10000 and vii.Price <= 12000 then '10-12k'
				when vii.Price > 12000 and vii.Price <= 14000 then '12-14k'
				when vii.Price > 14000 and vii.Price <= 16000 then '14-16k'
				when vii.Price > 16000 and vii.Price <= 18000 then '16-18k'
				when vii.Price > 18000 and vii.Price <= 20000 then '18-20k'
				when vii.Price > 20000 and vii.Price <= 22000 then '20-22k'
				when vii.Price > 22000 and vii.Price <= 24000 then '22-24k'
				when vii.Price > 24000 and vii.Price <= 26000 then '24-26k'
				when vii.Price > 26000 and vii.Price <= 28000 then '26-28k'
				when vii.Price > 28000 and vii.Price <= 30000 then '28-30k'
				when vii.Price > 30000 and vii.Price <= 32000 then '20-32k'
				when vii.Price > 32000 and vii.Price <= 34000 then '32-24k'
				when vii.Price > 34000 and vii.Price <= 36000 then '34-36k'
				when vii.Price > 36000 and vii.Price <= 38000 then '36-38k'
				when vii.Price > 38000 and vii.Price <= 40000 then '38-40k'
				else
					'40k+'
			end as "PriceBandDesc"
			FROM cra.temp vii;

			update cra.temp1
			set price_bands_5k = 
				case
					when Price = 0 then NULL
					WHEN Price IS NULL THEN NULL
					when Price >     0 and Price <= 10000 then '0-10k'
					when Price > 10000 and Price <= 15000 then '10-15k'
					when Price > 15000 and Price <= 20000 then '15-20k'
					when Price > 20000 and Price <= 25000 then '20-25k'
					when Price > 25000 and Price <= 30000 then '25-30k'
					when Price > 30000 and Price <= 35000 then '30-35k'
					when Price > 35000 and Price <= 40000 then '35-40k'
					when Price > 40000 and Price <= 45000 then '40-45k'
					when Price > 45000 and Price <= 50000 then '45-50k'
					else '50k+'
			end;			

			update cra.temp1
			set price_bands_10k = 
				case
					when Price = 0 then NULL
					WHEN Price IS NULL THEN NULL
					when Price >     0 and Price <= 10000 then '0-10k'
					when Price > 10000 and Price <= 20000 then '10-20k'
					when Price > 20000 and Price <= 30000 then '20-30k'
					when Price > 30000 and Price <= 40000 then '30-40k'
					when Price > 40000 and Price <= 50000 then '40-50k'
					when Price > 50000 and Price <= 60000 then '50-60k'
					when Price > 60000 and Price <= 70000 then '60-70k'
					when Price > 70000 and Price <= 80000 then '70-80k'
					else '80k+'
			end;		

			update cra.temp1
			set vehicle_type = 'SUV'
			where vehicle_type = 'crossover';

			-- added 05/19 to correct sales dates that were on a sunday
			update cra.temp1 x
			set date_sold = date_sold - 1
			from (
				select vehicle_inventory_item_id
				from cra.temp1 a
				join dds.dim_date b on a.date_sold = b.the_date
					and b.day_name = 'sunday') y
			where x.vehicle_inventory_item_id = y.vehicle_inventory_item_id;			
			
end $$;

-- add PK 5/15/22
-- ALTER TABLE cra.temp1 ADD PRIMARY KEY (vehicle_inventory_item_id);

------------------------------------------------------------------------
--< duplicate rows
------------------------------------------------------------------------
select vehicle_inventory_item_id
from cra.temp1
group by vehicle_inventory_item_id
having count(*) > 1

select a.*, md5(a::text) as hash
from cra.temp1 a
where vehicle_inventory_item_id = '42ec7ddf-112b-4fb5-a09f-f37762caeade'

select a.*, md5(a::text) as hash
from cra.temp1 a
join (
	select vehicle_inventory_item_id
	from cra.temp1
	group by vehicle_inventory_item_id
	having count(*) > 1) b on a.vehicle_inventory_item_id = b.vehicle_inventory_item_id
order by a.vehicle_inventory_item_id	

-- differences on the dups appear to be sales_status, some are simply duplicates
select vehicle_inventory_item_id, stock_number, vin, sales_status, hash
from (
	select a.*, md5(a::text) as hash
	from cra.temp1 a
	join (
		select vehicle_inventory_item_id
		from cra.temp1
		group by vehicle_inventory_item_id
		having count(*) > 1) b on a.vehicle_inventory_item_id = b.vehicle_inventory_item_id) x
order by vehicle_inventory_item_id	

05/15/22
the problem was in the sold part of statusesX

------------------------------------------------------------------------
--/> duplicate rows
------------------------------------------------------------------------




create index on cra.temp1
select count(*) from cra.temp1

select * from cra.statusesX
select * from ads.ext_vehicle_item_options

select * from ops.ads_Extract where task = 'ext_vehicle_item_options'

select * from ads.ext_typ_descriptions order by fromts desc



select * from cra.temp1 
where glump = 'pickups - high'
  and "PriceBandDesc" = '40k+'
  and location = 'GF-Rydells'
--   and disposition <> 'VehicleSale_Wholesale'
order by stocknumber

select * from cra.temp where stocknumber = 'G44424A'
select * from cra.temp1 where stocknumber = 'G44424A'

-- min max avg price by year for large pickup
select year, count(*), min(price)::integer, max(price)::integer, avg(price)::integer
from cra.temp1 
where vehicletype = 'pickup'
  and segment= 'large'
group by year  
order by year d esc

select * from ads.ext_glump_limits where shortvehicletyp = 'pickup' order by glumptyp

-- last 30 days of sales by glump (5k pricebands)
select 
  case
    when vehicle_type in ('SUV','Crossover') then 'SUV'
    else vehicle_type
  end, segment, price_bands_5k, count(*)
-- select * 
from cra.temp1 a
where date_sold between current_date - 30 and current_date - 1
  and disposition = 'VehicleSale_Retail'
group by 
  case
    when vehicle_type in ('SUV','Crossover') then 'SUV'
    else vehicle_type
  end, segment, price_bands_5k
order by count(*) Desc;

-- last 30 days of sales by type/segment
select 
  case
    when vehicle_type in ('SUV','Crossover') then 'SUV'
    
    else vehicle_type
  end, segment, count(*)
-- select * 
from cra.temp1 a
where date_sold between current_date - 30 and current_date - 1
  and disposition = 'VehicleSale_Retail'
group by 
  case
    when vehicle_type in ('SUV','Crossover') then 'SUV'
    else vehicle_type
  end, segment
order by count(*) Desc;


-- last 30 days of sales by 5k pricebands
select price_bands_5k, count(*)
-- select * 
from cra.temp1 a
where date_sold between current_date - 30 and current_date - 1
  and disposition = 'VehicleSale_Retail'
group by price_bands_5k
order by count(*) Desc;


-- most of the ones showing no eval/acv data are G stocknumbers
-- wait a fucking minute, don't care if there is a price, 
-- i am interested in a temp price for vehicles not yet priced
select a.vehicleinventoryitemid, stocknumber, vin,
  case
    when vehicletype in ('SUV','Crossover') then 'SUV'
    else vehicletype
  end, segment, price_bands_5k, mcrv, price, b.selectedacv, c.amountshown, c.*
-- select * 
from cra.temp1 a
left join ads.ext_acvs b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and b.basistable = 'vehicleevaluations'
left join ads.ext_vehicle_evaluations c on a.vehicleinventoryitemid = c.vehicleinventoryitemid  
where datesold between current_date - 30 and current_date - 1
  and disposition = 'VehicleSale_Retail'
and b.selectedacv  is null  
order by vehicletype,segment,price_bands_5k  


  
select  salesstatus, count(*)
from cra.temp1
group by salesstatus

select count(*), salesstatus from cra.temp1 where price is null group by salesstatus
-- looks like these are inandouts, date acquired = date sold
select * from cra.temp1 where price is null and salesstatus = 'Sold'

/* TODO */
price null sold vehicles
source on a lot of purchases (P stocknumber) are wrong
/* TODO */

-- all current inventory without a price
-- limit by daysowned unless it is in inspection pending, nope, leave out in transit/trade buffer
-- select stocknumber, source
-- include demand, appeal, desirablity, 3rd party, price components, total recon from eval
select stock_number, vin, model_year, make, model, trim_level, body_style, a.mileage, 
	mileage_category, vehicle_type, segment, days_owned, sales_status,
  b.desirability, 
  (b.reconmechanicalamount+b.reconbodyamount+reconappearanceamount+recontireamount+reconglassamount+reconinspectionamount)::integer as eval_recon,
  b.amountshown, c.amount::integer as eval_acv, d.amount::integer as eval_finished, f.amount as manheim,
  e.averagebase, e.regionaladjustment, e.averagemileageadjustment, e.addsdeductstotal
from cra.temp1 a
left join ads.ext_vehicle_evaluations b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
left join ads.ext_vehicle_price_components c on a.vehicle_inventory_item_id = c.vehicleinventoryitemid
  and c.typ = 'VehiclePriceComponent_ACV'
left join ads.ext_vehicle_price_components d on a.vehicle_inventory_item_id = d.vehicleinventoryitemid
  and d.typ = 'VehiclePriceComponent_Finished'
left join ads.ext_black_book_values e on b.vehicleevaluationid = e.tablekey
  and e.basistable = 'VehicleEvaluations'  
left join ads.ext_vehicle_third_party_values f on a.vehicle_inventory_item_id = f.vehicleinventoryitemid
  and f.basistable = 'VehicleEvaluations'  
where a.sales_status = 'Raw Materials' --not in ('Sold' ,'Wholesaled','In Transit/Trade Buffer')
  and price is null
order by days_owned desc  


-- just to see, same values for vehicles sold in the last 30 days
select stock_number, vin, model_year, make, model, trim_level, body_style, a.mileage, 
	mileage_category, vehicle_type, segment, days_owned, sales_status,
  b.desirability, 
  (b.reconmechanicalamount+b.reconbodyamount+reconappearanceamount+recontireamount+reconglassamount+reconinspectionamount)::integer as eval_recon,
  b.amountshown, c.amount::integer as eval_acv, d.amount::integer as eval_finished, a.price::integer, (d.amount + .1*d.amount)::integer, a.price - (d.amount + .1*d.amount)::integer--f.amount as manheim,  a.pric
--   e.averagebase, e.regionaladjustment, e.averagemileageadjustment, e.addsdeductstotal, a.price::integer
from cra.temp1 a
left join ads.ext_vehicle_evaluations b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
left join ads.ext_vehicle_price_components c on a.vehicle_inventory_item_id = c.vehicleinventoryitemid
  and c.typ = 'VehiclePriceComponent_ACV'
  and c.basistable = 'VehicleEvaluations'
left join ads.ext_vehicle_price_components d on a.vehicle_inventory_item_id = d.vehicleinventoryitemid
  and d.typ = 'VehiclePriceComponent_Finished'
  and d.basistable = 'VehicleEvaluations'  
left join ads.ext_black_book_values e on b.vehicleevaluationid = e.tablekey
  and e.basistable = 'VehicleEvaluations'  
left join ads.ext_vehicle_third_party_values f on a.vehicle_inventory_item_id = f.vehicleinventoryitemid
  and f.basistable = 'VehicleEvaluations'  
where date_sold between current_date - 30 and current_date - 1
  and disposition = 'VehicleSale_Retail'
order by a.price - (d.amount + .1*d.amount)::integer desc   






--------------------------------------------------------------------------
--< tool statuses
--------------------------------------------------------------------------
-- RawMaterials,RawMaterials_RawMaterials is the first vii status created
-- RMFlagRMP,RMFlagRMP_RawMaterialsPool is created after walk

select category, status, count(*)
from ads.ext_vehicle_inventory_item_statuses
where fromts::date > '01/01/2022'
group by category, status
order by category,status

--------------------------------------------------------------------------
--/> tool statuses
--------------------------------------------------------------------------


--------------------------------------------------------------------------
--< short today
--------------------------------------------------------------------------
from tool calculations (advantage Stored PROCEDURE GetAppraisalDemand)
roughly analagous to tool deamnd score 
base it on a days supply of < 30 (demand of 3), inventory/sales last 30

-- current inventory by 5k glumps
select 
  case
    when vehicle_type in ('SUV','Crossover') then 'SUV'
    else vehicle_type
  end, segment, coalesce(price_bands_5k, estimated_price_bands_5k) as price_bands_5k, count(*)
from cra.temp1 a
left join (
	select a.stock_number, (d.amount + .1*d.amount)::integer as est_price,
		case
			when (d.amount + .1*d.amount)::integer = 0 then NULL
			WHEN (d.amount + .1*d.amount)::integer IS NULL THEN NULL
			when (d.amount + .1*d.amount)::integer >     0 and (d.amount + .1*d.amount)::integer <= 10000 then '0-10k'
			when (d.amount + .1*d.amount)::integer > 10000 and (d.amount + .1*d.amount)::integer <= 15000 then '10-15k'
			when (d.amount + .1*d.amount)::integer > 15000 and (d.amount + .1*d.amount)::integer <= 20000 then '15-20k'
			when (d.amount + .1*d.amount)::integer > 20000 and (d.amount + .1*d.amount)::integer <= 25000 then '20-25k'
			when (d.amount + .1*d.amount)::integer > 25000 and (d.amount + .1*d.amount)::integer <= 30000 then '25-30k'
			when (d.amount + .1*d.amount)::integer > 30000 and (d.amount + .1*d.amount)::integer <= 35000 then '30-35k'
			when (d.amount + .1*d.amount)::integer > 35000 and (d.amount + .1*d.amount)::integer <= 40000 then '35-40k'
			when (d.amount + .1*d.amount)::integer > 40000 and (d.amount + .1*d.amount)::integer <= 45000 then '40-45k'
			when (d.amount + .1*d.amount)::integer > 45000 and (d.amount + .1*d.amount)::integer <= 50000 then '45-50k'
		else '50k+'  
		end as estimated_price_bands_5k
	from cra.temp1 a
	left join ads.ext_vehicle_evaluations b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
	left join ads.ext_vehicle_price_components d on a.vehicle_inventory_item_id = d.vehicleinventoryitemid
		and d.typ = 'VehiclePriceComponent_Finished'
	where a.sales_status not in ('Sold' ,'Wholesaled','In Transit/Trade Buffer')
		and a.price is null) aa on a.stock_number = aa.stock_number
where a.sales_status not in ('Sold' ,'Wholesaled','In Transit/Trade Buffer')
group by 
  case
    when vehicle_type in ('SUV','Crossover') then 'SUV'
    else vehicle_type
  end, segment, coalesce(price_bands_5k, estimated_price_bands_5k)
order by count(*) Desc;

-- current inventory by 5k glumps from greg.uc_daily_snapshot_beta_1 (they  don't match)
select shape, size, price_band_5k, count(*)
from (
	select 
		case
			when shape in ('SUV','Crossover') then 'SUV'
			else shape
		end as shape, size, best_price,
		case
			when best_price = 0 then NULL
			WHEN best_price IS NULL THEN NULL
			when best_price >     0 and best_price <= 10000 then '0-10k'
			when best_price > 10000 and best_price <= 15000 then '10-15k'
			when best_price > 15000 and best_price <= 20000 then '15-20k'
			when best_price > 20000 and best_price <= 25000 then '20-25k'
			when best_price > 25000 and best_price <= 30000 then '25-30k'
			when best_price > 30000 and best_price <= 35000 then '30-35k'
			when best_price > 35000 and best_price <= 40000 then '35-40k'
			when best_price > 40000 and best_price <= 45000 then '40-45k'
			when best_price > 45000 and best_price <= 50000 then '45-50k'
			else '50k+'  
		end as price_band_5k
	from greg.uc_daily_snapshot_beta_1
	where the_date = current_date
-- 		and status = 'avail_fresh') a
-- 		and status like 'avail%') a
		and sale_date > current_date) a
group by shape, size, price_band_5k
order by count(*) desc		

-- last 30 day sales

-- last 30 day avg over 365
-- moving average
--------------------------------------------------------------------------
--/> short today
--------------------------------------------------------------------------
----------------------------------------------------------------------------------
--< data anomalies in greg.uc_daily_snapshot_beta_1
----------------------------------------------------------------------------------
ended up just deleting the bad rows (88,941 for 124 unique vehicles)
added a clause to greg.update_daily_snapshot_beta_1() to ensure that greg.uc_daily_snapshot_beta_1 only 
	includes vehicles that exist in ads.ext_vehicle_inventory_items

create table greg.bad_vehicles (
  stock_number citext not null,
  vin citext not null,
  primary key(stock_number,vin));

insert into greg.bad_vehicles  
select distinct stock_number, vin
from greg.uc_daily_snapshot_beta_1 a
where not exists (
  select 1
  from ads.ext_vehicle_inventory_items
  where stocknumber = a.stock_number
    and vin = a.vin);


select count(*)
from greg.uc_base_vehicles a
where exists (
  select 1
  from ads.ext_vehicle_inventory_items
  where vehicle_inventory_item_id = a.vehicle_inventory_item_id)


delete
from greg.uc_daily_snapshot_beta_1 a
where not exists (
  select 1
  from ads.ext_vehicle_inventory_items
  where stocknumber = a.stock_number
    and vin = a.vin)

drop table  greg.bad_vehicles



select distinct a.the_date
from greg.uc_daily_snapshot_beta_1 a
join dds.dim_date b on a.the_date = b.the_date
  and extract(day from b.the_date) = 1 
order by a.the_date  

-- resulting in compare inventory between greg.uc_daily_snapshot_beta_1 and cra.temp1
-- fucking A, perfect match between cra.temp1 and greg.uc_daily_snapshot_beta_1 a
-- which means this may be a path to deriving historical shortness
select * 
from (
select 'greg'::text as source, stock_number, vin
from greg.uc_daily_snapshot_beta_1 a
join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
where the_date = current_date) a
full outer join (
select 'cra'::text as source, stock_number, vin
from cra.temp1 a
where a.sales_status not in ('Sold' ,'Wholesaled')) b on a.stock_number = b.stock_number
where a.stock_number is null or b.stock_number is null
order by coalesce(a.stock_number, b.stock_number)  
----------------------------------------------------------------------------------
--/> data anomalies in greg.uc_daily_snapshot_beta_1
----------------------------------------------------------------------------------    



