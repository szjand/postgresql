﻿select * from board.board_types
select * from board.sales_board limit 2

select b.board_type, b.board_sub_type, a.deal_number, a.stock_number, a.vin, a.boarded_date,
  deal_details->>'CompanyNumber' as store, deal_details->>'BuyerNumber' as buyer_number,
  deal_details->>'DealDate' as deal_date, deal_details->>'NumberOfTrades' as trades
-- select a.*  
from board.sales_board a
join board.board_types b on a.board_type_key = b.board_type_key
where a.boarded_ts::date > '09/30/2022'
limit 10

-- september retail deals
select count(*) over (partition by aa.store, aa.make, aa.n_u), * 
from (
	select b.board_type, b.board_sub_type, a.deal_number, a.stock_number, a.vin, c.make, a.deal_details->>'VehicleType' as n_u, a.boarded_date,
		a.deal_details->>'CompanyNumber' as store, a.deal_details->>'BuyerNumber' as buyer_number,
		dds.db2_integer_to_date((a.deal_details->>'DealDate')::integer) as deal_date, a.deal_details->>'NumberOfTrades' as trades
	-- select a.*  
	from board.sales_board a
	join board.board_types b on a.board_type_key = b.board_type_key
		and b.board_type = 'Deal'
		and b.board_sub_type <> 'wholesale'
	left join arkona.ext_inpmast c on a.vin = c.inpmast_vin  
	where a.boarded_ts::date between '09/01/2022' and '09/30/2022'
		and -- ctp shows up on statement line 44  -- but this does not seem to pick it up G45243
		  case
		    when a.deal_number = 'N/A' then b.board_sub_type = 'CTP'
		    else true
		  end
		and not a.is_deleted) aa
left join ( -- unwinds
	select b.board_type, b.board_sub_type, a.deal_number, a.stock_number, a.vin, '',
		a.deal_details->>'VehicleType' as n_u, a.boarded_date,'','','12/31/9999',''
	-- select a.*  
	from board.sales_board a 
	join board.board_types b on a.board_type_key = b.board_type_key
		and b.board_type = 'Back-on'
	--   and b.board_sub_type <> 'wholesale'
	left join arkona.ext_inpmast c on a.vin = c.inpmast_vin  
	where a.boarded_ts::date between '09/01/2022' and '09/30/2022'
		and a.deal_number <> 'N/A') bb on aa.stock_number = bb.stock_number 
order by aa.store, aa.n_u, aa.make, aa.stock_number


G45243 on FS, not in board
select *
	from board.sales_board a
	join board.board_types b on a.board_type_key = b.board_type_key
where a.stock_number = 'G45243'	
