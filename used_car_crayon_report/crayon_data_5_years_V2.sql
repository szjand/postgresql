﻿
do $$
	declare
		_market citext := 'GF-Grand Forks';
		_market_id citext := (select partyid from ads.ext_organizations where fullname = _market);
		_as_of_date date := '05/31/2022';
		_from_date date := '06/01/2017';
-- 		_temp_table_name citext := 'the_temp_table';
-- 		_load_table_sql citext;
-- 		_t1 timestamp := (select now());
-- 		_t2 timestamp;
-- 		_t3 timestamp;
-- 		_t4 timestamp;

  begin
-- Select all sales buffer statuses for vehicles of interest  
    truncate cra.salesbufferrecordsXxX;
		insert into cra.salesbufferrecordsXxX 
		select viis.VehicleInventoryItemID,
			case when viis.FromTS > now() then _as_of_date::TIMESTAMP else viis.FromTS end as FromTS,
			case when viis.ThruTS > now() then _as_of_date::TIMESTAMP else viis.ThruTS end as ThruTS
-- into  #salesbufferrecordsXxX
		from ads.ext_vehicle_inventory_item_statuses viis
		inner join ads.ext_vehicle_inventory_items vii on vii.vehicleinventoryitemid = viis.vehicleinventoryitemid
		where viis.Status = 'RMFlagSB_SalesBuffer'
			and vii.LocationID IN (
				SELECT PartyID2
				FROM ads.ext_party_relationships
				WHERE PartyID1 =  _Market_ID 
				  AND Typ = 'PartyRelationship_MarketLocations') 
			AND 
				CASE
					WHEN vii.ThruTS > now() then TRUE   --in inventory
					ELSE
						CASE 
							WHEN vii.ThruTS::date > _From_Date AND vii.ThruTS::date <= _as_of_date THEN TRUE  --Vehicle sold within last year
							ELSE FALSE 
						END
					END;
-- Calculate the hours available for each vehicle for each time in RMFlagAV_Available status
    truncate cra.totalavailablehoursX;
    insert into cra.totalavailablehoursX
		select viis.VehicleInventoryItemID, 
			case
				when viis.ThruTS > now() then extract(epoch from  _as_of_date - viis.fromts)/3600
				else extract(epoch from viis.thruts - viis.fromts)/ 3600
			end as Hours,
			case when viis.FromTS > now() then _as_of_date::TIMESTAMP else viis.FromTS end as FromTS,
			case when viis.ThruTS > now() then _as_of_date::TIMESTAMP else viis.ThruTS end as ThruTS, stocknumber
-- 		into #totalavailablehoursX
		from ads.ext_vehicle_inventory_item_statuses viis
		inner join ads.ext_vehicle_inventory_items vii on vii.vehicleinventoryitemid = viis.vehicleinventoryitemid
		where viis.Status = 'RMFlagAV_Available'
			and vii.LocationID IN (
				SELECT PartyID2
				FROM ads.ext_party_relationships
				WHERE PartyID1 =  _Market_ID 
				  AND Typ = 'PartyRelationship_MarketLocations') 
			AND 
				CASE
					WHEN vii.ThruTS > now() then TRUE   --in inventory
					ELSE
						CASE 
							WHEN vii.ThruTS::date > _From_Date AND vii.ThruTS::date <= _as_of_date THEN TRUE  --Vehicle sold within last year
							ELSE FALSE 
						END
					END;			

--Calculate total hours available for each vehicle, insert into #availablehoursadjustedX (adjustment comes later)
    truncate cra.availablehoursadjustedX;
		insert into cra.availablehoursadjustedX 
		select VehicleinventoryItemID, sum(hours) as AvailableHours
		FROM cra.totalavailablehoursX
		where hours is not null
		group by 1;		

-- Calculate adjustments for SalesBuffer hours for vehicles that are available
    truncate cra.totalsbadjustmentX;
    insert into cra.totalsbadjustmentX 
		select s.VehicleInventoryItemID, sum(s.Adjustment) as Adjustment
		from (
			select
				t1.vehicleinventoryitemid, --t1.FromTS, t1.thruTS, t2.FromTS, t2.ThruTS,
				case
					when ( --inner sb
							t1.FromTS <= t2.FromTS
							and 
							t1.ThruTS >= t2.FromTS
							and not t2.ThruTS > t1.ThruTS) then extract(epoch from t2.ThruTS - t2.FromTS)/ 3600
					when  ( --overlap from before
						t2.FromTS <= t1.FromTS
						and
						t2.ThruTS >= t1.FromTS
					  and not t2.ThruTS > t1.ThruTS) then extract(epoch from t2.ThruTS - t1.FromTS)/ 3600 
					when  (--overlap after
						t1.FromTS <= t2.FromTS
						and 
						t1.ThruTS >= t2.FromTS) then extract(epoch from t2.ThruTS - t2.FromTS)/ 3600 - extract(epoch from  t2.ThruTS - t1.ThruTS)/ 3600 
					else --overlap all
						 extract(epoch from t1.ThruTS - t1.FromTS)/ 3600  
				end as Adjustment
			from cra.totalavailablehoursX t1
			join cra.salesbufferrecordsXxX t2 on t1.vehicleInventoryItemID = t2.VehicleInventoryItemID  
			where (
				t1.FromTS <= t2.FromTS
				and 
				t1.ThruTS >= t2.FromTS)
			or (
				t2.FromTS <= t1.FromTS
				and
				t2.ThruTS > t1.FromTS)) s		
			group by 1;				

-- Update the #availablehoursadjustedX  
		update cra.availablehoursadjustedX x
		set availablehours = x.availablehours - y.adjustment
		from (
			select VehicleInventoryItemID, adjustment
			from cra.totalsbadjustmentX) y
		where x.VehicleInventoryItemID = y.VehicleInventoryItemID;

-- Create core statuses table. Contains SoldDate and flags for statuses
-- Joins current inventory and sold vehicle for time period
    truncate cra.statusesX;
    insert into cra.statusesX
		select vii.VehicleInventoryItemID,
			(Select 'X'::text from ads.ext_Vehicle_Inventory_Item_Statuses v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Status = 'RMFlagPIT_PurchaseInTransit' 
					AND  v.FromTS::date <= _as_of_date
					AND (
						v.ThruTS::date > _as_of_date
						OR v.ThruTS > now())) as PurchaseInTransit,
						
		 (Select 'X'::text from ads.ext_Vehicle_Inventory_Item_Statuses v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Status = 'RMFlagTNA_TradeNotAvailable' 
					AND  v.FromTS::date <= _as_of_date
					AND (
						v.ThruTS::date > _as_of_date
						OR v.ThruTS > now()))  as TradeNotAvailable,
						
		 (Select 'X'::text from ads.ext_Vehicle_Inventory_Item_Statuses v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Status = 'RMFlagRMP_RawMaterialsPool' 
					AND  v.FromTS::date <= _as_of_date
					AND (
						v.ThruTS::date > _as_of_date
						OR v.ThruTS > now()))  as RawMaterialsPool,

		 (Select 'X'::text from ads.ext_Vehicle_Inventory_Item_Statuses v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Status = 'RMFlagPulled_Pulled' 
					AND  v.FromTS::date <= _as_of_date
					AND (
						v.ThruTS::date > _as_of_date
						OR v.ThruTS > now()))  as Pulled,

		 (Select 'X'::text from ads.ext_Vehicle_Inventory_Item_Statuses v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Status = 'AppearanceReconProcess_InProcess' 
					AND  v.FromTS::date <= _as_of_date
					AND (
						v.ThruTS::date > _as_of_date
						OR v.ThruTS > now()))  as AppearanceWIP,

		 (Select 'X'::text from ads.ext_Vehicle_Inventory_Item_Statuses v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Status = 'MechanicalReconProcess_InProcess' 
					AND  v.FromTS::date <= _as_of_date
					AND (
						v.ThruTS::date > _as_of_date
						OR v.ThruTS > now()))  as MechanicalWIP,

		 (Select 'X'::text from ads.ext_Vehicle_Inventory_Item_Statuses v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Status = 'BodyReconProcess_InProcess' 
					AND  v.FromTS::date <= _as_of_date
					AND (
						v.ThruTS::date > _as_of_date
						OR v.ThruTS > now()))  as BodyWIP,

		 (Select 'X'::text from ads.ext_Vehicle_Inventory_Item_Statuses v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Status = 'RMFlagPB_PricingBuffer' 
					AND  v.FromTS::date <= _as_of_date
					AND (
						v.ThruTS::date > _as_of_date
						OR v.ThruTS > now()))  as PricingBuffer,

		 (Select 'X'::text from ads.ext_Vehicle_Inventory_Item_Statuses v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Status = 'RMFlagAV_Available' 
					AND  v.FromTS::date <= _as_of_date
					AND (
						v.ThruTS::date > _as_of_date
						OR v.ThruTS > now()))  as AvailableNow,

		 (Select 'X'::text from ads.ext_Vehicle_Inventory_Item_Statuses v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Status = 'RMFlagSB_SalesBuffer' 
					AND  v.FromTS::date <= _as_of_date
					AND (
						v.ThruTS::date > _as_of_date
						OR v.ThruTS > now()))  as SalesBuffer,

		 (Select 'X'::text from ads.ext_Vehicle_Inventory_Item_Statuses v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Status = 'RMFlagWSB_WholesaleBuffer' 
					AND  v.FromTS::date <= _as_of_date
					AND (
						v.ThruTS::date > _as_of_date
						OR v.ThruTS > now()))  as WholesaleBuffer,

		 (Select 'X'::text from ads.ext_Vehicle_Inventory_Item_Statuses v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Status = 'RawMaterials_Sold' 
					AND  v.FromTS::date <= _as_of_date
					AND (
						v.ThruTS::date > _as_of_date
						OR v.ThruTS > now()))  as SoldNotDelivered,
		 
			null::text as SoldRetail,
			null::text as SoldWholesale,
			null::date as SoldDate,
			null::text as SoldBy,
			null::integer as DaysAvailable,
			null::text as Status  
		from ads.ext_vehicle_inventory_items vii
		where vii.LocationID IN (
			SELECT PartyID2
			FROM ads.ext_party_relationships
			WHERE PartyID1 =  _Market_ID 
				AND Typ = 'PartyRelationship_MarketLocations') 
			AND vii.FromTS::date <= _as_of_date
			AND vii.ThruTS > now()

		UNION

		SELECT vii.VehicleInventoryItemID,
		 null as PurchaseInTransit,
		 null as TradeNotAvailable,
		 null as RawMaterialsPool,
		 null as Pulled,
		 null as AppearanceWIP,
		 null as MechanicalWIP,
		 null as BodyWIP,
		 null as PricingBuffer,
		 null as AvailableNow,
		 null as SalesBuffer,
		 null as WholesaleBuffer,
		 null as SoldNotDelivered,     
		 (Select 'X'::text from ads.ext_vehicle_sales v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Typ = 'VehicleSale_Retail' 
					AND v.SoldTS::date <= _as_of_date
					AND v.Status = 'VehicleSale_Sold'
			) as SoldRetail,
		 (Select 'X'::text from ads.ext_vehicle_sales v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND  v.Typ = 'VehicleSale_Wholesale' 
					AND v.SoldTS::date <= _as_of_date
					AND v.Status = 'VehicleSale_Sold'
			) as SoldWholesale,
		 (Select v.SoldTS::date from ads.ext_vehicle_sales v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND v.SoldTS::date <= _as_of_date
					AND v.Status = 'VehicleSale_Sold'
			) as SoldDate,
		 (Select SoldBy from ads.ext_vehicle_sales v
				where vii.VehicleInventoryItemID = v.VehicleInventoryItemID
					AND v.SoldTS::date <= _as_of_date
					AND v.Status = 'VehicleSale_Sold'
			) as SoldBy,  
			null::integer as DaysAvailable,
			null::text AS Status
		FROM ads.ext_vehicle_inventory_items vii
		WHERE vii.LocationID IN (
			SELECT PartyID2
			FROM ads.ext_party_relationships
			WHERE PartyID1 =  _Market_ID 
				AND Typ = 'PartyRelationship_MarketLocations') 
			AND vii.FromTS::date <= _as_of_date 
			AND vii.ThruTS::date >= _from_date
			and vii.ThruTS < now(); -- 05/15/2022 sold section only, this ensures that				

		update cra.statusesX x
		set daysavailable = y.days
		from (
			select VehicleInventoryItemID, (availablehours/24)::integer as days
			from cra.availablehoursadjustedX) y
		where x.VehicleInventoryItemID = y.VehicleInventoryItemID;

-- Calculate the current status value
	UPDATE cra.statusesX
	SET Status = 
		CASE
			WHEN SoldRetail IS NOT NULL THEN 'Sold'
			WHEN SoldNotDelivered is not null then 'Sold'
			WHEN SoldWholesale IS NOT NULL THEN 'Wholesaled'
			WHEN SalesBuffer IS NOT NULL THEN 'Sales Buffer'
			WHEN WholesaleBuffer IS NOT NULL THEN 'Wholesale Buffer'
			-- ok, we have handled all of the sold, wholesales, sales buffer, AND wholesale buffer vehicles.
			-- We DO NOT have to consider these flags any more
			WHEN AvailableNow IS NOT NULL THEN 'Available'
			WHEN (PurchaseInTransit IS NOT NULL) 
				OR (TradeNotAvailable IS NOT NULL) THEN 'In Transit/Trade Buffer'
			WHEN PricingBuffer IS NOT NULL THEN 'Pricing Buffer'
			WHEN Pulled IS NOT NULL THEN
				CASE 
					WHEN BodyWIP IS NOT NULL
						OR MechanicalWIP IS NOT NULL
						OR AppearanceWIP IS NOT NULL THEN 'Recon WIP'
					ELSE 'Recon Buffer'
				END  
			ELSE 'Raw Materials'
		END;                

-- Preliminaries done. 

		truncate cra.temp;
		insert into cra.temp 
		SELECT 
			vii.VehicleInventoryItemID, 
			_Market AS Market, 
			(SELECT Trim(FullName) from ads.ext_Organizations where PartyID = vii.LocationID) AS Location,  
			Trim(vii.StockNumber) AS StockNumber, 
			Trim(vi.VIN) AS VIN, 
			Trim(vi.YearModel) AS Year, 
			Trim(vi.Make) AS Make, 
			Trim(vi.Model) AS Model, 
			Trim(vi.Trim) AS TrimLevel, 
			Trim(vi.ExteriorColor) AS Color, 
			Trim(vi.InteriorColor) AS InteriorColor,
			Trim(vi.BodyStyle) AS BodyStyle, 
			CASE
				WHEN s.SoldDate IS not NULL THEN (
					SELECT Value 
					FROM ads.ext_Vehicle_Item_Mileages vim 
					WHERE vim.VehicleItemID = vii.VehicleItemID
						AND vim.VehicleItemMileageTS::date <= s.SoldDate
					ORDER BY vim.VehicleItemMileageTS DESC
					limit 1)
				ELSE (
					SELECT Value 
					FROM ads.ext_Vehicle_Item_Mileages vim 
					WHERE vim.VehicleItemID = vii.VehicleItemID
					ORDER BY vim.VehicleItemMileageTS DESC
					limit 1)
			END AS Mileage,

			Trim(vi.Engine) AS Engine, 
			Trim(vi.Transmission) AS Transmission,
			Trim(vio.VehicleOptions) AS Options, 
			Trim(vio.AdditionalEquipment) as OptionsExploded, -- actually additional equipment

			vii.FromTS::date as DateAcquired,
			case
				when s.DaysAvailable is null then 0
				else s.DaysAvailable
			end as DaysToSell,
			case
				when s.DaysAvailable is null then 0
				else s.DaysAvailable
			end as DaysOnLot,
			case
				when s.SoldDate is null then _as_of_date - vii.FromTS::date
				else s.SoldDate - vii.FromTS::date
			end as DaysOwned,
			s.SoldDate as DateSold,

			(
				SELECT vpd.Amount
				FROM ads.ext_Vehicle_Pricings vp
				inner join ads.ext_Vehicle_Pricing_Details vpd on vpd.VehiclePricingID = vp.VehiclePricingID 
					and vpd.Typ = 'VehiclePricingDetail_Invoice' 
				WHERE vp.VehicleInventoryItemID = vii.VehicleInventoryItemID
				ORDER BY VehiclePricingTS DESC
				limit 1) as MCRV,	 

			(
				SELECT vpd.Amount
				FROM ads.ext_Vehicle_Pricings vp
				inner join ads.ext_Vehicle_Pricing_Details vpd on vpd.VehiclePricingID = vp.VehiclePricingID 
					and vpd.Typ = 'VehiclePricingDetail_BestPrice' 
				WHERE vp.VehicleInventoryItemID = vii.VehicleInventoryItemID
				ORDER BY VehiclePricingTS DESC
				limit 1) as Price,	   

			Coalesce(glt.TotalCost, 0) AS Net,	   
			Coalesce(glt.SoldAmount, 0) as SoldAmount,
			Coalesce(glt.SoldAmount, 0) - Coalesce(glt.TotalCost, 0) as Gross,   	   	
			case
				when s.SoldRetail = 'X' then 'VehicleSale_Retail'
				when s.SoldWholesale = 'X' then 'VehicleSale_Wholesale'
				else null
			end as Disposition,    
			s.SoldBy as SoldBy,
			(
				SELECT p.FullName 
				FROM ads.ext_people p 
				WHERE p.PartyID = ve.VehicleEvaluatorID 
				 AND p.ThruTS IS NULL) AS AppraisedBy, 

			s.Status as SalesStatus,
			(
				SELECT td.Description 
				FROM ads.ext_Typ_Descriptions td 
				WHERE td.typ = ve.Typ AND td.ThruTS IS null) AS Source,
			(
				SELECT td.Description 
				FROM ads.ext_Typ_Descriptions td 
				WHERE td.typ = mmc.VehicleType AND td.ThruTS IS null) AS VehicleType,
			(
				SELECT td.Description 
				FROM ads.ext_Typ_Descriptions td 
				WHERE td.typ = mmc.VehicleSegment AND td.ThruTS IS null) AS Segment,   
			mmc.Luxury AS Luxury,
			mmc.Sport AS Sport,
			mmc.Commercial AS Commercial
			FROM ads.ext_Vehicle_Inventory_Items vii
			INNER JOIN ads.ext_Vehicle_Items vi ON vi.VehicleItemID = vii.VehicleItemID
			--  this inner join restricts query to vehicles of interest
			INNER JOIN cra.statusesX s on vii.VehicleInventoryItemID = s.VehicleInventoryItemID
			inner JOIN ads.ext_Make_Model_Classifications mmc ON  mmc.Make = vi.Make AND mmc.Model = vi.Model	    	      
			LEFT OUTER JOIN ads.ext_gross_log_table glt ON vii.StockNumber = glt.StockNumber 
			LEFT outer JOIN ads.ext_Vehicle_Item_Options vio ON vio.VehicleItemID = vii.VehicleItemID
			LEFT OUTER JOIN ads.ext_Vehicle_Evaluations ve ON ve.VehicleInventoryItemID = vii.VehicleInventoryItemID;

			
		truncate cra.five_years_2;
		insert into cra.five_years_2 
		SELECT
			vii.*,
			-- without the top 1 the following generates more than one record 
		 (
			SELECT gl.Description 
			FROM ads.ext_Glump_Limits gl 
			WHERE gl.ShortVehicleTyp = vii.VehicleType 
				AND (vii.Price >= gl.LowerLimit 
				AND vii.Price <= gl.UpperLimit)
			limit 1) AS Glump,  

			case
				when vii.DateSold IS NULL then NULL
				WHEN vii.DaysOnLot IS NULL THEN null
				when (vii.daysonlot/7.01)::integer > 8 then 8
				else (vii.DaysOnLot / 7.01)::integer

			end as "SellingWeek",    
			case
				when (vii.Mileage = 0) or (Length(Trim(vii.Year)) <> 4) or (position('.' in vii.Year) <> 0) or (position('C' in vii.Year) <> 0) or (position('/' in vii.Year) <> 0) or ((vii.Year < '1901') OR (vii.Year > '2050')) then Null
				when 
					mileage /
						case
							when round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
							else round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
						end <= 4000 then 'Freaky Low'
				when 
					mileage /
						case
							when round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
							else round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
						end <= 8000 then 'Very Low'
				when 
					mileage /
						case
							when round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
							else round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
						end <= 12000 then 'Low'
				when 
					mileage /
						case
							when round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
							else round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
						end <= 15000 then 'Average'
				when 
					mileage /
						case
							when round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
							else round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
						end <= 18000 then 'High'
				when 
					mileage /
						case
							when round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
							else round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
						end <= 22000 then 'Very High'
				when 
					mileage /
						case
							when round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
							else round((_as_of_date - to_date('09'||'01'||(year::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
						end <= 999999 then 'Freaky High'
				else
					'Unknown'																		
			end as mileagecategory,						
			case
				when DateSold = '1900-01-01' then null
				when vii.Disposition = 'VehicleSale_Retail' and vii.DaysOnLot BETWEEN  0 and 30 then 'Fresh'
				when vii.Disposition = 'VehicleSale_Retail' and vii.DaysOnLot > 30 then 'Aged'
				when vii.Disposition = 'VehicleSale_Wholesale' and vii.DaysOnLot is null then 'Pure W/S'
				when vii.Disposition = 'VehicleSale_Wholesale' and vii.DaysOnLot is not null then 'Aged W/S'
					else null
			end as "SalesCategory",
			case
				when vii.Price = 0 then NULL
				WHEN vii.Price IS NULL THEN NULL
				when vii.Price >     0 and vii.Price <=  6000 then  0
				when vii.Price >  6000 and vii.Price <=  8000 then  1
				when vii.Price >  8000 and vii.Price <= 10000 then  2
				when vii.Price > 10000 and vii.Price <= 12000 then  3
				when vii.Price > 12000 and vii.Price <= 14000 then  4
				when vii.Price > 14000 and vii.Price <= 16000 then  5
				when vii.Price > 16000 and vii.Price <= 18000 then  6
				when vii.Price > 18000 and vii.Price <= 20000 then  7
				when vii.Price > 20000 and vii.Price <= 22000 then 8
				when vii.Price > 22000 and vii.Price <= 24000 then 9
				when vii.Price > 24000 and vii.Price <= 26000 then 10
				when vii.Price > 26000 and vii.Price <= 28000 then 11
				when vii.Price > 28000 and vii.Price <= 30000 then 12
				when vii.Price > 30000 and vii.Price <= 32000 then 13
				when vii.Price > 32000 and vii.Price <= 34000 then 14
				when vii.Price > 34000 and vii.Price <= 36000 then 15
				when vii.Price > 36000 and vii.Price <= 38000 then 16
				when vii.Price > 38000 and vii.Price <= 40000 then 17
				else
					18
			end as "PriceBand",
			case
				when vii.Price = 0 then NULL
				WHEN vii.Price IS NULL THEN NULL
				when vii.Price >     0 and vii.Price <=  6000 then '0-6k'
				when vii.Price >  6000 and vii.Price <=  8000 then '6-8k'
				when vii.Price >  8000 and vii.Price <= 10000 then '8-10k'
				when vii.Price > 10000 and vii.Price <= 12000 then '10-12k'
				when vii.Price > 12000 and vii.Price <= 14000 then '12-14k'
				when vii.Price > 14000 and vii.Price <= 16000 then '14-16k'
				when vii.Price > 16000 and vii.Price <= 18000 then '16-18k'
				when vii.Price > 18000 and vii.Price <= 20000 then '18-20k'
				when vii.Price > 20000 and vii.Price <= 22000 then '20-22k'
				when vii.Price > 22000 and vii.Price <= 24000 then '22-24k'
				when vii.Price > 24000 and vii.Price <= 26000 then '24-26k'
				when vii.Price > 26000 and vii.Price <= 28000 then '26-28k'
				when vii.Price > 28000 and vii.Price <= 30000 then '28-30k'
				when vii.Price > 30000 and vii.Price <= 32000 then '20-32k'
				when vii.Price > 32000 and vii.Price <= 34000 then '32-24k'
				when vii.Price > 34000 and vii.Price <= 36000 then '34-36k'
				when vii.Price > 36000 and vii.Price <= 38000 then '36-38k'
				when vii.Price > 38000 and vii.Price <= 40000 then '38-40k'
				else
					'40k+'
			end as "PriceBandDesc"
			FROM cra.temp vii;

			update cra.five_years_2
			set price_bands_5k = 
				case
					when Price = 0 then NULL
					WHEN Price IS NULL THEN NULL
					when Price >     0 and Price <= 10000 then '0-10k'
					when Price > 10000 and Price <= 15000 then '10-15k'
					when Price > 15000 and Price <= 20000 then '15-20k'
					when Price > 20000 and Price <= 25000 then '20-25k'
					when Price > 25000 and Price <= 30000 then '25-30k'
					when Price > 30000 and Price <= 35000 then '30-35k'
					when Price > 35000 and Price <= 40000 then '35-40k'
					when Price > 40000 and Price <= 45000 then '40-45k'
					when Price > 45000 and Price <= 50000 then '45-50k'
					when Price > 50000 and Price <= 55000 then '50-55k'
					when Price > 55000 and Price <= 60000 then '55-60k'
					else '60k+'
			end;			

			update cra.five_years_2
			set price_bands_10k = 
				case
					when Price = 0 then NULL
					WHEN Price IS NULL THEN NULL
					when Price >     0 and Price <= 10000 then '0-10k'
					when Price > 10000 and Price <= 20000 then '10-20k'
					when Price > 20000 and Price <= 30000 then '20-30k'
					when Price > 30000 and Price <= 40000 then '30-40k'
					when Price > 40000 and Price <= 50000 then '40-50k'
					when Price > 50000 and Price <= 60000 then '50-60k'
					when Price > 60000 and Price <= 70000 then '60-70k'
					when Price > 70000 and Price <= 80000 then '70-80k'
					else '80k+'
			end;		

			update cra.five_years_2
			set vehicle_type = 'SUV'
			where vehicle_type = 'crossover';

			-- if date_sold = sunday, make it saturday
			update cra.five_years_2 x
			set date_sold = date_sold - 1
			from (
				select vehicle_inventory_item_id
				from cra.five_years_2 a
				join dds.dim_date b on a.date_sold = b.the_date
					and b.day_name = 'sunday') y
			where x.vehicle_inventory_item_id = y.vehicle_inventory_item_id;
						
end $$;
--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
select count(*) from cra.five_years_2;

-- ALTER TABLE cra.five_years_2 ADD PRIMARY KEY (vehicle_inventory_item_id);
-- create index on cra.five_years_2(vin);
-- create index on cra.five_years_2(make);
-- create index on cra.five_years_2(model);
-- create index on cra.five_years_2(chr_make);
-- create index on cra.five_years_2(chr_model);
-- create index on cra.five_years_2(vehicle_type);
-- create index on cra.five_years_2(segment);
-- create index on cra.five_years_2(shape);
-- create index on cra.five_years_2(size);
-- create index on cra.five_years_2(vehicle_inventory_item_id);
-- create index on cra.five_years_2(stock_number);


-----------------------------------------------------------------------------------------
--< get rid of bad vins from cra.five_years
-- first, check inpmast by stock_number for a "better" vin
-----------------------------------------------------------------------------------------
-- on 22 of them
select distinct vin, length(vin)
from cra.five_years_2 a
where length(vin) <> 17
 or not (select cra.vin_check_sum(coalesce(a.vin, 'xxx')))

-- these are the "bad" vins, less than 17 characters or fails check digit
drop table if exists wtf;
create temp table wtf as		 
	select stock_number
	from cra.five_years_2
	where vin in (
	 select distinct vin
	 from cra.five_years_2 a
	 where length(vin) <> 17
		 or not (select cra.vin_check_sum(coalesce(a.vin, 'xxx'))))

		 
drop table if exists wtf1;
create temp table wtf1 as		 
select distinct a.inpmast_vin, a.inpmast_stock_number, a.make, a.model, a.year
from arkona.xfm_inpmast a
join wtf b on a.inpmast_stock_number = b.stock_number

-- we can fix the bad vins using stocknumber to get the correct vin from inpmast
select inpmast_stock_number, inpmast_vin, length(inpmast_vin),  (select cra.vin_check_sum(coalesce(inpmast_vin, 'xxx'))), b.vin, b.stock_number
from wtf1 a
left join cra.five_years_2 b on a.inpmast_stock_number = b.stock_number

update cra.five_years_2 x
set vin = y.inpmast_vin
from (
	select inpmast_stock_number, inpmast_vin, length(inpmast_vin),  (select cra.vin_check_sum(coalesce(inpmast_vin, 'xxx'))), b.vin, b.stock_number
	from wtf1 a
	left join cra.five_years_2 b on a.inpmast_stock_number = b.stock_number) y
where x.stock_number = y.inpmast_stock_number	

and get rid of the bad ones
delete from cra.five_years_2 where length(vin) <> 17

-----------------------------------------------------------------------------------------
--/> get rid of bad vins from cra.five_years_2
-----------------------------------------------------------------------------------------



-------------------------------------------------------------------------------------------------
--< ok thought about going full chrome table with lots of decoded attributes
-- just add fields chr_make, chr_model to the five years table
-------------------------------------------------------------------------------------------------
BUT
then i get into multiple style counts, who knows what else
what do i really need for the immediate project
makes and models so that i can use veh.make_model_classifications
so
just add fields chr_make, chr_model to the five years table

alter table cra.five_years_2
add column chr_source citext,
add column chr_make citext,
add column chr_model citext;

update cra.five_years_2 x
set chr_make = y.make,
    chr_model = y.model,
    chr_source = y.source
from (
  select distinct a.vin, c->'division'->>'_value_1' as make,
  c->'model'->>'_value_1' as model,
  b.response->'vinDescription'->>'source' as source
  from cra.five_years_2 a
  join chr.build_data_describe_vehicle b on a.vin = b.vin
  join jsonb_array_elements(b.response->'style') as c on true) y
where x.vin = y.vin    

update cra.five_years_2 x
set chr_make = y.make,
    chr_model = y.model,
    chr_source = y.source
from (
  select distinct a.vin, c->'division'->>'$value' as make,
  c->'model'->>'$value' as model,
  b.response->'vinDescription'->'attributes'->>'source' as source
  from cra.five_years_2 a
  join chr.describe_vehicle b on a.vin = b.vin
  join jsonb_array_elements(b.response->'style') as c on true) y
where x.vin = y.vin  
  and (chr_make is null or chr_model is null)

-------------------------------------------------------------------------------------------------
--/> ok thought about going full chrome table with lots of decoded attributes
-- just add fields chr_make, chr_model to the five years table
-------------------------------------------------------------------------------------------------

-- fuck me, 829 where stock_number matches and vin does not
select a.stock_number, a.vin, b.inpmast_stock_number, b.inpmast_vin
from cra.five_years_2 a
join arkona.ext_inpmast b on a.stock_number = b.inpmast_stock_number
  and a.vin <> b.inpmast_vin
order by a.stock_number  

select * 
from arkona.xfm_inpmast
where inpmast_stock_number = '30177XXB'
