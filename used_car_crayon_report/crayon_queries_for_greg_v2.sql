﻿-----------------------------------------------------------
--< sales
-----------------------------------------------------------

-- last 30 days of sales by glump (5k pricebands)
select 
  case
    when vehicle_type in ('SUV','Crossover') then 'SUV'
    else vehicle_type
  end, segment, price_bands_5k, count(*)
-- select * 
from cra.temp1 a
where date_sold between current_date - 30 and current_date - 1
  and disposition = 'VehicleSale_Retail'
group by 
  case
    when vehicle_type in ('SUV','Crossover') then 'SUV'
    else vehicle_type
  end, segment, price_bands_5k
order by count(*) Desc;

-- last 30 days of sales by type/segment
select 
  case
    when vehicle_type in ('SUV','Crossover') then 'SUV'
    
    else vehicle_type
  end, segment, count(*)
-- select * 
from cra.temp1 a
where date_sold between current_date - 30 and current_date - 1
  and disposition = 'VehicleSale_Retail'
group by 
  case
    when vehicle_type in ('SUV','Crossover') then 'SUV'
    else vehicle_type
  end, segment
order by count(*) Desc;


-- last 30 days of sales by 5k pricebands
select price_bands_5k, count(*)
-- select * 
from cra.temp1 a
where date_sold between current_date - 30 and current_date - 1
  and disposition = 'VehicleSale_Retail'
group by price_bands_5k
order by count(*) Desc;

-----------------------------------------------------------
--/> sales
-----------------------------------------------------------

--------------------------------------------------------------------------
--< estimated retail price
--------------------------------------------------------------------------

-- vehicles currently not priced
select stock_number, vin, model_year, make, model, trim_level, body_style, a.mileage, 
	mileage_category, vehicle_type, segment, days_owned, sales_status,
  b.desirability, 
  (b.reconmechanicalamount+b.reconbodyamount+reconappearanceamount+recontireamount+reconglassamount+reconinspectionamount)::integer as eval_recon,
  b.amountshown, c.amount::integer as eval_acv, d.amount::integer as eval_finished, f.amount as manheim,
  e.averagebase, e.regionaladjustment, e.averagemileageadjustment, e.addsdeductstotal
from cra.temp1 a
left join ads.ext_vehicle_evaluations b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
left join ads.ext_vehicle_price_components c on a.vehicle_inventory_item_id = c.vehicleinventoryitemid
  and c.typ = 'VehiclePriceComponent_ACV'
left join ads.ext_vehicle_price_components d on a.vehicle_inventory_item_id = d.vehicleinventoryitemid
  and d.typ = 'VehiclePriceComponent_Finished'
left join ads.ext_black_book_values e on b.vehicleevaluationid = e.tablekey
  and e.basistable = 'VehicleEvaluations'  
left join ads.ext_vehicle_third_party_values f on a.vehicle_inventory_item_id = f.vehicleinventoryitemid
  and f.basistable = 'VehicleEvaluations'  
where a.sales_status = 'Raw Materials' --not in ('Sold' ,'Wholesaled','In Transit/Trade Buffer')
  and price is null
order by days_owned desc  


-- just to see, same values for vehicles sold in the last 30 days
select stock_number, vin, model_year, make, model, trim_level, body_style, a.mileage, 
	mileage_category, vehicle_type, segment, days_owned, sales_status,
  b.desirability, 
  (b.reconmechanicalamount+b.reconbodyamount+reconappearanceamount+recontireamount+reconglassamount+reconinspectionamount)::integer as eval_recon,
  b.amountshown, c.amount::integer as eval_acv, d.amount::integer as eval_finished, a.price::integer, (d.amount + .1*d.amount)::integer, a.price - (d.amount + .1*d.amount)::integer--f.amount as manheim,  a.pric
--   e.averagebase, e.regionaladjustment, e.averagemileageadjustment, e.addsdeductstotal, a.price::integer
from cra.temp1 a
left join ads.ext_vehicle_evaluations b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
left join ads.ext_vehicle_price_components c on a.vehicle_inventory_item_id = c.vehicleinventoryitemid
  and c.typ = 'VehiclePriceComponent_ACV'
  and c.basistable = 'VehicleEvaluations'
left join ads.ext_vehicle_price_components d on a.vehicle_inventory_item_id = d.vehicleinventoryitemid
  and d.typ = 'VehiclePriceComponent_Finished'
  and d.basistable = 'VehicleEvaluations'  
left join ads.ext_black_book_values e on b.vehicleevaluationid = e.tablekey
  and e.basistable = 'VehicleEvaluations'  
left join ads.ext_vehicle_third_party_values f on a.vehicle_inventory_item_id = f.vehicleinventoryitemid
  and f.basistable = 'VehicleEvaluations'  
where date_sold between current_date - 30 and current_date - 1
  and disposition = 'VehicleSale_Retail'
order by a.price - (d.amount + .1*d.amount)::integer desc   
 

--------------------------------------------------------------------------
--/> estimated retail price
--------------------------------------------------------------------------