﻿-- https://tapoueh.org/manual-post/2014/02/postgresql-histogram/
-- https://tapoueh.org/blog/2014/02/postgresql-aggregates-and-histograms/
-- check this one out

select width_bucket(price::integer, 40000, 50000, 10), count(*)
from cra.five_years
where sales_status = 'sold'
  and vehicle_type = 'pickup'
  and segment = 'large'
  and coalesce(price, 0) between 40000 and 49999
group by 1
order by 1;


 select width_bucket( 10, 0, 10, 10);


this query returns an actual histogram of the distribution of prices of large pickups sold over the last 5 years with a sale price between 40,000 and 49,999 into 10 buckets
 with 
	drb_stats as (
    select min(price::integer) as min, max(price::integer) as max
		from cra.five_years
		where sales_status = 'sold'
			and vehicle_type = 'pickup'
			and segment = 'large'
			and coalesce(price, 0) between 40000 and 50000),
	histogram as (
		select width_bucket(price, min, max, 10) as bucket,
			int4range(min(price::integer), max(price::integer), '[]') as range,
			count(*) as freq
		from (
			select price
			from cra.five_years
			where sales_status = 'sold'
				and vehicle_type = 'pickup'
				and segment = 'large'
				and coalesce(price, 0) between 40000 and 49999) a, drb_stats
		 group by bucket
		 order by bucket)
 select bucket, range, freq,
	repeat('*', (freq::float / max(freq) over() * 30)::int) as bar
from histogram;

select * from cra.five_years where price between 43100 and 43400

this query returns an actual histogram of the distribution of prices of large pickups sold over the last 5 years with a sale price between 40,000 and 49,999 into 10 buckets


-- *** whats the deal with ranges ending in 1, eg [8190,9001)

-- 5 years all sales < 30k
 with 
	drb_stats as ( 
    select min(price::integer) as min, max(price::integer) as max
		from cra.five_years
		where sales_status = 'sold'
			and coalesce(price, 0) between 1 and 30000),
	histogram as (
		select width_bucket(price, min, max, 30) as bucket,
			int4range(min(price::integer), max(price::integer), '[]') as range,
			count(*) as freq
		from (
			select price
			from cra.five_years
			where sales_status = 'sold'
			  and price is not null
			  and right(stock_number, 1) <> 'L'
				and coalesce(price, 0) between 0 and 29999) a, drb_stats
		group by bucket
		order by bucket)
 select bucket, range, freq,
	repeat('*', (freq::float / max(freq) over() * 50)::int) as bar
from histogram;

-- past year all sales < 30k
 with 
	drb_stats as ( 
    select min(price::integer) as min, max(price::integer) as max
		from cra.five_years
		where sales_status = 'sold'
			and coalesce(price, 0) between 1 and 30000),
	histogram as (
		select width_bucket(price, min, max, 30) as bucket,
			int4range(min(price::integer), max(price::integer), '[]') as range,
			count(*) as freq
		from (
			select price
			from cra.temp1
			where sales_status = 'sold'
			  and price is not null
			  and right(stock_number, 1) <> 'L'
				and coalesce(price, 0) between 0 and 29999) a, drb_stats
		group by bucket
		order by bucket)
 select bucket, range, freq,
	repeat('*', (freq::float / max(freq) over() * 50)::int) as bar
from histogram;