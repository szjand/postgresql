﻿select min(date_sold), max(date_sold) from cra.five_years

--< 5 years ------------------------------------------------------------------
-- shape/size
select shape, size, count(*), row_number() over(order by count(*) desc) as seq
from cra.five_years a
where disposition = 'VehicleSale_Retail'
  and shape is not null
group by shape, size
order by count(*) desc
limit 5

-- 10k pb
select price_bands_10k, count(*), row_number() over(order by count(*) desc) as seq
from cra.five_years a
where disposition = 'VehicleSale_Retail'
  and shape is not null
group by price_bands_10k
order by count(*) desc
limit 5

-- 5k pb
select price_bands_5k, count(*), row_number() over(order by count(*) desc) as seq
from cra.five_years a
where disposition = 'VehicleSale_Retail'
  and shape is not null
group by price_bands_5k
order by count(*) desc
limit 5

-- tool pb
select price_band_desc, count(*), row_number() over(order by count(*) desc) as seq
from cra.five_years a
where disposition = 'VehicleSale_Retail'
  and shape is not null
group by price_band_desc
order by count(*) desc
limit 5

--/> 5 years ------------------------------------------------------------------

--< past year ------------------------------------------------------------------
-- shape/size
select shape, size, count(*), row_number() over(order by count(*) desc) as seq
from cra.five_years a
where disposition = 'VehicleSale_Retail'
  and shape is not null
  and date_sold > '05/19/2021'
group by shape, size
order by count(*) desc
limit 5

-- 10k pb
select price_bands_10k, count(*), row_number() over(order by count(*) desc) as seq
from cra.five_years a
where disposition = 'VehicleSale_Retail'
  and shape is not null
  and date_sold > '05/19/2021'  
group by price_bands_10k
order by count(*) desc
limit 5

-- 5k pb
select price_bands_5k, count(*), row_number() over(order by count(*) desc) as seq
from cra.five_years a
where disposition = 'VehicleSale_Retail'
  and shape is not null
  and date_sold > '05/19/2021'  
group by price_bands_5k
order by count(*) desc
limit 5

-- tool pb
select price_band_desc, count(*), row_number() over(order by count(*) desc) as seq
from cra.five_years a
where disposition = 'VehicleSale_Retail'
  and shape is not null
  and date_sold > '05/19/2021'  
group by price_band_desc
order by count(*) desc
limit 5

--/> past year ------------------------------------------------------------------

-- shape size
select shape, size, count(*), row_number() over(order by count(*) desc) as seq
from cra.five_years a
where disposition = 'VehicleSale_Retail'
  and shape is not null
  and price_band is not null
group by shape, size
order by count(*) desc
limit 5

-- used this to generate the accumulated spreadsheeet
select price_band_desc as tool_pb, the_count, round(1.0 * the_count/total, 2) as proportion 
from (
	select price_band_desc, count(*) as the_count, b.total 
	from cra.five_years a
	left join (
		select count(*) as total
		from cra.five_years
		where disposition = 'VehicleSale_Retail'
			and shape is not null
			and price_band_desc is not null
	    and date_sold > '05/19/2021') b on true
	where a.disposition = 'VehicleSale_Retail'
		and a.shape is not null -- exclude those not classified
		and a.price_band_desc is not null 
	  and a.date_sold > '05/19/2021'
	group by a.price_band_desc, b.total) x
order by the_count desc
limit 10



-- used this to generate the accumulated spreadsheeet
-- and now for shape/size/pb
select shape, size, price_bands_10k as pb_10k, the_count, round(1.0 * the_count/total, 3) as proportion, total
from (
	select shape, size, price_bands_10k, count(*) as the_count, b.total 
	from cra.five_years a
	left join (
		select count(*) as total
		from cra.five_years
		where disposition = 'VehicleSale_Retail'
			and shape is not null
			and price_bands_10k is not null
	    and date_sold > '05/19/2017') b on true
	where a.disposition = 'VehicleSale_Retail'
		and a.shape is not null -- exclude those not classified
		and a.price_bands_10k is not null 
	  and a.date_sold > '05/19/2017'
	group by shape, size, price_bands_10k, b.total) x
order by the_count desc
limit 5

of the 152 missing a price, 142 have a sold amount which can be used to generate a price band
select count(*)
select count(*) over (partition by right(stock_number, 1)), *
from cra.five_years
where disposition = 'VehicleSale_Retail'
  and price_bands_10k is null

select *
from cra.five_years
where disposition = 'VehicleSale_Retail'
  and price_bands_10k is null
  and sold_amount <>  0

!!!
need to regenerate five_years
include all the changes here (including this new one of adding pb when price is null)
all the changes  in make_model_classificaiton.sql

move all those "changes" into the  crayon_data_5_years.sql file

the deal is, as i keep updating the fucking table, i dont know where all the changes are so it makes 
it hard if not impossible to recreate current state

!!!

06/04/22 so, lets give it a shot, because, actually, there is some additional clean up i would like to do
or should i just use greg.uc_daily_snapshot_beta_, hmm

-- 18 vehicles missing from greg.uc_daily_snapshot_beta_1, including some current inventory
select a.stock_number, a.vin, date_sold
from cra.five_years a
left join greg.uc_daily_snapshot_beta_1 b on a.stock_number = b.stock_number
  and a.vin = b.vin
where b.vin is null  

select * from greg.uc_daily_snapshot_beta_1
where stock_number = 'G43527C'

H15523P,KL4CJGSM4JB657649  vin conflict tool vs dt

select * 
from chr.describe_vehicle
where vin in ('KL4CJGSM4JB657649','KL4CJGSM4JB657645')

update greg.uc_daily_snapshot_beta_1
SET vin = 'KL4CJGSM4JB657645'
WHERE vin = 'KL4CJGSM4JB657649'

oh, shit, there are going to be conflicts because i updated a lot of vins in cra.five_years using inpmast
and those fixes did not go into greg.uc_daily_snapshot_beta_1


select a.stock_number, a.vin, date_sold, b.vin, c.inpmast_vin
from cra.five_years a
join (
  select distinct stock_number, vin 
  from greg.uc_daily_snapshot_beta_1) b on a.stock_number = b.stock_number
  and a.vin <>  b.vin
left join arkona.ext_inpmast c on a.stock_number = c.inpmast_stock_number 