﻿
05/19/22
was hoping to determine outlet sales from tool data
tried going through the consignment table, but that failed right away,
G43903PA sold at the outlet, but it was never consigned
outlet inventory is currently being determined by vii.locationid which is outlet, as opposed
to vii.owninglocationid which is always gm or honda
crayon report data does not expose outlet sales
currently, i think the only way is thru vision sold board, which is based on the sales consultant, 
which is mostly good enough, but occasionally, olderbak will sell a vehicle out of GM or Honda

-----------------------------------------------------------------------
--< 06/04/22 maybe any vehicle that has outlet location in history?
-----------------------------------------------------------------------



-----------------------------------------------------------------------
--/> 06/04/22 maybe any vehilce that has outlet location in history?
-----------------------------------------------------------------------
--< intra market wholesales
select b.fullname, count(*)
from ads.ext_Vehicle_sales a
join ads.ext_organizations b on a.soldto = b.partyid 
  and b.fullname in ('GF-Honda Cartiva','GF-Rydells','GF-Rydell Auto Outlet')
group by b.fullname  

select b.fullname, a.vehicleinventoryitemid
from ads.ext_Vehicle_sales a
join ads.ext_organizations b on a.soldto = b.partyid 
  and b.fullname in ('GF-Honda Cartiva','GF-Rydells','GF-Rydell Auto Outlet')
--/> intra market wholesales

-- location ids
GF-Rydells 							B6183892-C79D-4489-A58C-B526DF948B06
GF-Honda Cartiva				4CD8E72C-DC39-4544-B303-954C99176671
GF-Rydell Auto Outlet 	1B6768C6-03B0-4195-BA3B-767C0CAC40A6


select fullname, partyid
from ads.ext_organizations 
where fullname in ('GF-Honda Cartiva','GF-Rydells','GF-Rydell Auto Outlet')

  
  select * from cra.temp1 where date_sold = '04/30/2022'

select * from cra.temp1 where stock_number = 'G43865B'

select * from greg.uc_daily_snapshot_beta_1 where stock_number = 'G43865B' and the_date = sale_date

select * from ads.ext_vehicle_sales where vehicleinventoryitemid = '03b76ad9-5c02-471f-8377-a55a3d5390a7'

select * from ads.ext_vehicle_inventory_items where vehicleinventoryitemid = '03b76ad9-5c02-471f-8377-a55a3d5390a7'

select *
from ads.ext_vehicle_inventory_items
where thruts::date = '04/30/2022'

SELECT * 
FROM ads.ext_vehicle_inventory_consignments
where vehicleinventoryitemid = '9f3e5929-caa0-4128-b651-4144c07ee9c7'

select a.vehicleinventoryitemid, b.stocknumber, a.soldts::date, b.thruts::date, c.*
from ads.ext_vehicle_sales a
join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
left join ads.ext_vehicle_inventory_consignments c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
  and a.soldts::date = c.thruts::date
where a.soldts::date > '04/30/2022' -- and b.stocknumber = 'G43903PA'
order by a.soldts::date

stocknumber G43903PA sold on 5/4/22 at outlet, was never consigned

select * from ads.ext_vehicle_inventory_items where locationid = '1B6768C6-03B0-4195-BA3B-767C0CAC40A6' and thruts > now()