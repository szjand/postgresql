﻿ ------------------------------------------------------------------------
--< top_10_comparisions report
------------------------------------------------------------------------
-- the top ten from 1 year and 5 year by 10k price bands
select * from (
	select * 
	from (
		select vehicle_type as shape, segment as size, price_bands_10k, count(*), row_number() over(order by count(*) desc) as seq_b
		from cra.five_years a
		where disposition = 'VehicleSale_Retail'
		group by vehicle_type, segment, price_bands_10k
		having count(*) > 12
		order by count(*) desc) x
	where seq_b < 11) a
full outer  join (
	select * 
	from (
		select vehicle_type as shape, segment as size, price_bands_10k, count(*), row_number() over(order by count(*) desc) as seq_b
		from cra.temp1 a
		where disposition = 'VehicleSale_Retail'
		group by vehicle_type, segment, price_bands_10k
		having count(*) > 12
		order by count(*) desc) x
	where seq_b < 11) b on a.price_bands_10k = b.price_bands_10k
		and a.shape = b.shape
		and a.size = b.size
order by coalesce(a.seq_b, b.seq_b)

-- the top ten from 1 year and 5 year by 5k price bands
select * from (
	select * 
	from (
		select vehicle_type as shape, segment as size, price_bands_5k, count(*), row_number() over(order by count(*) desc) as seq_b
		from cra.five_years a
		where disposition = 'VehicleSale_Retail'
		group by vehicle_type, segment, price_bands_5k
		having count(*) > 12
		order by count(*) desc) x
	where seq_b < 11) a
full outer  join (
	select * 
	from (
		select vehicle_type as shape, segment as size, price_bands_5k, count(*), row_number() over(order by count(*) desc) as seq_b
		from cra.temp1 a
		where disposition = 'VehicleSale_Retail'
		group by vehicle_type, segment, price_bands_5k
		having count(*) > 12
		order by count(*) desc) x
	where seq_b < 11) b on a.price_bands_5k = b.price_bands_5k
		and a.shape = b.shape
		and a.size = b.size
order by coalesce(a.seq_b, b.seq_b)

-- the top ten from 1 year and 5 year by 2k price bands
select * from (
	select * 
	from (
		select vehicle_type as shape, segment as size, price_band_desc, count(*), row_number() over(order by count(*) desc) as seq_b
		from cra.five_years a
		where disposition = 'VehicleSale_Retail'
		group by vehicle_type, segment, price_band_desc
		having count(*) > 12
		order by count(*) desc) x
	where seq_b < 11) a
full outer  join (
	select * 
	from (
		select vehicle_type as shape, segment as size, price_band_desc, count(*), row_number() over(order by count(*) desc) as seq_b
		from cra.temp1 a
		where disposition = 'VehicleSale_Retail'
		group by vehicle_type, segment, price_band_desc
		having count(*) > 12
		order by count(*) desc) x
	where seq_b < 11) b on a.price_band_desc = b.price_band_desc
		and a.shape = b.shape
		and a.size = b.size
order by coalesce(a.seq_b, b.seq_b)
	
------------------------------------------------------------------------
--/> top_10_comparisions report
------------------------------------------------------------------------		

------------------------------------------------------------------------
--< 1_year_vs_5_years_sales report
------------------------------------------------------------------------	
-- 5 years 10k PB
select * from (
		select vehicle_type as shape, segment as size, price_bands_10k, count(*), row_number() over(order by count(*) desc) as rank
		from cra.five_years a
		where disposition = 'VehicleSale_Retail'
		group by vehicle_type, segment, price_bands_10k
		having count(*) > 60
		order by count(*) desc) x
where rank < 16		
-- 5 years 5k PB
select * from (
		select vehicle_type as shape, segment as size, price_bands_5k, count(*), row_number() over(order by count(*) desc) as rank
		from cra.five_years a
		where disposition = 'VehicleSale_Retail'
		group by vehicle_type, segment, price_bands_5k
		having count(*) > 60
		order by count(*) desc) x
where rank < 16		
-- 5 years 2k PB
		select vehicle_type as shape, segment as size, price_band_desc, count(*), row_number() over(order by count(*) desc) as rank
		from cra.five_years a
		where disposition = 'VehicleSale_Retail'
		group by vehicle_type, segment, price_band_desc
		having count(*) > 60
		order by count(*) desc

-- 1 year 10k PB
		select vehicle_type as shape, segment as size, price_bands_10k, count(*), row_number() over(order by count(*) desc) as rank
		from cra.temp1 a
		where disposition = 'VehicleSale_Retail'
		group by vehicle_type, segment, price_bands_10k
		having count(*) > 12
		order by count(*) desc
-- 1 year 5k PB
		select vehicle_type as shape, segment as size, price_bands_5k, count(*), row_number() over(order by count(*) desc) as rank
		from cra.temp1 a
		where disposition = 'VehicleSale_Retail'
		group by vehicle_type, segment, price_bands_5k
		having count(*) > 12
		order by count(*) desc
-- 1 year 2k PB
		select vehicle_type as shape, segment as size, price_band_desc, count(*), row_number() over(order by count(*) desc) as rank
		from cra.temp1 a
		where disposition = 'VehicleSale_Retail'
		group by vehicle_type, segment, price_band_desc
		having count(*) > 12
		order by count(*) desc




		select vehicle_type as shape, segment as size, price_bands_5k, count(*), row_number() over(order by count(*) desc) as rank
		from cra.five_years a
		where disposition = 'VehicleSale_Retail'
		  and vehicle_type = 'pickup'
		  and segment = 'large'
		group by vehicle_type, segment, price_bands_5k
		order by count(*) desc


select price, date_sold, vehicle_type, segment
from cra.five_years
where price is not null
  and vehicle_type = 'pickup'
  and segment = 'large'
  and price between 20000 and 30000
  and date_sold is not null
order by price desc

		
		select vehicle_type as shape, segment as size, price_bands_10k, count(*), row_number() over(order by count(*) desc) as seq_a
		from cra.temp1 a
		where disposition = 'VehicleSale_Retail'
		group by vehicle_type, segment, price_bands_10k
		having count(*) > 12
		order by count(*) desc		

		select vehicle_type as shape, segment as size, price_bands_5k, count(*), row_number() over(order by count(*) desc) as seq_a
		from cra.five_years a
		where disposition = 'VehicleSale_Retail'
		group by vehicle_type, segment, price_bands_5k
		having count(*) > 60
		order by count(*) desc

		select vehicle_type as shape, segment as size, price_bands_5k, count(*), row_number() over(order by count(*) desc) as seq_a
		from cra.temp1 a
		where disposition = 'VehicleSale_Retail'
		group by vehicle_type, segment, price_bands_5k
		having count(*) > 12
		order by count(*) desc	


		select vehicle_type as shape, segment as size, price_band_desc, count(*), row_number() over(order by count(*) desc) as seq_a
		from cra.temp1 a
		where disposition = 'VehicleSale_Retail'
		group by vehicle_type, segment, price_band_desc
		having count(*) > 12
		order by count(*) desc	
------------------------------------------------------------------------
--/> 1_year_vs_5_years_sales
------------------------------------------------------------------------	

------------------------------------------------------------------------
top 2 10k PB 5 years
Pickup	Large		20-30k	921	1
SUV			Compact	10-20k	909	2

top 2 10k PB past 2 years
Pickup	Large	30-40k		449	1
SUV			Compact	20-30k	341	2

top 2 10k PB past year
Pickup	Large 	30-40k	219	1
SUV 		Compact 20-30k	204	2

top 2 5k PB 5 years
Car			Midsize		0-10k		564	1
Pickup	Large			25-30k	522	2

top 2 5k PB past 2 years
Pickup	Large	35-40k	234	1
Pickup	Large	30-35k	215	2

top 2 5k PB past year
Pickup	Large		35-40k	122	1
SUV			Compact	20-25k	103	2

top 2 tool PB 5 years
Pickup	Large		40k+	423	1
Car			Midsize	0-6k	283	2

top 2 tool PB past 2 years
Pickup	Large	40k+	284	1
SUV			Large	40k+	153	2

top 2 tool PB past year
Pickup	Large	40k+	192	1
SUV			Large	40k+	71	2
------------------------------------------------------------------------	
-- 5 years  group by new classifications: shape/size
-- select vehicle_type as shape, segment as size, price_bands_10k, count(*), row_number() over(order by count(*) desc) as seq
select shape, size, price_bands_10k, count(*), row_number() over(order by count(*) desc) as seq
from cra.five_years a
where disposition = 'VehicleSale_Retail'
  and shape is not null
-- group by vehicle_type, segment, price_bands_10k
group by shape, size, price_bands_10k
having count(*) > 60
order by count(*) desc

select vehicle_type as shape, segment as size, price_bands_5k, count(*), row_number() over(order by count(*) desc) as seq
from cra.five_years a
where disposition = 'VehicleSale_Retail'
group by vehicle_type, segment, price_bands_5k
having count(*) > 60
order by count(*) desc

select vehicle_type as shape, segment as size, price_band_desc, count(*), row_number() over(order by count(*) desc) as seq
from cra.five_years a
where disposition = 'VehicleSale_Retail'
group by vehicle_type, segment, price_band_desc
having count(*) > 60
order by count(*) desc

-- past year
select vehicle_type as shape, segment as size, price_bands_10k, count(*), row_number() over(order by count(*) desc) as seq
from cra.temp1 a
where disposition = 'VehicleSale_Retail'
group by vehicle_type, segment, price_bands_10k
having count(*) > 12
order by count(*) desc

select vehicle_type as shape, segment as size, price_bands_5k, count(*), row_number() over(order by count(*) desc) as seq
from cra.temp1 a
where disposition = 'VehicleSale_Retail'
group by vehicle_type, segment, price_bands_5k
having count(*) > 12
order by count(*) desc 

select vehicle_type as shape, segment as size, price_band_desc, count(*), row_number() over(order by count(*) desc) as seq
from cra.temp1 a
where disposition = 'VehicleSale_Retail'
group by vehicle_type, segment, price_band_desc
having count(*) > 12
order by count(*) desc 

-- 2  years
select vehicle_type as shape, segment as size, price_bands_10k, count(*), row_number() over(order by count(*) desc) as seq
from cra.five_years a
where disposition = 'VehicleSale_Retail'
  and date_sold between '05/20/2020' and '05/19/2022'
group by vehicle_type, segment, price_bands_10k
having count(*) > 12
order by count(*) desc

select vehicle_type as shape, segment as size, price_bands_5k, count(*), row_number() over(order by count(*) desc) as seq
from cra.five_years a
where disposition = 'VehicleSale_Retail'
  and date_sold between '05/20/2020' and '05/19/2022'
group by vehicle_type, segment, price_bands_5k
having count(*) > 12
order by count(*) desc 

select vehicle_type as shape, segment as size, price_band_desc, count(*), row_number() over(order by count(*) desc) as seq
from cra.five_years a
where disposition = 'VehicleSale_Retail'
  and date_sold between '05/20/2020' and '05/19/2022'
group by vehicle_type, segment, price_band_desc
having count(*) > 12
order by count(*) desc 


-------------------------------------------------------------

