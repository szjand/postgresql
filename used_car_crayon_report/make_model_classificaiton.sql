﻿/*
05/24/22
model			tool				vision
equinox	  compact    small
pilot     compact		 midsize

trying to isolate high movers and discovered that one of the high movers, equinox, is
categorized differently in the tool (makemodelclassifications) than in vision (veh.shape_size_classifications)
which, of course, led to a rabbit hole
so, i am going to add attributes shape & size to the 2 tables (cra.temp1, cra.five_years)
but, first of course, i have to resolve the incompatabilities
the most effective way of doing that may be via chrome
*/

-- 490 distinct make/model
select distinct make, model
from cra.five_years

-- all make/model combinations have just a single classification
select make, model from (
select make, model, vehicle_type, segment
from cra.five_years
group by make, model, vehicle_type, segment
) x group by make, model having count(*) > 1


-- 109 where make/model does not match	
select a.*, b.shape, b.size
from (
	select make, model, vehicle_type, segment, count(*)
	from cra.five_years
	group by make, model, vehicle_type, segment) a
left join veh.shape_size_classifications b on a.make = b.make and a.model = b.model
where b.shape is null 
order by a.make, a.model

-- the first step with chrome is to get the vins from cra.five_years of the makes/models that
-- aren't in vision

drop table if exists t1 cascade;
create temp table t1 as
select aa.model_year, aa.vin, bb.*
from cra.five_years aa
join (
	select a.*
	from (
		select make, model, vehicle_type, segment, count(*)
		from cra.five_years
		group by make, model, vehicle_type, segment) a
	left join veh.shape_size_classifications b on a.make = b.make and a.model = b.model
	where b.shape is null ) bb on aa.make = bb.make
	  and aa.model = bb.model
order by bb.make, bb.model;


select a.*
from t1 a
join chr.build_data_describe_vehicle b on a.vin = b.vin

-----------------------------------------------------------------------
-- describe vehicles
-----------------------------------------------------------------------
select a.*, (r.style ->'attributes'->>'id')::citext as chr_style_id,
  (r.style ->'attributes'->>'modelYear')::integer as chr_model_year,
  (r.style ->'division'->>'$value')::citext as chr_make,
  (r.style ->'model'->>'$value'::citext)::citext as chr_model,
  (r.style ->'attributes'->>'mfrModelCode')::citext as chr_model_code,
  case
    when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as chr_drive,
  case
    when u.cab->>'$value' like 'Crew%' then 'crew' 
    when u.cab->>'$value' like 'Extended%' then 'double'  
    when u.cab->>'$value' like 'Regular%' then 'reg'  
    else 'n/a'   
  end as chr_cab,        
  coalesce(r.style ->'attributes'->>'trim', 'none')::citext as chr_trim,
  coalesce(t.displacement->>'$value', 'n/a') as engine_displacement, 
--   a.color, null::integer as configuration_id,
  case
    when v. bed->>'$value' like '%Bed' then substring(bed->>'$value', 1, position(' ' in bed->>'$value') - 1)
    else 'n/a'
  end as box_size    
-- select a.*, b.style_count
from t1 a
join chr.describe_vehicle c on a.vin = c.vin
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(r.style->'bodyType') with ordinality as v(bed) on true
  and v.ordinality =  1    
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
  and s.engine ? 'installed'
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->'attributes'->>'unit' = 'liters'  
where not exists ( -- exclude vins in build data
  select 1
  from chr.build_data_describe_vehicle
  where vin = a.vin)

-----------------------------------------------------------------------
-- build data
need to eliminate non build data
delete 
select * from chr.build_data_describe_vehicle where coalesce(source, '123') <> 'build'
there are 108 with style_count > 1 !?!?!?
-----------------------------------------------------------------------
select a.vin, (r.style ->>'id')::citext as chr_style_id,
  (r.style->>'modelYear')::integer as chr_model_year,
  (r.style ->'division'->>'_value_1')::citext as chr_make,
  (r.style ->'model'->>'_value_1'::citext)::citext as chr_model,
  (r.style ->>'mfrModelCode')::citext as chr_model_code,
  case
    when r.style ->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as chr_drive,
  case
    when u.cab->>'_value_1' like 'Crew%' then 'crew' 
    when u.cab->>'_value_1' like 'Extended%' then 'double'  
    when u.cab->>'_value_1' like 'Regular%' then 'reg'  
    else 'n/a'   
  end as chr_cab,
  coalesce(r.style ->>'trim', 'none')::citext as chr_trim,
  t->>'_value_1' as engine_displacement,
--    a.color, null::integer as configuration_id,
  case
    when v.bed->>'_value_1' like '%Bed' then substring(bed->>'_value_1', 1, position(' ' in bed->>'_value_1') - 1)
    else 'n/a'
  end as box_size,
  d->>'oemCode' as peg,
  c.source, c.response->'responseStatus'->>'responseCode',
  s->'fuelType'->>'_value_1'
-- select a.vin  
from (
  select distinct vin from cra.five_years) a
join chr.build_data_describe_vehicle c on a.vin = c.vin
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(r.style->'bodyType') with ordinality as v(bed) on true
  and v.ordinality =  1    
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
  and (s.engine->'installed'->>'cause' = 'OptionCodeBuild' or coalesce(s.engine->'installed'->>'cause', 'xxx') = 'VIN')
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->>'unit' = 'liters'  
left join jsonb_array_elements(c.response->'factoryOption') as d(factory_options) on true
  and d.factory_options->'header'->>'_value_1' = 'PREFERRED EQUIPMENT GROUP'
  and d.factory_options->'installed'->>'cause' = 'OptionCodeBuild'

-- build data crayon make/model does not match chrome make/model 
select * from (
select a.*, (r.style ->>'id')::citext as chr_style_id,
  (r.style->>'modelYear')::integer as chr_model_year,
  (r.style ->'division'->>'_value_1')::citext as chr_make,
  (r.style ->'model'->>'_value_1'::citext)::citext as chr_model,
  (r.style ->>'mfrModelCode')::citext as chr_model_code,
  style_count
-- select a.vin  
from (
  select distinct vin, make, model, vehicle_type, segment from cra.five_years_1) a
	join chr.build_data_describe_vehicle c on a.vin = c.vin
	join jsonb_array_elements(c.response->'style') as r(style) on true
	where c.style_count = 1
) x
left join veh.shape_size_classifications y on x.chr_make = y.make and x.chr_model = y.model
where x.make <> chr_make or x.model <> chr_model




drop table if exists chr.build_data_coverage cascade;
create table chr.build_data_coverage (
  oem citext not null,
  division citext not null primary key,
  from_year integer not null,
  thru_year integer not null);
insert into chr.build_data_coverage values
('bmw','bmw',2018,2100),('bmw','mini',2018,2100),('fca','alfa romeo',2015,2100),('fca','chrysler',2004,2100),('fca','dodge',2004,2100),
('fca','fiat',2012,2100),('fca','jeep',2004,2100),('fca','ram',2012,2100),('gm','buick',1997,2100),('gm','cadillac',1997,2100),
('gm','chevrolet',1997,2100),('gm','geo',1997,1997),('gm','gmc',1997,2100),('gm','hummer',2003,2010),('gm','pontiac',1997,2010),
('gm','oldsmobile',1997,2004),('gm','saab',1997,2011),('gm','saturn',1997,2010),('hyundai','hyundai',2009,2100),('hyundai','genesis',2017,2100),
('kia','kia',2007,2100),('mazda','mazda',2004,2100),('nissan','nissan',2001,2100),('nissan','infiniti',2001,2100),('subaru','subaru',2006,2100);
 
select distinct a.*, b.vin  -- 12617, 12615, 12608, 12557
from chr.build_data_coverage a
join cra.five_years b on a.division = b.make
  and b.model_year::integer between a.from_year and a.thru_year
where length(b.vin) = 17  
  and (select cra.vin_check_sum(vin)) -- passes check digit
  and not exists (
    select 1
    from chr.build_data_describe_vehicle
    where vin = b.vin)
order by a.oem, a.division, vin


-- for python build data
select distinct b.vin 
from chr.build_data_coverage a
join cra.five_years b on a.division = b.make
  and b.model_year::integer between a.from_year and a.thru_year
where length(b.vin) = 17  
  and (select cra.vin_check_sum(vin)) -- passes check digit
  and not exists (
    select 1
    from chr.build_data_describe_vehicle
    where vin = b.vin)
limit 1000


select distinct b.vin -- 3611
from cra.five_years b
where length(b.vin) = 17  
  and (select cra.vin_check_sum(vin)) -- passes check digit
  and not exists (
    select 1
    from chr.build_data_describe_vehicle
    where vin = b.vin)
 and not exists (
   select 1
   from chr.describe_vehicle
   where vin = b.vin)
limit 1000   


select distinct a.vin -- 1603 rows where vin does not resolve from black book
from cra.five_years a
join ads.ext_vehicle_items b on a.vin = b.vin
  and not b.vinresolved

-- wow, chrome brings it down to 22 unresolved vins !
-- 13 are less than 17 digits, the remaining 9 fail check digit
select aa.*, bb.vin as build, cc.vin as descr, (select cra.vin_check_sum(coalesce(aa.vin, 'xxx')))
from (
	select distinct a.vin
	from cra.five_years a
	join ads.ext_vehicle_items b on a.vin = b.vin
		and not b.vinresolved) aa
left join chr.build_data_describe_vehicle bb on aa.vin = bb.vin
left join chr.describe_vehicle cc on aa.vin = cc.vin
where bb.vin is null
  and cc.vin is null  
  and length(aa.vin) = 17              

-- ok, so now what am i going to do with all this wonderful chrome data
-- only 26 total vehicles in cra.five_years not in chrome tables
-- 11 where vin is 17 characters
select a.*
from (
select vin from cra.five_years) a
left join (
select vin from chr.build_data_describe_vehicle
union
select vin from chr.describe_vehicle) b on a.vin = b.vin
where b.vin is null 
   and length(a.vin) = 17  

-------------------------------------------------------------------------------------------------
--< ok thought about going full chrome table with lots of decoded attributes
-- just add fields chr_make, chr_model to the five years table
-------------------------------------------------------------------------------------------------
BUT
then i get into multiple style counts, who knows what else
what do i really need for the immediate project
makes and models so that i can use veh.make_model_classifications
so
just add fields chr_make, chr_model to the five years table

alter table cra.five_years
add column chr_source citext,
add column chr_make citext,
add column chr_model citext;

update cra.five_years x
set chr_make = y.make,
    chr_model = y.model,
    chr_source = y.source
from (
  select distinct a.vin, c->'division'->>'_value_1' as make,
  c->'model'->>'_value_1' as model,
  b.response->'vinDescription'->>'source' as source
  from cra.five_years a
  join chr.build_data_describe_vehicle b on a.vin = b.vin
  join jsonb_array_elements(b.response->'style') as c on true) y
where x.vin = y.vin    

update cra.five_years x
set chr_make = y.make,
    chr_model = y.model,
    chr_source = y.source
from (
  select distinct a.vin, c->'division'->>'$value' as make,
  c->'model'->>'$value' as model,
  b.response->'vinDescription'->'attributes'->>'source' as source
  from cra.five_years a
  join chr.describe_vehicle b on a.vin = b.vin
  join jsonb_array_elements(b.response->'style') as c on true) y
where x.vin = y.vin  
  and (chr_make is null or chr_model is null)

-------------------------------------------------------------------------------------------------
--/> ok thought about going full chrome table with lots of decoded attributes
-- just add fields chr_make, chr_model to the five years table
-------------------------------------------------------------------------------------------------


-----------------------------------------------------------------------------------------
--< get rid of bad vins from cra.five_years
-- this appears to be where i diverted to cleaning up bad vins in cra.five_years
-- first first, lets get rid of bad vins from cra.five_years
-----------------------------------------------------------------------------------------
-- on 22 of them
select distinct vin, length(vin)
from cra.five_years a
where length(vin) <> 17
 or not (select cra.vin_check_sum(coalesce(a.vin, 'xxx')))

-- these are the "bad" vins, less than 17 characters or fails check digit
drop table if exists wtf;
create temp table wtf as		 
	select stock_number
	from cra.five_years
	where vin in (
	 select distinct vin
	 from cra.five_years a
	 where length(vin) <> 17
		 or not (select cra.vin_check_sum(coalesce(a.vin, 'xxx'))))

		 
drop table if exists wtf1;
create temp table wtf1 as		 
select distinct a.inpmast_vin, a.inpmast_stock_number, a.make, a.model, a.year
from arkona.xfm_inpmast a
join wtf b on a.inpmast_stock_number = b.stock_number

-- we can fix the bad vins using stocknumber to get the correct vin from inpmast
select inpmast_stock_number, inpmast_vin, length(inpmast_vin),  (select cra.vin_check_sum(coalesce(inpmast_vin, 'xxx'))), b.vin, b.stock_number
from wtf1 a
left join cra.five_years b on a.inpmast_stock_number = b.stock_number

update cra.five_years x
set vin = y.inpmast_vin
from (
	select inpmast_stock_number, inpmast_vin, length(inpmast_vin),  (select cra.vin_check_sum(coalesce(inpmast_vin, 'xxx'))), b.vin, b.stock_number
	from wtf1 a
	left join cra.five_years b on a.inpmast_stock_number = b.stock_number) y
where x.stock_number = y.inpmast_stock_number	

and get rid of the bad ones
delete from cra.five_years where length(vin) <> 17

-----------------------------------------------------------------------------------------
--/> get rid of bad vins from cra.five_years
-----------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------
--< clean up the chrome data, build data should be exclusively build data
-- get rid of vins for which there is no or sparse chrome data
-------------------------------------------------------------------------------------------

-- process for adding vins to chr.build_data_describe_Vehicle
select distinct vin 
from cra.five_years a
where chr_make is null 
  and not exists (
    select 1
    from chr.build_data_describe_vehicle
    where vin = a.vin)

select * from chr.build_data_describe_vehicle where vin in ( '1G3NL52M9WM319554' ,'1GCDC14K5KZ261021','4S4BSBCC3GC229829','JN8AF5MV8ET366529','1D4HB48238F141487') 

get rid of the sparse/catalog entries

delete
-- select * 
from chr.build_data_describe_vehicle where source <> 'build'


-- describe vehicle for the remainder
select distinct b.vin
from cra.five_years b
where length(b.vin) = 17  
	and (select cra.vin_check_sum(vin)) -- passes check digit
	and not exists ( -- not in build_data_describe_vehicle
		select 1
		from chr.build_data_describe_vehicle
		where vin = b.vin)
 and not exists ( -- not in descrive_vehicle
	 select 1
	 from chr.describe_vehicle
	 where vin = b.vin)
limit 1000   

-- cra.five years that don't have a chrome make/model
select distinct a.vin, model_year, a.make, a.model
from cra.five_years a 
where chr_make is null

select distinct a.vin, a.model_year, a.make, a.model, b.vin as build_vin, b.source, c.vin as desc_vin
from cra.five_years a 
left join chr.build_data_describe_vehicle b on a.vin = b.vin
left join chr.describe_vehicle c on a.vin = c.vin
where chr_make is null


select distinct a.vin, c->'division'->>'_value_1' as make,
c->'model'->>'_value_1' as model,
b.response->'vinDescription'->>'source' as source, b.style_count
from cra.five_years a
left join chr.build_data_describe_vehicle b on a.vin = b.vin
left join jsonb_array_elements(b.response->'style') as c on true  
where a.chr_make is null

-- and update from build data
update cra.five_years x
set chr_make = y.make,
    chr_model = y.model,
    chr_source = y.source
from (
  select distinct a.vin, c->'division'->>'_value_1' as make,
  c->'model'->>'_value_1' as model,
  b.response->'vinDescription'->>'source' as source
  from cra.five_years a
  join chr.build_data_describe_vehicle b on a.vin = b.vin
  join jsonb_array_elements(b.response->'style') as c on true
  where a.chr_make is null) y
where x.vin = y.vin    

-- and from describe vehicle
4 vins, all with source = sparse, which means not much info
get rid of them
select distinct a.vin, a.model_year, a.make, a.model, c->'division'->>'_value_1' as make,
c->'model'->>'_value_1' as model,
b.response->'vinDescription'->>'source' as source, b.style_count, (select cra.vin_check_sum(coalesce(a.vin, 'xxx'))),
b.response->'vinDescription'->'attributes'->>'source' as source,
b.*
from cra.five_years a
left join chr.describe_vehicle b on a.vin = b.vin
left join jsonb_array_elements(b.response->'style') as c on true  
where a.chr_make is null  

-- delete the bad vins (sparse chrome)
delete
-- select * 
from cra.five_years where vin in ('1D4HB48238F141487','1FAFP5340XG108367','2S3DB717X96100015','4S4BSBCC3GC229829')

-- and update from describe vehicle
update cra.five_years x
set chr_make = y.make,
    chr_model = y.model,
    chr_source = y.source
from (
  select distinct a.vin, c->'division'->>'$value' as make,
  c->'model'->>'$value' as model,
  b.response->'vinDescription'->'attributes'->>'source' as source
  from cra.five_years a
  join chr.describe_vehicle b on a.vin = b.vin
  join jsonb_array_elements(b.response->'style') as c on true
  where a.chr_make is null) y
where x.vin = y.vin    

-- all vins in cra.five_years have chrome make/model
select * from cra.five_years where chr_make is null


-- 16668 distinct vins that all have been chrome decoded
select count(distinct vin) from cra.five_years

-------------------------------------------------------------------------------------------
--/> clean up the chrome data, build data should be exclusively build data
-------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------
--< add shape and size from veh.shape_size_classifications
-------------------------------------------------------------------------------------------
-- !! 05/27 looks like i never actually did this populating of shape/size, got distracted to something else
-- which is probably why the query is incomplete and the complete version does not exist in history
-- add fields shape/size to come from veh.shape_size_classifications (vision)

alter table cra.five_years
add column shape citext,
add column size citext;

update cra.five_years x
set shape = y.shape,
    size = 
      case
        when y.size = 'Medium' then 'Midsize'
        else y.size
      end
from (
  select a.vin, model_year, a.make, a.model, a.vehicle_type, a.segment, a.chr_make, a.chr_model, b.shape, b.size
  from cra.five_years a 
  left join veh.shape_size_classifications b on a.chr_make = b.make and a.chr_model = b.model) y
where x.vin = y.vin 


-- here are the vehicles not in veh.shape_size_classification, 56 of them
  select distinct model_year, a.make, a.model, a.vehicle_type, a.segment, a.chr_make, a.chr_model, b.shape, b.size
  from cra.five_years a 
  left join veh.shape_size_classifications b on a.chr_make = b.make and a.chr_model = b.model 
  where b.make is null
  order by make, model

  select distinct model_year, a.make, a.model
  from cra.five_years a 
  left join veh.shape_size_classifications b on a.chr_make = b.make and a.chr_model = b.model 
  where b.make is null
  order by model_year, make, model
  
select distinct vin, make, model, shape, size, chr_make, chr_model from cra.five_years where shape is not null

select distinct coalesce(shape, 'xxx') from cra.five_years

select * from chr.describe_vehicle where vin = '1HGCA5637JA066955'
----------------------------------------------------------------------------------------------------     

-- but it did show me some vehicles in five_years that don't exist in veh.shape_size_classifications, like the lexus rx 300
select * 
from veh.makes_models  
where make = 'lexus'
order by model

select * from veh.models where model like 'rx%'

insert into veh.models values('RX 300', current_date)

insert into veh.makes_models values('Lexus','RX 300', current_date)

select count(distinct vin) from cra.five_years

select vin, length(vin), substring(a.vin, 9, 1),
  (select cra.vin_check_sum(coalesce(a.vin, 'xxx'))) as check_digit,
  a.make, a.model, model_year
from cra.five_years a
where chr_make is null
  and length(vin) = 17
  and (select cra.vin_check_sum(coalesce(a.vin, 'xxx'))) = true
  
select * from chr.describe_vehicle where vin = '4S4BSBCC3GC229829'

select * from cra.five_years where vin = '4S4BSBCC3GC229829'


the issue is how to get the missing vehicles into vision classifications page
added lexus rx 300 to veh.makes and veh.models, so that model now shows up on the page (not completed)
no picture, where does that come from?
veh.additional_classification_data
/sql/pg/vehicles/additional_classification_data.sql

figure out what i was doing with python_projects/vehicles and the chr2 tables

-- ok, first, lets define what vehicles need to be dealt with
-- these are the 62 i stumbled on
  select a.vin, model_year, a.make, a.model, a.chr_make, a.chr_model, b.shape, b.size
  from cra.five_years a 
  left join veh.shape_size_classifications b on a.chr_make = b.make and a.chr_model = b.model
  where b.make is null

GET BACK TO THIS AFTER ALL THE CHROME/VIN STUFF

select *
from veh.vehicle_types
where make = 'chevrolet'
  and model_year = 2020

veh.vehicle_types built from chrome batches of style_id

--  05/28/22, trying to use single_batch_style_ids.py to update the tables necessary for vision to display the
--  vehicles that are not currently classified in veh.shape_size_classifications
-- delete the lexus rx 300 values that i have already inserted into veh.models and veh.makes_models
delete 
-- select *
from veh.makes_models
where model = 'rx 300'

delete 
-- select *
from veh.models
where model = 'rx 300'


-- so my first batch will be the lexus rx 300
select c->'attributes'->>'id'::citext as style_id, max(a.vin) as vin
from cra.five_years a
left join chr.describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'style') as c on true  
where a.shape is null
  and a.model = 'rx 300'
group by c->'attributes'->>'id'::citext  

select aa.style_id,null,null,aa.style_id,null,null,null,null,null,null,null,null,
	null,null,null,null,null,null,null,null
from (                   
	select c->'attributes'->>'id'::citext as style_id, max(a.vin) as vin
	from cra.five_years a
	left join chr.describe_vehicle b on a.vin = b.vin
	join jsonb_array_elements(b.response->'style') as c on true  
	where a.shape is null
		and a.model = 'rx 300'
	group by c->'attributes'->>'id'::citext) aa

-- ok, i didn't do it today, but the first attribute is passthru_id, what that has been in the past was stock_number:vin	
-- so
select aa.style_id,null,null,aa.style_id,null,null,null,null,null,null,null,null,
	null,null,null,null,null,null,null,null
from (                   
	select c->'attributes'->>'id'::citext as style_id, max(a.vin) as vin
	from cra.five_years a
	left join chr.describe_vehicle b on a.vin = b.vin
	join jsonb_array_elements(b.response->'style') as c on true  
	where a.shape is null
		and a.model = 'rx 300'
	group by c->'attributes'->>'id'::citext) aa

/*
looking at the first function call in single_batch_style_ids.py, chr2.drop_and_recreate_tmp_table_for_batch_processing()
prepares the chr2 tmp_tables for  populating with new data, 
then when function chr2.update_chr2_tables() is called, the chr2 tables are populated from the chr2 tmp_tables
checked the first one, chr2.style, and these lexus rx 300 style ids do not yet exist in the table
using these lexuses because they exists in describe_vehicles only (not build_data)
*/
select * 
from chr2.tmp_style
where style_id in(10757,4341,11489,11490)


  File "/mnt/hgfs/E/python_projects/vehicles/single_batch_style_ids.py", line 171, in process_batch
    "copy chr2." + 'tmp_' + the_file + " from stdin with csv header encoding 'latin-1'", io
psycopg2.errors.ForeignKeyViolation: insert or update on table "tmp_style" violates foreign key constraint "tmp_style_model_id_fkey"
DETAIL:  Key (model_id)=(3278) is not present in table "models".

looks like i am in a chicken and egg loop
i have not yet found where the table chr2.models gets populated
and single_batch_styhle_ids.py is failing because chr2.tmp_style has a foreign key of model_id to chr2.models
and the very point of what i am doing is that the model rx 300 does not exist in chr2.models yet
so, somewhere, there must be a preliminary script to populate the basic values that this script then fleshes out

/*
and just because things are not confusing enough, what about all the shit i did in luigi.chrome to the jon schema
this is the update that takes 6 hours to run that i havent done in quite a while
select * from jon.models where model like 'RX%'
select * from jon.makes order by make
i dont even know what that was all about
this sums it up
COMMENT ON TABLE jon.configurations
  IS chrome configurations at the level of chrome_style_id, does
  not include engines, interiors, colors, etc. Successful responses from chr.get_describe_vehicle_by_style_id,
  non fleet, 2018 and above, honda, nissan, chevrolet, buick, gmc, cadillac only.
  Need to figure out what to do about null trim_level;
these fucking tables are used in the daily all_nc_inventory_nightly.sql file that i run manually every day
select * from jon.describe_vehicle_by_style_id where chrome_style_id = '404224' order by from_date
and i am doing type 2 changes on at least some tables
*/


so, to circle back
where i am
the problem with running the batch script to generate new rows in veh.vehicle_types (with function chr2.update_veh_tables)

the answer might be ads_base_tables.py

--05/29/22
cant help but thinking, do i need the chr2 tables at all
why did i do batches as opposed to just using ADS
well, part of it was not limiting classification data to what we have experienced, ie self fulfilling prophecy defined
by just the vehicles we have seen vs all vehicles

definite flaw in the current flow as i am seeing it
when adding a new model, the model must exist before i can add the style_id


this is really confusing trying to sort out all these threads
trying to come up with a linear path for adding new models, eg lexus rs 300
i keep stumbling/thinking that all the existing code is  based on doing those large batches,
do i even need to do the batches?
do i even need the chr2 tables anymore
am i still interested in defining "all possible" vehicle types?
i dont think so
i just want to update the veh.vehicle_types table so that the missing models (and or makes and or model years) can 
make it onto the vehicle classifications page to get classified and provide the cra data with the current classifications!
so maybe back the fuck up

so, as i try to get the values i need from describe_vehicle, eg, passenger capacity, i am having no luck
could this be because i am not including tech specs in describe  vehicle?
but, does it exist in the batch file downloaded?
looking at chrome style 11489

all of which makes me think, stop looking for a shortcut
get to the root problem of why i could not process the batch
and the answer may be ads_base_tables
so, that is now the path that i am hgoing to pursue

the 1st challenge is defining the queries for ads_bas_tables




select * from chr2.divisions where division = 'lexus'

select * from chr2.sub_divisions

  select *
  from (
    select a.model_year, (attributes #>> '{id}')::integer as subdivision_id, b."$value" as subdivision
    from chr2.get_subdivisions a,
      jsonb_to_recordset(a.subdivisions) as b("$value" citext, attributes jsonb)) c


select * from chr2.models where model like 'rx%'

  select * 
  from (
    select (attributes #>> '{id}')::integer as model_id, model_year, division_id, b."$value" as model
    from chr2.get_models,
      jsonb_to_recordset(chr2.get_models.models) as b("$value" citext, attributes jsonb)) c
  where not exists (
    select 1
    from chr2.models
    where model_id = c.model_id);


select c->'attributes'->>'id'::citext as style_id, a.vin, a.stock_number,
  c->'model'->>'$value' as model, c->'division'->>'$value' as make, c->'attributes'->>'modelYear'
from cra.five_years a
left join chr.describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'style') as c on true  
where a.shape is null
  and a.model = 'rx 300'
  and a.vin in ('JT6HF10U6X0037758','JTJHF10U720239099','JT6HF10U1Y0121519','JT6GF10U2Y0065747')

-- divisions
select * from chr2.divisions where division = 'lexus' order by model_year

select model_year
from chr2.model_years
where model_year in (1999,2000,2001,2002);

-- subdivisions
select * from chr2.subdivisions where subdivision = 'lexus' order by  model_year

select model_year
from chr2.divisions 
where division = 'lexus' 
	and model_year in (1999,2000,2001,2002)

-- models
select * 
from chr2.models
where division_id = 24
  and model_year in (1999,2000,2001,2002)

select model_year, division_id
from chr2.divisions
where division = 'lexus'
  and model_year in (1999,2000,2001,2002)

-- style_ids
select * -- there are none
from chr2.style_ids
where model_id in (
  select model_id
  from chr2.models
  where division_id = 24
    and model_year in (1999,2000,2001,2002))

select model_id
from chr2.models a
where not exists (
	select 1 
	from chr2.style_ids
	where model_id = a.model_id)

--------------------
-- now back to single_batch_style_ids.py
seems to have run ok, 

select * 
from chr2.models
where model like 'rx 300%'

select * from chr2.get_style_ids where model_id in (4879,1029,1492,3278)

select * 
from chr2.tmp_style

-- so, try the first function
select chr2.update_chr2_tables()

ERROR:  ON CONFLICT DO UPDATE command cannot affect row a second time
HINT:  Ensure that no rows proposed for insertion within the same command have duplicate constrained values.
CONTEXT:  SQL function "update_chr2_tables" statement 19
********** Error **********

-- the problem is exterior_color with null color_code, which is a not null attribute
changing the PK to style_id, color_code, color_name works if null color_code is input as 'none'

ALTER TABLE chr2.exterior_color DROP CONSTRAINT exterior_color_pkey;
ALTER TABLE chr2.exterior_color
  ADD CONSTRAINT exterior_color_pkey PRIMARY KEY(style_id, color_code, color_name);

and the function chr2.update_chr2_tables() passes

now the last function
select chr2.update_veh_tables()
took 50 sec abut passed
AND the 2 new lexus models show on the vision classifications page as not yet classified

!! HOORAY !!
--------------------------------------------------------------------------------------------------------


create table cra.not_classified as
  select date_acquired, date_sold, a.stock_number, a.vin, model_year, a.make, a.model, a.chr_make, a.chr_model, b.shape, b.size,
    c.source as build_source, c.style_count as build_style_count, d.response->'vinDescription'->'attributes'->>'source' as source, d.style_count
  from cra.five_years a 
  left join veh.shape_size_classifications b on a.chr_make = b.make and a.chr_model = b.model
  left join chr.build_data_describe_vehicle c on a.vin = c.vin
  left join chr.describe_vehicle d on a.vin = d.vin
  where b.make is null
order by a.make, a.model, a.model_year
order by a.model_year, a.make, a.model

-- 05/30/22
what i want to do now is compare non build batch to the build batch

select * from chr.build_data_describe_vehicle where vin = '1GCUYHED7NZ178267'

select * from chr2.model_years


select distinct make, model_year from cra.not_classified order by make, model_year
select string_agg(distinct model_year,',') from cra.not_classified order by  model_year

select a.make, a.model, a.model_year, b.* from cra.not_classified a
left join chr2.models b on a.model = b.model and a.model_year::integer = b.model_year

select distinct * from cra.not_classified

1. ads_base_tables
-- divisions
model_years 1985, 1987, 1988, 1990, 1991
-- subdivisions
model_years 1985, 1987, 1988, 1990, 1991
-- models

                select model_year, division_id
                from chr2.divisions
                where (
                  (division = 'buick' and model_year in (1990, 1997)) or
                  (division = 'cadillac' and model_year in (1994, 2000)) or
                  (division = 'chevrolet' and model_year in (1990, 1991, 1994, 1996, 2000, 2002, 2005, 2022)))

                  
select * from chr2.divisions order by division, model_year


    select a.model_year, (attributes #>> '{id}')::integer as subdivision_id, b."$value" as subdivision, current_date 
    from chr2.get_subdivisions a,
      jsonb_to_recordset(a.subdivisions) as b("$value" citext, attributes jsonb)

select * from chr2.get_subdivisions

select * from chr2.subdivisions where model_year = 1985

select * from chr2.models where division_id = 8 order by model_year

-- ford pk multiple models per styleid
-- this effort got stalled when i tried to do a batch report using build data credentials
-- waiting to hear back from chrome
select c.vin, (r.style ->>'id')::citext as style_id,
  (r.style->>'modelYear')::integer as model_year,
  (r.style->'division'->>'_value_1')::citext as make,
  (r.style->'subdivision'->>'_value_1')::citext as subdivision, 
  (r.style ->'model'->>'_value_1'::citext)::citext as model,
  (r.style->>'mfrModelCode')::citext as chr_model_code,
 	(u.cab->>'_value_1')::citext || '::' || (v.bed->>'_value_1')::citext as body,
 	r.style->>'altBodyType',
  case
    when r.style->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style->>'drivetrain' like 'Front%' then 'FWD'
    when r.style->>'drivetrain' like 'Four%' then '4WD'
    when r.style->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drivetrain,
  r.style->>'trim' as trim_name,
  r.style->>'name' as style_name,
  r.style->>'nameWoTrim' as style_name_wo_trim,
  r.style->>'passDoors' as pass_doors,
  -- passenger capacity
--   case
--     when u.cab->>'$value' like 'Crew%' then 'crew' 
--     when u.cab->>'$value' like 'Extended%' then 'double'  
--     when u.cab->>'$value' like 'Regular%' then 'reg'  
--     else 'n/a'   
--   end as chr_cab,        
--   coalesce(r.style ->'attributes'->>'trim', 'none')::citext as chr_trim,
--   coalesce(t.displacement->>'$value', 'n/a') as engine_displacement, -- a.color, null::integer as configuration_id,
--   case
--     when v.bed->>'$value' like '%Bed' then substring(bed->>'$value', 1, position(' ' in bed->>'$value') - 1)
--     else 'n/a'
--   end as box_size ,
--   c.response  
-- from chr.describe_vehicle c 
from chr2.describe_vehicle_by_vin_with_tech c  
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(r.style->'bodyType') with ordinality as v(bed) on true
  and v.ordinality =  1    
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
  and s.engine ? 'installed'
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->'attributes'->>'unit' = 'liters'  
where c.vin = '1FTPX28L8WKA85591' and r.style->>'trim' = 'XLT'

 working with the json response in vehicles/json/with_tech.json
-- passenger capacity
select ts->'range'->>'max', ts->'range'->>'min'
select jsonb_array_length(ts ->'value') -- -> 2--@> [r.style ->>'id']
from chr2.describe_vehicle_by_vin_with_tech c  
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(c.response->'technicalSpecification') as ts(tech_specs) on true
  and ts->>'titleId' = '8' 
where vin = '1FTPX28L8WKA85591'

--sql/postgresql/nc_inventory/allocation_groups.sql
-- peg
select fo, fo->>'styleId'
from chr2.describe_vehicle_by_vin_with_tech c  
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(c.response->'factoryOption') as fo(factory_opts) on true
  and fo.factory_opts->'header'->>'_value_1' like 'PREFERRED%'
where vin = '1FTPX28L8WKA85591'




select * 
from chr2.describe_vehicle_by_vin_with_tech
where vin = '1FTPX28L8WKA85591'

ok, it looks like i can get what i need with describe vehilce with the ShowExtendedTechnicalSpecifications switch
that endpoint in vision (vehicles/describe_vehicle_by_vin_with_tech_specs.py) has been returning 500 all day
whereas in chrome/zeep_describeVehicle.py this works:
    response = client.service.describeVehicle(accountInfo=authentication, vin=vehicle_vin,
                                              switch=['ShowExtendedTechnicalSpecifications','ShowAvailableEquipment'])
using the old credentials, the data it returns is:
      "division": {
        "_value_1": "Ford",
        }
which looks like build data, but is not        

so, when i need the tech stuff, i will put it in chr2.describe_vehicle_by_vin_with_tech
i currently dont know if i am including tech data in build data or not, definitely not in plain describe_vehicle



-- 05/31/2022
this morning, leaning toward dropping all this for now, simply exclude the non classified vehicles for now
and proceed with the analysis of the remaining rows in cra.five_years

select count(*) from cra.five_years  -- 19616
select count(*) from cra.five_years where shape is not null -- 19554














-----------------------------------------------------------------------------------------------------
--< little did i realize, there are 3249 vins that are in both describe_vehicle and build_data
-----------------------------------------------------------------------------------------------------
SELECT a.vin, a.response->'vinDescription'->'attributes'->>'source', a.response, a.style_count, b.vin, b.source, b.response, b.style_count
from chr.describe_vehicle a
join chr.build_data_describe_vehicle b on a.vin = b.vin
where a.style_count <> 1
  and b.style_count = 1
  and a.response->'vinDescription'->'attributes'->>'source' is not null
limit 10  

select count(*)
from chr.describe_vehicle a
join chr.build_data_describe_vehicle b on a.vin = b.vin  

select 

-----------------------------------------------------------------------------------------------------
--/> little did i realize, there are 3249 vins that are in both describe_vehicle and build_data
-----------------------------------------------------------------------------------------------------