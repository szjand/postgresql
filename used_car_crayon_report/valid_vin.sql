﻿https://vin.dataonesoftware.com/vin_basics_blog/bid/112040/use-vin-validation-to-improve-inventory-quality

function IsVINValid(VIN: String): Boolean;
var
  SL: TStringList;
  AR: array [1..17] of Integer;
  I: Integer;
  Sum: Integer;
begin

  SL := TStringList.Create;
  try
    SL.Add('A=1');  SL.Add('B=2');  SL.Add('C=3');  SL.Add('D=4');  SL.Add('E=5');
    SL.Add('F=6');  SL.Add('G=7');  SL.Add('H=8');  SL.Add('J=1');  SL.Add('K=2');
    SL.Add('L=3');  SL.Add('M=4');  SL.Add('N=5');  SL.Add('P=7');  SL.Add('R=9');
    SL.Add('S=2');  SL.Add('T=3');  SL.Add('U=4');  SL.Add('V=5');  SL.Add('W=6');
    SL.Add('X=7');  SL.Add('Y=8');  SL.Add('Z=9');  SL.Add('A=1');  SL.Add('1=1');
    SL.Add('2=2');  SL.Add('3=3');  SL.Add('4=4');  SL.Add('5=5');  SL.Add('6=6');
    SL.Add('7=7');  SL.Add('8=8');  SL.Add('9=9');  SL.Add('0=0');
    AR[1]  := 8;    AR[2]  := 7;    AR[3]  := 6;    AR[4]  := 5;    AR[5]  := 4;
    AR[6]  := 3;    AR[7]  := 2;    AR[8]  := 10;   AR[9]  := 0;    AR[10] := 9;
    AR[11] := 8;    AR[12] := 7;    AR[13] := 6;    AR[14] := 5;    AR[15] := 4;
    AR[16] := 3;    AR[17] := 2;

    for i := 1 to 17 do
    begin
      if not ( VIN[i] in ['A' .. 'Z', '0' .. '9'])  then
        Exit;
      Sum := Sum + (StrToInt(SL.Values[VIN[i]]) * AR[i]);
    end;
    if (Sum mod 11) = 10 then
      Result := (VIN[9] = 'X')
    else
      Result := (VIN[9] = IntToStr((Sum mod 11)));
 finally
    SL.Free;
 end;
end;

drop table if exists cra.vin_transliteration;
create table cra.vin_transliteration(
  vin_char citext,
  vin_value integer);
insert into cra.vin_transliteration values
('a',1),('b',2),('c',3),('d',4),('e',5),('f',6),('g',7),('h',8),('j',1),('k',2),('l',3),('m',4),('n',5),('p',7),('r',9),('s',2),('t',3),
('u',4),('v',5),('w',6),('x',7),('y',8),('z',9),('1',1),('2',2),('3',3),('4',4),('5',5),('6',6),('7',7),('8',8),('9',9),('0',0);

drop table if exists cra.vin_weighting;
create table cra.vin_weighting(
  vin_position integer,
  factor integer);
insert into cra.vin_weighting values
(1,8),(2,7),(3,6),(4,5),(5,4),(6,3),(7,2),(8,10),(9,0),(10,9),(11,8),(12,7),(13,6),(14,5),(15,4),(16,3),(17,2);


1GNFLGEKXFZ137513


select b.*, c.vin_value, d.factor, c.vin_value * d.factor, sum(c.vin_value*d.factor) over (), 
	mod((sum(c.vin_value*d.factor) over ()), 11)
from (
	select a.*, row_number() over () as the_position
	from (
		select regexp_split_to_table('1GNFLGEKXFZ137513', '')::citext as the_char) a) b
join cra.vin_transliteration c on the_char = vin_char
join cra.vin_weighting d on b.the_position = d.vin_position
order by the_position

do $$
declare
	vin citext := '5XYKTCA66CG264950';
begin		
  if NOT (
		select
			case
				when check_digit = 10 then (select substring(vin, 9, 1)) = 'X'
				else check_digit = (select substring(vin, 9, 1))::bigint
			end
		from (  
			select distinct mod((sum(c.vin_value*d.factor) over ()), 11) as check_digit
			from (
				select a.*, row_number() over () as the_position
				from (
					select regexp_split_to_table(vin, '')::citext as the_char) a) b
			join cra.vin_transliteration c on the_char = vin_char
			join cra.vin_weighting d on b.the_position = d.vin_position) aa)
	THEN
	  raise exception 'Fails check digit';
	end if;
end $$

create or replace function cra.vin_check_sum(_vin citext)
returns boolean 
language sql as
$BODY$
/*
select * from cra.vin_check_sum('KL4CJGSM4JB657649')
*/
	select
		case
			when check_digit = 10 then (select substring(_vin, 9, 1)) = 'X'
			else check_digit = (select substring(_vin, 9, 1))::bigint
		end
	from (  
		select distinct mod((sum(c.vin_value*d.factor) over ()), 11) as check_digit
		from (
			select a.*, row_number() over () as the_position
			from (
				select regexp_split_to_table(_vin, '')::citext as the_char) a) b
		join cra.vin_transliteration c on the_char = vin_char
		join cra.vin_weighting d on b.the_position = d.vin_position) aa;
$BODY$


select * 
from (
select distinct vin, (select * from cra.vin_check_sum(vin)) as vin_check
from cra.temp1) a
where not vin_check