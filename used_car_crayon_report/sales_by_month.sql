﻿/*
this file is all about reconciling financial statements with the tool

concerned about the large discrepancies between accounting and tool
major source seems to be the inconsistent logging of L (customer buys out his own lease) stocknumbers
talked to greg, those "deals" should not be included in the statistical analysis of used car inventory/sales
eliminating the L stocknumbers brings the sales data for accounting and the tool to good enough
there are still some discrepancies, but, again, good enough

drop table if exists cra.crayon;
create table cra.crayon as
select location, year_month, stock_number
from cra.temp1 a
join dds.dim_date b on a.date_sold = b.the_date
  and b.year_month between 202201 and 202204
where a.disposition = 'vehiclesale_retail'
  and right(stock_number, 1) <> 'L'
--   and a.location = 'GF-Rydells'
group by  location, year_month, stock_number


drop table if exists cra.greg;
create table cra.greg as
select store_code, year_month, stock_number 
from greg.uc_daily_snapshot_beta_1 a
join dds.dim_date b on a.sale_date = b.the_date
  and year_month between 202201 and 202204
where sale_type = 'retail'
  and a.the_date = a.sale_date
  and right(stock_number, 1) <> 'L'
group by store_code, stock_number, year_month



drop table if exists cra.fs;
create table cra.fs as
select 'fs' ||'_' || store as store, year_month, control, sum(unit_count) as unit_count
from (
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 202201 and 202204-----------------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select b.year_month, f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month between 202201 and 202204  ---------------------------------------------------------------------
      and b.page = 16 and b.line between 1 and 6
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row
    inner join fin.dim_fs_org f
     on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account and b.year_month = d.year_month
  where a.post_status = 'Y' -- ) h
    and right(control, 1) <> 'L') h
group by store, control, year_month;


select *
from cra.fs a
full outer join cra.crayon b on a.control = b.stock_number
-- where b.stock_number is null
where a.control is null


select year_month, store, count(*)
from cra.fs
group by year_month,store
union
select year_month, location, count(*) 
from cra.crayon
group by year_month, location
order by year_month, store

select store, count(*)
from cra.fs
group by store
union
select location, count(*) 
from cra.crayon
group by location
order by store

*/
-- this matches financial statements
select store, year_month,sum(unit_count) as unit_count
from (
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 202201 and 202204-----------------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select b.year_month, f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month between 202201 and 202204  ---------------------------------------------------------------------
      and b.page = 16 and b.line between 1 and 6
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row
    inner join fin.dim_fs_org f
     on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account and b.year_month = d.year_month
  where a.post_status = 'Y'
    and right(control, 1) <> 'L') h
group by store, year_month
order by store, year_month;

-- cra.temp1 not even close to financial statements
select b.year_month, count(*) 
from cra.temp1 a
join dds.dim_date b on a.date_sold = b.the_date
where a.disposition = 'vehiclesale_retail'
group by b.year_month
order by year_month

select a.date_sold, a.stock_number
from cra.temp1 a
join dds.dim_date b on a.date_sold = b.the_date
  and b.year_month = 202204
where a.disposition = 'vehiclesale_retail'
order by stock_number


-- greg.uc_daily_snapshot_beta_1 not even close to financial statements
select a.*
from greg.uc_daily_snapshot_beta_1 a
join dds.dim_date b on a.sale_date = b.the_date
  and year_month = 202204
where sale_type = 'retail'
  and a.the_date = a.sale_date
  and a.store_code = 'ry1'

-- 22 vehicles in april fs that don't even exists in cra.temp1
-- they are  L with 1f G stock_numbers
-- drop table if exists april_fs;
-- create temp table april_fs as
-- select distinct aa.control 
select aa.* , bb.stock_number, bb.date_sold, bb.disposition
from (
  select d.store, a.control, 
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 202204-----------------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select b.year_month, f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 202204  ---------------------------------------------------------------------
      and b.page = 16 and b.line between 1 and 6
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row
    inner join fin.dim_fs_org f
     on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account and b.year_month = d.year_month
  where a.post_status = 'Y'
  order by d.store, a.control) aa
left join cra.temp1 bb on aa.control = bb.stock_number
where bb.stock_number is null
order by aa.store, coalesce(aa.control, bb.stock_number)   

-- they don't even exist in the tool
select * 
from april_fs a
left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber

select distinct bopmast_stock_number, bopmast_vin 
from arkona.xfm_bopmast a
join april_fs b on b.control = a.bopmast_stock_number
left join 

select * from arkona.xfm_bopmast where bopmast_vin = '3GNVKGE03AG227463'