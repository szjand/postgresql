﻿select * 
from (
select 'pg'::text as source, make, model, shape, size 
from veh.shape_size_classifications) a
full outer join(
select 'ads'::text as source, make, model, vehicletype, vehiclesegment
from ads.ext_make_model_classifications ) b on a.make = b.make and a.model = b.model
order by coalesce(a.make, b.make), coalesce(a.model, b.model)


select vin,yearmodel
from ads.ext_vehicle_items
where yearmodel::integer > 2019

select a.*, b.inpmast_stock_number, b.year
from ads.ext_vehicle_items a
left join arkona.ext_inpmast b on a.vin = b.inpmast_vin
where a.yearmodel::integer > 2022

select * 
from arkona.xfm_inpmast
where inpmast_vin = '1B7GG2AX9YS666694'


select a.vin, a.yearmodel, b.vin, c.vin, c.source
from ads.ext_vehicle_items a
left join chr.describe_vehicle b on a.vin = b.vin
left join chr.build_data_describe_vehicle c on a.vin = c.vin
where a.yearmodel::integer > 2019



select a.vin, a.yearmodel, a.make, a.model, 
  (r.style ->>'id')::citext as chr_style_id,
  (r.style->>'modelYear')::integer as chr_model_year,
  (r.style ->'division'->>'_value_1')::citext as chr_make,
  (r.style ->'model'->>'_value_1'::citext)::citext as chr_model,
  (r.style ->>'mfrModelCode')::citext as chr_model_code,
  case
    when r.style ->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as chr_drive,
  case
    when u.cab->>'_value_1' like 'Crew%' then 'crew' 
    when u.cab->>'_value_1' like 'Extended%' then 'double'  
    when u.cab->>'_value_1' like 'Regular%' then 'reg'  
    else 'n/a'   
  end as chr_cab,
  coalesce(r.style ->>'trim', 'none')::citext as chr_trim,
  t->>'_value_1' as engine_displacement,
  case
    when v.bed->>'_value_1' like '%Bed' then substring(bed->>'_value_1', 1, position(' ' in bed->>'_value_1') - 1)
    else 'n/a'
  end as box_size , x.*, c.response   
from ads.ext_vehicle_items a
join chr.build_data_describe_vehicle c on a.vin = c.vin
join jsonb_array_elements(c.response->'style') as r(style) on true 
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(r.style->'bodyType') with ordinality as v(bed) on true
  and v.ordinality =  1    
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
  and (s.engine->'installed'->>'cause' = 'OptionCodeBuild' or coalesce(s.engine->'installed'->>'cause', 'xxx') = 'VIN')
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->>'unit' = 'liters'  
left join jon.engines x on (r.style ->>'id')::citext = x.chrome_style_id
where a.yearmodel::integer > 2000


select * from chr.describe_vehicle where vin = '5FNYF6H52MB049809'

select * from chr.build_data_describe_Vehicle where source <> 'build'

select a.vin, b.vin 
from cra.temp1 a
left join chr.describe_vehicle b on a.vin = b.vin
where a.make = 'honda'
  and a.model_year = '2021'


-- how man already in chrome
select a.vin,b.vin, b.style_count
from (
	select vin
	from ads.ext_dim_vehicle 
	group by vin) a
join chr.describe_vehicle b on a.vin = b.vin	
 
select a.vin,b.vin, b.style_count
from (
	select vin
	from ads.ext_dim_vehicle 
	group by vin) a
join chr.build_data_describe_vehicle b on a.vin = b.vin	  