﻿-- categories vs classifications
select distinct
  (c ->'division'->>'_value_1')::citext as make,
--   (c ->'subdivision'->>'_value_1')::citext as subdivision,
  (c ->'model'->>'_value_1'::citext)::citext as model
from chr.build_data_describe_vehicle b
join jsonb_array_elements(b.response->'style') c on true
union
select distinct
  (c->'division'->>'$value')::citext as make,
  (c->'model'->>'$value'::citext)::citext as model
from chr.describe_vehicle b
join jsonb_array_elements(b.response->'style') c on true  
order by make, model

select distinct make, model, substring(vehicletype, position('_' in vehicletype) + 1, 12) as shape,
  substring(vehiclesegment, position('_' in vehiclesegment) + 1, 12) as size,
  case when luxury then 'luxury' else null end as luxury,
from ads.ext_make_model_classifications
order by make, model

select * from ads.ext_make_model_classifications where model = 'mdx'

select * 
from ads.ext_make_model_classifications
limit 10

select * from veh.shape_size_classifications limit 100