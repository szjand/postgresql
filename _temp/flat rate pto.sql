﻿-- function pto.flat_rate_pto_rate_recalc()
-- step by step using temp tables;
DO $$
	declare
		_first_of_pp date :=  '09/10/2023'; --(
-- 			select distinct biweekly_pay_period_start_date
-- 			from dds.dim_date
-- 			where biweekly_pay_period_start_date = '08/13/2023'); 
		_last_of_pp date := '09/23/2023'; --(
-- 			select biweekly_pay_period_end_date
-- 			from dds.dim_date 
-- 			where the_date = '08/26/2023');
		_pay_period_seq integer := 385; -- (
-- 			select biweekly_pay_period_sequence
-- 			from dds.dim_date
-- 			where the_date = '09/10/2023');
	begin
	-- this table is a non persistent set of employees upon which to do the pto rate calculations on anniversaries
		delete 
		from pto.flat_rate_employees;
		insert into pto.flat_rate_employees(last_name,first_name,employee_number,employee_id,pay_calc_profile,
		  store,department,cost_center,manager_1, manager_2,
			seniority_date,current_pto_rate,most_recent_anniv)		
		select * 
		from (
			select a.last_name, a.first_name, a.employee_number, a.employee_id, c.display_name as pay_calc_profile, 
				d.store,  d.department, d.cost_center, a.manager_1, a.manager_2, coalesce(a.seniority_date, a.hire_date) as seniority_date,
				coalesce(e.current_rate, 0) as current_pto_rate,
				case
					when -- current year anniversary
						to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' ) < _first_of_pp
							then to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' )
					else (to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' ) - interval '1 year')::date
				end as most_recent_anniv	
			from ukg.employees a
			join ukg.employee_profiles b on a.employee_id = b.employee_id
			join ukg.ext_pay_calc_profiles c on b.pay_calc_id = c.id
				and c.display_name = 'Full-Time Flat Rate'
			join (select * from ukg.get_cost_center_hierarchy()) d on a.cost_center = d.cost_center  
				and 
					case
						when a.store = 'GM' then d.store = 'Rydell GM'
						when a.store = 'HN' then d.store = 'Rydell Honda Nissan'
						when a.store = 'Toyota' then d.store = 'Rydell Toyota'
					end
			left join ukg.personal_rate_tables e on b.rate_table_id = e.rate_table_id
				and e.counter = 'paid time off'
			where a.status = 'active') s
-- 			  and a.employee_number = '184806') s
-- 		where seniority_date <> most_recent_anniv; -- excludes employees with less than a year employment
		where 
			case
				when seniority_date = most_Recent_anniv then _first_of_pp date /*current_Date*/ - seniority_date > 330
				else seniority_date <> most_Recent_anniv
			end;	

		drop table if exists employee_dates;
		create temp table employee_dates as
-- 		insert into pto.flat_rate_employee_dates(pay_period_seq,pay_period_start_date,pay_period_end_date,employee_number,
-- 			last_anniv,next_anniv,min_check_date,max_check_date,clock_from_date,clock_thru_date)
		select distinct _pay_period_seq as pay_period_seq, _first_of_pp, _last_of_pp,
		  aa.employee_number, aa.most_recent_anniv, aa.next_anniv, aa.min_check_date, 
			aa.max_check_date,c.biweekly_pay_period_start_date clock_from_date, e.biweekly_pay_period_end_date as clock_thru_date
		from (
			select a.employee_number, a.most_recent_anniv, a.next_anniv, 
			  min(c.pay_date) as min_check_date,
				max(c.pay_date) as max_check_date
			from (
				select *, (most_recent_anniv + interval '1 year')::date as next_anniv 
				from pto.flat_rate_employees
				where pay_calc_profile = 'Full-Time Flat Rate'
					and (most_recent_anniv + interval '1 year')::date between _first_of_pp and _last_of_pp) a 	
			join ukg.payrolls c on true	
				and c.payroll_name like '%bi-weekly%'
				and c.pay_date between a.most_recent_anniv and a.next_anniv - 1
			where a.pay_calc_profile = 'Full-Time flat Rate'
			group by a.employee_number, a.most_recent_anniv, a.next_anniv) aa	
		left join dds.dim_date b on aa.min_check_date = b.the_date
		-- the pay period previous to the one that includes the min_check_date
		left join dds.dim_date c on b.biweekly_pay_period_sequence = c.biweekly_pay_period_sequence + 1	
		left join dds.dim_date d on aa.max_check_date = d.the_date
		-- the pay period previous to the one that includes the max check date
		left join dds.dim_date e on d.biweekly_pay_period_sequence = e.biweekly_pay_period_sequence + 1;		

		drop table if exists earnings;
		create temp table earnings as
-- 		insert into pto.flat_rate_earnings(pay_period_seq,employee_number, earnings_code,earnings)
		select pay_period_seq, employee_number, earnings_code, sum(earnings) as earnings
		from (
			select aa.pay_period_seq,a.employee_number, c.pay_date, d.earnings_code, b.ee_amount as earnings
-- select a.*			
			from pto.flat_rate_employees a
			join employee_dates /*pto.flat_rate_employee_dates*/ aa on a.employee_number = aa.employee_number
			  and aa.pay_period_seq = _pay_period_seq
			join ukg.pay_statement_earnings b on a.employee_id = b.employee_account_id
				and coalesce(b.ee_amount, 0) <> 0
			join ukg.payrolls c on b.payroll_id = c.payroll_id
				and c.pay_date between aa.min_check_date and aa.max_check_date
			join pto.earnings_codes d on b.earning_code	= d.earnings_code
				and d.include_in_pto_rate_calc) e
		group by pay_period_seq, employee_number, earnings_code
		having sum(earnings) > 0;

-- 		delete
-- 		from pto.flat_rate_clock_hours
-- 		where pay_period_seq = _pay_period_seq;
-- 		insert into pto.flat_rate_clock_hours(pay_period_seq,employee_number,clock_hours,pto_hours,hol_hours)
    drop table if exists clock_hours;
    create temp table clock_hours as
		select _pay_period_seq as pay_period_seq, employee_number, sum(clock_hours) as clock_hours, sum(pto_hours) as pto_hours, sum(hol_hours) as hol_hours
		from (
			select a.employee_number, c.the_date, c.clock_hours, c.pto_hours, c.hol_hours
			from pto.flat_rate_employees a
			join employee_dates /*pto.flat_rate_employee_dates*/ b on a.employee_number = b.employee_number
				and b.pay_period_seq = _pay_period_seq
			join ukg.clock_hours c on a.employee_number = c.employee_number
				and c.the_date between b.clock_from_date and b.clock_thru_date
			where c.the_date > '12/18/2021') d
		group by employee_number;

-- !!!!!!!!!!!!!!!!!! fuck me, inadvertently deleted from pto.flat_rate_pto_data_summary for pay period seq 383
-- ********** fixed 9/29 *******************************
-- 		delete
-- 		from pto.flat_rate_pto_data_summary
-- 		where pay_period_seq = _pay_period_seq;		
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!		

-- 		delete
-- 		from pto.flat_rate_pto_data_summary
-- 		where pay_period_seq = _pay_period_seq;
-- 		insert into pto.flat_rate_pto_data_summary
		drop table if exists summary;
		create temp table summary as
		select -- _pay_period_seq, _first_of_pp, _last_of_pp, 
		  a.last_name, a.first_name, a.employee_number, a.store, a.department, a.cost_center, 
			a.manager_1, a.manager_2, a.seniority_date, b.next_anniv, d.earnings, c.clock_hours, a.current_pto_rate, 
			round(earnings/clock_hours, 2) as new_pto_rate,
			case
				when round(d.earnings/c.clock_hours, 2) > a.current_pto_rate then round(d.earnings/c.clock_hours, 2)
				else a.current_pto_rate
			end as effective_pto_rate,
			case
				when round(d.earnings/c.clock_hours, 2) < a.current_pto_rate then 0
				else round(d.earnings/c.clock_hours, 2) - a.current_pto_rate
			end as diff
		from pto.flat_rate_employees a
		join employee_dates /*pto.flat_rate_employee_dates*/ b on a.employee_number = b.employee_number
			and b.pay_period_seq = _pay_period_seq
		join clock_hours /*pto.flat_rate_clock_hours*/ c on a.employee_number = c.employee_number
			and b.pay_period_seq = c.pay_period_seq
		join (
			select pay_period_seq, employee_number, sum(earnings) as earnings
			from earnings /*pto.flat_rate_earnings*/
			where pay_period_seq = _pay_period_seq
			group by pay_period_seq, employee_number) d on a.employee_number = d.employee_number
			and b.pay_period_seq = d.pay_period_seq;	


			
end; $$;			

select * from pto.flat_rate_employees;
select * from employee_dates;
select * from earnings;
select * from clock_hours;
select * from summary;

-- use this query to verify in vision the flat rate pto changes for each pay period
-- this version of pto.flat_rate_employees generated using current pay period (9/24 - 10/7/23)
-- Ethan Farley 7/16 - 7/29, vision shows 65.43, query shows 0, UKG shows 24.98. guessing this is the goofy transition from salary to flat rate
-- 7/30 - 8/12/23, several small diffs, my guess is difference in earning/clock hours between now and when it was calculated in vision, leave them be
-- completely missing the pay period 9/10 - 09/23, need to rerun that in  production function
looks like paul leavy did not get updated
select * from pto.flat_rate_employees order by extract(month from seniority_date), extract(day from seniority_date)



select * from pto.flat_rate_pto_data_summary where pay_period_Seq = 383

select * from pto.flat_rate_pto_data_summary order by pay_period_seq

select * from pto.flat_rate_pto_data_summary order by last_name, pay_period_seq

select * from dds.dim_date where biweekly_pay_period_sequence = 383 order by the_date

select * from dds.dim_date where the_date = current_Date

select * from dds.dim_date where the_date = current_Date


for Kim
pay period 8/13 - 8/26/23
Looks like Paul Levy did not get updated. New PTO rate is $43.80.
Eric Moon, no PTO rate in UKG, Vision shows $51.45
pay period 8/27 - 9/9/23
Alex Waldbauer does no PTO rate in UKG, Vision shows 29.64
Nicholas Soberg did not get updated, Vision now shows an increase of $0.66 for a PTO rate of $31.96
pay period 9/10 - 9/23
Caleigh Kaml no PTO rate in UKG, Vision shows $19.78
Jeffrey Bear did not get updated, Vision now shows an increase of $2.35 for a PTO rate of $28.48









