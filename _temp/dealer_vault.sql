﻿
drop table if exists t1;
create temp table t1 as
select year_month, sum(total_labor_sale) as total_labor, sum(internal_labor_sale) as internal_labor
from (
	select 
		case 
			when close_date::date between '05/01/2023' and '05/31/2023' then 202305
			when close_date::date between '06/01/2023' and '06/30/2023' then 202306
			when close_date::date between '07/01/2023' and '07/31/2023' then 202307
			when close_date::date between '08/01/2023' and '08/31/2023' then 202308
			when close_date::date between '09/01/2023' and '09/30/2023' then 202309
		end as year_month, replace(total_labor_sale, ',', '')::numeric as total_labor_sale, replace(internal_labor_sale, ',', '')::numeric as internal_labor_sale
	from dv.service_data
	where close_date > '04/30/2023'
		and vendor_Dealer_id = 'DVD36911') a
group by year_month  

select year_month, total_labor::integer, internal_labor::integer , (total_labor - internal_labor)::integer
from t1
where year_month is not null 
order by year_month

select * 
from dv.service_data
where ro_number = '16593418'

select ro_number, total_labor_sale, customer_labor_sale, internal_labor_sale, warranty_labor_sale
from dv.service_Data 
where close_date::date between '09/01/2023' and '09/07/2023'
  and replace(total_labor_sale, ',', '')::numeric > 0 
  and replace(internal_labor_sale, ',', '')::numeric  > 0
  and total_labor_sale <> internal_labor_sale
  and vendor_Dealer_id in ('DVD36911',' DVD36913','DVD56474')

select ro_number, customer_labor_sale, warranty_labor_sale, internal_labor_sale, 


select * 
from dv.stores a
join dip.dealerships b on a.dealership_id = b.dealership_id



select b.page, b.line, b.col, b.line_label, c.store, c.department, c.sub_department, d.gm_account, d.gl_account, a.amount
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202309
  and b.page = 16
  and b.line in (21)
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
  and c.store = 'ry1'
join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  and d.current_row
order by b.line 



drop table if exists t1;
create temp table t1 as
select year_month, sum(total_labor_sale) as total_labor, sum(internal_labor_sale) as internal_labor
from (
	select 
		case 
			when close_date::date between '05/01/2023' and '05/31/2023' then 202305
			when close_date::date between '06/01/2023' and '06/30/2023' then 202306
			when close_date::date between '07/01/2023' and '07/31/2023' then 202307
			when close_date::date between '08/01/2023' and '08/31/2023' then 202308
			when close_date::date between '09/01/2023' and '09/30/2023' then 202309
		end as year_month, replace(total_labor_sale, ',', '')::numeric as total_labor_sale, replace(internal_labor_sale, ',', '')::numeric as internal_labor_sale
	from dv.service_data
	where close_date::date between '09/01/2023' and '09/30/2023'
		and vendor_Dealer_id = 'DVD36911') a
group by year_month  

select * from t1

select c.store, year_month, sum(-a.amount)
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202309 -- between 202303 and 202309
  and b.page = 16
  and b.line between 21 and 30
  and b.col = 9
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
  and c.store = 'ry1'
join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  and d.current_row
--   and d.gl_account not in ('146027','146028','146327','146328')
group by c.store, year_month