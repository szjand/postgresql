﻿from Anthony:
Hello sir, 
Just wanted to fill you in on a change we made that may affect payroll at the end of month so you can be ready 
for it or make changes now so there are no issues with our Single point payrolls at all stores. Not sure 
if they sent you anything on this?

The Rydell care/Toyota prepaid maintenance cost is set up under finance and in service contracts. 
This cost goes towards the unit and not the single points cost of products in finance. Youll see 
COST of $165, $260, $380, or $520 as cost in the system. RETAIL for guest will $0, $170, $290, or $430. 
We do not want them to get charged %20 of product on the negative difference. They will be getting $25 
spiff for any upgrade from free 1yr/15,000 maintenance. 

Please let me know if you have any quesitons.       
Anthony Michael, GSM

from Jeri:
Yes, I think it would be easier to talk through this.  Is there a time that works tomorrow?  I have to work from home from until 10 tomorrow, but I can still jump on a zoom anytime after 8:30.


-- from FUNCTION sls.update_deals_accounting_detail():
select distinct f.department, f.sub_department, f.store, b.line, b.col, b.line_label, d.gl_account, e.description,
  case
    when b.line in (3,13) then 'fi_chargeback'
    when b.line in (1,11) then 'fi_reserve'
    when b.line in (2,5,6,7,12,15,16,17) then 'fi_product'
    else '???'
  end as fi_category
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202309
  and b.page = 17
  and b.line between 1 and 17
  and b.line <> 9
--   and b.col = 5 
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store in ('ry1','ry2', 'ry8')
order by f.store, f.department, f.sub_department, b.line, d.gl_account

-- and these are the accounts
select *
from sls.deals_accounts_routes
where page = 17
order by store_Code, line

-- suprisingly, some of these numbers are off for each store
select *, (sum(amount) over (partition by store_code, line))::integer
from ( 
	select store_code, line, gl_account, sum(amount) as amount-- , sum(amount) over (partition by store_code, line)
	from sls.deals_Accounting_detail
	where page = 17
		and year_month = 202309
	group by store_code, line, gl_account) a
-- limit 10