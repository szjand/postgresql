﻿select * 
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202310
  and b.page = 17
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
limit 100  

select * from fin.dim_fs_org

select c.store, b.line, sum(-a.amount) 
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202310
  and b.page = 17
  and line between 1 and 20
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
group by c.store, b.line


select c.store, 
  sum(-a.amount) filter (where b.line < 11) as new_fi_gross,
  sum(-a.amount) filter (where b.line > 10) as used_fi_gross
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202310
  and b.page = 17
  and line between 1 and 20
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
group by c.store

select * from fin.dim_fs_Account

select c.store, 
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202310
  and b.page = 17
  and line between 1 and 20
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
group by c.store

select * from fin.accounting_gross_by_stock_number('G45657RC')

select control from sls.deals_gross_by_month where year_month = 202310 and fi_sales <> 0

select a.control, a.fi_sales - a.fi_cogs as fi_gross,
  (select back_gross from fin.accounting_gross_by_stock_number(a.control))
from sls.deals_gross_by_month a 
where year_month = 202310 and fi_sales <> 0
  and a.control = 'G47544A'
  
select control, fi_gross, back_gross, fi_gross - back_gross
from (
select a.control, a.fi_sales - a.fi_cogs as fi_gross,
  (select back_gross from fin.accounting_gross_by_stock_number(a.control))
from sls.deals_gross_by_month a 
where year_month = 202310 and fi_sales <> 0) b

G46797A,300.00,1848

select * from sls.deals_Accounting_detail where control = 'G46797A'

select gross_category, sum(amount) from sls.deals_Accounting_detail where control = 'G46797A' group by gross_Category

select * from luigi.luigi_log where task = 'DealsAccountingDetail' order by the_date desc limit 20

select * from sls.deals where stock_number = 'G46797A'

select * from sls.personnel where ry1_id = 'mls'

select * from sls.deals_Accounting_detail where control = 'G47544A'

select gross_category, sum(amount) from sls.deals_Accounting_detail where control = 'G47544A' group by gross_Category

select * from sls.deals where stock_number = 'G47544A'

select * from sls.deals_gross_by_month where control = 'G47544A'


select * from sls.deals_Accounting_detail where control = 'G47234A'

select gross_category, sum(amount) from sls.deals_Accounting_detail where control = 'G47234A' group by gross_Category

select * from sls.deals where stock_number = 'G47234A'

select * from sls.deals_gross_by_month where control = 'G47234A'




select * from sls.deals_Accounting_detail where control = 'G45657RC'

select gross_category, sum(amount) from sls.deals_Accounting_detail where control = 'G45657RC' group by gross_category

select * from sls.deals where stock_number = 'G45657RC'

select * from sls.deals_gross_by_month where control = 'G45657RC'

select year_month, sum(amount) from sls.deals_accounting_detail where control = 'G45657RC' and gross_category = 'front' group by year_month


select sum(amount)  -- 4518.25  the diff is in this inclusion of 476 cogs in november that was not included at the time of this table being built
from cu.uc_front_gross_detail
where control = 'G45657RC'

select *
from fin.accounting_gross_by_stock_number('G45657RC')



 -- cc: fi gross
--         select fi_employee_number, sum(fi_gross) as fi_gross, sum(fi_product_gross) as fi_product_gross, 
-- 					sum(fi_reserve_gross) as fi_reserve_gross
--         from (    
          select a.year_month, a.fi_employee_number, a.fi_last_name, 
            case when c.payplan in ('executive','croaker','outlet','sales consultant','tarr') then b.fi_sales - b.fi_cogs end as fi_gross,
            case when c.payplan = 'single point' then b.fi_product_sales - b.fi_product_cogs end as fi_product_gross,
            case when c.payplan = 'single point' then b.fi_reserve_sales - b.fi_reserve_cogs end as fi_reserve_gross
          from sls.deals_by_month a
          left join sls.deals_gross_by_month b on a.stock_number = b.control
--             and a.year_month = b.year_month
          left join sls.personnel_payplans c on a.fi_employee_number = c.employee_number
            and c.thru_date > current_date  
          where a.year_month = 202310 -- (select year_month from open_month)
            and a.stock_number = 'G47544A'
            and a.bopmast_id <> 79870
            and -- *e*
              case
                when a.stock_number = 'G47403B' and a.year_month = 202308 then a.bopmast_id = 78568
                else true
              end -- ) aaa 
--         group by aaa.fi_employee_number, aaa.year_month