﻿        select fi_employee_number, sum(fi_gross) as fi_gross, sum(fi_product_gross) as fi_product_gross, 
					sum(fi_reserve_gross) as fi_reserve_gross
        from (    
          select a.year_month, a.fi_employee_number, a.fi_last_name, 
            case when c.payplan in ('executive','croaker','outlet','sales consultant','tarr') then b.fi_sales - b.fi_cogs end as fi_gross,
            case when c.payplan = 'single point' then b.fi_product_sales - b.fi_product_cogs end as fi_product_gross,
            case when c.payplan = 'single point' then b.fi_reserve_sales - b.fi_reserve_cogs end as fi_reserve_gross
          from sls.deals_by_month a
          left join sls.deals_gross_by_month b on a.stock_number = b.control
            and a.year_month = b.year_month
          left join sls.personnel_payplans c on a.fi_employee_number = c.employee_number
--             and c.thru_date > current_date 
            and c.thru_date >= (
              select first_of_month
              from sls.months
              where open_closed = 'open')  
          where a.year_month = 202312
            and a.bopmast_id <> 79870
            and -- *e*
              case
                when a.stock_number = 'G47403B' and a.year_month = 202308 then a.bopmast_id = 78568
                else true
              end) aaa 
        where aaa.fi_employee_number = '136353'
        group by aaa.fi_employee_number, aaa.year_month


select * from sls.deals_gross_by_month limit 4        

select * from sls.deals_by_month where stock_number in ('G49206T','G48935')                                                 

select * from sls.personnel_payplans where employee_number = '136353'

select  sls.update_deals_by_month()

select sls.update_consultant_payroll() 

select * from sls.months where year_month = 202312

select first_of_month
from sls.months
where open_closed = 'open'

select * from sls.consultant_payroll where last_name = 'sabo'

select sls.json_get_deals_by_consultant_month('136353', 202312)


-- FI from FUNCTION sls.json_get_deals_by_consultant_month(citext, integer)

				select stock_number, customer, vehicle, fi_employee_number, 
				  case
				    when stock_number = 'G48067HB' and customer = 'HOYT, LONNIE ALLEN' then 0
				    else sum(fi_gross) 
				  end as fi_gross, 
					case
						-- 2 hagen
						when stock_number = 'T10827' and fi_employee_number = '8100207' and year_month = 202310 then 650
						else sum(fi_product_gross) 
					end as fi_product_gross, 
					case
						-- 1 chad wages 202310
						when stock_number = 'T10827' and fi_employee_number = '8100258' and year_month = 202310 then 0.00
						-- 2 hagen
						when stock_number = 'T10827' and fi_employee_number = '8100207' and year_month = 202310 then 738.71
						else sum(fi_reserve_gross) 
					end as fi_reserve_gross
				from (    
					select a.year_month, a.fi_employee_number, a.fi_last_name, a.stock_number, concat(model_year, ' ', make, ' ', model) as vehicle, 
						buyer as customer,
						case when c.payplan in ('executive','croaker','outlet','sales consultant', 'tarr') then b.fi_sales - b.fi_cogs end as fi_gross,
						case when c.payplan = 'single point' then b.fi_product_sales - b.fi_product_cogs end as fi_product_gross,
						case when c.payplan = 'single point' then b.fi_reserve_sales - b.fi_reserve_cogs end as fi_reserve_gross
					from sls.deals_by_month a
					left join sls.deals_gross_by_month b on a.stock_number = b.control
						and a.year_month = b.year_month
					left join sls.personnel_payplans c on a.fi_employee_number = c.employee_number
						and c.thru_date > '11/30/2023' -- current_date  
					where a.year_month = 202312 -- _year_month
						and
							case
								when a.stock_number = 'G47403B' and a.year_month = 202308 then a.bopmast_id = 78568
								else true
							end  order by stock_number) aaa
				where aaa.stock_number in ('G49206T','G48935')
				group by aaa.year_month, aaa.fi_employee_number, stock_number, customer,vehicle