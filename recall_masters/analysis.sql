﻿select *
from jon.recall_master_ros

-- extracting data from excel
-- ="insert into jon.recall_master_ros values ('GM','"&TEXT(C6, "mm/dd/yyyy")&"','"&D6&"',"&K6&","&L6&","&M6&");"
select *
from jon.recall_master_ros a
left join (
  select b.the_date, a.ro, a.line, a.flaghours, a.laborsales, c.opcode, c.description, d.opcode, d.description
  -- select a.*
  from ads.ext_fact_repair_order a
  inner join dds.dim_date b on a.opendatekey = b.date_key
  inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
  inner join ads.ext_dim_opcode d on a.corcodekey = d.opcodekey) b on a.ro = b.ro
where a.ro = '16318231'  
order by a.ro


select a.ro, a.total, b.amount, c.account, c.account_type, c.description
from jon.recall_master_ros a
left join fin.fact_gl b on a.ro = b.control
inner join fin.dim_account c on b.account_key = c.account_key

select a.ro, a.total, sum(b.amount)
from jon.recall_master_ros a
left join fin.fact_gl b on a.ro = b.control
  and b.post_status = 'Y'
inner join fin.dim_account c on b.account_key = c.account_key
group by a.ro, a.total





select *
from jon.recall_master_ros a
left join (
  select b.the_date, a.ro, a.line, a.flaghours, a.laborsales, c.opcode, c.description, d.opcode, d.description
  -- select a.*
  from ads.ext_fact_repair_order a
  inner join dds.dim_date b on a.opendatekey = b.date_key
  inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
  inner join ads.ext_dim_opcode d on a.corcodekey = d.opcodekey) b on a.ro = b.ro
order by a.ro


select opcode, op_desc, cor, cor_desc
from (
  select b.the_date, a.ro, a.line, a.flaghours, a.laborsales, c.opcode, c.description as op_desc, d.opcode as cor, d.description as cor_desc
  from ads.ext_fact_repair_order a
  inner join dds.dim_date b on a.opendatekey = b.date_key
  inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
  inner join ads.ext_dim_opcode d on a.corcodekey = d.opcodekey
  where exists (
    select 1
    from jon.recall_master_ros
    where ro = a.ro)) x
group by opcode, op_desc, cor, cor_desc  

-- ?!? only these are recall related
drop table if exists recalls;
create temp table recalls as
select *
from (
  select b.the_date, a.ro, a.line, a.flaghours, a.laborsales, c.opcode, 
    c.description as op_desc, d.opcode as cor, d.description as cor_desc,
    e.complaint, e.correction
  from ads.ext_fact_repair_order a
  inner join dds.dim_date b on a.opendatekey = b.date_key
  inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
  inner join ads.ext_dim_opcode d on a.corcodekey = d.opcodekey
  left join ads.ext_dim_ccc e on a.ccckey = e.ccckey
  where exists (
    select 1
    from jon.recall_master_ros
    where ro = a.ro)) x  
where op_desc like 'campaign%'  
  or cor_desc like '%recall%'
  or cor_desc like '%campaign%'
  or complaint like '%campaign%'
  or complaint like '%recall%'
  or correction like '%campaign%' 
  or correction like '%recall%'
  or complaint like '%18-002%'
  or complaint like '%18-021%'
  or complaint like '%p7318%'
  or complaint like '%p7305%'
  or complaint like '%pc090%'
  or complaint like '%pc142%'
  or complaint like '%pc426%'
  or complaint like '%pc490%'
  or complaint like '%pc516%'
  or complaint like '%pc601%'
  or complaint like '%pc618%'
  or complaint like '%pm683%'
  or complaint like '%pm685%'
  or complaint like '%pm823%'
  or complaint like '%r160%'
  or complaint like '%11-050 5TZ00%'
  or complaint like '%16-053 6BS00%'
  or complaint like '%17-069 6BT00%'
  or complaint like '%p8305%'
  or cor = '9101483'
  or cor = '8180A5'
  or cor = '8600A5'


select * from recalls where ro = '2776627'
select * from jon.recall_master_ros where ro = '2776627'
select * from ads.ext_fact_repair_order where ro = '2776627'
2776627
2775227
2776627
2775227




select *
from jon.recall_master_ros a
left join recalls b on a.ro = b.ro
where b.ro is null
order by a.ro


  select b.the_date, a.ro, a.line, a.flaghours, a.laborsales, c.opcode, 
    c.description as op_desc, d.opcode as cor, d.description as cor_desc,
    e.complaint, e.correction
  from ads.ext_fact_repair_order a
  inner join dds.dim_date b on a.opendatekey = b.date_key
  inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
  inner join ads.ext_dim_opcode d on a.corcodekey = d.opcodekey
  inner join ads.ext_dim_ccc e on a.ccckey = e.ccckey
where a.ro = '16321124' and a.line = 4


select *
from ( -- recall masters without recalls
  select a.*
  from jon.recall_master_ros a
  left join recalls b on a.ro = b.ro
  where b.ro is null) x
left join (
  select b.the_date, a.ro, a.line, a.flaghours, a.laborsales, c.opcode, 
  c.description, d.opcode as cor, d.description,
  e.complaint, e.cause, e.correction
  -- select a.*
  from ads.ext_fact_repair_order a
  inner join dds.dim_date b on a.opendatekey = b.date_key
  inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
  inner join ads.ext_dim_opcode d on a.corcodekey = d.opcodekey
  left join ads.ext_dim_ccc e on a.ccckey = e.ccckey) y on x.ro = y.ro
order by cor


  INSTALL AIR BAG INFLATOR OP.7541D1 .8 D.6EC00 S.B0K00 TEMP ID. A18002E F.P.77820-SNA-A82ZA SERIAL #25398430022079 715



select count(*), sum(total)  -- 379 /76415
from jon.recall_master_ros  

-- shit does not add up
select *, sum(total) over (partition by source)
from (
select 'all' as source, store, count(*), sum(total)  as total
from jon.recall_master_ros a 
group by store
union
select 'recall' as source, a.store, count(*), sum(total)  as total
from jon.recall_master_ros a
inner join recalls b on a.ro = b.ro
group by a.store
union
select 'no recall', a.store, count(*), sum(total)  
from jon.recall_master_ros a
left join recalls b on a.ro = b.ro
where b.ro is null
group by a.store) x
order by source, store


-- 9/28 no double counting
select ro
from jon.recall_master_ros
group by ro having count(*) > 1

-- this is where i fucked up
select ro
from recalls
group by ro
having count(*) > 1

select *
from recalls
where ro in (
  select ro
  from recalls
  group by ro
  having count(*) > 1)


-- this looks better
select *, sum(total) over (partition by source)
from (
  select 'all' as source, store, count(*), sum(total)  as total
  from jon.recall_master_ros a 
  group by store
  union
  select 'recall' as source, a.store, count(*), sum(total)  as total
  from jon.recall_master_ros a
  where exists (
    select 1
    from recalls
    where ro = a.ro)
  group by a.store
  union
  select 'no recall', a.store, count(*), sum(total)  
  from jon.recall_master_ros a
  where not exists (
    select 1
    from recalls
    where ro = a.ro)
  group by a.store) x
order by source, store


select *
from jon.recall_master_ros a
where not exists (
  select 1
  from recalls
  where ro = a.ro)
order by total desc


  
select *
from jon.recall_master_ros
where store = 'gm'
order by total desc 


select *
from ads.ext_dim_tech
where technumber = '575'

select a.*, .4 * warranty
from jon.recall_master_ros a
where ro = '16318231'

select a.*, b.account, b.description, account_type, sum(amount) over (partition by account), sum(amount) over ()
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
where control = '16318231'
  and account_type in ('sale','cogs','expense')
order by account

-- pretty close to what rm says, when all ros are included
select c.year_month, sum(-a.amount)::integer
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
inner join (
  select year_month, ro
  from jon.recall_master_ros
  where store = 'gm') c on a.control = c.ro
where post_status = 'Y'
  and account_type in ('sale','cogs','expense')
group by c.year_month


select year_month, count(*), sum(warranty)::integer as warranty, 
  sum(customer_pay)::integer as customer_pay, sum(total)::integer as total,
  (.4*sum(warranty) + .58*sum(customer_pay))::integer as rm_profit
from jon.recall_master_ros
where store = 'gm'
group by year_month




-- check some individual ros against dealertrack
select a.control, sum(-a.amount)::integer
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
inner join (
  select year_month, ro
  from jon.recall_master_ros
  where store = 'gm') c on a.control = c.ro
where post_status = 'Y'
  and account_type in ('sale','cogs','expense')
group by a.control

-- drill into some individual ros
select a.*, b.account, b.description, account_type, sum(amount) over (partition by account), sum(amount) over (partition by account_type)
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
where post_status = 'Y'
  and control = '16321732'
  and account_type in ('sale','cogs','expense')
order by account


-- 10/03 need to get something to andrew

-- tand this looks like a good place to start
select *, sum(total) over (partition by source)
from (
  select 'all' as source, store, count(*), sum(warranty)::integer as warranty, sum(customer_pay)::integer as customer_pay, sum(total)::integer  as total
  from jon.recall_master_ros a 
  group by store
  union
  select 'recall' as source, a.store, count(*), sum(warranty)::integer as warranty, sum(customer_pay)::integer as customer_pay, sum(total)::integer  as total
  from jon.recall_master_ros a
  where exists (
    select 1
    from recalls
    where ro = a.ro)
  group by a.store
  union
  select 'no recall', a.store, count(*), sum(warranty)::integer as warranty, sum(customer_pay)::integer as customer_pay, sum(total)::integer  
  from jon.recall_master_ros a
  where not exists (
    select 1
    from recalls
    where ro = a.ro)
  group by a.store) x
order by source, store


select *
from jon.recall_master_ros a 
limit 100