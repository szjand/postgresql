﻿DROP TABLE if exists jon.recall_master_ros;

CREATE TABLE jon.recall_master_ros
(
  store citext NOT NULL,
  the_date date not null,
  ro citext primary key,
  warranty numeric(8,2) NOT NULL,
  customer_pay numeric(8,2) NOT NULL,
  total numeric(8,2));

alter table jon.recall_master_ros
add column year_month integer;

update jon.recall_master_ros a
set year_month = (select year_month from dds.dim_date where the_date = a.the_date);

alter table jon.recall_master_ros
alter column year_month set not null;
  
insert into jon.recall_master_ros values ('Honda','09/14/2018','2784613',0,99.95,99.95);
insert into jon.recall_master_ros values ('Honda','09/14/2018','2784595',0,280.85,280.85);
insert into jon.recall_master_ros values ('Honda','09/14/2018','2784612',0,41.41,43.41);
insert into jon.recall_master_ros values ('Honda','09/13/2018','2784332',66.65,135.38,202.03);
insert into jon.recall_master_ros values ('Honda','09/13/2018','2784384',172.89,85.82,258.71);
insert into jon.recall_master_ros values ('Honda','09/13/2018','2784328',56.49,0,56.49);
insert into jon.recall_master_ros values ('Honda','09/13/2018','2784570',0,51.41,53.41);
insert into jon.recall_master_ros values ('Honda','09/12/2018','2783848',110.09,0,110.09);
insert into jon.recall_master_ros values ('Honda','09/12/2018','2784045',172.89,0,172.89);
insert into jon.recall_master_ros values ('Honda','09/12/2018','2784062',325.53,0,325.53);
insert into jon.recall_master_ros values ('Honda','09/12/2018','2783985',159.57,2091.16,2250.73);
insert into jon.recall_master_ros values ('Honda','09/11/2018','2784358',0,39.55,41.55);
insert into jon.recall_master_ros values ('Honda','09/08/2018','2784314',0,51.41,53.41);
insert into jon.recall_master_ros values ('Honda','09/08/2018','2784292',0,51.41,53.41);
insert into jon.recall_master_ros values ('Honda','09/08/2018','2784311',0,51.41,53.41);
insert into jon.recall_master_ros values ('Honda','09/07/2018','2784189',0,31.41,33.41);
insert into jon.recall_master_ros values ('Honda','09/07/2018','2784221',0,62.93,64.93);
insert into jon.recall_master_ros values ('Honda','09/04/2018','2783928',0,59.65,61.65);
insert into jon.recall_master_ros values ('Honda','08/31/2018','2783348',66.65,610.84,677.49);
insert into jon.recall_master_ros values ('Honda','08/31/2018','2783402',172.89,673.49,846.38);
insert into jon.recall_master_ros values ('Honda','08/31/2018','2783339',93.04,59.06,152.1);
insert into jon.recall_master_ros values ('Honda','08/31/2018','2783784',146.64,0,146.64);
insert into jon.recall_master_ros values ('Honda','08/31/2018','2783612',151.09,70.74,221.83);
insert into jon.recall_master_ros values ('Honda','08/31/2018','2783405',93.04,0,93.04);
insert into jon.recall_master_ros values ('Honda','08/31/2018','2783823',161.99,248.02,410.01);
insert into jon.recall_master_ros values ('Honda','08/30/2018','2783790',0,39.55,41.55);
insert into jon.recall_master_ros values ('Honda','08/27/2018','2783530',0,93.39,95.39);
insert into jon.recall_master_ros values ('Honda','08/27/2018','2783565',0,51.41,53.41);
insert into jon.recall_master_ros values ('Honda','08/25/2018','2783505',0,50.01,52.01);
insert into jon.recall_master_ros values ('Honda','08/24/2018','2783449',0,67.96,69.96);
insert into jon.recall_master_ros values ('Honda','08/24/2018','2783408',0,275.36,275.36);
insert into jon.recall_master_ros values ('Honda','08/24/2018','2783456',0,97.28,99.28);
insert into jon.recall_master_ros values ('Honda','08/23/2018','2783186',163.54,0,163.54);
insert into jon.recall_master_ros values ('Honda','08/23/2018','2783295',0,33.5,35.5);
insert into jon.recall_master_ros values ('Honda','08/22/2018','2783257',161.99,0,161.99);
insert into jon.recall_master_ros values ('Honda','08/22/2018','2783282',0,982.69,982.69);
insert into jon.recall_master_ros values ('Honda','08/21/2018','2783185',0,271.5,271.5);
insert into jon.recall_master_ros values ('Honda','08/20/2018','2783078',0,50.01,52.01);
insert into jon.recall_master_ros values ('Honda','08/20/2018','2782998',161.99,0,161.99);
insert into jon.recall_master_ros values ('Honda','08/20/2018','2783136',0,50.01,52.01);
insert into jon.recall_master_ros values ('Honda','08/20/2018','2782907',138.33,56.18,194.51);
insert into jon.recall_master_ros values ('Honda','08/16/2018','2782769',161.99,0,161.99);
insert into jon.recall_master_ros values ('Honda','08/16/2018','2782836',577.93,0,577.93);
insert into jon.recall_master_ros values ('Honda','08/16/2018','2782922',66.65,0,66.65);
insert into jon.recall_master_ros values ('Honda','08/16/2018','2782973',0,76.77,78.77);
insert into jon.recall_master_ros values ('Honda','08/16/2018','2782943',0,28.16,30.16);
insert into jon.recall_master_ros values ('Honda','08/15/2018','2782837',0,77.77,77.77);
insert into jon.recall_master_ros values ('Honda','08/14/2018','2782682',110.09,67.96,178.05);
insert into jon.recall_master_ros values ('Honda','08/14/2018','2782554',161.99,0,161.99);
insert into jon.recall_master_ros values ('Honda','08/14/2018','2782744',0,99.95,99.95);
insert into jon.recall_master_ros values ('Honda','08/14/2018','2782551',161.99,0,161.99);
insert into jon.recall_master_ros values ('Honda','08/14/2018','2782547',56.49,0,56.49);
insert into jon.recall_master_ros values ('Honda','08/11/2018','2782620',0,84.71,86.71);
insert into jon.recall_master_ros values ('Honda','08/10/2018','2782450',172.89,0,172.89);
insert into jon.recall_master_ros values ('Honda','08/08/2018','2782372',161.99,188.92,350.91);
insert into jon.recall_master_ros values ('Honda','08/08/2018','2782278',66.65,0,66.65);
insert into jon.recall_master_ros values ('Honda','08/08/2018','2782389',154.86,225.35,380.21);
insert into jon.recall_master_ros values ('Honda','08/08/2018','2782442',0,50.01,52.01);
insert into jon.recall_master_ros values ('Honda','08/08/2018','2782358',138.33,101.28,239.61);
insert into jon.recall_master_ros values ('Honda','08/08/2018','2782270',93.04,0,93.04);
insert into jon.recall_master_ros values ('Honda','08/08/2018','2782449',0,38.16,40.16);
insert into jon.recall_master_ros values ('Honda','08/08/2018','2782340',347.33,0,347.33);
insert into jon.recall_master_ros values ('Honda','08/07/2018','2782309',0,92.38,94.38);
insert into jon.recall_master_ros values ('Honda','08/07/2018','2781966',66.65,0,66.65);
insert into jon.recall_master_ros values ('Honda','08/07/2018','2782118',172.89,476.19,649.08);
insert into jon.recall_master_ros values ('Honda','08/07/2018','2782103',93.04,50.01,177.46);
insert into jon.recall_master_ros values ('Honda','08/07/2018','2782197',56.49,66.02,122.51);
insert into jon.recall_master_ros values ('Honda','08/07/2018','2782119',161.99,0,161.99);
insert into jon.recall_master_ros values ('Honda','08/06/2018','2782273',0,38.16,40.16);
insert into jon.recall_master_ros values ('Honda','08/03/2018','2782151',0,24.99,24.99);
insert into jon.recall_master_ros values ('Honda','08/03/2018','2782138',0,148.93,148.93);
insert into jon.recall_master_ros values ('Honda','08/03/2018','2781801',0,664.94,664.94);
insert into jon.recall_master_ros values ('Honda','08/02/2018','2782105',0,67.96,69.96);
insert into jon.recall_master_ros values ('Honda','08/02/2018','2782009',0,69.33,71.33);
insert into jon.recall_master_ros values ('Honda','08/02/2018','2781915',0,848.32,850.32);
insert into jon.recall_master_ros values ('Honda','08/02/2018','2782006',0,67.96,69.96);
insert into jon.recall_master_ros values ('Honda','08/02/2018','2782030',0,67.96,69.96);
insert into jon.recall_master_ros values ('Honda','07/31/2018','2781884',161.99,0,161.99);
insert into jon.recall_master_ros values ('Honda','07/31/2018','2781737',56.49,0,56.49);
insert into jon.recall_master_ros values ('Honda','07/31/2018','2781803',56.49,204.71,261.2);
insert into jon.recall_master_ros values ('Honda','07/30/2018','2781800',161.99,0,161.99);
insert into jon.recall_master_ros values ('Honda','07/30/2018','2781857',0,76.77,78.77);
insert into jon.recall_master_ros values ('Honda','07/30/2018','2781838',0,149.46,151.46);
insert into jon.recall_master_ros values ('Honda','07/29/2018','2781340',66.65,79.23,145.88);
insert into jon.recall_master_ros values ('Honda','07/29/2018','2781414',66.65,0,66.65);
insert into jon.recall_master_ros values ('Honda','07/29/2018','2781244',56.49,0,56.49);
insert into jon.recall_master_ros values ('Honda','07/28/2018','2781791',0,38.16,40.16);
insert into jon.recall_master_ros values ('Honda','07/27/2018','2781494',172.89,804.7,977.59);
insert into jon.recall_master_ros values ('Honda','07/27/2018','2781569',161.99,67.96,229.95);
insert into jon.recall_master_ros values ('Honda','07/27/2018','2781468',93.04,101.28,194.32);
insert into jon.recall_master_ros values ('Honda','07/25/2018','2781537',0,66.77,68.77);
insert into jon.recall_master_ros values ('Honda','07/24/2018','2781353',0,50.01,52.01);
insert into jon.recall_master_ros values ('Honda','07/23/2018','2780674',172.89,0,172.89);
insert into jon.recall_master_ros values ('Honda','07/23/2018','2781435',151.09,0,151.09);
insert into jon.recall_master_ros values ('Honda','07/23/2018','2781018',93.04,0,93.04);
insert into jon.recall_master_ros values ('Honda','07/23/2018','2781006',161.99,0,161.99);
insert into jon.recall_master_ros values ('Honda','07/20/2018','2781245',0,194.27,194.27);
insert into jon.recall_master_ros values ('Honda','07/20/2018','2781108',0,256.35,256.35);
insert into jon.recall_master_ros values ('Honda','07/20/2018','2780038',0,638.81,638.81);
insert into jon.recall_master_ros values ('Honda','07/19/2018','2780951',0,30.31,30.31);
insert into jon.recall_master_ros values ('Honda','07/18/2018','2780783',161.99,0,161.99);
insert into jon.recall_master_ros values ('Honda','07/18/2018','2780973',172.89,0,172.89);
insert into jon.recall_master_ros values ('Honda','07/18/2018','2780900',161.99,193.67,355.66);
insert into jon.recall_master_ros values ('Honda','07/17/2018','2780992',0,79.33,81.33);
insert into jon.recall_master_ros values ('Honda','07/17/2018','2780986',0,191.12,191.12);
insert into jon.recall_master_ros values ('Honda','07/16/2018','2780579',172.89,859.21,1032.1);
insert into jon.recall_master_ros values ('Honda','07/16/2018','2780421',161.99,0,163.99);
insert into jon.recall_master_ros values ('Honda','07/16/2018','2780955',0,50.01,52.01);
insert into jon.recall_master_ros values ('Honda','07/13/2018','2780589',138.33,50.01,188.34);
insert into jon.recall_master_ros values ('Honda','07/13/2018','2780411',56.49,0,56.49);
insert into jon.recall_master_ros values ('Honda','07/13/2018','2780784',0,108.29,110.29);
insert into jon.recall_master_ros values ('Honda','07/13/2018','2780518',88.45,0,88.45);
insert into jon.recall_master_ros values ('Honda','07/13/2018','2780828',0,33.5,35.5);
insert into jon.recall_master_ros values ('Honda','07/12/2018','2780672',0,66.02,66.02);
insert into jon.recall_master_ros values ('Honda','07/12/2018','2780682',0,236.15,236.15);
insert into jon.recall_master_ros values ('Honda','07/11/2018','2780615',0,492.64,492.64);
insert into jon.recall_master_ros values ('Honda','07/10/2018','2779975',138.96,0,138.96);
insert into jon.recall_master_ros values ('Honda','07/10/2018','2780172',325.53,123.55,451.08);
insert into jon.recall_master_ros values ('Honda','07/10/2018','2780206',0,50.01,52.01);
insert into jon.recall_master_ros values ('Honda','07/10/2018','2780274',161.99,38.16,200.15);
insert into jon.recall_master_ros values ('Honda','07/10/2018','2780061',161.99,0,161.99);
insert into jon.recall_master_ros values ('Honda','07/10/2018','2780472',0,120.15,122.15);
insert into jon.recall_master_ros values ('Honda','07/10/2018','2780009',56.49,50.01,106.5);
insert into jon.recall_master_ros values ('Honda','07/09/2018','2780447',0,473.26,473.26);
insert into jon.recall_master_ros values ('Honda','07/09/2018','2779972',110.09,26.76,136.85);
insert into jon.recall_master_ros values ('Honda','07/06/2018','2780258',0,269.33,269.33);
insert into jon.recall_master_ros values ('Honda','07/06/2018','2780323',0,93.39,95.39);
insert into jon.recall_master_ros values ('Honda','07/05/2018','2780195',0,100.61,100.61);
insert into jon.recall_master_ros values ('Honda','07/05/2018','2780190',0,66.02,66.02);
insert into jon.recall_master_ros values ('Honda','07/05/2018','2780204',0,34.34,36.34);
insert into jon.recall_master_ros values ('Honda','07/03/2018','2780130',0,97.28,99.28);
insert into jon.recall_master_ros values ('Honda','06/30/2018','2779914',172.89,0,172.89);
insert into jon.recall_master_ros values ('Honda','06/30/2018','2779903',56.49,275.88,332.37);
insert into jon.recall_master_ros values ('Honda','06/29/2018','2776696',93.04,0,93.04);
insert into jon.recall_master_ros values ('Honda','06/29/2018','2779759',161.99,0,161.99);
insert into jon.recall_master_ros values ('Honda','06/29/2018','2779945',0,38.16,40.16);
insert into jon.recall_master_ros values ('Honda','06/29/2018','2775227',93.04,50.01,143.05);
insert into jon.recall_master_ros values ('Honda','06/29/2018','2779898',0,38.16,40.16);
insert into jon.recall_master_ros values ('Honda','06/29/2018','2776627',93.04,595.29,688.33);
insert into jon.recall_master_ros values ('Honda','06/29/2018','2779692',115.99,31.82,149.81);
insert into jon.recall_master_ros values ('Honda','06/29/2018','2779917',0,38.16,40.16);
insert into jon.recall_master_ros values ('Honda','06/28/2018','2779838',0,260.77,260.77);
insert into jon.recall_master_ros values ('Honda','06/28/2018','2776775',56.49,0,56.49);
insert into jon.recall_master_ros values ('Honda','06/28/2018','2779690',0,50.01,52.01);
insert into jon.recall_master_ros values ('Honda','06/27/2018','2779563',56.49,50.01,106.5);
insert into jon.recall_master_ros values ('Honda','06/26/2018','2779696',0,99.95,99.95);
insert into jon.recall_master_ros values ('Honda','06/25/2018','2779398',471.35,273.42,744.77);
insert into jon.recall_master_ros values ('Honda','06/25/2018','2779442',172.89,0,172.89);
insert into jon.recall_master_ros values ('Honda','06/25/2018','2779226',161.99,547.25,709.24);
insert into jon.recall_master_ros values ('Honda','06/25/2018','2779295',161.99,0,161.99);
insert into jon.recall_master_ros values ('Honda','06/25/2018','2779481',161.99,0,161.99);
insert into jon.recall_master_ros values ('Honda','06/25/2018','2779330',0,93.39,95.39);
insert into jon.recall_master_ros values ('Honda','06/25/2018','2779512',56.49,0,56.49);
insert into jon.recall_master_ros values ('Honda','06/22/2018','2779441',0,38.16,40.16);
insert into jon.recall_master_ros values ('Honda','06/22/2018','2779109',0,108.71,108.71);
insert into jon.recall_master_ros values ('Honda','06/20/2018','2778853',32.7,0,32.7);
insert into jon.recall_master_ros values ('Honda','06/20/2018','2779219',161.99,176.8,338.79);
insert into jon.recall_master_ros values ('Honda','06/20/2018','2779018',161.99,0,161.99);
insert into jon.recall_master_ros values ('Honda','06/20/2018','2779135',56.49,82.53,139.02);
insert into jon.recall_master_ros values ('Honda','06/20/2018','2779250',161.99,50.01,212);
insert into jon.recall_master_ros values ('Honda','06/20/2018','2779028',56.49,0,56.49);
insert into jon.recall_master_ros values ('Honda','06/20/2018','2779220',56.49,151.3,207.79);
insert into jon.recall_master_ros values ('Honda','06/20/2018','2778946',93.04,0,93.04);
insert into jon.recall_master_ros values ('Honda','06/20/2018','2778854',56.49,0,56.49);
insert into jon.recall_master_ros values ('Honda','06/20/2018','2779026',146.64,85.82,232.46);
insert into jon.recall_master_ros values ('Honda','06/20/2018','2779094',43.6,0,43.6);
insert into jon.recall_master_ros values ('Honda','06/20/2018','2778939',56.49,0,56.49);
insert into jon.recall_master_ros values ('Honda','06/18/2018','2778715',93.04,0,93.04);
insert into jon.recall_master_ros values ('Honda','06/18/2018','2779012',0,32.44,34.44);
insert into jon.recall_master_ros values ('Honda','06/18/2018','2778792',172.89,0,172.89);
insert into jon.recall_master_ros values ('Honda','06/18/2018','2778659',0,50.01,52.01);
insert into jon.recall_master_ros values ('Honda','06/18/2018','2778909',0,67.96,69.96);
insert into jon.recall_master_ros values ('Honda','06/18/2018','2778550',0,76.77,78.77);
insert into jon.recall_master_ros values ('Honda','06/18/2018','2778761',56.49,0,56.49);
insert into jon.recall_master_ros values ('Honda','06/18/2018','2778880',0,50.01,52.01);
insert into jon.recall_master_ros values ('Nissan','09/07/2018','2784227',0,83.9,83.9);
insert into jon.recall_master_ros values ('Nissan','09/05/2018','2784054',0,79.33,81.33);
insert into jon.recall_master_ros values ('Nissan','09/04/2018','2784026',0,50.01,52.01);
insert into jon.recall_master_ros values ('Nissan','08/31/2018','2783625',197.78,0,197.78);
insert into jon.recall_master_ros values ('Nissan','08/31/2018','2783446',111.55,52.72,173.58);
insert into jon.recall_master_ros values ('Nissan','08/31/2018','2783382',220.96,356.88,577.84);
insert into jon.recall_master_ros values ('Nissan','08/31/2018','2783353',61.4,50.01,111.41);
insert into jon.recall_master_ros values ('Nissan','08/24/2018','2783411',0,96.51,98.51);
insert into jon.recall_master_ros values ('Nissan','08/23/2018','2783197',239.2,35,274.2);
insert into jon.recall_master_ros values ('Nissan','08/23/2018','2783113',416,0,416);
insert into jon.recall_master_ros values ('Nissan','08/16/2018','2782687',1220.47,0,1220.47);
insert into jon.recall_master_ros values ('Nissan','08/16/2018','2782758',37.3,56.11,93.41);
insert into jon.recall_master_ros values ('Nissan','08/16/2018','2782817',441.5,0,441.5);
insert into jon.recall_master_ros values ('Nissan','08/16/2018','2782841',416,0,416);
insert into jon.recall_master_ros values ('Nissan','08/14/2018','2782675',441.5,0,441.5);
insert into jon.recall_master_ros values ('Nissan','08/14/2018','2782655',57.23,0,57.23);
insert into jon.recall_master_ros values ('Nissan','08/14/2018','2782484',220.96,50.01,270.97);
insert into jon.recall_master_ros values ('Nissan','08/10/2018','2782172',0,737.75,737.75);
insert into jon.recall_master_ros values ('Nissan','08/07/2018','2782066',55.95,0,55.95);
insert into jon.recall_master_ros values ('Nissan','08/07/2018','2782003',27.98,0,27.98);
insert into jon.recall_master_ros values ('Nissan','08/07/2018','2782039',37.3,44.68,81.98);
insert into jon.recall_master_ros values ('Nissan','08/07/2018','2782165',441.5,416.97,858.47);
insert into jon.recall_master_ros values ('Nissan','08/04/2018','2782210',0,117.9,117.9);
insert into jon.recall_master_ros values ('Nissan','07/31/2018','2781771',21.32,0,21.32);
insert into jon.recall_master_ros values ('Nissan','07/31/2018','2781376',489.28,114.32,603.6);
insert into jon.recall_master_ros values ('Nissan','07/31/2018','2781819',37.3,832.44,869.74);
insert into jon.recall_master_ros values ('Nissan','07/28/2018','2781786',0,38.16,40.16);
insert into jon.recall_master_ros values ('Nissan','07/27/2018','2781470',441.5,0,441.5);
insert into jon.recall_master_ros values ('Nissan','07/27/2018','2781466',441.5,0,441.5);
insert into jon.recall_master_ros values ('Nissan','07/27/2018','2781614',37.3,99.07,136.37);
insert into jon.recall_master_ros values ('Nissan','07/27/2018','2781696',66.74,59.51,126.25);
insert into jon.recall_master_ros values ('Nissan','07/27/2018','2781748',0,50.01,52.01);
insert into jon.recall_master_ros values ('Nissan','07/27/2018','2781535',27.98,0,27.98);
insert into jon.recall_master_ros values ('Nissan','07/27/2018','2781543',38.58,0,38.58);
insert into jon.recall_master_ros values ('Nissan','07/24/2018','2781422',0,96.51,98.51);
insert into jon.recall_master_ros values ('Nissan','07/24/2018','2781270',121.23,56.18,177.41);
insert into jon.recall_master_ros values ('Nissan','07/24/2018','2781377',245.35,201.58,446.93);
insert into jon.recall_master_ros values ('Nissan','07/23/2018','2781007',65.28,0,65.28);
insert into jon.recall_master_ros values ('Nissan','07/23/2018','2781109',135.93,146.72,282.65);
insert into jon.recall_master_ros values ('Nissan','07/23/2018','2781149',135.93,394.84,530.77);
insert into jon.recall_master_ros values ('Nissan','07/23/2018','2781425',0,38.16,40.16);
insert into jon.recall_master_ros values ('Nissan','07/19/2018','2781171',0,94.72,96.72);
insert into jon.recall_master_ros values ('Nissan','07/18/2018','2780823',37.3,0,37.3);
insert into jon.recall_master_ros values ('Nissan','07/18/2018','2780850',196.15,0,196.15);
insert into jon.recall_master_ros values ('Nissan','07/17/2018','2780969',0,55.77,57.77);
insert into jon.recall_master_ros values ('Nissan','07/17/2018','2781025',0,38.16,40.16);
insert into jon.recall_master_ros values ('Nissan','07/16/2018','2780958',0,171.89,173.89);
insert into jon.recall_master_ros values ('Nissan','07/13/2018','2780800',0,41.49,41.49);
insert into jon.recall_master_ros values ('Nissan','07/13/2018','2780581',239.2,448.86,688.06);
insert into jon.recall_master_ros values ('Nissan','07/12/2018','2780713',0,32.53,34.53);
insert into jon.recall_master_ros values ('Nissan','07/10/2018','2780183',314.3,0,314.3);
insert into jon.recall_master_ros values ('Nissan','07/10/2018','2780024',239.2,35,274.2);
insert into jon.recall_master_ros values ('Nissan','07/10/2018','2780049',146.23,664.6,810.83);
insert into jon.recall_master_ros values ('Nissan','07/10/2018','2780541',0,50.01,52.01);
insert into jon.recall_master_ros values ('Nissan','07/10/2018','2780366',441.5,0,441.5);
insert into jon.recall_master_ros values ('Nissan','07/09/2018','2780110',119.79,16.8,136.59);
insert into jon.recall_master_ros values ('GM','06/15/2018','19311133',0,29.95,29.95);
insert into jon.recall_master_ros values ('GM','06/15/2018','19310987',0,39.95,41.95);
insert into jon.recall_master_ros values ('GM','06/15/2018','19311013',0,64.38,66.38);
insert into jon.recall_master_ros values ('GM','06/15/2018','19311078',0,57.73,59.73);
insert into jon.recall_master_ros values ('GM','06/15/2018','19310972',0,39.95,41.95);
insert into jon.recall_master_ros values ('GM','06/18/2018','16315444',0,127.1,127.1);
insert into jon.recall_master_ros values ('GM','06/18/2018','19311270',0,54.38,54.38);
insert into jon.recall_master_ros values ('GM','06/19/2018','16315251',421.69,0,421.69);
insert into jon.recall_master_ros values ('GM','06/20/2018','19311538',0,29.95,29.95);
insert into jon.recall_master_ros values ('GM','06/21/2018','16315605',113.74,0,113.74);
insert into jon.recall_master_ros values ('GM','06/22/2018','19311362',0,80.24,82.24);
insert into jon.recall_master_ros values ('GM','06/22/2018','19311199',0,54.95,56.95);
insert into jon.recall_master_ros values ('GM','06/22/2018','19311200',0,57.9,59.9);
insert into jon.recall_master_ros values ('GM','06/22/2018','19311819',0,44.95,44.95);
insert into jon.recall_master_ros values ('GM','06/25/2018','16315920',45.5,0,45.5);
insert into jon.recall_master_ros values ('GM','06/26/2018','19311938',0,39.95,41.95);
insert into jon.recall_master_ros values ('GM','06/27/2018','19312167',0,39.95,41.95);
insert into jon.recall_master_ros values ('GM','06/27/2018','19312284',0,15.55,15.55);
insert into jon.recall_master_ros values ('GM','06/28/2018','16315718',109.23,0,109.23);
insert into jon.recall_master_ros values ('GM','06/28/2018','16316012',34.12,0,34.12);
insert into jon.recall_master_ros values ('GM','06/29/2018','16316331',45.5,0,45.5);
insert into jon.recall_master_ros values ('GM','06/29/2018','19312395',0,41.77,43.77);
insert into jon.recall_master_ros values ('GM','06/30/2018','16316611',72.24,0,72.24);
insert into jon.recall_master_ros values ('GM','06/30/2018','16316719',47.78,0,47.78);
insert into jon.recall_master_ros values ('GM','06/30/2018','16316618',350.56,0,350.56);
insert into jon.recall_master_ros values ('GM','06/30/2018','16316577',2155.47,66.28,2221.75);
insert into jon.recall_master_ros values ('GM','06/30/2018','19312435',0,43.58,45.58);
insert into jon.recall_master_ros values ('GM','06/30/2018','16316730',34.12,0,34.12);
insert into jon.recall_master_ros values ('GM','06/30/2018','19312504',0,79.92,81.92);
insert into jon.recall_master_ros values ('GM','07/02/2018','16316846',0,43.62,43.62);
insert into jon.recall_master_ros values ('GM','07/02/2018','19312586',0,57.73,59.73);
insert into jon.recall_master_ros values ('GM','07/03/2018','16316819',207.35,0,207.35);
insert into jon.recall_master_ros values ('GM','07/03/2018','16316825',0,24.95,24.95);
insert into jon.recall_master_ros values ('GM','07/03/2018','19312730',0,29.95,29.95);
insert into jon.recall_master_ros values ('GM','07/03/2018','16316790',55.12,852.21,907.33);
insert into jon.recall_master_ros values ('GM','07/03/2018','16316917',0,398.59,398.59);
insert into jon.recall_master_ros values ('GM','07/05/2018','16316944',350.56,0,350.56);
insert into jon.recall_master_ros values ('GM','07/05/2018','16316659',1296.74,0,1296.74);
insert into jon.recall_master_ros values ('GM','07/05/2018','16316899',101.85,0,101.85);
insert into jon.recall_master_ros values ('GM','07/05/2018','16316963',282.89,0,282.89);
insert into jon.recall_master_ros values ('GM','07/05/2018','16314351',0,509.91,509.91);
insert into jon.recall_master_ros values ('GM','07/06/2018','19313021',0,29.95,29.95);
insert into jon.recall_master_ros values ('GM','07/09/2018','19313102',0,41.77,43.77);
insert into jon.recall_master_ros values ('GM','07/09/2018','16316783',0,406.37,406.37);
insert into jon.recall_master_ros values ('GM','07/09/2018','19313111',0,57.73,59.73);
insert into jon.recall_master_ros values ('GM','07/10/2018','16317313',56.87,0,56.87);
insert into jon.recall_master_ros values ('GM','07/10/2018','19313264',0,72.9,74.9);
insert into jon.recall_master_ros values ('GM','07/10/2018','16317439',0,115.93,115.93);
insert into jon.recall_master_ros values ('GM','07/10/2018','19313355',0,54.38,54.38);
insert into jon.recall_master_ros values ('GM','07/11/2018','16317225',235.11,0,235.11);
insert into jon.recall_master_ros values ('GM','07/11/2018','19313376',0,39.95,41.95);
insert into jon.recall_master_ros values ('GM','07/11/2018','16317227',34.12,0,34.12);
insert into jon.recall_master_ros values ('GM','07/11/2018','16317329',34.12,0,34.12);
insert into jon.recall_master_ros values ('GM','07/12/2018','19313490',0,54.95,56.95);
insert into jon.recall_master_ros values ('GM','07/16/2018','16317928',0,235.86,235.86);
insert into jon.recall_master_ros values ('GM','07/16/2018','16317913',234.8,0,234.8);
insert into jon.recall_master_ros values ('GM','07/16/2018','19313904',0,44.95,44.95);
insert into jon.recall_master_ros values ('GM','07/16/2018','16317899',161.52,404.22,565.74);
insert into jon.recall_master_ros values ('GM','07/16/2018','19313882',0,39.95,41.95);
insert into jon.recall_master_ros values ('GM','07/17/2018','19314069',0,59.38,61.38);
insert into jon.recall_master_ros values ('GM','07/17/2018','16316736',350.56,0,350.56);
insert into jon.recall_master_ros values ('GM','07/18/2018','19314149',0,47.73,47.73);
insert into jon.recall_master_ros values ('GM','07/19/2018','19314168',0,41.37,43.37);
insert into jon.recall_master_ros values ('GM','07/20/2018','16317816',101.85,414.79,516.64);
insert into jon.recall_master_ros values ('GM','07/20/2018','16318325',0,127.1,127.1);
insert into jon.recall_master_ros values ('GM','07/20/2018','16316367',257.87,0,257.87);
insert into jon.recall_master_ros values ('GM','07/20/2018','16318231',386.6,0,386.6);
insert into jon.recall_master_ros values ('GM','07/21/2018','19314565',0,44.95,46.95);
insert into jon.recall_master_ros values ('GM','07/21/2018','19314556',0,29.95,29.95);
insert into jon.recall_master_ros values ('GM','07/21/2018','19314550',0,54.38,54.38);
insert into jon.recall_master_ros values ('GM','07/24/2018','19314797',0,39.95,41.95);
insert into jon.recall_master_ros values ('GM','07/24/2018','19314822',0,29.95,29.95);
insert into jon.recall_master_ros values ('GM','07/24/2018','16318645',34.12,0,34.12);
insert into jon.recall_master_ros values ('GM','07/27/2018','16318742',56.87,130.39,187.26);
insert into jon.recall_master_ros values ('GM','07/27/2018','19315160',0,41.77,43.77);
insert into jon.recall_master_ros values ('GM','07/27/2018','16318784',45.5,468.92,514.42);
insert into jon.recall_master_ros values ('GM','07/27/2018','16318929',174.85,288.73,463.58);
insert into jon.recall_master_ros values ('GM','07/27/2018','19315032',0,39.95,41.95);
insert into jon.recall_master_ros values ('GM','07/30/2018','16319234',178.56,0,178.56);
insert into jon.recall_master_ros values ('GM','07/30/2018','19315427',0,29.95,29.95);
insert into jon.recall_master_ros values ('GM','07/31/2018','16319341',121.54,0,121.54);
insert into jon.recall_master_ros values ('GM','07/31/2018','16317474',791.84,0,791.84);
insert into jon.recall_master_ros values ('GM','07/31/2018','19315526',0,54.95,56.95);
insert into jon.recall_master_ros values ('GM','07/31/2018','19315592',0,29.95,29.95);
insert into jon.recall_master_ros values ('GM','07/31/2018','19315435',0,54.95,56.95);
insert into jon.recall_master_ros values ('GM','08/01/2018','19315553',0,39.95,41.95);
insert into jon.recall_master_ros values ('GM','08/02/2018','16319655',350.56,0,350.56);
insert into jon.recall_master_ros values ('GM','08/03/2018','19315940',0,64.38,66.38);
insert into jon.recall_master_ros values ('GM','08/06/2018','16318941',121.54,270.91,392.45);
insert into jon.recall_master_ros values ('GM','08/06/2018','16320032',97.17,20.96,118.13);
insert into jon.recall_master_ros values ('GM','08/06/2018','19316213',0,114.95,114.95);
insert into jon.recall_master_ros values ('GM','08/06/2018','19315973',0,54.95,56.95);
insert into jon.recall_master_ros values ('GM','08/07/2018','16319784',121.54,0,121.54);
insert into jon.recall_master_ros values ('GM','08/07/2018','19316196',0,57.9,59.9);
insert into jon.recall_master_ros values ('GM','08/09/2018','16320277',38.07,0,38.07);
insert into jon.recall_master_ros values ('GM','08/09/2018','19316434',0,34.95,36.95);
insert into jon.recall_master_ros values ('GM','08/10/2018','16320197',0,664.51,664.51);
insert into jon.recall_master_ros values ('GM','08/13/2018','16320594',178.56,0,178.56);
insert into jon.recall_master_ros values ('GM','08/14/2018','16320697',97.17,0,97.17);
insert into jon.recall_master_ros values ('GM','08/14/2018','16320705',22.75,454.37,477.12);
insert into jon.recall_master_ros values ('GM','08/14/2018','16320495',521.52,0,521.52);
insert into jon.recall_master_ros values ('GM','08/14/2018','16320717',34.12,73.1,107.22);
insert into jon.recall_master_ros values ('GM','08/15/2018','19317012',0,54.95,56.95);
insert into jon.recall_master_ros values ('GM','08/15/2018','19317085',0,82.33,84.33);
insert into jon.recall_master_ros values ('GM','08/15/2018','16318166',50.51,80.05,130.56);
insert into jon.recall_master_ros values ('GM','08/16/2018','19317288',0,29.95,29.95);
insert into jon.recall_master_ros values ('GM','08/16/2018','19317215',0,47.73,47.73);
insert into jon.recall_master_ros values ('GM','08/17/2018','19317282',0,39.95,41.95);
insert into jon.recall_master_ros values ('GM','08/20/2018','19317632',0,29.95,29.95);
insert into jon.recall_master_ros values ('GM','08/21/2018','19317542',0,72.9,74.9);
insert into jon.recall_master_ros values ('GM','08/21/2018','19317651',0,65.68,65.68);
insert into jon.recall_master_ros values ('GM','08/21/2018','16321423',34.12,0,34.12);
insert into jon.recall_master_ros values ('GM','08/21/2018','16321413',47.78,0,47.78);
insert into jon.recall_master_ros values ('GM','08/22/2018','16321732',0,189,189);
insert into jon.recall_master_ros values ('GM','08/22/2018','19317771',0,25.24,27.24);
insert into jon.recall_master_ros values ('GM','08/22/2018','19317826',0,57.73,59.73);
insert into jon.recall_master_ros values ('GM','08/22/2018','19317831',0,54.95,56.95);
insert into jon.recall_master_ros values ('GM','08/22/2018','19317740',0,54.95,56.95);
insert into jon.recall_master_ros values ('GM','08/23/2018','16321786',0,99.95,99.95);
insert into jon.recall_master_ros values ('GM','08/24/2018','16321683',160.63,0,160.63);
insert into jon.recall_master_ros values ('GM','08/24/2018','19318097',0,47.9,47.9);
insert into jon.recall_master_ros values ('GM','08/24/2018','19318030',0,31.77,31.77);
insert into jon.recall_master_ros values ('GM','08/24/2018','16319551',0,1525.05,1551.47);
insert into jon.recall_master_ros values ('GM','08/27/2018','16317630',0,514.41,514.41);
insert into jon.recall_master_ros values ('GM','08/27/2018','19318283',0,81.35,83.35);
insert into jon.recall_master_ros values ('GM','08/28/2018','19318333',0,41.77,43.77);
insert into jon.recall_master_ros values ('GM','08/28/2018','19318383',0,170.66,172.66);
insert into jon.recall_master_ros values ('GM','08/28/2018','19318364',0,124.95,126.95);
insert into jon.recall_master_ros values ('GM','08/28/2018','19318429',0,44.95,44.95);
insert into jon.recall_master_ros values ('GM','08/29/2018','16322106',0,892.24,892.24);
insert into jon.recall_master_ros values ('GM','08/30/2018','16322520',121.54,0,121.54);
insert into jon.recall_master_ros values ('GM','08/30/2018','16321734',219.4,482.22,701.62);
insert into jon.recall_master_ros values ('GM','08/31/2018','16322018',245.38,0,245.38);
insert into jon.recall_master_ros values ('GM','08/31/2018','16321124',123.57,1250.38,1373.95);
insert into jon.recall_master_ros values ('GM','08/31/2018','16322139',0,222.52,222.52);
insert into jon.recall_master_ros values ('GM','08/31/2018','16322356',0,197.25,197.25);
insert into jon.recall_master_ros values ('GM','09/04/2018','18062081',0,246.75,246.75);
insert into jon.recall_master_ros values ('GM','09/05/2018','19319054',0,75.68,77.68);
insert into jon.recall_master_ros values ('GM','09/05/2018','19319154',0,31.77,31.77);
insert into jon.recall_master_ros values ('GM','09/06/2018','16322893',235.11,0,235.11);
insert into jon.recall_master_ros values ('GM','09/11/2018','19319653',0,32.9,32.9);
insert into jon.recall_master_ros values ('GM','09/11/2018','19319658',47.78,0,47.78);
insert into jon.recall_master_ros values ('GM','09/12/2018','16323739',0,285.08,285.08);
insert into jon.recall_master_ros values ('GM','09/12/2018','19319616',0,39.95,41.95);
insert into jon.recall_master_ros values ('GM','09/13/2018','19319893',0,97.69,97.69);
insert into jon.recall_master_ros values ('GM','09/14/2018','16323966',178.56,0,178.56);
insert into jon.recall_master_ros values ('GM','09/14/2018','16323949',34.12,0,34.12);

  