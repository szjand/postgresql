﻿full_shop.py -- uses some ads.ext
sc_payroll_201803.py -- uses some ads.ext_


---------------------------------------------------------
--</ 07/25/22 from team_pay\test_data_comparison.sql
made this changes on 7/25/22, so team pay should be good
---------------------------------------------------------
-- all i really want to compare is tech_flag_hours_day
-- perfect
select sum(tech_clock_hours_day) as clock, sum(tech_flag_hours_day) as flag  -- 35895.49,50722.59
from tp.data
where the_date between '02/01/2022' and '06/30/2022'

select sum(tech_clock_hours_day) as clock, sum(tech_flag_hours_day) as flag  -- 6802.76,10133.26
from tp.test_data
where the_date between '02/01/2022' and '06/30/2022'

select max(thedate) from ads.ext_tp_data

so. what is the transition

so, currently, scraping advantage into tp.data
and nightly running team_pay.py DataUpdateNightly to populate tp.test_data

this also means that i now need to reconstruct updating team pay values in postgresql (teams,values, etc)

so it looks like all i need to do is to
	1.stop scraping advantage team pay data into postgresql
	2. refactor function tp.test_data_update_nightly() to update tp.data

	
1. does anything anywhere use ads.ext_tp_data
	functions: no
	luigi: no
		alter TABLE ads.ext_tp_data
		rename to z_unused_ext_tp_data;
		alter TABLE ads.ext_tp_team_techs
		rename to z_unused_ext_tp_team_techs;
		alter table ads.ext_tp_team_values
		rename to z_unused_ext_tp_team_values;
		alter table ads.ext_tp_teams
		rename to z_unused_ext_tp_teams;
		alter table ads.ext_tp_tech_values
		rename to z_unused_ext_tp_tech_values;
		alter table ads.ext_tp_techs
		rename to z_unused_ext_tp_techs;

2. discontinue scraping advantage data into postgresql (tp.data, tp.techs, etc)