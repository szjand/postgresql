﻿-- convert from ads.ext_ to dds.
/*
select * from fs.weeks where week_id = 969
07-24 thru 07-30
'07/24/2022' and '07/30/2022'
*/
-------------------------------------------------------------------
--< FUNCTION fs.update_advisor_ros_by_day() class 
-- 08/01/22 updated this function and full_shop.py class AdvisorRosByDay()
-------------------------------------------------------------------
select * from (
  select aa.personnel_id, bb.the_date, bb.ros
  from ( -- full shop writers
    select a.personnel_id, a.employee_number
    from fs.personnel a
    join fs.personnel_jobs b on a.personnel_id = b.personnel_id
      and  b.thru_date > current_date
    join fs.jobs c on b.job = c.job
      and b.store = c.store
      and c.job = 'Service Advisor'
    where not a.is_deleted) aa
  join (-- writer ros opened per day for a date range 
    select employeenumber, the_date, count(*) as ros
    from (
      select a.ro, c.name, c.employeenumber, c.storecode, b.the_date
      from ads.ext_fact_repair_order a
      join dds.dim_date b on a.opendatekey = b.date_key  
        and b.the_date between '07/24/2022' and '07/30/2022'
      join ads.ext_dim_Service_writer c on a.servicewriterkey = c.servicewriterkey
        and c.active 
        and c.currentrow  
      group by a.ro, c.name, c.employeenumber, c.storecode, b.the_date) x
    group by employeenumber, the_date) bb on aa.employee_number = bb.employeenumber) aaa

full outer join (

  select aa.personnel_id, bb.the_date, bb.ros
  from ( -- full shop writers
    select a.personnel_id, a.employee_number
    from fs.personnel a
    join fs.personnel_jobs b on a.personnel_id = b.personnel_id
      and  b.thru_date > current_date
    join fs.jobs c on b.job = c.job
      and b.store = c.store
      and c.job = 'Service Advisor'
    where not a.is_deleted) aa
  join (-- writer ros opened per day for a date range 
    select employee_number, the_date, count(*) as ros
    from (
      select a.ro, c.writer_name, c.employee_number, c.store_code, a.open_date as the_date
      from dds.fact_repair_order a
      join dds.dim_Service_writer c on a.service_writer_key = c.service_writer_key
        and c.active 
        and c.current_row  
      where a.open_date between _from_date and _thru_date
      group by a.ro, c.writer_name, c.employee_number, c.store_code, a.open_date) x
    group by employee_number, the_date) bb on aa.employee_number = bb.employee_number) bbb
on aaa.personnel_id = bbb.personnel_id
  and aaa.the_date = bbb.the_date   
-------------------------------------------------------------------
--/> FUNCTION fs.update_tech_flag_hours_by_day()
-------------------------------------------------------------------   

-------------------------------------------------------------------
--/> FUNCTION fs.update_advisor_ros_by_day()
------------------------------------------------------------------- 
-- need to do separate tech tables

  drop table if exists techs cascade;
  create temp table techs as
  select a.personnel_id, a.employee_number, c.job, a.full_name, b.store,
    d.technumber as tech_number, array_agg(distinct d.techkey) as tech_key
  from fs.personnel a
  join fs.personnel_jobs b on a.personnel_id = b.personnel_id
    and  b.thru_date > current_date
  join fs.jobs c on b.job = c.job
    and b.store = c.store
    and (c.job in ('Metal Tech','Main Shop Tech')
    or c.job like '%Main Shop Tech%')
  join ads.ext_dim_tech d on a.employee_number = d.employeenumber
    and d.techkey <> 100
    and d.technumber not in ('D63', '247')  -- exclude ben johnsons detail tech number
    and d.techkey not in (105,326,381,583,955,1628)
  where not a.is_deleted  
  group by a.full_name, a.personnel_id, c.job,  b.store, a.employee_number, d.technumber;
  create unique index on techs(personnel_id);
  create unique index on techs(employee_number);
  create unique index on techs(tech_number,store);
  CREATE INDEX ON techs USING GIN(tech_key);
  create index on techs(job);

  drop table if exists dds_techs cascade;
  create temp table dds_techs as
  select a.personnel_id, a.employee_number, c.job, a.full_name, b.store,
    d.tech_number as tech_number, array_agg(distinct d.tech_key) as tech_key
  from fs.personnel a
  join fs.personnel_jobs b on a.personnel_id = b.personnel_id
    and  b.thru_date > current_date
  join fs.jobs c on b.job = c.job
    and b.store = c.store
    and (c.job in ('Metal Tech','Main Shop Tech')
    or c.job like '%Main Shop Tech%')
  join dds.dim_tech d on a.employee_number = d.employee_number -----------------------------------------
    and d.tech_key <> 100 ------------------------------------------------
    and d.tech_number not in ('D63', '247')  -- exclude ben johnsons detail tech number  ---------------------------------------
    and d.tech_key not in (105,326,381,583,955,1628)   ------------------------------------------------------------------------------
  where not a.is_deleted  
  group by a.full_name, a.personnel_id, c.job,  b.store, a.employee_number, d.tech_number;
  create unique index on techs(personnel_id);
  create unique index on techs(employee_number);
  create unique index on techs(tech_number,store);
  CREATE INDEX ON techs USING GIN(tech_key);
  create index on techs(job);

drop table if exists wtf;
create temp table wtf as
select * from (   
    select a.storecode, c.personnel_id, b.the_date, ((select full_name from fs.personnel where personnel_id = c.personnel_id)), 
      coalesce(sum(case when aa.opcode not like 'TRAIN%'  and aa.opcode not like 'SHOP%' then a.flaghours end), 0) as flag_hours,
      coalesce(sum(case when aa.opcode like 'SHOP%' then a.flaghours end), 0) as shop,
      coalesce(sum(case when aa.opcode like 'TRAIN%' then a.flaghours end), 0) as training,
      array_agg(distinct ro order by ro)
    from ads.ext_fact_repair_order a
    join techs c on a.techkey = any(c.tech_key)  
    left join ads.ext_dim_opcode aa on a.opcodekey = aa.opcodekey
    join dds.dim_date b on 
      case
        when c.job = 'Metal Tech' then a.closedatekey = b.date_key
        else a.flagdatekey = b.date_key
      end
      and b.the_date between '07/24/2020' and '07/30/2022'
    group by a.storecode, c.personnel_id, b.the_date) aaa

full outer join (    

    select a.store_code as dds_store_code, c.personnel_id as dds_personnel_id, b.the_date as dds_the_date, 
			((select full_name as dds_full_name from fs.personnel where personnel_id = c.personnel_id)), 
      coalesce(sum(case when aa.opcode not like 'TRAIN%'  and aa.opcode not like 'SHOP%' then a.flag_hours end), 0) as dds_flag_hours,
      coalesce(sum(case when aa.opcode like 'SHOP%' then a.flag_hours end), 0) as dds_shop,
      coalesce(sum(case when aa.opcode like 'TRAIN%' then a.flag_hours end), 0) as dds_training,
      array_agg(distinct ro order by ro) as dds_ros
    from dds.fact_repair_order a
    join dds_techs c on a.tech_key = any(c.tech_key)  
    left join dds.dim_opcode aa on a.opcode_key = aa.opcode_key
    join dds.dim_date b on 
      case
        when c.job = 'Metal Tech' then a.close_date = b.the_date
        else a.flag_date = b.the_date
      end
      and b.the_date between '07/24/2020' and '07/30/2022'
    group by a.store_code, c.personnel_id, b.the_date) bbb on aaa.personnel_id = bbb.dds_personnel_id
      and aaa.the_date = bbb.dds_the_date


select * from wtf limit 10
 -- some minor differences, except mcveigh, way off
select *, total - dds_total from (
select full_name, sum(flag_hours) + sum(shop) + sum(training) as total
from wtf
group by full_name) a
full outer join (
select dds_full_name, sum(dds_flag_hours) + sum(dds_shop) + sum(dds_training) as dds_total
from wtf
group by dds_full_name) b on a.full_name = b.dds_full_name


select * from techs
select * from dds_techs