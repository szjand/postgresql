﻿CREATE OR REPLACE FUNCTION bspp.test_pay_summary(
    _employee_number citext,
    _pay_period_indicator integer)
  RETURNS SETOF json AS
$BODY$  

/*

select employee_number from bspp.personnel where last_name = 'evavold'

select * from bspp.test_pay_summary('141069', -1)

select * from bspp.personnel_roles

3/8/18 added else clause, returns _employee_number, _user_key
select * from bspp.json_get_pay_summary('123211', -1)

*/

declare
--   _employee_number citext := (select employee_number from bspp.personnel where last_name = 'blumhagen');
--   _pay_period_indicator integer := -1;
--   
  _user_name citext := (
    select user_name
    from bspp.personnel
    where employee_number = _employee_number);
  _user_key citext := 'user_key'; -----------------------------------------------------
--   _user_key uuid := ( --------------------------------------------------------------
--     select user_key
--     from nrv.users
--     where employee_number = _employee_number);
  _current_pay_period_seq integer := (
    select distinct biweekly_pay_period_sequence
    from dds.dim_date
    where the_date = current_date);
  _from_date date := (
    select distinct biweekly_pay_period_start_date
    from dds.dim_date
    where biweekly_pay_period_sequence = _current_pay_period_seq + _pay_period_indicator);
  _thru_date date := (
    select distinct biweekly_pay_period_end_date
    from dds.dim_date
    where biweekly_pay_period_sequence = _current_pay_period_seq + _pay_period_indicator);   
  _user_role citext := (
    select the_role
    from bspp.personnel_roles
    where user_name = _user_name
      and from_date <= _from_date
      and thru_date >= _thru_date);     

begin
  case
    when _user_role = 'shop_foreman' then
      return query
      select row_to_json (zzz) 
      from (
        select row_to_json(xxx) as body_shop_pay
        from ( 
          with 
            ros as (
              select ro, parts, labor, 
                parts + labor as ro_sales 
              from (  
                select ro,
                  coalesce(sum(amount * multiplier) filter (where category like '%parts'), 0) as parts,
                  coalesce(sum(amount) filter (where category like '%labor'), 0) as labor
                from bspp.bs_sales_detail
                where the_date between _from_date and _thru_date
                group by ro) a),
            sales as (
              select sum(parts) as parts_total,
                sum(labor) as labor_total,
                sum(parts + labor) as parts_and_labor_sales
              from ros)
          select '_user_key' as user, z.*, --------------------------------------------------------------------------------------------
              ( -- ros: an array of ro objects: multiple rows
                select coalesce(array_to_json(array_agg(row_to_json(g))), '[]')
                from ( -- g one row per ro
                  select * 
                  from ros) g) as ros
          from ( -- z all the individual's pay data + total pay:  single row
            select x.*, salary + commission_pay as total_pay
            from ( -- x all the individual's pay data elements:  single row
                select  _employee_number as id, b.base_pay_value as Salary, 
                  b.commission_perc as commission_percentage, 
                  (select round(parts_and_labor_sales, 2) from sales) as parts_and_labor_sales,
                  (select round(parts_total, 2) from sales) as parts_total,
                  (select round(labor_total, 2) from sales) as labor_total,
                  round(b.commission_perc * (select parts_and_labor_sales from sales), 2) as commission_pay
                from bspp.personnel a
                inner join bspp.personnel_roles b on a.user_name = b.user_name
                  and b.from_date <= _from_date
                  and b.thru_date >= _thru_date                
                where a.user_name = _user_name) x) z) xxx ) zzz;


                  
    when _user_role in ('parts','vda','csr') then

      return query
      select row_to_json (zzz) 
      from (
        select row_to_json(xxx) as body_shop_pay
        from ( 
          with 
            ros as (
              select ro, parts, labor, 
                parts + labor as ro_sales 
              from (  
                select ro,
                  coalesce(sum(amount * multiplier) filter (where category like '%parts'), 0) as parts,
                  coalesce(sum(amount) filter (where category like '%labor'), 0) as labor
                from bspp.bs_sales_detail
                where the_date between _from_date and _thru_date
                group by ro) a),
            sales as (
              select sum(parts) as parts_total,
                sum(labor) as labor_total,
                sum(parts + labor) as parts_and_labor_sales
              from ros)
          select _user_key as user, z.*, 
              ( -- ros: an array of ro objects: multiple rows
                select coalesce(array_to_json(array_agg(row_to_json(g))), '[]')
                from ( -- g one row per ro
                  select * 
                  from ros) g) as ros
          from ( -- z all the individual's pay data + total pay:  single row
          select x.*, regular_pay + overtime_pay + pto_pay + commission_pay + ot_variance as total_pay 
          from ( -- x all the individual's pay data elements:  single row
              select  _employee_number as id, b.base_pay_value as hourly_rate, round(1.5 * b.base_pay_value, 2) as overtime_rate,  
                a.pto_rate,
                b.commission_perc as commission_percentage, 
                coalesce(c.regular_clock_hours, 0) as regular_clock_hours,
                coalesce(c.overtime_hours, 0) as overtime_hours,
                coalesce(c.pto_hours, 0) as pto_hours,
                coalesce(round(c.regular_clock_hours * b.base_pay_value, 2), 0) as regular_pay,
                coalesce(round(c.overtime_hours * 1.5 * b.base_pay_value, 2), 0) as overtime_pay,
                coalesce(round(a.pto_rate * c.pto_hours, 2), 0) as pto_pay,
                (select round(parts_and_labor_sales, 2) from sales) as parts_and_labor_sales,
                (select round(parts_total, 2) from sales) as parts_total,
                (select round(labor_total, 2) from sales) as labor_total,
                round(b.commission_perc * (select parts_and_labor_sales from sales), 2) as commission_pay,
                 case
                   when c.regular_clock_hours + c.overtime_hours = 0 then 0
                   else coalesce(round((.5 * c.overtime_hours * 
                     (b.commission_perc * (select parts_and_labor_sales from sales)))/(c.regular_clock_hours + c.overtime_hours), 2), 0) 
                  end as ot_variance
              from bspp.personnel a
              inner join bspp.personnel_roles b on a.user_name = b.user_name 
               and b.from_date <= _from_date
               and b.thru_date >= _thru_date
              LEFT join ( -- users clock hours
                select user_name, sum(reg_hours) as regular_clock_hours, sum(ot_hours) as overtime_hours,
                  sum(pto_hours + hol_hours) as pto_hours
                from bspp.clock_hours
                where the_date between _from_date and _thru_date
                group by user_name) c on a.user_name = c.user_name
              where a.user_name = _user_name) x) z) xxx ) zzz;  

                    
    else 
      return query
      select row_to_json(A)
      from (
        select  json_build_object('id',_employee_number,'user',_user_key) as body_shop_pay  ) A;     
  end case;   

end
$BODY$
language plpgsql;
    
/*
writer:
	"body_shop_pay": {
		"user": "30548969-09e7-449c-86c1-99e72671a971",
		"id": "141069",
		"hourly_rate": 10.00,
		"overtime_rate": 15.00,
		"pto_rate": 10.00,
		"commission_percentage": 0.0067,
		"regular_clock_hours": 80.00,
		"overtime_hours": 11.95,
		"pto_hours": 0.00,
		"regular_pay": 800.00,
		"overtime_pay": 179.25,
		"pto_pay": 0.00,
		"parts_and_labor_sales": 190370.73,
		"parts_total": 92933.23,
		"labor_total": 97437.50,
		"commission_pay": 1275.48,
		"ot_variance": 82.88,
		"total_pay": 2337.61,
		"ros": [{
parts:
	"body_shop_pay": {
		"user": "93be8364-4592-4ac1-a7c4-04a818cedfb6",
		"id": "160930",
		"hourly_rate": 10.00,
		"overtime_rate": 15.00,
		"pto_rate": 10.00,
		"commission_percentage": 0.0055,
		"regular_clock_hours": 19.98,
		"overtime_hours": 0.00,
		"pto_hours": 0.00,
		"regular_pay": 199.80,
		"overtime_pay": 0.00,
		"pto_pay": 0.00,
		"parts_and_labor_sales": 33034.80,
		"parts_total": 12731.61,
		"labor_total": 20303.19,
		"commission_pay": 181.69,
		"ot_variance": 0.00,
		"total_pay": 381.49,
		"ros": [{		
shop foreman: 
	"body_shop_pay": {
		"user": "9413c199-5348-4e54-929d-52409e949ad8",
		"id": "1126040",
		"salary": 1250.00,
		"commission_percentage": 0.0055,
		"parts_and_labor_sales": 33034.80,
		"parts_total": 12731.61,
		"labor_total": 20303.19,
		"commission_pay": 181.69,
		"total_pay": 1431.69,
		"ros": [{		
*/