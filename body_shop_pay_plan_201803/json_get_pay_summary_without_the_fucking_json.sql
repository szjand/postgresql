﻿-- Function: bspp.json_get_pay_summary(citext, integer)

-- DROP FUNCTION bspp.json_get_pay_summary(citext, integer);

-- CREATE OR REPLACE FUNCTION bspp.json_get_pay_summary(
--     _employee_number citext,
--     _pay_period_indicator integer)
--   RETURNS SETOF json AS
-- $BODY$  

/*
04/15/18
  sales now includes account 147300 for everyone, sales is the same for all roles
  new estimator commission percentages
  
select employee_number from bspp.personnel where last_name = 'blumhagen'

select * from bspp.json_get_pay_summary('160930', 0)

select * from bspp.personnel_roles
*/



do 
$$

-- bspp.json_get_pay_summary without the fucking json
declare
  _employee_number citext := '1108200';
  _pay_period_indicator integer := 0;
  _user_name citext := (
    select user_name
    from bspp.personnel
    where employee_number = _employee_number);
  _user_key uuid := (
    select user_key
    from nrv.users
    where employee_number = _employee_number);
  _current_pay_period_seq integer := (
    select distinct biweekly_pay_period_sequence
    from dds.dim_date
    where the_date = current_date);
  _from_date date := (
    select distinct biweekly_pay_period_start_date
    from dds.dim_date
    where biweekly_pay_period_sequence = _current_pay_period_seq + _pay_period_indicator);
  _thru_date date := (
    select distinct biweekly_pay_period_end_date
    from dds.dim_date
    where biweekly_pay_period_sequence = _current_pay_period_seq + _pay_period_indicator);   
  _user_role citext := (
    select the_role
    from bspp.personnel_roles
    where user_name = _user_name
      and from_date <= _from_date
      and thru_date >= _thru_date);     

begin
--   case
--     when _user_role = 'shop_foreman' then
--       return query
--       select row_to_json (zzz) 
--       from (
--         select row_to_json(xxx) as body_shop_pay
--         from ( 
--           with 
--             ros as (
--               select ro, parts, labor, 
--                 parts + labor as ro_sales 
--               from (  
--                 select ro,
--                   coalesce(sum(amount * multiplier) filter (where category like '%parts'), 0) as parts,
--                   coalesce(sum(amount) filter (where category like '%labor'), 0) as labor
--                 from bspp.bs_sales_detail
--                 where the_date between _from_date and _thru_date
--                 group by ro) a),
--             sales as (
--               select sum(parts) as parts_total,
--                 sum(labor) as labor_total,
--                 sum(parts + labor) as parts_and_labor_sales
--               from ros)
--           select 'user_key'as user, z.*, 
--               ( -- ros: an array of ro objects: multiple rows
--                 select coalesce(array_to_json(array_agg(row_to_json(g))), '[]')
--                 from ( -- g one row per ro
--                   select * 
--                   from ros) g) as ros
--           from ( -- z all the individual's pay data + total pay:  single row
--             select x.*, salary + commission_pay as total_pay
--             from ( -- x all the individual's pay data elements:  single row
--                 select  _employee_number as id, b.base_pay_value as Salary, 
--                   b.commission_perc as commission_percentage, 
--                   (select round(parts_and_labor_sales, 2) from sales) as parts_and_labor_sales,
--                   (select round(parts_total, 2) from sales) as parts_total,
--                   (select round(labor_total, 2) from sales) as labor_total,
--                   round(b.commission_perc * (select parts_and_labor_sales from sales), 2) as commission_pay
--                 from bspp.personnel a
--                 inner join bspp.personnel_roles b on a.user_name = b.user_name
--                   and b.from_date <= _from_date
--                   and b.thru_date >= _thru_date                
--                 where a.user_name = _user_name) x) z) xxx ) zzz;
-- 
-- 
--                   
--     when _user_role in ('parts','vda','csr') then

drop table if exists wtf;
create temp table wtf as

          with 
            ros as (
              select ro, parts, labor, 
                parts + labor as ro_sales 
              from (  
                select ro,
                  coalesce(sum(amount * multiplier) filter (where category like '%parts'), 0) as parts,
                  coalesce(sum(amount) filter (where category like '%labor'), 0) as labor
                from bspp.bs_sales_detail
                where the_date between _from_date and _thru_date
                group by ro) a),
            sales as (
              select sum(parts) as parts_total,
                sum(labor) as labor_total,
                sum(parts + labor) as parts_and_labor_sales
              from ros)
              select * from ros
              order by ro_sales;
--           select _user_key as user, z.*, 
--               ( -- ros: an array of ro objects: multiple rows
--                 select coalesce(array_to_json(array_agg(row_to_json(g))), '[]')
--                 from ( -- g one row per ro
--                   select * 
--                   from ros) g) as ros
--           from ( -- z all the individual's pay data + total pay:  single row
--           select x.*, regular_pay + overtime_pay + pto_pay + commission_pay + ot_variance as total_pay 
--           from ( -- x all the individual's pay data elements:  single row
--               select  _employee_number as id, b.base_pay_value as hourly_rate, round(1.5 * b.base_pay_value, 2) as overtime_rate,  
--                 a.pto_rate,
--                 b.commission_perc as commission_percentage, 
--                 coalesce(c.regular_clock_hours, 0) as regular_clock_hours,
--                 coalesce(c.overtime_hours, 0) as overtime_hours,
--                 coalesce(c.pto_hours, 0) as pto_hours,
--                 coalesce(round(c.regular_clock_hours * b.base_pay_value, 2), 0) as regular_pay,
--                 coalesce(round(c.overtime_hours * 1.5 * b.base_pay_value, 2), 0) as overtime_pay,
--                 coalesce(round(a.pto_rate * c.pto_hours, 2), 0) as pto_pay,
--                 (select round(parts_and_labor_sales, 2) from sales) as parts_and_labor_sales,
--                 (select round(parts_total, 2) from sales) as parts_total,
--                 (select round(labor_total, 2) from sales) as labor_total,
--                 round(b.commission_perc * (select parts_and_labor_sales from sales), 2) as commission_pay,
--                  case
--                    when c.regular_clock_hours + c.overtime_hours = 0 then 0
--                    else coalesce(round((.5 * c.overtime_hours * 
--                      (b.commission_perc * (select parts_and_labor_sales from sales)))/(c.regular_clock_hours + c.overtime_hours), 2), 0) 
--                   end as ot_variance
--               from bspp.personnel a
--               inner join bspp.personnel_roles b on a.user_name = b.user_name 
--                and b.from_date <= _from_date
--                and b.thru_date >= _thru_date
--               LEFT join ( -- users clock hours
--                 select user_name, sum(reg_hours) as regular_clock_hours, sum(ot_hours) as overtime_hours,
--                   sum(pto_hours + hol_hours) as pto_hours
--                 from bspp.clock_hours
--                 where the_date between _from_date and _thru_date
--                 group by user_name) c on a.user_name = c.user_name
--               where a.user_name = _user_name) x) z;

                    
--     else 
--       return query
--       select row_to_json(A)
--       from (
--         select  json_build_object('id',_employee_number,'user',_user_key) as body_shop_pay  ) A;     
--   end case;   

end
$$;

select * from wtf;



11/8/18

wtf

ro list on writers vision page shows rows with negative amount

some are adjustments (18061878)

some are pdq ros dont know what they are yet

select * from bspp.bs_Sales_Detail where ro = '19324633'

select b.the_date, c.description, a.control, a.amount, sum(amount) over (partition by control)
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.the_date between '10/28/2018' and '11/10/2018'
join fin.dim_gl_Description c on a.gl_description_key = c.gl_description_key
where control in ('18061878', '18063211','18063005','19325023','19325154','19325090','19324633')
  and amount > 0
order by control

select *
from bspp.sale_Accounts

select *
from bspp.bs_sales_detail
where the_date between '10/28/2018' and '11/10/2018'
  and amount < 0
order by ro
  
select ro, sum(amount)
from bspp.bs_sales_detail
where the_date between '10/28/2018' and '11/10/2018'
  and amount < 0
group by ro  

select sum(amount), .0055*(sum(amount))
from bspp.bs_sales_detail
where the_date between '10/28/2018' and '11/10/2018'
  and amount < 0