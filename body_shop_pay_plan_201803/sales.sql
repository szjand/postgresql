﻿select *
from fin.dim_account
where account in ('147000','147100','147200','147300','147900','147600','147700','147701','147901')


select *
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201802 
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
  and c.store = 'ry1'
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  and d.gl_account in ('147000','147100','147200','147300','147900','147600','147700','147701','147901')
order by page, line  