﻿bspp.bs_ros
any ro for which writer is main writer or alt writer

                    SELECT ro, writer_id,
                      coalesce((
                        select
                          case opcodekey
                            when 26302 then '312'
                            when 26303 then '311'
                            when 26304 then '303'
                            when 26305 then '317'
                            when 26306 then '712'
                            when 26307 then '403'
                            when 26857 then '318'
                          end as alt_writer_id
                        from ads.ext_fact_repair_order
                        where ro = d.ro
                          and opcodekey in(26302,26303,26304,26305,26306,26307,26857)
                          group by opcodekey), 'none') as alt_writer_id
                    FROM (
                      SELECT a.ro, c.writernumber AS writer_id
                      FROM ads.ext_fact_repair_order a
                      INNER JOIN (
                        select a.control
                        from fin.fact_gl a
                        inner join fin.dim_account b on a.account_key = b.account_key
                        inner join bspp.sale_accounts c on b.account = c.account
                        inner join dds.dim_date e on a.date_key = e.date_key
                          and e.biweekly_pay_period_sequence = (
                            select biweekly_pay_period_sequence
                            from dds.dim_date
                            where the_date = current_date -1)
                        where a.post_status = 'Y'
                        group by a.control) b on a.ro = b.control
                      INNER JOIN ads.ext_dim_service_writer c on a.servicewriterkey = c.servicewriterkey
                      GROUP BY a.ro, c.writernumber) d

bspp.sales

                    select s.writer_id, s.alt_writer_id, r.the_date, r.control, r.gm_parts, r.non_gm_parts,
                      r.gm_parts + r.non_gm_parts as total_parts, r.labor,
                      r.gm_parts + r.non_gm_parts + r.labor as total_sales
                    from ( -- r: date, ro, amounts; 1 row per ro/date; 147700(GM Parts): multiplier = .5
                      select e.the_date, a.control,
                        round(sum(case when c.category = 'parts' and b.account = '147700'
                          then -1 * a.amount * c.multiplier else 0 end), 2) as gm_parts,
                        round(sum(case when c.category = 'parts' and b.account <> '147700'
                          then -1 * a.amount * c.multiplier else 0 end), 2) as non_gm_parts,
                        round(
                          sum(
                            case when c.category = 'labor' then -1 * a.amount * c.multiplier else 0 end), 2) as labor
                      from fin.fact_gl a -- same query that generates bs_ro_numbers.csv
                      inner join fin.dim_account b on a.account_key = b.account_key
                      inner join bspp.sale_accounts c on b.account = c.account
                      inner join dds.dim_date e on a.date_key = e.date_key
                        and e.biweekly_pay_period_sequence = (
                          select biweekly_pay_period_sequence
                          from dds.dim_date
                          where the_date = current_date - 1)
                      where a.post_status = 'Y'
                      group by a.control, e.the_date) r
                    inner join bspp.z_unused_bs_ros s on r.control = s.ro                      



bspp.update_pay_period_transactions()


bspp.update_writer_sales()


select *
from bspp.bs_sales_detail
where ro = '18057603'



select c.the_date, a.control as ro, bb.account, /*b.category, b.multiplier,*/ amount, bb.account_type, bb.department, bb.description
from fin.fact_gl a
-- inner join bspp.sale_accounts b on a.account_key = b.account_key
inner join fin.dim_account bb on a.account_key = bb.account_key
  
inner join dds.dim_date c on a.date_key = c.date_key
inner join fin.dim_doc_type d on a.doc_type_key = d.doc_type_key
where post_status = 'Y'
--   and c.the_date between _from_date and _thru_date
  and d.doc_type = 'service tickets' 
  and a.control = '18057415'
  and bb.account_type = 'Sale'
order by account


select *
from bspp.bs_sales_detail
where ro = '18057415'



----------------------------------------------------------------------------------------------

-- tried grouping fact_gl on dept, yuck
select *
from (
  select *
  from bspp.z_unused_writersales a
  where the_date > '12/31/2017'
  limit 100) x
left join (
  select a.control,
    sum(amount) filter (where department_code = 'bs') as bs,
    sum(amount) filter (where department_code = 'pd') as pd,
    sum(amount) filter (where department_code = 'ql') as ql,
    sum(amount) filter (where department_code = 're') as re,
    sum(amount) filter (where department_code = 'sd') as sd
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
    and b.account_type = 'sale'
  inner join dds.dim_date c on a.date_key = c.date_key
    and c.the_year = 2018  
  where post_status = 'Y'
  group by a.control) y on x.ro = y.control


-- only of of these where total does not add up
  select *
  from bspp.z_unused_writersales
  where total_sales <> total_parts + labor  


-- pay period transactions

select e.the_date, a.control, b.account, b.department, b.description, sum(a.amount) as amount
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
  and b.account_type = 'sale'
  and b.current_row = true
inner join dds.dim_date e on a.date_key = e.date_key
  and e.biweekly_pay_period_sequence = (
    select biweekly_pay_period_sequence
    from dds.dim_date
    where the_date = current_date -30)
where a.post_status = 'Y'
group by a.control, b.account, e.the_date, b.description, b.department;



-- bspp.update_writer_sales()

      select m.ro, m.writernumber as writer_id, coalesce(n.alt_writer_id, 'none') as alt_writer_id
      from ( -- writer and alt_writer where bs est is writer: 1 row per ro
          select a.ro, c.writernumber
          from ads.ext_fact_repair_order  a
          inner join ads.ext_dim_service_writer c on a.servicewriterkey = c.servicewriterkey 
            and c.writernumber in ('312','311','303','317','712','403','318')
          left join ads.ext_dim_opcode d on a.opcodekey = d.opcodekey
            and d.opcodekey in (26302,26303,26304,26305,26306,26307,26857)
          group by a.ro, c.writernumber) m
        left join ( -- alt_writer where bs est is writer : 1 ro per ro
          select a.ro, 
            case a.opcodekey
              when 26302 then '312'
              when 26303 then '311'
              when 26304 then '303'
              when 26305 then '317'
              when 26306 then '712'
              when 26307 then '403'
              when 26857 then '318'
              else 'none'
            end as alt_writer_id
          from ads.ext_fact_repair_order  a
          inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
            and c.opcodekey between 26302 and 26307
          where a.servicewriterkey in (
            select servicewriterkey
            from ads.ext_dim_service_Writer
            where  writernumber in (
              select writer_id
              from bspp.personnel b 
              inner join bspp.personnel_role c on b.user_name = c.user_name
                and c.the_role in ('vda','csr')))
          and ro not in ('18049420')
          group by a.ro, a.opcodekey) n on m.ro = n.ro





-- are they still adding alt_writer?
-- yep
select max(b.the_date)
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.opendatekey = b.date_key
inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
  and c.opcodekey in (26302,26303,26304,26305,26306,26307,26857)
          

so what i am going to have to do is to isolate and analyze pre payplan change pay and see how much
was paid on accounts other than what is in bspp.sale_accounts  

or, probably easier, i can look at past pay, and generate pay for the same ros with new pay plan 

yes yes yes

select *
from (
select * 
from bspp.z_unused_writersales
where the_date between '01/21/2018' and '02/03/2018') x
left join (
select control,
  sum(-amount) filter (where category = 'gm parts') as gm_parts,
  sum(-amount) filter (where category = 'non gm parts') as nopn_gm_parts,
  sum(-amount) filter (where category like '%parts') as total_parts,
  sum(-amount) filter (where category = 'labor') as labor,
  sum(-amount) filter (where category = 'internal labor') as int_labor,
  sum(-amount) as total_sales
from fin.fact_gl a
inner join bspp.sale_accounts b on a.account_key = b.account_key
inner join dds.dim_date c on a.date_key = c.date_key
inner join fin.dim_doc_type d on a.doc_type_key = d.doc_type_key
where post_status = 'Y'
  and c.the_date between '01/01/2018' and '03/03/2018'
  and d.doc_type = 'service tickets' 
group by a.control) y on x.ro = y.control

-- total sales by writer for a pay period
-- want it for multiple pay periods, i think
select a.writer_id, a.last_name, writer + alt_writer
from  (
select a.writer_id, b.last_name, sum(total_sales) as writer
from bspp.z_unused_writersales a
inner join bspp.personnel b on a.writer_id = b.writer_id
where the_date between '01/21/2018' and '02/03/2018'
group by a.writer_id, b.last_name) a
inner join (
select alt_writer_id, sum(total_sales) as alt_writer
from bspp.z_unused_writersales
where the_date between '01/21/2018' and '02/03/2018'
group by alt_writer_id) b on a.writer_id = b.alt_writer_id


-- close except totals
select aa.thru_date, 
  sum(writer) filter (where last_name = 'evavold') as evavold,
  sum(writer) filter (where last_name = 'hill') as hill,
  sum(writer) filter (where last_name = 'lueker') as lueker,
  sum(writer) filter (where last_name = 'marek') as marek,
  sum(writer) filter (where last_name = 'ostlund') as ostlund,
  sum(writer) filter (where last_name = 'steinke') as steinke  
-- select last_name, sum(writer) 
from (
  select distinct biweekly_pay_period_start_date as from_date, biweekly_pay_period_end_date as thru_date
  from dds.dim_date
  where the_date between '01/21/2018' and '02/03/2018') aa
left join (
  select a.the_date, a.writer_id, a.last_name, writer + alt_writer as writer_total, writer, alt_writer
  from  (
    select a.the_date, a.writer_id, b.last_name, sum(total_sales) as writer
    from bspp.z_unused_writersales a
    inner join bspp.personnel b on a.writer_id = b.writer_id
    group by a.the_date, a.writer_id, b.last_name) a
  inner join (
    select a.the_date, alt_writer_id, sum(total_sales) as alt_writer
    from bspp.z_unused_writersales a
    group by a.the_date, alt_writer_id) b on a.the_date = b.the_date 
      and a.writer_id = b.alt_writer_id) bb on bb.the_date between aa.from_Date and aa.thru_date
group by aa.thru_date
order by aa.thru_Date

-- wtf only steinke is correct
select last_name, sum(writer_total)
from (
  select a.the_date, a.writer_id, a.last_name, writer + alt_writer as writer_total, writer, alt_writer
  from  (
    select a.the_date, a.writer_id, b.last_name, sum(total_sales) as writer
    from bspp.z_unused_writersales a
    inner join bspp.personnel b on a.writer_id = b.writer_id
    group by a.the_date, a.writer_id, b.last_name) a
  inner join (
    select a.the_date, alt_writer_id, sum(total_sales) as alt_writer
    from bspp.z_unused_writersales a
    group by a.the_date, alt_writer_id) b on a.the_date = b.the_date 
      and a.writer_id = b.alt_writer_id) x
where the_date between '01/21/2018' and '02/03/2018' 
group by last_name



-- ok, this is correct, when i add the date somehow i'm fucking it up
select a.writer_id, a.last_name, writer + alt_writer
from  ( -- main writer
  select a.writer_id, b.last_name, sum(total_sales) as writer
  from bspp.z_unused_writersales a
  inner join bspp.personnel b on a.writer_id = b.writer_id
  where the_date between '01/21/2018' and '02/03/2018'
  group by a.writer_id, b.last_name) a
inner join ( -- alt writer
  select alt_writer_id, sum(total_sales) as alt_writer
  from bspp.z_unused_writersales
  where the_date between '01/21/2018' and '02/03/2018'
  group by alt_writer_id) b on a.writer_id = b.alt_writer_id


-- lets look at it from the perspective of ro
-- with high confidence that this table coincides with what was paid
-- has data from 2/20/2017 on
select a.*, 
  (select min(the_date)
    from dds.dim_date 
    where a.the_date between biweekly_pay_period_start_date and biweekly_pay_period_end_date)
from bspp.z_unused_writersales a
-- left join dds.dim_date b on a.the_date between b.biweekly_pay_period_start_date and biweekly_pay_period_end_date
where a.the_date < '03/01/2018'
  and ro in (
    select ro
    from bspp.z_unused_writersales
    group by ro 
    having count(*) > 1)
order by ro    

-- ok, the above shows that there are multiple rows for ros, sometimes different pay periods sometimes not
-- think where i want to be is 1 row per rop
-- for this look, i don't care about who the writer is, what i care about is the total sales for each ro
-- for which the writer is paid
drop table if exists old_plan;
create temp table old_plan as
select a.ro, max(a.the_date) as the_date, 
  (select distinct year_month from dds.dim_date where the_date = max(a.the_date)) as year_month,
  sum(a.gm_parts) as gm_parts, sum(a.non_gm_parts) as non_gm_parts,
  sum(a.total_parts) as total_parts, sum(a.labor) as labor, sum(a.total_sales) as total_sales
from bspp.z_unused_writersales a
group by ro;
create unique index on old_plan(ro);

select distinct category from bspp.sale_accounts

-- ok, want to compare that to total sales for which the writer would be paid in the current pay plan
-- same query used to generat bspp.bs_sales_detail
drop table if exists new_plan;
create temp table new_plan as
select a.control as ro, max(c.the_date) as the_date, 
  (select distinct year_month from dds.dim_date where the_date = max(c.the_date)) as year_month,
  coalesce(sum(multiplier * -amount) filter (where category = 'gm parts'), 0) as gm_parts,
  coalesce(sum(-amount) filter (where category = 'non gm parts'), 0)  as non_gm_parts,
  coalesce(sum(multiplier * -amount) filter (where category like '%parts'), 0)  as total_parts,
  coalesce(sum(-amount) filter (where category = 'labor'), 0)  as labor,
  coalesce(sum(multiplier * -amount), 0)  as total_sales
from fin.fact_gl a
inner join bspp.sale_accounts b on a.account_key = b.account_key
  and b.account <> '147300'
inner join dds.dim_date c on a.date_key = c.date_key
  and c.the_year > 2016
inner join fin.dim_doc_type d on a.doc_type_key = d.doc_type_key
where post_status = 'Y'
--   and c.the_date between _from_date and _thru_date
  and d.doc_type = 'service tickets' 
group by a.control

select *
from old_plan a
left join new_plan b on a.ro = b.ro
where b.ro is null
order by a.the_date


select *
from old_plan a
left join new_plan b on a.ro = b.ro
  and round(a.total_sales, 0) <> round(coalesce(b.total_sales, 0), 0)
    

select b.account, b.description, b.department_code, sum(a.amount), max(c.category)
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
  and b.account_type = 'sale'
left join bspp.sale_accounts c on b.account = c.account  
where a.control = '16271531'  
group by b.account, b.description, b.department_code


-- think this is what will interest jeri
-- ~ 40k a month more on old pay plan based on any ro with bs writers on it
select aa.*, bb.*, old_total_sales - new_total_sales as diff
from (
  select a.year_month, 
    sum(a.total_parts)::integer as old_parts, sum(a.labor)::integer as old_labor, 
    sum (a.total_parts + a.labor)::integer as old_total_sales  
  from old_plan a
  where a.year_month between 201703 and 201802
  group by a.year_month) aa  
left join (
  select b.year_month, 
    sum(b.total_parts)::integer as new_parts, sum(b.labor)::integer as new_labor, 
    sum (b.total_parts + b.labor)::integer as new_total_sales  
  from new_plan b
  where b.year_month between 201703 and 201802
    and exists (
      select 1
      from old_plan
      where ro = b.ro
        and year_month = b.year_month)
  group by b.year_month) bb on aa.year_month = bb.year_month
order by aa.year_month



-- take a month, 201711, for all those where new plan is less, look at actual accounting for the ro
-- where diff > 500
drop table if exists base_1;
create temp table base_1 as
select a.*, coalesce(b.total_sales, 0)::integer as new_plan_sales
from old_plan a
left join new_plan b on a.ro = b.ro -- and a.year_month = b.year_month
where a.year_month = 201711
  and a.total_sales - coalesce(b.total_sales, 0) > 500
order by a.ro;

select a.*, c.account, c.description, -b.amount as amount
from base_1 a
left join fin.fact_gl b on a.ro = b.control
  and b.post_status = 'Y'
inner join fin.dim_account c on b.account_key = c.account_key  
  and c.account_type = 'sale'




