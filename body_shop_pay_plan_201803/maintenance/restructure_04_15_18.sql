﻿/*
effective 4/15/18

update bspp.roles
set commission_perc = .0053
where the_role = 'csr';

update bspp.roles
set commission_perc = .0067
where the_role = 'vda';

shit, need to add the fucking dates, otherwise lose the ability to recreate
pay from the past
and the fucking web page allows them to log into previous pay periods

select * 
from bspp.roles a
left join bspp.personnel_role b on a.the_role = b.the_role


alter table bspp.roles
drop constraint roles_pkey;

can not drop the PK. because of the FK relationship

need to restructure, 
get rid of table bspp.roles, put everything in personnel_roles, with from_thru
*/
----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------
-- functions for valid dates
create or replace function bspp.valid_from_date(_the_date DATE)
returns boolean as
$BODY$
/*
select bspp.valid_from_date('03/04/2018');
*/
select exists (
  select 1
  from dds.dim_date
  where biweekly_pay_period_start_date = _the_date);
$BODY$
language sql; 

create or replace function bspp.valid_thru_date(_the_date DATE)
returns boolean as
$BODY$
/*
select bspp.valid_thru_date('04/14/2018');
*/
select 
  case
    when _the_date = '12/31/9999' then true
  else
    exists (
    select 1
    from dds.dim_date
    where biweekly_pay_period_end_date = _the_date)
  end;
$BODY$
language sql; 

drop table if exists bspp.personnel_roles_new;
create table bspp.personnel_roles_new (
  user_name citext NOT NULL references bspp.personnel(user_name),
  the_role citext NOT NULL,
  base_pay_value numeric(6,2) NOT NULL,
  base_pay_type citext not null,
  commission_perc numeric, 
  from_date date not null check (bspp.valid_from_date(from_date)),
  thru_date date not null default '12/31/9999'::date check (bspp.valid_thru_date(thru_date)),
--   constraint valid_from_date check (bspp.is_pay_period_date(from_date)),
  primary key (user_name,thru_date));

drop table if exists old_data;
create temp table old_data as 
select distinct a.*, b.base_pay_type, b.commission_perc, '03/04/2018'::date,
  case
    when a.the_role in ('vda','csr') then '04/14/2018'::date
    else '12/31/9999'::date
  end
from bspp.personnel_role a 
left join bspp.roles b on a.the_role = b.the_role;

insert into bspp.personnel_roles_new
select * from old_data;

insert into bspp.personnel_roles_new
select user_name, the_role, base_pay_value, base_pay_type,
  case 
    when the_role = 'csr' then .0047
    when the_role = 'vda' then .0058
  end as commission_perc,
  '04/15/2018'::Date, '12/31/9999'::date
from old_data
where the_role in ('csr','vda');

drop table bspp.personnel_role;
drop table bspp.roles;
alter table bspp.personnel_roles_new
rename to personnel_roles;

--
-- pay periods query:
-- Function: bspp.get_pay_periods()

-- DROP FUNCTION bspp.get_pay_periods();

CREATE OR REPLACE FUNCTION bspp.get_pay_periods()
  RETURNS SETOF json AS
$BODY$
/*
update for 4/15 revision, past pay periods would no longer be
represented correctly

select * from bspp.get_pay_periods()
*/
declare
  _thru_pay_period_seq integer := (
    select distinct biweekly_pay_period_sequence
    from dds.dim_date
    where the_date = current_date);
  _from_pay_period_seq integer := _thru_pay_period_seq - 7; 
begin  
return query
-- select '[' || row_to_json(x) || ']'
select row_to_json(A)
from (
select array_to_json(array_agg(row_to_json(x))) as pay_periods
from (
  select distinct to_char(biweekly_pay_period_start_date, 'Mon') 
    || ' ' || to_char(biweekly_pay_period_start_date, 'DD')
    || ' - ' 
    || to_char(biweekly_pay_period_end_date, 'Mon') 
    || ' ' || to_char(biweekly_pay_period_end_date, 'DD')
    || ', ' || to_char(biweekly_pay_period_end_date, 'YYYY') as pay_period_display,
    biweekly_pay_period_sequence - _thru_pay_period_seq as pay_period_indicator
  from dds.dim_date 
  where biweekly_pay_period_sequence between _from_pay_period_seq and _thru_pay_period_seq
    and the_date > '04/15/2018'
  order by biweekly_pay_period_sequence - _thru_pay_period_seq desc) x) A;
end 
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION bspp.get_pay_periods()
  OWNER TO rydell;


-- estimator my pay page query
alter function bspp.json_get_pay_summary(citext, integer)
rename to z_unused_json_get_pay_summary_pre_041518;


-- Function: bspp.json_get_pay_summary(citext, integer)

CREATE OR REPLACE FUNCTION bspp.json_get_pay_summary(
    _employee_number citext,
    _pay_period_indicator integer)
  RETURNS SETOF json AS
$BODY$  

/*
04/15/18
  sales now includes account 147300 for everyone, sales is the same for all roles
  new estimator commission percentages
  
select employee_number from bspp.personnel where last_name = 'blumhagen'

select * from bspp.json_get_pay_summary('160930', 0)

select * from bspp.personnel_roles
*/

declare
  _user_name citext := (
    select user_name
    from bspp.personnel
    where employee_number = _employee_number);
  _user_key uuid := (
    select user_key
    from nrv.users
    where employee_number = _employee_number);
  _current_pay_period_seq integer := (
    select distinct biweekly_pay_period_sequence
    from dds.dim_date
    where the_date = current_date);
  _from_date date := (
    select distinct biweekly_pay_period_start_date
    from dds.dim_date
    where biweekly_pay_period_sequence = _current_pay_period_seq + _pay_period_indicator);
  _thru_date date := (
    select distinct biweekly_pay_period_end_date
    from dds.dim_date
    where biweekly_pay_period_sequence = _current_pay_period_seq + _pay_period_indicator);   
  _user_role citext := (
    select the_role
    from bspp.personnel_roles
    where user_name = _user_name
      and from_date <= _from_date
      and thru_date >= _thru_date);     

begin
  case
    when _user_role = 'shop_foreman' then
      return query
      select row_to_json (zzz) 
      from (
        select row_to_json(xxx) as body_shop_pay
        from ( 
          with 
            ros as (
              select ro, parts, labor, 
                parts + labor as ro_sales 
              from (  
                select ro,
                  coalesce(sum(amount * multiplier) filter (where category like '%parts'), 0) as parts,
                  coalesce(sum(amount) filter (where category like '%labor'), 0) as labor
                from bspp.bs_sales_detail
                where the_date between _from_date and _thru_date
                group by ro) a),
            sales as (
              select sum(parts) as parts_total,
                sum(labor) as labor_total,
                sum(parts + labor) as parts_and_labor_sales
              from ros)
          select 'user_key'as user, z.*, 
              ( -- ros: an array of ro objects: multiple rows
                select coalesce(array_to_json(array_agg(row_to_json(g))), '[]')
                from ( -- g one row per ro
                  select * 
                  from ros) g) as ros
          from ( -- z all the individual's pay data + total pay:  single row
            select x.*, salary + commission_pay as total_pay
            from ( -- x all the individual's pay data elements:  single row
                select  _employee_number as id, b.base_pay_value as Salary, 
                  b.commission_perc as commission_percentage, 
                  (select round(parts_and_labor_sales, 2) from sales) as parts_and_labor_sales,
                  (select round(parts_total, 2) from sales) as parts_total,
                  (select round(labor_total, 2) from sales) as labor_total,
                  round(b.commission_perc * (select parts_and_labor_sales from sales), 2) as commission_pay
                from bspp.personnel a
                inner join bspp.personnel_roles b on a.user_name = b.user_name
                  and b.from_date <= _from_date
                  and b.thru_date >= _thru_date                
                where a.user_name = _user_name) x) z) xxx ) zzz;


                  
    when _user_role in ('parts','vda','csr') then

      return query
      select row_to_json (zzz) 
      from (
        select row_to_json(xxx) as body_shop_pay
        from ( 
          with 
            ros as (
              select ro, parts, labor, 
                parts + labor as ro_sales 
              from (  
                select ro,
                  coalesce(sum(amount * multiplier) filter (where category like '%parts'), 0) as parts,
                  coalesce(sum(amount) filter (where category like '%labor'), 0) as labor
                from bspp.bs_sales_detail
                where the_date between _from_date and _thru_date
                group by ro) a),
            sales as (
              select sum(parts) as parts_total,
                sum(labor) as labor_total,
                sum(parts + labor) as parts_and_labor_sales
              from ros)
          select _user_key as user, z.*, 
              ( -- ros: an array of ro objects: multiple rows
                select coalesce(array_to_json(array_agg(row_to_json(g))), '[]')
                from ( -- g one row per ro
                  select * 
                  from ros) g) as ros
          from ( -- z all the individual's pay data + total pay:  single row
          select x.*, regular_pay + overtime_pay + pto_pay + commission_pay + ot_variance as total_pay 
          from ( -- x all the individual's pay data elements:  single row
              select  _employee_number as id, b.base_pay_value as hourly_rate, round(1.5 * b.base_pay_value, 2) as overtime_rate,  
                a.pto_rate,
                b.commission_perc as commission_percentage, 
                coalesce(c.regular_clock_hours, 0) as regular_clock_hours,
                coalesce(c.overtime_hours, 0) as overtime_hours,
                coalesce(c.pto_hours, 0) as pto_hours,
                coalesce(round(c.regular_clock_hours * b.base_pay_value, 2), 0) as regular_pay,
                coalesce(round(c.overtime_hours * 1.5 * b.base_pay_value, 2), 0) as overtime_pay,
                coalesce(round(a.pto_rate * c.pto_hours, 2), 0) as pto_pay,
                (select round(parts_and_labor_sales, 2) from sales) as parts_and_labor_sales,
                (select round(parts_total, 2) from sales) as parts_total,
                (select round(labor_total, 2) from sales) as labor_total,
                round(b.commission_perc * (select parts_and_labor_sales from sales), 2) as commission_pay,
                 case
                   when c.regular_clock_hours + c.overtime_hours = 0 then 0
                   else coalesce(round((.5 * c.overtime_hours * 
                     (b.commission_perc * (select parts_and_labor_sales from sales)))/(c.regular_clock_hours + c.overtime_hours), 2), 0) 
                  end as ot_variance
              from bspp.personnel a
              inner join bspp.personnel_roles b on a.user_name = b.user_name 
               and b.from_date <= _from_date
               and b.thru_date >= _thru_date
              LEFT join ( -- users clock hours
                select user_name, sum(reg_hours) as regular_clock_hours, sum(ot_hours) as overtime_hours,
                  sum(pto_hours + hol_hours) as pto_hours
                from bspp.clock_hours
                where the_date between _from_date and _thru_date
                group by user_name) c on a.user_name = c.user_name
              where a.user_name = _user_name) x) z) xxx ) zzz;  

                    
    else 
      return query
      select row_to_json(A)
      from (
        select  json_build_object('id',_employee_number,'user',_user_key) as body_shop_pay  ) A;     
  end case;   

end
$BODY$
  LANGUAGE plpgsql;


