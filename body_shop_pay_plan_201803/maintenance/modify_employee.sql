﻿-- current population

select a.last_name, a.first_name, a.user_name, a.employee_number, a.writer_id,
  b.the_role, b.base_pay_value, b.commission_perc
-- select *  
from bspp.personnel a
join bspp.personnel_roles b on a.user_name = b.user_name
  and b.thru_date > current_date

-- 08/28/2023
-- Jon, 
-- 
-- Matt Paschke and Brian Hill have been moved to straight hourly and will no longer receive any sort of commission. 
-- 
-- Thank you, 
-- 
-- Tim 
select * from bspp.personnel_roles where user_name in ('mpaschke@rydellcars.com','bhill@rydellcars.com')

update bspp.personnel_roles
set thru_date = '08/26/2023'
where user_name in ('mpaschke@rydellcars.com','bhill@rydellcars.com')
  and thru_date = '12/31/9999';

-- update bspp.personnel_roles
-- set thru_date = '12/31/9999'
-- where user_name in ('mpaschke@rydellcars.com','bhill@rydellcars.com')
--   and thru_date = '08/26/2023';


-- dave lueker, termed 10/15/21
-- constraint personnel_roles_new_thru_date_check: thru date must be the last day of a biweekly pay period
select * from dds.dim_date where the_date = '07/28/2022'

select * from bspp.personnel_roles where user_name = 'aostlund@rydellcars.com'

update bspp.personnel_roles
set thru_date = '07/30/2022'
where user_name = 'dlueker@rydellcars.com'
  and commission_perc = .005

  

-- ann ostlund, termed 10/15/21
-- constraint personnel_roles_new_thru_date_check: thru date must be the last day of a biweekly pay period
select * from dds.dim_date where the_date = '10/15/2021'

select * from bspp.personnel_roles where user_name = 'aostlund@rydellcars.com'

update bspp.personnel_roles
set thru_date = '10/23/2021'
where user_name = 'aostlund@rydellcars.com'
  and commission_perc = .0047

/*
06/11/20
We have switched Tara from an estimator to our admin assistant.  Her pay rate is changing from the $9.00 hourly with commission to $15.50 straight hourly.  If you could please change the vision page and remove her from the payroll sheet that would be great!

Thank you

John Gardner
*/
select * from bspp.personnel_roles order by user_name, from_date

-- constraint personnel_roles_new_thru_date_check: thru date must be the last day of a biweekly pay period
update bspp.personnel_roles
set thru_date = '06/06/2020'
where user_name = 'tmcmillian@rydellcars.com'
  and thru_date > current_date

/*
I had emailed you about Matt Paschke changing to a VDA commission estimator back in March.  
Jeri looked at it and would like his percentage to be at .58% compared to what I had put him at before.  
He will be at $8 an hour and .58% commission starting 4/27/2020 please.  
If you have anything you need from me, let me know right away please.

Thank you
John Gardner

04/28
*/
select *
from bspp.personnel_roles
order by the_role, base_pay_value

update bspp.personnel set user_name = 'mpaschke@rydellcars.com' where user_name = '	mpaschke@rydellcars.com'
update bspp.personnel_roles set user_name = 'mpaschke@rydellcars.com' where user_name = '	mpaschke@rydellcars.com'
update bspp.personnel_roles set user_name = 'mpaschke@rydellcars.com' where user_name = '	mpaschke@rydellcars.com'

alter table bspp.personnel_roles
drop constraint personnel_roles_new_user_name_fkey



update bspp.personnel
set pto_rate = 8
-- select * from bspp.personnel
where user_name = 'mpaschke@rydellcars.com';

update bspp.personnel_roles
set thru_date = '04/25/2020'
where user_name = 'mpaschke@rydellcars.com';

insert into bspp.personnel_roles values('mpaschke@rydellcars.com','vda',8,'hourly',.0058,'04/26/2020','12/31/9999');