﻿----------------------------------------------------------
--< 04/27/23
----------------------------------------------------------
Jon, 
Could please you bump Lorens commission percentage from .55 to .61 so he matches Dan?
-- still using the old values for loren, don't know if he is a vda or a csr, so leave him as shop foreman
update bspp.personnel_roles
set thru_date = '04/22/2023'
where user_name = 'lshereck@rydellcars.com';

insert into bspp.personnel_roles(user_name,the_role,base_pay_value,base_pay_type,
	commission_perc,from_date)
values('lshereck@rydellcars.com','shop foreman',1250,'salary',.0061,'04/23/2023');
----------------------------------------------------------
--/> 04/27/23
----------------------------------------------------------
----------------------------------------------------------
--< 10/27/2021
----------------------------------------------------------
-- We are adjusting the % for the estimators commission pay.
-- 
-- Dan Evavold -from .0058 to .0061%
-- Brian Hill – from .0058 to .0061%
-- Kathy Blumhagen – from .0055 to .0058%
-- Dave Lueker – from .0047 to .0050%
-- Matt Paschke – from .0058 to .0061%
-- 
-- Thank you
-- 
-- John Gardner

do
$$
declare 
  _from_date date := '10/24/2021';
  _thru_date date := '10/23/2021';
  _user_name citext := (select user_name from bspp.personnel where last_name = 'paschke');
  _the_role citext := (select the_role from bspp.personnel_roles where user_name = _user_name and thru_date > current_date);
  _base_pay_value numeric := (select base_pay_value from bspp.personnel_roles where user_name = _user_name and thru_date > current_date);
  _base_pay_type citext := (select base_pay_type from bspp.personnel_roles where user_name = _user_name and thru_date > current_date);
  _commission_perc numeric := .0061;
  
begin  

  update bspp.personnel_roles
  set thru_date = _thru_date
  where user_name = _user_name
    and thru_date > current_date;

  insert into bspp.personnel_roles(user_name,the_role,base_pay_value,base_pay_type,
    commission_perc,from_date)
  values(_user_name,_the_role,_base_pay_value,_base_pay_type,_commission_perc,_from_date);
  
end
$$;

select * from bspp.personnel_roles where thru_date > current_Date
----------------------------------------------------------
--/> 10/27/2021
----------------------------------------------------------


effective 4/15/18

update bspp.roles
set commission_perc = .0053
where the_role = 'csr';

update bspp.roles
set commission_perc = .0067
where the_role = 'vda';

shit, need to add the fucking dates, otherwise lose the ability to recreate
pay from the past

select * 
from bspp.roles a
left join bspp.personnel_role b on a.the_role = b.the_role

alter table bspp.roles
drop constraint roles_pkey;

------------------------------------------------------------------------------
-- 7/10/19 evavold got an hourly increase, from 10 to 11
------------------------------------------------------------------------------

do
$$
declare 
  _from_date date := '06/23/2019';
  _thru_date date := '06/22/2019';
  _hourly_rate numeric(6,2) := 11;
  _user_name citext = 'devavold@rydellcars.com';
  _the_role citext;
  _base_pay_type citext;
  _commission_perc numeric;
  
begin  
  update bspp.personnel
  set pto_rate = _hourly_rate
  where user_name = _user_name;

  _the_role = (
    select the_role 
    from bspp.personnel_roles 
    where user_name = _user_name
      and thru_date > current_date);
  _base_pay_type = (
    select base_pay_type
    from bspp.personnel_roles 
    where user_name = _user_name
      and thru_date > current_date);  
  _commission_perc = (
    select commission_perc 
    from bspp.personnel_roles 
    where user_name = _user_name
      and thru_date > current_date);  

  update bspp.personnel_roles
  set thru_date = _thru_date
  where user_name = _user_name
    and thru_date > current_date;

  insert into bspp.personnel_roles(user_name,the_role,base_pay_value,base_pay_type,
    commission_perc,from_date)
  values(_user_name,_the_role,_hourly_rate,_base_pay_type,_commission_perc,_from_date);
  
end
$$;
