﻿/*
12/21/20
John had me break out the internal paint and metal labor. I have added these accounts 
to the pay plans (147302 and 147303).  This was the only change that was made to each 
of the attached plans.  It does not change the actual pay plan at all so I left the 
effective dates the same and added a version date.  Let me know if you have any questions.
*/
insert into bspp.sale_accounts
select c.gl_account, d.account_key, d.description, b.page, b.line, 'internal labor',1.0
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
join fin.dim_fs_account c on a.fs_Account_key = c.fs_account_key
  and c.gl_account in ('147302','147303')
join fin.dim_account d on c.gl_account = d.account; 


-- need to retroactively update the data in bspp.bs_sales_detail to account for these 2 new accounts
do $$
declare
  _from_date date := '12/06/2020';
  _thru_date date := '12/19/2020'; 
begin
delete
from bspp.bs_sales_detail
where the_date between _from_date and _thru_date;

insert into bspp.bs_sales_detail
select c.the_date, a.control as ro, b.account, b.category, b.multiplier, sum(-a.amount) as amount
from fin.fact_gl a
inner join bspp.sale_accounts b on a.account_key = b.account_key
inner join dds.dim_date c on a.date_key = c.date_key
where post_status = 'Y'
  and c.the_date between _from_date and _thru_date
group by c.the_date, a.control, b.account, b.category, b.multiplier;  
end $$;


-- for john gardner asking about the previous pay period
select sum(multiplier * amount) -- 214995.87
from bspp.bs_sales_detail
where the_date between '11/22/2020' and '12/05/2020'

select sum(multiplier * amount) -- 224250.57
from (
select c.the_date, a.control as ro, b.account, b.category, b.multiplier, sum(-a.amount) as amount
from fin.fact_gl a
inner join bspp.sale_accounts b on a.account_key = b.account_key
inner join dds.dim_date c on a.date_key = c.date_key
where post_status = 'Y'
  and c.the_date between '11/22/2020' and '12/05/2020'
group by c.the_date, a.control, b.account, b.category, b.multiplier) x


select 224250.57 - 214995.87 = 9254.70

-- -- so now of course, john:
-- Would you be able to help me determine what they were shorted during that pay period then?  I need to get them paid for the missing amount.


select a.last_name, a.first_name, a.employee_number, commission_perc, round(commission_perc * 9254.7, 2)
from bspp.personnel a
join bspp.personnel_roles b on a.user_name = b.user_name
  and b.thru_date > current_date
