﻿-- current population

select a.last_name, a.first_name, a.user_name, a.employee_number, a.writer_id,
  b.the_role, b.base_pay_value, b.commission_perc
-- select *  
from bspp.personnel a
join bspp.personnel_roles b on a.user_name = b.user_name
  and b.thru_date > current_date




--------------------------------------------------------------------------------------------
03/09/2020

I am going to switch Matt Paschke from his current hourly rate to the same commission plan that the other 5 CSR/VDA estimators have.  
His hourly wage should be at $9.00 and his commission rate should be set at 0.51% and his PTO should be at the $9.00, 
if this seems right let me know and I will submit a Compli form.  If there is anything else you need me to do please let me know. 
If this could go into effect on 3/16/2020 I would appreciate it.

He is part of the VDA team.

Thank you
John Gardner


do $$
  declare
    user_name citext := '	mpaschke@rydellcars.com';
    employee_number citext := '1109700';
    last_name citext := 'Paschke';
    first_name citext := 'Matthew';
    writer_id citext := '497';
    pto_rate numeric(6,2) := 9.00;
    the_role citext := 'vda';
    base_pay_type citext := 'hourly';
    commission_perc numeric := 0.0051;
    from_date date := '03/15/2020';
  begin
    insert into bspp.personnel (user_name,employee_number,last_name,first_name,writer_id,pto_rate)
    values(user_name,employee_number,last_name,first_name,writer_id,pto_rate);

    insert into bspp.personnel_roles (user_name,the_role,base_pay_value,base_pay_type,
      commission_perc, from_date)
    values (user_name,the_role,pto_rate,base_pay_type,commission_perc,from_date);
end $$;


---------------------------------------------------------------------------------------------
new employee Tara McMillian
writer_id: 811
employee_number: 198542
pay plan based on Body Shop CSR Entry-Level Compensation Plan

select * from bspp.personnel_roles where thru_Date > current_date

do $$
  declare
    user_name citext := 'tmcmillian@rydellcars.com';
    employee_number citext := '198542';
    last_name citext := 'McMillian';
    first_name citext := 'Tara';
    writer_id citext := '811';
    pto_rate numeric(6,2) := 9.00;
    the_role citext := 'csr';
    base_pay_type citext := 'hourly';
    commission_perc numeric := 0.0025;
    from_date date := '09/15/2019';
  begin
    insert into bspp.personnel (user_name,employee_number,last_name,first_name,writer_id,pto_rate)
    values(user_name,employee_number,last_name,first_name,writer_id,pto_rate);

    insert into bspp.personnel_roles (user_name,the_role,base_pay_value,base_pay_type,
      commission_perc, from_date)
    values (user_name,the_role,pto_rate,base_pay_type,commission_perc,from_date);
end $$;

