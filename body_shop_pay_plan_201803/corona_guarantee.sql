﻿drop table bspp.corona_guarantee;
create table bspp.corona_guarantee (
  employee_number citext not null,
  employee_name citext not null,
  guarantee numeric(8,2) not null);


insert into bspp.corona_guarantee values('1146991','WALDEN, CHRIS W',2702.82666666667);
insert into bspp.corona_guarantee values('1110425','PETERSON, BRIAN D',3524.93333333333);
insert into bspp.corona_guarantee values('160930','BLUMHAGEN, KATHERINE M',2373.15333333333);
insert into bspp.corona_guarantee values('135770','DRISCOLL, TERRANCE',3146.12266666667);
insert into bspp.corona_guarantee values('188338','LUEKER, DAVID C',2082.684);
insert into bspp.corona_guarantee values('165357','HILL, BRIAN',2610.07866666667);
insert into bspp.corona_guarantee values('1118722','ROSE, CORY',3142.20533333333);
insert into bspp.corona_guarantee values('1147061','WALTON, JOSHUA',2232.53733333333);
insert into bspp.corona_guarantee values('141069','EVAVOLD, DANIEL J',2587.87866666667);
insert into bspp.corona_guarantee values('191350','MAVITY, ROBERT',2308.35466666667);
insert into bspp.corona_guarantee values('1125565','SEVIGNY, SCOTT',2498.51866666667);
insert into bspp.corona_guarantee values('171055','JACOBSON, PETER A',3597.72);
insert into bspp.corona_guarantee values('1108200','OSTLUND, ANNE B',2077.788);
insert into bspp.corona_guarantee values('150105','GARDNER, CHAD A',2701.672);
insert into bspp.corona_guarantee values('1111325','POWELL, GAYLA R',1921.74933333333);
insert into bspp.corona_guarantee values('1106400','OLSON, JUSTIN S',1874.02666666667);
insert into bspp.corona_guarantee values('1107950','ORTIZ, GUADALUPE',2123.104);
insert into bspp.corona_guarantee values('150120','GARDNER, JOHN T',2104.61466666667);
insert into bspp.corona_guarantee values('184920','LENE, RYAN',2708.648);
insert into bspp.corona_guarantee values('1114921','REUTER, TIMOTHY J',2240.30666666667);
insert into bspp.corona_guarantee values('11732','AGUILAR, ANNA',1325.77733333333);
insert into bspp.corona_guarantee values('184614','LAMONT, BRANDON',2553.16266666667);
insert into bspp.corona_guarantee values('11660','ADAM, PATRICK',2538.44666666667);
insert into bspp.corona_guarantee values('1100625','OLSON, CODEY',2196.088);
insert into bspp.corona_guarantee values('137101','EBERLE, MARCUS M',1725.38133333333);
insert into bspp.corona_guarantee values('165275','HICKS, EMILY',1131.824);
insert into bspp.corona_guarantee values('1126040','SHERECK, LOREN',2490.70133333333);
insert into bspp.corona_guarantee values('151702','MULHERN, ALAN',1872.184);
insert into bspp.corona_guarantee values('145789','BUCHANAN, KYLE',2471.67466666667);
insert into bspp.corona_guarantee values('122558','GRYSKIEWICZ, JONATHAN',1740.45333333333);
insert into bspp.corona_guarantee values('15648','DIBI, ANDREW',1577.37466666667);
insert into bspp.corona_guarantee values('165327','GUST, MICAH',122.16);
insert into bspp.corona_guarantee values('198542','MCMILLIAN, TARA',1432.568);
insert into bspp.corona_guarantee values('167934','PFAFF, JONATHAN',1444.97066666667);
insert into bspp.corona_guarantee values('157953','BIES II, DAVID',2901.86666666667);
insert into bspp.corona_guarantee values('142589','HLAVAC, TERRI',1419.324);
insert into bspp.corona_guarantee values('1109700','PASCHKE, MATTHEW G',2135.07866666667);
insert into bspp.corona_guarantee values('162984','WIKSTROM, ABIGAIL',1080.56933333333);
insert into bspp.corona_guarantee values('174384','MOGAVERO, MICHAEL',1464.46);


 select * from bspp.corona_guarantee