﻿bspp.bs_ros
any ro for which writer is main writer or alt writer

                    SELECT ro, writer_id,
                      coalesce((
                        select
                          case opcodekey
                            when 26302 then '312'
                            when 26303 then '311'
                            when 26304 then '303'
                            when 26305 then '317'
                            when 26306 then '712'
                            when 26307 then '403'
                            when 26857 then '318'
                          end as alt_writer_id
                        from ads.ext_fact_repair_order
                        where ro = d.ro
                          and opcodekey in(26302,26303,26304,26305,26306,26307,26857)
                          group by opcodekey), 'none') as alt_writer_id
                    FROM (
                      SELECT a.ro, c.writernumber AS writer_id
                      FROM ads.ext_fact_repair_order a
                      INNER JOIN (
                        select a.control
                        from fin.fact_gl a
                        inner join fin.dim_account b on a.account_key = b.account_key
                        inner join bspp.sale_accounts c on b.account = c.account
                        inner join dds.dim_date e on a.date_key = e.date_key
                          and e.biweekly_pay_period_sequence = (
                            select biweekly_pay_period_sequence
                            from dds.dim_date
                            where the_date = current_date -1)
                        where a.post_status = 'Y'
                        group by a.control) b on a.ro = b.control
                      INNER JOIN ads.ext_dim_service_writer c on a.servicewriterkey = c.servicewriterkey
                      GROUP BY a.ro, c.writernumber) d

bspp.sales

                    select s.writer_id, s.alt_writer_id, r.the_date, r.control, r.gm_parts, r.non_gm_parts,
                      r.gm_parts + r.non_gm_parts as total_parts, r.labor,
                      r.gm_parts + r.non_gm_parts + r.labor as total_sales
                    from ( -- r: date, ro, amounts; 1 row per ro/date; 147700(GM Parts): multiplier = .5
                      select e.the_date, a.control,
                        round(sum(case when c.category = 'parts' and b.account = '147700'
                          then -1 * a.amount * c.multiplier else 0 end), 2) as gm_parts,
                        round(sum(case when c.category = 'parts' and b.account <> '147700'
                          then -1 * a.amount * c.multiplier else 0 end), 2) as non_gm_parts,
                        round(
                          sum(
                            case when c.category = 'labor' then -1 * a.amount * c.multiplier else 0 end), 2) as labor
                      from fin.fact_gl a -- same query that generates bs_ro_numbers.csv
                      inner join fin.dim_account b on a.account_key = b.account_key
                      inner join bspp.sale_accounts c on b.account = c.account
                      inner join dds.dim_date e on a.date_key = e.date_key
                        and e.biweekly_pay_period_sequence = (
                          select biweekly_pay_period_sequence
                          from dds.dim_date
                          where the_date = current_date - 1)
                      where a.post_status = 'Y'
                      group by a.control, e.the_date) r
                    inner join bspp.z_unused_bs_ros s on r.control = s.ro                      



bspp.update_pay_period_transactions()


bspp.update_writer_sales()


select *
from bspp.bs_sales_detail
where ro = '18057603'



select c.the_date, a.control as ro, bb.account, /*b.category, b.multiplier,*/ amount, bb.account_type, bb.department, bb.description
from fin.fact_gl a
-- inner join bspp.sale_accounts b on a.account_key = b.account_key
inner join fin.dim_account bb on a.account_key = bb.account_key
  
inner join dds.dim_date c on a.date_key = c.date_key
inner join fin.dim_doc_type d on a.doc_type_key = d.doc_type_key
where post_status = 'Y'
--   and c.the_date between _from_date and _thru_date
  and d.doc_type = 'service tickets' 
  and a.control = '18057415'
  and bb.account_type = 'Sale'
order by account


select *
from bspp.bs_sales_detail
where ro = '18057415'



----------------------------------------------------------------------------------------------

-- tried grouping fact_gl on dept, yuck
select *
from (
  select *
  from bspp.z_unused_writersales a
  where the_date > '12/31/2017'
  limit 100) x
left join (
  select a.control,
    sum(amount) filter (where department_code = 'bs') as bs,
    sum(amount) filter (where department_code = 'pd') as pd,
    sum(amount) filter (where department_code = 'ql') as ql,
    sum(amount) filter (where department_code = 're') as re,
    sum(amount) filter (where department_code = 'sd') as sd
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
    and b.account_type = 'sale'
  inner join dds.dim_date c on a.date_key = c.date_key
    and c.the_year = 2018  
  where post_status = 'Y'
  group by a.control) y on x.ro = y.control


-- only of of these where total does not add up
  select *
  from bspp.z_unused_writersales
  where total_sales <> total_parts + labor  


-- pay period transactions

select e.the_date, a.control, b.account, b.department, b.description, sum(a.amount) as amount
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
  and b.account_type = 'sale'
  and b.current_row = true
inner join dds.dim_date e on a.date_key = e.date_key
  and e.biweekly_pay_period_sequence = (
    select biweekly_pay_period_sequence
    from dds.dim_date
    where the_date = current_date -30)
where a.post_status = 'Y'
group by a.control, b.account, e.the_date, b.description, b.department;



-- bspp.update_writer_sales()

      select m.ro, m.writernumber as writer_id, coalesce(n.alt_writer_id, 'none') as alt_writer_id
      from ( -- writer and alt_writer where bs est is writer: 1 row per ro
          select a.ro, c.writernumber
          from ads.ext_fact_repair_order  a
          inner join ads.ext_dim_service_writer c on a.servicewriterkey = c.servicewriterkey 
            and c.writernumber in ('312','311','303','317','712','403','318')
          left join ads.ext_dim_opcode d on a.opcodekey = d.opcodekey
            and d.opcodekey in (26302,26303,26304,26305,26306,26307,26857)
          group by a.ro, c.writernumber) m
        left join ( -- alt_writer where bs est is writer : 1 ro per ro
          select a.ro, 
            case a.opcodekey
              when 26302 then '312'
              when 26303 then '311'
              when 26304 then '303'
              when 26305 then '317'
              when 26306 then '712'
              when 26307 then '403'
              when 26857 then '318'
              else 'none'
            end as alt_writer_id
          from ads.ext_fact_repair_order  a
          inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
            and c.opcodekey between 26302 and 26307
          where a.servicewriterkey in (
            select servicewriterkey
            from ads.ext_dim_service_Writer
            where  writernumber in (
              select writer_id
              from bspp.personnel b 
              inner join bspp.personnel_role c on b.user_name = c.user_name
                and c.the_role in ('vda','csr')))
          and ro not in ('18049420')
          group by a.ro, a.opcodekey) n on m.ro = n.ro





-- are they still adding alt_writer?
-- yep
select max(b.the_date)
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.opendatekey = b.date_key
inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
  and c.opcodekey in (26302,26303,26304,26305,26306,26307,26857)
          

so what i am going to have to do is to isolate and analyze pre payplan change pay and see how much
was paid on accounts other than what is in bspp.sale_accounts  

or, probably easier, i can look at past pay, and generate pay for the same ros with new pay plan 

yes yes yes

select *
from (
select * 
from bspp.z_unused_writersales
where the_date between '01/21/2018' and '02/03/2018') x
left join (
select control,
  sum(-amount) filter (where category = 'gm parts') as gm_parts,
  sum(-amount) filter (where category = 'non gm parts') as nopn_gm_parts,
  sum(-amount) filter (where category like '%parts') as total_parts,
  sum(-amount) filter (where category = 'labor') as labor,
  sum(-amount) filter (where category = 'internal labor') as int_labor,
  sum(-amount) as total_sales
from fin.fact_gl a
inner join bspp.sale_accounts b on a.account_key = b.account_key
inner join dds.dim_date c on a.date_key = c.date_key
inner join fin.dim_doc_type d on a.doc_type_key = d.doc_type_key
where post_status = 'Y'
  and c.the_date between '01/01/2018' and '03/03/2018'
  and d.doc_type = 'service tickets' 
group by a.control) y on x.ro = y.control


select a.writer_id, a.last_name, writer + alt_writer
from  (
select a.writer_id, b.last_name, sum(total_sales) as writer
from bspp.z_unused_writersales a
left join bspp.personnel b on a.writer_id = b.writer_id
where the_date between '01/21/2018' and '02/03/2018'
group by a.writer_id, a.last_name) a
full outer join (
select alt_writer_id, sum(total_sales) as alt_writer
from bspp.z_unused_writersales
where the_date between '01/21/2018' and '02/03/2018'
group by alt_writer_id) b on a.writer_id = b.alt_writer_id
