﻿/*
4/17, now works for the 4/15 payplan: vda: .58, csr: .47, 147300 (internal labor) included for everyone
  eliminated table bspp.roles, all info include in (renamed) bspp.personnel_roles

update bspp.personnel_roles
set thru_date = '08/18/2018'
where user_name = 'msteinke@rydellcars.com'  
  and thru_date = '12/31/9999'

04/26
  added corona guarantee  

05/01/2020
    matt paschke is not showing up in accrual
    *a* changed the from/thru on b spp.personnel_roles
07/07/20
  removed corona guarantee


12/19/20
    per Jeri's latest directive on overtime variance, PTO & holiday hours should be included in the denominator
      
12/21/20
    per Jeri, accounts 147302 & 147303 have been added to bspp.sale_accounts, bspp.bs_sale_details has been
    retroactively updated to include sales to those accounts for pay period 12/6 -> 12/19/20
1/2/22
	modified for managers spreadsheet, remove all the hourly stuff
*/
do 
$$
declare _from_date date := '12/19/2021';
declare _thru_date date := '12/31/2021';

begin
drop table if exists bs_payroll;
create temp table bs_payroll as

with 
  sales as (
    select round(sum(amount) filter (where category like '%labor'), 2) as all_labor,
      round(sum(multiplier * amount) filter (where category like '%parts'), 2) as parts
    from bspp.bs_sales_detail
    where the_date between _from_date and _thru_date)  
-- select e.*, 
--   coalesce(reg_pay, 0) + coalesce(ot_pay, 0) + coalesce(pto_hol_pay, 0) 
--   + coalesce(ot_variance, 0) + coalesce(commission_pay, 0) as total_pay
-- from (    
  select a.last_name, a.first_name, a.employee_number as "emp#", 
    round(100 * b.commission_perc, 2) as "commission %", --a.pto_rate,
--     case when b.base_pay_type = 'hourly' then b.base_pay_value end as hourly_rate,
--     case when b.base_pay_type = 'hourly' then 1.5 * b.base_pay_value end as ot_rate,
--     case when b.base_pay_type = 'salary' then b.base_pay_value end as monthly_salary,
--     d.reg_hours, d.ot_hours, d.pto_hours, d.hol_hours,
     (select all_labor + parts from sales) total_sales,
--     case 
--       when b.base_pay_type = 'salary' then b.base_pay_value
--       when b.base_pay_type = 'hourly' then round(b.base_pay_value * d.reg_hours, 2)      
--     end as reg_pay,
--     case when b.base_pay_type = 'hourly' then round(1.5 * b.base_pay_value * d.ot_hours, 2) end as ot_pay,
--     case when b.base_pay_type = 'hourly' then round(b.base_pay_value * (d.pto_hours + d.hol_hours), 2) end as pto_hol_pay,
--     case
--       when b.base_pay_type = 'hourly' then
--         case
--           when d.reg_hours + d.ot_hours = 0 then 0
--           else 
--             coalesce(round((.5 * d.ot_hours *      
--               (b.commission_perc * (select all_labor + parts from sales)/(d.reg_hours + d.ot_hours + d.pto_hours + d.hol_hours ))), 2), 0)           
--         end
--     end as ot_variance,
    case 
      when b.the_role = 'parts' then round(b.commission_perc * (select all_labor + parts from sales), 2)
      when b.the_role = 'shop_foreman' then round(b.commission_perc * (select all_labor + parts from sales), 2)  
      when b.the_role in ('vda','csr') then round(b.commission_perc * (select all_labor + parts from sales), 2)    
    end as commission_pay
  from bspp.personnel a
  inner join bspp.personnel_roles b on a.user_name = b.user_name
-- *a*
    and b.thru_date >= _thru_date  
--     and b.from_date <= _from_date
--     and b.thru_date >= _thru_date
  left join (
    select user_name, sum(reg_hours) as reg_hours, sum(ot_hours) as ot_hours,
      sum(pto_hours) as pto_hours, sum(hol_hours) as hol_hours
    from bspp.clock_hours
    where the_date between _from_date and _thru_date
    group by user_name) d on a.user_name = d.user_name
      and b.base_pay_type = 'hourly';--) e;
--   left join bspp.corona_guarantee f on e.employee_number = f.employee_number;


end
$$;

select * from bs_payroll order by last_name;


/*
for accrual

select last_name, first_name, "emp#", reg_pay, ot_pay, ot_variance, commission_pay, 
  coalesce(ot_variance, 0) + coalesce(commission_pay, 0) as total
from bs_payroll order by last_name;

6/1/17, the accrual period for may is 5/14 -> 5/31, (the last pay period ended 5/27)
so, i am going to add 30% to the total (3 working days in addition to the full pay period)

select a.*, round(.3 * total, 2), round(.3 * total + total, 2) 
from (
select "last name", "first name", "emp #", "reg pay", "ot pay", "ot variance", "commission pay", 
  coalesce("ot variance", 0) + coalesce("commission pay", 0) as total
from bs_payroll order by "last name") a;

*/
      