﻿-- bs_compare
drop table if exists new_plan;
create temp table new_plan as
select a.control as ro, max(c.the_date) as the_date, 
  (select distinct year_month from dds.dim_date where the_date = max(c.the_date)) as year_month,
  coalesce(sum(multiplier * -amount) filter (where category = 'gm parts'), 0) as gm_parts,
  coalesce(sum(-amount) filter (where category = 'non gm parts'), 0)  as non_gm_parts,
  coalesce(sum(multiplier * -amount) filter (where category like '%parts'), 0)  as total_parts,
  coalesce(sum(-amount) filter (where category = 'labor'), 0)  as labor,
  coalesce(sum(multiplier * -amount), 0)  as total_sales
from fin.fact_gl a
inner join bspp.sale_accounts b on a.account_key = b.account_key
inner join dds.dim_date c on a.date_key = c.date_key
  and c.the_year > 2016
inner join fin.dim_doc_type d on a.doc_type_key = d.doc_type_key
where post_status = 'Y'
--   and c.the_date between _from_date and _thru_date
  and d.doc_type = 'service tickets' 
group by a.control


select *
from new_plan



drop table if exists old_plan;
create temp table old_plan as
select a.ro, max(a.the_date) as the_date, 
  (select distinct year_month from dds.dim_date where the_date = max(a.the_date)) as year_month,
  sum(a.gm_parts) as gm_parts, sum(a.non_gm_parts) as non_gm_parts,
  sum(a.total_parts) as total_parts, sum(a.labor) as labor, sum(a.total_sales) as total_sales
from bspp.z_unused_writersales a
group by ro;
create unique index on old_plan(ro);

select * 
from old_plan


select *
from bspp.z_unused_writersales
limit 100


-- 3/30/18 
/*
jeri
Will you please run a comparison for last 12 months with this plan and original pay plan?

no sure what that looks like
*/
Will you please run a comparison for last 12 months with this plan and original pay plan?


select *
from (
  select * 
  from bspp.z_unused_writersales
  where the_date between '01/21/2018' and '02/03/2018') x
  left join (
  select control,
    sum(-amount) filter (where category = 'gm parts') as gm_parts,
    sum(-amount) filter (where category = 'non gm parts') as nopn_gm_parts,
    sum(-amount) filter (where category like '%parts') as total_parts,
    sum(-amount) filter (where category = 'labor') as labor,
    sum(-amount) filter (where category = 'internal labor') as int_labor,
    sum(-amount) as total_sales
  from fin.fact_gl a
  inner join bspp.sale_accounts b on a.account_key = b.account_key
  inner join dds.dim_date c on a.date_key = c.date_key
  inner join fin.dim_doc_type d on a.doc_type_key = d.doc_type_key
  where post_status = 'Y'
    and c.the_date between '01/01/2018' and '03/03/2018'
    and d.doc_type = 'service tickets' 
  group by a.control) y on x.ro = y.control




  -- total sales by writer for a pay period
-- want it for multiple pay periods, i think
select a.writer_id, a.last_name, writer + alt_writer
from  (
select a.writer_id, b.last_name, sum(total_sales) as writer
from bspp.z_unused_writersales a
inner join bspp.personnel b on a.writer_id = b.writer_id
where the_date between '01/21/2018' and '02/03/2018'
group by a.writer_id, b.last_name) a
inner join (
select alt_writer_id, sum(total_sales) as alt_writer
from bspp.z_unused_writersales
where the_date between '01/21/2018' and '02/03/2018'
group by alt_writer_id) b on a.writer_id = b.alt_writer_id




-- ok, here is the date range
drop table if exists date_range;
create temp table date_range as
select biweekly_pay_period_start_date as from_date, biweekly_pay_period_end_date as thru_date
from dds.dim_date
where the_date between '02/19/2017' and '02/17/2018'
group by biweekly_pay_period_start_date, biweekly_pay_period_end_date
order by biweekly_pay_period_start_date;

drop table if exists writers;
create temp table writers as
select last_name, first_name, writer_id, 1.25 as comm_rate, 'csr' as the_role, .0047 as new_rate from bspp.personnel where last_name = 'ostlund'
union
select last_name, first_name, writer_id, 2, 'vda' as the_role, .0058 as new_rate from bspp.personnel where last_name = 'evavold'
union
select last_name, first_name, writer_id, 1.5, 'csr' as the_role, .0047 as new_rate from bspp.personnel where last_name = 'lueker'
union
select last_name, first_name, writer_id, 2.35, 'vda' as the_role, .0058 as new_rate from bspp.personnel where last_name = 'steinke'
union
select last_name, first_name, writer_id, 1.25, 'csr' as the_role, .0047 as new_rate from bspp.personnel where last_name = 'marek'
union
select last_name, first_name, writer_id, 2.35, 'vda' as the_role, .0058 as new_rate from bspp.personnel where last_name = 'hill';

-- matches payroll spreadsheet perfectly
-- the old plan
drop table if exists old_plan;
create temp table old_plan as
select d.from_Date, d.thru_date, d.writer_id, e.last_name, e.first_name, e.comm_rate, e.the_role, round(sales  * (comm_rate/100), 2) as comm_pay
from (
  select from_date, thru_date, writer_id, sum(sales) as sales
  from (
    select b.from_date, b.thru_date, a.writer_id, sum(total_sales) as sales
    from bspp.z_unused_writersales a
    inner join date_range b on a.the_date between b.from_date and b.thru_date
    group by b.from_date, b.thru_date, a.writer_id
    union all
    select b.from_date, b.thru_date, a.alt_writer_id, sum(total_sales) as sales
    from bspp.z_unused_writersales a
    inner join date_range b on a.the_date between b.from_date and b.thru_date
    where a.alt_writer_id <> 'none'
    group by b.from_date, b.thru_date, a.alt_writer_id) c
  group by from_date, thru_date, writer_id) d
inner join writers e on d.writer_id = e.writer_id  
order by from_date, d.writer_id;



drop table if exists new_plan_sales;
create temp table new_plan_sales as
select c.from_date, c.thru_date, sum(multiplier * -amount):: integer as total_sales
from fin.fact_gl a
inner join bspp.sale_accounts b on a.account_key = b.account_key
inner join dds.dim_date aa on a.date_key = aa.date_key
inner join date_range c on aa.the_date between c.from_date and c.thru_date
inner join fin.dim_doc_type d on a.doc_type_key = d.doc_type_key
where post_status = 'Y'
  and d.doc_type = 'service tickets' 
group by c.from_date, c.thru_date;

select from_date, thru_date, last_name, first_name, writer_id, (new_rate * total_sales)::integer
from new_plan_sales a
cross join writers b 
order by from_date, last_name

select * from old_plan limit 10
-- by writer/pay period

date_range: '02/19/2017' -> '02/17/2018'
------------------------------------------------------------------------------------
-- this generates the spreadsheet bs_compare sent to jeri on 3/30/18
------------------------------------------------------------------------------------
drop table if exists comparison;
create temp table comparison as
select a.from_date, a.thru_date, a.last_name, a.first_name, a.the_role, 
  a.comm_pay::integer as old_plan_comm_pay, c.new_plan_comm_pay
from old_plan a
inner join (
  select from_date, thru_date, last_name, first_name, writer_id, (new_rate * total_sales)::integer as new_plan_comm_pay
  from new_plan_sales a
  cross join writers) c on a.writer_id = c.writer_id and a.from_Date = c.from_date;

select * from comparison order by from_date, last_name

-- total for date range
select last_name, first_name, sum(old_plan_comm_pay) as old_plan, sum(new_plan_comm_pay) as new_plan
from comparison
group by last_name, first_name

-- average over date range
select last_name, first_name, avg(old_plan_comm_pay)::integer as old_plan, avg(new_plan_comm_pay)::integer as new_plan, count(*)
from comparison
group by last_name, first_name


---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
jeri wants to see it the we did last week with "old plan" and "new plan" sales totals by month

-- ok, the above shows that there are multiple rows for ros, sometimes different pay periods sometimes not
-- think where i want to be is 1 row per rop
-- for this look, i don't care about who the writer is, what i care about is the total sales for each ro
-- for which the writer is paid
drop table if exists old_plan;
create temp table old_plan as
select a.ro, max(a.the_date) as the_date, 
  (select distinct year_month from dds.dim_date where the_date = max(a.the_date)) as year_month,
  sum(a.gm_parts) as gm_parts, sum(a.non_gm_parts) as non_gm_parts,
  sum(a.total_parts) as total_parts, sum(a.labor) as labor, sum(a.total_sales) as total_sales
from bspp.z_unused_writersales a
group by ro;
create unique index on old_plan(ro);

select distinct category from bspp.sale_accounts

-- ok, want to compare that to total sales for which the writer would be paid in the current pay plan
-- same query used to generat bspp.bs_sales_detail
drop table if exists new_plan;
create temp table new_plan as
select a.control as ro, max(c.the_date) as the_date, 
  (select distinct year_month from dds.dim_date where the_date = max(c.the_date)) as year_month,
  coalesce(sum(multiplier * -amount) filter (where category = 'gm parts'), 0) as gm_parts,
  coalesce(sum(-amount) filter (where category = 'non gm parts'), 0)  as non_gm_parts,
  coalesce(sum(multiplier * -amount) filter (where category like '%parts'), 0)  as total_parts,
  coalesce(sum(-amount) filter (where category like '%labor'), 0)  as labor,
  coalesce(sum(multiplier * -amount), 0)  as total_sales
from fin.fact_gl a
inner join bspp.sale_accounts b on a.account_key = b.account_key
inner join dds.dim_date c on a.date_key = c.date_key
  and c.the_year > 2016
inner join fin.dim_doc_type d on a.doc_type_key = d.doc_type_key
where post_status = 'Y'
--   and c.the_date between _from_date and _thru_date
  and d.doc_type = 'service tickets' 
group by a.control

select * from old_plan limit 10

-- think this is what will interest jeri
-- ~ 40k a month more on old pay plan based on any ro with bs writers on it
--------------------------------------------------------------------------------
-- this is the query that generates the bs_3 spreadsheet sent to jeri 3/30/18
--------------------------------------------------------------------------------
select aa.*, bb.*, old_total_sales - new_total_sales as diff
from (
  select a.year_month, 
    sum(a.total_parts)::integer as old_parts, sum(a.labor)::integer as old_labor, 
    sum (a.total_parts + a.labor)::integer as old_total_sales  
  from old_plan a
  where a.year_month between 201703 and 201802
  group by a.year_month) aa  
left join (
  select b.year_month, 
    sum(b.total_parts)::integer as new_parts, sum(b.labor)::integer as new_labor, 
    sum (b.total_parts + b.labor)::integer as new_total_sales  
  from new_plan b
  where b.year_month between 201703 and 201802
    and exists (
      select 1
      from old_plan
      where ro = b.ro
        and year_month = b.year_month)
  group by b.year_month) bb on aa.year_month = bb.year_month
order by aa.year_month

-----------------------------------------------------------------------------
-- 4/13 troubleshooting jeri's issue
-----------------------------------------------------------------------------

  select b.year_month, 
    sum(b.total_parts)::integer as new_parts, sum(b.labor)::integer as new_labor, 
    sum (b.total_parts + b.labor)::integer as new_total_sales  
--  select sum(b.total_parts + b.labor)::integer , .0315 * sum(b.total_parts + b.labor)::integer        
  from new_plan b
--   where b.year_month between 201703 and 201802
  where b.the_date between '02/19/2017' and '02/17/2018'
--     and exists (
--       select 1
--       from old_plan
--       where ro = b.ro
--         and year_month = b.year_month)
  group by b.year_month

if i exclude the exists ro in old_plan, total_new_sales is 6435729, as opposed to 5528861

 select *
  from new_plan b
  where b.year_month between 201703 and 201802
    and not exists (
      select 1
      from old_plan
      where ro = b.ro
        and year_month = b.year_month) 
------------------------------------------------------------------------------------------------------
-- 4/12/18
------------------------------------------------------------------------------------------------------
the problem jeri is having is the total of new sales on bs_3 spreadsheet disagrees with the bs_compare spreadsheet

she took new_total_sales (5,531,163) * .0315 ((.0047 * 3) + (.0058 * 3)) to get 174231

then she took the total new_plan from bs_compare 188950 and wants to know why the 14k difference

date range for bs_compare '02/19/2017' -> '02/17/2018'
vs bs_3 where the date range is 03/01/2017 thru 02/28/2018
so, change the date range for the bs_3 query:  and we end up with 5512269
  select 
    sum(b.total_parts)::integer as new_parts, sum(b.labor)::integer as new_labor, 
    sum (b.total_parts + b.labor)::integer as new_total_sales,  
    .0315 * sum (b.total_parts + b.labor)
  from new_plan b
  where b.the_date between '02/19/2017' and '02/17/2018'
    and exists (
      select 1
      from old_plan
      where ro = b.ro
        and year_month = b.year_month)


bs_compare
select '02/17/2018'::date - '02/19/2017'::date -- 363

bs_3
select '02/28/2018'::date - '03/01/2017'::date -- 364

SHIT I THINK I AM MIND FUCKING

so, lets bring it all down here with just what i need, which is sales

drop table if exists compare_sales;
create temp table compare_sales as
select c.from_date, c.thru_date, sum(multiplier * -amount):: integer as total_sales
from fin.fact_gl a
inner join bspp.sale_accounts b on a.account_key = b.account_key
inner join dds.dim_date aa on a.date_key = aa.date_key
inner join (
  select biweekly_pay_period_start_date as from_date, biweekly_pay_period_end_date as thru_date
  from dds.dim_date
  where the_date between '02/19/2017' and '02/17/2018'
  group by biweekly_pay_period_start_date, biweekly_pay_period_end_date) c on aa.the_date between c.from_date and c.thru_date
inner join fin.dim_doc_type d on a.doc_type_key = d.doc_type_key
where post_status = 'Y'
  and d.doc_type = 'service tickets' 
group by c.from_date, c.thru_date;

-- 6434531 * .0315 = 202687
select sum(total_sales), sum(total_sales) * .0315
from compare_sales

------------------
-- if i adjust the dates to be the same as compare_sales, the queries & results are identical
drop table if exists bs3;
create temp table bs3 as
select a.control as ro, max(c.the_date) as the_date, 
  (select distinct year_month from dds.dim_date where the_date = max(c.the_date)) as year_month,
  coalesce(sum(multiplier * -amount) filter (where category = 'gm parts'), 0) as gm_parts,
  coalesce(sum(-amount) filter (where category = 'non gm parts'), 0)  as non_gm_parts,
  coalesce(sum(multiplier * -amount) filter (where category like '%parts'), 0)  as total_parts,
  coalesce(sum(-amount) filter (where category like '%labor'), 0)  as labor,
  coalesce(sum(multiplier * -amount), 0)  as total_sales
from fin.fact_gl a
inner join bspp.sale_accounts b on a.account_key = b.account_key
inner join dds.dim_date c on a.date_key = c.date_key
inner join (
  select biweekly_pay_period_start_date as from_date, biweekly_pay_period_end_date as thru_date
  from dds.dim_date
  where the_date between '02/19/2017' and '02/17/2018'
  group by biweekly_pay_period_start_date, biweekly_pay_period_end_date) cc on c.the_date between cc.from_date and cc.thru_date
inner join fin.dim_doc_type d on a.doc_type_key = d.doc_type_key
where post_status = 'Y'
--   and c.the_date between _from_date and _thru_date
  and d.doc_type = 'service tickets' 
group by a.control

select sum(total_sales), .0315 * sum(total_sales) from bs3 