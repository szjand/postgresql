﻿do 
$$
declare _from_date date := '04/01/2018';
declare _thru_date date := '04/14/2018';

begin

drop table if exists bs_payroll;
create temp table bs_payroll as

with 
  sales as (
    select round(sum(amount) filter (where category like '%labor'), 2) as all_labor,
      round(sum(amount) filter (where category like '%labor' and account <> '147300'), 2) as labor_no_internal,
      round(sum(multiplier * amount) filter (where category like '%parts'), 2) as parts
    from bspp.bs_sales_detail
    where the_date between _from_date and _thru_date)  
select e.*, coalesce(reg_pay, 0) + coalesce(ot_pay, 0) + coalesce(pto_hol_pay, 0) 
  + coalesce(ot_variance, 0) + coalesce(commission_pay, 0) as total_pay
from (    
  select a.last_name, a.first_name, a.employee_number, 
    round(100 * c.commission_perc, 2) as "commission %", a.pto_rate,
    case when c.base_pay_type = 'hourly' then b.base_pay_value end as hourly_rate,
    case when c.base_pay_type = 'hourly' then 1.5 * b.base_pay_value end as ot_rate,
    case when c.base_pay_type = 'salary' then b.base_pay_value end as monthly_salary,
    d.reg_hours, d.ot_hours, d.pto_hours, d.hol_hours,
    case
      when b.the_role in ('vda','csr') then (select labor_no_internal + parts from sales) 
      else (select all_labor + parts from sales)
    end as total_sales,
    case 
      when c.base_pay_type = 'salary' then b.base_pay_value
      when c.base_pay_type = 'hourly' then round(b.base_pay_value * d.reg_hours, 2)
    end as reg_pay,
    case when c.base_pay_type = 'hourly' then round(1.5 * b.base_pay_value * d.ot_hours, 2) end as ot_pay,
    case when c.base_pay_type = 'hourly' then round(b.base_pay_value * (d.pto_hours + d.hol_hours), 2) end as pto_hol_pay,
    case
      when c.base_pay_type = 'hourly' then
        case
          when d.reg_hours + d.ot_hours = 0 then 0
          else 
            case 
              when b.the_role in ('vda','csr') then
                coalesce(round((.5 * d.ot_hours *
                  (c.commission_perc * (select labor_no_internal + parts from sales)/(d.reg_hours + d.ot_hours))), 2), 0)
              when b.the_role = 'parts' then
                coalesce(round((.5 * d.ot_hours *
                  (c.commission_perc * (select all_labor + parts from sales)/(d.reg_hours + d.ot_hours))), 2), 0)            
            end
        end
    end as ot_variance,
    case 
      when b.the_role in ('vda','csr') then round(c.commission_perc * (select labor_no_internal + parts from sales), 2)
      when b.the_role = 'parts' then round(c.commission_perc * (select all_labor + parts from sales), 2)
      when b.the_role = 'shop_foreman' then round(c.commission_perc * (select all_labor + parts from sales), 2)
    end as commission_pay
  -- select *
  from bspp.personnel a
  inner join bspp.personnel_role b on a.user_name = b.user_name
  inner join bspp.roles c on b.the_role = c.the_role
  left join (
    select user_name, sum(reg_hours) as reg_hours, sum(ot_hours) as ot_hours,
      sum(pto_hours) as pto_hours, sum(hol_hours) as hol_hours
    from bspp.clock_hours
    where the_date between _from_date and _thru_date
    group by user_name) d on a.user_name = d.user_name
      and c.base_pay_type = 'hourly') e;


end
$$;

select * from bs_payroll order by last_name;



/*
for accrual

select last_name, first_name, employee_number, reg_pay, ot_pay, ot_variance, commission_pay, 
  coalesce(ot_variance, 0) + coalesce(commission_pay, 0) as total
from bs_payroll order by last_name;

6/1/17, the accrual period for may is 5/14 -> 5/31, (the last pay period ended 5/27)
so, i am going to add 30% to the total (3 working days in addition to the full pay period)

select a.*, round(.3 * total, 2), round(.3 * total + total, 2) 
from (
select "last name", "first name", "emp #", "reg pay", "ot pay", "ot variance", "commission pay", 
  coalesce("ot variance", 0) + coalesce("commission pay", 0) as total
from bs_payroll order by "last name") a;

*/

