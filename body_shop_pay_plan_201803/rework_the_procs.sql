﻿-- Function: bspp.json_get_pay_summary(citext, integer)

-- DROP FUNCTION bspp.json_get_pay_summary(citext, integer);

CREATE OR REPLACE FUNCTION bspp.json_get_pay_summary(
    _employee_number citext,
    _pay_period_indicator integer)
  RETURNS SETOF json AS
$BODY$  
/*


select * from bspp.json_get_pay_summary('141069', -1)

*/

declare

  _user_name citext := (
    select user_name
    from bspp.personnel
    where employee_number = _employee_number);
  _user_key uuid := (
    select user_key
    from nrv.users
    where employee_number = _employee_number);
  _current_pay_period_seq integer := (
    select distinct biweekly_pay_period_sequence
    from dds.dim_date
    where the_date = current_date);
  _from_date date := (
    select distinct biweekly_pay_period_start_date
    from dds.dim_date
    where biweekly_pay_period_sequence = _current_pay_period_seq + _pay_period_indicator);
  _thru_date date := (
    select distinct biweekly_pay_period_end_date
    from dds.dim_date
    where biweekly_pay_period_sequence = _current_pay_period_seq + _pay_period_indicator);   
  _user_role citext := (
    select the_role
    from bspp.personnel_role
    where user_name = _user_name);   
   
begin  

case 
  when _user_role in ('csr','vda') then

    return query
    select row_to_json (zzz)
    from (
    select row_to_json(xxx) as body_shop_pay
    from (
      with 
        ros_exc_internal as (
          select ro, parts, labor, 
            parts + labor as ro_sales 
          from (  
            select ro,
              coalesce(sum(amount * multiplier) filter (where category like '%parts'), 0) as parts,
              coalesce(sum(amount) filter (where category like '%labor'), 0) as labor
            from bspp.bs_sales_detail
            where the_date between _from_date and _thru_date
              and account <> '147300'
            group by ro) a),   
        ros_exc_internal_sales as (
          select sum(parts) as parts_total,
            sum(labor) as labor_total,
            sum(parts + labor) as parts_and_labor_sales
          from ros_exc_internal),
        comm_perc as (  
          select (commission_perc) as perc
          from bspp.roles 
          where the_role = _user_role),
        comm_pay as (
          select round((
            (select perc from comm_perc)
            *
            (select parts_and_labor_sales from ros_exc_internal_sales)), 2) as commission_pay)
      select _user_key as user, z.*, 
          ( -- ros: an array of ro objects: multiple rows
            select coalesce(array_to_json(array_agg(row_to_json(g))), '[]')
            from ( -- g one row per ro
              select * 
              from ros_exc_internal) g) as ros
      from ( -- z all the individual's pay data + total pay:  single row
        select x.*, regular_pay + overtime_pay + pto_pay + commission_pay + ot_variance as total_pay 
        from ( -- x all the individual's pay data elements:  single row
            select b.base_pay_value as hourly_rate, round(1.5 * b.base_pay_value, 2) as overtime_rate, 
              a.pto_rate,
              (select round(100 * perc, 2) from comm_perc) as commission_percentage, 
              coalesce(c.regular_clock_hours, 0) as regular_clock_hours,
              coalesce(c.overtime_hours, 0) as overtime_hours,
              coalesce(c.pto_hours, 0) as pto_hours,
              coalesce(round(c.regular_clock_hours * b.base_pay_value, 2), 0) as regular_pay,
              coalesce(round(c.overtime_hours * 1.5 * b.base_pay_value, 2), 0) as overtime_pay,
              coalesce(round(a.pto_rate * c.pto_hours, 2), 0) as pto_pay,
              (select round(parts_and_labor_sales, 2) from ros_exc_internal_sales) as parts_and_labor_sales,
              (select round(parts_total, 2) from ros_exc_internal_sales) as parts_total,
              (select round(labor_total, 2) from ros_exc_internal_sales) as labor_total,
              (select round(commission_pay, 2) from comm_pay) as commission_pay,
               case
                 when c.regular_clock_hours + c.overtime_hours = 0 then 0
                 else coalesce(round((.5 * c.overtime_hours * 
                   (select commission_pay from comm_pay))/(c.regular_clock_hours + c.overtime_hours), 2), 0)
                end as ot_variance
            from bspp.personnel a
            inner join bspp.personnel_role b on a.user_name = b.user_name
            LEFT join ( -- users clock hours
              select user_name, sum(reg_hours) as regular_clock_hours, sum(ot_hours) as overtime_hours,
                sum(pto_hours + hol_hours) as pto_hours
              from bspp.clock_hours
              where the_date between _from_date and _thru_date
              group by user_name) c on a.user_name = c.user_name
            where a.user_name = _user_name) x) z) xxx ) zzz;

  when _user_role = 'parts' then

    return query
    select row_to_json (zzz)
    from (
    select row_to_json(xxx) as body_shop_pay
    from (
      with 
        all_ros as (
          select ro, parts, labor, 
            parts + labor as ro_sales 
          from (  
            select ro,
              coalesce(sum(amount * multiplier) filter (where category like '%parts'), 0) as parts,
              coalesce(sum(amount) filter (where category like '%labor'), 0) as labor
            from bspp.bs_sales_detail
            where the_date between _from_date and _thru_date
            group by ro) a),
        all_ros_sales as (
          select sum(parts) as parts_total,
            sum(labor) as labor_total,
            sum(parts + labor) as parts_and_labor_sales
          from all_ros),
        comm_perc as (  
          select (commission_perc) as perc
          from bspp.roles 
          where the_role = _user_role),
        comm_pay as (
          select round((
            (select perc from comm_perc)
            *
            (select parts_and_labor_sales from all_ros_sales)), 2) as commission_pay)
      select _user_key as user, z.*, 
          ( -- ros: an array of ro objects: multiple rows
            select coalesce(array_to_json(array_agg(row_to_json(g))), '[]')
            from ( -- g one row per ro
              select * 
              from all_ros) g) as ros
      from ( -- z all the individual's pay data + total pay:  single row
        select x.*, regular_pay + overtime_pay + pto_pay + commission_pay + ot_variance as total_pay 
        from ( -- x all the individual's pay data elements:  single row
            select b.base_pay_value as hourly_rate, round(1.5 * b.base_pay_value, 2) as overtime_rate, 
              a.pto_rate,
              (select round(100 * perc, 2) from comm_perc) as commission_percentage, 
              coalesce(c.regular_clock_hours, 0) as regular_clock_hours,
              coalesce(c.overtime_hours, 0) as overtime_hours,
              coalesce(c.pto_hours, 0) as pto_hours,
              coalesce(round(c.regular_clock_hours * b.base_pay_value, 2), 0) as regular_pay,
              coalesce(round(c.overtime_hours * 1.5 * b.base_pay_value, 2), 0) as overtime_pay,
              coalesce(round(a.pto_rate * c.pto_hours, 2), 0) as pto_pay,
              (select round(parts_and_labor_sales, 2) from all_ros_sales) as parts_and_labor_sales,
              (select round(parts_total, 2) from all_ros_sales) as parts_total,
              (select round(labor_total, 2) from all_ros_sales) as labor_total,
              (select round(commission_pay, 2) from comm_pay) as commission_pay,
               case
                 when c.regular_clock_hours + c.overtime_hours = 0 then 0
                 else coalesce(round((.5 * c.overtime_hours * 
                   (select commission_pay from comm_pay))/(c.regular_clock_hours + c.overtime_hours), 2), 0)
                end as ot_variance
            from bspp.personnel a
            inner join bspp.personnel_role b on a.user_name = b.user_name
            LEFT join ( -- users clock hours
              select user_name, sum(reg_hours) as regular_clock_hours, sum(ot_hours) as overtime_hours,
                sum(pto_hours + hol_hours) as pto_hours
              from bspp.clock_hours
              where the_date between _from_date and _thru_date
              group by user_name) c on a.user_name = c.user_name
            where a.user_name = _user_name) x) z) xxx) zzz;

  when _user_role = 'shop_foreman' then

    return query
    select row_to_json (zzz) 
    from (
    select row_to_json(xxx) as body_shop_pay
    from ( 
      with 
        all_ros as (
          select ro, parts, labor, 
            parts + labor as ro_sales 
          from (  
            select ro,
              coalesce(sum(amount * multiplier) filter (where category like '%parts'), 0) as parts,
              coalesce(sum(amount) filter (where category like '%labor'), 0) as labor
            from bspp.bs_sales_detail
            where the_date between _from_date and _thru_date
            group by ro) a),
        all_ros_sales as (
          select sum(parts) as parts_total,
            sum(labor) as labor_total,
            sum(parts + labor) as parts_and_labor_sales
          from all_ros),
        comm_perc as (  
          select (commission_perc) as perc
          from bspp.roles 
          where the_role = _user_role),
        comm_pay as (
          select round((
            (select perc from comm_perc)
            *
            (select parts_and_labor_sales from all_ros_sales)), 2) as commission_pay)
      select _user_key as user, z.*, 
          ( -- ros: an array of ro objects: multiple rows
            select coalesce(array_to_json(array_agg(row_to_json(g))), '[]')
            from ( -- g one row per ro
              select * 
              from all_ros) g) as ros
      from ( -- z all the individual's pay data + total pay:  single row
        select x.*, salary + commission_pay as total_pay
        from ( -- x all the individual's pay data elements:  single row
            select b.base_pay_value as Salary, -- round(1.5 * b.base_pay_value, 2) as overtime_rate, 
              (select round(100 * perc, 2) from comm_perc) as commission_percentage, 
              (select round(parts_and_labor_sales, 2) from all_ros_sales) as parts_and_labor_sales,
              (select round(parts_total, 2) from all_ros_sales) as parts_total,
              (select round(labor_total, 2) from all_ros_sales) as labor_total,
              (select round(commission_pay, 2) from comm_pay) as commission_pay
            from bspp.personnel a
            inner join bspp.personnel_role b on a.user_name = b.user_name
            where a.user_name = _user_name) x) z) xxx ) zzz;
end case;


   
ENd
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION bspp.json_get_pay_summary(citext, integer)
  OWNER TO postgres;
