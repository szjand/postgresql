﻿


---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
-- this is all the fix for personnel needing to be updated with fi_id's
-- update deals_by_month fi
select stock_number, unit_count, psc_last_name, fi_last_name
from sls.deals_by_month
where (psc_last_name in ('barker','seay','weber') or fi_last_name in ('barker','seay','weber'))
  and year_month = '201802'
order by fi_last_name

select b.*
from sls.deals_by_month a
left join sls.deals b on a.vin = b.vin
where b.year_month = 201802
order by b.fi_manager

select a.stock_number, a.psc_last_name, a.ssc_last_name, a.fi_last_name, b.fi_manager --b.primary_sc, b.secondary_sc, b.fi_manager
from sls.deals_by_month a
left join sls.deals b on a.bopmast_id = b.bopmast_id
-- where b.fi_manager = 'BFO'
where b.year_month = 201802
order by a.fi_last_name

-- there are  deals in deals_by_month where the fi is none because the personnel table had not been updated
-- with fi_id's, it's part of the a-z deal
-- ok, i think this shows the ones that need to be fixed
select a.fi_last_name, b.fi_manager, count(*)
from sls.deals_by_month a
left join sls.deals b on a.bopmast_id = b.bopmast_id
where b.year_month > 201712
group by a.fi_last_name, b.fi_manager
order by a.fi_last_name

select a.stock_number, b.bopmast_id, b.seq, a.psc_first_name, a.psc_last_name, a.psc_employee_number, a.fi_last_name, b.fi_manager 
from sls.deals_by_month a
left join sls.deals b on a.bopmast_id = b.bopmast_id
where b.year_month > 201712
  and a.fi_last_name = 'none' 
  and b.fi_manager in ('JWE','FRD','JOB')
order by b.fi_manager, a.psc_last_name  

select * 
from sls.deals_by_month
where stock_number in (
  select a.stock_number
  from sls.deals_by_month a
  left join sls.deals b on a.bopmast_id = b.bopmast_id
  where b.year_month > 201712
    and a.fi_last_name = 'none' 
    and b.fi_manager in ('JWE','FRD','JOB'))

update sls.deals_by_month
set fi_first_name = 'Frank',
    fi_last_name = 'Decouteau',
    fi_employee_number = '15552'
where bopmast_id = 46588
  and seq = 1;  

update sls.deals_by_month
set fi_first_name = 'Joshua',
    fi_last_name = 'Barker',
    fi_employee_number = '110052'
where bopmast_id in (46520,46492,46619,46607,46423,46622,46568)
  and seq = 1;

update sls.deals_by_month
set fi_first_name = 'James',
    fi_last_name = 'Weber',
    fi_employee_number = '1147810'
where bopmast_id in (46652,46522,46487,46334,46429,46569,46450,46498,46402,46422,46507,46589,46462,46408)
  and seq = 1;

    
select * from arkona.ext_bopslsp where active is null and sales_person_type = 'F' order by company_number, sales_person_name

-- as of now 3/19/18, all the a-z guys have the same fi_id for both stores
select sales_person_name,
  max(case when company_number = 'RY1' then sales_person_id end) as ry1_id,
  max(case when company_number = 'RY2' then sales_person_id end) as ry2_id
from arkona.ext_bopslsp 
where active is null 
  and sales_person_type = 'F' 
  and company_number in ('RY1','RY2')
group by sales_person_name
order by sales_person_name



---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
