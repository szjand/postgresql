﻿/*
shit that can be changed when new system in place
teams table
personnel.commision_perc
         .pto_rate

*/
delete from bspp.clock_hours where user_name = 'myem@rydellcars.com';

delete from bspp.personnel_role where user_name = 'myem@rydellcars.com';

delete from bspp.teams where csr_user_name = 'myem@rydellcars.com';

delete from bspp.personnel where user_name = 'myem@rydellcars.com';

delete from bspp.personnel


select *
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
where b.account in ('147102','147400')

delete from bspp.sale_accounts where account in ('147102','147400')

update bspp.sale_accounts
set category = 'internal labor' 
where account = '147300';
update bspp.sale_accounts
set category = 'gm parts' 
where account  = '147700';
update bspp.sale_accounts
set category = 'non gm parts' 
where account in ('147701','147900','147901');

commission percentage is now role based
csr: 1.6%
vda: 2.0%
parts: 0.55%
foreman: 0.55%

alter table bspp.roles
add column commission_perc numeric(6,4);
update bspp.roles
set commission_perc = .016
where the_role = 'csr';
update bspp.roles
set commission_perc = .02
where the_role = 'vda';
update bspp.roles
set commission_perc = .0055
where the_role = 'parts';
update bspp.roles
set commission_perc = .0055
where the_role = 'shop_foreman';

-- hourly rate & pto rate: 10 for all except brian: 12, loren salary & pto exempt
update bspp.personnel set pto_rate = 10 where user_name in ('aostlund@rydellcars.com','devavold@rydellcars.com',
  'dlueker@rydellcars.com','dmarek@rydellcars.com','kblumhagen@rydellcars.com','msteinke@rydellcars.com');
update bspp.personnel set pto_rate = 12 where user_name in ('bhill@rydellcars.com');
update bspp.personnel set pto_rate = 0 where user_name in ('lshereck@rydellcars.com');

update bspp.personnel_role set base_pay_value = 10 where user_name in ('aostlund@rydellcars.com','devavold@rydellcars.com',
  'dlueker@rydellcars.com','dmarek@rydellcars.com','kblumhagen@rydellcars.com','msteinke@rydellcars.com');
update bspp.personnel_role set base_pay_value = 12 where user_name in ('bhill@rydellcars.com');

-- since beginning of 2017 of the 22834 entries 7 were not doc_type service tickets so, 
-- include doc_type in query against fact_gl;
select c.the_date, b.account, a.control, a.amount, d.doc_type
-- select count(*)
from fin.fact_gl a
inner join bspp.sale_accounts b on a.account_key = b.account_key
inner join dds.dim_date c on a.date_key = c.date_key
inner join fin.dim_doc_type d on a.doc_type_key = d.doc_type_key
where post_status = 'Y'
  and c.year_month > 201612
  and d.doc_type <> 'service tickets'

drop table if exists bspp.bs_sales_detail cascade;
create table bspp.bs_sales_detail (
  the_date date not null,
  ro citext not null,
  account citext not null,
  category citext not null,
  multiplier numeric(2,1) not null,
  amount numeric(8,2) NOT NULL DEFAULT (0)::numeric,
  constraint bs_sales_detail_pkey primary key (the_date,ro, account));
create index on bspp.bs_sales_detail(category);

-- detail
-- drop table if exists bs_details;
-- create temp table bs_details as
insert into bspp.bs_sales_detail
select c.the_date, a.control as ro, b.account, b.category, b.multiplier, sum(-a.amount) as amount
from fin.fact_gl a
inner join bspp.sale_accounts b on a.account_key = b.account_key
inner join dds.dim_date c on a.date_key = c.date_key
inner join fin.dim_doc_type d on a.doc_type_key = d.doc_type_key
where post_status = 'Y'
  and c.year_month > 201712
  and d.doc_type = 'service tickets' 
group by c.the_date, a.control, b.account, b.category, b.multiplier;  

select * from bspp.bs_sales_detail limit 100

select the_date, ro, account from bs_details group by the_date, ro, account having count(*) > 1

select * from bs_details where ro = '18057386'

-- 3/8/18 nightly update


CREATE OR REPLACE FUNCTION bspp.update_bs_sales_detail()
  RETURNS void AS
$BODY$ 
/*
select * from bspp.update_bs_sales_detail()
*/
declare
  _from_date date := (
    select biweekly_pay_period_start_date
    from dds.dim_date
    where the_date = current_Date - 1);
  _thru_date date := (
    select biweekly_pay_period_end_date
    from dds.dim_date
    where the_date = current_Date - 1); 
begin

delete
from bspp.bs_sales_detail
where the_date between _from_date and _thru_date;

insert into bspp.bs_sales_detail
select c.the_date, a.control as ro, b.account, b.category, b.multiplier, sum(-a.amount) as amount
from fin.fact_gl a
inner join bspp.sale_accounts b on a.account_key = b.account_key
inner join dds.dim_date c on a.date_key = c.date_key
inner join fin.dim_doc_type d on a.doc_type_key = d.doc_type_key
where post_status = 'Y'
  and c.the_date between _from_date and _thru_date
  and d.doc_type = 'service tickets' 
group by c.the_date, a.control, b.account, b.category, b.multiplier;  
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


select count(*) from bspp.bs_sales_detail  -- 3021 


-- 3//8/18 deprecated db objects

alter table bspp.teams
rename to z_unused_teams;

alter table bspp.personnel
drop column commission_perc;

alter function bspp.get_estimator_pay_summary(citext,integer)
rename to z_unused_get_estimator_pay_summary;

alter function bspp.get_parts_pay_summary(citext,integer)
rename to z_unused_get_parts_pay_summary;

alter function bspp.get_shop_foreman_pay_summary(citext,integer)
rename to z_unused_get_shop_foreman_pay_summary;

alter function bspp.update_pay_period_transactions()
rename to z_unused_update_pay_period_transactions;

alter function bspp.update_writer_sales()
rename to z_unused_update_writer_sales;

alter table bspp.bs_ros
rename to z_unused_bs_ros;

alter table bspp.pay_period_transactions
rename to z_unused_pay_period_transactions;

alter table bspp.ro_writers
rename to z_unused_ro_writers;

alter table bspp.sales
rename to z_unused_sales;

alter table bspp.writer_sales
rename to z_unused_writersales;

select distinct category from bspp.sale_accounts