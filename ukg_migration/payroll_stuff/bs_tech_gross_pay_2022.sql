﻿/*
Jon, 

Have kind of a big favor. 

I am working on getting the budget planned for next year. Any way you would be able to send me the annual gross for each of my techs from 2022?

Brian Peterson
Benjamin Johnson
Marcus Eberle
Cory Rose (Salary)
Codey Olson
Ryan Lene
Dylan Brusseau
Scott Sevingy 
Artem Bazhyna 
Justin Olson
Chad Gardner 
Nathan Nichols
Dave Biese 
Jon Gryskiewicz
Patrick Adam
Pete Jacobson
Thomas Sauls
Terry Driscoll 

If you have any questions please let me know. 

Thank you, 
Tim
08/22/23
*/
select *
from ukg.employees
where last_name = 'andrews' 
  and first_name = 'jon'

select * from ukg.employees where first_name = 'artem'

select * 
from ukg.pay_statement_earnings
where employee_account_id = 12986719383

select a.first_name, a.last_name, a.employee_number, 
  b.earning_name, b.ee_amount, c.pay_date
from ukg.employees a
join ukg.pay_statement_earnings b on a.primary_account_id = b.employee_account_id
join ukg.payrolls c on b.payroll_id = c.payroll_id
  and c.pay_date between '01/01/2022' and '12/31/2022'
where a.employee_number in (
	'1110425',
	'184620',
	'137101',
	'111872',
	'1100625',
	'184920',
	'179535',
	'1125565',
	'145999',
	'1106400',
	'150105',
	'160432',	
 	'157953',
	'122558',
	'11660',
	'171055',
	'1122352',
	'135770')



select a.first_name, a.last_name, a.employee_number, c.pay_date, sum(b.ee_amount) as gross
from ukg.employees a
join ukg.pay_statement_earnings b on a.primary_account_id = b.employee_account_id
join ukg.payrolls c on b.payroll_id = c.payroll_id
  and c.pay_date between '01/01/2022' and '12/31/2022'
where a.employee_number in (
	'1110425', -- brian petersopn
	'184620', -- ben johnson
	'137101',  -- marcus eberle
	'1118722', -- cory rose
	'1100625',  -- codey olson
	'184920',  -- ryan lene
	'179535',  -- dylan brusseau
	'1125565',  -- scott sevigny
	'145999',
	'1106400',  -- justin olson
	'150105',  -- chad gardner
	'160432',	 -- nathan nichols
 	'157953',  -- dave bies
	'122558',  -- jon gryskiewicz
	'11660',  -- pat adam
	'171055',  -- pete jacobson
	'1122352',  -- thomas sauls
	'135770')  -- terrance driscoll
group by a.first_name, a.last_name, a.employee_number, c.pay_date	
order by first_name


select a.first_name, a.last_name, a.employee_number, round(sum(b.ee_amount), 2) as gross
from ukg.employees a
join ukg.pay_statement_earnings b on a.primary_account_id = b.employee_account_id
join ukg.payrolls c on b.payroll_id = c.payroll_id
  and c.pay_date between '01/01/2022' and '12/31/2022'
where a.employee_number in (
	'1110425', -- brian petersopn
	'184620', -- ben johnson
	'137101',  -- marcus eberle
	'1118722', -- cory rose
	'1100625',  -- codey olson
	'184920',  -- ryan lene
	'179535',  -- dylan brusseau
	'1125565',  -- scott sevigny
	'145999',  -- artem - no checks in 2022
	'1106400',  -- justin olson
	'150105',  -- chad gardner
	'160432',	 -- nathan nichols
 	'157953',  -- dave bies
	'122558',  -- jon gryskiewicz
	'11660',  -- pat adam
	'171055',  -- pete jacobson
	'1122352',  -- thomas sauls
	'135770')  -- terrance driscoll
group by a.first_name, a.last_name, a.employee_number
order by first_name