﻿/*
3/12 got totally mind fucked yesterday
attempting her eto step through it
*/

-----------------------------------------------------------------------
--< 1 prs.data ddl, keep tech_number (needed for adjustments), remove tech_key, add adjustments & total_flag_hours, 
--		add precision and range to numerics
--    but it turns out i also need tech number in prs.employees, the fiedl is in the ddl, but not in the function
------------------------------------------------------------------------
DROP TABLE if exists prs.data cascade;

CREATE TABLE prs.data
(
  the_date date,
  year_month integer,
  month_name citext,
  first_of_month date,
  last_of_month date,
  sunday_to_saturday_week integer,
  first_of_week date,
  last_of_week date,
  sunday_to_saturday_week_select_format citext,
  biweekly_pay_period_sequence integer,
  biweekly_pay_period_start_date date,
  biweekly_pay_period_end_date date,
  last_name citext,
  first_name citext,
  team_name citext,
  employee_number citext,
  tech_number citext,
--   tech_key integer,
  store citext,
  department citext,
  cost_center citext,
  pay_calc citext,
  flag_hours numeric(8,2),
  adjustments numeric(8,2),
  total_flag_hours numeric(8,2),
  shop_time numeric(8,2),
  clock_hours numeric(8,2));


		
-----------------------------------------------------------------------
--/> 1 prs.data ddl, keep tech_number, remove tech_key, add adjustments & total_flag_hours, 
--		add precision and range to numerics
------------------------------------------------------------------------  

-----------------------------------------------------------------------
--< 2 preload prs.data with 
-- 		refresh prs.tmpben from w10_python ads_for_luigi/tmpBen.py where date > 11/30/21
--    too many problems, go with 1/1/22 -> 3/10/22 (then do a pay period update in pg to compare pay period throught the 10th
------------------------------------------------------------------------

select a.* 
from prs.tmpben a
left join ukg.employees b on a.employeenumber = b.employee_number
where a.flagdeptcode not in ('RE', 'HS') 
  and a.storecode = 'RY1'
  and a.employeenumber not in ('NA','169420','1117960','1126040')
  and a.thedate <= b.term_date
  and a.thedate between '12/31/2021'

-----------------------------------------------------------------------
--/> 2 preload prs.data with 
-- 		refresh prs.tmpben from w10_python ads_for_luigi/tmpBen.py where date > 11/30/21
--    too many problems, go with 1/1/22 -> 3/10/22 (then do a pay period update in pg to compare pay period throught the 10th
------------------------------------------------------------------------


-----------------------------------------------------------------------
--< 3 populate prs.data with data from tmpben fleshed out with ukg
-- 		need adjustments first
--    verify prs.data with production summary pages
------------------------------------------------------------------------

-- need adjustments first
drop table if exists prs.adjustments cascade;
create table prs.adjustments (
  tech_number citext not null,
  the_date date not null,
  adjustment numeric(8,2) not null,
  primary key (tech_number, the_date));

insert into prs.adjustments
select a.technician_id, dds.db2_integer_to_date(a.trans_date) as trans_Date, round(SUM(a.labor_hours), 2) AS adj
FROM arkona.ext_sdpxtim a       
jOIN dds.dim_tech b on a.technician_id = b.tech_number
	and b.current_row
	and b.active
WHERE dds.db2_integer_to_date(a.trans_date) BETWEEN '12/01/2021' and '03/10/2022'
GROUP BY a.technician_id, dds.db2_integer_to_date(a.trans_date)
having sum(a.labor_hours) <> 0;


 
delete from prs.data;
insert into prs.data
select a.*, c.last_name, c.first_name, b.team as team_name, c.employee_number, b.technumber, --b.techkey, 
	e.store, e.department, e.cost_center, g.display_name as pay_calc, b.flaghours, coalesce(h.adjustment, 0), 
	b.flaghours + coalesce(h.adjustment, 0), b.shoptime, b.clockhours
-- select count(*) -- 4998
from (
	select the_date, year_month, month_name, first_of_month,
		(select the_date from dds.dim_date where year_month = a.year_month and last_day_of_month) as last_of_month,
		sunday_to_saturday_week, 
		(select min(the_date) from dds.dim_date where sunday_to_saturday_week = a.sunday_to_saturday_week) as first_of_week,
		(select max(the_date) from dds.dim_date where sunday_to_saturday_week = a.sunday_to_saturday_week) as last_of_week,
		sunday_to_saturday_week_select_format,
		biweekly_pay_period_sequence, biweekly_pay_period_start_date, biweekly_pay_period_end_date
	from dds.dim_date a
	where a.the_date between '12/01/2021' and '03/10/2022') a
join (
	select a.* 
	from prs.tmpben a
	left join ukg.employees b on a.employeenumber = b.employee_number
	where a.flagdeptcode not in ('RE', 'HS') 
		and a.storecode = 'RY1'
		and a.employeenumber not in ('NA','169420','1117960','1126040') -- exclude rogne, shereck, imberry
		and a.thedate <= b.term_date
		and a.thedate between '12/31/2021' and '03/10/2022') b on a.the_date = b.thedate
join ukg.employees c on b.employeenumber = c.employee_number
join ukg.ext_employee_details d on c.employee_id = d.employee_id
join (
	select * 
	from ukg.get_cost_center_hierarchy() a
	where department in ('body shop','main shop')
		and (
			cost_center like '%technician%'
			or
			cost_center like '%intern%')
	and store = 'Rydell GM' ) e on d.cost_center_id = e.id
join ukg.employee_profiles f on c.employee_id = f.employee_id		
join ukg.ext_pay_calc_profiles g on f.pay_calc_id = g.id
left join prs.adjustments h on b.technumber = h.tech_number
  and a.the_date = h.the_date;

select * from prs.data order by the_date, team_name, last_name

-- test look good
select department, sum(flag_hours), sum(shop_time), sum(clock_hours)
from prs.data
where the_date between '02/27/2022' and '03/05/2022'
group by department

select department, sum(flag_hours), sum(shop_time), sum(clock_hours)
from prs.data
where year_month = 202202
group by department

-- save this snapshot of data for later comparison
create table jon.prs_data as
select * from prs.data;

select the_date, count(*)
from jon.prs_data
group by the_date
order by the_date


-----------------------------------------------------------------------
--/> 3 populate prs.data with data from tmpben fleshed out with ukg
-- 		need adjustments first
--    verify prs.data with production summary pages
------------------------------------------------------------------------

-----------------------------------------------------------------------
--< 4 everything is going too well, try the all in one pay period update from yesterda
------------------------------------------------------------------------
-- the assumption here is due to prs.data, i don't need anything in prs.employees prior to the previous date
-- 1st bug, need select statement is not in synch with prs.data

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'prs'
  and table_name = 'data'

-- ok, got it to run, lets look at the week data for 2/27  
-- looks ok
select * from (
select department, sum(flag_hours), sum(shop_time), sum(clock_hours)
from prs.data
where the_date between '02/27/2022' and '03/05/2022'
group by department) a
full outer join (
select department, sum(flag_hours), sum(shop_time), sum(clock_hours)
from jon.prs_data
where the_date between '02/27/2022' and '03/05/2022'
group by department) b on a.department = b.department

-- and march 6, close enough
select * from (
select department, sum(flag_hours), sum(shop_time), sum(clock_hours)
from prs.data
where the_date between '03/06/2022' and '03/10/2022'
group by department) a
full outer join (
select department, sum(flag_hours), sum(shop_time), sum(clock_hours)
from jon.prs_data
where the_date between '03/06/2022' and '03/10/2022'
group by department) b on a.department = b.department

-- so, here is the function prototype

do $$
declare
  -- this will be run affter midnight daily, therefore the date we basing it on is "yesterday"
  -- update tables nightly throughout the pay period

  _current_date date := current_date - 1; -- (select current_date - 1);
  _day_in_pp integer := (
		-- this is to accomodate adjustments ken often makes on sunday at the end of the pay period to account for
		-- what was done on saturday, the last day of the pay period, eg, axtman team needs hours to meet their guarantee
		-- mimics what i do in the tpdata updates
    SELECT day_in_biweekly_pay_period
    FROM dds.dim_date
    WHERE the_date = current_date);  
  _cur_pp_from_date date := (
    select biweekly_pay_period_start_date
    from dds.dim_date
    where the_date = current_date);
  _prev_pp_from_date date := (
    select distinct biweekly_pay_period_start_date
    from dds.dim_date
    where biweekly_pay_period_sequence = (
      select biweekly_pay_period_sequence - 1
      from dds.dim_date
      where the_date = _current_date));
  -- this says no adjustmenting allowed after monday
  _from_date date := (
    select 
      case
        when _day_in_pp > 2 then _cur_pp_from_date
        else _prev_pp_from_date
      end);
  -- this says we are always updating the current pay period as well as sometimes the previous pay period
  _thru_date date := (
    select biweekly_pay_period_end_date
    from dds.dim_date
    where the_date = current_date);    
begin

--   drop table if exists wtf;
--   create temp table wtf as
--   select _current_date, _day_in_pp, _cur_pp_from_date, _prev_pp_from_date, _from_date, _thru_date;  
-- end $$;
-- select * from wtf;
--   only refresh prs.employees for the current_pay period
  delete 
  from prs.employees
  where the_date between _cur_pp_from_date and _thru_date;

	insert into prs.employees
	select aa.the_date, a.last_name, a.first_name, coalesce(i.team_name, 'Hourly') as team_name, a.employee_number, 
		f.tech_number, c.store, c.department, c.cost_center, e.display_name as pay_calc
	from ukg.employees a
	join dds.dim_date aa on aa.the_date between _cur_pp_from_date and _thru_date
	join ukg.ext_employee_details b on a.employee_id = b.employee_id
	join (
		select * 
		from ukg.get_cost_center_hierarchy() a
		where department in ('body shop','main shop')
			and (
				cost_center like '%technician%'
				or
				cost_center like '%intern%')
		and store = 'Rydell GM') c on b.cost_center_id = c.id
	join ukg.employee_profiles d on a.employee_id = d.employee_id		
	join ukg.ext_pay_calc_profiles e on d.pay_calc_id = e.id  
		and e.display_name not in ('salary exempt')  -- exclude craig rogne
	join dds.dim_tech f on a.employee_number = f.employee_number  -- limit to employees with a tech number
		and f.row_thru_date > _cur_pp_from_date
		and f.active		
	left join tp.techs g on a.employee_number = g.employee_number
		and g.thru_date > _cur_pp_from_date
	left join tp.team_techs h on g.tech_key = h.tech_key
		and h.thru_date > _cur_pp_from_date
	left join tp.teams i on h.team_key = i.team_key
		and i.thru_date > _cur_pp_from_date  
	where a.term_date >= _cur_pp_from_date
		and a.employee_number <> '147834'; -- exclude vonasek per John Gardner. only .5 flag hours since hired in 2020  

  delete 
  from prs.gm_main_shop_flag_hours
  where the_date between _cur_pp_from_date and _thru_date;  
  
  insert into prs.gm_main_shop_flag_hours
	select c.employee_number, a.flag_date as the_date, 
		coalesce(sum(flag_hours) filter (where a.opcode_key not in (22106,22107,22108))::numeric(8,2), 0) as flag_hours,
		coalesce(sum(flag_hours) filter (where a.opcode_key in (22106,22107,22108))::numeric(8,2), 0) as shop_time
	from dds.fact_repair_order a
	join dds.dim_tech b on a.tech_key = b.tech_key
	join prs.employees c on b.employee_number = c.employee_number  
		and c.department = 'main shop'
		and c.the_date = a.flag_date
	where a.flag_date between _cur_pp_from_date and _thru_date
	group by  c.employee_number, a.flag_date;

  delete 
  from prs.body_shop_flag_hours
  where the_date between _cur_pp_from_date and _thru_date;

  insert into prs.body_shop_flag_hours
	select c.employee_number, a.close_date as the_date, 
		coalesce(sum(flag_hours) filter (where a.opcode_key not in (22106,22107,22108))::numeric(8,2), 0) as flag_hours,
		coalesce(sum(flag_hours) filter (where a.opcode_key in (22106,22107,22108))::numeric(8,2), 0) as shop_time
	from dds.fact_repair_order a
	join dds.dim_tech b on a.tech_key = b.tech_key
	join prs.employees c on b.employee_number = c.employee_number  
		and c.department = 'body shop'
		and c.the_date = a.close_date
	where a.close_date between _cur_pp_from_date and _thru_date
	group by  c.employee_number, a.close_date;
  
	delete 
	from prs.adjustments
	where the_date between _cur_pp_from_date and _thru_date;
	
	insert into prs.adjustments
	select a.technician_id, dds.db2_integer_to_date(a.trans_date) as trans_Date, round(SUM(a.labor_hours), 2) AS adj
	FROM arkona.ext_sdpxtim a       
	jOIN dds.dim_tech b on a.technician_id = b.tech_number
		and b.current_row
		and b.active
	WHERE dds.db2_integer_to_date(a.trans_date) BETWEEN _cur_pp_from_date and _thru_date
	GROUP BY a.technician_id, dds.db2_integer_to_date(a.trans_date)
	having sum(a.labor_hours) <> 0;

  delete
  from prs.clock_hours
  where the_date between _cur_pp_from_date and _thru_date;

  insert into prs.clock_hours
	select a.the_date, a.employee_number, a.clock_hours::numeric(8,2)
	from ukg.clock_hours a
	join prs.employees b on a.employee_number = b.employee_number
	  and a.the_date = b.the_date
	where a.the_date between _cur_pp_from_date and _thru_date
		and a.clock_hours <> 0;  

  delete 
  from prs.data
  where the_date between _cur_pp_from_date and _thru_date;

  insert into prs.data
  select a.the_date,year_month,month_name,first_of_month,last_of_month,sunday_to_saturday_week,first_of_week,last_of_week,
		sunday_to_saturday_week_select_format,biweekly_pay_period_sequence,biweekly_pay_period_start_date,
		biweekly_pay_period_end_date,last_name,first_name,team_name,b.employee_number,b.tech_number,store,department,
		cost_center,pay_calc,coalesce(flag_hours,0),coalesce(adjustment,0),
		coalesce(flag_hours,0) + coalesce(adjustment,0) as total_flag_hours,coalesce(shop_time,0),coalesce(clock_hours,0)
	from (
		select the_date, year_month, month_name, first_of_month,
			(select the_date from dds.dim_date where year_month = a.year_month and last_day_of_month) as last_of_month,
			sunday_to_saturday_week, 
			(select min(the_date) from dds.dim_date where sunday_to_saturday_week = a.sunday_to_saturday_week) as first_of_week,
			(select max(the_date) from dds.dim_date where sunday_to_saturday_week = a.sunday_to_saturday_week) as last_of_week,
			sunday_to_saturday_week_select_format,
			biweekly_pay_period_sequence, biweekly_pay_period_start_date, biweekly_pay_period_end_date
		from dds.dim_date a
		where a.the_date between _cur_pp_from_date and _thru_date) a
	join prs.employees b on a.the_date = b.the_date
	left join (
		select the_date, employee_number, sum(flag_hours) as flag_hours, sum(shop_time) as shop_time
		from (
			select *
			from prs.gm_main_shop_flag_hours 
			union
			select * 
			from prs.body_shop_flag_hours) aa
		group by the_date, employee_number) c on a.the_date = c.the_date
			and b.employee_number = c.employee_number
	left join prs.adjustments e on a.the_date = e.the_date
	  and b.tech_number = e.tech_number
	left join prs.clock_hours d on a.the_date = d.the_date
		and b.employee_number = d.employee_number		
	where a.the_date between _cur_pp_from_date and _thru_date;

end $$;


-- and the function
create or replace function prs.update_data()
  returns void as
 $BODY$
 /*
  select prs.update_data();
 */
declare
  -- this will be run affter midnight daily, therefore the date we are basing it on is "yesterday"
  -- update tables nightly throughout the pay period

  _current_date date := (select current_date - 1);
  _day_in_pp integer := (
		-- this is to accomodate adjustments ken often makes on sunday at the end of the pay period to account for
		-- what was done on saturday, the last day of the pay period, eg, axtman team needs hours to meet their guarantee
		-- mimics what i do in the tpdata updates
    SELECT day_in_biweekly_pay_period
    FROM dds.dim_date
    WHERE the_date = current_date);  
  _cur_pp_from_date date := (
    select biweekly_pay_period_start_date
    from dds.dim_date
    where the_date = current_date);
  _prev_pp_from_date date := (
    select distinct biweekly_pay_period_start_date
    from dds.dim_date
    where biweekly_pay_period_sequence = (
      select biweekly_pay_period_sequence - 1
      from dds.dim_date
      where the_date = _current_date));
  -- this says no adjustmenting allowed after monday
  _from_date date := (
    select 
      case
        when _day_in_pp > 2 then _cur_pp_from_date
        else _prev_pp_from_date
      end);
  -- this says we are always updating the current pay period as well as sometimes the previous pay period
  _thru_date date := (
    select biweekly_pay_period_end_date
    from dds.dim_date
    where the_date = current_date);    
begin

--   -- this can be useful in troubleshooting if one needs to know the actual dates being used
--   drop table if exists wtf;
--   create temp table wtf as
--   select _current_date, _day_in_pp, _cur_pp_from_date, _prev_pp_from_date, _from_date, _thru_date;  

--   only refresh prs.employees for the current_pay period
  delete 
  from prs.employees
  where the_date between _cur_pp_from_date and _thru_date;

	insert into prs.employees
	select aa.the_date, a.last_name, a.first_name, coalesce(i.team_name, 'Hourly') as team_name, a.employee_number, 
		f.tech_number, c.store, c.department, c.cost_center, e.display_name as pay_calc
	from ukg.employees a
	join dds.dim_date aa on aa.the_date between _cur_pp_from_date and _thru_date
	join ukg.ext_employee_details b on a.employee_id = b.employee_id
	join (
		select * 
		from ukg.get_cost_center_hierarchy() a
		where department in ('body shop','main shop')
			and (
				cost_center like '%technician%'
				or
				cost_center like '%intern%')
		and store = 'Rydell GM') c on b.cost_center_id = c.id
	join ukg.employee_profiles d on a.employee_id = d.employee_id		
	join ukg.ext_pay_calc_profiles e on d.pay_calc_id = e.id  
		and e.display_name not in ('salary exempt')  -- exclude craig rogne
	join dds.dim_tech f on a.employee_number = f.employee_number  -- limit to employees with a tech number
		and f.row_thru_date > _cur_pp_from_date
		and f.active		
	left join tp.techs g on a.employee_number = g.employee_number
		and g.thru_date > _cur_pp_from_date
	left join tp.team_techs h on g.tech_key = h.tech_key
		and h.thru_date > _cur_pp_from_date
	left join tp.teams i on h.team_key = i.team_key
		and i.thru_date > _cur_pp_from_date  
	where a.term_date >= _cur_pp_from_date
		and a.employee_number <> '147834'; -- exclude vonasek per John Gardner. only .5 flag hours since hired in 2020  

  delete 
  from prs.gm_main_shop_flag_hours
  where the_date between _from_date and _thru_date;  
  
  insert into prs.gm_main_shop_flag_hours
	select c.employee_number, a.flag_date as the_date, 
		coalesce(sum(flag_hours) filter (where a.opcode_key not in (22106,22107,22108))::numeric(8,2), 0) as flag_hours,
		coalesce(sum(flag_hours) filter (where a.opcode_key in (22106,22107,22108))::numeric(8,2), 0) as shop_time
	from dds.fact_repair_order a
	join dds.dim_tech b on a.tech_key = b.tech_key
	join prs.employees c on b.employee_number = c.employee_number  
		and c.department = 'main shop'
		and c.the_date = a.flag_date
	where a.flag_date between _from_date and _thru_date
	group by  c.employee_number, a.flag_date;

  delete 
  from prs.body_shop_flag_hours
  where the_date between _from_date and _thru_date;

  insert into prs.body_shop_flag_hours
	select c.employee_number, a.close_date as the_date, 
		coalesce(sum(flag_hours) filter (where a.opcode_key not in (22106,22107,22108))::numeric(8,2), 0) as flag_hours,
		coalesce(sum(flag_hours) filter (where a.opcode_key in (22106,22107,22108))::numeric(8,2), 0) as shop_time
	from dds.fact_repair_order a
	join dds.dim_tech b on a.tech_key = b.tech_key
	join prs.employees c on b.employee_number = c.employee_number  
		and c.department = 'body shop'
		and c.the_date = a.close_date
	where a.close_date between _from_date and _thru_date
	group by  c.employee_number, a.close_date;
  
	delete 
	from prs.adjustments
	where the_date between _from_date and _thru_date;
	
	insert into prs.adjustments
	select a.technician_id, dds.db2_integer_to_date(a.trans_date) as trans_Date, round(SUM(a.labor_hours), 2) AS adj
	FROM arkona.ext_sdpxtim a       
	jOIN dds.dim_tech b on a.technician_id = b.tech_number
		and b.current_row
		and b.active
	WHERE dds.db2_integer_to_date(a.trans_date) BETWEEN _from_date and _thru_date
	GROUP BY a.technician_id, dds.db2_integer_to_date(a.trans_date)
	having sum(a.labor_hours) <> 0;

  delete
  from prs.clock_hours
  where the_date between _from_date and _thru_date;

  insert into prs.clock_hours
	select a.the_date, a.employee_number, a.clock_hours::numeric(8,2)
	from ukg.clock_hours a
	join prs.employees b on a.employee_number = b.employee_number
	  and a.the_date = b.the_date
	where a.the_date between _from_date and _thru_date
		and a.clock_hours <> 0;  

  delete 
  from prs.data
  where the_date between _from_date and _thru_date;

  insert into prs.data
  select a.the_date,year_month,month_name,first_of_month,last_of_month,sunday_to_saturday_week,first_of_week,last_of_week,
		sunday_to_saturday_week_select_format,biweekly_pay_period_sequence,biweekly_pay_period_start_date,
		biweekly_pay_period_end_date,last_name,first_name,team_name,b.employee_number,b.tech_number,store,department,
		cost_center,pay_calc,coalesce(flag_hours,0),coalesce(adjustment,0),
		coalesce(flag_hours,0) + coalesce(adjustment,0) as total_flag_hours,coalesce(shop_time,0),coalesce(clock_hours,0)
	from (
		select the_date, year_month, month_name, first_of_month,
			(select the_date from dds.dim_date where year_month = a.year_month and last_day_of_month) as last_of_month,
			sunday_to_saturday_week, 
			(select min(the_date) from dds.dim_date where sunday_to_saturday_week = a.sunday_to_saturday_week) as first_of_week,
			(select max(the_date) from dds.dim_date where sunday_to_saturday_week = a.sunday_to_saturday_week) as last_of_week,
			sunday_to_saturday_week_select_format,
			biweekly_pay_period_sequence, biweekly_pay_period_start_date, biweekly_pay_period_end_date
		from dds.dim_date a
		where a.the_date between _from_date and _thru_date) a
	join prs.employees b on a.the_date = b.the_date
	left join (
		select the_date, employee_number, sum(flag_hours) as flag_hours, sum(shop_time) as shop_time
		from (
			select *
			from prs.gm_main_shop_flag_hours 
			union
			select * 
			from prs.body_shop_flag_hours) aa
		group by the_date, employee_number) c on a.the_date = c.the_date
			and b.employee_number = c.employee_number
	left join prs.adjustments e on a.the_date = e.the_date
	  and b.tech_number = e.tech_number
	left join prs.clock_hours d on a.the_date = d.the_date
		and b.employee_number = d.employee_number		
	where a.the_date between _from_date and _thru_date;
end;
$BODY$
language plpgsql;	

-- the pacing function
create or replace function prs.get_gm_main_shop_pacing()
  returns table(flag_hours_to_date numeric,clock_hours_to_date numeric, proficiency_to_date integer,
    wd_in_month integer, wd_of_month_elapsed integer, flag_pace numeric, clock_pace numeric,
    proficiencey_pace integer) as
$BODY$
/*
  select * from prs.get_gm_main_shop_pacing();
*/
declare
  _year_month integer := (
		select year_month 
		from dds.dim_date    
		where the_date = current_date - 1);
begin		
return query
	select a.flag_hours_to_date, a.clock_hours_to_date, 
		case
			when a.clock_hours_to_date = 0 then 0
			else (100 * round(a.flag_hours_to_date/a.clock_hours_to_date, 2))::integer
		end as prof_to_date,
		b.wd_in_month, b.wd_of_month_elapsed,
		round(a.flag_hours_to_date * b.wd_in_month/b.wd_of_month_elapsed, 2) as flag_pace,
		round(a.clock_hours_to_date * b.wd_in_month/b.wd_of_month_elapsed, 2) as clock_pace,
		(100 * (round(a.flag_hours_to_date * b.wd_in_month/b.wd_of_month_elapsed, 2) / round(a.clock_hours_to_date * b.wd_in_month/b.wd_of_month_elapsed, 2)))::integer prof_pace
	from (
	select sum(total_flag_hours) as flag_hours_to_date, sum(clock_hours) as clock_hours_to_date
	from prs.data a
	where a.department = 'main shop'
		and a.year_month = _year_month) a
	join (
	select * 
	from dds.working_days 
	where year_month = _year_month
		and department = 'service'
		and the_date = current_date - 1) b on true;
end
$BODY$
language plpgsql;

