﻿decided to go the clone schema path
the idea is to incorporate all the hn and toyota data into the existing tables, see if that works out ok

-- for this to work, had to temporarily drop prs.get_json_gm_main_shop_pacing(); because it references prs.get_gm_main_shop_pacing() 
-- doesn't completely make sense, but i dropped the function, cloned the schema, it worked, and then added the function back to prs
-- so prs_test does not have function get_json_gm_main_shop_pacing()

SELECT jon.clone_schema('prs', 'prs_test', TRUE);

select store, count(*) from prs_test.employees group by store

select * from prs_test.employees where the_date = current_date - 1 order by store, team_name, last_name


select * from dds.dim_opcode where opcode_key in (22106,22107,22108)

select * from dds.dim_opcode where description like '%train%' or description like '%shop%' order by store_code, opcode

ry2: IST, SHOP, SHOPTIME, TRAIN

ry8: none



-- somehow got all store main shop flag hours in 1 table
-- i think this will just work
-- depends on how the final queries break it out, but i think it is by tech, so we should be good
	select c.employee_number, a.flag_date as the_date, 
		coalesce(sum(flag_hours) filter (where a.opcode_key not in (22106,22107,22108,15559,27588,17935))::numeric(8,2), 0) as flag_hours,
		coalesce(sum(flag_hours) filter (where a.opcode_key in (22106,22107,22108,15559,27588,17935))::numeric(8,2), 0) as shop_time
	from dds.fact_repair_order a
	join dds.dim_tech b on a.tech_key = b.tech_key
	join prs_test.employees c on b.employee_number = c.employee_number  
		and c.department = 'main shop'
		and c.the_date = a.flag_date
	where a.flag_date between '09/25/2022' and '10/08/2022'
	group by  c.employee_number, a.flag_date;


-- and the final query  select a.the_date,year_month,month_name,first_of_month,last_of_month,sunday_to_saturday_week,first_of_week,last_of_week,

  delete 
  from prs_test.data
  where the_date between '09/25/2022' and '10/08/2022';

  insert into prs_test.data
  select a.the_date,year_month,month_name,first_of_month,last_of_month,sunday_to_saturday_week,first_of_week,last_of_week,
		sunday_to_saturday_week_select_format,biweekly_pay_period_sequence,biweekly_pay_period_start_date,
		biweekly_pay_period_end_date,last_name,first_name,team_name,b.employee_number,b.tech_number,store,department,
		cost_center,pay_calc,coalesce(flag_hours,0),coalesce(adjustment,0),
		coalesce(flag_hours,0) + coalesce(adjustment,0) as total_flag_hours,coalesce(shop_time,0),coalesce(clock_hours,0)
	from (
		select the_date, year_month, month_name, first_of_month,
			(select the_date from dds.dim_date where year_month = a.year_month and last_day_of_month) as last_of_month,
			sunday_to_saturday_week, 
			(select min(the_date) from dds.dim_date where sunday_to_saturday_week = a.sunday_to_saturday_week) as first_of_week,
			(select max(the_date) from dds.dim_date where sunday_to_saturday_week = a.sunday_to_saturday_week) as last_of_week,
			sunday_to_saturday_week_select_format,
			biweekly_pay_period_sequence, biweekly_pay_period_start_date, biweekly_pay_period_end_date
		from dds.dim_date a
		where a.the_date between '09/25/2022' and '10/08/2022') a
	join prs_test.employees b on a.the_date = b.the_date
	left join (
		select the_date, employee_number, sum(flag_hours) as flag_hours, sum(shop_time) as shop_time
		from (
			select *
			from prs_test.gm_main_shop_flag_hours 
			union
			select * 
			from prs_test.body_shop_flag_hours) aa
		group by the_date, employee_number) c on a.the_date = c.the_date
			and b.employee_number = c.employee_number
	left join prs_test.adjustments e on a.the_date = e.the_date
	  and b.tech_number = e.tech_number
	left join prs_test.clock_hours d on a.the_date = d.the_date
		and b.employee_number = d.employee_number		
	where a.the_date between '09/25/2022' and '10/08/2022'



select store, department, team_name, last_name, sum(flag_hours) as flag, sum(adjustments) as adj,
  sum(shop_time) as shop, sum(clock_hours) as clock 
from prs_test.data where biweekly_pay_period_sequence = 360
group by store, department, team_name, last_name
order by store, department, team_name, last_name


-- added store to this function, seems to work
select prs_test.get_gm_main_shop_pacing()




































team name hourly wont work for honda and toyota
there are no teams
so it will have to be flat rate or hourly
probabl need a separate query for hn/ty techs

drop table if exists employees_gm cascade;
create temp table employees_gm as
	select aa.the_date, a.last_name, a.first_name, coalesce(i.team_name, 'Hourly') as team_name, a.employee_number, 
		f.tech_number, c.store, c.department, c.cost_center, e.display_name as pay_calc
	from ukg.employees a
	join dds.dim_date aa on aa.the_date between '09/25/2022' and '10/08/2022'
	join ukg.ext_employee_details b on a.employee_id = b.employee_id
	join (
		select * 
		from ukg.get_cost_center_hierarchy() a
		where department in ('body shop','main shop')
			and (
				cost_center like '%technician%'
				or
				cost_center like '%intern%')) c on b.cost_center_id = c.id
-- -- 		and store = 'Rydell GM') c on b.cost_center_id = c.id
	join ukg.employee_profiles d on a.employee_id = d.employee_id		
	join ukg.ext_pay_calc_profiles e on d.pay_calc_id = e.id  
		and e.display_name not in ('salary exempt')  -- exclude craig rogne
	join dds.dim_tech f on a.employee_number = f.employee_number  -- limit to employees with a tech number
		and f.row_thru_date > '09/25/2022'
		and f.active		
		and f.current_row
		and f.store_code = 'RY1' -- exclude mcveigh's ry2 instance
	left join tp.techs g on a.employee_number = g.employee_number
		and g.thru_date > '09/25/2022'
	left join tp.team_techs h on g.tech_key = h.tech_key
		and h.thru_date > '09/25/2022'
	left join tp.teams i on h.team_key = i.team_key
		and i.thru_date > '09/25/2022'  
	where a.term_date >= '09/25/2022'
	  and a.store = 'GM'
		and a.employee_number <> '147834'; -- exclude vonasek per John Gardner. only .5 flag hours since hired in 2020  
create unique index on employees_gm(employee_number,the_date);



	  
select * from employees_gm where employee_number = '194675' and the_date = '10/08/2022'
select * from tp.techs where employee_number = '194675'
select * from employees limit 10
select * From ukg.employees limit 10
select * from ukg.ext_pay_calc_profiles limit 10
select * from ukg.get_cost_center_hierarchy() 

drop table if exists employees_hn cascade;
create temp table employees_hn as
	select aa.the_date, a.last_name, a.first_name, 
		case
			when e.display_name like '%flat%' then 'flat rate'
			when e.display_name like '%hourly' then 'hourly'
			else 'XXXXXXXX'
		end as team_name, a.employee_number, 
		f.tech_number, c.store, c.department, c.cost_center, e.display_name as pay_calc
	from ukg.employees a
	join dds.dim_date aa on aa.the_date between '09/25/2022' and '10/08/2022'
	join ukg.ext_employee_details b on a.employee_id = b.employee_id
	join (
		select * 
		from ukg.get_cost_center_hierarchy() a
		where department = 'main shop') c on b.cost_center_id = c.id
	join ukg.employee_profiles d on a.employee_id = d.employee_id		
	join ukg.ext_pay_calc_profiles e on d.pay_calc_id = e.id  
		and e.display_name not in ('salary exempt')  -- exclude craig rogne
	join dds.dim_tech f on a.employee_number = f.employee_number  -- limit to employees with a tech number
		and f.row_thru_date > '09/25/2022'
		and f.active		
		and f.current_row
		and f.store_code = 'RY2' 		
	where a.term_date >= '09/25/2022'
	  and a.store = 'hn';
create unique index on employees_hn(employee_number,the_date);  	

drop table if exists employees_hn cascade;
create temp table employees_hn as
	select aa.the_date, a.last_name, a.first_name, 
		case
			when e.display_name like '%flat%' then 'flat rate'
			when e.display_name like '%hourly' then 'hourly'
			else 'XXXXXXXX'
		end as team_name, a.employee_number, 
		f.tech_number, c.store, c.department, c.cost_center, e.display_name as pay_calc
	from ukg.employees a
	join dds.dim_date aa on aa.the_date between '09/25/2022' and '10/08/2022'
	join ukg.ext_employee_details b on a.employee_id = b.employee_id
	join (
		select * 
		from ukg.get_cost_center_hierarchy() a
		where department = 'main shop') c on b.cost_center_id = c.id
	join ukg.employee_profiles d on a.employee_id = d.employee_id		
	join ukg.ext_pay_calc_profiles e on d.pay_calc_id = e.id  
		and e.display_name not in ('salary exempt')  -- exclude craig rogne
	join dds.dim_tech f on a.employee_number = f.employee_number  -- limit to employees with a tech number
		and f.row_thru_date > '09/25/2022'
		and f.active		
		and f.current_row
		and f.store_code = 'RY8' 		
	where a.term_date >= '09/25/2022'
	  and a.store = 'toyota';
create unique index on employees_hn(employee_number,the_date);  	


select * from dds.dim_opcode where opcode_key in (22106,22107,22108)

select * from dds.dim_opcode where description like '%train%' or description like '%shop%' order by store_code, opcode

ry2: IST, SHOP, SHOPTIME, TRAIN

ry8: none

select * from prs.ty_employees