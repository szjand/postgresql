﻿from data.php

all this ben stuff is in scotextK
execute procedure benGetProductionDetailsWeek( ?, ?, ? )
and all the other benGet stored procs only use the table tmpBen
tables:
	tmpBen
aha, the key is xfmTmpBen()  which is based on E:\sql\advantage\old_vision\v2.9.0.sql
tables from xfmTmpBen
	dds.stgArkonaSDPXTIM
	ukg.clock_hours
	tmpDeptTechCensus  EXECUTE PROCEDURE xfmDeptTechCensus ()
	dds.factRepairOrder
	dds.dimTech


	-- hourly techs gm main shop
			select 'GM Main Shop Hourly Technicians' as pay_group, a.store, a.last_name || ', ' || a.first_name  as employee,
			a.employee_id, a.employee_number,  a.cost_center, /*c.job_category, c.job_name,*/ coalesce(a.term_date, '12/31/9999')
		from ukg.employees a
		join ukg.ext_employee_pay_info b on a.employee_id = b.employee_id
		join ukg.ext_pay_types c on b.pay_type_id = c.id
		  and c.pay_type_name = 'Hourly'
		where cost_center in ( 'Main Shop Technician','Quick Service Technician','Accessory Technician')
			and store = 'GM'
			and a.status = 'active'



-- this gives me all the full 3 level identifiers with ids
select * 
from (
	select c.name as level_1, c.id as level_1_id, b.name as level_2, b.id as level_2_id, a.name as level_3, a.id as level_3_id
	from ukg.ext_cost_centers a
	left join ukg.ext_cost_centers b on a.parent_id = b.id
	left join ukg.ext_cost_centers c on b.parent_id = c.id) d
where level_1 <> 'Location' -- 2 level heirarchy
  and level_1 is not null
order by level_1, level_2, level_3

create or replace function ukg.get_cost_center_hierarchy()
returns table (store citext, department citext, cost_center citext) as
$BODY$
/*
select * from ukg.get_cost_center_hierarchy()
*/

select * 
from (
	select c.name as store, b.name as department, a.name as cost_center
	from ukg.ext_cost_centers a
	left join ukg.ext_cost_centers b on a.parent_id = b.id
	left join ukg.ext_cost_centers c on b.parent_id = c.id) d
where store <> 'Location' -- 2 level heirarchy
  and store is not null
order by store, department, cost_center;
$BODY$
language sql;


-- use this as a basis for census
select a.last_name, a.first_name, a.employee_number, a.cost_center, d.job_name, e.pay_type_name, 
	f.employee_type_name, g.tech_number, g.labor_cost, g.flag_department_code
from ukg.employees a
join (
	select  cost_center 
	from ukg.get_cost_center_hierarchy()
	where store = 'rydell gm'
		and department = 'main shop'
		and cost_center like '%tech%') b on a.cost_center = b.cost_center
join ukg.ext_employee_pay_info c on a.employee_id = c.employee_id		
left join ukg.ext_cost_center_job_details d on c.default_job_id = d.id
left join ukg.ext_pay_types e on c.pay_type_id = e.id
left join ukg.ext_employee_types f on c.employee_type_id = f.id
left join dds.dim_tech g on a.employee_number = g.employee_number
  and g.row_thru_date > current_date
-- join ukg.ext_pay_types c on a.pay_type_id = c.id
where a.status = 'active'		
  and e.pay_type_name <> 'salary'
  and a.store = 'GM'
order by pay_type_name, a.last_name

-- add team
-- don't know, need historical state as will so ... just the current team placement is not good enuf, time to go back and see whatt the current data structure is like
select a.last_name, a.first_name, a.employee_number, a.cost_center, d.job_name, e.pay_type_name, 
	f.employee_type_name, g.tech_number, g.labor_cost, g.flag_department_code
from ukg.employees a
join (
	select  cost_center 
	from ukg.get_cost_center_hierarchy()
	where store = 'rydell gm'
		and department = 'main shop'
		and cost_center like '%tech%') b on a.cost_center = b.cost_center
join ukg.ext_employee_pay_info c on a.employee_id = c.employee_id		
left join ukg.ext_cost_center_job_details d on c.default_job_id = d.id
left join ukg.ext_pay_types e on c.pay_type_id = e.id
left join ukg.ext_employee_types f on c.employee_type_id = f.id
left join dds.dim_tech g on a.employee_number = g.employee_number
  and g.row_thru_date > current_date
-- join ukg.ext_pay_types c on a.pay_type_id = c.id
where a.status = 'active'		
  and e.pay_type_name <> 'salary'
  and a.store = 'GM'
order by pay_type_name, a.last_name


select * from dds.dim_tech limit 50

-- use this as a basis for census
select a.last_name, a.first_name, a.employee_number, a.cost_center, d.job_name, e.pay_type_name, f.employee_type_name
from ukg.employees a
join (
	select  cost_center 
	from ukg.get_cost_center_hierarchy()
	where store = 'rydell gm'
		and department = 'body shop'
		and cost_center like '%tech%') b on a.cost_center = b.cost_center
join ukg.ext_employee_pay_info c on a.employee_id = c.employee_id		
left join ukg.ext_cost_center_job_details d on c.default_job_id = d.id
left join ukg.ext_pay_types e on c.pay_type_id = e.id
left join ukg.ext_employee_types f on c.employee_type_id = f.id
-- join ukg.ext_pay_types c on a.pay_type_id = c.id
where a.status = 'active'		
order by a.last_name


------------------------------------------------------
-- xfmDeptTechCensus()
------------------------------------------------------
select * from dds.dim_tech where tech_number = '135' limit 100

-- dds.dim_tech does not have the description for department techs
SELECT a.the_date, b.store_Code, -1 as employee_Key, b.employee_Number, 
  b.tech_Key, b.tech_Number, b.tech_name, -- coalesce(b.tech_name, b.description) AS description,
  c.first_name, c.last_name, TRIM(c.first_Name) || ' ' || c.last_Name AS fullName,
  b.flag_Department_Code, 
  CASE 
    WHEN b.employee_Number = 'NA' THEN 'Department'
    ELSE
      CASE
        WHEN d.team_Name IS NULL THEN 'Hourly'
        ELSE d.team_Name
      END
  END AS Team,
  b.labor_Cost , 'n/a' as fullPartTime 
from dds.dim_date a
left join dds.dim_tech b on a.the_date between b.row_from_date and b.row_thru_date
  and b.active
  and b.flag_department_code not in ('NA','UN')
  and b.tech_number <> '999'
LEFT JOIN ukg.employees c on b.employee_number = c.employee_number  
LEFT JOIN (
  SELECT w.team_key, w.team_name, w.department_key, x.from_Date, x.thru_Date, y.employee_number
  FROM tp.Teams w
  LEFT JOIN tp.Team_Techs x on w.team_key = x.team_key
  LEFT JOIN tp.Techs y on x.tech_Key = y.tech_Key) d 
    on b.employee_number = d.employee_number 
      AND a.the_date BETWEEN d.from_date AND d.thru_date    
WHERE a.the_date = current_date - 8 --BETWEEN current_date - 31 AND current_date
  AND b.employee_number <> 'NA' -- NOT IN ('174384','147834');   
order by tech_number


select * from dds.dim_tech where tech_number = '106'


		select 'HN Main Shop Hourly Technicians' as pay_group, a.store, a.last_name || ', ' || a.first_name  as employee,
			a.employee_id, a.employee_number,  a.cost_center, coalesce(a.term_date, '12/31/9999')
		from ukg.employees a
		join ukg.employee_profiles b on a.employee_id = b.employee_id
		join ukg.ext_pay_calc_profiles c on b.pay_calc_id = c.id
			and c.display_name in ('Full-Time Hourly','Part-Time Hourly')
		where cost_center = 'Main Shop Technician'
			and store = 'HN'
			and a.status = 'active'
    union
-- For GM Main Shop Hourly Technicians
		select 'GM Main Shop Hourly Technicians' as pay_group, a.store, a.last_name || ', ' || a.first_name  as employee,
			a.employee_id, a.employee_number,  a.cost_center, /*c.job_category, c.job_name,*/ coalesce(a.term_date, '12/31/9999')
		from ukg.employees a
		join ukg.employee_profiles b on a.employee_id = b.employee_id
		join ukg.ext_pay_calc_profiles c on b.pay_calc_id = c.id
			and c.display_name in ('Full-Time Hourly','Part-Time Hourly')
		where cost_center in ( 'Main Shop Technician','Quick Service Technician','Accessory Technician')
			and store = 'GM'
			and a.status = 'active'

		select 'GM Detail' as pay_group,a.store, a.last_name || ', ' || a.first_name,
			a.employee_id, a.employee_number,  a.cost_center,  coalesce(a.term_date, '12/31/9999')
		from ukg.employees a
		join ukg.employee_profiles b on a.employee_id = b.employee_id
		join ukg.ext_pay_calc_profiles c on b.pay_calc_id = c.id
			and c.display_name in ('Part-Time Flat Rate','Full-Time Flat Rate')
		where cost_center = 'Detail Technician'
			and a.status = 'active'
			and store = 'GM'					

-- instead of an exact mimicing of the ads function, think ukg
-- afton is thinking 3 months of history
-- i am thinking nightly of refreshing 14 days (pay period), actually, just refresh the pay period

use cost center and pay calc profiles to identify and categorize techs
select * from ukg.employees limit 10


----------------------------------------------------------------------------------
-- 3/3/22 decision has been made, get out of advantage
-- this is a reasonable start (i hope)
-- in spite of conversation with afton this morning, am not including detail techs at this time
----------------------------------------------------------------------------------
-- need to exclude rogne
-- reuter is wrong
-- this shows way more hourly bs techs than old vision
-- gm ms hourly includes valdez, old vision does not
create schema prs;
comment on schema prs is 'production summary, temp schema for developing the migration of old vision production summary 
to new vision';


-- may need to include techs termed within pay period
drop table if exists prs.employees cascade;
create table prs.employees as
select a.last_name, a.first_name, a.employee_number, f.tech_number, c.store, c.department, c.cost_center, e.display_name as pay_calc
from ukg.employees a
join ukg.ext_employee_details b on a.employee_id = b.employee_id
join (
	select * 
	from ukg.get_cost_center_hierarchy() a
	where department in ('body shop','main shop')
		and (
			cost_center like '%technician%'
			or
			cost_center like '%intern%')
	and store = 'Rydell GM') c on b.cost_center_id = c.id
join ukg.employee_profiles d on a.employee_id = d.employee_id		
join ukg.ext_pay_calc_profiles e on d.pay_calc_id = e.id  
  and e.display_name not in ('salary exempt')  -- exclude craig rogne
join dds.dim_tech f on a.employee_number = f.employee_number  -- limit to employees with a tech number
  and f.row_thru_date > current_date
  and f.active
where a.term_date > current_date;
alter table prs.employees add primary key (employee_number);

select * from prs.employees where last_name in ('bair','sauls')

select * from prs.employees where employee_number = '1122352'

select * from dds.dim_tech where tech_name like 'saul%'

select * from dds.dim_opcode where opcode_key in (22106,22107,22108)



-- body shop close date
select f.team_name, c.last_name, c.first_name, a.store_code, a.close_date, b.tech_key, b.tech_number,
  sum(flag_hours) filter (where a.opcode_key not in (22106,22107,22108)) as flag_hours,
  sum(flag_hours) filter (where a.opcode_key in (22106,22107,22108)) as shop_time
from dds.fact_repair_order a
join dds.dim_tech b on a.tech_key = b.tech_key
join prs.employees c on b.employee_number = c.employee_number  
  and c.department = 'body shop'
left join tp.techs d on b.employee_number = d.employee_number
  and d.thru_date > current_date
left join tp.team_techs e on d.tech_key = e.tech_key
  and e.thru_date > current_date
left join tp.teams f on e.team_key = f.team_key
  and f.thru_date > current_date    
where a.close_date between '02/01/2022' and '02/28/2022' 
group by f.team_name, c.last_name, c.first_name, a.store_code, a.close_date, b.tech_key, b.tech_number


select coalesce(team_name, 'Hourly') as team_name, last_name, first_name, sum(flag_hours), sum(shop_time)
from (
select f.team_name, c.last_name, c.first_name, a.store_code, a.close_date, b.tech_key, b.tech_number,
  sum(flag_hours) filter (where a.opcode_key not in (22106,22107,22108)) as flag_hours,
  sum(flag_hours) filter (where a.opcode_key in (22106,22107,22108)) as shop_time
from dds.fact_repair_order a
join dds.dim_tech b on a.tech_key = b.tech_key
join prs.employees c on b.employee_number = c.employee_number  
  and c.department = 'body shop'
left join tp.techs d on b.employee_number = d.employee_number
  and d.thru_date > current_date
left join tp.team_techs e on d.tech_key = e.tech_key
  and e.thru_date > current_date
left join tp.teams f on e.team_key = f.team_key
  and f.thru_date > current_date    
where a.close_date between '03/01/2022' and '03/06/2022' 
group by f.team_name, c.last_name, c.first_name, a.store_code, a.close_date, b.tech_key, b.tech_number) x
group by coalesce(team_name, 'Hourly'), last_name, first_name
order by team_name, last_name

select * from tp.techs where last_name like 'grys%'

--------------------------------------------------------------------------------------------
-- main shop flag date
select f.team_name, c.last_name, c.first_name, a.store_code, a.flag_date, b.tech_key, b.tech_number,
  sum(flag_hours) filter (where a.opcode_key not in (22106,22107,22108)) as flag_hours,
  sum(flag_hours) filter (where a.opcode_key in (22106,22107,22108)) as shop_time
from dds.fact_repair_order a
join dds.dim_tech b on a.tech_key = b.tech_key
join prs.employees c on b.employee_number = c.employee_number  
  and c.department = 'main shop'
left join tp.techs d on b.employee_number = d.employee_number
	and d.thru_date > current_date
left join tp.team_techs e on d.tech_key = e.tech_key
  and e.thru_date > current_date
left join tp.teams f on e.team_key = f.team_key
  and f.thru_date > current_date    
where a.flag_date between '02/13/2022' and '02/26/2022' 
group by f.team_name, c.last_name, c.first_name, a.store_code, a.flag_date, b.tech_key, b.tech_number

select coalesce(team_name, 'Hourly') as team_name, last_name, first_name, sum(flag_hours), sum(shop_time)
from (
	select f.team_name, c.last_name, c.first_name, a.store_code, a.flag_date, b.tech_key, b.tech_number,
		sum(flag_hours) filter (where a.opcode_key not in (22106,22107,22108)) as flag_hours,
		sum(flag_hours) filter (where a.opcode_key in (22106,22107,22108)) as shop_time
	from dds.fact_repair_order a
	join dds.dim_tech b on a.tech_key = b.tech_key
	join prs.employees c on b.employee_number = c.employee_number  
		and c.department = 'main shop'
	left join tp.techs d on b.employee_number = d.employee_number
	  and d.thru_date > current_date
	left join tp.team_techs e on d.tech_key = e.tech_key
		and e.thru_date > current_date
	left join tp.teams f on e.team_key = f.team_key
		and f.thru_date > current_date    
	where a.flag_date between '03/01/2022' and '03/06/2022' 
	group by f.team_name, c.last_name, c.first_name, a.store_code, a.flag_date, b.tech_key, b.tech_number) x
group by coalesce(team_name, 'Hourly'), last_name, first_name
order by team_name, last_name

----------------------------------------------------------------------------------
-- clock hours
select a.the_date, a.employee_number, a.clock_hours
from ukg.clock_hours a
join prs.employees b on a.employee_number = b.employee_number
where a.the_date between '02/01/2022' and '02/28/2022'
  and a.clock_hours <> 0

select * from prs.employees

select aa.the_date, bb.*, cc.clock_hours
from dds.dim_date aa
left join lateral (
	select c.store, c.department, coalesce(f.team_name, 'Hourly') as team, c.last_name, c.first_name, c.employee_number, a.close_date as the_date, b.tech_number,
		coalesce(sum(flag_hours) filter (where a.opcode_key not in (22106,22107,22108)), 0) as flag_hours,
		coalesce(sum(flag_hours) filter (where a.opcode_key in (22106,22107,22108)), 0) as shop_time
	from dds.fact_repair_order a
	join dds.dim_tech b on a.tech_key = b.tech_key
	join prs.employees c on b.employee_number = c.employee_number  
		and c.department = 'body shop'
	left join tp.techs d on b.employee_number = d.employee_number
		and d.thru_date > current_date
	left join tp.team_techs e on d.tech_key = e.tech_key
		and e.thru_date > current_date
	left join tp.teams f on e.team_key = f.team_key
		and f.thru_date > current_date    
	where a.close_date = aa.the_date -- between '01/01/2022' and '01/31/2022' 
	group by c.store, c.department, coalesce(f.team_name, 'Hourly'), c.last_name, c.first_name, c.employee_number,a.close_date, b.tech_number
	union 
	select c.store, c.department, coalesce(f.team_name, 'Hourly')as team, c.last_name, c.first_name, c.employee_number,a.flag_date, b.tech_number,
		coalesce(sum(flag_hours) filter (where a.opcode_key not in (22106,22107,22108)), 0) as flag_hours,
		coalesce(sum(flag_hours) filter (where a.opcode_key in (22106,22107,22108)), 0) as shop_time
	from dds.fact_repair_order a
	join dds.dim_tech b on a.tech_key = b.tech_key
	join prs.employees c on b.employee_number = c.employee_number  
		and c.department = 'main shop'
	left join tp.techs d on b.employee_number = d.employee_number
		and d.thru_date > current_date
	left join tp.team_techs e on d.tech_key = e.tech_key
		and e.thru_date > current_date
	left join tp.teams f on e.team_key = f.team_key
		and f.thru_date > current_date    
	where a.flag_date = aa.the_date -- between '01/01/2022' and '01/31/2022' 
	group by c.store, c.department, coalesce(f.team_name, 'Hourly'), c.last_name, c.first_name, c.employee_number,a.flag_date, b.tech_number) bb on aa.the_date = bb.the_date
left join lateral (
	select a.the_date, a.employee_number, a.clock_hours
	from ukg.clock_hours a
	join prs.employees b on a.employee_number = b.employee_number
	where a.the_date = aa.the_date
		and a.clock_hours <> 0) cc on aa.the_date = cc.the_date
		  and bb.employee_number = cc.employee_number
where aa.the_date between '01/01/2022' and '01/31/2022'

----------------------------------------------------------------------------------
-- break it up into separate prs. tables
-- update each table for the relevant pay period
-- final table prs.data will have tech data, date data, flag hours, shop time and clock hours

-- thru_date of team, tech, etc tables > the from_date of the relevant pay period
-- that will catch everyone employed for any part of the pay period
-- the first pay period will be 1/2 -> 1/15/22

-- so, for the initial creation of the tables, use that first pay period only
-- the problem here is history of cost_center and pay_calc
-- talk to jeri about those histories, and if we record them then this query has to accomodate that
-- it will ddefinitely have an effect on the history, but going forward, we may be ok, just that it ccould be out of synch temporarily within a  pay period,
-- don't need dim_tech.tech_key, dim tech is used in flag hour query, ties to prs.employees with employee_number (cost center, pay_calc, ???)
-- need the pay period sequence, but now that is going to complicate the PK, possible multiple versions of tech in a pay period
-- will drive off that bridge when we get to it
-- going with the data from tmpben thru 3/9/22, this needs to be rewritten to handle just daily updates, starting with  march 1st should be ok to avoid
-- any changes, oh, wait, i am going to update based on pay period, so, need to start with 02/27
drop table if exists prs.employees cascade;
create table prs.employees as
select a.last_name, a.first_name, coalesce(i.team_name, 'Hourly') as team_name, a.employee_number, 
	f.tech_number, f.tech_key, c.store, c.department, c.cost_center, e.display_name as pay_calc,
	aa.the_date
from ukg.employees a
join dds.dim_date aa on aa.the_date between '01/02/2022' and '01/15/2022'
join ukg.ext_employee_details b on a.employee_id = b.employee_id
join (
	select * 
	from ukg.get_cost_center_hierarchy() a
	where department in ('body shop','main shop')
		and (
			cost_center like '%technician%'
			or
			cost_center like '%intern%')
	and store = 'Rydell GM') c on b.cost_center_id = c.id
join ukg.employee_profiles d on a.employee_id = d.employee_id		
join ukg.ext_pay_calc_profiles e on d.pay_calc_id = e.id  
  and e.display_name not in ('salary exempt')  -- exclude craig rogne
join dds.dim_tech f on a.employee_number = f.employee_number  -- limit to employees with a tech number
  and f.row_thru_date >= '01/02/2022'
  and f.row_from_date <= '01/15/2022'
  and f.active
left join tp.techs g on a.employee_number = g.employee_number
	and g.thru_date >= '01/02/2022'
	and g.from_date <= '01/15/2022'
left join tp.team_techs h on g.tech_key = h.tech_key
	and h.thru_date >= '01/02/2022'
	and h.from_date <= '01/15/2022'
left join tp.teams i on h.team_key = i.team_key
	and i.thru_date >= '01/02/2022'
	and i.from_date <= '01/15/2022'	   
where a.term_date >= '01/02/2022'
  and a.employee_number <> '147834'; -- exclude vonasek per John Gardner. only .5 flag hours since hired in 2020
alter table prs.employees add primary key (employee_number,the_date);
create index on prs.employees(employee_number);


-- need to add adjustments

    UPDATE tp.Data aa
    SET Tech_Flag_Hour_Adjustments_Day = x.Adj
    FROM (
      SELECT a.tech_number, a.last_name, a.the_date, coalesce(Adj, 0) AS Adj
      FROM tp.Data a
      LEFT JOIN (  
        select a.technician_id, dds.db2_integer_to_date(a.trans_date) as trans_Date, round(SUM(a.labor_hours), 2) AS adj
        FROM arkona.ext_sdpxtim a       
        jOIN tp.Techs b on a.technician_id = b.tech_number
        WHERE dds.db2_integer_to_date(a.trans_date) BETWEEN _from_date AND current_date
        GROUP BY a.technician_id, dds.db2_integer_to_date(a.trans_date)) b on a.tech_number = b.technician_id AND a.the_date = b.trans_date
      WHERE a.the_date BETWEEN _from_date AND current_date) x    
    WHERE aa.tech_number = x.tech_number
      AND aa.the_date = x.the_date;    


      

drop table if exists prs.gm_main_shop_flag_hours cascade;
create table prs.gm_main_shop_flag_hours as
select c.employee_number, a.flag_date as the_date, 
  coalesce(sum(flag_hours) filter (where a.opcode_key not in (22106,22107,22108))::numeric(8,2), 0) as flag_hours,
  coalesce(sum(flag_hours) filter (where a.opcode_key in (22106,22107,22108))::numeric(8,2), 0) as shop_time
from dds.fact_repair_order a
join dds.dim_tech b on a.tech_key = b.tech_key
join prs.employees c on b.employee_number = c.employee_number  
  and c.department = 'main shop'
  and c.the_date = a.flag_date
where a.flag_date between '01/02/2022' and '01/15/2022' 
group by  c.employee_number, a.flag_date;
alter table prs.gm_main_shop_flag_hours add primary key (employee_number, the_date);

drop table if exists prs.body_shop_flag_hours cascade;
create table prs.body_shop_flag_hours as
select c.employee_number, a.close_date as the_date, 
  coalesce(sum(flag_hours) filter (where a.opcode_key not in (22106,22107,22108))::numeric(8,2), 0) as flag_hours,
  coalesce(sum(flag_hours) filter (where a.opcode_key in (22106,22107,22108))::numeric(8,2), 0) as shop_time
from dds.fact_repair_order a
join dds.dim_tech b on a.tech_key = b.tech_key
join prs.employees c on b.employee_number = c.employee_number  
  and c.department = 'body shop'
  and c.the_date = a.close_date
where a.close_date between '01/02/2022' and '01/15/2022' 
group by  c.employee_number, a.close_date;
alter table prs.body_shop_flag_hours add primary key (employee_number, the_date);

drop table if exists prs.clock_hours cascade;
create table prs.clock_hours as
select a.the_date, a.employee_number, a.clock_hours::numeric(8,2)
from ukg.clock_hours a
join prs.employees b on a.employee_number = b.employee_number
where a.the_date between '01/02/2022' and '01/15/2022' 
  and a.clock_hours <> 0;
alter table prs.clock_hours add primary key (employee_number, the_date);  

select * from dds.dim_date where the_date = current_date

---------------------------------------------------------------------
-- 03/10/22  modify tmpben data to fit into prs.data, get 3 months history then switch over to prs.data nightly
-- just using this query to generate the table ddl
drop table if exists prs.data cascade;
create table prs.data as
select a.*, b.last_name, b.first_name, b.team_name, b.employee_number, b.tech_number, b.tech_key, b.store, b.department,
  b.cost_center, b.pay_calc, coalesce(c.flag_hours, 0) as flag_hours, coalesce(c.shop_time, 0) as shop_time, coalesce(d.clock_hours, 0) as clock_hours
-- select department, team_name, last_name, first_name, coalesce(c.flag_hours, 0) as flag_hours, coalesce(c.shop_time) as shop_time, coalesce(d.clock_hours) as clock_hours
from (
	select the_date, year_month, month_name, first_of_month,
		(select the_date from dds.dim_date where year_month = a.year_month and last_day_of_month) as last_of_month,
		sunday_to_saturday_week, 
		(select min(the_date) from dds.dim_date where sunday_to_saturday_week = a.sunday_to_saturday_week) as first_of_week,
		(select max(the_date) from dds.dim_date where sunday_to_saturday_week = a.sunday_to_saturday_week) as last_of_week,
		sunday_to_saturday_week_select_format,
		biweekly_pay_period_sequence, biweekly_pay_period_start_date, biweekly_pay_period_end_date
	from dds.dim_date a
	where a.the_date between '01/02/2022' and '01/15/2022') a
join prs.employees b on a.the_date = b.the_date
left join (
  select the_date, employee_number, sum(flag_hours) as flag_hours, sum(shop_time) as shop_time
  from (
		select *
		from prs.gm_main_shop_flag_hours 
		union
		select * 
		from prs.body_shop_flag_hours) aa
  group by the_date, employee_number) c on a.the_date = c.the_date
		and b.employee_number = c.employee_number
left join prs.clock_hours d on a.the_date = d.the_date
  and b.employee_number = d.employee_number		
where a.the_date between '01/02/2022' and '01/15/2022'  
order by department, team_name, last_name, a.the_date  


-- -- week query
-- 
-- select b.store, b.department, b.team_name, b.last_name, b.first_name, sum(coalesce(c.flag_hours, 0)) as flag_hours, sum(coalesce(c.shop_time, 0)) as shop_time, sum(coalesce(d.clock_hours, 0)) as clock_hours
-- from (
-- 	select the_date, year_month, month_name, first_of_month,
-- 		(select the_date from dds.dim_date where year_month = a.year_month and last_day_of_month) as last_of_month,
-- 		sunday_to_saturday_week, 
-- 		(select min(the_date) from dds.dim_date where sunday_to_saturday_week = a.sunday_to_saturday_week) as first_of_week,
-- 		(select max(the_date) from dds.dim_date where sunday_to_saturday_week = a.sunday_to_saturday_week) as last_of_week,
-- 		sunday_to_saturday_week_select_format,
-- 		biweekly_pay_period_sequence, biweekly_pay_period_start_date, biweekly_pay_period_end_date
-- 	from dds.dim_date a
-- 	where a.the_date between '01/02/2022' and '01/15/2022') a
-- join prs.employees b on a.the_date = b.the_date
-- left join (
--   select the_date, employee_number, sum(flag_hours) as flag_hours, sum(shop_time) as shop_time
--   from (
-- 		select *
-- 		from prs.gm_main_shop_flag_hours
-- 		where the_date between '01/02/2022' and '01/08/2022' 
-- 		union 
-- 		select * 
-- 		from prs.body_shop_flag_hours
-- 		where the_date between '01/02/2022' and '01/08/2022') aa
--   group by the_date, employee_number) c on a.the_date = c.the_date
-- 		and b.employee_number = c.employee_number
-- left join prs.clock_hours d on a.the_date = d.the_date
--   and b.employee_number = d.employee_number		
-- where a.the_date between '01/02/2022' and '01/08/2022'  
-- group by b.store, b.department, b.team_name, b.last_name, b.first_name
-- order by b.store, b.department, b.team_name, b.last_name
-- 
-- 
-- declare
--   _current_date date := (select current_date);
--   _day_in_pp integer := (
--     SELECT day_in_biweekly_pay_period
--     FROM dds.dim_date
--     WHERE the_date = current_date);  
--   _cur_pp_from_date date := (
--     select biweekly_pay_period_start_date
--     from dds.dim_date
--     where the_date = current_date);
--   _prev_pp_from_date date := (
--     select distinct biweekly_pay_period_start_date
--     from dds.dim_date
--     where biweekly_pay_period_sequence = (
--       select biweekly_pay_period_sequence - 1
--       from dds.dim_date
--       where the_date = _current_date));
--   _from_date date := (
--     select 
--       case
--         when _day_in_pp > 3 then _cur_pp_from_date
--         else _prev_pp_from_date
--       end);


----------------------------------------------------------------------------------
-- 03/09/22 going with history from tmpben
------------------------------------------------------------------------------------      


drop table if exists prs.tmpben cascade;
create table prs.tmpben (
      theDate Date,
      dayOfWeek Integer,
      monthOfYear Integer,
      monthName citext,
      holiday boolean,
      sundayToSaturdayWeek Integer,
      yearMonth Integer,
      weekStartDate Date,
      weekEndDate Date,
      storecode citext,
      techKey Integer,
      employeeKey Integer,
      employeeNumber citext,
      descName citext,
      techNumber citext,
      flagDeptCode citext,
      firstname citext,
      lastname citext,
      team citext,
      flagHours numeric(8,2),
      shopTime numeric(8,2),
      clockHours numeric(8,2),
      SundayToSaturdayWeekSelectFormat citext)
create index on prs.tmpben(employeenumber);

drop table if exists prs.xfm_tmpben cascade;
create table prs.xfm_tmpben as
select a.* 
from prs.tmpben a 
v
where a.thedate between '12/01/2021' and current_date
  and a.flagdeptcode not in ('RE', 'HS') 
  and a.storecode = 'RY1'
  and a.employeenumber not in ('NA','169420','1117960','1126040')
  and a.thedate <= b.term_date;
alter table prs.xfm_tmpben add primary key(thedate,employeenumber);  
-- create index on prs.xfm_tmpben  

select * from prs.tmpben order by thedate desc limit 500

drop table if exists prs.adjustments cascade;
create table prs.adjustments (
  tech_number citext not null,
  the_date date not null,
  adjustment numeric not null,
  primary key (tech_number, the_date));

insert into prs.adjustments
select a.technician_id, dds.db2_integer_to_date(a.trans_date) as trans_Date, round(SUM(a.labor_hours), 2) AS adj
FROM arkona.ext_sdpxtim a       
jOIN dds.dim_tech b on a.technician_id = b.tech_number
	and b.current_row
	and b.active
WHERE dds.db2_integer_to_date(a.trans_date) BETWEEN '12/01/2021' and '03/09/2022'
GROUP BY a.technician_id, dds.db2_integer_to_date(a.trans_date)
having sum(a.labor_hours) <> 0;

          
-- this populates prs.data with data from tmpben fleshed out with ukg tables to provide all the fields for prs.data
-- loaded on 3/9/22
delete from prs.data;
insert into prs.data
select a.*, c.last_name, c.first_name, b.team as team_name, c.employee_number, b.technumber, b.techkey, 
	e.store, e.department, e.cost_center, g.display_name as pay_calc, b.flaghours, coalesce(h.adjustment, 0), 
	b.flaghours + coalesce(h.adjustment, 0), b.shoptime, b.clockhours
-- select count(*) -- 4998
from (
	select the_date, year_month, month_name, first_of_month,
		(select the_date from dds.dim_date where year_month = a.year_month and last_day_of_month) as last_of_month,
		sunday_to_saturday_week, 
		(select min(the_date) from dds.dim_date where sunday_to_saturday_week = a.sunday_to_saturday_week) as first_of_week,
		(select max(the_date) from dds.dim_date where sunday_to_saturday_week = a.sunday_to_saturday_week) as last_of_week,
		sunday_to_saturday_week_select_format,
		biweekly_pay_period_sequence, biweekly_pay_period_start_date, biweekly_pay_period_end_date
	from dds.dim_date a
	where a.the_date between '12/01/2021' and '03/09/2022') a
join prs.xfm_tmpben b on a.the_date = b.thedate
join ukg.employees c on b.employeenumber = c.employee_number
join ukg.ext_employee_details d on c.employee_id = d.employee_id
join (
	select * 
	from ukg.get_cost_center_hierarchy() a
	where department in ('body shop','main shop')
		and (
			cost_center like '%technician%'
			or
			cost_center like '%intern%')
	and store = 'Rydell GM' ) e on d.cost_center_id = e.id
join ukg.employee_profiles f on c.employee_id = f.employee_id		
join ukg.ext_pay_calc_profiles g on f.pay_calc_id = g.id
left join prs.adjustments h on b.technumber = h.tech_number
  and a.the_date = h.the_date;

select * from prs.xfm_tmpben where thedate = '03/05/2022'

select * from prs.data limit 200

select department, sum(flag_hours), sum(shop_time), sum(clock_hours)
from prs.data
where the_date between '02/27/2022' and '03/05/2022'
group by department

select team_name, sum(clock_hours)
from prs.data
where the_date between '02/27/2022' and '03/05/2022'
  and department = 'body shop'
group by team_name--, last_name, first_name  
order by team_name, first_name


select department, sum(flag_hours), sum(shop_time), sum(clock_hours)
from prs.data
where year_month = 202202
group by department

select team_name, sum(flag_hours)
from prs.data
where the_date between '02/27/2022' and '03/05/2022'
  and department = 'body shop'
group by team_name--, last_name, first_name  
order by team_name, first_name  


arther bair and thomas sauls missing from prs.employees
should not be a problem, not using prs.employees for the initial load, then for nightly, major rework of prs.employe
select * 
from (
select distinct firstname, lastname, employeenumber from prs.xfm_tmpben where thedate = '03/09/2022') a
left join (
select distinct last_name, first_name, team_name, employee_number, 
	tech_number, tech_key, store, department, cost_center, pay_calc
from prs.employees) b on a.employeenumber = b.employee_number
where b.employee_number is null



	


------------------------------------------------------------------------
-- requirements
------------------------------------------------------------------------
ukg.employees
ukg.clock_hours
ext_arkona.py ExtSdpxtim