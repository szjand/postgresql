﻿select the_date, count(*) from prs_test.employees group by the_date order by the_date

select * 
from prs_test.data
where the_date = '10/04/2022'
order by the_date, team_name, last_name

select *
from dds.dim_opcode
where opcode_key in (22106,22107,22108,15559,27588,17935)
order by store_code, opcode_key


select *
from dds.dim_opcode
where description like '%traini%'
order by store_code, opcode_key

select * 
from prs_test.clock_hours 
where the_date = '10/04/2022' 
  and employee_number = '135770'

select * 
from prs.clock_hours 
where the_date = '10/04/2022' 

select * from prs_test.employees where employee_number = '135770' and the_date = '10/04/2022'

select * from prs_test.clock_hours where employee_number = '135770' and the_date = '10/04/2022'

select * from prs.clock_hours where employee_number = '135770' and the_date = '10/04/2022'

	select a.the_date, a.employee_number, a.clock_hours::numeric(8,2)
	from ukg.clock_hours a
	join prs_test.employees b on a.employee_number = b.employee_number
	  and a.the_date = b.the_date
	where a.the_date = '10/04/2022' 
	  and a.employee_number = '135770'

select tp.get_production_summary_by_month_v2(0,'Rydell GM')
-- monthly
gehrtz gowen v1 2 hour adj not in v2
gehrtz soberg v1 172 flag, v2 159.4
mcveigh david v1 138.9 flag, v2 138.4
schwan schwan v1 193.6 flag, v2 192.6
hourly duckstad v1 61.5 flag, v2 59.7

--  pay period
select tp.get_production_summary_by_pay_period_v2(0, 'Rydell GM')
right on

-- week
select tp.get_production_summary_by_week_v2(0, 'Rydell GM')
right on


select store, min(the_date)
from prs_test.employees 
group by store

select distinct store, the_date
from prs_test.employees
where store <> 'Rydell GM'
order by store, the_date

pay periods
9/11 - 9/25
08/28 - 9/10
08/14 - 08/27
07/31 - 08/13


select * from prs.adjustments limit 100

select * from prs_test.clock_hours where employee_number not like '1%' limit 100

select * from prs_test.flag_hours limit 100

select * from prs_test.data
where store <> 'Rydell GM'
  and the_date = '08/04/2022' 

select *
from prs.data
where store <> 'Rydell GM'  

select *   -- 14876  -- 1260
from prs_test.data
where store <> 'Rydell GM'

insert into prs.data
select *
from prs_test.data
where store <> 'Rydell GM';

select *
from prs_test.flag_hours a
where not exists (
  select 1
  from (
		select * 
		from prs.gm_main_shop_flag_hours
		union
		select *
		from prs.body_shop_flag_hours) aa
  where aa.the_date = a.the_date
    and aa.employee_number = a.employee_number)
order by the_date

------------------------------------------------------------------------
--< create, consolidate and update prs.flag_hours
------------------------------------------------------------------------
drop table if exists prs.flag_hours cascade;
CREATE TABLE prs.flag_hours
(
  employee_number citext NOT NULL,
  the_date date NOT NULL,
  flag_hours numeric,
  shop_time numeric,
  PRIMARY KEY (employee_number, the_date));	

insert into prs.flag_hours
select * 
from prs.gm_main_shop_flag_hours
union
select *
from prs.body_shop_flag_hours;

insert into prs.flag_hours 
select * 
from prs_test.flag_hours a
where not exists (
  select 1
  from prs.flag_hours
  where the_date = a.the_date
    and employee_number = a.employee_number);

select the_date, left(employee_number, 1), count(*)
from prs.flag_hours
group by the_date, left(employee_number, 1)
order by the_date

alter table prs.body_shop_flag_hours
rename to z_unused_body_shop_flag_hours;
alter table prs.gm_main_shop_flag_hours
rename to z_unused_gm_main_shop_flag_hours; 

------------------------------------------------------------------------
--/> create, consolidate and update prs.flag_hours
------------------------------------------------------------------------

------------------------------------------------------------------------
--< adjustments
------------------------------------------------------------------------
-- duh, just dawning on me, dealertrack accepts the same tech number accross stores
alter table prs.adjustments
rename to z_unused_adjustments;

drop table if exists prs.adjustments cascade;
create table prs.adjustments (
  store citext not null,
  tech_number citext not null,
  the_date date not null,
  adjustment numeric not null,
  primary key(store,tech_number, the_date));

-- the old/current data is unreliable because of the wrong PK (tech/date)
  
insert into prs.adjustments
select company_number, a.technician_id, dds.db2_integer_to_date(a.trans_date) as trans_Date, round(SUM(a.labor_hours), 2) AS adj
FROM arkona.ext_sdpxtim a       
jOIN dds.dim_tech b on a.technician_id = b.tech_number
	and b.current_row
	and b.active
WHERE dds.db2_integer_to_date(a.trans_date) BETWEEN '07/31/2022' and current_date
GROUP BY a.company_number, a.technician_id, dds.db2_integer_to_date(a.trans_date)
having sum(a.labor_hours) <> 0; 

------------------------------------------------------------------------
--/> adjustments
------------------------------------------------------------------------	

------------------------------------------------------------------------
--< clock_hours
------------------------------------------------------------------------
insert into prs.clock_hours
select * 
from z_unused_prs_test.clock_hours a
where not exists (
  select 1
  from prs.clock_hours
  where employee_number = a.employee_number
    and the_date = a.the_date);

------------------------------------------------------------------------
--/> clock_hours
------------------------------------------------------------------------
-- clean up the test schemas
alter schema prs_test
rename to z_unused_prs_test;

alter schema tp_test
rename to z_unused_tp_test;

alter table prs.tmpben
rename to z_unused_tmpben;
alter table prs.xfm_tmpben
rename to z_unused_xfm_tmpben;

alter function tp.get_production_summary_by_month(integer)
rename to z_unused_get_production_summary_by_month;
alter function tp.get_production_summary_by_pay_period(integer)
rename to z_unused_get_production_summary_by_pay_period;
alter function tp.get_production_summary_by_week(integer)
rename to z_unused_get_production_summary_by_week;

comment on function  prs.update_data() is 'called nightly from luigi:misc.py class UpdatePrsData(), purges and repopulates table prs.data
for current pay period';
comment on table prs.adjustments is 'refreshed nightly for the current pay period by function prs.update_data';
comment on table prs.clock_hours is 'refreshed nightly for the current pay period by function prs.update_data';
comment on table prs.employees is 'refreshed nightly for the current pay period by function prs.update_data';
comment on table prs.flag_hours is 'refreshed nightly for the current pay period by function prs.update_data';
comment on table prs.data is 'refreshed nightly for the current pay period by function prs.update_data, this table is used
  to populate vision page MAIN SHOP -> Production Summary via calls to functions tp.get_production_summary_by_month(integer, citext),
  tp.get_production_summary_by_pay_period(integer, citext), tp.get_production_summary_by_week(integer, citext)';

comment on function tp.get_production_summary_by_month(integer, citext) is 'populates monthly data on vision page MAIN SHOP -> Production Summary';
comment on function tp.get_production_summary_by_pay_period(integer, citext) is 'populates pay period data on vision page MAIN SHOP -> Production Summary';
comment on function tp.get_production_summary_by_week(integer, citext) is 'populates weekly data on vision page MAIN SHOP -> Production Summary';