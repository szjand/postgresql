﻿/*
3/1/22
took the parts i wanted from shop_production_1

*/
----------------------------------------------------------------------------------
-- break it up into separate prs. tables
-- update each table for the relevant pay period
-- final table prs.data will have tech data, date data, flag hours, shop time and clock hours

-- thru_date of team, tech, etc tables > the from_date of the relevant pay period
-- that will catch everyone employed for any part of the pay period
-- the first pay period will be 1/2 -> 1/15/22

-- so, for the initial creation of the tables, use that first pay period only
-- the problem here is history of cost_center and pay_calc
-- talk to jeri about those histories, and if we record them then this query has to accomodate that
-- it will ddefinitely have an effect on the history, but going forward, we may be ok, just that it ccould be out of synch temporarily within a  pay period,
-- don't need dim_tech.tech_key, dim tech is used in flag hour query, ties to prs.employees with employee_number (cost center, pay_calc, ???)
-- need the pay period sequence, but now that is going to complicate the PK, possible multiple versions of tech in a pay period
-- will drive off that bridge when we get to it
-- going with the data from tmpben thru 3/9/22, this needs to be rewritten to handle just daily updates, starting with  march 1st should be ok to avoid
-- any changes, oh, wait, i am going to update based on pay period, so, need to start with 02/27
drop table if exists prs.employees cascade;
create table prs.employees as
select a.last_name, a.first_name, coalesce(i.team_name, 'Hourly') as team_name, a.employee_number, 
	f.tech_number, f.tech_key, c.store, c.department, c.cost_center, e.display_name as pay_calc,
	aa.the_date
from ukg.employees a
join dds.dim_date aa on aa.the_date between '01/02/2022' and '01/15/2022'
join ukg.ext_employee_details b on a.employee_id = b.employee_id
join (
	select * 
	from ukg.get_cost_center_hierarchy() a
	where department in ('body shop','main shop')
		and (
			cost_center like '%technician%'
			or
			cost_center like '%intern%')
	and store = 'Rydell GM') c on b.cost_center_id = c.id
join ukg.employee_profiles d on a.employee_id = d.employee_id		
join ukg.ext_pay_calc_profiles e on d.pay_calc_id = e.id  
  and e.display_name not in ('salary exempt')  -- exclude craig rogne
join dds.dim_tech f on a.employee_number = f.employee_number  -- limit to employees with a tech number
  and f.row_thru_date >= '01/02/2022'
  and f.row_from_date <= '01/15/2022'
  and f.active
left join tp.techs g on a.employee_number = g.employee_number
	and g.thru_date >= '01/02/2022'
	and g.from_date <= '01/15/2022'
left join tp.team_techs h on g.tech_key = h.tech_key
	and h.thru_date >= '01/02/2022'
	and h.from_date <= '01/15/2022'
left join tp.teams i on h.team_key = i.team_key
	and i.thru_date >= '01/02/2022'
	and i.from_date <= '01/15/2022'	   
where a.term_date >= '01/02/2022'
  and a.employee_number <> '147834'; -- exclude vonasek per John Gardner. only .5 flag hours since hired in 2020
alter table prs.employees add primary key (employee_number,the_date);
create index on prs.employees(employee_number);



select distinct the_date from prs.employees order by the_date

  -- should prs.employees be updated nightly, so, what if someone is termed in the middle of the pay period, if, during the pay period
  -- never mind, term can be handled as populating based on anyone employed at anytime during the pay period
  -- if cost center or pay calc change during the pay period, so be it, the old one gets overwritten
  -- for the forseeable future, i am in control of the team structure, and see to it that those changes only ever happen on pay period start/end
  -- so i don't need all the date range stuff on the team pay tables
  -- tech key: don't need it in this table, will get it from dim_tech in the join to fact repair order, likewise don't need
  --		tech number for production pages
  -- i am iffy on even needing the date in prs.employees, maybe all i need is pay period seq because that grain at which i am 
  --    populating the table, but what about when i am updating data for 2 pay periods, on sunday and mo nday of the new pay period
  --		when do i actually need dates in prs.employees (look at xfmtmpben)
  -- maybe, instead of deleting prs.employees every night, just update it for the current pay period, fuck including the date precludes that option i think
  -- fuck it just do the date every day
  
drop table if exists prs.employees cascade;
create table prs.employees as
select distinct aa.the_date, a.last_name, a.first_name, coalesce(i.team_name, 'Hourly') as team_name, a.employee_number, 
	f.tech_number, /*f.tech_key, */c.store, c.department, c.cost_center, e.display_name as pay_calc
-- 	aa.the_date
from ukg.employees a
join dds.dim_date aa on aa.the_date between '02/27/2022' and '03/12/2022'
join ukg.ext_employee_details b on a.employee_id = b.employee_id
join (
	select * 
	from ukg.get_cost_center_hierarchy() a
	where department in ('body shop','main shop')
		and (
			cost_center like '%technician%'
			or
			cost_center like '%intern%')
	and store = 'Rydell GM') c on b.cost_center_id = c.id
join ukg.employee_profiles d on a.employee_id = d.employee_id		
join ukg.ext_pay_calc_profiles e on d.pay_calc_id = e.id  
  and e.display_name not in ('salary exempt')  -- exclude craig rogne
join dds.dim_tech f on a.employee_number = f.employee_number  -- limit to employees with a tech number
  and f.row_thru_date >= '02/27/2022'
--   and f.row_from_date <= '01/15/2022'
  and f.active
left join tp.techs g on a.employee_number = g.employee_number
  and g.thru_date > current_date
-- 	and g.thru_date >= '01/02/2022'
-- 	and g.from_date <= '01/15/2022'
left join tp.team_techs h on g.tech_key = h.tech_key
  and h.thru_date > current_date
-- 	and h.thru_date >= '01/02/2022'
-- 	and h.from_date <= '01/15/2022'
left join tp.teams i on h.team_key = i.team_key
  and i.thru_date > current_date
-- 	and i.thru_date >= '01/02/2022'
-- 	and i.from_date <= '01/15/2022'	   
where a.term_date >= '02/27/2022'
  and a.employee_number <> '147834'; -- exclude vonasek per John Gardner. only .5 flag hours since hired in 2020
alter table prs.employees add primary key (employee_number,the_date);
create index on prs.employees(employee_number);


do $$
declare
  -- this will be run affter midnight daily, therefore the date we basing it on is "yesterday"
  -- update tables nightly throughout the pay period

  _current_date date := '03/12/2022'; -- (select current_date - 1);
  _day_in_pp integer := (
		-- this is to accomodate adjustments ken often makes on sunday at the end of the pay period to account for
		-- what was done on saturday, the last day of the pay period, eg, axtman team needs hours to meet their guarantee
		-- mimics what i do in the tpdata updates
    SELECT day_in_biweekly_pay_period
    FROM dds.dim_date
    WHERE the_date = current_date);  
  _cur_pp_from_date date := (
    select biweekly_pay_period_start_date
    from dds.dim_date
    where the_date = current_date);
  _prev_pp_from_date date := (
    select distinct biweekly_pay_period_start_date
    from dds.dim_date
    where biweekly_pay_period_sequence = (
      select biweekly_pay_period_sequence - 1
      from dds.dim_date
      where the_date = _current_date));
  -- this says no adjustments allowed after monday
  _from_date date := (
    select 
      case
        when _day_in_pp > 2 then _cur_pp_from_date
        else _prev_pp_from_date
      end);
  -- this says we are always updating the current pay period as well as sometimes the previous pay period
  _thru_date date := (
    select biweekly_pay_period_end_date
    from dds.dim_date
    where the_date = current_date);    
begin

  drop table if exists wtf;
  create temp table wtf as
  select _current_date, _day_in_pp, _cur_pp_from_date, _prev_pp_from_date, _from_date, _thru_date;  

--   only refresh prs.employees for the current_pay period
  delete 
  from prs.employees
  where the_date between _cur_pp_from_date and _thru_date;

	insert into prs.employees
	select aa.the_date, a.last_name, a.first_name, coalesce(i.team_name, 'Hourly') as team_name, a.employee_number, 
		f.tech_number, c.store, c.department, c.cost_center, e.display_name as pay_calc
	from ukg.employees a
-- 	join (
-- 	  select employee_number, tech_number
-- 	  from dds.dim_tech
-- 	    where employee_number <> 'NA'
-- 	  group by employee_number, tech_number) bb on a.employee_number = bb.employee_number
	join dds.dim_date aa on aa.the_date between _cur_pp_from_date and _thru_date
	join ukg.ext_employee_details b on a.employee_id = b.employee_id
	join (
		select * 
		from ukg.get_cost_center_hierarchy() a
		where department in ('body shop','main shop')
			and (
				cost_center like '%technician%'
				or
				cost_center like '%intern%')
		and store = 'Rydell GM') c on b.cost_center_id = c.id
	join ukg.employee_profiles d on a.employee_id = d.employee_id		
	join ukg.ext_pay_calc_profiles e on d.pay_calc_id = e.id  
		and e.display_name not in ('salary exempt')  -- exclude craig rogne
	join dds.dim_tech f on a.employee_number = f.employee_number  -- limit to employees with a tech number
		and f.row_thru_date > _cur_pp_from_date
		and f.active		
	left join tp.techs g on a.employee_number = g.employee_number
		and g.thru_date > _cur_pp_from_date
	left join tp.team_techs h on g.tech_key = h.tech_key
		and h.thru_date > _cur_pp_from_date
	left join tp.teams i on h.team_key = i.team_key
		and i.thru_date > _cur_pp_from_date  
	where a.term_date >= _cur_pp_from_date
		and a.employee_number <> '147834'; -- exclude vonasek per John Gardner. only .5 flag hours since hired in 2020  

  delete 
  from prs.gm_main_shop_flag_hours
  where the_date between _cur_pp_from_date and _thru_date;  
  
  insert into prs.gm_main_shop_flag_hours
	select c.employee_number, a.flag_date as the_date, 
		coalesce(sum(flag_hours) filter (where a.opcode_key not in (22106,22107,22108))::numeric(8,2), 0) as flag_hours,
		coalesce(sum(flag_hours) filter (where a.opcode_key in (22106,22107,22108))::numeric(8,2), 0) as shop_time
	from dds.fact_repair_order a
	join dds.dim_tech b on a.tech_key = b.tech_key
	join prs.employees c on b.employee_number = c.employee_number  
		and c.department = 'main shop'
		and c.the_date = a.flag_date
	where a.flag_date between _cur_pp_from_date and _thru_date
	group by  c.employee_number, a.flag_date;

  delete 
  from prs.body_shop_flag_hours
  where the_date between _cur_pp_from_date and _thru_date;

  insert into prs.body_shop_flag_hours
	select c.employee_number, a.close_date as the_date, 
		coalesce(sum(flag_hours) filter (where a.opcode_key not in (22106,22107,22108))::numeric(8,2), 0) as flag_hours,
		coalesce(sum(flag_hours) filter (where a.opcode_key in (22106,22107,22108))::numeric(8,2), 0) as shop_time
	from dds.fact_repair_order a
	join dds.dim_tech b on a.tech_key = b.tech_key
	join prs.employees c on b.employee_number = c.employee_number  
		and c.department = 'body shop'
		and c.the_date = a.close_date
	where a.close_date between _cur_pp_from_date and _thru_date
	group by  c.employee_number, a.close_date;
  
	delete 
	from prs.adjustments
	where the_date between _cur_pp_from_date and _thru_date;
	
	insert into prs.adjustments
	select a.technician_id, dds.db2_integer_to_date(a.trans_date) as trans_Date, round(SUM(a.labor_hours), 2) AS adj
	FROM arkona.ext_sdpxtim a       
	jOIN dds.dim_tech b on a.technician_id = b.tech_number
		and b.current_row
		and b.active
	WHERE dds.db2_integer_to_date(a.trans_date) BETWEEN _cur_pp_from_date and _thru_date
	GROUP BY a.technician_id, dds.db2_integer_to_date(a.trans_date)
	having sum(a.labor_hours) <> 0;

  delete
  from prs.clock_hours
  where the_date between _cur_pp_from_date and _thru_date;

  insert into prs.clock_hours
	select a.the_date, a.employee_number, a.clock_hours::numeric(8,2)
	from ukg.clock_hours a
	join prs.employees b on a.employee_number = b.employee_number
	  and a.the_date = b.the_date
	where a.the_date between _cur_pp_from_date and _thru_date
		and a.clock_hours <> 0;  

  delete 
  from prs.data
  where the_date between _cur_pp_from_date and _thru_date;

  insert into prs.data
-- 	select a.*, b.last_name, b.first_name, b.team_name, b.employee_number, b.tech_number, 0 as tech_key, b.store, b.department,
-- 		b.cost_center, b.pay_calc, coalesce(c.flag_hours, 0) as flag_hours, coalesce(e.adjustment, 0) as adjustment, 
-- 		coalesce(c.flag_hours) + coalesce(e.adjustment, 0) as total_flag, coalesce(c.shop_time, 0) as shop_time, coalesce(d.clock_hours, 0) as clock_hours
-- 	-- select a.the_date, department, team_name, last_name, first_name, coalesce(c.flag_hours, 0) as flag_hours, coalesce(e.adjustment, 0) as adj,coalesce(c.flag_hours, 0) +  coalesce(e.adjustment, 0) as total_flag, coalesce(c.shop_time, 0) as shop_time, coalesce(d.clock_hours, 0) as clock_hours
  select a.the_date,year_month,month_name,first_of_month,last_of_month,sunday_to_saturday_week,first_of_week,last_of_week,
		sunday_to_saturday_week_select_format,biweekly_pay_period_sequence,biweekly_pay_period_start_date,
		biweekly_pay_period_end_date,last_name,first_name,team_name,b.employee_number,b.tech_number,store,department,
		cost_center,pay_calc,coalesce(flag_hours,0),coalesce(adjustment,0),
		coalesce(flag_hours,0) + coalesce(adjustment,0) as total_flag_hours,coalesce(shop_time,0),coalesce(clock_hours,0)
	from (
		select the_date, year_month, month_name, first_of_month,
			(select the_date from dds.dim_date where year_month = a.year_month and last_day_of_month) as last_of_month,
			sunday_to_saturday_week, 
			(select min(the_date) from dds.dim_date where sunday_to_saturday_week = a.sunday_to_saturday_week) as first_of_week,
			(select max(the_date) from dds.dim_date where sunday_to_saturday_week = a.sunday_to_saturday_week) as last_of_week,
			sunday_to_saturday_week_select_format,
			biweekly_pay_period_sequence, biweekly_pay_period_start_date, biweekly_pay_period_end_date
		from dds.dim_date a
		where a.the_date between _cur_pp_from_date and _thru_date) a
	join prs.employees b on a.the_date = b.the_date
	left join (
		select the_date, employee_number, sum(flag_hours) as flag_hours, sum(shop_time) as shop_time
		from (
			select *
			from prs.gm_main_shop_flag_hours 
			union
			select * 
			from prs.body_shop_flag_hours) aa
		group by the_date, employee_number) c on a.the_date = c.the_date
			and b.employee_number = c.employee_number
	left join prs.adjustments e on a.the_date = e.the_date
	  and b.tech_number = e.tech_number
	left join prs.clock_hours d on a.the_date = d.the_date
		and b.employee_number = d.employee_number		
	where a.the_date between _cur_pp_from_date and _thru_date;

end $$;

select * from wtf

select coalesce(flag_hours, 0) 
from prs.gm_main_shop_flag_hours
limit 100

select coalesce(adjustment, 0) 
from prs.adjustments
limit 100

select * from prs.gm_main_shop_flag_hours where the_date = '03/05/2022'

select *, sum(flag) over (partition by team_name) as flag, sum(shop) over (partition by team_name) as shop
from (
select team_name, first_name, last_name, sum(flag_hours) as flag, sum(shop_time) as shop
from (
select a.team_name, a.first_name, a.last_name, b.*
from prs.employees a
join prs.gm_main_shop_flag_hours b on a.the_date = b.the_date
  and a.employee_number = b.employee_number
where a.the_date between '03/06/2022' and '03/12/2022') c
group by team_name, first_name, last_name) d
order by team_name, first_name, last_name