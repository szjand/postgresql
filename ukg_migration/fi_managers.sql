﻿the current process  all manual
1 \month_end\fi_mgr_comp\fi_manager_comp_for_kim.sql
		execute the function sls.update_fi_manager_comp() once for each fi mgr
		run a scrip that generates the file for kim
2. fi_mgr_comp.py: run manually creates a file with a list of the deals for each manager
				which is attached to an email, the body of which is a breakdown of the comp for the month
for november
  aubol breakdown:
			fi gross:        6070.23
			chargebacks:  +  -192.73
			draw:         -   500.00
			pto:         +   3906.00
			total:           9283.50

  grollimund breakdown:
    	fi gross:    8374.76
			chargebacks: -192.73
			draw:        -500.00
			pto:            0.00
			total:       7730.32

fi_sales	fi_cost	acq_fee	fi_gross chargeback	draw percentage	fi_comp

drop table if exists sls.fi_managers cascade;
create table sls.fi_managers (
  store citext not null,
  employee_number citext not null,
  name citext not null,
  percentage numeric not null,
  from_date date not null,
  thru_date date not null default '12/31/9999',
  primary key (employee_number, thru_date));

 insert into sls.fi_managers values
 ('GM', '17534','Tom Aubol',0.14, '01/01/2021', '12/31/9999'),
 ('HN', '254327','Tyler Grollimund',0.12, '11/01/2021', '12/31/9999');

create or replace function sls.get_fi_managers_payroll_approval_data(_year_month integer, _store citext)
  returns table(store citext, name citext, employee_number citext, percentage numeric,
			fi_gross numeric, chargebacks numeric, draw numeric, fi_comp numeric) as
$BODY$
/*
  select * from sls.get_fi_managers_payroll_approval_data(202201, 'GM')
*/
	select b.store, b.name, a.employee_number, b.percentage, a.fi_gross, a.chargebacks, a.draw, 
		a.fi_gross + a.chargebacks - a.draw as fi_comp
	from sls.fi_manager_comp a
	join sls.fi_managers b on a.employee_number = b.employee_number
		and thru_date > current_date
	where a.year_month = _year_month
	  and b.store = _store;
$BODY$
language sql;	  


		select b.employee_number, a.fi_gross + a.chargebacks + a.draw as fi_comp,
		  fi_gross, chargebacks, draw,
			arkona.db2_integer_to_date(c.hire_date) as hire_date, 
			arkona.db2_integer_to_date(c.termination_date) as term_date
		from sls.fi_manager_comp a
		join sls.fi_managers b on a.employee_number = b.employee_number
			and thru_date > current_date
		join arkona.ext_pymast c on b.employee_number = c.pymast_employee_number
		where a.year_month = 202201
			and b.store = 'hn'

			

CREATE OR REPLACE FUNCTION sls.create_fi_managers_payroll_import_file(IN _year_month integer, _store citext)
  RETURNS TABLE("Company Short Name" text, "Employee Id" citext, "EIN Name" text, "Payroll Name" text, 
  "Payroll Pay Date" text, "Payroll Type" text, "Pay Statement Type" text, "Start Date" text, 
  "End Date" text, "E/D/T" text, "E/D/T Name" text, "E/D/T Amount" numeric) AS
$BODY$
/*
	select * from sls.create_fi_managers_payroll_import_file(202111, 'GM') 
*/	

declare
  _year_month integer := _year_month;
	_payroll_pay_date date := (
	  select first_day_of_next_month
	  from sls.months
	  where year_month = _year_month);
  _first_of_month date := (
    select first_of_month
    from sls.months
    where year_month = _year_month);	  
  _last_of_month date := (
    select last_of_month
    from sls.months
    where year_month = _year_month);	    
begin 
	return QUERY
	select '6175637'::text, employee_number, 		
		case
			when _store = 'GM' then 'Rydell Auto Center, Inc.'
			when _store = 'HN' then 'H.G.F., Inc.'
		end,
		'RAC Semi-Monthly Regular ' || to_char(_payroll_pay_date, 'mm/dd/yyyy') as payroll_name,
		to_char(_payroll_pay_date, 'mm/dd/yyyy'), 'Regular'::text, 'Regular'::text, 
		(select to_char(start_date, 'mm/dd/yyyy') from ukg.get_payroll_start_end_dates(_year_month, a.hire_date, a.term_date)),
		(select to_char(end_date, 'mm/dd/yyyy') from ukg.get_payroll_start_end_dates(_year_month, a.hire_date, a.term_date)),
		'E'::text, 'F&I Commission'::text, fi_comp::numeric
	from (
		select b.employee_number, a.fi_gross + a.chargebacks - a.draw as fi_comp,
			arkona.db2_integer_to_date(c.hire_date) as hire_date, 
			arkona.db2_integer_to_date(c.termination_date) as term_date
		from sls.fi_manager_comp a
		join sls.fi_managers b on a.employee_number = b.employee_number
			and thru_date > current_date
		join arkona.ext_pymast c on b.employee_number = c.pymast_employee_number
		where a.year_month = _year_month
			and b.store = _store) a;	  	
end
$BODY$
  LANGUAGE plpgsql;


-- draw, this is what i am using in sls.update_consultant_payroll()

			select c.employee_number, d.ee_amount as draw, 
				(select year_month from dds.dim_date where the_date = a.payroll_end_date) as year_month
			from ukg.payrolls a
			join ukg.pay_statements b on a.payroll_id = b.payroll_id
			join ukg.employees c on b.employee_account_id = c.employee_id
			join ukg.pay_statement_earnings d on a.payroll_id = d.payroll_id
				and b.pay_statement_id = d.pay_statement_id
				and d.earning_code in ('f&i commission','sales volume commission')
				and d.type_name = 'Earning (Auto)'
			where a.payroll_name like '%semi-monthly regular%'





			select c.employee_number, d.earning_code, d.ee_amount as draw, 
				(select year_month from dds.dim_date where the_date = a.payroll_end_date) as year_month
-- select d.*				
			from ukg.payrolls a
			join ukg.pay_statements b on a.payroll_id = b.payroll_id
			join ukg.employees c on b.employee_account_id = c.employee_id
			  and c.employee_number in ('17534','254327')
			join ukg.pay_statement_earnings d on a.payroll_id = d.payroll_id
				and b.pay_statement_id = d.pay_statement_id
				and d.earning_code in( 'f&i commission','sales volume commission')
				and d.type_name = 'Earning (Auto)'
			where a.payroll_name like '%semi-monthly regular%'

select * from ukg.employees where employee_number = '17534'
select * 
from ukg.pay_statements
where employee_account_id = 12987528148

--------------------------------------------------------------
--< 02/05/22
--------------------------------------------------------------
1. chargebacks do not make sense :: because i was overlooking the fact the the total chargebacks is multipled by the percentage !!!

oh fuck
looks like for executive consultants the chargebacks are not multiplied by the percentage
but for fi managers they are
fuck menot true, it is done the same in both, just in different places

2. fuck, i left out draw

FUNCTION sls.update_fi_manager_comp(
    _year_month integer,
    _employee_number citext,
    _percentage numeric)
currently generates draw:    
    select a.draw
    from sls.paid_by_month a
    join arkona.xfm_pymast b on a.employee_number = b.pymast_employee_number
      and b.current_row
    where a.year_month = _year_month
      and a.employee_number = _employee_number

select * from sls.paid_by_month      

			select c.employee_number, d.earning_code, d.ee_amount as draw, 
				(select year_month from dds.dim_date where the_date = a.payroll_end_date) as year_month
-- select d.*			
			from ukg.payrolls a
			join ukg.pay_statements b on a.payroll_id = b.payroll_id
			join ukg.employees c on b.employee_account_id = c.employee_id
			  and c.employee_number in ('17534','254327')
			join ukg.pay_statement_earnings d on a.payroll_id = d.payroll_id
				and b.pay_statement_id = d.pay_statement_id
-- 				and d.earning_code in( 'f&i commission','sales volume commission')
-- 				and d.type_name = 'Earning (Auto)'
-- 			where a.payroll_name like '%semi-monthly regular%'
--------------------------------------------------------------
--/> 02/05/22
--------------------------------------------------------------

-- select * from sls.deals_by_month limit 100
-- select * from sls.fi_chargebacks limit 100
-- 
-- -- these queries based on FUNCTION sls.update_fi_manager_comp(
--     select aa.store, aa.name, aa.employee_number, sum(b.fi_sales) as sales,
-- 			sum(b.fi_cogs) as cogs, sum(b.fi_acq_fee) as acq_fee, sum(fi_sales - fi_cogs - fi_acq_fee) as fi_gross,  0 as chargeback
--     from sls.deals_by_month a
--     join sls.fi_managers aa on a.fi_employee_number = aa.employee_number
--       and aa.thru_date > current_date
--     left join sls.deals_gross_by_month b on a.stock_number = b.control
--     where a.year_month = 202111
--     group by aa.store, aa.name, aa.employee_number
--     having sum(fi_sales - fi_cogs - fi_acq_fee) <> 0
-- 
-- 
-- select s.store, s.name, s.employee_number, s.percentage, s.sales, s.cogs, s.acq_fee, t.chargeback, u.draw,
--   s.percentage * (s.sales - s.cogs - s.acq_fee) + s.percentage * t.chargeback - u.draw as fi_comp
--   from ( -- fi_gross
--     select aa.store, aa.name, aa.employee_number, aa.percentage, sum(b.fi_sales) as sales,
-- 			sum(b.fi_cogs) as cogs, sum(b.fi_acq_fee) as acq_fee, sum(fi_sales - fi_cogs - fi_acq_fee) as fi_gross
--     from sls.deals_by_month a
--     join sls.fi_managers aa on a.fi_employee_number = aa.employee_number
--       and aa.thru_date > current_date
--     left join sls.deals_gross_by_month b on a.stock_number = b.control
--     where a.year_month = 202111
--     group by aa.store, aa.name, aa.employee_number, aa.percentage
--     having sum(fi_sales - fi_cogs - fi_acq_fee) <> 0) s
--   left join ( -- chargebacks  theese numbers are fucked up
--     select aa.employee_number, sum(a.amount) as chargeback --round(aa.percentage * sum(a.amount), 2) as chargeback
--     from sls.fi_chargebacks a
--     join sls.fi_managers aa on a.fi_employee_number = aa.employee_number
--       and aa.thru_date > current_date
--     join sls.deals_by_month b on a.stock_number = b.stock_number
--       and aa.employee_number = b.fi_employee_number
--     where a.chargeback_year_month = 202111
--       -- chargeback within 6 months of delivery date  
--       and '11/01/2021' - a.delivery_date < 180
--     group by aa.employee_number, aa.percentage) t on s.employee_number = t.employee_number
--   left join ( -- draw
--     select a.employee_number, a.draw
--     from sls.paid_by_month a
--     join sls.fi_managers aa on a.employee_number = aa.employee_number
--     where a.year_month = 202111) u on s.employee_number = u.employee_number

-- 
-- select a.*, '11/01/2021' - a.delivery_date < 180, a.amount, aa.percentage
-- -- select aa.employee_number, round(aa.percentage * sum(a.amount), 2) as chargeback
-- from sls.fi_chargebacks a
-- join sls.fi_managers aa on a.fi_employee_number = aa.employee_number
-- 	and aa.thru_date > current_date
-- join sls.deals_by_month b on a.stock_number = b.stock_number
-- 	and aa.employee_number = b.fi_employee_number
-- where chargeback_year_month = 202111
-- --   and a.fi_employee_number = '254327'
--   and '11/01/2021' - a.delivery_date < 180
-- group by aa.employee_number, aa.percentage  
-- 
-- 
--     select aa.employee_number, round(aa.percentage * sum(a.amount), 2) as chargeback
--     from sls.fi_chargebacks a
--     join sls.fi_managers aa on a.fi_employee_number = aa.employee_number
--       and aa.thru_date > current_date
--     join sls.deals_by_month b on a.stock_number = b.stock_number
--       and aa.employee_number = b.fi_employee_number
--     where a.chargeback_year_month = 202111
--       -- chargeback within 6 months of delivery date  
--       and '11/01/2021' - a.delivery_date < 180
--     group by aa.employee_number, aa.percentage  