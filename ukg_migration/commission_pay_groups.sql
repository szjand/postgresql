﻿/*
to accommodate jeri's request for vision pages where managers can enter commission pay for those pay plans
not yet implemented in vision
12/25/21
	dropped the job_category and job_name attributes, not required for the page, api is a pia
*/

drop table if exists ukg.commission_pay_groups cascade;
create table ukg.commission_pay_groups (
  pay_group citext not null,
  store citext not null,
  employee citext not null,
  employee_id bigint not null,
  employee_number citext,
  cost_center citext not null,
--   job_category citext not null,
--   job_name citext not null,
  term_date date not null);
create unique index on ukg.commission_pay_groups(employee);
create unique index on ukg.commission_pay_groups(employee_id);
create unique index on ukg.commission_pay_groups(employee_number);
comment on table ukg.commission_pay_groups is 'pay groups defined by jeri for implementing a vision page for managers to input commission pay for those pay plans not yet implented in vision';

-- select * from ukg.commission_pay_groups


CREATE OR REPLACE FUNCTION ukg.update_commission_pay_groups()
  RETURNS void AS
$BODY$
/*
12/25/21
	removed job_category, job_name and joins on ukg.ext_employee_pay_info & ukg.ext_cost_center_job_details
	originally thought this might be information of interest
	turns out api is a pia, confusing "too many calls" errors
	
select * from ukg.commission_pay_groups

select ukg.update_commission_pay_groups();
*/
	truncate ukg.commission_pay_groups;
	insert into ukg.commission_pay_groups
		select * 
		from (
		-- For GM Sales BDC: 
		select 'GM Sales BDC' as pay_group, a.store, a.last_name || ', ' || a.first_name  as employee,
			a.employee_id, a.employee_number,  a.cost_center, /*c.job_category, c.job_name,*/ coalesce(a.term_date, '12/31/9999')
		from ukg.employees a
-- 		left join ukg.ext_employee_pay_info b on a.employee_id = b.employee_id
-- 		left join ukg.ext_cost_center_job_details c on b.default_job_id = c.id
		where cost_center = 'Customer Care Sales Specialist'
			and store = 'GM'
			and a.status = 'active'
		union  
		-- For HN Sales BDC: 
		select 'HN Sales BDC' as pay_group,a.store, a.last_name || ', ' || a.first_name,
			a.employee_id, a.employee_number,  a.cost_center, /*c.job_category, c.job_name,*/ coalesce(a.term_date, '12/31/9999')
		from ukg.employees a
-- 		left join ukg.ext_employee_pay_info b on a.employee_id = b.employee_id
-- 		left join ukg.ext_cost_center_job_details c on b.default_job_id = c.id
		where cost_center = 'Customer Care Sales Specialist'
			and store = 'HN'
			and a.status = 'active'
		union
		-- For Service BDC:  
		select 'Service BDC' as pay_group,a.store, a.last_name || ', ' || a.first_name,
			a.employee_id, a.employee_number,  a.cost_center, /*c.job_category, c.job_name,*/ coalesce(a.term_date, '12/31/9999')
		from ukg.employees a
-- 		left join ukg.ext_employee_pay_info b on a.employee_id = b.employee_id
-- 		left join ukg.ext_cost_center_job_details c on b.default_job_id = c.id
		where cost_center = 'Customer Care Service Specialist'
			and store = 'GM'
			and a.status = 'active'
		union
		-- For Car Wash:
		select 'Car Wash' as pay_group,a.store, a.last_name || ', ' || a.first_name, 
			a.employee_id, a.employee_number,  a.cost_center, /*c.job_category, c.job_name,*/ coalesce(a.term_date, '12/31/9999')
		from ukg.employees a
-- 		left join ukg.ext_employee_pay_info b on a.employee_id = b.employee_id
-- 		left join ukg.ext_cost_center_job_details c on b.default_job_id = c.id
		-- where cost_Center like '%car wash%'
		where cost_center in ('Car Wash','Car Wash Assistant Manager','Car Wash Maintenance Team Member','Car Wash Maintenance Technician','Car Wash Manager','Car Wash Team Member (Gateway)','Car Wash Team Member (South Washington)')
			and a.status = 'active'
		group by a.store, a.last_name || ', ' || a.first_name, 
			a.employee_id, a.employee_number,  a.cost_center, /*c.job_category, c.job_name,*/ coalesce(a.term_date, '12/31/9999')
		-- order by cost_center
		union
		-- For GM Detail
		select 'GM Detail' as pay_group,a.store, a.last_name || ', ' || a.first_name,
			a.employee_id, a.employee_number,  a.cost_center, /*c.job_category, c.job_name,*/ coalesce(a.term_date, '12/31/9999')
		from ukg.employees a
-- 		left join ukg.ext_employee_pay_info b on a.employee_id = b.employee_id
-- 		left join ukg.ext_cost_center_job_details c on b.default_job_id = c.id
		where cost_center = 'Detail Technician'
			and a.status = 'active'
			and store = 'GM'
		union
		-- For HN Detail  
		select 'HN Detail' as pay_group,a.store, a.last_name || ', ' || a.first_name,
			a.employee_id, a.employee_number,  a.cost_center, /*c.job_category, c.job_name,*/ coalesce(a.term_date, '12/31/9999')
		from ukg.employees a
-- 		left join ukg.ext_employee_pay_info b on a.employee_id = b.employee_id
-- 		left join ukg.ext_cost_center_job_details c on b.default_job_id = c.id
		where cost_center = 'Detail Technician'
			and a.status = 'active'
			and store = 'HN'
		union
		-- For GM PDQ Writers 
		select 'GM PDQ Writers' as pay_group,a.store, a.last_name || ', ' || a.first_name,
			a.employee_id, a.employee_number,  a.cost_center, /*c.job_category, c.job_name,*/ coalesce(a.term_date, '12/31/9999')
		from ukg.employees a
-- 		left join ukg.ext_employee_pay_info b on a.employee_id = b.employee_id
-- 		left join ukg.ext_cost_center_job_details c on b.default_job_id = c.id
		where cost_center = 'Express Service Representative'
			and a.status = 'active'
			and store = 'GM'
		union
		-- For HN PDQ Writers 
		select 'HN PDQ Writers' as pay_group,a.store, a.last_name || ', ' || a.first_name,
			a.employee_id, a.employee_number,  a.cost_center, /*c.job_category, c.job_name,*/ coalesce(a.term_date, '12/31/9999')
		from ukg.employees a
-- 		left join ukg.ext_employee_pay_info b on a.employee_id = b.employee_id
-- 		left join ukg.ext_cost_center_job_details c on b.default_job_id = c.id
		where cost_center = 'Express Service Representative'
			and a.status = 'active'
			and store = 'HN'  
		union
		 -- For BDC Managers 
		 select 'BDC Managers' as pay_group, a.store, a.last_name || ', ' || a.first_name,
			a.employee_id, a.employee_number,  a.cost_center, /*c.job_category, c.job_name,*/ coalesce(a.term_date, '12/31/9999')
		from ukg.employees a
-- 		left join ukg.ext_employee_pay_info b on a.employee_id = b.employee_id
-- 		left join ukg.ext_cost_center_job_details c on b.default_job_id = c.id
		where cost_center in ('Customer Care Sales Lead','Customer Care Service Lead')
			and a.status = 'active'
		union
		-- For GM Fixed Department Managers
		select 'GM Fixed Department Managers' as pay_group, a.store, a.last_name || ', ' || a.first_name,
			a.employee_id, a.employee_number,  a.cost_center, /*c.job_category, c.job_name,*/ coalesce(a.term_date, '12/31/9999')
		from ukg.employees a
-- 		left join ukg.ext_employee_pay_info b on a.employee_id = b.employee_id
-- 		left join ukg.ext_cost_center_job_details c on b.default_job_id = c.id
		where cost_center in ( 'Assistant Body Shop Manager','Body Shop Manager','Detail Manager','Main Shop Manager',
			'Service Drive Manager', 'Parts Manager', 'PDQ Manager', 'Service Director')
			and a.status = 'active'
			and store = 'GM' 
		union
		-- For HN Fixed Department Managers
		select 'HN Fixed Department Managers' as pay_group, a.store, a.last_name || ', ' || a.first_name,
			a.employee_id, a.employee_number,  a.cost_center, /*c.job_category, c.job_name,*/ coalesce(a.term_date, '12/31/9999')
		from ukg.employees a
-- 		left join ukg.ext_employee_pay_info b on a.employee_id = b.employee_id
-- 		left join ukg.ext_cost_center_job_details c on b.default_job_id = c.id
		where cost_center in ('Service Drive', 'Parts Manager', 'PDQ Manager','Service Manager')
			and a.status = 'active'
			and store = 'HN') a; 
$BODY$
  LANGUAGE sql VOLATILE
select * from ukg.commission_pay_groups



select 'Service BDC' as pay_group,a.store, a.last_name || ', ' || a.first_name,
	a.employee_id, a.employee_number,  a.cost_center, c.job_category, c.job_name, coalesce(a.term_date, '12/31/9999')
from ukg.employees a
left join ukg.ext_employee_pay_info b on a.employee_id = b.employee_id
left join ukg.ext_cost_center_job_details c on b.default_job_id = c.id
where cost_center = 'Customer Care Service Specialist'
	and store = 'GM'
	and a.status = 'active'


select * from ukg.ext_cost_center_job_details			

dustin shower  
cc: Customer Care Service Specialist
id: 12987528839
emp#: 197643

select * from ukg.ext_employee_pay_info where employee_id = 12987528839\

select * from ukg.ext_employee_pay_info order by employee_id

select * from ukg.json_employee_pay_info
where response::text like '%errors%'