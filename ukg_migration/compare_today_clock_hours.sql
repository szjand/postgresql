﻿select count(*) from ukg.ext_time_entries;  -- 174/187
select the_date, count(*) from  ukg.ext_time_entries group by the_date order by the_date; -- 12-15:11, 12-16:29/12-16:37, 12/17:6
select count(*) from ukg.ext_employees;  -- 602
select count(*) from ukg.ext_employee_details; -- 602

select * from ukg.json_employees
select* from ukg.ext_employees


select b.last_name, b.first_name, a.* 
from ukg.ext_time_entries a
left join ukg.ext_employees b on a.employee_account_id = b.employee_id
where a.the_date > '12/15/2021' -- and last_name = 'miller'
order by the_date, employee_account_id

--------------------------------------------------------------------------------------------
--< arkona
--------------------------------------------------------------------------------------------
drop table if exists arkona.today_clock_times cascade;
create table arkona.today_clock_times as
    select yico_ as store, pymast_employee_number as employee_number, yiclkind as the_date, yiclkint as clock_in, yiclkoutt as clock_out, current_time as cur_time,
      case
        when d.yicode = 'O' then
          coalesce(
            sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
              to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
              /3600.0)::numeric(6,2)) filter (where yicode in ('O')), 0) 
        when d.yicode = 'I' then   
          coalesce(
            sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
              to_timestamp(current_date::text || '-' || current_time::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
              /3600.0)::numeric(6,2)) filter (where yicode in ('I')), 0)       
      end as clock_hours    
    from ( -- d: clock hours
      select a.yico_, a.pymast_employee_number, a.yiclkind, a.yiclkint, a.yiclkoutd, a.yiclkoutt, a.yicode
      from arkona.ext_pypclockin_today a
      where a.yicode in ('I','O')) d
    group by yico_, pymast_employee_number, yiclkind, yiclkoutd, yicode, yiclkint, yiclkoutt, current_time;

create or replace function arkona.update_today_clock_times()
returns void as 
$BODY$
/*
select arkona.update_today_clock_times()
*/
	truncate arkona.today_clock_times;
  insert into arkona.today_clock_times
	select yico_ as store, pymast_employee_number as employee_number, yiclkind as the_date, yiclkint as clock_in, yiclkoutt as clock_out, current_time as cur_time,
		case
			when d.yicode = 'O' then
				coalesce(
					sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
						to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
						/3600.0)::numeric(6,2)) filter (where yicode in ('O')), 0) 
			when d.yicode = 'I' then   
				coalesce(
					sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
						to_timestamp(current_date::text || '-' || current_time::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
						/3600.0)::numeric(6,2)) filter (where yicode in ('I')), 0)       
		end as clock_hours    
	from ( -- d: clock hours
		select a.yico_, a.pymast_employee_number, a.yiclkind, a.yiclkint, a.yiclkoutd, a.yiclkoutt, a.yicode
		from arkona.ext_pypclockin_today a
		where a.yicode in ('I','O')) d
	group by yico_, pymast_employee_number, yiclkind, yiclkoutd, yicode, yiclkint, yiclkoutt, current_time;		
$BODY$
language sql;	

select sum(clock_hours) from arkona.today_clock_times  -- 1486.61/1491.26/1614.72

select * from arkona.today_clock_times

drop table if exists arkona.today_clock_hours cascade;
create table arkona.today_clock_hours (
  store citext not null,
  employee_number citext not null,
  last_name citext not null,
  first_name citext not null,
  department citext,
  supervisor citext,
  title citext,
  the_date date not null,
  clock_hours numeric,
  primary key (employee_number, the_date));

-- drop function ukg.update_today_clock_hours()
create or replace function arkona.update_today_clock_hours()
returns void as
$BODY$
/* 
select arkona.update_today_clock_hours();
*/
  truncate arkona.today_clock_hours;
  insert into arkona.today_clock_hours
  select store, a.employee_number, b.employee_last_name, b.employee_first_name, d.description as department, c.supervisor_name as supervisor, c.title, the_date, sum(clock_hours)
  from arkona.today_clock_times a
  left join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
  left join pto.compli_users c on a.employee_number = c.user_id
  left join arkona.ext_pypclkctl d on b.pymast_company_number = d.company_number 
    and b.department_code = d.department_code
  group by store, a.employee_number, b.employee_last_name, b.employee_first_name, the_date, d.description, c.supervisor_name, c.title;
$BODY$
language sql;  

select sum(clock_hours) from arkona.today_clock_hours;
select * from arkona.today_clock_hours;
select * from ukg.ext_employees 
select * from pto.compli_users
select * from arkona.ext_pymast where employee_last_name = 'aamodt'
select * from arkona. ext_PYPCLKCTL
--------------------------------------------------------------------------------------------
--/> arkona
--------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------
--< ukg
--------------------------------------------------------------------------------------------

CREATE UNLOGGED TABLE ukg.json_today_time_entries
(response jsonb);


drop table if exists ukg.ext_today_time_entries cascade;
create table ukg.ext_today_time_entries (
  employee_account_id bigint not null,
  start_date date not null,
  end_date date not null,
  time_entry_id bigint primary key,
  time_entry_type citext check (time_entry_type in ('TIME','EXTRA_PAY')),
  the_date date not null,
  start_time timestamptz,
  end_time timestamptz,
  group_hash integer,
  total bigint,
  time_off_id bigint,
  pay_category_id integer,
  premium_shift_id integer,
  is_raw boolean,
  is_calc boolean,
  calc_start_time timestamptz,
  calc_end_time timestamptz,
  calc_total bigint,
  calc_pay_category_id integer,
  calc_premium_shift_id integer,
  cost_centers jsonb,
  custom_fields jsonb,
  piecework numeric,
  amount numeric,
  from_date date,
  to_date date);
create unique index on ukg.ext_today_time_entries(employee_account_id,start_time) ;

select * from ukg.json_today_time_entries


create or replace function ukg.ext_today_time_entries()
  returns void as
$BODY$
/*
select  ukg.ext_today_time_entries()
*/
  truncate ukg.ext_today_time_entries;
  
	insert into ukg.ext_today_time_entries(
		employee_account_id,start_date,end_date,time_entry_id,time_entry_type,the_date,start_time,
		end_time,group_hash,total,time_off_id,pay_category_id,premium_shift_id,is_raw,is_calc,
		calc_start_time,calc_end_time,calc_total,calc_pay_category_id,calc_premium_shift_id,
		cost_centers,custom_fields,piecework,amount,from_date,to_date)
	select (b->'employee'->>'account_id')::bigint, 
		(b->>'start_date')::date, 
		(b->>'end_date')::date, 
		(c->>'id')::bigint, 
		c->>'type', 
		(c->>'date')::date,
		(c->>'start_time')::timestamptz, 
		(c->>'end_time')::timestamptz, 
		(c->>'group_hash')::integer, 
		(c->>'total')::bigint,
		(c->'time_off'->>'id')::bigint, 
		(c->'pay_category'->>'id')::bigint,
		(c->'premium_shift'->>'id')::bigint,
		(c->>'is_raw')::boolean, 
		(c->>'is_calc')::boolean,
		(c->>'calc_start_time')::timestamptz, 
		(c->>'calc_end_time')::timestamptz, 
		(c->>'calc_total')::bigint,
		(c->'calc_pay_category'->>'id')::bigint,
		(c->'calc_premium_shift'->>'id')::bigint,
		c->'cost_centers', 
		c->'custom_fields',
		(c->>'piecework')::numeric, 
		(c->>'amount')::numeric,
		(c->>'from_date')::date, 
		(c->>'to_date')::date
	from ukg.json_today_time_entries a
	join jsonb_array_elements(a.response->'time_entry_sets') b on true
	left join jsonb_array_elements(b->'time_entries') c on true
	where jsonb_array_length(b->'time_entries') <> 0;
$BODY$
language sql;	



drop table if exists ukg.today_clock_times cascade;
create table ukg.today_clock_times  as
select b.employee_number, 
	a.the_date, a.start_time AS clock_in, a.end_time as clock_out,  current_time as cur_time,
  case
    when a.end_time is null then
          ((extract(epoch from (current_date || ' ' || current_time)::timestamptz) - extract(epoch from a.start_time))/3600)::numeric(6,2)
    else 
					((extract(epoch from a.end_time) - extract(epoch from a.start_time))/3600)::numeric(6,2)
  end as clock_hours
from ukg.ext_today_time_entries a
join ukg.ext_employees b on a.employee_account_id = b.employee_id;

select * from ukg.today_clock_times order by employee_number

create or replace function ukg.update_today_clock_times()
returns void as 
$BODY$
/*
select ukg.update_today_clock_times()
*/
	truncate ukg.today_clock_times;
  insert into ukg.today_clock_times
	select b.employee_number, 
		a.the_date, a.start_time AS clock_in, a.end_time as clock_out,  current_time as cur_time,
		case
			when a.end_time is null then
						((extract(epoch from (current_date || ' ' || current_time)::timestamptz) - extract(epoch from a.start_time))/3600)::numeric(6,2)
			else 
						((extract(epoch from a.end_time) - extract(epoch from a.start_time))/3600)::numeric(6,2)
		end as clock_hours
	from ukg.ext_today_time_entries a
	join ukg.ext_employees b on a.employee_account_id = b.employee_id;	
$BODY$
language sql;	

select * from ukg.today_clock_times where employee_number = '175469'

drop table if exists ukg.today_clock_hours cascade;
create table ukg.today_clock_hours (
  store citext not null,
  employee_number citext not null,
  last_name citext not null,
  first_name citext not null,
  department citext,
  supervisor citext,
  title citext,
  the_date date not null,
  clock_hours numeric,
  primary key (employee_number, the_date));


create or replace function ukg.update_today_clock_hours()
returns void as
$BODY$
/* 
select ukg.update_today_clock_hours();
*/
  truncate ukg.today_clock_hours;
  insert into ukg.today_clock_hours
  select b.pymast_company_number as store, a.employee_number, b.employee_last_name, b.employee_first_name, 
		d.description as department, c.supervisor_name as supervisor, c.title, the_date, sum(clock_hours) as clock_hours
  from ukg.today_clock_times a
  left join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
		-- fucking arkona
    and 
      case
        when pymast_employee_number = '257240' then pymast_company_number = 'RY2'
        else 1 = 1
      end    
  left join pto.compli_users c on a.employee_number = c.user_id
  left join arkona.ext_pypclkctl d on b.pymast_company_number = d.company_number 
    and b.department_code = d.department_code
  group by b.pymast_company_number, a.employee_number, b.employee_last_name, b.employee_first_name, 
		the_date, d.description, c.supervisor_name, c.title;
$BODY$
language sql;  

select * 
from ukg.today_clock_times
--------------------------------------------------------------------------------------------
--/> ukg
--------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------
--< compare clock hours
--------------------------------------------------------------------------------------------
-- select a.*, b.clock_hours, c.clock_hours
create or replace function ukg.compare_today_clock_hours()
returns table(store citext,emp_number citext,employee text,dt_hours numeric,ukg_hours numeric,
  difference numeric, department citext,supervisor citext,title citext) as
$BODY$
/* 
select * from ukg.compare_today_clock_hours();
*/
	select a.store, a.employee_number as "emp #", a.employee, b.clock_hours as "DT Hours", 
		c.clock_hours as "UKG Hours", abs(coalesce(b.clock_hours, 0) - coalesce(c.clock_hours, 0)) as difference, 
		a.department, a.supervisor, a.title
	from (
		select store, employee_number, last_name || ', ' ||first_name as employee, department, supervisor, title
		from arkona.today_clock_hours
		union
		select store, employee_number, last_name || ', ' ||first_name, department, supervisor, title
		from ukg.today_clock_hours) a
	left join arkona.today_clock_hours b on a.employee_number = b.employee_number	
	left join ukg.today_clock_hours c on a.employee_number = c.employee_number	
	order by a.store, a.department, a.supervisor, a.employee;
$BODY$
language sql;

--------------------------------------------------------------------------------------------
--/> compare clock hours
--------------------------------------------------------------------------------------------

explanation to send in the email
send to jeri, kim
/*
The attached spreadsheet is a list of everyone that has clocked in Sunday in either Dealertrack or UKG.

For those that have clocked in and not yet clocked out, the hours are calculated using the current time as the "clock out" time.
If the report is run at 9:00AM and i clocked in at 7:00AM and have not yet clocked out, the report will show 2 as the clock hours.

If the user has clocked in to Dealertrack and not to UKG, colume E will be empty, likewise, if the user
has clocked in to UKG but not to Dealertrack, column D will be empty.

Initially, i am highlighting rows where the difference between Dealertrack hours and UKG hours is greater than 1.00.
Currently this is updated every 30 minutes between 5AM and 8PM.
The report is sorted by store, department, supervisor, employee.
The supervisors and jobs are pulled from Compli.

Let me know if this of use and if so, the frequency at which you would like to receive updates.
Should it be sent to anyone else?
if this is not of use, let me know and i will shut it off

*/
