﻿select biweekly_pay_period_sequence, min(the_date), max(the_date) from dds.dim_date
where the_date between '05/01/2022' and current_date
group by biweekly_pay_period_sequence

349,2022-05-01,2022-05-07
350,2022-05-08,2022-05-21
351,2022-05-22,2022-06-04

06/11/2022
these 2 tables actually need to be the same level of detail as sap.sales_details
as they will be used as the snapshot (generated a little late) to populate the missing 
	data for pay periods 350 & 351 in sap.sales_details


drop table if exists sap.labor_350_351;
create unlogged table sap.labor_350_351 as
select c.the_date, c.biweekly_pay_period_sequence as pay_period_id, a.control as ro, b.account, 
  b.category, sum(-a.amount) as amount, b.store
from fin.fact_gl a
join sap.sale_accounts b on a.account_key = b.account_key
  and b.category <> 'parts'
join dds.dim_date c on a.date_key = c.date_key
where a.post_status = 'Y'
  and c.biweekly_pay_period_sequence in (350,351)
group by c.the_date, c.biweekly_pay_period_sequence, a.control, b.account, b.category;

drop table if exists sap.parts_350_351;
create unlogged table sap.parts_350_351 as
select d.the_date, d.biweekly_pay_period_sequence as pay_period_id, a.control as ro, b.account, 
  b.category, sum(-a.amount) as amount, b.store
from fin.fact_gl a
join sap.sale_accounts b on a.account_key = b.account_key
  and b.category = 'parts'
join fin.dim_journal c on a.journal_key = c.journal_key
  and c.journal_code in ('SCA','SVI','SWA')  
join dds.dim_date d on a.date_key = d.date_key
where a.post_status = 'Y'
  and d.biweekly_pay_period_sequence in (350,351)
group by d.the_date, d.biweekly_pay_period_sequence, a.control, b.account, b.category;

-- sales for each store/pay period
select aa.*, bb.pp_start, bb.pp_end
from (
	select store, pay_period_id, sum(amount) as amount
	from sap.sales_details
	where pay_period_id between 344 and 349
	group by store, pay_period_id
	union
	select store, pay_period_id, sum(amount) as amount
	from (
		select store, pay_period_id, sum(amount) as amount
		from sap.labor_350_351
		group by store, pay_period_id
		union
		select store, pay_period_id, sum(amount) as amount
		from sap.parts_350_351
		group by store, pay_period_id) a
	group by store, pay_period_id) aa
left join (
  select biweekly_pay_period_sequence, min(biweekly_pay_period_start_date) as pp_start, max(biweekly_pay_period_end_date) as pp_end
  from dds.dim_date
  where biweekly_pay_period_sequence between 344 and 351
  group by biweekly_pay_period_sequence) bb on aa.pay_period_id = bb.biweekly_pay_period_sequence
order by store, pay_period_id


-- -- problem: sap.sales_detail does not agree with vision history
-- -- red herring that was because the vision bi weekly import page had not been advanced to the current pay period
-- -- so the pay period calculation was off by 1 pay period
-- -- updated that and that takes care of the lack of pay period synch
-- -- now it apperas that honda (or new employees) get commission in the form of B-Guarantee !?!?!?
-- -- NO, the guarantee is not the issue with the fluctuating ry2 pay, i don't know what is other than it looks like it was calc'd wrong on payday

-- select a.*, b.pp_start, b.pp_end 
-- from (
-- 	select store, pay_period_id, sum(amount)
-- 	from sap.sales_details
-- 	where pay_period_id between 344 and 349
-- 	group by store, pay_period_id) a
-- join (
--   select biweekly_pay_period_sequence, min(the_date) as pp_start, max(the_date) as pp_end
--   from dds.dim_date
--   group by biweekly_pay_period_sequence) b on a.pay_period_id = b.biweekly_pay_period_sequence
-- order by a.store, a.pay_period_id
-- 
-- select sum(amount) from sap.sales_details where pay_period_id = 348 and store = 'RY1'
-- -- this function was the culprit, it was returning the previous pay period
-- select * from ukg.get_current_pay_periods()

-- so, whgat's up with b-Guarantee
select * from ukg.employees where last_name = 'determan'
select c.* 
from ukg.payrolls a 
left join ukg.pay_statements b on a.payroll_id = b.payroll_id  
  and b.employee_account_id = 12992292321
left join ukg.pay_statement_earnings c on a.payroll_id = c.payroll_id
  and b.pay_statement_id = c.pay_statement_id
  and b.employee_account_id = c.employee_account_id    
where a.payroll_start_date = '03/13/2022'
  and a.ein_name = 'H.G.F., Inc.'



-- ok, initially, this looks correct
-- pay periods 344 & 345 match, 346 - 351 are probably different because they were paid based
-- on sales from pay period 345

-- this is horrible, the values in in sap.sales_details don't match the vision history
-- RY2 values are all over the place
-- invalid underpaid: determan & melicher, both were hired mid pay period, BUT both hired on 3/7, so but determan got .4 and melicher got .3
drop table if exists the_data cascade;
create temp table the_data as
select a.store, a.full_name, a.employee_number, b.commission_percentage, c.amount, 
	round(commission_percentage * c.amount, 2) as calc_commission, 
	c.pay_period_id,
  d.pp_start, d.pp_end,
  e.payroll_id, f.pay_statement_id,
  g.earning_code, g.earning_name, sum(g.ee_amount) as act_paid,
  case when sum(g.ee_amount) > round(commission_percentage * c.amount, 2) then sum(g.ee_amount) - round(commission_percentage * c.amount, 2) else 0 end as overpaid,
  case when round(commission_percentage * c.amount, 2) > sum(g.ee_amount) then round(commission_percentage * c.amount, 2) - sum(g.ee_amount) else 0 end as underpaid
from sap.personnel a
join ukg.employees aa on a.employee_number = aa.employee_number
join sap.personnel_pay_plan_details b on a.employee_number = b.employee_number
  and b.thru_date > current_date
	left join (
		select store, pay_period_id, sum(amount) as amount
		from sap.sales_details
		where pay_period_id between 345 and 349
		group by store, pay_period_id
		union
		select store, pay_period_id, sum(amount) as amount
		from (
			select store, pay_period_id, sum(amount) as amount
			from sap.labor_350_351
			group by store, pay_period_id
			union
			select store, pay_period_id, sum(amount) as amount
			from sap.parts_350_351
			group by store, pay_period_id) a
		group by store, pay_period_id) c on a.store = c.store
left join  (
  select biweekly_pay_period_sequence, min(the_date) as pp_start, max(the_date) as pp_end
  from dds.dim_date
  group by biweekly_pay_period_sequence) d on c.pay_period_id = d.biweekly_pay_period_sequence		
left join ukg.payrolls e on d.pp_start = e.payroll_start_date
  and d.pp_end = e.payroll_end_date
  and e.payroll_type = 'regular'
  and e.payroll_name like '%bi-weekly%' 
  and
    case
      when a.store = 'RY1' then  e.ein_name = 'Rydell Auto Center, Inc.'
      when a.store = 'RY2' then e.ein_name = 'H.G.F., Inc.'
    end
left join ukg.pay_statements f on e.payroll_id = f.payroll_id  
  and aa.employee_id = f.employee_account_id 
left join ukg.pay_statement_earnings g on f.payroll_id = g.payroll_id
  and f.pay_statement_id = g.pay_statement_id
  and f.employee_account_id = g.employee_account_id  
  and g.earning_code = 'Biweekly Commission'
where a.thru_date > current_date	
--   and aa.employee_id = 12992292321
-- and c.pay_period_id in (344,345)
--   and a.store = 'RY2'
group by a.store, a.full_name, a.employee_number, b.commission_percentage, c.amount, 
	commission_percentage * c.amount, 
	c.pay_period_id,
  d.pp_start, d.pp_end,
  e.payroll_id, f.pay_statement_id,
  g.earning_code, g.earning_name
-- order by store, pay_period_id  
order by full_name, pay_period_id desc


/* 
the only remaining discrepancies:
	Rodriguez  346  3/13 -> 3/26, actual pay: 1540.07,  pay due: 1493.73,  no adjustments
	guessing it is a typo, 1540.07 instead of 1440.07
	
*/
-- doubt that this is the only way, but it is a way of dealing with the data model including 
-- ukg.pay_plan_biweekly_commissions_snapshots & ukg.pay_plan_adjustments that works

select a.store, a.full_name, a.employee_number, a.commission_percentage as "com %", a.pay_period_id,
  a.pp_start, a.pp_end, a.calc_commission, a.act_paid, a.overpaid, a.underpaid, 
  coalesce(b.comm, 0) as comm, coalesce(c.adj, 0) as adj, coalesce(b.comm, 0) + coalesce(c.adj, 0),
  case 
    when coalesce(b.comm, 0) + coalesce(c.adj, 0) = a.act_paid then 'MATCH'
    else 'NO MATCH'
  end
from the_data a
left join (
	select a.pay_period_indicator, a.employee_number, sum(amount) as comm
	from ukg.pay_plan_biweekly_commissions_snapshots a
	where a.pay_period_indicator between 345 and 351
		and adjustment_id is null
	group by a.pay_period_indicator, a.employee_number) b on a.pay_period_id = b.pay_period_indicator
	  and a.employee_number = b.employee_number
left join (
	select adjustment_id, pay_period_indicator, employee_number, sum(amount) as adj
	from ukg.pay_plan_adjustments 
	where pay_period_indicator between  345 and 351
		and earnings_code = 'Biweekly Commission'
	group by adjustment_id, pay_period_indicator, employee_number  
	having sum(amount) <> 0) c on a.pay_period_id = c.pay_period_indicator
	  and a.employee_number = c.employee_number	  
order by a.employee_number, a.pay_period_id



-- format for presentation as sap_discrepancies.xlsx
select a.store, a.full_name, a.employee_number, a.pay_period_id,
  a.pp_start, a.pp_end, a.commission_percentage as "com %",  a.amount as total_sales,
  a.calc_commission, coalesce(c.adj, 0) as adj,
  a.calc_commission + coalesce(c.adj, 0) as total_due,
  a.act_paid as actually_paid, a.overpaid, a.underpaid
from the_data a
left join (
	select a.pay_period_indicator, a.employee_number, sum(amount) as comm
	from ukg.pay_plan_biweekly_commissions_snapshots a
	where a.pay_period_indicator between 345 and 351
		and adjustment_id is null
	group by a.pay_period_indicator, a.employee_number) b on a.pay_period_id = b.pay_period_indicator
	  and a.employee_number = b.employee_number
left join (
	select adjustment_id, pay_period_indicator, employee_number, sum(amount) as adj
	from ukg.pay_plan_adjustments 
	where pay_period_indicator between  345 and 351
		and earnings_code = 'Biweekly Commission'
	group by adjustment_id, pay_period_indicator, employee_number  
	having sum(amount) <> 0) c on a.pay_period_id = c.pay_period_indicator
	  and a.employee_number = c.employee_number	  
order by a.store, a.full_name, a.pay_period_id



















-- drop table if exists sap.labor_350_351;
-- create unlogged table sap.labor_350_351 as
-- select store, biweekly_pay_period_sequence, sum(amount) as amount
-- from (
-- select c.the_date, c.biweekly_pay_period_sequence, a.control as ro, b.account, 
--   b.category, sum(-a.amount) as amount, b.store
-- from fin.fact_gl a
-- join sap.sale_accounts b on a.account_key = b.account_key
--   and b.category <> 'parts'
-- join dds.dim_date c on a.date_key = c.date_key
-- where a.post_status = 'Y'
--   and c.biweekly_pay_period_sequence in (350,351)
-- group by c.the_date, c.biweekly_pay_period_sequence, a.control, b.account, b.category) x
-- group by store, biweekly_pay_period_sequence;
-- comment on table sap.labor_350_351 is 'service advisor payroll sales data for pay periods 350 and 351';
-- 
-- drop table if exists sap.parts_350_351;
-- create unlogged table sap.parts_350_351 as
-- select store, biweekly_pay_period_sequence, sum(amount) as amount
-- from (
-- select d.the_date, d.biweekly_pay_period_sequence, a.control as ro, b.account, 
--   b.category, sum(-a.amount) as amount, b.store
-- from fin.fact_gl a
-- join sap.sale_accounts b on a.account_key = b.account_key
--   and b.category = 'parts'
-- join fin.dim_journal c on a.journal_key = c.journal_key
--   and c.journal_code in ('SCA','SVI','SWA')  
-- join dds.dim_date d on a.date_key = d.date_key
-- where a.post_status = 'Y'
--   and d.biweekly_pay_period_sequence in (350,351)
-- group by d.the_date, d.biweekly_pay_period_sequence, a.control, b.account, b.category) x
-- group by store, biweekly_pay_period_sequence;
-- comment on table sap.parts_350_351 is 'service advisor payroll sales data for pay periods 350 and 351';



-- select a.store, a.full_name, a.employee_number, b.commission_percentage, c.total, 
-- 	round(commission_percentage * c.total, 2) as calc_commission, 
-- 	c.biweekly_pay_period_sequence,
--   d.pp_start, d.pp_end,
--   e.payroll_id, f.pay_statement_id,
--   g.earning_code, g.earning_name, sum(g.ee_amount) as act_paid,
--   case when sum(g.ee_amount) > round(commission_percentage * c.total, 2) then sum(g.ee_amount) - round(commission_percentage * c.total, 2) else 0 end as overpaid,
--   case when round(commission_percentage * c.total, 2) > sum(g.ee_amount) then round(commission_percentage * c.total, 2) - sum(g.ee_amount) else 0 end as underpaid
-- from sap.personnel a
-- join ukg.employees aa on a.employee_number = aa.employee_number
-- join sap.personnel_pay_plan_details b on a.employee_number = b.employee_number
--   and b.thru_date > current_date
-- 	left join (
-- 	select * 
-- 	from (
-- 		select store, biweekly_pay_period_sequence, sum(amount) as total
-- 		from (
-- 		select 'parts', * from sap.parts_350_351
-- 		union
-- 		select 'labor', * from sap.labor_350_351) a
-- 		group by store, biweekly_pay_period_sequence) b) c on a.store = c.store
-- left join  (
--   select biweekly_pay_period_sequence, min(the_date) as pp_start, max(the_date) as pp_end
--   from dds.dim_date
--   group by biweekly_pay_period_sequence) d on c.biweekly_pay_period_sequence = d.biweekly_pay_period_sequence		
-- left join ukg.payrolls e on d.pp_start = e.payroll_start_date
--   and d.pp_end = e.payroll_end_date
--   and e.payroll_type = 'regular'
--   and e.payroll_name like '%bi-weekly%' 
--   and
--     case
--       when a.store = 'RY1' then  e.ein_name = 'Rydell Auto Center, Inc.'
--       when a.store = 'RY2' then e.ein_name = 'H.G.F., Inc.'
--     end
-- left join ukg.pay_statements f on e.payroll_id = f.payroll_id  
--   and aa.employee_id = f.employee_account_id 
-- left join ukg.pay_statement_earnings g on f.payroll_id = g.payroll_id
--   and f.pay_statement_id = g.pay_statement_id
--   and f.employee_account_id = g.employee_account_id  
--   and g.earning_code = 'Biweekly Commission'
-- where a.thru_date > current_date	
-- group by a.store, a.full_name, a.employee_number, b.commission_percentage, c.total, 
-- 	commission_percentage * c.total, 
-- 	c.biweekly_pay_period_sequence,
--   d.pp_start, d.pp_end,
--   e.payroll_id, f.pay_statement_id,
--   g.earning_code, g.earning_name
-- order by store, biweekly_pay_period_sequence  
-- order by full_name, biweekly_pay_period_sequence desc

-- 06/11/22
eg Drew Brooks
  pay periods 344 - 347
		show as underpaid, but i am generateing the commission based on fact_gl
		which may have changed since the time of payroll
		compare sap.sales_detail to this generated sales number

select aa.*, bb.fact_gl
from (
	select a.store, a.pay_period_id, sum(a.amount) as sales_details
	from sap.sales_details a --limit 10
	where a.pay_period_id between 344 and 351
	group by a.store, a.pay_period_id) aa
join (
	select * 
	from (
		select store, biweekly_pay_period_sequence, sum(amount) as fact_gl
		from (
		select 'parts', * from sap.test_parts
		union
		select 'labor', * from sap.test_labor) a
		group by store, biweekly_pay_period_sequence) b) bb on aa.store = bb.store and aa.pay_period_id = bb.biweekly_pay_period_sequence
order by store, pay_period_id		
		
so what i have to do is regenerate sap.test_parts & sap.test_labor on pay periods 350 & 351 only (the missing data from sap.sales_details)
union that with the data from sap.sales_details
	