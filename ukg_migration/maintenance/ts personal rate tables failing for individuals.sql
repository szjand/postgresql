﻿12/20/23 at least 3 more failing
-- these 3, brusseau, bruns, bazhyna are all with hourly rate only, null for the other values
select b.*
from ukg.employee_profiles a
left join ukg.employees b on a.employee_id = b.employee_id
where a.rate_table_id in (4406448258, 4403018630, 4403018631)

select * from ukg.json_personal_rate_tables limit 10

-- looks like the best solution is to change the update function to
-- exclude those rows where rate_item_id is null 
select a.id as rate_table_id, (c->>'id')::bigint as rate_item_id,
	(b->'rate_table'->>'current_rate')::numeric as current_rate, 
	b->>'type' as type,
	(b->'rate_table'->>'current_effective_from')::date as current_rate_effective_from, 	
	(b->'rate_table'->>'current_effective_to')::date as current_rate_effective_to, 
	(b->'rate_table'->>'effective_from')::date as effective_from, 
	(b->'rate_table'->>'effective_to')::date as effective_to, 
	b->>'description' as description, 
	b->'counter_rec'->'counter'->>'name' as counter
from ukg.json_personal_rate_tables a
left join jsonb_array_elements(response->'rate_schedules') b on 1 = 1
left join jsonb_array_elements(b->'rate_items') c on true
  and c->>'effective_from' = b->'rate_table'->>'current_effective_from'
where exists ( -- active employees only
  select 1
  from ukg.employee_profiles m
  join ukg.employees n on m.employee_id = n.employee_id
    and n.status = 'active'
  join ukg.employee_profiles o on n.employee_id = o.employee_id
    and o.rate_table_id = a.id)
and c->>'id' is not null
and a.id in (4401193548,4406448258,4403018630,4403018631,4406448263,4406439947) -- these just have base comp, other rows are null


--------------------------------------------------------------------------------------------------------------------------------------------------------------
 -- 4406448263 tyler hinkle rates andd rate_items and cr rate eff & descr missing
--  email kim
-- from kim
-- I it shows blank but on 12-27-2023 it will show an amount.  Before 12-27-2023 he does not have pto pay .  if we have to change it  can look at doing it differently. 
-- 
-- kim
select a.id as rate_table_id, (c->>'id')::bigint as rate_item_id,
	(b->'rate_table'->>'current_rate')::numeric as current_rate, 
	b->>'type' as type,
	(b->'rate_table'->>'current_effective_from')::date as current_rate_effective_from, 	
	(b->'rate_table'->>'current_effective_to')::date as current_rate_effective_to, 
	(b->'rate_table'->>'effective_from')::date as effective_from, 
	(b->'rate_table'->>'effective_to')::date as effective_to, 
	b->>'description' as description, 
	b->'counter_rec'->'counter'->>'name' as counter
from ukg.json_personal_rate_tables a
left join jsonb_array_elements(response->'rate_schedules') b on 1 = 1
left join jsonb_array_elements(b->'rate_items') c on true
  and c->>'effective_from' = b->'rate_table'->>'current_effective_from'
where exists ( -- active employees only
  select 1
  from ukg.employee_profiles m
  join ukg.employees n on m.employee_id = n.employee_id
    and n.status = 'active'
  join ukg.employee_profiles o on n.employee_id = o.employee_id
    and o.rate_table_id = a.id)
and (c->>'id') is null    
and a.id in  ( 4406448263, 4401193475, 4406042266)





select 
	max((b->'rate_table'->>'current_effective_from')::date) as current_rate_effective_from
from ukg.json_personal_rate_tables a
left join jsonb_array_elements(response->'rate_schedules') b on 1 = 1
left join jsonb_array_elements(b->'rate_items') c on true
  and c->>'effective_from' = b->'rate_table'->>'current_effective_from'
where exists ( -- active employees only
  select 1
  from ukg.employee_profiles m
  join ukg.employees n on m.employee_id = n.employee_id
    and n.status = 'active'
  join ukg.employee_profiles o on n.employee_id = o.employee_id
    and o.rate_table_id = a.id)

-- 4406448263  Tyler Hinckle
select * 
from ukg.employee_profiles
where rate_table_id =  4406448263 

select * from ukg.json_personal_rate_tables limit 10

employee_id 13002150797  tyler hinkle

select * from ukg.employees where employee_id = 13002150797

select a.status, a.last_name, a.first_name, a.employee_id, a.employee_number, b.*
from ukg.employees a
join ukg.employee_profiles b on a.employee_id = b.employee_id
where a.last_name like 'h%'
  and a.status = 'Active'
order by last_name

-- 4406439947  Nathan Sloop
select * 
from ukg.employee_profiles
where rate_table_id =  4406439947 

select * from ukg.employees where employee_id = 13002131665