﻿-- 02/04/22 80 changed pay_statement_earnings
-- turns out, they are all cost center names that have been changed
-- same cost centers, just named differently
-- eg from Rydell GM/Sales/Dealer Trade Driver - Auto (GM New) to Rydell GM/Sales/DTD - Auto (GM New)


  select * from ukg.payroll_cdc()
----------------------------------------------------------------------------
/* 
i never remember how to approach this, a failure of function ukg.payroll_cdc()
boils down to needing to run each section of the function to find out which one is failing,
lately it seems pay_statement_earnings is the most common failure
the function could be better, returning the area of failure
*/
----------------------------------------------------------------------------

----------------------------------------------------------------------------
--< 05/02/23 payrolls changed  this is the most common, month end
----------------------------------------------------------------------------  
		select *
		from ( -- from ukg.payrolls 
			select payroll_id, 
				(
					select md5(z::text) as hash
					from (
						select payroll_id,payroll_name,payroll_start_date,payroll_end_date,pay_date,payroll_type,status,ein_name,billable 
						from ukg.payrolls
						where payroll_id = a.payroll_id) z)
			from ukg.payrolls a) aa
		join (  -- from ukg.json_payrolls
			select payroll_id, md5(a::text) as hash
			from (  
				select (b->>'id')::integer as payroll_id,
					b->>'name' as payroll_name,
					coalesce((b->>'start_date')::date, '12/31/9999'::date) as payroll_start_date, 
					coalesce((b->>'end_date')::date, '12/31/9999'::date) as payroll_end_date, 
					coalesce((b->>'pay_date')::date, '12/31/9999'::date) as pay_date,
					b->'type'->>'display_name' as payroll_type,
					b->>'status',
					b->'ein'->>'ein_name' as ein_name,
					(b->>'billable')::boolean
				from ukg.json_payrolls a
				join jsonb_array_elements(a.response->'payrolls') as b(payrolls) on true) a) bb on aa.payroll_id = bb.payroll_id
					and aa.hash <> bb.hash

-- what has changed in the payrolls from the above query
select 'payroll' as source, * from ukg.payrolls where payroll_id in(110137225,110229805,110229809) ----------------------
union 
select 'json', (b->>'id')::integer as payroll_id,
		b->>'name' as payroll_name,
		coalesce((b->>'start_date')::date, '12/31/9999'::date) as payroll_start_date, 
		coalesce((b->>'end_date')::date, '12/31/9999'::date) as payroll_end_date, 
		coalesce((b->>'pay_date')::date, '12/31/9999'::date) as pay_date,
		b->'type'->>'display_name' as payroll_type,
		b->>'status',
		b->'ein'->>'ein_name' as ein_name,
		(b->>'billable')::boolean
	from ukg.json_payrolls a
	join jsonb_array_elements(a.response->'payrolls') as b(payrolls) on true
	where (b->>'id')::integer in (110137225,110229805,110229809) ---------------------------------------------------------
order by payroll_id, source

-- these are typical on the 1st of the month, the status changing from open to finalized
update ukg.payrolls
set status = 'FINALIZED'
-- select * from ukg.payrolls
where payroll_id in(110137225,110229805,110229809); ---------------------------------------------------------------

----------------------------------------------------------------------------
--/> 05/02/23 payrolls changed
---------------------------------------------------------------------------- 

----------------------------------------------------------------------------
--< 09/16/23 29 more pay_statement_earnings changed
----------------------------------------------------------------------------  

-- select *
select string_agg(aa.earnings_id::text, ',') -- generate a list of earnings_ids with diffs
from ( -- from ukg.pay_statement_earnings
	select earnings_id, cost_center_name,cost_center_display_name,cost_center_job_name, cost_center_job_display_name ,
		(
			select md5(z::text) as hash
			from (
				select payroll_id,pay_statement_id,employee_account_id,earnings_id,batch_id,
					batch_name,hours,earning_id,earning_code,earning_name,base_rate,ee_amount,type_name,cost_center_id,
					cost_center_name,cost_center_display_name,cost_center_job_id,cost_center_job_name,cost_center_job_display_name
				from ukg.pay_statement_earnings
				where earnings_id = a.earnings_id) z)
	from ukg.pay_statement_earnings a) aa
	join (  -- from ukg.json_pay_statement_details
		select earnings_id, md5(a::text) as hash, cost_center_name,cost_center_display_name,cost_center_job_name, cost_center_job_display_name
		from (  
			select a.payroll_id, a.pay_statement_id, 
				(a.response->'employee'->>'account_id')::bigint as employee_id,
				(b->>'id')::bigint as earnings_id, 
				(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
				(b->>'hours')::BIGINT as hours,
				(b->'earning'->>'id')::integer as earning_id, b->'earning'->>'code' as earning_code, b->'earning'->>'name' as earning_name,
				(b->>'base_rate')::numeric as base_rate, (b->>'ee_amount')::numeric as ee_amount,
				b->>'type_name' as type_name,
				(c->'value'->>'id')::bigint as cost_center_id,
				c->'value'->>'name' as cost_center_name,
				c->'value'->>'display_name' as cost_center_display_name,
				(d->'value'->>'id')::BIGINT as cost_center_job_id,
				d->'value'->>'name' as cost_center_job_name,
				d->'value'->>'display_name' as cost_center_job_display_name
			from ukg.json_pay_statement_details a
			left join jsonb_array_elements(a.response->'earnings') b on true
			left join jsonb_array_elements(b->'cost_centers') c on true 
				and c->>'index' = '0'
			left join jsonb_array_elements(b->'cost_centers') d on true 
				and d->>'index' = '9') a) bb on aa.earnings_id = bb.earnings_id
					and aa.hash <> bb.hash



-- try a blanket update of cost_center_name,cost_center_display_name,cost_center_job_name, cost_center_job_display_name
-- for all the earnings_ids
-- looks like it worked ok
update ukg.pay_statement_earnings x
set cost_center_name = y.cost_center_name, 
		cost_center_display_name = y.cost_center_display_name,
		cost_center_job_name = y.cost_center_job_name, 
		cost_center_job_display_name = y.cost_center_job_display_name
from (
	select 'json', a.payroll_id, a.pay_statement_id, 
		(a.response->'employee'->>'account_id')::bigint as employee_id,
		(b->>'id')::bigint as earnings_id, 
		(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
	-- 	(b->>'hours')::BIGINT as hours,
		(b->'earning'->>'id')::integer as earning_id, b->'earning'->>'code' as earning_code, b->'earning'->>'name' as earning_name,
		(b->>'base_rate')::numeric as base_rate, (b->>'ee_amount')::numeric as ee_amount,
		b->>'type_name' as type_name,
		(c->'value'->>'id')::bigint as cost_center_id,
		c->'value'->>'name' as cost_center_name,
		c->'value'->>'display_name' as cost_center_display_name,
		(d->'value'->>'id')::BIGINT as cost_center_job_id,
		d->'value'->>'name' as cost_center_job_name,
		d->'value'->>'display_name' as cost_center_job_display_name
	from ukg.json_pay_statement_details a
	left join jsonb_array_elements(a.response->'earnings') b on true
	left join jsonb_array_elements(b->'cost_centers') c on true 
		and c->>'index' = '0'
	left join jsonb_array_elements(b->'cost_centers') d on true 
		and d->>'index' = '9'
	where (b->>'id')::bigint in  (28938276629,28938276628,28938276631,28938276630,28938278505,28938278504,28939170790,28938278658,
		28938278675,28938278674,28938297165,28938297167,28938297166,28938297169,28938297168,28938283961,28938283960,28938283962,28938283959,
		28952327408,28952329374,28952329394,28952329536,28952342461,28952342460,28952342459,28952342458,28952342457,28952336147)) y
where x.earnings_id = y.earnings_id;


----------------------------------------------------------------------------
--/> 09/16/23 29 more pay_statement_earnings changed
----------------------------------------------------------------------------


----------------------------------------------------------------------------
--< 09/15/23 173 pay_statement_earnings changed
----------------------------------------------------------------------------  
select *
-- select string_agg(aa.earnings_id::text, ',') -- generate a list of earnings_ids with diffs
from ( -- from ukg.pay_statement_earnings
	select earnings_id, cost_center_name,cost_center_display_name,cost_center_job_name, cost_center_job_display_name ,
		(
			select md5(z::text) as hash
			from (
				select payroll_id,pay_statement_id,employee_account_id,earnings_id,batch_id,
					batch_name,hours,earning_id,earning_code,earning_name,base_rate,ee_amount,type_name,cost_center_id,
					cost_center_name,cost_center_display_name,cost_center_job_id,cost_center_job_name,cost_center_job_display_name
				from ukg.pay_statement_earnings
				where earnings_id = a.earnings_id) z)
	from ukg.pay_statement_earnings a) aa
	join (  -- from ukg.json_pay_statement_details
		select earnings_id, md5(a::text) as hash, cost_center_name,cost_center_display_name,cost_center_job_name, cost_center_job_display_name
		from (  
			select a.payroll_id, a.pay_statement_id, 
				(a.response->'employee'->>'account_id')::bigint as employee_id,
				(b->>'id')::bigint as earnings_id, 
				(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
				(b->>'hours')::BIGINT as hours,
				(b->'earning'->>'id')::integer as earning_id, b->'earning'->>'code' as earning_code, b->'earning'->>'name' as earning_name,
				(b->>'base_rate')::numeric as base_rate, (b->>'ee_amount')::numeric as ee_amount,
				b->>'type_name' as type_name,
				(c->'value'->>'id')::bigint as cost_center_id,
				c->'value'->>'name' as cost_center_name,
				c->'value'->>'display_name' as cost_center_display_name,
				(d->'value'->>'id')::BIGINT as cost_center_job_id,
				d->'value'->>'name' as cost_center_job_name,
				d->'value'->>'display_name' as cost_center_job_display_name
			from ukg.json_pay_statement_details a
			left join jsonb_array_elements(a.response->'earnings') b on true
			left join jsonb_array_elements(b->'cost_centers') c on true 
				and c->>'index' = '0'
			left join jsonb_array_elements(b->'cost_centers') d on true 
				and d->>'index' = '9') a) bb on aa.earnings_id = bb.earnings_id
					and aa.hash <> bb.hash
order by aa.earnings_id



-- 09/14/23 173 cost cent and cost center job name changes
-- this is way the fuck too manual
select 'payroll' as source, payroll_id,pay_statement_id,employee_account_id,earnings_id,batch_id,
	batch_name,/*hours,*/earning_id,earning_code,earning_name,base_rate,ee_amount,type_name,cost_center_id,
	cost_center_name,cost_center_display_name,cost_center_job_id,cost_center_job_name,cost_center_job_display_name
from ukg.pay_statement_earnings 
where earnings_id in (28938268708,28938268709,28938299685,28938299684,28938299687,28938299686,28938299683,28938299682,28938301129,28938301128,28938301127,28938301126,
	28938271717,28938271718,28938271719,28938332146,28938301407,28938301406,28938271959,28938299317,28938299318,28938273639,28938301709,28938301708,28938301710,28938301705,
	28938274443,28938301707,28938301706,28938301737,28938301736,28938301738,28938301733,28938301735,28938301734,28938274813,28938275295,28938297460,28938297459,28938275460,
	28938276076,28938300456,28938300455,28938299293,28938299294,28938299701,28938299700,28938298373,28938298372,28938298369,28938298368,28938298371,28938298370,28938278696,
	28938301411,28938299679,28938299678,28938296736,28938280125,28938296733,28938296732,28938296735,28938296734,28938296731,28938277565,28938277564,28938277567,28938277566,
	28938277563,28938281827,28938297420,28938297417,28938297416,28938297419,28938297418,28938297415,28938301149,28938301148,28938301147,28938301146,28938283122,28938283205,
	28938283204,28938283915,28938284123,28938301181,28938301182,28938302028,28938302025,28938302024,28938302027,28938302026,28938302023,28938299657,28938299656,28938297901,
	28938285516,28938297197,28938297196,28938297199,28938297198,28938297201,28938297200,28938285702,28938300469,28938300468,28938300470,28938300467,28938300765,28938300764,
	28938300767,28938300766,28938300763,28938300768,28940490096,28940516355,28940516357,28940516356,28940516391,28940516390,28940516389,28940516388,28940516651,28940516650,
	28940516653,28940516652,28940507993,28940507992,28940492346,28940492344,28940492345,28940492343,28940508360,28940516367,28940516366,28940516365,28940516368,28940516403,
	28940516402,28940516401,28940516400,28940516411,28940516414,28940508413,28940516413,28940516412,28940516619,28940516618,28940516621,28940516620,28940516632,28940516631,
	28940516630,28940516629,28940516642,28940516641,28940516640,28940516655,28940516654,28940516658,28940516657,28940516656,28940490095,28940516667,28940516666,28940516670,
	28940516669,28940516668,28940516937,28940516936,28940516935,28940404701,28938090239)
union
select 'json', a.payroll_id, a.pay_statement_id, 
	(a.response->'employee'->>'account_id')::bigint as employee_id,
	(b->>'id')::bigint as earnings_id, 
	(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
	(b->'earning'->>'id')::integer as earning_id, b->'earning'->>'code' as earning_code, b->'earning'->>'name' as earning_name,
	(b->>'base_rate')::numeric as base_rate, (b->>'ee_amount')::numeric as ee_amount,
	b->>'type_name' as type_name,
	(c->'value'->>'id')::bigint as cost_center_id,
	c->'value'->>'name' as cost_center_name,
	c->'value'->>'display_name' as cost_center_display_name,
	(d->'value'->>'id')::BIGINT as cost_center_job_id,
	d->'value'->>'name' as cost_center_job_name,
	d->'value'->>'display_name' as cost_center_job_display_name
from ukg.json_pay_statement_details a
left join jsonb_array_elements(a.response->'earnings') b on true
left join jsonb_array_elements(b->'cost_centers') c on true 
	and c->>'index' = '0'
left join jsonb_array_elements(b->'cost_centers') d on true 
	and d->>'index' = '9'
where (b->>'id')::bigint in  (28938268708,28938268709,28938299685,28938299684,28938299687,28938299686,28938299683,28938299682,28938301129,28938301128,28938301127,28938301126,
	28938271717,28938271718,28938271719,28938332146,28938301407,28938301406,28938271959,28938299317,28938299318,28938273639,28938301709,28938301708,28938301710,28938301705,
	28938274443,28938301707,28938301706,28938301737,28938301736,28938301738,28938301733,28938301735,28938301734,28938274813,28938275295,28938297460,28938297459,28938275460,
	28938276076,28938300456,28938300455,28938299293,28938299294,28938299701,28938299700,28938298373,28938298372,28938298369,28938298368,28938298371,28938298370,28938278696,
	28938301411,28938299679,28938299678,28938296736,28938280125,28938296733,28938296732,28938296735,28938296734,28938296731,28938277565,28938277564,28938277567,28938277566,
	28938277563,28938281827,28938297420,28938297417,28938297416,28938297419,28938297418,28938297415,28938301149,28938301148,28938301147,28938301146,28938283122,28938283205,
	28938283204,28938283915,28938284123,28938301181,28938301182,28938302028,28938302025,28938302024,28938302027,28938302026,28938302023,28938299657,28938299656,28938297901,
	28938285516,28938297197,28938297196,28938297199,28938297198,28938297201,28938297200,28938285702,28938300469,28938300468,28938300470,28938300467,28938300765,28938300764,
	28938300767,28938300766,28938300763,28938300768,28940490096,28940516355,28940516357,28940516356,28940516391,28940516390,28940516389,28940516388,28940516651,28940516650,
	28940516653,28940516652,28940507993,28940507992,28940492346,28940492344,28940492345,28940492343,28940508360,28940516367,28940516366,28940516365,28940516368,28940516403,
	28940516402,28940516401,28940516400,28940516411,28940516414,28940508413,28940516413,28940516412,28940516619,28940516618,28940516621,28940516620,28940516632,28940516631,
	28940516630,28940516629,28940516642,28940516641,28940516640,28940516655,28940516654,28940516658,28940516657,28940516656,28940490095,28940516667,28940516666,28940516670,
	28940516669,28940516668,28940516937,28940516936,28940516935,28940404701,28938090239)
order by earnings_id, source

-- try a blanket update of cost_center_name,cost_center_display_name,cost_center_job_name, cost_center_job_display_name
-- for all the earnings_ids
-- looks like it worked ok
update ukg.pay_statement_earnings x
set cost_center_name = y.cost_center_name, 
		cost_center_display_name = y.cost_center_display_name,
		cost_center_job_name = y.cost_center_job_name, 
		cost_center_job_display_name = y.cost_center_job_display_name
from (
	select 'json', a.payroll_id, a.pay_statement_id, 
		(a.response->'employee'->>'account_id')::bigint as employee_id,
		(b->>'id')::bigint as earnings_id, 
		(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
	-- 	(b->>'hours')::BIGINT as hours,
		(b->'earning'->>'id')::integer as earning_id, b->'earning'->>'code' as earning_code, b->'earning'->>'name' as earning_name,
		(b->>'base_rate')::numeric as base_rate, (b->>'ee_amount')::numeric as ee_amount,
		b->>'type_name' as type_name,
		(c->'value'->>'id')::bigint as cost_center_id,
		c->'value'->>'name' as cost_center_name,
		c->'value'->>'display_name' as cost_center_display_name,
		(d->'value'->>'id')::BIGINT as cost_center_job_id,
		d->'value'->>'name' as cost_center_job_name,
		d->'value'->>'display_name' as cost_center_job_display_name
	from ukg.json_pay_statement_details a
	left join jsonb_array_elements(a.response->'earnings') b on true
	left join jsonb_array_elements(b->'cost_centers') c on true 
		and c->>'index' = '0'
	left join jsonb_array_elements(b->'cost_centers') d on true 
		and d->>'index' = '9'
	where (b->>'id')::bigint in  (28938268708,28938268709,28938299685,28938299684,28938299687,28938299686,28938299683,28938299682,28938301129,28938301128,28938301127,28938301126,
		28938271717,28938271718,28938271719,28938332146,28938301407,28938301406,28938271959,28938299317,28938299318,28938273639,28938301709,28938301708,28938301710,28938301705,
		28938274443,28938301707,28938301706,28938301737,28938301736,28938301738,28938301733,28938301735,28938301734,28938274813,28938275295,28938297460,28938297459,28938275460,
		28938276076,28938300456,28938300455,28938299293,28938299294,28938299701,28938299700,28938298373,28938298372,28938298369,28938298368,28938298371,28938298370,28938278696,
		28938301411,28938299679,28938299678,28938296736,28938280125,28938296733,28938296732,28938296735,28938296734,28938296731,28938277565,28938277564,28938277567,28938277566,
		28938277563,28938281827,28938297420,28938297417,28938297416,28938297419,28938297418,28938297415,28938301149,28938301148,28938301147,28938301146,28938283122,28938283205,
		28938283204,28938283915,28938284123,28938301181,28938301182,28938302028,28938302025,28938302024,28938302027,28938302026,28938302023,28938299657,28938299656,28938297901,
		28938285516,28938297197,28938297196,28938297199,28938297198,28938297201,28938297200,28938285702,28938300469,28938300468,28938300470,28938300467,28938300765,28938300764,
		28938300767,28938300766,28938300763,28938300768,28940490096,28940516355,28940516357,28940516356,28940516391,28940516390,28940516389,28940516388,28940516651,28940516650,
		28940516653,28940516652,28940507993,28940507992,28940492346,28940492344,28940492345,28940492343,28940508360,28940516367,28940516366,28940516365,28940516368,28940516403,
		28940516402,28940516401,28940516400,28940516411,28940516414,28940508413,28940516413,28940516412,28940516619,28940516618,28940516621,28940516620,28940516632,28940516631,
		28940516630,28940516629,28940516642,28940516641,28940516640,28940516655,28940516654,28940516658,28940516657,28940516656,28940490095,28940516667,28940516666,28940516670,
		28940516669,28940516668,28940516937,28940516936,28940516935,28940404701,28938090239)) y
where x.earnings_id = y.earnings_id;
----------------------------------------------------------------------------
--/> 09/15/23 173 pay_statement_earnings changed
----------------------------------------------------------------------------  

					
----------------------------------------------------------------------------
--< 04/26/23 2 pay_statement_earnings changed
----------------------------------------------------------------------------  
select *
from ( -- from ukg.pay_statement_earnings
	select earnings_id, 
		(
			select md5(z::text) as hash
			from (
				select payroll_id,pay_statement_id,employee_account_id,earnings_id,batch_id,
					batch_name,hours,earning_id,earning_code,earning_name,base_rate,ee_amount,type_name,cost_center_id,
					cost_center_name,cost_center_display_name,cost_center_job_id,cost_center_job_name,cost_center_job_display_name
				from ukg.pay_statement_earnings
				where earnings_id = a.earnings_id) z)
	from ukg.pay_statement_earnings a) aa
	join (  -- from ukg.json_pay_statement_details
		select earnings_id, md5(a::text) as hash
		from (  
			select a.payroll_id, a.pay_statement_id, 
				(a.response->'employee'->>'account_id')::bigint as employee_id,
				(b->>'id')::bigint as earnings_id, 
				(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
				(b->>'hours')::BIGINT as hours,
				(b->'earning'->>'id')::integer as earning_id, b->'earning'->>'code' as earning_code, b->'earning'->>'name' as earning_name,
				(b->>'base_rate')::numeric as base_rate, (b->>'ee_amount')::numeric as ee_amount,
				b->>'type_name' as type_name,
				(c->'value'->>'id')::bigint as cost_center_id,
				c->'value'->>'name' as cost_center_name,
				c->'value'->>'display_name' as cost_center_display_name,
				(d->'value'->>'id')::BIGINT as cost_center_job_id,
				d->'value'->>'name' as cost_center_job_name,
				d->'value'->>'display_name' as cost_center_job_display_name
			from ukg.json_pay_statement_details a
			left join jsonb_array_elements(a.response->'earnings') b on true
			left join jsonb_array_elements(b->'cost_centers') c on true 
				and c->>'index' = '0'
			left join jsonb_array_elements(b->'cost_centers') d on true 
				and d->>'index' = '9') a) bb on aa.earnings_id = bb.earnings_id
					and aa.hash <> bb.hash



-- ha, cost center name and display name for me changed from Database Developer to Database Developer (Remote)
-- 7/19/23 cost_center_job_name and cost_center_job_display_name changed from Human Resources Manager to Human Resources Director
--         and cost center display_name from Human Resources Director to Rydell GM/Office/Human Resources Director
-- 08/18 more cost center
-- cost center name from Detail Photo Specialist to Photo Specialist
-- cost center display name from Rydell GM/Detail/Detail Photo Specialist to Rydell GM/Sales/Photo Specialist

select 'payroll' as source, payroll_id,pay_statement_id,employee_account_id,earnings_id,batch_id,
	batch_name,/*hours,*/earning_id,earning_code,earning_name,base_rate,ee_amount,type_name,cost_center_id,
	cost_center_name,cost_center_display_name,cost_center_job_id,cost_center_job_name,cost_center_job_display_name
from ukg.pay_statement_earnings
where earnings_id in (28938276628, 28938276629,28938276630,28938276631,28938278504,28938278505)
union
select 'json', a.payroll_id, a.pay_statement_id, 
	(a.response->'employee'->>'account_id')::bigint as employee_id,
	(b->>'id')::bigint as earnings_id, 
	(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
-- 	(b->>'hours')::BIGINT as hours,
	(b->'earning'->>'id')::integer as earning_id, b->'earning'->>'code' as earning_code, b->'earning'->>'name' as earning_name,
	(b->>'base_rate')::numeric as base_rate, (b->>'ee_amount')::numeric as ee_amount,
	b->>'type_name' as type_name,
	(c->'value'->>'id')::bigint as cost_center_id,
	c->'value'->>'name' as cost_center_name,
	c->'value'->>'display_name' as cost_center_display_name,
	(d->'value'->>'id')::BIGINT as cost_center_job_id,
	d->'value'->>'name' as cost_center_job_name,
	d->'value'->>'display_name' as cost_center_job_display_name
from ukg.json_pay_statement_details a
left join jsonb_array_elements(a.response->'earnings') b on true
left join jsonb_array_elements(b->'cost_centers') c on true 
	and c->>'index' = '0'
left join jsonb_array_elements(b->'cost_centers') d on true 
	and d->>'index' = '9'
where (b->>'id')::bigint in (28938276628, 28938276629,28938276630,28938276631,28938278504,28938278505)                                                                                                                                                                                                     
order by earnings_id, source

update ukg.pay_statement_earnings
set cost_center_name = 'Photo Specialist', cost_center_display_name = 'Rydell GM/Sales/Photo Specialist'
-- select * from ukg.pay_statement_earnings
where earnings_id in (28938268708, 28938268709,28938299685,28938299684,28938299687,28938299686)
----------------------------------------------------------------------------
--</>04/26/23 2 pay_statement_earnings changed
---------------------------------------------------------------------------- 
----------------------------------------------------------------------------
--< 03/09/23 3 pay_statement_earnings changed
----------------------------------------------------------------------------

select *
from ( -- from ukg.pay_statement_earnings
	select earnings_id, 
		(
			select md5(z::text) as hash
			from (
				select payroll_id,pay_statement_id,employee_account_id,earnings_id,batch_id,
					batch_name,hours,earning_id,earning_code,earning_name,base_rate,ee_amount,type_name,cost_center_id,
					cost_center_name,cost_center_display_name,cost_center_job_id,cost_center_job_name,cost_center_job_display_name
				from ukg.pay_statement_earnings
				where earnings_id = a.earnings_id) z)
	from ukg.pay_statement_earnings a) aa
	join (  -- from ukg.json_pay_statement_details
		select earnings_id, md5(a::text) as hash
		from (  
			select a.payroll_id, a.pay_statement_id, 
				(a.response->'employee'->>'account_id')::bigint as employee_id,
				(b->>'id')::bigint as earnings_id, 
				(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
				(b->>'hours')::BIGINT as hours,
				(b->'earning'->>'id')::integer as earning_id, b->'earning'->>'code' as earning_code, b->'earning'->>'name' as earning_name,
				(b->>'base_rate')::numeric as base_rate, (b->>'ee_amount')::numeric as ee_amount,
				b->>'type_name' as type_name,
				(c->'value'->>'id')::bigint as cost_center_id,
				c->'value'->>'name' as cost_center_name,
				c->'value'->>'display_name' as cost_center_display_name,
				(d->'value'->>'id')::BIGINT as cost_center_job_id,
				d->'value'->>'name' as cost_center_job_name,
				d->'value'->>'display_name' as cost_center_job_display_name
			from ukg.json_pay_statement_details a
			left join jsonb_array_elements(a.response->'earnings') b on true
			left join jsonb_array_elements(b->'cost_centers') c on true 
				and c->>'index' = '0'
			left join jsonb_array_elements(b->'cost_centers') d on true 
				and d->>'index' = '9') a) bb on aa.earnings_id = bb.earnings_id
					and aa.hash <> bb.hash
					
-- cost center name chang from Human Resources Manager to Human Resources Director (cost_center_id 51655491148)
select 'payroll' as source, payroll_id,pay_statement_id,employee_account_id,earnings_id,batch_id,
	batch_name,hours,earning_id,earning_code,earning_name,base_rate,ee_amount,type_name,cost_center_id,
	cost_center_name,cost_center_display_name,cost_center_job_id,cost_center_job_name,cost_center_job_display_name
from ukg.pay_statement_earnings
where earnings_id in (28927102448,28927102449)
union
select 'json', a.payroll_id, a.pay_statement_id, 
	(a.response->'employee'->>'account_id')::bigint as employee_id,
	(b->>'id')::bigint as earnings_id, 
	(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
	(b->>'hours')::BIGINT as hours,
	(b->'earning'->>'id')::integer as earning_id, b->'earning'->>'code' as earning_code, b->'earning'->>'name' as earning_name,
	(b->>'base_rate')::numeric as base_rate, (b->>'ee_amount')::numeric as ee_amount,
	b->>'type_name' as type_name,
	(c->'value'->>'id')::bigint as cost_center_id,
	c->'value'->>'name' as cost_center_name,
	c->'value'->>'display_name' as cost_center_display_name,
	(d->'value'->>'id')::BIGINT as cost_center_job_id,
	d->'value'->>'name' as cost_center_job_name,
	d->'value'->>'display_name' as cost_center_job_display_name
from ukg.json_pay_statement_details a
left join jsonb_array_elements(a.response->'earnings') b on true
left join jsonb_array_elements(b->'cost_centers') c on true 
	and c->>'index' = '0'
left join jsonb_array_elements(b->'cost_centers') d on true 
	and d->>'index' = '9'
where (b->>'id')::bigint in (28927102448,28927102449)                                                                                                                                                                                                                
order by earnings_id, source

update ukg.pay_statement_earnings
set cost_center_job_name = 'Porter',
  cost_center_job_display_name = 'Porter'
-- select * from ukg.pay_statement_earnings
where earnings_id in (28927102448,28927102449)

----------------------------------------------------------------------------
--/> 03/09/23 3 pay_statement_earnings changed
----------------------------------------------------------------------------					
----------------------------------------------------------------------------
--< 02/02/23 payrolls changed
----------------------------------------------------------------------------
--from function ukg.payroll_cdc(), exposing the changed payrolls
-- all 3 were changed from CLOSED to FINALIZED
-- this shows which payrolls have changed
		select *
		from ( -- from ukg.payrolls 
			select payroll_id, 
				(
					select md5(z::text) as hash
					from (
						select payroll_id,payroll_name,payroll_start_date,payroll_end_date,pay_date,payroll_type,status,ein_name,billable 
						from ukg.payrolls
						where payroll_id = a.payroll_id) z)
			from ukg.payrolls a) aa
		join (  -- from ukg.json_payrolls
			select payroll_id, md5(a::text) as hash
			from (  
				select (b->>'id')::integer as payroll_id,
					b->>'name' as payroll_name,
					coalesce((b->>'start_date')::date, '12/31/9999'::date) as payroll_start_date, 
					coalesce((b->>'end_date')::date, '12/31/9999'::date) as payroll_end_date, 
					coalesce((b->>'pay_date')::date, '12/31/9999'::date) as pay_date,
					b->'type'->>'display_name' as payroll_type,
					b->>'status',
					b->'ein'->>'ein_name' as ein_name,
					(b->>'billable')::boolean
				from ukg.json_payrolls a
				join jsonb_array_elements(a.response->'payrolls') as b(payrolls) on true) a) bb on aa.payroll_id = bb.payroll_id
					and aa.hash <> bb.hash

-- what has changed in the payrolls from the above query
select 'payroll' as source, * from ukg.payrolls where payroll_id in(109682920) ----------------------
union 
select 'json', (b->>'id')::integer as payroll_id,
		b->>'name' as payroll_name,
		coalesce((b->>'start_date')::date, '12/31/9999'::date) as payroll_start_date, 
		coalesce((b->>'end_date')::date, '12/31/9999'::date) as payroll_end_date, 
		coalesce((b->>'pay_date')::date, '12/31/9999'::date) as pay_date,
		b->'type'->>'display_name' as payroll_type,
		b->>'status',
		b->'ein'->>'ein_name' as ein_name,
		(b->>'billable')::boolean
	from ukg.json_payrolls a
	join jsonb_array_elements(a.response->'payrolls') as b(payrolls) on true
	where (b->>'id')::integer in (109933508,109933511,109933513) ---------------------------------------------------------
order by payroll_id, source

-- these are typical on the 1st of the month, the status changing from open to finalized
update ukg.payrolls
set status = 'FINALIZED'
-- select * from ukg.payrolls
where payroll_id in(109933508,109933511,109933513); ---------------------------------------------------------------

----------------------------------------------------------------------------
--/> 02/02/23 3 pay_statement_earnings changed
----------------------------------------------------------------------------

----------------------------------------------------------------------------
--< 01/06/23 2 pay_statement_earnings changed
----------------------------------------------------------------------------

select *
from ( -- from ukg.pay_statement_earnings
	select earnings_id, 
		(
			select md5(z::text) as hash
			from (
				select payroll_id,pay_statement_id,employee_account_id,earnings_id,batch_id,
					batch_name,hours,earning_id,earning_code,earning_name,base_rate,ee_amount,type_name,cost_center_id,
					cost_center_name,cost_center_display_name,cost_center_job_id,cost_center_job_name,cost_center_job_display_name
				from ukg.pay_statement_earnings
				where earnings_id = a.earnings_id) z)
	from ukg.pay_statement_earnings a) aa
	join (  -- from ukg.json_pay_statement_details
		select earnings_id, md5(a::text) as hash
		from (  
			select a.payroll_id, a.pay_statement_id, 
				(a.response->'employee'->>'account_id')::bigint as employee_id,
				(b->>'id')::bigint as earnings_id, 
				(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
				(b->>'hours')::BIGINT as hours,
				(b->'earning'->>'id')::integer as earning_id, b->'earning'->>'code' as earning_code, b->'earning'->>'name' as earning_name,
				(b->>'base_rate')::numeric as base_rate, (b->>'ee_amount')::numeric as ee_amount,
				b->>'type_name' as type_name,
				(c->'value'->>'id')::bigint as cost_center_id,
				c->'value'->>'name' as cost_center_name,
				c->'value'->>'display_name' as cost_center_display_name,
				(d->'value'->>'id')::BIGINT as cost_center_job_id,
				d->'value'->>'name' as cost_center_job_name,
				d->'value'->>'display_name' as cost_center_job_display_name
			from ukg.json_pay_statement_details a
			left join jsonb_array_elements(a.response->'earnings') b on true
			left join jsonb_array_elements(b->'cost_centers') c on true 
				and c->>'index' = '0'
			left join jsonb_array_elements(b->'cost_centers') d on true 
				and d->>'index' = '9') a) bb on aa.earnings_id = bb.earnings_id
					and aa.hash <> bb.hash

-- spelling error in cost_center_job_name & cost_center_job_display_name
select 'payroll' as source, payroll_id,pay_statement_id,employee_account_id,earnings_id,batch_id,
	batch_name,hours,earning_id,earning_code,earning_name,base_rate,ee_amount,type_name,cost_center_id,
	cost_center_name,cost_center_display_name,cost_center_job_id,cost_center_job_name,cost_center_job_display_name
from ukg.pay_statement_earnings
where earnings_id in (28719833127, 28719834880)
union
select 'json', a.payroll_id, a.pay_statement_id, 
	(a.response->'employee'->>'account_id')::bigint as employee_id,
	(b->>'id')::bigint as earnings_id, 
	(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
	(b->>'hours')::BIGINT as hours,
	(b->'earning'->>'id')::integer as earning_id, b->'earning'->>'code' as earning_code, b->'earning'->>'name' as earning_name,
	(b->>'base_rate')::numeric as base_rate, (b->>'ee_amount')::numeric as ee_amount,
	b->>'type_name' as type_name,
	(c->'value'->>'id')::bigint as cost_center_id,
	c->'value'->>'name' as cost_center_name,
	c->'value'->>'display_name' as cost_center_display_name,
	(d->'value'->>'id')::BIGINT as cost_center_job_id,
	d->'value'->>'name' as cost_center_job_name,
	d->'value'->>'display_name' as cost_center_job_display_name
from ukg.json_pay_statement_details a
left join jsonb_array_elements(a.response->'earnings') b on true
left join jsonb_array_elements(b->'cost_centers') c on true 
	and c->>'index' = '0'
left join jsonb_array_elements(b->'cost_centers') d on true 
	and d->>'index' = '9'
where (b->>'id')::bigint in (28719833127, 28719834880)	
order by earnings_id, source

update ukg.pay_statement_earnings
set cost_center_job_name = 'Systems Administrator', cost_center_job_display_name = 'Systems Administrator'
-- select * from ukg.pay_statement_earnings
where earnings_id in (28719833127, 28719834880)
----------------------------------------------------------------------------
--/> 01/06/23 1 pay_statement_earnings changed
----------------------------------------------------------------------------		
----------------------------------------------------------------------------
--< 01/04/23 1 deduction changes
----------------------------------------------------------------------------
select * 
		from ( -- from ukg.pay_statement_deductions
			select deductions_id, 
				(
					select md5(z::text) as hash
					from (
						select payroll_id,pay_statement_id,employee_account_id,deductions_id,deduction_code,
							deduction_name,ee_amount,er_amount,type_name
						from ukg.pay_statement_deductions
						where deductions_id = a.deductions_id) z)
			from ukg.pay_statement_deductions a) aa
			join (  -- from ukg.json_pay_statement_details
				select deductions_id, md5(a::text) as hash
				from (  
					select a.payroll_id, a.pay_statement_id, 
						(a.response->'employee'->>'account_id')::bigint as employee_account_id,
						(b->>'id')::bigint as deductions_id, 
						b->'deduction'->>'code' as deduction_code, b->'deduction'->>'name' as deduction_name,
						(b->>'ee_amount')::numeric as ee_amount, (b->>'er_amount')::numeric as er_amount,
						b->>'type_name' as type_name
					from ukg.json_pay_statement_details a
					join jsonb_array_elements(a.response->'deductions') b on true) a) bb on aa.deductions_id = bb.deductions_id
							and aa.hash <> bb.hash;
							
-- craig croaker, deduction (AR) changed from 1244.56 to 614.07							
select 'payroll' as source, payroll_id,pay_statement_id,employee_account_id,deductions_id as deductions_id_1,deduction_code,
	deduction_name,ee_amount,er_amount,type_name
from ukg.pay_statement_deductions
where deductions_id in (28730196810)
UNION
select 'json', a.payroll_id, a.pay_statement_id, 
	(a.response->'employee'->>'account_id')::bigint as employee_account_id,
	(b->>'id')::bigint as deductions_id, 
	b->'deduction'->>'code' as deduction_code, b->'deduction'->>'name' as deduction_name,
	(b->>'ee_amount')::numeric as ee_amount, (b->>'er_amount')::numeric as er_amount,
	b->>'type_name' as type_name
from ukg.json_pay_statement_details a
join jsonb_array_elements(a.response->'deductions') b on true					
WHERE (b->>'id')::bigint in (28730196810)
order by deductions_id_1, source

update ukg.pay_statement_deductions
set ee_amount = 614.07
-- select * from ukg.pay_statement_deductions
where deductions_id = 28730196810;

and as a result, the pay_statement changes as well 

select *
from ( -- from ukg.pay_statement_payments
	select payments_id, 
		(
			select md5(z::text) as hash
			from (
				select payroll_id,pay_statement_id,employee_account_id,payments_id,ee_amount,type_name
				from ukg.pay_statement_payments
				where payments_id = a.payments_id) z)
	from ukg.pay_statement_payments a) aa
	join (  -- from ukg.json_pay_statement_details
		select payments_id, md5(a::text) as hash
		from (  
			select a.payroll_id, a.pay_statement_id, 
				(a.response->'employee'->>'account_id')::bigint as employee_account_id,
				(b->>'id')::bigint as payments_id, 
				(b->>'ee_amount')::numeric as ee_amount,
				b->>'type_name' as type_name
			from ukg.json_pay_statement_details a
			join jsonb_array_elements(a.response->'payments') b on true ) a) bb on aa.payments_id = bb.payments_id
					and aa.hash <> bb.hash

select 'payroll' as source, payroll_id,pay_statement_id,employee_account_id,payments_id,ee_amount,type_name
from ukg.pay_statement_payments
where payments_id = 28730141022
union
select 'json', a.payroll_id, a.pay_statement_id, 
	(a.response->'employee'->>'account_id')::bigint as employee_account_id,
	(b->>'id')::bigint as payments_id, 
	(b->>'ee_amount')::numeric as ee_amount,
	b->>'type_name' as type_name
from ukg.json_pay_statement_details a
join jsonb_array_elements(a.response->'payments') b on true 
  and (b->>'id')::bigint = 28730141022		

select * from ukg.employees where primary_account_id = 12987528581  	

update ukg.pay_statement_payments
set ee_amount = 6941.87
-- select * from ukg.pay_statement_payments
where payments_id = 28730141022;
----------------------------------------------------------------------------
--/> 01/04/23 1 deduction changes
----------------------------------------------------------------------------
----------------------------------------------------------------------------
--< 01/01/23 9 deduction changes
----------------------------------------------------------------------------
-- shows all the changed deductions
select * 
		from ( -- from ukg.pay_statement_deductions
			select deductions_id, 
				(
					select md5(z::text) as hash
					from (
						select payroll_id,pay_statement_id,employee_account_id,deductions_id,deduction_code,
							deduction_name,ee_amount,er_amount,type_name
						from ukg.pay_statement_deductions
						where deductions_id = a.deductions_id) z)
			from ukg.pay_statement_deductions a) aa
			join (  -- from ukg.json_pay_statement_details
				select deductions_id, md5(a::text) as hash
				from (  
					select a.payroll_id, a.pay_statement_id, 
						(a.response->'employee'->>'account_id')::bigint as employee_account_id,
						(b->>'id')::bigint as deductions_id, 
						b->'deduction'->>'code' as deduction_code, b->'deduction'->>'name' as deduction_name,
						(b->>'ee_amount')::numeric as ee_amount, (b->>'er_amount')::numeric as er_amount,
						b->>'type_name' as type_name
					from ukg.json_pay_statement_details a
					join jsonb_array_elements(a.response->'deductions') b on true) a) bb on aa.deductions_id = bb.deductions_id
							and aa.hash <> bb.hash;

-- this shows the detail on the changed deductions
-- deduction_code renamed from flex_credit_pretax to FSA Dependent Care and deduction_code from 
select 'payroll' as source, payroll_id,pay_statement_id,employee_account_id,deductions_id as deductions_id_1,deduction_code,
	deduction_name,ee_amount,er_amount,type_name
from ukg.pay_statement_deductions
where deductions_id in (28719819358, 28719819813,28719820275,28719821753,28719829644,28719831852,28719834397,28719315160,28719315148)
UNION
select 'json', a.payroll_id, a.pay_statement_id, 
	(a.response->'employee'->>'account_id')::bigint as employee_account_id,
	(b->>'id')::bigint as deductions_id, 
	b->'deduction'->>'code' as deduction_code, b->'deduction'->>'name' as deduction_name,
	(b->>'ee_amount')::numeric as ee_amount, (b->>'er_amount')::numeric as er_amount,
	b->>'type_name' as type_name
from ukg.json_pay_statement_details a
join jsonb_array_elements(a.response->'deductions') b on true					
WHERE (b->>'id')::bigint in (28719819358, 28719819813,28719820275,28719821753,28719829644,28719831852,28719834397,28719315160,28719315148)
order by deductions_id_1, source

-- and the fixx
update ukg.pay_statement_deductions
set deduction_name = 'FSA Dependent Care', deduction_code = 'FSA Dependent'
-- select * from ukg.pay_statement_deductions
where deductions_id in (28719819358, 28719819813,28719820275,28719821753,28719829644,28719831852,28719834397,28719315160,28719315148)
----------------------------------------------------------------------------
--/> 01/01/23 9 deduction changes
----------------------------------------------------------------------------			
----------------------------------------------------------------------------
--< 03/02/22 payroll changes, 2 payrolls where status changed from OPEN to FINALIZED
----------------------------------------------------------------------------
--from function ukg.payroll_cdc(), exposing the changed payrolls
-- 		select count(*)::integer 
-- this shows which payrolls have changed
		select *
		from ( -- from ukg.payrolls 
			select payroll_id, 
				(
					select md5(z::text) as hash
					from (
						select payroll_id,payroll_name,payroll_start_date,payroll_end_date,pay_date,payroll_type,status,ein_name,billable 
						from ukg.payrolls
						where payroll_id = a.payroll_id) z)
			from ukg.payrolls a) aa
		join (  -- from ukg.json_payrolls
			select payroll_id, md5(a::text) as hash
			from (  
				select (b->>'id')::integer as payroll_id,
					b->>'name' as payroll_name,
					coalesce((b->>'start_date')::date, '12/31/9999'::date) as payroll_start_date, 
					coalesce((b->>'end_date')::date, '12/31/9999'::date) as payroll_end_date, 
					coalesce((b->>'pay_date')::date, '12/31/9999'::date) as pay_date,
					b->'type'->>'display_name' as payroll_type,
					b->>'status',
					b->'ein'->>'ein_name' as ein_name,
					(b->>'billable')::boolean
				from ukg.json_payrolls a
				join jsonb_array_elements(a.response->'payrolls') as b(payrolls) on true) a) bb on aa.payroll_id = bb.payroll_id
					and aa.hash <> bb.hash

-- what has changed in the payrolls from the above query
select 'payroll' as source, * from ukg.payrolls where payroll_id in(109665399) ----------------------
union 
select 'json', (b->>'id')::integer as payroll_id,
		b->>'name' as payroll_name,
		coalesce((b->>'start_date')::date, '12/31/9999'::date) as payroll_start_date, 
		coalesce((b->>'end_date')::date, '12/31/9999'::date) as payroll_end_date, 
		coalesce((b->>'pay_date')::date, '12/31/9999'::date) as pay_date,
		b->'type'->>'display_name' as payroll_type,
		b->>'status',
		b->'ein'->>'ein_name' as ein_name,
		(b->>'billable')::boolean
	from ukg.json_payrolls a
	join jsonb_array_elements(a.response->'payrolls') as b(payrolls) on true
	where (b->>'id')::integer in (109665399) ---------------------------------------------------------
order by payroll_id, source

-- these are typical on the 1st of the month, the status changing from open to finalized
update ukg.payrolls
set status = 'FINALIZED'
-- select * from ukg.payrolls
where payroll_id in(109665399); ---------------------------------------------------------------

----------------------------------------------------------------------------
--/> 03/02/22 payroll changes, 2 payrolls where status changed from OPEN to FINALIZED
----------------------------------------------------------------------------

/*
2/18/22: jeri updated mispelled jobs/cost centers and earning codes, 616 rows changed
12/28/22: cost center changed for Craig Rogne and Mitch Rogers
*/
		select *
		from ( -- from ukg.pay_statement_earnings
			select earnings_id, 
				(
					select md5(z::text) as hash
					from (
						select payroll_id,pay_statement_id,employee_account_id,earnings_id,batch_id,
							batch_name,hours,earning_id,earning_code,earning_name,base_rate,ee_amount,type_name,cost_center_id,
							cost_center_name,cost_center_display_name,cost_center_job_id,cost_center_job_name,cost_center_job_display_name
						from ukg.pay_statement_earnings
						where earnings_id = a.earnings_id) z)
			from ukg.pay_statement_earnings a) aa
			join (  -- from ukg.json_pay_statement_details
				select earnings_id, md5(a::text) as hash
				from (  
					select a.payroll_id, a.pay_statement_id, 
						(a.response->'employee'->>'account_id')::bigint as employee_id,
						(b->>'id')::bigint as earnings_id, 
						(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
						(b->>'hours')::BIGINT as hours,
						(b->'earning'->>'id')::integer as earning_id, b->'earning'->>'code' as earning_code, b->'earning'->>'name' as earning_name,
						(b->>'base_rate')::numeric as base_rate, (b->>'ee_amount')::numeric as ee_amount,
						b->>'type_name' as type_name,
						(c->'value'->>'id')::bigint as cost_center_id,
						c->'value'->>'name' as cost_center_name,
						c->'value'->>'display_name' as cost_center_display_name,
						(d->'value'->>'id')::BIGINT as cost_center_job_id,
						d->'value'->>'name' as cost_center_job_name,
						d->'value'->>'display_name' as cost_center_job_display_name
					from ukg.json_pay_statement_details a
					left join jsonb_array_elements(a.response->'earnings') b on true
					left join jsonb_array_elements(b->'cost_centers') c on true 
						and c->>'index' = '0'
					left join jsonb_array_elements(b->'cost_centers') d on true 
						and d->>'index' = '9') a) bb on aa.earnings_id = bb.earnings_id
							and aa.hash <> bb.hash

			
-- all the affected earnings_id
drop table if exists ids;
create temp table ids as
		select aa.earnings_id
		from ( -- from ukg.pay_statement_earnings
			select earnings_id, 
				(
					select md5(z::text) as hash
					from (
						select payroll_id,pay_statement_id,employee_account_id,earnings_id,batch_id,
							batch_name,hours,earning_id,earning_code,earning_name,base_rate,ee_amount,type_name,cost_center_id,
							cost_center_name,cost_center_display_name,cost_center_job_id,cost_center_job_name,cost_center_job_display_name
						from ukg.pay_statement_earnings
						where earnings_id = a.earnings_id) z)
			from ukg.pay_statement_earnings a) aa
			join (  -- from ukg.json_pay_statement_details
				select earnings_id, md5(a::text) as hash
				from (  
					select a.payroll_id, a.pay_statement_id, 
						(a.response->'employee'->>'account_id')::bigint as employee_id,
						(b->>'id')::bigint as earnings_id, 
						(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
						(b->>'hours')::BIGINT as hours,
						(b->'earning'->>'id')::integer as earning_id, b->'earning'->>'code' as earning_code, b->'earning'->>'name' as earning_name,
						(b->>'base_rate')::numeric as base_rate, (b->>'ee_amount')::numeric as ee_amount,
						b->>'type_name' as type_name,
						(c->'value'->>'id')::bigint as cost_center_id,
						c->'value'->>'name' as cost_center_name,
						c->'value'->>'display_name' as cost_center_display_name,
						(d->'value'->>'id')::BIGINT as cost_center_job_id,
						d->'value'->>'name' as cost_center_job_name,
						d->'value'->>'display_name' as cost_center_job_display_name
					from ukg.json_pay_statement_details a
					left join jsonb_array_elements(a.response->'earnings') b on true
					left join jsonb_array_elements(b->'cost_centers') c on true 
						and c->>'index' = '0'
					left join jsonb_array_elements(b->'cost_centers') d on true 
						and d->>'index' = '9') a) bb on aa.earnings_id = bb.earnings_id
							and aa.hash <> bb.hash			

-- shows the differences (entire row) between existing data (ukg) and the most recent json scrape (json) unionedu
select dd.last_name, dd.first_name, ee.*, cc.* 
-- select distinct cost_center_id 
from (
select 'ukg' as source, a.*
from ukg.pay_statement_earnings a
join ids b on a.earnings_id = b.earnings_id							
union
select aa.*
from (
					select 'json' as source, a.payroll_id, a.pay_statement_id, 
						(a.response->'employee'->>'account_id')::bigint as employee_id,
						(b->>'id')::bigint as earnings_id, 
						(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
						(b->>'hours')::BIGINT as hours,
						(b->'earning'->>'id')::integer as earning_id, b->'earning'->>'code' as earning_code, b->'earning'->>'name' as earning_name,
						(b->>'base_rate')::numeric as base_rate, (b->>'ee_amount')::numeric as ee_amount,
						b->>'type_name' as type_name,
						(c->'value'->>'id')::bigint as cost_center_id,
						c->'value'->>'name' as cost_center_name,
						c->'value'->>'display_name' as cost_center_display_name,
						(d->'value'->>'id')::BIGINT as cost_center_job_id,
						d->'value'->>'name' as cost_center_job_name,
						d->'value'->>'display_name' as cost_center_job_display_name
					from ukg.json_pay_statement_details a
					left join jsonb_array_elements(a.response->'earnings') b on true
					left join jsonb_array_elements(b->'cost_centers') c on true 
						and c->>'index' = '0'
					left join jsonb_array_elements(b->'cost_centers') d on true 
						and d->>'index' = '9') aa
join ids bb on aa.earnings_id = bb.earnings_id) cc
left join ukg.employees dd on cc.employee_account_id = dd.employee_id					
left join ukg.payrolls ee on cc.payroll_id = ee.payroll_id
-- order by cost_center_id
order by earnings_id, employee_account_id, source			



-- 02/16/22 fix the incorrect earning code in the existing data
update ukg.pay_statement_earnings x
-- set cost_center_name = y.cost_center_name, cost_center_display_name = y.cost_center_display_name
set cost_center_job_name = y.cost_center_job_name,
    cost_center_job_display_name = y.cost_center_job_display_name
from (
		select 'json' as source, a.payroll_id, a.pay_statement_id, 
			(a.response->'employee'->>'account_id')::bigint as employee_id,
			(b->>'id')::bigint as earnings_id, 
			(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
			(b->>'hours')::BIGINT as hours,
			(b->'earning'->>'id')::integer as earning_id, b->'earning'->>'code' as earning_code, b->'earning'->>'name' as earning_name,
			(b->>'base_rate')::numeric as base_rate, (b->>'ee_amount')::numeric as ee_amount,
			b->>'type_name' as type_name,
			(c->'value'->>'id')::bigint as cost_center_id,
			c->'value'->>'name' as cost_center_name,
			c->'value'->>'display_name' as cost_center_display_name,
			(d->'value'->>'id')::BIGINT as cost_center_job_id,
			d->'value'->>'name' as cost_center_job_name,
			d->'value'->>'display_name' as cost_center_job_display_name
		from ukg.json_pay_statement_details a
		left join jsonb_array_elements(a.response->'earnings') b on true
		left join jsonb_array_elements(b->'cost_centers') c on true 
			and c->>'index' = '0'
		left join jsonb_array_elements(b->'cost_centers') d on true 
			and d->>'index' = '9') y
where x.employee_account_id = y.employee_id
  and x.earnings_id = y.earnings_id			
  and (x.cost_center_job_name <> y.cost_center_job_name or x.cost_center_job_display_name <> y.cost_center_job_display_name)


-- 04/05  31 failures in ukg.pay_statement_earnings
-- 12/29/2022
-- cost_center_name and cost_center_display_name changed from Body Shop Intern to Body Shop Intern Technician
update ukg.pay_statement_earnings x
set cost_center_name = y.cost_center_name, 
    cost_center_display_name = y.cost_center_display_name
from (
		select 'json' as source, a.payroll_id, a.pay_statement_id, 
			(a.response->'employee'->>'account_id')::bigint as employee_id,
			(b->>'id')::bigint as earnings_id, 
			(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
			(b->>'hours')::BIGINT as hours,
			(b->'earning'->>'id')::integer as earning_id, b->'earning'->>'code' as earning_code, b->'earning'->>'name' as earning_name,
			(b->>'base_rate')::numeric as base_rate, (b->>'ee_amount')::numeric as ee_amount,
			b->>'type_name' as type_name,
			(c->'value'->>'id')::bigint as cost_center_id,
			c->'value'->>'name' as cost_center_name,
			c->'value'->>'display_name' as cost_center_display_name,
			(d->'value'->>'id')::BIGINT as cost_center_job_id,
			d->'value'->>'name' as cost_center_job_name,
			d->'value'->>'display_name' as cost_center_job_display_name
		from ukg.json_pay_statement_details a
		left join jsonb_array_elements(a.response->'earnings') b on true
		left join jsonb_array_elements(b->'cost_centers') c on true 
			and c->>'index' = '0'
		left join jsonb_array_elements(b->'cost_centers') d on true 
			and d->>'index' = '9') y
where x.employee_account_id = y.employee_id
  and x.earnings_id = y.earnings_id			
  and (x.cost_center_name <> y.cost_center_name or x.cost_center_job_display_name <> y.cost_center_job_display_name)