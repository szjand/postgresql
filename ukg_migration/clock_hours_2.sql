﻿-- clock_times needs to include paid & unpaid time off
-- sometime Paid Time Off has start/end_times, sometime it doesen't
-- BUT in all cases Paid Time Off has a total value and unpaid time off (unpaid lunch, unpaid break) has a null total 
-- leaning toward, use start/end times if they exist, otherwise total
-- 12/25 decided to store a lot of attributes in clock_times, probably/maybe won't need them, too early to say 


-- ukg.ext_time_entries -> ukg.ext_clock_times
create or replace function ukg.ext_clock_times()
returns void as
$BODY$
/*
select ukg.ext_clock_times();
*/
	delete 
	from ukg.ext_clock_times
	where the_date between current_date - 31 and current_date -1;

	insert into ukg.ext_clock_times
	select 
	 case
		 when b.ein_name = 'Rydell Auto Center, Inc.' then 'GM'
		 when b.ein_name = 'H.G.F., Inc.' then 'HN'
		 else 'XX'
	 end as store,
	 b.last_name, b.first_name, b.employee_number, b.employee_id, 
	 a.start_date, a.end_date, a.time_entry_id, a.time_entry_type, a.the_date, a.start_time, a.end_time,
	 a.total, a.time_off_id, c.name as timeoff_name, a.is_raw, a.is_calc, a.calc_start_time, a.calc_end_time, a.calc_total,
	 a.cost_centers 
	from ukg.ext_time_entries a
	join ukg.ext_employees b on a.employee_account_id = b.employee_id
	left join ukg.ext_timeoffs c on a.time_off_id = c.id
	where a.the_date between current_date - 31 and current_date -1;
$BODY$
language sql;


-- 1 row per emp/date
-- first cut of first half of clock hours (pre ot)
-- looks pretty pretty good
-- switching to calc_times
-- exception: reuter 12/27 is_raw:t is_calc:f, calc_times are null
-- ukg.ext_clock_times --> ukg.clock_hours_tmp

create or replace function ukg.update_clock_hours_tmp()
returns void as
$BODY$
/*  
  select ukg.update_clock_hours_tmp();
*/
delete 
from ukg.clock_hours_tmp
where the_date between current_date - 31 and current_date -1;

insert into ukg.clock_hours_tmp
select store, last_name, first_name, employee_id, employee_number, the_date,
  sum(clock_hours) as clock_hours, 
  sum(pto_hours) as pto_hours,
  sum(pto_payout_hours) as pto_payout_hours,
  sum(hol_hours) as hol_hours
from (  
	select a.store, a.last_name, a.first_name, a.employee_id, a.employee_number, a.the_date,
		a.timeoff_name, a.start_time, a.end_time, a.total,
		coalesce(
			case
			  -- straight clock time
				when start_time is not null and end_time is not null and timeoff_name is null then 
					((extract(epoch from a.end_time) - extract(epoch from a.start_time))/3600.0)::numeric(6,2)
			  -- no clock out
				when start_time is not null and end_time is null then -1
				-- raw total time only entered
				when start_time is null and end_time is null and timeoff_name is null then (total/3600000.0)::numeric(6,2)
			end, 0) as clock_hours,
		coalesce(case when timeoff_name = 'Paid Time Off' then (total/3600000.0)::numeric(6,2) end, 0) as pto_hours,
		coalesce(case when timeoff_name = 'PTO Payout' then (total/3600000.0)::numeric(6,2) end, 0) as pto_payout_hours,
		coalesce(case when timeoff_name = 'Holiday' then (total/3600000.0)::numeric(6,2) end, 0) as hol_hours
	from ukg.ext_clock_times a
	where (start_time is not null or total is not null)
	  and the_date between current_date - 31 and current_date -1 ) aa
group by store, last_name, first_name, employee_id, employee_number, the_date;
$BODY$
language sql;



-- ukg.clock_hours_tmp --> ukg.clock_hours
create or replace function ukg.update_clock_hours()
returns void as
$BODY$
/*
  select ukg.update_clock_hours();
*/
delete 
from ukg.clock_hours
where the_date between current_date - 31 and current_date -1;

insert into ukg.clock_hours
select aa.store, aa.last_name, aa.first_name, aa.employee_id, aa.employee_number, 
	aa.the_date, aa.clock_hours, 
  bb.reg_hours, bb.ot_hours,
  aa.pto_hours, aa.pto_payout_hours,
  aa.hol_hours  
from ukg.clock_hours_tmp aa
left join ( -- bb: reg/ot hours
  select a.store, a.employee_number, a.the_week, a.the_date,
    round(
      case
        when coalesce(sum(b.clock_hours), 0) + a.clock_hours < 40 then a.clock_hours
        when coalesce(sum(b.clock_hours), 0) > 40 then 0
        else 40 - coalesce(sum(b.clock_hours), 0)
      end, 2) as reg_hours,
    round(
      case
        when coalesce(sum(b.clock_hours), 0) >= 40 then a.clock_hours
        when coalesce(sum(b.clock_hours), 0) + a.clock_hours < 40 then 0
        else a.clock_hours - (40 -coalesce(sum(b.clock_hours), 0))
      end, 2) as ot_hours  
  from ( -- a
    select b.sunday_to_saturday_week as the_week, a.* 
    from ukg.clock_hours_tmp a
    join dds.dim_date b on a.the_date = b.the_date
    where a.the_date between current_date - 31 and current_date -1) a  
  left join ( -- b
    select b.sunday_to_saturday_week as the_week, a.* 
    from ukg.clock_hours_tmp a
    join dds.dim_date b on a.the_date = b.the_date
    where a.the_date between current_date - 31 and current_date -1) b on a.employee_number = b.employee_number
      and a.the_week = b.the_week
      and b.the_date < a.the_date  -- this is the magix   
  group by a.store, a.the_week, a.employee_number, a.the_date, a.clock_hours) bb on aa.store = bb.store
    and aa.employee_number = bb.employee_number
    and aa.the_date = bb.the_date 
where aa.the_date between current_date - 31 and current_date -1;
$BODY$
language sql;    