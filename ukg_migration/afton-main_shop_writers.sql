﻿-- First query -  afton.test_all_pp_dates(337)
do $$
declare
  _pp integer := ( -- pay period indicator
    select distinct biweekly_pay_period_sequence
    from dds.dim_date
    where '11/07/2021' between biweekly_pay_period_start_date and biweekly_pay_period_end_date);
begin  
drop table if exists wtf;
create temp table wtf as
  select min(the_date) as first_day, max(the_date) as sec_sat, min(the_date) filter (where day_of_week = 7) as first_sat
  from dds.dim_date
  where biweekly_pay_period_sequence = _pp;
end $$;
select * from wtf;


-- Second query - afton.get_record_dates(337,'11/01/2021', '11/08/2021')
do $$
declare 
	_pay_period_id integer := (
    select distinct biweekly_pay_period_sequence
    from dds.dim_date
    where '11/07/2021' between biweekly_pay_period_start_date and biweekly_pay_period_end_date);	
	_hire_date date := '11/15/2020';
	_term_date date := '11/30/2021';
begin
  drop table if exists wtf;
  create temp table wtf as
	select  1,
		case 
			when _hire_date < beg_pp and _term_date > sec_sat then first_sat
			when _hire_date < beg_pp and _term_date between beg_pp and first_sat then _term_date
			when _hire_date < beg_pp and _term_date > first_sat and _term_date <= sec_sat then first_sat
			when _hire_date between beg_pp and first_sat and _term_date > sec_sat then first_sat
			when _hire_date between beg_pp and first_sat and _term_date between beg_pp and first_sat then _term_date
			when _hire_date between beg_pp and first_sat and _term_date > first_sat and _term_date <= sec_sat then first_sat
			when _hire_date > first_sat and _hire_date <= sec_sat and _term_date > first_sat and _term_date <= sec_sat then _term_date
		end as first_record_date
	from afton.test_all_pp_dates(_pay_period_id)
	union
	select 2,
		case 
			when _hire_date < beg_pp and _term_date > sec_sat then sec_sat
			when _hire_date < beg_pp and _term_date > first_sat and _term_date <= sec_sat then _term_date
			when _hire_date between beg_pp and first_sat and _term_date > sec_sat then sec_sat
			when _hire_date between beg_pp and first_sat and _term_date > first_sat and _term_date <= sec_sat then _term_date
			when _hire_date > first_sat and _hire_date <= sec_sat and _term_date > sec_sat then sec_sat
			when _hire_date < beg_pp and _term_date > sec_sat then first_sat
			when _hire_date < beg_pp and _term_date between beg_pp and first_sat then _term_date
			when _hire_date < beg_pp and _term_date > first_sat and _term_date <= sec_sat then first_sat
			when _hire_date between beg_pp and first_sat and _term_date > sec_sat then first_sat
			when _hire_date between beg_pp and first_sat and _term_date between beg_pp and first_sat then _term_date
			when _hire_date between beg_pp and first_sat and _term_date > first_sat and _term_date <= sec_sat then first_sat
			when _hire_date > first_sat and _hire_date <= sec_sat and _term_date > first_sat and _term_date <= sec_sat then _term_date
		end second_record_date
	from afton.test_all_pp_dates(_pay_period_id);
end $$;
select * from wtf;		

-- You can use this query to test all possible date options

select * from sap.payroll_submittals where pay_period_id = 337

with employees_comm_half as (
select 1 as id, employee_number, round(commission_pay/2, 2) as half_pay, commission_pay as total_pay
from sap.payroll_submittals
where pay_period_id = 337
union
select 2 as id, employee_number,   commission_pay - round(commission_pay/2, 2) as other_half_pay, commission_pay
from sap.payroll_submittals
where pay_period_id = 337 )
  select employee_number as employee_id, 
  'Rydell Auto Center, Inc.' as EIN_name, 
  record_date,  
  'ADD' as operation, 'Biweekly Commission' as counter_name, sum(half_pay) as value
  from employees_comm_half a
  left join arkona.ext_pymast c on a.employee_number = c.pymast_employee_number
  -- HARD CODED OPTION
 -- inner join afton.get_record_dates(337,arkona.db2_integer_to_date(c.hire_date), '11/09/2021') b  on a.id = b.record_id
  -- REAL
  inner join afton.get_record_dates(337,arkona.db2_integer_to_date(c.hire_date), arkona.db2_integer_to_date(c.termination_date) ) b on a.id = b.record_id
  group by employee_number, record_date
  order by employee_number
------------------------------------------------------------------------------------------------
--< tested with an actual in pay period term date for 1 employee, look ok
------------------------------------------------------------------------------------------------
with 
	employees_comm_half as (
		select 1 as id, employee_number, round(commission_pay/2, 2) as half_pay, commission_pay as total_pay
		from sap.payroll_submittals
		where pay_period_id = 337

		union

		select 2 as id, employee_number,   commission_pay - round(commission_pay/2, 2) as other_half_pay, commission_pay
		from sap.payroll_submittals
		where pay_period_id = 337 )

  select a.employee_number as employee_id, 
  'Rydell Auto Center, Inc.' as EIN_name, 
--   arkona.db2_integer_to_date(c.hire_date) as hire_date,
--   arkona.db2_integer_to_date(c.termination_date) as term_date,
  record_date,  
  'ADD' as operation, 'Biweekly Commission' as counter_name, sum(half_pay) as value
  from employees_comm_half a
--   left join arkona.ext_pymast c on a.employee_number = c.pymast_employee_number
  left join one_termed_employee c on a.employee_number = c.employee_number
  -- HARD CODED OPTION
  join afton.get_record_dates(337,c.hire_date, c.term_date) b  on a.id = b.record_id
--   inner join afton.get_record_dates(337,arkona.db2_integer_to_date(c.hire_date), '11/08/2021') b  on a.id = b.record_id
  -- REAL
--   inner join afton.get_record_dates(337,arkona.db2_integer_to_date(c.hire_date), arkona.db2_integer_to_date(c.termination_date) ) b on a.id = b.record_id
  group by a.employee_number, record_date
  order by a.employee_number


drop table if exists one_termed_employee;
create temp table one_termed_employee as
select a.employee_number, a.full_name, '01/01/2021'::date as hire_date , '12/31/2021'::date as term_date
from sap.payroll_submittals a 
where a.pay_period_id = 337;

select * from one_termed_employee
-- term nicholas maro on 11/11 (in 1st week)
update one_Termed_employee
set term_date = '11/11/2021'
where employee_number = '191060';
------------------------------------------------------------------------------------------------
--/> tested with an acual in pay period term date for 1 employee, look ok
------------------------------------------------------------------------------------------------
  