﻿-- turns out they don't do seconds, period
-- calc time (seconds truncated) should be used

select * from ukg.clock_hours where last_name = 'bair' order by the_Date
select * from ukg.ext_clock_times where last_name = 'bair' order by the_Date, start_time

THU Dec 23
07:56
12:23
4.45


01:23
05:04
3.68

8.13 hours



2021-12-23 07:56:13.755-06,2021-12-23 12:23:36.323-06  ukg total 16020000  4.45 hrs

2021-12-23 13:23:12.145-06,2021-12-23 17:04:47.24-06   ukg total 13248000  3.68 hrs  for a total of 8.13 hrs

i figure 8.15 hrs

select '2021-12-23 12:23:36.323-06'::timestamptz - '2021-12-23 07:56:13.755-06'::timestamptz  -- 04:27:22.568 :: 16042.568 sec

select '2021-12-23 17:04:47.24-06'::timestamptz - '2021-12-23 13:23:12.145-06'::timestamptz  -- 03:41:35.095 :: 13295.095 sec

select ((extract(epoch from '2021-12-23 17:04:47.24-06'::timestamptz) - extract(epoch from '2021-12-23 13:23:12.145-06'::timestamptz))/3600.0)::numeric(6,2)

select ((extract(epoch from '2021-12-23 12:23:36.323-06'::timestamptz) - extract(epoch from '2021-12-23 07:56:13.755-06'::timestamptz))/3600.0)::numeric(6,2)

select (extract(epoch from '2021-12-23 17:04:47.24-06'::timestamptz) - extract(epoch from '2021-12-23 13:23:12.145-06'::timestamptz))  -- 13295.0950000286 sec

select (extract(epoch from '2021-12-23 12:23:36.323-06'::timestamptz) - extract(epoch from '2021-12-23 07:56:13.755-06'::timestamptz)) -- 16042.5679998398 sec

select 13295.0950000286/60  -- 221.5849166671433333 minutes
select 221.5849166671433333/60  -- 3.6930819444523889 hours

select 16042.5679998398/60  -- 267.3761333306633333 minutes
select 267.3761333306633333/60  -- 4.4562688888443889 hours

select 3.6930819444523889 + 4.4562688888443889  -- 8.1493508332967778 total hours

-- check out some calc time

select (extract(epoch from '2021-12-19 08:58:00-06'::timestamptz) - extract(epoch from '2021-12-19 06:48:00-06'::timestamptz))
select 7800/3600.0
select (extract(epoch from '2021-12-19 14:01:00-06'::timestamptz) - extract(epoch from '2021-12-19 07:37:00-06'::timestamptz))

select (extract(epoch from '2021-12-19 14:01:00-06'::timestamptz) - extract(epoch from '2021-12-19 07:37:00-06'::timestamptz))