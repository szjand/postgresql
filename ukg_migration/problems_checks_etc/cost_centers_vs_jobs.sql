﻿/*
02/10/22 from jeri
I want 98% of them to match. The remote ones will not exactly—I have designated them remote 
because some of the tax settings are different for our people working out of state. 
The spelling issue, I will fix. I definitely want an alert eventually because some of these 
are unintentional, where one was changed but the other did not get updated in error. 
*/
-- compare jobs to cost centers
select * 
from (
	select job_category, job_name
	from ukg.ext_cost_center_job_details) aa
full outer join (
	select * 
	from (
		select c.name as level_1, b.name as level_2, a.name as level_3
		from ukg.ext_cost_centers a
		left join ukg.ext_cost_centers b on a.parent_id = b.id
		left join ukg.ext_cost_centers c on b.parent_id = c.id) d
	where level_1 <> 'Location' -- 2 level heirarchy
		and level_1 is not null) bb	on aa.job_name = bb.level_3
order by bb.level_1, bb.level_2, coalesce(bb.level_3, aa.job_name)		


-- jobs in data not in ukg ?!?!?
Parts Shipping Specialist

-- no employees assigned
-- don't know where in the api this can be found
-- in employee_pay_info.default_job_id
Aftermarket & Used Vehicle Coodinator

-- jobs with no employees:
job category    job
PDQ							PDQ
