﻿ ALTER TABLE ukg.ext_time_entries DROP CONSTRAINT ext_time_entries_time_entry_type_check;

 ALTER TABLE ukg.ext_time_entries DROP CONSTRAINT ext_time_entries_pkey;



 -- DROP INDEX ukg.ext_time_entries_employee_account_id_start_time_idx;

CREATE UNIQUE INDEX ext_time_entries_employee_account_id_start_time_idx
  ON ukg.ext_time_entries
  USING btree
  (employee_account_id, start_time);
