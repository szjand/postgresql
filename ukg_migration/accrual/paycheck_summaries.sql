﻿
04/22/22
expand to include the 4/1 check (3/13 -> 3/26)

-- do all 7 checks
drop table if exists checks;
create temp table checks as
select  d.last_name, d.first_name, a.pay_date, b.employee_account_id, d.employee_number, c.earning_code, c.ee_amount, e.store, e.department
from ukg.payrolls a
join ukg.pay_statements b on a.payroll_id = b.payroll_id
join ukg.pay_statement_earnings c on b.pay_statement_id = c.pay_statement_id
  and b.employee_account_id = c.employee_account_id
	and c.earning_code in ( 'Average Overtime (0.5)','Biweekly Commission','Biweekly Guarantee','Flat Rate Compensation',
		'Flat Rate Premium Pay','Hourly Rate Adjustment','Lookback','Overtime Straight','Regular','Salary','Tool Allowance')  
join ukg.employees d on b.employee_account_id = d.employee_id		
left join (select * from ukg.get_cost_center_hierarchy()) e on c.cost_center_id = e.id
where a.payroll_name like '%bi-weekly%'
--   and a.pay_date between '01/07/2022' and '03/18/2022'
  and a.pay_date between '01/07/2022' and '04/01/2022'
order by last_name, pay_date  



-- over 6 pay periods the average comp, compared to the apr 1 check
select *, round(the_total/the_count, 2) as the_avg
from (
	select a.*, last_name, first_name, jan7,jan21,feb4,feb18,mar4,mar18,apr01,
			case when jan7 is not null then 1 else 0 end +
			case when jan21 is not null then 1 else 0 end +
			case when feb4 is not null then 1 else 0 end +
			case when feb18 is not null then 1 else 0 end +
			case when mar4 is not null then 1 else 0 end +
			case when mar18 is not null then 1 else 0 end as the_count,
			coalesce(jan7, 0) + coalesce(jan21, 0) + coalesce(feb4, 0) + coalesce(feb18, 0) + coalesce(mar4, 0) + coalesce(mar18, 0) as the_total
	from pra.biweekly_employees a
	join (
-- remover store & dept, created multiple rows, eg, arnold iverson
		select last_name, first_name, employee_account_id, employee_number, --store, department,
			sum(ee_amount) filter (where pay_date = '01/07/2022') as jan7,
			sum(ee_amount) filter (where pay_date = '01/21/2022') as jan21,
			sum(ee_amount) filter (where pay_date = '02/04/2022') as feb4,
			sum(ee_amount) filter (where pay_date = '02/18/2022') as feb18,
			sum(ee_amount) filter (where pay_date = '03/04/2022') as mar4,
			sum(ee_amount) filter (where pay_date = '03/18/2022') as mar18,
			sum(ee_amount) filter (where pay_date = '04/01/2022') as apr01
		from checks a
		where department not in ('cartiva','hobby shop','rydell company')
		group by last_name, first_name, employee_account_id, employee_number) b on a.employee_number = b.employee_number
	where a.year_month = 202203) x
where the_count <> 0
order by last_name




-- lets see what it looks like compared to accrual comp
select aa.*, bb.comp, bb.comp - aa.the_avg
from (
	select *, round(the_total/the_count, 2) as the_avg
	from (
		select a.*, last_name, first_name, jan7,jan21,feb4,feb18,mar4,mar18,apr01,
				case when jan7 is not null then 1 else 0 end +
				case when jan21 is not null then 1 else 0 end +
				case when feb4 is not null then 1 else 0 end +
				case when feb18 is not null then 1 else 0 end +
				case when mar4 is not null then 1 else 0 end +
				case when mar18 is not null then 1 else 0 end as the_count,
				coalesce(jan7, 0) + coalesce(jan21, 0) + coalesce(feb4, 0) + coalesce(feb18, 0) + coalesce(mar4, 0) + coalesce(mar18, 0) as the_total
		from pra.biweekly_employees a
		join (
	-- remover store & dept, created multiple rows, eg, arnold iverson
			select last_name, first_name, employee_account_id, employee_number, --store, department,
				sum(ee_amount) filter (where pay_date = '01/07/2022') as jan7,
				sum(ee_amount) filter (where pay_date = '01/21/2022') as jan21,
				sum(ee_amount) filter (where pay_date = '02/04/2022') as feb4,
				sum(ee_amount) filter (where pay_date = '02/18/2022') as feb18,
				sum(ee_amount) filter (where pay_date = '03/04/2022') as mar4,
				sum(ee_amount) filter (where pay_date = '03/18/2022') as mar18,
				sum(ee_amount) filter (where pay_date = '04/01/2022') as apr01
			from checks a
			where department not in ('cartiva','hobby shop','rydell company')
			group by last_name, first_name, employee_account_id, employee_number) b on a.employee_number = b.employee_number
		where a.year_month = 202203) x
	where the_count <> 0) aa
left join (
select control, round(sum(amount)/2, 2) as comp 
from pra.biweekly_employee_comp_amounts
where year_month = 202203
group by control) bb on aa.employee_number = bb.control
order by bb.comp - aa.the_avg

select * from pra.biweekly_employee_comp_amounts 
where year_month = 202203
and control = '1126040'


select a.control, a.account, earnings_code,
  case
    when b.work_days_adj = 0 then 0
    else round(coalesce(d.accrual_days_adj, 4) * (a.amount/b.work_days_adj), 2) 
  end as amount 
-- select * 
from (
	select control, account, earnings_code, sum(amount) as amount
	from pra.biweekly_employee_comp_amounts 
	where year_month = 202203
	group by control, account, earnings_code) a
join pra.pto_adj_hours_biweekly_employees_202203 b on a.control = b.employee_number	
  and b.year_month = 202203
join pra.biweekly_dates c on c.year_month = 202203
left join pra.accrual_days_adj d on a.control = d.employee_number
  and d.year_month = 202203
where a.control = '1126040' 

-- 4/24/22
-- using  _pra.biweekly_employees from try_expanded_base_for_average_V3.sql
select *, round(the_total/the_count, 2) as the_avg
from (
	select a.*, last_name, first_name, jan7,jan21,feb4,feb18,mar4,mar18,apr01,
			case when jan7 is not null then 1 else 0 end +
			case when jan21 is not null then 1 else 0 end +
			case when feb4 is not null then 1 else 0 end +
			case when feb18 is not null then 1 else 0 end +
			case when mar4 is not null then 1 else 0 end +
			case when mar18 is not null then 1 else 0 end as the_count,
			coalesce(jan7, 0) + coalesce(jan21, 0) + coalesce(feb4, 0) + coalesce(feb18, 0) + coalesce(mar4, 0) + coalesce(mar18, 0) as the_total
	from _pra.biweekly_employees a
	join (
-- remover store & dept, created multiple rows, eg, arnold iverson
		select last_name, first_name, employee_account_id, employee_number, --store, department,
			sum(ee_amount) filter (where pay_date = '01/07/2022') as jan7,
			sum(ee_amount) filter (where pay_date = '01/21/2022') as jan21,
			sum(ee_amount) filter (where pay_date = '02/04/2022') as feb4,
			sum(ee_amount) filter (where pay_date = '02/18/2022') as feb18,
			sum(ee_amount) filter (where pay_date = '03/04/2022') as mar4,
			sum(ee_amount) filter (where pay_date = '03/18/2022') as mar18,
			sum(ee_amount) filter (where pay_date = '04/01/2022') as apr01
		from checks a
		where department not in ('cartiva','hobby shop','rydell company')
		group by last_name, first_name, employee_account_id, employee_number) b on a.employee_number = b.employee_number
	where a.year_month = 202203) x
where the_count <> 0
order by last_name