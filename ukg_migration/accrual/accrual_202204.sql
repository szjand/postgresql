﻿/*
04/30/22
this is a based on a copy of accrual_202203.ssql

both hire date and term date are considered to be days worked when they fall with in the accrual period

for february, the last biweekly check of the month pay date is 2/18, the pay period for that check was 1/30/22 thru 2/12/22, therefore the accrual period for February 2022 is 2/13/22 -> 2/28/22

rehire date has precedence over hire date

*/



---------------------------------------------------------------------------------------------------
--< biweekly payroll accrual  *** ACCRUAL IS ABOUT THE GENERAL LEDGER ***
---------------------------------------------------------------------------------------------------

DO $$
declare
	_year_month integer := 202204;

begin

-- add working days, won't always be 20, could be 30
  delete 
  from pra.biweekly_dates
  where year_month = _year_month;
  insert into pra.biweekly_dates
  select aa.*,
    (select count(*) from dds.dim_date where the_date between accrual_from_date and accrual_thru_date and weekday and not holiday) as work_days
  from (
		select _year_month,
			(select first_of_month from sls.months where year_month = _year_month),
			max(payroll_end_date) + 1 as accrual_from_date, 
			(select last_of_month from sls.months where year_month = _year_month) as accrual_thru_date,
			min(payroll_start_date) as pay_period_from_date, max(payroll_end_date) as pay_period_thru_date, count(distinct pay_date)	as pay_dates
		from ukg.payrolls a
		join dds.dim_date b on a.pay_date = b.the_date
			and b.year_month = _year_month
		where payroll_name like '%bi-weekly%') aa;

-- -- april has 3 biweekly paychecks with pay date in the month
-- select * from ukg.payrolls where pay_date between '04/01/2022' and '04/30/2022'	and payroll_name like '%bi-weekly%'
		
  
  delete 
  from pra.biweekly_employees
  where year_month = _year_month;			
  insert into pra.biweekly_employees 
	select  _year_month, a.employee_number, 
		case 
			-- 1: range is a.hire_date -> a.term_date
			when coalesce(a.rehire_date, hire_date) between d.pay_period_from_date and d.pay_period_thru_date and a.term_date between d.pay_period_from_date and d.pay_period_thru_date 
				then (select count(*) from dds.dim_date where the_date between coalesce(a.rehire_date, hire_date) and a.term_date and weekday and not holiday) 
			-- 2: range is a.hire_date -> d.thru_date
			when coalesce(a.rehire_date, hire_date) between d.pay_period_from_date and d.pay_period_thru_date and a.term_date not between d.pay_period_from_date and d.pay_period_thru_date 
				then (select count(*) from dds.dim_date where the_date between coalesce(a.rehire_date, hire_date) and d.pay_period_thru_date and weekday and not holiday) 
			-- 3: range is d.from_date -> a.term_date
			when coalesce(a.rehire_date, hire_date) not between d.pay_period_from_date and d.pay_period_thru_date and a.term_date between d.pay_period_from_date and d.pay_period_thru_date
				then (select count(*) from dds.dim_date where the_date between d.pay_period_from_date and a.term_date and weekday and not holiday) 
		  -- 4: range is d.from_date -> d.thru_date
			when coalesce(a.rehire_date, hire_date) not between d.pay_period_from_date and d.pay_period_thru_date and a.term_date not between d.pay_period_from_date and d.pay_period_thru_date then 10 * d.pay_dates
		end as work_days
	from ukg.employees a
	join ukg.ext_employee_profiles b on a.employee_id = b.employee_id
	join jsonb_array_elements(b.pay_period) c on true
		and c->>'name' in ('RAC Bi-Weekly','HGF Bi-Weekly')
	join pra.biweekly_dates d on  d.year_month = _year_month
	-- anyone employed at any time during the the pay period time frame
	where coalesce(a.rehire_date, hire_date) <= d.pay_period_thru_date
		and a.term_date >= d.pay_period_from_date;


-- -- consolidate tables pra.pto_adj_hours_biweekly_employees_202201, pra.pto_adj_hours_biweekly_employees_202202, pra.pto_adj_hours_biweekly_employees_202203
-- -- into pra.pto_adj_hours_biweekly_employees
-- CREATE TABLE pra.pto_adj_hours_biweekly_employees(
--   year_month integer,
--   employee_number citext,
--   work_days integer,
--   pto_days numeric,
--   work_days_adj numeric);
-- 
-- insert into pra.pto_adj_hours_biweekly_employees
-- select * from pra.pto_adj_hours_biweekly_employees_202201;
-- insert into pra.pto_adj_hours_biweekly_employees
-- select * from pra.pto_adj_hours_biweekly_employees_202202;
-- insert into pra.pto_adj_hours_biweekly_employees 
-- select * from pra.pto_adj_hours_biweekly_employees_202203;

-- alter table pra.pto_adj_hours_biweekly_employees_202201 rename to z_unused_pto_adj_hours_biweekly_employees_202201;
-- alter table pra.pto_adj_hours_biweekly_employees_202202 rename to z_unused_pto_adj_hours_biweekly_employees_202202;
-- alter table pra.pto_adj_hours_biweekly_employees_202203 rename to z_unused_pto_adj_hours_biweekly_employees_202203;

	delete 
	from pra.pto_adj_hours_biweekly_employees
	where year_month = _year_month;
	-- adj work days = base work days - pto/hol days
	-- work days only accounts for pto & hol hours in the accrual period, including partial pto/hol days
	insert into pra.pto_adj_hours_biweekly_employees
	select a.year_month, a.employee_number, a.work_days, coalesce(e.pto_days, 0) as pto_days, work_days - coalesce(pto_days, 0) as work_days_adj
	from pra.biweekly_employees a
	join ukg.employees b on a.employee_number = b.employee_number
	join ukg.ext_employee_pay_info c on b.employee_id = c.employee_id
	join ukg.ext_pay_types d on c.pay_type_id = d.id
	left join (
		select a.employee_number, round(sum(pto_hours + hol_hours)/8, 2) as pto_days
		from pra.biweekly_employees a
		join ukg.clock_hours b on a.employee_number = b.employee_number
			and pto_hours + hol_hours <> 0
		join pra.biweekly_dates c on b.the_date between c.pay_period_from_date and c.pay_period_thru_date
		  and c.year_month = _year_month
		where a.year_month = _year_month
		  and a.employee_number not in ('145801','152410','136170','18005','1117901','1106399') -- exclude teams axtman & blueprint (guarantees)
		group by a.employee_number) e on a.employee_number = e.employee_number
	where a.year_month = _year_month;

  delete 
  from pra.accrual_days_adj 
  where year_month = _year_month;
  -- accrual days accounts for days actually worked based on time clock
  -- see accrual_days_adjustment_sql for the dev of this basic query
  insert into pra.accrual_days_adj
	select _year_month, employee_number, count(*) as accrual_days_adj
	from (
		select a.year_month, a.the_date, d.last_name, d.first_name, c.employee_number, e.clock_hours
		from dds.dim_date a
		join pra.biweekly_dates b on a.the_date between b.accrual_from_date and b.accrual_thru_date
		join pra.biweekly_employees c on b.year_month = c.year_month
		join ukg.employees d on c.employee_number = d.employee_number
		join ukg.clock_hours e on c.employee_number = e.employee_number
			and a.the_date = e.the_date
			and e.clock_hours <> 0
		where a.weekday
			and not a.holiday
			and b.year_month = _year_month) f
	group by year_month, employee_number;

 
  delete
  from pra.biweekly_employee_comp_amounts
  where year_month = _year_month;
  insert into pra.biweekly_employee_comp_amounts
	select _year_month, d.the_date, a.control, b.account, c.category, e.description as earnings_code, a.amount 
	from fin.fact_gl a
	join fin.dim_account b on a.account_key = b.account_key
	  and b.current_row
	join pra.accounts c on b.account = c.account
	  and c.category = 'comp'
	join dds.dim_date d on a.date_key = d.date_key
		and d.year_month = _year_month
	join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key  
		and e.description in ( 'Average Overtime (0.5)','Biweekly Commission','Biweekly Guarantee','Flat Rate Compensation',
			'Flat Rate Premium Pay','Hourly Rate Adjustment','Lookback','Overtime Straight','Regular','Salary','Tool Allowance')
	join fin.dim_journal f on a.journal_key = f.journal_key
		and f.journal_code = 'PAY'
	join pra.biweekly_employees g on a.control = g.employee_number 
	  and g.year_month = _year_month
	where a.post_status = 'Y';	  

  delete
  from pra.biweekly_employee_tax_ret_amounts
  where year_month = _year_month;
  insert into pra.biweekly_employee_tax_ret_amounts
	select _year_month, d.the_date, a.control, b.account, c.category, e.description, sum(a.amount) as amount
	from fin.fact_gl a
	join fin.dim_account b on a.account_key = b.account_key
	  and b.current_row
	join pra.accounts c on b.account = c.account
	  and c.category in ('taxes','retirement')
	join dds.dim_date d on a.date_key = d.date_key
		and d.year_month = _year_month
	join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key  
	join fin.dim_journal f on a.journal_key = f.journal_key
		and f.journal_code = 'PAY'
	join pra.biweekly_employees g on a.control = g.employee_number 
	  and g.year_month = _year_month
	where a.post_status = 'Y'
	group by d.the_date, a.control, b.account, c.category, e.description;

end $$;
---------------------------------------------------------------------------------------------------
--/> biweekly payroll accrual
---------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
--< 03/25/22 feb accrual days did not come out so well, so, this is a different way
--   to adjust the accrual days: 1 accrual day for each non holiday weekday for which
--   there are any clock_hours
--------------------------------------------------------------------------------------
select * from dds.dim_date where the_date = current_date - 1  

DO $$
declare
	_year_month integer := 202204;
	_the_date text := '04/30/2022';
	_journal text := 'GLI';
	_document text := 'GLI043022';
	_reference text := 'GLI043022';

begin
	delete 
	from pra.biweekly_accrual_file_for_jeri
	where year_month = _year_month;
	
	insert into pra.biweekly_accrual_file_for_jeri
	select _journal, _the_date,account,control, _document,
		_reference, amount, earnings_code as description, _year_month
	from (  
	select a.control, a.account, earnings_code,
		case
			when b.work_days_adj = 0 then 0
			else round(coalesce(d.accrual_days_adj, 4) * (a.amount/b.work_days_adj), 2) 
		end as amount 
	from (
		select control, account, earnings_code, sum(amount) as amount
		from pra.biweekly_employee_comp_amounts 
		where year_month = _year_month
		group by control, account, earnings_code) a
	join pra.pto_adj_hours_biweekly_employees b on a.control = b.employee_number	
		and b.year_month = _year_month
	join pra.biweekly_dates c on c.year_month = _year_month
	left join pra.accrual_days_adj d on a.control = d.employee_number
		and d.year_month = _year_month
	union
	select a.control, a.account, description,
		case
			when b.work_days = 0 then 0
			 else round(coalesce(accrual_days_adj, 4) * (a.amount/b.work_days_adj), 2) 
		end as amount 
	from (
		select control, account, description, sum(amount) as amount
		from pra.biweekly_employee_tax_ret_amounts 
		where year_month = _year_month
		group by control, account, description) a
	join pra.pto_adj_hours_biweekly_employees b on a.control = b.employee_number	
		and b.year_month = _year_month
	join pra.biweekly_dates c on c.year_month = _year_month
	left join pra.accrual_days_adj d on a.control = d.employee_number
		and d.year_month = _year_month) x;

end $$;

-- alter table pra.biweekly_accrual_file_for_jeri
-- rename column thedate to the_date;
-- 
-- alter table pra.biweekly_accrual_file_for_jeri
-- add column year_month integer;
-- 
-- update pra.biweekly_accrual_file_for_jeri
-- set year_month = 202203;

select * from pra.biweekly_accrual_file_for_jeri where year_month = 202204

select * from pra.biweekly_dates

select account, sum(amount)
from pra.biweekly_accrual_file_for_jeri
group by account
order by account

select a.*, b.days, round(amount/days, 2) as per_day
from (
select year_month, account, sum(amount) as amount
from pra.biweekly_accrual_file_for_jeri
group by year_month, account) a
join pra.biweekly_dates b on a.year_month = b.year_month
order by account

-- and the file for jeri
select journal, the_date, account,control, document, reference, amount, description
from pra.biweekly_accrual_file_for_jeri 
where year_month = 202204


