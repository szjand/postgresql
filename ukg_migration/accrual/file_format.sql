﻿select * 
from pra.biweekly_employees a
where a.year_month = 202201

select * 
from pra.biweekly_dates a
join pra.biweekly_employees

-- file format
journal	thedate			account	control	document	reference	amount	description
GLI			12/31/2021	12405		1100625	GLI123121	GLI123121	649.6		Payroll Accrual
GLI			12/31/2021	124705	1100625	GLI123121	GLI123121	2626.6	Payroll Accrual


DELETE FROM AccrualDates;
INSERT INTO AccrualDates 
SELECT 'GLI', @theDate,
  'GLI' + 
    CASE 
      WHEN month(@thrudate) < 10 THEN '0' +  trim(CAST(month(@thrudate) AS sql_char)) 
      ELSE trim(CAST(month(@thrudate) AS sql_char))
    END  +
  trim(CAST(dayofmonth(@thrudate) AS sql_char)) +
  right(trim(CAST(year(@thrudate) AS sql_char)), 2),
  'GLI' +
    CASE 
      WHEN month(@thrudate) < 10 THEN '0' +  trim(CAST(month(@thrudate) AS sql_char)) 
      ELSE trim(CAST(month(@thrudate) AS sql_char))
    END  +
  trim(CAST(dayofmonth(@thrudate) AS sql_char)) +
  right(trim(CAST(year(@thrudate) AS sql_char)), 2),  
  'Payroll Accrual', @fromdate,@thrudate, 
  timestampdiff(sql_tsi_day, @FromDate, @ThruDate) + 1,
  (timestampdiff(sql_tsi_day, @FromDate, @ThruDate) + 1)/14.0
FROM system.iota;  

-- 03/22/22
-- from  jeri
-- this has switched it to an overaccrual which i was afraid of, this is we are accounting for days off in the amounts to calculate
-- the daily average, but are not accounting for days potentially not worked in the accrual period.
-- Is it feasible to look at the time dcard in UKG and reduce the accrual days by the amount of days off within that period

how the pto adjustment is actually being determined
	no changes to earnings
	pto & hol hours are retrieved from ukg.clock_hours for the pay periods paid in the month, summed and divided by 8
	these pto/hol days are then subtracted from the work days in the pay periods (usually 20) and this new number is
	then divided into the earnings to generate daily earnings
	
