﻿Payroll expenses for salaried individuals seem to be higher in March than expected.  
Looking back, it looks like the accrual for salaried individuals was potentially too few of days.  
For example for Kelly Waldeck employee #145928, I would have expected the accrual to be about $3000 for the 12 days, but it was only $1500.  
March appears to be correct at $1500 for 6 days.  There really isn’t anything to do to fix it, 
I just want to make sure that this is the source of my discrepancies so that I stop searching.  
No need to look into this until tomorrow.

 

Thanks!

select * from pra.biweekly_dates where year_month in (202302,202303)

select * from ukg.ext_employee_profiles limit 10

-- feb: 20 work days, mar: 30 work days
select * 
from pra.biweekly_employees
where year_month in( 202302, 202303)
  and employee_number = '145928'

-- work_days_adj: feb: 20, mar: 30
select * 
from pra.pto_adj_hours_biweekly_employees
where year_month in( 202302, 202303)
  and employee_number = '145928' 

-- no clock hours for salaried, so no records here
select * 
from pra.accrual_days_adj  
where year_month in( 202302, 202303)
  and employee_number = '145928'

select * 
from pra.biweekly_employee_comp_amounts  
where year_month in( 202302, 202303)
  and control = '145928' 
order by the_date  

select year_month, sum(amount) 
from pra.biweekly_employee_comp_amounts  
where year_month in( 202302, 202303)
  and control = '145928' 
group by year_month  

-- don't know, a little suspicious of the payroll dates, but work it thru to the end
select * from ukg.payrolls where pay_date > '01/01/2023' and  payroll_name like '%bi-weekly%' order by payroll_start_date
-- they are ok, payroll dates are based on pay dates in the month being accrued
-- skip the taxes



	select a.control, a.account, earnings_code,
		case
			when b.work_days_adj = 0 then 0
			-- accrual_days_adj are based on timeclock, salary has no timeclock, so coalesce to c.days
			else round(coalesce(d.accrual_days_adj, c.days) * (a.amount/b.work_days_adj), 2) 
		end as amount 
	from ( -- comp
		select control, account, earnings_code, sum(amount) as amount
		from pra.biweekly_employee_comp_amounts 
		where year_month = 202302
		group by control, account, earnings_code) a
	join pra.pto_adj_hours_biweekly_employees b on a.control = b.employee_number	
		and b.year_month = 202302
	join pra.biweekly_dates c on c.year_month = 202302
	left join pra.accrual_days_adj d on a.control = d.employee_number
		and d.year_month = 202302
where control = '145928'  		

	select a.control, a.account, earnings_code,
		case
			when b.work_days_adj = 0 then 0
			-- accrual_days_adj are based on timeclock, salary has no timeclock, so coalesce to c.days
			else round(coalesce(d.accrual_days_adj, c.days) * (a.amount/b.work_days_adj), 2) 
		end as amount 
	from ( -- comp
		select control, account, earnings_code, sum(amount) as amount
		from pra.biweekly_employee_comp_amounts 
		where year_month = 202303
		group by control, account, earnings_code) a
	join pra.pto_adj_hours_biweekly_employees b on a.control = b.employee_number	
		and b.year_month = 202303
	join pra.biweekly_dates c on c.year_month = 202303
	left join pra.accrual_days_adj d on a.control = d.employee_number
		and d.year_month = 202303
where control = '145928'  

select *
from pra.biweekly_accrual_file_for_jeri
where control = '145928'
  and year_month in (202302, 202303)
order by year_month  

select year_month, sum(amount)
from pra.biweekly_accrual_file_for_jeri
where control = '145928'
  and year_month in (202302, 202303)
group by year_month 

select journal, the_date, account,control, document, reference, amount, description
from pra.biweekly_accrual_file_for_jeri 
where year_month = 202303
  and control = '145928'

209.25
269.28  

what i sent to jeri
i am a little confused,

For Kelly, i see, in the file i send to you
      Feb: 2511.08
                209.25/day
                        12 days
                 she was still on hourly for part of the period used                to generate earnings
      March: 1346.41
                      269.28/day
                              5 days (i dont count weekends)

Are there any other examples that stand out to you?

------------------------------------------
lets toy around with february dates

fewb 3 was last pay date on hourly
that pay period was 1/15 -> 1/28

the base earnings amount is based on pay dates in the month being accrued
for february, that was 2/3 (1/15 -> 1/28) and 2/17 (1/29 -> 2/11)
for march, that was 3/3 (2/12 -> 2/25), 3/17 (2/26 -> 3/11) and 3/31 (3/12 -> 3/25)