﻿/*
V3
using the 4/1 check which only includes the pay period 3/13 -> 3/26
how much history does it take to generate a reasonable average to match the values on that check

this isn't going to be a real accrual, it is about tyring to figure out how much history is required to get a reasonable avg
so i will store the data in the _pra schema as though it were the 202203 accrual

4/24/22
don't know why i stopped before doing comp and taxes, lets do that now
*/



do $$
declare 
	-- both accrual and pay period from/thru dates
	_year_month integer := 202203;
  _from_date date := '03/13/2022';
  _thru_date date := '03/26/2022';
  _days integer := 10;

begin
delete from _pra.biweekly_employees where year_month = 202203;
insert into _pra.biweekly_employees
select  _year_month as year_month, a.employee_number, 
	case 
		-- 1: range is a.hire_date -> a.term_date
		when coalesce(a.rehire_date, hire_date) between _from_date and _thru_date and a.term_date between _from_date and _thru_date 
			then (select count(*) from dds.dim_date where the_date between coalesce(a.rehire_date, hire_date) and a.term_date and weekday and not holiday) 
		-- 2: range is a.hire_date -> d.thru_date
		when coalesce(a.rehire_date, hire_date) between _from_date and _thru_date and a.term_date not between _from_date and _thru_date 
			then (select count(*) from dds.dim_date where the_date between coalesce(a.rehire_date, hire_date) and _thru_date and weekday and not holiday) 
		-- 3: range is d.from_date -> a.term_date
		when coalesce(a.rehire_date, hire_date) not between _from_date and _thru_date and a.term_date between _from_date and _thru_date
			then (select count(*) from dds.dim_date where the_date between _from_date and a.term_date and weekday and not holiday) 
		-- 4: range is d.from_date -> d.thru_date
		when coalesce(a.rehire_date, hire_date) not between _from_date and _thru_date and a.term_date not between _from_date and _thru_date then 10 -- * d.pay_dates
	end as work_days
from ukg.employees a
join ukg.ext_employee_profiles b on a.employee_id = b.employee_id
join jsonb_array_elements(b.pay_period) c on true
	and c->>'name' in ('RAC Bi-Weekly','HGF Bi-Weekly')
-- hire on or before 3/26 and term on or after 3/13
where coalesce(a.rehire_date, hire_date) <= _thru_date
	and a.term_date >= _from_date;

-- remove employees based on accounting
delete 
from 	_pra.biweekly_employees
where employee_number not in (-- limit employees by accounts, emp count from 377 to 336
	select distinct a.control
	from fin.fact_gl a
	join fin.dim_account b on a.account_key = b.account_key
		and b.current_row
	join pra.accounts c on b.account = c.account
		and c.category = 'comp'
	join dds.dim_date d on a.date_key = d.date_key
		and d.the_date = '04/01/2022'
	join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key  
		and e.description in ( 'Average Overtime (0.5)','Biweekly Commission','Biweekly Guarantee','Flat Rate Compensation',
			'Flat Rate Premium Pay','Hourly Rate Adjustment','Lookback','Overtime Straight','Regular','Salary','Tool Allowance')
	join fin.dim_journal f on a.journal_key = f.journal_key
		and f.journal_code = 'PAY'
	join _pra.biweekly_employees g on a.control = g.employee_number 
		and g.year_month = _year_month
	where a.post_status = 'Y');


delete 
from _pra.pto_adj_hours_biweekly_employees
where year_month = _year_month;

insert into _pra.pto_adj_hours_biweekly_employees
	select a.year_month, a.employee_number, a.work_days, coalesce(e.pto_days, 0) as pto_days, work_days - coalesce(pto_days, 0) as work_days_adj
	from _pra.biweekly_employees a
	join ukg.employees b on a.employee_number = b.employee_number
	join ukg.ext_employee_pay_info c on b.employee_id = c.employee_id
	join ukg.ext_pay_types d on c.pay_type_id = d.id
	left join (
		select a.employee_number, round(sum(pto_hours + hol_hours)/8, 2) as pto_days
		from _pra.biweekly_employees a
		join ukg.clock_hours b on a.employee_number = b.employee_number
			and pto_hours + hol_hours <> 0
			and b.the_date between '03/13/2022' and '03/26/2022'
		where a.year_month = _year_month
		  and a.employee_number not in ('145801','152410','136170','18005','1117901','1106399') -- exclude teams axtman & blueprint (guarantees)
		group by a.employee_number) e on a.employee_number = e.employee_number
	where a.year_month = _year_month;

delete 
from _pra.accrual_days_adj
where year_month = _year_month;

insert into _pra.accrual_days_adj
select year_month, employee_number, 
  case
    when count(*) > 10 then 10
    else count(*)
  end as accrual_days_adj
from (
	select a.year_month, a.the_date, d.last_name, d.first_name, c.employee_number, e.clock_hours
	from dds.dim_date a
	join _pra.biweekly_employees c on c.year_month = _year_month
	join ukg.employees d on c.employee_number = d.employee_number
	join ukg.clock_hours e on c.employee_number = e.employee_number
		and a.the_date = e.the_date
		and e.clock_hours <> 0
	where a.the_date between _from_date and _thru_date
-- 	  and a.weekday  -- what about saturdays
		and not a.holiday) f
group by year_month, employee_number order by employee_number;



end $$;

-- comp
/*
i am totally mind fucked 
what comp do i use
ok, i want to compare what i would have generated for an accrual for 3/13 -> 3/26 and then compare that to the actual comp on the 4/1 check
so the question becomes what comp do i use to generate the accrual
for some period of time, i generate an average earned per day (avg/day)
take that value

where my head is going is that for whatever period i generate comp, i have use the actual number of days worked to get avg/day
but that doesn't make any sense for commission guys i think
*/

select *
from ukg.payrolls
where pay_date between '01/01/2022' and '03/31/2022'
  and payroll_name like '%bi-weekly%'


select  d.last_name, d.first_name, a.pay_date, b.employee_account_id, d.employee_number, c.earning_code, c.ee_amount, e.store, e.department
from ukg.payrolls a
join ukg.pay_statements b on a.payroll_id = b.payroll_id
join ukg.pay_statement_earnings c on b.pay_statement_id = c.pay_statement_id
  and b.employee_account_id = c.employee_account_id
	and c.earning_code in ( 'Average Overtime (0.5)','Biweekly Commission','Biweekly Guarantee','Flat Rate Compensation',
		'Flat Rate Premium Pay','Hourly Rate Adjustment','Lookback','Overtime Straight','Regular','Salary','Tool Allowance')  
join ukg.employees d on b.employee_account_id = d.employee_id		
join _pra.biweekly_employees dd on d.employee_number = dd.employee_number
  and dd.year_month = 202203
left join (select * from ukg.get_cost_center_hierarchy()) e on c.cost_center_id = e.id
where a.payroll_name like '%bi-weekly%'
  and a.pay_date between '01/07/2022' and '03/31/2022'
order by last_name, pay_date  

-- total earnings by emp/pay_date
-- employee comp by pay date
drop table if exists _pra.step_1 cascade;
create table _pra.step_1 as
select  d.last_name, d.first_name, a.pay_date, a.payroll_start_date, a.payroll_end_date, b.employee_account_id, d.employee_number,sum(c.ee_amount) as comp
from ukg.payrolls a
join ukg.pay_statements b on a.payroll_id = b.payroll_id
join ukg.pay_statement_earnings c on b.pay_statement_id = c.pay_statement_id
  and b.employee_account_id = c.employee_account_id
	and c.earning_code in ( 'Average Overtime (0.5)','Biweekly Commission','Biweekly Guarantee','Flat Rate Compensation',
		'Flat Rate Premium Pay','Hourly Rate Adjustment','Lookback','Overtime Straight','Regular','Salary','Tool Allowance')  
join ukg.employees d on b.employee_account_id = d.employee_id		
join _pra.biweekly_employees dd on d.employee_number = dd.employee_number
  and dd.year_month = 202203
left join (select * from ukg.get_cost_center_hierarchy()) e on c.cost_center_id = e.id
where a.payroll_name like '%bi-weekly%'
  and a.pay_date between '01/07/2022' and '03/31/2022'
group by d.last_name, d.first_name, a.pay_date, a.payroll_start_date, a.payroll_end_date, b.employee_account_id, d.employee_number;
alter table _pra.step_1
add primary key(employee_number,pay_date);

-- select a.last_name, a.first_name, a.employee_number, a.payroll_start_date, a.payroll_end_date, a.pay_date, 
--   b.*
-- from _pra.step_1 a
-- join ukg.clock_hours b on a.employee_number = b.employee_number
--   and b.the_date between a.payroll_start_date and  a.payroll_end_date
-- where a.employee_number = '168573'
--   and a.pay_date = '01/07/2022'
-- order by b.the_date

-- add days worked
-- for salaried it is 0, need to add pay type
-- tried to use ukg.ext_employee_pay_info, too many missing (michelle cochran, ken espelund, dylan dubois ...)
drop table if exists _pra.step_2 cascade;
create table _pra.step_2 as
select aa.*, 
  case 
    when dd.display_name like 'salary%' then 'salary'
    when dd.display_name like '%Hourly%' then 'hourly'
    when dd.display_name like '%commission%' then 'commission'
    when dd.display_name like '%flat%' then 'commission'
  end as pay_type,
  coalesce(bb.days_worked, 0) as days_worked
from _pra.step_1 aa
left join (
	select a.employee_number, a.pay_date, count(*) as days_worked
	from _pra.step_1 a
	join ukg.clock_hours b on a.employee_number = b.employee_number
		and b.the_date between a.payroll_start_date and  a.payroll_end_date
	join dds.dim_date c on b.the_date = c.the_date  
		and c.weekday
	where b.clock_hours <> 0
	group by a.employee_number, a.pay_date) bb on aa.employee_number = bb.employee_number
		and aa.pay_date = bb.pay_date
left join ukg.employee_profiles cc on aa.employee_account_id = cc.employee_id
left join ukg.ext_pay_calc_profiles dd on cc.pay_calc_id = dd.id	
-- where bb.days_worked is null 		
order by last_name, first_name, aa.pay_date;
alter table _pra.step_2
add primary key(employee_number,pay_date);

-- -- there are some odd ones, sett miller for one
-- select * from _pra.step_2
-- where days_worked = 0
--   and pay_type <> 'salary'
-- 
-- select * from _pra.step_1 where first_name = 'seth'  
-- 
-- select * from ukg.clock_hours where employee_number = '143245'
-- order by the_date
-- 
-- 	select a.employee_number, a.pay_date-- count(*) as days_worked
-- 	from _pra.step_1 a
-- 	left join ukg.clock_hours b on a.employee_number = b.employee_number
-- 		and b.the_date between a.payroll_start_date and  a.payroll_end_date
-- 	left join dds.dim_date c on b.the_date = c.the_date  
-- 		and c.weekday
-- 	where b.clock_hours <> 0 and a.employee_number = '143245' and pay_date = '01/15/2022'
-- 	group by a.employee_number, a.pay_date


-- add the comp per day, and the number of pay periods
-- thinking just exclude the 0 comp hourly pay periods
-- same thing for commission

select * from _pra.step_2

-- this doesn't work well enough
select * 
from (
	select *, 
		case
			when days_worked = 0 and pay_type = 'salary' then comp/10
			when days_worked = 0 then -1
			else round(comp/days_worked, 2)
		end as comp_per_day,
		count(*) over (partition by employee_number) as pay_periods
	from _pra.step_2) a
where 
  case
    when pay_type in ('hourly','commission') then comp_per_day > 0 
    else 1 = 1
  end
order by last_name, first_name, pay_date

-- identify anomalous salary rows
select *
from (
	select last_name, first_name, employee_number, pay_type, sum(days_worked) as days_worked, sum(comp) as comp, count(*) as pay_periods
	from _pra.step_2
	group by last_name, first_name, employee_number, pay_type) a
where pay_type = 'salary'	
order by pay_periods

-- simply get rid of the anomalous rows
delete from _pra.step_2 where employee_number in ('143424','143245');
delete from _pra.step_2 where employee_number = '132120' and pay_date = '02/04/2022';
delete from _pra.step_2 where employee_number = '169645' and pay_date in ('02/04/2022','03/04/2022')

-- this is it, often looks goofy, but let's play it out to the end
select aa.*, bb.comp_per_day, bb.total_comp, bb.total_days_worked
from _pra.step_2 aa
left join (
	select a.*,
		case
			when total_days_worked = 0 and pay_type = 'salary' then round(total_comp/(pay_periods  * 10), 2)
			else round(total_comp/total_days_worked, 2)
		end as comp_per_day
		from (
			select last_name, first_name, employee_number, pay_type, sum(days_worked) as total_days_worked, sum(comp) as total_comp, count(*) as pay_periods
			from _pra.step_2
			group by last_name, first_name, employee_number, pay_type) a) bb on aa.employee_number = bb.employee_number
where aa.pay_type = 'commission'			
order by last_name, first_name, pay_date

-- avg comp per day	
drop table if exists _pra.step_3 cascade;
create table _pra.step_3 as
select a.*,
	case
		when total_days_worked = 0 and pay_type = 'salary' then round(total_comp/(pay_periods  * 10), 2)
		else round(total_comp/total_days_worked, 2)
	end as avg_comp_per_day
	from (
		select last_name, first_name, employee_number, pay_type, sum(days_worked) as total_days_worked, sum(comp) as total_comp, count(*) as pay_periods
		from _pra.step_2
		group by last_name, first_name, employee_number, pay_type) a;

-- figure accrual based on avg_comp_per_day
-- use accrual_202203.sql as the model
-- what the fuck is the diff between work hours adj and accrual days adj
-- work days only accounts for pto & hol hours in the accrual period, including partial pto/hol days
-- accrual days accounts for days actually worked based on time clock
-- so, now this query makes more sense (was missing many, fixed ukg.employee_pay_info)
select * from (
select * from _pra.pto_adj_hours_biweekly_employees) a
full outer join (
select * from _pra.accrual_days_adj	where year_month = 202203) b on a.employee_number = b.employee_number
where a.work_days_adj <> b.accrual_days_adj


-- so, using the avg_comp_per_day * accrual_days_adj
-- !!! what i don't have is the earnings codes !!!
-- coalesce accrual_days_adj to account for salaried (no clock data)
select a.*, b.accrual_days_adj, a.avg_comp_per_day * coalesce(b.accrual_days_adj, 10) as accrued, c.actual_earnings
from _pra.step_3 a
left join _pra.accrual_days_adj b on a.employee_number = b.employee_number
  and b.year_month = 202203
left join ( -- c actual earnings for pay date 4/1
	select  d.last_name, d.first_name, d.employee_number, sum(c.ee_amount) as actual_earnings
	from ukg.payrolls a
	join ukg.pay_statements b on a.payroll_id = b.payroll_id
	join ukg.pay_statement_earnings c on b.pay_statement_id = c.pay_statement_id
		and b.employee_account_id = c.employee_account_id
		and c.earning_code in ( 'Average Overtime (0.5)','Biweekly Commission','Biweekly Guarantee','Flat Rate Compensation',
			'Flat Rate Premium Pay','Hourly Rate Adjustment','Lookback','Overtime Straight','Regular','Salary','Tool Allowance')  
	join ukg.employees d on b.employee_account_id = d.employee_id		
	join _pra.biweekly_employees dd on d.employee_number = dd.employee_number
		and dd.year_month = 202203
	left join (select * from ukg.get_cost_center_hierarchy()) e on c.cost_center_id = e.id
	where a.payroll_name like '%bi-weekly%'
		and a.pay_date = '04/01/2022'
	group by d.last_name, d.first_name, d.employee_number  ) c on a.employee_number = c.employee_number
-- where a.pay_type = 'salary'	
order by a.last_name, a.first_name


select sum(accrued) as accrued, sum(actual_earnings) as actual_earnings
from (
select a.*, b.accrual_days_adj, a.avg_comp_per_day * coalesce(b.accrual_days_adj, 10) as accrued, c.actual_earnings
from _pra.step_3 a
left join _pra.accrual_days_adj b on a.employee_number = b.employee_number
  and b.year_month = 202203
left join (
	select  d.last_name, d.first_name, d.employee_number, sum(c.ee_amount) as actual_earnings
	from ukg.payrolls a
	join ukg.pay_statements b on a.payroll_id = b.payroll_id
	join ukg.pay_statement_earnings c on b.pay_statement_id = c.pay_statement_id
		and b.employee_account_id = c.employee_account_id
		and c.earning_code in ( 'Average Overtime (0.5)','Biweekly Commission','Biweekly Guarantee','Flat Rate Compensation',
			'Flat Rate Premium Pay','Hourly Rate Adjustment','Lookback','Overtime Straight','Regular','Salary','Tool Allowance')  
	join ukg.employees d on b.employee_account_id = d.employee_id		
	join _pra.biweekly_employees dd on d.employee_number = dd.employee_number
		and dd.year_month = 202203
	left join (select * from ukg.get_cost_center_hierarchy()) e on c.cost_center_id = e.id
	where a.payroll_name like '%bi-weekly%'
		and a.pay_date = '04/01/2022'
	group by d.last_name, d.first_name, d.employee_number  ) c on a.employee_number = c.employee_number) x


-- i am guessing jeri is going to want to see a breakdown based on accounts	
-- don't believe i can do that with avg_comp_per_day, lets see


select a.*, b.accrual_days_adj, a.avg_comp_per_day * coalesce(b.accrual_days_adj, 10) as accrued, c.actual_earnings
from _pra.step_3 a
left join _pra.accrual_days_adj b on a.employee_number = b.employee_number
  and b.year_month = 202203
left join (
	select  d.last_name, d.first_name, d.employee_number, c.earning_code, sum(c.ee_amount) as actual_earnings
	from ukg.payrolls a
	join ukg.pay_statements b on a.payroll_id = b.payroll_id
	join ukg.pay_statement_earnings c on b.pay_statement_id = c.pay_statement_id
		and b.employee_account_id = c.employee_account_id
		and c.earning_code in ( 'Average Overtime (0.5)','Biweekly Commission','Biweekly Guarantee','Flat Rate Compensation',
			'Flat Rate Premium Pay','Hourly Rate Adjustment','Lookback','Overtime Straight','Regular','Salary','Tool Allowance')  
	join ukg.employees d on b.employee_account_id = d.employee_id		
	join _pra.biweekly_employees dd on d.employee_number = dd.employee_number
		and dd.year_month = 202203
	left join (select * from ukg.get_cost_center_hierarchy()) e on c.cost_center_id = e.id
	where a.payroll_name like '%bi-weekly%'
		and a.pay_date = '04/01/2022'
	group by d.last_name, d.first_name, d.employee_number, c.earning_code  ) c on a.employee_number = c.employee_number
-- where a.pay_type = 'salary'	
order by a.last_name, a.first_name



	select 202203, d.the_date, a.control, b.account, c.category, e.description as earnings_code, a.amount 
	from fin.fact_gl a
	join fin.dim_account b on a.account_key = b.account_key
	  and b.current_row
	join pra.accounts c on b.account = c.account
	  and c.category = 'comp'
	join dds.dim_date d on a.date_key = d.date_key
		and d.year_month = 202203
	join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key  
		and e.description in ( 'Average Overtime (0.5)','Biweekly Commission','Biweekly Guarantee','Flat Rate Compensation',
			'Flat Rate Premium Pay','Hourly Rate Adjustment','Lookback','Overtime Straight','Regular','Salary','Tool Allowance')
	join fin.dim_journal f on a.journal_key = f.journal_key
		and f.journal_code = 'PAY'
	join pra.biweekly_employees g on a.control = g.employee_number 
	  and g.year_month = 202203
	where a.post_status = 'Y';	  