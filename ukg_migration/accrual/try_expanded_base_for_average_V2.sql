﻿/*
V2
limited test looked promising in V1
going to set the employees by using jeri's file
hope for this to be a full accrual using the 4 pay periods

the jeri employees idea sucked, generated a huge difference, i have no idea why,
go back the the orig and see what happens
*/

drop table if exists _pra.biweekly_dates;
create table _pra.biweekly_dates as
select 202201::integer as year_month, '2022-01-16'::date as accrual_from_date,'2022-01-31'::date as accrual_thru_date,
	'2021-11-21'::date as pay_period_from_date, '2022-01-15'::date as pay_period_thru_date, 10::integer as days, 4::integer as pay_dates;

-- use jeri's jan file to give me the list of employees
drop table if exists	_pra.biweekly_employees;
create table _pra.biweekly_employees as
select  202201 as year_month, a.employee_number, 
	case 
		-- 1: range is a.hire_date -> a.term_date
		when coalesce(a.rehire_date, hire_date) between d.pay_period_from_date and d.pay_period_thru_date and a.term_date between d.pay_period_from_date and d.pay_period_thru_date 
			then (select count(*) from dds.dim_date where the_date between coalesce(a.rehire_date, hire_date) and a.term_date and weekday and not holiday) 
		-- 2: range is a.hire_date -> d.thru_date
		when coalesce(a.rehire_date, hire_date) between d.pay_period_from_date and d.pay_period_thru_date and a.term_date not between d.pay_period_from_date and d.pay_period_thru_date 
			then (select count(*) from dds.dim_date where the_date between coalesce(a.rehire_date, hire_date) and d.pay_period_thru_date and weekday and not holiday) 
		-- 3: range is d.from_date -> a.term_date
		when coalesce(a.rehire_date, hire_date) not between d.pay_period_from_date and d.pay_period_thru_date and a.term_date between d.pay_period_from_date and d.pay_period_thru_date
			then (select count(*) from dds.dim_date where the_date between d.pay_period_from_date and a.term_date and weekday and not holiday) 
		-- 4: range is d.from_date -> d.thru_date
		when coalesce(a.rehire_date, hire_date) not between d.pay_period_from_date and d.pay_period_thru_date and a.term_date not between d.pay_period_from_date and d.pay_period_thru_date then 10 * d.pay_dates
	end as work_days
from ukg.employees a
join ukg.ext_employee_profiles b on a.employee_id = b.employee_id
join jsonb_array_elements(b.pay_period) c on true
	and c->>'name' in ('RAC Bi-Weekly','HGF Bi-Weekly')
join _pra.biweekly_dates d on  d.year_month = 202201---------------_year_month
where coalesce(a.rehire_date, hire_date) <= d.pay_period_thru_date
	and a.term_date >= '11/21/2021'; ----- d.pay_period_from_date;
-- where a.employee_number in ( --345
--   select distinct control
--   from pra.jeri_202201);

drop table if exists _pra.pto_adj_hours_biweekly_employees_202201;
create table _pra.pto_adj_hours_biweekly_employees_202201 as
select a.year_month, a.employee_number, a.work_days, coalesce(e.pto_days, 0) as pto_days, work_days - coalesce(pto_days, 0) as work_days_adj
from _pra.biweekly_employees a
join ukg.employees b on a.employee_number = b.employee_number
join ukg.ext_employee_pay_info c on b.employee_id = c.employee_id
join ukg.ext_pay_types d on c.pay_type_id = d.id
left join (  -- looks like i did this wrong, instead of counting a day for any day with clock hours
	select a.employee_number, round(sum(pto_hours + hol_hours)/8, 2) as pto_days
	from _pra.biweekly_employees a
	join ukg.clock_hours b on a.employee_number = b.employee_number
		and pto_hours + hol_hours <> 0
	join _pra.biweekly_dates c on b.the_date between c.pay_period_from_date and c.pay_period_thru_date
		and c.year_month = 202201
	where a.year_month = 202201
		and a.employee_number not in ('145801','152410','136170','18005','1117901','1106399') -- exclude teams axtman & blueprint (guarantees)
	group by a.employee_number) e on a.employee_number = e.employee_number
where a.year_month = 202201;  

-- accrual period, and accrual days remains the same, base of 10 days
drop table if exists _pra.accrual_days_adj;
create table _pra.accrual_days_adj as  	
-- see accrual_days_adjustment_sql for the dev of this basic query
select year_month, employee_number, 
  case
    when count(*) > 10 then 10
    else count(*)
  end as accrual_days_adj
from (
	select a.year_month, a.the_date, d.last_name, d.first_name, c.employee_number, e.clock_hours
	from dds.dim_date a
	join _pra.biweekly_dates b on a.the_date between b.accrual_from_date and b.accrual_thru_date
	join _pra.biweekly_employees c on b.year_month = c.year_month
	join ukg.employees d on c.employee_number = d.employee_number
	join ukg.clock_hours e on c.employee_number = e.employee_number
		and a.the_date = e.the_date
		and e.clock_hours <> 0
	where a.weekday
		and not a.holiday
		and b.year_month = 202201) f
group by year_month, employee_number order by employee_number;

-- first insert the ukg
drop table if exists _pra.biweekly_employee_comp_amounts;
create table _pra.biweekly_employee_comp_amounts as
select 202201::integer as year_month, d.the_date, a.control, b.account, c.category, e.description as earnings_code, a.amount 
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
	and b.current_row
join pra.accounts c on b.account = c.account
	and c.category = 'comp'
join dds.dim_date d on a.date_key = d.date_key
	and d.the_date between '12/01/2021' and '01/31/2022' -- d.year_month in ( 202201, 202112) ------------------------_year_month
join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key  
	and e.description in ( 'Average Overtime (0.5)','Biweekly Commission','Biweekly Guarantee','Flat Rate Compensation',
		'Flat Rate Premium Pay','Hourly Rate Adjustment','Lookback','Overtime Straight','Regular','Salary','Tool Allowance')
join fin.dim_journal f on a.journal_key = f.journal_key
	and f.journal_code = 'PAY'
join _pra.biweekly_employees g on a.control = g.employee_number 
	and g.year_month = 202201 -----------------_year_month
where a.post_status = 'Y' order by control;	

-- use the ukg to limit the dealertrack
insert into _pra.biweekly_employee_comp_amounts
select 202201::integer as year_month,  (c.check_month || '-' || c.check_day || '-' || (2000 + c. check_year))::date as check_date,
	a.employee_number, 'account','comp',
	d.code_id || ' ' || d.description as earnings_code, coalesce(c.base_pay, 0) + coalesce(d.amount, 0) as earnings
from _pra.biweekly_employees a
join arkona.ext_pyhshdta c on a.employee_number = c.employee_
	and (c.check_month || '-' || c.check_day || '-' || (2000 + c. check_year))::date between '12/01/2021' and '01/31/2022'
	and c.total_gross_pay <> 0
left join arkona.ext_pyhscdta d on c.company_number = d.company_number -- does not exists for everyone, so left join
	and c.payroll_run_number = d.payroll_run_number 
	and a.employee_number = d.employee_number
-- 	and d.code_type = '1'
	and d.code_id in ('79','C19','cov','75','87','70','OVT');

-- jan comp
select e.last_name, e.first_name, a.control, accrual_days_adj, amount, work_days_adj, coalesce(accrual_days_adj, 10) * round(a.amount/work_days_adj, 2)
from (  -- 27 rows
	select control, sum(amount) as amount
	from _pra.biweekly_employee_comp_amounts
	where year_month = 202201
	group by control) a
join _pra.pto_adj_hours_biweekly_employees_202201 b on a.control = b.employee_number	
	and b.year_month = 202201
join _pra.biweekly_dates c on c.year_month = 202201
left join _pra.accrual_days_adj d on a.control = d.employee_number
	and d.year_month = 202201 
left join ukg.employees e on a.control = e.employee_number	
order by e.last_name

-- compare to jeri
select *, jon - jeri
from (
	select e.last_name, e.first_name, a.control, sum(coalesce(accrual_days_adj, 10) * round(a.amount/work_days_adj, 2)) as jon
	from (  -- 27 rows
		select control, account, sum(amount) as amount
		from _pra.biweekly_employee_comp_amounts
		where year_month = 202201
		group by control, account) a
	join _pra.pto_adj_hours_biweekly_employees_202201 b on a.control = b.employee_number	
		and b.year_month = 202201
	join _pra.biweekly_dates c on c.year_month = 202201
	left join _pra.accrual_days_adj d on a.control = d.employee_number
		and d.year_month = 202201 
	left join ukg.employees e on a.control = e.employee_number 
	group by last_name, first_name, control) aa	
left join (select control, sum(amount) as jeri from pra.jeri_202201 group by control) bb on aa.control = bb.control	
order by last_name