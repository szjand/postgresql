﻿/*
5/31/2022
for some reason, jeri wants this run a day early
anyway
here i am still doing a separate script for the new accrual
not even going to look into it, just want to get it done quickly for jeri if i can

i think, for june, it will be ok to reuse this script, just change the dates
08/01/22
  good to go for July
  starting to get a little nervous at 2:45,4:15,6:52 did it always take this long to run?
  fuck, stopped it at  9 minutes
  ran each section  manually, went fine
  then reran this, and it was done in 8.3 secs

09/01/22
this month (august), received this from jeri:
	If it won’t cause too many issues for you, I’m going to accrue the payroll through this past 
	Saturday (biweekly payroll) using actuals as I feel that will be more accurate since I haven’t 
	had time to really dive into dialing in on limiting variances.  Will you just run the accrual 
	for Sunday-Wednesday of this week instead?  If that’s a big headache, I will void out my entry.

	Jeri

so, that is what i need to do here, figure out how to do the accrual for just the last 4 days of the month
hmm, looking at accrual_202203.sql, looks like we did the same thing in march  

EZ PZ
just replace the generated pra.biweekly_dates with a hard coded set of values for this month

09/01
realize i need a plain english description of what this all does
*/

select * from pra.biweekly_dates

---------------------------------------------------------------------------------------------------
--< biweekly payroll accrual  *** ACCRUAL IS ABOUT THE GENERAL LEDGER ***
---------------------------------------------------------------------------------------------------

DO $$
declare
	_year_month integer := 202401; --------------------------------------------------------------

begin

-- add working days, won't always be 20, could be 30
  delete 
  from pra.biweekly_dates
  where year_month = _year_month;

-- -- for august 2022 only  
--   insert into pra.biweekly_dates 
--   select 202208::integer, '2022-08-01'::date, '2022-08-28'::date, '2022-08-31'::date,'2022-07-17'::date,'2022-08-13'::date, 2, 3;

-- the pay_period from and thru dates (the earnings period) are based on pay periods that have pay dates (the following friday) in the month being accrued

  insert into pra.biweekly_dates
  select aa.*,
    (select count(*) from dds.dim_date where the_date between accrual_from_date and accrual_thru_date and weekday and not holiday) as work_days
  from (
		select _year_month,
			(select first_of_month from sls.months where year_month = _year_month),
			max(payroll_end_date) + 1 as accrual_from_date, 
			(select last_of_month from sls.months where year_month = _year_month) as accrual_thru_date,
			min(payroll_start_date) as pay_period_from_date, max(payroll_end_date) as pay_period_thru_date, count(distinct pay_date)	as pay_dates
		from ukg.payrolls a
		join dds.dim_date b on a.pay_date = b.the_date
			and b.year_month = _year_month
		where payroll_name like '%bi-weekly%') aa;


  
  delete 
  from pra.biweekly_employees
  where year_month = _year_month;			
  insert into pra.biweekly_employees 
	select  _year_month, a.employee_number, 
		case 
			-- 1: range is a.hire_date -> a.term_date
			when coalesce(a.rehire_date, hire_date) between d.pay_period_from_date and d.pay_period_thru_date and a.term_date between d.pay_period_from_date and d.pay_period_thru_date 
				then (select count(*) from dds.dim_date where the_date between coalesce(a.rehire_date, hire_date) and a.term_date and weekday and not holiday) 
			-- 2: range is a.hire_date -> d.thru_date
			when coalesce(a.rehire_date, hire_date) between d.pay_period_from_date and d.pay_period_thru_date and a.term_date not between d.pay_period_from_date and d.pay_period_thru_date 
				then (select count(*) from dds.dim_date where the_date between coalesce(a.rehire_date, hire_date) and d.pay_period_thru_date and weekday and not holiday) 
			-- 3: range is d.from_date -> a.term_date
			when coalesce(a.rehire_date, hire_date) not between d.pay_period_from_date and d.pay_period_thru_date and a.term_date between d.pay_period_from_date and d.pay_period_thru_date
				then (select count(*) from dds.dim_date where the_date between d.pay_period_from_date and a.term_date and weekday and not holiday) 
		  -- 4: range is d.from_date -> d.thru_date
			when coalesce(a.rehire_date, hire_date) not between d.pay_period_from_date and d.pay_period_thru_date and a.term_date not between d.pay_period_from_date and d.pay_period_thru_date then 10 * d.pay_dates
		end as work_days
	from ukg.employees a
	join ukg.ext_employee_profiles b on a.employee_id = b.employee_id
	join jsonb_array_elements(b.pay_period) c on true
		and c->>'name' in ('RAC Bi-Weekly','HGF Bi-Weekly')
	join pra.biweekly_dates d on  d.year_month = _year_month
	-- anyone employed at any time during the the pay period time frame
	where coalesce(a.rehire_date, hire_date) <= d.pay_period_thru_date
		and a.term_date >= d.pay_period_from_date;


-- 05/31/22 removed gavin flaat (145801) from the blueprint team exemption, he is now in pdq mgmt
-- 08/01/22 axtman and duckstad are now hourly, remove them from the exclusion clause
	delete 
	from pra.pto_adj_hours_biweekly_employees
	where year_month = _year_month;
	-- adj work days = base work days - pto/hol days
	-- work days only accounts for pto & hol hours in the accrual period, including partial pto/hol days
	insert into pra.pto_adj_hours_biweekly_employees
	select a.year_month, a.employee_number, a.work_days, 
	-- bad fix for doug pede who came up with negative work_days_adj because of 0 work days
	coalesce(e.pto_days, 0) as pto_days, 
	case 
	  when work_days - coalesce(pto_days, 0) < 0 then 0
	  else work_days - coalesce(pto_days, 0) 
	end as work_days_adj
	from pra.biweekly_employees a
	join ukg.employees b on a.employee_number = b.employee_number
	join ukg.ext_employee_pay_info c on b.employee_id = c.employee_id
	join ukg.ext_pay_types d on c.pay_type_id = d.id
	left join (
		select a.employee_number, round(sum(pto_hours + hol_hours)/8, 2) as pto_days
		from pra.biweekly_employees a
		join ukg.clock_hours b on a.employee_number = b.employee_number
			and pto_hours + hol_hours <> 0
		join pra.biweekly_dates c on b.the_date between c.pay_period_from_date and c.pay_period_thru_date
		  and c.year_month = _year_month
		where a.year_month = _year_month
		  and a.employee_number not in ('152410','1117901','1106399') -- exclude team blueprint (guarantees)
		group by a.employee_number) e on a.employee_number = e.employee_number
	where a.year_month = _year_month;

  delete 
  from pra.accrual_days_adj 
  where year_month = _year_month;
  -- accrual days accounts for days actually worked based on time clock
  -- see accrual_days_adjustment_sql for the dev of this basic query
  insert into pra.accrual_days_adj
	select _year_month, employee_number, count(*) as accrual_days_adj
	from (
		select a.year_month, a.the_date, d.last_name, d.first_name, c.employee_number, e.clock_hours
		from dds.dim_date a
		join pra.biweekly_dates b on a.the_date between b.accrual_from_date and b.accrual_thru_date
		join pra.biweekly_employees c on b.year_month = c.year_month
		join ukg.employees d on c.employee_number = d.employee_number
		join ukg.clock_hours e on c.employee_number = e.employee_number
			and a.the_date = e.the_date
			and e.clock_hours <> 0
		where a.weekday
			and not a.holiday
			and b.year_month = _year_month) f
	group by year_month, employee_number;

/*
05/31/22 failed with, something with Shawn Anderson pay, grouping should take care of it
ERROR:  duplicate key value violates unique constraint "employee_comp_amounts_pkey"
DETAIL:  Key (year_month, the_date, control, account, earnings_code)=(202205, 2022-05-27, 148024, 12304B, REGULAR) already exists.
*/ 
  delete
  from pra.biweekly_employee_comp_amounts
  where year_month = _year_month;
  insert into pra.biweekly_employee_comp_amounts
	select _year_month, d.the_date, a.control, b.account, c.category, e.description as earnings_code, sum(a.amount) as amount 
	from fin.fact_gl a
	join fin.dim_account b on a.account_key = b.account_key
	  and b.current_row
	join pra.accounts c on b.account = c.account
	  and c.category = 'comp'
	join dds.dim_date d on a.date_key = d.date_key
		and d.year_month = _year_month
	join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key  
		and e.description in ( 'Average Overtime (0.5)','Biweekly Commission','Biweekly Guarantee','Flat Rate Compensation',
			'Flat Rate Premium Pay','Hourly Rate Adjustment','Lookback','Overtime Straight','Regular','Salary','Tool Allowance')
	join fin.dim_journal f on a.journal_key = f.journal_key
		and f.journal_code = 'PAY'
	join pra.biweekly_employees g on a.control = g.employee_number 
	  and g.year_month = _year_month
	where a.post_status = 'Y'
	group by _year_month, d.the_date, a.control, b.account, c.category, e.description;	  

  delete
  from pra.biweekly_employee_tax_ret_amounts
  where year_month = _year_month;
  insert into pra.biweekly_employee_tax_ret_amounts
	select _year_month, d.the_date, a.control, b.account, c.category, e.description, sum(a.amount) as amount
	from fin.fact_gl a
	join fin.dim_account b on a.account_key = b.account_key
	  and b.current_row
	join pra.accounts c on b.account = c.account
	  and c.category in ('taxes','retirement')
	join dds.dim_date d on a.date_key = d.date_key
		and d.year_month = _year_month
	join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key  
	join fin.dim_journal f on a.journal_key = f.journal_key
		and f.journal_code = 'PAY'
	join pra.biweekly_employees g on a.control = g.employee_number 
	  and g.year_month = _year_month
	where a.post_status = 'Y'
	group by d.the_date, a.control, b.account, c.category, e.description;

end $$;
---------------------------------------------------------------------------------------------------
--/> biweekly payroll accrual
---------------------------------------------------------------------------------------------------

-- select * from pra.biweekly_dates


DO $$
declare
	_year_month integer := 202401; ------------------------------------------------------------------
	_the_date text := '01/31/2024'; -----------------------------------------------------------------
	_journal text := 'GLI';
	_document text := 'GLI013124'; ------------------------------------------------------------------
	_reference text := 'GLI013124'; -----------------------------------------------------------------

begin
	delete 
	from pra.biweekly_accrual_file_for_jeri
	where year_month = _year_month;
	
	insert into pra.biweekly_accrual_file_for_jeri
	select _journal, _the_date,account,control, _document,
		_reference, amount, earnings_code as description, _year_month
	from (  
	select a.control, a.account, earnings_code,
		case
			when b.work_days_adj = 0 then 0
			-- accrual_days_adj are based on timeclock, salary has no timeclock, so coalesce to c.days
			else round(coalesce(d.accrual_days_adj, c.days) * (a.amount/b.work_days_adj), 2) 
		end as amount 
	from ( -- comp
		select control, account, earnings_code, sum(amount) as amount
		from pra.biweekly_employee_comp_amounts 
		where year_month = _year_month
		group by control, account, earnings_code) a
	join pra.pto_adj_hours_biweekly_employees b on a.control = b.employee_number	
		and b.year_month = _year_month
	join pra.biweekly_dates c on c.year_month = _year_month
	left join pra.accrual_days_adj d on a.control = d.employee_number
		and d.year_month = _year_month
	union
	-- taxes
	select a.control, a.account, description,
		case
			when b.work_days = 0 then 0
			when b.work_days_adj = 0 then 0 -- added 7/1/23, not sure i understand, but its the only thing that makes sense
			-- accrual_days_adj are based on timeclock, salary has no timeclock, so coalesce to c.days
			else round(coalesce(accrual_days_adj, c.days) * (a.amount/b.work_days_adj), 2) 
		end as amount 
	from (
		select control, account, description, sum(amount) as amount
		from pra.biweekly_employee_tax_ret_amounts 
		where year_month = _year_month
		group by control, account, description) a
	join pra.pto_adj_hours_biweekly_employees b on a.control = b.employee_number	
		and b.year_month = _year_month
	join pra.biweekly_dates c on c.year_month = _year_month
	left join pra.accrual_days_adj d on a.control = d.employee_number
		and d.year_month = _year_month
	where control not in ('137826','24665')) x;  -- terms

end $$;

/*
02/01/23
failed with division by zero
issue is pra.pto_adj_hours_biweekly_employees, emp# 137826, Ruben Delacruz, termed on 12/25/22
he worked 5 days but had 8.5 pto days


/*
ran May a day early at Jeri's request
that screwed up pra.accrual_days_adj, using 153865, generated 4 accrual days instead of 5
reran it on 06/01 for jeri
but wanted to save the data from the early run
*/
create table pra.tmp_biweekly_accrual_file_for_jeri_202205 as
select * from pra.biweekly_accrual_file_for_jeri where year_month = 202205 and control = '153865';

-- alter table pra.biweekly_accrual_file_for_jeri
-- rename column thedate to the_date;
-- 
-- alter table pra.biweekly_accrual_file_for_jeri
-- add column year_month integer;
-- 
-- update pra.biweekly_accrual_file_for_jeri
-- set year_month = 202203;

select * from pra.biweekly_accrual_file_for_jeri where year_month = 202204

select * from pra.biweekly_dates

select account, sum(amount)
from pra.biweekly_accrual_file_for_jeri
group by account
order by account

-- want this to have months for columns, accounts/amounts for rows
select a.*, b.days, round(amount/days, 2) as per_day
from (
	select year_month, account, sum(amount) as amount
	from pra.biweekly_accrual_file_for_jeri
	group by year_month, account) a
join pra.biweekly_dates b on a.year_month = b.year_month
where b.year_month in (202307, 202308, 202309, 202310,202311,202312,202401)  ---------------------------------------------------------
-- where b.year_month in (202301, 202302, 202303, 202304, 202305, 202306, 202307, 202308, 202309, 202310)  ---------------------------------------------------------
order by account, year_month



-- and the file for jeri
select journal, the_date, account,control, document, reference, amount, description
from pra.biweekly_accrual_file_for_jeri 
where year_month = 202401  -----------------------------------------------------------------------------
order by amount desc

14 - 31
=====================================================================================================================================
=====================================================================================================================================
generate some history of earnings per day

select d.the_date, b.account, a.*
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
join fin.dim_journal c on a.journal_key = c.journal_key
  and c.journal_code = 'PAY'
join dds.dim_date d on a.date_key = d.date_key  
where a.post_status = 'Y'
  and a.control in ('190324', '8100238')
order by d.the_date, b.account 
