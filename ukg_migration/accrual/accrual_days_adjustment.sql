﻿----------------------------------------------------------------------
--< 3/25
----------------------------------------------------------------------
after stunning results for january, february does not look so good
suggested instead of total hours / 8, 1 accrual day for each day worked

-- this was the previous way
	select employee_number, round(sum(clock_hours)/8, 1) as accrual_days
	from (  
		select a.year_month, a.accrual_from_date, a.accrual_thru_date, a.days, 
			bb.last_name, bb.first_name, b.employee_number, 
				case
					when c.clock_hours > 8 then 8
					else c.clock_hours
				end as clock_hours
		from pra.biweekly_dates a
		join pra.biweekly_employees b on a.year_month = b.year_month
		join ukg.employees bb on b.employee_number = bb.employee_number
		left join ukg.clock_hours c on b.employee_number = c.employee_number
			and c.the_date between a.accrual_from_date and a.accrual_thru_date
		where a.year_month = 202202 
		  and clock_hours is not null) x

select * from pra.biweekly_dates
select * from dds.dim_date where the_date = current_date

--1 row for each employee in the accrual period (monday - friday, non holiday only) with any clock hours
select sum(clock_hours), count(*) from ( -- 20007.42  2621
select a.the_date, d.last_name, d.first_name, c.employee_number, e.clock_hours
from dds.dim_date a
join pra.biweekly_dates b on a.the_date between b.accrual_from_date and b.accrual_thru_date
join pra.biweekly_employees c on b.year_month = c.year_month
join ukg.employees d on c.employee_number = d.employee_number
join ukg.clock_hours e on c.employee_number = e.employee_number
  and a.the_date = e.the_date
  and e.clock_hours <> 0
where a.weekday
  and not a.holiday
  and b.year_month = 202202
) x  
order by c.employee_number, a.the_date  

-- compare with the previous way (total hours / 8)
select * from (  
select last_name, first_name, employee_number, count(*)
from (
select a.the_date, d.last_name, d.first_name, c.employee_number, e.clock_hours
from dds.dim_date a
join pra.biweekly_dates b on a.the_date between b.accrual_from_date and b.accrual_thru_date
join pra.biweekly_employees c on b.year_month = c.year_month
join ukg.employees d on c.employee_number = d.employee_number
join ukg.clock_hours e on c.employee_number = e.employee_number
  and a.the_date = e.the_date
  and e.clock_hours <> 0
where a.weekday
  and not a.holiday
  and b.year_month = 202201
order by c.employee_number, a.the_date) f
group by last_name, first_name, employee_number) g
full outer join pra.accrual_hours_adj_biweekly_employees_202201 h on g.employee_number = h.employee_number

select a.year_month, a.the_date, d.last_name, d.first_name, c.employee_number, e.clock_hours
from dds.dim_date a
join pra.biweekly_dates b on a.the_date between b.accrual_from_date and b.accrual_thru_date
join pra.biweekly_employees c on b.year_month = c.year_month
join ukg.employees d on c.employee_number = d.employee_number
join ukg.clock_hours e on c.employee_number = e.employee_number
  and a.the_date = e.the_date
  and e.clock_hours <> 0
where a.weekday
  and not a.holiday
  and b.year_month = 202202
order by c.employee_number, a.the_date  
----------------------------------------------------------------------
--/> 3/25
----------------------------------------------------------------------

----------------------------------------------------------------------
--< 3/24
----------------------------------------------------------------------
select a.year_month, a.accrual_from_date, a.accrual_thru_date, a.days, 
  bb.last_name, bb.first_name, b.employee_number, 
    case
      when c.clock_hours > 8 then 8
      else c.clock_hours
    end as clock_hours
from pra.biweekly_dates a
join pra.biweekly_employees b on a.year_month = b.year_month
join ukg.employees bb on b.employee_number = bb.employee_number
left join ukg.clock_hours c on b.employee_number = c.employee_number
  and c.the_date between a.accrual_from_date and a.accrual_thru_date
where a.year_month = 202201

select a.year_month, a.accrual_from_date, a.accrual_thru_date, a.days, 
	bb.last_name, bb.first_name, b.employee_number, 
		case
			when c.clock_hours > 8 then 8
			else c.clock_hours
		end as clock_hours
from pra.biweekly_dates a
join pra.biweekly_employees b on a.year_month = b.year_month
join ukg.employees bb on b.employee_number = bb.employee_number
left join ukg.clock_hours c on b.employee_number = c.employee_number
	and c.the_date between a.accrual_from_date and a.accrual_thru_date
where a.year_month = 202201
order by bb.last_name		


-- work days are the number of days worked (non pto hol) in the pay periods
-- accrual days are the number of days worked in the accrual period
drop table if exists test_1;
create temp table test_1 as
select aa.*, bb.work_days, bb.pto_days, bb.work_days_adj, cc.earnings
from (
	select year_month,accrual_from_date, accrual_thru_date, days,
		last_name, first_name, employee_number, round(sum(clock_hours)/8, 1) as accrual_days
	from (  
		select a.year_month, a.accrual_from_date, a.accrual_thru_date, a.days, 
			bb.last_name, bb.first_name, b.employee_number, 
				case
					when c.clock_hours > 8 then 8
					else c.clock_hours
				end as clock_hours
		from pra.biweekly_dates a
		join pra.biweekly_employees b on a.year_month = b.year_month
		join ukg.employees bb on b.employee_number = bb.employee_number
		left join ukg.clock_hours c on b.employee_number = c.employee_number
			and c.the_date between a.accrual_from_date and a.accrual_thru_date
		where a.year_month = 202201) x
	group by year_month,accrual_from_date, accrual_thru_date, days,
		last_name, first_name, employee_number) aa
left join pra.pto_adj_hours_biweekly_employees_202201 bb on aa.employee_number = bb.employee_number  
left join (
  select control, sum(amount) as earnings
  from pra.biweekly_employee_comp_amounts
  where year_month = 202201
  group by control) cc on aa.employee_number = cc.control
order by aa.last_name


-- per employee, all 3 versions
select *,
	round(days * (earnings/work_days), 2) as orig_acc,
	round(days * (earnings/work_days_adj), 2) as work_days_acc,
	round(coalesce(accrual_days, days) * (earnings/work_days_adj), 2) as final
from test_1
where earnings is not null
order by employee_number::integer

	-- totals for all 3 versions
select sum(orig_acc), sum(work_days_acc), sum(final)
from (
	select *,
		round(days * (earnings/work_days), 2) as orig_acc,
		round(days * (earnings/work_days_adj), 2) as work_days_acc,
		round(coalesce(accrual_days, days) * (earnings/work_days_adj), 2) as final
	from test_1
	where earnings is not null) a



	select employee_number, round(sum(clock_hours)/8, 1) as accrual_days
	from (  
		select a.year_month, a.accrual_from_date, a.accrual_thru_date, a.days, 
			bb.last_name, bb.first_name, b.employee_number, 
				case
					when c.clock_hours > 8 then 8
					else c.clock_hours
				end as clock_hours
		from pra.biweekly_dates a
		join pra.biweekly_employees b on a.year_month = b.year_month
		join ukg.employees bb on b.employee_number = bb.employee_number
		left join ukg.clock_hours c on b.employee_number = c.employee_number
			and c.the_date between a.accrual_from_date and a.accrual_thru_date
		where a.year_month = 202201 
		  and clock_hours is not null) x
	group by employee_number


-- march, figures accrual period correctly
  select aa.*,
    (select count(*) from dds.dim_date where the_date between accrual_from_date and accrual_thru_date and weekday and not holiday) as work_days
  from (
		select 202203,
			(select first_of_month from sls.months where year_month = 202203),
			max(payroll_end_date) + 1 as accrual_from_date, 
			(select last_of_month from sls.months where year_month = 202203) as accrual_thru_date,
			min(payroll_start_date) as pay_period_from_date, max(payroll_end_date) as pay_period_thru_date, count(distinct pay_date)	as pay_dates
		from ukg.payrolls a
		join dds.dim_date b on a.pay_date = b.the_date
			and b.year_month = 202203
		where payroll_name like '%bi-weekly%') aa;

----------------------------------------------------------------------
--< 3/24
----------------------------------------------------------------------		