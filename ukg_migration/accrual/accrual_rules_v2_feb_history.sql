﻿create schema pra;
comment on schema pra is 'tables and functions for payroll accruals';

drop table if exists pra.accounts cascade;
create table pra.accrual_accounts (
	account citext primary key,
	category citext not null);

insert into pra.haccounts
select account, 
  case
    when description like '%tax%' then 'taxes'
    else 'retirement'
  end
from fin.dim_account
where account in ('22501','22502','22503','22506','22510','22524','22901','22902',
	'22903','22904','22906','22910','22924','12501','12502','12502W','12503','12504',
	'12505','12506','12524','12527','12528','12901','12902','12902W','12903','12904',
	'12905','12906','12924','12927','12928');

V3, jeri detected some missing accounts
insert into pra.accounts values ('21101','comp');
insert into pra.accounts values ('21102','comp');
insert into pra.accounts values ('21110','comp');
insert into pra.accounts values ('22504','tax');
insert into pra.accounts values ('22303','comp');
insert into pra.accounts values ('224704','comp');
insert into pra.accounts values ('224723','comp');
insert into pra.accounts values ('224724','comp');
insert into pra.accounts values ('12201C','comp');
insert into pra.accounts values ('12202C','comp');
insert into pra.accounts values ('12204C','comp');
insert into pra.accounts values ('12205C','comp');
insert into pra.accounts values ('12206C','comp');

	
drop table if exists pra.dates;
create table pra.dates (
  year_month integer primary key,
  first_of_month date not null,
  accrual_from_date date not null,
  accrual_thru_date date not null,
  pay_period_from_date date not null,
  pay_period_thru_date date not null,
  pay_dates integer not null,
  days integer not null);

drop table if exists pra.employees cascade;
create table pra.employees (
  year_month integer not null,
  employee_number citext not null, 
  work_days integer not null,
  primary key (year_month, employee_number));

drop table if exists pra.employee_comp_amounts cascade;
create table pra.employee_comp_amounts (
  year_month integer not null,
  the_date date not null,
  control citext not null, 
  account citext not null,
  category citext not null,
  earnings_code citext not null,
  amount numeric(8,2),
  primary key (year_month, the_date, control, account, earnings_code));

drop table if exists pra.employee_tax_ret_amounts cascade;
create table pra.employee_tax_ret_amounts (
  year_month integer not null,
  the_date date not null,
  control citext not null, 
  account citext not null,
  category citext not null,
  description citext not null,
  amount numeric(8,2),
  primary key (year_month, the_date, control, account, description));  
/*
both hire date and term date are considered to be days worked when they fall with in the accrual period

for february, the last biweekly check of the month pay date is 2/18, the pay period for that check was 1/30/22 thru 2/12/22, therefore the accrual period for February 2022 is 2/13/22 -> 2/28/22

rehire date has precedence over hire date


*/


---------------------------------------------------------------------------------------------------
--< biweekly payroll accrual  *** ACCRUAL IS ABOUT THE GENERAL LEDGER ***
---------------------------------------------------------------------------------------------------
/* after a day of living hell jeri doesn't want to bother proofing january to 4 pay periods 
		when this all calms down, i can use the 4/1 check to do an all ukg 4 pay period test	
*/
DO $$
declare
	_year_month integer := 202203;

begin
/*
-- add working days, won't always be 20, could be 30
  delete 
  from pra.biweekly_dates
  where year_month = _year_month;
  insert into pra.biweekly_dates
  select aa.*,
    (select count(*) from dds.dim_date where the_date between accrual_from_date and accrual_thru_date and weekday and not holiday) as work_days
  from (
		select _year_month,
			(select first_of_month from sls.months where year_month = _year_month),
			max(payroll_end_date) + 1 as accrual_from_date, 
			(select last_of_month from sls.months where year_month = _year_month) as accrual_thru_date,
			min(payroll_start_date) as pay_period_from_date, max(payroll_end_date) as pay_period_thru_date, count(distinct pay_date)	as pay_dates
		from ukg.payrolls a
		join dds.dim_date b on a.pay_date = b.the_date
			and b.year_month = _year_month
		where payroll_name like '%bi-weekly%') aa;
*/		
/*
exception for 202203
the actual accrual period is 3/13 - 3/31
but since we have yet to prove out a solid routine
jeri is going to handle the first 10 days based on the payroll file
this will only generate 4 days worth of accrual
*/

-- did this on 3/31
--   insert into pra.biweekly_dates 
--   select 202203::integer, '2022-03-01'::date, '2022-03-28'::date, '2022-03-31'::date,'2202-02-13'::date,'2022-03-12'::date, 2, 4

  -- select * from pra.biweekly_dates
  
  delete 
  from pra.biweekly_employees
  where year_month = _year_month;			
  insert into pra.biweekly_employees 
	select  _year_month, a.employee_number, 
		case 
			-- 1: range is a.hire_date -> a.term_date
			when coalesce(a.rehire_date, hire_date) between d.pay_period_from_date and d.pay_period_thru_date and a.term_date between d.pay_period_from_date and d.pay_period_thru_date 
				then (select count(*) from dds.dim_date where the_date between coalesce(a.rehire_date, hire_date) and a.term_date and weekday and not holiday) 
			-- 2: range is a.hire_date -> d.thru_date
			when coalesce(a.rehire_date, hire_date) between d.pay_period_from_date and d.pay_period_thru_date and a.term_date not between d.pay_period_from_date and d.pay_period_thru_date 
				then (select count(*) from dds.dim_date where the_date between coalesce(a.rehire_date, hire_date) and d.pay_period_thru_date and weekday and not holiday) 
			-- 3: range is d.from_date -> a.term_date
			when coalesce(a.rehire_date, hire_date) not between d.pay_period_from_date and d.pay_period_thru_date and a.term_date between d.pay_period_from_date and d.pay_period_thru_date
				then (select count(*) from dds.dim_date where the_date between d.pay_period_from_date and a.term_date and weekday and not holiday) 
		  -- 4: range is d.from_date -> d.thru_date
			when coalesce(a.rehire_date, hire_date) not between d.pay_period_from_date and d.pay_period_thru_date and a.term_date not between d.pay_period_from_date and d.pay_period_thru_date then 10 * d.pay_dates
		end as work_days
	from ukg.employees a
	join ukg.ext_employee_profiles b on a.employee_id = b.employee_id
	join jsonb_array_elements(b.pay_period) c on true
		and c->>'name' in ('RAC Bi-Weekly','HGF Bi-Weekly')
	join pra.biweekly_dates d on  d.year_month = _year_month
	-- anyone employed at any time during the the pay period time frame
	where coalesce(a.rehire_date, hire_date) <= d.pay_period_thru_date
		and a.term_date >= d.pay_period_from_date;


-- -- teams axtman & pre-diagnosis, makes no sense to adjust their working days as they get paid a guarantee
--   drop table if exists pra.pto_adj_hours_biweekly_employees_202201 cascade;
-- 	create table pra.pto_adj_hours_biweekly_employees_202201 as
-- 	select a.year_month, a.employee_number, a.work_days, coalesce(e.pto_days, 0) as pto_days, work_days - coalesce(pto_days, 0) as work_days_adj
-- 	from pra.biweekly_employees a
-- 	join ukg.employees b on a.employee_number = b.employee_number
-- 	join ukg.ext_employee_pay_info c on b.employee_id = c.employee_id
-- 	join ukg.ext_pay_types d on c.pay_type_id = d.id
-- 	left join (
-- 		select a.employee_number, round(sum(pto_hours + hol_hours)/8, 2) as pto_days
-- 		from pra.biweekly_employees a
-- 		join ukg.clock_hours b on a.employee_number = b.employee_number
-- 			and pto_hours + hol_hours <> 0
-- 		join pra.biweekly_dates c on b.the_date between c.pay_period_from_date and c.pay_period_thru_date
-- 		  and c.year_month = 202201
-- 		where a.year_month = 202201
-- 		  and a.employee_number not in ('145801','152410','136170','18005','1117901','1106399') -- exclude teams axtman & blueprint (guarantees)
-- 		group by a.employee_number) e on a.employee_number = e.employee_number
-- 	where a.year_month = 202201;
-- 
-- -- same thing for february
--   drop table if exists pra.pto_adj_hours_biweekly_employees_202202 cascade;
-- 	create table pra.pto_adj_hours_biweekly_employees_202202 as
-- 	select a.year_month, a.employee_number, a.work_days, coalesce(e.pto_days, 0) as pto_days, work_days - coalesce(pto_days, 0) as work_days_adj
-- 	from pra.biweekly_employees a
-- 	join ukg.employees b on a.employee_number = b.employee_number
-- 	join ukg.ext_employee_pay_info c on b.employee_id = c.employee_id
-- 	join ukg.ext_pay_types d on c.pay_type_id = d.id
-- 	left join (
-- 		select a.employee_number, round(sum(pto_hours + hol_hours)/8, 2) as pto_days
-- 		from pra.biweekly_employees a
-- 		join ukg.clock_hours b on a.employee_number = b.employee_number
-- 			and pto_hours + hol_hours <> 0
-- 		join pra.biweekly_dates c on b.the_date between c.pay_period_from_date and c.pay_period_thru_date
-- 		  and c.year_month = 202202
-- 		where a.year_month = 202202
-- 		  and a.employee_number not in ('145801','152410','136170','18005','1117901','1106399') -- exclude teams axtman & blueprint (guarantees)
-- 		group by a.employee_number) e on a.employee_number = e.employee_number
-- 	where a.year_month = 202202;

-- 4/3 i am confused, at first thought this was wrong, but maybe not: adj work days = base work days - pto/hol days
-- same thing for March
  drop table if exists pra.pto_adj_hours_biweekly_employees_202203 cascade;
	create table pra.pto_adj_hours_biweekly_employees_202203 as
	select a.year_month, a.employee_number, a.work_days, coalesce(e.pto_days, 0) as pto_days, work_days - coalesce(pto_days, 0) as work_days_adj
	from pra.biweekly_employees a
	join ukg.employees b on a.employee_number = b.employee_number
	join ukg.ext_employee_pay_info c on b.employee_id = c.employee_id
	join ukg.ext_pay_types d on c.pay_type_id = d.id
	left join (
		select a.employee_number, round(sum(pto_hours + hol_hours)/8, 2) as pto_days
		from pra.biweekly_employees a
		join ukg.clock_hours b on a.employee_number = b.employee_number
			and pto_hours + hol_hours <> 0
		join pra.biweekly_dates c on b.the_date between c.pay_period_from_date and c.pay_period_thru_date
		  and c.year_month = 202203
		where a.year_month = 202203
		  and a.employee_number not in ('145801','152410','136170','18005','1117901','1106399') -- exclude teams axtman & blueprint (guarantees)
		group by a.employee_number) e on a.employee_number = e.employee_number
	where a.year_month = 202203;
-- this approach (total hours / 8) bombed on february, jeri thinks it is because of the part timers	
-- -- 3/24 see accrual_days_adjustment_sql for the dev of this basic query
--   drop table if exists pra.accrual_hours_adj_biweekly_employees_202201;
--   create table pra.accrual_hours_adj_biweekly_employees_202201 as
-- 	select employee_number, round(sum(clock_hours)/8, 1) as accrual_days
-- 	from (  
-- 		select a.year_month, a.accrual_from_date, a.accrual_thru_date, a.days, 
-- 			bb.last_name, bb.first_name, b.employee_number, 
-- 				case
-- 					when c.clock_hours > 8 then 8
-- 					else c.clock_hours
-- 				end as clock_hours
-- 		from pra.biweekly_dates a
-- 		join pra.biweekly_employees b on a.year_month = b.year_month
-- 		join ukg.employees bb on b.employee_number = bb.employee_number
-- 		left join ukg.clock_hours c on b.employee_number = c.employee_number
-- 			and c.the_date between a.accrual_from_date and a.accrual_thru_date
-- 		where a.year_month = 202201 
-- 		  and clock_hours is not null) x
-- 	group by employee_number
-- 
-- -- same thing for 202202
--   drop table if exists pra.accrual_hours_adj_biweekly_employees_202202;
--   create table pra.accrual_hours_adj_biweekly_employees_202202 as
-- 	select employee_number, round(sum(clock_hours)/8, 1) as accrual_days
-- 	from (  
-- 		select a.year_month, a.accrual_from_date, a.accrual_thru_date, a.days, 
-- 			bb.last_name, bb.first_name, b.employee_number, 
-- 				case
-- 					when c.clock_hours > 8 then 8
-- 					else c.clock_hours
-- 				end as clock_hours
-- 		from pra.biweekly_dates a
-- 		join pra.biweekly_employees b on a.year_month = b.year_month
-- 		join ukg.employees bb on b.employee_number = bb.employee_number
-- 		left join ukg.clock_hours c on b.employee_number = c.employee_number
-- 			and c.the_date between a.accrual_from_date and a.accrual_thru_date
-- 		where a.year_month = 202202 
-- 		  and clock_hours is not null) x
-- 	group by employee_number	

  delete 
  from pra.accrual_days_adj 
  where year_month = _year_month;
--   drop table if exists pra.accrual_days_adj;
--   create table pra.accrual_days_adj as  	
  -- see accrual_days_adjustment_sql for the dev of this basic query
  insert into pra.accrual_days_adj
	select _year_month, employee_number, count(*) as accrual_days_adj
	from (
		select a.year_month, a.the_date, d.last_name, d.first_name, c.employee_number, e.clock_hours
		from dds.dim_date a
		join pra.biweekly_dates b on a.the_date between b.accrual_from_date and b.accrual_thru_date
		join pra.biweekly_employees c on b.year_month = c.year_month
		join ukg.employees d on c.employee_number = d.employee_number
		join ukg.clock_hours e on c.employee_number = e.employee_number
			and a.the_date = e.the_date
			and e.clock_hours <> 0
		where a.weekday
			and not a.holiday
			and b.year_month = _year_month) f
	group by year_month, employee_number;

--   delete 
--   from pra.accrual_days_adj 
--   where year_month = 202202;
--   insert into pra.accrual_days_adj(year_month,employee_number,accrual_days_adj)
-- 	select year_month, employee_number, count(*) as accrual_days_adj
-- 	from (
-- 		select a.year_month, a.the_date, d.last_name, d.first_name, c.employee_number, e.clock_hours
-- 		from dds.dim_date a
-- 		join pra.biweekly_dates b on a.the_date between b.accrual_from_date and b.accrual_thru_date
-- 		join pra.biweekly_employees c on b.year_month = c.year_month
-- 		join ukg.employees d on c.employee_number = d.employee_number
-- 		join ukg.clock_hours e on c.employee_number = e.employee_number
-- 			and a.the_date = e.the_date
-- 			and e.clock_hours <> 0
-- 		where a.weekday
-- 			and not a.holiday
-- 			and b.year_month = 202202) f
-- 	group by year_month, employee_number;


    
  delete
  from pra.biweekly_employee_comp_amounts
  where year_month = _year_month;
  insert into pra.biweekly_employee_comp_amounts
	select _year_month, d.the_date, a.control, b.account, c.category, e.description as earnings_code, a.amount 
	from fin.fact_gl a
	join fin.dim_account b on a.account_key = b.account_key
	  and b.current_row
	join pra.accounts c on b.account = c.account
	  and c.category = 'comp'
	join dds.dim_date d on a.date_key = d.date_key
		and d.year_month = _year_month
	join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key  
		and e.description in ( 'Average Overtime (0.5)','Biweekly Commission','Biweekly Guarantee','Flat Rate Compensation',
			'Flat Rate Premium Pay','Hourly Rate Adjustment','Lookback','Overtime Straight','Regular','Salary','Tool Allowance')
	join fin.dim_journal f on a.journal_key = f.journal_key
		and f.journal_code = 'PAY'
	join pra.biweekly_employees g on a.control = g.employee_number 
	  and g.year_month = _year_month
	where a.post_status = 'Y';	  

  delete
  from pra.biweekly_employee_tax_ret_amounts
  where year_month = _year_month;
  insert into pra.biweekly_employee_tax_ret_amounts
	select _year_month, d.the_date, a.control, b.account, c.category, e.description, sum(a.amount) as amount
	from fin.fact_gl a
	join fin.dim_account b on a.account_key = b.account_key
	  and b.current_row
	join pra.accounts c on b.account = c.account
	  and c.category in ('taxes','retirement')
	join dds.dim_date d on a.date_key = d.date_key
		and d.year_month = _year_month
	join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key  
	join fin.dim_journal f on a.journal_key = f.journal_key
		and f.journal_code = 'PAY'
	join pra.biweekly_employees g on a.control = g.employee_number 
	  and g.year_month = _year_month
	where a.post_status = 'Y'
	group by d.the_date, a.control, b.account, c.category, e.description;

end $$;
---------------------------------------------------------------------------------------------------
--/> biweekly payroll accrual
---------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
--< 03/25/22 feb accrual days did not come out so well, so, this is a different way
--   to adjust the accrual days: 1 accrual day for each non holiday weekday for which
--   there are any clock_hours
--------------------------------------------------------------------------------------
-- select sum(amount) from (  

drop table if exists pra.biweekly_accrual_file_for_jeri;
create table pra.biweekly_accrual_file_for_jeri as
select 'GLI'::Text as journal,'03/31/2022'::Text as thedate,account,control, 'GLI033122'::Text as document,
  'GLI033122'::Text as reference, amount, earnings_code as description
from (  
select a.control, a.account, earnings_code,
  case
    when b.work_days_adj = 0 then 0
    else round(coalesce(d.accrual_days_adj, 4) * (a.amount/b.work_days_adj), 2) 
  end as amount 
-- select * 
from (
	select control, account, earnings_code, sum(amount) as amount
	from pra.biweekly_employee_comp_amounts 
	where year_month = 202203
	group by control, account, earnings_code) a
join pra.pto_adj_hours_biweekly_employees_202203 b on a.control = b.employee_number	
  and b.year_month = 202203
join pra.biweekly_dates c on c.year_month = 202203
left join pra.accrual_days_adj d on a.control = d.employee_number
  and d.year_month = 202203
-- ) x

union
-- select sum(amount) from (  
select a.control, a.account, description,
  case
    when b.work_days = 0 then 0
     else round(coalesce(accrual_days_adj, 4) * (a.amount/b.work_days_adj), 2) 
  end as amount 
-- select a.*  
from (
	select control, account, description, sum(amount) as amount
	from pra.biweekly_employee_tax_ret_amounts 
	where year_month = 202203
	group by control, account, description) a
join pra.pto_adj_hours_biweekly_employees_202203 b on a.control = b.employee_number	
  and b.year_month = 202203
join pra.biweekly_dates c on c.year_month = 202203
left join pra.accrual_days_adj d on a.control = d.employee_number
  and d.year_month = 202203) x
-- ) x

select * from pra.biweekly_accrual_file_for_jeri;

select account, sum(amount)
from pra.biweekly_accrual_file_for_jeri
group by account
order by account

-- not a single pto day
select * from pra.pto_adj_hours_biweekly_employees_20220
--------------------------------------------------------------------------------------
--/> 03/25/22 feb accrual days did not come out so well, so, this is a different way
--   to adjust the accrual days: 1 accrual day for each non holiday weekday for which
--   there are any clock_hours
--------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------
--< 03/24/22 jeri wants the same (adj work days and adj accrual days) for february
-- oh, and combine comp and tax_ret into 1 file
--------------------------------------------------------------------------------------
select sum(amount) from ( 
select a.control, a.account, 
  case
    when b.work_days_adj = 0 then 0
    else round(coalesce(accrual_days, 11) * (a.amount/b.work_days_adj), 2) 
  end as amount 
from (
	select control, account, sum(amount) as amount
	from pra.biweekly_employee_comp_amounts 
	where year_month = 202201
	group by control, account) a
join pra.pto_adj_hours_biweekly_employees_202201 b on a.control = b.employee_number	
  and b.year_month = 202201
join pra.biweekly_dates c on c.year_month = 202201
left join pra.accrual_hours_adj_biweekly_employees_202202 d on a.control = d.employee_number
) x

union
-- select sum(amount) from (  
select a.control, a.account, 
  case
    when b.work_days = 0 then 0
     else round(coalesce(accrual_days, 10) * (a.amount/b.work_days_adj), 2) 
  end as amount 
from (
	select control, account, sum(amount) as amount
	from pra.biweekly_employee_tax_ret_amounts 
	where year_month = 202202
	group by control, account) a
join pra.pto_adj_hours_biweekly_employees_202202 b on a.control = b.employee_number	
  and b.year_month = 202202
join pra.biweekly_dates c on c.year_month = 202202
left join pra.accrual_hours_adj_biweekly_employees_202202 d on a.control = d.employee_number
-- ) x
--------------------------------------------------------------------------------------
--/> 03/24/22 jeri wants the same (adj work days and adj accrual days) for february
-- oh, and combine comp and tax_ret into 1 file
--------------------------------------------------------------------------------------

select * from pra.biweekly_dates where year_month = 202201

comment on table pra.biweekly_dates is 'for each month, the relevant dates for generating a payroll accrual file';

select * from pra.biweekly_employee_comp_amounts where year_month = 202201 order by earnings_code


select * from pra.employee_tax_ret_amounts;
select * from pra.employee_comp_amounts order by control, account
select * from pra.biweekly_employees where year_month = 202201 order by work_days
select * from  pra.dates		

select* from ukg.employees where employee_number = '135987'
-- 3/9 sent to jeri as 202202_biweekly_comp_accrual_v1_3.9.22.csv
-- 3/10 sent to jeri as 202202_biweekly_comp_accrual_v2_3.10.22.csv & 202202_biweekly_comp_accrual_10_days_v2.csv after adding the missing accounts
-- 3/14 sent to jeri as 202201_biweekly_comp_accrual_v1_3.14.22.csv & 202201_biweekly_comp_accrual_10_days_v1_3.14.22.csv after adding the missing accounts
select sum(amount) from ( -- 546248.35
select a.control, a.account, 
-- (employee's comp for the included pay periods/days worked for the included pay periods) = employee's comp per day worked
-- the number of days being accrued * (employee's comp for the included pay periods/days worked for the included pay periods)
  round(c.days * (a.amount/b.work_days), 2) as amount 
from (
	select control, account, sum(amount) as amount
	from pra.biweekly_employee_comp_amounts 
	where year_month = 202201
	group by control, account) a
join pra.biweekly_employees b on a.control = b.employee_number	
  and b.year_month = 202201
join pra.biweekly_dates c on c.year_month = 202201
) x

-- 3/9 sent to jeri as 202202_biweekly_tax_ret_accrual_v1_3.9.22.csv
-- 3/10 sent to jeri as 202202_biweekly_tax_ret_accrual_v2_3.10.22.csv & 202202_biweekly_tax_ret_accrual_10_days_v2.csv after adding the missing accounts
select sum(amount) from ( -- 58099.49
select a.control, a.account, 
  round(c.days * (a.amount/b.work_days), 2) as amount
from (
	select control, account, sum(amount) as amount
	from pra.biweekly_employee_tax_ret_amounts 
	where year_month = 202201
	group by control, account) a
join pra.biweekly_employees b on a.control = b.employee_number	
  and b.year_month = 202201
join pra.biweekly_dates c on c.year_month = 202201 order by control::integer, account
) x

----------------------------------------------------------------------------------------------------
--< this is the employee (non salary) work days adjusted for pto/hol
-- fuck i have to adjust for non salaried, but i have to include salaried in the query
-- i should do that in the adj query
----------------------------------------------------------------------------------------------------
-- -- after screwing it up hugely (from jeri Something is wrong here.  I have some that are negative values.  Others are incredibly high, such as control 136170 (which is over $25k).)
-- -- think this is better now, here is some validation work that i did:
-- 
-- drop table if exists t1 cascade;
-- create temp table t1 as
-- select a.year_month, c.last_name, c.first_name, c.employee_number, a.days as accrual_days, 
--   b.work_days, sum(d.amount) as earnings
-- from pra.biweekly_dates a
-- join pra.biweekly_employees b on a.year_month = b.year_month
--   and b.year_month = 202201
-- join ukg.employees c on b.employee_number = c.employee_number
-- join pra.biweekly_employee_comp_amounts d on c.employee_number = d.control
--   and d.year_month = 202201
-- where a.year_month = 202201
-- group by a.year_month, c.last_name, c.first_name, c.employee_number, a.days, b.work_days;
-- 
-- drop table if exists t2 cascade;
-- create temp table t2 as
-- select a.*, round(a.accrual_days * a.earnings/a.work_days, 2) as orig_accrual, b.work_days_adj, round(a.accrual_days * a.earnings/b.work_days_adj, 2) as adj_accrual
-- from t1 a
-- left join pra.pto_adj_hours_biweekly_employees_202201 b on a.employee_number = b.employee_number
-- order by last_name
-- 
-- -- this look reasonable
-- select sum(orig_accrual), sum(adj_accrual)  -- 546248.10 / 612503.50
-- from t2

-- sent these new results to jeri on 3/18 as V2
select sum(amount) from (  --612503.70 / 556821.64

select a.control, a.account, 
  case
    when b.work_days_adj = 0 then 0
    else round(10 * (a.amount/b.work_days_adj), 2) 
  end as amount 
from (
	select control, account, sum(amount) as amount
	from pra.biweekly_employee_comp_amounts 
	where year_month = 202201
	group by control, account) a
join pra.pto_adj_hours_biweekly_employees_202201 b on a.control = b.employee_number	
  and b.year_month = 202201
join pra.biweekly_dates c on c.year_month = 202201
) x

select sum(amount) from (  -- 65954.52 / 59959.42
select a.control, a.account, 
  case
    when b.work_days = 0 then 0
    else round(10 * (a.amount/b.work_days_adj), 2) 
  end as amount 
from (
	select control, account, sum(amount) as amount
	from pra.biweekly_employee_tax_ret_amounts 
	where year_month = 202201
	group by control, account) a
join pra.pto_adj_hours_biweekly_employees_202201 b on a.control = b.employee_number	
  and b.year_month = 202201
join pra.biweekly_dates c on c.year_month = 202201 order by control::integer, account
) x

-- 03/22/22
-- from  jeri
-- this has switched it to an overaccrual which i was afraid of, this is we are accounting for days off in the amounts to calculate
-- the daily average, but are not accounting for days potentially not worked in the accrual period.
-- Is it feasible to look at the time dcard in UKG and reduce the accrual days by the amount of days off within that period

-- 3/23 had to get her to explain it to me, what she wants is to adjust the accrual days per employee, so for those
--			folks with clock hours, determine how many days they actually worked
-- so this "accrual" includes both adjusted work days as well as adjusted accrual days
-- sent to jeri as 10/11 day, comp only, 202201_biweekly_comp_accrual_PTO-HOL_DAYS_AND_ACCRUAL_DAYS_ADJUSTED_V1.csv

select sum(amount) from (  --612503.70 / 556821.64 
select a.control, a.account, 
  case
    when b.work_days_adj = 0 then 0
    else round(coalesce(accrual_days, 10) * (a.amount/b.work_days_adj), 2) 
  end as amount 
from (
	select control, account, sum(amount) as amount
	from pra.biweekly_employee_comp_amounts 
	where year_month = 202201
	group by control, account) a
join pra.pto_adj_hours_biweekly_employees_202201 b on a.control = b.employee_number	
  and b.year_month = 202201
join pra.biweekly_dates c on c.year_month = 202201
left join pra.accrual_hours_adj_biweekly_employees_202201 d on a.control = d.employee_number
) x



----------------------------------------------------------------------------------------------------
--/> this is the employee (non salary) work days adjusted for pto/hol
----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------
--< pto
-----------------------------------------------------------------------------
/*
3/16/22
830 with jeri, suggested adjusting individual employees working days for pto//hol, exclude salaried employees
*/

select a.year_month, a.employee_number, a.work_days, coalesce(e.pto_days, 0), a.work_days - coalesce(e.pto_days, 0) as work_days_adj
from pra.biweekly_employees a
join ukg.employees b on a.employee_number = b.employee_number
join ukg.ext_employee_pay_info c on b.employee_id = c.employee_id
join ukg.ext_pay_types d on c.pay_type_id = d.id
left join (
	select a.employee_number, round(sum(pto_hours + hol_hours)/8, 2) as pto_days
	from pra.biweekly_employees a
	join ukg.clock_hours b on a.employee_number = b.employee_number
		and pto_hours + hol_hours <> 0
	join pra.biweekly_dates c on b.the_date between c.pay_period_from_date and c.pay_period_thru_date
	group by a.employee_number) e on a.employee_number = e.employee_number
where a.year_month = 202201
  and d.pay_type_name <> 'salary'
order by work_days_adj



/*
3/14/22 from jeri
Most employees will not have a personal rate table for PTO.  
The employees who are hourly and have a PTO/holiday rate equal to their regular hourly rate, will not have one set up.  
They holiday/PTO rate defaults to the hourly rate when this is not entered.  

January is pretty far off—around 65k in total.  I’m struggling with where to go from here.  
Looking through, it seems that PTO is really throwing the month of January off as both Christmas and New Year’s 
were paid holidays during this time, along with it being a high PTO time of year.  
With this being excluded from the averages, it is making the average compensation 
piece look much lower than it actually is in times when there is less PTO/holiday time.
*/
select cardinality(accounts), a.*
from (
	select control, array_agg(distinct account) as accounts
	from pra.employee_comp_amounts
	where year_month = 202201
	group by control) a
order by cardinality(accounts) desc 	

select * from pra.dates where year_month = 202201


select *, pto_rate * pto_hours as pto, hol_rate * hol_hours as hol 
from ( 
	select a.employee_number, b.last_name, b.first_name, coalesce(d.current_rate, aa.current_rate) as pto_rate, 
	  coalesce(e.current_rate, aa.current_rate) as hol_rate, f.pto_hours, f.hol_hours
	from pra.biweekly_employees a
	join ukg.employees b on a.employee_number = b.employee_number
	join ukg.employee_profiles c on b.employee_id = c.employee_id
	left join ukg.personal_rate_tables d on c.rate_table_id = d.rate_table_id
		and d.counter = 'Paid Time Off'
	left join ukg.personal_rate_tables e on c.rate_table_id = e.rate_table_id
		and e.counter = 'Holiday'  
	left join ukg.personal_rate_tables aa on c.rate_table_id = aa.rate_table_id
		and aa.description = 'Base Compensation Record'  		
	left join (-- pto and hol hours
		select a.employee_number, sum(pto_hours) as pto_hours, sum(hol_hours) as hol_hours
		from ukg.clock_hours a
		join (
			select *
			from pra.biweekly_employees
			where year_month = 202201) b on a.employee_number = b.employee_number
		where the_date between '01/01/2022' and '01/31/2022'
			and (
				pto_hours <> 0
				or 
				hol_hours <> 0)
		group by a.employee_number) f on a.employee_number = f.employee_number  
	where a.year_month = 202201) g





-- timeentries does have future pto, but i am not currently scraping future dates
-- currently 31 days of data thru current date - 1
-- could expand the thru date to current _date + 31, 
-- wait a minute, here we go again, another mind fuck, not focusing on the scope of the project,
-- accrual is only looking at the just completed month, don't need anything from the future
select b.last_name, b.first_name, a.* 
from pra.employees a
join ukg.employees b on a.employee_number = b.employee_number

select a.* 
from ukg.clock_hours a
join ukg.employees b on a.employee_number = b.employee_number
--   and b.last_name = 'graesser'
where a.the_date = '03/22/2022'

select * 
from ukg.employees where last_name = 'graesser'

select * from ukg.json_time_entries

select *, thru_ts - from_ts
from luigi.luigi_log
where task = 'gettimeentries'
  and status = 'pass'
order by the_date  
-----------------------------------------------------------------------------
--/> pto
-----------------------------------------------------------------------------

-- work files

-- includes connor arel, 187458 which previous queries did not, coalescing rehire date caused this and is correct
select * from (
	select 202202, ------------------------------ _year_month
		a.employee_number, a.last_name, a.first_name, coalesce(a.rehire_date, hire_date) as hire_date, a.hire_date, a.rehire_date, a.term_date, d.pay_period_from_date, d.pay_period_thru_date,
		case 
			-- 1: range is a.hire_date -> a.term_date
			when coalesce(a.rehire_date, hire_date) between d.pay_period_from_date and d.pay_period_thru_date and a.term_date between d.pay_period_from_date and d.pay_period_thru_date 
				then (select count(*) from dds.dim_date where the_date between coalesce(a.rehire_date, hire_date) and a.term_date and weekday and not holiday) 
			-- 2: range is a.hire_date -> d.thru_date
			when coalesce(a.rehire_date, hire_date) between d.pay_period_from_date and d.pay_period_thru_date and a.term_date not between d.pay_period_from_date and d.pay_period_thru_date 
				then (select count(*) from dds.dim_date where the_date between coalesce(a.rehire_date, hire_date) and d.pay_period_thru_date and weekday and not holiday) 
			-- 3: range is d.from_date -> a.term_date
			when coalesce(a.rehire_date, hire_date) not between d.pay_period_from_date and d.pay_period_thru_date and a.term_date between d.pay_period_from_date and d.pay_period_thru_date
				then (select count(*) from dds.dim_date where the_date between d.pay_period_from_date and a.term_date and weekday and not holiday) 
			when coalesce(a.rehire_date, hire_date) not between d.pay_period_from_date and d.pay_period_thru_date and a.term_date not between d.pay_period_from_date and d.pay_period_thru_date then 10 * d.pay_dates
		end as work_days
	from ukg.employees a
	join ukg.ext_employee_profiles b on a.employee_id = b.employee_id
	join jsonb_array_elements(b.pay_period) c on true
		and c->>'name' in ('RAC Bi-Weekly','HGF Bi-Weekly')
	join pra.dates d on  d.year_month = 202202 -------------------------------- _year_month
	-- anyone employed at any time during the the pay period time frame
	where /*coalesce(a.rehire_date, hire_date)*/ a.hire_date <= d.pay_period_thru_date
		and a.term_date >= d.pay_period_from_date) aa
where work_days < 20		
order by employee_number



select a.control, b.account, b.description as acct_descr, e.description as trans_descr, a.amount 
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
join pra.accounts c on b.account = c.account
join dds.dim_date d on a.date_key = d.date_key
  and d.year_month = 202202
join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key  
  and e.description in ( 'Average Overtime (0.5)','Biweekly Commission','Biweekly Guarantee','Flat Rate Compensation',
		'Flat Rate Premium Pay','Hourly Rate Adjustment','Lookback','Overtime Straight','Regular','Salary','Tool Allowance')
join fin.dim_journal f on a.journal_key = f.journal_key
  and f.journal_code = 'PAY'
join (
	select a.employee_number, a.last_name, a.first_name
	from ukg.employees a
	join ukg.ext_employee_profiles b on a.employee_id = b.employee_id
	join jsonb_array_elements(b.pay_period) c on true
		and c->>'name' in ('RAC Bi-Weekly','HGF Bi-Weekly')
	where a.hire_date <= '02/28/2022'
	  and a.term_date > '02/28/2022') g on a.control = g.employee_number 
where a.post_status = 'Y'	

-- select the_date, control, account, earnings_code from (
select d.the_date, a.control, b.account, c.category, e.description as earnings_code, a.amount 
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
join pra.accounts c on b.account = c.account
join dds.dim_date d on a.date_key = d.date_key
  and d.year_month = 202202
join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key  
  and e.description in ( 'Average Overtime (0.5)','Biweekly Commission','Biweekly Guarantee','Flat Rate Compensation',
		'Flat Rate Premium Pay','Hourly Rate Adjustment','Lookback','Overtime Straight','Regular','Salary','Tool Allowance')
join fin.dim_journal f on a.journal_key = f.journal_key
  and f.journal_code = 'PAY'
join pra.employees g on a.control = g.employee_number 
where a.post_status = 'Y'	
-- ) x
-- group by the_date, control, account, earnings_code
-- having count(*) > 1


-- taxes and retirement
-- select year_month, the_date, control, account, description from (
	select 202202 as year_month, d.the_date, a.control, b.account, c.category, e.description, sum(a.amount) as amount 
	from fin.fact_gl a
	join fin.dim_account b on a.account_key = b.account_key
	join pra.accounts c on b.account = c.account
	  and c.category <> 'comp'
	join dds.dim_date d on a.date_key = d.date_key
		and d.year_month = 202202
	join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key  
	join fin.dim_journal f on a.journal_key = f.journal_key
		and f.journal_code = 'PAY'
	join pra.employees g on a.control = g.employee_number 
	where a.post_status = 'Y'
	group by d.the_date, a.control, b.account, c.category, e.description
-- ) x group by year_month, the_date, control, account, description
-- having count(*) > 1	


	select 202202 as year_month, d.the_date, a.control, b.account, c.category, e.description, a.amount
	from fin.fact_gl a
	join fin.dim_account b on a.account_key = b.account_key
	join pra.accounts c on b.account = c.account
	  and c.category <> 'comp'
	join dds.dim_date d on a.date_key = d.date_key
		and d.year_month = 202202
	join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key  
	join fin.dim_journal f on a.journal_key = f.journal_key
		and f.journal_code = 'PAY'
	join pra.employees g on a.control = g.employee_number 
	where a.post_status = 'Y'
	  and control = '196452'
	    and b.account = '12505'
order by the_date, description	    

select *,  from ukg.employees where employee_number = '196452'