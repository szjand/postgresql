﻿select * 
from ukg.pay_plans a
left join ukg.pay_plan_earnings_codes b on a.pay_plan_id = b.pay_plan_id


select * from ukg.get_cost_center_hierarchy() order by cost_center

select * 
from ukg.ext_employee_details a
left join (select * from ukg.get_cost_center_hierarchy()) b on a.cost_center_id = b.id
where last_name = 'adam'

select * 
from ukg.ext_employee_details
where cost_center_id in (51655490469,51655491085)

select * from ukg.ext_cost_centers order by name

-- need the full cost center hierarchy for an employee

-- currently these are the available pay periods
select distinct c->>'name'
from ukg.employees a
join ukg.ext_employee_profiles b on a.employee_id = b.employee_id
join jsonb_array_elements(b.pay_period) c on true

select c
from ukg.employees a
join ukg.ext_employee_profiles b on a.employee_id = b.employee_id
join jsonb_array_elements(b.pay_period) c on true


-- this is good enough for now, but need to normalize the pattern for lookups, because this ain't it
-- amongst other problems, this does not accommodate multiple effective_from values
-- needs to be integrated into ukg.employee_profiles
-- pay period for an employee
select a.store, a.last_name, a.first_name, a.status, c->>'name'
from ukg.employees a
join ukg.ext_employee_profiles b on a.employee_id = b.employee_id
join jsonb_array_elements(b.pay_period) c on true
where a.status = 'active'


		select *
		from (
			select a.employee_id,
				(b->>'id')::bigint as pay_period_id, (b->>'effective_from')::date as pay_period_effective_from,
				row_number() over (partition by a.employee_id order by (b->>'effective_from')::date desc) as seq
			from ukg.ext_employee_profiles a
			left join jsonb_array_elements(a.pay_period) b on true) c
		where seq = 1

select * -- 420, 2 non employee, test direct and api api
from ukg.employees
where status = 'active'

select * from ukg.ext_employee_details limit 20

-- as the data model now stands(limps), this is the pay_period/pay_calc for current employees 
-- eg #2, identify who is in accrual
select a.store, a.last_name, a.first_name, a.status, a.cost_center, (c->>'name')::citext as pay_period, e.display_name as pay_calc_profile
from ukg.employees a
join ukg.ext_employee_details aa on a.employee_id = aa.employee_id
  and aa.cost_center_id not in (
    select id 
    from ukg.get_cost_center_hierarchy() 
    where store = 'Rydell GM' 
			and department in ('cartiva','hobby shop','rydell company'))
left join ukg.ext_employee_profiles b on a.employee_id = b.employee_id
left join jsonb_array_elements(b.pay_period) c on true
left join ukg.employee_profiles d on a.employee_id = d.employee_id
left join ukg.ext_pay_calc_profiles e on d.pay_calc_id = e.id
-- left join (select * from ukg.get_cost_center_hierarchy() where store = 'Rydell GM' and department in ('cartiva','hobby shop','rydell company'))
where a.status = 'active'
  and a.last_name not in ('direct','api')
-- order by a.last_name
order by (c->>'name')::citext, e.display_name



select * from ukg.earnings_codes order by earnings_code
