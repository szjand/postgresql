﻿/*
Will you please take a look at the biweekly payroll accrual?  I’m having issues with personnel costs this month, and when I dug into one account (12127), some employees such as number 145982 have the same accrual number in 12127 for June/July/August, but the number of accrual days was different.   We can talk through this if you would like.

Also employee 121869 was accrue din June and August but not July


I’m about 22k over in Carwash, but other departments are very high as well.

*/

select * from pra.biweekly_employees where employee_number = '121869' order by year_month
select * from ukg.employees where employee_number = '121869'
select * from pra.biweekly_accrual_file_for_jeri where control = '121869' order by year_month
select * from ukg.clock_hours where employee_number = '121869' order by the_date desc

select * 
from pra.biweekly_accrual_file_for_jeri 
where control = '145982'
  and account in ( '12127', '12128')
 order by year_month 

select * from pra.biweekly_employees where year_month = 202208 and employee_number = '145982'
select * from pra.pto_adj_hours_biweekly_employees where year_month = 202208 and employee_number = '145982'
select * from pra.accrual_days_adj  where year_month = 202208 and employee_number = '145982'  -- salaried
select * from pra.biweekly_employee_comp_amounts where year_month = 202208 and control = '145982'
-- select * from pra.biweekly_employee_tax_ret_amounts where year_month = 202208 and control = '145982'
comp

202208,2022-08-05,145982,12127,comp,SALARY,1669.23
202208,2022-08-05,145982,12128,comp,SALARY,715.39
202208,2022-08-19,145982,12127,comp,SALARY,1669.23
202208,2022-08-19,145982,12128,comp,SALARY,715.39

*** accrual days are based on time clock, salaried people dont punch the clock

-- this is comp from the final query
	select a.control, a.account, earnings_code,
		case
			when b.work_days_adj = 0 then 0
			else round(coalesce(d.accrual_days_adj, 4) * (a.amount/b.work_days_adj), 2) 
		end as amount 
	from (
		select control, account, earnings_code, sum(amount) as amount
		from pra.biweekly_employee_comp_amounts 
		where year_month = 202208 and control = '145982'
		group by control, account, earnings_code) a
	join pra.pto_adj_hours_biweekly_employees b on a.control = b.employee_number	
		and b.year_month = 202208
	join pra.biweekly_dates c on c.year_month = 202208
	left join pra.accrual_days_adj d on a.control = d.employee_number
		and d.year_month = 202208

-- this should fix the accrual days adjusted -- don't need this, the final query coalesces to the accrual adjusted days
delete 
from pra.accrual_days_adj 
where year_month = 202208;
insert into pra.accrual_days_adj
select 202208::integer as year_month, a.employee_number,
  coalesce(b.accrual_days_adj, 3) as accrual_days_adj
from pra.biweekly_employees a
left join pra.accrual_days_adj b on a.employee_number = b.employee_number
  and b.year_month = 202208
where a.year_month = 202208  

select * from pra.accrual_days_adj 
where year_month = 202208
and accrual_days_adj <> 3



-- brittany's comp: 4769
select sum(amount)  from pra.biweekly_employee_comp_amounts where year_month = 202208 and control = '145982'
the adj should be 3/20, 15% of that
select .15 * 4769  = 715.35
yep


in the orig accrual acct 12127

select sum(amount) -- 2526.93
from pra.biweekly_accrual_file_for_jeri 
where year_month = 202208
  and account in ('12127','12128')

changing the 4 to a 3 gets it down 10k
 select sum(amount) -- 179155.69,  169062  
from pra.biweekly_accrual_file_for_jeri 
where year_month = 202208 

-----------------------------------------------------------------------------------------------------
--< Also employee 121869 was accrue din June and August but not July
-- i fucked up on the manual insert into pra.biweekly_dates
-----------------------------------------------------------------------------------------------------
select * from pra.biweekly_employees where employee_number = '121869' order by year_month
select * from ukg.employees where employee_number = '121869'
select * from pra.biweekly_accrual_file_for_jeri where control = '121869' order by year_month
select * from ukg.clock_hours where employee_number = '121869' order by the_date desc
select * from ukg.employees where employee_number = '121869'

select * from pra.accrual_days_adj where year_month = 202208 and employee_number = '121869'

select * from pra.biweekly_accrual_file_for_jeri where year_month = 202208 and control = '121869'

-- pra.accrual_days_adj
		select a.year_month, a.the_date--, d.last_name, d.first_name, c.employee_number, e.clock_hours
		from dds.dim_date a
		join pra.biweekly_dates b on a.the_date between b.accrual_from_date and b.accrual_thru_date
		join pra.biweekly_employees c on b.year_month = c.year_month
		  and c.employee_number = '121869'
		join ukg.employees d on c.employee_number = d.employee_number
		join ukg.clock_hours e on c.employee_number = e.employee_number
			and a.the_date = e.the_date
			and e.clock_hours <> 0
		where a.weekday
			and not a.holiday
			and a.year_month = 202208
			
	
	select  a.employee_number, a.hire_Date, a.rehire_date, a.term_date, d.pay_period_thru_date, d.pay_period_from_date,  coalesce(a.rehire_date, hire_date)
-- 		case 
-- 			-- 1: range is a.hire_date -> a.term_date
-- 			when coalesce(a.rehire_date, hire_date) between d.pay_period_from_date and d.pay_period_thru_date and a.term_date between d.pay_period_from_date and d.pay_period_thru_date 
-- 				then (select count(*) from dds.dim_date where the_date between coalesce(a.rehire_date, hire_date) and a.term_date and weekday and not holiday) 
-- 			-- 2: range is a.hire_date -> d.thru_date
-- 			when coalesce(a.rehire_date, hire_date) between d.pay_period_from_date and d.pay_period_thru_date and a.term_date not between d.pay_period_from_date and d.pay_period_thru_date 
-- 				then (select count(*) from dds.dim_date where the_date between coalesce(a.rehire_date, hire_date) and d.pay_period_thru_date and weekday and not holiday) 
-- 			-- 3: range is d.from_date -> a.term_date
-- 			when coalesce(a.rehire_date, hire_date) not between d.pay_period_from_date and d.pay_period_thru_date and a.term_date between d.pay_period_from_date and d.pay_period_thru_date
-- 				then (select count(*) from dds.dim_date where the_date between d.pay_period_from_date and a.term_date and weekday and not holiday) 
-- 		  -- 4: range is d.from_date -> d.thru_date
-- 			when coalesce(a.rehire_date, hire_date) not between d.pay_period_from_date and d.pay_period_thru_date and a.term_date not between d.pay_period_from_date and d.pay_period_thru_date then 10 * d.pay_dates
-- 		end as work_days
	from ukg.employees a
	join ukg.ext_employee_profiles b on a.employee_id = b.employee_id
	join jsonb_array_elements(b.pay_period) c on true
		and c->>'name' in ('RAC Bi-Weekly','HGF Bi-Weekly')
	join pra.biweekly_dates d on  d.year_month = 202208
-- 	-- anyone employed at any time during the the pay period time frame
	where coalesce(a.rehire_date, hire_date) <= d.pay_period_thru_date
		and a.term_date >= d.pay_period_from_date
and a.employee_number = '121869'		

-----------------------------------------------------------------------------------------------------
--/> Also employee 121869 was accrue din June and August but not July
-----------------------------------------------------------------------------------------------------
select count(*) from pra.biweekly_employees where year_month = 202208  -- 394

select a.*, b.last_name, b.first_name, b.cost_center
from pra.biweekly_employees a
left join ukg.employees b on a.employee_number = b.employee_number
where a.year_month = 202208
  and cost_center like 'car%'
order by b.cost_center

