﻿comment on table pra.biweekly_dates is 'for a given month, these are the relevant dates for generating the biweekly payroll accrual';
comment on table pra.biweekly_employees is 'for a given month, these are the relevant employees and the days worked, adjusted
for mid time frame hire/term, for tgenertating the biweekly payroll accrual';

select * from pra.pto_adj_hours_biweekly_employees_202201
/*
due to the fabulous success in generating historical accruals for 202201 and 202202 in 
ukg_migration\accrual\accrual\rules\v2\feb_history.sql
this is the "final" version of the accrual generating script for biweekly accrual
*/
DO $$
declare
	_year_month integer := 202202;

begin
-- add working days, won't always be 20, could be 30
  delete 
  from pra.biweekly_dates
  where year_month = _year_month;
  insert into pra.biweekly_dates
  select aa.*,
    (select count(*) from dds.dim_date where the_date between accrual_from_date and accrual_thru_date and weekday and not holiday) as work_days
  from (
		select _year_month,
			(select first_of_month from sls.months where year_month = _year_month),
			max(payroll_end_date) + 1 as accrual_from_date, 
			(select last_of_month from sls.months where year_month = _year_month) as accrual_thru_date,
			min(payroll_start_date) as pay_period_from_date, max(payroll_end_date) as pay_period_thru_date, count(distinct pay_date)	as pay_dates
		from ukg.payrolls a
		join dds.dim_date b on a.pay_date = b.the_date
			and b.year_month = _year_month
		where payroll_name like '%bi-weekly%') aa;

  delete 
  from pra.biweekly_employees
  where year_month = _year_month;			
  insert into pra.biweekly_employees 
	select  _year_month, a.employee_number, 
		case 
			-- 1: range is a.hire_date -> a.term_date
			when coalesce(a.rehire_date, hire_date) between d.pay_period_from_date and d.pay_period_thru_date and a.term_date between d.pay_period_from_date and d.pay_period_thru_date 
				then (select count(*) from dds.dim_date where the_date between coalesce(a.rehire_date, hire_date) and a.term_date and weekday and not holiday) 
			-- 2: range is a.hire_date -> d.thru_date
			when coalesce(a.rehire_date, hire_date) between d.pay_period_from_date and d.pay_period_thru_date and a.term_date not between d.pay_period_from_date and d.pay_period_thru_date 
				then (select count(*) from dds.dim_date where the_date between coalesce(a.rehire_date, hire_date) and d.pay_period_thru_date and weekday and not holiday) 
			-- 3: range is d.from_date -> a.term_date
			when coalesce(a.rehire_date, hire_date) not between d.pay_period_from_date and d.pay_period_thru_date and a.term_date between d.pay_period_from_date and d.pay_period_thru_date
				then (select count(*) from dds.dim_date where the_date between d.pay_period_from_date and a.term_date and weekday and not holiday) 
		  -- 4: range is d.from_date -> d.thru_date
			when coalesce(a.rehire_date, hire_date) not between d.pay_period_from_date and d.pay_period_thru_date and a.term_date not between d.pay_period_from_date and d.pay_period_thru_date then 10 * d.pay_dates
		end as work_days
	from ukg.employees a
	join ukg.ext_employee_profiles b on a.employee_id = b.employee_id
	join jsonb_array_elements(b.pay_period) c on true
		and c->>'name' in ('RAC Bi-Weekly','HGF Bi-Weekly')
	join pra.biweekly_dates d on  d.year_month = _year_month
	-- anyone employed at any time during the the pay period time frame
	where coalesce(a.rehire_date, hire_date) <= d.pay_period_thru_date
		and a.term_date >= d.pay_period_from_date;

-- teams axtman & pre-diagnosis, makes no sense to adjust their working days as they get paid a guarantee
--   drop table if exists pra.pto_adj_hours_biweekly_employees_202201 cascade;
-- 	create table pra.pto_adj_hours_biweekly_employees_202201 as
  delete 
  from pra.work_days_adjusted_for_pto_hol
  where year_month = _year_month;

  insert into pra.work_days_adjusted_for_pto_hol(year_month,employee_number,work_days,pto_days,work_days_adj)
	select a.year_month, a.employee_number, a.work_days, coalesce(e.pto_days, 0) as pto_days, work_days - coalesce(pto_days, 0) as work_days_adj
	from pra.biweekly_employees a
	join ukg.employees b on a.employee_number = b.employee_number
	join ukg.ext_employee_pay_info c on b.employee_id = c.employee_id
	join ukg.ext_pay_types d on c.pay_type_id = d.id
	left join (
		select a.employee_number, round(sum(pto_hours + hol_hours)/8, 2) as pto_days
		from pra.biweekly_employees a
		join ukg.clock_hours b on a.employee_number = b.employee_number
			and pto_hours + hol_hours <> 0
		join pra.biweekly_dates c on b.the_date between c.pay_period_from_date and c.pay_period_thru_date
		  and c.year_month = 202202
		where a.year_month = 202202
		  and a.employee_number not in ('145801','152410','136170','18005','1117901','1106399') -- exclude teams axtman & blueprint (guarantees)
		group by a.employee_number) e on a.employee_number = e.employee_number
	where a.year_month = 202202;		