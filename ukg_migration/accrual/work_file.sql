﻿select * from pra.biweekly_dates where year_month = 202201

select * from pra.biweekly_employees where year_month = 202202 order by work_days

select* from ukg.employees where employee_number = '135987'a



select a.year_month, a.employee_number, a.work_days, coalesce(e.pto_days, 0), a.work_days - coalesce(e.pto_days, 0) as work_days_adj
from pra.biweekly_employees a
join ukg.employees b on a.employee_number = b.employee_number
join ukg.ext_employee_pay_info c on b.employee_id = c.employee_id
join ukg.ext_pay_types d on c.pay_type_id = d.id
left join (
	select a.employee_number, round(sum(pto_hours + hol_hours)/8, 2) as pto_days
	from pra.biweekly_employees a
	join ukg.clock_hours b on a.employee_number = b.employee_number
		and pto_hours + hol_hours <> 0
	join pra.biweekly_dates c on b.the_date between c.pay_period_from_date and c.pay_period_thru_date
	group by a.employee_number) e on a.employee_number = e.employee_number
where a.year_month = 202201
  and d.pay_type_name <> 'salary'
order by work_days_adj

select a.*
from ukg.employee_profiles a
join ukg.employees b on a.employee_id = b.employee_id
order by rate_table_effective_from desc nulls last

select b->>'name', count(*)
from ukg.ext_employee_profiles a
join jsonb_array_elements(pay_period) b on true
group by b->>'name'

select * 
from ukg.ext_employee_pay_info
limit 10

select a.employee_number, round(sum(pto_hours + hol_hours)/8, 2) as pto_days
select a.*, b.*
-- SELECT SUM(pto_hours), sum(hol_hours)
from pra.biweekly_employees a
join ukg.clock_hours b on a.employee_number = b.employee_number
  and pto_hours + hol_hours <> 0
join pra.biweekly_dates c on b.the_date between c.pay_period_from_date and c.pay_period_thru_date
where a.employee_number= '195401'
group by a.employee_number
