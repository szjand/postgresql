﻿select d.the_date, a.control, a.doc, a.ref, a.amount, b.account, b.description as acct_descr,c.description, e.journal_code, e.journal
from fin.fact_gl  a
JOIn fin.dim_account b on a.account_key = b.account_key
--   and b.account = '12502'
join fin.dim_gl_description c on a.gl_description_key = c.gl_Description_key
  and c.description like 'payrol%'
join dds.dim_date d on a.date_key = d.date_key
join fin.dim_journal e on a.journal_key = e.journal_key
limit 1000

select * 
from arkona.ext_pyhshdta
where check_year = 21
  and check_month = 2


select d.the_date, a.control, a.doc, a.ref, a.amount, b.account, b.description as acct_descr,c.description, e.journal_code, e.journal
from fin.fact_gl  a
JOIn fin.dim_account b on a.account_key = b.account_key
--   and b.account = '12502'
join fin.dim_gl_description c on a.gl_description_key = c.gl_Description_key
--   and c.description like 'payrol%'
join dds.dim_date d on a.date_key = d.date_key
  and d.year_month = 202112
join fin.dim_journal e on a.journal_key = e.journal_key  
where control = '1149180'

arkona.ext_pyhshdta.reference_check = fin.fact_gl.ref

select * from fin.dim_account limit 10


---------------------------------------------------------------------
-- dealertrack:  
--		arkona.ext_pyhshdta.reference_check = fin.fact_gl.ref
--    arkona.ext_pyhshdta.payroll_cen_year = 121
--    arkona.ext_pyhshdta.payroll_run_number = 1001210
--    fin.dim_gl_description.description = PAYROLL BATCH=121-1001210
---------------------------------------------------------------------
-- gehrtz 202110
select d.the_date, a.control, a.doc, a.ref, a.amount, b.account, b.account_type, b.description as acct_descr,c.description, e.journal_code, e.journal
from fin.fact_gl  a
JOIn fin.dim_account b on a.account_key = b.account_key
--   and b.account = '12502'
join fin.dim_gl_description c on a.gl_description_key = c.gl_Description_key
--   and c.description like 'payrol%'
join dds.dim_date d on a.date_key = d.date_key
  and d.year_month = 202110
join fin.dim_journal e on a.journal_key = e.journal_key  
where control = '150126'
  and a.ref in ('107391','107765','108065')
order by a.ref, b.account  

-- wilkie 202112

	select d.the_date, a.control, a.doc, a.ref, a.amount, b.account, b.account_type, b.description as acct_descr,c.description, e.journal_code, e.journal
	from fin.fact_gl  a
	JOIn fin.dim_account b on a.account_key = b.account_key
	--   and b.account = '12502'
	join fin.dim_gl_description c on a.gl_description_key = c.gl_Description_key
	--   and c.description like 'payrol%'
	join dds.dim_date d on a.date_key = d.date_key
		and d.year_month = 202112
	join fin.dim_journal e on a.journal_key = e.journal_key  
	where control = '1149180'
		and a.ref in ('109008','109341','109370')
order by a.ref, b.account  		
---------------------------------------------------------------------
-- ukg:  
--		fin.fact_gl.ref = voucher, the problem is i can see the voucheer in the preview but can't find it in the data
--    	submitted a case with ukg to see if the voucher exists in the api\
--    in the mean time, should be able to work with fin.fact_gl.date = ukg.payroll.pay_date
--  

---------------------------------------------------------------------

-- gehrtz 202110
select d.the_date, a.control, a.doc, a.ref, a.amount, b.account, b.account_type, b.description as acct_descr,c.description, e.journal_code, e.journal
from fin.fact_gl  a
JOIn fin.dim_account b on a.account_key = b.account_key
--   and b.account = '12502'
join fin.dim_gl_description c on a.gl_description_key = c.gl_Description_key
--   and c.description like 'payrol%'
join dds.dim_date d on a.date_key = d.date_key
  and d.year_month = 202202
join fin.dim_journal e on a.journal_key = e.journal_key  
where control = '150126'
  and a.ref = '9275'
order by a.ref, b.account  

select * from ukg.employees where last_name = 'gehrtz'  -- 150126 / 12987528627


select * 
from ukg.payrolls a
join ukg.pay_statements b on a.payroll_id = b.payroll_id
join ukg.pay_statement_earnings c on a.payroll_id = c.payroll_id and b.pay_statement_id = c.pay_statement_id
  and c.employee_account_id = 12987528627
where pay_date = '02/18/2022'




select * from ukg.json_payroll_batches