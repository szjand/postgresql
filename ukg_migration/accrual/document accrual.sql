﻿select * from pra.biweekly_dates

accrual dates: 
	from: the day after the end of the last pay period in the mo nth
	thru: last day of the month
pay period dates:
  from: the first pay period start date in the  month
  thru: 
  select aa.*,
    (select count(*) from dds.dim_date where the_date between accrual_from_date and accrual_thru_date and weekday and not holiday) as work_days
  from (
		select 202208,
			(select first_of_month from sls.months where year_month = 202208),
			max(payroll_end_date) + 1 as accrual_from_date, 
			(select last_of_month from sls.months where year_month = 202208) as accrual_thru_date,
			min(payroll_start_date) as pay_period_from_date, max(payroll_end_date) as pay_period_thru_date, count(distinct pay_date)	as pay_dates
		from ukg.payrolls a
		join dds.dim_date b on a.pay_date = b.the_date
			and b.year_month = 202208
		where payroll_name like '%bi-weekly%') aa; 


	