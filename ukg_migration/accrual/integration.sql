﻿drop table if exists  ukg_mig.accrual_integration;
create table ukg_mig.accrual_integration (
  salary_key citext primary key,
  salary_value citext);
  

-- from afton
select the_key, split_part(trim(the_value),' ',1) as account ,round((replace(substring(the_value,'\((.+)\)'),'%','')::numeric)/100,3) as percentage
from (
	select the_key, unnest(string_to_array(the_value,',')) as the_value
	from afton.afton_puzz) A

-- had to increase the rounding to 4, to handle 16.66%, 16.67%,  rounding to 3 resulted in sums > 1
select salary_key, split_part(trim(salary_value),' ',1) as account ,round((replace(substring(salary_value,'\((.+)\)'),'%','')::numeric)/100,4) as percentage
from (
	select salary_key, unnest(string_to_array(salary_Value,',')) as salary_value 
	from ukg_mig.accrual_integration) a



select a.*
from ukg_mig.accrual_integration a
join (
	select salary_key
	from (
	select salary_key, split_part(trim(salary_value),' ',1) as account ,round((replace(substring(salary_value,'\((.+)\)'),'%','')::numeric)/100,4) as percentage
	from (
		select salary_key, unnest(string_to_array(salary_Value,',')) as salary_value 
		from ukg_mig.accrual_integration) a) b
	group by salary_key	
	having sum(percentage) > 1) c on a.salary_key = c.salary_key
order by salary_key


	select salary_key, salary_value, split_part(trim(salary_value),' ',1) as account ,round((replace(substring(salary_value,'\((.+)\)'),'%','')::numeric)/100,4) as percentage
	from (
		select salary_key, unnest(string_to_array(salary_Value,',')) as salary_value 
		from ukg_mig.accrual_integration) a
where salary_key = 'taxes4'		
	