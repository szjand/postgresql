﻿/*
3/11/22
jeri says we are good to go:

I have finalized PTO rates and they are being worked through payroll.  If the current rate was less, we ended up not doing an adjustment.  

The next step would be to determine the process for identifying when updates are needed and putting this into a report format in vision that Kim can run for payroll.


payroll is updating everyone from the catch up series, the decision has been made that noone is to receive a decrease in pto rate

we are to build a vision page, that will for each pay period/month display everyone
with an anniv in that period and their new pto rate 
the fields: 
last_name	first_name	emp #	store 	dept	manager_1	manager_2	seniority_date	upcoming_anniv	earnings	clock_hours	current_pto_rate	new_pto_rate	effective_rate	diff
everyone is to be displayed if diff is negative, the effective ratte is the current rate and diff is displayed as a 0

for biweekly earnings are based on check pay dates, hours are based on the pay periods
semi monthly is the full month approach
*/

ALTER SCHEMA pto
RENAME TO pto_pre_ukg;

create schema pto;
comment on schema pto is 'pto objects for use with ukg';

the first thing i want to do is to capture the catch up period data

-------------------------------------------------------------------------------
--< 3/12/22
-- 		these are the queries for biweekly catch up period
-- 		flat_rate_pto_data_catchup_period_v3.xlsx
-- convert all these tables to schema pto
-------------------------------------------------------------------------------
-- checked against spreadsheet
select a.last_name, a.first_name, a.employee_number, a.cost_center, a.seniority_date, a.current_pto_rate, 
	/*b.from_date, b.thru_date,*/ 
	c.earnings, d.clock_hours, --d.pto_hours, d.hol_hours,
--   clock_hours - pto_hours - hol_hours as total_hours,
--   round(c.earnings/(clock_hours - pto_hours - hol_hours), 2) as calc_pto_rate, 
--   (round(c.earnings/(clock_hours - pto_hours - hol_hours), 2)) - current_pto_rate as diff,
  round(c.earnings/clock_hours, 2) as calc_pto_rate, round(c.earnings/clock_hours, 2) - current_pto_rate as diff
from ukg_mig.pto_employees a
-- join ukg_mig.flat_rate_hours_date_range b on a.employee_number = b.employee_number
join ukg_mig.pto_flat_rate_catch_up_earnings c on a.employee_number = c.employee_number
join ukg_mig.flat_rate_catch_up_clock_hours d on a.employee_number = d.employee_number
left join ukg_mig.gm_tech_pto e on a.employee_number = e.employee_number
where a.pay_calc_profile = 'Full-Time Flat Rate'

-- checked against spreadsheet (flat rate and sales)
drop table if exists ukg_mig.pto_employees cascade;
create table ukg_mig.pto_employees as
elect * from (
select a.last_name, a.first_name, a.employee_number, a.employee_id, c.display_name as pay_calc_profile, 
	(d.store||'/'|| d.department||'/'|| d.cost_center)::citext as cost_center, coalesce(a.seniority_date, a.hire_date) as seniority_date,
	e.current_rate as current_pto_rate,
	case
		when -- current year anniversary
			to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' ) < current_date 
				then to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' )
		else (to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' ) - interval '1 year')::date
	end as most_recent_anniv	
from ukg.employees a
join ukg.employee_profiles b on a.employee_id = b.employee_id
join ukg.ext_pay_calc_profiles c on b.pay_calc_id = c.id
  and c.display_name in ('Full-Time Flat Rate','Full-Time Sales Commission Only')
join (select * from ukg.get_cost_center_hierarchy()) d on a.cost_center = d.cost_center  
  and 
    case
      when a.store = 'GM' then d.store = 'Rydell GM'
      when a.store = 'HN' then d.store = 'Rydell Honda Nissan'
    end
left join ukg.personal_rate_tables e on b.rate_table_id = e.rate_table_id
  and e.counter = 'paid time off'
where a.status = 'active') s
where seniority_date <> most_Recent_anniv; -- excludes employees with less than a year employment


-- checked against spreadsheet
drop table if exists ukg_mig.pto_flat_rate_catch_up_earnings cascade;
create table ukg_mig.pto_flat_rate_catch_up_earnings as
-- no no no not using pay period dates for earnings, but check dates in the previous year
select employee_number, sum(earnings) as earnings 
from (-- dealer track 2020/2021 earnings
	select a.employee_number, coalesce(b.base_pay, 0) + coalesce(c.amount, 0) as earnings -- needed base pay for some techs sum(c.amount) as earnings
	from ukg_mig.pto_employees  a
	join arkona.ext_pyhshdta b on a.employee_number = b.employee_
		and (b.check_month || '-' || b.check_day || '-' || (2000 + b. check_year))::date between '03/01/2021' and '02/28/2022'
		and b.total_gross_pay <> 0
	left join arkona.ext_pyhscdta c on b.company_number = c.company_number -- does not exists for everyone, so left join
		and b.payroll_run_number = c.payroll_run_number 
		and a.employee_number = c.employee_number
		and c.code_type = '1'
		and c.code_id in ('79','C19','cov','75','87','70')
	where a.pay_calc_profile = 'Full-Time Flat Rate'    
	union all -- ukg earnings
	select a.employee_number, b.ee_amount as earnings
	from ukg_mig.pto_employees  a
	join ukg.pay_statement_earnings b on a.employee_id = b.employee_account_id
		and coalesce(b.ee_amount, 0) <> 0
	join ukg.payrolls c on b.payroll_id = c.payroll_id
		and c.pay_date between '03/01/2021' and '02/28/2022'
	where b.earning_code not in (
			'Benefit Allowance','Deferred Comp','Employee Referral Bonus','Expense Reimbursement','Gift Card',
			'Holiday','Lease Program', 'Lease Program 2', 'Lease Program 3', 'Lease Program 4', 'Lease Program 5',
			'Moving Expenses','Non-Cash Wages','OnStar Commission','Other Discretionary Bonus',
			'Owner Health Insurance Premium','Paid Time Off','PTO Payout','Severance','Sign-On Bonus',
			'Tool Allowance','Vision Award','Volunteer PTO','Year End Manager Bonus','Year End Owner Bonus',
			'2021 401K Hours')  -- just the 2 anomalies with amounts
	and a.pay_calc_profile = 'Full-Time Flat Rate'  ) x
group by employee_number;
alter table ukg_mig.pto_flat_rate_catch_up_earnings add primary key (employee_number);

drop table if exists ukg_mig.pto_flat_rate_catch_up_dates cascade;
create table ukg_mig.pto_flat_rate_catch_up_dates as
select distinct aa.min_check_date, aa.max_check_date, c.biweekly_pay_period_start_date, /*d.biweekly_pay_period_end_date,*/ e.biweekly_pay_period_end_date
from (
	select min((b.check_month || '-' || b.check_day || '-' || (2000 + b. check_year))::date) as min_check_date,
		max(c.pay_date) as max_check_date
	from ukg_mig.pto_employees a
	join arkona.ext_pyhshdta b on a.employee_number = b.employee_
		and (b.check_month || '-' || b.check_day || '-' || (2000 + b. check_year))::date between '03/01/2021' and '02/28/2022'
		and b.total_gross_pay <> 0
	join ukg.payrolls c on true	
		and c.payroll_name like '%bi-weekly%'
		and c.pay_date < '02/28/2022'
	where a.pay_calc_profile = 'Full-Time flat Rate') aa
left join dds.dim_date b on aa.min_check_date = b.the_date
left join dds.dim_date c on b.biweekly_pay_period_sequence = c.biweekly_pay_period_sequence + 1	
left join dds.dim_date d on aa.max_check_date = d.the_date
left join dds.dim_date e on d.biweekly_pay_period_sequence = e.biweekly_pay_period_sequence + 1;

-- checked against spreadsheet
drop table if exists ukg_mig.flat_rate_catch_up_clock_hours cascade;
create table ukg_mig.flat_rate_catch_up_clock_hours as
select employee_number, sum(clock_hours) as clock_hours, sum(pto_hours) as pto_hours, sum(hol_hours) as hol_hours
from (
	select a.employee_number, a.clock_hours, a.pto_hours, a.hol_hours
	from ukg.clock_hours a
	join ukg_mig.pto_employees b on a.employee_number = b.employee_number
	  and b.pay_calc_profile = 'Full-Time flat Rate'
	join ukg_mig.pto_flat_rate_catch_up_dates c on a.the_date between c.biweekly_pay_period_start_date and c.biweekly_pay_period_end_date
	where a.the_date > '12/18/2021'
	union all
	select a.employee_number, a.clock_hours, a.vac_hours + a.pto_hours as pto_hours, a.hol_hours
	from arkona.xfm_pypclockin a
	join ukg_mig.pto_employees b on a.employee_number = b.employee_number
	  and b.pay_calc_profile = 'Full-Time flat Rate'
	join ukg_mig.pto_flat_rate_catch_up_dates c on a.the_date between c.biweekly_pay_period_start_date and c.biweekly_pay_period_end_date
	where a.the_date < '12/19/2021') x
group by employee_number;
-------------------------------------------------------------------------------
--/> 3/12/22
-- 		these are the queries for biweekly catch up period
-- 		flat_rate_pto_data_catchup_period_v3.xlsx
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--< 3/12/22
-- 		these are the queries for semimonthly catch up period
-- 		sales_pto_data_catchup_period_V2_3.8.22.xlsx
-------------------------------------------------------------------------------

-- checked against spreadsheet
select a.last_name, a.first_name, a.employee_number as "emp #", a.cost_center, pay_calc_profile, manager_1, manager_2, a.seniority_date, most_recent_anniv, a.current_pto_rate, c.earnings,
-- 	a.*, /*b.from_date, b.thru_date,*/ c.earnings, /*d.clock_hours,*/ d.pto_hours, d.hol_hours,
  pto_hours, hol_hours,
  case
    when a.employee_number = '159857' then 1360 - pto_hours - hol_hours
    else 2080 - pto_hours - hol_hours 
  end as total_hours,
  case
    when a.employee_number = '159857' then round(c.earnings/(1360 - pto_hours - hol_hours), 2)
    else round(c.earnings/(2080 - pto_hours - hol_hours), 2) 
  end as calc_pto_rate,
  case
    when a.employee_number = '159857' then (round(c.earnings/(1360 - pto_hours - hol_hours), 2)) - a.current_pto_rate
    else (round(c.earnings/(2080 - pto_hours - hol_hours), 2)) - a.current_pto_rate 
  end as diff
--   e.cur_rate_eff_date
from ukg_mig.pto_employees a
join ukg.employees aa on a.employee_id = aa.employee_id 
-- join ukg_mig.sales_date_range b on a.employee_number = b.employee_number
join ukg_mig.pto_sales_catch_up_earnings c on a.employee_number = c.employee_number
join ukg_mig.pto_sales_catch_up_clock_hours d on a.employee_number = d.employee_number
left join ukg_mig.sc e on a.employee_number = e.employee_number
where a.pay_calc_profile = 'Full-Time Sales Commission Only'
order by cost_center, last_name

-- checked against spreadsheet
drop table if exists ukg_mig.pto_sales_catch_up_earnings cascade;
create table ukg_mig.pto_sales_catch_up_earnings as
select employee_number, sum(earnings) as earnings
from (
	select a.employee_number, e.amount as earnings
	from ukg_mig.pto_employees a
-- 	join ukg_mig.sales_date_range b on a.employee_number = b.employee_number
	join arkona.ext_pyptbdta c on arkona.db2_integer_to_date(c.payroll_start_date) between '03/01/2021' and '02/28/2022'
		and
			case
				when a.cost_center like '%GM%' then c.company_number = 'RY1'
				when a.cost_center like '%Honda%' then c.company_number = 'RY2'
			end
-- 	join arkona.ext_pyhshdta d on c.company_number = d.company_number
-- 		and c.payroll_run_number = d.payroll_run_number
-- 		and a.employee_number = d.employee_
-- 		and d.total_gross_pay <> 0
	left join arkona.ext_pyhscdta e on c.company_number = e.company_number -- does not exists for everyone, so left join
		and c.payroll_run_number = e.payroll_run_number 
		and a.employee_number = e.employee_number
		and e.code_type = '1'
		and e.code_id in ('79A','79','78','c19','79B','82')        
	where a.pay_calc_profile = 'Full-Time Sales Commission Only'    
	union all -- ukg earnings
	select a.employee_number, c.ee_amount as earnings
	from ukg_mig.pto_employees  a
-- 	join ukg_mig.sales_date_range b on a.employee_number = b.employee_number
	join ukg.pay_statement_earnings c on a.employee_id = c.employee_account_id
		and coalesce(c.ee_amount, 0) <> 0
	join ukg.payrolls d on c.payroll_id = d.payroll_id
		and d.payroll_start_date between '03/01/2021' and '02/28/2022'
	where c.earning_code not in (
			'Benefit Allowance','Deferred Comp','Employee Referral Bonus','Expense Reimbursement','Gift Card',
			'Holiday','Lease Program', 'Lease Program 2', 'Lease Program 3', 'Lease Program 4', 'Lease Program 5',
			'Moving Expenses','Non-Cash Wages','OnStar Commission',/*'Other Discretionary Bonus',*/
			'Owner Health Insurance Premium','Paid Time Off','PTO Payout','Severance','Sign-On Bonus',
			'Tool Allowance','Vision Award','Volunteer PTO','Year End Manager Bonus','Year End Owner Bonus',
			'2021 401K Hours')  -- just the 2 anomalies with amounts
		and a.pay_calc_profile = 'Full-Time Sales Commission Only') x
group by employee_number;

-- select * from ukg_mig.pto_sales_catch_up_earnings

drop table if exists ukg_mig.pto_sales_catch_up_clock_hours cascade;
create table ukg_mig.pto_sales_catch_up_clock_hours as
select employee_number, sum(clock_hours) as clock_hours, sum(pto_hours) as pto_hours, sum(hol_hours) as hol_hours
from (
	select a.employee_number, a.clock_hours, a.pto_hours, a.hol_hours
	from ukg.clock_hours a
	join ukg_mig.pto_employees b on a.employee_number = b.employee_number
    and pay_calc_profile = 'Full-Time Sales Commission Only'
	where 
	  case
	    when a.employee_number = '159857' then a.the_date between '07/01/2021' and '02/28/2022'
	    else a.the_date between '03/01/2021' and '02/28/2022'
	  end
	and a.the_date > '12/18/2021'
	union all
	select a.employee_number, a.clock_hours, a.vac_hours + a.pto_hours as pto_hours, a.hol_hours
	from arkona.xfm_pypclockin a
	join ukg_mig.pto_employees b on a.employee_number = b.employee_number
    and pay_calc_profile = 'Full-Time Sales Commission Only'
	where 
	  case
	    when a.employee_number = '159857' then a.the_date between '07/01/2021' and '02/28/2022'
	    else a.the_date between '03/01/2021' and '02/28/2022'
	  end
	and a.the_date < '12/19/2021') x
group by employee_number
order by employee_number::integer

-------------------------------------------------------------------------------
--/> 3/12/22
-- 		these are the queries for semimonthly catch up period
-- 		sales_pto_data_catchup_period_V2_3.8.22.xlsx
-------------------------------------------------------------------------------

select * from dds.dim_date where the_date = current_date - 1


-- drop table if exists ukg_mig.pto_employees cascade;
-- create table ukg_mig.pto_employees as
-- after going round and round, this table does not need history, the history for the pay periods will be
-- in the earnings and clockhours, and i believe that is good enough
-- this is just a working table, generated at the beginning of the pay period, used for generating those other tables

/*
fuck fuck fuck
ok, where i am now, run employees on each sunday, fine
but only run the rest on the actual pay period start dates, handle that
in python, if it is the first of the pay period run the functions, if not exit
03/17 never mind, not an issue, simple if statement in this function can handle it, 
	if it is the 1st of a pay period, run, else don't run
	
in addition the whole current_date -1 is not necessary, these functdions are about generating data
based on a date range that actually starts when the script is running

also, fuck it, separate tables pto.flat_rate_employees, pto.sales_employees

03/17/22
	big discovery, brian peterson falls within 3/13 -> 3/26 with and anniv of 3/15
	but when i run these scripts on 3/17, he doesn't show up, because his most recent anniv
	now is 3/15/2022 instead of 3/15/2021
*/

-------------------------------------------------------------------------------------------------
--< flat rate
-------------------------------------------------------------------------------------------------
-- need to adjust this date to be sunday, 3/13
do $$
declare
-- this can actually run okay on each sunday, if clause does nothing if the date is not the first of a pay period
	_first_of_pp date := (
	  select distinct biweekly_pay_period_start_date
	  from dds.dim_date
	  where biweekly_pay_period_start_date = current_date); 
	_last_of_pp date := (
	  select biweekly_pay_period_end_date
	  from dds.dim_date 
	  where the_date = _first_of_pp);
	_pay_period_seq integer := (
	  select biweekly_pay_period_sequence
	  from dds.dim_date
	  where the_date = _first_of_pp);
begin
	if _first_of_pp is not null then 
	-- this table is a non persistent set of employees upon which to do the pto rate calculations on anniversaries
		delete 
		from pto.flat_rate_employees;
		insert into pto.flat_rate_employees(last_name,first_name,employee_number,employee_id,pay_calc_profile,
		  store,department,cost_center,manager_1, manager_2,
			seniority_date,current_pto_rate,most_recent_anniv)
		select * 
		from (
			select a.last_name, a.first_name, a.employee_number, a.employee_id, c.display_name as pay_calc_profile, 
				d.store,  d.department, d.cost_center, a.manager_1, a.manager_2, coalesce(a.seniority_date, a.hire_date) as seniority_date,
				e.current_rate as current_pto_rate,
				case
					when -- current year anniversary
						to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' ) < current_date 
							then to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' )
					else (to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' ) - interval '1 year')::date
				end as most_recent_anniv	
			from ukg.employees a
			join ukg.employee_profiles b on a.employee_id = b.employee_id
			join ukg.ext_pay_calc_profiles c on b.pay_calc_id = c.id
				and c.display_name = 'Full-Time Flat Rate'
			join (select * from ukg.get_cost_center_hierarchy()) d on a.cost_center = d.cost_center  
				and 
					case
						when a.store = 'GM' then d.store = 'Rydell GM'
						when a.store = 'HN' then d.store = 'Rydell Honda Nissan'
					end
			left join ukg.personal_rate_tables e on b.rate_table_id = e.rate_table_id
				and e.counter = 'paid time off'
			where a.status = 'active') s
		where seniority_date <> most_Recent_anniv; -- excludes employees with less than a year employment




-- -- these are the employees with anniv in the pay period
-- 		select *, (most_recent_anniv + interval '1 year')::date, (most_recent_anniv + interval '1 year')::date - 1
-- 		from pto.flat_rate_employees
-- 		where pay_calc_profile = 'Full-Time Flat Rate'
-- 		  and (most_recent_anniv + interval '1 year')::date between '03/13/2022' and '03/26/2022'; -- _first_of_pp and _last_of_pp
-- 		  
-- check dates
-- first ukg check: check date: 1/7/22, pay period: 12/19/21 -> 1/1/22
-- last dt check: check_date 12/24/21, pay period 12/5 -> 12/18/21


-- Looking at the flat rate techs that have an anniversary in the pay period 03/13 -03/26/22
-- There are 2, Brian Peterson and Paul Sobolik.
-- Brian: 
-- 	Anniversary: 3/15/22
-- 	For the period of 3/15/21 thru 3/14/22, the first check date is 3/19/21, the last check date is 3/4/22, equaling 25 pay checks for earnings
-- 	The pay periods covered by those check dates are 2/28/21 thru 2/26/22, a total of 364 days for clock hours
-- Paul:
-- 	Anniversary: 3/23/22
-- 	For the period of 3/23/21 thru 3/22/22, the first check date is 4/2/21, the last check date is 3/4/22, equaling 24 pay checks for earnings
-- 	The pay periods covered by those check date are 3/14/221 thru 2/26/22, a total of 350 days for clock hours
-- 
-- Initially I was taken aback at the difference in checks and days, but it actually equals out.

		delete
		from pto.flat_rate_employee_dates
		where pay_period_seq = _pay_period_seq;
		
		insert into pto.flat_rate_employee_dates(pay_period_seq,pay_period_start_date,pay_period_end_date,employee_number,
			last_anniv,next_anniv,min_check_date,max_check_date,clock_from_date,clock_thru_date)
		select distinct _pay_period_seq, _first_of_pp, _last_of_pp, aa.employee_number, aa.most_recent_anniv, aa.next_anniv, aa.min_check_date, 
			aa.max_check_date,c.biweekly_pay_period_start_date clock_from_date, e.biweekly_pay_period_end_date as clock_thru_date
		from (
			select a.employee_number, a.most_recent_anniv, a.next_anniv, 
			  case  -- the first biweekly check in ukg had a pay date of 1/7/22, anything before that requires dealertrack payroll data
			    when a.most_recent_anniv < '01/07/2022' then min((b.check_month || '-' || b.check_day || '-' || (2000 + b. check_year))::date) 
			    else min(c.pay_date)
			  end as min_check_date,
				max(c.pay_date) as max_check_date
			from (
				select *, (most_recent_anniv + interval '1 year')::date as next_anniv 
				from pto.flat_rate_employees
				where pay_calc_profile = 'Full-Time Flat Rate'
					and (most_recent_anniv + interval '1 year')::date between _first_of_pp and _last_of_pp) a 	
			join arkona.ext_pyhshdta b on a.employee_number = b.employee_
				and (b.check_month || '-' || b.check_day || '-' || (2000 + b. check_year))::date between a.most_recent_anniv and a.next_anniv - 1
				and b.total_gross_pay <> 0
			join ukg.payrolls c on true	
				and c.payroll_name like '%bi-weekly%'
				and c.pay_date between a.most_recent_anniv and a.next_anniv - 1
			where a.pay_calc_profile = 'Full-Time flat Rate'
			group by a.employee_number, a.most_recent_anniv, a.next_anniv) aa	
		left join dds.dim_date b on aa.min_check_date = b.the_date
		-- the pay period previous to the one that includes the min_check_date
		left join dds.dim_date c on b.biweekly_pay_period_sequence = c.biweekly_pay_period_sequence + 1	
		left join dds.dim_date d on aa.max_check_date = d.the_date
		-- the pay period previous to the one that includes the max check date
		left join dds.dim_date e on d.biweekly_pay_period_sequence = e.biweekly_pay_period_sequence + 1;

  -- flat rate:
    -- include commissions,covid,rate adjust, tech hours pay,tech xtra bonus,
		-- exclude holiday pay, health insurance,lease program, pay out pto,vac/pto pay
  -- sales:
		-- include A-Z Sales F&I, COMMISSIONS,DRAWS','PULSE PAY','COVID 19','SPIFF PAY OUTS'
		-- exclude LEASE PROGRAM,PAY OUT PTO,'SALES VACATION'


-- need 2 versions somehow
-- only need dealertrack data if the min_check_date is prior to 01/07/2022
-- rather than hassle with the logic now, when we get to that point in time, the lack of data
-- from dealertrack will not have any effect on this query, uinion ukg earnings with null = ukg earnings

		delete
		from pto.flat_rate_earnings
		where pay_period_seq = _pay_period_seq;

		insert into pto.flat_rate_earnings(pay_period_seq,employee_number, earnings_code,earnings)
		select pay_period_seq, employee_number, earnings_code, sum(earnings) as earnings
		from (
			select b.pay_period_seq, a.employee_number, 
				(c.check_month || '-' || c.check_day || '-' || (2000 + c. check_year))::date as check_date,
				d.code_id || ' ' || d.description as earnings_code, coalesce(c.base_pay, 0) + coalesce(d.amount, 0) as earnings
			from pto.flat_rate_employees a
			join pto.flat_rate_employee_dates b on a.employee_number = b.employee_number
			  and b.pay_period_seq = _pay_period_seq
			join arkona.ext_pyhshdta c on a.employee_number = c.employee_
				and (c.check_month || '-' || c.check_day || '-' || (2000 + c. check_year))::date between b.min_check_date and b.max_check_date
				and c.total_gross_pay <> 0
			left join arkona.ext_pyhscdta d on c.company_number = d.company_number -- does not exists for everyone, so left join
				and c.payroll_run_number = d.payroll_run_number 
				and a.employee_number = d.employee_number
				and d.code_type = '1'
				and d.code_id in ('79','C19','cov','75','87','70')	
			union all
			select aa.pay_period_seq,a.employee_number, c.pay_date, d.earnings_code, b.ee_amount as earnings
			from pto.flat_rate_employees a
			join pto.flat_rate_employee_dates aa on a.employee_number = aa.employee_number
			  and aa.pay_period_seq = _pay_period_seq
			join ukg.pay_statement_earnings b on a.employee_id = b.employee_account_id
				and coalesce(b.ee_amount, 0) <> 0
			join ukg.payrolls c on b.payroll_id = c.payroll_id
				and c.pay_date between aa.min_check_date and aa.max_check_date
			join pto.earnings_codes d on b.earning_code	= d.earnings_code
				and d.include_in_pto_rate_calc) e
		group by pay_period_seq, employee_number, earnings_code;

		delete
		from pto.flat_rate_clock_hours
		where pay_period_seq = _pay_period_seq;

		insert into pto.flat_rate_clock_hours(pay_period_seq,employee_number,clock_hours,pto_hours,hol_hours)
		select _pay_period_seq, employee_number, sum(clock_hours) as clock_hours, sum(pto_hours) as pto_hours, sum(hol_hours) as hol_hours
		from (
			select a.employee_number, c.the_date, c.clock_hours, c.pto_hours, c.hol_hours
			from pto.flat_rate_employees a
			join pto.flat_rate_employee_dates b on a.employee_number = b.employee_number
				and b.pay_period_seq = _pay_period_seq
			join ukg.clock_hours c on a.employee_number = c.employee_number
				and c.the_date between b.clock_from_date and b.clock_thru_date
			where c.the_date > '12/18/2021'  
			union
			select a.employee_number, c.the_date, c.clock_hours, c.vac_hours + c.pto_hours as pto_hours, c.hol_hours
			from pto.flat_rate_employees a
			join pto.flat_rate_employee_dates b on a.employee_number = b.employee_number
				and b.pay_period_seq = _pay_period_seq
			join arkona.xfm_pypclockin c on a.employee_number = c.employee_number
				and c.the_date between b.clock_from_date and b.clock_thru_date
			where c.the_date < '12/19/2021') d
		group by employee_number;

	end if;		  
end $$;

select * from pto.flat_rate_earnings

-- this looks good for the page/spreadsheet for biweekly pay period 3/13 -> 3/26 
-- made sure that current pto rate had been updated to the catch up values
do $$
declare
	_pay_period_seq integer := (
	  select biweekly_pay_period_sequence
	  from dds.dim_date
	  where the_date = '03/13/2022');
begin
drop table if exists wtf;
create temp table wtf as	  
select a.last_name, a.first_name, a.employee_number, a.store, a.department, a.cost_center, 
  a.manager_1, a.manager_2, a.seniority_date, b.next_anniv, d.earnings, c.clock_hours, a.current_pto_rate, 
  round(earnings/clock_hours, 2) as new_pto_rate,
  case
    when round(d.earnings/c.clock_hours, 2) > a.current_pto_rate then round(d.earnings/c.clock_hours, 2)
    else a.current_pto_rate
  end as effective_pto_rate,
  case
    when round(d.earnings/c.clock_hours, 2) < a.current_pto_rate then 0
    else round(d.earnings/c.clock_hours, 2) - a.current_pto_rate
  end as diff
-- select *
from pto.flat_rate_employees a
join pto.flat_rate_employee_dates b on a.employee_number = b.employee_number
  and b.pay_period_seq = _pay_period_seq
join pto.flat_rate_clock_hours c on a.employee_number = c.employee_number
  and b.pay_period_seq = c.pay_period_seq
join (
  select pay_period_seq, employee_number, sum(earnings) as earnings
  from pto.flat_rate_earnings
  where pay_period_seq = _pay_period_seq
  group by pay_period_seq, employee_number) d on a.employee_number = d.employee_number
  and b.pay_period_seq = d.pay_period_seq;
end $$;
select * from wtf;  


-------------------------------------------------------------------------------------------------
--/> flat rate
-------------------------------------------------------------------------------------------------
  			
-- sent to jeri as flat_rate_pto_data_catchup_period.xlsx on 2/28/22
select * from (
select a.last_name, a.first_name, a.employee_number, a.cost_center, a.seniority_date, a.current_pto_rate, 
	/*b.from_date, b.thru_date,*/ 
	c.earnings, d.clock_hours, --d.pto_hours, d.hol_hours,
--   clock_hours - pto_hours - hol_hours as total_hours,
--   round(c.earnings/(clock_hours - pto_hours - hol_hours), 2) as calc_pto_rate, 
--   (round(c.earnings/(clock_hours - pto_hours - hol_hours), 2)) - current_pto_rate as diff,
  round(c.earnings/clock_hours, 2) as calc_pto_rate, round(c.earnings/clock_hours, 2) - current_pto_rate as diff
from ukg_mig.pto_employees a
-- join ukg_mig.flat_rate_hours_date_range b on a.employee_number = b.employee_number
join ukg_mig.pto_flat_rate_catch_up_earnings c on a.employee_number = c.employee_number
join ukg_mig.flat_rate_catch_up_clock_hours d on a.employee_number = d.employee_number
left join ukg_mig.gm_tech_pto e on a.employee_number = e.employee_number
where a.pay_calc_profile = 'Full-Time Flat Rate'
) x where last_name in ('peterson','sobolik')
order by cost_Center, last_name
order by employee_number:: integer





selecT code_id, description from arkona.ext_pyhscdta where code_id in ('79','C19','cov','75','87','70') and payroll_cen_year = 121 group by code_id, description
-- 		select *, (most_recent_anniv + interval '1 year')::date, (most_recent_anniv + interval '1 year')::date - 1
-- 		from pto.flat_rate_employees
-- 		where pay_calc_profile = 'Full-Time Flat Rate'
-- 		  and (most_recent_anniv + interval '1 year')::date between '03/13/2022' and '03/26/2022'
select * from pto.flat_rate_earnings

select * from pto.flat_rate_employee_dates

select * 
from pto.flat_rate_employees a
join pto.flat_rate_employee_dates b on a.employee_number = b.employee_number


select c.*
from ukg.employees a
join ukg.employee_profiles b on a.employee_id = b.employee_id
left join ukg.personal_rate_tables c on b.rate_table_id = c.rate_table_id
where a.employee_number = '1106399'


select * from wtf
select * from wtf1

 




-- biweekly and semi-monthly should probably be done separately
-- biweekly pay period of 3/13 -> 3/26
select *, (most_recent_anniv + interval '1 year')::date
from ukg_mig.pto_employees
where (most_recent_anniv + interval '1 year')::date between '03/13/2022' and '03/26/2022'


select * from ukg_mig.pto_employees order by last_name


-- semi-monthly for march 22
select *, (most_recent_anniv + interval '1 year')::date
from ukg_mig.pto_employees
where cost_center like '%sales%' and (most_recent_anniv + interval '1 year')::date between '03/01/2022' and '03/31/2022'


-------------------------------------------------------------------------------------
--< sales
-- store base days, 2080 or gonzalez
-------------------------------------------------------------------------------------
-- select * from dds.dim_date where the_date = '03/01/2022'
do $$
declare
-- this will only be run on the first of the month via cron job
	_first_of_pp date := '03/01/2022'; ------------------------------ current_date;

	_last_of_pp date := (
	  select the_date
	  from dds.dim_date 
	  where month_of_year = extract(month from current_date)
	    and the_year = extract(year from current_date)
	    and last_day_of_month); 
	_year_month integer := (
	  select distinct year_month
	  from dds.dim_date
	  where the_date = current_date);
begin

-- this table is a non persistent set of employees upon which to do the pto rate calculations on anniversaries
	delete 
	from pto.sales_employees;
	insert into pto.sales_employees(last_name,first_name,employee_number,employee_id,pay_calc_profile,
		store,department,cost_center,manager_1, manager_2,
		seniority_date,current_pto_rate,most_recent_anniv)
	select * 
	from (
		select a.last_name, a.first_name, a.employee_number, a.employee_id, c.display_name as pay_calc_profile, 
			d.store,  d.department, d.cost_center, a.manager_1, a.manager_2, coalesce(a.seniority_date, a.hire_date) as seniority_date,
			e.current_rate as current_pto_rate,
			case
				when -- current year anniversary
					to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' ) < '03/01/2022' -----------------------current_date 
						then to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' )
				else (to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' ) - interval '1 year')::date
			end as most_recent_anniv	
		from ukg.employees a
		join ukg.employee_profiles b on a.employee_id = b.employee_id
		join ukg.ext_pay_calc_profiles c on b.pay_calc_id = c.id
			and c.display_name = 'Full-Time Sales Commission Only'
		join (select * from ukg.get_cost_center_hierarchy()) d on a.cost_center = d.cost_center  
			and 
				case
					when a.store = 'GM' then d.store = 'Rydell GM'
					when a.store = 'HN' then d.store = 'Rydell Honda Nissan'
				end
		left join ukg.personal_rate_tables e on b.rate_table_id = e.rate_table_id
			and e.counter = 'paid time off'
		where a.status = 'active') s
	where seniority_date <> most_Recent_anniv; -- excludes employees with less than a year employment

  delete 
  from pto.sales_employee_dates
  where year_month = _year_month;
  
  insert into pto.sales_employee_dates(year_month,pay_period_start,pay_period_end,employee_number,last_anniv,
    next_anniv,prev_year_from_date,prev_year_thru_date)
	SELECT _year_month, _first_of_pp, _last_of_pp, employee_number, most_recent_anniv as last_anniv, 
	  (most_recent_anniv + interval '1 year')::date as next_anniv,
		(
			select first_of_month
			from sls.months
			WHERE seq = (
				select seq - 12
				from sls.months
				where (most_recent_anniv + interval '1 year')::date between first_of_month and last_of_month)) as prev_year_from_date,
		(
			select last_of_month
			from sls.months
			WHERE seq = (
				select seq -1
				from sls.months
				where (most_recent_anniv + interval '1 year')::date between first_of_month and last_of_month)) as prev_year_thru_date
	from pto.sales_employees 
	where (most_recent_anniv + interval '1 year')::date between _first_of_pp and _last_of_pp;

  delete 
  from pto.sales_earnings
  where year_month = _year_month;

  insert into pto.sales_earnings(year_month,employee_number,earnings_code,earnings)
	select year_month, employee_number, earnings_code, sum(earnings) as earnings
	from (
		select b.year_month, a.employee_number, e.code_id || ' ' || e.description as earnings_code,
			coalesce(d.base_pay, 0) + coalesce(e.amount, 0) as earnings
		from pto.sales_employees a
		join pto.sales_employee_dates b on a.employee_number = b.employee_number
			and b.year_month = _year_month
		join arkona.ext_pyptbdta c on arkona.db2_integer_to_date(c.payroll_start_date) between b.prev_year_from_date and b.prev_year_thru_date
			and
				case
					when a.cost_center like '%GM%' then c.company_number = 'RY1'
					when a.cost_center like '%Honda%' then c.company_number = 'RY2'
				end
		join arkona.ext_pyhshdta d on c.company_number = d.company_number
			and c.payroll_run_number = d.payroll_run_number
			and a.employee_number = d.employee_
			and d.total_gross_pay <> 0
		left join arkona.ext_pyhscdta e on c.company_number = e.company_number -- does not exists for everyone, so left join
			and c.payroll_run_number = e.payroll_run_number 
			and a.employee_number = e.employee_number
			and e.code_type = '1'
			and e.code_id in ('79A','79','78','c19','79B','82')    	
		union all -- ukg earnings
		select b.year_month, a.employee_number, e.earnings_code, c.ee_amount as earnings
		from pto.sales_employees a
		join pto.sales_employee_dates b on a.employee_number = b.employee_number
			and b.year_month = _year_month
		join ukg.pay_statement_earnings c on a.employee_id = c.employee_account_id
			and coalesce(c.ee_amount, 0) <> 0
		join ukg.payrolls d on c.payroll_id = d.payroll_id
			and d.payroll_start_date between b.prev_year_from_date and b.prev_year_thru_date
		join pto.earnings_codes e on c.earning_code = e.earnings_code
			and e.include_in_pto_rate_calc) f
	group by year_month, employee_number, earnings_code;	

	delete 
	from pto.sales_clock_hours
	where year_month = _year_month;

	insert into pto.sales_clock_hours(year_month,employee_number,clock_hours,pto_hours,hol_hours)
	select year_month, employee_number, sum(clock_hours) as clock_hours, sum(pto_hours) as pto_hours, sum(hol_hours) as hol_hours
	from (
		select b.year_month, a.last_name, a.first_name, a.employee_number, c.the_date, c.clock_hours, c.pto_hours, c.hol_hours
		from pto.sales_employees a
		join pto.sales_employee_dates b on a.employee_number = b.employee_number
			and b.year_month = _year_month
		join ukg.clock_hours c on a.employee_number = c.employee_number
			and
				case
					when a.employee_number = '159857' then c.the_date between '07/01/2021' and b.prev_year_thru_date
					else c.the_date between b.prev_year_from_date and b.prev_year_thru_date
				end
			and c.the_date > '12/18/2021'
		union 
		select b.year_month, a.last_name, a.first_name, a.employee_number, c.the_date, c.clock_hours, c.pto_hours + c.vac_hours as pto_hours, c.hol_hours
		from pto.sales_employees a
		join pto.sales_employee_dates b on a.employee_number = b.employee_number
			and b.year_month = _year_month
		join arkona.xfm_pypclockin c on a.employee_number = c.employee_number
			and
				case
					when a.employee_number = '159857' then c.the_date between '07/01/2021' and b.prev_year_thru_date
					else c.the_date between b.prev_year_from_date and b.prev_year_thru_date
				end
			and c.the_date < '12/19/2021') x
	group by year_month, employee_number; 	
end $$;




do $$
declare
	_year_month integer := (
	  select year_month
	  from dds.dim_date
	  where the_date = '03/01/2022');
begin
drop table if exists wtf;
create temp table wtf as	
	select *, new_pto_rate - current_pto_rate as diff
	from ( 
	select a.last_name, a.first_name, a.employee_number, a.store, a.department, a.cost_center, 
		a.manager_1, a.manager_2, a.seniority_date, b.next_anniv, d.earnings, c.pto_hours, c.hol_hours,
		case
			when a.employee_number = '159857' then 1360 - c.pto_hours - c.hol_hours
			else 2080 - c.pto_hours - c.hol_hours 
		end as total_hours,
		a.current_pto_rate, 
		case
			when a.employee_number = '159857' then round(d.earnings/(1360 - c.pto_hours - c.hol_hours), 2)
			else round(d.earnings/(2080 - c.pto_hours - c.hol_hours), 2) 
		end as new_pto_rate-- select *
	from pto.sales_employees a
	join pto.sales_employee_dates b on a.employee_number = b.employee_number
		and b.year_month = _year_month
	join pto.sales_clock_hours c on a.employee_number = c.employee_number
		and b.year_month = c.year_month
	join (
		select year_month, employee_number, sum(earnings) as earnings
		from pto.sales_earnings
		where year_month = _year_month
		group by year_month, employee_number) d on a.employee_number = d.employee_number
			and b.year_month = d.year_month) e;
end $$;
select * from wtf;  




	
-------------------------------------------------------------------------------------
--/> sales
-------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------
--< admin
-------------------------------------------------------------------------------------
earnings codes


-------------------------------------------------------------------------------------
--/> admin
-------------------------------------------------------------------------------------