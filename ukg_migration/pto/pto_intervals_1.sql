﻿-- 01/26/22 we are definitely going to need 2021 earnings for computing pto rates, for sales, flat rate, ???



from sc_payroll_201803.py class PtoIntervalsDates(luigi.Task)
--< ----------------------------------------------------------------------------------------------------------------
comment on TABLE sls.pto_interval_dates is 'updated nightly in luigi sc_payroll_201803.py class PtoIntervalsDates(),
the table contains the 12 month period upon which the consultants pto rate is based, ie the earnings for that period';
                    select count(*)
                    from ( --c: personnel assigned to teams in open month
                      select a.*
                      from sls.personnel a
                      inner join sls.team_personnel b  on a.employee_number = b.employee_number
                      where b.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
                        and thru_date > (select last_day_of_previous_month from sls.months where open_closed = 'open')) c
                    left join sls.pto_intervals d on c.employee_number = d.employee_number
                      and d.year_month = (select year_month from sls.months where open_closed = 'open')
                    where d.employee_number is null;

--                     insert into sls.pto_interval_dates
                    select year_month, employee_number, anniversary_date, most_recent_anniversary,
                      case
                        when anniversary_date = most_recent_anniversary then anniversary_date
                      else (
                        select first_of_month
                        from sls.months
                        WHERE seq = (
                          select seq - 12
                          from sls.months
                          where v.most_recent_anniversary between first_of_month and last_of_month))
                      end as pto_period_from,
                      case
                        when anniversary_date = most_recent_anniversary then anniversary_date
                      else (
                        select last_of_month
                        from sls.months
                        WHERE seq = (
                          select seq -1
                          from sls.months
                          where v.most_recent_anniversary between first_of_month and last_of_month))
                      END as pto_period_thru
                    from ( -- v: 1 row per emp with most recent anniversary and no row in pto_intervals for open month
                           -- to_date is the anniv date for the current year
                      select (select year_month from sls.months where open_closed = 'open'),
                        last_name, first_name, employee_number, anniversary_date,
                        case -- most recent anniversary
                          when anniversary_date = to_date then anniversary_date
                          when to_date < current_date then to_date
                          when to_date > current_date then (to_date - interval '1 year'):: date
                        end as most_recent_anniversary
                      from (-- u: 1 row per emp with anniv date for cur year,  no row in pto_intervals for open month
                        select c.*,
                          to_date(extract(year from current_date)::text ||'-'||
                            extract(month from c.anniversary_date)::text
                            ||'-'|| extract(day from c.anniversary_date)::text, 'YYYY MM DD' ) -- current yr anniv date
                        from ( --c: personnel assigned to teams in open month
                          select a.*
                          from sls.personnel a
                          inner join sls.team_personnel b  on a.employee_number = b.employee_number
                          where b.from_Date <
                            (select first_day_of_next_month from sls.months where open_closed = 'open')
                            and thru_date >
                                (select last_day_of_previous_month from sls.months where open_closed = 'open')) c
                        left join sls.pto_intervals d on c.employee_number = d.employee_number
                          and d.year_month = (select year_month from sls.months where open_closed = 'open')
                        where d.employee_number is not null) u) v;

select b.last_name, b.first_name, a.* 
from sls.pto_interval_dates a
join ukg.employees b on a.employee_number = b.employee_number   

                
--/> ----------------------------------------------------------------------------------------------------------------

--< ---------------------------------------------------------------------------------------------------------------------------
class PtoIntervals(luigi.Task)

-- 01/26/22
-- how is this query returning values
--                     insert into sls.pto_intervals (employee_number,year_month,from_date,thru_date,
--                         most_recent_anniv,pto_rate)
                    select c.employee_number, c.year_month, c.from_date, c.thru_date, c.most_recent_anniversary,
                      coalesce(x.pto_rate, 0) as pto_rate
                    from sls.pto_interval_dates c
                    left join sls.pto_intervals d on c.employee_number = d.employee_number
                      and d.year_month = (select year_month from sls.months where open_closed = 'open')
                    left join (
                      select employee_number, year_month, total_gross,
                        -- 4.333 weeks/month, 5 working days/month, 8 hrs/day
                        round(total_gross/(4.333 * adj_count/2)/5/8, 2) as pto_rate, adj_count
                      from  (
                        select a.employee_number, a.year_month, sum(b.total_gross_pay) as total_gross,
                          count(*),
                          case
                            when count(*) >= 24 then 24
                            else count(*)
                          end as adj_count
                        from sls.pto_interval_dates a
                        left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
                          and (b.check_month || '-' || b.check_day || '-' || (2000 + b.check_year))::date
                            between a.from_date and a.thru_date
                        where a.from_date <> a.thru_date
                          and b.seq_void = '00'
                        group by a.employee_number, a.year_month) e) x on  c.employee_number = x.employee_number
                    where d.employee_number is null;
                    
select a.* , b.last_name, b.first_name, b.cost_center
from sls.pto_intervals a
join ukg.employees b on a.employee_number = b.employee_number
  and status = 'active'
left join  
where year_month = 202201
order by pto_rate

select a.*, b.last_name ||', ' || b.first_name, thru_date - from_date from sls.pto_interval_dates a join ukg.employees b on a.employee_number = b.employee_number

-- need a table with the 2021 earnings for sales consultants

select * from arkona.ext_pyhshdta where check_year = 21 limit 10
i keep getting confused over who should and who should not be in this
well, jeri clarified it today, anyone on full commission, so that includes aubol and grollimund but not longoira, dockendorf, etc

Now longoria tells me that since corey was out on the 3rd sick, he does not get the holiday pay, verified by jeri, the problem
is that is not documented anywhere

so, regardless of any currently 
                        select b.employee_name, a.employee_number, a.year_month, sum(b.total_gross_pay) as total_gross,
                          count(*),
                          case
                            when count(*) >= 24 then 24
                            else count(*)
                          end as adj_count
                        from sls.pto_interval_dates a
                        left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
                          and (b.check_month || '-' || b.check_day || '-' || (2000 + b.check_year))::date
                            between a.from_date and a.thru_date
--                         where a.from_date <> a.thru_date
--                           and b.seq_void = '00'
                        group by b.employee_name, a.employee_number, a.year_month
-- these are the consultants that show as no pto
-- from jeri 1/25:
-- In visiting with Laura and Kim—we are going to use the base of $36k over 2080 hours—so it would be $17.30
-- These rates should be pulled from the rate tables in UKG.

select * 
from ukg.employees
where employee_number in ('126948','135795','159857','189768','195480','254665','264845')
-- all consultants, & commission based fi mgrs (aubol & grollimund)
-- this gives me 29 consultants
-- but grollimund is cost center sales consultant and should be finance manager, this has been fixed
select a.employee_number, a.last_name, a.first_name, a.cost_center, a.status, c.pay_type_name, d.employee_type_name
from ukg.employees a
left join ukg.ext_employee_pay_info b on a.employee_id = b.employee_id
left join ukg.ext_pay_types c on b.pay_type_id = c.id
left join ukg.ext_employee_types d on b.employee_type_id = d.id
where a.cost_center in ( 'sales consultant', 'sales consultant outlet', 'sales consultant gm', 'finance manager', 'fleet sales consultant')
  and a.status = 'active'
order by cost_center, last_name  

select a.last_name, a.first_name, b.*
from ukg.employees a
left join ukg.ext_employee_pay_info b on a.employee_id = b.employee_id
where a.status = 'active'
order by a.last_name
                        
--/> ----------------------------------------------------------------------------------------------------------------




--< ---------------------------------------------------------------------------------------------------------------------------
from  FUNCTION sls.update_consultant_payroll()

        select a.employee_number, round(a.pto_hours, 2) as pto_hours, b.pto_rate, round(a.pto_hours * b.pto_rate, 2) as pto_pay
        from ukg.clock_hours a-- sls.clock_hours_by_month a
        left join sls.pto_intervals b on a.employee_number = b.employee_number
          and a.year_month = b.year_month
        where a.year_month = 202201
          and a.pto_hours <> 0
          and b.pto_rate <> 0      

select * from sls.clock_hours_by_month limit 10

select a.employee_number, b.year_month, sum(pto_hours) as pto_hours, sum(hol_hours) as hol_hours
from ukg.clock_hours a
join dds.dim_date b on a.the_date = b.the_date
join sls.personnel c on a.employee_number = c.employee_number
group by a.employee_number, b.year_month              

-- now why the fuck is this only 23 rows?
        select a.employee_number, a.pto_hours, a.hol_hours, b.pto_rate, round(a.pto_hours * b.pto_rate, 2) as pto_pay,
          round(a.hol_hours * b.pto_rate, 2) as hol_pay
        from (
					select a.employee_number, b.year_month, sum(pto_hours) as pto_hours, sum(hol_hours) as hol_hours
					from ukg.clock_hours a
					join dds.dim_date b on a.the_date = b.the_date
					join sls.personnel c on a.employee_number = c.employee_number
					group by a.employee_number, b.year_month) a        
        left join sls.pto_intervals b on a.employee_number = b.employee_number
          and a.year_month = b.year_month
        where a.year_month = 202201
          and (a.pto_hours <> 0 or a.hol_hours <> 0)
          and b.pto_rate <> 0    

-- while this shows all 28 consultants
-- should be 29, corey wilde does not show jan1 as holiday 1/20 emailed longoria he doesn't get the holiday due to out sick 1/3/22
select a.store_code, a.last_name ||', '|| a.first_name, a.employee_number, b.hol_hours 
from sls.personnel a
left join ukg.clock_hours b on a.employee_number = b.employee_number
  and b.the_date >= '01/01/2022'
  and b.hol_hours <> 0
where a.end_date > current_date  
  and a.last_name <> 'HSE'     

select * 
from sls.consultant_payroll
where year_month = 202112

select * 
from ukg.clock_hours
where employee_number = '2150210'


select aa.employee_number, cc.year_month, dd.pto_rate, sum(pto_hours) as pto_hours, round(sum(bb.pto_hours * dd.pto_rate), 2), sum(hol_hours) as hol_hours, round(sum(bb.hol_hours * dd.pto_rate), 2)
from (
	select a.employee_number, a.last_name, a.first_name
	from sls.personnel a
	inner join sls.team_personnel c on a.employee_number = c.employee_number
		and c.from_Date < (select first_day_of_next_month from sls.months
			where open_closed = 'open')
		and c.thru_date > (select last_day_of_previous_month from sls.months
			where open_closed = 'open')
	inner join sls.personnel_payplans d  on a.employee_number = d.employee_number
		and d.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
		and d.thru_date > (select last_day_of_previous_month from sls.months
			where open_closed = 'open')) aa
join ukg.clock_hours bb on aa.employee_number = bb.employee_number
  and (bb.pto_hours <> 0 or bb.hol_hours <> 0)
join dds.dim_date cc on bb.the_date = cc.the_date
  and cc.year_month = (select year_month from sls.months where open_closed = 'open')
join sls.pto_intervals dd on aa.employee_number = dd.employee_number
          and cc.year_month = dd.year_month
group by aa.employee_number, cc.year_month, dd.pto_rate


select * from sls.pto_intervals where employee_number = '159857'
!!!! 01/19/22 and this is where i go back to pto_intervals, because i have folks with no pto rate, eg 159857
 
                    insert into sls.clock_hours_by_month
                    select a.employee_number, c.year_month, sum(clock_hours) as clock_hours,
                      sum(regular_hours) as regular_hours, sum(b.overtime_hours) as overtime_hours,
                      sum(b.vacation_hours + b.pto_hours) as pto_hours,
                      sum(b.holiday_hours) as holiday_hours
                    from (
                        select c.team, a.employee_number, a.last_name, a.first_name,
                          a.ry1_id, a.ry2_id, a.fi_id, d.payplan, a.is_senior
                        from sls.personnel a
                        inner join sls.team_personnel c on a.employee_number = c.employee_number
                          and c.from_Date < (select first_day_of_next_month from sls.months
                            where open_closed = 'open')
                          and c.thru_date > (select last_day_of_previous_month from sls.months
                            where open_closed = 'open')
                        inner join sls.personnel_payplans d  on a.employee_number = d.employee_number
                          and d.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
                          and d.thru_date > (select last_day_of_previous_month from sls.months
                            where open_closed = 'open')) a
                    inner join sls.xfm_fact_clock_hours b on a.employee_number = b.employee_number
                    inner join dds.dim_date c on b.the_date = c.the_date
                    where c.year_month = (select year_month from sls.months where open_closed = 'open')
                      and b.clock_hours + b.vacation_hours + b.pto_hours + b.holiday_hours <> 0
                    group by c.year_month, a.employee_number;
--/> ---------------------------------------------------------------------------------------------------------------------------    

compare vision pto rates to ukg pto rates

select aa.employee_number, cc.year_month, dd.pto_rate, sum(pto_hours) as pto_hours, round(sum(bb.pto_hours * dd.pto_rate), 2), sum(hol_hours) as hol_hours, round(sum(bb.hol_hours * dd.pto_rate), 2)
from (
	select a.employee_number, a.last_name, a.first_name
	from sls.personnel a
	inner join sls.team_personnel c on a.employee_number = c.employee_number
		and c.from_Date < (select first_day_of_next_month from sls.months
			where open_closed = 'open')
		and c.thru_date > (select last_day_of_previous_month from sls.months
			where open_closed = 'open')
	inner join sls.personnel_payplans d  on a.employee_number = d.employee_number
		and d.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
		and d.thru_date > (select last_day_of_previous_month from sls.months
			where open_closed = 'open')) aa
join ukg.clock_hours bb on aa.employee_number = bb.employee_number
  and (bb.pto_hours <> 0 or bb.hol_hours <> 0)
join dds.dim_date cc on bb.the_date = cc.the_date
  and cc.year_month = (select year_month from sls.months where open_closed = 'open')
join sls.pto_intervals dd on aa.employee_number = dd.employee_number
          and cc.year_month = dd.year_month
group by aa.employee_number, cc.year_month, dd.pto_rate


-- sent to jeri
select aa.*, bb.pto_rate as vision_pto, cc.current_rate as ukg_pto, dd.current_rate as ukg_holiday
from (
	select 
	  case
	    when a.store_code = 'RY1' then 'GM'
	    when a.store_code = 'RY2' then 'HN'
	  end as store, a.employee_number, a.last_name, a.first_name
	from sls.personnel a
	inner join sls.team_personnel c on a.employee_number = c.employee_number
		and c.from_Date < (select first_day_of_next_month from sls.months
			where open_closed = 'open')
		and c.thru_date > (select last_day_of_previous_month from sls.months
			where open_closed = 'open')
	inner join sls.personnel_payplans d  on a.employee_number = d.employee_number
		and d.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
		and d.thru_date > (select last_day_of_previous_month from sls.months
			where open_closed = 'open')) aa
left join sls.pto_intervals bb on aa.employee_number = bb.employee_number
  and bb.year_month = 202201		
left join (
	select a.last_name, a.first_name, a.employee_number, c.current_rate
	from ukg.employees a
	join ukg.employee_profiles b on a.employee_id = b.employee_id
	join ukg.personal_rate_tables c on b.rate_table_id = c.rate_table_id
		and c.counter in ('paid time off')) cc on aa.employee_number = cc.employee_number  	
left join (
	select a.last_name, a.first_name, a.employee_number, c.current_rate
	from ukg.employees a
	join ukg.employee_profiles b on a.employee_id = b.employee_id
	join ukg.personal_rate_tables c on b.rate_table_id = c.rate_table_id
		and c.counter in ('holiday')) dd on aa.employee_number = dd.employee_number  			
 order by aa.last_name 


select * from ukg.employees
where last_name = 'callahan'

select * from ukg.employee_profiles where employee_id = 12988635287

select * 
from luigi.luigi_log
where pipeline = 'ukg'
and the_date = current_date
order by from_ts