﻿/*
jeri 2/22/22
I was able to recalculate the earnings and am good with that portion.

For the hours—the hours that are used for the time frame do not tie to the pay check dates.  
I believe that the hours included should correspond with the earnings that are paid on the check dates.  
For Marcus—I would use dates 8/2/20 – 7/31/21 as these correspond to the checks included.  
I got a total of 2220.27 clock hours (2068.27 reg+OT,  104 PTO, 48 holiday).  
Open to discussion on this.
*/

drop table if exists checks cascade;
create temp table checks as
select * 
from ( -- all check dates for an employee
	select a.employee_number, (check_month || '-' || check_day || '-' || (2000 + check_year))::date as check_date, 
		sum(b.base_pay + coalesce(c.amount, 0)) as earnings -- needed base pay for some techs sum(c.amount) as earnings&#13;
	from ukg_mig.pto_employees a
	join arkona.ext_pyhshdta b on a.employee_number = b.employee_
		and b.check_year in (20, 21)
		and b.total_gross_pay <> 0
	left join arkona.ext_pyhscdta c on b.company_number = c.company_number -- does not exists for everyone, so left join&#13;
		and b.payroll_run_number = c.payroll_run_number 
		and a.employee_number = c.employee_number
		and c.code_type = '1'
		and 
		-- flat rate:&#13;
			-- include commissions,covid,rate adjust, tech hours pay,tech xtra bonus,&#13;
			-- exclude holiday pay, health insurance,lease program, pay out pto,vac/pto pay&#13;
		-- sales:&#13;
			-- include A-Z Sales F&amp;I, COMMISSIONS,DRAWS','PULSE PAY','COVID 19','SPIFF PAY OUTS'&#13;
			-- exclude LEASE PROGRAM,PAY OUT PTO,'SALES VACATION'&#13;
			case 
				when a.pay_calc_profile = 'Full-Time Sales Commission Only' then c.code_id in ('79A','79','78','c19','79B','82')  
				when a.pay_calc_profile = 'Full-Time Flat Rate' then c.code_id in ('79','C19','cov','75','87','70')
			end
	where a.employee_number = '137101'    
	group by a.employee_number, (check_month || '-' || check_day || '-' || (2000 + check_year))::date) x
where check_date between '08/15/2020' and '08/14/2021'
order by check_date

select * from checks

-- this gives me marcus's dates
select a.employee_number, min(c.biweekly_pay_period_start_date), max(c.biweekly_pay_period_end_date)
from checks a
left join dds.dim_date b on a.check_date = b.the_date
left join dds.dim_date c on b.biweekly_pay_period_sequence = c.biweekly_pay_period_sequence + 1
group by  a.employee_number


-- start_over
-- what i need to determine hours for each employee is the check dates between most_recent_anniv - 1 year and most_recent_anniv - 1 day

select *
from (
	select * 
	from ukg_mig.pto_earnings
	where employee_number = '137101') a
order by employee_number, check_date

select * from ukg_mig.pto_calc_dates where employee_number = '137101'


-- this looks like it works
-- need to explicitly exclude those with less than a year
select distinct aa.employee_number, c.biweekly_pay_period_start_date  as from_date, e.biweekly_pay_period_end_date as thru_date
from (
	select a.employee_number, min(a.check_date) as min_check , max(a.check_date) as max_check
	from ukg_mig.pto_earnings a
	join ukg_mig.pto_employees aa on a.employee_number = aa.employee_number
	  and aa.pay_calc_profile = 'Full-Time flat Rate'
	join ukg_mig.pto_calc_dates b on a.employee_number = b.employee_number
		and a.check_date between b.rate_calc_from_date and b.rate_calc_thru_date
	group by a.employee_number) aa
left join dds.dim_date b on aa.min_check = b.the_date
left join dds.dim_date c on b.biweekly_pay_period_sequence = c.biweekly_pay_period_sequence + 1	
left join dds.dim_date d on aa.max_check = d.the_date
left join dds.dim_date e on d.biweekly_pay_period_sequence = e.biweekly_pay_period_sequence + 1	



select * 
from ukg_mig.pto_employees x
left join (
select distinct aa.employee_number, c.biweekly_pay_period_start_date  as from_date, e.biweekly_pay_period_end_date as thru_date
from (
	select a.employee_number, min(a.check_date) as min_check , max(a.check_date) as max_check
	from ukg_mig.pto_earnings a
	join ukg_mig.pto_employees aa on a.employee_number = aa.employee_number
	  and aa.pay_calc_profile = 'Full-Time flat Rate'
	join ukg_mig.pto_calc_dates b on a.employee_number = b.employee_number
	join ukg_mig.pto_calc_dates b on a.employee_number = b.employee_number
		and a.check_date between b.rate_calc_from_date and b.rate_calc_thru_date
	group by a.employee_number) aa
left join dds.dim_date b on aa.min_check = b.the_date
left join dds.dim_date c on b.biweekly_pay_period_sequence = c.biweekly_pay_period_sequence + 1	
left join dds.dim_date d on aa.max_check = d.the_date
left join dds.dim_date e on d.biweekly_pay_period_sequence = e.biweekly_pay_period_sequence + 1) y on x.employee_number = y.employee_number


-- got myself in a loop bind with calc_from & thru dates
-- need to generate the time frame based on most recent anniv
select a.last_name, a.first_name, a.most_recent_anniv, (a.most_recent_anniv - interval '1 year')::date as rate_calc_from_date, most_recent_anniv - 1 as rate_calc_thru_date 
from ukg_mig.pto_employees a

	select a.employee_number, min(a.check_date) as min_check , max(a.check_date) as max_check
	from ukg_mig.pto_earnings a
	join ukg_mig.pto_employees aa on a.employee_number = aa.employee_number
	  and aa.pay_calc_profile = 'Full-Time flat Rate'
	join ukg_mig.pto_calc_dates b on a.employee_number = b.employee_number
	join ukg_mig.pto_calc_dates b on a.employee_number = b.employee_number
		and a.check_date between b.rate_calc_from_date and b.rate_calc_thru_date
	group by a.employee_number

-- going back a year from the most recent anniversary, any checks issued in that date range
-- then, extract the pay period dates, minimum start date & maximum end dates for the entire
-- series of pay periods, and that date range is the one to use for hours
-- the final answer
select distinct aa.employee_number, c.biweekly_pay_period_start_date  as from_date, e.biweekly_pay_period_end_date as thru_date
from (
	select a.employee_number, min(check_date) as min_check, max(check_date) as max_check
	from (-- going back a year from the most recent anniversary, any checks issued in that date range
		select last_name, first_name, employee_number, 
			(most_recent_anniv - interval '1 year')::date as from_date, most_recent_anniv - 1 as thru_date 
		from ukg_mig.pto_employees
		where pay_calc_profile = 'Full-Time flat Rate')  a
	left join  ukg_mig.pto_base_earnings b on a.employee_number = b.employee_number
		and b.check_date between a.from_date and a.thru_date
	group by a.employee_number) aa
left join dds.dim_date b on aa.min_check = b.the_date
left join dds.dim_date c on b.biweekly_pay_period_sequence = c.biweekly_pay_period_sequence + 1	
left join dds.dim_date d on aa.max_check = d.the_date
left join dds.dim_date e on d.biweekly_pay_period_sequence = e.biweekly_pay_period_sequence + 1	
order by employee_number

-- yep, this is it
select sum(clock_hours), sum(pto_hours + vac_hours), sum(hol_hours)
from arkona.xfm_pypclockin
where employee_number = '137101'
  and the_date between '08/02/2020' and '07/31/2021'