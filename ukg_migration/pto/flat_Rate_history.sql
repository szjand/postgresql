﻿select case when department_key = 13 then 'body shop' else 'main shop' end as dept, first_name, last_name, employee_number, round(tech_tfr_rate, 2) as flat_rate, min(the_date) as from_date, max(the_date) as thru_date
from tp.data 
where the_date between '03/01/2021' and '02/28/2022'
group by department_key, first_name, last_name, employee_number, round(tech_tfr_rate, 2)

union
select 'honda', b.first_name, b.last_name, a.employee_number, flat_rate, from_date, thru_date 
from hs.main_shop_flat_rate_techs a
join ukg.employees b on a.employee_number = b.employee_number
  and b.status = 'active'
-- where from_date >= '03/01/2021'
group by b.first_name, b.last_name, a.employee_number, flat_rate, from_date, thru_date 
order by dept, last_name, from_date