﻿
/*
ukg_mig.pto_employees
ukg_mig.pto_calc_dates
ukg_mig.pto_earnings
ukg_mig.pto_clock_hours
*/
/*
jeri 2/22/22
I was able to recalculate the earnings and am good with that portion.

For the hours—the hours that are used for the time frame do not tie to the pay check dates.  
I believe that the hours included should correspond with the earnings that are paid on the check dates.  
For Marcus—I would use dates 8/2/20 – 7/31/21 as these correspond to the checks included.  
I got a total of 2220.27 clock hours (2068.27 reg+OT,  104 PTO, 48 holiday).  
Open to discussion on this.

jeri 2/22/22
I would like to talk through sales a bit.  I feel that in order to get the best data to base the PTO rate on, 
we should be using full months of payroll info, just due to how the draws work.  So for example, 
Dave Vanyo’s anniversary date is 12/18.  Rather than using the check from 12/15 that is just his draw, 
I feel that it would be more accurate to use the previous full month.  In this case we would go 
from 12/1/20 to 11/30/21.   The check dates are a bit of an issue between DT and UKG as we 
changed the process/timing of checks partway through last year.  Now, the month-end checks 
will have a date in the following month, so that check would be dated 12/1/21 or so.  I would 
like the hours to apply the same way.  We might need to actually talk over this one on a call.  
I’m around this afternoon if you have some time.
*/
-- include current pto_rate, seniority date
drop table if exists ukg_mig.pto_employees cascade;
create table ukg_mig.pto_employees as
select a.last_name, a.first_name, a.employee_number, a.employee_id, c.display_name as pay_calc_profile, 
	(d.store||'/'|| d.department||'/'|| d.cost_center)::citext as cost_center, coalesce(a.seniority_date, a.hire_date) as seniority_date,
	e.current_rate as current_pto_rate
from ukg.employees a
join ukg.employee_profiles b on a.employee_id = b.employee_id
join ukg.ext_pay_calc_profiles c on b.pay_calc_id = c.id
  and c.display_name in ('Full-Time Flat Rate','Full-Time Sales Commission Only')
join (select * from ukg.get_cost_center_hierarchy()) d on a.cost_center = d.cost_center  
  and 
    case
      when a.store = 'GM' then d.store = 'Rydell GM'
      when a.store = 'HN' then d.store = 'Rydell Honda Nissan'
    end
left join ukg.personal_rate_tables e on b.rate_table_id = e.rate_table_id
  and e.counter = 'paid time off'
where a.status = 'active'
order by d.store, d.department, c.display_name, d.cost_center, a.last_name;


drop table if exists ukg_mig.pto_calc_dates cascade;
create table ukg_mig.pto_calc_dates as
select *, (a.most_recent_anniv - interval '1 year')::date as rate_calc_from_date, most_recent_anniv - 1 as rate_calc_thru_date
from (
	select employee_number, 
		case
			when -- current year anniversary
				to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' ) < current_date 
					then to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' )
			else (to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' ) - interval '1 year')::date
		end as most_recent_anniv
	from ukg_mig.pto_employees) a;

select a.last_name, a.first_name, b.most_recent_anniv
from ukg_mig.pto_employees a
join ukg_mig.pto_calc_dates b on a.employee_number = b.employee_number
where seniority_date <> most_Recent_anniv
order by a.last_name, a.first_name
	

drop table if exists ukg_mig.pto_earnings cascade;
create table ukg_mig.pto_earnings as
-- dealer track 2021 earnings
select a.employee_number, (check_month || '-' || check_day || '-' || (2000 + check_year))::date as check_date, 
  sum(b.base_pay + coalesce(c.amount, 0)) as earnings -- needed base pay for some techs sum(c.amount) as earnings
from ukg_mig.pto_employees  a
join arkona.ext_pyhshdta b on a.employee_number = b.employee_
  and b.check_year in (20, 21)
  and b.total_gross_pay <> 0
left join arkona.ext_pyhscdta c on b.company_number = c.company_number -- does not exists for everyone, so left join
  and b.payroll_run_number = c.payroll_run_number 
  and a.employee_number = c.employee_number
  and c.code_type = '1'
  and 
  -- flat rate:
    -- include commissions,covid,rate adjust, tech hours pay,tech xtra bonus,
		-- exclude holiday pay, health insurance,lease program, pay out pto,vac/pto pay
  -- sales:
		-- include A-Z Sales F&I, COMMISSIONS,DRAWS','PULSE PAY','COVID 19','SPIFF PAY OUTS'
		-- exclude LEASE PROGRAM,PAY OUT PTO,'SALES VACATION'
    case 
      when a.pay_calc_profile = 'Full-Time Sales Commission Only' then c.code_id in ('79A','79','78','c19','79B','82')  
      when a.pay_calc_profile = 'Full-Time Flat Rate' then c.code_id in ('79','C19','cov','75','87','70')
    end
group by a.employee_number, (check_month || '-' || check_day || '-' || (2000 + check_year))::date
union all
-- ukg earnings
select a.employee_number, c.pay_date, sum(b.ee_amount) as earnings
from ukg_mig.pto_employees  a
join ukg.pay_statement_earnings b on a.employee_id = b.employee_account_id
  and coalesce(b.ee_amount, 0) <> 0
join ukg.payrolls c on b.payroll_id = c.payroll_id
-- where b.earning_code <> b.earning_name
where b.earning_name not in (
		'Benefit Allowance','Deferred Comp','Employee Referral Bonus','Expense Reimbursement','Gift Card',
		'Holiday','Lease Program', 'Lease Program 2', 'Lease Program 3', 'Lease Program 4', 'Lease Program 5',
		'Moving Expenses','Non-Cash Wages','OnStar Commission','Other Discretionary Bonus',
		'Owner Health Insurance Premium','Paid Time Off','PTO Payout','Severance','Sign-On Bonus',
		'Tool Allowance','Vision Award','Volunteer PTO','Year End Manager Bonus','Year End Owner Bonus',
		'2021 401K Hours')  -- just the 2 anomalies with amounts
and b.earning_code not in (
		'Benefit Allowance','Deferred Comp','Employee Referral Bonus','Expense Reimbursement','Gift Card',
		'Holiday','Lease Program', 'Lease Program 2', 'Lease Program 3', 'Lease Program 4', 'Lease Program 5',
		'Moving Expenses','Non-Cash Wages','OnStar Commission','Other Discretionary Bonus',
		'Owner Health Insurance Premium','Paid Time Off','PTO Payout','Severance','Sign-On Bonus',
		'Tool Allowance','Vision Award','Volunteer PTO','Year End Manager Bonus','Year End Owner Bonus')	
group by a.employee_number, c.pay_date;


select * from ukg.pay_statement_earnings where earning_code in ('Owner Health Insurance Premium','Owner Health Insurance Premium Paid')

drop table if exists ukg_mig.pto_clock_hours;
create table ukg_mig.pto_clock_hours as
select a.employee_number, a.the_date, sum(a.clock_hours) as clock_hours, sum(a.pto_hours) as pto_hours, sum(a.hol_hours) as hol_hours
from ukg.clock_hours a
join ukg_mig.pto_employees b on a.employee_number = b.employee_number
where a.the_date > '12/18/2021'
group by a.employee_number, a.the_date
union
select a.employee_number, a.the_date, sum(a.clock_hours) as clock_hours, sum(a.vac_hours + a.pto_hours) as pto_hours, sum(a.hol_hours) as hol_hours 
from arkona.xfm_pypclockin a
join ukg_mig.pto_employees b on a.employee_number = b.employee_number
where a.the_date between '01/01/2020' and '12/18/2021'
group by a.employee_number, a.the_date;

-- current rate from personal rate tables
drop table if exists ukg_mig.pto_output_1 cascade;
create table ukg_mig.pto_output_1 as
select e.*, 
  round (
			case
			when total_hours = 0 then 0
			else earnings/total_hours
		end, 2) calc_pto_rate,
		case
		  when total_hours = 0 then 0
		  else
				round (
						case
						when total_hours = 0 then 0
						else earnings/total_hours
					end, 2) - current_pto_rate
		end as calc_rate_minus_cur_rate
from (
	select a.* , b.most_recent_anniv, b.rate_calc_from_date, b.rate_calc_thru_date, c.earnings,
		d.clock_hours, d.pto_hours, d.hol_hours,
		case
			when a.seniority_date = b.most_recent_anniv then 0
			when a.pay_calc_profile = 'Full-Time Sales Commission Only' then 2080 - coalesce(pto_hours, 0) - coalesce(hol_hours, 0) 
			when a.pay_calc_profile = 'Full-Time Flat Rate' then clock_hours - coalesce(pto_hours, 0) - coalesce(hol_hours, 0)  
		end as total_hours
	from ukg_mig.pto_employees a
	left join ukg_mig.pto_calc_dates b on a.employee_number = b.employee_number
	left join (
		select a.employee_number, sum(c.earnings) as earnings
		from ukg_mig.pto_employees a
		left join ukg_mig.pto_calc_dates b on a.employee_number = b.employee_number
		left join ukg_mig.pto_earnings c on a.employee_number = c.employee_number
			and c.check_date between b.rate_calc_from_date and b.rate_calc_thru_date
		group by a.employee_number) c on a.employee_number = c.employee_number
	left join (
		select a.employee_number, sum(c.clock_hours) as clock_hours, sum(c.pto_hours) as pto_hours, sum(c.hol_hours) as hol_hours
		from ukg_mig.pto_employees a
		left join ukg_mig.pto_calc_dates b on a.employee_number = b.employee_number
		left join ukg_mig.pto_clock_hours c on a.employee_number = c.employee_number
			and c.the_date between b.rate_calc_from_date and b.rate_calc_thru_date
		group by a.employee_number) d on a.employee_number = d.employee_number) e
order by cost_center, pay_calc_profile, last_name


-- rate_calc_from_dates agree with accrual start dates
select a.* , 
  case
    when cost_center like '%detail%' then 'unknown'
    else coalesce(b.cur_rate_eff_date, c.cur_rate_eff_date, d.cur_rate_eff_date)::text
  end as cur_rate_eff_date
--   e.start_date, e.end_date
from ukg_mig.pto_outptut_1 a
left join  ukg_mig.hn_tech_pto b on a.employee_number = b.employee_number
left join ukg_mig.gm_tech_pto c on a.employee_number = c.employee_number
left join  ukg_mig.sc d on a.employee_number = d.employee_number
-- left join ukg.ext_employee_accrual_balances e on a.employee_id = e.employee_id
-- where seniority_date <> most_recent_anniv -- excludes no earned pto
-- and a.current_pto_rate is null
order by most_recent_anniv
order by cost_center, pay_calc_profile, last_name


-- pto/hol hours during calc period

select * from ukg_mig.pto_output_1

select a.last_name, a.first_name, a.employee_number, b.rate_calc_from_date, b.rate_calc_thru_date,
  sum(c.pto_hours) as pto_hours, sum(c.hol_hours) as hol_hours
from ukg_mig.pto_employees a
left join ukg_mig.pto_calc_dates b on a.employee_number = b.employee_number
left join ukg_mig.pto_clock_hours c on a.employee_number = c.employee_number
  and c.the_date between b.rate_calc_from_date and b.rate_calc_thru_date
where a.seniority_date <> b.most_recent_anniv
group by a.last_name, a.first_name, a.employee_number, cost_center, b.rate_calc_from_date, b.rate_calc_thru_date
order by a.cost_center

select sum(pto_hours)
from ukg_mig.clock_hours
where employee_number = '164015' 
  and the-date bes

issues
	no paid time off personal rate table entry
		Thomas Berg 195359  10/08/2019  no holiday
		Charley Jacob  135795  01/11/2021  holiday = 17.45
		Daniel Gonzalez  159857  12/17/2019  holiday = 14.79
		
  Tyler Grollimund, 22.23 since 8/1/2019
  Tom Aubol 69.75 since 10/31/2020

  

select * from hs.main_shop_flat_rate_techs where employee_number = '254327'

select * from arkona.xfm_pymast where pymast_employee_number = '153865' order by row_from_date

select * from sls.personnel where employee_number = '254327'

select * from sls.pto_intervals where employee_number = '17534' order by from_date