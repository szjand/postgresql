﻿
do $$
declare
-- this can actually run okay on each sunday, if clause does nothing if the date is not the first of a pay period
	_first_of_pp date := '03/26/2023'; -- (
-- 	  select distinct biweekly_pay_period_start_date
-- 	  from dds.dim_date
-- 	  where biweekly_pay_period_start_date = current_date); 
	_last_of_pp date := (
	  select biweekly_pay_period_end_date
	  from dds.dim_date 
	  where the_date = _first_of_pp);
	_pay_period_seq integer := (
	  select biweekly_pay_period_sequence
	  from dds.dim_date
	  where the_date = _first_of_pp);
begin
	-- called every Sunday, but only runs on those sundays that are the start of a biweekly pay period
	if _first_of_pp is not null then 
	-- this table is a non persistent set of employees upon which to do the pto rate calculations on anniversaries
		delete 
		from pto.flat_rate_employees;
		insert into pto.flat_rate_employees(last_name,first_name,employee_number,employee_id,pay_calc_profile,
		  store,department,cost_center,manager_1, manager_2,
			seniority_date,current_pto_rate,most_recent_anniv)
		select * 
		from (
			select a.last_name, a.first_name, a.employee_number, a.employee_id, c.display_name as pay_calc_profile, 
				d.store,  d.department, d.cost_center, a.manager_1, a.manager_2, coalesce(a.seniority_date, a.hire_date) as seniority_date,
--         e.current_rate as current_pto_rate,
				coalesce(e.current_rate, 0) as current_pto_rate, --------------------------------------------------------------
				case 
					when -- current year anniversary
						to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' ) < _first_of_pp 
							then to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' )
					else (to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' ) - interval '1 year')::date
				end as most_recent_anniv	
			from ukg.employees a
			join ukg.employee_profiles b on a.employee_id = b.employee_id
			join ukg.ext_pay_calc_profiles c on b.pay_calc_id = c.id
				and c.display_name = 'Full-Time Flat Rate'
			join (select * from ukg.get_cost_center_hierarchy()) d on a.cost_center = d.cost_center  
				and 
					case
						when a.store = 'GM' then d.store = 'Rydell GM'
						when a.store = 'HN' then d.store = 'Rydell Honda Nissan'
						when a.store = 'Toyota' then d.store = 'Rydell Toyota'
					end
			left join ukg.personal_rate_tables e on b.rate_table_id = e.rate_table_id
				and e.counter = 'paid time off'
			where a.status = 'active') s
-- 		where seniority_date <> most_recent_anniv; -- excludes employees with less than a year employment
		where 
			case
				when seniority_date = most_Recent_anniv then current_Date - seniority_date > 330
				else seniority_date <> most_Recent_anniv
			end;

/*
03/28/2023
from kim
Jeri I do not think the PTO rate on the vision page is working there has been no one on the reports for the last several payrolls.  
Have not had any this year for Flat rate
Maybe it is just system odd that we would have no techs with anniversary’s Jan-March

1. 
pto.flat_rate_employees
ERROR:  null value in column "current_pto_rate" violates not-null constraint
Brannin, Trhea, 185930, 12990434111
nothing in ukg.personal_rate_tables except base compensation
seniority date 12/13/21
************* coalesce current_pto_rate in select to 0

2.
pto.flat_rate_employee_dates
goodien should show in pp 1/1/23 - 1/14/23 but does not
eliminate the join to arkona.ext_pyhshdta: pto.flat_rate_earnings

3. don't know if they actually create a problem but, i can now remove the union queries on arkona tables
pto.flat_rate_clock_hours & pto.flat_rate_earnings
*/


-- -- these are the employees with anniv in the pay period
-- 		select *, (most_recent_anniv + interval '1 year')::date, (most_recent_anniv + interval '1 year')::date - 1
-- 		from pto.flat_rate_employees
-- 		where pay_calc_profile = 'Full-Time Flat Rate'
-- 		  and (most_recent_anniv + interval '1 year')::date between '03/13/2022' and '03/26/2022'; -- _first_of_pp and _last_of_pp
-- 		  
-- check dates
-- first ukg check: check date: 1/7/22, pay period: 12/19/21 -> 1/1/22
-- last dt check: check_date 12/24/21, pay period 12/5 -> 12/18/21


-- Looking at the flat rate techs that have an anniversary in the pay period 03/13 -03/26/22
-- There are 2, Brian Peterson and Paul Sobolik.
-- Brian: 
-- 	Anniversary: 3/15/22
-- 	For the period of 3/15/21 thru 3/14/22, the first check date is 3/19/21, the last check date is 3/4/22, equaling 25 pay checks for earnings
-- 	The pay periods covered by those check dates are 2/28/21 thru 2/26/22, a total of 364 days for clock hours
-- Paul:
-- 	Anniversary: 3/23/22
-- 	For the period of 3/23/21 thru 3/22/22, the first check date is 4/2/21, the last check date is 3/4/22, equaling 24 pay checks for earnings
-- 	The pay periods covered by those check date are 3/14/221 thru 2/26/22, a total of 350 days for clock hours
-- 
-- Initially I was taken aback at the difference in checks and days, but it actually equals out.

		delete
		from pto.flat_rate_employee_dates
		where pay_period_seq = _pay_period_seq;
		
		insert into pto.flat_rate_employee_dates(pay_period_seq,pay_period_start_date,pay_period_end_date,employee_number,
			last_anniv,next_anniv,min_check_date,max_check_date,clock_from_date,clock_thru_date)
		select distinct _pay_period_seq, _first_of_pp, _last_of_pp, aa.employee_number, aa.most_recent_anniv, aa.next_anniv, aa.min_check_date, 
			aa.max_check_date,c.biweekly_pay_period_start_date clock_from_date, e.biweekly_pay_period_end_date as clock_thru_date
		from (
			select a.employee_number, a.most_recent_anniv, a.next_anniv, 
-- 			  case  -- the first biweekly check in ukg had a pay date of 1/7/22, anything before that requires dealertrack payroll data
-- 			    when a.most_recent_anniv < '01/07/2022' then min((b.check_month || '-' || b.check_day || '-' || (2000 + b. check_year))::date) 
-- 			    else min(c.pay_date)
-- 			  end as min_check_date,
			   min(c.pay_date) as min_check_date, ----------------------------------------
				max(c.pay_date) as max_check_date
			from (
				select *, (most_recent_anniv + interval '1 year')::date as next_anniv 
				from pto.flat_rate_employees
				where pay_calc_profile = 'Full-Time Flat Rate'
					and (most_recent_anniv + interval '1 year')::date between _first_of_pp and _last_of_pp) a 	
-- 			join arkona.ext_pyhshdta b on a.employee_number = b.employee_
-- 				and (b.check_month || '-' || b.check_day || '-' || (2000 + b. check_year))::date between a.most_recent_anniv and a.next_anniv - 1
-- 				and b.total_gross_pay <> 0
			join ukg.payrolls c on true	
				and c.payroll_name like '%bi-weekly%'
				and c.pay_date between a.most_recent_anniv and a.next_anniv - 1
			where a.pay_calc_profile = 'Full-Time flat Rate'
			group by a.employee_number, a.most_recent_anniv, a.next_anniv) aa	
		left join dds.dim_date b on aa.min_check_date = b.the_date
		-- the pay period previous to the one that includes the min_check_date
		left join dds.dim_date c on b.biweekly_pay_period_sequence = c.biweekly_pay_period_sequence + 1	
		left join dds.dim_date d on aa.max_check_date = d.the_date
		-- the pay period previous to the one that includes the max check date
		left join dds.dim_date e on d.biweekly_pay_period_sequence = e.biweekly_pay_period_sequence + 1;

  -- flat rate:
    -- include commissions,covid,rate adjust, tech hours pay,tech xtra bonus,
		-- exclude holiday pay, health insurance,lease program, pay out pto,vac/pto pay
  -- sales:
		-- include A-Z Sales F&I, COMMISSIONS,DRAWS','PULSE PAY','COVID 19','SPIFF PAY OUTS'
		-- exclude LEASE PROGRAM,PAY OUT PTO,'SALES VACATION'


-- need 2 versions somehow
-- only need dealertrack data if the min_check_date is prior to 01/07/2022
-- rather than hassle with the logic now, when we get to that point in time, the lack of data
-- from dealertrack will not have any effect on this query, uinion ukg earnings with null = ukg earnings

		delete
		from pto.flat_rate_earnings
		where pay_period_seq = _pay_period_seq;

		insert into pto.flat_rate_earnings(pay_period_seq,employee_number, earnings_code,earnings)
		select pay_period_seq, employee_number, earnings_code, sum(earnings) as earnings
		from (
-- 			select b.pay_period_seq, a.employee_number, 
-- 				(c.check_month || '-' || c.check_day || '-' || (2000 + c. check_year))::date as check_date,
-- 				d.code_id || ' ' || d.description as earnings_code, coalesce(c.base_pay, 0) + coalesce(d.amount, 0) as earnings
-- 			from pto.flat_rate_employees a
-- 			join pto.flat_rate_employee_dates b on a.employee_number = b.employee_number
-- 			  and b.pay_period_seq = _pay_period_seq
-- 			join arkona.ext_pyhshdta c on a.employee_number = c.employee_
-- 				and (c.check_month || '-' || c.check_day || '-' || (2000 + c. check_year))::date between b.min_check_date and b.max_check_date
-- 				and c.total_gross_pay <> 0
-- 			left join arkona.ext_pyhscdta d on c.company_number = d.company_number -- does not exists for everyone, so left join
-- 				and c.payroll_run_number = d.payroll_run_number 
-- 				and a.employee_number = d.employee_number
-- 				and d.code_type = '1'
-- 				and d.code_id in ('79','C19','cov','75','87','70')	
-- 			union all
			select aa.pay_period_seq,a.employee_number, c.pay_date, d.earnings_code, b.ee_amount as earnings
			from pto.flat_rate_employees a
			join pto.flat_rate_employee_dates aa on a.employee_number = aa.employee_number
			  and aa.pay_period_seq = _pay_period_seq
			join ukg.pay_statement_earnings b on a.employee_id = b.employee_account_id
				and coalesce(b.ee_amount, 0) <> 0
			join ukg.payrolls c on b.payroll_id = c.payroll_id
				and c.pay_date between aa.min_check_date and aa.max_check_date
			join pto.earnings_codes d on b.earning_code	= d.earnings_code
				and d.include_in_pto_rate_calc) e
		group by pay_period_seq, employee_number, earnings_code
		having sum(earnings) > 0;  -- added 05/09/22 due to weird check on 12/17 for patrick adam (11660)

		delete
		from pto.flat_rate_clock_hours
		where pay_period_seq = _pay_period_seq;

		insert into pto.flat_rate_clock_hours(pay_period_seq,employee_number,clock_hours,pto_hours,hol_hours)
		select _pay_period_seq, employee_number, sum(clock_hours) as clock_hours, sum(pto_hours) as pto_hours, sum(hol_hours) as hol_hours
		from (
			select a.employee_number, c.the_date, c.clock_hours, c.pto_hours, c.hol_hours
			from pto.flat_rate_employees a
			join pto.flat_rate_employee_dates b on a.employee_number = b.employee_number
				and b.pay_period_seq = _pay_period_seq
			join ukg.clock_hours c on a.employee_number = c.employee_number
				and c.the_date between b.clock_from_date and b.clock_thru_date
			where c.the_date > '12/18/2021') d  
-- 			union
-- 			select a.employee_number, c.the_date, c.clock_hours, c.vac_hours + c.pto_hours as pto_hours, c.hol_hours
-- 			from pto.flat_rate_employees a
-- 			join pto.flat_rate_employee_dates b on a.employee_number = b.employee_number
-- 				and b.pay_period_seq = _pay_period_seq
-- 			join arkona.xfm_pypclockin c on a.employee_number = c.employee_number
-- 				and c.the_date between b.clock_from_date and b.clock_thru_date
-- 			where c.the_date < '12/19/2021') d
		group by employee_number;

		delete
		from pto.flat_rate_pto_data_summary
		where pay_period_seq = _pay_period_seq;

		insert into pto.flat_rate_pto_data_summary
		select _pay_period_seq, _first_of_pp, _last_of_pp, a.last_name, a.first_name, a.employee_number, a.store, a.department, a.cost_center, 
			a.manager_1, a.manager_2, a.seniority_date, b.next_anniv, d.earnings, c.clock_hours, a.current_pto_rate, 
			round(earnings/clock_hours, 2) as new_pto_rate,
			case
				when round(d.earnings/c.clock_hours, 2) > a.current_pto_rate then round(d.earnings/c.clock_hours, 2)
				else a.current_pto_rate
			end as effective_pto_rate,
			case
				when round(d.earnings/c.clock_hours, 2) < a.current_pto_rate then 0
				else round(d.earnings/c.clock_hours, 2) - a.current_pto_rate
			end as diff
		-- select *
		from pto.flat_rate_employees a
		join pto.flat_rate_employee_dates b on a.employee_number = b.employee_number
			and b.pay_period_seq = _pay_period_seq
		join pto.flat_rate_clock_hours c on a.employee_number = c.employee_number
			and b.pay_period_seq = c.pay_period_seq
		join (
			select pay_period_seq, employee_number, sum(earnings) as earnings
			from pto.flat_rate_earnings
			where pay_period_seq = _pay_period_seq
			group by pay_period_seq, employee_number) d on a.employee_number = d.employee_number
			and b.pay_period_seq = d.pay_period_seq;		
	end if;		  
end $$;

-- select * from pto.flat_rate_pto_data_summary order by last_name;
-- 
-- select * from pto.flat_rate_pto_data_summary order by next_anniv;

select * from pto.flat_rate_pto_data_summary order by extract(month from seniority_date), extract(day from seniority_date)
