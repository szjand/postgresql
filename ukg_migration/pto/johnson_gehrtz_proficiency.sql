﻿select * from (
select aa.*, round(100 * flag_hours/4/clock_hours, 0) from (
    select a.biweekly_pay_period_sequence, sum(b.flaghours) as flag_hours, min(a.biweekly_pay_period_start_date), max(a.biweekly_pay_period_end_date)
    from dds.dim_date a
    inner join ads.ext_fact_repair_order b on a.date_key = b.flagdatekey
    inner join ads.ext_dim_tech c on b.techkey = c.techkey
    inner join hs.main_shop_flat_rate_techs d on c.employeenumber = d.employee_number
      and d.employee_number = '273315'
    where a.the_date between '02/14/2021' and '02/12/2022'
    group by a.biweekly_pay_period_sequence) aa
left join (
select b.biweekly_pay_period_sequence, sum(clock_hours) as clock_hours,  min(b.biweekly_pay_period_start_date), max(b.biweekly_pay_period_end_date)
from arkona.xfm_pypclockin a
join dds.dim_date b on a.the_date = b.the_date
  and b.the_date between '02/14/2021' and '02/12/2022'
where a.employee_number = '273315' 
group by b.biweekly_pay_period_sequence
union
select b.biweekly_pay_period_sequence, sum(clock_hours),  min(b.biweekly_pay_period_start_date), max(b.biweekly_pay_period_end_date)
from ukg.clock_hours a
join dds.dim_date b on a.the_date = b.the_date
--   and b.the_date between '02/14/2021' and '02/12/2022'
  and b.biweekly_pay_period_sequence > 340
where a.employee_number = '273315' 
group by b.biweekly_pay_period_sequence
) bb on aa.biweekly_pay_period_sequence = bb.biweekly_pay_period_sequence) aaa
left join (
select pay_period_seq,team_prof_pptd::integer
from tp.data a
join dds.dim_date b on a.pay_period_seq = b.biweekly_pay_period_sequence
  and b.the_date = biweekly_pay_period_end_date
where last_name = 'gehrtz'
  and a.pay_period_seq between 318 and 343
  and a.the_date = b.biweekly_pay_period_end_date) bbb on aaa.biweekly_pay_period_sequence = bbb.pay_period_seq