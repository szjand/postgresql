﻿-- TODO::
--		gonazlez issue maybe use sls.personnel first_full_month
--    tables for history
comment on schema pto is 'for employees with a pay calc profile of Full-Time Flat Rate or Full-Time Sales Commission Only, they have
their pto rates recaculated annualy based on earnings for the previous year, this schema supports that process. If a flat rate employee''s
pto rate decreases, the rate does not change';

DROP TABLE if exists pto.flat_rate_employees cascade;
CREATE TABLE pto.flat_rate_employees
(
  last_name citext not null,
  first_name citext not null,
  employee_number citext NOT NULL,
  employee_id bigint not null,
  pay_calc_profile citext not null,
  store citext not null,
  department citext not null,
  cost_center citext not null,
  manager_1 citext,
  manager_2 citext,
  seniority_date date not null,
  current_pto_rate numeric not null,
  most_recent_anniv date not null,
  CONSTRAINT employees_pkey PRIMARY KEY (employee_number));
comment on table pto.flat_rate_employees is 'a working set of currently employeed flat rate employees (their pay calc profile is Full-Time Flat Rate)
that may be eligible for pto rate recalculation based upon the previous years earnings and clock hours.  
This is just a working table, no need to persist any of the data beyond the function run.  Data is generated on the first day of the biweekly pay period';

DROP TABLE if exists pto.sales_employees cascade;
CREATE TABLE pto.sales_employees
(
  last_name citext not null,
  first_name citext not null,
  employee_number citext NOT NULL,
  employee_id bigint not null,
  pay_calc_profile citext not null,
  store citext not null,
  department citext not null,
  cost_center citext not null,
  manager_1 citext,
  manager_2 citext,
  seniority_date date not null,
  current_pto_rate numeric not null,
  most_recent_anniv date not null,
  PRIMARY KEY (employee_number));
comment on table pto.sales_employees is 'a working set of currently employeed sales employees (their pay calc profile is Full-Time Sales Commission Only)
that may be eligible for pto rate recalculation based upon the previous years earnings and clock hours.  
This is just a working table, no need to persist any of the data beyond the function run.  Data is generated on the first day of the month';
  
drop table if exists pto.flat_rate_employee_dates cascade;
create table pto.flat_rate_employee_dates (
  pay_period_seq integer not null,
  pay_period_start_date date not null,
  pay_period_end_date date not null,
  employee_number citext not null,
  last_anniv date not null,
  next_anniv date not null, 
  min_check_date date not null,
  max_check_date date not null,
  clock_from_date date not null,
  clock_thru_date date not null,
  primary key (employee_number,pay_period_seq));
comment on table pto.flat_rate_employee_dates is 'for a given biweekly pay period, these are the employees whose anniversary fall
within the pay period, and the dates needed to calculate their new pto rate';

drop table if exists pto.sales_employee_dates cascade;
create table pto.sales_employee_dates (
  year_month integer not null,
  pay_period_start date not null,
  pay_period_end date not null,
  employee_number citext not null,
  last_anniv date not null,
  next_anniv date not null,
  prev_year_from_date date not null,
  prev_year_thru_date date not null,
  primary key (year_month,employee_number));
comment on table pto.sales_employee_dates is 'for a given month, these are the employees whose anniversary fall
within the pay period, and the dates needed to calculate their new pto rate';
  
drop table if exists pto.flat_rate_earnings cascade;
create table pto.flat_rate_earnings (
  pay_period_seq integer not null,
  employee_number citext not null,
  earnings_code citext not null,
  earnings numeric(8,2) not null,
  primary key(pay_period_seq,employee_number,earnings_code));
comment on table  pto.flat_rate_earnings is 'the earnings for an employee due for a pto rate adjustment, earnings are based on
check pay dates for the year preceding the employee''s new anniversary. Until January 2023, the earnings include
data from dealertrack payroll, the first UKG check date was January 7, 2022';

drop table if exists pto.sales_earnings cascade;
create table pto.sales_earnings (
  year_month integer not null,
  employee_number citext not null,
  earnings_code citext not null,
  earnings numeric(8,2) not null,
  primary key(year_month,employee_number,earnings_code));
comment on table  pto.sales_earnings is 'the earnings for an employee due for a pto rate adjustment, earnings are based on
check pay dates for the year preceding the employee''s new anniversary. Until January 2023, the earnings include
data from dealertrack payroll, the first UKG check date was January 7, 2022';
  
drop table if exists pto.flat_rate_clock_hours cascade;
create table pto.flat_rate_clock_hours (
  pay_period_seq integer not null,
  employee_number citext not null,
  clock_hours numeric(8,2) not null,
  pto_hours numeric(8,2) not null,
  hol_hours numeric(8,2) not null,
  primary key(pay_period_seq,employee_number));
comment on table  pto.flat_rate_clock_hours is 'the clock hours for an employee due for a pto rate adjustment, clock hours
  are based on the year preceding the employee''s new anniversary, the pay periods covered by the checks issued
  in that time frame. Prior to 12/19/21 clock hours come from dealertrack, after 12/18/2021 clock hours come from UKG';

drop table if exists pto.sales_clock_hours cascade;
create table pto.sales_clock_hours(
  year_month integer not null,
  employee_number citext not null,
  clock_hours numeric(8,2) not null,
  pto_hours numeric(8,2) not null,
  hol_hours numeric(8,2) not null,
  primary key(year_month,employee_number));
comment on table  pto.sales_clock_hours is 'the clock hours for an employee due for a pto rate adjustment, clock hours
  are based on the year preceding the employee''s new anniversary, total hours is 2080 - the pto * hol hours
  in that time frame. Prior to 12/19/21 clock hours come from dealertrack, after 12/18/2021 clock hours come from UKG';

drop table if exists pto.flat_rate_pto_data_summary cascade;
create table pto.flat_rate_pto_data_summary (
  pay_period_seq integer not null,
  first_of_pp date not null,
  last_of_pp date not null,
  last_name citext not null,
  first_name citext not null,
  employee_number citext not null,
  store citext not null,
  department citext not null, 
  cost_center citext not null,
  manager_1 citext not null,
  manager_2 citext,
  seniority_date date not null,
  next_anniv date not null,
  earnings numeric(8,2) not null,
  clock_hours numeric(8,2) not null,
  current_pto_rate numeric(6,2) not null,
  new_pto_rate numeric(6,2) not null,
  effective_pto_rate numeric(6,2) not null,
  diff numeric(6,2) not null,
  primary key(employee_number,pay_period_seq));
comment on table pto.flat_rate_pto_data_summary is 'the data submitted to vision & payroll for each pay period with the pto rate calculation data for those
employees whose anniversary falls within the pay period'; 


drop table if exists pto.sales_pto_data_summary cascade;
create table pto.sales_pto_data_summary (
  year_month integer not null,
  last_name citext not null,
  first_name citext not null,
  employee_number citext not null,
  store citext not null,
  department citext not null, 
  cost_center citext not null,
  manager_1 citext not null,
  manager_2 citext,
  seniority_date date not null,
  next_anniv date not null,
  earnings numeric(8,2) not null,
  pto_hours numeric(8,2) not null,
  hol_hours numeric(8,2) not null,
  total_hours numeric(8,2) not null,
  current_pto_rate numeric(6,2) not null,
  new_pto_rate numeric(6,2) not null,
  diff numeric(6,2) not null,
  primary key(employee_number,year_month));
comment on table pto.sales_pto_data_summary is 'the data submitted to vision & payroll for each month with the pto rate calculation data for those
employees whose anniversary falls within the pay period'; 
  
-----------------------------------------------------------------------------------------
--< earnings codes
-----------------------------------------------------------------------------------------
select *
from ukg.earnings_codes		

alter table ukg.earnings_codes
add column include_in_pto_rate_calc boolean;

update ukg.earnings_codes
set include_in_pto_rate_calc = true;

alter table ukg.earnings_codes
alter column include_in_pto_rate_calc set not null;

update ukg.earnings_codes
set include_in_pto_rate_calc = false
where earnings_code in (
			'Benefit Allowance','Deferred Comp','Employee Referral Bonus','Expense Reimbursement','Gift Card',
			'Holiday','Lease Program', 'Lease Program 2', 'Lease Program 3', 'Lease Program 4', 'Lease Program 5',
			'Moving Expenses','Non-Cash Wages','OnStar Commission',--'Other Discretionary Bonus',
			'Owner Health Insurance Premium','Paid Time Off','PTO Payout','Severance','Sign-On Bonus',
			'Tool Allowance','Vision Award','Volunteer PTO','Year End Manager Bonus','Year End Owner Bonus',
			'2021 401K Hours');
-- 03/14/22 oops, that fucked up the nightly updating of ukg.earnings_codes
-- make it a pto table, then a script to check consistency with ukg.earnings_codes			


CREATE TABLE pto.earnings_codes
(
  id integer NOT NULL,
  earnings_code citext NOT NULL,
  earnings_code_name citext NOT NULL,
  include_in_pto_rate_calc boolean NOT NULL,
  CONSTRAINT earnings_codes_pkey PRIMARY KEY (id)
);

insert into pto.earnings_codes
select * from ukg.earnings_codes

alter table ukg.earnings_codes
drop column include_in_pto_rate_calc;

-----------------------------------------------------------------------------------------
--/> earnings codes
-----------------------------------------------------------------------------------------