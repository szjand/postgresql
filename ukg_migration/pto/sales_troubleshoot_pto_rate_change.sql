﻿/*
replace current_date everywhere where _first_of_pp_date
get rid of arkona tables
*/

do $$

declare
-- this will only be run on the first of the month via cron job
	_first_of_pp date := '03/01/2023';
	_last_of_pp date := '03/31/2023'; --(
-- 	  select the_date
-- 	  from dds.dim_date 
-- 	  where month_of_year = extract(month from current_date)
-- 	    and the_year = extract(year from current_date)
-- 	    and last_day_of_month); 
	_year_month integer := 202303; --(
-- 	  select distinct year_month
-- 	  from dds.dim_date
-- 	  where the_date = current_date);
begin

-- this table is a non persistent set of employees upon which to do the pto rate calculations on anniversaries
	delete 
	from pto.sales_employees;
	insert into pto.sales_employees(last_name,first_name,employee_number,employee_id,pay_calc_profile,
		store,department,cost_center,manager_1, manager_2,
		seniority_date,current_pto_rate,most_recent_anniv)
	select * 
	from (
		select a.last_name, a.first_name, a.employee_number, a.employee_id, c.display_name as pay_calc_profile, 
			d.store,  d.department, d.cost_center, a.manager_1, a.manager_2, coalesce(a.seniority_date, a.hire_date) as seniority_date,
			coalesce(e.current_rate, 0) as current_pto_rate,
			case
				when -- current year anniversary
					to_date(extract(year from _first_of_pp)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' ) < _first_of_pp   --   current_date 
						then to_date(extract(year from _first_of_pp)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' )
				else (to_date(extract(year from _first_of_pp)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' ) - interval '1 year')::date
			end as most_recent_anniv	
		from ukg.employees a
		join ukg.employee_profiles b on a.employee_id = b.employee_id
		join ukg.ext_pay_calc_profiles c on b.pay_calc_id = c.id
			and c.display_name = 'Full-Time Sales Commission Only'
		join (select * from ukg.get_cost_center_hierarchy()) d on a.cost_center = d.cost_center  
			and 
				case
					when a.store = 'GM' then d.store = 'Rydell GM'
					when a.store = 'HN' then d.store = 'Rydell Honda Nissan'
					when a.store = 'Toyota' then d.store = 'Rydell Toyota'
				end
		left join ukg.personal_rate_tables e on b.rate_table_id = e.rate_table_id
			and e.counter = 'paid time off'
		where a.status = 'active') s
  where 
    case
      when seniority_date = most_Recent_anniv then _first_of_pp /*current_Date*/ - seniority_date > 330
      else seniority_date <> most_Recent_anniv
    end;

  delete 
  from pto.sales_employee_dates
  where year_month = _year_month;
  
  insert into pto.sales_employee_dates(year_month,pay_period_start,pay_period_end,employee_number,last_anniv,
    next_anniv,prev_year_from_date,prev_year_thru_date)
	SELECT _year_month, _first_of_pp, _last_of_pp, employee_number, most_recent_anniv as last_anniv, 
	  (most_recent_anniv + interval '1 year')::date as next_anniv,
		(
			select first_of_month
			from sls.months
			WHERE seq = (
				select seq - 12
				from sls.months
				where (most_recent_anniv + interval '1 year')::date between first_of_month and last_of_month)) as prev_year_from_date,
		(
			select last_of_month
			from sls.months
			WHERE seq = (
				select seq -1
				from sls.months
				where (most_recent_anniv + interval '1 year')::date between first_of_month and last_of_month)) as prev_year_thru_date
	from pto.sales_employees 
	where (most_recent_anniv + interval '1 year')::date between _first_of_pp and _last_of_pp;

  delete 
  from pto.sales_earnings
  where year_month = _year_month;

  insert into pto.sales_earnings(year_month,employee_number,earnings_code,earnings)
	select year_month, employee_number, earnings_code, sum(earnings) as earnings
	from (
-- 		select b.year_month, a.employee_number, e.code_id || ' ' || e.description as earnings_code,
-- 			coalesce(d.base_pay, 0) + coalesce(e.amount, 0) as earnings
-- 		from pto.sales_employees a
-- 		join pto.sales_employee_dates b on a.employee_number = b.employee_number
-- 			and b.year_month = _year_month
-- 		join arkona.ext_pyptbdta c on arkona.db2_integer_to_date(c.payroll_start_date) between b.prev_year_from_date and b.prev_year_thru_date
-- 			and
-- 				case
-- 					when a.store like '%GM%' then c.company_number = 'RY1'
-- 					when a.store like '%Honda%' then c.company_number = 'RY2'
-- 					when a.store like '%Toyota%' then c.company_number = 'RY8'
-- 				end
-- 		join arkona.ext_pyhshdta d on c.company_number = d.company_number
-- 			and c.payroll_run_number = d.payroll_run_number
-- 			and a.employee_number = d.employee_
-- 			and d.total_gross_pay <> 0
-- 		left join arkona.ext_pyhscdta e on c.company_number = e.company_number -- does not exists for everyone, so left join
-- 			and c.payroll_run_number = e.payroll_run_number 
-- 			and a.employee_number = e.employee_number
-- 			and e.code_type = '1'
-- 			and e.code_id in ('79A','79','78','c19','79B','82')    	
-- 		union all -- ukg earnings
		select b.year_month, a.employee_number, e.earnings_code, c.ee_amount as earnings
		from pto.sales_employees a
		join pto.sales_employee_dates b on a.employee_number = b.employee_number
			and b.year_month = _year_month
		join ukg.pay_statement_earnings c on a.employee_id = c.employee_account_id
			and coalesce(c.ee_amount, 0) <> 0
		join ukg.payrolls d on c.payroll_id = d.payroll_id
			and d.payroll_start_date between b.prev_year_from_date and b.prev_year_thru_date
		join pto.earnings_codes e on c.earning_code = e.earnings_code
			and e.include_in_pto_rate_calc) f
	group by year_month, employee_number, earnings_code;	

	delete 
	from pto.sales_clock_hours
	where year_month = _year_month;

	insert into pto.sales_clock_hours(year_month,employee_number,clock_hours,pto_hours,hol_hours)
	select year_month, employee_number, sum(clock_hours) as clock_hours, sum(pto_hours) as pto_hours, sum(hol_hours) as hol_hours
	from (
		select b.year_month, a.last_name, a.first_name, a.employee_number, c.the_date, c.clock_hours, c.pto_hours, c.hol_hours
		from pto.sales_employees a
		join pto.sales_employee_dates b on a.employee_number = b.employee_number
			and b.year_month = _year_month
		join ukg.clock_hours c on a.employee_number = c.employee_number
			and
				case
					when a.employee_number = '159857' then c.the_date between '07/01/2021' and b.prev_year_thru_date
					else c.the_date between b.prev_year_from_date and b.prev_year_thru_date
				end
			and c.the_date > '12/18/2021') x
-- 		union 
-- 		select b.year_month, a.last_name, a.first_name, a.employee_number, c.the_date, c.clock_hours, c.pto_hours + c.vac_hours as pto_hours, c.hol_hours
-- 		from pto.sales_employees a
-- 		join pto.sales_employee_dates b on a.employee_number = b.employee_number
-- 			and b.year_month = _year_month
-- 		join arkona.xfm_pypclockin c on a.employee_number = c.employee_number
-- 			and
-- 				case
-- 					when a.employee_number = '159857' then c.the_date between '07/01/2021' and b.prev_year_thru_date
-- 					else c.the_date between b.prev_year_from_date and b.prev_year_thru_date
-- 				end
-- 			and c.the_date < '12/19/2021') x
	group by year_month, employee_number; 	

	delete
	from pto.sales_pto_data_summary
	where year_month = _year_month;

	insert into pto.sales_pto_data_summary
	select e.*, new_pto_rate - current_pto_rate as diff
	from ( 
	select b.year_month, a.last_name, a.first_name, a.employee_number, a.store, a.department, a.cost_center, 
		a.manager_1, a.manager_2, a.seniority_date, b.next_anniv, d.earnings, c.pto_hours, c.hol_hours,
		case
			-- if it is employees first anniversary, pto becomes available
			when a.current_pto_rate = 0 then (
				select (8 * count(*)) - c.pto_hours - c.hol_hours 
				from dds.dim_date 
				where weekday 
					and not holiday 
					and the_date between b.last_anniv and _first_of_pp) -- current_date)
			else 2080 - c.pto_hours - c.hol_hours
		end as total_hours,
		a.current_pto_rate, 
		case
			when a.current_pto_rate = 0  then round(d.earnings/((
				select (8 * count(*)) - c.pto_hours - c.hol_hours 
				from dds.dim_date 
				where weekday 
					and not holiday 
					and the_date between b.last_anniv and _first_of_pp)), 2) -- current_date)), 2)
			else round(d.earnings/(2080 - c.pto_hours - c.hol_hours), 2) 
		end as new_pto_rate
	from pto.sales_employees a
	join pto.sales_employee_dates b on a.employee_number = b.employee_number
		and b.year_month = _year_month
	join pto.sales_clock_hours c on a.employee_number = c.employee_number
		and b.year_month = c.year_month
	join (
		select year_month, employee_number, sum(earnings) as earnings
		from pto.sales_earnings
		where year_month = _year_month
		group by year_month, employee_number) d on a.employee_number = d.employee_number
			and b.year_month = d.year_month) e;	
	
end $$;