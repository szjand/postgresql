﻿/*
checked 4/10/23, ok, no anniversaries

*/

do $$
declare
-- this can actually run okay on each sunday, if clause does nothing if the date is not the first of a pay period
	_first_of_pp date := '04/09/2023'; -- (
-- 	  select distinct biweekly_pay_period_start_date
-- 	  from dds.dim_date
-- 	  where biweekly_pay_period_start_date = current_date); 
	_last_of_pp date := (
	  select biweekly_pay_period_end_date
	  from dds.dim_date 
	  where the_date = _first_of_pp);
	_pay_period_seq integer := (
	  select biweekly_pay_period_sequence
	  from dds.dim_date
	  where the_date = _first_of_pp);
begin
  drop table if exists employees;
  create temp table employees as
	select a.last_name, a.first_name, a.employee_number, a.employee_id, c.display_name as pay_calc_profile, 
		d.store,  d.department, d.cost_center, a.manager_1, a.manager_2, coalesce(a.seniority_date, a.hire_date) as seniority_date,
		coalesce(e.current_rate, 0) as current_pto_rate, 
		case 
			when -- current year anniversary
				to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' ) < _first_of_pp 
					then to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' )
			else (to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' ) - interval '1 year')::date
		end as most_recent_anniv	
	from ukg.employees a
	join ukg.employee_profiles b on a.employee_id = b.employee_id
	join ukg.ext_pay_calc_profiles c on b.pay_calc_id = c.id
		and c.display_name = 'Full-Time Flat Rate'
	join (select * from ukg.get_cost_center_hierarchy()) d on a.cost_center = d.cost_center  
		and 
			case
				when a.store = 'GM' then d.store = 'Rydell GM'
				when a.store = 'HN' then d.store = 'Rydell Honda Nissan'
				when a.store = 'Toyota' then d.store = 'Rydell Toyota'
			end
	left join ukg.personal_rate_tables e on b.rate_table_id = e.rate_table_id
		and e.counter = 'paid time off'
	where a.status = 'active';
end	$$;

select * from employees order by extract(month from seniority_date), extract(day from seniority_date);