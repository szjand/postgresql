﻿/*

jeri 2/22/22
I would like to talk through sales a bit.  I feel that in order to get the best data to base the PTO rate on, 
we should be using full months of payroll info, just due to how the draws work.  So for example, 
Dave Vanyo’s anniversary date is 12/18.  Rather than using the check from 12/15 that is just his draw, 
I feel that it would be more accurate to use the previous full month.  In this case we would go 
from 12/1/20 to 11/30/21.   The check dates are a bit of an issue between DT and UKG as we 
changed the process/timing of checks partway through last year.  Now, the month-end checks 
will have a date in the following month, so that check would be dated 12/1/21 or so.  I would 
like the hours to apply the same way.  We might need to actually talk over this one on a call.  
I’m around this afternoon if you have some time.
*/

-- borrowed from sc_payroll_201803.py (sls.pto_interval_dates)
-- this generates what jeri is looking for

select employee_number, seniority_date, most_recent_anniv,
	(
		select first_of_month
		from sls.months
		WHERE seq = (
			select seq - 12
			from sls.months
			where v.most_recent_anniv between first_of_month and last_of_month)) as from_date,
	(
		select last_of_month
		from sls.months
		WHERE seq = (
			select seq -1
			from sls.months
			where v.most_recent_anniv between first_of_month and last_of_month)) as thru_date
from (
	select a.last_name, a.first_name, a.employee_number, a.seniority_date, a.current_pto_rate, a.most_recent_anniv 
	from ukg_mig.pto_employees a
	where pay_calc_profile like '%sales%'
		and seniority_date <> a.most_recent_anniv) v -- exclude those with less than a year tenure

-- for semi monthly, need the relevant month of the pay check

select * 
from arkona.ext_pyptbdta
where payroll_cen_year = 122

select max(payroll_end_date)
from arkona.ext_pyptbdta
where payroll_end_date <> 20220215

select *
from arkona.ext_pyptbdta
where payroll_end_date = 20220215

select * 
from arkona.ext_pyptbdta
where payroll_cen_year = 121

-- should be able to use pyptbdta payroll_start_date to get the relevant month
-- starting in june, the month end check is dated in the following month, eg 7/2 for june, 8/2 for july, etc
select a.payroll_run_number, a.check_month, a.check_day, a.check_year, b.*
from arkona.ext_pyhshdta a
left join arkona.ext_pyptbdta b on a.payroll_run_number = b.payroll_run_number 
  and b.company_number = 'RY1'
where a.employee_ = '128530'
  and a.payroll_cen_year = 121
order by a.payroll_run_number

-- what about ukg
-- same thing   ukg.payrolls.payroll_start_date gives me the relevant month
select * 
from ukg.payrolls




