﻿/*
built from pto_5.sql
catch everybody's pto rate up to 3/1/2022, in effect on pay period 2/27 -> 3/13,
subsequently, everyone in this group, Full-Time Flat Rate,Full-Time Sales Commission Only will have rate updated
on anniversary
for biweekly the last check will be 2/18, for sales 3/1-ish (february pay)

do a report for each manager

*/
-- include current pto_rate, seniority date, most_recent anniv
-- no changes to this table
-- 03/05/2022 had to recreated this table, reuter is no longer flat rate
drop table if exists ukg_mig.pto_employees cascade;
create table ukg_mig.pto_employees as
select * from (
select a.last_name, a.first_name, a.employee_number, a.employee_id, c.display_name as pay_calc_profile, 
	(d.store||'/'|| d.department||'/'|| d.cost_center)::citext as cost_center, coalesce(a.seniority_date, a.hire_date) as seniority_date,
	e.current_rate as current_pto_rate,
	case
		when -- current year anniversary
			to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' ) < current_date 
				then to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' )
		else (to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' ) - interval '1 year')::date
	end as most_recent_anniv	
from ukg.employees a
join ukg.employee_profiles b on a.employee_id = b.employee_id
join ukg.ext_pay_calc_profiles c on b.pay_calc_id = c.id
  and c.display_name in ('Full-Time Flat Rate','Full-Time Sales Commission Only')
join (select * from ukg.get_cost_center_hierarchy()) d on a.cost_center = d.cost_center  
  and 
    case
      when a.store = 'GM' then d.store = 'Rydell GM'
      when a.store = 'HN' then d.store = 'Rydell Honda Nissan'
    end
left join ukg.personal_rate_tables e on b.rate_table_id = e.rate_table_id
  and e.counter = 'paid time off'
where a.status = 'active') s
where seniority_date <> most_Recent_anniv;

ALTER TABLE ukg_mig.pto_employees ADD PRIMARY KEY (employee_number);

select * from ukg_mig.pto_employees
-----------------------------------------------------------------
--< flat rate only
-----------------------------------------------------------------
-- dont think this table is necessary since we have fixed dates; 3/1/21 -> 2/28/22
-- !!!! this table form pto_5 is fucked up, it uses a table, ukg_mig.pto_flat_rate_base_earnings that does not exist !!!
-- drop table if exists ukg_mig.flat_rate_hours_date_range cascade;
-- create table ukg_mig.flat_rate_hours_date_range as
-- -- biweekly
-- select distinct aa.employee_number, c.biweekly_pay_period_start_date  as from_date, e.biweekly_pay_period_end_date as thru_date
-- from (
-- 	select a.employee_number, min(check_date) as min_check, max(check_date) as max_check
-- 	from (-- going back a year from the most recent anniversary, any checks issued in that date range
-- 		select last_name, first_name, employee_number, 
-- 			(most_recent_anniv - interval '1 year')::date as from_date, most_recent_anniv - 1 as thru_date 
-- 		from ukg_mig.pto_employees
-- 		where pay_calc_profile = 'Full-Time flat Rate')  a
-- 	left join  ukg_mig.pto_flat_rate_base_earnings b on a.employee_number = b.employee_number
-- 		and b.check_date between a.from_date and a.thru_date
-- 	group by a.employee_number) aa
-- left join dds.dim_date b on aa.min_check = b.the_date
-- left join dds.dim_date c on b.biweekly_pay_period_sequence = c.biweekly_pay_period_sequence + 1	
-- left join dds.dim_date d on aa.max_check = d.the_date
-- left join dds.dim_date e on d.biweekly_pay_period_sequence = e.biweekly_pay_period_sequence + 1;
-- -- 
-- -- alter table ukg_mig.flat_rate_hours_date_range add primary key (employee_number);

-- 02/26/22
-- fucked up the end date, should have noticed it was past the check date, but i didn't and jeri did

drop table if exists ukg_mig.pto_flat_rate_catch_up_dates cascade;
create table ukg_mig.pto_flat_rate_catch_up_dates as
select distinct aa.min_check_date, aa.max_check_date, c.biweekly_pay_period_start_date, /*d.biweekly_pay_period_end_date,*/ e.biweekly_pay_period_end_date
from (
	select min((b.check_month || '-' || b.check_day || '-' || (2000 + b. check_year))::date) as min_check_date,
		max(c.pay_date) as max_check_date
	from ukg_mig.pto_employees a
	join arkona.ext_pyhshdta b on a.employee_number = b.employee_
		and (b.check_month || '-' || b.check_day || '-' || (2000 + b. check_year))::date between '03/01/2021' and '02/28/2022'
		and b.total_gross_pay <> 0
	join ukg.payrolls c on true	
		and c.payroll_name like '%bi-weekly%'
		and c.pay_date < '02/28/2022'
	where a.pay_calc_profile = 'Full-Time flat Rate') aa
left join dds.dim_date b on aa.min_check_date = b.the_date
left join dds.dim_date c on b.biweekly_pay_period_sequence = c.biweekly_pay_period_sequence + 1	
left join dds.dim_date d on aa.max_check_date = d.the_date
left join dds.dim_date e on d.biweekly_pay_period_sequence = e.biweekly_pay_period_sequence + 1;

select * from ukg_mig.pto_flat_rate_catch_up_dates

drop table if exists ukg_mig.pto_flat_rate_catch_up_earnings cascade;
create table ukg_mig.pto_flat_rate_catch_up_earnings as
-- no no no not using pay period dates for earnings, but check dates in the previous year
select employee_number, sum(earnings) as earnings 
from (-- dealer track 2020/2021 earnings
	select a.employee_number, coalesce(b.base_pay, 0) + coalesce(c.amount, 0) as earnings -- needed base pay for some techs sum(c.amount) as earnings
	from ukg_mig.pto_employees  a
	join arkona.ext_pyhshdta b on a.employee_number = b.employee_
		and (b.check_month || '-' || b.check_day || '-' || (2000 + b. check_year))::date between '03/01/2021' and '02/28/2022'
		and b.total_gross_pay <> 0
	left join arkona.ext_pyhscdta c on b.company_number = c.company_number -- does not exists for everyone, so left join
		and b.payroll_run_number = c.payroll_run_number 
		and a.employee_number = c.employee_number
		and c.code_type = '1'
		and c.code_id in ('79','C19','cov','75','87','70')
	where a.pay_calc_profile = 'Full-Time Flat Rate'    
	union all -- ukg earnings
	select a.employee_number, b.ee_amount as earnings
	from ukg_mig.pto_employees  a
	join ukg.pay_statement_earnings b on a.employee_id = b.employee_account_id
		and coalesce(b.ee_amount, 0) <> 0
	join ukg.payrolls c on b.payroll_id = c.payroll_id
		and c.pay_date between '03/01/2021' and '02/28/2022'
	where b.earning_code not in (
			'Benefit Allowance','Deferred Comp','Employee Referral Bonus','Expense Reimbursement','Gift Card',
			'Holiday','Lease Program', 'Lease Program 2', 'Lease Program 3', 'Lease Program 4', 'Lease Program 5',
			'Moving Expenses','Non-Cash Wages','OnStar Commission','Other Discretionary Bonus',
			'Owner Health Insurance Premium','Paid Time Off','PTO Payout','Severance','Sign-On Bonus',
			'Tool Allowance','Vision Award','Volunteer PTO','Year End Manager Bonus','Year End Owner Bonus',
			'2021 401K Hours')  -- just the 2 anomalies with amounts
	and a.pay_calc_profile = 'Full-Time Flat Rate'  ) x
group by employee_number

alter table ukg_mig.pto_flat_rate_catch_up_earnings add primary key (employee_number);

select * from ukg_mig.pto_flat_rate_catch_up_earnings;

-- jeri noticed an anomaly, i forgot to account for the period of dual clocking
drop table if exists ukg_mig.flat_rate_catch_up_clock_hours cascade;
create table ukg_mig.flat_rate_catch_up_clock_hours as
-- select employee_number, sum(clock_hours) as clock_hours, sum(pto_hours) as pto_hours, sum(hol_hours) as hol_hours
-- from (
-- 	select a.employee_number, a.clock_hours, a.pto_hours, a.hol_hours
-- 	from ukg.clock_hours a
-- 	join ukg_mig.pto_employees b on a.employee_number = b.employee_number
-- 	  and b.pay_calc_profile = 'Full-Time flat Rate'
-- 	join ukg_mig.pto_flat_rate_catch_up_dates c on a.the_date between c.biweekly_pay_period_start_date and c.biweekly_pay_period_end_date
-- 	union all
-- 	select a.employee_number, a.clock_hours, a.vac_hours + a.pto_hours as pto_hours, a.hol_hours
-- 	from arkona.xfm_pypclockin a
-- 	join ukg_mig.pto_employees b on a.employee_number = b.employee_number
-- 	  and b.pay_calc_profile = 'Full-Time flat Rate'
-- 	join ukg_mig.pto_flat_rate_catch_up_dates c on a.the_date between c.biweekly_pay_period_start_date and c.biweekly_pay_period_end_date) x
-- group by employee_number;

select employee_number, sum(clock_hours) as clock_hours, sum(pto_hours) as pto_hours, sum(hol_hours) as hol_hours
from (
	select a.employee_number, a.clock_hours, a.pto_hours, a.hol_hours
	from ukg.clock_hours a
	join ukg_mig.pto_employees b on a.employee_number = b.employee_number
	  and b.pay_calc_profile = 'Full-Time flat Rate'
	join ukg_mig.pto_flat_rate_catch_up_dates c on a.the_date between c.biweekly_pay_period_start_date and c.biweekly_pay_period_end_date
	where a.the_date > '12/18/2021'
	union all
	select a.employee_number, a.clock_hours, a.vac_hours + a.pto_hours as pto_hours, a.hol_hours
	from arkona.xfm_pypclockin a
	join ukg_mig.pto_employees b on a.employee_number = b.employee_number
	  and b.pay_calc_profile = 'Full-Time flat Rate'
	join ukg_mig.pto_flat_rate_catch_up_dates c on a.the_date between c.biweekly_pay_period_start_date and c.biweekly_pay_period_end_date
	where a.the_date < '12/19/2021') x
group by employee_number;

alter table ukg_mig.pto_flat_rate_catch_up_earnings add primary key (employee_number);

-- sent to jeri as flat_rate_pto_data_catchup_period.xlsx on 2/28/22
select * from (
select a.last_name, a.first_name, a.employee_number, a.cost_center, a.seniority_date, a.current_pto_rate, 
	/*b.from_date, b.thru_date,*/ 
	c.earnings, d.clock_hours, --d.pto_hours, d.hol_hours,
--   clock_hours - pto_hours - hol_hours as total_hours,
--   round(c.earnings/(clock_hours - pto_hours - hol_hours), 2) as calc_pto_rate, 
--   (round(c.earnings/(clock_hours - pto_hours - hol_hours), 2)) - current_pto_rate as diff,
  round(c.earnings/clock_hours, 2) as calc_pto_rate, round(c.earnings/clock_hours, 2) - current_pto_rate as diff
from ukg_mig.pto_employees a
-- join ukg_mig.flat_rate_hours_date_range b on a.employee_number = b.employee_number
join ukg_mig.pto_flat_rate_catch_up_earnings c on a.employee_number = c.employee_number
join ukg_mig.flat_rate_catch_up_clock_hours d on a.employee_number = d.employee_number
left join ukg_mig.gm_tech_pto e on a.employee_number = e.employee_number
where a.pay_calc_profile = 'Full-Time Flat Rate'
) x 
order by cost_Center, last_name
order by employee_number:: integer


-- 03/05/2022
-- add most recent anniversary, pay calc, managers
-- and needed to recreate some tables, reuter should not be in flat rate
select * from ukg.employees where status = 'active' limit 20
select * from ukg_mig.pto_employees

-- send to jeri as flat_rate_pto_data_catchup_period_v2.xlsx on 03/06/22
-- advise her about added fields and removal of reuter
-- send to jeri as flat_rate_pto_data_catchup_period_v3.xlsx on 03/07/22 after accounting for the double punching of clock hours
select a.last_name, a.first_name, a.employee_number as "emp #", a.cost_center, pay_calc_profile, manager_1, manager_2, a.seniority_date, most_recent_anniv, a.current_pto_rate, 
	/*b.from_date, b.thru_date,*/ 
	c.earnings, d.clock_hours, --d.pto_hours, d.hol_hours,
--   clock_hours - pto_hours - hol_hours as total_hours,
--   round(c.earnings/(clock_hours - pto_hours - hol_hours), 2) as calc_pto_rate, 
--   (round(c.earnings/(clock_hours - pto_hours - hol_hours), 2)) - current_pto_rate as diff,
  round(c.earnings/clock_hours, 2) as calc_pto_rate, round(c.earnings/clock_hours, 2) - current_pto_rate as diff
from ukg_mig.pto_employees a
join ukg.employees aa on a.employee_id = aa.employee_id
join ukg_mig.pto_flat_rate_catch_up_earnings c on a.employee_number = c.employee_number
join ukg_mig.flat_rate_catch_up_clock_hours d on a.employee_number = d.employee_number
left join ukg_mig.gm_tech_pto e on a.employee_number = e.employee_number
where a.pay_calc_profile = 'Full-Time Flat Rate'
order by cost_Center, last_name
-----------------------------------------------------------------
--/> flat rate only
-----------------------------------------------------------------

-----------------------------------------------------------------
--< sales only
-----------------------------------------------------------------
-- daniel gonzalez needs a special calc, started in sales on 6/29/21

/*
Unfortunately, as the “other bonus” has been used incorrectly for street purchases—I think 
we need to go ahead and include the “Other Discretionary Bonus” earning code for sales earnings.
*/

select * 
from ukg_mig.pto_employees
where pay_calc_profile = 'Full-Time Sales Commission Only'

-- 03/08 include Other Discretionary Bonus pay
drop table if exists ukg_mig.pto_sales_catch_up_earnings cascade;
create table ukg_mig.pto_sales_catch_up_earnings as
select employee_number, sum(earnings) as earnings
from (
	select a.employee_number, e.amount as earnings
	from ukg_mig.pto_employees a
-- 	join ukg_mig.sales_date_range b on a.employee_number = b.employee_number
-- payroll start date to get the checks for the relevant months, pay date may be in the next month
	join arkona.ext_pyptbdta c on arkona.db2_integer_to_date(c.payroll_start_date) between '03/01/2021' and '02/28/2022'
		and
			case
				when a.cost_center like '%GM%' then c.company_number = 'RY1'
				when a.cost_center like '%Honda%' then c.company_number = 'RY2'
			end
-- 	join arkona.ext_pyhshdta d on c.company_number = d.company_number
-- 		and c.payroll_run_number = d.payroll_run_number
-- 		and a.employee_number = d.employee_
-- 		and d.total_gross_pay <> 0
	left join arkona.ext_pyhscdta e on c.company_number = e.company_number -- does not exists for everyone, so left join
		and c.payroll_run_number = e.payroll_run_number 
		and a.employee_number = e.employee_number
		and e.code_type = '1'
		and e.code_id in ('79A','79','78','c19','79B','82')        
	where a.pay_calc_profile = 'Full-Time Sales Commission Only'    
	union all -- ukg earnings
	select a.employee_number, c.ee_amount as earnings
	from ukg_mig.pto_employees  a
-- 	join ukg_mig.sales_date_range b on a.employee_number = b.employee_number
	join ukg.pay_statement_earnings c on a.employee_id = c.employee_account_id
		and coalesce(c.ee_amount, 0) <> 0
	join ukg.payrolls d on c.payroll_id = d.payroll_id
		and d.payroll_start_date between '03/01/2021' and '02/28/2022'
	where c.earning_code not in (
			'Benefit Allowance','Deferred Comp','Employee Referral Bonus','Expense Reimbursement','Gift Card',
			'Holiday','Lease Program', 'Lease Program 2', 'Lease Program 3', 'Lease Program 4', 'Lease Program 5',
			'Moving Expenses','Non-Cash Wages','OnStar Commission',/*'Other Discretionary Bonus',*/
			'Owner Health Insurance Premium','Paid Time Off','PTO Payout','Severance','Sign-On Bonus',
			'Tool Allowance','Vision Award','Volunteer PTO','Year End Manager Bonus','Year End Owner Bonus',
			'2021 401K Hours')  -- just the 2 anomalies with amounts
		and a.pay_calc_profile = 'Full-Time Sales Commission Only') x
group by employee_number;
alter table ukg_mig.pto_sales_catch_up_earnings add primary key (employee_number);


-- gonazlez, these earnings are from sale dept only
select * from ukg_mig.pto_sales_catch_up_earnings where employee_number = '159857'


drop table if exists ukg_mig.pto_sales_catch_up_clock_hours cascade;
create table ukg_mig.pto_sales_catch_up_clock_hours as
select employee_number, sum(clock_hours) as clock_hours, sum(pto_hours) as pto_hours, sum(hol_hours) as hol_hours
from (
	select a.employee_number, a.clock_hours, a.pto_hours, a.hol_hours
	from ukg.clock_hours a
	join ukg_mig.pto_employees b on a.employee_number = b.employee_number
    and pay_calc_profile = 'Full-Time Sales Commission Only'
	where 
	  case
	    when a.employee_number = '159857' then a.the_date between '07/01/2021' and '02/28/2022'
	    else a.the_date between '03/01/2021' and '02/28/2022'
	  end
	and a.the_date > '12/18/2021'
	union all
	select a.employee_number, a.clock_hours, a.vac_hours + a.pto_hours as pto_hours, a.hol_hours
	from arkona.xfm_pypclockin a
	join ukg_mig.pto_employees b on a.employee_number = b.employee_number
    and pay_calc_profile = 'Full-Time Sales Commission Only'
	where 
	  case
	    when a.employee_number = '159857' then a.the_date between '07/01/2021' and '02/28/2022'
	    else a.the_date between '03/01/2021' and '02/28/2022'
	  end
	and a.the_date < '12/19/2021') x
group by employee_number;
alter table ukg_mig.pto_sales_catch_up_clock_hours add primary key (employee_number);

select * from ukg_mig.pto_sales_catch_up_clock_hours order by employee_number
-- -- gonzalez
-- select count(*)/7 -- 243 days, 34 weeks, 1360 hours
-- from dds.dim_date
-- where the_date between '07/01/2021' and '02/28/2022'
-- 03/09 sent to jeri as V2, cleanend up double punching in clock hours, included earnings code Other Discretionary Bonus
select a.last_name, a.first_name, a.employee_number as "emp #", a.cost_center, pay_calc_profile, manager_1, manager_2, a.seniority_date, most_recent_anniv, a.current_pto_rate, c.earnings,
-- 	a.*, /*b.from_date, b.thru_date,*/ c.earnings, /*d.clock_hours,*/ d.pto_hours, d.hol_hours,
  pto_hours, hol_hours,
  case
    when a.employee_number = '159857' then 1360 - pto_hours - hol_hours
    else 2080 - pto_hours - hol_hours 
  end as total_hours,
  case
    when a.employee_number = '159857' then round(c.earnings/(1360 - pto_hours - hol_hours), 2)
    else round(c.earnings/(2080 - pto_hours - hol_hours), 2) 
  end as calc_pto_rate,
  case
    when a.employee_number = '159857' then (round(c.earnings/(1360 - pto_hours - hol_hours), 2)) - a.current_pto_rate
    else (round(c.earnings/(2080 - pto_hours - hol_hours), 2)) - a.current_pto_rate 
  end as diff
--   e.cur_rate_eff_date
from ukg_mig.pto_employees a
join ukg.employees aa on a.employee_id = aa.employee_id 
-- join ukg_mig.sales_date_range b on a.employee_number = b.employee_number
join ukg_mig.pto_sales_catch_up_earnings c on a.employee_number = c.employee_number
join ukg_mig.pto_sales_catch_up_clock_hours d on a.employee_number = d.employee_number
left join ukg_mig.sc e on a.employee_number = e.employee_number
where a.pay_calc_profile = 'Full-Time Sales Commission Only'
order by cost_center, last_name


-----------------------------------------------------------------
--/> sales only
-----------------------------------------------------------------

