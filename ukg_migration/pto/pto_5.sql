﻿
/*
ukg_mig.pto_employees
ukg_mig.pto_calc_dates
ukg_mig.pto_earnings
ukg_mig.pto_clock_hours
*/
/*
jeri 2/22/22
I was able to recalculate the earnings and am good with that portion.

For the hours—the hours that are used for the time frame do not tie to the pay check dates.  
I believe that the hours included should correspond with the earnings that are paid on the check dates.  
For Marcus—I would use dates 8/2/20 – 7/31/21 as these correspond to the checks included.  
I got a total of 2220.27 clock hours (2068.27 reg+OT,  104 PTO, 48 holiday).  
Open to discussion on this.

jeri 2/22/22
I would like to talk through sales a bit.  I feel that in order to get the best data to base the PTO rate on, 
we should be using full months of payroll info, just due to how the draws work.  So for example, 
Dave Vanyo’s anniversary date is 12/18.  Rather than using the check from 12/15 that is just his draw, 
I feel that it would be more accurate to use the previous full month.  In this case we would go 
from 12/1/20 to 11/30/21.   The check dates are a bit of an issue between DT and UKG as we 
changed the process/timing of checks partway through last year.  Now, the month-end checks 
will have a date in the following month, so that check would be dated 12/1/21 or so.  I would 
like the hours to apply the same way.  We might need to actually talk over this one on a call.  
I’m around this afternoon if you have some time.
*/ 

-- 2/22/22 built on pto_4, to include jeris vision for dates

-- include current pto_rate, seniority date, most_recent anniv
drop table if exists ukg_mig.pto_employees cascade;
create table ukg_mig.pto_employees as
select * from (
select a.last_name, a.first_name, a.employee_number, a.employee_id, c.display_name as pay_calc_profile, 
	(d.store||'/'|| d.department||'/'|| d.cost_center)::citext as cost_center, coalesce(a.seniority_date, a.hire_date) as seniority_date,
	e.current_rate as current_pto_rate,
	case
		when -- current year anniversary
			to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' ) < current_date 
				then to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' )
		else (to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' ) - interval '1 year')::date
	end as most_recent_anniv	
from ukg.employees a
join ukg.employee_profiles b on a.employee_id = b.employee_id
join ukg.ext_pay_calc_profiles c on b.pay_calc_id = c.id
  and c.display_name in ('Full-Time Flat Rate','Full-Time Sales Commission Only')
join (select * from ukg.get_cost_center_hierarchy()) d on a.cost_center = d.cost_center  
  and 
    case
      when a.store = 'GM' then d.store = 'Rydell GM'
      when a.store = 'HN' then d.store = 'Rydell Honda Nissan'
    end
left join ukg.personal_rate_tables e on b.rate_table_id = e.rate_table_id
  and e.counter = 'paid time off'
where a.status = 'active') s
where seniority_date <> most_Recent_anniv;

ALTER TABLE ukg_mig.pto_employees ADD PRIMARY KEY (employee_number);

-----------------------------------------------------------------
--< flat rate only
-----------------------------------------------------------------
/*
02/25 as i am working on pto_6, the catch up iteration, realized
that this query is using a non existent table, ukg_mig.pto_flat_rate_base_earnings
!!! WTF !!!
*/
drop table if exists ukg_mig.flat_rate_hours_date_range cascade;
create table ukg_mig.flat_rate_hours_date_range as
-- biweekly
select distinct aa.employee_number, c.biweekly_pay_period_start_date  as from_date, e.biweekly_pay_period_end_date as thru_date
from (
	select a.employee_number, min(check_date) as min_check, max(check_date) as max_check
	from (-- going back a year from the most recent anniversary, any checks issued in that date range
		select last_name, first_name, employee_number, 
			(most_recent_anniv - interval '1 year')::date as from_date, most_recent_anniv - 1 as thru_date 
		from ukg_mig.pto_employees
		where pay_calc_profile = 'Full-Time flat Rate')  a
	left join  ukg_mig.pto_flat_rate_base_earnings b on a.employee_number = b.employee_number
		and b.check_date between a.from_date and a.thru_date
	group by a.employee_number) aa
left join dds.dim_date b on aa.min_check = b.the_date
left join dds.dim_date c on b.biweekly_pay_period_sequence = c.biweekly_pay_period_sequence + 1	
left join dds.dim_date d on aa.max_check = d.the_date
left join dds.dim_date e on d.biweekly_pay_period_sequence = e.biweekly_pay_period_sequence + 1;

alter table ukg_mig.flat_rate_hours_date_range add primary key (employee_number);


-- select * from ukg_mig.pto_employees;

  -- flat rate:
    -- include commissions,covid,rate adjust, tech hours pay,tech xtra bonus,
		-- exclude holiday pay, health insurance,lease program, pay out pto,vac/pto pay
  -- sales:
		-- include A-Z Sales F&I, COMMISSIONS,DRAWS','PULSE PAY','COVID 19','SPIFF PAY OUTS'
		-- exclude LEASE PROGRAM,PAY OUT PTO,'SALES VACATION'

drop table if exists ukg_mig.pto_flat_rate_earnings cascade;
create table ukg_mig.pto_flat_rate_earnings as
-- no no no not using pay period dates for earnings, but check dates in the previous year
select employee_number, sum(earnings) as earnings 
from (-- dealer track 2020/2021 earnings
	select a.employee_number, coalesce(b.base_pay, 0) + coalesce(c.amount, 0) as earnings -- needed base pay for some techs sum(c.amount) as earnings
	from ukg_mig.pto_employees  a
	join arkona.ext_pyhshdta b on a.employee_number = b.employee_
		and (b.check_month || '-' || b.check_day || '-' || (2000 + b. check_year))::date between (a.most_recent_anniv - interval '1 year')::date and a.most_recent_anniv - 1 
		and b.total_gross_pay <> 0
	left join arkona.ext_pyhscdta c on b.company_number = c.company_number -- does not exists for everyone, so left join
		and b.payroll_run_number = c.payroll_run_number 
		and a.employee_number = c.employee_number
		and c.code_type = '1'
		and c.code_id in ('79','C19','cov','75','87','70')
	where a.pay_calc_profile = 'Full-Time Flat Rate'    
	union all -- ukg earnings
	select a.employee_number, b.ee_amount as earnings
	from ukg_mig.pto_employees  a
	join ukg.pay_statement_earnings b on a.employee_id = b.employee_account_id
		and coalesce(b.ee_amount, 0) <> 0
	join ukg.payrolls c on b.payroll_id = c.payroll_id
		and c.pay_date between (a.most_recent_anniv - interval '1 year')::date and a.most_recent_anniv - 1 
	where b.earning_code not in (
			'Benefit Allowance','Deferred Comp','Employee Referral Bonus','Expense Reimbursement','Gift Card',
			'Holiday','Lease Program', 'Lease Program 2', 'Lease Program 3', 'Lease Program 4', 'Lease Program 5',
			'Moving Expenses','Non-Cash Wages','OnStar Commission','Other Discretionary Bonus',
			'Owner Health Insurance Premium','Paid Time Off','PTO Payout','Severance','Sign-On Bonus',
			'Tool Allowance','Vision Award','Volunteer PTO','Year End Manager Bonus','Year End Owner Bonus',
			'2021 401K Hours')  -- just the 2 anomalies with amounts
	and a.pay_calc_profile = 'Full-Time Flat Rate'  ) x
group by employee_number;

alter table ukg_mig.pto_flat_rate_earnings add primary key (employee_number);

-- select * from ukg_mig.pto_flat_rate_base_earnings;



drop table if exists ukg_mig.flat_rate_clock_hours cascade;
create table ukg_mig.flat_rate_clock_hours as
select employee_number, sum(clock_hours) as clock_hours, sum(pto_hours) as pto_hours, sum(hol_hours) as hol_hours
from (
	select a.employee_number, a.clock_hours, a.pto_hours, a.hol_hours
	from ukg.clock_hours a
	join ukg_mig.pto_employees b on a.employee_number = b.employee_number
	join ukg_mig.flat_rate_hours_date_range c on a.employee_number = c.employee_number
		and a.the_date between c.from_date and c.thru_date
	union all
	select a.employee_number, a.clock_hours, a.vac_hours + a.pto_hours as pto_hours, a.hol_hours
	from arkona.xfm_pypclockin a
	join ukg_mig.pto_employees b on a.employee_number = b.employee_number
	join ukg_mig.flat_rate_hours_date_range c on a.employee_number = c.employee_number
		and a.the_date between c.from_date and c.thru_date) x
group by employee_number;

alter table ukg_mig.flat_rate_clock_hours add primary key (employee_number);

select * from (
select a.*, b.from_date, b.thru_date, c.earnings, d.clock_hours, d.pto_hours, d.hol_hours,
  clock_hours - pto_hours - hol_hours as total_hours,
  round(c.earnings/(clock_hours - pto_hours - hol_hours), 2) as calc_pto_rate,
  (round(c.earnings/(clock_hours - pto_hours - hol_hours), 2)) - current_pto_rate as diff,
  e.cur_rate_eff_date
from ukg_mig.pto_employees a
join ukg_mig.flat_rate_hours_date_range b on a.employee_number = b.employee_number
join ukg_mig.pto_flat_rate_earnings c on a.employee_number = c.employee_number
join ukg_mig.flat_rate_clock_hours d on a.employee_number = d.employee_number
left join ukg_mig.gm_tech_pto e on a.employee_number = e.employee_number
where a.pay_calc_profile = 'Full-Time Flat Rate'
) x 
order by cost_Center
order by employee_number:: integer

-----------------------------------------------------------------
--/> flat rate only
-----------------------------------------------------------------


-----------------------------------------------------------------
--< sales only
-----------------------------------------------------------------

drop table if exists ukg_mig.flat_rate_hours_date_range cascade;
create table ukg_mig.flat_rate_hours_date_range as
-- biweekly
select distinct aa.employee_number, c.biweekly_pay_period_start_date  as from_date, e.biweekly_pay_period_end_date as thru_date
from (
	select a.employee_number, min(check_date) as min_check, max(check_date) as max_check
	from (-- going back a year from the most recent anniversary, any checks issued in that date range
		select last_name, first_name, employee_number, 
			(most_recent_anniv - interval '1 year')::date as from_date, most_recent_anniv - 1 as thru_date 
		from ukg_mig.pto_employees
		where pay_calc_profile = 'Full-Time flat Rate')  a
	left join  ukg_mig.pto_flat_rate_base_earnings b on a.employee_number = b.employee_number
		and b.check_date between a.from_date and a.thru_date
	group by a.employee_number) aa
left join dds.dim_date b on aa.min_check = b.the_date
left join dds.dim_date c on b.biweekly_pay_period_sequence = c.biweekly_pay_period_sequence + 1	
left join dds.dim_date d on aa.max_check = d.the_date
left join dds.dim_date e on d.biweekly_pay_period_sequence = e.biweekly_pay_period_sequence + 1;

alter table ukg_mig.flat_rate_hours_date_range add primary key (employee_number);


drop table if exists ukg_mig.sales_date_range cascade;
create table ukg_mig.sales_date_range as
select employee_number, 
	(
		select first_of_month
		from sls.months
		WHERE seq = (
			select seq - 12
			from sls.months
			where v.most_recent_anniv between first_of_month and last_of_month)) as from_date,
	(
		select last_of_month
		from sls.months
		WHERE seq = (
			select seq -1
			from sls.months
			where v.most_recent_anniv between first_of_month and last_of_month)) as thru_date
from (
	select a.last_name, a.first_name, a.employee_number, a.seniority_date, a.current_pto_rate, a.most_recent_anniv 
	from ukg_mig.pto_employees a
	where pay_calc_profile = 'Full-Time Sales Commission Only'
		and seniority_date <> a.most_recent_anniv) v; -- exclude those with less than a year tenure
alter table ukg_mig.sales_date_range add primary key (employee_number);



  -- flat rate:
    -- include commissions,covid,rate adjust, tech hours pay,tech xtra bonus,
		-- exclude holiday pay, health insurance,lease program, pay out pto,vac/pto pay
  -- sales:
		-- include A-Z Sales F&I, COMMISSIONS,DRAWS','PULSE PAY','COVID 19','SPIFF PAY OUTS'
		-- exclude LEASE PROGRAM,PAY OUT PTO,'SALES VACATION'

drop table if exists ukg_mig.pto_sales_earnings cascade;
create table ukg_mig.pto_sales_earnings as
select employee_number, sum(earnings) as earnings
from (
	select a.employee_number, e.amount as earnings
	from ukg_mig.pto_employees a
	join ukg_mig.sales_date_range b on a.employee_number = b.employee_number
	join arkona.ext_pyptbdta c on arkona.db2_integer_to_date(c.payroll_start_date) between b.from_date and b.thru_date
		and
			case
				when a.cost_center like '%GM%' then c.company_number = 'RY1'
				when a.cost_center like '%Honda%' then c.company_number = 'RY2'
			end
	join arkona.ext_pyhshdta d on c.company_number = d.company_number
		and c.payroll_run_number = d.payroll_run_number
		and a.employee_number = d.employee_
		and d.total_gross_pay <> 0
	left join arkona.ext_pyhscdta e on c.company_number = e.company_number -- does not exists for everyone, so left join
		and c.payroll_run_number = e.payroll_run_number 
		and a.employee_number = e.employee_number
		and e.code_type = '1'
		and e.code_id in ('79A','79','78','c19','79B','82')        
	where a.pay_calc_profile = 'Full-Time Sales Commission Only'    
	union all -- ukg earnings
	select a.employee_number, c.ee_amount as earnings
	from ukg_mig.pto_employees  a
	join ukg_mig.sales_date_range b on a.employee_number = b.employee_number
	join ukg.pay_statement_earnings c on a.employee_id = c.employee_account_id
		and coalesce(c.ee_amount, 0) <> 0
	join ukg.payrolls d on c.payroll_id = d.payroll_id
		and d.payroll_start_date between b.from_date and b.thru_date
	where c.earning_code not in (
			'Benefit Allowance','Deferred Comp','Employee Referral Bonus','Expense Reimbursement','Gift Card',
			'Holiday','Lease Program', 'Lease Program 2', 'Lease Program 3', 'Lease Program 4', 'Lease Program 5',
			'Moving Expenses','Non-Cash Wages','OnStar Commission','Other Discretionary Bonus',
			'Owner Health Insurance Premium','Paid Time Off','PTO Payout','Severance','Sign-On Bonus',
			'Tool Allowance','Vision Award','Volunteer PTO','Year End Manager Bonus','Year End Owner Bonus',
			'2021 401K Hours')  -- just the 2 anomalies with amounts
		and a.pay_calc_profile = 'Full-Time Sales Commission Only') x
group by employee_number;
alter table ukg_mig.pto_sales_earnings add primary key (employee_number);


drop table if exists ukg_mig.pto_sales_clock_hours cascade;
create table ukg_mig.pto_sales_clock_hours as
select employee_number, sum(clock_hours) as clock_hours, sum(pto_hours) as pto_hours, sum(hol_hours) as hol_hours
from (
	select a.employee_number, a.clock_hours, a.pto_hours, a.hol_hours
	from ukg.clock_hours a
	join ukg_mig.pto_employees b on a.employee_number = b.employee_number
	join ukg_mig.sales_date_range c on a.employee_number = c.employee_number
		and a.the_date between c.from_date and c.thru_date
	union all
	select a.employee_number, a.clock_hours, a.vac_hours + a.pto_hours as pto_hours, a.hol_hours
	from arkona.xfm_pypclockin a
	join ukg_mig.pto_employees b on a.employee_number = b.employee_number
	join ukg_mig.sales_date_range c on a.employee_number = c.employee_number
		and a.the_date between c.from_date and c.thru_date) x
group by employee_number;
alter table ukg_mig.pto_sales_clock_hours add primary key (employee_number);

select * from arkona.xfm_pymast where pymast_employee_number = '159857';

select * from (
select a.*, b.from_date, b.thru_date, c.earnings, d.clock_hours, d.pto_hours, d.hol_hours,
  2080 - pto_hours - hol_hours as total_hours,
  round(c.earnings/(2080 - pto_hours - hol_hours), 2) as calc_pto_rate,
  (round(c.earnings/(2080 - pto_hours - hol_hours), 2)) - a.current_pto_rate as diff,
  e.cur_rate_eff_date
from ukg_mig.pto_employees a
join ukg_mig.sales_date_range b on a.employee_number = b.employee_number
join ukg_mig.pto_sales_earnings c on a.employee_number = c.employee_number
join ukg_mig.pto_sales_clock_hours d on a.employee_number = d.employee_number
left join ukg_mig.sc e on a.employee_number = e.employee_number
where a.pay_calc_profile = 'Full-Time Sales Commission Only'
) x 
-- order by cost_Center
order by employee_number:: integer
-----------------------------------------------------------------
--/> sales only
-----------------------------------------------------------------
gonzalez started in sales on june 29
select * from arkona.xfm_pymast where pymast_employee_number = '159857' order by row_from_date