﻿---------------------------------------------------------------------
--< gm flat rate
---------------------------------------------------------------------
select * 
from (
	select *, row_number() over (partition by employee_number order by from_date desc) as seq
	from (
		select a.last_name, a.first_name, a.employee_number, aa.most_recent_anniv, a.current_pto_rate, c.hourly_rate, 
			min(c.from_date) as from_date, max(c.thru_date) as thru_date
		from ukg_mig.pto_employees a
		join ukg_mig.pto_calc_dates aa on a.employee_number = aa.employee_number
		join tp.techs b on a.employee_number = b.employee_number
		join tp.tech_values c on b.tech_key = c.tech_key
		group by a.last_name, a.first_name, a.employee_number, aa.most_recent_anniv, a.current_pto_rate, c.hourly_rate) d) e
where seq = 1
order by last_name, first_name, seq

select * from tp.tech_values 

select * from 

drop table if exists ukg_mig.gm_tech_pto cascade;
create table ukg_mig.gm_tech_pto as
select *,
  (
		select min(from_date) as cur_rate_eff_date
		from tp.tech_values
		where tech_key = x.tech_key
		  and hourly_rate = x.hourly_rate)
from (
  select aa.last_name, aa.first_name, aa.employee_number, aa.tech_key,
		(
			select hourly_rate
			from tp.tech_values
			where thru_date > current_date
			  and tech_key = aa.tech_key)
  from (
		select a.last_name, a.first_name, a.employee_number, max(b.tech_key) as tech_key
		from ukg_mig.pto_employees a
		join tp.techs b on a.employee_number = b.employee_number
		group by a.last_name, a.first_name, a.employee_number
		order by a.last_name) aa) x

---------------------------------------------------------------------
--< gm flat rate
---------------------------------------------------------------------

---------------------------------------------------------------------
--/> hn flat rate
---------------------------------------------------------------------
select * 
from hs.main_shop_flat_rate_techs a
join ukg_mig.pto_employees b on a.employee_number = b.employee_number
  and b.cost_center = 'Rydell Honda Nissan/Main Shop/Main Shop Technician'

-- current pto rate and the date from which that rate was effective
drop table if exists ukg_mig.hn_tech_pto cascade;
create table ukg_mig.hn_tech_pto as
select *,
  (
		select min(from_date) as cur_rate_eff_date
		from hs.main_shop_flat_rate_techs
		where employee_number = x.employee_number
		  and pto_rate = x.pto_rate)
from (
	select aa.employee_number,
		(
			select pto_rate
			from hs.main_shop_flat_rate_techs
			where thru_date > current_date
				and employee_number = aa.employee_number)
	from (  
		select distinct a.employee_number 
		from hs.main_shop_flat_rate_techs a
		join ukg_mig.pto_employees b on a.employee_number = b.employee_number
			and b.cost_center = 'Rydell Honda Nissan/Main Shop/Main Shop Technician') aa) x

---------------------------------------------------------------------
--/> hn flat rate
---------------------------------------------------------------------

  
-- detail, don't know where the pto rate is stored, does not appear to be pymast
select * 
from ukg_mig.pto_employees a
where a.cost_center like '%detail%'


---------------------------------------------------------------------
--< sales consultants
---------------------------------------------------------------------
select a.*, aa.*, b.from_date, b.thru_date, b.most_recent_anniv, b.pto_rate 
from ukg_mig.pto_employees a
join ukg_mig.pto_calc_dates aa on a.employee_number = aa.employee_number
left join sls.pto_intervals b on a.employee_number = b.employee_number
  and b.year_month = 202202
where a.cost_center like '%consultant%'

select * 
from sls.pto_intervals
limit 200

select * from ukg.employees where last_name = 'miller'
-- this is fucked up because of the incomplete earnings for the recently 
-- calculated values for pto_rate, eg, croaker 1/1/21 -> 12/31/21 no earnings in dealertrack past 12/18
select aa.*, row_number() over (partition by employee_number order by from_date desc) as seq 
from (
	select a.last_name, a.first_name, a.employee_number, b.pto_rate, min(b.from_date) as from_date, max(b.thru_date) as thru_date
	from ukg_mig.pto_employees a
	left join sls.pto_intervals b on a.employee_number = b.employee_number
	where a.cost_center like '%consultant%'
		and pto_rate <> 0
	group by a.last_name, a.first_name, a.employee_number, b.pto_rate) aa
order by aa.last_name, aa.first_name, seq

-- so a better way may be the from_date for the current rate
drop table if exists ukg_mig.sc cascade;
create table ukg_mig.sc as
select a.last_name, a.first_name, a.employee_number, a.current_pto_rate, b.pto_rate, max(b.from_date) as cur_rate_eff_date
from ukg_mig.pto_employees a		
left join sls.pto_intervals b on a.employee_number = b.employee_number
  and a.current_pto_rate = b.pto_rate
where a.cost_center like '%consultant%' 
group by a.last_name, a.first_name, a.employee_number, a.current_pto_rate, b.pto_rate 
order by a.last_name, a.first_name 

-- these are the months the most recent reates went into effect
select a.last_name, a.first_name, a.employee_number, a.current_pto_rate, b.pto_rate, min(b.year_month)
from ukg_mig.pto_employees a		
left join sls.pto_intervals b on a.employee_number = b.employee_number
  and a.current_pto_rate = b.pto_rate
where a.cost_center like '%consultant%' 
group by a.last_name, a.first_name, a.employee_number, a.current_pto_rate, b.pto_rate 
order by last_name


select a.last_name, a.first_name, a.employee_number, a.current_pto_rate, b.*
from ukg_mig.pto_employees a		
left join sls.pto_intervals b on a.employee_number = b.employee_number
--   and a.current_pto_rate = b.pto_rate
where a.cost_center like '%consultant%' 
  and last_name = 'croaker'
order by last_name, b.year_month 



---------------------------------------------------------------------
--/> sales consultants
---------------------------------------------------------------------

