﻿		select * 
		from (
			select a.last_name, a.first_name, a.employee_number, a.employee_id, c.display_name as pay_calc_profile, 
				d.store,  d.department, d.cost_center, a.manager_1, a.manager_2, coalesce(a.seniority_date, a.hire_date) as seniority_date,
				coalesce(e.current_rate, 0) as current_pto_rate,
				case
					when -- current year anniversary
						to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' ) < '06/04/2022'
							then to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' )
					else (to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' ) - interval '1 year')::date
				end as most_recent_anniv	
			from ukg.employees a
			join ukg.employee_profiles b on a.employee_id = b.employee_id
			join ukg.ext_pay_calc_profiles c on b.pay_calc_id = c.id
				and c.display_name = 'Full-Time Flat Rate'
			join (select * from ukg.get_cost_center_hierarchy()) d on a.cost_center = d.cost_center  
				and 
					case
						when a.store = 'GM' then d.store = 'Rydell GM'
						when a.store = 'HN' then d.store = 'Rydell Honda Nissan'
					end
			left join ukg.personal_rate_tables e on b.rate_table_id = e.rate_table_id
				and e.counter = 'paid time off'
			where a.status = 'active') s
-- 		where seniority_date <> most_recent_anniv -- excludes employees with less than a year employment
		where 
			case
				when seniority_date = most_Recent_anniv then current_Date - seniority_date > 330
				else seniority_date <> most_Recent_anniv
			end
-- where seniority_date = most_recent_anniv 
order by extract(month from seniority_date), extract(day from seniority_date)	