﻿select * 
from sls.pto_intervals
where year_month = 202202



select b.last_name, b.first_name, b.employee_id, b.hire_date, b.seniority_date, a.*
from sls.pto_interval_dates a
left join ukg.employees b on a.employee_number = b.employee_number
where a.year_month = 202202
order by a.most_recent_anniversary


-- PtoIntervalsDates first part
                    select count(*)
                    from ( --c: personnel assigned to teams in open month
                      select a.*
                      from sls.personnel a
                      inner join sls.team_personnel b  on a.employee_number = b.employee_number
                      where b.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
                        and thru_date >
                            (select last_day_of_previous_month from sls.months where open_closed = 'open')) c
                    left join sls.pto_intervals d on c.employee_number = d.employee_number
                      and d.year_month = (select year_month from sls.months where open_closed = 'open')
                    where d.employee_number is null;
                    
-- PtoIntervalsDates create rows in sls.pto_interval_dates
                    select year_month, employee_number, anniversary_date, most_recent_anniversary,
                      case
                        when anniversary_date = most_recent_anniversary then anniversary_date
                      else (
                        select first_of_month
                        from sls.months
                        WHERE seq = (
                          select seq - 12
                          from sls.months
                          where v.most_recent_anniversary between first_of_month and last_of_month))
                      end as pto_period_from,
                      case
                        when anniversary_date = most_recent_anniversary then anniversary_date
                      else (
                        select last_of_month
                        from sls.months
                        WHERE seq = (
                          select seq -1
                          from sls.months
                          where v.most_recent_anniversary between first_of_month and last_of_month))
                      END as pto_period_thru
                    from ( -- v: 1 row per emp with most recent anniversary and no row in pto_intervals for open month
                      select (select year_month from sls.months where open_closed = 'open'),
                        last_name, first_name, employee_number, anniversary_date,
                        case -- most recent anniversary
                          when anniversary_date = to_date then anniversary_date
                          when to_date < current_date then to_date
                          when to_date > current_date then (to_date - interval '1 year'):: date
                        end as most_recent_anniversary
                      from (-- u: 1 row per emp with anniv date for cur year,  no row in pto_intervals for open month
                        select c.*,
                          to_date(extract(year from current_date)::text ||'-'||
                            extract(month from c.anniversary_date)::text
                            ||'-'|| extract(day from c.anniversary_date)::text, 'YYYY MM DD' ) -- current yr anniv date
                        from ( --c: personnel assigned to teams in open month
                          select a.*
                          from sls.personnel a
                          inner join sls.team_personnel b  on a.employee_number = b.employee_number
                          where b.from_Date <
                            (select first_day_of_next_month from sls.months where open_closed = 'open')
                            and thru_date >
                                (select last_day_of_previous_month from sls.months where open_closed = 'open')) c
                        left join sls.pto_intervals d on c.employee_number = d.employee_number
                          and d.year_month = (select year_month from sls.months where open_closed = 'open')
                        where d.employee_number is null) u) v;                    

-- PtoIntervals
--                     insert into sls.pto_intervals (employee_number,year_month,from_date,thru_date,
--                         most_recent_anniv,pto_rate)
                    select c.employee_number, c.year_month, c.from_date, c.thru_date, c.most_recent_anniversary,
                      coalesce(x.pto_rate, 0) as pto_rate
                    from sls.pto_interval_dates c
                    left join sls.pto_intervals d on c.employee_number = d.employee_number
                      and d.year_month = (select year_month from sls.months where open_closed = 'open')
                    left join (
                      select employee_number, year_month, total_gross,
                        -- 4.333 weeks/month, 5 working days/month, 8 hrs/day
                        round(total_gross/(4.333 * adj_count/2)/5/8, 2) as pto_rate, adj_count
                      from  (
                        select a.employee_number, a.year_month, sum(b.total_gross_pay) as total_gross,
                          count(*),
                          case
                            when count(*) >= 24 then 24
                            else count(*)
                          end as adj_count
                        from sls.pto_interval_dates a
                        left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
                          and (b.check_month || '-' || b.check_day || '-' || (2000 + b.check_year))::date
                            between a.from_date and a.thru_date
                        where a.from_date <> a.thru_date
                          and b.seq_void = '00'
                        group by a.employee_number, a.year_month) e) x on  c.employee_number = x.employee_number
                    where d.employee_number is null;           


                    select y.last_name, y.first_name, c.employee_number, c.year_month, c.from_date, c.thru_date, c.most_recent_anniversary,
                      coalesce(x.pto_rate, 0) as pto_rate,
                      c.thru_date - c.from_date
                    from sls.pto_interval_dates c
                    left join sls.pto_intervals d on c.employee_number = d.employee_number
                      and d.year_month = (select year_month from sls.months where open_closed = 'open')
                    left join (
                      select employee_number, year_month, total_gross,
                        -- 4.333 weeks/month, 5 working days/month, 8 hrs/day
                        round(total_gross/(4.333 * adj_count/2)/5/8, 2) as pto_rate, adj_count
                      from  (
                        select a.employee_number, a.year_month, sum(b.total_gross_pay) as total_gross,
                          count(*),
                          case
                            when count(*) >= 24 then 24
                            else count(*)
                          end as adj_count
                        from sls.pto_interval_dates a
                        left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
                          and (b.check_month || '-' || b.check_day || '-' || (2000 + b.check_year))::date
                            between a.from_date and a.thru_date
                        where a.from_date <> a.thru_date
                          and b.seq_void = '00'
                        group by a.employee_number, a.year_month) e) x on  c.employee_number = x.employee_number
left join ukg.employees y on c.employee_number = y.employee_number    
order by thru_date               




drop table if exists jon.earnings cascade;
create table jon.earnings as
-- 2021 earnings from dealertrack
select 'arkona' as source, employee_ as employee_number, (check_month || '-' || check_day || '-' || (2000 + check_year))::date as check_date, sum(total_gross_pay) as gross
from arkona.ext_pyhshdta a
where check_year = 21
  and total_gross_pay <> 0
group by employee_, (check_month || '-' || check_day || '-' || (2000 + check_year))::date
union
-- earnings from ukg
select 'ukg', d.employee_number, a.pay_date, sum(c.ee_amount) as gross
from ukg.payrolls a
join ukg.pay_statements b on a.payroll_id = b.payroll_id
join ukg.pay_statement_earnings c on a.payroll_id = c.payroll_id
  and b.pay_statement_id = c.pay_statement_id
join ukg.employees d on c.employee_account_id = d.employee_id
group by d.employee_number, a.pay_date
having sum(c.ee_amount) <> 0
order by employee_number, check_date;
alter table jon.earnings
add primary key(employee_number, check_date);

                    select f.last_name, f.first_name, c.employee_number, c.year_month, c.from_date, c.thru_date, c.most_recent_anniversary,
--                       coalesce(x.pto_rate, 0) as pto_rate,
                      c.thru_date - c.from_date + 1, e.check_date, e.gross, e.source
                    from sls.pto_interval_dates c
                    left join sls.pto_intervals d on c.employee_number = d.employee_number
                      and d.year_month = (select year_month from sls.months where open_closed = 'open')
                    left join jon.earnings e on c.employee_number = e.employee_number
                      and e.check_date between c.from_date and c.thru_date
                    left join ukg.employees f on c.employee_number = f.employee_number
                    order by c.employee_number, e.check_date


select '02/10/2022'::Date - '02/01/2022'::date              


so far, what are all the earnings codes for sales
-- select distinct earning_code, earning_name, type_name
select b.last_name, b.first_name, b.employee_number, b.employee_id, a.pto_rate, c.*
from sls.pto_intervals a
join ukg.employees b on a.employee_number = b.employee_number
left join ukg.pay_statement_earnings c on b.employee_id = c.employee_account_id
where a.year_month = 202202
  and ee_amount <> 0
  and (c.cost_center_display_name like 'Rydell GM/Sales/%' or c.cost_center_display_name like 'Rydell Honda Nissan/Sales/%')
order by earning_code


-------------------------------------------------------------------------------
--< 02/09/22
-------------------------------------------------------------------------------
from jeris initial doc on calculating pto rates
specifies ukg earnings codes to exempt:
•	Benefit Allowance
•	Deferred Comp
•	Employee Referral Bonus
•	Expense Reimbursement
•	Gift Card
•	Holiday
•	Lease Program, Lease Program 2, Lease Program 3, Lease Program 4, Lease Program 5
•	Moving Expenses
•	Non-Cash Wages
•	OnStar Commission
•	Other Discretionary Bonus
•	Paid Time Off
•	PTO Payout
•	Severance
•	Sign-On Bonus
•	Tool Allowance
•	Vision Award
•	Volunteer PTO

From what i can see in dealertrack, just leave out Lease, PTO, Holiday, PTO Payout


select 'arkona' as source, employee_ as employee_number, (check_month || '-' || check_day || '-' || (2000 + check_year))::date as check_date, sum(total_gross_pay) as gross
from arkona.ext_pyhshdta a
where check_year = 21
  and total_gross_pay <> 0
group by employee_, (check_month || '-' || check_day || '-' || (2000 + check_year))::date

------------------------------------------------------------------------------------
--< 2021 dealertrack earnings for sales
------------------------------------------------------------------------------------
-- rough sales consultants (also has some managers)
drop table if exists sales_employees;
create temp table sales_employees as
select a.*
from sls.personnel a
inner join sls.team_personnel b  on a.employee_number = b.employee_number
where b.from_Date <
	(select first_day_of_next_month from sls.months where open_closed = 'open')
	and thru_date >
			(select last_day_of_previous_month from sls.months where open_closed = 'open');

-- first identify the pay codes
select a.last_name, a.first_name, a.employee_number, c.code_type, c.code_id, c.description, sum(c.amount) as amount
from sales_employees a
join arkona.ext_pyhshdta b on a.employee_number = b.employee_
  and b.check_year = 21
  and b.total_gross_pay <> 0
join arkona.ext_pyhscdta c on b.company_number = c.company_number
  and b.payroll_run_number = c.payroll_run_number 
  and a.employee_number = c.employee_number
  and c.code_type = '1'
  -- include A-Z Sales F&I, COMMISSIONS,DRAWS','PULSE PAY','COVID 19','SPIFF PAY OUTS'
  -- exclude LEASE PROGRAM,PAY OUT PTO,'SALES VACATION'
  and code_id in ('79A','79','78','c19','79B','82')  
group by a.last_name, a.first_name, a.employee_number, c.code_type, c.code_id, c.description  
order by c.description

-- now i want check dates
select a.last_name, a.first_name, a.employee_number, (check_month || '-' || check_day || '-' || (2000 + check_year))::date as check_date, sum(c.amount) as earnings
from sales_employees a
join arkona.ext_pyhshdta b on a.employee_number = b.employee_
  and b.check_year = 21
  and b.total_gross_pay <> 0
join arkona.ext_pyhscdta c on b.company_number = c.company_number
  and b.payroll_run_number = c.payroll_run_number 
  and a.employee_number = c.employee_number
  and c.code_type = '1'
  -- include A-Z Sales F&I, COMMISSIONS,DRAWS','PULSE PAY','COVID 19','SPIFF PAY OUTS'
  -- exclude LEASE PROGRAM,PAY OUT PTO,'SALES VACATION'
  and code_id in ('79A','79','78','c19','79B','82')  
group by a.last_name, a.first_name, a.employee_number, (check_month || '-' || check_day || '-' || (2000 + check_year))::date
order by last_name, first_name, check_date
------------------------------------------------------------------------------------
--/> 2021 dealertrack earnings for sales
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
--< 2021 dealertrack earnings for flat rate
------------------------------------------------------------------------------------
-- rough sales consultants (also has some managers)
drop table if exists flat_rate_employees;
create temp table flat_rate_employees as
select distinct a.last_name, a.first_name, a.employee_number
from tp.techs a
join tp.team_Techs aa on a.tech_key = aa.tech_key
  and aa.thru_date > current_date 
left join tp.tech_values b on a.tech_key = b.tech_key
where a.thru_date > current_date 
union
select b.last_name, b.first_name, a.employee_number 
from hs.main_shop_flat_rate_techs a
join ukg.employees b on a.employee_number = b.employee_number
where thru_date > current_date;

-- first identify the pay codes
select a.last_name, a.first_name, a.employee_number, c.code_type, c.code_id, c.description, sum(c.amount) as amount
from flat_rate_employees a
join arkona.ext_pyhshdta b on a.employee_number = b.employee_
  and b.check_year = 21
  and b.total_gross_pay <> 0
join arkona.ext_pyhscdta c on b.company_number = c.company_number
  and b.payroll_run_number = c.payroll_run_number 
  and a.employee_number = c.employee_number
  and c.code_type = '1'
  -- include commissions,covid,rate adjust, tech hours pay,tech xtra bonus,
  -- exclude holiday pay, health insurance,lease program, pay out pto,vac/pto pay,
--   and code_id in ('79A','79','78','c19','79B','82')  
   and code_id in ('79','C19','cov','75','87','70')
group by a.last_name, a.first_name, a.employee_number, c.code_type, c.code_id, c.description  
order by c.description

-- now i want check dates
select a.last_name, a.first_name, a.employee_number, (check_month || '-' || check_day || '-' || (2000 + check_year))::date as check_date, sum(c.amount) as earnings
from flat_rate_employees a
join arkona.ext_pyhshdta b on a.employee_number = b.employee_
  and b.check_year = 21
  and b.total_gross_pay <> 0
join arkona.ext_pyhscdta c on b.company_number = c.company_number
  and b.payroll_run_number = c.payroll_run_number 
  and a.employee_number = c.employee_number
  and c.code_type = '1'
   and code_id in ('79','C19','cov','75','87','70')
group by a.last_name, a.first_name, a.employee_number, (check_month || '-' || check_day || '-' || (2000 + check_year))::date
order by last_name, first_name, check_date
------------------------------------------------------------------------------------
--/> 2021 dealertrack earnings for flat rate
------------------------------------------------------------------------------------


-- most recen anniv
-- daniel gonzalez is wrong in sls.pto_intervals, seems to have picked up the start date in sales rather than hire date
select a.*, ((most_recent_anniv + interval '1 year')::date) - 1 as thru_date, most_recent_anniv - seniority_date
from (
	select a.last_name, a.first_name, a.employee_number, coalesce(a.seniority_date, a.hire_date) as seniority_date,
		case
			when -- current year anniversary
				to_date(extract(year from current_date)::text ||'-'|| extract(month from coalesce(seniority_date, hire_date))::text ||'-'|| extract(day from coalesce(seniority_date, hire_date))::text, 'YYYY MM DD' ) < current_date 
					then to_date(extract(year from current_date)::text ||'-'|| extract(month from coalesce(seniority_date, hire_date))::text ||'-'|| extract(day from coalesce(seniority_date, hire_date))::text, 'YYYY MM DD' )
			else (to_date(extract(year from current_date)::text ||'-'|| extract(month from coalesce(seniority_date, hire_date))::text ||'-'|| extract(day from coalesce(seniority_date, hire_date))::text, 'YYYY MM DD' ) - interval '1 year')::date
		end as most_recent_anniv, cost_center
	from ukg.employees a
	join flat_rate_employees b on a.employee_number = b.employee_number
	where status = 'active') a
union
select a.*, ((most_recent_anniv + interval '1 year')::date) - 1 as thru_date, most_recent_anniv - seniority_date
from (
	select a.last_name, a.first_name, a.employee_number, coalesce(a.seniority_date, a.hire_date) as seniority_date,
		case
			when -- current year anniversary
				to_date(extract(year from current_date)::text ||'-'|| extract(month from coalesce(seniority_date, hire_date))::text ||'-'|| extract(day from coalesce(seniority_date, hire_date))::text, 'YYYY MM DD' ) < current_date 
					then to_date(extract(year from current_date)::text ||'-'|| extract(month from coalesce(seniority_date, hire_date))::text ||'-'|| extract(day from coalesce(seniority_date, hire_date))::text, 'YYYY MM DD' )
			else (to_date(extract(year from current_date)::text ||'-'|| extract(month from coalesce(seniority_date, hire_date))::text ||'-'|| extract(day from coalesce(seniority_date, hire_date))::text, 'YYYY MM DD' ) - interval '1 year')::date
		end as most_recent_anniv, cost_center
	from ukg.employees a
	join sales_employees b on a.employee_number = b.employee_number
	where status = 'active') a	
order by most_recent_anniv desc 

biggest issue for me right now is, to who all will this apply??????
from calculated pto rate v2.docx: 
INCLUDED PAY CALCULATION PROFILES
•	Full-Time Flat Rate
•	Full-Time Sales Commission Only  

these queries need to be reconfigured using the pay calc profiles

-------------------------------------------------------------------------------
--/> 02/09/22
-------------------------------------------------------------------------------