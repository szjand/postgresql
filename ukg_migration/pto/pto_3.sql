﻿-- 
-- select a.last_name, a.first_name, a.employee_number, a.employee_id, c.display_name as pay_calc_profile, 
-- 	d.store||'/'|| d.department||'/'|| d.cost_center as cost_center
-- from ukg.employees a
-- join ukg.employee_profiles b on a.employee_id = b.employee_id
-- join ukg.ext_pay_calc_profiles c on b.pay_calc_id = c.id
--   and c.display_name in ('Full-Time Flat Rate','Full-Time Sales Commission Only')
-- join (select * from ukg.get_cost_center_hierarchy()) d on a.cost_center = d.cost_center  
--   and 
--     case
--       when a.store = 'GM' then d.store = 'Rydell GM'
--       when a.store = 'HN' then d.store = 'Rydell Honda Nissan'
--     end
-- where a.status = 'active'
-- order by d.store, d.department, c.display_name, d.cost_center, a.last_name;
-- 



-- include current pto_rate, seniority date
drop table if exists pto_employees cascade;
create temp table pto_employees as
select a.last_name, a.first_name, a.employee_number, a.employee_id, c.display_name as pay_calc_profile, 
	d.store||'/'|| d.department||'/'|| d.cost_center as cost_center, coalesce(a.seniority_date, a.hire_date) as seniority_date,
	e.current_rate as current_pto_rate
from ukg.employees a
join ukg.employee_profiles b on a.employee_id = b.employee_id
join ukg.ext_pay_calc_profiles c on b.pay_calc_id = c.id
  and c.display_name in ('Full-Time Flat Rate','Full-Time Sales Commission Only')
join (select * from ukg.get_cost_center_hierarchy()) d on a.cost_center = d.cost_center  
  and 
    case
      when a.store = 'GM' then d.store = 'Rydell GM'
      when a.store = 'HN' then d.store = 'Rydell Honda Nissan'
    end
left join ukg.personal_rate_tables e on b.rate_table_id = e.rate_table_id
  and e.counter = 'paid time off'
where a.status = 'active'
order by d.store, d.department, c.display_name, d.cost_center, a.last_name;

select * from pto_employees

drop table if exists base_earnings cascade;
create temp table base_earnings as
-- dealer track 2021 earnings
select a.employee_number, (check_month || '-' || check_day || '-' || (2000 + check_year))::date as check_date, 
  sum(b.base_pay + c.amount) as earnings -- needed base pay for some techs sum(c.amount) as earnings
from pto_employees a
join arkona.ext_pyhshdta b on a.employee_number = b.employee_
  and b.check_year in (20, 21)
  and b.total_gross_pay <> 0
join arkona.ext_pyhscdta c on b.company_number = c.company_number
  and b.payroll_run_number = c.payroll_run_number 
  and a.employee_number = c.employee_number
  and c.code_type = '1'
  and 
  -- flat rate:
    -- include commissions,covid,rate adjust, tech hours pay,tech xtra bonus,
		-- exclude holiday pay, health insurance,lease program, pay out pto,vac/pto pay
  -- sales:
		-- include A-Z Sales F&I, COMMISSIONS,DRAWS','PULSE PAY','COVID 19','SPIFF PAY OUTS'
		-- exclude LEASE PROGRAM,PAY OUT PTO,'SALES VACATION'
    case 
      when a.pay_calc_profile = 'Full-Time Sales Commission Only' then c.code_id in ('79A','79','78','c19','79B','82')  
      when a.pay_calc_profile = 'Full-Time Flat Rate' then c.code_id in ('79','C19','cov','75','87','70')
    end
group by a.employee_number, (check_month || '-' || check_day || '-' || (2000 + check_year))::date
union all
-- ukg earnings
select a.employee_number, c.pay_date, sum(b.ee_amount) as earnings
from pto_employees a
join ukg.pay_statement_earnings b on a.employee_id = b.employee_account_id
  and coalesce(b.ee_amount, 0) <> 0
join ukg.payrolls c on b.payroll_id = c.payroll_id
-- where b.earning_code <> b.earning_name
where b.earning_name not in (
		'Benefit Allowance','Deferred Comp','Employee Referral Bonus','Expense Reimbursement','Gift Card',
		'Holiday','Lease Program', 'Lease Program 2', 'Lease Program 3', 'Lease Program 4', 'Lease Program 5',
		'Moving Expenses','Non-Cash Wages','OnStar Commission','Other Discretionary Bonus',
		'Owner Health Insurance Premiums Paid','Paid Time Off','PTO Payout','Severance','Sign-On Bonus',
		'Tool Allowance','Vision Award','Volunteer PTO','Year End Manager Bonus','Year End Owner Bonus',
		'2021 401K Hours')  -- just the 2 anomalies with amounts
and b.earning_code not in (
		'Benefit Allowance','Deferred Comp','Employee Referral Bonus','Expense Reimbursement','Gift Card',
		'Holiday','Lease Program', 'Lease Program 2', 'Lease Program 3', 'Lease Program 4', 'Lease Program 5',
		'Moving Expenses','Non-Cash Wages','OnStar Commission','Other Discretionary Bonus',
		'Owner Health Insurance Premiums Paid','Paid Time Off','PTO Payout','Severance','Sign-On Bonus',
		'Tool Allowance','Vision Award','Volunteer PTO','Year End Manager Bonus','Year End Owner Bonus')	
group by a.employee_number, c.pay_date;

select * from base_earnings

-- -- ukg earnings inspection and troubleshooting
-- select a.*, b.earning_code, b.earning_name, b.ee_amount, c.pay_date, b.*
-- -- select distinct earning_code, earning_name
-- -- select a.last_name, a.first_name, a.employee_number, c.pay_date, b.ee_amount
-- from pto_employees a
-- join ukg.pay_statement_earnings b on a.employee_id = b.employee_account_id
--   and coalesce(b.ee_amount, 0) <> 0
-- join ukg.payrolls c on b.payroll_id = c.payroll_id
-- -- where b.earning_code <> b.earning_name
-- where b.earning_name not in (
-- 		'Benefit Allowance','Deferred Comp','Employee Referral Bonus','Expense Reimbursement','Gift Card',
-- 		'Holiday','Lease Program', 'Lease Program 2', 'Lease Program 3', 'Lease Program 4', 'Lease Program 5',
-- 		'Moving Expenses','Non-Cash Wages','OnStar Commission','Other Discretionary Bonus',
-- 		'Owner Health Insurance Premiums Paid','Paid Time Off','PTO Payout','Severance','Sign-On Bonus',
-- 		'Tool Allowance','Vision Award','Volunteer PTO','Year End Manager Bonus','Year End Owner Bonus',
-- 		'2021 401K Hours')  -- just the 2 anomalies with amounts
-- and b.earning_code not in (
-- 		'Benefit Allowance','Deferred Comp','Employee Referral Bonus','Expense Reimbursement','Gift Card',
-- 		'Holiday','Lease Program', 'Lease Program 2', 'Lease Program 3', 'Lease Program 4', 'Lease Program 5',
-- 		'Moving Expenses','Non-Cash Wages','OnStar Commission','Other Discretionary Bonus',
-- 		'Owner Health Insurance Premiums Paid','Paid Time Off','PTO Payout','Severance','Sign-On Bonus',
-- 		'Tool Allowance','Vision Award','Volunteer PTO','Year End Manager Bonus','Year End Owner Bonus')		
-- -- order by earning_code

-- -- earning code <> earning name
-- select distinct earning_code, md5(earning_code), length(earning_code), earning_name, md5(earning_name), length(earning_name)
-- from ukg.employees a
-- join ukg.pay_statement_earnings b on a.employee_id = b.employee_account_id
--   and coalesce(b.ee_amount, 0) <> 0
-- join ukg.payrolls c on b.payroll_id = c.payroll_id
-- where b.earning_code <> b.earning_name		


-- -- most recen anniv
-- -- daniel gonzalez is wrong in sls.pto_intervals, seems to have picked up the start date in sales rather than hire date
-- drop table if exists most_recent_anniv cascade;
-- create temp table most_recent_anniv as
-- select a.*, ((most_recent_anniv + interval '1 year')::date) - 1 as thru_date, most_recent_anniv - seniority_date as days_emp
-- from (
-- 	select a.last_name, a.first_name, a.employee_number, b.seniority_date,
-- 		case
-- 			when -- current year anniversary
-- 				to_date(extract(year from current_date)::text ||'-'|| extract(month from coalesce(b.seniority_date, hire_date))::text ||'-'|| extract(day from coalesce(seniority_date, hire_date))::text, 'YYYY MM DD' ) < current_date 
-- 					then to_date(extract(year from current_date)::text ||'-'|| extract(month from coalesce(seniority_date, hire_date))::text ||'-'|| extract(day from coalesce(seniority_date, hire_date))::text, 'YYYY MM DD' )
-- 			else (to_date(extract(year from current_date)::text ||'-'|| extract(month from coalesce(seniority_date, hire_date))::text ||'-'|| extract(day from coalesce(seniority_date, hire_date))::text, 'YYYY MM DD' ) - interval '1 year')::date
-- 		end as most_recent_anniv
-- 	from ukg.employees a
-- 	join pto_employees b on a.employee_number = b.employee_number
-- 	where status = 'active') a;

drop table if exists pto_calc_dates cascade;
create temp table pto_calc_dates as
select *, (a.most_recent_anniv - interval '1 year')::date as rate_calc_from_date, most_recent_anniv - 1 as rate_calc_thru_date
from (
	select employee_number, 
		case
			when -- current year anniversary
				to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' ) < current_date 
					then to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' )
			else (to_date(extract(year from current_date)::text ||'-'|| extract(month from seniority_date)::text ||'-'|| extract(day from seniority_date)::text, 'YYYY MM DD' ) - interval '1 year')::date
		end as most_recent_anniv
	from pto_employees) a;


-- getting there, here are the employees, current pto reate, most recent anniv
-- select a.*, c.current_rate as current_pto_rate, seniority_date, most_recent_anniv , thru_date
-- from pto_employees a
-- join ukg.employee_profiles b on a.employee_id = b.employee_id
-- left join ukg.personal_rate_tables c on b.rate_table_id = c.rate_table_id
--   and c.counter = 'paid time off'
-- left join (
-- 	select a.*, ((most_recent_anniv + interval '1 year')::date) - 1 as thru_date, most_recent_anniv - seniority_date as days_emp
-- 	from (
-- 		select a.last_name, a.first_name, a.employee_number, coalesce(a.seniority_date, a.hire_date) as seniority_date,
-- 			case
-- 				when -- current year anniversary
-- 					to_date(extract(year from current_date)::text ||'-'|| extract(month from coalesce(seniority_date, hire_date))::text ||'-'|| extract(day from coalesce(seniority_date, hire_date))::text, 'YYYY MM DD' ) < current_date 
-- 						then to_date(extract(year from current_date)::text ||'-'|| extract(month from coalesce(seniority_date, hire_date))::text ||'-'|| extract(day from coalesce(seniority_date, hire_date))::text, 'YYYY MM DD' )
-- 				else (to_date(extract(year from current_date)::text ||'-'|| extract(month from coalesce(seniority_date, hire_date))::text ||'-'|| extract(day from coalesce(seniority_date, hire_date))::text, 'YYYY MM DD' ) - interval '1 year')::date
-- 			end as most_recent_anniv, a.cost_center
-- 		from ukg.employees a
-- 		join pto_employees b on a.employee_number = b.employee_number
-- 		where status = 'active') a) d on a.employee_number = d.employee_number  
-- order by most_recent_anniv	desc 

-- -- need previous year dates for calculating hours and earnings
-- drop table if exists pto_intervals;
-- create temp table pto_intervals as
-- select a.last_name, a.first_name, a.employee_number, a.seniority_date, 
--   a.most_recent_anniv, (a.most_recent_anniv - interval '1 year')::date as from_date, most_recent_anniv - 1 as thru_date
-- from most_recent_anniv a;

-- -- need hours from dealertrack
-- select min(the_date), max(the_date)  -- 12/01/21 ->
-- from ukg.clock_hours;
-- 
-- select min(the_date), max(the_date) -- 07/16/2009 -> 01/21/2022
-- from arkona.xfm_pypclockin
-- 
-- -- the conclusion from this query:
-- --  01/01/2021 -> 12/18/2021  arkona
-- --  12/19/2021 ->   ukg
-- select * 
-- from (
-- 	select the_date, sum(clock_hours) as clock_hours, sum(pto_hours) as pto_hours, sum(hol_hours) as hol_hours 
-- 	from ukg.clock_hours
-- 	group by the_date) a
-- full outer join (
-- 	select the_date, sum(clock_hours) as clock_hours, sum(vac_hours + pto_hours) as pto_hours, sum(hol_hours) as hol_hours 
-- 	from arkona.xfm_pypclockin
-- 	where the_date between '12/01/2021' and '01/21/2022'
-- 	group by the_date) b on a.the_date = b.the_date
-- order by coalesce(a.the_date, b.the_date);


drop table if exists clock_hours;
create temp table clock_hours as
select a.employee_number, a.the_date, sum(a.clock_hours) as clock_hours, sum(a.pto_hours) as pto_hours, sum(a.hol_hours) as hol_hours
from ukg.clock_hours a
join pto_employees b on a.employee_number = b.employee_number
where a.the_date > '12/18/2021'
group by a.employee_number, a.the_date
union
select a.employee_number, a.the_date, sum(a.clock_hours) as clock_hours, sum(a.vac_hours + a.pto_hours) as pto_hours, sum(a.hol_hours) as hol_hours 
from arkona.xfm_pypclockin a
join pto_employees b on a.employee_number = b.employee_number
where a.the_date between '01/01/2020' and '12/18/2021'
group by a.employee_number, a.the_date;


pto_employees
base_earnings
select * from pto_calc_dates
select * from clock_hours

select e.*, 
  round (
			case
			when total_hours = 0 then 0
			else earnings/total_hours
		end, 2) calc_pto_rate,
		round (
				case
				when total_hours = 0 then 0
				else earnings/total_hours
			end, 2) - current_pto_rate
from (
	select a.* , b.most_recent_anniv, b.rate_calc_from_date, b.rate_calc_thru_date, c.earnings,
		d.clock_hours, d.pto_hours, d.hol_hours,
		case
			when a.seniority_date = b.most_recent_anniv then 0
			when a.pay_calc_profile = 'Full-Time Sales Commission Only' then 2080 - coalesce(pto_hours, 0) - coalesce(hol_hours, 0) 
			when a.pay_calc_profile = 'Full-Time Flat Rate' then clock_hours - coalesce(pto_hours, 0) - coalesce(hol_hours, 0)  
		end as total_hours
	from pto_employees a
	left join pto_calc_dates b on a.employee_number = b.employee_number
	left join (
		select a.employee_number, sum(c.earnings) as earnings
		from pto_employees a
		left join pto_calc_dates b on a.employee_number = b.employee_number
		left join base_earnings c on a.employee_number = c.employee_number
			and c.check_date between b.rate_calc_from_date and b.rate_calc_thru_date
		group by a.employee_number) c on a.employee_number = c.employee_number
	left join (
		select a.employee_number, sum(c.clock_hours) as clock_hours, sum(c.pto_hours) as pto_hours, sum(c.hol_hours) as hol_hours
		from pto_employees a
		left join pto_calc_dates b on a.employee_number = b.employee_number
		left join clock_hours c on a.employee_number = c.employee_number
			and c.the_date between b.rate_calc_from_date and b.rate_calc_thru_date
		group by a.employee_number) d on a.employee_number = d.employee_number) e
order by cost_center, pay_calc_profile, last_name

-- select aa.*, bb.earnings, d.clock_hours, d.pto_hours, d.hol_hours
-- from (
-- select a.last_name, a.first_name, a.employee_number, a.seniority_date, 
--   a.most_recent_anniv, a.from_date, a.thru_date, sum(b.clock_hours) as clock_hours,
--   sum(b.pto_hours) as pto_hours, sum(b.hol_hours) as hol_hours
-- --   sum(c.earnings) as earnings
-- from pto_intervals a
-- left join clock_hours b on a.employee_number = b.employee_number
--   and b.the_date between a.from_date and a.thru_date
-- -- left join base_earnings c on a.employee_number = c.employee_number
-- --   and c.check_date between a.from_date and a.thru_date
-- group by a.last_name, a.first_name, a.employee_number, a.seniority_date, 
--   a.most_recent_anniv, a.from_date, a.thru_date ) aa
-- left join (
-- select a.last_name, a.first_name, a.employee_number, a.seniority_date, 
--   a.most_recent_anniv, a.from_date, a.thru_date,
--   sum(c.earnings) as earnings
-- from pto_intervals a
-- left join base_earnings c on a.employee_number = c.employee_number
--   and c.check_date between a.from_date and a.thru_date
-- where seniority_date <> most_recent_anniv    
-- group by a.last_name, a.first_name, a.employee_number, a.seniority_date, 
--   a.most_recent_anniv, a.from_date, a.thru_date) bb on aa.employee_number = bb.employee_number   

-- select b.last_name, b.first_name, a.* 
-- from sls.pto_intervals a
-- join pto_employees b on a.employee_number = b.employee_number
-- order by employee_number, year_month

select a.employee_number, (check_month || '-' || check_day || '-' || (2000 + check_year))::date as check_date, sum(b.base_pay + c.amount) as earnings
from pto_employees a
join arkona.ext_pyhshdta b on a.employee_number = b.employee_
  and b.check_year in (20, 21)
  and b.total_gross_pay <> 0
join arkona.ext_pyhscdta c on b.company_number = c.company_number
  and b.payroll_run_number = c.payroll_run_number 
  and a.employee_number = c.employee_number
  and c.code_type = '1'
  and 
  -- flat rate:
    -- include commissions,covid,rate adjust, tech hours pay,tech xtra bonus,
		-- exclude holiday pay, health insurance,lease program, pay out pto,vac/pto pay
  -- sales:
		-- include A-Z Sales F&I, COMMISSIONS,DRAWS','PULSE PAY','COVID 19','SPIFF PAY OUTS'
		-- exclude LEASE PROGRAM,PAY OUT PTO,'SALES VACATION'
    case 
      when a.pay_calc_profile = 'Full-Time Sales Commission Only' then c.code_id in ('79A','79','78','c19','79B','82')  
      when a.pay_calc_profile = 'Full-Time Flat Rate' then c.code_id in ('79','C19','cov','75','87','70')
    end
where a.employee_number = '195359'    
group by a.employee_number, (check_month || '-' || check_day || '-' || (2000 + check_year))::date  
order by (check_month || '-' || check_day || '-' || (2000 + check_year))::date  

  