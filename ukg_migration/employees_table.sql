﻿-------------------------------------------------------------------
-- employees
-------------------------------------------------------------------
1/25/22 added seniority_date to the employees table, will be used for establishing pto anniversary,
when there is a value for seniority_date it will supercede the hire_date for the pto anniversary

-- requires cost_centers
referencing python_projects/ukg/sql/api_cost_centers.sql looks like i have not figured out the schema yet
fuck
this table looks interesting
select * from ukg.cost_center_jobs includes the job catetgory and job name

select a.parent_id, a.name, b.job_category, b.job_name
from ukg.ext_cost_centers a
left join ukg.cost_center_jobs b on a.id = b.id


-- employees
select a.employee_id, a.employee_number, a.last_name, a.first_name, a.managers, a.cost_center_index, a.cost_center_id
from ukg.ext_employee_details a
where a.status = 'Active'


select distinct cost_Center_index from ukg.ext_employee_Details

select * from ukg.ext_employees where status = 'active'

select * from ukg.ext_employee_details where status = 'active'

select a.employee_id, a.employee_number, a.last_name, a.first_name, a.status, a.cost_center_id,  (b->'empl_ref'->>'account_id')::bigint as manager_id
from ukg.ext_employee_details a
left join jsonb_array_elements(a.managers) as b on true  
-- where a.status = 'Active'
where a.employee_number <> '1API'

select * from ukg.ext_cost_centers

drop table if exists ukg.employees cascade;
create table ukg.employees (
  store citext not null,
  last_name citext not null,
  first_name citext not null,
  middle_name citext,
  nick_name citext,
  employee_id bigint not null,
  primary_account_id bigint not null,
  employee_number citext not null,
  status citext not null,
  cost_center citext,
  manager_1 citext,
  manager_2 citext,
  primary_email citext,
  ssn citext,
  hire_date date not null,
  rehire_date date,
  start_date date,
  birth_date date not null,
  seniority_date date,
  term_date date not null,
	home_phone citext,
	cell_phone citext,
	work_phone citext,
  preferred_phone citext,
  address_line_1 citext,
  address_line_2 citext,
  address_line_3 citext,
  city citext,
  state citext,
  zip citext,
  primary key(employee_id));
  
create or replace function ukg.update_employees()
returns void as
$BODY$
/*
  this table should include ALL employees (like 1API) for now
  select ukg.update_employees();
*/
  truncate ukg.employees;
  insert into ukg.employees
	select 
		case
			when aa.ein_id = 101919425 then 'GM'
			when aa.ein_id = 101933376 then 'HN'
		end as store, 
		aa.last_name, aa.first_name, aa.middle_name, aa.nickname, aa.employee_id, aa.primary_account_id, 
		aa.employee_number, aa.status, cc.name as cost_center, 
		bb.manager_1, bb.manager_2,
		aa.primary_email, aa.social_security as ssn, 
		aa.hired_date as hire_date,aa.re_hired_date as rehire_date, 
		aa.started_date as start_date, aa.birthday as birth_date, aa.seniority_date, coalesce(aa.terminated_date, '12/31/9999') as term_date,
		aa.home_phone, aa.cell_phone, aa.work_phone, aa.preferred_phone,
		aa.address_line_1, aa.address_line_2, aa.address_line_3, aa.city, aa.state, aa.zip
  from ukg.ext_employee_details aa
  left join ( -- managers)
		select a.employee_id, c.last_name ||', '|| c.first_name as manager_1, e.last_name ||', '|| e.first_name as manager_2
		from ukg.ext_employee_details a
		left join jsonb_array_elements(a.managers) as b on b->>'index' = '0'
		left join ukg.ext_employee_details c on (b->'empl_ref'->>'account_id')::bigint = c.employee_id
		left join jsonb_array_elements(a.managers) as d on d->>'index' = '1'
		left join ukg.ext_employee_details e on (d->'empl_ref'->>'account_id')::bigint = e.employee_id) bb on aa.employee_id = bb.employee_id
  left join  ukg.ext_cost_centers cc on aa.cost_center_id = cc.id;
$BODY$
language sql;		


select *
from ukg.ext_employee_details
where seniority_date is not null
