﻿select b.the_date, a.employee_number, a.last_name, a.first_name, b.pto_hours, b.hol_hours
from sls.personnel a
join ukg.clock_hours b on a.employee_number = b.employee_number
  and b.the_date between '01/01/2022' and current_date
  and (pto_hours <> 0 or hol_hours <> 0)

 need pto pay, & holiday pay 

 select * 
 from luigi.luigi_log
 where pipeline = 'sales_consultant_payroll'
   and the_date = current_date
 order by from_ts


sls.clock_hours_by_month  is updated nightly in luigi with a call to fuction sls.update_clock_hours()

this is table ee in function sls.update_consultant_payroll()

sls.pto_intervals has to be maintained but only as a basis for updating ukg personal rate tables

select employee_number, pto_hours, hol_hours, coalesce(pto_rate, 0), hol_rate, 
  round(pto_hours * coalesce(pto_rate, 0), 2) as pto_pay, round(hol_hours * hol_rate, 2) as hol_pay
from (
	select aa.employee_number, aa.pto_hours, aa.holiday_hours as hol_hours, 
		case
			when aa.employee_number = '169645' then 17.35
			else cc.current_rate 
		end as pto_rate, dd.current_rate as hol_rate
	from sls.clock_hours_by_month aa
	left join (
		select a.last_name, a.first_name, a.employee_number, c.current_rate
		from ukg.employees a
		join ukg.employee_profiles b on a.employee_id = b.employee_id
		join ukg.personal_rate_tables c on b.rate_table_id = c.rate_table_id
			and c.counter = 'paid time off') cc on aa.employee_number = cc.employee_number  	
	left join (
		select a.last_name, a.first_name, a.employee_number, c.current_rate
		from ukg.employees a
		join ukg.employee_profiles b on a.employee_id = b.employee_id
		join ukg.personal_rate_tables c on b.rate_table_id = c.rate_table_id
			and c.counter = 'holiday') dd on aa.employee_number = dd.employee_number   
	where aa.year_month = 202201) cc -- (select year_month from open_month)) cc

-- final version in function
			left join ( -- ee
				select employee_number, pto_hours, hol_hours, coalesce(pto_rate, 0) as pto_rate, coalesce(hol_rate, 0) as hol_rate,
					round(pto_hours * coalesce(pto_rate, 0), 2) as pto_pay, round(hol_hours * hol_rate, 2) as hol_pay
				from (
					select aa.employee_number, aa.pto_hours, aa.holiday_hours as hol_hours, 
						case
							when aa.employee_number = '169645' then 17.35
							else cc.current_rate 
						end as pto_rate, dd.current_rate as hol_rate
					from sls.clock_hours_by_month aa
					left join (
						select a.last_name, a.first_name, a.employee_number, c.current_rate
						from ukg.employees a
						join ukg.employee_profiles b on a.employee_id = b.employee_id
						join ukg.personal_rate_tables c on b.rate_table_id = c.rate_table_id
							and c.counter = 'paid time off') cc on aa.employee_number = cc.employee_number  	
					left join (
						select a.last_name, a.first_name, a.employee_number, c.current_rate
						from ukg.employees a
						join ukg.employee_profiles b on a.employee_id = b.employee_id
						join ukg.personal_rate_tables c on b.rate_table_id = c.rate_table_id
							and c.counter = 'holiday') dd on aa.employee_number = dd.employee_number   
					where aa.year_month = (select year_month from open_month)) bb) ee on aa.employee_number = ee.employee_number) ff

now need to make room in the table for holiday data

alter table sls.consultant_payroll
add column hol_hours numeric(6,2),
add column hol_rate numeric(6,2),
add column hol_pay numeric(8,2);					

select * from sls.consultant_payroll where year_month = 202201