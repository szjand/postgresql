﻿
from FUNCTION sls.update_consultant_payroll():
        select a.employee_number, round(a.pto_hours, 2) as pto_hours, b.pto_rate, round(a.pto_hours * b.pto_rate, 2) as pto_pay
        from sls.clock_hours_by_month a
        left join sls.pto_intervals b on a.employee_number = b.employee_number
          and a.year_month = b.year_month
        where a.year_month = (select year_month from open_month)
          and a.pto_hours <> 0
          and b.pto_rate <> 0

table sls.clock_hours_by_month
  populated in luigi sc_payroll_201803.py ClockHoursByMonth()
-- not yet any 202201 data
select * 
-- delete
from sls.clock_hours_by_month
where year_month = 202201

-- this is the query in sc_payroll_201803.py
--                     insert into sls.clock_hours_by_month
select a.employee_number, c.year_month, sum(clock_hours) as clock_hours,
	sum(regular_hours) as regular_hours, sum(b.overtime_hours) as overtime_hours,
	sum(b.vacation_hours + b.pto_hours) as pto_hours,
	sum(b.holiday_hours) as holiday_hours
from (
		select c.team, a.employee_number, a.last_name, a.first_name,
			a.ry1_id, a.ry2_id, a.fi_id, d.payplan, a.is_senior
		from sls.personnel a
		inner join sls.team_personnel c on a.employee_number = c.employee_number
			and c.from_Date < (select first_day_of_next_month from sls.months
				where open_closed = 'open')
			and c.thru_date > (select last_day_of_previous_month from sls.months
				where open_closed = 'open')
		inner join sls.personnel_payplans d  on a.employee_number = d.employee_number
			and d.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
			and d.thru_date > (select last_day_of_previous_month from sls.months
				where open_closed = 'open')) a
inner join sls.xfm_fact_clock_hours b on a.employee_number = b.employee_number
inner join dds.dim_date c on b.the_date = c.the_date
where c.year_month = (select year_month from sls.months where open_closed = 'open')
	and b.clock_hours + b.vacation_hours + b.pto_hours + b.holiday_hours <> 0
group by c.year_month, a.employee_number

-- refactor using ukg.clock_hours
-- select * from ukg.clock_hours limit 10
insert into sls.clock_hours_by_month
select a.employee_number, c.year_month, sum(clock_hours) as clock_hours,
	sum(reg_hours) as regular_hours, sum(b.ot_hours) as overtime_hours,
	sum(b.pto_hours) as pto_hours,
	sum(b.hol_hours) as holiday_hours
from (
		select c.team, a.employee_number, a.last_name, a.first_name,
			a.ry1_id, a.ry2_id, a.fi_id, d.payplan, a.is_senior
		from sls.personnel a
		join sls.team_personnel c on a.employee_number = c.employee_number
			and c.from_Date < (select first_day_of_next_month from sls.months
				where open_closed = 'open')
			and c.thru_date > (select last_day_of_previous_month from sls.months
				where open_closed = 'open')
		join sls.personnel_payplans d  on a.employee_number = d.employee_number
			and d.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
			and d.thru_date > (select last_day_of_previous_month from sls.months
				where open_closed = 'open')) a
join ukg.clock_hours b on a.employee_number = b.employee_number
join dds.dim_date c on b.the_date = c.the_date
where c.year_month = (select year_month from sls.months where open_closed = 'open')
	and b.clock_hours + b.pto_hours + b.hol_hours <> 0
group by c.year_month, a.employee_number

create or replace function sls.update_clock_hours()
returns void as
$BODY$
/*
   select sls.update_clock_hours();
*/

	delete
	from sls.clock_hours_by_month
	where year_month = (select year_month from sls.months where open_closed = 'open');

	insert into sls.clock_hours_by_month
	select a.employee_number, c.year_month, sum(clock_hours) as clock_hours,
		sum(reg_hours) as regular_hours, sum(b.ot_hours) as overtime_hours,
		sum(b.pto_hours) as pto_hours,
		sum(b.hol_hours) as holiday_hours
	from (
			select c.team, a.employee_number, a.last_name, a.first_name,
				a.ry1_id, a.ry2_id, a.fi_id, d.payplan, a.is_senior
			from sls.personnel a
			join sls.team_personnel c on a.employee_number = c.employee_number
				and c.from_Date < (select first_day_of_next_month from sls.months
					where open_closed = 'open')
				and c.thru_date > (select last_day_of_previous_month from sls.months
					where open_closed = 'open')
			join sls.personnel_payplans d  on a.employee_number = d.employee_number
				and d.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
				and d.thru_date > (select last_day_of_previous_month from sls.months
					where open_closed = 'open')) a
	join ukg.clock_hours b on a.employee_number = b.employee_number
	join dds.dim_date c on b.the_date = c.the_date
	where c.year_month = (select year_month from sls.months where open_closed = 'open')
		and b.clock_hours + b.pto_hours + b.hol_hours <> 0
	group by c.year_month, a.employee_number;	

$BODY$
language sql;
comment on function	sls.update_clock_hours() is 'called nightly from luigk sc_payroll_201803.py, class sls.update_clock_hours() to update
  sales personnel clock hours for the currently open month';

comment on TABLE sls.clock_hours_by_month is 'clock hours for sales personnel, updated nightly by function	sls.update_clock_hours()';