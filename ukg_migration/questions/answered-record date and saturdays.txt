

12/1/21: see the matrix mailed to Jeri


Hourly 
1--if the employee is employed during the entire pay period, the record date is the Saturday of the payroll week
change to:
1--if the employee is employed during the entire pay period, the record date is the second Saturday of the pay period

2--If the employee's hire date falls within the second week of the pay period, only the second Saturday is used as a record date
ok as is
3--If the employee's term date is prior to the Saturday record date, then the term date is used as the record date
change to:
3--If the employee's term date is prior to the second Saturday of the pay period, then the term date is used as the record date

4-- What about if the employee's hire date falls within the first week of the pay period?

Commissioned
1--if the employee is employed during the entire pay period, the record date is the Saturday of the payroll week
change to:
1--if the employee is employed during the entire pay period, the record date is the second Saturday of the pay period

2--If the employee's hire date falls within the second week of the pay period, only the second Saturday is used as a record date
ok as is

3--If the employee's term date is prior to the Saturday record date, then the term date is used as the record date

it appears that jeri is using "the Saturday record date"

Pay Period: 11/07/2021 -> 11/20/2021
First Saturday: 11/13/2021
Second Saturday: 11/20/2021

TLM Commission rules
Hourly
if the hire date falls within first week of the pay period

The Saturday of the payroll week refers to the 2 Saturdays of the 2 week pay period,
so, #1 says, there are 2 rows/employee and the record date is the saturday of each payroll week
