﻿
comment on table hs.hn_main_shop_flat_rate_payroll_data is 'table for storing all necessary hn main shop flat rate data to generate ukg import files and manager approval pages';
comment on function hs.update_hn_main_shop_flat_rate_payroll_data(integer) is 'updates table hs.hn_main_shop_flat_rate_payroll_data, probably called
	from a luigi task on the first day of biweekly pay period for the previous pay period';
comment on function hs.get_hn_flat_rate_managers_payroll_approval_data(integer) is 'generate the data necessary for the managers approval/submittal page in Vision';
comment on function hs.create_hn_flat_rate_tlm_import_file(integer) is 'generate data for the ukg tlm import file';


create or replace function hs.update_hn_main_shop_flat_rate_payroll_data(_pay_period_seq integer)
  returns void 
as
$BODY$
/*
  select hs.update_hn_main_shop_flat_rate_payroll_data(338);
*/
declare 
	_pay_period_start date := (
	  select min(the_date)
	  from dds.dim_date
	  where biweekly_pay_period_sequence = _pay_period_seq);
  _pay_period_end date := (
		select max(the_date)
		from dds.dim_date
		where biweekly_pay_period_sequence = _pay_period_seq);
begin 		
  delete 
  from  hs.hn_main_shop_flat_rate_payroll_data
  where pay_period_seq = _pay_period_seq;
  
	insert into hs.hn_main_shop_flat_rate_payroll_data
	select aa.*, 
		aa.flat_rate * aa.comm_hours * aa.prof as comm_pay,
		aa.bonus_rate * aa.bonus_hours * aa.prof as bonus_pay,
		(aa.flat_rate * aa.comm_hours * aa.prof) + (aa.bonus_rate * aa.bonus_hours * aa.prof)
	from (
		with 
			clock_hours as ( 
		--     create temp table clock_hours as
				select d.employee_number,
					sum(coalesce(clock_hours, 0)) as clock_hours, 
					sum(coalesce(pto_hours, 0) + coalesce(vac_hours, 0)) as pto_hours,
					sum(coalesce(hol_hours, 0)) as hol_hours,
					sum(coalesce(clock_hours, 0) + coalesce(pto_hours, 0) + coalesce(vac_hours, 0) + coalesce(hol_hours, 0)) as total_hours
				from dds.dim_date a
				join arkona.xfm_pypclockin b on a.the_date = b.the_date
				inner join hs.main_shop_flat_rate_techs d on b.employee_number = d.employee_number
					and d.thru_date > _pay_period_end
				where a.the_date between _pay_period_start and _pay_period_end
				group by d.employee_number),
			flag_hours as (
		--     create temp table flag_hours as 
				select d.employee_number, sum(b.flaghours) as flag_hours
				from dds.dim_date a
				inner join ads.ext_fact_repair_order b on a.date_key = b.flagdatekey
				inner join ads.ext_dim_tech c on b.techkey = c.techkey
				inner join hs.main_shop_flat_rate_techs d on c.employeenumber = d.employee_number
					and d.thru_date > _pay_period_end
				where a.the_date between _pay_period_start and _pay_period_end
				group by d.employee_number),
			adjustments as (
		--     create temp table adjustments as
				select a.tech_number, a.employee_number, sum(labor_hours) as flag_hours
				from hs.main_shop_flat_rate_techs a
				inner join arkona.ext_sdpxtim b on a.tech_number = b.technician_id
				where arkona.db2_integer_to_date_long(b.trans_date) between _pay_period_start and _pay_period_end
					and a.thru_date > _pay_period_end
				group by a.tech_number, a.employee_number
				having sum(labor_hours) <> 0)    
		select 
			_pay_period_start, _pay_period_end, _pay_period_seq,
			trim(to_char(_pay_period_start, 'Month')) || ' ' || extract(day from _pay_period_start) || ' - ' || trim(to_char(_pay_period_end, 'Month')) || ' ' || extract(day from _pay_period_end) || ', ' || extract(year from _pay_period_start),
			_pay_period_start + 6 as first_saturday,
			aa.employee_last_name, aa.employee_first_name, a.employee_number, 
			arkona.db2_integer_to_date(e.hire_date) as hire_date,
			arkona.db2_integer_to_date(e.termination_date) as term_date,  
			c.flag_hours, coalesce(d.flag_hours, 0) as adjustments,
			c.flag_hours + coalesce(d.flag_hours,0) as total_flag_hours,
			b.clock_hours, b.pto_hours, b.hol_hours, b.total_hours,
			case
				when b.clock_hours = 0 then 0
				else coalesce(c.flag_hours + coalesce(d.flag_hours,0), 0) / b.clock_hours 
			end as prof,
			a.flat_rate, 2 * flat_rate as bonus_rate,
			case
				when b.total_hours > 90 
					and  
						case
							when b.clock_hours = 0 then 0
							else coalesce(c.flag_hours + coalesce(d.flag_hours,0), 0) / b.clock_hours 
						end >= 1 then b.clock_hours - (b.total_hours - 90)
				else b.clock_hours
			end as comm_hours,
			case
				when b.total_hours > 90 
					and  
						case
							when b.clock_hours = 0 then 0
							else coalesce(c.flag_hours + coalesce(d.flag_hours,0), 0) / b.clock_hours 
						end >= 1 then b.total_hours - 90
				else 0
			end as bonus_hours
		-- select a.*  
		from hs.main_shop_flat_rate_techs a
		join arkona.ext_pymast aa on a.employee_number = aa.pymast_employee_number
		join clock_hours b on a.employee_number = b.employee_number
		join flag_hours c on a.employee_number = c.employee_number
		left join adjustments d on a.employee_number = d.employee_number
		left join arkona.ext_pymast e on a.employee_number = e.pymast_employee_number
		where a.thru_date > _pay_period_end
			and a.employee_number <> '210073') aa;
end
$BODY$
language plpgsql;			


-- select * from hs.hn_main_shop_flat_rate_payroll_data

-----------------------------------------------------------------------
-- manager approval/submittal page
-----------------------------------------------------------------------
--  DROP FUNCTION hs.get_hn_flat_rate_managers_payroll_approval_data(integer);
create or replace function hs.get_hn_flat_rate_managers_payroll_approval_data(_pay_period_seq integer)	
returns table (pay_period_select_format citext,tech citext, employee_number citext,flat_rate 
	numeric, bonus_rate numeric, prof numeric, flag_hours numeric, adjustments numeric, 
	total_flag_hours numeric, clock_hours numeric, pto_hours numeric, hol_hours numeric, 
	total_hours numeric, comm_hours numeric, comm_pay numeric, 
	bonus_hours numeric, bonus_pay numeric, total numeric)
as
$BODY$
/*
  it is necessary to include pto and hol hours because they contribute to total_hours which is used to figure bonus
  example: (includes pto, holiday & bonus, o'neil 1106421, pp_seq = 338)
	select * from tp.gm_main_shop_flat_rate_payroll_data where pay_period_seq = 338 and employee_number = '1106421'
	clock  88.60
	pto     4.00
	hol     8.00
	total 100.60
	comm hours = 88.6 - (100.6 - 90) = 78 (when there is no bonus, clock_hours = comm_hours)
	bonus hours = 100.6 - 90 = 10.6
	comm_pay = comm_hours * flat_rate * prof = 78 * 23.50 * 1.3692 = 2509.74
	bonus pay = bonus_hours * bonus_rate * prof = 10.6 * 47 * 1.3692 = 682.14
	total for tlm = comm_pay + bonus_pay = 3191.88    
	
  select * from hs.get_hn_flat_rate_managers_payroll_approval_data(338) order by tech ;
*/
		select pay_period_select_format, (last_name || ', ' || first_name)::citext as tech, 
			employee_number, flat_rate,bonus_rate, round(prof, 2) as prof,
			flag_hours,round(adjustments,2) as adjustments, 
			round(total_flag_hours,2) as total_flag_hours,round(clock_hours,2) as clock_hours, 
			pto_hours, hol_hours, total_hours,
			round(comm_hours, 2) as comm_hours, round(comm_pay, 2) as comm_pay,
			bonus_hours, round(bonus_pay, 2) as bonus_pay, round(comm_pay + bonus_pay, 2) as total_pay
		from hs.hn_main_shop_flat_rate_payroll_data
		where pay_period_seq = _pay_period_seq
		order by tech; 
$BODY$
language sql;

-----------------------------------------------------------------------
-- import file
-----------------------------------------------------------------------

create or replace function hs.create_hn_flat_rate_tlm_import_file(_pay_period_seq integer)
returns table("Employee Id" citext,"EIN Name" text,"Counter Name" text,"Record Date" date,
	"Operation" text, "Value" numeric)
as
$BODY$
/*
  select * from hs.create_hn_flat_rate_tlm_import_file(338);
*/
-- flat rate comp
select employee_number as "Employee Id", 'Rydell Auto Center, Inc.' as "EIN Name", 'Flat Rate Compensation' as "Counter Name",
  case
    when hire_date <= pay_period_start and term_date >= pay_period_end then pay_period_end
    when hire_date <= pay_period_start and term_date < pay_period_end then term_date
    when hire_date between pay_period_start and first_saturday and term_date > pay_period_end then pay_period_end
    when hire_date between pay_period_start and first_saturday and term_date < pay_period_end then term_date
    when hire_date between first_saturday + 1 and pay_period_end and term_date < pay_period_end then term_date
  end as "Record Date",  
  'ADD' as "Operation", comm_pay as "Value"  
from hs.hn_main_shop_flat_rate_payroll_data
where pay_period_seq = _pay_period_seq
union  
-- flat rate premium (bonus pay)
select employee_number as "Employee Id", 'Rydell Auto Center, Inc.' as "EIN Name", 'Flat Rate Premium Pay' as "Counter Name",
  case
    when hire_date <= pay_period_start and term_date >= pay_period_end then pay_period_end
    when hire_date <= pay_period_start and term_date < pay_period_end then term_date
    when hire_date between pay_period_start and first_saturday and term_date > pay_period_end then pay_period_end
    when hire_date between pay_period_start and first_saturday and term_date < pay_period_end then term_date
    when hire_date between first_saturday + 1 and pay_period_end and term_date < pay_period_end then term_date
  end as "Record Date",  
  'ADD' as "Operation", bonus_pay as "Value"  
from hs.hn_main_shop_flat_rate_payroll_data
where pay_period_seq = _pay_period_seq
  and bonus_pay <> 0
$BODY$
language sql;  
  