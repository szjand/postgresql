﻿
-- yikes, lots of diffs
select * 
from (
select the_date, pay_period_seq, first_name , last_name, employee_number, tech_clock_hours_day, tech_flag_hours_day, team_prof_pptd
from tp.test_data
where the_date between current_date - 31 and current_date - 1) a
join (
select the_date, pay_period_seq, first_name , last_name, employee_number, tech_clock_hours_day, tech_flag_hours_day, team_prof_pptd
from tp.data
where the_date between current_date - 31 and current_date - 1) b on a.the_date = b.the_date and a.employee_number = b.employee_number
  and a.team_prof_pptd <> b.team_prof_pptd


select * 
from (
	select the_date, pay_period_seq, first_name , last_name, employee_number, tech_clock_hours_day, tech_flag_hours_day, team_prof_pptd,
		(
			select md5(z::text) as hash
			from (
				select the_date, pay_period_seq, first_name , last_name, employee_number, tech_clock_hours_day::numeric(6,2), tech_flag_hours_day::numeric(6,2), team_prof_pptd::numeric(8,2)
				from tp.test_data
				where the_date = x.the_date
					and department_key = x.department_key
					and tech_number = x.tech_number) z)
	from tp.test_data x
	where the_date between current_date - 31 and current_date - 1) a
join (
	select the_date, pay_period_seq, first_name , last_name, employee_number, tech_clock_hours_day, tech_flag_hours_day, team_prof_pptd,
		(
			select md5(z::text) as hash
			from (
				select the_date, pay_period_seq, first_name , last_name, employee_number, tech_clock_hours_day::numeric(6,2), tech_flag_hours_day::numeric(6,2), team_prof_pptd::numeric(8,2)
				from tp.data
				where the_date = y.the_date
					and department_key = y.department_key
					and tech_number = y.tech_number) z)	
	from tp.data y
	where the_date between current_date - 31 and current_date - 1) b on a.the_date = b.the_date and a.employee_number = b.employee_number and a.hash <> b.hash

-- pre-diag team prof goes off on 2/10
select 'test data' as source, * from tp.test_data where pay_period_seq = 343 and employee_number = '1106399'
union
select 'tp.data', * from tp.data where pay_period_seq = 343 and employee_number = '1106399'
order by the_date, source

-- 03/05/22 have not come to a conclusion about what is correct on these flag hour discrepancies

-- this is very disheartening 
looking at jay olson on 2/10/22
tp.test_data shows 17 hours
tp.data shows 15.9 hours

-- before i close this up for the day, look at jay's total for the pay period
-- the same 1.1 hour diffeence
select sum(tech_flag_hours_day)
from tp.test_data
where pay_period_seq = 343
  and employee_number = '1106399'

select sum(tech_flag_hours_day)
from tp.data
where pay_period_seq = 343
  and employee_number = '1106399'  

-- both pg and advantage fact_repair_order show jay wwith 15.4 hours
-- pg
select a.ro, a.flag_hours, sum(flag_hours) over ()
from dds.fact_repair_order a
join dds.dim_tech b on a.tech_key = b.tech_key
  and b.tech_number = '631'
where a.flag_date = '02/10/2022'  

-- advantage (from scotest)
SELECT d.lastname, d.firstname, sum(a.flaghours)
FROM dds.factrepairorder a
JOIN dds.day b on a.flagdatekey = b.datekey
  AND b.thedate = '02/10/2022'
JOIN dds.dimtech c on a.techkey = c.techkey
JOIN tptechs d on c.employeenumber = d.employeenumber
JOIN tpteamtechs e on d.techkey = e.techkey
JOIN tpteams f on e.teamkey = f.teamkey
  AND f.teamname = 'pre-diagnosis'
GROUP BY d.lastname, d.firstname	

-- the total here is 16.9 hours
DMS Service Technician Time by Flag Date Report
 SD3002P                                       Rydell Auto Center Inc.                                         3/05/22   9:54:35
 JANDR561                                          Technician Time--Flag Date                                           Page   1
 From  2/10/22 to  2/10/22 (Flag Time)             Technician: Tech 631 (631)
 =================================================================================================================================
                                                                      Total
       RO# Line Name                   VIN                 Date       Hours   Labor Op   Description
 =================================================================================================================================
  16502580    2 FITZPATRICK, DESTINY   2GNAXUEV1M6119563  2/10/22        .20 23I        MULTIPOINT INSPECTION
              1                                           2/10/22       1.10 DIA        DIAGNOSTIC TIME
              3                                           2/10/22            DIA        DIAGNOSTIC TIME
  16504296    1 JOSSUND, ANDREW        3GCUKREC8EG101406  2/10/22 *     1.00
  16504943    1 FENTON, KENDRA         1GCSHAF46E1199414  2/10/22       1.00 DIA        DIAGNOSTIC TIME
  16504957    1 BRUMITT, ALIXANDRIA D  1G1ZE5STXHF205338  2/10/22       1.00 *
              2                                           2/10/22        .20 23I        MULTIPOINT INSPECTION
  16505758    1 RODSETH, JOHN          1GCRYDED2LZ317491  2/10/22       1.00 DIA        DIAGNOSTIC TIME
              2                                           2/10/22        .20 23I        MULTIPOINT INSPECTION
  16506121    1 QUEST DIAGNOSTICS      2GNAXTEV3M6155474  2/10/22       3.00 *
              2                                           2/10/22        .20 23I        MULTIPOINT INSPECTION
  16506477    1 ZELTINGER, TYSON MARK  1GKS2JKL8MR288565  2/10/22       1.00 *
              2                                           2/10/22        .20 23I        MULTIPOINT INSPECTION
  16506532*   1 ANDERSON, MAGGEN       ZACCJBCB4HPF96335  2/10/22        .20 23I        MULTIPOINT INSPECTION
              2                                           2/10/22       1.00 DIA        DIAGNOSTIC TIME
              4                                           2/10/22 *      .50
  16506571    1 SUBBA MAGAR, SUK       1FADP3F2XEL382007  2/10/22        .20 23I        MULTIPOINT INSPECTION
              2                                           2/10/22        .50 DIA        DIAGNOSTIC TIME
  16506611    1 ATHERTON, REBECCA SUE  2GNFLEEK8D6407819  2/10/22       1.00 DIA        DIAGNOSTIC TIME
              3                                           2/10/22        .20 23I        MULTIPOINT INSPECTION
              4                                           2/10/22       1.00 *
  16506689    1 MCGREGOR, KENTON MERWI 1G6DC5E50C0118969  2/10/22        .20 23I        MULTIPOINT INSPECTION
              2                                           2/10/22       2.00 DIA        DIAGNOSTIC TIME

