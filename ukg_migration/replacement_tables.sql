﻿create schema ukg_mig;
comment on schema ukg_mig is ' repository for tables used in the migration from dealertrack to ukg';

select * 
from arkona.ext_pymast
where active_code <> 'T'
limit 10

drop table if exists ukg_mig.pymast cascade;
create table ukg_mig.pymast (
  store citext,  
  employee_number citext,
  dept_code citext,
  employee_name citext,
  first_name citext,
  middle_name citext, 
  last_name citext,
  hire_date date,
  orig_hire_date date,
  term_date date,
  distrib_code citext);
  



insert into ukg_mig.pymast
select pymast_company_number, pymast_employee_number, department_code, 
  employee_name, employee_first_name, employee_middle_name, employee_last_name, 
  arkona.db2_integer_to_date(hire_date), arkona.db2_integer_to_date(org_hire_date),
  arkona.db2_integer_to_date(termination_date), distrib_code
from arkona.ext_pymast
where active_code <> 'T'
  and pymast_company_number in ('RY1','RY2');  
   