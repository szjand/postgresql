﻿/*
for the managers page need (from kims spreadsheet):
	Other Addtl Comp (79)
	Unit Commission less draw (79)
	F&I Comm (79A)
	Pulse (79B)
	adjustments will be handled on the UI

this is essentially taken from function sls.json_get_payroll_for_submittal(citext)	

until there is clarity with afton, will work only on the import file
so, the data i need is :
	employee_number
	dates
	Other Addtl Comp (79) : Monthly Commission x
	Unit Commission less draw (79): Sales Volume Commission x
	F&I Comm (79A): F&I Commission
	Pulse (79B)	: Pulse Pay
	Guarantee - Amount Earned: Monthly Guarantee

!!! need aubol and grollimund

select * from sls.consultant_payroll where year_month = 202109 order by left(employee_number, 1), last_name 
select * from sls.personnel
select * from sls.additional_comp
select * from sls.months where year_month = 202109
select * from sls.paid_by_month where year_month = 202109
initially thinking no need to break out the stores, same data for each store, but different
values for company short name(?), ein name, payroll name, 

uh oh, total earned includes what specifically
from sls.update_consultant_payroll
ff.unit_pay + ff.fi_pay + ff.pto_pay + coalesce(kk.pulse, 0) as total_earned,
*/

-- drop function sls.create_sales_payroll_import_file(integer);
create or replace function sls.create_sales_payroll_import_file(_year_month integer) 
	returns table(
		"Company Short Name" text,"Employee Id" citext,"EIN Name" text,"Payroll Name" text,
	  "Payroll Pay Date" date,"Payroll Type" text,"Pay Statement Type" text,"Start Date" text,
	  "End Date" text,"E/D/T" text,"E/D/T Name" text,"E/D/T Amount" numeric)
-- 	  consultant text)
AS
$BODY$
/*
uh oh, total earned includes what specifically
from sls.update_consultant_payroll
ff.unit_pay + ff.fi_pay + ff.pto_pay + coalesce(kk.pulse, 0) as total_earned,

select *  from sls.create_sales_payroll_import_file(202112);

-- add consultant for testing
select "Employee Id", "E/D/T Name", consultant, "E/D/T Amount", sum("E/D/T Amount") over (partition by consultant) as consultant_total
from (
  select * 
  from sls.create_sales_payroll_import_file(202112)) x order by left("Employee Id", 1), consultant

  
*/	
-- do $$
declare
  _year_month integer := _year_month;
	_payroll_pay_date date := (
	  select first_day_of_next_month
	  from sls.months
	  where year_month = _year_month);
  _first_of_month date := (
    select first_of_month
    from sls.months
    where year_month = _year_month);	  
  _last_of_month date := (
    select last_of_month
    from sls.months
    where year_month = _year_month);	    
begin 
  return query
	with
		payroll_data as (
			select f.store_code, a.last_name || ', ' || a.first_name as consultant, a.employee_number, 
				coalesce(e.additional_comp, 0) as other_additional_comp, -- "Other - Addtl Comp (79)",
				a.total_earned, a.guarantee,
				case -- sales_volume_commission
					when a.total_earned >= a.guarantee then
						a.unit_pay - coalesce(a.draw, 0)
					when a.total_earned < a.guarantee then a.unit_pay - a.pulse - a.pto_pay - a.fi_pay
				end  as sales_volume_commission, -- "Unit Commission less draw (79)",
				round( -- monthly_guarantee
					CASE
						WHEN a.total_earned < a.guarantee then
							(a.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) - coalesce(a.unit_pay, 0)
						else 0
					end, 2) as monthly_guarantee,
				a.fi_pay as fi_commission, -- "F&I Comm (79A)",   
				a.pulse as pulse -- "Pulse (79B)"   
			from sls.consultant_payroll a
			left join sls.pto_intervals b on a.employee_number = b.employee_number
				and a.year_month = b.year_month
			left join sls.paid_by_month c on a.employee_number = c.employee_number
				and a.year_month = c.year_month
			left join (
				select employee_number, sum(amount) as adjusted_amount
				from sls.payroll_adjustments
				where year_month = _year_month
				group by employee_number) d on a.employee_number = d.employee_number
			left join (
				select employee_number, sum(amount) as additional_comp
				from sls.additional_comp
				where thru_date > _first_of_month
				group by employee_number) e on a.employee_number = e.employee_number  
			left join sls.personnel f on a.employee_number = f.employee_number
			left join ( -- if this is the first month of employment, multiplier = days worked/days in month
				select a.employee_number, 
					round(wd_of_month_remaining::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as guarantee_multiplier 
				from sls.personnel a
				join dds.dim_date b on a.start_date = b.the_date
					and b.year_month = _year_month) g on f.employee_number = g.employee_number      
			left join ( -- if employee termed mid month, multiplier elapsed wd in month at term date/work days in month
				select a.employee_number, 
					round(wd_of_month_elapsed::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as term_guarantee_multiplier
				from sls.personnel a
				join dds.dim_date b on a.end_date = b.the_date
					and b.year_month = _year_month) h on f.employee_number = h.employee_number         
			where a.year_month = _year_month)
	select '6175637'::text, employee_number, 		
		case
			when store_code = 'RY1' then 'Rydell Auto Center, Inc.'
			when store_code = 'RY2' then 'H.G.F., Inc.'
		end,
		'RAC Semi-Monthly Regular ' || to_char(_payroll_pay_date, 'MM') || '/' || to_char(_payroll_pay_date, 'DD') || '/' || extract(year from _payroll_pay_date) as payroll_name,
		current_date, 'Regular'::text, 'Regular'::text, to_char(_first_of_month, 'MM/DD/YYYY'), to_char(_last_of_month, 'MM/DD/YYYY'),
		'E'::text, 'Sales Volume Commission'::text, sales_volume_commission::numeric--, a.consultant
	from payroll_data a
	where sales_volume_commission <> 0
	union
	select '6175637'::text, employee_number, 		
		case
			when store_code = 'RY1' then 'Rydell Auto Center, Inc.'
			when store_code = 'RY2' then 'H.G.F., Inc.'
		end,
		'RAC Semi-Monthly Regular ' || to_char(_payroll_pay_date, 'MM') || '/' || to_char(_payroll_pay_date, 'DD') || '/' || extract(year from _payroll_pay_date) as payroll_name,
		current_date, 'Regular'::text, 'Regular'::text, to_char(_first_of_month, 'MM/DD/YYYY'), to_char(_last_of_month, 'MM/DD/YYYY'),
		'E'::text, 'Monthly Guarantee'::text, monthly_guarantee::numeric--, a.consultant
	from payroll_data a
	where monthly_guarantee <> 0
	union
	select '6175637'::text, employee_number, 		
		case
			when store_code = 'RY1' then 'Rydell Auto Center, Inc.'
			when store_code = 'RY2' then 'H.G.F., Inc.'
		end,
		'RAC Semi-Monthly Regular ' || to_char(_payroll_pay_date, 'MM') || '/' || to_char(_payroll_pay_date, 'DD') || '/' || extract(year from _payroll_pay_date) as payroll_name,
		current_date, 'Regular'::text, 'Regular'::text, to_char(_first_of_month, 'MM/DD/YYYY'), to_char(_last_of_month, 'MM/DD/YYYY'),
		'E'::text, 'F&I Commission'::text, fi_commission::numeric--, a.consultant
	from payroll_data a
	where fi_commission <> 0	
	union
	select '6175637'::text, employee_number, 		
		case
			when store_code = 'RY1' then 'Rydell Auto Center, Inc.'
			when store_code = 'RY2' then 'H.G.F., Inc.'
		end,
		'RAC Semi-Monthly Regular ' || to_char(_payroll_pay_date, 'MM') || '/' || to_char(_payroll_pay_date, 'DD') || '/' || extract(year from _payroll_pay_date) as payroll_name,
		current_date, 'Regular'::text, 'Regular'::text, to_char(_first_of_month, 'MM/DD/YYYY'), to_char(_last_of_month, 'MM/DD/YYYY'),
		'E'::text, 'Pulse'::text, pulse::numeric--, a.consultant
	from payroll_data a
	where pulse <> 0;	
end
$BODY$
language plpgsql;		

