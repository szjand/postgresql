﻿/*
TODO
12/26/2021
	figure out howw i am going to track for undefined timeoff types


NEW
12/26/21
  in function jon.compare_hours(_the_date date) exclude rows where both systems are 0
  mgr enters raw hours rather than start/end

QUESTIONS
12/26/21
	wtf is up with xmas day?
	pto vs pto payout
	where does ot show up in timesheets?
  
*/
ukg.json_time_entries
ukg.ext_time_entries
ukg.clock_times
ukg.clock_hours

luigi: ukg.py
ukg.json_timeoffs
ukg.ext_timeoffs

select * from ukg.ext_time_entries order by the_date desc
select * from luigi.luigi_log where the_date = current_date and pipeline = 'ukg'


select * from ukg.ext_time_entries where the_date = '12/23/2021' and employee_account_id = 12990767642 order by employee_account_id

-- 31 days
select the_date from dds.dim_date where the_date between '11/27/2021' and '12/27/2021'


-- 12/24/2021
-- currently only 2 parent timeoffs
select * from ukg.ext_timeoffs where id in (select distinct parent_id from ukg.ext_timeoffs)

select * from ukg.ext_clock_times where the_date = current_date -1

select * from ukg.ext_clock_times limit 10

select the_date from dds.dim_date where the_date between current_date - 31 and current_date - 1

-- clock_times needs to include paid & unpaid time off
-- sometime Paid Time Off has start/end_times, sometime it doesen't
-- BUT in all cases Paid Time Off has a total value and unpaid time off (unpaid lunch, unpaid break) has a null total 
-- leaning toward, use start/end times if they exist, otherwise total
-- 12/25 decided to store a lot of attributes in clock_times, probably/maybe won't need them, too early to say 
create or replace function ukg.ext_clock_times()
returns void as
$BODY$
/*
select ukg.ext_clock_times();
*/
	delete 
	from ukg.ext_clock_times
	where the_date between current_date - 31 and current_date -1;
	
	insert into ukg.ext_clock_times
	select 
	 case
		 when b.ein_name = 'Rydell Auto Center, Inc.' then 'GM'
		 when b.ein_name = 'H.G.F., Inc.' then 'HN'
		 else 'XX'
	 end as store,
	 b.last_name, b.first_name, b.employee_number, b.employee_id, 
	 a.start_date, a.end_date, a.time_entry_id, a.time_entry_type, a.the_date, a.start_time, a.end_time,
	 a.total, a.time_off_id, c.name as timeoff_name, a.is_raw, a.is_calc, a.calc_start_time, a.calc_end_time, a.calc_total,
	 a.cost_centers 
	from ukg.ext_time_entries a
	join ukg.ext_employees b on a.employee_account_id = b.employee_id
	left join ukg.ext_timeoffs c on a.time_off_id = c.id
	where a.the_date between current_date - 31 and current_date -1;
$BODY$
language sql;

drop table if exists ukg.ext_clock_times cascade;
create table ukg.ext_clock_times (
  store citext not null,
  last_name citext not null,
  first_name citext not null,
  employee_number citext not null,
  employee_id bigint not null,
  start_date date, 
  end_date date,
  time_entry_id bigint not null,
  time_entry_type citext,
  the_date date not null,
  start_time timestamptz,
  end_time timestamptz,
  total bigint,
  timeoff_id bigint,
  timeoff_name citext,
  is_raw boolean not null, 
  is_calc boolean not null,
  calc_start_time timestamptz,
  calc_end_time timestamptz,
  cal_total bigint,
  cost_centers jsonb);


select * from ukg.ext_clock_times limit 10
select * from ukg.ext_timeoffs

select employee_number, sum(ot_hours) from arkona.xfm_pypclockin where the_date between '12/19/2021' and '01/01/2022' and ot_hours <> 0 group by employee_number

-- need to figure out which of any of the the other timeoffs are paid
-- sometime Paid Time Off has start/end_times, sometime it doesen't
-- BUT in all cases Paid Time Off has a total value and unpaid time off (unpaid lunch, unpaid break) has a null total 
-- leaning toward, use start/end times if they exist, otherwise total
drop table if exists ukg.clock_hours_tmp cascade;
create table ukg.clock_hours_tmp (
  store citext not null,
  last_name citext not null,
  first_name citext not null,
  employee_id bigint not null,
  employee_number citext not null,
  the_date date not null,
  clock_hours numeric not null,
  pto_hours numeric not null,
  pto_payout_hours numeric not null,
  hol_hours numeric not null,
  primary key (employee_id, the_date));
  

-- -- test specifically for known paid time off
-- -- start_time is not null: clock hours
-- -- total is not null: paid time off
-- -- stop using current time for end time in end of day queries, managers have easy indicators of missing clock outs, use -1
-- --
-- -- if a manager enters raw time rather than start and end times there will be a total eg Dylan Helm 12/20/2021
-- -- select 72000/3600000.0
-- select * from (
-- select a.store, a.last_name, a.first_name, a.employee_id, a.employee_number, a.the_date,
--   a.timeoff_name, a.start_time, a.end_time, a.total,
--   coalesce(
-- 		case
-- 			when start_time is not null and end_time is not null and timeoff_name is null then 
-- 				((extract(epoch from a.end_time) - extract(epoch from a.start_time))/3600.0)::numeric(6,2)
-- 			when start_time is not null and end_time is null then -1
-- 			when start_time is null and end_time is null and timeoff_name is null then (total/3600000.0)::numeric(6,2)
-- 		end, 0) as clock_hours,
--   coalesce(case when timeoff_name = 'Paid Time Off' then (total/3600000.0)::numeric(6,2) end, 0) as pto_hours,
--   coalesce(case when timeoff_name = 'PTO Payout' then (total/3600000.0)::numeric(6,2) end, 0) as pto_payout_hours,
--   coalesce(case when timeoff_name = 'Holiday' then (total/3600000.0)::numeric(6,2) end, 0) as hol_hours
-- from ukg.ext_clock_times a
-- where (start_time is not null or total is not null)
-- -- ) x where last_name = 'miller' order by the_date
-- ) x --order by clock_hours
-- --   and employee_number = '196341'
-- where the_date = '12/25/2021'
-- --   and the_date between '12/19/2021' and '12/24/2021'
-- -- order by the_date
-- order by a.last_name, a.first_name, the_date
-- 
-- select * from ukg.ext_clock_times where the_date = '12/25/2021'
-- -- this yields, as expected, all unpaid time intervals (Unpaid Lunch & Unpaid Break)
-- select * from ukg.ext_clock_times where start_time is null and total is null

-- 1 row per emp/date
-- first cut of first half of clock hours (pre ot)
-- looks pretty pretty good
-- switching to calc_times
-- exception: reuter 12/27 is_raw:t is_calc:f, calc_times are null

select * from ukg.clock_hours_tmp where the_date = current_date

create or replace function ukg.update_clock_hours_tmp()
returns void as
$BODY$
/*  
  select ukg.update_clock_hours_tmp();
*/
delete 
from ukg.clock_hours_tmp
where the_date between current_date - 31 and current_date -1;

insert into ukg.clock_hours_tmp
select store, last_name, first_name, employee_id, employee_number, the_date,
  sum(clock_hours) as clock_hours, 
  sum(pto_hours) as pto_hours,
  sum(pto_payout_hours) as pto_payout_hours,
  sum(hol_hours) as hol_hours
from (  
	select a.store, a.last_name, a.first_name, a.employee_id, a.employee_number, a.the_date,
		a.timeoff_name, a.start_time, a.end_time, a.total,
		coalesce(
			case
			  -- straight clock time
				when start_time is not null and end_time is not null and timeoff_name is null then 
					((extract(epoch from a.end_time) - extract(epoch from a.start_time))/3600.0)::numeric(6,2)
			  -- no clock out
				when start_time is not null and end_time is null then -1
				-- raw total time only entered
				when start_time is null and end_time is null and timeoff_name is null then (total/3600000.0)::numeric(6,2)
			end, 0) as clock_hours,
		coalesce(case when timeoff_name = 'Paid Time Off' then (total/3600000.0)::numeric(6,2) end, 0) as pto_hours,
		coalesce(case when timeoff_name = 'PTO Payout' then (total/3600000.0)::numeric(6,2) end, 0) as pto_payout_hours,
		coalesce(case when timeoff_name = 'Holiday' then (total/3600000.0)::numeric(6,2) end, 0) as hol_hours
	from ukg.ext_clock_times a
	where (start_time is not null or total is not null)
	  and the_date between current_date - 31 and current_date -1 ) aa 
group by store, last_name, first_name, employee_id, employee_number, the_date;
$BODY$
language sql;


select * from ukg.ext_clock_times order by timeoff_name, start_time

-- calc_times exceptions
-- rows where is_calc is false
select start_time, date_trunc('minute', start_time)
from ukg.ext_clock_times where is_raw and not is_calc and timeoff_name is null

-- straight clock time  also is_raw and is calc both appear to be true
select * from ukg.ext_clock_times where start_time is not null and end_time is not null and timeoff_name is null

-- no clock out
select * from ukg.ext_clock_times where start_time is not null and end_time is null

-- raw total time only entered
select * from ukg.ext_clock_times where start_time is null and end_time is null and timeoff_name is null

-- not is_raw: no rows
select * from ukg.ext_clock_times where not is_raw

-- not is_calc
select * from ukg.ext_clock_times where not is_calc



select * 
from ukg.clock_hours_tmp a
full outer join (
select * 
from arkona.xfm_pypclockin
where the_date between '12/19/2021' and '12/25/2021') b on a.employee_number = b.employee_number and a.the_date = b.the_date

select * 
from arkona.xfm_pypclockin a
full outer join (
  select * 
  from ukg.clock_hours_tmp) b on a.employee_number = b.employee_number and a.the_date = b.the_date
where a.the_date between '12/19/2021' and '12/25/2021'


  -- first cut at ot
  
select * from ukg.clock_hours_tmp limit 100
-- insert into arkona.xfm_pypclockin
-- select aa.store, aa.employee_number, aa.the_date, coalesce(aa.clock_hours, 0) as clock_hours, 
--   coalesce(bb.reg_hours, 0) as reg_hours, coalesce(bb.ot_hours, 0) as ot_hours,
--   coalesce(aa.pto_hours, 0) as pto_hours, 
--   coalesce(aa.hol_hours, 0) as  hol_hours
-- drop table if exists ukg.clock_hours cascade;
-- create table ukg.clock_hours as

select * from ukg.clock_hours where the_date = current_date

create or replace function ukg.update_clock_hours()
returns void as
$BODY$
/*
  select ukg.update_clock_hours();
*/
delete 
from ukg.clock_hours
where the_date between current_date - 31 and current_date -1;

insert into ukg.clock_hours
select aa.store, aa.last_name, aa.first_name, aa.employee_id, aa.employee_number, 
	aa.the_date, aa.clock_hours, 
  bb.reg_hours, bb.ot_hours,
  aa.pto_hours, aa.pto_payout_hours,
  aa.hol_hours  
from ukg.clock_hours_tmp aa
left join ( -- bb: reg/ot hours
  select a.store, a.employee_number, a.the_week, a.the_date,
    round(
      case
        when coalesce(sum(b.clock_hours), 0) + a.clock_hours < 40 then a.clock_hours
        when coalesce(sum(b.clock_hours), 0) > 40 then 0
        else 40 - coalesce(sum(b.clock_hours), 0)
      end, 2) as reg_hours,
    round(
      case
        when coalesce(sum(b.clock_hours), 0) >= 40 then a.clock_hours
        when coalesce(sum(b.clock_hours), 0) + a.clock_hours < 40 then 0
        else a.clock_hours - (40 -coalesce(sum(b.clock_hours), 0))
      end, 2) as ot_hours  
  from ( -- a
    select b.sunday_to_saturday_week as the_week, a.* 
    from ukg.clock_hours_tmp a
    join dds.dim_date b on a.the_date = b.the_date
    where a.the_date between current_date - 31 and current_date -1) a  
  left join ( -- b
    select b.sunday_to_saturday_week as the_week, a.* 
    from ukg.clock_hours_tmp a
    join dds.dim_date b on a.the_date = b.the_date
    where a.the_date between current_date - 31 and current_date -1) b on a.employee_number = b.employee_number
      and a.the_week = b.the_week
      and b.the_date < a.the_date  -- this is the magix   
  group by a.store, a.the_week, a.employee_number, a.the_date, a.clock_hours) bb on aa.store = bb.store
    and aa.employee_number = bb.employee_number
    and aa.the_date = bb.the_date 
where aa.the_date between current_date - 31 and current_date -1;
$BODY$
language sql;    

-- alter table ukg.clock_hours add primary key(employee_id,the_date)      

-- compare dt to ukg
select * 
from (
	select * 
	from arkona.xfm_pypclockin
	where the_date between '12/19/2021' and '12/25/2021'
		and ot_hours <> 0) a
full outer join (
	select * 
	from ukg.clock_hours
	where ot_hours <> 0) b on a.the_date = b.the_date
		and a.employee_number = b.employee_number

select * 
from ukg.clock_hours
where pto_payout_hours <> 0
		
select *
from arkona.xfm_pypclockin
where employee_number = '111210'
  and the_date between '12/23/2021' and '12/24/2021'



-- this is the python script way
-- utilizing ukg.clock_hours and arkona.xfm_pypclockin
-- this is the script for generating the mgr spreadsheets

-- auto lunch times, code 666, -.5 hours, not done in ukg
select * from arkona.ext_pypclockin_tmp where /*pymast_employee_number in ('195675','190600') and */yicode = '666' and yiclkind > '12/18/2021'
select a.*, ((extract(epoch from a.yiclkoutt) - extract(epoch from a.yiclkint))/3600.0)::numeric(6,2)
from arkona.ext_pypclockin_tmp a 
where pymast_employee_number in ('195675','190600','182713','187526','197642') 
  and  yiclkind = '12/20/2021' 
  and yicode = '666'
order by pymast_employee_number, yicode

select distinct pymast_employee_number
from arkona.ext_pypclockin_tmp a 
where yicode = '666'
order by pymast_employee_number, yicode


-- fuckit, manually edit the spreadsheet

create or replace function jon.compare_hours(_the_date date)
returns void as
$BODY$
/*
 select jon.compare_hours('12/30/2021');
*/
	drop table if exists jon.compare_hours;
	create table jon.compare_hours as
	select _the_date as the_date, a.store, a.employee_number as "emp #", a.employee, b.clock_hours as "DT Hours", 
-- 	  b.clock_hours + d.lunch_hours as "DT Hours 2",
		c.clock_hours as "UKG Hours", abs(coalesce(b.clock_hours, 0) - coalesce(c.clock_hours, 0)) as difference, 
		c.pto_hours + c.pto_payout_hours as "UKG PTO Hours",
		b.pto_hours + b.vac_hours as "DT PTO Hours",
		abs((coalesce(c.pto_hours, 0) + coalesce(c.pto_payout_hours, 0)) - (coalesce(b.pto_hours, 0) + coalesce(b.vac_hours, 0))) as "PTO Diff",
		a.manager_1, a.cost_center
	from (
		select a.store, a.last_name || ', ' || a.first_name as employee, a.employee_id, a.employee_number, 
			b.cost_center, b.manager_1
		from ukg.clock_hours a
		left join ukg.employees b on a.employee_id = b.employee_id
		where a.the_date = _the_date
		union
		select b.store, b.last_name || ', ' || b.first_name as employee, b.employee_id, a.employee_number,
			b.cost_center, b.manager_1
		from arkona.xfm_pypclockin a
		left join ukg.employees b on a.employee_number = b.employee_number
		where the_date = _the_date) a
	left join arkona.xfm_pypclockin b on a.employee_number = b.employee_number
		and b.the_date = _the_date
	left join ukg.clock_hours c on a.employee_id = c.employee_id
		and c.the_date = _the_date	

	order by employee;
$BODY$
language sql;
 select * from jon.compare_hours 
where difference > '.05'
or "PTO Diff" > 1
order by manager_1, employee


-- -- xmas day
-- i fucking updated the missing ones in UKG, 10 of them
-- select * 
-- from (
-- select store, last_name, first_name, employee_id, employee_number, the_date, hol_hours 
-- from ukg.clock_hours
-- where the_date = '12/25/2021') a
-- full outer join (
-- select * 
-- from arkona.xfm_pypclockin
-- where the_date = '12/25/2021') b on a.employee_number = b.employee_number
-- 
-- select * 
-- from (
-- 	select '12/25/2021'::date as the_date, a.store, a.employee_number as "emp #", a.employee, b.hol_hours as "DT Hours", 
-- -- 	  b.clock_hours + d.lunch_hours as "DT Hours 2",
-- 		c.hol_hours as "UKG Hours", abs(coalesce(b.hol_hours, 0) - coalesce(c.hol_hours, 0)) as difference, 
-- 		a.manager_1, a.cost_center
-- 	from (
-- 		select a.store, a.last_name || ', ' || a.first_name as employee, a.employee_id, a.employee_number, 
-- 			b.cost_center, b.manager_1
-- 		from ukg.clock_hours a
-- 		left join ukg.employees b on a.employee_id = b.employee_id
-- 		where a.the_date between '12/19/2021' and '12/28/2021'
-- 		union
-- 		select b.store, b.last_name || ', ' || b.first_name as employee, b.employee_id, a.employee_number,
-- 			b.cost_center, b.manager_1
-- 		from arkona.xfm_pypclockin a
-- 		left join ukg.employees b on a.employee_number = b.employee_number
-- 		where the_date between '12/19/2021' and '12/28/2021') a
-- 	left join arkona.xfm_pypclockin b on a.employee_number = b.employee_number
-- 		and b.the_date ='12/25/2021'
-- 	left join ukg.clock_hours c on a.employee_id = c.employee_id
-- 		and c.the_date = '12/25/2021') x
-- where difference <> 0
------------------------------------------------------------------------------
--< timeoff definitions as of 12/26
------------------------------------------------------------------------------
select distinct timeoff_name -- paid: Paid Time Off, PTO Payout ??? do these need to be tracked separately?
from ukg.ext_clock_times

select * from ukg.ext_timeoffs -- Donation PTO, Time Off, Volunteer PTO, Jury Duty, Paid Time Off, Unpaid Time Off
where visible and requestable

select * from ukg.ext_timeoffs -- PTO Payout, Holiday, FMLA
where visible and not requestable

select * from ukg.ext_timeoffs -- NONE
where not visible and requestable

select * from ukg.ext_timeoffs -- Comp Time, Paid Lunch, Paid Break, Unpaid Lunch, Unpaid Break, Vacation, Sick, Bereavement, Leave of Absence
where not visible and not requestable

is not visible the same as not used

------------------------------------------------------------------------------
--/> timeoff definitions as of 12/26
------------------------------------------------------------------------------







-------------------------------------------------------------------------------------------------------
--< this is the current routine for generating the mgr spreadsheets
-------------------------------------------------------------------------------------------------------
I. run luigi_1804_36.ukg.py GetTimeEntries
II. run the following scripts
III. run python ukg.dt_Vs_ukg.py
-- handle no clock out with current ts
-- clock_times

drop table if exists jon.clock_times cascade;
create table jon.clock_times  as
select b.employee_number, 
	a.the_date, a.start_time AS clock_in, a.end_time as clock_out,  current_time as cur_time,
  case
    when a.end_time is null then
          ((extract(epoch from (current_date || ' ' || current_time)::timestamptz) - extract(epoch from a.start_time))/3600)::numeric(6,2)
    else 
					((extract(epoch from a.end_time) - extract(epoch from a.start_time))/3600)::numeric(6,2)
  end as clock_hours
from ukg.ext_time_entries a
join ukg.ext_employees b on a.employee_account_id = b.employee_id
where the_date between '12/19/2021' and '12/27/2021'


-- ukg clock hours
drop table if exists jon.ukg_clock_hours;
create table jon.ukg_clock_hours as
  select b.pymast_company_number as store, a.employee_number, b.employee_last_name as last_name, b.employee_first_name as first_name, 
		d.description as department, c.supervisor_name as supervisor, c.title, the_date, sum(clock_hours) as clock_hours
  from jon.clock_times a
  left join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
		-- fucking arkona
    and 
      case
        when pymast_employee_number = '257240' then pymast_company_number = 'RY2'
        else 1 = 1
      end    
  left join pto.compli_users c on a.employee_number = c.user_id
  left join arkona.ext_pypclkctl d on b.pymast_company_number = d.company_number 
    and b.department_code = d.department_code
  where a.the_date between '12/19/2021' and '12/27/2021'
  group by b.pymast_company_number, a.employee_number, b.employee_last_name, b.employee_first_name, 
		the_date, d.description, c.supervisor_name, c.title;

-- -- using only ukg tables, replace compli with cost centers
-- -- ukg clock hours
-- drop table if exists jon.ukg_clock_hours;
-- create table jon.ukg_clock_hours as
--   select b.store, a.employee_number, b.last_name, b.first_name, 
-- 		b.cost_center, b.manager_1, a.the_date, sum(clock_hours) as clock_hours
--   from jon.clock_times a
--   left join ukg.employees b on a.employee_number = b.employee_number 
--   where a.the_date between '12/19/2021' and '12/26/2021'
--   group by b.store, a.employee_number, b.last_name, b.first_name, 
-- 		b.cost_center, b.manager_1, a.the_date


		

-- arkona clock_hours
drop table if exists jon.dt_clock_hours;
create table jon.dt_clock_hours as
  select store_code as store, a.employee_number, b.employee_last_name as last_name, b.employee_first_name as first_name, 
  d.description as department, c.supervisor_name as supervisor, c.title, the_date, sum(clock_hours) as clock_hours
  from arkona.xfm_pypclockin a
  left join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
		-- fucking arkona
    and 
      case
        when pymast_employee_number = '257240' then pymast_company_number = 'RY2'
        else 1 = 1
      end      
  left join pto.compli_users c on a.employee_number = c.user_id
  left join arkona.ext_pypclkctl d on b.pymast_company_number = d.company_number 
    and b.department_code = d.department_code
  where a.the_date between '12/19/2021' and '12/27/2021'
  group by store_code, a.employee_number, b.employee_last_name, b.employee_first_name, the_date, d.description, c.supervisor_name, c.title;

-- -- using ukg tables with arkona.xfm_pypclockin, replace compli with cost centers
-- drop table if exists jon.dt_clock_hours;
-- create table jon.dt_clock_hours as
--   select store_code as store, a.employee_number, b.last_name as last_name, b.first_name as first_name, 
--   b.cost_center, b.manager_1, a.the_date, sum(clock_hours) as clock_hours
--   from arkona.xfm_pypclockin a
--   left join ukg.employees b on a.employee_number = b.employee_number
--   where a.the_date between '12/19/2021' and '12/26/2021'
--   group by store_code, a.employee_number, b.last_name, b.first_name, 
-- 		b.cost_center, b.manager_1, a.the_date



		  
select * from dt_clock_hours where employee_number = '115921'
select * from ukg.json_time_entries
select * from ukg.clock_times where employee_number = '145789'

-- compare DT & UKG
-- this was the manual way
do $$
declare
	_the_date date := '12/19/2021';
begin
  drop table if exists jon.compare_hours;
  create table jon.compare_hours as
	select a.store, a.employee_number as "emp #", a.employee, b.clock_hours as "DT Hours", 
		c.clock_hours as "UKG Hours", abs(coalesce(b.clock_hours, 0) - coalesce(c.clock_hours, 0)) as difference, 
		a.department, a.supervisor, a.title
	from (
		select store, employee_number, last_name || ', ' ||first_name as employee, department, supervisor, title
		from jon.dt_clock_hours
		where the_date = _the_date
		union
		select store, employee_number, last_name || ', ' ||first_name, department, supervisor, title
		from jon.ukg_clock_hours
		where the_date = _the_date) a
	left join jon.dt_clock_hours b on a.employee_number = b.employee_number	
	  and b.the_date = _the_date
	left join jon.ukg_clock_hours c on a.employee_number = c.employee_number	
	  and c.the_date = _the_date
	order by a.store, a.department, a.supervisor, a.employee;
end $$;
select * from jon.compare_hours -- where difference > 05 order by employee


-- this is the python script way
create or replace function jon.compare_hours(_the_date date)
returns void as
$BODY$
/*
  select jon.compare_hours('12/19/2021');
*/
--   drop table if exists jon.compare_hours;
--   create table jon.compare_hours as
  insert into jon.compare_hours
	select _the_date as the_date, a.store, a.employee_number as "emp #", a.employee, b.clock_hours as "DT Hours", 
		c.clock_hours as "UKG Hours", abs(coalesce(b.clock_hours, 0) - coalesce(c.clock_hours, 0)) as difference, 
		a.department, a.supervisor, a.title
	from (
		select store, employee_number, last_name || ', ' ||first_name as employee, department, supervisor, title
		from jon.dt_clock_hours
		where the_date = _the_date
		union
		select store, employee_number, last_name || ', ' ||first_name, department, supervisor, title
		from jon.ukg_clock_hours
		where the_date = _the_date) a
	left join jon.dt_clock_hours b on a.employee_number = b.employee_number	
	  and b.the_date = _the_date
	left join jon.ukg_clock_hours c on a.employee_number = c.employee_number	
	  and c.the_date = _the_date
	-- exclude rows where both systems have 0 hours
	where not (coalesce(b.clock_hours, 0) = 0 and coalesce(c.clock_hours, 0) = 0)
	order by a.store, a.department, a.supervisor, a.employee;
$BODY$
language sql;

select * from jon.compare_hours where the_date = '12/26/2021'

select * 
from arkona.xfm_pypclockin
where the_date = '12/25/2021'

-------------------------------------------------------------------------------
--< fucking holiday hours
-------------------------------------------------------------------------------

-- recreated the above daily process to handle only xmas holiday
-- this picks up the 1 row with a timeoff record
drop table if exists jon.clock_times cascade;
create table jon.clock_times  as
select b.employee_number, 
	a.the_date, a.start_time AS clock_in, a.end_time as clock_out,  current_time as cur_time,
  case
    when a.end_time is null then
          ((extract(epoch from (current_date || ' ' || current_time)::timestamptz) - extract(epoch from a.start_time))/3600)::numeric(6,2)
    else 
					((extract(epoch from a.end_time) - extract(epoch from a.start_time))/3600)::numeric(6,2)
  end as clock_hours
from ukg.ext_time_entries a
join ukg.ext_employees b on a.employee_account_id = b.employee_id
join ukg.ext_timeoffs c on a.time_off_id = c.id
where the_date = '12/25/2021'


-- ukg clock hours
drop table if exists jon.ukg_clock_hours;
create table jon.ukg_clock_hours as
  select b.pymast_company_number as store, a.employee_number, b.employee_last_name as last_name, b.employee_first_name as first_name, 
		d.description as department, c.supervisor_name as supervisor, c.title, the_date, sum(clock_hours) as clock_hours
  from jon.clock_times a
  left join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
		-- fucking arkona
    and 
      case
        when pymast_employee_number = '257240' then pymast_company_number = 'RY2'
        else 1 = 1
      end    
  join pto.compli_users c on a.employee_number = c.user_id
  join arkona.ext_pypclkctl d on b.pymast_company_number = d.company_number 
    and b.department_code = d.department_code
  where a.the_date = '12/25/2021'
  group by b.pymast_company_number, a.employee_number, b.employee_last_name, b.employee_first_name, 
		the_date, d.description, c.supervisor_name, c.title;	

select * from arkona.xfm_pypclockin where the_date = '12/25/2021'
-- arkona clock_hours
drop table if exists jon.dt_clock_hours;
create table jon.dt_clock_hours as
  select store_code as store, a.employee_number, b.employee_last_name as last_name, b.employee_first_name as first_name, 
  d.description as department, c.supervisor_name as supervisor, c.title, the_date, sum(hol_hours) as clock_hours
  from arkona.xfm_pypclockin a
  left join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
		-- fucking arkona
    and 
      case
        when pymast_employee_number = '257240' then pymast_company_number = 'RY2'
        else 1 = 1
      end      
  left join pto.compli_users c on a.employee_number = c.user_id
  left join arkona.ext_pypclkctl d on b.pymast_company_number = d.company_number 
    and b.department_code = d.department_code
  where a.the_date = '12/25/2021'
    and hol_hours <> 0
  group by store_code, a.employee_number, b.employee_last_name, b.employee_first_name, the_date, d.description, c.supervisor_name, c.title;

-- compare DT & UKG
-- this was the manual way
-- saved as xmas_end_of_day.csv
-- select * from arkona.xfm_pypclockin where the_date = '12/25/2021' and clock_hours <> 0
-- select * from arkona.xfm_pypclockin where the_date = '12/25/2021' and hol_hours <> 0
do $$
declare
	_the_date date := '12/25/2021';
begin
  drop table if exists jon.compare_hours;
  create table jon.compare_hours as
	select a.store, a.employee_number as "emp #", a.employee, b.clock_hours as "DT Hours", 
		c.clock_hours as "UKG Hours", abs(coalesce(b.clock_hours, 0) - coalesce(c.clock_hours, 0)) as difference, 
		a.department, a.supervisor, a.title
	from (
		select store, employee_number, last_name || ', ' ||first_name as employee, department, supervisor, title
		from jon.dt_clock_hours
		where the_date = _the_date
		union
		select store, employee_number, last_name || ', ' ||first_name, department, supervisor, title
		from jon.ukg_clock_hours
		where the_date = _the_date) a
	left join jon.dt_clock_hours b on a.employee_number = b.employee_number	
	  and b.the_date = _the_date
	left join jon.ukg_clock_hours c on a.employee_number = c.employee_number	
	  and c.the_date = _the_date
	order by a.store, a.department, a.supervisor, a.employee;
end $$;
select * from jon.compare_hours;
  
-------------------------------------------------------------------------------
--/> fucking holiday hours
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
--/> this is the current routine for generating the mgr spreadsheets
-------------------------------------------------------------------------------------------------------

-- ukg folks not clocking out	
the "blank" rows are the time off shit
select * from ukg.ext_time_entries where the_date = '12/20/2021' and employee_number = '179775'
select * from ukg.clock_times where the_date = '12/20/2021' and employee_number = '179775'


select * from ukg.ext_time_entries a
join ukg.ext_employees b on a.employee_account_id = b.employee_id
where a.the_date = '12/20/2021' 
and b.last_name = 'kolstad'


select * 
from luigi.luigi_log
where the_date = current_Date
  and pipeline = 'ukg'
order by from_ts  


select * 
from ukg.clock_hours
where the_date = '12/23/2021'
  and last_name = 'richardson'

select * 
from arkona.xfm_pypclockin
where the_date = '12/20/2021'
  and employee_number = '140583'

select min(ylclkind), max(ylclkind)
from arkona.ext_pypclkl  

select * 
from arkona.ext_pypclkl
where ylclkind = '12/20/2021'
  and pymast_employee_number = '140583'

select * 
from arkona.ext_pypclockin_tmp
where yiclkind = '12/20/2021'
  and pymast_employee_number = '140583'