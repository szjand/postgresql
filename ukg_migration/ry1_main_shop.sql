﻿/*
11/29/21
confusing discrepancies trying to use the postgresql generated version of tpdata
appears to be primarily a clock hours issue
for the moment, the following queries work on the tp schema as long as the tables
are all populated by a scrape of tee advantage data (of course), but the queries work
and that is what i will go with for a proof of concept for generating the import files

so what i will do today, is to generate an actual import file
	try to include the hire and term date provisions using current data

11/28
found it, luigi team_pay.py, called from run_all, calls tp.data_update_nightly() which
populates tp.data with data from postgresql tables
what is not done is updating the supporting tables teams, techs,team_techs, tech_values, team_values

the script for simply updating from advantage is python_projects/ads_for_luigi/ext_tpdata.py
updated all the tables, payroll now looks ok
add the blueprint guarantee	

*/

/*
so what i will do today, is to generate an actual import file
	try to include the hire and term date provisions using current data

looking at a union situations,
there will be employees with multiple lines
flat rate comp & flat rate premium, then bluepring will usually be a separate line, guarantee
so, maybe a base table with everything needed, then a unioned cte ?
first the base table, what do i need
	hire date & term date -> record date
	total comm, total bonus, guarantee -> will need the total to determine whether guarantee is applicable
*/

/*
12/7/21
things going a bit better
what i am thinking on this, initially is that this first table (and tp data and all the support tables) need to be put
in a nightly task
today, just formalize (with afton) all the import files for flat rate

this is a bit raggedy but works for generating the import
!!! the advantage task must be run to copy data to the postgresql tables !!!

actual next step will be to see aftons approval page and get her a query to populate that
*/
-- select * from ukg.import_gm_flat_rate_base
do $$
declare
	_pay_period_start date := '11/21/2021';
	_pay_period_end date := '12/04/2021';
	_first_saturday date := (
	  select the_date
	  from dds.dim_date
	  where day_name = 'saturday'
	    and the_date between _pay_period_start and _pay_period_end
	  order by the_date
	  limit 1) ;
	_second_saturday date := (
	  select the_date
	  from dds.dim_date
	  where day_name = 'saturday'
	    and the_date between _pay_period_start and _pay_period_end
	  order by the_date desc
	  limit 1) ;	  

	  
begin
drop table if exists ukg.import_gm_flat_rate_base;
create unlogged table ukg.import_gm_flat_rate_base as
select "Emp#" as "Employee Id",  _second_saturday as "Record Date", 
  "Total Comm Pay", "Total Bonus Pay", "Hol Pay", 
  "Total Comm Pay"+"Total Bonus Pay"+"Vac PTO Pay"+"Hol Pay" AS "Total",
  guarantee,
  _pay_period_start, _pay_period_end, _first_saturday, _second_saturday
FROM ( --zz  
  SELECT z.*, 
    "Total Comm Pay"+"Total Bonus Pay"+"Vac PTO Pay"+"Hol Pay" AS "Total",
    y.guarantee 
  FROM (
    SELECT trim(x.last_name) || ', ' || trim(first_Name) as Name, 
      x.employee_number AS "Emp#",
      commRate AS "Comm Rate", 
      round(commHours * teamProf/100, 2) AS "Comm Clock Hours",
      round((commhours * teamprof/100) * commrate, 2)  AS "Total Comm Pay",
      bonusRate AS "Bonus Rate",
      round(bonusHours * teamProf/100, 2) AS "Bonus Hours",
      round((bonushours * teamprof/100) * bonusrate, 2) AS "Total Bonus Pay",
      round(ptoRate, 2) AS "Vac PTO Hol Rate",
      vacationHours + ptoHours AS "Vac PTO Hours",
      round(ptoRate, 2) * (vacationHours + ptoHours) AS "Vac PTO Pay",
      holidayHours AS "Hol Hours",
      ptoRate * holidayHours AS "Hol Pay"
    FROM (  
			select d.team, d.last_name, d.first_name, d.employee_number, d.ptoRate, d.commRate, 
				d.bonusRate, d.teamProf, d.flagHours, e.clockHours, e.vacationHours, e.ptoHours, e.holidayHours, 
				e.totalHours,
				CASE WHEN e.totalHours > 90 and TeamProf >= 100 THEN e.clockhours - (totalhours - 90) else e.clockHours end AS commHours,
				round(CASE WHEN totalHours > 90 and TeamProf >= 100 THEN totalHours - 90 ELSE 0 END, 2) AS bonusHours   
			FROM ( --d    
				select c.team_name AS Team, last_name, first_name, employee_number, 
					tech_hourly_rate AS ptoRate, round(tech_tfr_rate, 2) AS CommRate, 
					2 * round(tech_tfr_rate, 2) AS BonusRate, team_prof_pptd AS teamProf, 
					Tech_Flag_Hours_PPTD as FlagHours
				FROM tp.data a
				INNER JOIN tp.team_techs b on a.tech_key = b.tech_key
					AND a.team_key = b.team_key
					AND b.thru_date >= '11/20/2021' -- @payRollEnd
				LEFT JOIN tp.Teams c on b.team_Key = c.team_Key
				WHERE the_date = '11/20/2021'
					AND a.department_Key = 18) d -- @department
			left join ( -- e
				select employee_number, tech_clock_hours_pptd AS clockHours, 
					tech_vacation_hours_pptd AS vacationHours,  tech_pto_hours_pptd AS ptoHours, 
					tech_holiday_hours_pptd AS holidayHours,
					tech_clock_hours_pptd + tech_vacation_hours_pptd + tech_pto_hours_pptd + tech_holiday_hours_pptd AS totalHours  
				FROM tp.data a
				INNER JOIN tp.team_techs b on a.tech_key = b.tech_key
					AND a.team_key = b.team_key
					AND b.thru_date >= '11/20/2021' -- @payRollEnd
				LEFT JOIN tp.Teams c on b.team_Key = c.team_Key
				WHERE the_date = '11/20/2021' --_pay_period_end
					AND a.department_Key = 18) e on d.employee_number = e.employee_number) x) z   
LEFT JOIN ( -- guarantee for the blueprint team
-- replace dds.edwclockhoursfact with arkona.xfm_pypclockin, and subsequently UKG time data
--   dds.day with dds.dim_date
--   and dds.edwEmployeedim with arkona.ext_pymast, and subsequently UKG data
	SELECT employee_number, reg + ot + pto + bonus AS guarantee
	FROM (
		SELECT aa.name, aa.employee_number, round(aa.hourly_rate * bb.clock, 2) AS reg, 
			round(aa.hourly_rate * 1.5 * bb.ot, 2) AS ot, 
			round(aa.pto_rate * (vac + pto + hol), 2) AS pto,
			case
				when bb.clock + bb.ot + bb.pto > 90 THEN  ((bb.clock + bb.ot + bb.pto) - 90) * 2 * aa.hourly_rate
			ELSE 0
			END AS bonus
		FROM tp.blueprint_guarantee_rates aa
		JOIN (			 
			select c.employee_name, c.pymast_employee_number, SUM(a.reg_hours) AS clock, SUM(a.ot_hours) AS ot,
				SUM(a.vac_hours) AS vac, SUM(a.pto_hours) AS pto, SUM(a.hol_hours) AS hol 
			FROM arkona.xfm_pypclockin a
			JOIN dds.dim_date b on a.the_date = b.the_date
				AND b.the_date BETWEEN _pay_period_start and _pay_period_end -- @payRollStart AND @payRollEnd
			JOIN arkona.ext_pymast c on a.employee_number = c.pymast_employee_number
				AND c.pymast_employee_number IN ('145801','1106399','1117901','152410')
			GROUP BY c.employee_name, c.pymast_employee_number) bb on aa.employee_number = bb.pymast_employee_number
		WHERE aa.thru_date = '12/31/9999') cc) y on z."Emp#" = y.employee_number) zz						 
order by ("Emp#")::integer;

end $$ ;

-- flat rate comp
select "Employee Id"::integer, 'Rydell Auto Center, Inc.' as "EIN Name", 'Flat Rate Compensation' as "Counter Name",
  case
    when arkona.db2_integer_to_date(b.hire_date) <= _pay_period_start 
			and arkona.db2_integer_to_date(b.termination_date) >= _pay_period_end then _second_saturday
    when arkona.db2_integer_to_date(b.hire_date) <= _pay_period_start
      and arkona.db2_integer_to_date(b.termination_date) < _pay_period_end then arkona.db2_integer_to_date(b.termination_date)

    when arkona.db2_integer_to_date(b.hire_date) between _pay_period_start and _first_saturday
      and arkona.db2_integer_to_date(b.termination_date) > _pay_period_end then _second_saturday
    when arkona.db2_integer_to_date(b.hire_date) between _pay_period_start and _first_saturday
      and arkona.db2_integer_to_date(b.termination_date) < _pay_period_end then arkona.db2_integer_to_date(b.termination_date)

    when arkona.db2_integer_to_date(b.hire_date) between _first_saturday + 1 and _pay_period_end
      and arkona.db2_integer_to_date(b.termination_date) < _pay_period_end then arkona.db2_integer_to_date(b.termination_date)
  end as "Record Date",  
  'ADD' as "Operation", "Total Comm Pay" as "Value"  
from tp.gm_flat_rate_for_approval a
left join arkona.ext_pymast b	on a."Employee Id" = b.pymast_employee_number	
where coalesce("Total Comm Pay", 0) > 0
  and guarantee is null
union
-- flat rate premium (bonus pay)  
select "Employee Id"::integer, 'Rydell Auto Center, Inc.' as "EIN Name", 'Flat Rate Premium Pay' as "Counter Name",
  case
    when arkona.db2_integer_to_date(b.hire_date) <= _pay_period_start 
			and arkona.db2_integer_to_date(b.termination_date) >= _pay_period_end then _second_saturday
    when arkona.db2_integer_to_date(b.hire_date) <= _pay_period_start
      and arkona.db2_integer_to_date(b.termination_date) < _pay_period_end then arkona.db2_integer_to_date(b.termination_date)

    when arkona.db2_integer_to_date(b.hire_date) between _pay_period_start and _first_saturday
      and arkona.db2_integer_to_date(b.termination_date) > _pay_period_end then _second_saturday
    when arkona.db2_integer_to_date(b.hire_date) between _pay_period_start and _first_saturday
      and arkona.db2_integer_to_date(b.termination_date) < _pay_period_end then arkona.db2_integer_to_date(b.termination_date)

    when arkona.db2_integer_to_date(b.hire_date) between _first_saturday + 1 and _pay_period_end
      and arkona.db2_integer_to_date(b.termination_date) < _pay_period_end then arkona.db2_integer_to_date(b.termination_date)
  end as "Record Date",  
  'ADD' as "Operation", "Total Bonus Pay" as "Value"  
from tp.gm_flat_rate_for_approval a
left join arkona.ext_pymast b	on a."Employee Id" = b.pymast_employee_number	
where coalesce("Total Bonus Pay", 0) > 0
  and guarantee is null
union
-- guarantee (blueprint team)  
select "Employee Id"::integer, 'Rydell Auto Center, Inc.' as "EIN Name", 
  case
    when guarantee > "Total" then 'Biweekly Guarantee'
    else 'Flat Rate Compensation'
  end as "Counter Name",
  case
    when arkona.db2_integer_to_date(b.hire_date) <= _pay_period_start 
			and arkona.db2_integer_to_date(b.termination_date) >= _pay_period_end then _second_saturday
    when arkona.db2_integer_to_date(b.hire_date) <= _pay_period_start
      and arkona.db2_integer_to_date(b.termination_date) < _pay_period_end then arkona.db2_integer_to_date(b.termination_date)

    when arkona.db2_integer_to_date(b.hire_date) between _pay_period_start and _first_saturday
      and arkona.db2_integer_to_date(b.termination_date) > _pay_period_end then _second_saturday
    when arkona.db2_integer_to_date(b.hire_date) between _pay_period_start and _first_saturday
      and arkona.db2_integer_to_date(b.termination_date) < _pay_period_end then arkona.db2_integer_to_date(b.termination_date)

    when arkona.db2_integer_to_date(b.hire_date) between _first_saturday + 1 and _pay_period_end
      and arkona.db2_integer_to_date(b.termination_date) < _pay_period_end then arkona.db2_integer_to_date(b.termination_date)
  end as "Record Date",  
  'ADD' as "Operation", 
  case
    when guarantee > "Total" then guarantee
    else "Total"   
  end as "Value"  
from tp.gm_flat_rate_for_approval a
left join arkona.ext_pymast b	on a."Employee Id" = b.pymast_employee_number	
where guarantee is not null
order by "Employee Id"


  
/*
-- drop table if exists tp.blueprint_guarantee_rates cascade;
-- create table tp.blueprint_guarantee_rates (
--   employee_number citext,
--   name citext,
--   hourly_rate numeric(6,2),
--   pto_rate numeric(6,2),
--   from_date date,
--   thru_date date);
-- 
-- insert into tp.blueprint_guarantee_Rates values('145801','flaat, gavin',35.98,36.09,'01/01/2021','02/13/2021');
-- insert into tp.blueprint_guarantee_Rates values('1106399','olson, jay',38.56,38.68,'01/01/2021','02/13/2021');
-- insert into tp.blueprint_guarantee_Rates values('1117901','rogers, mitch',26.79,24.8,'01/01/2021','02/13/2021');
-- insert into tp.blueprint_guarantee_Rates values('152410','gray, nathan',29.62,29.8,'01/01/2021','02/13/2021');
-- insert into tp.blueprint_guarantee_Rates values('145801','flaat, gavin',38,36.98,'02/14/2021','12/31/9999');
-- insert into tp.blueprint_guarantee_Rates values('1106399','olson, jay',40,41.98,'02/14/2021','12/31/9999');
-- insert into tp.blueprint_guarantee_Rates values('1117901','rogers, mitch',31,27.71,'02/14/2021','12/31/9999');
-- insert into tp.blueprint_guarantee_Rates values('152410','gray, nathan',32,30.92,'02/14/2021','12/31/9999');

*/
