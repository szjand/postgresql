﻿/*
12/08/21
after talking to andrew, seems he would actually like to see what he currently gets on the spreadsheet
so, this is a reworking of the manager summary spreadsheet

12/09/21
this table should contain the base data to satisfy all purposes
which means that this query populates a table that has EVERYTHING
subsequently
	manager approval page
	import file generation
	manager oversight page
	tech page
ALL query this table for the requ'd data

so, need to include 
  emp#
	dates (pay period, hire, term)
	all rates
	all hours (flag, adjustment, pto, holiday, clock, bonus)
	all pay components

so, the import file would query this table unioned with the adjustments (from approval/submittal page)
i think it is ok for the import to have multiple lines for the same employee, counter

afton seems to understand and agrees
so
need a table for all fields

these are the fields needed for andrews approval/submittal page:
		SELECT team,last_name || ', ' || first_name as employee,
			-- employee_number::integer AS "emp #",ptorate,
			commission_rate,bonus_rate,team_prof,
			flag_hours,adjustments,total_flag_hours,clock_hours,--vacationhours,ptohours,
		--   holidayhours,total
		--   CASE
		--     WHEN guarantee IS NULL THEN total
		-- 	WHEN guarantee > total THEN guarantee
		-- 	ELSE total
		--   END AS "total pay" , 
			commission_pay, bonus_pay,guarantee

and now, change the query below to generate the all-data table			
*/

/*
conversion from arkona to ukg notes
  fucking advantage tpdata is the source of everything
	arkona has vac_hours, ukg will not
*/	

-- this is kicking my ass trying to include "everything" the tech daily page is the problem
-- limit "everything" to payroll

select * from dds.dim_date where the_date = current_Date
-- select * from tp.gm_main_shop_flat_rate_payroll_data where pay_period_seq = 337 order by team, first_name

select * from tp.data where employee_number = '1137590' and the_date = '11/20/2021'

-- delete from tp.gm_main_shop_flat_rate_payroll_data where pay_period_seq = 337
-- drop table if exists tp.gm_main_shop_flat_rate_payroll_data;
-- create table tp.gm_main_shop_flat_rate_payroll_data as

select * from tp.gm_main_shop_flat_rate_payroll_data where employee_number = '164015' and pay_period_seq = 336

comment on table tp.gm_main_shop_flat_rate_payroll_data is 'table for storing all necessary gm main shop flat rate data to generate ukg import files and manager approval pages';
comment on function tp.update_gm_main_shop_flat_rate_payroll_data(integer) is 'updates table tp.gm_main_shop_flat_rate_payroll_data, probably called
	from a luigi task on the first day of biweekly pay period for the previous pay period';
comment on function tp.get_gm_flat_rate_managers_payroll_approval_data(integer) is 'generate the data necessary for the managers approval/submittal page in Vision';
comment on function tp.create_gm_flat_rate_tlm_import_file(integer) is 'generate data for the ukg tlm import file';


create or replace function tp.update_gm_main_shop_flat_rate_payroll_data(_pay_period_seq integer)
  returns void 
as
$BODY$
/*
  select tp.update_gm_main_shop_flat_rate_payroll_data(338)
*/
declare 
	_pay_period_start date := (
	  select min(the_date)
	  from dds.dim_date
	  where biweekly_pay_period_sequence = _pay_period_seq);
  _pay_period_end date := (
		select max(the_date)
		from dds.dim_date
		where biweekly_pay_period_sequence = _pay_period_seq);
begin 		
  delete 
  from  tp.gm_main_shop_flat_rate_payroll_data
  where pay_period_seq = _pay_period_seq;
  
	insert into tp.gm_main_shop_flat_rate_payroll_data
	select dd.*,
		comm_hours * team_prof/100 * flat_rate as comm_pay,
		bonus_hours * team_prof/100 * bonus_rate as bonus_pay,
		pto_rate * (pto_hours + hol_hours) as pto_pay,
		case
			when coalesce(guarantee, 0) > comm_hours * team_prof/100 * flat_rate 
				+ bonus_hours * team_prof/100 * bonus_rate
				+ pto_rate * (pto_hours + hol_hours) then guarantee
			else comm_hours * team_prof/100 * flat_rate
				+ bonus_hours * team_prof/100 * bonus_rate
		end as total_pay
	from ( -- if there are no bonus hours, comm_hours = clock_hours
		select cc.*,
			case
				when cc.total_hours > 90 and cc.team_prof >= 100 then cc.clock_hours - (total_hours -90)
				else cc.clock_hours
			end as comm_hours,
			case
				when cc.total_hours > 90 and team_prof >= 100 then total_hours - 90
				else 0
			 end as bonus_hours
		from (
			select aa.*, bb.guarantee 
			from ( -- tech data
				select pay_period_start, pay_period_end, pay_period_seq, pay_period_select_format,
					pay_period_start + 6 as first_saturday,
					c.team_name AS Team, last_name, first_name, employee_number, 
					arkona.db2_integer_to_date(d.hire_date) as hire_date,
					arkona.db2_integer_to_date(d.termination_date) as term_date,
					tech_hourly_rate AS pto_rate, tech_tfr_rate AS flat_rate, 
					2 * tech_tfr_rate AS bonus_rate,
					tech_flag_hours_pptd as flag_hours, tech_Flag_Hour_Adjustments_PPTD as Adjustments,
					tech_flag_hours_pptd + tech_Flag_Hour_Adjustments_PPTD as total_flag_hours,
					team_prof_pptd AS team_prof,
					tech_clock_hours_pptd AS clock_hours, 
					(tech_vacation_hours_pptd + tech_pto_hours_pptd) AS pto_hours, 
					tech_holiday_hours_pptd AS hol_hours,
					tech_clock_hours_pptd + tech_vacation_hours_pptd + tech_pto_hours_pptd + tech_holiday_hours_pptd AS total_hours            
				FROM tp.data a
				INNER JOIN tp.team_techs b on a.tech_key = b.tech_key
					AND a.team_key = b.team_key
				AND _pay_period_end BETWEEN b.from_date AND b.thru_date
				LEFT JOIN tp.teams c on b.team_key = c.team_key
				left join arkona.ext_pymast d on a.employee_number = d.pymast_employee_number
				WHERE the_date = _pay_period_end
					AND a.department_Key = 18) aa
			left join (		
				SELECT employee_number, reg + ot + pto + bonus AS guarantee
				FROM ( -- blueprint team guarantees
					SELECT a.name, a.employee_number, a.hourly_rate * b.clock AS reg, 
						a.hourly_rate * 1.5 * b.ot AS ot, 
						a.pto_rate * (vac + pto + hol) AS pto,
						case
							when b.clock + b.ot + b.pto > 90 THEN  ((b.clock + b.ot + b.pto) - 90) * 2 * a.hourly_rate
						ELSE 0
						END AS bonus
					FROM tp.blueprint_guarantee_rates a
					JOIN (			 
						select c.employee_number, SUM(a.reg_hours) AS clock, SUM(a.ot_hours) AS ot,
							SUM(a.vac_hours) AS vac, SUM(a.pto_hours) AS pto, SUM(a.hol_hours) AS hol 
						FROM arkona.xfm_pypclockin a
						JOIN dds.dim_date b on a.the_date = b.the_date
							AND b.the_date BETWEEN _pay_period_start AND _pay_period_end
						join tp.blueprint_guarantee_rates c on a.employee_number = c.employee_number
							and c.thru_date > current_date
						GROUP BY c.employee_number) b on a.employee_number = b.employee_number
					WHERE a.thru_date = '12/31/9999') d) bb on aa.employee_number = bb.employee_number) cc) dd;
end
$BODY$
language plpgsql;
				

-----------------------------------------------------------------------
-- manager approval/submittal page
-----------------------------------------------------------------------
--  DROP FUNCTION tp.get_gm_flat_rate_managers_payroll_approval_data(integer);
create or replace function tp.get_gm_flat_rate_managers_payroll_approval_data(_pay_period_seq integer)	
returns table (pay_period_select_format citext,team citext, tech citext, employee_number citext,flat_rate 
	numeric, bonus_rate numeric, team_prof numeric, flag_hours numeric, adjustments numeric, 
	total_flag_hours numeric, clock_hours numeric, pro_hours NUMERIC, hol_hours numeric, 
	total_hours numeric, comm_hours numeric, comm_pay numeric, 
	bonus_hours numeric, bonus_pay numeric, guarantee numeric, total numeric)
as
$BODY$
/*
  it is necessary to include pto and hol hours because they contribute to total_hours which is used to figure bonus
  example: (includes pto, holiday & bonus, o'neil 1106421, pp_seq = 338)
	select * from tp.gm_main_shop_flat_rate_payroll_data where pay_period_seq = 338 and employee_number = '1106421'
	clock  88.60
	pto     4.00
	hol     8.00
	total 100.60
	comm hours = 88.6 - (100.6 - 90) = 78 (when there is no bonus, clock_hours = comm_hours)
	bonus hours = 100.6 - 90 = 10.6
	comm_pay = comm_hours * flat_rate * prof = 78 * 23.50 * 1.3692 = 2509.74
	bonus pay = bonus_hours * bonus_rate * prof = 10.6 * 47 * 1.3692 = 682.14
	total for tlm = comm_pay + bonus_pay = 3191.88    
	
  select * from tp.get_gm_flat_rate_managers_payroll_approval_data(338) order by tech ;
*/
		select pay_period_select_format,  team, (last_name || ', ' || first_name)::citext as tech, 
			employee_number, flat_rate,bonus_rate,team_prof,
			flag_hours,round(adjustments,2) as adjustments, 
			round(total_flag_hours,2) as total_flag_hours,round(clock_hours,2) as clock_hours, 
			pto_hours, hol_hours, total_hours,
			round(comm_hours, 2), comm_pay, bonus_hours, bonus_pay, round(guarantee,2),
			round(
					case
					when guarantee is null then comm_pay + bonus_pay
					when guarantee > comm_pay + bonus_pay + pto_pay then guarantee
					else comm_pay + bonus_pay
				end, 2) as total 
		from tp.gm_main_shop_flat_rate_payroll_data
		where pay_period_seq = _pay_period_seq
		order by team, last_name; 
$BODY$
language sql;


-----------------------------------------------------------------------
-- import file
-----------------------------------------------------------------------
-- flat rate comp
create or replace function tp.create_gm_flat_rate_tlm_import_file(_pay_period_seq integer)
returns table("Employee Id" citext,"EIN Name" text,"Counter Name" text,"Record Date" date,
	"Operation" text, "Value" numeric)
as
$BODY$
/*
  select * from tp.create_gm_flat_rate_tlm_import_file(337);
*/

select employee_number as "Employee Id", 'Rydell Auto Center, Inc.' as "EIN Name", 'Flat Rate Compensation' as "Counter Name",
  case
    when hire_date <= pay_period_start and term_date >= pay_period_end then pay_period_end
    when hire_date <= pay_period_start and term_date < pay_period_end then term_date
    when hire_date between pay_period_start and first_saturday and term_date > pay_period_end then pay_period_end
    when hire_date between pay_period_start and first_saturday and term_date < pay_period_end then term_date
    when hire_date between first_saturday + 1 and pay_period_end and term_date < pay_period_end then term_date
  end as "Record Date",  
  'ADD' as "Operation", comm_pay as "Value"  
from tp.gm_main_shop_flat_rate_payroll_data
where pay_period_seq = _pay_period_seq
  and  guarantee is null
union  
-- flat rate premium (bonus pay)
select employee_number as "Employee Id", 'Rydell Auto Center, Inc.' as "EIN Name", 'Flat Rate Premium Pay' as "Counter Name",
  case
    when hire_date <= pay_period_start and term_date >= pay_period_end then pay_period_end
    when hire_date <= pay_period_start and term_date < pay_period_end then term_date
    when hire_date between pay_period_start and first_saturday and term_date > pay_period_end then pay_period_end
    when hire_date between pay_period_start and first_saturday and term_date < pay_period_end then term_date
    when hire_date between first_saturday + 1 and pay_period_end and term_date < pay_period_end then term_date
  end as "Record Date",  
  'ADD' as "Operation", bonus_pay as "Value"  
from tp.gm_main_shop_flat_rate_payroll_data
where pay_period_seq = _pay_period_seq
  and  guarantee is null
  and bonus_pay <> 0
union  
-- guarantee (blueprint team)  
select employee_number as "Employee Id", 'Rydell Auto Center, Inc.' as "EIN Name", 
  case
    when guarantee > comm_pay + bonus_pay + pto_pay then 'Biweekly Guarantee'
    else 'Flat Rate Compensation'
  end as "Counter Name",
  case
    when hire_date <= pay_period_start and term_date >= pay_period_end then pay_period_end
    when hire_date <= pay_period_start and term_date < pay_period_end then term_date
    when hire_date between pay_period_start and first_saturday and term_date > pay_period_end then pay_period_end
    when hire_date between pay_period_start and first_saturday and term_date < pay_period_end then term_date
    when hire_date between first_saturday + 1 and pay_period_end and term_date < pay_period_end then term_date
  end as "Record Date",  
  'ADD' as "Operation", 
  case
    when guarantee > total_pay then guarantee
    else total_pay   
  end as "Value"  
from tp.gm_main_shop_flat_rate_payroll_data
where pay_period_seq = _pay_period_seq
  and guarantee is not null;
$BODY$
language sql;  

