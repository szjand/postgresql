﻿FUNCTION bspp.create_ukg_import_file(_pay_period_id integer)

-- afton uses this one
FUNCTION bspp.create_body_shop_estimators_tlm_import_file(IN _pay_period_id integer)


Jeri texted me and said bs estimators were not showing in the spreadsheet. 
She was correct, they show on the page but not the sheet. long story short.. 
the function that makes the import bspp.create_estimators_import_file 
references the table bspp.ukg_1. I found the function bspp.update_ukg_import1 
and updated for the current pay period. I hope I didn’t screw anythjng up. 
I have a note to look at the estimators functions and correct.
Sorry if the function names are way off. I am away from my computer and trying to remember the names


FUNCTION bspp.populate_ukg_import_1(_pay_period_id integer)
-> purges and populates table bspp.ukg_import_1 for the _pay_period_id with data from bspp.bs_sales_detail
table ukg_import_1 is queried/consumed by FUNCTION bspp.populate_ukg_import_1(_pay_period_id integer) by the page to generate the tlm data


*** idea to upgrade friendly files
friendly version of the complete tlm or payroll import file with pay plan name along with employee name

rename FUNCTION bspp.create_ukg_import_file(integer);

alter function bspp.create_ukg_import_file(integer)
rename to z_unused_create_ukg_import_file

alter TABLE bspp.ukg_import_file
rename to z_unused_ukg_import_file


COMMENT ON TABLE bspp.ukg_import_1
  IS 'the first table generated in the production of the ukg TLM import file, table is populated by the manager approval page call to function bspp.populate_ukg_import_1';
