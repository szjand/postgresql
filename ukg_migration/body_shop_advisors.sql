﻿/*
12/6/21
feels like where i am spinning out is not separating the import file from the page(s)
there is a page for manager approval
and there is a page for employee visibility

so, let's just do the import today
barring something different for loren schereck (salaried) this is a TLM import
*/
lets, like afton do functions
having a hard time juggling this all in my head
so, the first function: relevant apy period dates

create or replace function jon.pay_period_dates (in _pp integer)
  returns table(beg_pp date, sec_sat date, first_sat date) as
$BODY$  
/*
select jon.pay_period_dates ((select distinct biweekly_pay_period_sequence from dds.dim_date where biweekly_pay_period_start_date = '11/07/2021'));
*/
  select min(the_date) as first_day, max(the_date) as sec_sat, min(the_date) filter (where day_of_week = 7) as first_sat
  from dds.dim_date
  where biweekly_pay_period_sequence = _pp
$BODY$
  LANGUAGE sql;
comment on function jon.pay_period_dates(integer) is 'function to populate table bspp.ukg_import_1 with the relevant data for generating the ukg TLM import';
select jon.pay_period_dates ((select distinct biweekly_pay_period_sequence from dds.dim_date where biweekly_pay_period_start_date = '11/07/2021'));
   
select * from dds.dim_Date where the_date = '11/07/2021'

drop table if exists bspp.ukg_import_1 cascade;
create table bspp.ukg_import_1 (
  pay_period_id integer not null,
  employee_number citext not null,
  commission_pay numeric(6,2) not null,
  primary key(pay_period_id,employee_number));
comment on table bspp.ukg_import_1 is 'the first table generated in the production of the ukg TLM import file, table is populated by the manager approval page call to function bspp.populate_ukg_import_1';


create or replace function bspp.populate_ukg_import_1(_pay_period_id integer) 
	returns void as
$BODY$
/*
select bspp.populate_ukg_import_1(337);
*/
  delete from bspp.ukg_import_1 where pay_period_id = _pay_period_id;
  insert into bspp.ukg_import_1 
  select  _pay_period_id , a.employee_number ,round(b.commission_perc * c.total_sales, 2)
  from bspp.personnel a
  join bspp.personnel_roles b on a.user_name = b.user_name
    and b.thru_date > (select distinct biweekly_pay_period_end_date from dds.dim_date where biweekly_pay_period_sequence = _pay_period_id)
  left join (
      select round(sum(amount) filter (where category like '%labor'), 2) + 
				round(sum(multiplier * amount) filter (where category like '%parts'), 2) as total_sales
			from bspp.bs_sales_detail
			where the_date 
				between 
					(select min(the_date) from dds.dim_date where biweekly_pay_period_sequence = _pay_period_id) 
			  and 
			    (select max(the_date) from dds.dim_date where biweekly_pay_period_sequence = _pay_period_id)) c on  true;
$BODY$
LANGUAGE sql;
comment on function bspp.populate_ukg_import_1(integer) is 'populates table bspp.ukg_import_1 for generating ukg TLM import file, called from managers approval page';
-- select * from bspp.ukg_import_1;


CREATE OR REPLACE FUNCTION jon.get_record_dates(
    IN _pay_period_id integer,
    IN _hire_date date,
    IN _term_date date)
  RETURNS TABLE(record_id integer, record_date date) AS
$BODY$
/*
select * from jon.get_record_dates(337,'11/01/2021', '11/08/2021')
*/
	select  1,
		case 
			when _hire_date < beg_pp and _term_date > sec_sat then first_sat
			when _hire_date < beg_pp and _term_date between beg_pp and first_sat then _term_date
			when _hire_date < beg_pp and _term_date > first_sat and _term_date <= sec_sat then first_sat
			when _hire_date between beg_pp and first_sat and _term_date > sec_sat then first_sat
			when _hire_date between beg_pp and first_sat and _term_date between beg_pp and first_sat then _term_date
			when _hire_date between beg_pp and first_sat and _term_date > first_sat and _term_date <= sec_sat then first_sat
			when _hire_date > first_sat and _hire_date <= sec_sat and _term_date > first_sat and _term_date <= sec_sat then _term_date
		end as first_record_date
	from bspp.ukg_import_1(_pay_period_id)
	union
	select 2,
		case 
			when _hire_date < beg_pp and _term_date > sec_sat then sec_sat
			when _hire_date < beg_pp and _term_date > first_sat and _term_date <= sec_sat then _term_date
			when _hire_date between beg_pp and first_sat and _term_date > sec_sat then sec_sat
			when _hire_date between beg_pp and first_sat and _term_date > first_sat and _term_date <= sec_sat then _term_date
			when _hire_date > first_sat and _hire_date <= sec_sat and _term_date > sec_sat then sec_sat
			when _hire_date < beg_pp and _term_date > sec_sat then first_sat
			when _hire_date < beg_pp and _term_date between beg_pp and first_sat then _term_date
			when _hire_date < beg_pp and _term_date > first_sat and _term_date <= sec_sat then first_sat
			when _hire_date between beg_pp and first_sat and _term_date > sec_sat then first_sat
			when _hire_date between beg_pp and first_sat and _term_date between beg_pp and first_sat then _term_date
			when _hire_date between beg_pp and first_sat and _term_date > first_sat and _term_date <= sec_sat then first_sat
			when _hire_date > first_sat and _hire_date <= sec_sat and _term_date > first_sat and _term_date <= sec_sat then _term_date
		end second_record_date
		from bspp.ukg_import_1(_pay_period_id);
$BODY$
LANGUAGE sql;
comment on function jon.get_record_dates(integer,date,date) is 'generates the relevant dates for creating the ukg TLM import file';


drop table if exists bspp.ukg_import_file cascade;
create table bspp.ukg_import_file (
	pay_period_id integer not null,
  employee_number citext not null,
  ein_name citext not null, 
  counter_name citext not null,
  record_date date not null,
  operation citext not null,
  amount numeric(6,2),
  primary key (employee_number, counter_name, record_date));
comment on table bspp.ukg_import_file is 'table for storing the ukg TLM import file data for body shop estimators';

create or replace function bspp.create_ukg_import_file(_pay_period_id integer)
  returns void as
$BODY$
/*
select bspp.create_ukg_import_file(337);
*/
	delete from bspp.ukg_import_file where pay_period_id = _pay_period_id;
	insert into bspp.ukg_import_file 
	with 
		employees_comm_half as (
			select 1 as id, employee_number, round(commission_pay/2, 2) as half_pay, commission_pay as total_pay
			from bspp.ukg_import_1
			where pay_period_id = 337
  		union
			select 2 as id, employee_number,   commission_pay - round(commission_pay/2, 2) as other_half_pay, commission_pay
			from bspp.ukg_import_1
			where pay_period_id = 337 )

  select _pay_period_id,
	employee_number as "Employee Id", 
  'Rydell Auto Center, Inc.' as "EIN Name", 
  'Biweekly Commission' as "Counter Name",
  record_date as "Record Date",  
  'ADD' as "Operation", sum(half_pay) as "Value"
  from employees_comm_half a
  left join arkona.ext_pymast c on a.employee_number = c.pymast_employee_number
  inner join jon.get_record_dates(_pay_period_id,arkona.db2_integer_to_date(c.hire_date), arkona.db2_integer_to_date(c.termination_date) ) b on a.id = b.record_id
  group by employee_number, record_date;
$BODY$
LANGUAGE sql;
comment on function bspp.create_ukg_import_file(integer) is 'stores the data for the ukg TLM imports into table bspp.ukg_import_file';

select * from bspp.ukg_import_file;

-- for the approval page
-- drop function bspp.get_commission_for_approval(_pay_period_id integer);
create or replace function bspp.get_commission_for_approval(_pay_period_id integer)
  returns table(last_name citext, first_name citext, commission_perc numeric, total_sales numeric, commission_pay numeric) as
$BODY$
/*
select * from bspp.get_commission_for_approval(337);
*/
  select a.last_name, a.first_name, 
    b.commission_perc,
     c.all_labor + c.parts as total_sales,
     round(b.commission_perc * (c.all_labor + c.parts), 2) as commission_pay
  from bspp.personnel a
  inner join bspp.personnel_roles b on a.user_name = b.user_name
    and b.thru_date >  (select max(the_date) from dds.dim_date where biweekly_pay_period_sequence = _pay_period_id)
  left join (
    select round(sum(amount) filter (where category like '%labor'), 2) as all_labor,
      round(sum(multiplier * amount) filter (where category like '%parts'), 2) as parts
    from bspp.bs_sales_detail
    where the_date 
				between 
					(select min(the_date) from dds.dim_date where biweekly_pay_period_sequence = _pay_period_id) 
			  and 
			    (select max(the_date) from dds.dim_date where biweekly_pay_period_sequence = _pay_period_id)) c on true; 
$BODY$
language sql;
comment on function bspp.get_commission_for_approval(integer) is 'returns data to populate the manager approval page';

select * from bs_payroll

select * from jon.get_record_dates(337,'11/01/2021', '11/08/2021')






























do $$
declare _from_date date := '11/07/2021';
declare _thru_date date := '11/21/2021';
/*
for the import all i need are emp#, commission pay, hire_Date, term_Date, pay period id
*/
begin
drop table if exists bs_payroll;
create temp table bs_payroll as


  select a.last_name, a.first_name, a.employee_number as "emp#", 
    b.commission_perc as "commission %", 
    total_sales,
    case 
      when b.the_role = 'parts' then round(b.commission_perc * c.total_sales, 2)
      when b.the_role = 'shop_foreman' then round(b.commission_perc * c.total_sales, 2)  
      when b.the_role in ('vda','csr') then round(b.commission_perc * c.total_sales, 2)    
    end as commission_pay,
    _from_date, _thru_date, 
    (select the_date from dds.dim_date where day_name = 'saturday' and the_date between _from_date and _thru_date order by the_date limit 1) as first_saturday,
    (select the_date from dds.dim_date where day_name = 'saturday' and the_date between _from_date and _thru_date order by the_date desc limit 1) as second_saturday,
    arkona.db2_integer_to_date(d.hire_date) as hire_date, arkona.db2_integer_to_date(d.termination_date) as term_date
  from bspp.personnel a
  join bspp.personnel_roles b on a.user_name = b.user_name
    and b.thru_date >= _thru_Date 
  left join (
      select round(sum(amount) filter (where category like '%labor'), 2) + 
				round(sum(multiplier * amount) filter (where category like '%parts'), 2) as total_sales
			from bspp.bs_sales_detail
			where the_date between _from_date and _thru_date) c on  true   
  left join arkona.ext_pymast d on a.employee_number = d.pymast_employee_number;


end $$;

select * from bs_payroll order by last_name;

----------------------------------------------------------------------------------------------------------------
do $$
declare _from_date date := '11/07/2021';
declare _thru_date date := '11/21/2021';

begin
	with employees_comm_half as (
	select 1 as id, employee_number, round(commission_pay/2, 2) as half_pay, commission_pay as total_pay
	from sap.payroll_submittals
	where pay_period_id = 337
	union
	select 2 as id, employee_number,   commission_pay - round(commission_pay/2, 2) as other_half_pay, commission_pay
	from sap.payroll_submittals
	where pay_period_id = 337 )







-- 12/4/21 this will be the same as afton's main shop advisors

create a base table with all required values

drop table if exists ukg.body_shop_estimator_payroll_import_base;
create table ukg.body_shop_estimator_payroll_import_base (
  "Employee Id" citext not null,
  "EIN Name" citext not null,
  "Counter Name" citext not null,
  "Record Date" date not null,
  "Operation" citext not null,
  "Value" numeric(6,2) not null,
  total_sales numeric(8,2) not null);

  
thinking of a supplemental table for adjustments, but if there is an adjustment, in this case, of Biweekly Commission,
all those amounts would need to be totalled and split, or when entering an adjustment, does it need to be for
a particular week?

