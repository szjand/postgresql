﻿/*
this is the kim payroll query in postgresql
*/
  SELECT round(clockhours, 2) AS actual_clock_hours,
    trim(last_name) || ', ' || trim(first_name) AS Name, employee_number, --AS [Emp#], 
    round(commrate, 2),--AS [Comm Rate],
    round(teamprof*clockhours/100, 2),-- AS [Clock Hours],
    round(round(commrate, 2)*round(teamprof*clockhours/100, 2), 2) as total_commission_pay,-- AS [Total Comm Pay],
    HourlyRate,-- AS [Vac PTO Hol Rate],
    vacationhours + ptohours,-- AS [Vac PTO Hours],
    round(hourlyrate * (vacationhours + ptohours), 2),-- AS [Vac PTO Pay],
    HolidayHours,-- AS [Hol Hours],
    round(hourlyrate * holidayhours, 2),-- AS [Hol Pay],
-- 	a.adjustment AS [Adj],
    round(round(commrate, 2)*round(teamprof*clockhours/100, 2), 2)
        + round(hourlyrate * holidayhours, 2)
        + round(hourlyrate * (vacationhours + ptohours), 2) 
-- 		+ a.adjustment AS [Total Gross Pay]    
  FROM (
    select last_name, first_name, employee_number,  
      tech_hourly_rate AS HourlyRate, tech_tfr_rate AS CommRate, 
      tech_clock_hours_pptd AS ClockHours, Tech_Flag_Hours_PPTD as FlagHours, team_prof_pptd AS TeamProf, 
      tech_vacation_hours_pptd AS VacationHours,
      tech_pto_hours_pptd AS PTOHours, tech_holiday_hours_pptd AS HolidayHours,
      round(tech_clock_hours_pptd * tech_tfr_rate * team_prof_pptd/100, 2) + 
        (tech_vacation_hours_pptd + tech_pto_hours_pptd + 
          tech_holiday_hours_pptd)*tech_hourly_rate AS CommPay,
       round((tech_clock_hours_pptd + tech_vacation_hours_pptd + tech_pto_hours_pptd + 
         tech_holiday_hours_pptd)*tech_hourly_rate, 2) AS TransitionPay--, c.adjustment
-- select a.*         
    FROM tp.data a
    INNER JOIN tp.team_techs b on a.tech_key = b.tech_key
      AND a.team_key = b.team_key
      AND b.thru_date >= '12/04/2021' -- @thruDate
    WHERE the_date = '12/04/2021' -- @thruDate
      AND department_Key = 13) a 
where round(round(commrate, 2)*round(teamprof*clockhours/100, 2), 2) <> 0
order by name

/*
remove unnecessary fields
*/

select -- last_name, first_name, 
	a.employee_number, c.hire_date, c.term_date,
	round(round(tech_tfr_rate, 2)*round(team_prof_pptd*tech_clock_hours_pptd/100, 2), 2) as total_commission_pay     
FROM tp.data a
JOIN tp.team_techs b on a.tech_key = b.tech_key
	AND a.team_key = b.team_key
	AND b.thru_date >= '11/20/2021' -- @thruDate
join (
  select pymast_employee_number, arkona.db2_integer_to_date(hire_date) as hire_date,
    arkona.db2_integer_to_date(termination_date) as term_date
  from arkona.ext_pymast) c on a.employee_number = c.pymast_employee_number	
WHERE the_date = '11/20/2021' -- @thruDate
	AND department_Key = 13
	and round(round(tech_tfr_rate, 2)*round(team_prof_pptd*tech_clock_hours_pptd/100, 2), 2) <> 0
order by employee_number

-- add the hire/termdate calcs

DROP TABLE if exists tp.ukg_import_file cascade;
CREATE TABLE tp.ukg_import_file (
  pay_period_id integer NOT NULL,
  employee_number citext NOT NULL,
  ein_name citext NOT NULL,
  counter_name citext NOT NULL,
  record_date date NOT NULL,
  operation citext NOT NULL,
  amount numeric(6,2),
  CONSTRAINT ukg_import_file_pkey PRIMARY KEY (employee_number, counter_name, record_date));
COMMENT ON TABLE tp.ukg_import_file
  IS 'table for storing the ukg TLM import file data for body shop flat rate (team pay)';

select * from dds.dim_date where the_date = current_date

create or replace function tp.create_ukg_import_file(_pay_period_id integer)
  returns void as
$BODY$
/*
select tp.create_ukg_import_file(337);
*/
	delete from tp.ukg_import_file where pay_period_id = _pay_period_id;
	insert into tp.ukg_import_file 
	select _pay_period_id, employee_number as "Employee Id", 'Rydell Auto Center, Inc.' as "EIN Name", 'Flat Rate Compensation' as "Counter Name",
		case
			when c.hire_date <= d.pay_period_start and c.term_date >= d.pay_period_end then d.pay_period_end
			when c.hire_date <= d.pay_period_start and c.term_date < d.pay_period_end then c.term_date

			when c.hire_date between d.pay_period_start and d.first_saturday and c.term_date > d.pay_period_end then d.pay_period_end
			when c.hire_date between d.pay_period_start and d.first_saturday and c.term_date < d.pay_period_end then c.term_date

			when c.hire_date between d.first_saturday + 1 and d.pay_period_end and c.term_date < d.pay_period_end then c.term_date
		end as "Record Date",  
		'ADD' as "Operation",
		round(round(tech_tfr_rate, 2)*round(team_prof_pptd*tech_clock_hours_pptd/100, 2), 2) as "Value"
	FROM tp.data a
	JOIN tp.team_techs b on a.tech_key = b.tech_key
		AND a.team_key = b.team_key
		AND b.thru_date >= '11/20/2021' -- @thruDate
	join (
		select pymast_employee_number, arkona.db2_integer_to_date(hire_date) as hire_date,
			arkona.db2_integer_to_date(termination_date) as term_date
		from arkona.ext_pymast) c on a.employee_number = c.pymast_employee_number	
  join (
    select distinct biweekly_pay_period_start_date as pay_period_start, 
      biweekly_pay_period_end_date as pay_period_end,
      biweekly_pay_period_start_date + 6 as first_saturday
    from dds.dim_date
    where biweekly_pay_period_sequence = _pay_period_id) d on true	
	WHERE a.the_date = d.pay_period_end
		AND a.department_Key = 13;
$BODY$		
language sql;

select * from tp.ukg_import_file 