﻿/*
12/10/21
after rethinking main shop, the managers spreadsheet is more appropriate for the mgrs page
so here we go with v2 of body shop flat rate
mimc the main shop scripts

*/

comment on table tp.body_shop_flat_rate_payroll_data is 'table for storing all necessary body shop flat rate data to generate ukg import files and manager approval pages';
comment on function tp.update_body_shop_flat_rate_payroll_data(integer) is 'updates table tp.update_body_shop_flat_rate_payroll_data, probably called
	from a luigi task on the first day of biweekly pay period for the previous pay period';
comment on function tp.get_body_shop_flat_rate_managers_payroll_approval_data(integer) is 'generate the data necessary for the managers approval/submittal page in Vision';
comment on function tp.create_body_shop_flat_rate_tlm_import_file(integer) is 'generate data for the ukg tlm import file';



-- select * from dds.dim_date where biweekly_pay_period_sequence = 336
create or replace function tp.update_body_shop_flat_rate_payroll_data(_pay_period_seq integer)
	returns void 
as
$BODY$
/*
  select tp.update_body_shop_flat_rate_payroll_data(337);
*/	
declare 
	_pay_period_start date := (
	  select min(the_date)
	  from dds.dim_date
	  where biweekly_pay_period_sequence = _pay_period_seq);
  _pay_period_end date := (
		select max(the_date)
		from dds.dim_date
		where biweekly_pay_period_sequence = _pay_period_seq);
begin 		
  delete 
  from  tp.body_shop_flat_rate_payroll_data
  where pay_period_seq = _pay_period_seq;

  insert into tp.body_shop_flat_rate_payroll_data
	select pay_period_start, pay_period_end, pay_period_seq, pay_period_select_format,
		pay_period_start + 6 as first_saturday,
		c.team_name AS team, last_name, first_name, employee_number, 
		arkona.db2_integer_to_date(d.hire_date) as hire_date,
		arkona.db2_integer_to_date(d.termination_date) as term_date,      
		tech_hourly_rate AS pto_rate, round(tech_tfr_rate, 2) AS flat_rate,
		Tech_Flag_Hours_PPTD as flag_hours, team_prof_pptd AS team_prof,
		tech_clock_hours_pptd AS clock_hours,   
		tech_clock_hours_pptd*Team_Prof_PPTD/100 as comm_hours, 
		tech_tfr_rate * Team_Prof_PPTD * tech_clock_hours_pptd/100 AS comm_pay
	FROM tp.data a
	INNER JOIN tp.team_techs b on a.tech_key = b.tech_key
		AND a.team_key = b.team_key
		AND b.thru_Date >= _pay_period_end
	LEFT JOIN tp.Teams c on b.team_Key = c.team_Key
	left join arkona.ext_pymast d on a.employee_number = d.pymast_employee_number
	WHERE the_date = _pay_period_end
		AND a.department_Key = 13;
end
$BODY$
language plpgsql;

-----------------------------------------------------------------------
-- manager approval/submittal page
-----------------------------------------------------------------------
--  DROP FUNCTION tp.get_managers_payroll_approval_data(integer);
create or replace function tp.get_body_shop_flat_rate_managers_payroll_approval_data(_pay_period_seq integer)	
returns table (pay_period_select_format citext,team citext, tech citext, flat_rate numeric,
	team_prof numeric, flag_hours numeric, clock_hours numeric, comm_hours numeric, comm_pay numeric)
as
$BODY$
/*
  select * from tp.get_body_shop_flat_rate_managers_payroll_approval_data(338);
*/
		select pay_period_select_format,  team, (last_name || ', ' || first_name)::citext as tech, 
			flat_rate,team_prof, flag_hours,
			round(clock_hours, 2) as clock_hours, round(comm_hours, 2) as comm_hours, 
			round(comm_pay, 2) as comm_ppay
		from tp.body_shop_flat_rate_payroll_data
		where pay_period_seq = _pay_period_seq
		order by team, last_name; 
$BODY$
language sql;

-----------------------------------------------------------------------
-- import file
-----------------------------------------------------------------------
create or replace function tp.create_body_shop_flat_rate_tlm_import_file(_pay_period_seq integer)
returns table("Employee Id" citext,"EIN Name" text,"Counter Name" text,"Record Date" date,
	"Operation" text, "Value" numeric)
as
$BODY$
/*
  select * from tp.create_body_shop_flat_rate_tlm_import_file(337);
*/

select employee_number as "Employee Id", 'Rydell Auto Center, Inc.'::TEXT as "EIN Name", 'Flat Rate Compensation'::TEXT as "Counter Name",
  case
    when hire_date <= pay_period_start and term_date >= pay_period_end then pay_period_end
    when hire_date <= pay_period_start and term_date < pay_period_end then term_date
    when hire_date between pay_period_start and first_saturday and term_date > pay_period_end then pay_period_end
    when hire_date between pay_period_start and first_saturday and term_date < pay_period_end then term_date
    when hire_date between first_saturday + 1 and pay_period_end and term_date < pay_period_end then term_date
  end as "Record Date",  
  'ADD'::TEXT as "Operation", round(comm_pay, 2) as "Value"  
from tp.body_shop_flat_rate_payroll_data
where pay_period_seq = _pay_period_seq;
$BODY$
language sql;  

