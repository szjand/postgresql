﻿/*
11/03/22 from nick neumann

Good afternoon Jon,
Would there be a way for you to run a Honda Nissan report on the 4NT labor op?  Ben C. suggested it.  
We have been doing a buy 3 get 1 free tire sale for the last 4 years, and we would basically like to 
see what kind of service gross we have gained from the people who have gotten tires here.  It is definitely a 
great retention tool, just want to know the nuts and bolts.  It is painful to see a $190 discount on 4 tires…..  
Let me know if this is possible please
Thanks!

TO answer you questions Jon, yes, for every vehicle/customer we did a 4NT on in the last 4 years, 
how much service business have we done
For headers
Vehicle/customer            Date of 4NT                        Service(parts and labor)  gross since


I just want a very broad overview to know if us selling these cheap ass tires is all we are getting or if the customer/vehicle is coming in for other service
*/

-- because customer numbers for honda are fucked up, base this on vehicles only
-- 11/06 got the customers fixed
-- add full_name & bnkey
drop table if exists tem.vins cascade;
create unlogged table tem.vins as
select c.vin, c.model_year, c.make, c.model, min(open_date) as open_date, d.full_name, d.bnkey
from dds.fact_repair_order a
join dds.dim_opcode b on a.opcode_key = b.opcode_key
  and b.opcode = '4NT'
join dds.dim_vehicle c on a.vehicle_key = c.vehicle_key  
  and length(c.vin) = 17
  and vin not like '0%'
join dds.dim_customer d on a.customer_key = d.customer_key
  and d.bnkey <> 0
where a.ro like '2%'
  and a.open_date > current_date - interval '4 years'  
group by c.vin, c.model_year, c.make, c.model, d.full_name, d.bnkey;

delete from tem.vins where vin in ('5J6RM4H75CL028040','2HGFC1F34GH651521','JN8AE2KP3C9035101','5FNRL6H75JB002168','1HGCV1F55JA030809','1N4AL3AP0HC215032','5FNYF6H55KB049056','5N1AT2MV4FC810781');


-- limit to HN ros
drop table if exists tem.post_4nt_ros cascade;
create unlogged table tem.post_4nt_ros as
select a.ro, b.vin--, a.line, d.payment_type, a.flag_hours, e.service_type
from dds.fact_repair_order a
join dds.dim_vehicle b on  a.vehicle_key = b.vehicle_key
join tem.vins c on b.vin = c.vin
join dds.dim_payment_type d on a.payment_type_key = d.payment_type_key
  and d.payment_type_code in ('c','s','w')
join dds.dim_service_type e on a.service_type_key = e.service_type_key 
  and e.service_type <> 'carwash'
where a.open_date > c.open_date
  and a.ro like '2%'
group by a.ro, b.vin;
create index on tem.post_4nt_ros(ro);

select * from tem.post_4nt_ros

-- select a.amount, b.account, b.account_type, b.description, b.department
drop table if exists tem.post_4nt_gross cascade;
create unlogged table tem.post_4nt_gross as
select a.control,
  sum(-a.amount) filter (where department = 'service') as service_gross,
  sum(-a.amount) filter (where department = 'quick lane') as pdq_gross,
  sum(-a.amount) filter (where department = 'parts') as parts_gross
--   sum(a.amount) filter (where b.account_type = 'sale' and department = 'service') as service_sales,
--   sum(a.amount) filter (where b.account_type = 'sale' and department = 'quick lane') as pdq_sales,
--   sum(a.amount) filter (where b.account_type = 'sale' and department = 'parts') as parts_sales,
--   sum(a.amount) filter (where b.account_type = 'cogs' and department = 'service') as service_cost,
--   sum(a.amount) filter (where b.account_type = 'cogs' and department = 'quick lane') as pdq_cost,
--   sum(a.amount) filter (where b.account_type = 'cogs' and department = 'parts') as parts_cost
from fin.fact_gl a
join tem.post_4nt_ros aa on a.control = aa.ro
join fin.dim_account b on a.account_key = b.account_key
  and b.account_type in ('cogs','sale')
where a.post_status = 'Y'
group by a.control;
create index on tem.post_4nt_gross(control);

select * from tem.post_4nt_gross


-- HN ROs only
-- sent to nick as post_4nt_fixed_gross
drop table if exists tem.final_gross cascade;
create unlogged table tem.final_gross as
select  aa.vin, aa.full_name as customer, aa.open_date as "4NT Date", aa.model_year as year, aa.make, aa.model, 
	coalesce(bb.service_gross, 0) as service_gross, coalesce(bb.pdq_gross, 0) as pdq_gross, coalesce(bb.parts_gross, 0) as parts_gross
from tem.vins aa
left join (
	select a.vin, sum(service_gross) as service_gross, sum(pdq_gross) as pdq_gross, sum(parts_gross) as parts_gross
	from tem.post_4nt_ros a
	join  tem.post_4nt_gross b on a.ro = b.control
	group by a.vin) bb on aa.vin = bb.vin
order by aa.vin

select * from tem.final_gross

-- sum summary for nick
select vin from tem.vins group by vin having count(*) > 1
select count(*) from tem.vins  -- 1547

select count(*) from tem.final_gross where service_gross + pdq_gross + parts_gross = 0  -- 453

select count(*) from tem.final_gross where service_gross + pdq_gross + parts_gross <> 0 -- 1094

select sum(service_gross) as service_gross, sum(pdq_gross) as pdq_gross, sum(parts_gross) as parts_gross from tem.final_gross  -- 330780, 52147, 145264

select sum(service_gross + pdq_gross + parts_gross) from tem.final_gross  -- 539191

select 539191/1547  -- 348

select 539191/1094  -- 492







select a.* from tem.vins a 
left join (
select  bb.vin, bb.full_name as customer, bb.open_date as "4NT Date", bb.model_year as year, bb.make, bb.model, aa.service_gross, aa.pdq_gross, aa.parts_gross
from (
	select a.vin, sum(service_gross) as service_gross, sum(pdq_gross) as pdq_gross, sum(parts_gross) as parts_gross
	from tem.post_4nt_ros a
	left join  tem.post_4nt_gross b on a.ro = b.control
	group by a.vin) aa
join tem.vins bb on aa.vin = bb.vin) b on a.vin = b.vin
