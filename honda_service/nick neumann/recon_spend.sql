﻿select * 
from fin.dim_account where account = '231900'


select * 
from dds.fact_repair_order
where ro = '16598392'

select *
from dds.dim_customer
where customer_key = 2


select a.*, e.description, c.the_date 
select c.year_month, sum(a.amount)::integer
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
  and b.account = '231900'
join dds.dim_date c on a.date_key = c.date_key
  and c.year_month between 202112 and 202311
join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code = 'SVI'  
join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key  
where a.post_status = 'Y'  
  and a.amount > 0
group by c.year_month
order by c.year_month


select c.the_date, e.description, a.control, a.amount
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
  and b.account = '231900'
join dds.dim_date c on a.date_key = c.date_key
  and c.year_month between 202112 and 202311
join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code = 'SVI'  
join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key  
where a.post_status = 'Y'  
  and a.amount > 0
order by a.control  