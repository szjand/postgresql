﻿/*
Jon can you run a report with store name and field 500   
sales would like to know how many employees are leasing.  
In the payroll set up I do have and end date for each lease I do not know if you can get that or not let me know.
Thanks Jon 

I am working from home and they do not have my phone set up yet  my cell is 701-740-4708 if you have questions 

*/

select a.company_number as store, b.employee_name, a.fixed_Ded_amt as lease_amt, 
  arkona.db2_integer_to_date(a.bank_account) as end_date
-- select *  
from arkona.ext_pydeduct a
join arkona.xfm_pymast b on a.employee_number = b.pymast_employee_number
  and b.active_code <> 'T'
  and b.current_row
where a.ded_pay_code = '500'
  and arkona.db2_integer_to_date(a.bank_account) > current_date
order by a.company_number, b.employee_name

/*
Jon can you so a report for September with what we paid employees for the lease payments?  
I post them to  field 500.   Laura is looking for about how much we are paying out each month on the leases

Kim
*/
select *
from arkona.ext_pyhscdta
where payroll_cen_year = 119
  and code_id = '500'


select max(payroll_end_date) from arkona.ext_pyptbdta  


select a.check_month, a.employee_, c.employee_name, b.amount, b.code_id
from arkona.ext_pyhshdta a
join arkona.ext_pyhscdta b on a.company_number = b.company_number
  and a.payroll_run_number = b.payroll_run_number
  and a.employee_ = b.employee_number
  and b.code_id = '500'  
left join arkona.xfm_pymast c on a.company_number = c.pymast_company_number
  and a.employee_ = c.pymast_employee_number
  and c.current_row  
where a.payroll_cen_year = 119
order by b.amount


-- why are there non .00 amounts
select distinct amount
from (
select a.check_month, a.employee_, c.employee_name, b.amount, b.code_id
from arkona.ext_pyhshdta a
join arkona.ext_pyhscdta b on a.company_number = b.company_number
  and a.payroll_run_number = b.payroll_run_number
  and a.employee_ = b.employee_number
  and b.code_id = '500'  
left join arkona.xfm_pymast c on a.company_number = c.pymast_company_number
  and a.employee_ = c.pymast_employee_number
  and c.current_row  
where a.payroll_cen_year = 119) x

-- who are they, ah, Larry Stadstad
select a.check_month, a.employee_, c.employee_name, b.amount, b.code_id
from arkona.ext_pyhshdta a
join arkona.ext_pyhscdta b on a.company_number = b.company_number
  and a.payroll_run_number = b.payroll_run_number
  and a.employee_ = b.employee_number
  and b.code_id = '500'  
left join arkona.xfm_pymast c on a.company_number = c.pymast_company_number
  and a.employee_ = c.pymast_employee_number
  and c.current_row  
where a.payroll_cen_year = 119
  and b.amount in( 349.95, 395.54)
order by b.amount



select 
  case check_month::integer
    when 1 then 'Jan'
    when 2 then 'Feb'
    when 3 then 'Mar'
    when 4 then 'Apr'
    when 5 then 'May'
    when 6 then 'Jun'
    when 7 then 'Jul'
    when 8 then 'Aug'
    when 9 then 'Sep'
  end as month, sum(amount)
from (
  select a.check_month, a.employee_, c.employee_name, b.amount, b.code_id
  from arkona.ext_pyhshdta a
  join arkona.ext_pyhscdta b on a.company_number = b.company_number
    and a.payroll_run_number = b.payroll_run_number
    and a.employee_ = b.employee_number
    and b.code_id = '500'  
  left join arkona.xfm_pymast c on a.company_number = c.pymast_company_number
    and a.employee_ = c.pymast_employee_number
    and c.current_row  
  where a.payroll_cen_year = 119
    and check_month < 10) x
group by check_month
order by check_month

