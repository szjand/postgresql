﻿select a.pymast_company_number as store, employee_name as name, 
  arkona.db2_integer_to_date(a.org_hire_date) as orig_hire_date, 
  arkona.db2_integer_to_date(a.hire_date) as last_hire_date,
  arkona.db2_integer_to_date(a.termination_date) as term_date,
  case active_code
    when 'P' then 'part'
    else 'full'
  end as full_part_time
-- select *   
from arkona.xfm_pymast a
where a.current_row
  and active_code <> 'T'
  and pymast_company_number in ('RY1','RY2') 
order by store, name



select pymast_company_number, a.pymast_employee_number, a.employee_name, active_code,
  arkona.db2_integer_to_date(a.org_hire_date) as orig_hire_date, 
  arkona.db2_integer_to_date(a.hire_date) as last_hire_date,
  arkona.db2_integer_to_date(a.termination_date) as term_date,
  row_from_date, row_thru_date, pymast_key,
  a.union_anniversary_date
-- select *  
from arkona.xfm_pymast a
where employee_name = 'longoria, michael'
order by pymast_key

select storecode, employeenumber, employeekey, name, activecode,hiredate,termdate,
  rowchangedate, rowchangereason, employeekeyfromdate, employeekeythrudate
from ads.ext_edw_employee_dim
where employeenumber in ('165328','265328')
order by employeekey


-- from employeedim
-- just '07/14/2020' between employeekeyfromdate and employeekeythrudate is not good enough,
--  becuase termdate updates all rows
drop table if exists tem.emp_dim_1 cascade;
create table tem.emp_dim_1 as
select storecode, employeenumber, name, activecode, hiredate, termdate,
  rowchangedate, rowchangereason, employeekeyfromdate, employeekeythrudate
from ads.ext_edw_employee_dim
where storecode = 'RY1'
  and '07/14/2020' between employeekeyfromdate and employeekeythrudate
  and (activecode <> 'T' or termdate > '07/14/2020');
create unique index on tem.emp_dim_1(employeenumber);  

drop table if exists tem.emp_dim_2 cascade;
create table tem.emp_dim_2 as
select storecode, employeenumber, employeekey, name, activecode,hiredate,termdate,
  rowchangedate, rowchangereason, employeekeyfromdate, employeekeythrudate
from ads.ext_edw_employee_dim
where storecode = 'RY1'
  and '07/14/2020' between hiredate and termdate
create unique index on tem.emp_dim_2(employeenumber);  


-- from pymast 
drop table if exists tem.xfm_pymast cascade;
create table tem.xfm_pymast as
select pymast_company_number, a.pymast_employee_number, a.employee_name, active_code,
  arkona.db2_integer_to_date(a.org_hire_date) as orig_hire_date, 
  arkona.db2_integer_to_date(a.hire_date) as last_hire_date,
  arkona.db2_integer_to_date(a.termination_date) as term_date,
  row_from_date, row_thru_date, pymast_key,
  a.union_anniversary_date
-- select *  
from arkona.xfm_pymast a
where pymast_company_number = 'RY1'
  and active_code <> 'T'
  and '07/14/2020' between row_from_date and row_thru_date;
create unique index on tem.xfm_pymast(pymast_employee_number);  

-- needed to include ext_pymast, row_from date is operational and may not catch everyone, eg, employees hired on 07/13
drop table if exists tem.ext_pymast cascade;
create table tem.ext_pymast as
select pymast_company_number, a.pymast_employee_number, a.employee_name, active_code,
  arkona.db2_integer_to_date(a.org_hire_date) as orig_hire_date, 
  arkona.db2_integer_to_date(a.hire_date) as last_hire_date,
  arkona.db2_integer_to_date(a.termination_date) as term_date,
  a.union_anniversary_date
-- select *  
from arkona.ext_pymast a
where pymast_company_number = 'RY1'
  and active_code <> 'T'
  and '07/14/2020' between arkona.db2_integer_to_date(a.hire_date) and arkona.db2_integer_to_date(a.termination_date);
create unique index on tem.ext_pymast(pymast_employee_number);  


select * 
from tem.ext_pymast a
union
select pymast_company_number, pymast_employee_number, employee_name, active_code,
  orig_hire_date, last_hire_date, term_date,
  union_anniversary_date
from tem.xfm_pymast b  



-- this looks good enough for ry1  
select bb.*
from tem.emp_dim_1 aa
full outer join (
  select * 
  from tem.ext_pymast a
  where pymast_employee_number not in ('123946','154876','145785','165328','129874','115883','192348')
  union
  select pymast_company_number, pymast_employee_number, employee_name, active_code,
    orig_hire_date, last_hire_date, term_date,
    union_anniversary_date
  from tem.xfm_pymast b
    where pymast_employee_number not in ('123946','154876','145785','165328','129874','115883','192348')) bb on aa.employeenumber = bb.pymast_Employee_number
order by aa.name, bb.employee_name
-- all these exceptions were termed prior to 07/14/20
-- tested them against emp_dim and xfm_pymast for actual term dates

-- spread sheet for ry1 07/14
select pymast_company_number as store, pymast_employee_number as employee_number, employee_name, 
  orig_hire_date, last_hire_date,term_date,
  case active_code
    when 'P' then 'part'
    else 'full'
  end as full_part_time
from (
  select bb.*
  from tem.emp_dim_1 aa
  full outer join (
    select * 
    from tem.ext_pymast a
    where pymast_employee_number not in ('123946','154876','145785','165328','129874','115883','192348')
    union
    select pymast_company_number, pymast_employee_number, employee_name, active_code,
      orig_hire_date, last_hire_date, term_date,
      union_anniversary_date
    from tem.xfm_pymast b
      where pymast_employee_number not in ('123946','154876','145785','165328','129874','115883','192348')) bb on aa.employeenumber = bb.pymast_Employee_number
  order by aa.name, bb.employee_name) cc

-- now the same thing for honda
drop table if exists tem.emp_dim_2 cascade;
create table tem.emp_dim_2 as
select storecode, employeenumber, name, activecode, hiredate, termdate,
  rowchangedate, rowchangereason, employeekeyfromdate, employeekeythrudate
from ads.ext_edw_employee_dim
where storecode = 'RY2'
  and '07/28/2020' between employeekeyfromdate and employeekeythrudate
  and (activecode <> 'T' or termdate > '07/28/2020');
create unique index on tem.emp_dim_1(employeenumber);  

-- from pymast 
drop table if exists tem.xfm_pymast_2 cascade;
create table tem.xfm_pymast_2 as
select pymast_company_number, a.pymast_employee_number, a.employee_name, active_code,
  arkona.db2_integer_to_date(a.org_hire_date) as orig_hire_date, 
  arkona.db2_integer_to_date(a.hire_date) as last_hire_date,
  arkona.db2_integer_to_date(a.termination_date) as term_date,
  row_from_date, row_thru_date, pymast_key,
  a.union_anniversary_date
-- select *  
from arkona.xfm_pymast a
where pymast_company_number = 'RY2'
  and active_code <> 'T'
  and '07/28/2020' between row_from_date and row_thru_date;
create unique index on tem.xfm_pymast_2(pymast_employee_number);  

-- needed to include ext_pymast, row_from date is operational and may not catch everyone, eg, employees hired on 07/13
drop table if exists tem.ext_pymast_2 cascade;
create table tem.ext_pymast_2 as
select pymast_company_number, a.pymast_employee_number, a.employee_name, active_code,
  arkona.db2_integer_to_date(a.org_hire_date) as orig_hire_date, 
  arkona.db2_integer_to_date(a.hire_date) as last_hire_date,
  arkona.db2_integer_to_date(a.termination_date) as term_date,
  a.union_anniversary_date
-- select *  
from arkona.ext_pymast a
where pymast_company_number = 'RY2'
  and active_code <> 'T'
  and '07/28/2020' between arkona.db2_integer_to_date(a.hire_date) and arkona.db2_integer_to_date(a.termination_date);
create unique index on tem.ext_pymast_2(pymast_employee_number);  

select *
from tem.emp_dim_2 aa
full outer join (
  select * 
  from tem.ext_pymast_2 a
  where pymast_employee_number not in ('212589','258763','2100750','2114160','2135031','256984')
  union
  select pymast_company_number, pymast_employee_number, employee_name, active_code,
    orig_hire_date, last_hire_date, term_date,
    union_anniversary_date
  from tem.xfm_pymast_2 b
    where pymast_employee_number not in ('212589','258763','2100750','2114160','2135031','256984')) bb on aa.employeenumber = bb.pymast_Employee_number
order by aa.name, bb.employee_name

-- spread sheet for ry2 07/28
select pymast_company_number as store, pymast_employee_number as employee_number, employee_name, 
  orig_hire_date, last_hire_date,term_date,
  case active_code
    when 'P' then 'part'
    else 'full'
  end as full_part_time
from (
  select bb.*
  from tem.emp_dim_2 aa
  full outer join (
    select * 
    from tem.ext_pymast_2 a
    where pymast_employee_number not in ('212589','258763','2100750','2114160','2135031','256984')
    union
    select pymast_company_number, pymast_employee_number, employee_name, active_code,
      orig_hire_date, last_hire_date, term_date,
      union_anniversary_date
    from tem.xfm_pymast_2 b
      where pymast_employee_number not in ('212589','258763','2100750','2114160','2135031','256984')) bb on aa.employeenumber = bb.pymast_Employee_number
  order by aa.name, bb.employee_name) cc

  
/*
thought maybe compli would be an answer, but it does not handle xfr between stores,
just overwrites the employeenumbers

drop table if exists tem.compli cascade;
create table tem.compli as
select a.location_id as store, a.user_id as employee_number, a.first_name, a.last_name, a.date_of_hire as hire_date, coalesce(b.termination_Date, '12/31/9999'::date) as term_date
from pto.compli_users a
left join pto.compli_departures b on a.user_id = b.employee_number;


select * 
from tem.compli
where last_name = 'monreal'

-- fucking compli has no history, jesse has been at both store and that is not inidicated
select * from pto.compli_users where user_id = '215883'

select * from pto.compli_departures where last_name = 'monreal'


select * 
from tem.emp_dim_1 a
full outer join (
  select * 
  from tem.compli
  where hire_date < '07/14/2020'
    and term_Date > '07/14/2020'
  and store = 'RY1') b on a.employeenumber = b.employee_number
order by a.name
*/