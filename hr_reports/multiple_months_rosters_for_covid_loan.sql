﻿/*
Hey Jon-

I am working on something rather urgent for Jeri that needs to be done today, sorry it’s for this COVID loan application and it has been quick turnaround on everything.  

I need the FTE count for February-June of 2019, on the last day of each of those months.  Would you be able to pull the roster report that you usually send me for each of those months, as of the last day of the month?

Thanks!

Laura Roth 
04/01/2020
*/

select *
from arkona.xfm_pymast
order by row_from_Date desc
limit 10

select *
from arkona.xfm_pymast
where employee_last_name = 'hall'

select year_month, the_date
from dds.dim_date
where year_month between 201902 and 201906
  and last_day_of_month


select pymast_company_number, pymast_employee_number, active_code, 
  employee_name, pymast_employee_number,
  arkona.db2_integer_to_date(a.org_hire_date) as orig_hire_date,
  arkona.db2_integer_to_date(a.hire_date) as hire_date,
  arkona.db2_integer_to_date(a.termination_date) as term_date,
  row_from_Date, row_thru_date, current_row
from arkona.xfm_pymast a
where arkona.db2_integer_to_date(a.hire_date) <= '02/28/2019'
  and arkona.db2_integer_to_date(a.termination_date) > '02/28/2019'
  and current_row
order by employee_name  

select pymast_company_number, pymast_employee_number, active_code, 
  employee_name, pymast_employee_number,
  arkona.db2_integer_to_date(a.org_hire_date) as orig_hire_date,
  arkona.db2_integer_to_date(a.hire_date) as hire_date,
  arkona.db2_integer_to_date(a.termination_date) as term_date,
  row_from_Date, row_thru_date, current_row
select *  
from arkona.xfm_pymast a
where employee_name = 'ANDERSON, HAILEE'
order by row_from_Date

select *  
from arkona.xfm_pymast a
where pymast_employee_number = '12800'
order by row_from_Date

select pymast_company_number, active_code, 
  employee_name, pymast_employee_number,
  arkona.db2_integer_to_date(a.org_hire_date) as orig_hire_date,
  arkona.db2_integer_to_date(a.hire_date) as hire_date,
  arkona.db2_integer_to_date(a.termination_date) as term_date,
  row_from_Date, row_thru_date, current_row
from arkona.xfm_pymast a
left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
left join arkona.ext_pyprjobd c on b.company_number = c.company_number
  and b.job_description = c.job_description 
where arkona.db2_integer_to_date(a.hire_date) <= '02/28/2019'
  and arkona.db2_integer_to_date(a.termination_date) > '02/28/2019'
  and pymast_company_number in ('RY1','RY2')
  and current_row
  and a.hire_date <> a.org_hire_date
order by hire_date
  

select *
from (
select pymast_company_number, active_code, 
  employee_name, pymast_employee_number,
  arkona.db2_integer_to_date(a.org_hire_date) as orig_hire_date,
  arkona.db2_integer_to_date(a.hire_date) as hire_date,
  arkona.db2_integer_to_date(a.termination_date) as term_date,
  row_from_Date, row_thru_date, current_row
from arkona.xfm_pymast a
left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
left join arkona.ext_pyprjobd c on b.company_number = c.company_number
  and b.job_description = c.job_description 
where arkona.db2_integer_to_date(a.hire_date) <= '02/28/2019'
  and arkona.db2_integer_to_date(a.termination_date) > '02/28/2019'
  and pymast_company_number in ('RY1','RY2')
  and current_row) x
full outer join (  
select pymast_company_number, active_code, 
  employee_name, pymast_employee_number,
  arkona.db2_integer_to_date(a.org_hire_date) as orig_hire_date,
  arkona.db2_integer_to_date(a.hire_date) as hire_date,
  arkona.db2_integer_to_date(a.termination_date) as term_date
from arkona.ext_pymast a
where arkona.db2_integer_to_date(a.hire_date) <= '02/28/2019'
  and arkona.db2_integer_to_date(a.termination_date) > '02/28/2019'
  and pymast_company_number in ('RY1','RY2')) y on x.pymast_employee_number = y.pymast_employee_number



-- all i really need is hire and term dates, most recent extract is adequate

select pymast_company_number, active_code, 
  employee_name, pymast_employee_number,
  arkona.db2_integer_to_date(a.org_hire_date) as orig_hire_date,
  arkona.db2_integer_to_date(a.hire_date) as hire_date,
  arkona.db2_integer_to_date(a.termination_date) as term_date
from arkona.ext_pymast a
where arkona.db2_integer_to_date(a.termination_date) between '02/28/2019' and '06/30/2019'
  and pymast_company_number in ('RY1','RY2')

select year_month, the_date
from dds.dim_date
where year_month between 201902 and 201906
  and last_day_of_month

-- february
select a.pymast_company_number as store, employee_name as name, 
  arkona.db2_integer_to_date(a.org_hire_date) as orig_hire_date, 
  arkona.db2_integer_to_date(a.hire_date) as hire_date, 
  c.data as job,
  case active_code
    when 'P' then 'part'
    else 'full'
  end as full_part_time
-- select *   
from arkona.ext_pymast a
left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
left join arkona.ext_pyprjobd c on b.company_number = c.company_number
  and b.job_description = c.job_description 
where arkona.db2_integer_to_date(a.hire_date) <= '02/28/2019'
  and arkona.db2_integer_to_date(a.termination_date) > '02/28/2019'
  and pymast_company_number in ('RY1','RY2')

-- march
select a.pymast_company_number as store, employee_name as name, 
  arkona.db2_integer_to_date(a.org_hire_date) as orig_hire_date, 
  arkona.db2_integer_to_date(a.hire_date) as hire_date, 
  c.data as job,
  case active_code
    when 'P' then 'part'
    else 'full'
  end as full_part_time
-- select *   
from arkona.ext_pymast a
left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
left join arkona.ext_pyprjobd c on b.company_number = c.company_number
  and b.job_description = c.job_description 
where arkona.db2_integer_to_date(a.hire_date) <= '03/31/2019'
  and arkona.db2_integer_to_date(a.termination_date) > '03/31/2019'
  and pymast_company_number in ('RY1','RY2')

 -- april
select a.pymast_company_number as store, employee_name as name, 
  arkona.db2_integer_to_date(a.org_hire_date) as orig_hire_date, 
  arkona.db2_integer_to_date(a.hire_date) as hire_date, 
  c.data as job,
  case active_code
    when 'P' then 'part'
    else 'full'
  end as full_part_time
-- select *   
from arkona.ext_pymast a
left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
left join arkona.ext_pyprjobd c on b.company_number = c.company_number
  and b.job_description = c.job_description 
where arkona.db2_integer_to_date(a.hire_date) <= '04/30/2019'
  and arkona.db2_integer_to_date(a.termination_date) > '04/30/2019'
  and pymast_company_number in ('RY1','RY2')

-- may
select a.pymast_company_number as store, employee_name as name, 
  arkona.db2_integer_to_date(a.org_hire_date) as orig_hire_date, 
  arkona.db2_integer_to_date(a.hire_date) as hire_date, 
  c.data as job,
  case active_code
    when 'P' then 'part'
    else 'full'
  end as full_part_time
-- select *   
from arkona.ext_pymast a
left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
left join arkona.ext_pyprjobd c on b.company_number = c.company_number
  and b.job_description = c.job_description 
where arkona.db2_integer_to_date(a.hire_date) <= '05/31/2019'
  and arkona.db2_integer_to_date(a.termination_date) > '05/31/2019'
  and pymast_company_number in ('RY1','RY2')

-- june
select a.pymast_company_number as store, employee_name as name, 
  arkona.db2_integer_to_date(a.org_hire_date) as orig_hire_date, 
  arkona.db2_integer_to_date(a.hire_date) as hire_date, 
  c.data as job,
  case active_code
    when 'P' then 'part'
    else 'full'
  end as full_part_time
-- select *   
from arkona.ext_pymast a
left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
left join arkona.ext_pyprjobd c on b.company_number = c.company_number
  and b.job_description = c.job_description 
where arkona.db2_integer_to_date(a.hire_date) <= '06/30/2019'
  and arkona.db2_integer_to_date(a.termination_date) > '06/30/2019'
  and pymast_company_number in ('RY1','RY2')     