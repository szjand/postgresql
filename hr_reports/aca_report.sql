﻿/*
01/12/23
time to try it in ukg

and again onn 1/11/24
*/

select a.store as location, 
  case
    when a.last_name = 'Bies Ii' then 'Bies'
    when a.last_name = 'jBrooks Jr' then 'Brooks'
		when a.last_name = 'Van Heste Ii' then 'Van Heste'
		else a.last_name
  end as last_name, 
  case
    when a.last_name = 'Bies Ii' then 'II'
    when a.last_name = 'jBrooks Jr' then 'Jr'
		when a.last_name = 'Van Heste Ii' then 'II'  
		else null
  end as suffix, a.first_name, a.middle_name, a.ssn, a.address_line_1 || ' ' || coalesce(a.address_line_2, '') as address,
  initcap(a.city), a.state, a.zip, a.hire_date, 
  case
    when a.term_date = '12/31/9999' then null
    else a.term_date
  end as term_date, 
  c.employee_type_name as full_part_time
from ukg.employees a
left join ukg.ext_employee_pay_info b on a.employee_id = b.employee_id
left join ukg.ext_employee_types c on b.employee_type_id = c.id
where term_date > '12/31/2022'
  and a.last_name <> 'api'
--   and position(' ' in last_name) <> 0 and last_name not in ('Schmiess penas') -- to expose the presence of suffix
order by a.store, a.last_name

-- need full/part time

select a.last_name, a.first_name, a.employee_id, c.employee_type_name
from  ukg.employees a
left join ukg.ext_employee_pay_info b on a.employee_id = b.employee_id
left join ukg.ext_employee_types c on b.employee_type_id = c.id
where a.term_date > '12/31/2021'
  and a.last_name <> 'api'

----------------------------------------------------------------------------------------------------------
drop table if exists jon.employee_addresses;
create table jon.employee_addresses (
  store_code citext,
  employee_number citext,
  address citext,
  city citext,
  state citext,
  zip citext,
  primary key (store_code, employee_number)) ;

insert into jon.employee_addresses
select pymast_company_number, pymast_employee_number, address_1, city_name, state_code_address_, zip_code
from arkona.ext_pymast
where pymast_employee_number <> 'TEST'; 

/* 
1/24/20
no need for this
jon.ssn is populated for kims 401k report and she updates it with new employees

drop table if exists ads.ext_ssn;
create table ads.ext_ssn (
  employee_number citext,
  name citext, 
  ssn citext);

-- see DDS2/scripts/SSN for instructions on getting the ssn from dealertrack
*/


select * from jon.ssn
insert into jon.ssn values
('195034','BAIR, ARTHER','306-98-9501'),
('148735','SNYDER, JACE','477-43-1166');

and this is the report for laura
-- fuck forgot suffix, don't see where i did that before so...

-- select * from ( 
select case when a.storecode = 'RY1' then 'GM' else 'Honda Nissan' end as location, 
  case
    when lastname like '%HESTE%' then 'Van Heste'
    when lastname like '%ABDULAHI%' then 'Abdulahi Mohamed'
    when position(' ' in lastname) <> 0 and lastname not in ('hill defender','Schmiess penas') then 
      left(a.lastname, position(' ' in lastname))
    else lastname
  end as lastname,
  case
    when lastname like '%HESTE%' then 'II'
    when position(' ' in lastname) <> 0 and lastname not in ('hill defender','Schmiess penas','abdulahi mohamed') then 
      upper(substring(lastname, position(' ' in lastname)  + 1, 4))
    else null
  end as suffix,
  a.firstname, a.middlename, c.ssn, b.address, b.city, b.state, 
  b.zip, a.hiredate, 
  case when a.termdate > current_date then null else termdate end, 
  fullparttime
-- select *  
from ads.ext_edw_employee_dim a
left join jon.employee_addresses b on a.employeenumber = b.employee_number
left join jon.ssn c on a.employeenumber = c.employee_number
where a.termdate > '12/31/2020'
  and a.currentrow = true
order by location, lastname
-- ) x where ssn is null or address is null




