﻿6/24/21
I know you are busy with Hireology stuff but could I have you run this for me but this time could you 
include their hourly wage amount?  Or if they don’t have an hourly wage, you can send me their annual wage.  
Basically I just need whatever income information you can provide for each person.  
If you are pulling the annual wage, you can send me their wages for YTD through 5/31/2021.
Laura Roth 

select a.pymast_company_number as store, employee_last_name as last_name, employee_first_name as first_name,
  arkona.db2_integer_to_date(a.org_hire_date) as orig_hire_date, 
  arkona.db2_integer_to_date(a.hire_date) as last_hire_date,
  c.data as job,
  case active_code
		when 'P' then 'part'
    else 'full'
  end as full_part_time,
  round(d.fixed_ded_amt, 2) as medical, payroll_class, --pay_period, base_Salary, base_hrly_rate,
  case when payroll_class = 'h' then base_hrly_rate end as hourly_rate,
	case when payroll_class = 's' then base_salary end as salary,
	e.total_gross_pay
-- select *   
from arkona.ext_pymast a
left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
left join arkona.ext_pyprjobd c on b.company_number = c.company_number
  and b.job_description = c.job_description 
left join arkona.ext_pydeduct d on a.pymast_employee_number = d.employee_number  
  and d.ded_pay_code = '107'
left join (
	select employee_, sum(total_gross_pay) as total_gross_pay
	from arkona.ext_pyhshdta a
	join arkona.ext_pymast b on a.employee_ = b.pymast_employee_number
		and b.active_code <> 'T'
	where a.company_number in ('RY1','RY2')
		and check_year = 21
		and check_month between 1 and 5  
	group by employee_) e on a.pymast_employee_number = e.employee_  
where active_code <> 'T'
  and pymast_company_number in ('RY1','RY2') 
-- order by payroll_class  
order by store, last_name, first_name





-- 05/27/21 this is the version that laura requests regularly
-- roster with job
select a.pymast_company_number as store, employee_last_name as last_name, employee_first_name as first_name,
  arkona.db2_integer_to_date(a.org_hire_date) as orig_hire_date, 
  arkona.db2_integer_to_date(a.hire_date) as last_hire_date,
  c.data as job,
  case active_code
when 'P' then 'part'
    else 'full'
  end as full_part_time,
  d.fixed_ded_amt
-- select *   
from arkona.ext_pymast a
left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
left join arkona.ext_pyprjobd c on b.company_number = c.company_number
  and b.job_description = c.job_description 
left join arkona.ext_pydeduct d on a.pymast_employee_number = d.employee_number  
  and d.ded_pay_code = '107'
where active_code <> 'T'
  and pymast_company_number in ('RY1','RY2') 
order by store, last_name, first_name



/*
05/07/21
Hi Jon-

When you have a chance, could I have you repull me a census from Compli with just employee names and job titles?

Thanks!

Laura Roth 
*/

select last_name||', '||first_name, title
from pto.compli_users
where status = '1'


	
-- 4ca254d -----------------------------------------------------------------------------------------------------
-- trade analysis, more kronos queries updated
-- 2021-05-16

/*
Could I please have you pull this for me when you get a chance?  
Could I also have you add a field to this going forward?  I’d like to have the original hire date 
still but then next to it, can you add the “Last Hire” date?

Thanks!

Laura Roth 
06/29/20

12/22/20
Could I please have you pull this report for me whenever you get a chance but could I have you add in there 
anyone who has a dollar amount in field 107 in dealertrack and also the amount that’s in that field?  
I’m doing something for health insurance and trying to kill 2 birds with this report.
*/
-- roster with job
select a.pymast_company_number as store, employee_name as name, 
  arkona.db2_integer_to_date(a.org_hire_date) as orig_hire_date, 
  arkona.db2_integer_to_date(a.hire_date) as last_hire_date,
  c.data as job,
  case active_code
    when 'P' then 'part'
    else 'full'
  end as full_part_time,
  d.fixed_ded_amt
-- select *   
from arkona.xfm_pymast a
left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
left join arkona.ext_pyprjobd c on b.company_number = c.company_number
  and b.job_description = c.job_description 
left join arkona.ext_pydeduct d on a.pymast_employee_number = d.employee_number  
  and d.ded_pay_code = '107'
where a.current_row
  and active_code <> 'T'
  and pymast_company_number in ('RY1','RY2') 
order by store, name


/*
05/07/21
Hi Jon-

When you have a chance, could I have you repull me a census from Compli with just employee names and job titles?

Thanks!

Laura Roth 
*/

select last_name||', '||first_name, title
from pto.compli_users
where status = '1'

/*
5/14/21
Would you be able to pull this same census but break out their first and last names into separate columns 
and then add their social security number in?  I know it would pulling from 2 different systems, 
but figured I would ask anyway.
*/

select a.last_name, a.first_name, a.title, b.ssn
from pto.compli_users a
left join jon.ssn b on a.user_id = b.employee_number
where status = '1'

