﻿
select b.pymast_company_number as store, a.employee_number as "emp#", b.employee_name as name, c.description as dept, a.ot_hours,
  sum(a.ot_hours) over (partition by b.pymast_company_number,c.description) as dept_total_ot
from (
	select employee_number, sum(ot_hours) as ot_hours
	from arkona.xfm_pypclockin 
	where the_Date between '01/01/2021' and current_date
		and ot_hours <> 0
	group by employee_number) a
left join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
left join arkona.ext_pypclkctl c on b.department_code = c.department_code
  and b.pymast_company_number = c.company_number
order by b.pymast_company_number, c.description, b.employee_name
