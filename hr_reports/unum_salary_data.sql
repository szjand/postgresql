﻿/*
Hey Jon-

Is there any way that you could add RY1 or RY2 to the report for GM or Honda?

Thanks!

Laura Roth 
04/30/20
*/

/*
Hi Jon-

Could you please run a report for the information in the attached grid for 2019?  I don’t need to include any employee that has been terminated.

Thanks!

Laura Roth 
0-4/22/20

Policy | Division	| SS Number	| First Name | Middle Name	| Last Name	| Salary	| Salary Mode(A = Annually,M = Monthly,B = Bi-Weekly,S = Semi-Monthly,W= Weekly,H=Hourly) |	Number of Hours (if hourly) 
								

*/

drop table if exists unum;
create temp table unum as
select pymast_employee_number as employee_number,
  case a.pymast_company_number 
    when 'RY1' then 'GM'
    when 'RY2' then 'Honda Nissan'   
  end as store_location, a.employee_last_name as last_name, a.employee_middle_name as middle_name, a.employee_first_name as first_name, 
  a.payroll_class, a.pay_period, a.base_salary, a.base_hrly_Rate, b.ssn
from arkona.xfm_pymast a
left join jon.ssn b on a.pymast_employee_number = b.employee_number
where a.current_row
  and a.active_code <> 'T'
  and a.pymast_company_number in ('RY1','RY2')
  and arkona.db2_integer_to_date(a.hire_date) < '01/01/2020';

  
drop table if exists gross;
create temp table gross as
select a.employee_ as employee_number, sum(a.total_gross_pay) as total_gross, count(*) as checks
from arkona.ext_pyhshdta a
join unum b on a.employee_ = b.employee_number
where a.payroll_cen_year = 119
  and a.seq_void <> '0J'
group by a.employee_;


drop table if exists total_hours;
create temp table total_hours as
select a.employee_number, sum(clock_hours) as clock_hours
from arkona.xfm_pypclockin a
join unum b on a.employee_number = b.employee_number
where a.the_date between '01/01/2019' and '12/31/2019'
group by a.employee_number
having sum(clock_hours) > 0


-- adds store
select null as "Policy", null as "Division", ssn as "SS Number", a.store_location as store, first_name as "First Name",
  middle_name as "Middle Name", last_name as "Last Name", coalesce(e.total_gross, 0) as "Salary",
  case
    when payroll_class = 'H' then 'Hourly'
    when pay_period = 'B' then 'Bi-Weekly'
    when pay_period = 'S' then 'Semi-Monthly'
  end as "Salary Mode",
  case
    when c.clock_hours > 3 and payroll_class <> 'S' and pay_period <> 'S' then coalesce(c.clock_hours, 0)
    else null 
  end as "Number of Hours"
from unum a
left join  total_hours c on a.employee_number = c.employee_number
left join gross e on a.employee_number = e.employee_number
order by a.store_location, last_name


