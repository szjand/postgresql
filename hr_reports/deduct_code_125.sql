﻿/*
04/15/21
Hey Jon-

Could you do me a favor and run a report from Dealertrack for Code 125 to show all of the employees that have that code on their profile from March 1st onward?

People that elected to carry the critical illness benefit can get $50 for annual checkups so want to make sure employees capitalize on that.

Thanks!

Laura Roth 
*/


select a.pymast_company_number as "Store", a.employee_name as "Name", a.pymast_employee_number as "Emp #"
from arkona.ext_pymast a
join arkona.ext_pydeduct b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
  and b.ded_pay_code = '125'
left join arkona.ext_pypcodes c on b.company_number = c.company_number and b.ded_pay_code = c.ded_pay_code  
where a.active_code <> 'T'
order by a.pymast_company_number, a.employee_name


