﻿/*

Hi Jon-

Jeri wants another way to look at our FTE for this loan deal.  
Do you think you could run this exact report for each independent week starting with the week of 4/12 to current?
The only thing I actually need is their name, store location and status (full time or part time).
Thanks!

Laura Roth 
06/03/20

*/
select storecode, name, fullparttime, hiredate, termdate, employeekeyfromdate,employeekeythrudate
from ads.ext_edw_employee_dim
group by storecode, name, fullparttime, hiredate,termdate, employeekeyfromdate,employeekeythrudate
order by name, employeekeyfromdate

select * from dds.dim_Date where the_date = current_date

select sunday_to_saturday_week, min(the_date) as from_date, max(the_date) as thru_date
from dds.dim_date
where the_date between '04/12/2020' and current_date
group by sunday_to_saturday_week

-- select name from (
select storecode, name, fullparttime
from ads.ext_edw_employee_dim
where hiredate <= '04/12/2020'
  and termdate > '04/18/2020'
  and employeekeyfromdate <= '04/12/2020'
  and employeekeythrudate > '04/18/2020'
group by storecode, name, employeenumber, fullparttime, hiredate,termdate, employeekeyfromdate,employeekeythrudate
order by storecode, name, employeekeyfromdate
-- ) a group by name having count(*) > 1

-- select name from (
select storecode, name, fullparttime
from ads.ext_edw_employee_dim
where hiredate <= '04/19/2020'
  and termdate > '04/25/2020'
  and employeekeyfromdate <= '04/19/2020'
  and employeekeythrudate > '04/25/2020'
group by storecode, name, employeenumber, fullparttime, hiredate,termdate, employeekeyfromdate,employeekeythrudate
order by storecode, name, employeekeyfromdate
-- ) a group by name having count(*) > 1


-- select name from (
select storecode, name, fullparttime
from ads.ext_edw_employee_dim
where hiredate <= '04/26/2020'
  and termdate > '05/02/2020'
  and employeekeyfromdate <= '04/26/2020'
  and employeekeythrudate > '05/02/2020'
group by storecode, name, employeenumber, fullparttime, hiredate,termdate, employeekeyfromdate,employeekeythrudate
order by storecode, name, employeekeyfromdate
-- ) a group by name having count(*) > 1


-- select name from (
select storecode, name, fullparttime
from ads.ext_edw_employee_dim
where hiredate <= '05/03/2020'
  and termdate > '05/09/2020'
  and employeekeyfromdate <= '05/03/2020'
  and employeekeythrudate > '05/09/2020'
group by storecode, name, employeenumber, fullparttime, hiredate,termdate, employeekeyfromdate,employeekeythrudate
order by storecode, name, employeekeyfromdate
-- ) a group by name having count(*) > 1


-- select name from (
select storecode, name, fullparttime
from ads.ext_edw_employee_dim
where hiredate <= '05/10/2020'
  and termdate > '05/16/2020'
  and employeekeyfromdate <= '05/10/2020'
  and employeekeythrudate > '05/16/2020'
group by storecode, name, employeenumber, fullparttime, hiredate,termdate, employeekeyfromdate,employeekeythrudate
order by storecode, name, employeekeyfromdate
-- ) a group by name having count(*) > 1

-- select name from (
select storecode, name, fullparttime
from ads.ext_edw_employee_dim
where hiredate <= '05/17/2020'
  and termdate > '05/23/2020'
  and employeekeyfromdate <= '05/17/2020'
  and employeekeythrudate > '05/23/2020'
group by storecode, name, employeenumber, fullparttime, hiredate,termdate, employeekeyfromdate,employeekeythrudate
order by storecode, name, employeekeyfromdate
-- ) a group by name having count(*) > 1

-- select name from (
select storecode, name, fullparttime
from ads.ext_edw_employee_dim
where hiredate <= '05/24/2020'
  and termdate > '05/30/2020'
  and employeekeyfromdate <= '05/24/2020'
  and employeekeythrudate > '05/30/2020'
group by storecode, name, employeenumber, fullparttime, hiredate,termdate, employeekeyfromdate,employeekeythrudate
order by storecode, name, employeekeyfromdate
-- ) a group by name having count(*) > 1