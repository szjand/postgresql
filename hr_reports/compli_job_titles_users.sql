﻿/*
Hey Jon-

Would you be able to send me a list of all job titles out of Compli again whenever you have a chance?  
I want to make sure they are still aligning to what we have in UKG.

Thanks!
*/

select location_id as store, title
from pto.compli_users 
-- where status = '1'
group by store, title
order by store, title

select location_id as store, last_name, first_name, user_id as "emp#", title
from pto.compli_users 
where status = '1'
order by store, last_name


