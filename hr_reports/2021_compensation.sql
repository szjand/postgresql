﻿/*
Hi Jon-

Could you please pull a report for me that shows everyone’s 2021 compensation?  I also need to know pay type for everyone (salary, hourly).  Then could you also include the hourly rate of the hourly people?

Thanks!

Laura Roth 
*/
-- select * from arkona.exT_pymast limit 200


select a.pymast_company_number as store, a.pymast_employee_number as "emp #", a.employee_name, arkona.db2_integer_to_date(a.hire_date) as hire_date,
  case 
    when a.active_code = 'A' then 'full'
    when a.active_code = 'P' then 'part'
  end as full_part, a.payroll_class,
  case
    when a.payroll_class = 'H' then a.base_hrly_rate
    else null
  end as hourly_rate, b.total_gross_pay
from arkona.ext_pymast a
left join (  
	select employee_, sum(total_gross_pay) as total_gross_pay
	from arkona.ext_pyhshdta
	where payroll_cen_year = 121
	group by employee_) b on a.pymast_employee_number = b.employee_
where a.pymast_company_number in ('RY1','RY2')	
  and length(a.employee_name) > 3
  and a.active_code <> 'T'
  and arkona.db2_integer_to_date(a.hire_date) < '01/01/2022'
order by a.employee_name

