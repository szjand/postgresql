﻿select a.pymast_company_number as "Store", a.employee_name as "Name", a.pymast_employee_number as "Emp #", 
  case a.active_code
    when 'A' then 'FT'
    when 'P' then 'PT'
  end as "PT/FT", 
  sum(fixed_ded_amt) filter (where b.ded_pay_code = '107') as "code 107",
  sum(fixed_ded_amt) filter (where b.ded_pay_code = '111') as "code 111",
  sum(fixed_ded_amt) filter (where b.ded_pay_code = '295') as "code 295"
from arkona.ext_pymast a
join arkona.ext_pydeduct b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
  and b.ded_pay_code in ('107','111','295')
where a.active_code <> 'T'
group by a.pymast_company_number, a.employee_name, a.pymast_employee_number, 
  case a.active_code
    when 'A' then 'FT'
    when 'P' then 'PT'
  end
order by a.pymast_company_number, a.employee_name



