﻿/*
Are you able to pull a report for me from Dealertrack for Code 96?  I basically just 
need a report of everyone that has something in that field and then what the amount is.  
If you could add in the store location so I can sort them out, that would be perfect.
1/18/21
Laura Roth 
*/
select b.pymast_company_number as store, b.pymast_employee_number as "emp #", b.employee_name as name, a.fixed_Ded_amt as amount
from arkona.ext_pydeduct a
join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
  and b.active_code <> 'T'
where a.ded_pay_code = '96'
order by store, name