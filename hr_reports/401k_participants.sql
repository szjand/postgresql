﻿/*
Hi Jon-

Can I please get a spreadsheet with all of these 401k participants from the last email but with their address?  
I have to send something in the mail to everyone.  If you could please break it out by address, city, state and zip so that I can make labels, the would be great.

Thanks!

Laura Roth 
05/13/20
*/

drop table if exists wtf cascade;
create temp table wtf as
select a.pymast_company_number as store, a.pymast_employee_number as emp_number, 
  employee_first_name as first_name, employee_last_name as last_name, address_1 as address, city_name as city, state_code_address_ as state, zip_code
from arkona.xfm_pymast a
join arkona.ext_pydeduct b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
  and b.ded_pay_code in ('99','91','91B','91C','99A','99B','99C')
join nrv.users c on a.pymast_employee_number = c.employee_number  
  and c.email_address not in ('tmonson@rydellcars.com')
-- left join jon.ssn d on a.pymast_employee_number = d.employee_number  
where a.current_row
  and a.active_code <> 'T'
group by a.pymast_company_number, a.pymast_employee_number, 
  employee_first_name, employee_last_name, address_1, city_name, state_code_address_, zip_code;
create unique index on wtf(emp_number);
select * from wtf
order by store, last_name;  

/*
05/06/20
Hi Jon-
Our 401k plan is asking for all employee’s company email addresses.  Is there a way for you to pull a report with every 401K participant, 
their social with no dashes and their email address?
Thanks,
Laura Roth 

If you can identify for me the deduction codes in Dealertrack that indicate 401k participation, I should be able to get the social and email addresses
jon

The below codes are all 401K
91
91b
91c
99
99a
99b
99c
Thanks!
Laura Roth 
*/

drop table if exists wtf cascade;
create temp table wtf as
select a.pymast_company_number as store, a.pymast_employee_number as emp_number, a.employee_name as name, replace(d.ssn, '-', '') as ssn, c.email_address as email
from arkona.xfm_pymast a
join arkona.ext_pydeduct b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
  and b.ded_pay_code in ('99','91','91B','91C','99A','99B','99C')
join nrv.users c on a.pymast_employee_number = c.employee_number  
  and c.email_address not in ('tmonson@rydellcars.com')
left join jon.ssn d on a.pymast_employee_number = d.employee_number  
where a.current_row
  and a.active_code <> 'T'
group by a.pymast_company_number, a.pymast_employee_number, a.employee_name, c.email_address, d.ssn;  
create unique index on wtf(emp_number);
alter table wtf alter column ssn set not null;
alter table wtf alter column email set not null;
select * from wtf
order by store, name;  

/*
Hi Jon-

I have to send a mailing out next week to everyone who is involved in our 401k plan.  Could you pull a list for me with the below information?

Name – Would be helpful if it’s either written like normal first name/last name or else in 2 different cells so that I can create labels.  
Street Address
City, State Zip

Thanks!

Laura Roth 

8/14/19

Hey Jon-

I decided that rather than mail a lot of these, I am going to walk them around the dealership.  
I think that will take me less time than stuffing envelopes 
Would it be too much to ask to have you add in department or job title to this spreadsheet?

Thanks!

Laura Roth 

*/
/*
11/22/21
Hey Jon-

Could you please pull this same report for me? -- she included the spreadsheet i sent to her on 12/2/19, and this query generates that

Thanks!
*/

select aa.pymast_company_number as store, aa.first_name, aa.last_name, aa.department, cc.data as job_title, 
  aa.address, aa.city, aa.state, aa.zip
from (
  select b.pymast_company_number, b.pymast_employee_number, b.employee_first_name as first_name, b.employee_last_name as last_name, 
    c.description as department,
    b.address_1 as address, b.city_name as city, b.state_code_address_ as state, b.zip_code as zip
  from arkona.ext_pydeduct a
  join arkona.xfm_pymast b on a.employee_number = b.pymast_employee_number
    and b.current_row
    and b.active_code <> 'T'
  join arkona.ext_pypclkctl c on b.pymast_company_number = c.company_number 
    and b.department_code = c.department_code   
  where a.ded_pay_code in ('99','91','91B','91C','99A','99B','99C')    
  group by b.pymast_company_number, b.pymast_employee_number, b.employee_first_name, b.employee_last_name, description,
    b.address_1, b.city_name, b.state_code_address_, b.zip_code) aa
left join arkona.ext_pyprhead bb on aa.pymast_employee_number = bb.employee_number
left join arkona.ext_pyprjobd cc on bb.company_number = cc.company_number
  and bb.job_description = cc.job_description
order by store, last_name  