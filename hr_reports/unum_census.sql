﻿select employee_, max(employee_name) as name, sum(total_gross_pay) as total_gross
from dds.ext_pyhshdta
where check_year = 16
group by employee_
order by name


--select a.pymast_company_number, a.pymast_employee_number, b.ssn, c.*
select c.firstname as "First Name", c.lastname as "Last Name", b.ssn, 
  c.birthdate as "Date of Birth", c.hiredate as "Date of Hire", a.sex as Gender, 
  d.total_gross as "Annual Earnings", 
  case a.pymast_company_number
    WHEN 'RY1' THen 'Rydell GM'
    WHEN 'RY2' THEN 'Honda Nissan' 
  end as Division, 
  case c.payperiod 
    when 'Bi-Weekly' then '24'
    when 'Semi-Monthly' then '12'
  end as "# Payroll Deductions per Year",
  a.address_1 as "Employee Home-Street Address",
  a.city_name as "Employee Home-City",
  a.state_code_address_ as "Employee Home-State",
  a.zip_code as "Employee Home-Zip Code"
-- select distinct payperiod 
from dds.ext_pymast a
left join ads.ext_dds_ssn b on a.pymast_employee_number = b.employee_number
left join ads.ext_dds_edwemployeedim c on a.pymast_employee_number = c.employeenumber
  and c.currentrow = true
left join (
  select employee_, max(employee_name) as name, sum(total_gross_pay) as total_gross
  from dds.ext_pyhshdta
  where check_year = 16
  group by employee_) d on a.pymast_employee_number = d.employee_
where a.active_code = 'A'
  and pymast_company_number in ('RY1','RY2')