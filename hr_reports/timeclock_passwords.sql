﻿/*
02/16/21

PDQ is going to be using a new process to track oil and they are in need of all of the PDQ Tech’s passwords.  
Are you able to pull this from Dealertrack for me?  It would be all people with the Department Code of “03” 
and then the Distribution Code of “PTEC”.

It’s the codes that they all use to punch into the timeclocks each day.  It’s usually the last 4 digits 
of their social unless that number is taken by someone else and then I just assign a different 4 digit 
code for them.

Laura Roth 


hmmm, never looked for this before
lucked out and found the table in AQT and scraped it
*/

drop table if exists arkona.ext_pypclkuser cascade;
create table arkona.ext_pypclkuser (
  company citext,
  employee_number citext,
  log_on_user_id citext,
  average_in_time time,
  average_out_time time,
  password citext);

 select * from arkona.ext_pypclkuser 


 select a.employee_name, a.pymast_employee_number, '="' ||c.password|| '"'
-- select *
from arkona.ext_pymast a
left join jon.ssn b on a.pymast_employee_number = b.employee_number
left join arkona.ext_pypclkuser c on a.pymast_employee_number = c.employee_number
where active_code <> 'T'
  and department_code = '03'
  and distrib_code = 'PTEC'