﻿/*
11/19/19
Hi Jon-

Would you be able to pull a report with the attached information for everyone on our medical plan?  
From your end you probably can’t see which deductible option people have but if you were able to 
tell me what their per paycheck deduction is from line 107, I would be able to figure it out.

If I could get this sometime by the end of the week, that would be great.

Thanks!

Laura Roth 
*/

select a.pymast_company_number as store, a.employee_last_name as last_name, employee_first_name as first_name,
  a.birth_date as dob, sex as  gender, replace(address_1, ',', ' ') as address_1, address_2, city_name as city,
  state_code_address_ as state,
  zip_code as zip, telephone_number as telephone,
  b.fixed_ded_amt as deductible_option
from arkona.xfm_pymast a
join arkona.ext_pydeduct b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
  and b.ded_pay_code ='107'
where a.current_row
  and a.active_code <> 'T'
  and a.pymast_company_number in ('RY1','RY2')
order by a.pymast_company_number, a.employee_last_name
