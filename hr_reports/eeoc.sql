﻿
/*
08/17/21
Hey Jon-

We didn’t have to file this last year with COVID but now this year I have to report for both 2019 and 2020.  
This time they are just wanting this for one snapshot of time so could you send me this report for just the dates of October 1,2019-October 31, 2019
and you don’t need to include the pay data.  And then also for October 1,2020-October 31,2020?  Don’t need pay data at all for these right now.

Thanks!
*/
-- dont know what i was on about with the rehires
-- dont know why i did ry1 only, include ry2
-- pyprhead & pyprjobd are now in nightly update
-- go with ext_pymast, good enough
-- yikes, need to cleanse for dups, eg tyler bedney
-- fixed date range, anyone employed in october 2019 was hired before 11/1/19 and terminated after 9/30/19

-- 11/10/21
-- Hey Jon-
-- Sorry to bug you with these reports again but are you able to break them out by store as well?
-- Thanks!
-- Laura Roth 
just redo 2019 and 2020 including store
-----------------------------------------------------------------------------------------------
--< 2019
-----------------------------------------------------------------------------------------------
-- dup cleanup
  select 
    a.employee_name, a.sex, c.data as job_title, pymast_employee_number, current_row,
    arkona.db2_integer_to_date(a.hire_date), arkona.db2_integer_to_date(a.org_hire_date), arkona.db2_integer_to_date(a.termination_date)
  from arkona.xfm_pymast a
  left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
  left join arkona.ext_pyprjobd c on b.job_description = c.job_description
    and b.company_number = c.company_number
  where arkona.db2_integer_to_date(a.hire_date) < '11/01/2019'
    and arkona.db2_integer_to_date(a.termination_date) > '09/30/2019'
    and a.employee_name in (
			select 
				a.employee_name
			from arkona.ext_pymast a
			where arkona.db2_integer_to_date(a.hire_date) < '11/01/2019'
				and arkona.db2_integer_to_date(a.termination_date) > '09/30/2019'
		group by a.employee_name having count(*) > 1)
order by a.employee_name, a.pymast_employee_number, a.pymast_key


-- selection from dups based on term dates   
-- final for 2019 
  select distinct a.pymast_company_number as store,
    a.employee_name, a.sex, c.data as job_title
--     arkona.db2_integer_to_date(a.hire_date), arkona.db2_integer_to_date(a.org_hire_date), arkona.db2_integer_to_date(a.termination_date)
  from arkona.ext_pymast a
  left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
    and b.job_description is not null
  left join arkona.ext_pyprjobd c on b.job_description = c.job_description
    and b.company_number = c.company_number
  where arkona.db2_integer_to_date(a.hire_date) < '11/01/2019'
    and arkona.db2_integer_to_date(a.termination_date) > '09/30/2019'
    and case when employee_name = 'BEDNEY, TYLER' then pymast_employee_number = '112589' else 1 = 1 end
    and case when employee_name = 'BOOKER, JEREMIAH' then pymast_employee_number = '269843' else 1 = 1 end
    and case when employee_name = 'BRUNK, JUSTIN' then pymast_employee_number = '158763' else 1 = 1 end
    and case when employee_name = 'HALEY, DYLANGER' then pymast_employee_number = '163700' else 1 = 1 end
    and case when employee_name = 'HINTZ, MATTHEW' then pymast_employee_number = '164329' else 1 = 1 end
    and case when employee_name = 'HOLLAND, NIKOLAI' then pymast_employee_number = '211232' else 1 = 1 end
    and case when employee_name = 'LONGORIA, MICHAEL' then pymast_employee_number = '265328' else 1 = 1 end
    and case when employee_name = 'MASON, MYRON' then pymast_employee_number = '298432' else 1 = 1 end
    and case when employee_name = 'MONREAL, JESSIE' then pymast_employee_number = '215883' else 1 = 1 end
    and case when employee_name = 'ROSE, BRITTANY' then pymast_employee_number = '159875' else 1 = 1 end
order by store, employee_name

-----------------------------------------------------------------------------------------------
--/> 2019
-----------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------
--< 2020
-----------------------------------------------------------------------------------------------
-- dup cleanup
  select 
    a.employee_name, a.sex, c.data as job_title, pymast_employee_number, current_row,
    arkona.db2_integer_to_date(a.hire_date), arkona.db2_integer_to_date(a.org_hire_date), arkona.db2_integer_to_date(a.termination_date)
  from arkona.xfm_pymast a
  left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
  left join arkona.ext_pyprjobd c on b.job_description = c.job_description
    and b.company_number = c.company_number
  where arkona.db2_integer_to_date(a.hire_date) < '11/01/2020'
    and arkona.db2_integer_to_date(a.termination_date) > '09/30/2020'
    and a.employee_name in (
			select 
				a.employee_name
			from arkona.ext_pymast a
			where arkona.db2_integer_to_date(a.hire_date) < '11/01/2020'
				and arkona.db2_integer_to_date(a.termination_date) > '09/30/2020'
		group by a.employee_name having count(*) > 1)
order by a.employee_name, a.pymast_employee_number, a.pymast_key


-- selection from dups based on term dates   
-- final for 2020
  select distinct pymast_company_number as store,  
    a.employee_name, a.sex, c.data as job_title
--     arkona.db2_integer_to_date(a.hire_date), arkona.db2_integer_to_date(a.org_hire_date), arkona.db2_integer_to_date(a.termination_date)
  from arkona.ext_pymast a
  left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
    and b.job_description is not null
  left join arkona.ext_pyprjobd c on b.job_description = c.job_description
    and b.company_number = c.company_number
  where arkona.db2_integer_to_date(a.hire_date) <= '10/01/2020'
    and arkona.db2_integer_to_date(a.termination_date) > '09/30/2020'
    and arkona.db2_integer_to_date(a.termination_date) > '09/30/2019'
    and case when employee_name = 'FARLEY, ETHAN' then pymast_employee_number = '169846' else 1 = 1 end
    and case when employee_name = 'HALEY, DYLANGER' then pymast_employee_number = '163700' else 1 = 1 end
    and case when employee_name = 'HINTZ, MATTHEW' then pymast_employee_number = '164329' else 1 = 1 end
    and case when employee_name = 'HOLLAND, NIKOLAI' then pymast_employee_number = '211232' else 1 = 1 end
    and case when employee_name = 'LONGORIA, MICHAEL' then pymast_employee_number = '165328' else 1 = 1 end
    and case when employee_name = 'MASON, MYRON' then pymast_employee_number = '298432' else 1 = 1 end
    and case when employee_name = 'PETERSEN, BENJAMIN' then pymast_employee_number = '136548' else 1 = 1 end
    and case when employee_name = 'ROSE, BRITTANY' then pymast_employee_number = '159875' else 1 = 1 end
    and case when employee_name = 'THORSELL, JESSE' then pymast_employee_number = '259375' else 1 = 1 end
order by store, employee_name

-----------------------------------------------------------------------------------------------
--/> 2020
-----------------------------------------------------------------------------------------------


        
/*
01/29/2020
Hey Jon-

This is my own personal nightmare of a task.

I have to go back into this EEOC system and enter yearly salary information for the attached 2018 report.  
Could you help me with that?  I just need total yearly gross income.  
I don’t have to do 2019 yet, just need 2018 data.

Thanks!
*/
, row_number() over (order by a.employee_name) + 1

-- trying to match what i sent her before, probem is rehires where current row does not match the spec
select x.employee_name, x.sex, x.job_title, y.total_yearly_gross
from (
  select 
    a.employee_name, a.sex, c.data as job_title, pymast_employee_number
  -- select *  
  from arkona.xfm_pymast a
  left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
  left join arkona.ext_pyprjobd c on b.job_description = c.job_description
    and b.company_number = c.company_number
  where arkona.db2_integer_to_date(a.hire_date) < '10/01/2018'
    and arkona.db2_integer_to_date(a.termination_date) > '09/30/2018'
    and a.pymast_company_number = 'RY1'
    and a.current_row
  union
  select -- berge
    a.employee_name, a.sex, c.data as job_title, pymast_employee_number
  -- select *  
  from arkona.xfm_pymast a
  left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
  left join arkona.ext_pyprjobd c on b.job_description = c.job_description
    and b.company_number = c.company_number
  where (a.employee_last_name = 'berge' or (employee_last_name = 'chavez' and employee_first_name = 'roman') 
    or (employee_last_name = 'magnuson' and employee_first_name = 'tyler' and employee_middle_name = 'D')
    or (employee_last_name = 'SIEDSCHLAG' and employee_first_name = 'JACOB')
    or (employee_last_name = 'svenson' and employee_first_name = 'james'))
    and a.current_row) x
left join (
  select employee_, sum(total_gross_pay) total_yearly_gross
  from arkona.ext_pyhshdta
  where check_year = 18
    and seq_void in ('00','01')
  group by employee_) y on x.pymast_employee_number = y.employee_    
order by employee_name  






/*
Hi Jon-

I couldn’t find the email from last year where I had you pull the EEOC report for me but attached is the spreadsheet that I ended up with in the end.  I know you can’t pull race yourself so I will do that manually, but could you pull me a report with everyone employed from October-December of 2018, just for the Rydell store?  I don’t need Honda data.

I will just need name, gender and job title.

If I could get this by the end of the week, that would be great.

Thanks!

*/


requires updating pyprhead, pyprjobd

select 
--   arkona.db2_integer_to_date(a.hire_date) as hire_date, 
--   arkona.db2_integer_to_date(a.termination_date) as termination_date,
--   a.pymast_employee_number, 
  a.employee_name, a.sex, c.data as job_title
-- select *  
from arkona.xfm_pymast a
left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
left join arkona.ext_pyprjobd c on b.job_description = c.job_description
  and b.company_number = c.company_number
where arkona.db2_integer_to_date(a.hire_date) < '10/01/2018'
  and arkona.db2_integer_to_date(a.termination_date) > '09/30/2018'
  and a.pymast_company_number = 'RY1'
  and a.current_row
order by a.employee_name