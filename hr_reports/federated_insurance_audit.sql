﻿/*
11/15/21
federated audit reports
based, loosely on E:\sql\advantage\DDS\reports\HR\federated ins audit.sql

Will you please run this report for 9/1/20 – 8/31/21?  They will be in on the 18th to do the audit—is it possible to have this back around noon tomorrow?

Thank you,

Jeri

from ads:
09/16/20
jeri requesting both reports again
1. ALL employees employed during the period with clock hours
2. current employees with dl
period: 09/01/2019 thru 08/31/2020
drivers license number no longer IN pymast, get it FROM employee status report
was going to move this ALL to pg, but, fuck it, i am tired
*/

---------------------------------------------------------------
--< REPORT 1 a little ratty, but good enough
---------------------------------------------------------------
-- base employees
drop table if exists employees cascade;
create temp table employees as
select pymast_company_number, pymast_employee_number, employee_name, active_code, arkona.db2_integer_to_date(hire_date) as hire_date, 
  arkona.db2_integer_to_date(org_hire_date) as org_hire_date, 
  case
    when arkona.db2_integer_to_date(termination_date) = '12/31/9999' then null
    else arkona.db2_integer_to_date(termination_date)
  end as term_date
from arkona.ext_pymast
where pymast_company_number in ('RY1','RY2')
  and arkona.db2_integer_to_date(hire_date) < '08/31/2021'
  and arkona.db2_integer_to_date(termination_date) > '09/01/2020'
order by employee_name;

select * from employees order by term_date desc nulls last

-- this gives me 646 rows (580 in employees) because there are emp with multiple active codes
-- 24 employees termed after 08/31/21
-- fuck it, group and max
drop table if exists employees_1 cascade;
create temp table employees_1 as
select a.pymast_company_number as "Store", a.pymast_employee_number as "Emp #", a.employee_name as name,
  a.hire_date as "Hire Date", a.org_hire_date as "Orig Hire Date", a.term_date as "Term Date", max(b.active_code) as "Full/Part Time"
from employees a
left join (
	select a.pymast_employee_number, b.active_code
	from employees a
	left join arkona.xfm_pymast b on a.pymast_employee_number = b.pymast_employee_number
	  and b.active_code <> 'T'
	group by a.pymast_employee_number, b.active_code) b on a.pymast_employee_number = b.pymast_employee_number
group by a.pymast_company_number, a.pymast_employee_number, a.employee_name,
  a.hire_date, a.org_hire_date, a.term_date
 order by a.employee_name;

-- this is what i sent to jeri
select a."Store", a."Emp #", a.name as "Name", a."Hire Date", a."Orig Hire Date", a."Term Date",
  c.data as "Job", 
  case "Full/Part Time"
    when 'A' then 'Full'
    else 'Part'
  end as "Full/Part Time", d.clock_hours as "Clock Hours"
from employees_1 a
left join arkona.ext_pyprhead b on a."Emp #" = b.employee_number
  and a."Store" = b.company_number
left join arkona.ext_pyprjobd c on b.company_number = c.company_number
  and b.job_description = c.job_description 
left join (
	select a.employee_number, sum(clock_hours)::integer as clock_hours
	from arkona.xfm_pypclockin a 
	join employees_1 b on a.employee_number = b."Emp #"  
		and a.the_date between '09/01/2020' and '08/31/2021'
	group by a.employee_number) d on a."Emp #" = d.employee_number 
order by a."Store", a.name

---------------------------------------------------------------
--/> REPORT 1
---------------------------------------------------------------

---------------------------------------------------------------
--< REPORT 2
---------------------------------------------------------------

create table jon.drivers_licenses (
  employee_number citext,
  employee_name citext,
  dl citext);
alter table jon.drivers_licenses
add primary key(employee_number)

--imported 1736 dls from ads dds.tmp_dl  
-- so this give me about half of them
select a.pymast_employee_number, a.employee_name, b.*
from arkona.ext_pymast a
left join jon.drivers_licenses b on a.pymast_employee_number = b.employee_number
where a.active_code <> 'T'
order by a.employee_name;

-- so, next, go to the dealertrack employee_status report
-- it looks like a mess, but, drivers license is on the top row for each employee
-- so it parses out easily enough sorting on row A
drop table if exists jon.temp_dl;
create table jon.temp_dl (
  employee_number citext,
  employee_name citext,
  dl citext);

insert into jon.drivers_licenses
select * from  jon.temp_dl
on conflict (employee_number)
do update
  set dl = (excluded.dl);


-- this is what i sent to jeri
select a.pymast_company_number as "Store", a.pymast_employee_number as "Emp #", a.employee_name as "Name", 
  c.data as "Job", 
  case
    when a.active_code = 'A' then 'Full'
    when a.active_code = 'P' then 'Part'
  end as "Full/Part Time", e.dl as "DL #", a.driver_lic_state_code as "DL State"
from arkona.ext_pymast a
left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
left join arkona.ext_pyprjobd c on b.company_number = c.company_number
  and b.job_description = c.job_description 
left join jon.drivers_licenses e on a.pymast_employee_number = e.employee_number
where a.active_code <> 'T'
  and pymast_company_number in ('RY1','RY2')
order by "Store", "Name"

---------------------------------------------------------------
--/> REPORT 2
---------------------------------------------------------------  