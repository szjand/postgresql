﻿/*Hey Jon-

I need a brand new census pulled for all employees in the system that are classified as Full Time.  
Could you please pull the attached information for me?  I put an example in the first line to show what information I need for each category.  
For the employees that have been here for all of 2020, I will need their total gross income for the year.  
For those employees that have been here less than a year I will likely have to figure manually so you don’t need to do anything special with them.

Please let me know if you have any questions.

Thanks!

Laura Roth 
1/4/21

jon:
1. Scheduled Hours: 40  regardless of full/part time?

2. “For those employees that have been here less than a year I will likely have to figure manually”:
                Tell me what you need, I may be able to help, I do have access to all payroll data

3. Work Phone: should this just be the store #  701-772-7211 ?  Not everyone has an extension and I am not aware of any directory of existing extensions.

laura:
If you are able to pull their actual average hours then I would like that information instead.  For those that are salary and Sales we can just put 40.  

For those that are under a year, I need their projected yearly income.  So if it’s a PDQ Tech then I usually just take $12 x 40 hours x 52 weeks and call it good with that.  

For the work phone # let’s just skip that one and leave blank.

*/

SELECT * FROM ARKONA.EXT_PYMAST where arkona.db2_integer_to_date(hire_date) > '06/01/2020' limit 20
SELECT distinct pay_period FROM ARKONA.EXT_PYMAST


drop table if exists hours;
create temp table hours as
select aa.*,
  '12/31/2020' - hire_date as days_emp,
  case
    when hire_date < '01/01/2020' then round(total_hours/52, 1)
    else round(total_hours/('12/31/2020' - hire_date)/7, 1)
  end as "Scheduled Hours"
from (
  select a.employee_number, arkona.db2_integer_to_date(b.hire_date) as hire_date, sum(a.clock_hours) as total_hours, 
    min(b.employee_name), max(b.active_code), max(b.payroll_class) as payroll_class,  max(b.base_hrly_rate) as hourly_rate
  from arkona.xfm_pypclockin a
  join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
    and b.active_code <> 'T'
    and b.pymast_company_number <> 'MJA'
  where a.the_date between '01/01/2020' and '12/31/2020'
  group by a.employee_number, arkona.db2_integer_to_date(b.hire_date)
  having sum(a.clock_hours) > 1
  order by arkona.db2_integer_to_date(b.hire_date)) aa;



-- Select string_agg("First Name" || ' ' || "Last Name", ', ') 
-- from (
select a.active_code, a.pymast_employee_number as "EID", a.employee_first_name as "First Name", a.employee_middle_name as "Middle Name",
  a.employee_last_name as "Last Name", 'Employee' as "Relationship", a.pymast_company_number as "Division", c.data as "Job Title", d.ssn,
  arkona.db2_integer_to_date(a.hire_date) as "Hire Date",
  coalesce(f."Scheduled Hours", 40) as "Scheduled Hours",
  case a.payroll_class
    when 'H' then 'Hourly'
    when 'C' then 'Commision'
    when 'S' then 'Salary'
  end as "Compensation Type",
  case  -- use lauras "formula"
    when arkona.db2_integer_to_date(a.hire_date) > '01/01/2020' then a.base_hrly_rate * 40 * 52
    else e.total
  end as "Compensation Amount", 
  case a.pay_period when 'B' then 'Bi-weekly' when 'S' then 'Semi-monthly' end as "Pay Cycle",
  arkona.db2_integer_to_date(a.birth_date) as "Birth Date", 
  a.email_address as "Email", a.sex, a.address_1 as "Address 1", a.address_2 as "Address 2",
  a.city_name as "City", a.state_code_address_ as "State", a.zip_code as "Zip Code", 
  case a.telephone_number
    when 0 then null
    else 
      case
        when a.tel_area_code = '0' then a.telephone_number::text 
        else a.tel_area_code || '-' || a.telephone_number 
      end
  end as "Personal Phone", null as "Work Phone", 
  case when a.cell_phone_number = 0 then null else a.cell_phone_number end as "Mobile Phone", 
  a.marital_status as "Marital Status"
  -- select * 
from arkona.ext_pymast a 
left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number = b.company_number
left join arkona.ext_pyprjobd c on b.company_number = c.company_number
  and b.job_description = c.job_description 
left join jon.ssn d on a.pymast_employee_number = D.employee_number
left join (
  select employee_, sum(total_gross_pay) as total
  from arkona.ext_pyhshdta
  where payroll_cen_year = 120
  group by employee_) e on a.pymast_employee_number = e.employee_
left join hours f on a.pymast_employee_number = f.employee_number
where a.pymast_company_number <> 'MJA'
  and a.active_code <> 'T'
  and arkona.db2_integer_to_date(a.hire_date) < '01/01/2021'
-- order by "Hire Date"
-- ) aa where "Compensation Amount" = 0
