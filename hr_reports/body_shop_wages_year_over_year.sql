﻿select a.employee_name as name, a.pymast_employee_number as emp_number, coalesce(b.gross_2018, 0) as gross_2018, a.gross_2019
from (
  select a.employee_name , a.pymast_employee_number, sum(b.total_gross_pay)::integer as gross_2019
  from arkona.xfm_pymast a
  left join arkona.ext_pyhshdta b on a.pymast_employee_number = b.employee_
    and b.check_year = 19
    and b.check_month between 1 and 10
  where a.current_row
    and a.active_code <> 'T'
    and a.pymast_company_number = 'RY1'
    and a.department_code = '05'
  group by a.employee_name, a.pymast_employee_number) a
left join (
  select a.employee_name, a.pymast_employee_number, sum(b.total_gross_pay)::integer as gross_2018
  from arkona.xfm_pymast a
  left join arkona.ext_pyhshdta b on a.pymast_employee_number = b.employee_
    and b.check_year = 18
    and b.check_month between 1 and 10
  where a.current_row
    and a.active_code <> 'T'
    and a.pymast_company_number = 'RY1'
    and a.department_code = '05'
  group by a.employee_name, a.pymast_employee_number) b on a.pymast_employee_number = b.pymast_employee_number
