﻿--12/16/22
---------------------------------------------------------------------------------------------
-- try to get multiple months data from accounting including sale price
-- 
-- -- do $$
-- -- 	declare
-- -- 	  _year_month integer := 202210;
-- -- begin	  
-- -- 	insert into pp.category_sourcing_1
-- 	select year_month, store, page, line, line_label, control, sum(unit_count) as unit_count, sum(amount) as sale_price
-- 	from (
-- 		select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
-- 			a.control, a.amount,
-- 			case when a.amount < 0 then 1 else -1 end as unit_count
-- 		from fin.fact_gl a
-- 		inner join dds.dim_date b on a.date_key = b.date_key
-- 			and b.year_month between 202201 and 202210-----------------------------------------------------------------------------
-- 		inner join fin.dim_account c on a.account_key = c.account_key
-- 			and c.current_row
-- 	-- add journal
-- 		inner join fin.dim_journal aa on a.journal_key = aa.journal_key
-- 			and aa.journal_code in ('VSN','VSU')
-- 		inner join ( -- d: fs gm_account page/line/acct description
-- 			select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
-- 			from fin.fact_fs a
-- 			inner join fin.dim_fs b on a.fs_key = b.fs_key
-- 				and b.year_month between 202201 and 202210  ---------------------------------------------------------------------
-- 				and b.page = 16 and b.line between 1 and 12 -- used cars, retail & wholesale
-- 			inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
-- 			inner join fin.dim_account e on d.gl_account = e.account
-- 				and e.account_type_code = '4'
-- 				and e.current_row
-- 			inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
-- 		where a.post_status = 'Y') h
-- 	group by year_month, store, page, line, line_label, control;
-- -- end $$;	

-- first issue, why is this giving  me 10 rows for each control
-- narrow it down to 2 months, and, yes, now 2 rows per control
-- add distinct, that may have done it
-- expand to 2 full years
-- 12/16/22 2 ayears data, thru nov 22 and limit to retail



select * from pp.new_categories_2 -- 5900



select * 
from pp.toc_new_categories_3
where right(stock_number, 1) = 'G'




select category, 
  count(*) filter (where source = 'trade') as trade,
  count(*) filter (where source = 'auction') as auction,
  count(*) filter (where source = 'street') as street,
  count(*) filter (where source = 'lease') as lease
--   count(*) filter (where source = 'unknown') as unknown
from (
	select * ,
		case
			when sale_price is null then 'not priced'
			when make in ('chevrolet','gmc','buick','cadillac') and size = 'Large' and shape = 'Pickup' and sale_price between 15000 and 24999 then 'gm_large_pk_15-25'
			when make in ('chevrolet','gmc','buick','cadillac') and size = 'Large' and shape = 'Pickup' and sale_price between 25000 and 34999 then 'gm_large_pk_25-35'
			when make in ('chevrolet','gmc','buick','cadillac') and size = 'Large' and shape = 'Pickup' and sale_price between 35000 and 44999 then 'gm_large_pk_35-45'
			when make in ('chevrolet','gmc','buick','cadillac') and size = 'Large' and shape = 'Pickup' and sale_price between 45000 and 145000 then 'gm_large_pk_45+'
			when make in ('chevrolet','gmc','buick','cadillac') and size = 'Small' and shape = 'SUV' and sale_price between 15000 and 24999 then 'gm_small_suv_15-25'
			when make in ('chevrolet','gmc','buick','cadillac') and size = 'Small' and shape = 'SUV' and sale_price between 25000 and 125000 then 'gm_small_suv_25+'
			when make in ('toyota','honda','nissan') and size = 'Small' and shape = 'SUV' and sale_price between 15000 and 24999 then 'import_small_suv_15-25'
			when make in ('toyota','honda','nissan') and size = 'Small' and shape = 'SUV' and sale_price between 25000 and 125000 then 'import_small_suv_25+'   
			when sale_price < 15000 then 'outlet'   
		end::citext as category	
	from (	
		select *,
			split_part(vehicletype, '_', 2)::citext as shape, 
			case
				when split_part(vehiclesegment, '_', 2) = 'Extra' then 'Extra_Large'
				else split_part(vehiclesegment, '_', 2) 
			end::citext as size,	
			coalesce(
				case
					when prior_stock is null then
						case
							when right(stock_number, 1) in ('A','B','C','D','E') then 'trade'
							when right(stock_number, 1) = 'L' then 'lease'
							when right(stock_number, 1) in ('X','M') then 'auction'
							when right(stock_number, 1) = 'P' then 'street'
						end
					else
						case
							when right(prior_stock, 1) in ('A','B','C','D','E') then 'trade'
							when right(prior_stock, 1) = 'L' then 'lease'
							when right(prior_stock, 1) in ('X','M') then 'auction'
							when right(prior_stock, 1) = 'P' then 'street'		
						end
				end::citext, 'unknown') as source	 
		from pp.toc_new_categories_4) a) b
group by category		
union
select 'TOTAL'::citext, 
  count(*) filter (where source = 'trade') as trade,
  count(*) filter (where source = 'auction') as auction,
  count(*) filter (where source = 'street') as street,
  count(*) filter (where source = 'lease') as lease
--   count(*) filter (where source = 'unknown') as unknown
from (
	select * ,
		case
			when sale_price is null then 'not priced'
			when make in ('chevrolet','gmc','buick','cadillac') and size = 'Large' and shape = 'Pickup' and sale_price between 15000 and 24999 then 'gm_large_pk_15-25'
			when make in ('chevrolet','gmc','buick','cadillac') and size = 'Large' and shape = 'Pickup' and sale_price between 25000 and 34999 then 'gm_large_pk_25-35'
			when make in ('chevrolet','gmc','buick','cadillac') and size = 'Large' and shape = 'Pickup' and sale_price between 35000 and 44999 then 'gm_large_pk_35-45'
			when make in ('chevrolet','gmc','buick','cadillac') and size = 'Large' and shape = 'Pickup' and sale_price between 45000 and 145000 then 'gm_large_pk_45+'
			when make in ('chevrolet','gmc','buick','cadillac') and size = 'Small' and shape = 'SUV' and sale_price between 15000 and 24999 then 'gm_small_suv_15-25'
			when make in ('chevrolet','gmc','buick','cadillac') and size = 'Small' and shape = 'SUV' and sale_price between 25000 and 125000 then 'gm_small_suv_25+'
			when make in ('toyota','honda','nissan') and size = 'Small' and shape = 'SUV' and sale_price between 15000 and 24999 then 'import_small_suv_15-25'
			when make in ('toyota','honda','nissan') and size = 'Small' and shape = 'SUV' and sale_price between 25000 and 125000 then 'import_small_suv_25+'   
			when sale_price < 15000 then 'outlet'   
		end::citext as category	
	from (	
		select *,
			split_part(vehicletype, '_', 2)::citext as shape, 
			case
				when split_part(vehiclesegment, '_', 2) = 'Extra' then 'Extra_Large'
				else split_part(vehiclesegment, '_', 2) 
			end::citext as size,	
			coalesce(
				case
					when prior_stock is null then
						case
							when right(stock_number, 1) in ('A','B','C','D','E') then 'trade'
							when right(stock_number, 1) = 'L' then 'lease'
							when right(stock_number, 1) in ('X','M') then 'auction'
							when right(stock_number, 1) = 'P' then 'street'
						end
					else
						case
							when right(prior_stock, 1) in ('A','B','C','D','E') then 'trade'
							when right(prior_stock, 1) = 'L' then 'lease'
							when right(prior_stock, 1) in ('X','M') then 'auction'
							when right(prior_stock, 1) = 'P' then 'street'		
						end
				end::citext, 'unknown') as source	 
		from pp.toc_new_categories_4) a) b
order by category







---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
-- 12/18 ts the category_data_dec_22.sql

select * 
from pp.toc_category_analysis_detail_2
where category = 'gm_large_pk_15-25'
  and the_date between '12/04/2022' and '12/06/2022'
order by stock_number, the_date

select * 
from pp.toc_category_analysis_detail_2
where stock_number = 'G44375RB'
order by the_date

select *, count(stock_number) over (partition by the_date order by the_date)
from pp.toc_category_analysis_detail_2
where category = 'gm_large_pk_15-25'
  and replen_status = 'acquired not on the lot'
  and the_date < '12/10/2022' -- between '12/04/2022' and '12/06/2022'
order by the_date, stock_number



		select aa.category, aa.the_date, 
			max(bb.the_count) filter (where aa.replen_status = 'on the way') as "on the way",
			max(bb.the_count) filter (where aa.replen_status = 'acquired not on the lot' and seq_2 = 1) as "acq not on lot(new)",
			max(bb.the_count) filter (where aa.replen_status = 'acquired not on the lot') as "acq not on lot",
			max(bb.the_count) filter (where aa.replen_status = 'on lot') as "on lot",
			max(bb.the_count) filter (where aa.replen_status = 'outflow') as "outflow"
		from (
			select * 
			from pp.toc_categories, 
				pp.toc_replenishment_statuses, 
				(SELECT day::date as the_date FROM generate_series(timestamp '2022-11-17', '2022-12-16', '1 day') day) a) aa
		left join (
			select a.the_date, b.category::citext, b.replen_status::citext, coalesce(count(*), 0) as the_count
			from dds.dim_date a
			left join pp.toc_category_analysis_detail_2 b on a.the_date = b.the_date
				and b.seq_1 = 1
			where a.the_date between '11/17/2022' and current_date - 1  
			group by a.the_date, b.category, b.replen_status) bb on aa.the_date = bb.the_date
				and aa.category = bb.category
				and aa.replen_status = bb.replen_status
		group by aa.the_date, aa.category

select * from pp.toc_category_analysis_detail_2 limit 1000		


			select a.the_date, b.category::citext, b.replen_status::citext, coalesce(count(*), 0) as the_count
			from dds.dim_date a
			left join pp.toc_category_analysis_detail_2 b on a.the_date = b.the_date
				and b.seq_1 = 1
			where a.the_date between '11/17/2022' and current_date - 1  
			group by a.the_date, b.category, b.replen_status

			select x.the_date, x.category::citext, x.replen_status::citext, coalesce(count(*), 0) as the_count
			from (
				select a.the_date, b.category::citext, 
					case
						when seq_2 = 1 then (b.replen_status || ' new')::citext
						else b.replen_status::citext
					end as replen_status
				from dds.dim_date a
				left join pp.toc_category_analysis_detail_2 b on a.the_date = b.the_date
					and b.seq_1 = 1
				where a.the_date between '11/17/2022' and current_date - 1) x 
			group by x.the_date, x.category, x.replen_status

		select aa.category, aa.the_date, 
			max(bb.the_count) filter (where aa.replen_status = 'on the way') as "on the way",
			max(bb.the_count) filter (where aa.replen_status = 'acquired not on the lot') as "acq not on lot",
			max(bb.the_count) filter (where aa.replen_status = 'acquired not on the lot new') as "acq not on lot new",
			max(bb.the_count) filter (where aa.replen_status = 'on lot') as "on lot",
			max(bb.the_count) filter (where aa.replen_status = 'outflow') as "outflow"
		from (
			select * 
			from pp.toc_categories, 
				pp.toc_replenishment_statuses, 
				(SELECT day::date as the_date FROM generate_series(timestamp '2022-11-17', '2022-12-16', '1 day') day) a) aa
		left join (
			select x.the_date, x.category::citext, x.replen_status::citext, coalesce(count(*), 0) as the_count
			from (
				select a.the_date, b.category::citext, 
					case
						when seq_2 = 1 then (b.replen_status || ' new')::citext
						else b.replen_status::citext
					end as replen_status
				from dds.dim_date a
				left join pp.toc_category_analysis_detail_2 b on a.the_date = b.the_date
					and b.seq_1 = 1
				where a.the_date between '11/17/2022' and current_date - 1) x 
			group by x.the_date, x.category, x.replen_status) bb on aa.the_date = bb.the_date
				and aa.category = bb.category
				and aa.replen_status = bb.replen_status
		group by aa.the_date, aa.category
order by category, the_date


















-- bad shot at graph in sql that didn't work
select stock_number, count(*)
from (
select stock_number, replen_status
from pp.toc_category_analysis_detail_2
group by stock_number, replen_status) a
group by stock_number
order by count(*) desc

select stock_number , max("on the way")
from (
select stock_number,
  repeat('*', case when sale_date > current_date then current_date - from_date else sale_date - from_date end) as "on the way"
from pp.toc_category_analysis_detail_2
where stock_number in ('G45931A','T10274A','G45821A')
  and replen_status = 'on the way') a
group by stock_number

select stock_number , max("acquired not on the lot")
from (
select stock_number,
  repeat('!', case when sale_date > current_date then current_date - from_date else sale_date - from_date end) as "acquired not on the lot"
from pp.toc_category_analysis_detail_2
where stock_number in ('G45931A','T10274A','G45821A')
  and replen_status = 'acquired not on the lot') a
group by stock_number

select stock_number , max("on lot")
from (
select stock_number,
  repeat('-', case when sale_date > current_date then current_date - from_date else sale_date - from_date end) as "on lot"
from pp.toc_category_analysis_detail_2
where stock_number in ('G45931A','T10274A','G45821A')
  and replen_status = 'on lot') a
group by stock_number
---------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------
-- 12/18
-- fuck me production summary 505 on sunda
-- 
--     select distinct biweekly_pay_period_sequence + 0  -- 366
--     from dds.dim_date
--     where the_date = current_date
-- 
-- 	select case when max(the_date) <= current_date then max(the_date) else current_date end
-- 	from dds.dim_date
-- 	where biweekly_pay_period_sequence = 366
-- 
--              select case
--                 when store = 'Rydell GM' then 1
--                 when store = 'Rydell Honda Nissan' then 2
--                 when store = 'Rydell Toyota' then 3
--                 else 0
--               end as id, team_name, employee_number, concat(first_name, ' ', last_name) as full_name,
--              sum(clock_hours) as clock_hours, sum(flag_hours) as flag_hours, sum(adjustments) as adjustments, sum(shop_time) as shop_time, sum(total_flag_hours) as total_flag_hours, 
--                 case 
--                     when sum(total_flag_hours) = 0 or sum(clock_hours) = 0 
--                     then 0 
--                     else round(sum(total_flag_hours)/sum(clock_hours),4) end as prof
--               from prs.data a
--              where a.biweekly_pay_period_sequence = 366
--              and department = 'Main Shop'
--              and store = 'Rydell GM'
--              group by team_name, employee_number, first_name, last_name,store	
-- 
-- select * 
-- from prs.data
-- where biweekly_pay_period_sequence = 366           
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------

