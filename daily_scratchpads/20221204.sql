﻿--12/4/22
---------------------------------------------------------------------------------------------
main shop production page shows 505
funciton tp.get_production_summary_by_pay_period(integer, citext)

  _current_pay_period_seq integer := (  -- 365
    select distinct biweekly_pay_period_sequence + 0
    from dds.dim_date
    where the_date = current_date);

  _date date := (  -- 2022-12-04
	select case when max(the_date) <= current_date then max(the_date) else current_date end
	from dds.dim_date
	where biweekly_pay_period_sequence = 365)    

-- select coalesce(json_build_object('ms-production-summary-pay-periods', row_to_json(c)),[])
-- from ( -- C
select json_build_object('ms-production-summary-pay-periods', coalesce(row_to_json(c), {}::json))
from ( -- C
-- select json_build_object('ms-production-summary-pay-periods', row_to_json(c))
-- from ( -- C
    select id, sum(clock_hours) as clock_hours, sum(flag_hours) as flag_hours, sum(adjustments) as adjustments,
    case 
    when sum(total_flag_hours) = 0 or sum(clock_hours) = 0  then 0
    else round(sum(total_flag_hours)/sum(clock_hours),4) end as prof, 
        sum(shop_time) as shop_time, sum(total_flag_hours) as total_flag_hours, json_agg(json_build_object('team', initcap(team_name), 'flag_hours', flag_hours,
        'total_flag_hours', total_flag_hours, 'clock_hours', clock_hours, 'shop_time', shop_time, 'adjustments', adjustments, 'prof', prof, 'techs', techs)
                order by case when team_name = 'Hourly' then 'ZZZ' else team_name end) as teams
    from ( -- B
        select id,team_name, sum(clock_hours) as clock_hours, sum(flag_hours) as flag_hours, sum(adjustments) as adjustments, 
        sum(shop_time) as shop_time, sum(total_flag_hours) as total_flag_hours,
        case 
        when sum(total_flag_hours) = 0 or sum(clock_hours) = 0 
            then 0
        else round(sum(total_flag_hours)/sum(clock_hours),4) end as prof, 
        json_agg(json_build_object('employee', full_name, 'flag_hours', flag_hours,'total_flag_hours', total_flag_hours, 
        'clock_hours', clock_hours, 'shop_time', shop_time, 'adjustments', adjustments, 'prof', prof)order by full_name) as techs
        from ( -- A
             select case
                when store = 'Rydell GM' then 1
                when store = 'Rydell Honda Nissan' then 2
                when store = 'Rydell Toyota' then 3
                else 0
              end as id, team_name, employee_number, concat(first_name, ' ', last_name) as full_name,
             sum(clock_hours) as clock_hours, sum(flag_hours) as flag_hours, sum(adjustments) as adjustments, sum(shop_time) as shop_time, sum(total_flag_hours) as total_flag_hours, 
                case 
                    when sum(total_flag_hours) = 0 or sum(clock_hours) = 0 
                    then 0 
                    else round(sum(total_flag_hours)/sum(clock_hours),4) end as prof
              from prs.data a
             where a.biweekly_pay_period_sequence = 365 --_current_pay_period_seq
             and department = 'Main Shop'
             and store = 'Rydell GM' --_store
             group by team_name, employee_number, first_name, last_name,store
         ) A
         group by team_name, id
     ) B
    group by id) c	
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------