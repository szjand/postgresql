﻿

select ro_number, labor_tech_hours, replace(labor_tech_hours, '|', ',')
from dv.service_data 
where ro_number = '16180184'

-- this works
with 
	the_data(str) as (
		select replace(labor_tech_hours, '|', ',')
		from dv.service_data 
		where ro_number = '16180184')
select elem::numeric
from the_data,
unnest(string_to_array(str, ',')) elem;



-- and so does this
select unnest( string_to_array((
	select replace(labor_tech_hours, '|', ',')
	from dv.service_data 
	where ro_number = '16180184'), ',' ) )::numeric


	
select ro_number, unnest(array[labor_tech_hours])
from dv.service_data 
where ro_number = '16180184'