﻿-- intra market trades, the lineage indicator changes

-- 3 advantage queries with wash sheet in it
-- DDS/Vehicle Acquisitions Inventory Sale/come at it from dps 2-2-15.sql (5/7/15)
-- old_vision/crayon report/wash_sheet/wash_sheet_201611.sql (1/23/17)
-- tool/Inventory/why so few trades.sql (8/1/14)



-- drop table if exists new_cars;
-- create temp table new_cars as
-- select stock_number, delivery_date, extract(month from delivery_date)
-- from sls.deals 
-- where vehicle_type_code = 'N'
--   and extract(month from delivery_date) = 2
-- order by delivery_date
-- limit 100
-- 
-- drop table if exists used_cars;
-- create temp table used_cars as
-- select stock_number, delivery_date, extract(month from delivery_date)
-- from sls.deals 
-- where vehicle_type_code = 'U'
--   and extract(month from delivery_date) = 2
-- order by delivery_date
-- limit 100
-- 
-- 
-- 
-- select distinct b.stock_number, a.inpmast_stock_number, a.inpmast_vin 
-- from arkona.xfm_inpmast a
-- join new_cars b on position(b.stock_number in a.inpmast_stock_number) > 0
--   and  b.stock_number <> a.inpmast_stock_number
-- 
-- 
-- select distinct b.stock_number, a.inpmast_stock_number, a.inpmast_vin 
-- from arkona.xfm_inpmast a
-- join used_cars b on position(b.stock_number in a.inpmast_stock_number) > 0
--   and  b.stock_number <> a.inpmast_stock_number
-- 
-- drop table if exists all_stock_numbers cascade; 
-- create temp table all_stock_numbers as 
-- select inpmast_stock_number as stock_number, inpmast_vin as vin
-- from arkona.xfm_inpmast a
-- join fin.fact_gl b on a.inpmast_stock_number = b.control  -- limit to "valid" stock_numbers
--   and b.post_status = 'Y'
-- group by inpmast_stock_number, inpmast_vin;
-- create unique index on all_stock_numbers(stock_number, vin);
-- 
-- select count(*) from all_Stock_numbers  -- 232900/ 71131 with fact_gl
-- select * from all_stock_numbers order by stock_number
-- 
-- -- uhoh, included 1, 0 & 10 as lvl_1, shit
-- select a.stock_number as lvl_1, b.stock_number as lvl2 
-- from (select * from all_stock_numbers where length(stock_number) > 4 limit 10) a
-- join all_stock_numbers b on position(a.stock_number in b.stock_number) > 0
--   and a.stock_number <> b.stock_number
-- 
-- select * from all_Stock_numbers order by length(stock_number)  
-- 
-- select * from arkona.xfm_inpmast where inpmast_stock_number = '3166'
-- 
-- select * from all_Stock_numbers where position ('30298' in stock_number) > 0 and stock_number <> '30298'
-- 
-- select stocknumber from ads.ext_vehicle_inventory_items
-- 
-- select * from arkona.ext_bopmast limit 5
-- 
-- -- 70903, 69096, 68891
-- drop table if exists bopmast_stk cascade;
-- create temp table bopmast_stk as
-- select bopmast_Stock_number as stock_number 
-- from arkona.ext_bopmast 
-- where record_status = 'U' and bopmast_stock_number is not null and date_capped > '12/31/2014'
-- group by bopmast_Stock_number;
-- create unique index on bopmast_stk(stock_number);
-- 
-- select count(*) from bopmast_stk; -- 38415
-- 
-- select a.stock_number as lvl_1, array_agg(b.stock_number order by b.stock_number) as lvl2 
-- from (select * from bopmast_stk  limit 100) a
-- join bopmast_stk b on position(a.stock_number in b.stock_number) > 0
--   and a.stock_number <> b.stock_number
--   and right(b.stock_number, 1) <> 'R'
-- group by a.stock_number  
-- order by a.stock_number  
-- 
-- -- 04/11/21 use tables from postgresql\pacing\04-04.sql as a basis
-- 
-- select * from pac.variable_daily_unit_counts  where year_month = 201601
-- 
-- select * 
-- from (
-- 	select store, year_month, 
-- 		sum(unit_count) filter (where page < 16) as new,
-- 		sum(unit_count) filter (where page = 16) as used
-- 	from pac.variable_daily_unit_counts
-- 	where (
-- 		(page between 5 and 15 and line < 40) -- retail new
-- 		or
-- 		(page = 16 and line < 8)) -- retail used
-- 	group by store, year_month) a
-- order by (new + used) desc
-- 
-- 
-- select count(*) from arkona.ext_boptrad limit 100
-- 
-- select * from arkona.ext_boptrad limit 100
-- 
-- 
-- -- bopmast & boptrad
-- drop table if exists bopmast_stk cascade;
-- create temp table bopmast_stk as
-- select bopmast_company_number as store, record_key, bopmast_Stock_number as stock_number, bopmast_vin as vin, date_capped
-- from arkona.ext_bopmast 
-- where record_status = 'U' and bopmast_stock_number is not null and date_capped > '12/31/2015'
-- group by store, record_key, bopmast_Stock_number, bopmast_vin;
-- create unique index on bopmast_stk(record_key);
-- create index on bopmast_Stk(stock_number);
-- create index on bopmast_stk(date_capped);
-- 
-- select count(*) from bopmast_stk  -- 32067
-- select count(*) from arkona.ext_boptrad  --32460
-- 
-- select *
-- from bopmast_stk a
-- left join arkona.ext_boptrad b on a.record_key = b.key
--   and a.store = b.company_number
-- 
-- select a.stock_number
-- from bopmast_stk a
-- left join arkona.ext_boptrad b on a.record_key = b.key
--   and a.store = b.company_number
-- group by a.stock_number having count(*) > 1
-- 
-- select *
-- from bopmast_stk a
-- left join arkona.ext_boptrad b on a.record_key = b.key
--   and a.store = b.company_number
-- where a.stock_number = 'G34617'
-- 
-- -- 201611 best total count new + used since 201601, 408 sales, 224 trades
-- drop table if exists first_trade cascade;
-- create temp table first_trade as
-- select stock_number as sale_stock, b.stock_ as first_trade_stock, b.vin
-- from bopmast_stk a
-- join arkona.ext_boptrad b on a.record_key = b.key
--   and a.store = b.company_number
-- where date_capped between '11/01/2016' and '11/30/2016'
--   and a.store = 'ry1';
-- create index on first_trade(sale_stock);
-- create index on first_trade(first_trade_stock);  
-- 
-- drop table if exists second_trade cascade;
-- create temp table second_trade as
-- select a.*, c.stock_ as second_trade_stock  -- 74 trades
-- from first_trade a
-- join arkona.ext_bopmast b on a.first_trade_stock = b.bopmast_stock_number
-- join arkona.ext_boptrad c on b.record_key = c.key
--   and b.bopmast_company_number = c.company_number;

-- select * from arkona.ext_bopmast limit 5;
drop table if exists bopmast cascade;
create temp table bopmast as
select bopmast_company_number as store, record_key, bopmast_stock_number as stock_number, date_capped, vehicle_type as "N/U"
from arkona.ext_bopmast
where record_status = 'U'
  and date_capped between '01/01/2011' and '03/31/2021';
create index on bopmast(store);
create index on bopmast(record_key);
create index on bopmast(stock_number);
create index on bopmast(date_capped);
create index on arkona.ext_boptrad(stock_);
create index on arkona.ext_boptrad(key);

drop table if exists level_6 cascade;  --47716
create temp table level_6 as
select a.date_capped as date_sold, a."N/U" , a.stock_number as stock_number, b.stock_ as first_trade, d.stock_ as second_trade, 
	f.stock_ as third_trade, h.stock_ as fourth_trade, j.stock_ as fifth_trade, l.stock_ as sixth_trade
from bopmast a
left join arkona.ext_boptrad b on a.record_key = b.key
  and a.store = b.company_number
left join bopmast c on b.stock_ = c.stock_number
left join arkona.ext_boptrad d on c.record_key = d.key  
  and c.store = d.company_number
left join bopmast e on d.stock_ = e.stock_number
left join arkona.ext_boptrad f on e.record_key = f.key
  and e.store = f.company_number
left join bopmast g on f.stock_ = g.stock_number
left join arkona.ext_boptrad h on g.record_key = h.key
  and g.store = h.company_number
left join bopmast i on h.stock_ = i.stock_number
left join arkona.ext_boptrad j on i.record_key = j.key
  and i.store = j.company_number
left join bopmast k on j.stock_ = k.stock_number
left join arkona.ext_boptrad l on k.record_key = l.key
  and k.store = l.company_number  
where a.store = 'ry1';
create index on level_6(stock_number);
create index on level_6(first_trade);

-- remove second -> sixth from the sale_stock
drop table if exists level_6_final cascade; -- 14884
create table level_6_final as
select a.*
from level_6 a
left join (
  select first_trade
  from level_6) b on a.stock_number = b.first_trade
where b.first_trade is null
  and a.first_trade is not null
order by a.stock_number;



-- 35420
-- 04/12/21 do an inner join on bopmast, only include sold stock_numbers
-- 33723 with join on bopmast
drop table if exists all_stock_numbers cascade;
create temp table all_stock_numbers as
select a.* 
from (
	select stock_number from level_6_final where first_trade is not null -- 23444
	union select first_trade from level_6_final
	union select second_Trade from level_6_final
	union select third_Trade from level_6_final
	union select fourth_Trade from level_6_final
	union select fifth_Trade from level_6_final
	union select sixth_Trade from level_6_final) a
join bopmast b on a.stock_number = b.stock_number	
where a.stock_number is not null;
create index on all_stock_numbers(stock_number);

-- 
-- shit pac.variable_gross_accounts dont go back far enough
-- select min(year_month) from fin.dim_Fs
-- select date_capped from bopmast order by date_capped limit 100
-- 04/12/21 added gross_type: distinguish between front (vehicle) and back (fi) gross
drop table if exists variable_accounts cascade; --31308
create temp table variable_accounts as
select distinct b.year_month, d.store, b.page, b.line_label, b.line, b.col, c.gl_account,
  case
    when b.page = 17 then 'fi_gross'
    else 'vehicle_gross'
  end as gross_type
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201101 and 202103
  and (
		(b.page in (5,7,8,9,10,14) and b.line < 48)
		or
		(b.page = 16 and line < 12)
		or
		(b.page = 17 and line < 20))
join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key  
join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  and d.area = 'variable'; 
create unique index on variable_Accounts(year_month, gl_account);  
create index on variable_accounts (gl_account); 
create index on variable_accounts(gross_type); 


-- 04/21 break out gross_type, include only sold stock_numbers, count goes down to 33707
--26 sec
drop table if exists stock_number_gross cascade;  --34917
create temp table stock_number_gross as
select aa.stock_number, 
  (sum(-a.amount) filter (where gross_type = 'vehicle_gross'))::integer as vehicle_gross,
  (sum(-a.amount) filter (where gross_type = 'fi_gross'))::integer as fi_gross,
  sum(-a.amount)::integer as total_gross
from all_stock_numbers aa
join fin.fact_gl a on aa.stock_number = a.control
join dds.dim_date b on a.date_key = b.date_key
join fin.dim_account c on a.account_key = c.account_key
join variable_accounts d on c.account = d.gl_account  
  and b.year_month = d.year_month
where a.post_status = 'Y'
group by aa.stock_number;
create index on stock_number_gross(stock_number);


-- select * from stock_number_gross where stock_number = 'G34772'
-- select * from level_6_final limit 10

-- sent as wash_sheet_v1.xlsx
-- sent as wash_sheet_v2, fixed the multiple rows for multiple trades
-- still shows separate lines for multiple trades (aa, bb, etc), makes some sense
-- wash_sheet_v3 added date_sold, n/u
-- wash_sheet_v4 break out front and back gross on initial sale, removed rows with unsold trades
-- 07/4/22 saved in lisa_toc, limited to only those with at least a 4th trade, manually dedited spreadsheet
--		to remove obvious wtf, eg, 34000 gross, limited to sales in 2015 and beyond
select a.date_sold, a."N/U", a.stock_number, coalesce(b.vehicle_gross, 0) as vehicle_gross, 
  coalesce(b.fi_gross, 0) as fi_gross, coalesce(b.vehicle_gross, 0) + coalesce(b.fi_gross, 0) as total_gross,-- 14880
  a.first_trade, c.total_gross,
  a.second_trade, d.total_gross,
  a.third_trade, e.total_gross,
  a.fourth_trade, f.total_gross,
  a.fifth_trade, g.total_gross,
  a.sixth_trade, h.total_gross,
  (coalesce(b.total_gross, 0) + coalesce(c.total_gross, 0) + coalesce(d.total_gross, 0) + coalesce(e.total_gross, 0) + 
		coalesce(f.total_gross, 0) + coalesce(g.total_gross, 0) + coalesce(h.total_gross, 0)) as total_gross
from level_6_final a
left join stock_number_gross b on a.stock_number = b.stock_number
left join stock_number_gross c on a.first_trade = c.stock_number
left join stock_number_gross d on a.second_trade = d.stock_number
left join stock_number_gross e on a.third_trade = e.stock_number
left join stock_number_gross f on a.fourth_trade = f.stock_number
left join stock_number_gross g on a.fifth_trade = g.stock_number
left join stock_number_gross h on a.sixth_trade = h.stock_number
where first_trade is not null 
  and fourth_trade is not null  -- added 7/4/22, just a sample wash sheet for TOC conversation
  -- clean up based on goofy first_trade values
  and a.stock_number not in ('20241','30694XX','13871','16895A','16860','21797','17205','19715XX','19174','YK197074','XH422691','X4634151','TU225269','TJ384367')
order by stock_number, first_trade, second_trade, third_trade;


