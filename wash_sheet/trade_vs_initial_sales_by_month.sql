﻿

select control, page, line, 
  case when page = 16 then 'U' else 'N' end as "N/U",
  case 
    when page <> 16 then 'initial'
    else 
      case
        when right(control, 1) in ('L','M','X','P') then 'initial'
        else 'trade'
      end
  end as source
from pac.variable_daily_unit_counts 
where year_month > 201712
  and unit_count = 1
  and store = 'ry1'
  and 
    case 
      when page <> 16 then line < 40
      else line < 8
    end
order by year_month, page, line    

-- uploaded
-- response: -- Cool -- I don't see new car sales though. May be helpful to have New Sales, Purchased Used Sales, New Trade Sales and Used Trade Sales
select b.*, (100 * b.initial/b.total)::TEXT || '%' as "intial %", (100 * b.trade/b.total)::TEXT || '%' as "trade %"
from (
	select year_month, 
		count(source) filter (where source = 'initial') as initial,
		count(source) filter (where source = 'trade') as trade,
		count(source) as total
	from (  
		select year_month, control, page, line, 
			case
				when right(control, 1) in ('L','M','X','P') then 'initial'
				else 'trade'
			end as source
		from pac.variable_daily_unit_counts 
		where year_month > 201712
			and unit_count = 1
			and store = 'ry1'
			and page = 16
			and line < 8) a
	group by year_month) b
order by year_month

-- uploaded as trade_vs_intial_v2
-- think this is wrong, this would classify an ----XA as a new trade sale, and shouldn't
select year_month, 
  count(*) filter (where page <> 16) as "New Sales",
  count(*) filter (where page = 16 and right(control, 1) in ('L','M','X','P')) as "Purchased Used Sales",
  count(*) filter (where page = 16 and right(control, 1) = 'A') as "New Trade Sales",
  count(*) filter (where page = 16 and right(control, 1) <> 'A') as "Used Trade Sales"
from pac.variable_daily_unit_counts 
where year_month > 201712
  and unit_count = 1
  and store = 'ry1'
  and 
    case 
      when page <> 16 then line < 40
      else line < 8
    end
group by year_month    
order by year_month

--- v3
select right(control, 2), right(control, 3), count(*)
from pac.variable_daily_unit_counts a
where year_month > 202100
  and right(control, 1) = 'A'
  and unit_count = 1
  and store = 'ry1'
  and 
    case 
      when page <> 16 then line < 40
      else line < 8
    end
group by right(control, 2), right(control, 3)   

select right(control, 2), count(*)
from pac.variable_daily_unit_counts a
where year_month > 202100
  and right(control, 1) = 'A'
  and unit_count = 1
  and store = 'ry1'
  and 
    case 
      when page <> 16 then line < 40
      else line < 8
    end
group by right(control, 2)


select * from pac.variable_daily_unit_counts 
-- where right(control, 2) in ('LA','MA', 'PA', 'XA')
where page = 16 and right(control, 1) = 'A' and right(control, 2) not in ('LA','MA','PA')
  and unit_count = 1
  and store = 'ry1'
--   and line < 8
order by right(control, 2), control

-- this looks better
-- total matches up with the first spreadsheet which was much more basic
-- still mind fucking on the meaning of each column 
-- uploaded as v3
select a.*, "Purchased Used Sales" + "New Trade Sales" + "Used Trade Sales" as "Total Used Sales"
from (
	select year_month, 
		count(*) filter (where page <> 16) as "New Sales",
		count(*) filter (where page = 16 and ((right(control, 1) in ('L','M','X','P'))) or (right(control, 2) in ('LA','MA','PA','XA'))) as "Purchased Used Sales",
		count(*) filter (where page = 16 and right(control, 1) = 'A' and right(control, 2) not in ('LA','MA','PA','XA')) as "New Trade Sales",
		count(*) filter (where page = 16 and right(control, 1) <> 'A' and right(control, 1) not in ('L','M','X','P')) as "Used Trade Sales"
	from pac.variable_daily_unit_counts 
	where year_month > 201712
		and unit_count = 1
		and store = 'ry1'
		and 
			case 
				when page <> 16 then line < 40
				else line < 8
			end
	group by year_month) a 
order by year_month

-- testing my assumptions
select year_month, page, line, control,
  case when page <> 16 then 'X' end  as "New Sales",
  case when page = 16 and ((right(control, 1) in ('L','M','X','P')) or (right(control, 2) in ('LA','MA','PA','XA'))) then 'X' end as "Purchased Used Sales",
  case when page = 16 and right(control, 1) = 'A' and right(control, 2) not in ('LA','MA','PA','XA') then 'X' end as "New Trade Sales",
  case when page = 16 and right(control, 1) <> 'A' and right(control, 1) not in ('L','M','X','P') then 'X' end as "Used Trade Sales"
from pac.variable_daily_unit_counts 
where year_month = 202103
  and unit_count = 1
  and store = 'ry1'
  and page = 16 and line < 8


