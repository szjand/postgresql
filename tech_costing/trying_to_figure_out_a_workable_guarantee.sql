﻿/*
Would there be a way that we could make Dale and Jared finish at 125% every time without having 
to flag them it OT tech time? We have been managing those hours/dollars because that is where 
our tech training time goes as well. The issue we run into is that the dollar amount that is 
associated with how many hours we have to flag them to get them to 125% is a large amount. 
We have these guys do all of the very tedious type high level driveability repair work and 
we cannot claim all the labor time involved in most cases. Thoughts or potential solutions 
you could think of? I have them costed out at a pretty high dollar amount and I am wondering 
if that number should be less? If it ends up being less it could hurt us once 
adjusted cost of labor is figured out. 

jon:
I either don’t remember or just don’t know how to figure costing, can you help?

andrew:
If you look under technicians in application environment under their technician numbers 
it will show you what their costing is per sold hour. Dale is at $65.00 and Jared is at $55.00.

jon:
Have you considered hourly for Dale & Jared?
Just tossing it out there, I am frustrated with how little I understand the big picture and don’t really even understand if hourly is a workable solution for you or not.
What bothers me, is that we should be able to come up with something that is clear rather than trying to game a pay plan that doesn’t work.

Just venting

I am trying to put some numbers together and see if I can  come up with a picture of how all this shit works

andrew
That does actually make sense if we were to do that. I don’t think there is a whole lot of overtime put in by either of 
them but that would probably be the only hang-up. The good part about them being hourly would be we could cost them out 
correctly though. I will keep you in the loop once Ken, Randy and myself get a chance to spend some time visiting about this
*/

main table for old acl (sco_test.acl_v1) stuff is dds2/scripts/accounting/financial_statement/scratchpad_2.sql

select  b.yearmonth, b.thedate, aa.storecode, 
  CASE 
    WHEN a.gtacct IN ('124700', '124704', '224700', '224704') THEN 'Main Shop'
    WHEN a.gtacct IN ('124701','224701', '124723', '224723') THEN 'PDQ'
    WHEN a.gtacct = '124703' THEN 'Body Shop'
    WHEN a.gtacct IN ('124702','124724') THEN 'Detail'
  END AS department, a.gtacct, a.gttamt,
  a.gtjrnl, a.gtctl#
INTO #wtf
from dds.stgArkonaGLPTRNS a
INNER JOIN dds.gmfs_accounts aa on a.gtacct = aa.gl_account
INNER JOIN dds.day b on a.gtdate = b.thedate
WHERE a.gtdate between '02/01/2021' AND '02/28/2021'
  AND a.gtjrnl <> 'std'
  AND aa.gm_account = '247';

drop table if exists wtf cascade;
create temp table wtf as
select  b.year_month, b.the_date, 
  CASE 
    WHEN c.account IN ('124700', '124704', '224700', '224704') THEN 'Main Shop'
    WHEN c.account IN ('124701','224701', '124723', '224723') THEN 'PDQ'
    WHEN c.account = '124703' THEN 'Body Shop'
    WHEN c.account IN ('124702','124724') THEN 'Detail'
  END AS department, c.account, a.amount,
  e.journal_code, a.control
-- select a.*  
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 202102
join fin.dim_account c on a.account_key = c.account_key
join fin.dim_fs_account d on c.account = d.gl_account
  and d.gm_account = '247'
join fin.dim_journal e on a.journal_key = e.journal_key
  and e.journal_code <> 'std';

select * from wtf limit 10

select year_month, account, 
  SUM(CASE WHEN journal_code IN ('sca','svi','swa') THEN amount ELSE 0 END) AS costed,
  SUM(CASE WHEN journal_code IN ('pay','gli') THEN amount ELSE 0 END) AS paid
FROM wtf
GROUP BY year_month, account  

-- where does 247 go on the statement

select b.* 
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202102
join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  and c.gl_account like '%247%'  


-- from the statement and this gives the correct amounts
-- adj cost of labor
select g.*, sum(amount) over (partition by store, line)
from (
  select f.store, d.gl_account, b.line, e.description, e.department, a.amount
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 202102
    and b.page = 16
    and b.line in (30,39)
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  inner join fin.dim_account e on d.gl_account = e.account
    and e.current_row
  inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) g

------------------------------------------------------------------------------------------------------------
-- jared and dale overtime
select b.year_month, 
  coalesce(sum(ot_hours) filter (where employee_number = '18005'), 0) as dale,
  sum(ot_hours) filter (where employee_number = '136170') as jared
from arkona.xfm_pypclockin a
join dds.dim_date b on a.the_date = b.the_date
  and b.the_year in (2019,2020,2021)
where employee_number in ('18005','136170')
  and ot_hours <> 0
group by b.year_month
------------------------------------------------------------------------------------------------------------

select * from fin.dim_Account where account like '1247%'

select * from fin.dim_fs_account where gm_account = '247'

select * 
from fin.dim_fs limit 100
------------------------------------------------------------------------------------------------------------
how is costing done on menu items



------------------------------------------------------------------------------------------------------------
-- lets do the easy part first, pay
select * from arkona.ext_pymast where employee_last_name in ('duckstad','axtman')
dale: 18005   65/hr  595
jared: 136170 55/hr  537

pay periods: 1/31 - 2/13 2/14 - 2/27
which translates to check dates of 2/19 & 3/5 

select * from dds.dim_tech where tech_name like '%axtman%' or tech_name like '%duckstad%'


-- this matches the payroll spreadsheets  
select employee_, check_month,check_day,payroll_run_number, total_gross_pay
from arkona.ext_pyhshdta 
where employee_ in ('18005','136170')
  and check_year = 21
  and payroll_run_number in (219210,305210)
order by employee_, check_month, check_day  

dale
jared
                                pto                flag      clock         pto 
Axtman	Axtman	  Dale	18005	  35	  35	70	125	102.68	0	102.68	82.14	0	0	0	3593.63		3593.63
Axtman	Duckstad	Jared	136170	30.29	30	60	125	 97.17	0	 97.17	77.74	0	8	0	3157.57		3157.57  2915.25
Axtman	Axtman	  Dale	18005	  43.65	35	70	125	116.6	  0	116.6 	93.28	0	0	0	4224.5		4224.5
Axtman	Duckstad	Jared	136170	37.39	30	60	125	122.12	0	122.12	97.7	0	0	0	3952.5		3952.5

select 3157.57 - (8 * 30.29)

-- what i want to see here is how many flag hours are padded by opcode
select b.tech_name,
  sum(a.flag_hours) filter (where c.opcode = 'shop' and a.flag_date between '01/31/2021' and '02/13/2021') as shop_1,
  sum(a.flag_hours) filter (where c.opcode <> 'shop' and a.flag_date between '01/31/2021' and '02/13/2021') as other_1,
  sum(a.flag_hours) filter (where c.opcode = 'shop' and a.flag_date between '02/14/2021' and '02/27/2021') as shop_2,
  sum(a.flag_hours) filter (where c.opcode <> 'shop' and a.flag_date between '02/14/2021' and '02/27/2021') as other_2 
from dds.fact_repair_order a
join dds.dim_tech b on a.tech_key = b.tech_key
  and b.tech_number in ('537','595')
join dds.dim_opcode c on a.opcode_key = c.opcode_key  
where a.flag_date between '01/31/2021' and '02/27/2021'
group by b.tech_name