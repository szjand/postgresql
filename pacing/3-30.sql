﻿-- trying to resolve count differences

drop table if exists month_days cascade;
create temp table month_days as
select year_month,
  (select max(the_date)as the_first from dds.dim_date where year_month = a.year_month and first_day_of_month),
  (select max(the_date) as the_last from dds.dim_date where year_month = a.year_month and last_day_of_month)
from dds.dim_date a
group by a.year_month;
create unique index on month_days(year_month);
create unique index on month_days(the_first);
create unique index on month_days(the_last);

-- variable sale accounts
-- used to determine the sales count
drop table if exists fs_sale_accounts;
create temp table fs_sale_accounts as
select distinct b.year_month, f.store, d.gl_account, b.page, b.line, b.line_label, e.description
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
	and (
		(b.page between 5 and 15 and b.line between 1 and 45) 
		or
		(b.page = 16 and b.line between 1 and 14)) 
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
	and e.account_type_code = '4'
	and e.current_row
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key;
create unique index on fs_sale_accounts(year_month, gl_account);
create index on fs_sale_accounts(gl_account);
create index on fs_sale_accounts(page);
create index on fs_sale_accounts(line);

drop table if eXISTS fs_count cascade;
-- this gives the fs count at the month/line level
create temp table fs_count as
select year_month, store, page, line, line_label, control, sum(unit_count) as unit_count
from (
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201601 and 202103
  join fin.dim_account c on a.account_key = c.account_key
--     this fixed the blazer count problem  
--     and c.current_row  -- this is not required, the join is on the account_key which is in itself time sensitive
--     whereas the current row key may not match the dated record in fact_gl
  join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')  
  join fs_sale_accounts d on c.account = d.gl_account
    and b.year_month = d.year_month
  where a.post_status = 'Y' 
    and a.control not in ('RGJ240218','HGJ190420','H11735', 'G34382')) h
group by year_month, store, page, line, line_label, control;
create unique index on fs_count(year_month, control) where unit_count = 1;

select year_month,
  sum(unit_count) filter (where page = 5 and line < 20) as chev_cars,
  sum(unit_count) filter (where page = 5 and line between 25 and 38) as chev_trucks,
  sum(unit_count) filter (where page = 5 and line in (22, 43)) as chev_fleet,
  sum(unit_count) filter (where page = 5 and line in (23, 44)) as chev_internal,
  sum(unit_count) filter (where page = 8 and line < 20) as buick_cars,
  sum(unit_count) filter (where page = 8 and line between 25 and 38) as buick_trucks,
  sum(unit_count) filter (where page = 8 and line in (22, 43)) as buick_fleet,
  sum(unit_count) filter (where page = 8 and line in (23, 44)) as buick_internal,
  sum(unit_count) filter (where page = 9 and line < 20) as cad_cars,
  sum(unit_count) filter (where page = 9 and line between 25 and 38) as cad_trucks,
  sum(unit_count) filter (where page = 9 and line in (22, 43)) as cad_fleet,
  sum(unit_count) filter (where page = 9 and line in (23, 44)) as cad_internal,
  sum(unit_count) filter (where page = 10 and line between 25 and 38) as gmc_trucks,
  sum(unit_count) filter (where page = 10 and line in (22, 43)) as gmc_fleet,
  sum(unit_count) filter (where page = 10 and line in (23, 44)) as gmv_internal,
  sum(unit_count) filter (where page = 16 and line between 1 and 2) as used_cars_ret,
  sum(unit_count) filter (where page = 16 and line between 4 and 5) as used_trucks_ret,
  sum(unit_count) filter (where page = 16 and line = 8) as used_cars_ws,
  sum(unit_count) filter (where page = 16 and line = 10) as used_trucks_ws  
from fs_count
where store = 'RY1'
group by year_month; 


select year_month,
  sum(unit_count) filter (where page = 7 and line < 20) as honda_cars,
  sum(unit_count) filter (where page = 7 and line between 25 and 40) as honda_trucks,
  sum(unit_count) filter (where page = 14 and line < 20) as nissan_cars,
  sum(unit_count) filter (where page = 14 and line between 25 and 40) as nissan_trucks,
  sum(unit_count) filter (where page = 16 and line between 1 and 2) as used_cars_ret,
  sum(unit_count) filter (where page = 16 and line between 4 and 5) as used_trucks_ret,
  sum(unit_count) filter (where page = 16 and line = 8) as used_cars_ws,
  sum(unit_count) filter (where page = 16 and line = 10) as used_trucks_ws  
from fs_count
where store = 'RY2'
group by year_month; 


drop table if exists daily_count cascade;
-- daily counts
-- grouped to handle multiple dates in accounting
-- to get the count correct, in this table, removed unit_count from the grouping and summed it,
-- include unwinds (-1 unit_count), include unit_count in PK
create temp table daily_count as
select distinct aa.year_month, aa.store, aa.page, aa.line, aa.line_label, aa.control,
	sum(aa.unit_count) as unit_count, min(bb.the_date) as the_date
from fs_count aa 
left join ( 
	select b.year_month, a.control, b.the_date, c.account, e.journal_code, a.amount
	from fin.fact_gl a
	join dds.dim_date b on a.date_key = b.date_key
	join fin.dim_account c on a.account_key = c.account_key
	join fs_sale_accounts d on c.account = d.gl_account
		and b.year_month = d.year_month
	join fin.dim_journal e on a.journal_key = e.journal_key
		and e.journal_code in ('VSN','VSU')
	where a.post_status = 'Y') bb on aa.control = bb.control
		and aa.year_month = bb.year_month
where aa.unit_count <> 0
group by aa.year_month, aa.store, aa.page, aa.line, aa.line_label, aa.control;
-- 	aa.unit_count;
create unique index on daily_count(year_month,control,unit_count);




-- ok, this fixed the 202001 nc+2, check the rest
drop table if exists daily_count cascade;
-- daily counts
-- grouped to handle multiple dates in accounting
-- to get the count correct, in this table, removed unit_count from the grouping and summed it,
-- include unwinds (-1 unit_count), include unit_count in PK
-- should no longer need to sum the unit_count, in fact, think no need for outer grouping
-- because the pk on fs_count is month/control
-- remove the where aa.unit_count <> 0
-- still have cases like 32696xxb, sold, unwind, sold all in the same month, forgot the unique key
-- in fs_count on month/control is conditional
-- did the same conditional unique index on this
create temp table daily_count as
select distinct aa.year_month, aa.store, aa.page, aa.line, aa.line_label, aa.control,
	sum(unit_count) as unit_count, bb.the_date
from fs_count aa 
-- this is the left join for daily count
-- joins to fs_account on control & year_month only	  
-- returns 3 rows, this is the problem, this inner query needs to retrun 1 row per control/month, i think
-- this subquery provides the date for the daily count, so can group this and do a min(date) at this level
-- will now need to include the_date in the outer grouping
left join ( 
	select b.year_month, a.control, min(b.the_date) as the_date --, c.account, e.journal_code, a.amount
	from fin.fact_gl a
	join dds.dim_date b on a.date_key = b.date_key
	join fin.dim_account c on a.account_key = c.account_key
	join fs_sale_accounts d on c.account = d.gl_account
		and b.year_month = d.year_month
	join fin.dim_journal e on a.journal_key = e.journal_key
		and e.journal_code in ('VSN','VSU')
	where a.post_status = 'Y'
-- 	  and a.control = 'g38994'    
  group by b.year_month, a.control) bb on aa.control = bb.control
		and aa.year_month = bb.year_month
where aa.unit_count <> 0
group by aa.year_month, aa.store, aa.page, aa.line, aa.line_label, aa.control, bb.the_date;
create unique index on daily_count(year_month,control) where unit_count = 1;





-- this is the way to get the correct totals
select *, sum(unit_count) over (partition by page, line)
from daily_count
where year_month = 202103
  and store = 'ry1'
  and page = 16

-- and the running total
select b.*, sum(daily_count) over (partition by store, page order by the_date asc rows between unbounded preceding and current row)
from (
	select store, page, the_date, sum(unit_count) as daily_count
	from daily_count a
	where year_month = 202103
	group by store, page, the_date) b
order by store, page, the_date

lets check a bunch of different statements

ry2 201612 uc 4 hi, 201606 uc 4 hi
ry1 201606 uc 1 lo 201706 uc 2 hi 201701 nc 2 hi, 201806 uc 2 hi, 201912 nc 6 lo, 201801 nc 2 lo, 201901 nc 1 hi, 202001 uc 2 hi, nc 1 lo

ry1 201601 nc +4 uc +4, 201706 uc+2,201712 nc+2,201806 uc+2,202001 uc+2 nc +2

select year_month,
  sum(unit_count) filter (where page = 5) as chev,
  sum(unit_count) filter (where page = 8) as buick,
  sum(unit_count) filter (where page = 9) as cad,
  sum(unit_count) filter (where page = 10) as gmc,
  sum(unit_count) filter (where page = 16) as uc,
  sum(unit_count) filter (where page in (5,8,9,10,16)) as total
from daily_count
where store = 'RY1'
group by year_month
order by year_month

select year_month, line,
  sum(unit_count) filter (where page = 5) as chev,
  sum(unit_count) filter (where page = 8) as buick,
  sum(unit_count) filter (where page = 9) as cad,
  sum(unit_count) filter (where page = 10) as gmc,
  sum(unit_count) filter (where page = 16) as uc,
  sum(unit_count) filter (where page in (5,8,9,10,16)) as total
from daily_count
where store = 'RY1'
  and year_month = 202001
group by year_month, line
order by year_month

202001 nc
	gmc +2 line 31
select * from fs_sale_accounts where year_month = 202001 and page = 10 order by line

select * from daily_count where year_month = 202001 and page = 10 and line = 31
unit_count = 3 for G38994

	select b.year_month, a.control, b.the_date, c.account, e.journal_code, a.amount
	from fin.fact_gl a
	join dds.dim_date b on a.date_key = b.date_key
	join fin.dim_account c on a.account_key = c.account_key
	  and 
	join fs_sale_accounts d on c.account = d.gl_account
		and b.year_month = d.year_month
	join fin.dim_journal e on a.journal_key = e.journal_key
		and e.journal_code in ('VSN','VSU')
	where a.post_status = 'Y'
	
select b.*, sum(daily_count) over (partition by store, page order by the_date asc rows between unbounded preceding and current row)
from (
	select store, page, the_date, sum(unit_count) as daily_count
	-- select *
	from daily_count a
	where year_month = 202001
	  and page = 10
	  and line = 31 and control = 'G38994'
	group by store, page, the_date) b
order by store, page, the_date

select * from fs_count where control = 'g38994'
-- this is the basis for fs_count and looks correct
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 202001
  join fin.dim_account c on a.account_key = c.account_key
  join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')  
  join fs_sale_accounts d on c.account = d.gl_account
    and b.year_month = d.year_month
  where a.post_status = 'Y' 
    and a.control = 'G38994'

-- this is the left join for daily count
-- joins to fs_account on control & year_month only	  
-- returns 3 rows, this is the problem, this inner query needs to retrun 1 row per control/month, i think
-- this subquery provides the date for the daily count, so can group this and do a min(date) at this level
	select b.year_month, a.control, min(b.the_date) as the_date --, c.account, e.journal_code, a.amount
	from fin.fact_gl a
	join dds.dim_date b on a.date_key = b.date_key
	join fin.dim_account c on a.account_key = c.account_key
	join fs_sale_accounts d on c.account = d.gl_account
		and b.year_month = d.year_month
	join fin.dim_journal e on a.journal_key = e.journal_key
		and e.journal_code in ('VSN','VSU')
	where a.post_status = 'Y'
	  and a.control = 'g38994'    
  group by b.year_month, a.control


drop table if exists tem.fs_actual_counts cascade;
create table tem.fs_actual_counts (
  store citext,
  year_month integer,
  new integer default '0',
  used integer default '0',
  total integer default '0',
  primary key (store,year_month));

insert into tem.fs_actual_counts (store, year_month)
select distinct 'ry1', year_month
from dds.dim_date
where year_month between 201601 and 212002;  

update tem.fs_actual_counts
set new = total - used
where used <> 0
