﻿drop schema if exists pac cascade;
create schema pac;
comment on schema pac is 'pac, short for pacing, tables for attempting to generate a concensus on reliable monthly pacing. the initial cut covers January 2016 thru March 2021';

/*
06/02/21
  updated table pac.variable_daily_unit_counts  to include data thru 202105
*/
-----------------------------------------------------------------------------------------------------
--< dates
-----------------------------------------------------------------------------------------------------

drop table if exists pac.month_first_and_last_days cascade;
create table pac.month_first_and_last_days as
select year_month,
  (select max(the_date)as the_first from dds.dim_date where year_month = a.year_month and first_day_of_month),
  (select max(the_date) as the_last from dds.dim_date where year_month = a.year_month and last_day_of_month)
from dds.dim_date a
group by a.year_month;
create unique index on pac.month_first_and_last_days(year_month);
create unique index on pac.month_first_and_last_days(the_first);
create unique index on pac.month_first_and_last_days(the_last);
comment on table pac.month_first_and_last_days is 'for each year_month, the date of the first day of the month and the date of the lastt day of the month';


-- working days
drop table if exists pac.variable_working_days cascade;
create table pac.variable_working_days as
select a.year_month, a.the_date, a.day_of_week, b.day_name, a.wd_of_month, a.wd_in_month, a.wd_of_month_elapsed, a.wd_of_month_remaining 
from dds.working_days a
join dds.dim_date b on a.the_date = b.the_date
where a.department = 'sales';
create unique index on pac.variable_working_days(the_date);

-- weeks
drop table if exists pac.weeks cascade;
create table pac.weeks as
select a.the_date, a.day_of_month,
  case
    when a.day_of_month between 1 and 7 then 1
    when a.day_of_month between 8 and 14 then 2
    when a.day_of_month between 15 and 21 then 3
    when a.day_of_month > 21 then 4
  end as week_of_month
from dds.dim_Date a
join dds.dim_date b on a.year_month = b.year_month
  and b.last_day_of_month
where a.the_year > 2015
order by a.the_date;
create unique index on pac.weeks(the_date);



-----------------------------------------------------------------------------------------------------
--/> dates
-----------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------
--/> dates
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
--< count
-----------------------------------------------------------------------------------------------------
-- variable sale accounts
-- used to determine the sales count
drop table if exists pac.variable_sale_accounts cascade;
create table pac.variable_sale_accounts as
select distinct b.year_month, f.store, d.gl_account, b.page, b.line, b.line_label, e.description
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
	and (
		(b.page between 5 and 15 and b.line between 1 and 45) 
		or
		(b.page = 16 and b.line between 1 and 14)) 
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
	and e.account_type_code = '4'
	and e.current_row
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key;
create unique index on pac.variable_sale_accounts(year_month, gl_account);
create index on pac.variable_sale_accounts(gl_account);
create index on pac.variable_sale_accounts(page);
create index on pac.variable_sale_accounts(line);
comment on table pac.variable_sale_accounts is 'variable sale accounts from financial statements, grain of month/account,
  these accounts are used to determine the number of units sold, matches financial statment unit counts';
  
drop table if exists pac.variable_daily_unit_counts cascade;
create table pac.variable_daily_unit_counts as
select distinct aa.year_month, aa.store, aa.page, aa.line, aa.line_label, aa.control,
	sum(aa.unit_count) as unit_count, bb.the_date
from ( -- monthly totals	
	select year_month, store, page, line, line_label, control, sum(unit_count) as unit_count
	from (
		select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
			a.control, a.amount,
			case when a.amount < 0 then 1 else -1 end as unit_count
		from fin.fact_gl a
		join dds.dim_date b on a.date_key = b.date_key
			and b.year_month between 201601 and 202105
		join fin.dim_account c on a.account_key = c.account_key
		join fin.dim_journal aa on a.journal_key = aa.journal_key
			and aa.journal_code in ('VSN','VSU')  
		join pac.variable_sale_accounts d on c.account = d.gl_account
			and b.year_month = d.year_month
		where a.post_status = 'Y' 
			and a.control not in ('RGJ240218','HGJ190420','H11735', 'G34382')) h
	group by year_month, store, page, line, line_label, control) aa
left join ( -- dates
	select b.year_month, a.control, min(b.the_date) as the_date --, c.account, e.journal_code, a.amount
	from fin.fact_gl a
	join dds.dim_date b on a.date_key = b.date_key
	join fin.dim_account c on a.account_key = c.account_key
	join pac.variable_sale_accounts d on c.account = d.gl_account
		and b.year_month = d.year_month
	join fin.dim_journal e on a.journal_key = e.journal_key
		and e.journal_code in ('VSN','VSU')
	where a.post_status = 'Y'
  group by b.year_month, a.control) bb on aa.control = bb.control
		and aa.year_month = bb.year_month
where aa.unit_count <> 0
group by aa.year_month, aa.store, aa.page, aa.line, aa.line_label, aa.control, bb.the_date;
create unique index on pac.variable_daily_unit_counts(the_date,control,page,line);
create index on pac.variable_daily_unit_counts(control);
create index on pac.variable_daily_unit_counts(year_month);
create index on pac.variable_daily_unit_counts(page);
create index on pac.variable_daily_unit_counts(line);
create index on pac.variable_daily_unit_counts(store);create index on pac.variable_daily_unit_counts(store);
comment on table pac.variable_daily_unit_counts is 'daily count of deals as represented on financial statement';

select year_month,
  sum(unit_count) filter (where page = 5 and line < 20) as chev_cars,
  sum(unit_count) filter (where page = 5 and line between 25 and 38) as chev_trucks,
  sum(unit_count) filter (where page = 5 and line in (22, 43)) as chev_fleet,
  sum(unit_count) filter (where page = 5 and line in (23, 44)) as chev_internal,
  sum(unit_count) filter (where page = 8 and line < 20) as buick_cars,
  sum(unit_count) filter (where page = 8 and line between 25 and 38) as buick_trucks,
  sum(unit_count) filter (where page = 8 and line in (22, 43)) as buick_fleet,
  sum(unit_count) filter (where page = 8 and line in (23, 44)) as buick_internal,
  sum(unit_count) filter (where page = 9 and line < 20) as cad_cars,
  sum(unit_count) filter (where page = 9 and line between 25 and 38) as cad_trucks,
  sum(unit_count) filter (where page = 9 and line in (22, 43)) as cad_fleet,
  sum(unit_count) filter (where page = 9 and line in (23, 44)) as cad_internal,
  sum(unit_count) filter (where page = 10 and line between 25 and 38) as gmc_trucks,
  sum(unit_count) filter (where page = 10 and line in (22, 43)) as gmc_fleet,
  sum(unit_count) filter (where page = 10 and line in (23, 44)) as gmv_internal,
  sum(unit_count) filter (where page = 16 and line between 1 and 2) as used_cars_ret,
  sum(unit_count) filter (where page = 16 and line between 4 and 5) as used_trucks_ret,
  sum(unit_count) filter (where page = 16 and line = 8) as used_cars_ws,
  sum(unit_count) filter (where page = 16 and line = 10) as used_trucks_ws  
from fs_count
where store = 'RY1'
group by year_month; 


select year_month,
  sum(unit_count) filter (where page = 7 and line < 20) as honda_cars,
  sum(unit_count) filter (where page = 7 and line between 25 and 40) as honda_trucks,
  sum(unit_count) filter (where page = 14 and line < 20) as nissan_cars,
  sum(unit_count) filter (where page = 14 and line between 25 and 40) as nissan_trucks,
  sum(unit_count) filter (where page = 16 and line between 1 and 2) as used_cars_ret,
  sum(unit_count) filter (where page = 16 and line between 4 and 5) as used_trucks_ret,
  sum(unit_count) filter (where page = 16 and line = 8) as used_cars_ws,
  sum(unit_count) filter (where page = 16 and line = 10) as used_trucks_ws  
from fs_count
where store = 'RY2'
group by year_month; 

-- and the running total
select b.*, sum(daily_count) over (partition by store, page order by the_date asc rows between unbounded preceding and current row)
from (
	select store, page, the_date, sum(unit_count) as daily_count
	from daily_count a
-- 	where year_month = 202103
	group by store, page, the_date) b
order by store, page, the_date


-----------------------------------------------------------------------------------------------------
--/> count
-----------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------
--< gross
-----------------------------------------------------------------------------------------------------

drop table if exists pac.variable_gross_accounts cascade;
create table pac.variable_gross_accounts as
select distinct b.year_month, d.store, b.page, b.line_label, b.line, b.col, c.gl_account
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201601 and 202103
  and (
		(b.page in (5,7,8,9,10,14) and b.line < 48)
		or
		(b.page = 16 and line < 12)
		or
		(b.page = 17 and line < 20))
join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key  
join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  and d.area = 'variable'; 
  -- fuck me, dim_account has bad department for passport cogs, 263500
insert into pac.variable_gross_accounts
select year_month, store, page, line_label,line, col,'263500'
from pac.variable_gross_accounts where gl_account = '243500';
-- and the wrong department for accessories cogs
insert into pac.variable_gross_accounts
select year_month, store, page, line_label,line,col,'2657001'
from pac.variable_gross_accounts where gl_account = '2457001';
create unique index on pac.variable_gross_accounts(year_month, gl_account);
create index on pac.variable_gross_accounts(gl_account);
create index on pac.variable_gross_accounts(page);
create index on pac.variable_gross_accounts(line);
create index on pac.variable_gross_accounts(col);
comment on table pac.variable_gross_accounts is 'all accounts that feed financial statement gross lines (page 3 line 2),
  includes accessories and adjustments (eg ry1 pg 5 line 47, 202103 ry1 pg 5 line 6: no camaro sold, but 195 adjustment to camaro acct)';

  
-- gross at fs line/day granularity
drop table if exists pac.variable_daily_gross cascade;
create table pac.variable_daily_gross as
select b.year_month, b.the_date, d.store, d.page, d.line_label, d.line, 
  (sum(-a.amount) filter (where col = 1)) as sales, 
  (sum(a.amount) filter (where col = 3)) as cogs,
  sum(-a.amount) as gross
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
join fin.dim_account c on a.account_key = c.account_key
join pac.variable_gross_accounts d on c.account = d.gl_account  
  and b.year_month = d.year_month
where a.post_status = 'Y'  
group by b.year_month, b.the_date, d.store, d.page, d.line_label, d.line;
create unique index on pac.variable_daily_gross(store,the_date,page,line);
comment on table pac.variable_daily_gross is 'daily gross at the level of store, date, financial statement page and line';


select a.*, (sum(gross) over (partition by store,page,line))::integer from pac.variable_daily_gross a where year_month = 202103 order by store,page,line, the_date

-----------------------------------------------------------------------------------------------------
--/> gross
-----------------------------------------------------------------------------------------------------

-- unit count
drop table if exists pac.variable_unit_count_mtd_temp cascade;
create table pac.unit_count_mtd_temp as
select aa.year_month, aa.the_date, aa.store, bb.daily_count, bb.mtd
from (
	select year_month, the_date, store
	from dds.dim_date a
	cross join (select 'ry1' as store union select'ry2') b
	where the_date between '01/01/2016' and '03/31/2021') aa
left join (
	select b.*, sum(daily_count) over (partition by store, year_month order by the_date asc rows between unbounded preceding and current row) as mtd
	from (
		select store, year_month, the_date, sum(unit_count) as daily_count
		from pac.variable_daily_unit_counts a
		group by store, year_month, the_date) b) bb on aa.the_date = bb.the_date and aa.store = bb.store
order by aa.store, aa.the_date;	
create unique index on pac.unit_count_mtd_temp(the_date,store);
comment on table pac.unit_count_mtd_temp is 'interrim table for generating daily unit count & mtd totals';

drop table if exists pac.variable_mtd_unit_counts cascade;
create table pac.variable_mtd_unit_counts as
select a.store, a.year_month, a.the_date, coalesce(a.daily_count, 0) as daily_count,  
  case
    when a.the_date = b.the_first and a.mtd is null then 0
    else first_value(mtd) over (partition by a.store, a.year_month, the_partition order by a.the_date)
  end as mtd
from (
	select store, year_month, the_date, daily_count, mtd,
		sum(case when mtd is null then 0 else 1 end) over (partition by store, year_month order by the_date) as the_partition
	from pac.unit_count_mtd_temp
	order by the_date) a
join pac.month_first_and_last_days b on a.year_month = b.year_month	
order by a.store, a.the_date;
create unique index on pac.variable_mtd_unit_counts(store,the_date);
create index on pac.variable_mtd_unit_counts(year_month);
create index on pac.variable_mtd_unit_counts(the_date);

-- gross

drop table if exists pac.variable_gross_mtd_temp cascade;
create table pac.variable_gross_mtd_temp as
select aa.year_month, aa.the_date, aa.store, bb.daily_gross, bb.mtd
from (
	select year_month, the_date, store
	from dds.dim_date a
	cross join (select 'ry1' as store union select'ry2') b
	where the_date between '01/01/2016' and '03/31/2021') aa
left join (
	select b.*, (sum(daily_gross) over (partition by store, year_month order by the_date asc rows between unbounded preceding and current row))::integer as mtd
	from (
		select store, year_month, the_date, sum(gross) as daily_gross
		from pac.variable_daily_gross a
		group by store, year_month, the_date) b) bb on aa.the_date = bb.the_date and aa.store = bb.store
order by aa.store, aa.the_date;	
create unique index on pac.variable_gross_mtd_temp(the_date,store);
comment on table pac.variable_gross_mtd_temp is 'interrim table for generating daily gross & mtd totals';


drop table if exists pac.variable_mtd_gross cascade;
create table pac.variable_mtd_gross as
select a.store, a.year_month, a.the_date, coalesce(a.daily_gross, 0) as daily_gross,  
  case
    when a.the_date = b.the_first and a.mtd is null then 0
    else (first_value(mtd) over (partition by a.store, a.year_month, the_partition order by a.the_date))::integer
  end as mtd
from (
	select store, year_month, the_date, daily_gross, mtd,
		sum(case when mtd is null then 0 else 1 end) over (partition by store, year_month order by the_date) as the_partition
	from pac.variable_gross_mtd_temp
	order by the_date) a
join pac.month_first_and_last_days b on a.year_month = b.year_month	
order by a.store, a.the_date;
create unique index on pac.variable_mtd_gross(store,the_date);
create index on pac.variable_mtd_gross(year_month);
create index on pac.variable_mtd_gross(the_date);


select a.*, b.daily_gross, b.mtd
from pac.variable_mtd_unit_counts a
join pac.variable_mtd_gross b on a.store = b.store
  and a.the_date = b.the_date

-- this adds the % of monthly gross and counts
select a.store, a.year_month, a.the_date, a.daily_count, 
  round(100.0 * a.mtd/d.month_total_count, 1) as "% of count", a.mtd as mtd_count, 
  b.daily_gross::integer, 
  round(100.0 * b.mtd/e.month_total_gross, 1) as "% of gross", b.mtd as mtd_gross
from pac.variable_mtd_unit_counts a
join pac.variable_mtd_gross b on a.store = b.store
  and a.the_date = b.the_date
left join (
	select a.*,  sum(a.daily_count) over (partition by a.store, a.year_month) as month_total_count
	from pac.variable_mtd_unit_counts a
	order by a.store, a.the_date) d on a.the_date = d.the_date
	  and a.store = d.store
left join (
	select a.*,  sum(a.daily_gross) over (partition by a.store, a.year_month) as month_total_gross
	from pac.variable_mtd_gross a
	order by a.store, a.the_date) e on a.the_date = e.the_date
	  and a.store = e.store
join pac.weeks f on a.the_date = f.the_date	  
-- where b.daily_gross < 0
order by a.store, a.the_date


select a.store, a.year_month, f.week_of_month, 
  d.month_total_count, sum(a.daily_count)  as count_by_week,
  round(100.0 * sum(a.daily_count)/d.month_total_count, 1) as "% of count",
  e.month_total_gross::integer, sum(b.daily_gross)::integer as gross_by_week,
  round(100.0 * sum(b.daily_gross)/e.month_total_gross, 1) as "% of gross"
from pac.variable_mtd_unit_counts a
join pac.variable_mtd_gross b on a.store = b.store
  and a.the_date = b.the_date
left join (
	select a.*,  sum(a.daily_count) over (partition by a.store, a.year_month) as month_total_count
	from pac.variable_mtd_unit_counts a
	order by a.store, a.the_date) d on a.the_date = d.the_date
	  and a.store = d.store
left join (
	select a.*,  sum(a.daily_gross) over (partition by a.store, a.year_month) as month_total_gross
	from pac.variable_mtd_gross a
	order by a.store, a.the_date) e on a.the_date = e.the_date
	  and a.store = e.store
join pac.weeks f on a.the_date = f.the_date	  
-- where a.year_month = 202103	  
group by a.store, a.year_month, f.week_of_month, d.month_total_count, e.month_total_gross
order by a.store, a.year_month, f.week_of_month


select * from pac.variable_daily_unit_counts limit 100

select * from pac.variable_daily_gross limit 100