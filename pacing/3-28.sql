﻿-- 03/28/21 comfortable with this for the monthly count
-- working through the count first, once that is good, then can "easily" get the daily gross for those vehicles
-- not sure yet how i will integrate the monthly gross for vehicles not in the count
-- eg, 202102, xt4-162006, xt5-1627006, etc
-- but count first
-- montly total count by fs page/line

drop table if exists month_days cascade;
create temp table month_days as
select year_month,
  (select max(the_date)as the_first from dds.dim_date where year_month = a.year_month and first_day_of_month),
  (select max(the_date) as the_last from dds.dim_date where year_month = a.year_month and last_day_of_month)
from dds.dim_date a
group by a.year_month;
create unique index on month_days(year_month);
create unique index on month_days(the_first);
create unique index on month_days(the_last);

-- variable sale accounts
-- used to determine the sales count
drop table if exists fs_sale_accounts;
create temp table fs_sale_accounts as
select distinct b.year_month, f.store, d.gl_account, b.page, b.line, b.line_label, e.description
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
	and (
		(b.page between 5 and 15 and b.line between 1 and 45) 
		or
		(b.page = 16 and b.line between 1 and 14)) 
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
	and e.account_type_code = '4'
	and e.current_row
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key;
create unique index on fs_sale_accounts(year_month, gl_account);
create index on fs_sale_accounts(gl_account);
create index on fs_sale_accounts(page);
create index on fs_sale_accounts(line);

drop table if eXISTS fs_count cascade;
-- this gives the fs count at the month/line level
create temp table fs_count as
select year_month, store, page, line, line_label, control, sum(unit_count) as unit_count
from (
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201601 and 202102
  join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
  join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')  
  join fs_sale_accounts d on c.account = d.gl_account
    and b.year_month = d.year_month
  where a.post_status = 'Y' 
    and a.control not in ('RGJ240218','HGJ190420')) h
group by year_month, store, page, line, line_label, control;
create unique index on fs_count(year_month, control) where unit_count = 1;

/*
-- dups in fs_count
-- all the "dups" are the result of backons
select * from fs_count a
join (
select year_month, control--, unit_count
from fs_count
where unit_count = 1
group by year_month, control--, unit_count
having count(*) > 1) b on a.control = b.control and a.year_month = b.year_month
order by a.year_month, a.control

-- so, eliminating unit_count = 0 eliminates those that sold and unwound in the same month
the rest of the dups (except 2) are used vehicles that unwound the retail deal, then sold wholesale
in the same month, ie, different lines on page 16
the 2 exceptions are new cars, 1 gm and 1 honda where the accounting changed , ie, backed on from one
account and resold on another account (H11735, G34382)
the most of the unwinds in any one month is 2, most are one
a total of 15 used vehicles and 2 new
*/

************************ CONCLUSION *************************************
fs_count will work just fine for the daily counts,
in use, limit to unit_count = 1


select year_month,
  sum(unit_count) filter (where page = 5 and line < 20) as chev_cars,
  sum(unit_count) filter (where page = 5 and line between 25 and 38) as chev_trucks,
  sum(unit_count) filter (where page = 5 and line in (22, 43)) as chev_fleet,
  sum(unit_count) filter (where page = 5 and line in (23, 44)) as chev_internal,
  sum(unit_count) filter (where page = 8 and line < 20) as buick_cars,
  sum(unit_count) filter (where page = 8 and line between 25 and 38) as buick_trucks,
  sum(unit_count) filter (where page = 8 and line in (22, 43)) as buick_fleet,
  sum(unit_count) filter (where page = 8 and line in (23, 44)) as buick_internal,
  sum(unit_count) filter (where page = 9 and line < 20) as cad_cars,
  sum(unit_count) filter (where page = 9 and line between 25 and 38) as cad_trucks,
  sum(unit_count) filter (where page = 9 and line in (22, 43)) as cad_fleet,
  sum(unit_count) filter (where page = 9 and line in (23, 44)) as cad_internal,
  sum(unit_count) filter (where page = 10 and line between 25 and 38) as gmc_trucks,
  sum(unit_count) filter (where page = 10 and line in (22, 43)) as gmc_fleet,
  sum(unit_count) filter (where page = 10 and line in (23, 44)) as gmv_internal,
  sum(unit_count) filter (where page = 16 and line between 1 and 2) as used_cars_ret,
  sum(unit_count) filter (where page = 16 and line between 4 and 5) as used_trucks_ret,
  sum(unit_count) filter (where page = 16 and line = 8) as used_cars_ws,
  sum(unit_count) filter (where page = 16 and line = 10) as used_trucks_ws  
from fs_count
where store = 'RY1'
group by year_month 


select year_month,
  sum(unit_count) filter (where page = 7 and line < 20) as honda_cars,
  sum(unit_count) filter (where page = 7 and line between 25 and 40) as honda_trucks,
  sum(unit_count) filter (where page = 14 and line < 20) as nissan_cars,
  sum(unit_count) filter (where page = 14 and line between 25 and 40) as nissan_trucks,
  sum(unit_count) filter (where page = 16 and line between 1 and 2) as used_cars_ret,
  sum(unit_count) filter (where page = 16 and line between 4 and 5) as used_trucks_ret,
  sum(unit_count) filter (where page = 16 and line = 8) as used_cars_ws,
  sum(unit_count) filter (where page = 16 and line = 10) as used_trucks_ws  
from fs_count
where store = 'RY2'
group by year_month 


********************** 03/29/21 ***************************************

ok, comfortable with the count
now need the "sale date", start with bopmast
/*
the problem with bopmast, date capped is not actually a sold or delivered date,
for example, G40805R physically delivered (board) on 2/24, subsequently the deal was changed in bopmast, and it was re-capped on 3/15 & 3/17
ext_bopmast will show a date_capped of 3/17
select bopmast_stock_number, record_key, record_status, record_type, date_saved, date_approved, date_capped, row_from_date from arkona.xfm_bopmast where bopmast_stock_number = 'G40805R' order by row_from_date
the point is bopmast.date_capped is the date that the deal in DT was last fucked with
*/

***************** CONCLUSION, GO WITH ACCOUNTING FOR "SALE DATES" *********************************
/*
drop table if exists test_1 cascade;
create temp table test_1 as
select distinct a.*, c.date_capped -- , d.the_date
from fs_count a 
left join arkona.ext_bopmast c on a.control = c.bopmast_stock_number
  and a.store = c.bopmast_company_number
  and c.record_status = 'U'
where unit_count = 1;

select year_month, control
from test_1
group by year_month, control
having count(*) > 1

-- the only dups
201709;H9960A
201910;G36862R

select * from test_1
where (
  (year_month = 201709 and control = 'H9960A')
  or
  (year_month = 201910 and control = 'G36862R'))

select * from arkona.ext_bopmast where bopmast_stock_number = 'G36862R'
same  vehicle (stock, vin, customer) 2 separate deals (57815,57822)

select * from arkona.ext_bopmast where bopmast_stock_number = 'H9960A'
2 separate vehicles (vin, customer, price, etc) 2 separate deals with the same stock number

-- no deals on wholesales, so they are null
select * from test_1 where date_capped is null

-- 41 retail sales with no bopmast record, ranging from 201612 - 202102, both new and used
select * from test_1 where date_capped is null and line_label not like '%wholesale%'

lets look at a couple
stock number changes after the sale, no stock number in bopmast

-- valid, in my opinion, shortcut to deal with the anomalies,
-- get the date from accounting
select aa.*, bb.the_date, 
  case -- check that the date is in the month
    when bb.the_date between cc.the_first and cc.the_last then 'GOOD'
    else 'BAD'
  end
from test_1 aa
left join ( 
	select b.year_month, a.control, b.the_date, c.account, e.journal_code, a.amount
	from fin.fact_gl a
	join dds.dim_date b on a.date_key = b.date_key
	join fin.dim_account c on a.account_key = c.account_key
	join fs_sale_accounts d on c.account = d.gl_account
		and b.year_month = d.year_month
	join fin.dim_journal e on a.journal_key = e.journal_key
		and e.journal_code in ('VSN','VSU')
	where a.post_status = 'Y') bb on aa.control = bb.control
		and aa.year_month = bb.year_month
left join month_days cc on aa.year_month = cc.year_month      
where aa.date_capped is null
  and aa.line_label not like '%wholesale%'
order by aa.control  

*/

/*
got a major exception here
G40201A, everything, tool, board says delivered in January
select bopmast_stock_number, record_key, record_status, record_type, date_saved, date_approved, date_capped, row_from_date from arkona.xfm_bopmast where bopmast_stock_number = 'G40201a' order by row_from_date
but the fs query, DT doc show february
the reason it comes up is because it shows up as multiple when i add accounting to the query
SO
the query for adding the accounting date needs to have no dups, which means a min or max on the date
*/


-- dates from accounting
drop table if exists test_2 cascade;
-- daily counts
-- grouped to handle multiple dates in accounting
-- to get the count correct, in this table, removed unit_count from the grouping and summed it,
-- include unwinds (-1 unit_count), include unit_count in PK
create temp table test_2 as
select distinct aa.year_month, aa.store, aa.page, aa.line, aa.line_label, aa.control,
	sum(aa.unit_count) as unit_count, min(bb.the_date) as the_date
from fs_count aa 
left join ( 
	select b.year_month, a.control, b.the_date, c.account, e.journal_code, a.amount
	from fin.fact_gl a
	join dds.dim_date b on a.date_key = b.date_key
	join fin.dim_account c on a.account_key = c.account_key
	join fs_sale_accounts d on c.account = d.gl_account
		and b.year_month = d.year_month
	join fin.dim_journal e on a.journal_key = e.journal_key
		and e.journal_code in ('VSN','VSU')
	where a.post_status = 'Y') bb on aa.control = bb.control
		and aa.year_month = bb.year_month
where aa.unit_count <> 0
group by aa.year_month, aa.store, aa.page, aa.line, aa.line_label, aa.control;
-- 	aa.unit_count;
create unique index on test_2(year_month,control,unit_count);

-- this is the way to get the correct totals
select *, sum(unit_count) over (partition by line)
from test_2
where year_month = 202102
  and store = 'ry1'
  and page = 16

-- and the running total
select b.*, sum(daily_count) over (partition by store, page order by the_date asc rows between unbounded preceding and current row)
from (
	select store, page, the_date, sum(unit_count) as daily_count
	from test_2 a
	where year_month = 202102
	group by store, page, the_date) b
order by store, page, the_date

lets check a bunch of different statements

ry2 201612 uc 4 hi, 201606 uc 4 hi
ry1 201606 uc 1 lo 201706 uc 2 hi 201701 nc 2 hi, 201806 uc 2 hi, 201912 nc 6 lo, 201801 nc 2 lo, 201901 nc 1 hi, 202001 uc 2 hi, nc 1 lo

select year_month,
  sum(unit_count) filter (where page = 5) as chev,
  sum(unit_count) filter (where page = 8) as buick,
  sum(unit_count) filter (where page = 9) as cad,
  sum(unit_count) filter (where page = 10) as gmc,
  sum(unit_count) filter (where page = 16) as uc,
  sum(unit_count) filter (where page in (5,8,9,10,16)) as total
from test_2
where store = 'RY1'
group by year_month
order by year_month

select year_month,
  sum(unit_count) filter (where page = 7) as honda,
  sum(unit_count) filter (where page = 14) as nissan,
  sum(unit_count) filter (where page = 16) as uc,
  sum(unit_count) filter (where page in (7,14,16)) as total
from test_2
where store = 'RY2'
group by year_month
order by year_month


/*
validity tests for test_2	
-- no nulls
select * from test_2 where the_date is null
        
select count(*) from fs_count where unit_count = 1 --33026
-- looks like there are some dups
select count(*) from test_2 -- 33076
-- in the vast majority of a random sampling, the earlier of the dates matches the deal 
select *
from test_2 a
join (
  select year_month, control
  from test_2
  group by year_month, control
  having count(*) > 1) b on a.year_month = b.year_month and a.control = b.control


select a.*, b.the_date,
  case when a.date_capped = b.the_date then 'MATCH' else 'NOPE' end 
from test_1 a
full outer join test_2 b on a.year_month = b.year_month and a.control = b.control

*/


NOW IT IS ON TO GROSS, BOTH SALES AND FI


this is the sample that returns non sale gross

-- -- gross
-- -- this includes non sale adjustments page 9 lines 88, 29 so it works for gross, but not for count
-- select b.page, b.line_label, b.line, array_agg(c.gl_account), 
--   sum(-amount) filter (where b.col = 1) as sales,
--   sum(amount) filter (where b.col = 3) as cogs,
--   sum(-amount) as gross
-- -- select a.*
-- from fin.fact_fs a
-- join fin.dim_fs b on a.fs_key = b.fs_key
--   and b.year_month = 202102
--   and (
-- 		(b.page in (5,8,9,10) and b.line < 45)
-- 		or
-- 		(b.page = 16 and line < 7))
-- join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key  
-- join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
--   and d.store = 'RY1'
--   and d.area = 'variable' 
--   and d.department = 'sales'
-- --   and d.sub_Department = 'new'
-- group by b.page, b.line_label, b.line
-- order by page, line 

drop table if exists fs_gross_1 cascade;
-- this table gives the fs count by year_month,store,page,line
-- page 3 line 2 includes accessories page 5 line 47
create temp table fs_gross_1 as
select b.year_month, d.store, b.page, b.line_label, b.line, array_agg(c.gl_account), 
  sum(-amount) filter (where b.col = 1) as sales,
  sum(amount) filter (where b.col = 3) as cogs,
  sum(-amount) as gross
-- select a.*
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201601 and 202102
  and (
		(b.page between 5 and 14 and b.line < 48)
		or
		(b.page = 16 and line < 12)
		or
		(b.page = 17 and line < 20))
join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key  
join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
--   and d.store = 'RY1'
  and d.area = 'variable' 
--   and d.department = 'sales'
--   and d.sub_Department = 'new'
group by b.year_month, d.store, b.page, b.line_label, b.line;


select store, year_month,
  sum(gross) filter (where page between 5 and 15) as nc,
  sum(gross) filter (where page = 16) as uc,
  sum(gross) filter (where page = 17) as fi
from fs_gross_1
-- where year_month = 202011
group by store, year_month
order by store, year_month


select store, page,
  sum(gross) filter (where page between 5 and 15) as nc,
  sum(gross) filter (where page = 16) as uc,
  sum(gross) filter (where page = 17) as fi
from fs_gross_1
where year_month = 201601
group by store, page
order by store, page

select store, page, line,
  sum(gross) filter (where page between 5 and 15) as nc,
  sum(gross) filter (where page = 16) as uc,
  sum(gross) filter (where page = 17) as fi
from fs_gross_1
where year_month = 202102
group by store, page, line
order by store, page, line



drop table if exists fs_gross_accounts cascade;
-- all accounts that feed gross lines (page 3 line 2)
create temp table fs_gross_accounts as
select distinct b.year_month, d.store, b.page, b.line_label, b.line, c.gl_account
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201601 and 202102
  and (
		(b.page in (5,7,8,9,10,14) and b.line < 48)
		or
		(b.page = 16 and line < 12)
		or
		(b.page = 17 and line < 20))
join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key  
join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  and d.area = 'variable'; 
  -- fuck me, dim_account has bad department for passport cogs, 263500
insert into fs_gross_accounts
select year_month, store, page, line_label,line, '263500'
from fs_gross_accounts where gl_account = '243500';
-- and the wrong department for accessories cogs
insert into fs_gross_accounts
select year_month, store, page, line_label,line, '2657001'
from fs_gross_accounts where gl_account = '2457001';
create unique index on fs_gross_accounts(year_month, gl_account);
create index on fs_gross_accounts(gl_account);
create index on fs_gross_accounts(page);
create index on fs_gross_accounts(line);

-- gross at fs line/day granularity
drop table if exists gross_2 cascade;
create temp table gross_2 as
select b.year_month, b.the_date, d.store, d.page, d.line_label, d.line, sum(a.amount) as gross
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
--   and b.year_month = 202102
join fin.dim_account c on a.account_key = c.account_key
join fs_gross_accounts d on c.account = d.gl_account  
  and b.year_month = d.year_month
where a.post_status = 'Y'  
group by b.year_month, b.the_date, d.store, d.page, d.line_label, d.line;

select b.*, sum(gross) over (partition by store, page order by the_date asc rows between unbounded preceding and current row)
from (
	select store, year_month, page, the_date, sum(-gross)::integer as gross
	from gross_2
	group by store, year_month, page, the_date
	order by store,page, the_date) b
order by store, year_month, page, the_date	


select store, page, line, sum(-gross)::integer as gross
from gross_2
group by store, page, line
order by store,page, line

