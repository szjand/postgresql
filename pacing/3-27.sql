﻿-- the step_1 query compared to flightplan
03/27/21
retail mtd thru 03/26
sales  vision    doc    fp
used		 152		 148     188
chev      48      40      47
buick     10      10      10
cad        8       6       9
gmc       47      43      47 

202102
sales  vision    doc    fp
used		 129		 129     168
chev      46      42      46
buick     11      11      11
cad        5       5       5
gmc       48      48      48 

select * from fp.flightplan 
where the_date = '02/28/2021'
  and seq in (1,2,3,4,5,6,8)
order by seq



drop table if exists step_1;
create temp table step_1 as
select store, page, line, line_label, control, sum(unit_count) as unit_count, sum(amount)::integer as amount
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 202102
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 202102
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by store, page, line, line_label, control
order by store, page, line;


-- gross is wacky
-- nc gross accounts from fact_fs_monthly_update
select distinct d.gl_account, e.account_type, e.department
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
--   and b.year_month = 201612
  and b.page between 5 and 15 -- new cars
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account

select b.the_date, c.account, e.journal_code, a.amount
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
join fin.dim_account c on a.account_key = c.account_key
join ( -- nc gross accounts
	select distinct d.gl_account, e.account_type, e.department
	from fin.fact_fs a
	inner join fin.dim_fs b on a.fs_key = b.fs_key
	--   and b.year_month = 201612
		and b.page between 5 and 15 -- new cars
	inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
	inner join fin.dim_account e on d.gl_account = e.account) d on c.account = d.gl_account
join fin.dim_journal e on a.journal_key = e.journal_key
where a.control = 'g41952'	

fuck, current month g41952 makes no sense, query shows cost as 46564.15, deal shows cost as 48083.25, vision shows front gross as 1019

select -47364.00 + 46564.15

select row_From_Date, inpmast_vehicle_cost from arkona.xfm_inpmast where inpmast_stock_number = 'g41952'	
fuck it, this is an historical project
work from the fs

so, back to 202102
dont know which fs info greg actually wants
going with page 3 lines 1 & 2 as driven by pages 5,8,9,10
granularity page & line
this will give me the month end data, nmot the daily/weekly

-- gross
-- this includes non sale adjustments so it works for gross, but not for count
select b.page, b.line_label, b.line, array_agg(c.gl_account), 
  sum(-amount) filter (where b.col = 1) as sales,
  sum(amount) filter (where b.col = 3) as cogs,
  sum(-amount) as gross
-- select a.*
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202102
  and (
		(b.page in (5,8,9,10) and b.line < 45)
		or
		(b.page = 16 and line < 7))
join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key  
join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  and d.store = 'RY1'
  and d.area = 'variable' 
  and d.department = 'sales'
--   and d.sub_Department = 'new'
group by b.page, b.line_label, b.line
order by page, line 

-- this may be a way to get stock numbers from gross
select b.the_date, c.account, e.journal_code, a.control, a.amount
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 202102
join fin.dim_Account c on a.account_key = c.account_key
join (  
	select distinct c.gl_account
	-- select a.*
	from fin.fact_fs a
	join fin.dim_fs b on a.fs_key = b.fs_key
		and b.year_month = 202102
		and (
			(b.page in (5,8,9,10) and b.line < 45)
			or
			(b.page = 16 and line < 7))
	join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key  
	join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
		and d.store = 'RY1'
		and d.area = 'variable' 
		and d.department = 'sales') d on c.account = d.gl_account
join fin.dim_journal e on a.journal_key = e.journal_key
where a.control in ('G41469','G41331')
  
-- count
drop table if eXISTS fs_count cascade;
create temp table fs_count as
select year_month, store, page, line, line_label, control, sum(unit_count) as unit_count, sum(amount)::integer as amount
from (
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201601 and 202102
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs sale accounts gm_account page/line/acct description
    select distinct b.year_month, f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
--       and b.year_month between 201800 and 202102
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key order by store, page, line) d on c.account = d.gl_account
      and b.year_month = d.year_month -- this join is necessary when using multiple months
  where a.post_status = 'Y') h
group by year_month, store, page, line, line_label, control
-- order by control
order by year_month, store, page, line;



select year_month,
  sum(unit_count) filter (where page = 5 and line < 20) as chev_cars,
  sum(unit_count) filter (where page = 5 and line between 25 and 38) as chev_trucks,
  sum(unit_count) filter (where page = 5 and line in (22, 43)) as chev_fleet,
  sum(unit_count) filter (where page = 5 and line in (23, 44)) as chev_internal,
  sum(unit_count) filter (where page = 8 and line < 20) as buick_cars,
  sum(unit_count) filter (where page = 8 and line between 25 and 38) as buick_trucks,
  sum(unit_count) filter (where page = 8 and line in (22, 43)) as buick_fleet,
  sum(unit_count) filter (where page = 8 and line in (23, 44)) as buick_internal,
  sum(unit_count) filter (where page = 9 and line < 20) as cad_cars,
  sum(unit_count) filter (where page = 9 and line between 25 and 38) as cad_trucks,
  sum(unit_count) filter (where page = 9 and line in (22, 43)) as cad_fleet,
  sum(unit_count) filter (where page = 9 and line in (23, 44)) as cad_internal,
  sum(unit_count) filter (where page = 10 and line between 25 and 38) as gmc_trucks,
  sum(unit_count) filter (where page = 10 and line in (22, 43)) as gmc_fleet,
  sum(unit_count) filter (where page = 10 and line in (23, 44)) as gmv_internal,
  sum(unit_count) filter (where page = 16 and line between 1 and 2) as used_cars_ret,
  sum(unit_count) filter (where page = 16 and line between 4 and 5) as used_trucks_ret,
  sum(unit_count) filter (where page = 16 and line = 8) as used_cars_ws,
  sum(unit_count) filter (where page = 16 and line = 10) as used_trucks_ws  
from fs_count
where store = 'RY1'
group by year_month 

-- fs_count: 506, add sold_board: 512
select a.*, c.date_capped, d.the_date
-- select a.*
from fs_count a 
-- left join sold_board b on a.control = b.stock_number
left join arkona.ext_bopmast c on a.control = c.bopmast_stock_number
  and c.record_status = 'U'
left join ( -- look at accounting timeline for weird  ones
	select b.year_month, a.control, b.the_date, c.account, e.journal_code, a.amount
	from fin.fact_gl a
	join dds.dim_date b on a.date_key = b.date_key
	join fin.dim_account c on a.account_key = c.account_key
	join ( -- nc gross accounts
		select distinct b.year_month, d.gl_account, e.account_type, e.department
		from fin.fact_fs a
		inner join fin.dim_fs b on a.fs_key = b.fs_key
			and (b.page between 5 and 15 or b.page = 16)
		inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
		inner join fin.dim_account e on d.gl_account = e.account
		where e.account_type_code = '4') d on c.account = d.gl_account and b.year_month = d.year_month
	join fin.dim_journal e on a.journal_key = e.journal_key
	  and e.journal_code in ('VSN','VSU')) d on a.control = d.control
	  and a.year_month = d.year_month
where a.year_month = 202102
--   and a.store = 'ry1'
  and a.unit_count = 1
order by c.date_capped, page, line, a.control

-- look at accounting timeline for weird  ones
select a.control, b.the_date, c.account, e.journal_code, a.amount
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
join fin.dim_account c on a.account_key = c.account_key
join ( -- nc gross accounts
	select distinct b.year_month, d.gl_account, e.account_type, e.department
	from fin.fact_fs a
	inner join fin.dim_fs b on a.fs_key = b.fs_key
		and b.page between 5 and 15 -- new cars
	inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
	inner join fin.dim_account e on d.gl_account = e.account
	where e.account_type_code = '4') d on c.account = d.gl_account and b.year_month = d.year_month
join fin.dim_journal e on a.journal_key = e.journal_key





-- gross by day
-- possibly interesting, need to include the page/line
select b.the_date, c.account, e.journal_code, a.control, a.amount
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 202102
join fin.dim_Account c on a.account_key = c.account_key
join (  
	select distinct c.gl_account
	-- select a.*
	from fin.fact_fs a
	join fin.dim_fs b on a.fs_key = b.fs_key
		and b.year_month = 202102
		and (
			(b.page in (5,8,9,10) and b.line < 45)
			or
			(b.page = 16 and line < 7))
	join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key  
	join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
		and d.store = 'RY1'
		and d.area = 'variable' 
		and d.department = 'sales') d on c.account = d.gl_account
join fin.dim_journal e on a.journal_key = e.journal_key
where c.account_type_code = '4'
order by the_date, journal_code, control


-- sold board
drop table if exists sold_board cascade;
create temp table sold_board as
  select (select year_month from dds.dim_date where the_date = current_date),
    boarded_ts ::date as boarded_date,
    case
      when d.store like '%outlet' then 'outlet'::citext
      when d.store like '%Chev%' then 'RY1':: citext
      when d.store like '%Honda%' then 'RY2'::citext
    end as store, a.stock_number, 
    e.vehicle_type, e.vehicle_make,
    case
      when g.amount = 'NaN' then 0
      else coalesce(g.amount, 0)::integer 
    end as front_gross,
     case
      when h.amount = 'NaN' then 0
      else coalesce(h.amount, 0)::integer 
    end as fi_gross, b.*
  from board.sales_board a
  inner join board.board_types b on a.board_type_key = b.board_type_key
  inner join dds.dim_date c on a.boarded_ts::date = c.the_date
  inner join onedc.stores d on a.store_key = d.store_key
  inner join board.daily_board e on a.board_id = e.board_id
  left join board.deal_gross g on a.board_id = g.board_id
    and g.current_row = true
    and g.amount_type =  'front_end_gross'
  left join board.deal_gross h on a.board_id = h.board_id
    and h.current_row = true
    and h.amount_type = 'fi_gross'
  where not a.is_backed_on
    and  b.board_type in ('Deal')
    and b.board_sub_type in ('retail','lease')
    and c.year_month = 201801
    and not a.is_deleted
    and d.store not like '%honda%'
order by boarded_date, vehicle_type, stock_number;  

select * from sold_board

select vehicle_type, count(*)
from sold_board
group by vehicle_type

select * 
from fp.sales_counts
order by the_date



