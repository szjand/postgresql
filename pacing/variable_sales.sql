﻿-------------------------------------------------------------------
--< weeks
-------------------------------------------------------------------
select a.the_date, a.day_of_week, a.day_name, a.day_of_month,
  case
    when a.day_of_month between 1 and 7 then 1
    when a.day_of_month between 8 and 14 then 2
    when a.day_of_month between 15 and 21 then 3
    when a.day_of_month between 22 and 28 then 4
    when a.day_of_month > 28 then round((1.0 * b.day_of_month - 28)/b.day_of_month, 2) -- round(1.0 * a.day_of_month/b.day_of_month, 2)
  end,
  c.wd_in_month
from dds.dim_Date a
join dds.dim_date b on a.year_month = b.year_month
  and b.last_day_of_month
join dds.working_days c on a.the_date = c.the_date
  and  c.department = 'sales'
where a.the_year = 2021
order by a.the_date

select * from dds.working_days where the_date = current_date

select * from dds.dim_Date where the_date = current_date


select distinct wd_in_month, count(*)
from dds.working_days
where the_year between 2017 and 2020
  and department = 'sales'
group by wd_in_month

-------------------------------------------------------------------
--/> weeks
-------------------------------------------------------------------

drop table if exists step_1;
create temp table step_1 as
select store, page, line, line_label, control, sum(unit_count) as unit_count, sum(amount)::integer as amount
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 202102
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 202102
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
--         or
--         (b.page = 17 and b.line between 1 and 20))-- f/i counts don't make sense
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by store, page, line, line_label, control
order by store, page, line;

select * from step_1

select * from step_1 where page = 16 and line > 5

select store, page, line, line_label, sum(unit_count), sum(amount)
from (
  select *
  from step_1) a
group by store, page, line, line_label
order by store, page, line, line_label

select store, page, sum(unit_count), sum(amount)
from (
  select *
  from step_1) a
group by store, page
order by store, page



-- from the statement, page 3 gross
--nc
	    select c.store as store_code, b.year_month, 
	      sum(case when amount < 0 then - 1 * amount else 0 end) as nc_sales,
	      sum(-1 * amount) as nc_gross
	    from fin.fact_fs a
	    inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
	    inner join fin.dim_account aaa on aa.gl_account = aaa.account
	      and aaa.current_row = true
	    inner join fin.dim_fs b on a.fs_key = b.fs_key
	      and b.year_month = 202102
	      and b.page between 5 and 15
	    inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
	    group by c.store, b.year_month
	    having sum(amount) <> 0

--uc
	    select c.store as store_code, b.year_month, page,
	      sum(case when amount < 0 then -1 * amount else 0 end) as uc_sales,
	      sum(-1 * amount) as uc_gross
	    from fin.fact_fs a
	    inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
	    inner join fin.dim_account aaa on aa.gl_account = aaa.account
	      and aaa.current_row = true
	    inner join fin.dim_fs b on a.fs_key = b.fs_key
	      and b.year_month = 202102
	      and b.page = 16 and b.line between 1 and 14 -- used cars
	    inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
	    group by c.store, b.year_month, page
	    having sum(amount) <> 0
	    order by c.store, b.year_month

--fi
	    select store as store_code, year_month, -1 * (sales_a + sales_b) as fi_sales, fi_gross
	    from (
	      select c.store, b.year_month,
	      sum(case when line in (1, 2, 11, 12) then amount else 0 end) as sales_a,
	      sum(case when line in (6, 7, 17) and amount < 0 then amount else 0 end) as sales_b,
	      sum(-1 * amount) as fi_gross
	      from fin.fact_fs a
	      inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
	      inner join fin.dim_account aaa on aa.gl_account = aaa.account
	        and aaa.current_row = true
	      inner join fin.dim_fs b on a.fs_key = b.fs_key
	        and b.year_month = 202102
	        and b.page = 17 
	        and b.line between 1 and 20 -- f/i
	      inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
	      group by c.store, b.year_month
	      having sum(amount) <> 0) x


-- from morgan gross
-- select * from step_1
-- maybe this will work, but not good enough in this state
select a.page, a.line, sum(a.amount), sum(-b.amount) as gross
from step_1 a
join fin.fact_gl b on a.control = b.control
  and b.post_status = 'Y'
join dds.dim_date bb on b.date_key = bb.date_key
  and bb.year_month = 202102  
join fin.dim_account c on b.account_key = c.account_key
  and c.account in (
    select b.g_l_acct_number::citext as account
    from arkona.ext_eisglobal_sypffxmst a
    inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
      and b.factory_financial_year  = (select max(factory_financial_year) from arkona.ext_ffpxrefdta)
    where a.fxmcyy = (select max(fxmcyy) from arkona.ext_eisglobal_sypffxmst)
      and coalesce(b. consolidation_grp, '1') <> '3'
      and b.g_l_acct_number <> ''
      and a.fxmpge between 5 and 15
--       and (
--         (a.fxmpge between 5 and 15) or
--         (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
--         (a.fxmpge = 17 and a.fxmlne between 1 and 19))
      and b.g_l_acct_number not in ('144502','164502')) 
group by a.page, a.line
order by page, line

----------------------------------------------------------------------------
--< flightplan  counts and gross don't look good, but sale dates could be interesting
--               but it only includes retail
----------------------------------------------------------------------------
-- count is ok
-- only includes retail
-- gross is not even close ~ 50%
-- used count ok, gross os way off
select * from fp.flightplan 
where year_month = 202102 
and department = 'gm sales'
order by area, the_date

select * from fp.flightplan 
where the_date = current_Date - 1
and department = 'gm sales'
order by area, the_date
-- this is the sales update from flightplan
-- insert into fp.acct_sales_counts_details
-- used count no good
select year_month, the_date, store::citext, page, line, control, sum(unit_count) as unit_count, count(line) over (partition by store, page, line),
  count(page) over (partition by store, page)
from (
  select b.year_month, b.the_date, d.store, d.page, d.line, a.control, 
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 202102
  join fin.dim_account c on a.account_key = c.account_key
    and c.account_type_code = '4'
  join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  join fp.gl_accounts d on c.account = d.account
    and (
      (d.page in (5,7,8,9,10,14) and (d.line between 1 and 19 or d.line between 25 and 45))  or
      (d.page = 16 and d.line between 1 and 6))
  where a.post_status = 'Y'
--  *a*  
    and a.control <> 'H13881G') h
group by year_month, the_date, store, page, line, control
order by page, line


--03/26/21
-- wd_factor:  wd_in_month/wd_of_month_elapsed
select (select fp.get_working_days(a.the_Date, a.department)), a.the_date, a.wd_in_month, a.wd_of_month_elapsed from dds.working_Days a where department = 'sales' and year_month = 202103
-- the count looks pretty good, need to look at gross
hmmm, gross today fp: 121608, doc: 117890


select * from fp.flightplan 
where the_date = current_date - 1
--   and seq in (1,2,3,4,5,6,8)
order by seq

-- counts
-- accounting
select a.*
from fp.acct_sales_counts_details a
where year_month = 202103
order by store, the_date
-- boards
select * from fp.boarded_Deals_counts
where year_month = 202103
order by the_date


-- board counts
select year_month, boarded_date, store, stock_number, vehicle_type,
  vehicle_make, front_gross, fi_gross, count(vehicle_type) over (partition by store,vehicle_type,vehicle_make),
  sum(front_gross + fi_gross) over (partition by store, vehicle_type,vehicle_make)
from (
  select (select year_month from dds.dim_date where the_date = current_date),
    boarded_ts ::date as boarded_date,
    case
      when d.store like '%outlet' then 'outlet'::citext
      when d.store like '%Chev%' then 'RY1':: citext
      when d.store like '%Honda%' then 'RY2'::citext
    end as store, a.stock_number, 
    e.vehicle_type, e.vehicle_make,
    case
      when g.amount = 'NaN' then 0
      else coalesce(g.amount, 0)::integer 
    end as front_gross,
     case
      when h.amount = 'NaN' then 0
      else coalesce(h.amount, 0)::integer 
    end as fi_gross
  from board.sales_board a
  inner join board.board_types b on a.board_type_key = b.board_type_key
  inner join dds.dim_date c on a.boarded_ts::date = c.the_date
  inner join onedc.stores d on a.store_key = d.store_key
  inner join board.daily_board e on a.board_id = e.board_id
  left join board.deal_gross g on a.board_id = g.board_id
    and g.current_row = true
    and g.amount_type =  'front_end_gross'
  left join board.deal_gross h on a.board_id = h.board_id
    and h.current_row = true
    and h.amount_type = 'fi_gross'
  where not a.is_backed_on
    and  b.board_type in ('Deal')
    and b.board_sub_type in ('retail','lease')
    and c.year_month = 202103
    and not a.is_deleted) x 
group by year_month, boarded_date, store, stock_number, vehicle_type,
  vehicle_make, front_gross, fi_gross;
----------------------------------------------------------------------------
--/> flightplan
----------------------------------------------------------------------------


select min(boarded_Date) from board.sales_board  -- 11/01/2017

*** will need dates for non deal transactions, dealer trades, wholesale

select * from nc.vehicle_sales where delivery_date between '02/01/2021' and '02/28/2021' and sale_type = 'ctp' -- 'dealer trade'

select * from step_1 where control = 'G41386'

-- dealer trade, yuck
-- not in bopmast
-- not in step_1 select * from step_1 where control = 'G41386'
select b.the_date, d.journal_code, c.account, c.description, a.control, a.doc, a.ref, a.amount
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
join fin.dim_account c on a.account_key = c.account_key
join fin.dim_journal d on a.journal_key = d.journal_key
where a.post_status = 'Y'
  and a.control = 'G41386'
order by d.journal_code, c.account


-- uc intramarket wholesale
-- not in bopmast select * from arkona.ext_bopmast where bopmast_stock_number = 'G41839A'
-- in step_1 select * from step_1 where control = 'G41839A'
select b.the_date, d.journal_code, c.account, c.description, a.control, a.doc, a.ref, a.amount
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
join fin.dim_account c on a.account_key = c.account_key
join fin.dim_journal d on a.journal_key = d.journal_key
where a.post_status = 'Y'
  and a.control = 'G41839A'
order by d.journal_code, c.account

-- uc wholesale
-- in bopmast select * from arkona.ext_bopmast where bopmast_stock_number = 'G38431XA'
-- in step_1 select * from step_1 where control = 'G38431XA'
select b.the_date, d.journal_code, c.account, c.description, a.control, a.doc, a.ref, a.amount
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
join fin.dim_account c on a.account_key = c.account_key
join fin.dim_journal d on a.journal_key = d.journal_key
where a.post_status = 'Y'
  and a.control = 'G38431XA'
order by d.journal_code, c.account


-- ctp
-- in bopmast select * from arkona.ext_bopmast where bopmast_stock_number = 'G41612'
-- in step_1 select * from step_1 where control = 'G41612'
select b.the_date, d.journal_code, c.account, c.description, a.control, a.doc, a.ref, a.amount
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
join fin.dim_account c on a.account_key = c.account_key
join fin.dim_journal d on a.journal_key = d.journal_key
where a.post_status = 'Y'
  and a.control = 'G41612'
order by d.journal_code, c.account