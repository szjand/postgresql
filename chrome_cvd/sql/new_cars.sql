﻿select count(*) from cu.used_3 a
-- where not exists (
-- select 1
-- from cvd.vin_descriptions
-- where vin = a.vin)
where not exists (
  select 1
  from cvd.bad_vins
  where vin = a.vin)
and not exists (
  select 1
  from cvd.vehicles
  where vin = a.vin)  

select count(distinct vin) from cu.used_3  

-- initially, most recent arrivals
select vin
from (
	select *
	from nc.vehicle_acquisitions
	where stock_number like 'G%' 
	order by ground_date desc
	limit 20) a
union (
	select vin
	from nc.vehicle_acquisitions
	where stock_number like 'H%'
	order by ground_date desc
	limit 20) 
union (
	select vin
	from nc.vehicle_acquisitions
	where stock_number like 'T%'
	order by ground_date desc
	limit 20) 


-- DROP TABLE cvd.vin_descriptions;

CREATE TABLE cvd.nc_vin_descriptions
(
  vin citext NOT NULL,
  response jsonb,
  style_count integer,
  source citext,
  the_date date,
  error citext,
  CONSTRAINT nc_vin_descriptions_pkey PRIMARY KEY (vin));
COMMENT ON TABLE cvd.nc_vin_descriptions
  IS 'this is the chrome data returned from submitting a new car vins to their CVD API.  includes the vin, the entire json response and 
a couple more miscellaneous attributes';

select * from cvd.nc_vin_descriptions



-- errors  none
select vin from cvd.nc_vin_descriptions where response->>'error' = 'true'

select vin, a.response->>'message'
from cvd.nc_vin_descriptions a
where the_date = current_date
  and (
			a.response->>'message' = 'Invalid vin'
      or a.response->>'message' = 'The VIN passed was modified to convert invalid characters'
      or a.response->>'message' = 'VIN not carried in our data')
union
select vin, 'motorcycle'
from cvd.nc_vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
where b->>'bodyType' = 'Motorcycle'
	and the_Date = current_date
union
select vin, 'Sparse with no style_id' 
from cvd.nc_vin_descriptions
where source = 'S'
	and the_date = current_date	  
	and vin in (
		select vin 
		from cvd.nc_vin_descriptions a
		join jsonb_array_elements(a.response->'result'->'vehicles') b on true
		where b->>'styleId' is null
			and the_date = current_date)	  
union			
select vin, 'No source'
from cvd.nc_vin_descriptions
where source is null
  and the_date = current_date;

select vin, 'vinSubmitted <> vinProcessed'
from cvd.nc_vin_descriptions a
where a.response->'result'->>'vinProcessed' <> a.response->'result'->>'vinSubmitted'  

-- colors

CREATE TABLE cvd.nc_colors (
  vin citext NOT NULL,
  exterior citext,
  interior citext,
  CONSTRAINT nc_colors_pkey PRIMARY KEY (vin),
  CONSTRAINT nc_colors_vin_fkey FOREIGN KEY (vin)
      REFERENCES cvd.nc_vin_descriptions (vin) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION);
COMMENT ON TABLE cvd.nc_colors
  IS 'this table will be populated when cvd.nc_vin_descriptions is used to populate a table that includes colors, should this
  table also include generic colors?';

-- insert 52 rows
insert into cvd.nc_colors(vin,exterior,interior) 
select vin, b->>'description', c->>'description'
from cvd.nc_vin_descriptions a
join jsonb_array_elements(a.response->'result'->'exteriorColors') b on true
join jsonb_array_elements(a.response->'result'->'interiorColors') c on true
where jsonb_array_length(a.response->'result'->'exteriorColors') = 1
  and jsonb_array_length(a.response->'result'->'interiorColors') = 1;

-- leaves 8 to figure out  
select *
from cvd.nc_vin_descriptions a
where not exists (
  select 1
  from cvd.nc_colors
  where vin = a.vin)

-- 3N1CP5DV3RL485524, 4T1KZ1AK1RU095772, 3N1AB8DV6RY246169  source = B style_count = 1, 2 exterior colors, type 1 and type 2
-- with this small return set, easy enough to pick these 3 out
-- not sure how to do it in code
insert into cvd.nc_colors(vin,exterior,interior) 
select vin,  b->>'description', c->>'description'
from cvd.nc_vin_descriptions a
join jsonb_array_elements(a.response->'result'->'exteriorColors') b on true and b->>'type' = '1'
join jsonb_array_elements(a.response->'result'->'interiorColors') c on true
where a.vin in ('3N1CP5DV3RL485524','4T1KZ1AK1RU095772','3N1AB8DV6RY246169');

-- so, now down to 5
select *
from cvd.nc_vin_descriptions a
where not exists (
  select 1
  from cvd.nc_colors
  where vin = a.vin)

All 5 are source = C with style_count = 1

2HKRS4H27RH442465 224 Honda, 8 exterior colors, all are type 1 and primary true
so, lets go to inpmast

select a.vin, inpmast_stock_number, b.make, b.color, b.trim
from cvd.nc_vin_descriptions a
left join arkona.ext_inpmast b on a.vin = b.inpmast_vin
where not exists (
  select 1
  from cvd.nc_colors
  where vin = a.vin)

2HKRS4H27RH442465,H17317,HONDA,RED,
5FNRL6H96RB017727,H17309,HONDA,GREY,
5FNYF8H84RB001287,H17202,HONDA,RADIANT RED METALLIC,BK
2HGFE2F23RH520171,H17305,HONDA,BLACK,
5FNRL6H68RB020005,H17201,HONDA,MODERN STEEL METALLIC,GR





identify hondas by model code, then use model code to define style_id, will that help with identifying the color?


    
-- insert into cvd.colors(vin,exterior,interior)  
drop table if exists jon.color1;
create unlogged table jon.color1 as
select vin, b->>'description' as exterior, c->>'description' as interior
from cvd.nc_vin_descriptions a
join jsonb_array_elements(a.response->'result'->'exteriorColors') b on true
join jsonb_array_elements(a.response->'result'->'interiorColors') c on true
where jsonb_array_length(a.response->'result'->'exteriorColors') = 1
  and jsonb_array_length(a.response->'result'->'interiorColors') = 1
  and the_date = current_date;
-- that returns 52 rows
-- this returns 8 rows
drop table if exists jon.color2;
create unlogged table jon.color2 as
select *
from cvd.nc_vin_descriptions a
-- join jsonb_array_elements(a.response->'result'->'exteriorColors') b on true
-- join jsonb_array_elements(a.response->'result'->'interiorColors') c on true
where jsonb_array_length(a.response->'result'->'exteriorColors') <> 1
  or jsonb_array_length(a.response->'result'->'interiorColors') <> 1

-- all have style_count = 1
select * 
from jon.color2 a
left join jon.color1 b on a.vin = b.vin  

select a.*, d.color, d.trim, b->>'description' as exterior, c->>'description' as interior
from jon.color2 a
join jsonb_array_elements(a.response->'result'->'exteriorColors') b on true
join jsonb_array_elements(a.response->'result'->'interiorColors') c on true
left join arkona.ext_inpmast d on a.vin = d.inpmast_vin
where b->>'type' = '1'




-- drop table if exists jon.color1;
-- create unlogged table jon.color1 as

select vin from (
select vin, b->>'description' as exterior, c->>'description' as interior
from cvd.nc_vin_descriptions a
join jsonb_array_elements(a.response->'result'->'exteriorColors') b on true
join jsonb_array_elements(a.response->'result'->'interiorColors') c on true
where b->>'type' = '1'
  and jsonb_array_length(a.response->'result'->'interiorColors') = 1
  and the_date = current_date
) x group by vin having count(*) > 1  


select * from cvd.nc_vin_descriptions where vin = '5FNYF8H84RB001287'


-- from cvd_colors.sql
select vin, b->>'description', c->>'description'
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'exteriorColors') b on true
join jsonb_array_elements(a.response->'result'->'interiorColors') c on true
where jsonb_array_length(a.response->'result'->'exteriorColors') = 1
  and jsonb_array_length(a.response->'result'->'interiorColors') = 1
  and the_date = current_date


select count(*) from cvd.nc_vin_descriptions

-- introducing vehicles brought in 1 dup, toyota JTMABACA7PA044168 style_count = 2
select a.vin, a.response->'result'->>'make', aa->>'mfrModelCode', b.color, b.trim, b.color_code, b.model_code, a.source, a.style_count
from cvd.nc_vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') aa on true
left join arkona.ext_inpmast b on a.vin = b.inpmast_vin
  and 
    case
      when a.response->'result'->>'make' = 'Toyota' then b.inpmast_company_number = 'RY8'
      else b.inpmast_company_number = 'RY1'
    end
order by  a.response->'result'->>'make', a.vin


2T3E6RFV5RW052244


































































drop table if exists cvd.z_wtf cascade;
create unlogged table cvd.z_wtf as
select a.vin, (a.response->'result'->>'year')::integer as model_year, a.response->'result'->>'make' as make, 
  a.response->'result'->>'model' as model, b->>'trim' as trim_level, 
  case when coalesce(length(b->>'driveType'), 0) = 0 then 'unknown' else b->>'driveType' end as drive,
  coalesce(b->>'mfrModelCode', 'unknown') as model_code,
  case 
    when b->>'bodyType' like '%Cab%' or b->>'bodyType' like '%Crew%' 
      then 'Pickup' 
    else b->>'bodyType' 
  end as body_type, -- problem, doesn't break out cab type separate from body type, so, no body type of pickup !?!?! Toyota has some as crew max
  k as segment,
  case when b->>'bodyType' like '%Cab%' then b->>'bodyType' else 'n/a' end as cab,
  coalesce(j->>'name', 'n/a') as box_length,
  f->>'name' as eng_disp,
  g->>'nameNoBrand' as cylinders,
  coalesce(h->>'name', l->>'name', 'unknown') as fuel, 
--   l->>'name' as fuel_2,
  coalesce(i->>'name', 'unknown') as transmission,
  c.exterior as ext_color, -- c->>'genericDesc' as generic_ext_color, no longer have the generics, hmm maybe should have included them
  coalesce(c.interior, 'unknown') as int_color, -- d->>'genericDesc' as generic_int_color,
  (b->>'doors')::integer as doors, -- also features id 12175
  (a.response->'result'->>'buildMSRP')::numeric::integer as build_msrp, (b->>'baseMSRP')::numeric::integer as base_msrp,
  b->>'styleDescription' as style_description,
  a.response->'result'->>'source' as source,
  b->>'styleId' as style_id
from cvd.nc_vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
join cvd.colors c on a.vin = c.vin
left join jsonb_array_elements(a.response->'result'->'techSpecs') f on f->>'id' = '10120'
left join jsonb_array_elements(a.response->'result'->'techSpecs') g on g->>'description' = 'Engine Cylinders'
left join jsonb_array_elements(a.response->'result'->'features') h on h->>'id' = '10030'
left join jsonb_array_elements(a.response->'result'->'features') i on i->>'id' = '25190'
left join jsonb_array_elements(a.response->'result'->'techSpecs') j on j->>'id' = '18090'
left join jsonb_array_elements_text(b->'segment') k on true
left join jsonb_array_elements(a.response->'result'->'techSpecs') l on  l->>'id' = '22360'
-- where a.source <> 'B' 
--   and a.style_count = 1   
--   and a.the_date = current_date;
-- create unique index on cvd.z_wtf(vin);  