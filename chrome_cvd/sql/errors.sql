﻿----------------------------------------------------------------------------------------------------------
--< 12/5/23  remove the bad vins from cvd.vin_descriptions, and delete the relevant vins from cvd.colors
----------------------------------------------------------------------------------------------------------

select a.vin, a.source, a.response->'result'->>'make' as make, a.response->'result'->>'model' as model, a.response->'result'->>'year' as model_year,
  a.response->>'message' as error
from cvd.vin_descriptions a
where response->>'error' = 'true'
order by a.response->>'message'

drop table if exists cvd.bad_vins cascade;
create table cvd.bad_vins (
  vin citext primary key,
  reason citext);
comment on table cvd.bad_vins is 'vins that do not decode in chrome cvd. these vins are to be removed from cvd.vin_descriptions, 
don''t yet know where this fits into the workflow, also need to deal with vin foreign keys, at this time, cvd.colors(vin)';

select a.vin, a.source, a.response->'result'->>'make' as make, a.response->'result'->>'model' as model, 
  a.response->'result'->>'year' as model_year, style_count, source,
  a.response->>'message' as error
from cvd.vin_descriptions a
where response->>'error' = 'true'
  and a.response->>'message' <> 'No Match Found in request Locale. Returning result for alternate Locale'
order by a.response->>'message'

select * from cvd.vin_descriptions where source is null or style_count is null
1J8FF57BX9D175061
4Y83Z187497
F10YPS81146
-- these will have to be removed to delete the vins from cvd.vin_descriptions
select * from cvd.colors where vin in ('1J8FF57BX9D175061','4Y83Z187497','F10YPS81146')

-- JF1GJAC64FHO22665, the api changed the O to a 0 and generated a response
select * 
from cvd.vin_descriptions
where response->>'message' = 'The VIN passed was modified to convert invalid characters'

select * from cvd.colors where vin = 'JF1GJAC64FHO22665'
JF1GJAC64FHO22665,Jasmine Green Metallic,Ivory


-- BAD VINS -------------------------------------------------------------
--step 1 add the vins to cvd.bad_vins
insert into cvd.bad_vins(vin,reason)
select vin, error
from cvd.vin_descriptions
where response->>'error' = 'true'
  and (
    style_count is null
    or
    source is null); 

-- step 2, delete FK instances of the vins
 delete
 from cvd.colors
 where vin in (
   select vin
   from cvd.bad_vins);   

-- step 3 delete vins from cvd.vin_descriptions
delete 
from cvd.vin_descriptions
where vin in (
  select vin
  from cvd.bad_vins);   


-- CORRECTED VINS ---------------------------------------------------
-- a single vin
select response->'result'->>'vinSubmitted' as vin_submitted, response->'result'->>'vinProcessed' as vin_processed
from cvd.vin_descriptions
where response->>'message' = 'The VIN passed was modified to convert invalid characters';

-- delete row from cvd.colors
select * from cvd.colors where vin = 'JF1GJAC64FHO22665'
JF1GJAC64FHO22665,Jasmine Green Metallic,Ivory

delete 
from cvd.colors
where vin = 'JF1GJAC64FHO22665';

-- update cvd.vin_descriptions
-- this failed, because the good vin had actually been run the day before !?!?!
-- select * from cvd.vin_descriptions where vin in ('JF1GJAC64FH022665','JF1GJAC64FHO22665')
update cvd.vin_descriptions
set vin = 'JF1GJAC64FH022665'
where vin = 'JF1GJAC64FHO22665'

-- insert correct vin in cvd.colors
-- and so this failed, because the good vin had already been processed
insert into cvd.colors(vin,exterior,interior)
values('JF1GJAC64FH022665','Jasmine Green Metallic','Ivory');

select * from cvd.vin_descriptions where vin in ('JF1GJAC64FH022665','JF1GJAC64FHO22665')
select * from cvd.colors where vin = 'JF1GJAC64FH022665'

-- delete the bad row
delete 
from cvd.vin_descriptions
where vin = 'JF1GJAC64FHO22665';
----------------------------------------------------------------------------------------------------------
--/> 12/5/23  remove the bad vins from cvd.vin_descriptions, and delete the relevant vins from cvd.colors
----------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------
--< 12/4/23
----------------------------------------------------------------------------------------------------------
-- 83 rows with errors
select a.vin, a.source, a.response->'result'->>'make' as make, a.response->'result'->>'model' as model, a.response->'result'->>'year' as model_year,
  a.response->>'message' as error
from cvd.vin_descriptions a
where response->>'error' = 'true'
order by a.response->>'message'

select count(*) , error
from (
select a.vin, a.source, a.response->'result'->>'make' as make, a.response->'result'->>'model' as model, a.response->'result'->>'year' as model_year,
  a.response->>'message' as error
from chr.cvd a
where response->>'error' = 'true') B
group by error;


2,Invalid vin
79,No Match Found in request Locale. Returning result for alternate Locale
1,The VIN passed was modified to convert invalid characters
1,VIN not carried in our data


-- think it would be a good idea to expose the error at the base level
select x.vin, x.response->'result'->>'source' as source,
		jsonb_array_length(x.response->'result'->'vehicles') as style_count,
		case
		  when x.response->>'error' = 'true' then x.response->>'message' 
		end as error
from cvd.vin_descriptions x
where vin = '3C4PDDFG5CT148294'

select * 
from chr.cvd 
where vin = '3C4PDDFG5CT148294'

----------------------------------------------------------------------------------------------------------
--/> 12/4/23
----------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------
--< 12/8/23  no style_id = bad vin
----------------------------------------------------------------------------------------------------------
select a.*
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
where a.source = 's'
  and b->>'styleId' is null

-- BAD VINS -------------------------------------------------------------
--step 1 add the vins to cvd.bad_vins
insert into cvd.bad_vins(vin,reason)
select a.vin, 'NULL style_id'
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
where a.source = 's'
  and b->>'styleId' is null;

-- step 2, delete FK instances of the vins
delete
from cvd.colors
where vin in (
  select vin
  from cvd.bad_vins);   

-- step 3 delete vins from cvd.vin_descriptions
delete 
from cvd.vin_descriptions
where vin in (
  select vin
  from cvd.bad_vins);   
----------------------------------------------------------------------------------------------------------
--/> 12/8/23  no style_id = bad vin
----------------------------------------------------------------------------------------------------------