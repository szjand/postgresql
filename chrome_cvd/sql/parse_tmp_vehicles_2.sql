﻿-- a continuation of E:\sql\postgresql\chrome_cvd\sql\parse_tmp_vehicles.sql, which was catching up on getting old data into cvd.vehicles
-- this is the monthly exercise of getting the vins sold in the just closed month into cvd.vehicles with 
-- reference to E:\sql\postgresql\canonical_used_vehicle_source_data\month_end_update_tmp_vehicles.sql

-- first thing i see, need to update chr.build_data_coverage, double check existing makes and add ford and toyota, 
--   the table needs to be in cvd, not chrome

DROP TABLE if exists cvd.build_data_coverage;
CREATE TABLE cvd.build_data_coverage
(
  oem citext NOT NULL,
  division citext NOT NULL,
  from_year integer NOT NULL,
  thru_year integer NOT NULL,
  CONSTRAINT build_data_coverage_pkey PRIMARY KEY (division));


insert into cvd.build_data_coverage
select * from chr.build_data_coverage;

insert into cvd.build_data_coverage(oem,division,from_year,thru_year) values
('ford','ford',2011,2100),
('ford','lincoln',2011,2100),
('ford','mercury',2011,2011),
('toyota','toyota',2001,2100),
('toyota','lexus',2010,2100);

update cvd.build_data_coverage
set oem = 'stellantis'
where oem = 'FCA';

update cvd.build_data_coverage
set thru_year = 2100
where division = 'hummer';

select * from cvd.build_Data_coverage order by oem, division

-- 1. what are the new vins that first need to get cvd'd
-- there is no need to start out breaking the data up into build/not build etc, all vehicles now get run in one system: CVD
-- 1/7/24 463 vins
-- !!!!!!!!!!  forgot to exclude vins in cvd.unresolved_vins
-- once a vin is in cvd.unresolved_vins, it is out of play forever
select distinct a.vin --2303
from cu.used_3  a
where not exists (
  select 1
  from cvd.vin_descriptions
  where vin = a.vin)
and not exists (
  select 1
  from cvd.bad_vins
  where vin = a.vin)  
and not exists (
  select 1
  from cvd.unresolved_vins
  where vin = a.vin)    


/* 
this number of vehicle, 2303, is way higher than i expected, because i based the previous catch up effort
on cu.tmp_vehicles instead of cu.useed_3
*/

-- this is the query to feed to cvd API in python
select distinct a.vin  --2303
from cu.used_3  a
left join arkona.ext_inpmast b on a.vin = b.inpmast_vin
  and b.inpmast_company_number = 'RY1'
where not exists (
  select 1
  from cvd.vin_descriptions
  where vin = a.vin)
and not exists (
  select 1
  from cvd.bad_vins
  where vin = a.vin)  
order by make,model,year


drop table if exists t1;
create temp table t1 as
select *
from cvd.vin_descriptions a
where not exists (
  select 1
  from cvd.vehicles
  where vin = a.vin)
and the_date = current_Date  

 select source, style_count, count(*)
 from t1 
 group by source, style_count

drop table if exists errors;
create temp table errors as
select a.vin, a.source, a.response->'result'->>'make' as make, a.response->'result'->>'model' as model, a.response->'result'->>'year' as model_year,
  a.response->>'message' as error
from cvd.vin_descriptions a
where response->>'error' = 'true'
  and the_date = current_date
order by a.response->>'message' 

select * from errors


-- /***************************************************************************************
-- step 1 is to process all the errors first
-- ***************************************************************************************/

select vin, response->>'message', the_date from cvd.vin_descriptions where response->>'error' = 'true' and the_date = current_date

-- 12/12/23 ------------------------------------------------------------------------------
-- I. The VIN passed was modified to convert invalid characters
-- bad vin JF1GJAC64FHO22665
select * from cvd.vin_descriptions where vin in ('JF1GJAC64FHO22665','JF1GJAC64FH022665')
insert into cvd.bad_vins values('JF1GJAC64FHO22665','the correct vin, with a 0 not an O previously run');
delete from cvd.vin_descriptions where vin = 'JF1GJAC64FHO22665';

-- bad vin 1GKS2GKC9GR299732, replaced by API with 1GKS2GKC2GR299732

select * from cvd.vin_descriptions where vin in ('1GKS2GKC9GR299732','1GKS2GKC2GR299732')
no record of the good vin in either Dealertrack or Tool, very sparse owner record of bad vin in dealertrack
I believe the thing to do here is to edit the bad vin in cvd.vin_descriptions to the good vin, the json object
retains the fact that the bad vin was submitted and the good vin was processed

update cvd.vin_descriptions
set vin = '1GKS2GKC2GR299732'
where vin = '1GKS2GKC9GR299732';

-- II. Invalid vin
insert into cvd.bad_vins
select vin, a.response->>'message'
from cvd.vin_descriptions a
where a.response->>'message' = 'Invalid vin'
  and the_date = current_date;

delete from cvd.vin_descriptions
where vin in (
	select vin
	from cvd.vin_descriptions a
	where a.response->>'message' = 'Invalid vin'
	  and the_date = current_date);

-- III. VIN not carried in our data
-- 3C63R2HL7MG540368 decodes in ADS, sent an email to chrome, in the meantime, i will delete with the others

insert into cvd.bad_vins
select vin, a.response->>'message'
from cvd.vin_descriptions a
where a.response->>'message' = 'VIN not carried in our data'
  and the_date = current_date; 

delete from cvd.vin_descriptions
where vin in (
	select vin
	from cvd.vin_descriptions a
	where a.response->>'message' = 'VIN not carried in our data'
	  and the_date = current_date);

-- IV. Motorcycles
-
delete 
from cvd.vin_descriptions
where vin in (
	select vin 
	from cvd.vin_descriptions a
	join jsonb_array_elements(a.response->'result'->'vehicles') b on true
	where b->>'bodyType' = 'Motorcycle'
		and the_Date = current_date);


-- V. Sparse with no style_id

delete
select vin
from cvd.vin_descriptions
where vin in (
	select vin 
	from cvd.vin_descriptions
	where source = 'S'
		and the_date = current_date	  
		and vin in (
			select vin 
			from cvd.vin_descriptions a
			join jsonb_array_elements(a.response->'result'->'vehicles') b on true
			where b->>'styleId' is null
				and the_date = current_date));

			
-- /***************************************************************************************
-- step 2 is colors
-- ***************************************************************************************/				
-- 117 vin already exist in cvd.colors, leaving 346 to add
select a.vin
from cvd.vin_descriptions a
where a.the_date = current_date - 1
  and not exists (
    select 1
    from cvd.colors
    where vin = a.vin)

-- 1st, always, those with a single color, both exterior and interior
-- 1/7/24  117
insert into cvd.colors(vin,exterior,interior)  -- 117
select vin, b->>'description' as exterior, c->>'description' as interior
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'exteriorColors') b on true
join jsonb_array_elements(a.response->'result'->'interiorColors') c on true
where the_date = current_date - 1
	and jsonb_array_length(a.response->'result'->'exteriorColors') = 1
	and jsonb_array_length(a.response->'result'->'interiorColors') = 1
	and not exists (
		select 1
		from cvd.colors
		where vin = a.vin);	

-- 1/7/24 and that leaves us with 346
select vin
from cvd.vin_descriptions a
where the_date = current_date
	and not exists (
		select 1
		from cvd.colors
		where vin = a.vin)

-- do inpmast and see if i can get a match from cvd
drop table if exists cvd.tmp_exterior;
create table cvd.tmp_exterior (
  vin citext primary key,
  exterior citext not null,
  interior citext);

-- 1/7/24: 148 rows
insert into cvd.tmp_exterior(vin,exterior)
select distinct vin, exterior
from (
	select a.vin, b.color_code, b.color, c->>'description' as exterior, similarity(b.color, c->>'description'),
		row_number() over (partition by vin order by similarity(b.color, c->>'description') desc) as seq
	from cvd.vin_descriptions a
	join arkona.ext_inpmast b on a.vin = b.inpmast_vin
		and b.inpmast_company_number = 'RY1'
	left join jsonb_array_elements(a.response->'result'->'exteriorColors') c on similarity(b.color, c->>'description') > .2
	where a.the_date = current_date - 1
		and c->>'description' is not null 
		and not exists (
			select 1
			from cvd.colors
			where vin = a.vin)
	order by a.vin asc, similarity(b.color, c->>'description') desc) aa
where aa.seq = 1	
	and not exists (
		select 1
		from cvd.colors
		where vin = aa.vin);

-- same thing with toyota vins
-- this gives me no vins not already processed
insert into cvd.tmp_exterior(vin,exterior)
select distinct vin, exterior
from (
	select a.vin, b.color_code, b.color, c->>'description' as exterior, similarity(b.color, c->>'description'),
		row_number() over (partition by vin order by similarity(b.color, c->>'description') desc) as seq
	from cvd.vin_descriptions a
	join arkona.ext_inpmast b on a.vin = b.inpmast_vin
		and b.inpmast_company_number = 'RY8'
	left join jsonb_array_elements(a.response->'result'->'exteriorColors') c on similarity(b.color, c->>'description') > .2
	where a.the_date = current_date - 1
		and c->>'description' is not null 
		and not exists (
			select 1
			from cvd.colors
			where vin = a.vin)
	order by a.vin asc, similarity(b.color, c->>'description') desc) aa
where aa.seq = 1	
	and not exists (
		select 1
		from cvd.colors
		where vin = aa.vin)
	and not exists (
		select 1
		from cvd.tmp_exterior
		where vin = aa.vin);		

		
-- try the same thing with vehicle items
-- 1/7/24 81 rows
insert into cvd.tmp_exterior(vin,exterior)
select distinct vin, exterior
from (
	select a.vin, b.exteriorcolor, c->>'description' as exterior, similarity(b.exteriorcolor, c->>'description'),
		row_number() over (partition by a.vin order by similarity(b.exteriorcolor, c->>'description') desc) as seq
	from cvd.vin_descriptions a
	join ads.ext_vehicle_items b on a.vin = b.vin
	left join jsonb_array_elements(a.response->'result'->'exteriorColors') c on similarity(b.exteriorcolor, c->>'description') > .2
	where a.the_date = current_date - 1
		and c->>'description' is not null 
		and not exists (
			select 1
			from cvd.colors
			where vin = a.vin)		
		and not exists (
			select 1
			from cvd.tmp_exterior
			where vin = a.vin)
	order by a.vin asc, similarity(b.exteriorcolor, c->>'description') desc) aa
where aa.seq = 1	
	and not exists (
		select 1
		from cvd.colors
		where vin = aa.vin);


-- leaving 117 rows
insert into cvd.tmp_exterior(vin,exterior)
select distinct vin, coalesce(exteriorcolor, color)
from (
	select a.vin, source, style_count, b.color, b.trim, c.exteriorcolor, c.interiorcolor
	from cvd.vin_descriptions a
	left join arkona.ext_inpmast b on a.vin = b.inpmast_vin
	left join ads.ext_vehicle_items c on a.vin = c.vin
	where a.the_date = current_date - 1
		and not exists (
			select 1
			from cvd.tmp_exterior
			where vin = a.vin)
		and not exists (
			select 1
			from cvd.colors
			where vin = a.vin)) x		
--------------------------
-- now interior
-- just making sure
select vin from cvd.tmp_exterior a
where exists (select 1 from cvd.colors where vin = a.vin)		

update cvd.tmp_exterior x
set interior = y.interior
from (		
	select distinct b.vin, coalesce(initcap(c.interiorcolor), initcap(a.trim), 'unknown') interior
	from cvd.vin_descriptions b
	join arkona.ext_inpmast a on a.inpmast_vin = b.vin
		and a.inpmast_company_number = 'RY1'
	left join ads.ext_vehicle_items c on  b.vin = c.vin
	where b.the_date = current_date - 1
		and not exists (
			select 1
			from cvd.colors
			where vin = a.inpmast_vin)) y
where x.vin = y.vin;			

select vin a
from cvd.vin_descriptions a
where the_date = current_date - 1
  and not exists (
    select 1
    from cvd.tmp_exterior
    where vin = a.vin)

update cvd.tmp_exterior
set interior = 'unknown'
where interior is null;

insert into cvd.colors(vin,exterior,interior)
select * from cvd.tmp_exterior;

-- bingo
select vin
from cvd.vin_descriptions a
where not exists (
  select 1
  from cvd.colors
  where vin = a.vin)


	
=====================================================================================================================================================
=====================================================================================================================================================

--------------------------------------------------------------------------------------------------------------------------
--< CASE 1 source = B style_count = 1
--------------------------------------------------------------------------------------------------------------------------
drop table if exists cvd.z_wtf cascade;
create unlogged table cvd.z_wtf as
select a.vin, (a.response->'result'->>'year')::integer as model_year, a.response->'result'->>'make' as make, 
  a.response->'result'->>'model' as model, b->>'trim' as trim_level, 
  b->>'driveType' as drive,
  b->>'mfrModelCode' as model_code,
  case 
		when b->>'bodyType' like '%Cab%' or b->>'bodyType' like '%Crew%' 
			then 'Pickup' 
		else b->>'bodyType' 
	end as body_type, -- problem, doesn't break out cab type separate from body type, so, no body type of pickup !?!?! Toyota has some as crew max
  k as segment,
  case when b->>'bodyType' like '%Cab%' then b->>'bodyType' else 'n/a' end as cab,
  coalesce(j->>'name', 'n/a') as box_length,
--   b->>'boxStyle' as box_style, -- box style is not size
  f->>'name' as eng_disp,
  g->>'nameNoBrand' as cylinders,
  coalesce(h->>'name', l->>'name', 'unknown') as fuel, 
--   l->>'name' as fuel_2,
  coalesce(i->>'name', 'unknown') as transmission,
  c.exterior as ext_color, -- c->>'genericDesc' as generic_ext_color, no longer have the generics, hmm maybe should have included them
  coalesce(c.interior, 'unknown') as int_color, -- d->>'genericDesc' as generic_int_color,
  (b->>'doors')::integer as doors, -- also features id 12175
  (a.response->'result'->>'buildMSRP')::numeric::integer as build_msrp, (b->>'baseMSRP')::numeric::integer as base_msrp,
  b->>'styleDescription' as style_description,
  a.response->'result'->>'source' as source,
  b->>'styleId' as style_id
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
join cvd.colors c on a.vin = c.vin
left join jsonb_array_elements(a.response->'result'->'techSpecs') f on f->>'id' = '10120' -- engine displacement
left join jsonb_array_elements(a.response->'result'->'techSpecs') g on g->>'description' = 'Engine Cylinders'
left join jsonb_array_elements(a.response->'result'->'features') h on h->>'id' = '10030' -- fuel 1
left join jsonb_array_elements(a.response->'result'->'features') i on i->>'id' = '25190'  -- transmission
left join jsonb_array_elements(a.response->'result'->'techSpecs') j on j->>'id' = '18090'  -- box length
left join jsonb_array_elements_text(b->'segment') k on true  -- segment
left join jsonb_array_elements(a.response->'result'->'techSpecs') l on  l->>'id' = '22360' -- fuel 2
where a.source = 'B' 
  and a.style_count = 1
  and a.the_date = current_date - 1; -- **************************************************************************************
create unique index on cvd.z_wtf(vin);  

-- insert 1629 rows
-- 1/7/24 insert 121 rows
insert into cvd.vehicles
select * from cvd.z_wtf;

--------------------------------------------------------------------------------------------------------------------------
--/> CASE 1 source = B style_count = 1
--------------------------------------------------------------------------------------------------------------------------  

--------------------------------------------------------------------------------------------------------------------------
--< CASE 2 source <> B style_count = 1
--------------------------------------------------------------------------------------------------------------------------
  
select vin  --281
from cvd.vin_descriptions a
where source <> 'B'
  and style_count = 1
  and not exists (
    select 1
    from cvd.vehicles
    where vin = a.vin)
  and the_date = current_date - 1 -- **************************************************************************************
drop table if exists cvd.z_wtf cascade;
create unlogged table cvd.z_wtf as
select a.vin, (a.response->'result'->>'year')::integer as model_year, a.response->'result'->>'make' as make, 
  a.response->'result'->>'model' as model, b->>'trim' as trim_level, 
  case when coalesce(length(b->>'driveType'), 0) = 0 then 'unknown' else b->>'driveType' end as drive,
  coalesce(b->>'mfrModelCode', 'unknown') as model_code,
  case 
    when b->>'bodyType' like '%Cab%' or b->>'bodyType' like '%Crew%' 
      then 'Pickup' 
    else b->>'bodyType' 
  end as body_type, -- problem, doesn't break out cab type separate from body type, so, no body type of pickup !?!?! Toyota has some as crew max
  k as segment,
  case when b->>'bodyType' like '%Cab%' then b->>'bodyType' else 'n/a' end as cab,
  coalesce(j->>'name', 'n/a') as box_length,
  f->>'name' as eng_disp,
  g->>'nameNoBrand' as cylinders,
  coalesce(h->>'name', l->>'name', 'unknown') as fuel, 
--   l->>'name' as fuel_2,
  coalesce(i->>'name', 'unknown') as transmission,
  c.exterior as ext_color, -- c->>'genericDesc' as generic_ext_color, no longer have the generics, hmm maybe should have included them
  coalesce(c.interior, 'unknown') as int_color, -- d->>'genericDesc' as generic_int_color,
  (b->>'doors')::integer as doors, -- also features id 12175
  (a.response->'result'->>'buildMSRP')::numeric::integer as build_msrp, (b->>'baseMSRP')::numeric::integer as base_msrp,
  b->>'styleDescription' as style_description,
  a.response->'result'->>'source' as source,
  b->>'styleId' as style_id
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
join cvd.colors c on a.vin = c.vin
left join jsonb_array_elements(a.response->'result'->'techSpecs') f on f->>'id' = '10120'
left join jsonb_array_elements(a.response->'result'->'techSpecs') g on g->>'description' = 'Engine Cylinders'
left join jsonb_array_elements(a.response->'result'->'features') h on h->>'id' = '10030'
left join jsonb_array_elements(a.response->'result'->'features') i on i->>'id' = '25190'
left join jsonb_array_elements(a.response->'result'->'techSpecs') j on j->>'id' = '18090'
left join jsonb_array_elements_text(b->'segment') k on true
left join jsonb_array_elements(a.response->'result'->'techSpecs') l on  l->>'id' = '22360'
where a.source <> 'B' 
  and a.style_count = 1   
  and a.the_date = current_date - 1;  -- **************************************************************************************
create unique index on cvd.z_wtf(vin);   

-- insert 270 rows
-- 1/7/24 insert 20 rows
insert into cvd.vehicles
select * 
from cvd.z_wtf;

--------------------------------------------------------------------------------------------------------------------------
--/> CASE 2 source <> B style_count = 1
--------------------------------------------------------------------------------------------------------------------------  

--------------------------------------------------------------------------------------------------------------------------
--< CASE 3 source = B style_count <> 1
--------------------------------------------------------------------------------------------------------------------------

-- 7 rows
-- 1/7/24 no rows
select *  
from cvd.vin_descriptions a
where source = 'B'
  and style_count <> 1
  and not exists (
    select 1
    from cvd.vehicles
    where vin = a.vin)
  and the_date = current_date - 1 -- **************************************************************************************

drop table if exists cvd.z_wtf cascade;
create unlogged table cvd.z_wtf as  
select a.vin, (a.response->'result'->>'year')::integer as model_year, a.response->'result'->>'make' as make, 
  a.response->'result'->>'model' as model, b->>'trim' as trim_level, 
  case when coalesce(length(b->>'driveType'), 0) = 0 then 'unknown' else b->>'driveType' end as drive,
  coalesce(b->>'mfrModelCode', 'unknown') as model_code,
  case 
		when b->>'bodyType' like '%Cab%' or b->>'bodyType' like '%Crew%' 
			then 'Pickup' 
		else b->>'bodyType' 
	end as body_type, -- problem, doesn't break out cab type separate from body type, so, no body type of pickup !?!?! Toyota has some as crew max
  k as segment,
  case when b->>'bodyType' like '%Cab%' then b->>'bodyType' else 'n/a' end as cab,
  coalesce(j->>'name', 'n/a') as box_length,
  f->>'name' as eng_disp,
  g->>'nameNoBrand' as cylinders,
  coalesce(h->>'name', l->>'name', 'unknown') as fuel, 
--   l->>'name' as fuel_2,
  coalesce(i->>'name', 'unknown') as transmission,
  c.exterior as ext_color, -- c->>'genericDesc' as generic_ext_color, no longer have the generics, hmm maybe should have included them
  coalesce(c.interior, 'unknown') as int_color, -- d->>'genericDesc' as generic_int_color,
  (b->>'doors')::integer as doors, -- also features id 12175
  (a.response->'result'->>'buildMSRP')::numeric::integer as build_msrp, (b->>'baseMSRP')::numeric::integer as base_msrp,
  b->>'styleDescription' as style_description,
  a.response->'result'->>'source' as source,
  b->>'styleId' as style_id
-- select a.*  
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
--   and 
--     case
--       when a.vin = 'KL5JD66Z15K091508' then b->>'styleId' = '268817'
--       when a.vin = '3MYDLBYV6JY326396' then b->>'styleId' = '393455'
--       when a.vin = 'JTDBE32K420076396' then b->>'styleId' = '1555'
--     end
join cvd.colors c on a.vin = c.vin
left join jsonb_array_elements(a.response->'result'->'techSpecs') f on f->>'id' = '10120'
left join jsonb_array_elements(a.response->'result'->'techSpecs') g on g->>'description' = 'Engine Cylinders'
left join jsonb_array_elements(a.response->'result'->'features') h on h->>'id' = '10030'
left join jsonb_array_elements(a.response->'result'->'features') i on i->>'id' = '25190'
left join jsonb_array_elements(a.response->'result'->'techSpecs') j on j->>'id' = '18090'
left join jsonb_array_elements_text(b->'segment') k on true
left join jsonb_array_elements(a.response->'result'->'techSpecs') l on  l->>'id' = '22360'
where a.source = 'B' 
  and a.style_count <> 1  
  and a.vin in ('2C8GF68494R555451','1B3LC56K38N159038','5NPDH4AE2DH315435','1B3LC56K58N690533','1B3LC46K38N175931','2C8GF68454R539151','5TDDKRFH4GS310320')
  and a.the_date = current_date;

select a.*, b.body_style, b.key_to_cap_explosion_data
from cvd.z_wtf a
left join arkona.ext_inpmast b on a.vin = b.inpmast_vin
  and inpmast_company_number = 'RY1'
order by vin

-- insert 7 rows
insert into cvd.vehicles
select a.*
from cvd.z_wtf a
where
  case
    when vin = '1B3LC46K38N175931' then style_description = '4dr Sdn SE FWD'
    when vin = '1B3LC56K38N159038' then style_description = '4dr Sdn SXT FWD'
		when vin = '5NPDH4AE2DH315435'then style_description = '4dr Sdn Auto GL'
		when vin = '1B3LC56K58N690533'then style_description = '4dr Sdn SXT FWD'
		when vin = '2C8GF68454R539151'then style_id = '108864'
		when vin = '2C8GF68494R555451'then style_id = '108864'
		when vin = '5TDDKRFH4GS310320'then style_description = 'AWD 4dr V6 Limited (Natl)'
  end

--------------------------------------------------------------------------------------------------------------------------
--/> CASE 3 source = B style_count <> 1
--------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------
--< CASE 4 source <> B style_count <> 1 and style_id exists in inpmast with no duplicate rows
--------------------------------------------------------------------------------------------------------------------------
-- 1/7/24 322
select vin  -- 372
from cvd.vin_descriptions a
where source <> 'B'
  and the_date = current_date - 1 -- **************************************************************************************
  and style_count <> 1
  and not exists (
    select 1
    from cvd.vehicles
    where vin = a.vin)


drop table if exists cvd.z_wtf cascade;
create unlogged table cvd.z_wtf as  
select a.vin, (a.response->'result'->>'year')::integer as model_year, a.response->'result'->>'make' as make, 
  a.response->'result'->>'model' as model, b->>'trim' as trim_level, 
  case when coalesce(length(b->>'driveType'), 0) = 0 then 'unknown' else b->>'driveType' end as drive,
  coalesce(b->>'mfrModelCode', 'unknown') as model_code,
  case 
		when b->>'bodyType' like '%Cab%' or b->>'bodyType' like '%Crew%' 
			then 'Pickup' 
		else b->>'bodyType' 
	end as body_type, -- problem, doesn't break out cab type separate from body type, so, no body type of pickup !?!?! Toyota has some as crew max
  k as segment,
  case when b->>'bodyType' like '%Cab%' then b->>'bodyType' else 'n/a' end as cab,
  coalesce(j->>'name', 'n/a') as box_length,
--   b->>'boxStyle' as box_style, -- box style is not size
  f->>'name' as eng_disp,
  g->>'nameNoBrand' as cylinders,
  coalesce(h->>'name', l->>'name', 'unknown') as fuel, 
--   l->>'name' as fuel_2,
  coalesce(i->>'name', 'unknown') as transmission,
  c.exterior as ext_color, -- c->>'genericDesc' as generic_ext_color, no longer have the generics, hmm maybe should have included them
  coalesce(c.interior, 'unknown') as int_color, -- d->>'genericDesc' as generic_int_color,
  (b->>'doors')::integer as doors, -- also features id 12175
  (a.response->'result'->>'buildMSRP')::numeric::integer as build_msrp, (b->>'baseMSRP')::numeric::integer as base_msrp,
  b->>'styleDescription' as style_description,
  a.response->'result'->>'source' as source,
  b->>'styleId' as style_id
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
join arkona.ext_inpmast bb on a.vin = bb.inpmast_vin
  and bb.inpmast_company_number = 'RY1'
  and b->>'styleId' = bb.key_to_cap_explosion_data::citext
join cvd.colors c on a.vin = c.vin
left join jsonb_array_elements(a.response->'result'->'techSpecs') f on f->>'id' = '10120' -- engine displ
left join jsonb_array_elements(a.response->'result'->'techSpecs') g on g->>'description' = 'Engine Cylinders'
left join jsonb_array_elements(a.response->'result'->'features') h on h->>'id' = '10030' -- fuel
left join jsonb_array_elements(a.response->'result'->'features') i on i->>'id' = '25190' -- transmission
left join jsonb_array_elements(a.response->'result'->'techSpecs') j on j->>'id' = '18090' -- box length
left join jsonb_array_elements_text(b->'segment') k on true
left join jsonb_array_elements(a.response->'result'->'techSpecs') l on  l->>'id' = '22360' -- fuel
where a.source <> 'B' 
  and a.style_count <> 1  
  and not exists (
    select 1
    from cvd.vehicles
    where vin = a.vin)
  and not exists (
    select 1
    from dups
    where vin = a.vin)

-- after initially creating the table cvd.z_wtf, create the temp table dups
-- to re-create the table cvd.z_wtf excluding the dups
drop table if exists dups;
create temp table dups as
select vin
from cvd.z_wtf
group by vin
having count(*) > 1


-- insert 80 rows
-- 1/7/24 2 rows
insert into cvd.vehicles
select *
from cvd.z_wtf 


--------------------------------------------------------------------------------------------------------------------------
--/> CASE 4 source <> B style_count <> 1 and style_id exists in inpmast with no duplicate rows
--------------------------------------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------------------------------------
--< CASE 5 not exists in cvd.vehicles a few rows with help from inpmast
--------------------------------------------------------------------------------------------------------------------------

select count(*) -- 463
from cvd.vin_descriptions
where the_date = current_date - 1

select distinct vin  -- 320
from cvd.vin_descriptions a
where not exists (
    select 1
    from cvd.vehicles
    where vin = a.vin)

drop table if exists cvd.z_wtf cascade;   --2455 w/distinct, 2759 without
create unlogged table cvd.z_wtf as  
select a.vin, (a.response->'result'->>'year')::integer as model_year, a.response->'result'->>'make' as make, 
  a.response->'result'->>'model' as model, b->>'trim' as trim_level, 
  case when coalesce(length(b->>'driveType'), 0) = 0 then 'unknown' else b->>'driveType' end as drive,
  coalesce(b->>'mfrModelCode', 'unknown') as model_code,
  case 
		when b->>'bodyType' like '%Cab%' or b->>'bodyType' like '%Crew%' 
			then 'Pickup' 
		else b->>'bodyType' 
	end as body_type, -- problem, doesn't break out cab type separate from body type, so, no body type of pickup !?!?! Toyota has some as crew max
  k as segment,
  case when b->>'bodyType' like '%Cab%' then b->>'bodyType' else 'n/a' end as cab,
  coalesce(j->>'name', 'n/a') as box_length,
--   b->>'boxStyle' as box_style, -- box style is not size
  f->>'name' as eng_disp,
  g->>'nameNoBrand' as cylinders,
  coalesce(h->>'name', l->>'name', 'unknown') as fuel, 
--   l->>'name' as fuel_2,
  coalesce(i->>'name', 'unknown') as transmission,
  c.exterior as ext_color, -- c->>'genericDesc' as generic_ext_color, no longer have the generics, hmm maybe should have included them
  coalesce(c.interior, 'unknown') as int_color, -- d->>'genericDesc' as generic_int_color,
  (b->>'doors')::integer as doors, -- also features id 12175
  (a.response->'result'->>'buildMSRP')::numeric::integer as build_msrp, (b->>'baseMSRP')::numeric::integer as base_msrp,
  b->>'styleDescription' as style_description,
  a.response->'result'->>'source' as source,
  b->>'styleId' as style_id
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
-- join arkona.ext_inpmast bb on a.vin = bb.inpmast_vin
--   and bb.inpmast_company_number = 'RY1'
--   and b->>'styleId' = bb.key_to_cap_explosion_data::citext
join cvd.colors c on a.vin = c.vin
left join jsonb_array_elements(a.response->'result'->'techSpecs') f on f->>'id' = '10120' -- engine displ
left join jsonb_array_elements(a.response->'result'->'techSpecs') g on g->>'description' = 'Engine Cylinders'
left join jsonb_array_elements(a.response->'result'->'features') h on h->>'id' = '10030' -- fuel
left join jsonb_array_elements(a.response->'result'->'features') i on i->>'id' = '25190' -- transmission
left join jsonb_array_elements(a.response->'result'->'techSpecs') j on j->>'id' = '18090' -- box length
left join jsonb_array_elements_text(b->'segment') k on true
left join jsonb_array_elements(a.response->'result'->'techSpecs') l on  l->>'id' = '22360' -- fuel
where not exists (
    select 1
    from cvd.vehicles
    where vin = a.vin)
order by a.vin        


-- managed to wheedle out 8 more rows
-- 1/7/24 0 rows
insert into cvd.vehicles
select a.*
from cvd.z_wtf a
join arkona.ext_inpmast b on a.vin = b.inpmast_vin
	and a.style_id = b.key_to_cap_explosion_data::text
where vin in (
	select vin 
	from (
		select a.*
		from cvd.z_wtf a
		join arkona.ext_inpmast b on a.vin = b.inpmast_vin
			and a.style_id = b.key_to_cap_explosion_data::text) x 
	group by vin
	having count(*) = 1)

--------------------------------------------------------------------------------------------------------------------------
--< CASE 5 not exists in cvd.vehicles a few rows with help from inpmast
--------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------
--< CASE 6 not exists in cvd.vehicles a few more rows with help from inpmast and tool
--------------------------------------------------------------------------------------------------------------------------
/*
 ************************* pay attention ***********************************
 the vast majority of these that i generated already exist in cvd.unresolved_vins



*/

select count(*) -- 18864
from cvd.vin_descriptions
where the_date < '12/13/2023'

select the_date, count(*)
from cvd.vin_descriptions
group by the_date
order by the_date

select count(*) -- 18547
from cvd.vehicles

select vin  -- 1/7/24:320
from cvd.vin_descriptions a
where not exists (
    select 1
    from cvd.vehicles
    where vin = a.vin)
and a.the_date = current_date - 1

as of this reading ( < 12/13/23, when current was added to cvd.vin_descriptions)
1.68% of the vins are not parseable

drop table if exists cvd.z_wtf cascade;   
create unlogged table cvd.z_wtf as 
select distinct a.vin, (a.response->'result'->>'year')::integer as model_year, a.response->'result'->>'make' as make, 
  a.response->'result'->>'model' as model, b->>'trim' as trim_level, 
  case when coalesce(length(b->>'driveType'), 0) = 0 then 'unknown' else b->>'driveType' end as drive,
  coalesce(b->>'mfrModelCode', 'unknown') as model_code,
  case 
		when b->>'bodyType' like '%Cab%' or b->>'bodyType' like '%Crew%' 
			then 'Pickup' 
		else b->>'bodyType' 
	end as body_type, -- problem, doesn't break out cab type separate from body type, so, no body type of pickup !?!?! Toyota has some as crew max
  k as segment,
  case when b->>'bodyType' like '%Cab%' then b->>'bodyType' else 'n/a' end as cab,
  coalesce(j->>'name', 'n/a') as box_length,
--   b->>'boxStyle' as box_style, -- box style is not size
  f->>'name' as eng_disp,
  g->>'nameNoBrand' as cylinders,
  coalesce(h->>'name', l->>'name', 'unknown') as fuel, 
--   l->>'name' as fuel_2,
  coalesce(i->>'name', 'unknown') as transmission,
  c.exterior as ext_color, -- c->>'genericDesc' as generic_ext_color, no longer have the generics, hmm maybe should have included them
  coalesce(c.interior, 'unknown') as int_color, -- d->>'genericDesc' as generic_int_color,
  (b->>'doors')::integer as doors, -- also features id 12175
  (a.response->'result'->>'buildMSRP')::numeric::integer as build_msrp, (b->>'baseMSRP')::numeric::integer as base_msrp,
  b->>'styleDescription' as style_description,
  a.response->'result'->>'source' as source,
  b->>'styleId' as style_id
  
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
join cvd.colors c on a.vin = c.vin
left join jsonb_array_elements(a.response->'result'->'techSpecs') f on f->>'id' = '10120' -- engine displ
left join jsonb_array_elements(a.response->'result'->'techSpecs') g on g->>'description' = 'Engine Cylinders'
left join jsonb_array_elements(a.response->'result'->'features') h on h->>'id' = '10030' -- fuel
left join jsonb_array_elements(a.response->'result'->'features') i on i->>'id' = '25190' -- transmission
left join jsonb_array_elements(a.response->'result'->'techSpecs') j on j->>'id' = '18090' -- box length
left join jsonb_array_elements_text(b->'segment') k on true
left join jsonb_array_elements(a.response->'result'->'techSpecs') l on  l->>'id' = '22360' -- fuel
where not exists (
    select 1
    from cvd.vehicles
    where vin = a.vin)
and not exists (
    select 1
    from cvd.unresolved_vins
    where vin = a.vin)    
  and the_date = current_date - 1 -- ***********************************************************************************************
order by a.vin;

create index on cvd.z_wtf(vin);
create index on cvd.z_wtf(style_description);


1
select count(*) from cvd.z_wtf  --909

select vin  -- none, of course
from cvd.z_wtf
group by vin
having count(*) = 1

-- tool trim
-- insert 21 more
insert into cvd.vehicles
select e.*  -- 21
from cvd.z_wtf e
join ads.ext_vehicle_items f on e.trim_level = f.trim::text
  and e.vin = f.vin
where e.vin in (
	select vin from (
	select a.*, aa.trim
		from cvd.z_wtf a
		join ads.ext_vehicle_items aa on a.vin = aa.vin
			and a.trim_level = aa.trim::text) x
	group by vin
	having count(*) = 1)

select count(distinct vin) from cvd.z_wtf  -- 318

-- a bunch more with a more generalized use of inpmast.body_style
select a.*, b.body_style, b.key_to_cap_explosion_data, b.model_code, c.trim, c.engine
from cvd.z_wtf a
left join arkona.ext_inpmast b on a.vin = b.inpmast_vin
  and b.inpmast_company_number = 'RY1'
left join ads.ext_vehicle_items c on a.vin = c.vin
-- where vin not in ('1B7FL26X5XS271894','1B7GG22Y4WS582609','1B7GG23Y9VS190836','1B7HF13Z3VJ500950')
where not exists (select 1 from cvd.vehicles where vin = a.vin)
  and b.body_style is not null
--   and b.key_to_cap_explosion_data is not null
order by vin, style_id

1/7/24 looks like very few vins are going to be salvaged this time

select model_year, make, count(*)
from (
	select vin, model_year, make
	from cvd.z_wtf
	group by vin,model_year, make) a
group by model_year, make
order by count(*) desc

-- ****************** regenerate cvd.z_wtf after each insert ********************************
-- bunch more vins using inpmast body_style
-- 1/7/24: only able to salvage 10 vins
insert into cvd.vehicles

select distinct vin,model_year,make,model,trim_level,drive,model_code,body_type,
	segment,cab,box_length,eng_disp,cylinders,fuel,'unknown',ext_color,
	int_color,doors,build_msrp,base_msrp,style_description,source,style_id 
from cvd.z_wtf
where vin = '1FTRF17W72NA49496'
and style_id = '10320' and box_length = '97"'
union
select distinct vin,model_year,make,model,trim_level,drive,model_code,body_type,
	segment,cab,box_length,eng_disp,cylinders,fuel,'unknown',ext_color,
	int_color,doors,build_msrp,base_msrp,style_description,source,style_id 
from cvd.z_wtf
where vin = '1FTZR45E73PB47081' -- and eng_disp = '4.6 L'
and style_id = '100723' 
union
select distinct vin,model_year,make,model,trim_level,drive,model_code,body_type,
segment,cab,box_length,eng_disp,cylinders,fuel,transmission,ext_color,
int_color,doors,build_msrp,base_msrp,style_description,source,style_id 
from cvd.z_wtf
where vin = '1GNFK130X7J309556'
and style_description = '4WD 4dr 1500 LTZ' 
union
select distinct vin,model_year,make,model,trim_level,drive,model_code,body_type,
	segment,cab,box_length,eng_disp,cylinders,fuel,transmission,ext_color,
	int_color,doors,build_msrp,base_msrp,style_description,source,style_id 
from cvd.z_wtf
where vin = 'JHMFA36248S017299' and style_description = '4dr Sdn'
union
select distinct vin,model_year,make,model,trim_level,drive,model_code,body_type,
	segment,cab,box_length,eng_disp,cylinders,fuel,transmission,ext_color,
	int_color,doors,build_msrp,base_msrp,style_description,source,style_id 
from cvd.z_wtf
where vin = 'JHLRE487X7C041457'
and style_description = '4WD 5dr EX-L' 
union
select distinct vin,model_year,make,model,trim_level,drive,model_code,body_type,
	segment,cab,box_length,eng_disp,cylinders,fuel,transmission,ext_color,
	int_color,doors,build_msrp,base_msrp,style_description,source,style_id 
from cvd.z_wtf
where vin = 'JHLRE48757C034612'
and style_description = '4WD 5dr EX-L'
union
select distinct vin,model_year,make,model,trim_level,drive,model_code,body_type,
	segment,cab,box_length,eng_disp,cylinders,fuel,transmission,ext_color,
	int_color,doors,build_msrp,base_msrp,style_description,source,style_id 
from cvd.z_wtf
where vin = '5N1AR2MM9FC682852'
and style_description = '4WD 4dr SV'
union
select distinct vin,model_year,make,model,trim_level,drive,model_code,body_type,
	segment,cab,box_length,eng_disp,cylinders,fuel,transmission,ext_color,
	int_color,doors,build_msrp,base_msrp,style_description,source,style_id 
from cvd.z_wtf
where vin = '5J6TF2H52AL002298'
and style_description = '4WD 5dr EX-L'
union
select distinct vin,model_year,make,model,trim_level,drive,model_code,body_type,
	segment,cab,box_length,eng_disp,cylinders,fuel,transmission,ext_color,
	int_color,doors,build_msrp,base_msrp,style_description,source,style_id 
from cvd.z_wtf
where vin = '5FPYK16599B105549'
and style_description = '4WD Crew Cab RTL'
union
select distinct vin,model_year,make,model,trim_level,drive,model_code,body_type,
	segment,cab,box_length,eng_disp,cylinders,fuel,transmission,ext_color,
	int_color,doors,build_msrp,base_msrp,style_description,source,style_id 
from cvd.z_wtf
where vin = '1HGCP2F87BA104713'
and style_description = '4dr I4 Auto EX-L' 


union
select distinct vin,model_year,make,model,trim_level,drive,model_code,body_type,
	segment,cab,box_length,eng_disp,cylinders,fuel,'unknown',ext_color,
	int_color,doors,build_msrp,base_msrp,style_description,source,style_id 
from cvd.z_wtf
where vin = ''
and style_id = '279747'
union
select distinct vin,model_year,make,model,trim_level,drive,model_code,body_type,
	segment,cab,box_length,eng_disp,cylinders,fuel,'unknown',ext_color,
	int_color,doors,build_msrp,base_msrp,style_description,source,style_id 
from cvd.z_wtf
where vin = ''
and style_id = '354271'
union
select distinct vin,model_year,make,model,trim_level,drive,model_code,body_type,
	segment,cab,'unknown',eng_disp,cylinders,fuel,transmission,ext_color,
	int_color,doors,build_msrp,base_msrp,style_description,source,style_id 
from cvd.z_wtf
where vin = ''
and style_id = '284993'
union
select distinct vin,model_year,make,model,trim_level,drive,model_code,body_type,
	segment,cab,'unknown',eng_disp,cylinders,fuel,transmission,ext_color,
	int_color,doors,build_msrp,base_msrp,style_description,source,style_id 
from cvd.z_wtf
where vin = ''
and style_id = '284993'
union
select distinct vin,model_year,make,model,trim_level,drive,model_code,body_type,
	segment,cab,box_length,eng_disp,cylinders,fuel,'unknown',ext_color,
	int_color,doors,build_msrp,base_msrp,style_description,source,style_id
from cvd.z_wtf
where vin = ''
and style_id = '102488'
union
select distinct vin,model_year,make,model,trim_level,drive,model_code,body_type,
	segment,cab,box_length,eng_disp,cylinders,fuel,'unknown',ext_color,
	int_color,doors,build_msrp,base_msrp,style_description,source,style_id
from cvd.z_wtf
where vin = ''
and style_id = '300005'
union
select * from cvd.z_wtf
where vin = ''
and style_description = '4dr Sdn DX Manual' and Transmission = 'Manual'
union
select distinct vin,model_year,make,model,trim_level,drive,model_code,body_type,
	segment,cab,box_length,eng_disp,cylinders,fuel,'unknown',ext_color,
	int_color,doors,build_msrp,base_msrp,style_description,source,style_id
from cvd.z_wtf
where vin = ''
and style_id = '354989'
union
select distinct vin,model_year,make,model,trim_level,drive,model_code,body_type,
	segment,cab,box_length,eng_disp,cylinders,fuel,'unknown',ext_color,
	int_color,doors,build_msrp,base_msrp,style_description,source,style_id
from cvd.z_wtf
where vin = ''
and style_id = '307511'
union
select distinct vin,model_year,make,model,trim_level,drive,model_code,body_type,
	segment,cab,box_length,eng_disp,cylinders,fuel,'unknown',ext_color,
	int_color,doors,build_msrp,base_msrp,style_description,source,style_id
from cvd.z_wtf
where vin = ''
and style_id = '361732'
union
select distinct vin,model_year,make,model,trim_level,drive,model_code,body_type,
	segment,cab,box_length,eng_disp,cylinders,fuel,'unknown',ext_color,
	int_color,doors,build_msrp,base_msrp,style_description,source,style_id 
from cvd.z_wtf
where vin = ''
and style_id = '268029'



--------------------------------------------------------------------------------------------------------------------------
--/> CASE 6 not exists in cvd.vehicles a few more rows with help from inpmast and tool
--------------------------------------------------------------------------------------------------------------------------

It is time to start making the arguement about daily inventory maintenance
a lot of it can be automated
a significant proportion can not, and will have to have manual intervention
i am doing it for the catch upp/development phase, i am not going to do it daily
and if it does not get done, the foundation of the inventory system is fucked

what do we do with the 317 vins that can not be defined to a single row?
they decode in chrome, just not able to parse the data to a single row
Dealertrack and the Tool have already been used where they have info that helps

start looking at current inventory

select count(*) from cvd.vin_descriptions where the_date < '12/13/2023'
select count(*) from cvd.vehicles

18,864 vin_descriptions
18,547 vehicles

select vin, source, style_count  --317
from cvd.vin_descriptions a
where not exists (
  select 1
  from cvd.vehicles
  where vin = a.vin)
and a.the_date < '12/13/2023'  
order by style_count

-- bad vins are deleted from cvd.vin_descriptions
select *
from cvd.vin_descriptions a
where exists (
  select 1
  from cvd.bad_vins
  where vin = a.vin)


CREATE TABLE cvd.unresolved_vins
(
  vin citext NOT NULL,
  response jsonb,
  style_count integer,
  source citext,
  the_date date,
  CONSTRAINT unresolved_vins_pkey PRIMARY KEY (vin));
COMMENT ON TABLE cvd.unresolved_vins
  IS 'these are records, moved from cvd.vin_descriptions, that we are unable to parse to a single row per vin,
    that is these vins resolve to multiple chrome style_ids.
    these vins are not put into cvd.vehcles. currently, 12/20/23, it is ~ 1.7% of the total vins';

-- ***** on 12/13/23 422 vehicles from current inventory were added to cvd.vin_descriptions *****

-- 1/7/24: 310 unresolved vins
insert into cvd.unresolved_vins(vin,response,style_count,source,the_date) 
select a.vin, a.response, 
  a.style_count, a.source, a.the_date
-- select count(*)
from cvd.vin_descriptions a  
where the_date = current_date - 1 -- *************************************************************
	and not exists (
		select 1
		from cvd.vehicles
		where vin = a.vin)
	and not exists (
		select 1
		from cvd.unresolved_vins
		where vin = a.vin)		

-- delete from cvd.colors
-- 1/7/24 310 vins
delete 
-- select vin
from cvd.colors
where vin in (
	select a.vin
	from cvd.vin_descriptions a  
	where the_date = current_date - 1 -- *************************************************************
		and not exists (
			select 1
			from cvd.vehicles
			where vin = a.vin));
			
-- delete from cvd.vin_descriptions			
delete 
-- select count(*)
from cvd.vin_descriptions a
where vin in (
	select a.vin
	from cvd.vin_descriptions a  
	where the_date = current_date - 1 -- *************************************************************
		and not exists (
			select 1
			from cvd.vehicles
			where vin = a.vin));

select * from cvd.vehicles where vin in 
delete from cvd.unresolved_vins where vin in (
select vin
from cvd.vin_descriptions a
where the_date = current_date - 1 -- *************************************************************
and exists (
  select 1
  from cvd.unresolved_vins
  where vin = a.vin))
  
select vin
from cvd.vin_descriptions a
where not exists (
  select 1
  from cvd.vehicles
  where vin = a.vin)

select count(*) from cvd.vehicles  -- 19123
select count(*) from cvd.vin_descriptions c -- 19123
select count(*) from cvd.unresolved_vins  -- 308
select count(*) from cvd.bad_vins -- 29

select 337.0/(19123 + 337)  -- 1.732%