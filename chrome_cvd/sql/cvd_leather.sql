﻿select *
from jon.tmp_cvd

select * 
from jon.tmp_cvd
where style_id = '427216'

select * from chr.cvd where vin = '2GCUDEED6P1102772'

-- cad
select * from chr.cvd where vin = '1G6DV5RW7R0111183'


gmc 3500hd denali
select * from chr.cvd where vin = '1GT49YEY2RF268742'


-- these 3 feature ids don't work for a 24 silverado 1500, the vehicle has a leather package, which doesn't break out where the leather is

select a.vin, a.response->'result'->>'year' as model_year, a.response->'result'->>'make' as make, 
  a.response->'result'->>'model' as model, bb->>'trim' as trim_level, 
  c->>'isStandard', c->>'installCause', b->>'id', b->>'sectionName', b->>'name'
from chr.cvd a
join jsonb_array_elements(a.response->'result'->'vehicles') bb on true
left join jsonb_array_elements(a.response->'result'->'features') b on b->>'id' in ('14380','16830','14200')
left join jsonb_array_elements(b->'styles') c on true
where a.source = 'B' 
  and a.style_count = 1
  and (a.response->'result'->>'year')::integer > 2022
limit 100  


{"modelYearId":35452,"year":2023,"makeCode":"CH","description":"Silverado 1500"}  