﻿-- cu.tmp_vehicles not in veh.shape_size_classifications
select * 
from cu.tmp_vehicles a
where not exists (
  select 1
  from veh.shape_size_classifications
  where model = a.model and make = a.make)

-- cvd.vehicles not in veh.shape_size_classifications
--  !!! 780 make/model that do not match
select a.vin
from cvd.vehicles a
where not exists (
  select 1
  from veh.Shape_size_classifications
  where model = a.model and make = a.make)

-- and reverse it  677 non matching
select make, model
from veh.shape_size_classifications a
where not exists (
  select 1
  from cvd.vehicles 
  where model = a.model 
  and make = a.make)


select make, model, count(*) 
from cvd.vehicles a
where not exists (
  select 1
  from veh.shape_size_classifications
  where model = a.model and make = a.make)
group by make, model  


-- bmw model differences between ads and cvd
select *
from (
select vin, model_year, make, model from cu.tmp_vehicles where make = 'bmw') a
full outer join (
select vin, model_year, make, model from cvd.vehicles where make = 'bmw') b on a.vin = b.vin
where a.model <> b.model
order by a.vin

-- chev model differences between ads and cvd
select *
from (
select vin, model_year, make, model from cu.tmp_vehicles where make = 'chevrolet') a
full outer join (
select vin, model_year, make, model from cvd.vehicles where make = 'chevrolet') b on a.vin = b.vin
where a.model <> b.model
order by a.vin

-- gmc model differences between ads and cvd
select *
from (
select vin, model_year, make, model from cu.tmp_vehicles where make = 'gmc') a
full outer join (
select vin, model_year, make, model from cvd.vehicles where make = 'gmc') b on a.vin = b.vin
where a.model <> b.model
order by a.vin

-- full outer join on model_year/make/model
select *
from (
select distinct model_year, make, model from cu.tmp_vehicles) a
full outer join (
select distinct model_year, make, model from cvd.vehicles) b on a.model_year::integer = b.model_year
  and a.make = b.make
  and a.model = b.model
order by a.model_year, a.make, a.model, b.model_year, b.make, b.model


-- match vins models don't match
select *
from (
  select vin, model_year, make, model from cu.tmp_vehicles) a
join (
  select vin, model_year, make, model from cvd.vehicles) b on a.vin = b.vin  
where a.model <> b.model  
order by a.make, a.model



select distinct a.make as ads_make, a.model as ads_model, 
  b.make as cvd_make, b.model as cvd_model
from (
  select vin, model_year, make, model from cu.tmp_vehicles) a
join (
  select vin, model_year, make, model from cvd.vehicles) b on a.vin = b.vin  
where a.model <> b.model  
order by a.make, a.model


select * from cvd.vehicles limit 100