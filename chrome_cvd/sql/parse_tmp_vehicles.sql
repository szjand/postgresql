﻿TABLE cu.tmp_vehicles
  vin citext NOT NULL,
  model_year citext NOT NULL,
  make citext NOT NULL,
--   subdivision citext,
  model citext NOT NULL,
  trim_level citext,
  drive citext,  -- Integration Guide p25, recommends using feature id 10750, but i prefer the shorter designation in vehicles
--   fleet_only boolean NOT NULL,
--   model_fleet boolean NOT NULL,
  model_code citext,
  cab citext,  -- problem, doesn't break out cap type separate from body type, so, no body type of pickup !?!?!
  box_size citext,
  engine_disp citext,
  cylinders citext,
  fuel citext,
  transmission citext,
  ext_color citext,
  int_color citext,
  pass_doors citext,
  msrp integer,
--   market_class citext, field does not exist in cvd
  body_type citext,  -- confusion with cab vs body type
--   alt_body_type citext,
  style_name citext,  -- styleDescription
--   alt_style_name citext,
--   name_wo_trim citext,
--   best_make_name citext,
--   best_model_name citext,
--   best_trim_name citext,
--   best_style_name citext,
--   pic_url citext,
  source citext,
  chrome_style_id citext,
  CONSTRAINT tmp_vehicles_pkey PRIMARY KEY (vin)

-- build data by year,make,model
drop table if exists the_list;
create temp table the_list as
select a.response->'result'->>'year' as model_year, a.response->'result'->>'make' as make, a.response->'result'->>'model' as model, count(*), min(vin), max(vin)
from cvd.vin_descriptions a
where source = 'B'
  and style_count = 1
group by a.response->'result'->>'year', a.response->'result'->>'make', a.response->'result'->>'model' 
order by a.response->'result'->>'year', a.response->'result'->>'make', a.response->'result'->>'model' 



-- 11/28/23 now that we have ford and toyota
-- ford build data starts at 2010
-- toyota starts at 1996
  2GCUDEED6P1102772: 23 silverado 1500

drop table if exists wtf cascade;
create temp table wtf as
-- 13969 rows of the 16589 in chr.cvd
select a.vin, a.response->'result'->>'year' as model_year, a.response->'result'->>'make' as make, 
  a.response->'result'->>'model' as model, b->>'trim' as trim_level, 
  b->>'driveType' as drive,
--   e->>'name' as drive,
  b->>'mfrModelCode' as model_code,
  case 
		when b->>'bodyType' like '%Cab%' or b->>'bodyType' like '%Crew%' 
			then 'Pickup' 
		else b->>'bodyType' 
	end as body_type, -- problem, doesn't break out cab type separate from body type, so, no body type of pickup !?!?! Toyota has some as crew max
  k as segment,
  case when b->>'bodyType' like '%Cab%' then b->>'bodyType' else 'n/a' end as cab,
  coalesce(j->>'name', 'n/a') as box_length,
--   b->>'boxStyle' as box_style, -- box style is not size
  f->>'name' as eng_disp,
  g->>'nameNoBrand' as cylinders,
  coalesce(h->>'name', l->>'name', 'unknown') as fuel, 
--   l->>'name' as fuel_2,
  i->>'name' as transmission,
  c.exterior as ext_color, -- c->>'genericDesc' as generic_ext_color, no longer have the generics, hmm maybe should have included them
  c.interior as int_color, -- d->>'genericDesc' as generic_int_color,
  b->>'doors' as doors, -- also features id 12175
  a.response->'result'->>'buildMSRP' as build_msrp, b->>'baseMSRP' as base_msrp,
  b->>'styleDescription' as style_description,
  a.response->'result'->>'source' as source,
  b->>'styleId' as style_id
--   a.response->'result'->>'source' as source, a.response->'result'->>'validVin' as valid_vin,
--   b->'doors' as doors,
--   b->'segment'->>0 as segment, b->>'styleId' as style_id, b->>'baseMSRP' as base_msrp, 
--    b->>'bodyType' as body_type, 
--   
--   b->>'driveType' as drive,
--   b->>'wheelbase' as wheelbase, b->>'styleDescription' as style_description,
--   c->>'description' as ext_color, d->>'description' as int_color,
--   a.response->'result'->>'vinProcessed' = a.response->'result'->>'vinSubmitted'
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
join cvd.colors c on a.vin = c.vin
left join jsonb_array_elements(a.response->'result'->'techSpecs') f on f->>'id' = '10120'
left join jsonb_array_elements(a.response->'result'->'techSpecs') g on g->>'description' = 'Engine Cylinders'
left join jsonb_array_elements(a.response->'result'->'features') h on h->>'id' = '10030'
left join jsonb_array_elements(a.response->'result'->'features') i on i->>'id' = '25190'
left join jsonb_array_elements(a.response->'result'->'techSpecs') j on j->>'id' = '18090'
left join jsonb_array_elements_text(b->'segment') k on true
left join jsonb_array_elements(a.response->'result'->'techSpecs') l on  l->>'id' = '22360'
where source = 'B' 
  and style_count = 1
--   and (a.response->'result'->>'year')::integer > 2018
order by a.response->'result'->>'make', a.response->'result'->>'model', a.response->'result'->>'year'


select a.*
from wtf a
order by right(a.vin, 5)
limit 20


select *
from wtf 
order by right(vin, 6)
limit 500

--------------------------------------------------------------------------------------------------------------------------
--< CASE 1 source = B style_count = 1
--------------------------------------------------------------------------------------------------------------------------

drop table if exists cvd.vehicles cascade;
create table cvd.vehicles(
  vin citext primary key references cvd.vin_descriptions(vin),
  model_year integer not null,
  make citext not null,
  model citext not null,
  trim_level citext,
  drive citext not null,
  model_code citext not null,
  body_type citext not null,
  segment citext,
  cab citext not null,
  box_length citext not null,
  eng_disp citext,
  cylinders citext,
  fuel citext not null,
  transmission citext not null,
  ext_color citext not null,
  int_color citext not null,
  doors integer,
  build_msrp integer,
  base_msrp integer,
  style_desription citext not null,
  source citext not null,
  style_id citext not null);
  
insert into cvd.vehicles
select a.vin, (a.response->'result'->>'year')::integer as model_year, a.response->'result'->>'make' as make, 
  a.response->'result'->>'model' as model, b->>'trim' as trim_level, 
  b->>'driveType' as drive,
  b->>'mfrModelCode' as model_code,
  case 
		when b->>'bodyType' like '%Cab%' or b->>'bodyType' like '%Crew%' 
			then 'Pickup' 
		else b->>'bodyType' 
	end as body_type, -- problem, doesn't break out cab type separate from body type, so, no body type of pickup !?!?! Toyota has some as crew max
  k as segment,
  case when b->>'bodyType' like '%Cab%' then b->>'bodyType' else 'n/a' end as cab,
  coalesce(j->>'name', 'n/a') as box_length,
--   b->>'boxStyle' as box_style, -- box style is not size
  f->>'name' as eng_disp,
  g->>'nameNoBrand' as cylinders,
  coalesce(h->>'name', l->>'name', 'unknown') as fuel, 
--   l->>'name' as fuel_2,
  i->>'name' as transmission,
  c.exterior as ext_color, -- c->>'genericDesc' as generic_ext_color, no longer have the generics, hmm maybe should have included them
  coalesce(c.interior, 'unknown') as int_color, -- d->>'genericDesc' as generic_int_color,
  (b->>'doors')::integer as doors, -- also features id 12175
  (a.response->'result'->>'buildMSRP')::numeric::integer as build_msrp, (b->>'baseMSRP')::numeric::integer as base_msrp,
  b->>'styleDescription' as style_description,
  a.response->'result'->>'source' as source,
  b->>'styleId' as style_id
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
join cvd.colors c on a.vin = c.vin
left join jsonb_array_elements(a.response->'result'->'techSpecs') f on f->>'id' = '10120'
left join jsonb_array_elements(a.response->'result'->'techSpecs') g on g->>'description' = 'Engine Cylinders'
left join jsonb_array_elements(a.response->'result'->'features') h on h->>'id' = '10030'
left join jsonb_array_elements(a.response->'result'->'features') i on i->>'id' = '25190'
left join jsonb_array_elements(a.response->'result'->'techSpecs') j on j->>'id' = '18090'
left join jsonb_array_elements_text(b->'segment') k on true
left join jsonb_array_elements(a.response->'result'->'techSpecs') l on  l->>'id' = '22360'
where source = 'B' 
  and style_count = 1  


-- body type -------------------------------------------------------------------------
-- oddball toyota, crew max body type
select body_type, count(*), min(vin), max(vin)
from wtf
group by body_type
order by body_type

-- segment --------------------------------------------------------------------------
-- in the source = B and style_count = 1, are there any with multiple segments?
-- nope
select vin
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
where jsonb_array_length(b->'segment') > 1

-- fuel -----------------------------------------------------------------------------
-- 42 with null fuel, most are 2012 equinox
select fuel, fuel_2, count(*)
from wtf
group by fuel, fuel_2
order by fuel

select *
from wtf 
where fuel is null
  and fuel_2 is null

--------------------------------------------------------------------------------------------------------------------------
--/> CASE 1 source = B style_count = 1
--------------------------------------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------------------------------------
--< CASE 2 source <> B style_count = 1
--------------------------------------------------------------------------------------------------------------------------
/*
	for all those where b->>'driveType' resolves to unknown, feature 10750 is also missing
  typo in my where clause resulted in entire batch with drive = unknown

delete 
from cvd.vehicles
where vin in (
	select vin  --1902
	from cvd.vin_descriptions a
	where source <> 'B'
		and style_count = 1);
*/
  
select vin  --1902
from cvd.vin_descriptions a
where source <> 'B'
  and style_count = 1
  and not exists (
    select 1
    from cvd.vehicles
    where vin = a.vin)
  

drop table if exists wtf;
create temp table wtf as 
-- insert into cvd.vehicles
select a.vin, (a.response->'result'->>'year')::integer as model_year, a.response->'result'->>'make' as make, 
  a.response->'result'->>'model' as model, b->>'trim' as trim_level, 
  case when coalesce(length(b->>'driveType'), 0) = 0 then 'unknown' else b->>'driveType' end as drive,
  coalesce(b->>'mfrModelCode', 'unknown') as model_code,
  case 
		when b->>'bodyType' like '%Cab%' or b->>'bodyType' like '%Crew%' 
			then 'Pickup' 
		else b->>'bodyType' 
	end as body_type, -- problem, doesn't break out cab type separate from body type, so, no body type of pickup !?!?! Toyota has some as crew max
  k as segment,
  case when b->>'bodyType' like '%Cab%' then b->>'bodyType' else 'n/a' end as cab,
  coalesce(j->>'name', 'n/a') as box_length,
--   b->>'boxStyle' as box_style, -- box style is not size
  f->>'name' as eng_disp,
  g->>'nameNoBrand' as cylinders,
  coalesce(h->>'name', l->>'name', 'unknown') as fuel, 
--   l->>'name' as fuel_2,
  coalesce(i->>'name', 'unknown') as transmission,
  c.exterior as ext_color, -- c->>'genericDesc' as generic_ext_color, no longer have the generics, hmm maybe should have included them
  coalesce(c.interior, 'unknown') as int_color, -- d->>'genericDesc' as generic_int_color,
  (b->>'doors')::integer as doors, -- also features id 12175
  (a.response->'result'->>'buildMSRP')::numeric::integer as build_msrp, (b->>'baseMSRP')::numeric::integer as base_msrp,
  b->>'styleDescription' as style_description,
  a.response->'result'->>'source' as source,
  b->>'styleId' as style_id
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
join cvd.colors c on a.vin = c.vin
left join jsonb_array_elements(a.response->'result'->'techSpecs') f on f->>'id' = '10120'
left join jsonb_array_elements(a.response->'result'->'techSpecs') g on g->>'description' = 'Engine Cylinders'
left join jsonb_array_elements(a.response->'result'->'features') h on h->>'id' = '10030'
left join jsonb_array_elements(a.response->'result'->'features') i on i->>'id' = '25190'
left join jsonb_array_elements(a.response->'result'->'techSpecs') j on j->>'id' = '18090'
left join jsonb_array_elements_text(b->'segment') k on true
left join jsonb_array_elements(a.response->'result'->'techSpecs') l on  l->>'id' = '22360'
where source <> 'B' 
  and style_count = 1  

-- no dups
select vin from wtf group by vin having count(*) > 1

-- style_id is null, process in errors.sql, cvd.bad_vins
-- this vehicle is sparse and has no style_id, leaning towards making it a bad vin
select * from cvd.vin_descriptions where vin = '1FAFP5340XG108367'
-- it is the only Sparse record with no style_id
select a.*, b->>'styleId'
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
where a.source = 's'
  
select a.*, b->>'styleId'
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
where a.source = 's'
  and b->>'styleId' is null

select * from wtf where drive is null

select * from wtf order by source

select length(drive) from wtf where vin = '2G1FP22SXS2220353'

select * from cvd.vin_descriptions where vin = '2G1FP22SXS2220353'  

-- successfully inserted 1902 rows into cvd.vehicles
insert into cvd.vehicles
select * from wtf;
--------------------------------------------------------------------------------------------------------------------------
--/> CASE 2 source <> B style_count = 1
--------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------
--< CASE 3 source = B style_count <> 1
--------------------------------------------------------------------------------------------------------------------------

select *  
from cvd.vin_descriptions a
where source = 'B'
  and style_count <> 1
  and not exists (
    select 1
    from cvd.vehicles
    where vin = a.vin)

KL5JD66Z15K091508: 2005 Suzuki Reno 2 styles, one auto one automatic, ours is auto, style_id = 268817    
3MYDLBYV6JY326396: 2018 Toyota Yaris 2 styles, one auto one automatic, ours is auto, style_id = 393455
JTDBE32K420076396: 2002 Toyota Camry 5 styles, tool says base, trims avail are LE SE & XLE, go with LLE style_id = 1555

drop table if exists wtf;
create temp table wtf as 
-- insert into cvd.vehicles
select a.vin, (a.response->'result'->>'year')::integer as model_year, a.response->'result'->>'make' as make, 
  a.response->'result'->>'model' as model, b->>'trim' as trim_level, 
  'FWD' as drive,
  coalesce(b->>'mfrModelCode', 'unknown') as model_code,
  case 
		when b->>'bodyType' like '%Cab%' or b->>'bodyType' like '%Crew%' 
			then 'Pickup' 
		else b->>'bodyType' 
	end as body_type, -- problem, doesn't break out cab type separate from body type, so, no body type of pickup !?!?! Toyota has some as crew max
  k as segment,
  case when b->>'bodyType' like '%Cab%' then b->>'bodyType' else 'n/a' end as cab,
  coalesce(j->>'name', 'n/a') as box_length,
--   b->>'boxStyle' as box_style, -- box style is not size
  f->>'name' as eng_disp,
  g->>'nameNoBrand' as cylinders,
  coalesce(h->>'name', l->>'name', 'unknown') as fuel, 
--   l->>'name' as fuel_2,
  coalesce(i->>'name', 'unknown') as transmission,
  c.exterior as ext_color, -- c->>'genericDesc' as generic_ext_color, no longer have the generics, hmm maybe should have included them
  coalesce(c.interior, 'unknown') as int_color, -- d->>'genericDesc' as generic_int_color,
  (b->>'doors')::integer as doors, -- also features id 12175
  (a.response->'result'->>'buildMSRP')::numeric::integer as build_msrp, (b->>'baseMSRP')::numeric::integer as base_msrp,
  b->>'styleDescription' as style_description,
  a.response->'result'->>'source' as source,
  b->>'styleId' as style_id
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
  and 
    case
      when a.vin = 'KL5JD66Z15K091508' then b->>'styleId' = '268817'
      when a.vin = '3MYDLBYV6JY326396' then b->>'styleId' = '393455'
      when a.vin = 'JTDBE32K420076396' then b->>'styleId' = '1555'
    end
join cvd.colors c on a.vin = c.vin
left join jsonb_array_elements(a.response->'result'->'techSpecs') f on f->>'id' = '10120'
left join jsonb_array_elements(a.response->'result'->'techSpecs') g on g->>'description' = 'Engine Cylinders'
left join jsonb_array_elements(a.response->'result'->'features') h on h->>'id' = '10030'
left join jsonb_array_elements(a.response->'result'->'features') i on i->>'id' = '25190'
left join jsonb_array_elements(a.response->'result'->'techSpecs') j on j->>'id' = '18090'
left join jsonb_array_elements_text(b->'segment') k on true
left join jsonb_array_elements(a.response->'result'->'techSpecs') l on  l->>'id' = '22360'
where source = 'B' 
  and style_count <> 1  
  and a.vin in ('KL5JD66Z15K091508','3MYDLBYV6JY326396','JTDBE32K420076396')

update wtf set transmission = 'Automatic' where vin in ('JTDBE32K420076396', 'KL5JD66Z15K091508');
update wtf set transmission = 'Manual' where vin = '3MYDLBYV6JY326396';

-- successfully inserted 3 rows into cvd.vehicles
insert into cvd.vehicles
select * from wtf;

--------------------------------------------------------------------------------------------------------------------------
--/> CASE 3 source = B style_count <> 1
--------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------
--< CASE 4 source <> B style_count <> 1 and style_id exists in inpmast with no duplicate rows
--------------------------------------------------------------------------------------------------------------------------

select vin  -- 711
from cvd.vin_descriptions a
where source <> 'B'
  and style_count <> 1
  and not exists (
    select 1
    from cvd.vehicles
    where vin = a.vin)
  

drop table if exists wtf;
create temp table wtf as 
select a.vin, (a.response->'result'->>'year')::integer as model_year, a.response->'result'->>'make' as make, 
  a.response->'result'->>'model' as model, b->>'trim' as trim_level, 
  case when coalesce(length(b->>'driveType'), 0) = 0 then 'unknown' else b->>'driveType' end as drive,
  coalesce(b->>'mfrModelCode', 'unknown') as model_code,
  case 
		when b->>'bodyType' like '%Cab%' or b->>'bodyType' like '%Crew%' 
			then 'Pickup' 
		else b->>'bodyType' 
	end as body_type, -- problem, doesn't break out cab type separate from body type, so, no body type of pickup !?!?! Toyota has some as crew max
  k as segment,
  case when b->>'bodyType' like '%Cab%' then b->>'bodyType' else 'n/a' end as cab,
  coalesce(j->>'name', 'n/a') as box_length,
--   b->>'boxStyle' as box_style, -- box style is not size
  f->>'name' as eng_disp,
  g->>'nameNoBrand' as cylinders,
  coalesce(h->>'name', l->>'name', 'unknown') as fuel, 
--   l->>'name' as fuel_2,
  coalesce(i->>'name', 'unknown') as transmission,
  c.exterior as ext_color, -- c->>'genericDesc' as generic_ext_color, no longer have the generics, hmm maybe should have included them
  coalesce(c.interior, 'unknown') as int_color, -- d->>'genericDesc' as generic_int_color,
  (b->>'doors')::integer as doors, -- also features id 12175
  (a.response->'result'->>'buildMSRP')::numeric::integer as build_msrp, (b->>'baseMSRP')::numeric::integer as base_msrp,
  b->>'styleDescription' as style_description,
  a.response->'result'->>'source' as source,
  b->>'styleId' as style_id
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
join arkona.ext_inpmast bb on a.vin = bb.inpmast_vin
  and bb.inpmast_company_number = 'RY1'
  and b->>'styleId' = bb.key_to_cap_explosion_data::citext
join cvd.colors c on a.vin = c.vin
left join jsonb_array_elements(a.response->'result'->'techSpecs') f on f->>'id' = '10120' -- engine displ
left join jsonb_array_elements(a.response->'result'->'techSpecs') g on g->>'description' = 'Engine Cylinders'
left join jsonb_array_elements(a.response->'result'->'features') h on h->>'id' = '10030' -- fuel
left join jsonb_array_elements(a.response->'result'->'features') i on i->>'id' = '25190' -- transmission
left join jsonb_array_elements(a.response->'result'->'techSpecs') j on j->>'id' = '18090' -- box length
left join jsonb_array_elements_text(b->'segment') k on true
left join jsonb_array_elements(a.response->'result'->'techSpecs') l on  l->>'id' = '22360' -- fuel
where source <> 'B' 
  and style_count <> 1  
  and not exists (
    select 1
    from dups
    where vin = a.vin)
--   and a.vin = '1FMZU34E7WZA47562'
--   and b->>'styleId' = bb.key_to_cap_explosion_data::text

select * from wtf where style_id <> key_to_cap_explosion_data::text

-- a lot of piuckups have n/a for box_length, looks like it's just not in the data

select * from wtf where body_type = 'Pickup'

select * from cvd.vin_descriptions where vin = '1GCUYDEDXMZ200950'

-- successfully inserted 402 rows into cvd.vehicles
insert into cvd.vehicles
select * from wtf;


--------------------------------------------------------------------------------------------------------------------------
--/> CASE 4 source <> B style_count <> 1  with no duplicate rows
--------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------
--< CASE 5 not exists in cvd.vehicles a few rows with help from inpmast
--------------------------------------------------------------------------------------------------------------------------

select count(*) -- 16586
from cvd.vin_descriptions

select vin  -- 309
from cvd.vin_descriptions a
where source <> 'B'
  and style_count <> 1
  and not exists (
    select 1
    from cvd.vehicles
    where vin = a.vin)


drop table if exists wtf;
create temp table wtf as 
select distinct a.vin, (a.response->'result'->>'year')::integer as model_year, a.response->'result'->>'make' as make, 
  a.response->'result'->>'model' as model, b->>'trim' as trim_level, 
  case when coalesce(length(b->>'driveType'), 0) = 0 then 'unknown' else b->>'driveType' end as drive,
  coalesce(b->>'mfrModelCode', 'unknown') as model_code,
  case 
		when b->>'bodyType' like '%Cab%' or b->>'bodyType' like '%Crew%' 
			then 'Pickup' 
		else b->>'bodyType' 
	end as body_type, -- problem, doesn't break out cab type separate from body type, so, no body type of pickup !?!?! Toyota has some as crew max
  k as segment,
  case when b->>'bodyType' like '%Cab%' then b->>'bodyType' else 'n/a' end as cab,
  coalesce(j->>'name', 'n/a') as box_length,
--   b->>'boxStyle' as box_style, -- box style is not size
  f->>'name' as eng_disp,
  g->>'nameNoBrand' as cylinders,
  coalesce(h->>'name', l->>'name', 'unknown') as fuel, 
--   l->>'name' as fuel_2,
  coalesce(i->>'name', 'unknown') as transmission,
  c.exterior as ext_color, -- c->>'genericDesc' as generic_ext_color, no longer have the generics, hmm maybe should have included them
  coalesce(c.interior, 'unknown') as int_color, -- d->>'genericDesc' as generic_int_color,
  (b->>'doors')::integer as doors, -- also features id 12175
  (a.response->'result'->>'buildMSRP')::numeric::integer as build_msrp, (b->>'baseMSRP')::numeric::integer as base_msrp,
  b->>'styleDescription' as style_description,
  a.response->'result'->>'source' as source,
  b->>'styleId' as style_id
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
-- join arkona.ext_inpmast bb on a.vin = bb.inpmast_vin
--   and bb.inpmast_company_number = 'RY1'
--   and b->>'styleId' = bb.key_to_cap_explosion_data::citext
join cvd.colors c on a.vin = c.vin
left join jsonb_array_elements(a.response->'result'->'techSpecs') f on f->>'id' = '10120' -- engine displ
left join jsonb_array_elements(a.response->'result'->'techSpecs') g on g->>'description' = 'Engine Cylinders'
left join jsonb_array_elements(a.response->'result'->'features') h on h->>'id' = '10030' -- fuel
left join jsonb_array_elements(a.response->'result'->'features') i on i->>'id' = '25190' -- transmission
left join jsonb_array_elements(a.response->'result'->'techSpecs') j on j->>'id' = '18090' -- box length
left join jsonb_array_elements_text(b->'segment') k on true
left join jsonb_array_elements(a.response->'result'->'techSpecs') l on  l->>'id' = '22360' -- fuel
where not exists (
    select 1
    from cvd.vehicles
    where vin = a.vin)
order by a.vin    


select count(*) from wtf -- 2271, with distinct, 2024

-- none of the remaining vehicles resolve to a single row per vin
select a.*
from wtf a
join (
	select vin
	from wtf
	group by vin
	having count(*) = 1) b on a.vin = b.vin



select a.*
from wtf a
join arkona.ext_inpmast b on a.vin = b.inpmast_vin
	and a.style_id = b.key_to_cap_explosion_data::text
where vin in (
	select vin 
	from (
		select a.*
		from wtf a
		join arkona.ext_inpmast b on a.vin = b.inpmast_vin
			and a.style_id = b.key_to_cap_explosion_data::text) x 
	group by vin
	having count(*) = 1)

update wtf 
set cab = 'SuperCrew'
where vin in ('1FTEW1EG9FFB83324','1FTPW14V16FB68664','1FTPW14V97KC73607')

-- managed to wheedle out 7 more rows
insert into cvd.vehicles
select a.*
from wtf a
join arkona.ext_inpmast b on a.vin = b.inpmast_vin
	and a.style_id = b.key_to_cap_explosion_data::text
where vin in (
	select vin 
	from (
		select a.*
		from wtf a
		join arkona.ext_inpmast b on a.vin = b.inpmast_vin
			and a.style_id = b.key_to_cap_explosion_data::text) x 
	group by vin
	having count(*) = 1)

--------------------------------------------------------------------------------------------------------------------------
--/> CASE 5 not exists in cvd.vehicles a few rows with help from inpmast
--------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------
--< CASE 6 not exists in cvd.vehicles a few more rows with help from inpmast
--------------------------------------------------------------------------------------------------------------------------
select count(*) -- 16586
from cvd.vin_descriptions

select vin  -- 293
from cvd.vin_descriptions a
where not exists (
    select 1
    from cvd.vehicles
    where vin = a.vin)


drop table if exists wtf;
create temp table wtf as 
select distinct a.vin, (a.response->'result'->>'year')::integer as model_year, a.response->'result'->>'make' as make, 
  a.response->'result'->>'model' as model, b->>'trim' as trim_level, 
  case when coalesce(length(b->>'driveType'), 0) = 0 then 'unknown' else b->>'driveType' end as drive,
  coalesce(b->>'mfrModelCode', 'unknown') as model_code,
  case 
		when b->>'bodyType' like '%Cab%' or b->>'bodyType' like '%Crew%' 
			then 'Pickup' 
		else b->>'bodyType' 
	end as body_type, -- problem, doesn't break out cab type separate from body type, so, no body type of pickup !?!?! Toyota has some as crew max
  k as segment,
  case when b->>'bodyType' like '%Cab%' then b->>'bodyType' else 'n/a' end as cab,
  coalesce(j->>'name', 'n/a') as box_length,
--   b->>'boxStyle' as box_style, -- box style is not size
  f->>'name' as eng_disp,
  g->>'nameNoBrand' as cylinders,
  coalesce(h->>'name', l->>'name', 'unknown') as fuel, 
--   l->>'name' as fuel_2,
  coalesce(i->>'name', 'unknown') as transmission,
  c.exterior as ext_color, -- c->>'genericDesc' as generic_ext_color, no longer have the generics, hmm maybe should have included them
  coalesce(c.interior, 'unknown') as int_color, -- d->>'genericDesc' as generic_int_color,
  (b->>'doors')::integer as doors, -- also features id 12175
  (a.response->'result'->>'buildMSRP')::numeric::integer as build_msrp, (b->>'baseMSRP')::numeric::integer as base_msrp,
  b->>'styleDescription' as style_description,
  a.response->'result'->>'source' as source,
  b->>'styleId' as style_id
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
join cvd.colors c on a.vin = c.vin
left join jsonb_array_elements(a.response->'result'->'techSpecs') f on f->>'id' = '10120' -- engine displ
left join jsonb_array_elements(a.response->'result'->'techSpecs') g on g->>'description' = 'Engine Cylinders'
left join jsonb_array_elements(a.response->'result'->'features') h on h->>'id' = '10030' -- fuel
left join jsonb_array_elements(a.response->'result'->'features') i on i->>'id' = '25190' -- transmission
left join jsonb_array_elements(a.response->'result'->'techSpecs') j on j->>'id' = '18090' -- box length
left join jsonb_array_elements_text(b->'segment') k on true
left join jsonb_array_elements(a.response->'result'->'techSpecs') l on  l->>'id' = '22360' -- fuel
where not exists (
    select 1
    from cvd.vehicles
    where vin = a.vin)
order by a.vin 


-- managed to wheedle out 11 more rows, guessed at transmission on old F150s
insert into cvd.vehicles
select * from wtf 
where vin in ('1FTRX17L91NB54767','1FTRX18L03NB34618','1FTRX18L3YNA34410','1FTRX18L4XKA96527','1FTRX18LX3NA62858','1FTRX18LX3NA98694','1FTSW21P26EA38338','1FTSW21P75EB90419','1FTSW21R18EB23367','2FTRX18L41CA54497','2FTRX18W7YCA47225')
  and transmission = 'Automatic';

--------------------------------------------------------------------------------------------------------------------------
--/> CASE 6 not exists in cvd.vehicles a few more rows with help from inpmast
--------------------------------------------------------------------------------------------------------------------------  
select count(*) -- 16586
from cvd.vin_descriptions

select vin  -- 293
from cvd.vin_descriptions a
where not exists (
    select 1
    from cvd.vehicles
    where vin = a.vin)


drop table if exists wtf;
create temp table wtf as 
select distinct a.vin, (a.response->'result'->>'year')::integer as model_year, a.response->'result'->>'make' as make, 
  a.response->'result'->>'model' as model, b->>'trim' as trim_level, 
  case when coalesce(length(b->>'driveType'), 0) = 0 then 'unknown' else b->>'driveType' end as drive,
  coalesce(b->>'mfrModelCode', 'unknown') as model_code,
  case 
		when b->>'bodyType' like '%Cab%' or b->>'bodyType' like '%Crew%' 
			then 'Pickup' 
		else b->>'bodyType' 
	end as body_type, -- problem, doesn't break out cab type separate from body type, so, no body type of pickup !?!?! Toyota has some as crew max
  k as segment,
  case when b->>'bodyType' like '%Cab%' then b->>'bodyType' else 'n/a' end as cab,
  coalesce(j->>'name', 'n/a') as box_length,
--   b->>'boxStyle' as box_style, -- box style is not size
  f->>'name' as eng_disp,
  g->>'nameNoBrand' as cylinders,
  coalesce(h->>'name', l->>'name', 'unknown') as fuel, 
--   l->>'name' as fuel_2,
  coalesce(i->>'name', 'unknown') as transmission,
  c.exterior as ext_color, -- c->>'genericDesc' as generic_ext_color, no longer have the generics, hmm maybe should have included them
  coalesce(c.interior, 'unknown') as int_color, -- d->>'genericDesc' as generic_int_color,
  (b->>'doors')::integer as doors, -- also features id 12175
  (a.response->'result'->>'buildMSRP')::numeric::integer as build_msrp, (b->>'baseMSRP')::numeric::integer as base_msrp,
  b->>'styleDescription' as style_description,
  a.response->'result'->>'source' as source,
  b->>'styleId' as style_id
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
join cvd.colors c on a.vin = c.vin
left join jsonb_array_elements(a.response->'result'->'techSpecs') f on f->>'id' = '10120' -- engine displ
left join jsonb_array_elements(a.response->'result'->'techSpecs') g on g->>'description' = 'Engine Cylinders'
left join jsonb_array_elements(a.response->'result'->'features') h on h->>'id' = '10030' -- fuel
left join jsonb_array_elements(a.response->'result'->'features') i on i->>'id' = '25190' -- transmission
left join jsonb_array_elements(a.response->'result'->'techSpecs') j on j->>'id' = '18090' -- box length
left join jsonb_array_elements_text(b->'segment') k on true
left join jsonb_array_elements(a.response->'result'->'techSpecs') l on  l->>'id' = '22360' -- fuel
where not exists (
    select 1
    from cvd.vehicles
    where vin = a.vin)


select * from wtf order by vin

select * from wtf where make = 'honda' order by vin

------------------------------------------------------------------
-- 42 more based on tool trim
insert into cvd.vehicles
select e.*
from wtf e
join ads.ext_vehicle_items f on e.trim_level = f.trim::text
  and e.vin = f.vin
where e.vin in (
	select vin from (
	select a.*, aa.trim
		from wtf a
		join ads.ext_vehicle_items aa on a.vin = aa.vin
			and a.trim_level = aa.trim::text) x
	group by vin
	having count(*) = 1)
------------------------------------------------------------------

-- ****************************** regenerate wtf after each insert ************************************
select vin  -- 178
from cvd.vin_descriptions a
where not exists (
    select 1
    from cvd.vehicles
    where vin = a.vin)

select a.*, aa.body_style, aa.key_to_cap_explosion_data
from wtf a
join arkona.ext_inpmast aa on a.vin = aa.inpmast_vin
	and position(a.trim_level in aa.body_style::text) > 0
where not exists (
  select 1 
  from cvd.vehicles
  where vin = a.vin)	
order by vin
limit 100

-- 54 more vins using inpmast body_style
insert into cvd.vehicles

select * from wtf
where vin = '1HGFA16818L032459'
and style_description = '4dr Auto EX w/Navi'
union
select * from wtf
where vin = '5J6RE4H77AL051426'
and style_description = '4WD 5dr EX-L'
union
select * from wtf
where vin = '5J6RE4H7XAL045393'
and style_description = '4WD 5dr EX-L w/Navi'
union
select * from wtf
where vin = '5NMSG73D18H194545'
and style_description = 'AWD 4dr Auto GLS'
union
select * from wtf
where vin = '5NPEU46F76H037099'
and style_description = '4dr Sdn GLS V6 Auto'
union
select * from wtf
where vin = '5NPEU46F76H095312'
and style_description = '4dr Sdn GLS V6 Auto'
union
select * from wtf
where vin = 'JHMFA3F29AS006918'
and style_description = '4dr Sdn L4 CVT w/Leather'
union
select * from wtf
where vin = 'KMHDU46D48U537221'
and style_description = '4dr Sdn Auto GLS'
union
select * from wtf
where vin = 'KMHHN65F63U065092'
and style_description = '2dr Cpe GT V6 4-spd Auto'
union
select * from wtf
where vin = 'KMHHN65F73U073444'
and style_description = '2dr Cpe GT V6 4-spd Auto'
union
select * from wtf
where vin = 'KMHWF35H05A165410'
and style_description = '4dr Sdn GLS V6 Auto *Ltd Avail*'
----------------------------------------------------------------------------------------------------

select * from wtf order by vin

select count(distinct vin) from wtf

-- a bunch more with a more generalized use of inpmast.body_style
select a.*, b.body_style, b.key_to_cap_explosion_data
from wtf a
left join arkona.ext_inpmast b on a.vin = b.inpmast_vin
  and b.inpmast_company_number = 'RY1'


-- more vins using inpmast body_style
insert into cvd.vehicles


select * from wtf
where vin = '1FTEF15NXSLB89143'
and style_description = 'Reg Cab Flareside 116.8" WB'
union
select * from wtf
where vin = '1FTFW1EV9AFA84952'
and style_description = '4WD SuperCrew 145" Lariat'
union
select * from wtf
where vin = '1FTFW1EV9AFD79825'
and style_description = '4WD SuperCrew 145" Lariat'
union
select * from wtf
where vin = '1FTFW1EV9AFD79842'
and style_description = '4WD SuperCrew 145" Lariat'
union
select * from wtf
where vin = '1FTFW1EV9AFD79999'
and style_description = '4WD SuperCrew 145" Lariat'
union
select * from wtf
where vin = '1FTFW1EVXAFC65963'
and style_description = '4WD SuperCrew 145" FX4'
union
select * from wtf
where vin = ''
and style_description = ''
union
select * from wtf
where vin = ''
and style_description = ''
union
select * from wtf
where vin = ''
and style_description = ''
union
select * from wtf
where vin = ''
and style_description = ''
union
select * from wtf
where vin = ''
and style_description = ''
union
select * from wtf
where vin = ''
and style_description = ''
union
select * from wtf
where vin = ''
and style_description = ''
union
select * from wtf
where vin = ''
and style_description = ''
union
select * from wtf
where vin = ''
and style_description = ''
union
select * from wtf
where vin = ''
and style_description = ''
union
select * from wtf
where vin = ''
and style_description = ''
union
select * from wtf
where vin = ''
and style_description = ''
union
select * from wtf
where vin = ''
and style_description = ''
union
select * from wtf
where vin = ''
and style_description = ''
union
select * from wtf
where vin = ''
and style_description = ''












































