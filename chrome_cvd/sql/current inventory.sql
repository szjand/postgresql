﻿-- 1.  Get the vins to submit to CVD ***************************************
select *
from arkona.ext_inpmast 
where inpmast_stock_number = 'G48564A'


select distinct inpmast_vin  -- 477
from arkona.ext_inpmast
where status = 'I'
  and type_n_u = 'U'
--   and inpmast_company_number = 'RY1'

select distinct inpmast_vin  -- 55 are already in cvd.vehicles
from arkona.ext_inpmast a
where status = 'I'
  and type_n_u = 'U'
--   and inpmast_company_number = 'RY1'
  and exists (
    select 1
    from cvd.vehicles  
    where vin = a.inpmast_vin)

select distinct inpmast_vin  -- same for vin_descriptions
from arkona.ext_inpmast a
where status = 'I'
  and type_n_u = 'U'
--   and inpmast_company_number = 'RY1'
  and exists (
    select 1
    from cvd.vin_descriptions
    where vin = a.inpmast_vin)

-- this is the query to generate the vins for CVD
-- 1/10/24: 158 vins
select distinct inpmast_vin  
from arkona.ext_inpmast a
where status = 'I'
  and type_n_u = 'U'  
  and not exists (
    select 1
    from cvd.vin_descriptions
    where vin = a.inpmast_vin) 
  and not exists (
    select 1
    from cvd.bad_vins
    where vin = a.inpmast_vin)    
  and not exists (
    select 1
    from cvd.unresolved_vins
    where vin = a.inpmast_vin)
        

-- 2. Errors/Bad Vins ************************************************************
select vin, a.response->>'message'
from cvd.vin_descriptions a
where the_date = current_date
  and (
			a.response->>'message' = 'Invalid vin'
      or a.response->>'message' = 'The VIN passed was modified to convert invalid characters'
      or a.response->>'message' = 'VIN not carried in our data')
union
select vin, 'motorcycle'
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
where b->>'bodyType' = 'Motorcycle'
	and the_Date = current_date
union
select vin, 'Sparse with no style_id' 
from cvd.vin_descriptions
where source = 'S'
	and the_date = current_date	  
	and vin in (
		select vin 
		from cvd.vin_descriptions a
		join jsonb_array_elements(a.response->'result'->'vehicles') b on true
		where b->>'styleId' is null
			and the_date = current_date)	  
union			
select vin, 'No source'
from cvd.vin_descriptions
where source is null
  and the_date = current_date
union
select vin, 'vinSubmitted <> vinProcessed'
from cvd.vin_descriptions a
where a.response->'result'->>'vinProcessed' <> a.response->'result'->>'vinSubmitted';

-- 1/10/24
    "vinProcessed": "1GKS2GKC2GR299732",
    "vinSubmitted": "1GKS2GKC9GR299732",
select * from cvd.vin_descriptions where vin = '1GKS2GKC2GR299732'
select * from cvd.vin_descriptions where vin = '1GKS2GKC9GR299732'
select * from arkona.ext_inpmast where right(inpmast_vin, 6) = '299732'
select * from cvd.vin_descriptions where right(vin, 6) = '299732'
this one puzzles me, the vin submitted was incorrect, 9th digit a 9 s/b a 2
but the vin in cvd.vin_descriptions is correct, ie, 9th digit is 2, wtf?!?!?!?!
and in dealertrack, it is not in inventory, sparse entry, not a sale, not inventory, not service
in ADS 9 also throws Failed checksum or bad check digit

select * from arkona.xfm_inpmast where right(inpmast_vin, 6) = '299732'
gives me stocknumber G47741B, which is in the tool with 9 as 9th digit, and it decoded in black book
-- 3. Colors ******************************************************************
-- 1/10/24 126 of 158
insert into cvd.colors(vin,exterior,interior)  
select vin, b->>'description', c->>'description'
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'exteriorColors') b on true
join jsonb_array_elements(a.response->'result'->'interiorColors') c on true
where jsonb_array_length(a.response->'result'->'exteriorColors') = 1
  and jsonb_array_length(a.response->'result'->'interiorColors') = 1
  and the_date = current_date; -- ************************************************************************************************

-- 1/10 from parse_tmp_vehicles_2.sql

-- do inpmast and see if i can get a match from cvd
drop table if exists cvd.tmp_exterior;
create table cvd.tmp_exterior (
  vin citext primary key,
  exterior citext not null,
  interior citext);

-- 1/7/24: 148 rows
-- 1/10/24: 20 rows
insert into cvd.tmp_exterior(vin,exterior)
select distinct vin, exterior
from (
	select a.vin, b.color_code, b.color, c->>'description' as exterior, similarity(b.color, c->>'description'),
		row_number() over (partition by vin order by similarity(b.color, c->>'description') desc) as seq
	from cvd.vin_descriptions a
	join arkona.ext_inpmast b on a.vin = b.inpmast_vin
		and b.inpmast_company_number = 'RY1'
	left join jsonb_array_elements(a.response->'result'->'exteriorColors') c on similarity(b.color, c->>'description') > .2
	where a.the_date = current_date  -- ************************************************************************************************
		and c->>'description' is not null 
		and not exists (
			select 1
			from cvd.colors
			where vin = a.vin)
	order by a.vin asc, similarity(b.color, c->>'description') desc) aa
where aa.seq = 1	
	and not exists (
		select 1
		from cvd.colors
		where vin = aa.vin);

-- 1/10/24: 12 rows
insert into cvd.tmp_exterior(vin,exterior)
select distinct vin, coalesce(initcap(exteriorcolor), initcap(color))
from (
	select a.vin, source, style_count, b.color, b.trim, c.exteriorcolor, c.interiorcolor
	from cvd.vin_descriptions a
	left join arkona.ext_inpmast b on a.vin = b.inpmast_vin
	left join ads.ext_vehicle_items c on a.vin = c.vin
	where a.the_date = current_date -- *************************************************************************************
		and not exists (
			select 1
			from cvd.tmp_exterior
			where vin = a.vin)
		and not exists (
			select 1
			from cvd.colors
			where vin = a.vin)) x	

-- 1/7/24 and that leaves us with 346
select vin
from cvd.vin_descriptions a
where the_date = current_date -- *************************************************************************************
	and not exists (
		select 1
		from cvd.tmp_exterior
		where vin = a.vin)
  and not exists (
    select 1
    from cvd.colors
    where vin = a.vin)		
	
--------------------------
-- now interior
-- just making sure
select vin from cvd.tmp_exterior a
where exists (select 1 from cvd.colors where vin = a.vin)		

update cvd.tmp_exterior x
set interior = y.interior
from (		
	select distinct b.vin, coalesce(initcap(c.interiorcolor), initcap(a.trim), 'unknown') interior
	from cvd.vin_descriptions b
	join arkona.ext_inpmast a on a.inpmast_vin = b.vin
		and a.inpmast_company_number = 'RY1'
	left join ads.ext_vehicle_items c on  b.vin = c.vin
	where b.the_date = current_date -- *************************************************************************************
		and not exists (
			select 1
			from cvd.colors
			where vin = a.inpmast_vin)) y
where x.vin = y.vin;			

select vin a
from cvd.vin_descriptions a
where the_date = current_date -- *************************************************************************************
  and not exists (
    select 1
    from cvd.tmp_exterior
    where vin = a.vin)

update cvd.tmp_exterior
set interior = 'unknown'
where interior is null;

insert into cvd.colors(vin,exterior,interior)
select * from cvd.tmp_exterior;

-- bingo
select vin
from cvd.vin_descriptions a
where not exists (
  select 1
  from cvd.colors
  where vin = a.vin)













/* some older stuff
insert into cvd.colors(vin,exterior,interior)  69
select distinct b.vin, coalesce(initcap(c.exteriorcolor), initcap(a.color)), coalesce(initcap(c.interiorcolor), initcap(a.trim))
from cvd.vin_descriptions b
join arkona.ext_inpmast a on a.inpmast_vin = b.vin
  and a.inpmast_company_number = 'RY1'
left join ads.ext_vehicle_items c on  b.vin = c.vin
where b.the_date = current_date
	and not exists (
		select 1
		from cvd.colors
		where vin = a.inpmast_vin);

-- 1 left, vin WVGEF9BP8GD012687, 16 VW Touareg, no colors in tool or Dealertrack
select vin, style_count, source, the_date,
  jsonb_array_length(a.response->'result'->'exteriorColors') as ext,
  jsonb_array_length(a.response->'result'->'interiorColors') as int
from cvd.vin_descriptions a
where not exists (
  select 1 
  from cvd.colors
  where vin = a.vin)
  
-- so
insert into cvd.colors(vin,exterior,interior) 
values('WVGEF9BP8GD012687','unknown','unknown');

-- and we are good
select vin
from cvd.vin_descriptions a
where not exists (
  select 1
  from cvd.colors
  where vin = a.vin)
*/






		
--------------------------------------------------------------------------------------------------------------------------
--< CASE 1 source = B style_count = 1
--------------------------------------------------------------------------------------------------------------------------

-- 1/10/24: 158 vins
select count(*) 
from cvd.vin_descriptions a
where not exists (
  select 1
  from cvd.vehicles
  where vin = a.vin)


drop table if exists cvd.z_wtf cascade;
create unlogged table cvd.z_wtf as
select a.vin, (a.response->'result'->>'year')::integer as model_year, a.response->'result'->>'make' as make, 
  a.response->'result'->>'model' as model, b->>'trim' as trim_level, 
  b->>'driveType' as drive,
  b->>'mfrModelCode' as model_code,
  case 
		when b->>'bodyType' like '%Cab%' or b->>'bodyType' like '%Crew%' 
			then 'Pickup' 
		else b->>'bodyType' 
	end as body_type, -- problem, doesn't break out cab type separate from body type, so, no body type of pickup !?!?! Toyota has some as crew max
  k as segment,
  case when b->>'bodyType' like '%Cab%' then b->>'bodyType' else 'n/a' end as cab,
  coalesce(j->>'name', 'n/a') as box_length,
  f->>'name' as eng_disp,
  g->>'nameNoBrand' as cylinders,
  coalesce(h->>'name', l->>'name', 'unknown') as fuel, 
  coalesce(i->>'name', 'unknown') as transmission,
  c.exterior as ext_color, -- c->>'genericDesc' as generic_ext_color, no longer have the generics, hmm maybe should have included them
  coalesce(c.interior, 'unknown') as int_color, -- d->>'genericDesc' as generic_int_color,
  (b->>'doors')::integer as doors, -- also features id 12175
  (a.response->'result'->>'buildMSRP')::numeric::integer as build_msrp, (b->>'baseMSRP')::numeric::integer as base_msrp,
  b->>'styleDescription' as style_description,
  a.response->'result'->>'source' as source,
  b->>'styleId' as style_id
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
join cvd.colors c on a.vin = c.vin
left join jsonb_array_elements(a.response->'result'->'techSpecs') f on f->>'id' = '10120'
left join jsonb_array_elements(a.response->'result'->'techSpecs') g on g->>'description' = 'Engine Cylinders'
left join jsonb_array_elements(a.response->'result'->'features') h on h->>'id' = '10030'
left join jsonb_array_elements(a.response->'result'->'features') i on i->>'id' = '25190'
left join jsonb_array_elements(a.response->'result'->'techSpecs') j on j->>'id' = '18090'
left join jsonb_array_elements_text(b->'segment') k on true
left join jsonb_array_elements(a.response->'result'->'techSpecs') l on  l->>'id' = '22360'
where a.source = 'B' 
  and a.style_count = 1
  and not exists (
    select 1
    from cvd.vehicles
    where vin = a.vin);
create unique index on cvd.z_wtf(vin);  

-- 1/10/24: inserted 128 of the 158 vins
insert into cvd.vehicles
select * from cvd.z_wtf;  

--------------------------------------------------------------------------------------------------------------------------
--/> CASE 1 source = B style_count = 1
--------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------
--< CASE 2 source <> B style_count = 1
--------------------------------------------------------------------------------------------------------------------------

drop table if exists cvd.z_wtf cascade;
create unlogged table cvd.z_wtf as
select a.vin, (a.response->'result'->>'year')::integer as model_year, a.response->'result'->>'make' as make, 
  a.response->'result'->>'model' as model, b->>'trim' as trim_level, 
  case when coalesce(length(b->>'driveType'), 0) = 0 then 'unknown' else b->>'driveType' end as drive,
  coalesce(b->>'mfrModelCode', 'unknown') as model_code,
  case 
    when b->>'bodyType' like '%Cab%' or b->>'bodyType' like '%Crew%' 
      then 'Pickup' 
    else b->>'bodyType' 
  end as body_type, -- problem, doesn't break out cab type separate from body type, so, no body type of pickup !?!?! Toyota has some as crew max
  k as segment,
  case when b->>'bodyType' like '%Cab%' then b->>'bodyType' else 'n/a' end as cab,
  coalesce(j->>'name', 'n/a') as box_length,
  f->>'name' as eng_disp,
  g->>'nameNoBrand' as cylinders,
  coalesce(h->>'name', l->>'name', 'unknown') as fuel, 
--   l->>'name' as fuel_2,
  coalesce(i->>'name', 'unknown') as transmission,
  c.exterior as ext_color, -- c->>'genericDesc' as generic_ext_color, no longer have the generics, hmm maybe should have included them
  coalesce(c.interior, 'unknown') as int_color, -- d->>'genericDesc' as generic_int_color,
  (b->>'doors')::integer as doors, -- also features id 12175
  (a.response->'result'->>'buildMSRP')::numeric::integer as build_msrp, (b->>'baseMSRP')::numeric::integer as base_msrp,
  b->>'styleDescription' as style_description,
  a.response->'result'->>'source' as source,
  b->>'styleId' as style_id
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
join cvd.colors c on a.vin = c.vin
left join jsonb_array_elements(a.response->'result'->'techSpecs') f on f->>'id' = '10120'
left join jsonb_array_elements(a.response->'result'->'techSpecs') g on g->>'description' = 'Engine Cylinders'
left join jsonb_array_elements(a.response->'result'->'features') h on h->>'id' = '10030'
left join jsonb_array_elements(a.response->'result'->'features') i on i->>'id' = '25190'
left join jsonb_array_elements(a.response->'result'->'techSpecs') j on j->>'id' = '18090'
left join jsonb_array_elements_text(b->'segment') k on true
left join jsonb_array_elements(a.response->'result'->'techSpecs') l on  l->>'id' = '22360'
where a.source <> 'B' 
  and a.style_count = 1
  and not exists (
    select 1
    from cvd.vehicles
    where vin = a.vin);    
create unique index on cvd.z_wtf(vin);  

-- 1/10/24: insert 22 rows
insert into cvd.vehicles
select *
from cvd.z_wtf;


--------------------------------------------------------------------------------------------------------------------------
--/> CASE 2 source <> B style_count = 1
--------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------
--< CASE 3 the remaining 12 rows, able to define them with help of inpmast and the tool
--------------------------------------------------------------------------------------------------------------------------

-- 1/10/24: 8 vins
select *  
from cvd.vin_descriptions a
where not exists (
  select 1
  from cvd.vehicles
  where vin = a.vin)


drop table if exists cvd.z_wtf cascade;
create unlogged table cvd.z_wtf as
select a.vin, (a.response->'result'->>'year')::integer as model_year, a.response->'result'->>'make' as make, 
  a.response->'result'->>'model' as model, b->>'trim' as trim_level, 
  case when coalesce(length(b->>'driveType'), 0) = 0 then 'unknown' else b->>'driveType' end as drive,
  coalesce(b->>'mfrModelCode', 'unknown') as model_code,
  case 
    when b->>'bodyType' like '%Cab%' or b->>'bodyType' like '%Crew%' 
      then 'Pickup' 
    else b->>'bodyType' 
  end as body_type, -- problem, doesn't break out cab type separate from body type, so, no body type of pickup !?!?! Toyota has some as crew max
  k as segment,
  case when b->>'bodyType' like '%Cab%' then b->>'bodyType' else 'n/a' end as cab,
  coalesce(j->>'name', 'n/a') as box_length,
  f->>'name' as eng_disp,
  g->>'nameNoBrand' as cylinders,
  coalesce(h->>'name', l->>'name', 'unknown') as fuel, 
--   l->>'name' as fuel_2,
  coalesce(i->>'name', 'unknown') as transmission,
  c.exterior as ext_color, -- c->>'genericDesc' as generic_ext_color, no longer have the generics, hmm maybe should have included them
  coalesce(c.interior, 'unknown') as int_color, -- d->>'genericDesc' as generic_int_color,
  (b->>'doors')::integer as doors, -- also features id 12175
  (a.response->'result'->>'buildMSRP')::numeric::integer as build_msrp, (b->>'baseMSRP')::numeric::integer as base_msrp,
  b->>'styleDescription' as style_description,
  a.response->'result'->>'source' as source,
  b->>'styleId' as style_id
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
join cvd.colors c on a.vin = c.vin
left join jsonb_array_elements(a.response->'result'->'techSpecs') f on f->>'id' = '10120'
left join jsonb_array_elements(a.response->'result'->'techSpecs') g on g->>'description' = 'Engine Cylinders'
left join jsonb_array_elements(a.response->'result'->'features') h on h->>'id' = '10030'
left join jsonb_array_elements(a.response->'result'->'features') i on i->>'id' = '25190'
left join jsonb_array_elements(a.response->'result'->'techSpecs') j on j->>'id' = '18090'
left join jsonb_array_elements_text(b->'segment') k on true
left join jsonb_array_elements(a.response->'result'->'techSpecs') l on  l->>'id' = '22360'
where not exists (
    select 1
    from cvd.vehicles
    where vin = a.vin);    


select a.*, b.body_style, b.key_to_cap_explosion_data, b.model_code
from cvd.z_wtf a
left join arkona.ext_inpmast b on a.vin = b.inpmast_vin
  and b.inpmast_company_number = 'RY1'
left join ads.ext_vehicle_items c on a.vin = c.vin
where not exists (select 1 from cvd.vehicles where vin = a.vin)
order by a.vin

-- 1/10/24: insert 4 of 8

insert into cvd.vehicles

select distinct vin,model_year,make,model,trim_level,drive,model_code,body_type,
	segment,cab,box_length,eng_disp,cylinders,fuel,'unknown',ext_color,
	int_color,doors,build_msrp,base_msrp,style_description,source,style_id 
from cvd.z_wtf
where vin = '1HGCY2F62PA052023'
and style_description = 'EX-L Sedan w/o BSI' 
union
select distinct vin,model_year,make,model,trim_level,drive,model_code,body_type,
	segment,cab,box_length,eng_disp,cylinders,fuel,'unknown',ext_color,
	int_color,doors,build_msrp,base_msrp,style_description,source,style_id 
from cvd.z_wtf
where vin = '1N4AL3APXGC223489'
and style_description = '4dr Sdn I4 2.5 SV' 
union
select distinct vin,model_year,make,model,trim_level,drive,model_code,body_type,
	segment,cab,box_length,eng_disp,cylinders,fuel,'unknown',ext_color,
	int_color,doors,build_msrp,base_msrp,style_description,source,style_id 
from cvd.z_wtf
where vin = '5FNRL5H6XEB020365'
and style_description = '5dr EX-L w/RES'
union
select distinct vin,model_year,make,model,trim_level,drive,model_code,body_type,
	segment,cab,box_length,eng_disp,cylinders,fuel,'unknown',ext_color,
	int_color,doors,build_msrp,base_msrp,style_description,source,style_id 
from cvd.z_wtf
where vin = '5J6RE4H79BL071436'
and style_description = '4WD 5dr EX-L'


--------------------------------------------------------------------------------------------------------------------------
--< unresolved vins
--------------------------------------------------------------------------------------------------------------------------


-- 1/7/24: 310 unresolved vins
-- 1/10/24: 4 unresolved vins
insert into cvd.unresolved_vins(vin,response,style_count,source,the_date) 
select a.vin, a.response, 
  a.style_count, a.source, a.the_date
-- select count(*)
from cvd.vin_descriptions a  
where the_date = current_date -- *************************************************************
	and not exists (
		select 1
		from cvd.vehicles
		where vin = a.vin)
	and not exists (
		select 1
		from cvd.unresolved_vins
		where vin = a.vin);

-- delete from cvd.colors
delete 
-- select vin
from cvd.colors
where vin in (
	select a.vin
	from cvd.vin_descriptions a  
	where the_date = current_date -- *************************************************************
		and not exists (
			select 1
			from cvd.vehicles
			where vin = a.vin));			

-- delete from cvd.vin_descriptions			
delete 
-- select count(*)
from cvd.vin_descriptions a
where vin in (
	select a.vin
	from cvd.vin_descriptions a  
	where the_date = current_date -- *************************************************************
		and not exists (
			select 1
			from cvd.vehicles
			where vin = a.vin));


select * from cvd.vehicles where vin in (
select vin
from cvd.vin_descriptions a
where the_date = current_date -- *************************************************************
and exists (
  select 1
  from cvd.unresolved_vins
  where vin = a.vin))
  
select vin
from cvd.vin_descriptions a
where not exists (
  select 1
  from cvd.vehicles
  where vin = a.vin)

--------------------------------------------------------------------------------------------------------------------------
--/> unresolved vins
--------------------------------------------------------------------------------------------------------------------------


select count(*) from cvd.vehicles  -- 19277
select count(*) from cvd.vin_descriptions  -- 19277
select count(*) from cvd.unresolved_vins  -- 312
select count(*) from cvd.bad_vins -- 29

select 341.0/(19277 + 341)  -- 1.738%


