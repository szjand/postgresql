﻿select a.vin, a.response->'result'->>'year', a.response->'result'->>'make', a.response->'result'->>'model',b->>'driveType', c->>'name', a.source, a.style_count
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
left join jsonb_array_elements(a.response->'result'->'features') c on c->>'id' = '10750'
-- order by a.response->'result'->>'year', a.response->'result'->>'make', a.response->'result'->>'model'
order by right(vin, 6)
limit 50


-- most nulls from feature result, though not many actually
select distinct a.vin, a.response->'result'->>'year', a.response->'result'->>'make', a.response->'result'->>'model',b->>'driveType', c->>'name', a.source, a.style_count
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
left join jsonb_array_elements(a.response->'result'->'features') c on c->>'id' = '10750'
where b->>'driveType' is null or c->>'name' is null
order by a.response->'result'->>'year', a.response->'result'->>'make', a.response->'result'->>'model'

prefer the short title. FWD vs Front-wheel drive, etc


