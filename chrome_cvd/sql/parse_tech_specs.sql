﻿-- tech specs for source = B, 2023 Silverado 1500
select a.vin,  
  b->>'id' as id, b->>'key' as key, b->>'name' as name, 
  c->'styleIds' as style_id, c->>'isStandard' as standard, c->>'installCause' as install_cause,
  b->>'sectionId' as section_id, b->>'description' as description, b->>'isEVFeature' as ev_feature, b->>'nameNoBrand' as name_no_brand,
  b->>'sectionName' as section_name, b->>'rankingValue' as ranking, b->>'subSectionId' as subsection_id, b->>'isHybridFeature' as hybrid_feature,
  b->>'unitsOfMeasureAndValues' as units_of_measure, b->'benefitStatement' as benefit
-- select a.* , b
from chr.cvd a
join jsonb_array_elements(a.response->'result'->'techSpecs') b on true
join jsonb_array_elements(b->'styles') c on true
where vin = '2GCUDEED6P1102772'
-- order by b->>'description'
order by b->>'sectionName', b->>'description'
-- order by b->>'id'

-- what i want to do is to list out the section_names

select a.vin,  
  b->>'id' as id, b->>'key' as key, b->>'name' as name, 
  c->'styleIds' as style_id, c->>'isStandard' as standard, c->>'installCause' as install_cause,
  b->>'sectionId' as section_id, b->>'description' as description, b->>'isEVFeature' as ev_feature, b->>'nameNoBrand' as name_no_brand,
  b->>'sectionName' as section_name, b->>'rankingValue' as ranking, b->>'subSectionId' as subsection_id, b->>'isHybridFeature' as hybrid_feature,
  b->>'unitsOfMeasureAndValues' as units_of_measure, b->'benefitStatement' as benefit
-- select a.* , b
from chr.cvd a
join jsonb_array_elements(a.response->'result'->'features') b on true
join jsonb_array_elements(b->'styles') c on true
where vin = '2GCUDEED6P1102772'
-- order by b->>'description'
order by b->>'sectionName', b->>'description'