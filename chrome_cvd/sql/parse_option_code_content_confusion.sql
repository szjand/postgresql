﻿This is confusing as hell
thought to compare this object, optionCodeContent, with the array optionCodes, but they do noT match 

select a.vin,  
  b->>'msrp' as msrp, 
  b->>'group' as group, 
  b->'content' as content,
  b->>'isStandard' as standard,
  b->>'optionCode' as option_code,
  b->'featureKeys' as feature_keys,
  b->>'installCause' as install_cause,
  b->>'isChromeCode' as is_chrome_code,
  b->>'optionDescription' as description,
  jsonb_array_length(b->'content')
-- select a.* , b
from chr.cvd a
join jsonb_array_elements(a.response->'result'->'optionCodeContent') b on true
where vin = '2GCUDEED6P1102772'
-- order by b->>'description'
order by b->>'optionCode'
-- order by b->>'group'


-- compare cvd to rydellcars
-- this proved to go nowhere, the feature groups did not line up at all
select * from chr.cvd where vin = '2GCUDEED4R1144067'

select a.vin,  
  b->>'msrp' as msrp, 
  b->>'group' as group, 
  b->'content' as content,
  b->>'isStandard' as standard,
  b->>'optionCode' as option_code,
  b->'featureKeys' as feature_keys,
  b->>'installCause' as install_cause,
  b->>'isChromeCode' as is_chrome_code,
  b->>'optionDescription' as description,
  jsonb_array_length(b->'content')
from chr.cvd a
join jsonb_array_elements(a.response->'result'->'optionCodeContent') b on true
where vin = '2GCUDEED6P1102772'
  and b->>'description' like '%Bumper%'
-- order by b->>'description'
-- order by b->>'optionCode'
order by b->>'group'