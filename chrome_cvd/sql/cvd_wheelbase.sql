﻿select distinct a.vin, a.response->'result'->>'year', a.response->'result'->>'make', a.response->'result'->>'model',b->>'wheelbase', c->>'name', a.source, a.style_count
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
left join jsonb_array_elements(a.response->'result'->'techSpecs') c on c->>'id' = '18530'
limit 5
order by a.response->'result'->>'year', a.response->'result'->>'make', a.response->'result'->>'model'




select a.vin, c->>'wheelbase'--, d->>'name'
from cvd.vehicles a
join cvd.vin_descriptions b on a.vin = b.vin
left join jsonb_array_elements(b.response->'result'->'vehicles') c on true
  and a.style_id = c->>'styleId'
-- left join jsonb_array_elements(b.response->'result'->'techSpecs') d on d->>'id' = '18530'