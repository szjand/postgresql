﻿afton
gmgl.ad_price_drop_down_options() - nc.vehicles
gmgl.get_models_for_maap() - nc.year_make_models
gmgl.get_sold_vehicles() - nc.vehicle_acquisitions
gmgl.get_vehicles() - nc.vehicle_acquisitions
insert_maap_make_model() - nc.year_make_models

gmgl.daily_inventory_update()
nc.open_orders
nc.vehicle_configurations
nc.vehicles
nc.vehicle_acqusitions


jon 
i understand the first group of tables, they are used in the functions
ah, and the second group of tables are all used in gmgl.daily_inventory_update()
the obvious question to me is, are these function all currently in use on active pages?
Thank you so much for looking into these for me

afton
Yes they are


E:\sql\postgresql\nc_inventory\all_nc_inventory_nightly.sql

1/3/24 afton has removed nc.vehicle_configurations from her stuff
so i can now remove fropm all_nc_inventory_nightly.sql
-------------------------------------------------------------------------------------
--< ground dates
-------------------------------------------------------------------------------------

so, of course, the immediate question is, into what table do i put the cvd data for these vins?
12/29/23
meeting w/afton, objective (for now) is to end reliance on ADS, so, this process will be to populate
the existing tables with CVD rather than ADS

for now, the, the cvd data will go into cvd.nc_vin_descriptions and cvd.nc_colors and from there, populate the nc tables as required 

-------------------------------------------------------------------------------------
--/> ground dates
-------------------------------------------------------------------------------------       

-- test for multiple chrome styles not resolved by model code
select * from cvd.nc_vin_descriptions

do
$$
begin
assert (
  select count(*) 
  from (
    select a.stock_number, a.vin
    from nc.stg_availability a      
    join cvd.nc_vin_descriptions b on a.vin = b.vin
    join jsonb_array_elements(b.response->'result'->'vehicles') c on true
    left join gmgl.vehicle_orders d on a.vin = d.vin 
    where b.style_count = 1
      and a.model_code = c->>'mfrModelCode'::citext
--       and r.style ->'attributes'->>'fleetOnly' = 'false' -- resolves the traverse problem fleet only does not appear to exists in cvd
--       and -- this handles 1500 LD 1LT vs 2LT
--         case
--           when (b.response->'result'->>'year')::integer = 2024 
--             and (b.a.response->'result'->>'model'::citext)::citext in('Silverado 1500 LD','Escalade ESV') then 
-- --             case
-- --               when d.peg = '2LT' then (r.style ->'attributes'->>'id')::citext = '398577'
-- --               when d.peg = '1LT' then (r.style ->'attributes'->>'id')::citext = '398576'
-- --               when d.peg = '1SA' then (r.style ->'attributes'->>'id')::citext = '398555'
-- --               when d.peg = '1SB' then (r.style ->'attributes'->>'id')::citext = '398556'
-- --               when d.peg = '1SD' then (r.style ->'attributes'->>'id')::citext = '398558'                
-- --               else 1 = 1
-- --             end
--           else 1 = 1
--         end 
    group by a.stock_number, a.vin
    having count(*) > 1) c) = 0, 'multiple chrome styles not resolved by model code, 1500 LD trim, exc fleet';
end
$$;    

-- parse cvd.nc_vin_descriptions into nc.stg_vehicles

truncate nc.stg_vehicles;
insert into nc.stg_vehicles (vin, chrome_Style_id, model_year, make, model,
  model_code, drive, cab, trim_level, engine, color, box_size)
select a.vin, c->>'styleId' as style_id,
  (b.response->'result'->>'year')::integer as model_year, b.response->'result'->>'make' as make, 
  b.response->'result'->>'model' as model, c->>'mfrModelCode' as model_code,
  c->>'driveType' as drive,
  case when c->>'bodyType' like '%Cab%' then c->>'bodyType' else 'n/a' end as cab,
  c->>'trim' as trim_level, 
  case
    when (b.response->'result'->>'model')::citext in ('bolt', 'bolt ev','lyriq','bZ4X') then 'EV'
    else e->>'name'
  end as eng_disp, 
  d.exterior as color,
  coalesce(f->>'name', 'n/a') as box_length
from nc.stg_Availability a
join cvd.nc_vin_descriptions b on a.vin = b.vin
join jsonb_array_elements(b.response->'result'->'vehicles') as c on true
join cvd.nc_colors d on a.vin = d.vin
left join jsonb_array_elements(b.response->'result'->'techSpecs') e on e->>'id' = '10120' -- engine displacement
left join jsonb_array_elements(b.response->'result'->'techSpecs') f on f->>'id' = '18090'; -- box length

-------------------------------------------------------------------------------------
--< fucking configurations are going to be a pain in the ass
-------------------------------------------------------------------------------------

select * 
from nc.stg_vehicles a
left join nc.vehicle_configurations b on a.chrome_style_id = b.chrome_style_id

-- chrome style ids with multiple configuration_ids
select chrome_Style_id
from (
select configuration_id, chrome_style_id
from nc.vehicle_configurations
group by configuration_id, chrome_style_id) a
group by chrome_style_id
having count(*) > 1

-- multiple engines (therefore config_id) per chrome style_id
select * from nc.vehicle_configurations where chrome_style_id = '438059'

just fucking did them, yes, multiple config_ids because ads trim <> cvd trim
i dont care
-------------------------------------------------------------------------------------
--/> fucking configurations are going to be a pain in the ass
-------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------
--< CVD for open orders: colors
-------------------------------------------------------------------------------------

-- colors for 50 of the 87 
-- 35 of the 37 with no color are source = C
select a.vin, b.*, a.source, a.style_count
from cvd.nc_vin_descriptions a
left join (
	select vin, b->>'description', c->>'description'
	from cvd.nc_vin_descriptions a
	join jsonb_array_elements(a.response->'result'->'exteriorColors') b on true
	join jsonb_array_elements(a.response->'result'->'interiorColors') c on true
	where jsonb_array_length(a.response->'result'->'exteriorColors') = 1
		and jsonb_array_length(a.response->'result'->'interiorColors') = 1
		and the_date = current_date) b on a.vin = b.vin
where not exists (
  select 1
  from cvd.nc_colors
  where vin = a.vin)	


select * from cvd.nc_vin_descriptions where vin = 'KL79MUSL0MB023683'

select a.vin, b.*, a.source, a.style_count
from cvd.nc_vin_descriptions a
left join (
select vin, b->>'description', c->>'description'
from cvd.nc_vin_descriptions a
join jsonb_array_elements(a.response->'result'->'exteriorColors') b on true and b->>'type' = '1'
join jsonb_array_elements(a.response->'result'->'interiorColors') c on true
-- where jsonb_array_length(a.response->'result'->'exteriorColors') = 1
where jsonb_array_length(a.response->'result'->'interiorColors') = 1
  and the_date = current_date
	and not exists (
		select 1
		from cvd.nc_colors
		where vin = a.vin)) b on a.vin = b.vin	 
order by b.vin


select * from nc.open_orders where vin = 'KL77LJE23RC160095'		

select * from cvd.nc_vin_descriptions where vin = 'KL77LJE23RC160095'


select a.vin, coalesce(b.exterior, c.color) as exterior, coalesce(b.interior, 'unknown') as interior
from cvd.nc_vin_descriptions a
left join (
	select vin, b->>'description' as exterior, c->>'description' as interior
	from cvd.nc_vin_descriptions a
	join jsonb_array_elements(a.response->'result'->'exteriorColors') b on true 
	join jsonb_array_elements(a.response->'result'->'interiorColors') c on true
	where jsonb_array_length(a.response->'result'->'exteriorColors') = 1
		and jsonb_array_length(a.response->'result'->'interiorColors') = 1
		and the_date = current_date
		and not exists (
			select 1
			from cvd.nc_colors
			where vin = a.vin)) b on a.vin = b.vin
left join nc.open_orders c on a.vin = c.vin			
where not exists (
  select 1
  from cvd.nc_colors
  where vin = a.vin)			

alter table cvd.nc_colors alter column exterior set not null;		
alter table cvd.nc_colors alter column interior set not null;		
-------------------------------------------------------------------------------------
--/> CVD for open orders: colors
-------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------
--< generate configuration data for open orders where it does not exist yet
---------------------------------------------------------------------------------------------------------	

fields needed: model_year, make, model, model_code, trim, cab, drive, engine, style_id, alloc_group, box_size, name_wo_trim

select * from nc.open_orders limit 6

-- limited to > 2022
select distinct aa.*
from (
	select (b.response->'result'->>'year')::integer as chr_model_year,
	  b.response->'result'->>'make' as chr_make, 
	  b.response->'result'->>'model' as chr_model,
	  c->>'mfrModelCode' as model_code,
	  coalesce(c->>'trim', 'none') as chr_trim,
	  case when c->>'bodyType' like '%Cab%' then c->>'bodyType' else 'n/a' end as cab, 
	  c->>'driveType' as drive,
	  d->>'name' as engine,
	  c->>'styleId' as chr_style_id,
	  a.alloc_group,
-- 	  coalesce(e->>'name', 'n/a') as box_length,
		case 
			when c->>'bodyType' like '%Cab%' or c->>'bodyType' like '%Crew%' then
			  case
			    when position('Short' in c->>'trim') > 0 then 'Short'
			    when position('Standard' in c->>'trim') > 0 then 'Standard'
			    when position('Long' in c->>'trim') > 0 then 'Long'
			  end
			else 'n/a'
	  end as box_size,
	  'n/a'::text
	from nc.open_orders a
	join cvd.nc_vin_descriptions b on a.vin = b.vin
	join jsonb_array_elements(b.response->'result'->'vehicles') c on true
	left join jsonb_array_elements(b.response->'result'->'techSpecs') d on d->>'id' = '10120' -- engine displacement
	left join jsonb_array_elements(b.response->'result'->'techSpecs') e on e->>'id' = '18090'  -- box length
	where a.model_code = c->>'mfrModelCode'
	  and (b.response->'result'->>'year')::integer > 2022) aa 
left join nc.vehicle_configurations bb on aa.chr_style_id = bb.chrome_style_id
  and aa.chr_trim = bb.trim_level
  and aa.engine = bb.engine;
where bb.configuration_id is null;	


---------------------------------------------------------------------------------------------------------
--< orders with multiple styles not resolved by model_code
---------------------------------------------------------------------------------------------------------	
do
$$
begin
assert (
  select count(*)
  from (
    select a.order_number, a.vin
    from (
      select aa.*
      from nc.open_orders aa
      join cvd.nc_vin_descriptions bb on aa.vin = bb.vin
      where aa.vin is not null
        and bb.style_count > 1) a
    left join cvd.nc_vin_descriptions b on a.vin = b.vin
    join jsonb_array_elements(b.response->'result'->'vehicles') c on true   
    where a.model_code = (c->>'mfrModelCode')::citext
    group by order_number, a.vin
    having count(*) > 1) c) = 0, 'uhoh, multiple styles not resolved by model_code';
end
$$; 

---------------------------------------------------------------------------------------------------------
--/> orders with multiple styles not resolved by model_code
---------------------------------------------------------------------------------------------------------	

-- don't have to do colors because that is handled in the CVD process


select j->>'name'
from cvd.nc_vin_descriptions a
left join jsonb_array_elements(a.response->'result'->'techSpecs') j on j->>'id' = '18090'  -- box length
where vin = '1GT49REYXRF297122'


---------------------------------------------------------------------------------------------------------
--< sold vehicle with null ground date
---------------------------------------------------------------------------------------------------------	

truncate nc.stg_vehicles;
insert into nc.stg_vehicles (vin, chrome_Style_id, model_year, make, model,
  model_code, drive, cab, trim_level, engine, color, configuration_id)

-- nc.vehicles * nc.vehicle_acquisitions
do $$
declare
  _stock_number citext := 'G49268';
begin  
	select a.vin, c->>'styleId' as style_id,
		(b.response->'result'->>'year')::integer as model_year, b.response->'result'->>'make' as make, 
		b.response->'result'->>'model' as model, c->>'mfrModelCode' as model_code,
		c->>'driveType' as drive,
		case when c->>'bodyType' like '%Cab%' then c->>'bodyType' else 'n/a' end as cab,
		c->>'trim' as trim_level, 
		case
			when (b.response->'result'->>'model')::citext in ('bolt', 'bolt ev','lyriq','bZ4X') then 'EV'
			else e->>'name'
		end as eng_disp, 
		d.exterior as color,
		g.configuration_id
	from nc.vehicle_acquisitions a -- nc.stg_Availability a
	join cvd.nc_vin_descriptions b on a.vin = b.vin
	join jsonb_array_elements(b.response->'result'->'vehicles') as c on true
	join cvd.nc_colors d on a.vin = d.vin
	left join jsonb_array_elements(b.response->'result'->'techSpecs') e on e->>'id' = '10120' -- engine displacement
	left join jsonb_array_elements(b.response->'result'->'techSpecs') f on f->>'id' = '18090' -- box length
	left join nc.vehicle_configurations g on (c->>'styleId') = g.chrome_style_id
	-- for case derived vin (equinox FWD, blazer, ...) have to comment out this line, r.style ->'attributes'->>'trim' won't work  --- blazer, equinox FWD
		and coalesce((c->>'trim')::citext, 'none') = g.trim_level
		and e->>'name' = g.engine -- !!!! this may fail on EV
	where a.stock_number = _stock_number;
end $$;

select * from wtf	


-- nc.vehicle_configurations

insert into nc.vehicle_configurations(model_year,make,model,model_code,trim_level,cab,
  drive, engine, chrome_style_id,alloc_Group,box_size,name_wo_trim,config_type,level_header)


select * from nc.vehicles where vin = '3GTUUGEL6RG201394'  

do $$
declare
  _stock_number citext := 'G49261';
  
select a.model_year, a.make, a.model, a.model_code, a.trim_level, a.cab,
  a.drive, a.engine, a.chrome_style_id, 
  'n/a' as alloc_group, ---------------------------------------------- alloc_group
  'n/a' as box, ------------------------------------------------------ box
  'n/a' name_wo_trim,------------------------------------------------- name_wo_trim
  'td' as config_type, -------------------------------------------------
  'Work Truck 4WD' as level_header -------------------------------------------------
 from nc.vehicles a  
 join nc.stg_sales b on a.vin = b.vin
   and stock_number = _stock_number;

--------------------------------------------------------------------------------------------------
--< nc.vehicles parse cvd.nc_vin_descriptions into nc.stg_vehicles
--------------------------------------------------------------------------------------------------
-- 01/03/2024 time to get rid of nc.vehicle_configurations

ALTER TABLE nc.stg_vehicles DROP CONSTRAINT stg_vehicles_configuration_id_fkey;   

ALTER TABLE nc.vehicles DROP CONSTRAINT vehicles_configuration_id_fkey;

alter table nc.vehicles alter column configuration_id drop not null;

DROP INDEX nc.vehicles_configuration_id_idx;

ALTER TABLE nc.vehicles DROP CONSTRAINT vehicles_chrome_style_id_fkey;

alter table nc.daily_inventory alter column configuration_id drop not null;

DROP INDEX nc.daily_inventory_configuration_id_idx;

--------------------------------------------------------------------------------------------------
--/> nc.vehicles parse cvd.nc_vin_descriptions into nc.stg_vehicles
--------------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------------
--< nc.vehicles  query return multiples
--------------------------------------------------------------------------------------------------
-- issue is dealer track has an extra character on Toyota model codes

select a.vin, c->>'styleId' as style_id,
  (b.response->'result'->>'year')::integer as model_year, b.response->'result'->>'make' as make, 
  b.response->'result'->>'model' as model, c->>'mfrModelCode' as model_code,
  c->>'driveType' as drive,
  case when c->>'bodyType' like '%Cab%' then c->>'bodyType' else 'n/a' end as cab,
  c->>'trim' as trim_level, 
  case
    when (b.response->'result'->>'model')::citext in ('bolt', 'bolt ev','lyriq','bZ4X') then 'EV'
    else e->>'name'
  end as eng_disp, 
  d.exterior as color,
  coalesce(f->>'name', 'n/a') as box_length
from nc.stg_Availability a
join cvd.nc_vin_descriptions b on a.vin = b.vin
join jsonb_array_elements(b.response->'result'->'vehicles') as c on true
join cvd.nc_colors d on a.vin = d.vin
left join jsonb_array_elements(b.response->'result'->'techSpecs') e on e->>'id' = '10120' -- engine displacement
left join jsonb_array_elements(b.response->'result'->'techSpecs') f on f->>'id' = '18090' -- box length
where not exists (
  select 1
  from nc.vehicles
  where vin = a.vin)
  and 
    case
      when a.make = 'toyota' then left(a.model_code, 4) = c->>'mfrModelCode'
      else a.model_code = c->>'mfrModelCode'
    end
--   and a.make = 'honda'
 order by vin 

 select * from cvd.nc_vin_descriptions where vin = '3CZRZ2H50RM751225'

 select * from nc.stg_availability where vin = '5TDKDRBH2RS532476'

 --------------------------------------------------------------------------------------------------
--/> nc.vehicles  query return multiples
--------------------------------------------------------------------------------------------------