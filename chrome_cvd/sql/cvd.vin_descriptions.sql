﻿actually, first is replacing table chr.cvd with cvd.vin_descriptions

DROP TABLE if exists cvd.vin_descriptions;
CREATE TABLE cvd.vin_descriptions
(
  vin citext NOT NULL,
  response jsonb,
  style_count integer,
  source citext,
  the_date date,
  error citext,
  CONSTRAINT vin_descriptions_pkey PRIMARY KEY (vin));

insert into cvd.vin_descriptions(vin,response,style_count,source,the_date,error)
select vin, response, style_count, source, the_date,
	case
		when response->>'error' = 'true' then response->>'message' 
	end as error
from chr.cvd;

alter table chr.cvd
rename to z_unused_cvd;


-- i think right here is where i decide whawt to do with bad vins
-- thinking, a bad vin table, attributes: vin, error
-- so, that would be only 3 rows of the 16591 (added 2 new vehicles today)
-- and then delete those vins from cvd.vin_descriptions


started with cu.tmp_vehicles in "E:\sql\postgresql\canonical_used_vehicle_source_data\month_end_update_tmp_vehicles.sql"

then started converting that to cvd compatible structure in jon.tmp_cvd in "E:\chrome_cvd\sql\parse_tmp_vehicles.sql"

today, 12/4/23, moved cvd stuff to E:\cvd, and created a cvd schema.

create schema cvd;
comment on schema cvd is 'to house all things based on the new chrome data, cvd (Chromedata Vehicle Description)';

as a starting place for table cvd.vin_description, staart with what was used to populate jon.tmp_cvd as a replacement
for cu.tmp_vehicles
drop table if exists cvd.vin_description cascade;
create table cvd.vin_description as
TABLE cu.tmp_vehicles
  vin citext NOT NULL,
  model_year integer NOT NULL,
  make citext NOT NULL,
--   subdivision citext,
  model citext NOT NULL,
  trim_level citext,
  drive citext,  -- Integration Guide p25, recommends using feature id 10750, but i prefer the shorter designation in vehicles
--   fleet_only boolean NOT NULL,
--   model_fleet boolean NOT NULL,
  model_code citext,
  body_type citext, 
--   cab citext,  -- problem, doesn't break out cab type separate from body type, so, no body type of pickup !?!?!
--   box_size citext,  -- box size is not consistently broken out as short, standard & long
  engine_disp citext,
  cylinders citext,
  fuel citext,
  transmission citext,
  ext_color citext,
  int_color citext,
  pass_doors citext,
  msrp integer,
  market_class citext,
  body_type citext,  -- confusion with cab vs body type
--   alt_body_type citext,
  style_name citext,  -- styleDescription
--   alt_style_name citext,
--   name_wo_trim citext,
--   best_make_name citext,
--   best_model_name citext,
--   best_trim_name citext,
--   best_style_name citext,
--   pic_url citext,
  source citext,
  chrome_style_id citext,
  CONSTRAINT tmp_vehicles_pkey PRIMARY KEY (vin)