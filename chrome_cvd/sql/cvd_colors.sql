﻿-------------------------------------------------------------------------------------------------
--< 12/05/23 cvd.colors is populated whenever cvd.vin_descriptions is used to populate a table
--						that includes colors
-------------------------------------------------------------------------------------------------

DROP TABLE if exists cvd.colors;
CREATE TABLE cvd.colors
(
  vin citext NOT NULL references cvd.vin_descriptions(vin) on delete cascade,
  exterior citext,
  interior citext,
  CONSTRAINT colors_pkey PRIMARY KEY (vin));
comment on table cvd.colors is 'this table will be populated when cvd.vin_descriptions is used to populate a table that includes colors, should this
  table also include generic colors?'

insert into cvd.colors(vin,exterior,interior)
select vin, exterior, interior
from chr.cvd_colors;

alter table chr.cvd_colors
rename to z_unused_cvd_colors;
-------------------------------------------------------------------------------------------------
--/> 12/05/23
-------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------
--< 11/29/23
-------------------------------------------------------------------------------------------------
select count(*) from chr.cvd  -- 16589



select vin, style_count, source, the_date,
  jsonb_array_length(a.response->'result'->'exteriorColors') as ext,
  jsonb_array_length(a.response->'result'->'interiorColors') as int
from chr.cvd a
where source <> 'B'

drop table if exists chr.cvd_colors cascade;
create table chr.cvd_colors (
  vin citext primary key,
  exterior citext,
  interior citext);

-- 12/12/23 -------------------------------------------------------
/*
change schemas and table names
*/
-- 1. low hanging fruit only 1 ext & int color available, 13461 rows

-- 12/12/23 1627 of 2303
insert into cvd.colors(vin,exterior,interior)
select vin, b->>'description', c->>'description'
from cvd.vin_descriptions a
join jsonb_array_elements(a.response->'result'->'exteriorColors') b on true
join jsonb_array_elements(a.response->'result'->'interiorColors') c on true
where jsonb_array_length(a.response->'result'->'exteriorColors') = 1
  and jsonb_array_length(a.response->'result'->'interiorColors') = 1
  and the_date = current_date

  
-- -- this all turned out to be a bust
-- select vin from (
-- select distinct vin, b->>'description', c->>'description'
-- from chr.cvd a
-- join jsonb_array_elements(a.response->'result'->'exteriorColors') b on true
--   and b->>'primary' = 'true'
--   and b->>'type' = '1'
-- join jsonb_array_elements(a.response->'result'->'interiorColors') c on true
-- where jsonb_array_length(a.response->'result'->'exteriorColors') = 2
--   and jsonb_array_length(a.response->'result'->'interiorColors') = 1
--   and not exists (
--     select 1 
--     from chr.cvd_colors
--     where vin =  a.vin)
-- ) x group by vin having count(*) > 1  
-- 
-- select * from chr.cvd where vin = 'JA32U2FU2DU016071'
-- 
-- 1D7HU18298S580812 -- dup ext color
-- 1HGCR2E54DA214803 -- 2 ext colors primary and type 1
-- 5FNYF6H71MB095035 -- 2 ext colors primary and type 1
-- 5FNYF6H72MB042604 -- 2 ext colors primary and type 1

select * from ads.ext_vehicle_items limit 10

-- 2. tool appears to be more complete, so coalesce tool with inpmast
-- select vin from (  -- 245 mult rows per vin, adding company to inpmast got rid of all
select distinct b.vin, a.color, a.trim, jsonb_array_length(b.response->'result'->'exteriorColors') as ext, 
  jsonb_array_length(b.response->'result'->'exteriorColors') as int, c.exteriorcolor, c.interiorcolor
-- from chr.cvd b
from cvd.vin_descriptions b
join arkona.ext_inpmast a on a.inpmast_vin = b.vin
  and a.inpmast_company_number = 'RY1'
left join ads.ext_vehicle_items c on  b.vin = c.vin
where not exists (
  select 1
  from cvd.colors
  where vin = a.inpmast_vin)
-- ) x group by vin having count(*) > 1 


insert into cvd.colors(vin,exterior,interior)
select distinct b.vin, coalesce(initcap(c.exteriorcolor), initcap(a.color)), coalesce(initcap(c.interiorcolor), initcap(a.trim))
from cvd.vin_descriptions b
join arkona.ext_inpmast a on a.inpmast_vin = b.vin
  and a.inpmast_company_number = 'RY1'
left join ads.ext_vehicle_items c on  b.vin = c.vin
where not exists (
  select 1
  from cvd.colors
  where vin = a.inpmast_vin);


-- 3. what's left, 7 rows
select vin, style_count, source, the_date,
  jsonb_array_length(a.response->'result'->'exteriorColors') as ext,
  jsonb_array_length(a.response->'result'->'interiorColors') as int
from cvd.vin_descriptions a
where not exists (
  select 1 
  from cvd.colors
  where vin = a.vin)

-- google these vins, and check inpmast for RY8



insert into cvd.colors(vin,exterior,interior) values
('JTNKHMBX7L1078689', '',  null),
('1FTRW14W48FC32524', 'White',  'Black'),
('1GKS2GKC2GR299732', '',  null),
('5NPEU46F96H118363', 'Black',  'Black'),
('2GCEK19K1N1167182', 'Sonoma Red Metallic',  'Jet Black/Rec Velvet');

select * from cvd.colors where exterior is null or interior is  null

update cvd.colors
set exterior = 'unknown'
where exterior is null;

update cvd.colors
set interior = 'unknown'
where interior is null;

******************************************************************************************
11/29/23  and that is it, all vins in cvd.vin_descriptions have colors
******************************************************************************************
