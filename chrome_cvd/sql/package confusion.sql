﻿the build data for this vin seems to imply the installation of 3 packages, the invoice shows only 1
select * from gmgl.vehicle_invoices where vin = '1GTR9CED6NZ105817'



2022 SIERRA 1500 LIMITED ELEVATION           GENERAL MOTORS LLC         
GA0  PACIFIC BLUE METALLIC          /V8G                                
H0U  JET BLACK                               RENAISSANCE CENTER         
ORDER NO. ZWSK54/TRE      STOCK NO.          DETROIT      MI  48243-1114
VIN 1GT R9CE D6 NZ105817                     VEHICLE INVOICE 5AD42814447
*****************************************************9380******48*14014S
MODEL & FACTORY OPTIONS               MSRP    INV AMT  RETAIL - STOCK   
TK18753 SIERRA 1500 LIMITED ELEVA 47400.00   44603.40  INVOICE 10/19/21 
C5W GVWR 7,000 LBS. (3,175 KG)         N/C        N/C  SHIPPED 10/19/21 
FE9 50-STATE EMISSIONS                 N/C        N/C  EXP I/T 10/29/21 
GA0 PACIFIC BLUE METALLIC           495.00     450.45  INT COM 10/29/21 
GU5 REAR AXLE 3.23 RATIO               N/C        N/C  PRC EFF 10/18/21 
JL1 TRAILER BRAKE CONTROLLER,       275.00     250.25  KEYS Z1339 Z1339 
    INTEGRATED                                         WFP-S QTR  OPT-1 
K05 ENGINE BLOCK HEATER             100.00      91.00  BANK: ALLY - 007 
L84 ENGINE:  5.3L ECOTEC3 V8 W/ DF 1595.00    1451.45  CHG-TO    14-014 
MQB TRANSMISSION:  10-SPEED AUTO       N/C        N/C                   
NSS CREDIT-NOT EQUIPPED WITH         50.00-     45.50- SHIP WT:  5211   
    AUTOMATIC STOP/START                               HP:       45.4   
RD3 20"" BLACK GLOSS PAINTED            N/C        N/C  GVWR:     7000   
    ALUMINUM WHEELS                                    GAWR.FT:  3800   
RFX X31 OFF-ROAD AND PROTECTION    1625.00    1478.75  GAWR.RR:  3800   
    PACKAGE                                            EMPLOY:  47889.34
    * OFF-ROAD SUSPENSION                              SUPPLR:  49801.60
    * 2-SPEED AUTOTRAC TRANSFER                        NTR: 1/2         
      CASE                                             DAN:      3SB53  
    * HILL DESCENT CONTROL                             EMPINC:   2846.47
    * SKID PLATES                                      SUPINC:    934.21
    * HEAVY-DUTY AIR FILTER                                             
    * INTEGRATED DUAL EXHAUST                                           
    * ALL TERRAIN TIRES                                                 
    * X31 BADGE                                                         
    * SPRAY-ON BEDLINER                                                 
    * ALL-WEATHER FLOOR LINERS                                          
      (DEALER INSTALLED)                                                
00D CREDIT-NOT EQUIPPED W/ DIGITAL   20.00-     18.20-                  
    CLIMATE TEMP. DISPLAY KNOBS                                         
1SZ X31 OFF-ROAD AND PROTECTION     500.00-    455.00-                  
    PACKAGE DISCOUNT                                                    
                                                                        
                                                                        
                                                                        
TOTAL MODEL & OPTIONS              50920.00  47806.60  ACT 237 47974.00 
DESTINATION CHARGE                  1695.00   1695.00  H/B 261  1527.60 
DEALER IMR CONTRIBUTION                        254.60  ADV 261   254.60 
LMA GROUP CONTRIBUTION                         254.60  EXP 65A   254.60 
                                                                        
                                                                        
TOTAL                              52615.00  50010.80  PAY 310 50010.80 
MEMO: TOTAL LESS HOLDBACK AND                                           
      APPROX WHOLESALE FINANCE CREDIT        47487.80                   
*********************************************************************** 
INVOICE DOES NOT REFLECT DEALER''S ULTIMATE COST BECAUSE OF MANUFACTURER 
REBATES, ALLOWANCES, INCENTIVES, HOLDBACK, FINANCE CREDIT AND RETURN TO 
DEALER OF ADVERTISING MONIES, ALL OF WHICH MAY APPLY TO VEHICLE.        
*********************************************************************** 
THIS MOTOR VEHICLE IS SUBJECT TO A SECURITY INTEREST HELD BY ALLY.      
                                                                        
                                         REMIT TO ALLY NO. 007          
RYDELL CHEVROLET BUICK GMC CADILLAC      VIN 1GTR9CED6NZ105817          
                                         $  50010.80 INV  5AD42814447   
                                         DUE 10/29/21  DEALER  14-014   