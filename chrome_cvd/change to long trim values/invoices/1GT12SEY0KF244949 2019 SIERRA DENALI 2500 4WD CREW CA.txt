2019 SIERRA DENALI 2500 4WD CREW CAB         GENERAL MOTORS LLC         
GBA  ONYX BLACK                     /V8D                                
H2X  JET BLACK DENALI                        RENAISSANCE CENTER         
ORDER NO. WSPD5Z/SRE      STOCK NO.          DETROIT      MI  48243-1114
VIN 1GT 12SE Y0 KF244949                     VEHICLE INVOICE 5AD36965064
*****************************************************2333******48*14014S
MODEL & FACTORY OPTIONS               MSRP    INV AMT  RETAIL - SOLD    
TK25743 SIERRA DENALI 2500 4WD CR 59800.00   55733.60  INVOICE 03/12/19 
C7A GVW RATING - 10,000 LBS            N/C        N/C  SHIPPED 03/12/19 
FE9 50-STATE EMISSIONS                 N/C        N/C  EXP I/T 03/31/19 
GT4 REAR AXLE, 3.73 RATIO              N/C        N/C  INT COM 04/01/19 
K05 ENGINE BLOCK HEATER                N/C        N/C  PRC EFF 03/11/19 
L5P DURAMAX 6.6L V8 TURBO DIESEL       N/C        N/C  KEYS V3192 V3192 
    W/ ALLISON 6-SPEED AUTOMATIC                       WFP-S QTR  OPT-1 
    TRANS (INCL. 5YR/100,000 MILE                      BANK: ALLY - 007 
    POWERTRAIN LIMITED WARRANTY                        CHG-TO    14-014 
    - SEE DEALER FOR DETAILS)                                           
MW7 ALLISON 6-SPEED AUTOMATIC TRANS    N/C        N/C  SHIP WT:  7556   
PCH DURAMAX PLUS PACKAGE           9940.00    9045.40  HP:       52.4   
    * DURAMAX 6.6L V8 TURBO DIESEL                     GVWR:    10000   
      W/ ALLISON 6-SPEED AUTOMATIC                     GAWR.FT:  5200   
      TRANS (INCL. 5YR/100,000                         GAWR.RR:  6200   
      POWERTRAIN LIMITED WARRANTY                      EMPLOY:  63580.98
      - SEE DEALER FOR DETAILS)                        SUPPLR:  66155.40
    * POWER ADJUSTABLE HEATED                          NTR: 3/4         
      TRAILER MIRROR                                   DAN:      FIELD  
    * CHROME RECOVERY HOOKS                            EMPINC:   3917.69
      (DEALER INSTALLED)                               SUPINC:   1343.27
    * CAPPED FUEL FILL                                                  
U01 ROOF MARKER LAMPS                55.00      50.05                   
V10 RADIATOR COVER                   55.00      50.05                   
Z71 OFF-ROAD SUSPENSION PACKAGE     180.00     163.80                   
    * HILL DESCENT CONTROL                                              
    * TWIN TUBE RANCHO SHOCKS                                           
1SZ DURAMAX PLUS PACKAGE DISCOUNT   750.00-    682.50-                  
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
TOTAL MODEL & OPTIONS              69280.00  64360.40  ACT 237 63777.00 
DESTINATION CHARGE                  1495.00   1495.00  H/B 261  2078.40 
DEALER IMR CONTRIBUTION                        346.40  ADV 261   346.40 
LMA GROUP CONTRIBUTION                         346.40  EXP 65A   346.40 
                                                                        
                                                                        
TOTAL                              70775.00  66548.20  PAY 310 66548.20 
MEMO: TOTAL LESS HOLDBACK AND                                           
      APPROX WHOLESALE FINANCE CREDIT        63453.20                   
*********************************************************************** 
INVOICE DOES NOT REFLECT DEALER'S ULTIMATE COST BECAUSE OF MANUFACTURER 
REBATES, ALLOWANCES, INCENTIVES, HOLDBACK, FINANCE CREDIT AND RETURN TO 
DEALER OF ADVERTISING MONIES, ALL OF WHICH MAY APPLY TO VEHICLE.        
*********************************************************************** 
THIS MOTOR VEHICLE IS SUBJECT TO A SECURITY INTEREST HELD BY ALLY.      
                                                                        
                                         REMIT TO ALLY NO. 007          
RYDELL CHEVROLET BUICK GMC CADILLAC      VIN 1GT12SEY0KF244949          
                                         $  66548.20 INV  5AD36965064   
                                         DUE 04/01/19  DEALER  14-014   
