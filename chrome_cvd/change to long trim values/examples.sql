﻿select * from gmgl.vehicle_invoices where vin = '1GT12SEY0KF244949'

-- this change in trim is not present in the ADS service
select distinct b->>'modelYear', b->'division'->>'_value_1', b->'model'->>'_value_1', b->>'trim'
from chr.build_data_describe_vehicle a
join jsonb_array_elements(a.response->'style') b on true
where b->>'modelYear' in ('2019','2020')
order by  b->'division'->>'_value_1', b->'model'->>'_value_1',  b->>'trim', b->>'modelYear'



starting in 2020 longer trim values

no change
Jeep 
Hyundai Tuscon
Kia
Mazda
Subaru
Toyota
Dodge

Ram appears to have always been the long trim

Changes
nissan armada
JN8AY2NC1K9588344,2019,Nissan,Armada,Platinum
JN8AY2NE9L9781554,2020,Nissan,Armada,Platinum 4WD


GMC Yukon
1GKS2GKCXKR344795,2019,GMC,Yukon XL,SLT
1GKS2HKJ1KR278769,2019,GMC,Yukon XL,Denali
1GKS2GKC3LR161806,2020,GMC,Yukon XL,4WD SLT
1GKS2HKJ3LR151071,2020,GMC,Yukon XL,4WD Denali


Sierra 1500
3GTU9DELXKG189014,2019,GMC,Sierra 1500,SLT
1GTU9BED2KZ342398,2019,GMC,Sierra 1500,SLE
3GTP9EED1LG145853,2020,GMC,Sierra 1500,4WD Crew Cab Short Box AT4
1GTU9DEL1LZ257959,2020,GMC,Sierra 1500,4WD Crew Cab Standard Box SLT


Nissan Altima
1N4BL4BW0KN322514,2019,Nissan,Altima,2.5 S
1N4BL4CWXLN311441,2020,Nissan,Altima,SR Intelligent AWD
1N4BL4CW1LC186485,2020,Nissan,Altima,SR Intelligent AWD
1N4BL4CVXLC152441,2020,Nissan,Altima,SR FWD
1N4BL4BV7LC171143,2020,Nissan,Altima,S FWD


nissan frontier
1N6AD0EV8KN881417,2019,Nissan,Frontier,SV
1N6ED0EB6LN726860,2020,Nissan,Frontier,Crew Cab SV 4x4
1N6ED0EB0LN727518,2020,Nissan,Frontier,Crew Cab SV 4x4

nissan murano
5N1AZ2MS5KN132612,2019,Nissan,Murano,SL
5N1AZ2CS6LN159854,2020,Nissan,Murano,SL Intelligent AWD

Cadillac
1GYS4CKJ0KR302398,2019,Cadillac,Escalade,Premium Luxury
1GYS4BKJ0LR254378,2020,Cadillac,Escalade,4WD Luxury

1GYS4HKJ4KR329908,2019,Cadillac,Escalade ESV,Luxury
1GYS4JKJXLR141354,2020,Cadillac,Escalade ESV,4WD Premium Luxury
1GYS4HKJ2LR252022,2020,Cadillac,Escalade ESV,4WD Luxury

1GYFZDR43KF148103,2019,Cadillac,XT4,Premium Luxury
1GYFZFR4XLF043846,2020,Cadillac,XT4,AWD Sport
1GYFZFR46LF094356,2020,Cadillac,XT4,AWD Spor
1GYFZBR41LF132715,2020,Cadillac,XT4,AWD Luxury
1GYFZDR47LF084519,2020,Cadillac,XT4,AWD Premium Luxury
1GYFZDR41LF048633,2020,Cadillac,XT4,AWD Premium Luxury
1GYFZFR46LF053564,2020,Cadillac,XT4,AWD Sport

Chev

3GNKBHRS8KS700566,2019,Chevrolet,Blazer,Base w/3LT
3GNKBHRSXLS683710,2020,Chevrolet,Blazer,AWD 2LT

1G1FB3DS6K0152880,2019,Chevrolet,Camaro,1LT
1G1FH1R71L0149488,2020,Chevrolet,Camaro,RWD Coupe 2SS

1GCGTEEN4K1103975,2019,Chevrolet,Colorado,ZR2
1GCGTCEN7K1356129,2019,Chevrolet,Colorado,LT
1GCGTDEN9L1102669,2020,Chevrolet,Colorado,4WD Crew Cab Short Box Z71
1GCGTDEN3L1239171,2020,Chevrolet,Colorado,4WD Crew Cab Short Box Z71

3GNAXUEV4KS614273,2019,Chevrolet,Equinox,LT
3GNAXUEV4KS639478,2019,Chevrolet,Equinox,LT
2GNAXHEV8K6176153,2019,Chevrolet,Equinox,LS
3GNAXUEV2LS550767,2020,Chevrolet,Equinox,AWD LT 1.5L Turbo,AWD
3GNAXTEV8LS509442,2020,Chevrolet,Equinox,AWD 2FL

1G1ZG5ST6KF159069,2019,Chevrolet,Malibu,RS
1G1ZD5ST1KF127980,2019,Chevrolet,Malibu,LT
1G1ZG5ST5LF106588,2020,Chevrolet,Malibu,FWD RS
1G1ZD5ST4LF062804,2020,Chevrolet,Malibu,FWD LT

1GCUYGEDXKZ424072,2019,Chevrolet,Silverado 1500,LTZ
1GCRYDED1KZ188206,2019,Chevrolet,Silverado 1500,LT
1GCUYGEL8LZ275721,2020,Chevrolet,Silverado 1500,4WD Crew Cab Short Bed LTZ
1GCPYFED6LZ106278,2020,Chevrolet,Silverado 1500,4WD Crew Cab Short Bed LT Trail Boss

1GKKNXLS7KZ252895,2019,GMC,Acadia,Denali
1GKKNXLS7LZ153687,2020,GMC,Acadia,AWD Denali







