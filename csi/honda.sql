﻿
drop table if exists hn.mtd_hin_sales_make_csi_ext cascade;
create table hn.mtd_hin_sales_make_csi_ext (
  store_nps citext,
  zone_nps citext,
  national_nps citext,
  store_rec citext,
  zone_rec citext,
  national_rec citext);

drop table if exists hn.prev90_hin_sales_make_csi_ext cascade;
create table hn.prev90_hin_sales_make_csi_ext (
  store_nps citext,
  zone_nps citext,
  national_nps citext,
  store_rec citext,
  zone_rec citext,
  national_rec citext);

  
drop table if exists hn.hin_sales_make_csi cascade;
create table hn.hin_sales_make_csi (
  report_date date,
  entity citext,
  period citext,
  dealer_nps numeric,
  dealer_recommendation numeric,
  primary key(report_date,entity,period)); 
alter table hn.hin_sales_make_csi
alter column dealer_nps type citext;  
alter table hn.hin_sales_make_csi
alter column dealer_recommendation type citext;  
comment on table hn.hin_sales_make_csi is 'csi scores at the store level scraped from hin daily via luigi';

insert into hn.hin_sales_make_csi values('01/28/2021','store','mtd',100,99.4);  
insert into hn.hin_sales_make_csi values('01/28/2021','zone','mtd',90.6,96.8);  
insert into hn.hin_sales_make_csi values('01/28/2021','national','mtd',90.2,96.6);  

insert into hn.hin_sales_make_csi values('01/28/2021','store','prev90',100,null);  
insert into hn.hin_sales_make_csi values('01/28/2021','zone','prev90',90.6,null);  
insert into hn.hin_sales_make_csi values('01/28/2021','national','prev90',90.2,null); 

select * from hn.hin_sales_make_csi

create or replace function hn.hin_sale_make_csi_update()
returns void as
$BODY$
insert into hn.hin_sales_make_csi
select current_date,  'store', 'mtd', store_nps, store_rec
from hn.mtd_hin_sales_make_csi_ext
union
select current_date,  'zone', 'mtd', zone_nps, zone_rec
from hn.mtd_hin_sales_make_csi_ext
union
select current_date,  'national', 'mtd', national_nps, national_rec
from hn.mtd_hin_sales_make_csi_ext
union
select current_date,  'store', 'prev90', store_nps, nullif(store_rec, '-')
from hn.prev90_hin_sales_make_csi_ext
union
select current_date,  'zone', 'prev90', zone_nps, nullif(zone_rec, '-')
from hn.prev90_hin_sales_make_csi_ext
union
select current_date,  'national', 'prev90', national_nps, nullif(national_rec,'-')
from hn.prev90_hin_sales_make_csi_ext;
$BODY$
language sql;


drop table if exists hn.hin_service_make_csi cascade;
create table hn.hin_service_make_csi (
  report_date date,
  entity citext,
  period citext,
  cse_index numeric,
  primary key(report_date,entity,period)); 

alter table hn.hin_service_make_csi
alter column cse_index type citext;

comment on table hn.hin_service_make_csi is 'csi scores at the store level scraped from hin daily via luigi';

insert into hn.hin_service_make_csi values('01/28/2021','store','mtd',93.5);  
insert into hn.hin_service_make_csi values('01/28/2021','zone','mtd',92.4);  
insert into hn.hin_service_make_csi values('01/28/2021','national','mtd',92.3);  

insert into hn.hin_service_make_csi values('01/28/2021','store','prev90',94.6);  
insert into hn.hin_service_make_csi values('01/28/2021','zone','prev90',92.2);  
insert into hn.hin_service_make_csi values('01/28/2021','national','prev90',92.2);  

select * from hn.hin_service_make_csi

drop table if exists hn.mtd_hin_service_make_csi_ext cascade;
create table hn.mtd_hin_service_make_csi_ext (
  store_index citext,
  zone_index citext,
  national_index citext);

drop table if exists hn.prev90_hin_service_make_csi_ext cascade;
create table hn.prev90_hin_service_make_csi_ext (
  store_index citext,
  zone_index citext,
  national_index citext);

select * from hn.hin_service_make_csi

select * from hn.mtd_hin_service_make_csi_ext

select * from hn.prev90_hin_service_make_csi_ext


create or replace function hn.hin_service_make_csi_update()
returns void as
$BODY$
insert into hn.hin_service_make_csi
select current_date,  'store', 'mtd', store_index
from hn.mtd_hin_service_make_csi_ext
union
select current_date,  'zone', 'mtd', zone_index
from hn.mtd_hin_service_make_csi_ext
union
select current_date,  'national', 'mtd', national_index
from hn.mtd_hin_service_make_csi_ext
union
select current_date,  'store', 'prev90', store_index
from hn.prev90_hin_service_make_csi_ext
union
select current_date,  'zone', 'prev90', zone_index
from hn.prev90_hin_service_make_csi_ext
union
select current_date,  'national', 'prev90', national_index
from hn.prev90_hin_service_make_csi_ext
$BODY$
language sql;