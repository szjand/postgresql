﻿drop table if exists hn.nna_sales_make_csi_ext cascade;
create table hn.nna_sales_make_csi_ext (
  store_mtd integer,
  district_mtd  citext,
  store_qtd integer,
  tier citext,
  store_3mtd integer,
  district_3mtd citext);

drop table if exists hn.nna_sales_make_csi cascade;
create table hn.nna_sales_make_csi (
  report_date date not null,
  mtd integer,
  district  integer,
  qtd integer,
  qgp citext,
  primary key(report_date));
comment on table hn.nna_sales_make_csi is 'csi scores at the store level scraped from nnanet daily via luigi';

drop table if exists hn.nna_service_make_csi_ext cascade;
create table hn.nna_service_make_csi_ext (
  store_mtd integer,
  district_mtd  citext,
  store_qtd integer,
  tier citext,
  store_3mtd integer,
  district_3mtd citext);

drop table if exists hn.nna_service_make_csi cascade;
create table hn.nna_service_make_csi (
  report_date date not null,
  mtd integer,
  district  integer,
  qtd integer,
  qgp citext,
  primary key(report_date));
comment on table hn.nna_service_make_csi is 'csi scores at the store level scraped from nnanet daily via luigi';

insert into hn.nna_sales_make_csi values('01/28/2021',966,954,966,'QGP TIER 1');

insert into hn.nna_service_make_csi values('01/28/2021',852,912,852,'Below QGP Obj');


alter table hn.nna_sales_make_csi
rename column mtd to store_mtd;

alter table hn.nna_sales_make_csi
rename column district to district_mtd;

alter table hn.nna_sales_make_csi
rename column qtd to store_qtd;

alter table hn.nna_sales_make_csi
rename column qgp to tier;

alter table hn.nna_sales_make_csi
add column store_3mtd integer;

alter table hn.nna_sales_make_csi
add column district_3mtd integer;


alter table hn.nna_service_make_csi
rename column mtd to store_mtd;

alter table hn.nna_service_make_csi
rename column district to district_mtd;

alter table hn.nna_service_make_csi
rename column qtd to store_qtd;

alter table hn.nna_service_make_csi
rename column qgp to tier;

alter table hn.nna_service_make_csi
add column store_3mtd integer;

alter table hn.nna_service_make_csi
add column district_3mtd integer;



select * from hn.nna_sales_make_csi_ext
select * from hn.nna_sales_make_csi
hn.nna_service_make_csi_ext
hn.nna_service_make_csi


create or replace function hn.nna_sales_make_csi_update()
returns void as
$BODY$
insert into hn.nna_sales_make_csi
select current_date, store_mtd, replace(district_mtd, 'District= ', ''), store_qtd,
  tier, store_3mtd, replace(district_3mtd,'District= ','')
from hn.nna_sales_make_csi_ext;
$BODY$
language sql;

create or replace function hn.nna_service_make_csi_update()
returns void as
$BODY$
insert into hn.nna_service_make_csi
select current_date, store_mtd, replace(district_mtd, 'District= ', ''), store_qtd,
  tier, store_3mtd, replace(district_3mtd,'District= ','')
from hn.nna_service_make_csi_ext;
$BODY$
language sql;

/*
2/1/21
on the first, with no values, the fuckers use a dash, so i need to change the datatypes on all the 
numeric fields in both tables and the functions
*/

alter table hn.nna_sales_make_csi_ext
alter column store_mtd type citext,
alter column store_qtd type citext,
alter column store_3mtd type citext;

alter table hn.nna_sales_make_csi
alter column store_mtd type citext,
alter column store_qtd type citext,
alter column store_3mtd type citext,
alter column district_mtd type citext,
alter column district_3mtd type citext;

alter table hn.nna_service_make_csi_ext
alter column store_mtd type citext,
alter column store_qtd type citext,
alter column store_3mtd type citext;

alter table hn.nna_service_make_csi
alter column store_mtd type citext,
alter column store_qtd type citext,
alter column store_3mtd type citext,
alter column district_mtd type citext,
alter column district_3mtd type citext;