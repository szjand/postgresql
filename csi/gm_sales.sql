﻿comment on table gmgl.service_make_csi is 'daily download via luigi of global connect reports in reputation.com.
reports built by ben cahalan, 1 report for each make (buick, chev, gmc).
the field period is a bit misleading: when the value is "5120 CHV MN/ND" that decodes to current current quarter zone,
that is these rows are the current quarter scores for the zone. the other values in this column are 
actual periods: mtd, qtd & previous_quarter. feel free to complain to jon.'


drop table if exists gmgl.sales_make_csi_ext cascade;
create table gmgl.sales_make_csi_ext (
  date_range citext not null,
  blended_metric_tb citext,
  blended_metric_index citext,
  file_name citext not null,
  primary key(date_range,file_name));

 drop table if exists gmgl.sales_make_csi cascade;
 create table gmgl.sales_make_csi (
   report_date date not null, 
   period citext not null,
   make citext not null,
   blended_metric_tb citext,
   blended_metric_index citext,
   primary key(report_date,period,make));

comment on table gmgl.sales_make_csi is 'daily download via luigi of global connect reports in reputation.com.
reports built by ben cahalan, 1 report for each make (buick, chev, gmc).
the field period is a bit misleading: when the value is "5120 CHV MN/ND" that decodes to current quarter zone,
that is these rows are the current quarter scores for the zone. the other values in this column are 
actual periods: mtd, qtd & previous_quarter. feel free to complain to jon.'

-- doing intial load of data from the spreadsheets to get data for afton to work with
-- the luigi scripts are in process and will be complete shortly
insert into gmgl.sales_make_csi values('01/28/2021','5120 CHV MN/ND','buick','92.91','96.75');
insert into gmgl.sales_make_csi values('01/28/2021','12mth','buick','90','95.36');
insert into gmgl.sales_make_csi values('01/28/2021','mtd','buick','100','100');
insert into gmgl.sales_make_csi values('01/28/2021','previous_quarter','buick','92.83','95.33');
insert into gmgl.sales_make_csi values('01/28/2021','qtd','buick','100','100');
insert into gmgl.sales_make_csi values('01/28/2021','3mth','buick','92.83','95.33');



insert into gmgl.sales_make_csi values('01/28/2021','5120 CHV MN/ND','chevy','84.82','93.4');
insert into gmgl.sales_make_csi values('01/28/2021','previous_quarter','chevy','92.99','96.95');
insert into gmgl.sales_make_csi values('01/28/2021','mtd','chevy','85.77','93.33');
insert into gmgl.sales_make_csi values('01/28/2021','12mth','chevy','88.93','95.23');
insert into gmgl.sales_make_csi values('01/28/2021','3mth','chevy','92.99','96.95');
insert into gmgl.sales_make_csi values('01/28/2021','qtd','chevy','85.77','93.33');



insert into gmgl.sales_make_csi values('01/28/2021','5120 CHV MN/ND','gmc','87.27','94.26');
insert into gmgl.sales_make_csi values('01/28/2021','previous_quarter','gmc','91.4','96.01');
insert into gmgl.sales_make_csi values('01/28/2021','qtd','gmc','96.48','98.18');
insert into gmgl.sales_make_csi values('01/28/2021','3mth','gmc','91.4','96.01');
insert into gmgl.sales_make_csi values('01/28/2021','12mth','gmc','93.39','96.99');
insert into gmgl.sales_make_csi values('01/28/2021','mtd','gmc','96.48','98.18');


select * from gmgl.sales_make_csi where report_date = current_date 
-- shit don't know why it fucked up today

insert into gmgl.sales_make_csi values('01/30/2021','12mth','buick','90','95.36');
insert into gmgl.sales_make_csi values('01/30/2021','mtd','buick','100','100');
insert into gmgl.sales_make_csi values('01/30/2021','previous_quarter','buick','92.83','95.33');
insert into gmgl.sales_make_csi values('01/30/2021','qtd','buick','100','100');
insert into gmgl.sales_make_csi values('01/30/2021','3mth','buick','92.83','95.33');