﻿need to limit the time window to coincide with the times when the task runs
as well as the dates, not all tasks run all days
1. CompliReports
2. homenet new car feed

---------------------------------------------------------------------------------------------------------
--< 1. CompliReports
---------------------------------------------------------------------------------------------------------
every 20 minutes 7AM to 7PM
every day
/*
do i check from_ts or thru_ts
what exactly am i testing
i actually don't care about pass or fail, if the task fails, luigi will send an email
what i want to know, is simply, has the task run when it was suppose to
therefore from_ts

1st simplistic attempt:
  select case when now() - max(from_ts) > '00:21:00'::time then 1 else 0 end as failed
  from luigi.luigi_log
  where task = 'CompliReports'
    and the_date = current_date
    and from_ts::time between '07:30:00'::time and '19:00:00'::time
what if it never ran - this would not detect it

2nd attempt:
  select 
    case
      when extract (epoch from (now() - coalesce(max(from_ts), (now() - interval '1 day'))))/60 > 39 then 1
      else 0
    end as failed
  from luigi.luigi_log
  where task = 'CompliReports'
    and the_date = current_date -- nor do i need this, all i am looking at is from_ts where task = complireports
    and from_ts::time between '07:00:00'::time and '19:00:00'::time  -- don't actually need this
    
what this overlooks is that the cronjob runs every 5 minutes between 5AM and 7PM
so what does this query return at 6AM?
have to accomodate those run times in the case statement, that is always return 0 (pass) except for within
the relevant time frame
same thing with dates for those jobs that don't run every day

3rd attempt, thought i had it
select 
  case when now()::time between '07:41:00' and '19:00:00' then  
    case
      when extract (epoch from (now() - coalesce(max(from_ts), (now() - interval '1 day'))))/60 > 39 then 1
    end
    else 0
  end as failed
from luigi.luigi_log
where task = 'CompliReports'

turns out the handling of a null from_ts is intermittent, sometimes works, sometimes not
  
so the first case is
  within the given time frame, the difference between the max from_ts and now should always be < 20 minutes
*/

-- returns 1 when failed, 0 when passed
-- the outer case: the time frame in which the CompliReports cronjob runs, built in slack for the start of the day
-- the actual test forgives one missed run: 

select 
  case when now()::time between '07:41:00' and '19:00:00' then  
    case
      when extract (epoch from (now() - coalesce(max(from_ts), (now() - interval '1 day'))))/60 > 39 then 1
    end
    else 0
  end as pass_fail
from luigi.luigi_log
where task = 'CompliReports'


select *
from luigi.luigi_log
where task = 'CompliReports'
  and the_date = current_date
order by from_ts desc   

--------------------------------------------------------------------------------------------------------
--/> 1. CompliReports
---------------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------------
--< 2. homenet new car feed
---------------------------------------------------------------------------------------------------------
monday thru saturday @ 7 past the hour from 9AM to 9PM


select a.*, day_of_week, day_name
from luigi.luigi_log a
join dds.dim_date b on a.the_date = b.the_date
where a.pipeline = 'internet_feeds'
  and a.task = 'EmailFile'
  and a.the_date > current_date - 10
order by from_ts desc  


select 
  case
    when max(b.day_of_week) in (2,3,4,5,6,7) and now()::time between '10:00:00'::time and '21:00:00'::time then
    case
      when extract (epoch from (now() - coalesce(max(from_ts), (now() - interval '1 day'))))/60 > 100 then 1
    end
    else 0
  end as failed    
from luigi.luigi_log a
join dds.dim_date b on a.the_date = b.the_date
  and b.the_date = current_date
where a.pipeline = 'internet_feeds'
  and a.task = 'EmailFile';

select now(), max(from_ts), 
  now() - coalesce(max(from_ts), 
  (now() - interval '1 day'))
from luigi.luigi_log a
join dds.dim_date b on a.the_date = b.the_date
where a.pipeline = 'internet_feeds'
  and a.task = 'EmailFile'
  and a.the_date = current_date + 1
---------------------------------------------------------------------------------------------------------
--< 2. homenet new car feed
---------------------------------------------------------------------------------------------------------






