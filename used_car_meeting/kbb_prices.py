# encoding=utf-8
"""
5:00 AM Daily on 10.139.196.15 scheduled task that scrapes vauto used cars
and populates vauto.ben_used_car_pricing
This script pulls the kbb info from that scrapes, sticks it in a table, dpsveries.vauto_kbb_price,
and creates a row in dpsvseries.vehiclethirdpartyvalues
for that vehicle
since i can no longer get ads to work consistently in linux (hence no luigi),
this is run as a scheduled task on 10.130.196.22 daily at 5:30 AM
"""
import db_cnx
import smtplib
from email.mime.text import MIMEText

pg_con = None
ads_con = None


def email_results(_task,  _error):
    body = str(_error)
    message = MIMEText(body)
    message['Subject'] = _task
    smtp_server = 'mail.cartiva.com'
    e = smtplib.SMTP(smtp_server)
    addr_to = 'jandrews@cartiva.com'
    addr_from = 'jandrews@cartiva.com'
    message['To'] = addr_to
    message['From'] = addr_from
    receivers = ['jandrews@cartiva.com']
    e.sendmail(addr_from, receivers, message.as_string())
    e.quit()


try:
    # verify that vauto scrape was successful
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                select count(*)
                from vauto.ben_used_car_pricing
                where lookup_date = current_date;
            """
            pg_cur.execute(sql)
            if pg_cur.fetchone()[0] == 0:
                raise ValueError('No current data in vauto.ben_used_car_pricing')
    # empty vauto_kbb_price
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            ads_cur.execute("delete from vauto_kbb_price")
    # get kbb data into ads
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                select stock_number, kbb_retail_price
                from vauto.ben_used_car_pricing
                where lookup_date = current_date
                  and kbb_retail_price is not null;
            """
            pg_cur.execute(sql)
            for row in pg_cur.fetchall():
                with db_cnx.ads_dps() as ads_con:
                    with ads_con.cursor() as ads_cur:
                        ads_cur.execute("""
                            insert into vauto_kbb_price
                            values ('{0}', {1})
                        """.format(row[0], row[1]))
                        sql = """
                            INSERT INTO vehiclethirdpartyvalues (typ,tablekey,basistable,
                              amount,VehicleInventoryItemID, VehicleItemID,VehicleThirdPartyValueTS)
                            SELECT 'VehicleThirdPartyValue_KelleyBlueBookFair', c.VehicleEvaluationID, 
                              'VehicleEvaluations', round(.97 * a.kbb_retail_price, 0), 
                              b.VehicleInventoryItemID, b.VehicleItemID, now()    
                            from  vauto_kbb_price a
                            inner join VehicleInventoryItems b on a.stock_number = b.stocknumber
                            inner join VehicleEvaluations c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
                            LEFT JOIN vehicleThirdPartyValues d on b.VehicleInventoryItemID = d.VehicleInventoryItemID 
                              AND d.typ = 'VehicleThirdPartyValue_KelleyBlueBookFair'
                            WHERE b.thruts IS null  
                              AND d.VehicleInventoryItemID IS NULL;                        
                        """
                        ads_cur.execute(sql)
    email_results('KBB PASS', 'Success')
except Exception as error:
    email_results('KBB FAIL', error)
finally:
    if pg_con:
        pg_con.close()
