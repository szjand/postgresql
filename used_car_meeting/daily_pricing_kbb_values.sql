﻿with
  the_cost as (
    select a.inpmast_stock_number, a.inpmast_vin, a.inventory_account, a.inpmast_vehicle_cost::integer as the_cost
    from arkona.xfm_inpmast a
    where a.current_row = true
      and a.status = 'I'
      and a.type_n_u = 'U'
    group by a.inpmast_stock_number, a.inpmast_vin, a.inventory_account, a.inpmast_vehicle_cost)
select a.stocknumber, aa.vin, aa.yearmodel, aa.make, aa.model, a.fromts::date, d.the_cost, (.97 *c.kbb_retail_price)::integer as kbb_retail_97
from ads.ext_vehicle_inventory_items a
inner join ads.ext_vehicle_items aa on a.vehicleitemid = aa.vehicleitemid
inner join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and b.category = 'RMFlagWP'
  and b.thruts::date > current_date
left join vauto.ben_used_car_pricing c on a.stocknumber = c.stock_number
  and c.lookup_date = current_date
left join the_cost d on a.stocknumber = d.inpmast_stock_number
where a.thruts::date > current_date
  and current_date - a.fromts::date < 31
  and a.stocknumber not like 'H%'
--   and c.kbb_retail_price is not null
order by a.stocknumber  




select a.stocknumber, aa.vin, aa.yearmodel, aa.make, aa.model, a.fromts::date, d.the_cost, (.97 *c.kbb_retail_price)::integer as kbb_retail_97
from ads.ext_vehicle_inventory_items a
inner join ads.ext_vehicle_items aa on a.vehicleitemid = aa.vehicleitemid
inner join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and b.category = 'RMFlagWP'
  and b.thruts::date > current_date
left join vauto.ben_used_car_pricing c on a.stocknumber = c.stock_number
  and c.lookup_date = current_date
left join (
    select a.inpmast_stock_number, a.inpmast_vin, a.inventory_account, a.inpmast_vehicle_cost::integer as the_cost
    from arkona.xfm_inpmast a
    where a.current_row = true
      and a.status = 'I'
      and a.type_n_u = 'U'
    group by a.inpmast_stock_number, a.inpmast_vin, a.inventory_account, a.inpmast_vehicle_cost) d on a.stocknumber = d.inpmast_stock_number
where a.thruts::date > current_date
  and current_date - a.fromts::date < 31
  and a.stocknumber not like 'H%'
--   and c.kbb_retail_price is not null
order by a.stocknumber  

download vehicle third party values nightly
only insert where no row in vehiclethirdpartyvalues


select 'INSERT INTO vehiclethirdpartyvalues SELECT ''VehicleThirdPartyValue_KelleyBlueBookFair'',' 
  || '''' || vehicleevaluationid || ''','
  || '''VehicleEvaluations'',' 
  || kbb_fair || ','
  || '''' || vehicleinventoryitemid || ''','
  || '''' || vehicleitemid || ''','
  || 'now() '
  || 'from system.iota;'
from (
select a.stock_number, a.vin, (.97 * a.kbb_retail_price)::integer as kbb_fair, 
  b.vehicleinventoryitemid,b.vehicleitemid, c.vehicleevaluationid
from  vauto.ben_used_car_pricing a
inner join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
inner join ads.ext_vehicle_evaluations c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
where a.lookup_date = current_date
  and a.kbb_Retail_price is not null
  and a.stock_number <> 'g33293b'
order by a.stock_number) x



select * from vauto.ben_used_car_pricing limit 100

-- 8/22 something is fucked up, no data in vauto.ben_used_car_pricing for 8/22
-- but, this is wacky
-- don't really know what is currently in vauto.ext_ben_used_car_pricing, but it does not agree with what i put in third party values yesterday
-- again, i don't really know wtf afton is doing
with
  kbb_scrape as (
    select replace(age,'.0','')::integer, body, color, vehicle, stock_number, vin, replace(replace(odometer,',',''),'.0','')::integer,replace(replace(replace(current_price,'$',''),',',''),'.0','')::integer,
     case when kbb_retail_price = '-' then null else replace(replace(replace(kbb_retail_price,'$',''),',',''),'.0','')::integer end as kbb,
     case when mmr_wholesale = '-' then null else replace(replace(replace(mmr_wholesale,'$',''),',',''),'.0','')::integer end, current_date
    from vauto.ext_ben_used_car_pricing)
select a.stock_number, a.kbb, c.amount
from kbb_scrape a
left join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
left join ads.ext_vehicle_third_party_values c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
  and c.typ = 'VehicleThirdPartyValue_KelleyBlueBookFair'


-- but this looks ok
 select stock_number,
   max(case when lookup_date = '08/20/2018' then kbb_retail_price end) as aug_20, 
   max(case when lookup_date = '08/21/2018' then kbb_retail_price end) as aug_21,
   max(case when lookup_date = '08/22/2018' then kbb_retail_price end) as aug_22,
   max(case when lookup_date = '08/23/2018' then kbb_retail_price end) as aug_23,
   max(case when lookup_date = '08/24/2018' then kbb_retail_price end) as aug_24,
   max(case when lookup_date = '08/25/2018' then kbb_retail_price end) as aug_25,
   max(case when lookup_date = '08/26/2018' then kbb_retail_price end) as aug_26,
   max(case when lookup_date = '08/27/2018' then kbb_retail_price end) as aug_27,
   max(case when lookup_date = '08/28/2018' then kbb_retail_price end) as aug_28
 from vauto.ben_used_car_pricing
 group by stock_number 
 order by stock_number


-- 8/23 afton's scrape is having problems

select lookup_date, count(*)
from vauto.ben_used_car_pricing
group by lookup_date


select *
from vauto.ben_used_car_pricing
where lookup_date = current_date 


select *
from vauto.ext_ben_used_car_pricing