﻿-- pretty fucking close: inpmast_cost matching total accounting in inventory accounts
drop table if exists the_cost;
create temp table the_cost as
select b.store_code, a.inpmast_stock_number, a.inpmast_vin, a.inventory_account, a.inpmast_vehicle_cost, sum(c.amount)
from arkona.xfm_inpmast a
inner join fin.dim_account b on a.inventory_account = b.account
left join fin.fact_gl c on a.inpmast_stock_number = c.control
  and c.post_status = 'Y'
  and b.account_key = c.account_key
where a.current_row = true
  and a.status = 'I'
  and a.type_n_u = 'U'
group by b.store_code, a.inpmast_stock_number, a.inpmast_vin, a.inventory_account, a.inpmast_vehicle_cost; 

-- only 4 mismatches
select *
from (
  select a.inpmast_stock_number, a.inpmast_vin, a.inventory_account, a.inpmast_vehicle_cost, sum(c.amount) as amount
  from arkona.xfm_inpmast a
  inner join fin.dim_account b on a.inventory_account = b.account
  left join fin.fact_gl c on a.inpmast_stock_number = c.control
    and c.post_status = 'Y'
    and b.account_key = c.account_key
  where a.current_row = true
    and a.status = 'I'
    and a.type_n_u = 'U'
  group by a.inpmast_stock_number, a.inpmast_vin, a.inventory_account, a.inpmast_vehicle_cost ) x 
where inpmast_vehicle_cost <> amount

-- available
drop table if exists available;
create temp table available as
select  a.stocknumber, b.vehiclepricingts, c.amount as best_price, current_date - d.fromts::date as age
from ads.ext_vehicle_inventory_items a
inner join  ads.ext_vehicle_inventory_item_statuses d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
  and d.status = 'RMFlagAV_Available'
  and d.thruts > current_date
left join ads.ext_vehicle_pricings b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
inner join lateral(
  select vehicleinventoryitemid, max(vehiclepricingts) as vehiclepricingts
  from ads.ext_vehicle_pricings
  where vehicleinventoryitemid =  b.vehicleinventoryitemid
  group by vehicleinventoryitemid) bb on b.vehicleinventoryitemid = bb.vehicleinventoryitemid
    and b.vehiclepricingts = bb.vehiclepricingts
left join ads.ext_vehicle_pricing_details c on b.vehiclepricingid = c.vehiclepricingid
  and c.typ = 'VehiclePricingDetail_BestPrice'
where a.thruts > current_date;
create unique index on available(stocknumber);

select * from available order by stocknumber

-- raw materials
drop table if exists raw_materials;
create temp table raw_materials as
select  a.stocknumber, b.vehiclepricingts, c.amount as best_price, current_date - d.fromts::date as age
from ads.ext_vehicle_inventory_items a
inner join  ads.ext_vehicle_inventory_item_statuses d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
  and d.status = 'RMFlagRMP_RawMaterialsPool'
  and d.thruts > current_date
left join ads.ext_vehicle_pricings b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
inner join lateral(
  select vehicleinventoryitemid, max(vehiclepricingts) as vehiclepricingts
  from ads.ext_vehicle_pricings
  where vehicleinventoryitemid =  b.vehicleinventoryitemid
  group by vehicleinventoryitemid) bb on b.vehicleinventoryitemid = bb.vehicleinventoryitemid
    and b.vehiclepricingts = bb.vehiclepricingts
left join ads.ext_vehicle_pricing_details c on b.vehiclepricingid = c.vehiclepricingid
  and c.typ = 'VehiclePricingDetail_BestPrice'
where a.thruts > current_date
  AND NOT EXISTS (
    SELECT 1 
    FROM ads.ext_vehicle_inventory_item_statuses
    WHERE VehicleInventoryItemID = d.VehicleInventoryItemID
    AND status = 'RMFlagRB_ReconBuffer'
    AND ThruTS > current_date)
  AND NOT EXISTS (
    SELECT 1 
    FROM ads.ext_vehicle_inventory_item_statuses
    WHERE VehicleInventoryItemID = d.VehicleInventoryItemID
    AND category = 'RMFlagSB'
    AND ThruTS > current_date);
create unique index on raw_materials(stocknumber);

select distinct status from ads.ext_vehicle_inventory_item_statuses order by status

select *, best_price - inpmast_vehicle_cost, count(stocknumber) over (partition by age)
from available a
inner join the_cost b on a.stocknumber = b.inpmast_stock_number
where store_code = 'RY1'
order by age, stocknumber

select 'avail', store_code, 
  count(stocknumber) filter (where age < 31) as count_0_to_30,
  sum(best_price - inpmast_vehicle_cost) filter (where age < 31) as pot_lot_gross_0_to_30,
  count(stocknumber) filter (where age between 31 and 60) as count_31_to_60,
  sum(best_price - inpmast_vehicle_cost) filter (where age between 31 and 60) as pot_lot_gross_31_to_60,
      count(stocknumber) filter (where age > 60) as count_over_60,
  sum(best_price - inpmast_vehicle_cost) filter (where age > 60) as pot_lot_gross_over_60
from available a
inner join the_cost b on a.stocknumber = b.inpmast_stock_number
group by store_code
  


select *, best_price - inpmast_vehicle_cost, count(stocknumber) over (partition by age)
from raw_materials a
inner join the_cost b on a.stocknumber = b.inpmast_stock_number
where age < 31
  and store_code = 'RY1'
order by age, stocknumber

select 'raw', store_code, 
  count(stocknumber) filter (where age < 31) as count_0_to_30,
  sum(best_price - inpmast_vehicle_cost) filter (where age < 31) as pot_lot_gross_0_to_30,
  count(stocknumber) filter (where age between 31 and 60) as count_31_to_60,
  sum(best_price - inpmast_vehicle_cost) filter (where age between 31 and 60) as pot_lot_gross_31_to_60,
      count(stocknumber) filter (where age > 60) as count_over_60,
  sum(best_price - inpmast_vehicle_cost) filter (where age > 60) as pot_lot_gross_over_60
from raw_materials a
inner join the_cost b on a.stocknumber = b.inpmast_stock_number
group by store_code


-- dave's summary
select 'avail', store_code, 
  count(stocknumber) filter (where age < 31) as count_0_to_30,
  sum(best_price - inpmast_vehicle_cost) filter (where age < 31) as pot_lot_gross_0_to_30,
  count(stocknumber) filter (where age between 31 and 60) as count_31_to_60,
  sum(best_price - inpmast_vehicle_cost) filter (where age between 31 and 60) as pot_lot_gross_31_to_60,
      count(stocknumber) filter (where age > 60) as count_over_60,
  sum(best_price - inpmast_vehicle_cost) filter (where age > 60) as pot_lot_gross_over_60
from available a
inner join the_cost b on a.stocknumber = b.inpmast_stock_number
group by store_code
union
select 'raw', store_code, 
  count(stocknumber) filter (where age < 31) as count_0_to_30,
  sum(best_price - inpmast_vehicle_cost) filter (where age < 31) as pot_lot_gross_0_to_30,
  count(stocknumber) filter (where age between 31 and 60) as count_31_to_60,
  sum(best_price - inpmast_vehicle_cost) filter (where age between 31 and 60) as pot_lot_gross_31_to_60,
      count(stocknumber) filter (where age > 60) as count_over_60,
  sum(best_price - inpmast_vehicle_cost) filter (where age > 60) as pot_lot_gross_over_60
from raw_materials a
inner join the_cost b on a.stocknumber = b.inpmast_stock_number
group by store_code

-- dave's detail
select 
  case
    when b.store_code = 'RY1' then 'GM'
    else 'Honda'
  end as store, a.stocknumber, 'available' as status, a.age, a.best_price, b.inpmast_vehicle_cost, best_price - inpmast_vehicle_cost as pot_lot_gross
from available a
inner join the_cost b on a.stocknumber = b.inpmast_stock_number
union
select 
  case
    when b.store_code = 'RY1' then 'GM'
    else 'Honda'
  end as store, a.stocknumber, 'raw materials', a.age, a.best_price, b.inpmast_vehicle_cost, best_price - inpmast_vehicle_cost as pot_lot_gross
from raw_materials a
inner join the_cost b on a.stocknumber = b.inpmast_stock_number
order by store, status, age


-- 3/26/18
/*
problems problems problems
last minute wilkie showed up wanting the potential lot gross report
got it to him,
1. after the meeting he waid numbers were wrong, don't think he realizes
that outlet shows up in ry1 numbers

2. cahalan wants outstanding est auth recon subtracted from gross

3. crayon report (GetGlumpExtract_New) outlet graphs and numbers fucked up
rj asked about including outlet into ry1
not sure if i can
not sure what exactly it is that they want

regardless need to rethink the report, automate the generation of it at least
*/


-- 4/1/18  recon
-- need to look at detailed data

SELECT a.stocknumber, a.VehicleInventoryItemID, ari.Typ, ari.Status,
  vri.TotalPartsAmount,vri.LaborAmount, vri.*  
FROM ads.ext_Recon_Authorizations ra
inner JOIN ads.ext_Authorized_Recon_Items ari ON ari.ReconAuthorizationID = ra.ReconAuthorizationID
INNER JOIN ads.ext_Vehicle_Recon_Items vri ON vri.VehicleReconItemID = ari.VehicleReconItemID  
INNER JOIN ads.ext_Vehicle_Inventory_Items a on ra.VehicleInventoryItemID = a.VehicleInventoryItemID 
  AND a.thruts::date > current_date
WHERE ra.ThruTS::date is null
  and ari.status <> 'AuthorizedReconItem_Complete'
order by stocknumber

-- total by vehicle
SELECT a.stocknumber, a.VehicleInventoryItemID,
  sum(vri.TotalPartsAmount + vri.LaborAmount)  
FROM ads.ext_Recon_Authorizations ra
inner JOIN ads.ext_Authorized_Recon_Items ari ON ari.ReconAuthorizationID = ra.ReconAuthorizationID
INNER JOIN ads.ext_Vehicle_Recon_Items vri ON vri.VehicleReconItemID = ari.VehicleReconItemID  
INNER JOIN ads.ext_Vehicle_Inventory_Items a on ra.VehicleInventoryItemID = a.VehicleInventoryItemID 
  AND a.thruts::date > current_date
WHERE ra.ThruTS::date is null
  and ari.status <> 'AuthorizedReconItem_Complete'
GROUP BY a.stocknumber, a.VehicleInventoryItemID
order by stocknumber

--------------------------------------------------------------------------------------------------------
-- 4/8/18 here are all the tables
--------------------------------------------------------------------------------------------------------
-- open recon
drop table if exists open_recon;
create temp table open_recon as
SELECT a.stocknumber,
  sum(vri.TotalPartsAmount + vri.LaborAmount) as open_recon
FROM ads.ext_Recon_Authorizations ra
inner JOIN ads.ext_Authorized_Recon_Items ari ON ari.ReconAuthorizationID = ra.ReconAuthorizationID
INNER JOIN ads.ext_Vehicle_Recon_Items vri ON vri.VehicleReconItemID = ari.VehicleReconItemID  
INNER JOIN ads.ext_Vehicle_Inventory_Items a on ra.VehicleInventoryItemID = a.VehicleInventoryItemID 
  AND a.thruts::date > current_date
WHERE ra.ThruTS::date is null
  and ari.status <> 'AuthorizedReconItem_Complete'
GROUP BY a.stocknumber;

drop table if exists the_cost;
create temp table the_cost as
select b.store_code, a.inpmast_stock_number, a.inpmast_vin, a.inventory_account, a.inpmast_vehicle_cost, sum(c.amount)
from arkona.xfm_inpmast a
inner join fin.dim_account b on a.inventory_account = b.account
left join fin.fact_gl c on a.inpmast_stock_number = c.control
  and c.post_status = 'Y'
  and b.account_key = c.account_key
where a.current_row = true
  and a.status = 'I'
  and a.type_n_u = 'U'
group by b.store_code, a.inpmast_stock_number, a.inpmast_vin, a.inventory_account, a.inpmast_vehicle_cost; 

-- available
drop table if exists available;
create temp table available as
select  a.stocknumber, b.vehiclepricingts, c.amount as best_price, current_date - d.fromts::date as age
from ads.ext_vehicle_inventory_items a
inner join  ads.ext_vehicle_inventory_item_statuses d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
  and d.status = 'RMFlagAV_Available'
  and d.thruts > current_date
left join ads.ext_vehicle_pricings b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
inner join lateral(
  select vehicleinventoryitemid, max(vehiclepricingts) as vehiclepricingts
  from ads.ext_vehicle_pricings
  where vehicleinventoryitemid =  b.vehicleinventoryitemid
  group by vehicleinventoryitemid) bb on b.vehicleinventoryitemid = bb.vehicleinventoryitemid
    and b.vehiclepricingts = bb.vehiclepricingts
left join ads.ext_vehicle_pricing_details c on b.vehiclepricingid = c.vehiclepricingid
  and c.typ = 'VehiclePricingDetail_BestPrice'
where a.thruts > current_date;
create unique index on available(stocknumber);

-- raw materials
drop table if exists raw_materials;
create temp table raw_materials as
select  a.stocknumber, b.vehiclepricingts, c.amount as best_price, current_date - d.fromts::date as age
from ads.ext_vehicle_inventory_items a
inner join  ads.ext_vehicle_inventory_item_statuses d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
  and d.status = 'RMFlagRMP_RawMaterialsPool'
  and d.thruts > current_date
left join ads.ext_vehicle_pricings b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
inner join lateral(
  select vehicleinventoryitemid, max(vehiclepricingts) as vehiclepricingts
  from ads.ext_vehicle_pricings
  where vehicleinventoryitemid =  b.vehicleinventoryitemid
  group by vehicleinventoryitemid) bb on b.vehicleinventoryitemid = bb.vehicleinventoryitemid
    and b.vehiclepricingts = bb.vehiclepricingts
left join ads.ext_vehicle_pricing_details c on b.vehiclepricingid = c.vehiclepricingid
  and c.typ = 'VehiclePricingDetail_BestPrice'
where a.thruts > current_date
  AND NOT EXISTS (
    SELECT 1 
    FROM ads.ext_vehicle_inventory_item_statuses
    WHERE VehicleInventoryItemID = d.VehicleInventoryItemID
    AND status = 'RMFlagRB_ReconBuffer'
    AND ThruTS > current_date)
  AND NOT EXISTS (
    SELECT 1 
    FROM ads.ext_vehicle_inventory_item_statuses
    WHERE VehicleInventoryItemID = d.VehicleInventoryItemID
    AND category = 'RMFlagSB'
    AND ThruTS > current_date);
create unique index on raw_materials(stocknumber);

-- dave's summary  including open recon
drop table if exists dave_summary;
create temp table dave_summary as
select 'avail' as status, store_code, 
  count(a.stocknumber) filter (where age < 31) as count_0_to_30,
  (sum(best_price - inpmast_vehicle_cost - coalesce(c.open_recon, 0)) filter (where age < 31))::integer as pot_lot_gross_0_to_30,
  count(a.stocknumber) filter (where age between 31 and 60) as count_31_to_60,
  (sum(best_price - inpmast_vehicle_cost - coalesce(c.open_recon, 0)) filter (where age between 31 and 60))::integer as pot_lot_gross_31_to_60,
  count(a.stocknumber) filter (where age > 60) as count_over_60,
  (sum(best_price - inpmast_vehicle_cost - coalesce(c.open_recon, 0)) filter (where age > 60))::integer as pot_lot_gross_over_60
from available a
inner join the_cost b on a.stocknumber = b.inpmast_stock_number
left join open_recon c on a.stocknumber = c.stocknumber
group by store_code
union
select 'raw', store_code, 
  count(a.stocknumber) filter (where age < 31) as count_0_to_30,
  (sum(best_price - inpmast_vehicle_cost - coalesce(c.open_recon, 0)) filter (where age < 31))::integer as pot_lot_gross_0_to_30,
  count(a.stocknumber) filter (where age between 31 and 60) as count_31_to_60,
  (sum(best_price - inpmast_vehicle_cost - coalesce(c.open_recon, 0)) filter (where age between 31 and 60))::integer as pot_lot_gross_31_to_60,
  count(a.stocknumber) filter (where age > 60) as count_over_60,
  (sum(best_price - inpmast_vehicle_cost - coalesce(c.open_recon, 0)) filter (where age > 60))::integer as pot_lot_gross_over_60
from raw_materials a
inner join the_cost b on a.stocknumber = b.inpmast_stock_number
left join open_recon c on a.stocknumber = c.stocknumber
group by store_code;


-- RY1
select 'Available 0 - 30' as status, count_0_to_30 as units, pot_lot_gross_0_to_30 as pot_lot_gross, pot_lot_gross_0_to_30/count_0_to_30 as "gross/unit"
from dave_summary 
where status = 'avail'
  and store_code = 'RY1'
union
select 'Available 31 - 60' as status, count_31_to_60 as units, pot_lot_gross_31_to_60 as pot_lot_gross, pot_lot_gross_31_to_60/count_31_to_60 as "gross/unit"
from dave_summary 
where status = 'avail'
  and store_code = 'RY1'
union
select 'Available > 60' as status, count_over_60 as units, pot_lot_gross_over_60 as pot_lot_gross, pot_lot_gross_over_60/count_over_60 as "gross/unit"
from dave_summary 
where status = 'avail'
  and store_code = 'RY1'
union  
select 'Raw Materials 0 - 30' as status, count_0_to_30 as units, pot_lot_gross_0_to_30 as pot_lot_gross, pot_lot_gross_0_to_30/count_0_to_30 as "gross/unit"
from dave_summary 
where status = 'raw'
  and store_code = 'RY1'
union
select 'Raw Materials 31 - 60' as status, count_31_to_60 as units, pot_lot_gross_31_to_60 as pot_lot_gross, pot_lot_gross_31_to_60/count_31_to_60 as "gross/unit"
from dave_summary 
where status = 'raw'
  and store_code = 'RY1'
union
select 'Raw Materials > 60' as status, count_over_60 as units, pot_lot_gross_over_60 as pot_lot_gross, pot_lot_gross_over_60/count_over_60 as "gross/unit"
from dave_summary 
where status = 'raw'
  and store_code = 'RY1'
order by status  

-- ry2
select 'Available 0 - 30' as status, count_0_to_30 as units, pot_lot_gross_0_to_30 as pot_lot_gross, pot_lot_gross_0_to_30/count_0_to_30 as "gross/unit"
from dave_summary 
where status = 'avail'
  and store_code = 'RY2'
union
select 'Available 31 - 60' as status, count_31_to_60 as units, pot_lot_gross_31_to_60 as pot_lot_gross, pot_lot_gross_31_to_60/count_31_to_60 as "gross/unit"
from dave_summary 
where status = 'avail'
  and store_code = 'RY2'
union
select 'Available > 60' as status, count_over_60 as units, pot_lot_gross_over_60 as pot_lot_gross, pot_lot_gross_over_60/count_over_60 as "gross/unit"
from dave_summary 
where status = 'avail'
  and store_code = 'RY2'
union  
select 'Raw Materials 0 - 30' as status, count_0_to_30 as units, pot_lot_gross_0_to_30 as pot_lot_gross, pot_lot_gross_0_to_30/count_0_to_30 as "gross/unit"
from dave_summary 
where status = 'raw'
  and store_code = 'RY2'
union
select 'Raw Materials 31 - 60' as status, count_31_to_60 as units, pot_lot_gross_31_to_60 as pot_lot_gross, pot_lot_gross_31_to_60/count_31_to_60 as "gross/unit"
from dave_summary 
where status = 'raw'
  and store_code = 'RY2'
union
select 'Raw Materials > 60' as status, count_over_60 as units, pot_lot_gross_over_60 as pot_lot_gross, pot_lot_gross_over_60/count_over_60 as "gross/unit"
from dave_summary 
where status = 'raw'
  and store_code = 'RY2'
order by status  



-- for autogenerating
-- RY1
select *
from (
select 'GM' as store, null  as status, null as units, null as pot_lot_gross, null as "gross/unit"
union
select null, 'Available 0 - 30' as status, count_0_to_30 as units, pot_lot_gross_0_to_30 as pot_lot_gross, pot_lot_gross_0_to_30/count_0_to_30 as "gross/unit"
from dave_summary 
where status = 'avail'
  and store_code = 'RY1'
union
select null, 'Available 31 - 60' as status, count_31_to_60 as units, pot_lot_gross_31_to_60 as pot_lot_gross, pot_lot_gross_31_to_60/count_31_to_60 as "gross/unit"
from dave_summary 
where status = 'avail'
  and store_code = 'RY1'
union
select null, 'Available > 60' as status, count_over_60 as units, pot_lot_gross_over_60 as pot_lot_gross, pot_lot_gross_over_60/count_over_60 as "gross/unit"
from dave_summary 
where status = 'avail'
  and store_code = 'RY1'
union  
select null, 'Raw Materials 0 - 30' as status, count_0_to_30 as units, pot_lot_gross_0_to_30 as pot_lot_gross, pot_lot_gross_0_to_30/count_0_to_30 as "gross/unit"
from dave_summary 
where status = 'raw'
  and store_code = 'RY1'
union
select null, 'Raw Materials 31 - 60' as status, count_31_to_60 as units, pot_lot_gross_31_to_60 as pot_lot_gross, pot_lot_gross_31_to_60/count_31_to_60 as "gross/unit"
from dave_summary 
where status = 'raw'
  and store_code = 'RY1'
union
select null, 'Raw Materials > 60' as status, count_over_60 as units, pot_lot_gross_over_60 as pot_lot_gross, pot_lot_gross_over_60/count_over_60 as "gross/unit"
from dave_summary 
where status = 'raw'
  and store_code = 'RY1'
order by store, status  ) a
  

select * from (
-- ry2
select 'HONDA/NISSAN' as store, null  as status, null as units, null as pot_lot_gross, null as "gross/unit"
union
select null, 'Available 0 - 30' as status, count_0_to_30 as units, pot_lot_gross_0_to_30 as pot_lot_gross, pot_lot_gross_0_to_30/count_0_to_30 as "gross/unit"
from dave_summary 
where status = 'avail'
  and store_code = 'RY2'
union
select null, 'Available 31 - 60' as status, count_31_to_60 as units, pot_lot_gross_31_to_60 as pot_lot_gross, pot_lot_gross_31_to_60/count_31_to_60 as "gross/unit"
from dave_summary 
where status = 'avail'
  and store_code = 'RY2'
union
select null, 'Available > 60' as status, count_over_60 as units, pot_lot_gross_over_60 as pot_lot_gross, pot_lot_gross_over_60/count_over_60 as "gross/unit"
from dave_summary 
where status = 'avail'
  and store_code = 'RY2'
union  
select null, 'Raw Materials 0 - 30' as status, count_0_to_30 as units, pot_lot_gross_0_to_30 as pot_lot_gross, pot_lot_gross_0_to_30/count_0_to_30 as "gross/unit"
from dave_summary 
where status = 'raw'
  and store_code = 'RY2'
union
select null, 'Raw Materials 31 - 60' as status, count_31_to_60 as units, pot_lot_gross_31_to_60 as pot_lot_gross, pot_lot_gross_31_to_60/count_31_to_60 as "gross/unit"
from dave_summary 
where status = 'raw'
  and store_code = 'RY2'
union
select null, 'Raw Materials > 60' as status, count_over_60 as units, pot_lot_gross_over_60 as pot_lot_gross, pot_lot_gross_over_60/count_over_60 as "gross/unit"
from dave_summary 
where status = 'raw'
  and store_code = 'RY2'
order by store, status    ) b

order by status


-- 5/7/18 put it in a python script and schedule it weekly
drop table if exists ads.potential_lot_gross_report;
create table ads.potential_lot_gross_report (
  the_date date not null,
  store citext not null, 
  status citext not null,
  units integer not null,
  pot_lot_gross integer not null,
  gross_per_unit integer not null,
  primary key (the_date,store, status));