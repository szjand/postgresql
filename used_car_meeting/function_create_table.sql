﻿create or replace function ads.create_potential_lot_gross_report()
returns void as
$BODY$
/*
select ads.create_potential_lot_gross_report()
*/
-- open recon
begin
drop table if exists open_recon;
create temp table open_recon as
SELECT a.stocknumber,
  sum(vri.TotalPartsAmount + vri.LaborAmount) as open_recon
FROM ads.ext_Recon_Authorizations ra
inner JOIN ads.ext_Authorized_Recon_Items ari ON ari.ReconAuthorizationID = ra.ReconAuthorizationID
INNER JOIN ads.ext_Vehicle_Recon_Items vri ON vri.VehicleReconItemID = ari.VehicleReconItemID  
INNER JOIN ads.ext_Vehicle_Inventory_Items a on ra.VehicleInventoryItemID = a.VehicleInventoryItemID 
  AND a.thruts::date > current_date
WHERE ra.ThruTS::date is null
  and ari.status <> 'AuthorizedReconItem_Complete'
GROUP BY a.stocknumber;

drop table if exists the_cost;
create temp table the_cost as
select b.store_code, a.inpmast_stock_number, a.inpmast_vin, a.inventory_account, a.inpmast_vehicle_cost, sum(c.amount)
from arkona.xfm_inpmast a
inner join fin.dim_account b on a.inventory_account = b.account
left join fin.fact_gl c on a.inpmast_stock_number = c.control
  and c.post_status = 'Y'
  and b.account_key = c.account_key
where a.current_row = true
  and a.status = 'I'
  and a.type_n_u = 'U'
group by b.store_code, a.inpmast_stock_number, a.inpmast_vin, a.inventory_account, a.inpmast_vehicle_cost; 

-- available
drop table if exists available;
create temp table available as
select  a.stocknumber, b.vehiclepricingts, c.amount as best_price, current_date - d.fromts::date as age
from ads.ext_vehicle_inventory_items a
inner join  ads.ext_vehicle_inventory_item_statuses d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
  and d.status = 'RMFlagAV_Available'
  and d.thruts > current_date
left join ads.ext_vehicle_pricings b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
inner join lateral(
  select vehicleinventoryitemid, max(vehiclepricingts) as vehiclepricingts
  from ads.ext_vehicle_pricings
  where vehicleinventoryitemid =  b.vehicleinventoryitemid
  group by vehicleinventoryitemid) bb on b.vehicleinventoryitemid = bb.vehicleinventoryitemid
    and b.vehiclepricingts = bb.vehiclepricingts
left join ads.ext_vehicle_pricing_details c on b.vehiclepricingid = c.vehiclepricingid
  and c.typ = 'VehiclePricingDetail_BestPrice'
where a.thruts > current_date;
create unique index on available(stocknumber);

-- raw materials
drop table if exists raw_materials;
create temp table raw_materials as
select  a.stocknumber, b.vehiclepricingts, c.amount as best_price, current_date - d.fromts::date as age
from ads.ext_vehicle_inventory_items a
inner join  ads.ext_vehicle_inventory_item_statuses d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
  and d.status = 'RMFlagRMP_RawMaterialsPool'
  and d.thruts > current_date
left join ads.ext_vehicle_pricings b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
inner join lateral(
  select vehicleinventoryitemid, max(vehiclepricingts) as vehiclepricingts
  from ads.ext_vehicle_pricings
  where vehicleinventoryitemid =  b.vehicleinventoryitemid
  group by vehicleinventoryitemid) bb on b.vehicleinventoryitemid = bb.vehicleinventoryitemid
    and b.vehiclepricingts = bb.vehiclepricingts
left join ads.ext_vehicle_pricing_details c on b.vehiclepricingid = c.vehiclepricingid
  and c.typ = 'VehiclePricingDetail_BestPrice'
where a.thruts > current_date
  AND NOT EXISTS (
    SELECT 1 
    FROM ads.ext_vehicle_inventory_item_statuses
    WHERE VehicleInventoryItemID = d.VehicleInventoryItemID
    AND status = 'RMFlagRB_ReconBuffer'
    AND ThruTS > current_date)
  AND NOT EXISTS (
    SELECT 1 
    FROM ads.ext_vehicle_inventory_item_statuses
    WHERE VehicleInventoryItemID = d.VehicleInventoryItemID
    AND category = 'RMFlagSB'
    AND ThruTS > current_date);
create unique index on raw_materials(stocknumber);

-- dave's summary  including open recon
drop table if exists dave_summary;
create temp table dave_summary as
select 'avail' as status, store_code, 
  count(a.stocknumber) filter (where age < 31) as count_0_to_30,
  (sum(best_price - inpmast_vehicle_cost - coalesce(c.open_recon, 0)) filter (where age < 31))::integer as pot_lot_gross_0_to_30,
  count(a.stocknumber) filter (where age between 31 and 60) as count_31_to_60,
  (sum(best_price - inpmast_vehicle_cost - coalesce(c.open_recon, 0)) filter (where age between 31 and 60))::integer as pot_lot_gross_31_to_60,
  count(a.stocknumber) filter (where age > 60) as count_over_60,
  (sum(best_price - inpmast_vehicle_cost - coalesce(c.open_recon, 0)) filter (where age > 60))::integer as pot_lot_gross_over_60
from available a
inner join the_cost b on a.stocknumber = b.inpmast_stock_number
left join open_recon c on a.stocknumber = c.stocknumber
group by store_code
union
select 'raw', store_code, 
  count(a.stocknumber) filter (where age < 31) as count_0_to_30,
  (sum(best_price - inpmast_vehicle_cost - coalesce(c.open_recon, 0)) filter (where age < 31))::integer as pot_lot_gross_0_to_30,
  count(a.stocknumber) filter (where age between 31 and 60) as count_31_to_60,
  (sum(best_price - inpmast_vehicle_cost - coalesce(c.open_recon, 0)) filter (where age between 31 and 60))::integer as pot_lot_gross_31_to_60,
  count(a.stocknumber) filter (where age > 60) as count_over_60,
  (sum(best_price - inpmast_vehicle_cost - coalesce(c.open_recon, 0)) filter (where age > 60))::integer as pot_lot_gross_over_60
from raw_materials a
inner join the_cost b on a.stocknumber = b.inpmast_stock_number
left join open_recon c on a.stocknumber = c.stocknumber
group by store_code;

truncate ads.potential_lot_gross_report; -- *****************************************************************************************
insert into ads.potential_lot_gross_report (the_date,store,status,units,pot_lot_gross,gross_per_unit)
select *
from ((
  select current_date, 'GM'::text as store) a
  cross join (
    select 'Available 0 - 30' as status, count_0_to_30 as units, pot_lot_gross_0_to_30 as pot_lot_gross, pot_lot_gross_0_to_30/count_0_to_30 as "gross/unit"
    from dave_summary 
    where status = 'avail'
      and store_code = 'RY1'
    union
    select 'Available 31 - 60' as status, count_31_to_60 as units, pot_lot_gross_31_to_60 as pot_lot_gross, pot_lot_gross_31_to_60/count_31_to_60 as "gross/unit"
    from dave_summary 
    where status = 'avail'
      and store_code = 'RY1'
    union
    select 'Available > 60' as status, count_over_60 as units, pot_lot_gross_over_60 as pot_lot_gross, pot_lot_gross_over_60/count_over_60 as "gross/unit"
    from dave_summary 
    where status = 'avail'
      and store_code = 'RY1'
    union  
    select 'Raw Materials 0 - 30' as status, count_0_to_30 as units, pot_lot_gross_0_to_30 as pot_lot_gross, pot_lot_gross_0_to_30/count_0_to_30 as "gross/unit"
    from dave_summary 
    where status = 'raw'
      and store_code = 'RY1'
    union
    select 'Raw Materials 31 - 60' as status, count_31_to_60 as units, pot_lot_gross_31_to_60 as pot_lot_gross, pot_lot_gross_31_to_60/count_31_to_60 as "gross/unit"
    from dave_summary 
    where status = 'raw'
      and store_code = 'RY1'
    union
    select 'Raw Materials > 60' as status, count_over_60 as units, pot_lot_gross_over_60 as pot_lot_gross, pot_lot_gross_over_60/count_over_60 as "gross/unit"
    from dave_summary 
    where status = 'raw'
      and store_code = 'RY1') b) c
union
select *
from ((
  select current_date, 'HONDA/NISSAN') a
  cross join (
    select 'Available 0 - 30' as status, count_0_to_30 as units, pot_lot_gross_0_to_30 as pot_lot_gross, pot_lot_gross_0_to_30/count_0_to_30 as "gross/unit"
    from dave_summary 
    where status = 'avail'
      and store_code = 'RY2'
    union
    select 'Available 31 - 60' as status, count_31_to_60 as units, pot_lot_gross_31_to_60 as pot_lot_gross, pot_lot_gross_31_to_60/count_31_to_60 as "gross/unit"
    from dave_summary 
    where status = 'avail'
      and store_code = 'RY2'
    union
    select 'Available > 60' as status, count_over_60 as units, pot_lot_gross_over_60 as pot_lot_gross, pot_lot_gross_over_60/count_over_60 as "gross/unit"
    from dave_summary 
    where status = 'avail'
      and store_code = 'RY2'
    union  
    select 'Raw Materials 0 - 30' as status, count_0_to_30 as units, pot_lot_gross_0_to_30 as pot_lot_gross, pot_lot_gross_0_to_30/count_0_to_30 as "gross/unit"
    from dave_summary 
    where status = 'raw'
      and store_code = 'RY2'
    union
    select 'Raw Materials 31 - 60' as status, count_31_to_60 as units, pot_lot_gross_31_to_60 as pot_lot_gross, pot_lot_gross_31_to_60/count_31_to_60 as "gross/unit"
    from dave_summary 
    where status = 'raw'
      and store_code = 'RY2'
    union
    select 'Raw Materials > 60' as status, count_over_60 as units, pot_lot_gross_over_60 as pot_lot_gross, pot_lot_gross_over_60/count_over_60 as "gross/unit"
    from dave_summary 
    where status = 'raw'
      and store_code = 'RY2') b) c;  
end;
      
$BODY$
language plpgsql;

select * from ads.potential_lot_gross_report ;