﻿select x.*, y.gross
from ( -- count
  select year_month, sum(unit_count) as unit_count
  from (
    select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
      a.control, a.amount,
      case when a.amount < 0 then 1 else -1 end as unit_count
    from fin.fact_gl a
    inner join dds.dim_date b on a.date_key = b.date_key
      and b.year_month between 201301 and 201802 --------------------------------------------------------------------
    inner join fin.dim_account c on a.account_key = c.account_key
  -- add journal
    inner join fin.dim_journal aa on a.journal_key = aa.journal_key
      and aa.journal_code in ('VSN','VSU')
    inner join ( -- d: fs gm_account page/line/acct description
      select b.year_month, f.store, d.gl_account, b.page, b.line, b.line_label, e.description
      from fin.fact_fs a
      inner join fin.dim_fs b on a.fs_key = b.fs_key
        and b.year_month between 201301 and 201802 -------------------------------------------------------------------
        and b.page = 16 and b.line in (8,10)-- used cars
      inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
      inner join fin.dim_account e on d.gl_account = e.account
        and e.account_type_code = '4'
      inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account and b.year_month = d.year_month
    where a.post_status = 'Y') h
  where store = 'RY1'  
  group by year_month) x
left join (  -- gross
  select c.store, b.year_month, 
    sum(-amount) as gross
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month between 201301 and 201802
    and b.page = 16
    and b.line in (8, 10)
  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
    and c.store = 'ry1'
  group by c.store, b.year_month
  having sum(amount) <> 0) y on x.year_month = y.year_month



