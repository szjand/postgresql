﻿/*
based on function ads.create_potential_lot_gross_report()
which generates a weekly email of avail gross on mondays to wilkie
!!! discovered that i am night scraping recon tables nightly, so those emails have been wrong !!!
*/
drop table if exists jon.uc_pot_gross;
create table jon.uc_pot_gross (
  store citext,
  stock_number citext primary key,
  vin citext,
  model_year citext,
  make citext,
  model citext,
  recon_package citext,
  age integer,
  acct_cost integer,
  open_recon integer, 
  cert_fees integer,
  est_total_cost integer,
  best_price integer,
  potential_gross integer);
insert into jon.uc_pot_gross  
select * 
from (
  with
    the_cost as (
      select b.store_code, a.inpmast_stock_number, a.inpmast_vin, a.inventory_account, a.inpmast_vehicle_cost, sum(c.amount) as cost
      from arkona.xfm_inpmast a
      inner join fin.dim_account b on a.inventory_account = b.account
      left join fin.fact_gl c on a.inpmast_stock_number = c.control
        and c.post_status = 'Y'
        and b.account_key = c.account_key
      where a.current_row = true
        and a.status = 'I'
        and a.type_n_u = 'U'
      group by b.store_code, a.inpmast_stock_number, a.inpmast_vin, a.inventory_account, a.inpmast_vehicle_cost), 
    open_recon as (
      SELECT a.stocknumber,
        sum(vri.TotalPartsAmount + vri.LaborAmount) as open_recon
      FROM ads.ext_Recon_Authorizations ra
      inner JOIN ads.ext_Authorized_Recon_Items ari ON ari.ReconAuthorizationID = ra.ReconAuthorizationID
      INNER JOIN ads.ext_Vehicle_Recon_Items vri ON vri.VehicleReconItemID = ari.VehicleReconItemID  
      INNER JOIN ads.ext_Vehicle_Inventory_Items a on ra.VehicleInventoryItemID = a.VehicleInventoryItemID 
        AND a.thruts::date > current_date
      WHERE ra.ThruTS::date is null
        and ari.status <> 'AuthorizedReconItem_Complete'
      GROUP BY a.stocknumber),
    current_inventory_under_30 as (
      select a.stocknumber, aa.vin, aa.yearmodel, aa.make, aa.model, a.vehicleinventoryitemid, current_date - b.thruts::date as age
      from ads.ext_vehicle_inventory_items a
      inner join ads.ext_vehicle_items aa on a.vehicleitemid = aa.vehicleitemid
      inner join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
        and b.category = 'RMFlagWP'
        and b.thruts::date < current_date
      where a.thruts::date > current_date
        and current_date - b.thruts::date < 31),
    current_price as (
      select b.vehicleinventoryitemid, c.*
      from current_inventory_under_30 a
      inner join ads.ext_vehicle_pricings b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
      inner join ads.ext_vehicle_pricing_details c on b.vehiclepricingid = c.vehiclepricingid
        and c.typ = 'VehiclePricingDetail_BestPrice' 
      inner join (
        select a.vehicleinventoryitemid, max(b.vehiclepricingts) as vehiclepricingts
        from current_inventory_under_30 a
        inner join ads.ext_vehicle_pricings b on a.vehicleinventoryitemid = b.vehicleinventoryitemid    
        group by a.vehicleinventoryitemid) d on b.vehicleinventoryitemid = d.vehicleinventoryitemid
          and b.vehiclepricingts = d.vehiclepricingts),
     current_package as (
       select b.*
       from current_inventory_under_30 a
       inner join ads.ext_selected_recon_packages b on a.vehicleinventoryitemid = b.vehicleinventoryitemid  
       inner join (
         select b.vehicleinventoryitemid,max(b.selectedreconpackagets) as selectedreconpackagets
         from current_inventory_under_30 a
         inner join ads.ext_selected_recon_packages b on a.vehicleinventoryitemid = b.vehicleinventoryitemid  
         group by b.vehicleinventoryitemid) c on b.vehicleinventoryitemid = c.vehicleinventoryitemid  
           and b.selectedreconpackagets = c.selectedreconpackagets)
  select store,stocknumber,vin,model_year,make,model,package,age, cost,open_recon,cert_fees,
    cost + open_recon + cert_fees as est_total_cost,
    best_price, best_price - (cost+ open_recon + cert_fees) as potential_gross
  from (
    select case left(a.stocknumber, 1) when 'H' then 'RY2' else 'RY1' end as store,
      a.stocknumber, a.vin, a.yearmodel as model_year, a.make, a.model, 
      substring(e.typ, position('_' in e.typ) + 1, 12) as package,
      a.age, 
      b.cost::integer, coalesce(c.open_recon, 0) as open_recon, 
      case
        when make = 'cadillac' and substring(e.typ, position('_' in e.typ) + 1, 12) = 'Factory' then 1295
        when make in ('buick','chevrolet', 'gmc') and substring(e.typ, position('_' in e.typ) + 1, 12) = 'Factory' then 499
        else 0
      end as cert_fees,
      d.amount::integer as best_price
    from current_inventory_under_30 a
    inner join the_cost b on a.stocknumber = b.inpmast_stock_number
    left join open_recon c on a.stocknumber = c.stocknumber  
    left join current_price d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
    left join current_package e on a.vehicleinventoryitemid = e.vehicleinventoryitemid) f) g;




select a.*, b.kbb_retail, (.97 * kbb_retail)::integer as "97%_kbb", (.97 * kbb_retail)::integer - best_price
from jon.uc_pot_gross a
left join jon.kbb_values b on a.stock_number = b.stock_number
where a.potential_gross < 2000
  and store = 'RY1'
  and recon_package <> 'WS'
  and best_price >  (.97 * kbb_retail)::integer



