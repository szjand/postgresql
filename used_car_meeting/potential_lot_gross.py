import db_cnx
import csv
from openpyxl import load_workbook
import openpyxl
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
from email.mime.text import MIMEText
import os

##########################################################################
#             email list for deploy
##########################################################################

file_name = 'files/pot_lot_gross.csv'
spreadsheet = 'files/pot_lot_gros.xlsx'


def email_failure(_task,  _error):
    body = str(_error)
    message = MIMEText(body)
    message['Subject'] = _task
    smtp_server = 'mail.cartiva.com'
    x = smtplib.SMTP(smtp_server)
    addr_to = 'jandrews@cartiva.com'
    addr_from = 'jandrews@cartiva.com'
    message['To'] = addr_to
    message['From'] = addr_from
    receivers = ['jandrews@cartiva.com']
    x.sendmail(addr_from, receivers, message.as_string())
    x.quit()


try:
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = "select ads.create_potential_lot_gross_report()"
            pg_cur.execute(sql)
            pg_con.commit()
            sql = """
                select status,units,pot_lot_gross,gross_per_unit
                from ads.potential_lot_gross_report 
                where store = 'GM' 
                  and the_date = current_date
                order by status
            """
            pg_cur.execute(sql)
            with open(file_name, 'w') as f:
                writer = csv.writer(f)
                writer.writerow(["GM", "Units", "Pot Lot Gross", "Gross/Unit"])
                writer.writerows(pg_cur)
            wb = openpyxl.Workbook()
            wb.save(spreadsheet)
            wb = load_workbook(spreadsheet)
            ws = wb.get_active_sheet()
            with open(file_name) as f:
                reader = csv.reader(f, delimiter=',')
                for row in reader:
                    ws.append(row)
            wb.save(spreadsheet)

    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = "select ads.create_potential_lot_gross_report()"
            pg_cur.execute(sql)
            pg_con.commit()
            sql = """
                select status,units,pot_lot_gross,gross_per_unit
                from ads.potential_lot_gross_report 
                where store = 'HONDA/NISSAN' 
                  and the_date = current_date
                order by status
            """
            pg_cur.execute(sql)
            with open(file_name, 'a') as f:
                writer = csv.writer(f)
                writer.writerow(["HONDA/NISSAN", "", "", ""])
                writer.writerows(pg_cur)
            wb = openpyxl.Workbook()
            wb.save(spreadsheet)
            wb = load_workbook(spreadsheet)
            ws = wb.get_active_sheet()
            with open(file_name) as f:
                reader = csv.reader(f, delimiter=',')
                for row in reader:
                    ws.append(row)
            # https://stackoverflow.com/questions/13197574/openpyxl-adjust-column-width-size/14450572
            # autosizes columns
            dims = {}
            for row in ws.rows:
                for cell in row:
                    if cell.value:
                        dims[cell.column] = max((dims.get(cell.column, 0), len(cell.value)))
            for col, value in dims.items():
                ws.column_dimensions[col].width = value
            wb.save(spreadsheet)

    try:
        COMMASPACE = ', '
        sender = 'jandrews@cartiva.com'
        recipients = ['test@cartiva.com', 'jandrews@cartiva.com']
        # recipients = ['test@cartiva.com', 'jandrews@cartiva.com', 'dwilkie@rydellcars.com']
        outer = MIMEMultipart()
        outer['Subject'] = 'Potential Lot Gross'
        outer['To'] = COMMASPACE.join(recipients)
        outer['From'] = sender
        outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'
        attachments = [spreadsheet]
        for x_file in attachments:
            try:
                with open(x_file, 'rb') as fp:
                    msg = MIMEBase('application', "octet-stream")
                    msg.set_payload(fp.read())
                encoders.encode_base64(msg)
                msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(x_file))
                outer.attach(msg)
            except Exception:
                raise
        composed = outer.as_string()
        e = smtplib.SMTP('mail.cartiva.com')
        try:
            e.sendmail(sender, recipients, composed)
        except smtplib.SMTPException:
            print("Error: unable to send email")
        e.quit()
    except Exception as error:
        print(error)


except Exception as error:
    email_failure('Potential Lot Gross Failed', error)
