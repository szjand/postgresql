﻿select b.stocknumber
from ads.ext_vehicle_inventory_item_notes a
inner join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
where notes = 'kbb'





create table jon.kbb_repriced_vehicles (
  vehicle_inventory_item_id citext,
  stock_number citext primary key references jon.uc_pot_gross(stock_number),
  kbb_pricing integer);
  
insert into jon.kbb_repriced_vehicles
select b.vehicleinventoryitemid, a.stock_number, d.amount::integer 
from jon.uc_pot_gross a
inner join ( -- kbb test vehicles
  select b.stocknumber, a.vehicleinventoryitemid
  from ads.ext_vehicle_inventory_item_notes a
  inner join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  where notes = 'kbb') b on a.stock_number = b.stocknumber
left join ads.ext_vehicle_pricings c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
  and c.vehiclepricingts::date = '08/10/2018'
inner join (
  select vehicleinventoryitemid, max(vehiclepricingts) as vehiclepricingts
  from ads.ext_vehicle_pricings
  group by vehicleinventoryitemid) cc on c.vehicleinventoryitemid = cc.vehicleinventoryitemid
    and c.vehiclepricingts = cc.vehiclepricingts
left join ads.ext_vehicle_pricing_details d on c.vehiclepricingid = d.vehiclepricingid
  and d.typ = 'VehiclePricingDetail_BestPrice' ; 

-- 37 vehicles repriced 8/10/18
select * 
from jon.kbb_repriced_vehicles
