﻿6/8/21
entire first quarter payroll history per ukg request
first thing, can a payroll run have multiple check dates

-- looks ok
select company_number, payroll_cen_year, payroll_run_number
from arkona.ext_pyptbdta
group by company_number, payroll_cen_year, payroll_run_number
having count(*) > 1

based on the original query .../kronos/payroll_history

check date

EIN Tax Id	EIN Name
45-0236743	Rydell GM

EIN Tax Id	EIN Name
45-0415948	Honda Nissan

-- -- better handle void checks
-- -- yep 0J is void checks
-- select seq_void, count(*) from arkona.ext_pyhshdta where check_year in (20,21) group by seq_void
-- 
-- select reference_check_, seq_void, company_number, check_year, check_month, check_day, employee_, employee_name 
-- from arkona.ext_pyhshdta 
-- where seq_void in ('02','01','0J') and check_year in (20,21)
-- order by seq_void, employee_name

-- add the ein & check date, leave pyhshdta in to limit by check_month		

-- want to order the headers by code type and description, but only want have the headers display the description
-- that proved to be not possible

drop table if exists first_quarter_adds_deducts cascade;
create temp table first_quarter_adds_deducts as
		select dds.db2_integer_to_date(('20'||c.check_year::text || lpad(c.check_month::text, 2, '0') ||lpad(c.check_day::text, 2, '0'))::integer) as check_date,
		  a.company_number, --a.payroll_run_number,
			a.employee_number, a.code_type ||'-'|| b.description as sort_order, b.description, sum(amount) as amount
		from arkona.ext_pyhscdta a
		join jon.payroll_codes b on a.code_type = b.code_type
			and a.code_id = b.code_id
			and a.company_number = b.store
		join arkona.ext_pyhshdta c on a.company_number = c.company_number
			and a.payroll_run_number = c.payroll_run_number
			and a.employee_number = c.employee_
			and c.check_year = 21
			and c.check_month between 1 and 3
			and c.seq_void <> '0J' -- void checks
-- 		where a.company_number = 'RY1' 
		group by dds.db2_integer_to_date(('20'||c.check_year::text || lpad(c.check_month::text, 2, '0') ||lpad(c.check_day::text, 2, '0'))::integer),
		  a.company_number, --a.payroll_run_number,
			a.employee_number, a.code_type ||'-'|| b.description, b.description;
alter table first_quarter_adds_deducts
add primary key(check_date,employee_number,sort_order);

-- get rid of sort order, just use description


drop table if exists first_quarter_adds_deducts cascade;
create temp table first_quarter_adds_deducts as
		select dds.db2_integer_to_date(('20'||c.check_year::text || lpad(c.check_month::text, 2, '0') ||lpad(c.check_day::text, 2, '0'))::integer) as check_date,
		  a.company_number, --a.payroll_run_number,
			a.employee_number, 
			a.code_type ||'-'|| 
				case 
					when b.description like '%COMM%' then 'COMMISSIONS' 
					when b.description in ('1  ACCTS REC', '2 ACCOUNTS RECI') then 'ACCTS REC'
					else b.description 
				end as description, sum(amount) as amount
		from arkona.ext_pyhscdta a
		join jon.payroll_codes b on a.code_type = b.code_type
			and a.code_id = b.code_id
			and a.company_number = b.store
		join arkona.ext_pyhshdta c on a.company_number = c.company_number
			and a.payroll_run_number = c.payroll_run_number
			and a.employee_number = c.employee_
			and c.check_year = 21
			and c.check_month between 1 and 3
			and c.seq_void <> '0J' -- void checks
			and a.amount <> 0
-- 		where a.company_number = 'RY1' 
		group by dds.db2_integer_to_date(('20'||c.check_year::text || lpad(c.check_month::text, 2, '0') ||lpad(c.check_day::text, 2, '0'))::integer),
		  a.company_number, --a.payroll_run_number,
			a.employee_number, 
			a.code_type ||'-'|| 
				case 
					when b.description like '%COMM%' then 'COMMISSIONS' 
					when b.description in ('1  ACCTS REC', '2 ACCOUNTS RECI') then 'ACCTS REC'
					else b.description 
				end;
alter table first_quarter_adds_deducts
add primary key(check_date,employee_number,description);



select * from first_quarter_adds_deducts where description like '5%' and employee_number = '1130420'

drop table if exists first_quarter_adds_deducts_pivot;
select jon.colpivot (
	'first_quarter_adds_deducts_pivot',
	$$
	  select * 
	  from first_quarter_adds_deducts
	$$,
	array['company_number','check_date','employee_number'],
	array['description'],
	'#.amount',
	'description');
alter table first_quarter_adds_deducts_pivot
add primary key(employee_number, check_date);

select * from first_quarter_adds_deducts_pivot where employee_number = '1130420'


-- check_date, ein stuff needs to come from pyhshdta, not all checks have entries in pyhscdta
-- which means this join needs to be on emp# & payroll_run_number & store
drop table if exists first_quarter_all_pay;
create temp table first_quarter_all_pay as
select *
from ( -- need to group, multiple checks per payroll_run
	select 
	  dds.db2_integer_to_date(('20'||check_year::text || lpad(check_month::text, 2, '0') ||lpad(check_day::text, 2, '0'))::integer) as the_date,
	  company_number as store, -- payroll_run_number as batch,
		case company_number
			when 'RY1' then '45-0236743' 
			when 'RY2' then '45-0415948'
		end as "EIN Tax Id", 
		case company_number
			when 'RY1' then 'Rydell GM' 
			when 'RY2' then 'Honda Nissan'
		end as "EIN NAME", 	
		employee_ as user_id, employee_name, state_tax_tab_code as withholding_state,
		sum(base_pay) as base_pay, sum(total_gross_pay) as total_gross_pay, sum(curr_federal_tax) as fed_tax, sum(curr_fica) as fica, 
		sum(curr_employee_medicare) as medicare, sum(curr_state_tax) as state_tax, sum(emplr_curr_ret) as emplr_ret, sum(current_net) as net
	from arkona.ext_pyhshdta 
-- 	where payroll_run_number = 108210
  where check_year = 21 and check_month between 1 and 3
-- 		and company_number = 'RY1'
	group by dds.db2_integer_to_date(('20'||check_year::text || lpad(check_month::text, 2, '0') ||lpad(check_day::text, 2, '0'))::integer),
	  company_number, --payroll_run_number,
		case company_number
			when 'RY1' then '45-0236743' 
			when 'RY2' then '45-0415948'
		end, 
		case company_number
			when 'RY1' then 'Rydell GM' 
			when 'RY2' then 'Honda Nissan'
		end, 	
		employee_, employee_name, state_tax_tab_code order by employee_name) a
left join ( -- 118010 had 2 identical rows
	select distinct * 
	from first_quarter_adds_deducts_pivot) b on a.store = b.company_number
	  and a.user_id = b.employee_number 
	  and a.the_date = b.check_date
order by a.employee_name;
alter table first_quarter_all_pay
add primary key(the_date,user_id);

select * from first_quarter_all_pay
-- now need the clock hours

-- i believe this is adequate granularity from this table
drop table if exists batches cascade;
create temp table batches as
select distinct company_number, payroll_run_number,
		arkona.db2_integer_to_date(payroll_start_date) as from_date, arkona.db2_integer_to_date(payroll_end_date) as thru_date, 
   dds.db2_integer_to_date(check_date) as check_date
from arkona.ext_pyptbdta
where dds.db2_integer_to_date(check_date) between '01/01/2021' and '03/31/2021'
  and description not like '%void%';
alter table batches
add primary key(company_number,payroll_run_number);
create unique index on batches(company_number, payroll_run_number,check_date);

select * from batches

-- i think now add pyhshdta to get employeenumbers relevant to these payroll events
-- includes hours from pyhshdta
-- add a level of aggregation that gets rid of the payroll run number and just uses check date
-- because this is going to be joined to xfm_pypclockin where only dates matter


drop table if exists batch_employees cascade;
create temp table batch_employees as
select company_number, from_date, thru_date, check_date, employee_,
	sum(reg_hours) as reg_hours,
	sum(overtime_hours) as overtime_hours, sum(vacation_taken) as vacation_taken, 
	sum(sick_leave_taken) as sick_leave_taken, sum(holiday_taken) as holiday_taken
from (-- get rid of payroll_run_number
	select a.company_number, a.payroll_run_number, a.from_date, a.thru_date, a.check_date, b.employee_,
		sum(reg_hours) as reg_hours,
		sum(overtime_hours) as overtime_hours, sum(vacation_taken) as vacation_taken, 
		sum(sick_leave_taken) as sick_leave_taken, sum(holiday_taken) as holiday_taken
	from batches a
	left join arkona.ext_pyhshdta b on a.company_number = b.company_number
		and a.payroll_run_number = b.payroll_run_number
		and b.seq_void <> '0J'
	group by a.company_number, a.payroll_run_number, a.from_date, a.thru_date, a.check_date, b.employee_) aa
group by company_number, from_date, thru_date, check_date, employee_;
alter table batch_employees
add primary key(check_date, employee_);


drop table if exists first_quarter_clock_hours cascade;
create temp table first_quarter_clock_hours as
select a.company_number, a.check_date, a.employee_,
  coalesce(sum(b.reg_hours), a.reg_hours) as "REG HOURS", 
  coalesce(sum(b.ot_hours), a.overtime_hours) as "OT HOURS",
  coalesce(sum(b.vac_hours), a.vacation_taken) as "VAC HOURS",
  coalesce(sum(b.pto_hours), a.sick_leave_taken) as "PTO HOURS",
  coalesce(sum(b.hol_hours), a.holiday_taken) as "HOL HOURS"		
from batch_employees a
left join arkona.xfm_pypclockin b on a.employee_ = b.employee_number
  and b.the_date between a.from_date and a.thru_date
group by a.company_number, a.check_date, a.employee_;
alter table first_quarter_clock_hours
add primary key(check_date,employee_);


-- this is what was submitted
select * 
from first_quarter_all_pay a
join first_quarter_clock_hours b on a.the_date = b.check_date
  and a.user_id = b.employee_



 -- 06/17/21  working with ukg getting on mapping of field names, jeri stuck at base_pay, no counterpart
-- these received base pay in 1st quarter
 	select employee_ as user_id, employee_name
	from arkona.ext_pyhshdta 
-- 	where payroll_run_number = 108210
  where check_year = 21 and check_month between 1 and 3
  and base_pay <> 0
 group by employee_, employee_name

-- these have checks in the 1st quarter with 0 for base pay
  select dds.db2_integer_to_date(('20'||check_year::text || lpad(check_month::text, 2, '0') ||lpad(check_day::text, 2, '0'))::integer) as the_date,
   employee_ as user_id, employee_name
	from arkona.ext_pyhshdta 
-- 	where payroll_run_number = 108210
  where check_year = 21 and check_month between 1 and 3  
    and base_pay = 0
  order by employee_name, dds.db2_integer_to_date(('20'||check_year::text || lpad(check_month::text, 2, '0') ||lpad(check_day::text, 2, '0'))::integer)


  -- 6/18 i need a little more detail to try to understand jeri's suggestions

drop table if exists base_pay cascade;
create temp table base_pay as
select employee_ as user_id, employee_name, a.distrib_code, a.payroll_class, a.pay_period, 'base_pay' as base_pay
from arkona.ext_pyhshdta a
where check_year = 21 and check_month between 1 and 3
	and base_pay <> 0
group by employee_, employee_name, a.distrib_code, a.payroll_class, a.pay_period 
union
select employee_ as user_id, employee_name, a.distrib_code, a.payroll_class, a.pay_period, 'no_base_pay'
from arkona.ext_pyhshdta a
where check_year = 21 and check_month between 1 and 3
	and base_pay = 0
group by employee_, employee_name, a.distrib_code, a.payroll_class, a.pay_period 
order by employee_name;

-- no commission with base pay
select * 
from base_pay
where payroll_class = 'c'
  and base_pay = 'base_pay'

 select * 
from base_pay
where payroll_class = 's'
  and base_pay = 'base_pay' 