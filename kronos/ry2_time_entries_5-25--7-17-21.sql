﻿/*
11/06/21
	kim is asking for time clock data for kronos
	Dates for the  honda timesheets would be:

	May 25 to june 5th
	June 6 to 19
	June 20 to july 3
	July 4 to 17

	Thanks Jon if you have any questions give me a call
	
this is formatted correctly per Time Entries Import

sent it all in one spreadsheet (xls)
*/
-- select "Employee Name", count(*), sum("Total Time") from (
-- select distinct "Pay Date" from (
select f.employee_name as "Employee Name", "Employee ID", "EIN Tax Id", "EIN Name", "Pay Date",
  "In Date", 
  case 
		when yicode in ('VAC','PTO','HOL') then null
		
		else "Time In"
	end as "Time In", 
  case 
		when yicode in ('VAC','PTO','HOL') then null
		else "Time Out"
	end as "Time Out", 
	clock_hours + hol_hours + pto_hours + vac_hours as "Total Time", 
	case
		when yicode in ('VAC','PTO') then 'PTO'
		when yicode = 'HOL' then 'Holiday'
-- 		when yicode = '666' then 'Auto'
		else null
  end as "Time Off"
from (
	select pymast_employee_number as "Employee ID",
			case yico_
				when 'RY1' then '45-0236743' 
				when 'RY2' then '45-0415948'
			end as "EIN Tax Id", 
			case yico_
				when 'RY1' then 'Rydell Auto Center Inc.' 
				when 'RY2' then 'Honda Nissan of Grand Forks'
			end as "EIN Name", yiclkind as "Pay Date", yiclkind as "In Date", yiclkint as "Time In", yiclkoutt as "Time Out", 
			coalesce(
				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
					/3600.0)::numeric(6,2)) filter (where yicode in ('666','O')), 0) as clock_hours,    
			coalesce(
				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
					/3600.0)::numeric(6,2)) filter (where yicode = 'HOL'), 0) as hol_hours, 
			coalesce(
				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
					/3600.0)::numeric(6,2)) filter (where yicode = 'PTO'), 0) as pto_hours, 
			coalesce(
				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
					/3600.0)::numeric(6,2)) filter (where yicode = 'VAC'), 0) as vac_hours,
			yicode
	from ( -- d: clock hours
		select a.yico_, a.pymast_employee_number, a.yiclkind, a.yiclkint, a.yiclkoutd, a.yiclkoutt, a.yicode
		from arkona.ext_pypclockin a
		where a.yicode in ('HOL','O','PTO','VAC') -- ('HOL','666','O','PTO','VAC')
			and a.yiclkind between '05/25/2021' and '07/17/2021'
			and yico_ = 'RY2') d
	group by yico_, pymast_employee_number, yiclkint, yiclkoutt, yiclkind, yiclkoutd, yicode) e
left join arkona.ext_pymast f on e."Employee ID" = f.pymast_employee_number
-- 
-- ) x order by "Pay Date"
-- group by "Employee Name"
order by "Employee Name", "In Date", "Time In"
