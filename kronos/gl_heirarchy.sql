﻿select a.payroll_run_number, a.company_number, a.employee_, a.employee_name, a.department_code, c.description, a.distrib_code,
  d.title, b.gross_dist, b.gross_Expense_act_

select a.company_number, c.description as department, d.title as position, a.distrib_code, 
  a.employee_name, b.gross_dist, b.gross_Expense_act_  
from arkona.ext_pyhshdta a
left join arkona.ext_pyactgr b on a.distrib_code = b.dist_code
  and a.company_number = b.company_number
--   and b.current_row
left join arkona.ext_pypclkctl c on a.company_number = c.company_number
  and a.department_code = c.department_code
left join pto.compli_users d on a.employee_ = d.user_id  
where payroll_cen_year = 120
  and payroll_run_number = 1231200
order by a.company_number, c.description, d.title, a.distrib_code, a.employee_name, b.gross_Expense_act_ 


-- from the perspective of account 
select distinct gross_expense_act_, gross_dist, a.company_number, c.description as department, title as position, distrib_code  
from arkona.ext_pyhshdta a
left join arkona.ext_pyactgr b on a.distrib_code = b.dist_code
  and a.company_number = b.company_number
--   and b.current_row
left join arkona.ext_pypclkctl c on a.company_number = c.company_number
  and a.department_code = c.department_code
left join pto.compli_users d on a.employee_ = d.user_id  
where payroll_cen_year = 120
  and payroll_run_number = 1231200
order by gross_expense_act_, gross_dist, a.company_number, c.description, title, distrib_code  



select distinct payroll_run_number from arkona.ext_pyhshdta where payroll_cen_year = 121 order by payroll_run_number

select *
from arkona.ext_pyactgr
-- where current_row
order by dist_code

select * 
from pto.compli_users
limit 100

select * 
from arkona.ext_glpdept