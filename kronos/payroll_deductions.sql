﻿/*
04/29/21
looking to help kim build the deduction upload for ukg
starting with the master_deduction_list


Priority sequence would not come out of payroll it is which deduction comes first if they do not have enough pay to cover all of them
Eefrequency type would be EveryScheduledPay   (bi-weekly employees)  or Monthly  (semi-monthly employees)
Account Type is  if they have Checking or Savings
ABA  is the route number
The eefrequencyproperty and eeamount earningslist you can disregard 
On the 401k  those are correct for there percent

Let me know if that makes sense
Thanks
KIm


Can we add employee name column after employee id 
Yes we should clean up column c   Medical  ,   Dental,  Vision,   STD,  LTD,   Life, Accident, Hospital,  Critical.  HSA, 
Column H    will be  Fixed Amount would change to  (Flat Amount)  and Percentage would change to (% Of Earnings List)



*/

1st thing i know is need 1 row for every employee/deduction

-- 04/30/21 this looked good to kim, she has now sent me the ukg spreadsheet for me to flesh out
-- Eefrequency type would be EveryScheduledPay   (bi-weekly employees)  or Monthly  (semi-monthly employees)
-- Priority sequence would not come out of payroll it is which deduction comes first if they do not have enough pay to cover all of them
-- Account Type is  if they have Checking or Savings
-- ABA  is the route number
-- The eefrequencyproperty and eeamount earningslist you can disregard 


select * from jon.direct_deposit limit 10


select distinct ded_pay_code, description  
from  arkona.ext_pypcodes  
where ded_pay_code in ('103','107','111','115','119','120','121','122','123','124','125','126','299','295','305','91B','91C','99A','99B','99C','96')
order by description

select * 
from  arkona.ext_pydeduct  
where ded_pay_code in ('91B','91C','99A','99B','99C','96')
order by ded_pay_code

select * from arkona.ext_pydeduct where fixed_ded_amt = 50 order by ded_pay_code

select fixed_ded_amt, count(*) from arkona.ext_pydeduct where ded_pay_code = '96' group by fixed_ded_amt order by fixed_ded_amt


select * from arkona.ext_pypcodes where ded_pay_code in ('91B','91C','99A','99B','99C','96')

select * from arkona.ext_pypcodes where ded_pay_code = '227' order by ded_pay_code

--05/07/21 saved as ukg_employee_deductions_v3, sent to kim as GM_employee_deductions_v3 & HN_employee_deductions_v3
-- shit fucking excel converting some account_number to scientific notation
-- 05/11 V4 omitted ded code 91 & 99, don't need username, bank info
-- 05/12 V5 need to add deduction 227, new labels for deductions, add priority sequencing for deductions

-- 1579
drop table if exists deductions;
create temp table deductions as
select 'Auto Generate' as "Username",aa."Employee ID", aa."Employee Name", cc.ssn as "SSN", 
  case aa.store
		when 'RY1' then '45-0236743'
		when 'RY2' then '45-0415948'
  end as "EIN Tax Id",
  case aa.store
		when 'RY1' then 'Rydell GM'
		when 'RY2' then 'Honda Nissan'
  end as "EIN Name",
  case 
		when bb.ded_pay_code = '111' then 'Dental Pre-Tax'
		when bb.ded_pay_code = '295' then 'Vision Pre-Tax'
		when bb.ded_pay_code = '107' then 'Medical Pre-Tax'
		when bb.ded_pay_code = '96' then 'HSA Pre-Tax Individual'
		when bb.ded_pay_code = '115' then 'FSA Dependent Care'
		when bb.ded_pay_code = '119' then 'FSA Medical'
		when bb.ded_pay_code in ('91','91b','91c') then '401K'
		when bb.ded_pay_code in ('99','99a','99b','99c') then 'Roth 401k'
		when bb.ded_pay_code = '121' then 'Long Term Disability'
		when bb.ded_pay_code = '120' then 'Short Term Disability'
		when bb.ded_pay_code = '122' then 'Employee Voluntary Life'
		when bb.ded_pay_code = '123' then 'Accident'
		when bb.ded_pay_code = '124' then 'Hospital'
		when bb.ded_pay_code = '125' then 'Critical Illness'
		when bb.ded_pay_code = '103' then 'Rydell Cares'
		when bb.ded_pay_code = '305' then 'Flex Fee'
		when bb.ded_pay_code = '227' then 'Uniforms'
		else 'XXXXXXXXXXXXXXXXXXX'
  end as "Deduction Name", 
  aa.hire_date as "Start Date",
  case 
		when bb.ded_pay_code = '111' then 4
		when bb.ded_pay_code = '295' then 5
		when bb.ded_pay_code = '107' then 3
		when bb.ded_pay_code = '96' then 7
		when bb.ded_pay_code = '115' then 8
		when bb.ded_pay_code = '119' then 9
		when bb.ded_pay_code in ('91','91b','91c') then 1
		when bb.ded_pay_code in ('99','99a','99b','99c') then 2
		when bb.ded_pay_code = '121' then 10
		when bb.ded_pay_code = '120' then 11
		when bb.ded_pay_code = '122' then 12
		when bb.ded_pay_code = '123' then 13
		when bb.ded_pay_code = '124' then 14
		when bb.ded_pay_code = '125' then 15
		when bb.ded_pay_code = '103' then 18
		when bb.ded_pay_code = '305' then 17
		when bb.ded_pay_code = '227' then 16
  end as "PrioritySequence",   
  case when aa.pay_period = 'B' then 'EveryScheduledPay' else 'Monthly' end as "EEFrequencyType", 
  null as "EEFrequencyProperty",
  case when bb.fixed_ded_pct = 'Y' then '% Of Earnings List' else 'Flat Amount' end as "EEAmountCalcMethod",
  case when bb.fixed_ded_pct = 'Y' then bb.fixed_Ded_amt end as "EEAmountPercent",
	case when bb.fixed_ded_pct is null then bb.fixed_ded_amt end as "EEAmountDollars",
	null as "EEAmountEarningsList",
	case when aa.dep_acct_type = 'C' then 'Checking' else 'Savings' end as "Account Type",
	dd.routing as "ABA",
	'="' ||dd.account|| '"' as "Account Number"
from (
	select a.pymast_company_number as store, a.pymast_employee_number as "Employee ID", b.ssn, a.employee_name as "Employee Name",
		arkona.db2_integer_to_date(a.hire_date) as hire_date, a.pay_period, c.dep_acct_type
	from arkona.ext_pymast a
	left join jon.ssn b on a.pymast_employee_number = b.employee_number
	left join arkona.ext_pypeddep c on a.pymast_employee_number = c.employee_number
	where a.pymast_company_number in ('RY1', 'RY2')
		and a.active_code <> 'T') aa
join (		
	select a.employee_number, a.ded_pay_code, a.fixed_ded_pct, a.fixed_ded_amt, a.ded_freq, b.description, b.account_number
	from arkona.ext_pydeduct a
	join arkona.ext_pypcodes b on a.company_number = b.company_number
		and a.ded_pay_code = b.ded_pay_code
	where a.ded_pay_code in ('103','107','111','115','119','120','121','122','123','124','125','227',
			'126','299','295','305','91','91B','91C','99','99A','99B','99C','96')) bb on aa."Employee ID" = bb.employee_number
left join jon.ssn cc on aa."Employee ID" = cc.employee_number	
left join jon.direct_deposit dd on aa."Employee ID" = dd.employee_number	
-- order by aa.store, "Employee Name", bb.ded_pay_code, priority
order by aa.store, "Employee Name", "PrioritySequence"
-- order by "Deduction Name"


-- select string_agg('"'||column_name||'"',',')
-- from information_schema.columns 
-- where table_name = 'deductions';

select "Username","Employee ID","Employee Name","SSN","EIN Tax Id","EIN Name","Deduction Name",	"Start Date",
	/*"PrioritySequence",*/ row_number() over (partition by "Employee Name" order by "PrioritySequence") as "PrioritySequence",
	"EEFrequencyType","EEFrequencyProperty","EEAmountCalcMethod","EEAmountPercent","EEAmountDollars",
	"EEAmountEarningsList","Account Type","ABA","Account Number","ded_pay_code"
from deductions
order by "EIN Tax Id", "Employee Name", 


select "Employee ID", "Employee Name", count(*)
from deductions
group by "Employee ID", "Employee Name"
order by count(*) desc







