﻿
for kim
one time knock off of unum_misc_monthly for comparing ukg deductions
use the 10/01/2021 check date, should include everyone
add the following deductions

107- Medical ins
103-rydell cares
305-flex 
115- day/care expense
119- med expense fsa
227- uniforms



do $$
declare
  _year integer := 21;
  _month integer := 10;
  _day integer := 1;
begin  
drop table if exists unum cascade;
create temp table unum as
select a.company_number as "STORE", a.employee_name as "NAME", c.ssn,
  sum(amount) filter (where b.code_id = '111') as "DENTAL (111)",
  sum(amount) filter (where b.code_id = '120') as "STD (120)",
  sum(amount) filter (where b.code_id = '121') as "LTD (121)",
  sum(amount) filter (where b.code_id = '122') as "LIFE (122)",
  sum(amount) filter (where b.code_id = '123') as "ACCIDENT (123)",
  sum(amount) filter (where b.code_id = '124') as "HOSPITAL (124)",
  sum(amount) filter (where b.code_id = '125') as "CRITICAL (125)",
  sum(amount) filter (where b.code_id = '295') as "VISION (295)",

  sum(amount) filter (where b.code_id = '107') as "MEDICAL (107)",
  sum(amount) filter (where b.code_id = '103') as "RYDELL CARES (103)",
  sum(amount) filter (where b.code_id = '305') as "FLEX (305)",
  sum(amount) filter (where b.code_id = '115') as "DAY CARE (115)",
  sum(amount) filter (where b.code_id = '119') as "MED FSA (119)",
  sum(amount) filter (where b.code_id = '227') as "UNIFORMS (227)"
from (
  select a.company_number, a.employee_name, payroll_run_number, employee_, payroll_cen_year
  from arkona.ext_pyhshdta a
  where a.check_year = _year
    and a.check_month = _month
    and a.check_day = _day
  group by a.company_number, a.employee_name, payroll_run_number, employee_, payroll_cen_year) a
join arkona.ext_pyhscdta b on a.company_number = b.company_number
  and a.payroll_cen_year = b.payroll_cen_year
  and a.payroll_run_number = b.payroll_run_number
  and a.employee_ = b.employee_number
  and b.code_id in ('111','120','121','122','123','124','125','295',
		'107','103','305','115','119','227') 
left join jon.ssn c on a.employee_ = c.employee_number
where a.company_number in ('RY1','RY2')  
group by a.company_number, a.employee_name, c.ssn  
order by a.company_number, a.employee_name;
end  
$$;
select * from unum;

select pymast_company_number, employee_name from arkona.ext_pymast where active_code <> 'T' order by pymast_company_number, employee_name