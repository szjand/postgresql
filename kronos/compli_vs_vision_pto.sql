﻿
select last_name, first_name, user_id as "emp#", title, supervisor_name
from pto.compli_users 
where status = '1'


select location_id as store, supervisor_name, title, count(*), string_agg(last_name, ',') --last_name, first_name, user_id as "emp#", title
from pto.compli_users 
where status = '1'
group by location_id, supervisor_name, title
order by title

-- compli supervisors & positions
drop table if exists tem.compli_supervisor_positions cascade;
create table tem.compli_supervisor_positions as
select a.supervisor_name, b.title, b.user_id
-- select * 
from (
  select supervisor_name
  from pto.compli_users 
  where status = '1'
  group by supervisor_name) a
left join  pto.compli_users b on a.supervisor_name = b.first_name || ' ' || b.last_name;

-- employees who are their own supervisor
select * 
from pto.compli_users
where supervisor_name = first_name || ' ' || last_name
  and status = '1'

-- vision
drop table if exists tem.vision_pto cascade;
create table tem.vision_pto as
select a.auth_by_department, a.auth_by_position, (initcap(c.employee_first_name)  || ' ' || initcap(c.employee_last_name))::citext as authorizer, 
  a.auth_for_department, a.auth_for_position, d.employee_number as emp_num, e.employee_name
from pto.authorization a
join pto.position_fulfillment b on a.auth_by_department = b.department
  and a.auth_by_position = b.the_position
join arkona.ext_pymast c on b.employee_number = c.pymast_employee_number
join pto.position_fulfillment d on a.auth_for_department = d.department
  and a.auth_for_position = d.the_position
join arkona.ext_pymast e on d.employee_number = e.pymast_employee_number;

select employee_name, emp_num, auth_for_department ||' : ' ||  auth_for_position as emp_position, authorizer, auth_for_department ||' : ' ||  auth_for_position as auth_position
from tem.vision_pto

-- compare vision to compli
select aa.*, bb.*
from (
  select a.last_name ||', ' || a.first_name as employee_name, a.user_id as emp_num, a.title, b.supervisor_name, b.title
  from pto.compli_users a
  left join tem.compli_supervisor_positions b on a.supervisor_name = b.supervisor_name
  where a.status = '1') aa
join (
  select employee_name, emp_num, auth_for_department ||' : ' ||  auth_for_position as emp_position, authorizer, auth_for_department ||' : ' ||  auth_for_position as auth_position
  from tem.vision_pto) bb on aa.emp_num = bb.emp_num
where aa.supervisor_name <> bb.authorizer

select * from arkona.ext_pymast limit 10