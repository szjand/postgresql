﻿/*
04/28/21
Hey Jon-

Sorry, I never followed back up with you on this one.  Could you please send me this same report but with 
only the people that have been terminated this year so far?  I already sent Kronos all of the information 
that you originally sent so would like to just send a completely new file to them with just the terminated people.

Thanks!

Laura Roth 

*/

select pymast_company_number,  '' as "Username", a.pymast_employee_number as "Employee Id", employee_first_name as "First Name", employee_last_name as "Last Name", 
	b.ssn as "Social Security", tel_area_code||'-'||telephone_number as "Home Phone", address_1 as "Address 1", address_2 as "Address 2",
	city_name as "City", state_code_address_ as "State", zip_code as "Zip", 
	case when active_code = 'A' then 'Full' when active_code = 'P' then 'Part' end as "Status",
	case payroll_class
		when 'S' then 'Salary'
		when 'H' then 'Hourly'
		when 'C' then 'Commission'
	end as "Pay Type",
	arkona.db2_integer_to_date(org_hire_date) as "Hired",
	c."Terminated",
-- 	case when c."Terminated" is null then Null else arkona.db2_integer_to_date(hire_date) end as "Re Hired",
-- 	case when arkona.db2_integer_to_date(hire_date) > c."Terminated" then arkona.db2_integer_to_date(hire_date) else null end as "Re Hired",
	 arkona.db2_integer_to_date(hire_date),
	d.*
from arkona.ext_pymast a
left join jon.ssn b on a.pymast_employee_number = b.employee_number
left join (
	select distinct pymast_employee_number, arkona.db2_integer_to_date(termination_date) as "Terminated"
	from arkona.xfm_pymast a	
	where pymast_company_number in ('RY1','RY2')
		and termination_date <> 0
		and employee_name not in ('AAMODT, MATTHEW','BINA, BENJAMIN','HAGER, DELWYN D' -- exclude xf between stores
			'HOLLAND, NIKOLAI','LONGORIA, MICHAEL')) c on a.pymast_employee_number = c.pymast_employee_number
left join (
  select employee_name, array_agg(distinct pymast_employee_number order by pymast_employee_number)
  from arkona.xfm_pymast
  group by employee_name) d on a.employee_name = d.employee_name		
where c."Terminated" between '01/01/2021' and current_date -- active_code <> 'T'
  and pymast_company_number in ('RY1','RY2')  --and a.pymast_employee_number = '135987'
  and a.employee_last_name not in ('HOLLAND')
order by  pymast_company_number, "Last Name"


/*
04/20/21
Hi Jon-

Attached is the census that we have been talking about on the calls.  I am not planning to fill out every box on here 
so I highlighted in red the ones that we need to add in.  Would you be able to do this in 2 separate files for Honda and GM?  
We will need to pull one for benefits later but I would like them to send me a grid for that to tell me what kind of formatting they want.

Please let know if you have any questions.

Thanks!

questions: 
What are you looking for in column C (Username)?

In Hired/Terminated/ReHired (BA/BG/BH) does that include transfers between stores, like Nik Holland going back and forth and back and forth?

Hi Jon-

Let’s leave “c” blank for right now.  For the Rehire status I would like to only include dates of people that were actually rehired and not people that transferred back and forth from store to store.  

Thanks!



*/

-- select * from arkona.ext_pymast where active_code <> 'T' and org_hire_date <> hire_date limit 4
-- select x."Employee Id" from (
select pymast_company_number,  '' as "Username", a.pymast_employee_number as "Employee Id", employee_first_name as "First Name", employee_last_name as "Last Name", 
	b.ssn as "Social Security", tel_area_code||'-'||telephone_number as "Home Phone", address_1 as "Address 1", address_2 as "Address 2",
	city_name as "City", state_code_address_ as "State", zip_code as "Zip", 
	case when active_code = 'A' then 'Full' when active_code = 'P' then 'Part' end as "Status",
	case payroll_class
		when 'S' then 'Salary'
		when 'H' then 'Hourly'
		when 'C' then 'Commission'
	end as "Pay Type",
	arkona.db2_integer_to_date(org_hire_date) as "Hired",
	c."Terminated",
	case when c."Terminated" is null then Null else arkona.db2_integer_to_date(hire_date) end as "Re Hired",
	d.*
from arkona.ext_pymast a
left join jon.ssn b on a.pymast_employee_number = b.employee_number
left join (
	select distinct pymast_employee_number, arkona.db2_integer_to_date(termination_date) as "Terminated"
	from arkona.xfm_pymast a	
	where pymast_company_number in ('RY1','RY2')
		and termination_date <> 0
		and employee_name not in ('AAMODT, MATTHEW','BINA, BENJAMIN','HAGER, DELWYN D' -- exclude xf between stores
			'HOLLAND, NIKOLAI','LONGORIA, MICHAEL')) c on a.pymast_employee_number = c.pymast_employee_number
left join (
  select employee_name, array_agg(distinct pymast_employee_number order by pymast_employee_number)
  from arkona.xfm_pymast
  group by employee_name) d on a.employee_name = d.employee_name		
where active_code <> 'T'
  and pymast_company_number in ('RY1') --and a.pymast_employee_number = '135987'  
order by  pymast_company_number, "Last Name"
-- ) x group by x."Employee Id" having count(*) > 1
-- jacob siedschlag : delete 1 row from spreadsheet

select distinct pymast_employee_number, arkona.db2_integer_to_date(termination_date)
from arkona.xfm_pymast a	
where active_code <> 'T'
  and pymast_company_number in ('RY1','RY2')
  and termination_date <> 0


select pymast_company_number, pymast_employee_number, employee_name, hire_Date, org_hire_Date,termination_date, row_from_Date, row_thru_date, current_row
from arkona.xfm_pymast a	
-- where pymast_employee_number = '168573'
where employee_name = 'anderson, nicholas'
order by pymast_key
	  

select * from jon.ssn order by full_name

-- 05/04/2021 3 new employees
insert into jon.ssn (employee_number,full_name,ssn)values
('135768','ALBAUGH, MICKEY','502-82-5831'),
('147658','HARRIS, ANTHONY','357-94-7286'),
('120318','TIERNEY, GABRIELLE','647-60-8541');