 
During the discovery call we will review the items in the workbook to ensure all is correct, answer any questions you have, and allow me to confirm any items I need further detail on.  In addition to this document, I will need the following documents uploaded to Feedback (instructions attached) in order to begin your build: 
•	Recent payroll register for each entity 
•	GL reports from the same pay date as the payroll register(s) 
•	401k reports from the same pay date as the above items 
•	Most recent tax reports for each entity (these often come in one quarterly package from your payroll provider):  
o	941 
o	State Income Tax for each state 
o	SUI/SUTA for each state 
o	Local tax reports for all localities (if applicable) 
•	Copy of the administrator’s plan document for each retirement plan 
Please review the available times I have provided below and reply to advise which one(s) work for your team.  We will schedule this call for 2 hours but will respect your team’s time and only take as long as we need to get through the material.   
