Attached is the TLM discovery we reviewed today. Please complete the highlighted tabs in yellow: Counters, and begin Cost Center tab. You can return back to me by 2/3. 
Please let me know if I missed anything.

Items Covered on call: 
•	Success criteria
•	Pay periods
•	Holiday
•	Counters 
•	Time off Request (Yes to Best Practice)
•	Timesheet change request (Yes to Best Practice)
•	Timesheet Approval (Yes to Best Practice)
•	Pay Calculations
Action Items: 
Client Action Items	Owner	Due Date
Completed highlighted tabs in yellow on TLM discovery and send back to me	Rydell	2/3/2021
		


Please let me know if I missed anything from our call today. Our next call is scheduled for 2/3/2021 at 11:30am EST 10:30am CST  and the agenda is included below. Have a great day!

Agenda for Next Call:
•	Complete review of Pay calculations
•	Manager permissions
•	Go over Cost Center structure
•	Activities
•	Any additional questions
