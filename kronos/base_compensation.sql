﻿/*
05/06/2021
Good Morning Rachel  I know that Richard is gone was wondering if someone else could answer some questions for me on the payroll templates?
So on the base compensation   for the amount column   would that be there hourly rate/  and salary person would it be what they get each pay period? 
		– Yes to hourly, I would do salary using the yearly amount. But either would work. 
And then the amount period  would be hour   for the hourly people and pay period for the salary people? 
		- Yes, if doing yearly salary for salaried employees them they would be Year. 
Hours we are beweekly  so 80 ? 
		– Yes, and 2080 for salaried if you do it per year
Hours period  would be pay period? 
		– yes, and year for salaried if you do year
Num pp in year  for biweekly  26  and for monthly 12 ? 
		- Yes
Auto pay amount earning code  would be salary or hourly or commission? 
		– This one is not required per the instructions tab, but go ahead and put one of the 3 options you have listed and Richard can remove if not needed. 

Kim Miller
*/

/*
5/10/21
see docs/base_compensation_questions.xlsx
*/
select pymast_employee_number, employee_name, payroll_class, base_salary, base_hrly_rate
from arkona.ext_pymast
where active_code <> 'T'
order by base_hrly_rate desc

select * from jon.ssn limit 10
insert into jon.ssn (employee_number, full_name, ssn) values
('121869','HAMS, JADIN','477-39-9483'),
('184937','MARTIN, JOSEPH','501-84-0965');

select * from arkona.ext_pymast where pymast_employee_number = '16425'

select * from ads.ext_tp_techs order by fromdate desc	

select * from sls.personnel where end_date > current_date

select 'Auto Generate' as "Username", a.pymast_employee_number as "Employee ID", 
	b.ssn as "SSN", a.employee_name as "Employee Name",
  case a.pymast_company_number
		when 'RY1' then '45-0236743'
		when 'RY2' then '45-0415948'
  end as "EIN Tax Id",
  case a.pymast_company_number
		when 'RY1' then 'Rydell GM'
		when 'RY2' then 'Honda Nissan'
  end as "EIN Name",
	arkona.db2_integer_to_date(a.hire_date) as "Start Date",
	case 
		when a.payroll_class = 'H' then a.base_hrly_rate::text
		when a.payroll_class = 'S' then a.base_salary::TEXT
		when a.payroll_class =  'C' and a.distrib_code in ('STEC','BTEC','TECH','WTEC') then null
		when a.payroll_class =  'C' then 'Commission'
	end as "Amount",
	case 
		when a.payroll_class =  'H' then 'Hour'
		when a.payroll_class =  'S' then 'Pay Period'
		when a.payroll_class =  'C' then null
	end as "Amount Period",	
	case
		when a.payroll_class =  'H' then 80
		when a.payroll_class =  'S' then 80
		when a.payroll_class =  'C' and a.distrib_code in ('SALM','MGR') then 2080
		when a.payroll_class =  'C' and a.distrib_code in ('STEC','BTEC','TECH','WTEC') then 80
		when a.payroll_class = 'C' and c.employee_number is not null then 2080
		when a.payroll_class =  'C' then null
	end as "Hours",
	case 
		when a.payroll_class =  'H' then 'Pay Period'
		when a.payroll_class =  'S' then 'Pay Period'
		when a.payroll_class =  'C' and a.distrib_code in ('SALM','MGR') then 'Year'
		when a.payroll_class =  'C' and a.distrib_code in ('STEC','BTEC','TECH','WTEC') then 'Pay Period'
		when a.payroll_class =  'C' and c.employee_number is not null then 'Year'
		when a.payroll_class =  'C' then null
	end as "Hours Period",
	case
		when a.payroll_class =  'H' then 26
		when a.payroll_class =  'S' then 26
		when a.payroll_class =  'C' and a.distrib_code in ('SALM','MGR') then 12
		when a.payroll_class =  'C' and a.distrib_code in ('STECH','BTEC','TECH','WTEC') then 26
		when a.payroll_class =  'C' and c.employee_number is not null then 12
		when a.payroll_class =  'C' then null
	end as "Num PP In Year"	,
	case 
		when a.payroll_class =  'H' then 'Hourly'
		when a.payroll_class =  'S' then 'Salary'
		when a.payroll_class =  'C' then 'Commission'
	end as "AutoPay AMount Earning Code",
	a.distrib_code						
from arkona.ext_pymast a
left join jon.ssn b on a.pymast_employee_number = b.employee_number
left join sls.personnel c on a.pymast_employee_number = c.employee_number
  and c.end_date > current_date
where active_code <> 'T' -- and employee_number in ('16425','1118722','13725','123100','128530')
  and a.pymast_company_number in ('RY1','RY2')
order by "EIN Tax Id", "Employee Name";
-- order by "AutoPay AMount Earning Code", a.distrib_code, "EIN Tax Id", "Employee Name"

