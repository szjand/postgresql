﻿07/13/21
start with exact copy of payroll_history_2nd_quarter_2021.sql
kim running 3rd july payroll today, should be ready about lunchtime
need to rename all third_quarter to third_quarter
need fresh cut of payroll tables
change check_month to 7

drop table if exists ukg.ph_third_quarter_adds_deducts_base cascade;
create unlogged table ukg.ph_third_quarter_adds_deducts_base as
		select dds.db2_integer_to_date(('20'||c.check_year::text || lpad(c.check_month::text, 2, '0') ||lpad(c.check_day::text, 2, '0'))::integer) as check_date,
		  a.company_number, --a.payroll_run_number,
			a.employee_number, 
			a.code_type ||'-'|| 
				case 
					when b.description like '%COMM%' and d.pay_period = 'b' then 'Biweekly Commission' 
					when b.description like '%COMM%' and d.pay_period = 's' then 'Monthly Commission' 
					when b.description = 'draws' and d.distrib_code = 'sale' then 'Sales Volume Commission' ------------------------------------------------
					when b.description = 'draws' and d.distrib_code = 'team' then 'F&I Commission' --------------------------------------------------
					when b.description = 'draws' then 'Salary' --------------------------------------------------------------------------------------
					when b.description in ('1  ACCTS REC', '2 ACCOUNTS RECI') then 'Accounts Receivable'
					when b.description = 'holiday pay' then 'Holiday'
					when b.description = 'overtime pay' then 'Overtime'
					when b.description = 'pto pay' then 'PTO'
					when b.description = 'vacation pay' then 'PTO'
					when b.description = 'a-z sales f&i' then 'F&I Commission'
					when b.description = 'covid 19' then 'FFCRA Sick Leave 10 Days'
					when b.description = 'covid-fmla' then 'FFCRA FMLA 10 Weeks'
					when b.description =  'lease program' then 'Lease Program'
					when b.description = 'pay out pto' then 'PTO Payout'
					when b.description = 'pulse pay' then 'Pulse Pay'
					when b.description in ('rate adjust','rate adjustment') then 'Hourly Rate Adjust'
					when b.description = 'salary' then 'Salary'
					when b.description = 'sales vacation' then 'PTO'
					when b.description = 'spiff pay outs' then 'Cash Spiff'
					when b.description = 'tech hours pay' then 'Flat Rate'
					when b.description = 'tech xtra bonus' then 'Flat Rate Premium Pay'
					when b.description = 'vac/pto pay' then 'PTO'
					when b.description = 'volunterr pto' then 'Volunteer PTO'
					when b.description = '13 garnishment' then 'Garnshiment'
					when b.description = '15 life aft tax' then 'Employee Voluntary Life'
					when b.description in ('Critical Illness', '17 cancer prem') then 'Critical Illness'
					when b.description in ('401k','Accident','Accounts Receivable','Dental Pre-Tax','Employee Voluntary Life',
																	'Flex Fee','FSA Dependent Care','FSA Medical','Hospital','HSA Pre-Tax Individual','Long Term Disability',
																	'Medical Pre-Tax','Rydell Cares','Short Term Disability','Uniforms','Vision Pre-Tax','Whole Life') then b.description
					when b.description = 'Roth 401k' then 'ROTH'
					when b.description in ('ER CONTR- 91B','ER CONTR- 91C') then '401k'
					when b.description in ('ER CONTR- 99','ER CONTR- 99B','ER CONTR- 99C') then 'ROTH'
					else '**************' || b.description 
				end as description, a.amount -- sum(amount) as amount
		from arkona.ext_pyhscdta a
		join jon.payroll_codes b on a.code_type = b.code_type
			and a.code_id = b.code_id
			and a.company_number = b.store
		join arkona.ext_pyhshdta c on a.company_number = c.company_number
			and a.payroll_run_number = c.payroll_run_number
			and a.employee_number = c.employee_
			and c.check_year = 21
-- 			and c.check_month between 4 and 6  -- 2nd quarter
			and c.check_month = 7  -- 3rd quarter
			and c.seq_void <> '0J' -- void checks
			and a.amount <> 0
-- 		where a.company_number = 'RY1' 
		left join arkona.ext_pymast d on a.employee_number = d.pymast_employee_number;


drop table if exists ukg.ph_third_quarter_adds_deducts cascade;
create unlogged table ukg.ph_third_quarter_adds_deducts as
select check_date, company_number, employee_number, description, sum(amount) as amount
from ukg.ph_third_quarter_adds_deducts_base
group by check_date, company_number, employee_number, description;
alter table ukg.ph_third_quarter_adds_deducts
add primary key(check_date,employee_number,description);

-- insert this result into a table with mapped field names
drop table if exists third_quarter_adds_deducts_pivot;
select jon.colpivot (
	'third_quarter_adds_deducts_pivot',
	$$
	  select * 
	  from ukg.ph_third_quarter_adds_deducts
	$$,
	array['company_number','check_date','employee_number'], -- non pivoted column headers
	array['description'], -- pivot headers
	'#.amount', -- data for the pivoted columns
	'description'); -- sort order (left to right) of the pivoted headers
alter table third_quarter_adds_deducts_pivot
add primary key(employee_number, check_date);


-- -- 1-FFCRA does not exist in the second quarter
-- -- regenerate the alter table statements to get the actual column names for adds deducts used in the second quarter
-- -- -- remove the single quotes from the column names
select string_agg('alter table third_quarter_adds_deducts_pivot rename column "'||column_name||'" to "'||replace(column_name,'''','')||'"', ';' order by ordinal_position)
from information_schema.columns
where table_name = 'third_quarter_adds_deducts_pivot'
  and column_name not in ('company_number','check_date','employee_number')

alter table third_quarter_adds_deducts_pivot rename column "'0-Overtime'" to "0-Overtime";alter table third_quarter_adds_deducts_pivot rename column "'0-PTO'" to "0-PTO";alter table third_quarter_adds_deducts_pivot rename column "'1-Biweekly Commission'" to "1-Biweekly Commission";alter table third_quarter_adds_deducts_pivot rename column "'1-F&I Commission'" to "1-F&I Commission";alter table third_quarter_adds_deducts_pivot rename column "'1-Flat Rate'" to "1-Flat Rate";alter table third_quarter_adds_deducts_pivot rename column "'1-Flat Rate Premium Pay'" to "1-Flat Rate Premium Pay";alter table third_quarter_adds_deducts_pivot rename column "'1-Hourly Rate Adjust'" to "1-Hourly Rate Adjust";alter table third_quarter_adds_deducts_pivot rename column "'1-Lease Program'" to "1-Lease Program";alter table third_quarter_adds_deducts_pivot rename column "'1-Monthly Commission'" to "1-Monthly Commission";alter table third_quarter_adds_deducts_pivot rename column "'1-PTO'" to "1-PTO";alter table third_quarter_adds_deducts_pivot rename column "'1-Pulse Pay'" to "1-Pulse Pay";alter table third_quarter_adds_deducts_pivot rename column "'1-Salary'" to "1-Salary";alter table third_quarter_adds_deducts_pivot rename column "'1-Sales Volume Commission'" to "1-Sales Volume Commission";alter table third_quarter_adds_deducts_pivot rename column "'2-401k'" to "2-401k";alter table third_quarter_adds_deducts_pivot rename column "'2-Accident'" to "2-Accident";alter table third_quarter_adds_deducts_pivot rename column "'2-Accounts Receivable'" to "2-Accounts Receivable";alter table third_quarter_adds_deducts_pivot rename column "'2-Critical Illness'" to "2-Critical Illness";alter table third_quarter_adds_deducts_pivot rename column "'2-Dental Pre-Tax'" to "2-Dental Pre-Tax";alter table third_quarter_adds_deducts_pivot rename column "'2-Employee Voluntary Life'" to "2-Employee Voluntary Life";alter table third_quarter_adds_deducts_pivot rename column "'2-FSA Dependent Care'" to "2-FSA Dependent Care";alter table third_quarter_adds_deducts_pivot rename column "'2-FSA Medical'" to "2-FSA Medical";alter table third_quarter_adds_deducts_pivot rename column "'2-Garnshiment'" to "2-Garnshiment";alter table third_quarter_adds_deducts_pivot rename column "'2-Hospital'" to "2-Hospital";alter table third_quarter_adds_deducts_pivot rename column "'2-HSA Pre-Tax Individual'" to "2-HSA Pre-Tax Individual";alter table third_quarter_adds_deducts_pivot rename column "'2-Long Term Disability'" to "2-Long Term Disability";alter table third_quarter_adds_deducts_pivot rename column "'2-Medical Pre-Tax'" to "2-Medical Pre-Tax";alter table third_quarter_adds_deducts_pivot rename column "'2-ROTH'" to "2-ROTH";alter table third_quarter_adds_deducts_pivot rename column "'2-Rydell Cares'" to "2-Rydell Cares";alter table third_quarter_adds_deducts_pivot rename column "'2-Short Term Disability'" to "2-Short Term Disability";alter table third_quarter_adds_deducts_pivot rename column "'2-Uniforms'" to "2-Uniforms";alter table third_quarter_adds_deducts_pivot rename column "'2-Vision Pre-Tax'" to "2-Vision Pre-Tax";alter table third_quarter_adds_deducts_pivot rename column "'5-401k'" to "5-401k";alter table third_quarter_adds_deducts_pivot rename column "'5-ROTH'" to "5-ROTH"

-- -- remove the code type from the column names

select string_agg('alter table third_quarter_adds_deducts_pivot rename column "'||column_name||'" to "'||substring(column_name,3)||'"', ';' order by ordinal_position)
from information_schema.columns
where table_name = 'third_quarter_adds_deducts_pivot'
  and column_name not in ('company_number','check_date','employee_number')

-- must rename first instance of holiday to holiday_1 and pto to pto_1
-- and last instances of 401k and roth as _ec
-- 3rd quarter no Holiday ?

alter table third_quarter_adds_deducts_pivot rename column "0-Overtime" to "Overtime";alter table third_quarter_adds_deducts_pivot rename column "0-PTO" to "PTO_1";alter table third_quarter_adds_deducts_pivot rename column "1-Biweekly Commission" to "Biweekly Commission";alter table third_quarter_adds_deducts_pivot rename column "1-F&I Commission" to "F&I Commission";alter table third_quarter_adds_deducts_pivot rename column "1-Flat Rate" to "Flat Rate";alter table third_quarter_adds_deducts_pivot rename column "1-Flat Rate Premium Pay" to "Flat Rate Premium Pay";alter table third_quarter_adds_deducts_pivot rename column "1-Hourly Rate Adjust" to "Hourly Rate Adjust";alter table third_quarter_adds_deducts_pivot rename column "1-Lease Program" to "Lease Program";alter table third_quarter_adds_deducts_pivot rename column "1-Monthly Commission" to "Monthly Commission";alter table third_quarter_adds_deducts_pivot rename column "1-PTO" to "PTO";alter table third_quarter_adds_deducts_pivot rename column "1-Pulse Pay" to "Pulse Pay";alter table third_quarter_adds_deducts_pivot rename column "1-Salary" to "Salary";alter table third_quarter_adds_deducts_pivot rename column "1-Sales Volume Commission" to "Sales Volume Commission";alter table third_quarter_adds_deducts_pivot rename column "2-401k" to "401k";alter table third_quarter_adds_deducts_pivot rename column "2-Accident" to "Accident";alter table third_quarter_adds_deducts_pivot rename column "2-Accounts Receivable" to "Accounts Receivable";alter table third_quarter_adds_deducts_pivot rename column "2-Critical Illness" to "Critical Illness";alter table third_quarter_adds_deducts_pivot rename column "2-Dental Pre-Tax" to "Dental Pre-Tax";alter table third_quarter_adds_deducts_pivot rename column "2-Employee Voluntary Life" to "Employee Voluntary Life";alter table third_quarter_adds_deducts_pivot rename column "2-FSA Dependent Care" to "FSA Dependent Care";alter table third_quarter_adds_deducts_pivot rename column "2-FSA Medical" to "FSA Medical";alter table third_quarter_adds_deducts_pivot rename column "2-Garnshiment" to "Garnshiment";alter table third_quarter_adds_deducts_pivot rename column "2-Hospital" to "Hospital";alter table third_quarter_adds_deducts_pivot rename column "2-HSA Pre-Tax Individual" to "HSA Pre-Tax Individual";alter table third_quarter_adds_deducts_pivot rename column "2-Long Term Disability" to "Long Term Disability";alter table third_quarter_adds_deducts_pivot rename column "2-Medical Pre-Tax" to "Medical Pre-Tax";alter table third_quarter_adds_deducts_pivot rename column "2-ROTH" to "ROTH";alter table third_quarter_adds_deducts_pivot rename column "2-Rydell Cares" to "Rydell Cares";alter table third_quarter_adds_deducts_pivot rename column "2-Short Term Disability" to "Short Term Disability";alter table third_quarter_adds_deducts_pivot rename column "2-Uniforms" to "Uniforms";alter table third_quarter_adds_deducts_pivot rename column "2-Vision Pre-Tax" to "Vision Pre-Tax";alter table third_quarter_adds_deducts_pivot rename column "5-401k" to "401k_ec";alter table third_quarter_adds_deducts_pivot rename column "5-ROTH" to "ROTH_ed"

-- 2nd quarter remove FFCRA FMLA 10 Weeks, FFCRA Sick Leave 10 Days, Volunteer PTO
-- 3rd quarter no need for Holiday_1 or Holiday, No Cash Spiffs, No PTO Payouts, No Flex Fee, no Whole Life, no Roth: left aliases in set value to 0
drop table if exists ukg.ph_third_quarter_adds_deducts_pivot cascade;
create unlogged table ukg.ph_third_quarter_adds_deducts_pivot as
-- combine the pto and holiday fields into one
select "company_number","check_date","employee_number",/*coalesce("Holiday_1",0) + coalesce("Holiday",0) as "Holiday" */ 0 as "Holiday",
	"Overtime",coalesce("PTO_1",0) + coalesce("PTO",0) as "PTO","Biweekly Commission",
	0 as "Cash Spiff",/*"FFCRA FMLA 10 Weeks","FFCRA Sick Leave 10 Days",*/"F&I Commission","Flat Rate","Flat Rate Premium Pay",
	"Hourly Rate Adjust","Lease Program","Monthly Commission",0 as "PTO Payout","Pulse Pay","Salary",
	"Sales Volume Commission",/*"Volunteer PTO",*/"401k","Accident","Accounts Receivable","Critical Illness",
	"Dental Pre-Tax","Employee Voluntary Life",0 as "Flex Fee","FSA Dependent Care","FSA Medical","Garnshiment","Hospital",
	"HSA Pre-Tax Individual","Long Term Disability","Medical Pre-Tax","ROTH","Rydell Cares","Short Term Disability",
	"Uniforms","Vision Pre-Tax",0 as "Whole Life","401k_ec",0 as "ROTH_ec"
from third_quarter_adds_deducts_pivot;
alter table ukg.ph_third_quarter_adds_deducts_pivot
add primary key(check_date, employee_number);

select * from third_quarter_adds_deducts_pivot where "employee_number" = '128530'


-- 2nd quarter remove FFCRA FMLA 10 Weeks
-- cant use pymast for payroll class
drop table if exists ukg.ph_third_quarter_paychecks cascade;
create unlogged table ukg.ph_third_quarter_paychecks as
select aa.the_date as "Check Date", aa."EIN Tax Id", aa."EIN NAME", aa.user_id, aa.employee_name, 
	-- compensation
  aa."Regular", coalesce(aa."Salary",0) + coalesce(bb."Salary", 0) as "Salary", bb."Holiday",
  bb."Overtime", bb."PTO", bb."PTO Payout", bb."Monthly Commission", bb."Biweekly Commission", bb."F&I Commission", bb."Sales Volume Commission",
  bb."Lease Program", bb."Pulse Pay", /*bb."FFCRA Sick Leave 10 Days", bb."FFCRA FMLA 10 Weeks",*/ bb."Hourly Rate Adjust",
  bb."Cash Spiff", bb."Flat Rate", bb."Flat Rate Premium Pay", /*bb."Volunteer PTO",*/
  --deductions
  bb."Garnshiment", bb."Accounts Receivable",
  bb."401k", bb."Accident", bb."Critical Illness",
	bb."Dental Pre-Tax", bb."Employee Voluntary Life", bb."Flex Fee", "FSA Dependent Care", bb."FSA Medical", bb."Hospital",
	bb."HSA Pre-Tax Individual", bb."Long Term Disability", bb."Medical Pre-Tax", bb."ROTH", bb."Rydell Cares", bb."Short Term Disability",
	bb."Uniforms", bb."Vision Pre-Tax", bb."Whole Life",
	-- taxes
	aa."FIT", aa."FICA", aa."MEDI", aa."SIT:AZ", aa."SIT:MN", aa."SIT:ND", aa."SIT:OR",
	-- emplr contributions
-- 	coalesce(aa."401K_ec", 0) + coalesce(bb."401k_ec", 0) as "401k_ec", bb."ROTH_ec"
	aa."401K_ec", bb."ROTH_ec"
-- select *
from (
	select 
	  dds.db2_integer_to_date(('20'||a.check_year::text || lpad(a.check_month::text, 2, '0') ||lpad(a.check_day::text, 2, '0'))::integer) as the_date,
	  a.company_number as store, -- payroll_run_number as batch,
		case a.company_number
			when 'RY1' then '45-0236743' 
			when 'RY2' then '45-0415948'
		end as "EIN Tax Id", 
		case a.company_number
			when 'RY1' then 'Rydell Auto Center, Inc.' 
			when 'RY2' then 'Honda Nissan of Grand Forks'
		end as "EIN NAME", 	
		a.employee_ as user_id, a.employee_name, 
		sum(case when a.payroll_class = 'h' then a.base_pay end) as "Regular",
		sum(case when a.payroll_class = 's' then a.base_pay end) as "Salary", 
		-- a.total_gross_pay, 
		sum(a.curr_federal_tax) as "FIT", sum(a.curr_fica) as "FICA", 
		sum(a.curr_employee_medicare) as "MEDI", 
		sum(case when a.state_tax_tab_code = 'AZ' then a.curr_state_tax end) as "SIT:AZ", 
		sum(case when a.state_tax_tab_code = 'MN' then a.curr_state_tax end) as "SIT:MN",
		sum(case when a.state_tax_tab_code = 'ND' then a.curr_state_tax end) as "SIT:ND",
		sum(case when a.state_tax_tab_code = 'OR' then a.curr_state_tax end) as "SIT:OR",
		sum(a.emplr_curr_ret) as "401K_ec"
	from arkona.ext_pyhshdta a
  where a.check_year = 21 
-- 		and a.check_month between 4 and 6
		and a.check_month = 7
  group by 	  dds.db2_integer_to_date(('20'||a.check_year::text || lpad(a.check_month::text, 2, '0') ||lpad(a.check_day::text, 2, '0'))::integer),
	  a.company_number,
		case a.company_number
			when 'RY1' then '45-0236743' 
			when 'RY2' then '45-0415948'
		end, 
		case a.company_number
			when 'RY1' then 'Rydell Auto Center, Inc.' 
			when 'RY2' then 'Honda Nissan of Grand Forks'
		end, 	
		a.employee_, a.employee_name) aa
left join ( -- 118010 had 2 identical rows
	select distinct "company_number","check_date","employee_number","Holiday",
	"Overtime","PTO","Biweekly Commission",
	"Cash Spiff",/*"FFCRA FMLA 10 Weeks","FFCRA Sick Leave 10 Days",*/"F&I Commission","Flat Rate","Flat Rate Premium Pay",
	"Hourly Rate Adjust","Lease Program","Monthly Commission","PTO Payout","Pulse Pay","Salary",
	"Sales Volume Commission",/*"Volunteer PTO",*/"401k","Accident","Accounts Receivable","Critical Illness",
	"Dental Pre-Tax","Employee Voluntary Life","Flex Fee","FSA Dependent Care","FSA Medical","Garnshiment","Hospital",
	"HSA Pre-Tax Individual","Long Term Disability","Medical Pre-Tax","ROTH","Rydell Cares","Short Term Disability",
	"Uniforms","Vision Pre-Tax","Whole Life","401k_ec","ROTH_ec"
	from ukg.ph_third_quarter_adds_deducts_pivot) bb on aa.store = bb.company_number
	  and aa.user_id = bb.employee_number 
	  and aa.the_date = bb.check_date
order by aa.employee_name, check_date;
alter table ukg.ph_third_quarter_paychecks
add primary key("EIN NAME", employee_name, "Check Date");  -- longoria got a 4/15 check from both stores


select * from ukg.ph_third_quarter_paychecks where user_id = '128530'



-- i believe this is adequate granularity from this table
ext_pyptbdta is not in routine update, need to manually update it for new date range
drop table if exists ukg.ph_third_quarter_payroll_batches cascade;
create unlogged table ukg.ph_third_quarter_payroll_batches as
select distinct company_number, payroll_run_number,
	arkona.db2_integer_to_date(payroll_start_date) as from_date, arkona.db2_integer_to_date(payroll_end_date) as thru_date, 
	dds.db2_integer_to_date(check_date) as check_date
from arkona.ext_pyptbdta
-- where dds.db2_integer_to_date(check_date) between '03/01/2021' and '06/30/2021'  -- 2nd quarter
where dds.db2_integer_to_date(check_date) between '07/01/2021' and '07/31/2021'  -- 3rd quarter
  and description not like '%void%';
alter table ukg.ph_third_quarter_payroll_batches
add primary key(company_number,payroll_run_number);
create unique index on ukg.ph_third_quarter_payroll_batches(company_number, payroll_run_number,check_date);

select * from ukg.ph_third_quarter_payroll_batches

-- -- from 2nd quarter
-- --  Key (check_date, employee_)=(2021-04-02, 241085) is duplicated.
-- -- garret e got 2 checks on 4/2 with different from thru dates
-- -- garret k got 2 checks on 4/30 with different from thru dates
-- select * from ukg.ph_third_quarter_employees where employee_ = '241085'
-- select employee_name from arkona.ext_pymast where pymast_employee_number = '241085'
-- select * from ukg.ph_third_quarter_employees where reg_hours is null
-- select employee_name from arkona.ext_pymast where pymast_employee_number = '172308'
-- select * from ukg.ph_third_quarter_employees where employee_ = '172308'

-- -- 3rd quarter
--  Key (check_date, employee_)=(2021-07-09, 190910) is duplicated.
--  dayton got 2 checks on 7/9
-- select employee_name from arkona.ext_pymast where pymast_employee_number = '190910'
-- select * from ukg.ph_third_quarter_employees where employee_ = '190910'
-- Key (check_date, employee_)=(2021-07-09, 18574) is duplicated.
-- select employee_name from arkona.ext_pymast where pymast_employee_number = '18574'
-- select * from ukg.ph_third_quarter_employees where employee_ = '18574'
-- 
-- actually, looks like everyone got 7/08 -> 7/08 checks, but no hours, so just eliminate the from_date = 07/08
-- select * from ukg.ph_third_quarter_employees where from_date = '07/08/2021'
-- 
-- but then 
-- Key (check_date, employee_)=(2021-07-02, 279380) is duplicated. -- ben knudson
-- select employee_name from arkona.ext_pymast where pymast_employee_number = '279380'
-- select * from ukg.ph_third_quarter_employees where employee_ = '279380'
-- 
-- select * from arkona.ext_pyhshdta where employee_ = '279380' and check_month = 7 order by check_day
-- 
-- select * from  ukg.ph_third_quarter_payroll_batches where company_number = 'ry2' order by payroll_run_number
-- 
-- select * from arkona.ext_pyptbdta where payroll_run_number in (702210,702212,709210,709211,713210,715210) and company_number = 'ry2'

drop table if exists ukg.ph_third_quarter_employees cascade;
create unlogged  table ukg.ph_third_quarter_employees as
select company_number, from_date, thru_date, check_date, employee_,  -- remove from thru
-- select company_number, check_date, employee_,
	sum(reg_hours) as reg_hours,
	sum(overtime_hours) as overtime_hours, sum(vacation_taken) as vacation_taken, 
	sum(sick_leave_taken) as sick_leave_taken, sum(holiday_taken) as holiday_taken
from (-- get rid of payroll_run_number
	select a.company_number, a.payroll_run_number, 
		a.from_date, a.thru_date,
-- 		case 
-- 			when b.employee_ = '241085' and a.thru_date = '04/01/2021' then '03/14/2021'
-- 			when b.employee_ = '172308' and a.check_date = '04/30/2021' then '04/11/2021'
-- 			else a.from_date
-- 		end as from_date, 
-- 		case
-- 			when b.employee_ = '172308' and a.check_date = '04/30/2021' then '04/30/2021'
-- 			else a.thru_date
-- 		end as thru_date, 
		a.check_date, b.employee_,
		sum(reg_hours) as reg_hours,
		sum(overtime_hours) as overtime_hours, sum(vacation_taken) as vacation_taken, 
		sum(sick_leave_taken) as sick_leave_taken, sum(holiday_taken) as holiday_taken
	from ukg.ph_third_quarter_payroll_batches a
	left join arkona.ext_pyhshdta b on a.company_number = b.company_number
		and a.payroll_run_number = b.payroll_run_number
		and b.seq_void <> '0J'
  where from_date <> '07/08/2021' and a.payroll_run_number <> 702210  -- 3rd quarter fix for 2 checks on 7/9 and void checks on run 702210
	group by a.company_number, a.payroll_run_number, a.from_date, a.thru_date, a.check_date, b.employee_) aa
group by company_number, from_date, thru_date, check_date, employee_;  -- remove from thru
-- group by company_number, check_date, employee_;
alter table ukg.ph_third_quarter_employees
add primary key(check_date, employee_);

drop table if exists ukg.ph_third_quarter_clock_hours cascade;
create unlogged table ukg.ph_third_quarter_clock_hours as
select a.company_number, a.check_date, a.employee_,
  coalesce(sum(b.reg_hours), a.reg_hours) as "REG", 
  coalesce(sum(b.ot_hours), a.overtime_hours) as "Overtime",
  coalesce(sum(b.vac_hours), a.vacation_taken) + coalesce(sum(b.pto_hours), a.sick_leave_taken) as "PTO",
  coalesce(sum(b.hol_hours), a.holiday_taken) as "Holiday"		
from ukg.ph_third_quarter_employees a
left join arkona.xfm_pypclockin b on a.employee_ = b.employee_number
  and b.the_date between a.from_date and a.thru_date
group by a.company_number, a.check_date, a.employee_;
alter table ukg.ph_third_quarter_clock_hours
add primary key(check_date,employee_);

select * from ukg.ph_third_quarter_clock_hours where employee_ = '128530'

-- -- this is what was submitted
-- 07/13/21 why does croaker have 1 check, should be 3  7/2 7/9 & 7/15
-- had to do a left join on the clock hours
select a.*, b."REG", b."Overtime", b."PTO", b."Holiday"
from ukg.ph_third_quarter_paychecks a
left join ukg.ph_third_quarter_clock_hours b on a."Check Date" = b.check_date
  and a.user_id = b.employee_
order by employee_name, "Check Date";

i expect that i will need to manually adjust sorum on the spreadsheet
and rename columns for emplr cont (get rid of the _ec)
for the second quarter, also added added columns for the comp types no longer used (so this spreadsheet is structurally the same as 1st quarter)