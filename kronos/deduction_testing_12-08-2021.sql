﻿-- from spreadsheet (deduction_testing_12-08-2021.xlsx) from kim ukg deductions to compare to arkona


drop table if exists jon.ukg_ded_1 cascade;
create table jon.ukg_ded_1 (
  first_name citext,
  last_name citext,
  deduction_code citext,
  deduction_name citext,
  begin_date date,
  end_date date,  
  ee_percent citext,
  ee_amount citext);



select distinct first_name, last_name, deduction_code, deduction_name, begin_date, end_date, ee_percent, ee_amount
from jon.ukg_ded_1
order by last_name 

drop table if exists jon.arkona_deductions;
create table jon.arkona_deductions as
select aa.store, aa.employee_last_name, employee_first_name,
  case 
		when bb.ded_pay_code = '111' then 'Dental Pre-Tax'
		when bb.ded_pay_code = '295' then 'Vision Pre-Tax'
		when bb.ded_pay_code = '107' then 'Medical Pre-Tax'
		when bb.ded_pay_code = '96' then 'HSA Pre Tax' -- Individual'
		when bb.ded_pay_code = '115' then 'FSA Dependent Care'
		when bb.ded_pay_code = '119' then 'FSA Medical'
		when bb.ded_pay_code in ('91','91b','91c') then '401K'
		when bb.ded_pay_code in ('99','99a','99b','99c') then 'Roth 401k'
		when bb.ded_pay_code = '121' then 'Long Term Disability'
		when bb.ded_pay_code = '120' then 'Short Term Disability'
		when bb.ded_pay_code = '122' then 'Employee Voluntary Life'
		when bb.ded_pay_code = '123' then 'Accident'
		when bb.ded_pay_code = '124' then 'Hospital'
		when bb.ded_pay_code = '125' then 'Critical Illness'
		when bb.ded_pay_code = '103' then 'Rydell Cares'
		when bb.ded_pay_code = '305' then 'Flex Fee'
		when bb.ded_pay_code = '227' then 'Uniforms'
		when bb.ded_pay_code = '275' then 'Child Support'
		else 'XXXXXXXXXXXXXXXXXXX'
  end as "Deduction Name", 
  aa.hire_date as "Start Date",
--   case 
-- 		when bb.ded_pay_code = '111' then 4
-- 		when bb.ded_pay_code = '295' then 5
-- 		when bb.ded_pay_code = '107' then 3
-- 		when bb.ded_pay_code = '96' then 7
-- 		when bb.ded_pay_code = '115' then 8
-- 		when bb.ded_pay_code = '119' then 9
-- 		when bb.ded_pay_code in ('91','91b','91c') then 1
-- 		when bb.ded_pay_code in ('99','99a','99b','99c') then 2
-- 		when bb.ded_pay_code = '121' then 10
-- 		when bb.ded_pay_code = '120' then 11
-- 		when bb.ded_pay_code = '122' then 12
-- 		when bb.ded_pay_code = '123' then 13
-- 		when bb.ded_pay_code = '124' then 14
-- 		when bb.ded_pay_code = '125' then 15
-- 		when bb.ded_pay_code = '103' then 18
-- 		when bb.ded_pay_code = '305' then 17
-- 		when bb.ded_pay_code = '227' then 16
--   end as "PrioritySequence",   

  case when bb.fixed_ded_pct = 'Y' then bb.fixed_Ded_amt end as "EEAmountPercent",
	case when bb.fixed_ded_pct is null then bb.fixed_ded_amt end as "EEAmountDollars"

from (
	select a.pymast_company_number as store, a.pymast_employee_number as "Employee ID", b.ssn, a.employee_last_name, a.employee_first_name,
		arkona.db2_integer_to_date(a.hire_date) as hire_date, a.pay_period, c.dep_acct_type
	from arkona.ext_pymast a
	left join jon.ssn b on a.pymast_employee_number = b.employee_number
	left join arkona.ext_pypeddep c on a.pymast_employee_number = c.employee_number
	where a.pymast_company_number in ('RY1', 'RY2')
		and a.active_code <> 'T') aa
join (		
	select a.employee_number, a.ded_pay_code, a.fixed_ded_pct, a.fixed_ded_amt, a.ded_freq, b.description, b.account_number
	from arkona.ext_pydeduct a
	join arkona.ext_pypcodes b on a.company_number = b.company_number
		and a.ded_pay_code = b.ded_pay_code
	where a.ded_pay_code in ('103','107','111','115','119','120','121','122','123','124','125','227',
			'126','299','295','305','91','91B','91C','99','99A','99B','99C','96','275')) bb on aa."Employee ID" = bb.employee_number
left join jon.ssn cc on aa."Employee ID" = cc.employee_number	
left join jon.direct_deposit dd on aa."Employee ID" = dd.employee_number;


select * from jon.arkona_deductions

--   case when coalesce(a."EEAmountPercent", 0) = coalesce(b.ee_percent, 0) then 'TRUE' else 'FALSE' end as percent,
--   case when coalesce(a."EEAmountDollars", 0) = coalesce(b.ee_amount, 0) then 'TRUE' else 'FALSE' end as amount
arkona is on the left  ukg on the right

-- this is the version that i sent to kim on 12/10
select * 
from (
	select c.*,
		case when coalesce(ee_perc, 0) = coalesce(ee_percent, 0) then 'YES' else 'NO' end as percent,
		case when coalesce(ee_dollars, 0) = coalesce(ee_amount, 0) then 'YES' else 'NO' end as amount
	from (
		select a.store, a.employee_last_name, employee_first_name, a."Deduction Name" as ark_deduction_name, 
			a."EEAmountPercent" as ee_perc, round(a."EEAmountDollars", 2) as ee_dollars,
			b.last_name, b.first_name, b.deduction_code, b.deduction_name, 
			nullif((replace(b.ee_percent, '%', '')), '')::numeric as ee_percent,
			nullif(replace(replace(b.ee_amount, '$', ''), ',', ''), '')::numeric as ee_amount
		from jon.arkona_deductions a
		full outer join (
			select distinct first_name, last_name, deduction_code, 
				case
					when deduction_name like 'HSA Pre Tax%' then 'HSA Pre Tax'
					else deduction_name
				end as deduction_name, 
				ee_percent, ee_amount
		from jon.ukg_ded_1
		where (length(ee_percent) + length(ee_amount) <> 0)
			and deduction_name not in ('Medical Pre-Tax  1','Vision Pre-Tax  1', 'Dental Pre-Tax  1')
			and last_name not in ('Appleby')
			and end_date > current_date) b on a.employee_last_name = b.last_name 
				and a.employee_first_name = b.first_name 
				and trim(a."Deduction Name") =  trim(b.deduction_name)) c) d
order by coalesce(d.employee_last_name, last_name), d.employee_first_name, ark_deduction_name





		select a.store, a.employee_last_name, employee_first_name, a."Deduction Name" as ark_deduction_name, 
			a."EEAmountPercent" as ee_perc, round(a."EEAmountDollars", 2) as ee_dollars
		from jon.arkona_deductions a
		where employee_last_name = 'robles'
  