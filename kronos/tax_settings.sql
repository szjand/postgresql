﻿/*
05/13/21

*/

select * from arkona.ext_pymast where employee_last_name in ( 'sorum', 'cochran')
select federal_exemptions, count(*) from arkona.ext_pymast group by federal_exemptions
select * from jon.ssn limit 10
insert into jon.ssn values
('235478','MILES, ALLYSON','003-78-5554'),
('135482','NELSON, QUINN','501-31-5561'),
('172581','CHRISTIANSEN, KARISSA','475-37-3006');


select 'Auto Generate' as "Username", a.pymast_employee_number as "Employee ID", 
	b.ssn as "SSN", a.employee_name as "Employee Name",
  case a.pymast_company_number
		when 'RY1' then '45-0236743'
		when 'RY2' then '45-0415948'
  end as "EIN Tax Id",
  case a.pymast_company_number
		when 'RY1' then 'Rydell GM'
		when 'RY2' then 'Honda Nissan'
  end as "EIN Name",
	arkona.db2_integer_to_date(a.hire_date) as "Start Date",
	a.federal_filing_status as "Federal Filing Status",
	null as "2020 and forward Form W-4", 'Yes' as "Federal Withhold",
	null as "Federal Multiple Jobs", null as "Federal Claim Dependents",
	null as "Federal Other Income", null as "Federal Deductions",
	null as "Federal Allowances", null as "Federal Lock In Letter",
	null as "Federal Lock In Limit", '$' as "Federal Additional Withholding Type",
	a.federal_exemptions as "Federal Additional Withholding",
	'No' as "Federal Rounding", state_tax_tab_code as "State",
	'Yes' as "State Withhold",
	case a.state_code_unemployment
		when 'AZ' then 'Arizona'
		when 'ND' then 'North Dakota'
		when 'OR' then 'Oregon'
		else 'XXXXXXXXXXXXXXXXXXXXXXXXX'
  end as "Unemployment State",
	'$' as "State Additional Whitholding Type",
	a.state_exemptions as "State Additional Withholding"
	
from arkona.ext_pymast a
left join jon.ssn b on a.pymast_employee_number = b.employee_number
  and a.pymast_company_number in ('RY1','RY2')
where a.active_code <> 'T'
  and a.pymast_company_number in ('RY1','RY2')  
order by "EIN Tax Id", "Employee Name";