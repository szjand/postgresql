﻿/*
05/13/21
from kim
Hi Jon  can I get a report for Kronos for the Draw checks  so I would need :
Same as most of the other ones
Id number,  name,  ss , ein tax id, ein name,   start date   draw amount
*/



select 'Auto Generate' as "Username", a.pymast_employee_number as "Employee ID", 
	b.ssn as "SSN", a.employee_name as "Employee Name",
  case a.pymast_company_number
		when 'RY1' then '45-0236743'
		when 'RY2' then '45-0415948'
  end as "EIN Tax Id",
  case a.pymast_company_number
		when 'RY1' then 'Rydell GM'
		when 'RY2' then 'Honda Nissan'
  end as "EIN Name",
	arkona.db2_integer_to_date(a.hire_date) as "Start Date",
	 c.amount as "Draw Amount"
from arkona.ext_pymast a
left join jon.ssn b on a.pymast_employee_number = b.employee_number
join (
  select employee_number, amount
  from arkona.ext_pyhscdta
  where code_id = '78'
    and payroll_cen_year = 121
  group by employee_number, amount) c on a.pymast_employee_number = c.employee_number
where active_code <> 'T' -- and employee_number in ('16425','1118722','13725','123100','128530')
  and a.pymast_company_number in ('RY1','RY2')
order by "EIN Tax Id", "Employee Name";


