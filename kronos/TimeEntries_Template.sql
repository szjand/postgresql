﻿/*
We were just going to do the last pay period 5/25/21  to 6/5/21 and would need all punches that fall between this date.  
I don’t believe we can do anything with cost center at this time  
Check date is 6/11/21.  

6/23/21
call w/ hartzog
wrong ein names
pay date = clock date
add a time off column with indicator pto, holiday, etc

*/
select * 
from arkona.xfm_pypclockin 
where the_date between '05/25/2021' and '06/05/2021'


-- from Function: arkona.xfm_pypclockin()

select * from arkona.ext_pypclockin_tmp limit 10

select * from arkona.xfm_pypclockin_tmp

6/23/ version 3
Sorry for making this a pain but if we add the lunch punches we have to have the punch added at the time that 
the lunch was taken in order for the system to recognize it correctly. The other option is to only add the 
in punch for the day and out punch for the day and the total hours for the day. I added an example below of 
what the system is doing to the punches. Also you don’t need punches for the PTO and Holiday lines. You can 
just add the Total hours for those.

in other words, their fucking system cant handle the negative hour punches
so i either have to fabricate a punch out and punch in for lunch time

spoke with jeri she said go ahead and exclude the negative hours

also noticed that i had the wrong start date for the pay period, should be 5/23 not 5/25

select f.employee_name as "Employee Name", "Employee ID", "EIN Tax Id", "EIN Name", "Pay Date",
  "In Date", 
  case 
		when yicode in ('VAC','PTO','HOL') then null
		
		else "Time In"
	end as "Time In", 
  case 
		when yicode in ('VAC','PTO','HOL') then null
		else "Time Out"
	end as "Time Out", 
	clock_hours + hol_hours + pto_hours + vac_hours as "Total Time", 
	case
		when yicode in ('VAC','PTO') then 'PTO'
		when yicode = 'HOL' then 'Holiday'
-- 		when yicode = '666' then 'Auto'
		else null
  end as "Time Off"
from (
	select pymast_employee_number as "Employee ID",
			case yico_
				when 'RY1' then '45-0236743' 
				when 'RY2' then '45-0415948'
			end as "EIN Tax Id", 
			case yico_
				when 'RY1' then 'Rydell Auto Center Inc.' 
				when 'RY2' then 'Honda Nissan of Grand Forks'
			end as "EIN Name", yiclkind as "Pay Date", yiclkind as "In Date", yiclkint as "Time In", yiclkoutt as "Time Out", 
			coalesce(
				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
					/3600.0)::numeric(6,2)) filter (where yicode in ('666','O')), 0) as clock_hours,    
			coalesce(
				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
					/3600.0)::numeric(6,2)) filter (where yicode = 'HOL'), 0) as hol_hours, 
			coalesce(
				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
					/3600.0)::numeric(6,2)) filter (where yicode = 'PTO'), 0) as pto_hours, 
			coalesce(
				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
					/3600.0)::numeric(6,2)) filter (where yicode = 'VAC'), 0) as vac_hours,
			yicode
	from ( -- d: clock hours
		select a.yico_, a.pymast_employee_number, a.yiclkind, a.yiclkint, a.yiclkoutd, a.yiclkoutt, a.yicode
		from arkona.ext_pypclockin_tmp a
		where a.yicode in ('HOL','O','PTO','VAC') -- ('HOL','666','O','PTO','VAC')
			and a.yiclkind between '05/23/2021' and '06/05/2021') d
	group by yico_, pymast_employee_number, yiclkint, yiclkoutt, yiclkind, yiclkoutd, yicode) e
left join arkona.ext_pymast f on e."Employee ID" = f.pymast_employee_number
order by "Employee Name", "In Date", "Time In"




-- do a summary of the negative clock hours excluded from v3
-- save this in docs as negative_clock_hours_excluded_from_time_series
select f.employee_name as "Employee Name", "Employee ID", "EIN Tax Id", "EIN Name", "Pay Date",
  "In Date", 
  case 
		when yicode in ('VAC','PTO','HOL') then null
		else "Time In"
	end as "Time In", 
  case 
		when yicode in ('VAC','PTO','HOL') then null
		else "Time Out"
	end as "Time Out", 
	clock_hours + hol_hours + pto_hours + vac_hours as "Total Time", 
	case
		when yicode in ('VAC','PTO') then 'PTO'
		when yicode = 'HOL' then 'Holiday'
		when yicode = '666' then 'Auto'
		else null
  end as "Time Off"
from (
	select pymast_employee_number as "Employee ID",
			case yico_
				when 'RY1' then '45-0236743' 
				when 'RY2' then '45-0415948'
			end as "EIN Tax Id", 
			case yico_
				when 'RY1' then 'Rydell Auto Center Inc.' 
				when 'RY2' then 'Honda Nissan of Grand Forks'
			end as "EIN Name", yiclkind as "Pay Date", yiclkind as "In Date", yiclkint as "Time In", yiclkoutt as "Time Out", 
			coalesce(
				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
					/3600.0)::numeric(6,2)) filter (where yicode in ('666','O')), 0) as clock_hours,    
			coalesce(
				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
					/3600.0)::numeric(6,2)) filter (where yicode = 'HOL'), 0) as hol_hours, 
			coalesce(
				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
					/3600.0)::numeric(6,2)) filter (where yicode = 'PTO'), 0) as pto_hours, 
			coalesce(
				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
					/3600.0)::numeric(6,2)) filter (where yicode = 'VAC'), 0) as vac_hours,
			yicode
	from ( -- d: clock hours
		select a.yico_, a.pymast_employee_number, a.yiclkind, a.yiclkint, a.yiclkoutd, a.yiclkoutt, a.yicode
		from arkona.ext_pypclockin_tmp a
		where a.yicode in ('666') -- ('HOL','666','O','PTO','VAC')
			and a.yiclkind between '05/23/2021' and '06/05/2021') d
	group by yico_, pymast_employee_number, yiclkint, yiclkoutt, yiclkind, yiclkoutd, yicode) e
left join arkona.ext_pymast f on e."Employee ID" = f.pymast_employee_number
order by "Employee Name", "In Date", "Time In"
 




-- -- 06/23/21 sent as rydell_time_series_v2
-- 
-- select f.employee_name as "Employee Name", "Employee ID", "EIN Tax Id", "EIN Name", "Pay Date",
--   "In Date", "Time In", "Time Out", clock_hours + hol_hours + pto_hours + vac_hours as "Total Time", 
-- 	case
-- 		when yicode in ('VAC','PTO') then 'PTO'
-- 		when yicode = 'HOL' then 'Holiday'
-- 		when yicode = '666' then 'Auto'
-- 		else null
--   end as "Time Off"
-- from (
-- 	select pymast_employee_number as "Employee ID",
-- 			case yico_
-- 				when 'RY1' then '45-0236743' 
-- 				when 'RY2' then '45-0415948'
-- 			end as "EIN Tax Id", 
-- 			case yico_
-- 				when 'RY1' then 'Rydell Auto Center Inc.' 
-- 				when 'RY2' then 'Honda Nissan of Grand Forks'
-- 			end as "EIN Name", yiclkind as "Pay Date", yiclkind as "In Date", yiclkint as "Time In", yiclkoutt as "Time Out", 
-- 			coalesce(
-- 				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
-- 					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
-- 					/3600.0)::numeric(6,2)) filter (where yicode in ('666','O')), 0) as clock_hours,    
-- 			coalesce(
-- 				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
-- 					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
-- 					/3600.0)::numeric(6,2)) filter (where yicode = 'HOL'), 0) as hol_hours, 
-- 			coalesce(
-- 				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
-- 					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
-- 					/3600.0)::numeric(6,2)) filter (where yicode = 'PTO'), 0) as pto_hours, 
-- 			coalesce(
-- 				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
-- 					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
-- 					/3600.0)::numeric(6,2)) filter (where yicode = 'VAC'), 0) as vac_hours,
-- 			yicode
-- 	from ( -- d: clock hours
-- 		select a.yico_, a.pymast_employee_number, a.yiclkind, a.yiclkint, a.yiclkoutd, a.yiclkoutt, a.yicode
-- 		from arkona.ext_pypclockin_tmp a
-- 		where a.yicode in ('HOL','666','O','PTO','VAC')
-- 			and a.yiclkind between '05/25/2021' and '06/05/2021') d
-- 	-- left join arkona.ext_pymast e on d.pymast_employee_number = e		
-- 	-- where pymast_employee_number = '197642'      
-- 	group by yico_, pymast_employee_number, yiclkint, yiclkoutt, yiclkind, yiclkoutd, yicode) e
-- left join arkona.ext_pymast f on e."Employee ID" = f.pymast_employee_number
-- order by "Employee Name", "In Date", "Time In"


-- 
-- 
-- -- sent as rydell_time_series_v1
-- select f.employee_name as "Employee Name", "Employee ID", "EIN Tax Id", "EIN Name", "Pay Date",
--   "In Date", "Time In", "Time Out", clock_hours + hol_hours + pto_hours + vac_hours as "Total Time"
-- from (
-- 	select pymast_employee_number as "Employee ID",
-- 			case yico_
-- 				when 'RY1' then '45-0236743' 
-- 				when 'RY2' then '45-0415948'
-- 			end as "EIN Tax Id", 
-- 			case yico_
-- 				when 'RY1' then 'Rydell GM' 
-- 				when 'RY2' then 'Honda Nissan'
-- 			end as "EIN Name", '06/11/2021'::date as "Pay Date", yiclkind as "In Date", yiclkint as "Time In", yiclkoutt as "Time Out", 
-- 			coalesce(
-- 				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
-- 					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
-- 					/3600.0)::numeric(6,2)) filter (where yicode in ('666','O')), 0) as clock_hours,    
-- 			coalesce(
-- 				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
-- 					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
-- 					/3600.0)::numeric(6,2)) filter (where yicode = 'HOL'), 0) as hol_hours, 
-- 			coalesce(
-- 				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
-- 					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
-- 					/3600.0)::numeric(6,2)) filter (where yicode = 'PTO'), 0) as pto_hours, 
-- 			coalesce(
-- 				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
-- 					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
-- 					/3600.0)::numeric(6,2)) filter (where yicode = 'VAC'), 0) as vac_hours 
-- 	from ( -- d: clock hours
-- 		select a.yico_, a.pymast_employee_number, a.yiclkind, a.yiclkint, a.yiclkoutd, a.yiclkoutt, a.yicode
-- 		from arkona.ext_pypclockin_tmp a
-- 		where a.yicode in ('HOL','666','O','PTO','VAC')
-- 			and a.yiclkind between '05/25/2021' and '06/05/2021') d
-- 	-- left join arkona.ext_pymast e on d.pymast_employee_number = e		
-- 	-- where pymast_employee_number = '197642'      
-- 	group by yico_, pymast_employee_number, yiclkint, yiclkoutt, yiclkind, yiclkoutd) e
-- left join arkona.ext_pymast f on e."Employee ID" = f.pymast_employee_number
-- order by "Employee Name", "In Date", "Time In"
-- 
-- 
