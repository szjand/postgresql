﻿
05/04/21
after meeting with ukg payroll, the first version was wrong in several areas and missing several fields
in the first take at this, gave laura 2 spreadsheets, 1 for actdive employees and 1 for terms since 01/01/2021
tlhis time, combine those into a single spreadsheet

from the ukg status call 5/4/21
		See feedback ticket 102211905 for import template with highlight fields
		1.  Employees Import-  The following require the clients attention:
								Username  (Column C)- First IN- Last Name  *** ukg can do this with a formula
								Primary Email (Column Z) - Client Did Not Populate
								Status (Column AP)- Incorrect values entered, client has values of Full or Part. Should be Active or Terminated
								Employee Type (Column AR)- Client Did Not Populate
													Started (Column BB) - Client Did Not Populate
								Birthday (Column BC) - Client Did Not Populate
in addition there were several attributes not populated, ukg set an updated template spreadsheet 
	with reqd fields highlighted:  \docs\Employees_Import_Template_w_highlighted_fields_5.4.21.xls

	
i sent laura a list of the "new" attributes indicating those which i dont understand, she replied:

		Let’s do one spreadsheet that has all of the information in it for active and terminated employees.
		I answered what I could below in red but also have questions about the same things you do.  
		I would say let’s just pull what we can and send them that and then go from there.

and inline on specific attributes: 

		The template ukg sent is both GM and Honda, but just the currently employed.
		In the meeting I suggested combining the currently employed with the terms. Rachel was completely indifferent. 
		One spreadsheet or two, she would not express a preference.  

		As for the additional fields:
		Username: Kim suggested first initial + last name 
				– I am fine with this.  We have a few employees who are duplicates and this affects their email address as well.  
					Like for instance Brady and Brandon Johnson.  Brady Johnson’s email is bjohnson2@rydellcars.com so I would like 
					his username to be bjohnson2.  There’s only a handful of duplicates, but this is the only one I can think of right now.

		Email: I can pull from compli/dealertrack, since it sounds like a critical field, any that are missing we can have IT create one 
				– Compli is the most accurate and then I can create the email addresses for the ones missing.

		Job Last Change: I can probably come up with a reasonable value 
		Accruals: ?
		Benefit Profile: (eligible or not) where is this located in dealertrack or how is it figured? 
				- People are benefit eligible on the first day of the month following 30 days of employment so every 
					full time person in the system who was hired in March 2021 or earlier would be benefit eligible at this point.

		Holiday: ?
		Pay Calculations: not sure how to decode that
		Security: ? 
		Timesheet: sales consultant vs main shop team techs, both are paid on commission, only one punches the clock ?

		Manager 1: which is more accurate, compli or dealertrack?  
				- Compli

		I have no preference for the number of spreadsheets or what to combine with what, tell me what you want and I will do it

-- email
select * from pto.compli_users limit 100

-- active employees

select a.pymast_company_number,  'Auto Generate' as "Username", a.pymast_employee_number as "Employee Id", 
  case pymast_company_number
		when 'RY1' then '45-0236743'
		when 'RY2' then '45-0415948'
  end as "EIN Tax Id",
  case pymast_company_number
		when 'RY1' then 'Rydell GM'
		when 'RY2' then 'Honda Nissan'
  end as "EIN Name",
	employee_first_name as "First Name", employee_last_name as "Last Name", b.ssn as "Social Security",
	'No' as "Allow Change To Social Security", 
	e.email as "Primary Email", 
	case
		when a.tel_area_code = 0 and a.telephone_number = 0 then null::text
		when a.tel_area_code = 0 and a.telephone_number <> 0 then a.telephone_number::text
		else tel_area_code::Text||'-'||telephone_number::text 
	end as "Home Phone",
	 address_1||' '||coalesce(address_2, '') as "Address 1",  null as "Address 2",  null as "Address 3", 
	city_name as "City",state_code_address_ as "State", zip_code as "Zip", 
	'Active' as "Status", 
	case when active_code = 'A' then 'Full-Time' when active_code = 'P' then 'Part-Time' end as "Employee Type",
	case payroll_class
		when 'S' then 'Salary'
		when 'H' then 'Hourly'
		when 'C' then 'Commission'
	end as "Pay Type",
	arkona.db2_integer_to_date(org_hire_date) as "Hired",
	arkona.db2_integer_to_date(org_hire_date) as "Started",
	arkona.db2_integer_to_date(birth_date) as "Birthday",
	c."Terminated",
	case when c."Terminated" is null then Null else arkona.db2_integer_to_date(hire_date) end as "Re Hired",
	e.title as "Default Cost Center", null as "Job Last Change", null as "Accruals", 
	case when arkona.db2_integer_to_date(hire_date) < '03/31/2021' then 'Eligible' else 'Not Eligible' end as "Benefit Profile",
	null as "Holiday", null as "Pay Calculations", 
	case when a.pay_period = 'B' then 'Bi-Weekly' else 'Semi-Monthly' end as "Pay Period",
	null as "Pay Prep", Null as "Security", null as "Timesheet", 
	e.supervisor_id as "Manager 1",
	d.*
from arkona.ext_pymast a
left join jon.ssn b on a.pymast_employee_number = b.employee_number
left join (
	select distinct pymast_employee_number, arkona.db2_integer_to_date(termination_date) as "Terminated"
	from arkona.xfm_pymast a	
	where pymast_company_number in ('RY1','RY2')
		and termination_date <> 0
		and employee_name not in ('AAMODT, MATTHEW','BINA, BENJAMIN','HAGER, DELWYN D' -- exclude xf between stores
			'HOLLAND, NIKOLAI','LONGORIA, MICHAEL')) c on a.pymast_employee_number = c.pymast_employee_number
left join (
  select employee_name, array_agg(distinct pymast_employee_number order by pymast_employee_number)
  from arkona.xfm_pymast
  group by employee_name) d on a.employee_name = d.employee_name		
left join pto.compli_users e on a.pymast_employee_number = e.user_id  
where active_code <> 'T'
  and pymast_company_number in ('RY2') --and a.pymast_employee_number = '135987'  
-- jacob siedschlag : delete 1 row from spreadsheet


-- terminated since 01/01/2021
select a.pymast_company_number,  'Auto Generate' as "Username", a.pymast_employee_number as "Employee Id", 
  case pymast_company_number
		when 'RY1' then '45-0236743'
		when 'RY2' then '45-0415948'
  end as "EIN Tax Id",
  case pymast_company_number
		when 'RY1' then 'Rydell GM'
		when 'RY2' then 'Honda Nissan'
  end as "EIN Name",
	employee_first_name as "First Name", employee_last_name as "Last Name", b.ssn as "Social Security",
	'No' as "Allow Change To Social Security", 
	e.email as "Primary Email", 
	case
		when a.tel_area_code = 0 and a.telephone_number = 0 then null::text
		when a.tel_area_code = 0 and a.telephone_number <> 0 then a.telephone_number::text
		else tel_area_code::Text||'-'||telephone_number::text 
	end as "Home Phone",
	 address_1||' '||coalesce(address_2, '') as "Address 1",  null as "Address 2",  null as "Address 3", 
	city_name as "City",state_code_address_ as "State", zip_code as "Zip", 
	'Terminated' as "Status", 
	case when active_code = 'A' then 'Full-Time' when active_code = 'P' then 'Part-Time' end as "Employee Type",
	case payroll_class
		when 'S' then 'Salary'
		when 'H' then 'Hourly'
		when 'C' then 'Commission'
	end as "Pay Type",
	arkona.db2_integer_to_date(org_hire_date) as "Hired",
	arkona.db2_integer_to_date(org_hire_date) as "Started",
	arkona.db2_integer_to_date(birth_date) as "Birthday",
	c."Terminated",
	case when c."Terminated" is null then Null else arkona.db2_integer_to_date(hire_date) end as "Re Hired",
	e.title as "Default Cost Center", null as "Job Last Change", null as "Accruals", 
	case when arkona.db2_integer_to_date(hire_date) < '03/31/2021' then 'Eligible' else 'Not Eligible' end as "Benefit Profile",
	null as "Holiday", null as "Pay Calculations", 
	case when a.pay_period = 'B' then 'Bi-Weekly' else 'Semi-Monthly' end as "Pay Period",
	null as "Pay Prep", Null as "Security", null as "Timesheet", 
	e.supervisor_id as "Manager 1",
	d.*
from arkona.ext_pymast a
left join jon.ssn b on a.pymast_employee_number = b.employee_number
left join (
	select distinct pymast_employee_number, arkona.db2_integer_to_date(termination_date) as "Terminated"
	from arkona.xfm_pymast a	
	where pymast_company_number in ('RY1','RY2')
		and termination_date <> 0
		and employee_name not in ('AAMODT, MATTHEW','BINA, BENJAMIN','HAGER, DELWYN D', -- exclude xf between stores
			'HOLLAND, NIKOLAI','LONGORIA, MICHAEL')) c on a.pymast_employee_number = c.pymast_employee_number
left join (
  select employee_name, array_agg(distinct pymast_employee_number order by pymast_employee_number)
  from arkona.xfm_pymast
  group by employee_name) d on a.employee_name = d.employee_name		
left join pto.compli_users e on a.pymast_employee_number = e.user_id    
where c."Terminated" between '01/01/2021' and current_date -- active_code <> 'T'
  and pymast_company_number in ('RY2')  --and a.pymast_employee_number = '135987'
  and a.employee_last_name not in ('HOLLAND', 'KIEFER')
order by  pymast_company_number, "Last Name"


-- 05/04/2021 3 new employees
insert into jon.ssn (employee_number,full_name,ssn)values
('135768','ALBAUGH, MICKEY','502-82-5831'),
('147658','HARRIS, ANTHONY','357-94-7286'),
('120318','TIERNEY, GABRIELLE','647-60-8541');