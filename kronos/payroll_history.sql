﻿
-- v1 5/27/21 payroll_history_v1, jan 8th check for ry1
-- 05/28 pyhshdta does not show other reg & ot hours (maybe)
-- group the whole thing, greg sorum, nicholas bach, on just this check have multiple checks for the date
-- was using the wrong pyhshdta field for withholding state, should be state_tax_tab_code
drop table if exists testing;
create temp table testing as
select  a.company_number,a.check_month, a.check_day, a.payroll_run_number, 
	a.employee_name, a.employee_ as user_id, sum(a.total_gross_pay) as total_gross, sum(a.curr_federal_tax) as federal_tax, sum(a.curr_fica) as fica, 
	sum(a.curr_employee_medicare) as medicare, state_tax_tab_code as withholding_state,
	sum(curr_state_tax) as state_tax, sum(current_net) as net_pay,
	sum(b.amount) as "401k",
	sum(c.amount) as "Roth 401k",
	sum(d.amount) as "Medical Pre-Tax",
	sum(e.amount) as "Dental Pre-Tax",
	sum(f.amount) as "Vision Pre-Tax",
	sum(g.amount) as "HSA Pre-Tax Individual",
	sum(h.amount) as "FSA Dependent Care",
	sum(i.amount) as "FSA Medical",
	sum(j.amount) as "Long Term Disability",
	sum(k.amount) as "Short Term Disability",
	sum(l.amount) as "Employee Voluntary Life",
	sum(m.amount) as "Accident",
	sum(n.amount) as "Hospital",
	sum(o.amount) as "Critical Illness",
	sum(p.amount) as "Uniforms",
	sum(q.amount) as "Flex Fee",
	sum(r.amount) as "Rydell Cares",
	sum(s.amount) as "Whole Life",
	sum(vacation_taken) as vac_hours, sum(holiday_taken) as hol_hours, sum(sick_leave_taken) as pto_hours, sum(reg_hours) as reg_hours, sum(overtime_hours) as ot_hours
from arkona.ext_pyhshdta a
join arkona.ext_pyptbdta aa on a.company_number = aa.company_number
  and a.payroll_run_number = aa.payroll_run_number
  and aa.description like '1B%'  -- nicholas bach
left join (
  select company_number, payroll_run_number, employee_number, sum(amount) as amount
  from arkona.ext_pyhscdta
  where code_id in ('91','91b','91c')
    and code_type = '2'
  group by company_number, payroll_run_number, employee_number) b on a.company_number = b.company_number
  and a.payroll_run_number = b.payroll_run_number
  and a.employee_ = b.employee_number
left join (
  select company_number, payroll_run_number, employee_number, sum(amount) as amount
  from arkona.ext_pyhscdta
  where code_id in ('99','99a','99b','99c')
    and code_type = '2'
  group by company_number, payroll_run_number, employee_number) c on a.company_number = c.company_number
  and a.payroll_run_number = c.payroll_run_number
  and a.employee_ = c.employee_number
left join (
  select company_number, payroll_run_number, employee_number, sum(amount) as amount
  from arkona.ext_pyhscdta
  where code_id = '107'
    and code_type = '2'
  group by company_number, payroll_run_number, employee_number) d on a.company_number = d.company_number
  and a.payroll_run_number = d.payroll_run_number
  and a.employee_ = d.employee_number
left join (
  select company_number, payroll_run_number, employee_number, sum(amount) as amount
  from arkona.ext_pyhscdta
  where code_id = '111'
    and code_type = '2'
  group by company_number, payroll_run_number, employee_number) e on a.company_number = e.company_number
  and a.payroll_run_number = e.payroll_run_number
  and a.employee_ = e.employee_number
left join (
  select company_number, payroll_run_number, employee_number, sum(amount) as amount
  from arkona.ext_pyhscdta
  where code_id = '295'
    and code_type = '2'
  group by company_number, payroll_run_number, employee_number) f on a.company_number = f.company_number
  and a.payroll_run_number = f.payroll_run_number
  and a.employee_ = f.employee_number
left join arkona.ext_pyhscdta g on a.company_number = g.company_number
  and a.payroll_run_number = g.payroll_run_number
  and a.employee_ = g.employee_number
  and g.code_id = '96'
left join (
  select company_number, payroll_run_number, employee_number, sum(amount) as amount
  from arkona.ext_pyhscdta
  where code_id = '115'
    and code_type = '2'
  group by company_number, payroll_run_number, employee_number) h on a.company_number = h.company_number
  and a.payroll_run_number = h.payroll_run_number
  and a.employee_ = h.employee_number
left join (
  select company_number, payroll_run_number, employee_number, sum(amount) as amount
  from arkona.ext_pyhscdta
  where code_id = '119'
    and code_type = '2'
  group by company_number, payroll_run_number, employee_number) i on a.company_number = i.company_number
  and a.payroll_run_number = i.payroll_run_number
  and a.employee_ = i.employee_number
left join (
  select company_number, payroll_run_number, employee_number, sum(amount) as amount
  from arkona.ext_pyhscdta
  where code_id = '121'
    and code_type = '2'
  group by company_number, payroll_run_number, employee_number) j on a.company_number = j.company_number
  and a.payroll_run_number = j.payroll_run_number
  and a.employee_ = j.employee_number
left join (
  select company_number, payroll_run_number, employee_number, sum(amount) as amount
  from arkona.ext_pyhscdta
  where code_id = '120'
    and code_type = '2'
  group by company_number, payroll_run_number, employee_number) k on a.company_number = k.company_number
  and a.payroll_run_number = k.payroll_run_number
  and a.employee_ = k.employee_number
left join (
  select company_number, payroll_run_number, employee_number, sum(amount) as amount
  from arkona.ext_pyhscdta
  where code_id = '122'
    and code_type = '2'
  group by company_number, payroll_run_number, employee_number) l on a.company_number = l.company_number
  and a.payroll_run_number = l.payroll_run_number
  and a.employee_ = l.employee_number
left join (
  select company_number, payroll_run_number, employee_number, sum(amount) as amount
  from arkona.ext_pyhscdta
  where code_id = '123'
    and code_type = '2'
  group by company_number, payroll_run_number, employee_number) m on a.company_number = m.company_number
  and a.payroll_run_number = m.payroll_run_number
  and a.employee_ = m.employee_number
left join (
  select company_number, payroll_run_number, employee_number, sum(amount) as amount
  from arkona.ext_pyhscdta
  where code_id = '124'
    and code_type = '2'
  group by company_number, payroll_run_number, employee_number) n on a.company_number = n.company_number
  and a.payroll_run_number = n.payroll_run_number
  and a.employee_ = n.employee_number
left join (
  select company_number, payroll_run_number, employee_number, sum(amount) as amount
  from arkona.ext_pyhscdta
  where code_id = '125'
    and code_type = '2'
  group by company_number, payroll_run_number, employee_number) o on a.company_number = o.company_number
  and a.payroll_run_number = o.payroll_run_number
  and a.employee_ = o.employee_number
left join (
  select company_number, payroll_run_number, employee_number, sum(amount) as amount
  from arkona.ext_pyhscdta
  where code_id = '227'
    and code_type = '2'
  group by company_number, payroll_run_number, employee_number) p on a.company_number = p.company_number
  and a.payroll_run_number = p.payroll_run_number
  and a.employee_ = p.employee_number
left join (
  select company_number, payroll_run_number, employee_number, sum(amount) as amount
  from arkona.ext_pyhscdta
  where code_id = '305'
    and code_type = '2'
  group by company_number, payroll_run_number, employee_number) q on a.company_number = q.company_number
  and a.payroll_run_number = q.payroll_run_number
  and a.employee_ = q.employee_number
left join (
  select company_number, payroll_run_number, employee_number, sum(amount) as amount
  from arkona.ext_pyhscdta
  where code_id = '103'
    and code_type = '2'
  group by company_number, payroll_run_number, employee_number) r on a.company_number = r.company_number
  and a.payroll_run_number = r.payroll_run_number
  and a.employee_ = r.employee_number
left join (
  select company_number, payroll_run_number, employee_number, sum(amount) as amount
  from arkona.ext_pyhscdta
  where code_id = '126'
    and code_type = '2'
  group by company_number, payroll_run_number, employee_number) s on a.company_number = s.company_number
  and a.payroll_run_number = s.payroll_run_number
  and a.employee_ = s.employee_number 
where a.check_year = 21
  and a.check_month = 1
  and a.check_day = 8
  and a.company_number = 'RY1'
--   and a.employee_ in  ('1130426',
group by a.company_number, a.check_month, a.check_day, a.payroll_run_number, 
	a.employee_name, a.employee_, state_tax_tab_code; 



-- jeri & kim aggreed to go with timeclock hours, paycheck hours when no timeclock
-- sent to kim as payroll_history_Testing_v2.xlsx on 5/28
-- she replied with:
-- 			2 things I am wondering can we have the gross pay split by the different pay codes ( reg, o/t, lease, comm …)  
-- 			and can we have the match in a column  I know that is not part of their pay but we would want that loaded in the payroll?

select employee_name,user_id,total_gross,federal_tax,fica,medicare,withholding_state,state_tax,net_pay,
	"401k","Roth 401k","Medical Pre-Tax","Dental Pre-Tax","Vision Pre-Tax","HSA Pre-Tax Individual",
	"FSA Dependent Care","FSA Medical","Long Term Disability","Short Term Disability",
	"Employee Voluntary Life","Accident","Hospital","Critical Illness","Uniforms","Flex Fee",
	"Rydell Cares","Whole Life",
  coalesce(bb.reg_hours, aa.reg_hours) as reg_hours, 
  coalesce(bb.ot_hours, aa.ot_hours) as overtime_hours,
  coalesce(bb.vac_hours, aa.vac_hours) as vacation_hours,
  coalesce(bb.pto_hours, aa.pto_hours) as pto_hours,
  coalesce(bb.hol_hours, aa.hol_hours) as holiday_hours
from testing aa
left join (
	select a.company_number,  a.payroll_run_number, a.description, b.employee_number, 
		sum(reg_hours) as reg_hours, sum(ot_hours) as ot_hours, sum(vac_hours) as vac_hours,
		sum(pto_hours) as pto_hours, sum(hol_hours) as hol_hours
	from arkona.ext_pyptbdta a
	left join arkona.xfm_pypclockin b on the_date between arkona.db2_integer_to_date(a.payroll_start_date) and arkona.db2_integer_to_date(payroll_end_date) 
	where a.payroll_run_number = 108210 
		and a.company_number = 'ry1' 
	group by a.company_number,  a.payroll_run_number, a.description, b.employee_number) bb on aa.user_id = bb.employee_number	
order by aa.employee_name		

select a.*, b.description
from (
select company_number, code_id, code_type, count(*)  -- ry1: 55 codes, 60 code/type in 2020 & 2021
from arkona.ext_pyhscdta
where payroll_cen_year in (120,121)
group by company_number, code_id, code_type) a
left join arkona.ext_pypcodes b on a.company_number = b.company_number
  and a.code_id = b.ded_pay_code
  and a.code_type = b.code_type
order by a.code_id


select * 
from arkona.ext_pyhscdta 
where employee_number = '168573'
  and payroll_run_number = 108210

what i want to find is for emp 168573 (aamodt) on the 1st check of jan, where can i find the valud of 1021.70 which is labeled as base
for him it does not seem to exists in pyphshdta or pyhscdta, but = gross - hol - comm

find an employee with a shit load of items on the main pay page breakdows, because if the above holds to i can generate "BASE" as a derived amount

shawn anderson
select * 
from arkona.ext_pyhscdta 
where employee_number = '13725'
  and payroll_run_number = 108210
  and code_type <> '2'
there is no BASE, all the items add up, the closes similarity is the tech hours pay
select 5244.160-1.46-872.33-81.25-837.12

for me 16425, the base = 3269.24
select * 
from arkona.ext_pyhscdta 
where employee_number = '16425'
  and payroll_run_number = 108210
  and code_type <> '2'


fuck_me it is base pay in pyhshdta, shawn has 0, aamodt has 1021.7
 select * --base_pay 
from arkona.ext_pyhshdta
where employee_ = '168573'
  and payroll_run_number = 108210

the challenge now is to pivot the fucking pyhscdta 

figure out if emp match is code 2 or something else
then, can limit the pivot to none type 2 codes

-- void
select voided_flag, count(*)
from arkona.ext_pyhshdta
where check_year in (20,21)
group by voided_flag

select * from arkona.ext_pyhshdta where voided_flag is not null

select seq_void, count(*) from arkona.ext_pyhscdta group by seq_void

select employee_number, employee_name, array_agg(distinct payroll_run_number)
from arkona.ext_pyhscdta a
join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
where seq_void = '0J'
group by employee_number, employee_name

select distinct voided_flag from arkona.ext_pyhshdta where payroll_run_number = 1130151

-- 05/30/21
-- so far, code_id 1 = pay types, code_id 2 = deductions
kim also mentioned wanted emplr contributions

select * 
from arkona.ext_pypcodes
where company_number in ('RY1','RY2')
order by ded_pay_code

-- codes not in pypcodes: CON, FED, RTM, STX
select distinct code_id
from arkona.ext_pyhscdta a
where not exists (
  select 1
  from arkona.ext_pypcodes
  where ded_pay_code = a.code_id)

select * from arkona.ext_pyhscdta where code_id in ('107') and trim(description) not like '%MED%'

drop table if exists jon.payroll_code_labels cascade;
create table jon.payroll_code_labels (
  code_id citext primary key,
  code_type citext,
  the_label citext not null,
  sequence integer not null);

insert into jon.payroll_code_labels values
('111','2','Dental Pre-Tax',4),
('295','2','Vision Pre-Tax',5),
('107','2','Medical Pre-Tax',3),
('96','2','HSA Pre-Tax Individual',7),
('115','2','FSA Dependent Care',8),
('119','2','FSA Medical',9),
('91','2','401k',1),
('91B','2','401k',1),
('91C','2','401k',1),
('99','2','Roth 401k',2),
('99A','2','Roth 401k',2),
('99B','2','Roth 401k',2),
('99C','2','Roth 401k',2),
('121','2','Long Term Disability',10),
('120','2','Short Term Disability',11),
('122','2','Employee Voluntary Life',12),
('123','2','Accident',13),
('124','2','Hospital',14),
('125','2','Critical Illness',15),
('103','2','Rydell Cares',18),
('305','2','Flex Fee',17),
('227','2','Uniforms',16),
('126','2','Whole Life',19);

-- aha, pyhscdta has a description attribute as well
-- very few where the pypcodes.description <> pyhpcdta.description, essentially one offs, ignore, use coalese to
-- get theose without descriptions in pypcodes
select a.*, b.description
from (
select company_number, code_id, code_type, count(*)  -- ry1: 55 codes, 60 code/type in 2020 & 2021
from arkona.ext_pyhscdta
where payroll_cen_year in (120,121)
group by company_number, code_id, code_type) a
left join arkona.ext_pypcodes b on a.company_number = b.company_number
  and a.code_id = b.ded_pay_code
  and a.code_type = b.code_type
where b.ded_pay_code is null  
order by a.code_id


drop table if exists jon.payroll_codes cascade;
create table jon.payroll_codes (
	store citext not null,
	code_id citext not null,
	code_type citext not null,
	description citext not null,
	primary key (store,code_id, code_type));
insert into jon.payroll_codes	
select a.company_number, a.code_id, a.code_type, 
	coalesce(c.the_label, b.description, 
		case 
			when a.code_id = 'CON' then 'EMPLR CONTR' 
			when a.code_id = '91b' then 'ER CONTR- 91B'  
			when a.code_type = '4' and a.code_id = 'CON' then 'EMPLR CONTR' 
			when a.code_type = '4' and a.code_id = 'FED' then 'FEDERAL' 
			when a.code_type = '4' and a.code_id = 'RTM' then 'RETIRE EMPLR' 
			when a.code_type = '4' and a.code_id = 'STX' then 'STATE TAX'
			when a.code_type = '0' then a.description
			else a.description 
		end) as description
from (
	select company_number, code_id, code_type, description, count(*)  -- ry1: 55 codes, 60 code/type in 2020 & 2021
	from arkona.ext_pyhscdta
	where payroll_cen_year in (120,121)
	--   and code_id in ('CON', 'FED', 'RTM', 'STX')
	group by company_number, code_id, code_type, description) a
left join arkona.ext_pypcodes b on a.company_number = b.company_number
  and a.code_id = b.ded_pay_code
  and a.code_type = b.code_type
left join jon.payroll_code_labels c on a.code_id = c.code_id  
  and a.code_type = c.code_type
group by a.company_number, a.code_id, a.code_type, 
  coalesce(c.the_label, b.description, 
		case 
			when a.code_id = 'CON' then 'EMPLR CONTR' 
		  when a.code_id = '91b' then 'ER CONTR- 91B'  
		 when a.code_type = '4' and a.code_id = 'CON' then 'EMPLR CONTR' 
		 when a.code_type = '4' and a.code_id = 'FED' then 'FEDERAL' 
		 when a.code_type = '4' and a.code_id = 'RTM' then 'RETIRE EMPLR' 
		 when a.code_type = '4' and a.code_id = 'STX' then 'STATE TAX'
		 when a.code_type = '0' then a.description
		 else a.description 
	  end) 
order by a.code_id;



select * from jon.payroll_codes where code_id = '91b'

select * from arkona.ext_pypcodes where ded_pay_code = '91b'

select distinct company_number, code_type, code_seq_, code_id, description from arkona.ext_pyhscdta where code_id in ('91b') and code_type = '5' and payroll_cen_year in (120,121) order by code_id

select * from arkona.ext_pymast where pymast_employee_number = '1117842'

select distinct code_type from arkona.ext_pypcodes  -- only 1, 2
select distinct code_type from arkona.ext_pyhscdta  -- 0,1,2,4,5
	0: hol/ot/vac pay
	4: mostly emp contrib, some fed (FED), some state tax
	5: emp contrib

select company_number, employee_, employee_name, payroll_run_number, payroll_ending_year, payroll_ending_month, payroll_ending_day, 
	base_pay, total_gross_pay, total_adj_gross, curr_federal_tax, curr_fica, curr_emplr_fica, curr_employee_medicare, curr_employer_medicare,
	curr_state_tax, emplr_curr_ret, total_other_pay, current_net, base_salary, base_hrly_rate
from arkona.ext_pyhshdta a
where a.check_year = 21
  and a.check_month = 1
  and a.check_day = 8
  and a.company_number = 'RY1'
order by employee_name  
-- ok, labels are ok
-- now about the non 1, 2 code_types

select c.employee_name, a.employee_number, a.code_type, a.code_id, b.description, sum(amount)
-- select a.*
from arkona.ext_pyhscdta a
join jon.payroll_codes b on a.code_type = b.code_type
  and a.code_id = b.code_id
  and a.company_number = b.store
join arkona.ext_pyhshdta c on a.company_number = c.company_number
  and a.payroll_run_number = c.payroll_run_number
  and a.employee_number = c.employee_
  and c.check_year = 21
  and c.check_month = 1
  and c.check_day = 8
where a.company_number = 'RY1'
  and a.employee_number = '168573'
  and a.payroll_run_number = 108210
group by c.employee_name, a.employee_number, a.code_type, a.code_id, b.description 

select * from arkona.ext_pyhscdta a
where payroll_run_number = 108210
  and employee_number = '168573'



separate columns to employee comp/ded/taxes  employer

ok, lets try colpivot
-- this works
drop table if exists _test_payroll_jan_2021;
select jon.colpivot (
	'_test_payroll_jan_2021',
	$$
		select c.employee_name, a.employee_number, a.code_type, a.code_id, b.description, sum(amount) as amount
		-- select a.*
		from arkona.ext_pyhscdta a
		join jon.payroll_codes b on a.code_type = b.code_type
			and a.code_id = b.code_id
			and a.company_number = b.store
		join arkona.ext_pyhshdta c on a.company_number = c.company_number
			and a.payroll_run_number = c.payroll_run_number
			and a.employee_number = c.employee_
			and c.check_year = 21
			and c.check_month = 1
			and c.check_day = 8
		where a.company_number = 'RY1'
		--   and a.employee_number = '1117842'
			and a.payroll_run_number = 108210
		group by c.employee_name, a.employee_number, a.code_type, a.code_id, b.description 
	$$,
	array['employee_name','employee_number'], -- data headers 
	array['description'], -- pivoted header
	'#.amount', -- data
	'description'); -- sort order of pivoted headers
select * from _test_payroll_jan_2021 order by employee_name;	

-- try to include code type in description
-- yep, the headers are now ordered by the type code_type
-- aamodt missing 20.31 emplr retirement - nope, that is from physhdta, neither pay nor deduction, employer contribution
drop table if exists _test_payroll_jan_2021;
select jon.colpivot (
	'_test_payroll_jan_2021',
	$$
	  select * 
	  from (
			select c.employee_name, a.employee_number, a.code_type ||'-'|| b.description as description, sum(amount) as amount
			-- select a.*
			from arkona.ext_pyhscdta a
			join jon.payroll_codes b on a.code_type = b.code_type
				and a.code_id = b.code_id
				and a.company_number = b.store
			join arkona.ext_pyhshdta c on a.company_number = c.company_number
				and a.payroll_run_number = c.payroll_run_number
				and a.employee_number = c.employee_
				and c.check_year = 21
				and c.check_month = 1
				and c.check_day = 8
			where a.company_number = 'RY1'
			--   and a.employee_number = '1117842'
				and a.payroll_run_number = 108210
			group by c.employee_name, a.employee_number, a.code_type, a.code_id, b.description) x 
	$$,
	array['employee_name','employee_number'],
	array['description'],
	'#.amount',
	'description');
select * from _test_payroll_jan_2021 order by employee_name;	

-- 05/31 removed pyhshdta, greg gets 2 checks each payroll_run, don't need the name in this query
drop table if exists _test_payroll_jan_2021;
select jon.colpivot (
	'_test_payroll_jan_2021',
	$$
	  select * 
	  from (
			select a.employee_number, a.code_type ||'-'|| b.description as description, sum(amount) as amount
			-- select a.*
			from arkona.ext_pyhscdta a
			join jon.payroll_codes b on a.code_type = b.code_type
				and a.code_id = b.code_id
				and a.company_number = b.store
			where a.company_number = 'RY1'
			--   and a.employee_number = '1117842'
				and a.payroll_run_number = 108210
			group by a.employee_number, a.code_type, a.code_id, b.description) x 
	$$,
	array['employee_number'],
	array['description'],
	'#.amount',
	'description');
select * from _test_payroll_jan_2021

select * from _test_payroll_jan_2021 order by employee_name;	



******************************************************


-- colpivot_4 fixed greg
-- this is good, but it requires a lot of spreadsheet formatting
select *
from ( -- need to group, multiple checks per payroll_run
	select company_number,check_month, check_day, payroll_run_number, employee_ as user_id, employee_name, state_tax_tab_code as withholding_state,
		sum(base_pay) as base_pay, sum(total_gross_pay) as total_gross_pay, sum(curr_federal_tax) as fed_tax, sum(curr_fica) as fica, 
		sum(curr_employee_medicare) as medicare, sum(curr_state_tax) as state_tax, sum(emplr_curr_ret) as emplr_ret, sum(current_net) as net
	from arkona.ext_pyhshdta 
	where payroll_run_number = 108210
		and company_number = 'RY1'
	group by company_number, check_month, check_day, payroll_run_number, employee_, employee_name, state_tax_tab_code) a
left join ( -- 118010 had 2 identical rows
	select distinct * 
	from _test_payroll_jan_2021) b on a.user_id = b.employee_number
order by a.employee_name 


-- 06/28/21 experiment with renaming columns 
-- ugh
drop table if exists test_1;
create temp table test_1 as
select *
from ( -- need to group, multiple checks per payroll_run
	select company_number,check_month, check_day, payroll_run_number, employee_ as user_id, employee_name, state_tax_tab_code as withholding_state,
		sum(base_pay) as base_pay, sum(total_gross_pay) as total_gross_pay, sum(curr_federal_tax) as fed_tax, sum(curr_fica) as fica, 
		sum(curr_employee_medicare) as medicare, sum(curr_state_tax) as state_tax, sum(emplr_curr_ret) as emplr_ret, sum(current_net) as net
	from arkona.ext_pyhshdta 
	where payroll_run_number = 108210
		and company_number = 'RY1'
	group by company_number, check_month, check_day, payroll_run_number, employee_, employee_name, state_tax_tab_code) a
left join ( -- 118010 had 2 identical rows
	select distinct * 
	from _test_payroll_jan_2021) b on a.user_id = b.employee_number
order by a.employee_name;

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_name = 'test_1'
  and column_name like '%''%'

alter table test_1
rename column "'0-HOLIDAY PAY'" to "HOLIDAY PAY";
alter table test_1
rename column "'0-OVERTIME PAY'" to "OVERTIME PAY";
alter table test_1
rename column "'0-OVERTIME PAY'" to "OVERTIME PAY";
'0-PTO PAY','0-VACATION PAY','1-COMMISSIONS','1-COVID 19','1-COVID-FMLA','1-HOLIDAY PAY','1-LEASE PROGRAM','1-PAY OUT PTO','1-RATE ADJUSTMENT','1-TECH HOURS PAY','1-TECH XTRA BONUS','1-VAC/PTO PAY','2-13 GARNISHMENT','2-15 LIFE AFT TAX','2-17 CANCER PREM','2-1  ACCTS REC','2-401k','2-Accident','2-Critical Illness','2-Dental Pre-Tax','2-Employee Voluntary Life','2-FSA Dependent Care','2-FSA Medical','2-Hospital','2-HSA Pre-Tax Individual','2-Long Term Disability','2-Medical Pre-Tax','2-Roth 401k','2-Rydell Cares','2-Short Term Disability','2-Uniforms','2-Vision Pre-Tax','2-Whole Life','5-ER CONTR- 91B','5-ER CONTR- 91C','5-ER CONTR- 99','5-ER CONTR- 99C'





















1. move ded 0 & 1 code_types to the right of base pay, 14 columns
1. code_type 2, 21 columns


need to tack on clock hours

-- replace testing with pyhshdta
select -- a.company_number,  a.payroll_run_number, a.description, 
	distinct aa.employee_, aa.employee_name, 
  coalesce(bb.reg_hours, aa.reg_hours) as "REG HOURS", 
  coalesce(bb.ot_hours, aa.overtime_hours) as "OT HOURS",
  coalesce(bb.vac_hours, aa.vacation_taken) as "VAC HOURS",
  coalesce(bb.pto_hours, aa.sick_leave_taken) as "PTO HOURS",
  coalesce(bb.hol_hours, aa.holiday_taken) as "HOL HOURS"
from ( -- fucking sorum and his multiple checks
  select employee_, employee_name, sum(reg_hours) as reg_hours,
		sum(overtime_hours) as overtime_hours, sum(vacation_taken) as vacation_taken, 
		sum(sick_leave_taken) as sick_leave_taken, sum(holiday_taken) as holiday_taken
		from arkona.ext_pyhshdta
	where payroll_run_number = 108210
		and company_number = 'RY1'	
	group by employee_, employee_name) aa	
left join (
	select a.company_number,  a.payroll_run_number, a.description, b.employee_number, 
		sum(reg_hours) as reg_hours, sum(ot_hours) as ot_hours, sum(vac_hours) as vac_hours,
		sum(pto_hours) as pto_hours, sum(hol_hours) as hol_hours
	from arkona.ext_pyptbdta a
	left join arkona.xfm_pypclockin b on the_date between arkona.db2_integer_to_date(a.payroll_start_date) and arkona.db2_integer_to_date(payroll_end_date) 
	where a.payroll_run_number = 108210 
		and a.company_number = 'ry1' 
	group by a.company_number,  a.payroll_run_number, a.description, b.employee_number) bb on aa.employee_ = bb.employee_number	
-- group by aa.employee_  
order by aa.employee_name

	
