﻿/*
1/25/22

Good afternoon Jon  I am working with  Laura on the PTO  for part-time going full-time.  
In the Vision page can you run me a report with what there hire date is and what there PTO date is?  
If that is possible we could flag the PTO dates that are different from there hire dates.  
Then I will post those in UKG under seniority. In UKG they said we can set the accruals to use 
the hire date unless there is a date in seniority.  Let me know
Kim
*/

-- don't know if ukg considers rehire date or not, leave them in
select a.last_name, a.first_name, a.employee_number, a.hire_date, a.rehire_date, a.seniority_date, b.pto_anniversary
from ukg.employees a
left join pto.employees b on a.employee_number = b.employee_number
where status = 'active'
  and a.hire_date <> b.pto_anniversary
order by a.last_name  

