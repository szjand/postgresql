﻿1. Cash Spiff - is this correct for amounts paid on a payroll check

2. When i asked kim about the Draw -> Sales Volume Pay
she replied: 'Jeri said there should be one for  Sales Volume Commission  in UKG  not Sales Volume Pay'
which i dont understand
bear in mind (from jeri): 
'Just FYI—they have a mess in the earnings codes.  '

6175637 emp# regular E  code  

richard spreadsheet fuckups: 
3.	lines 38, 39 ?
4.	line 34 17 cancer prem: cant be removed, because of use in 1st quarter  include in critical illness

06/24/21
Just to be clear, 
On the payroll history data that I will be submitting, we want to change the columns headings
From HSA Pre Tax Individual to HSAInd
From Long Term Disability to  ltd_posttax
From Medical Pre-Tax to medical_pretax
From Dental Pre-Tax to dental_pretax
From Vision Pre-Tax to vision_pretax
Is that correct?
Thanks
jon

from Jeri
Yes, that’s correct.  

in addition 
from Short Term Disability to std_posttax

So, on 6/24, to expedite matters, i am simply going to update the spreadsheet headers to 
get the spreadsheet to kim payroll_history_1st_quarter_v2.xlsx

also need to clean up some data on the spreadsheet: greg sorum, abigail sorum, steve zoellick
then, i will go back and update the queries


no need to do the combination of code type with code
this is where i need to do the mapping on deductions
also need pymast for dist code
ok, 1124436 has multiple 401 on check date 1/0/21, so, need to map deductions, then group

drop table if exists ukg.ph_first_quarter_Adds_deducts_base cascade;
create unlogged table ukg.ph_first_quarter_Adds_deducts_base as
		select dds.db2_integer_to_date(('20'||c.check_year::text || lpad(c.check_month::text, 2, '0') ||lpad(c.check_day::text, 2, '0'))::integer) as check_date,
		  a.company_number, --a.payroll_run_number,
			a.employee_number, 
			a.code_type ||'-'|| 
				case 
					when b.description like '%COMM%' and d.pay_period = 'b' then 'Biweekly Commission' 
					when b.description like '%COMM%' and d.pay_period = 's' then 'Monthly Commission' 
					when b.description = 'draws' and d.distrib_code = 'sale' then 'Sales Volume Commission' ------------------------------------------------
					when b.description = 'draws' and d.distrib_code = 'team' then 'F&I Commission' --------------------------------------------------
					when b.description = 'draws' then 'Salary' --------------------------------------------------------------------------------------
					when b.description in ('1  ACCTS REC', '2 ACCOUNTS RECI') then 'Accounts Receivable'
					when b.description = 'holiday pay' then 'Holiday'
					when b.description = 'overtime pay' then 'Overtime'
					when b.description = 'pto pay' then 'PTO'
					when b.description = 'vacation pay' then 'PTO'
					when b.description = 'a-z sales f&i' then 'F&I Commission'
					when b.description = 'covid 19' then 'FFCRA Sick Leave 10 Days'
					when b.description = 'covid-fmla' then 'FFCRA FMLA 10 Weeks'
					when b.description =  'lease program' then 'Lease Program'
					when b.description = 'pay out pto' then 'PTO Payout'
					when b.description = 'pulse pay' then 'Pulse Pay'
					when b.description in ('rate adjust','rate adjustment') then 'Hourly Rate Adjust'
					when b.description = 'salary' then 'Salary'
					when b.description = 'sales vacation' then 'PTO'
					when b.description = 'spiff pay outs' then 'Cash Spiff'
					when b.description = 'tech hours pay' then 'Flat Rate'
					when b.description = 'tech xtra bonus' then 'Flat Rate Premium Pay'
					when b.description = 'vac/pto pay' then 'PTO'
					when b.description = 'volunterr pto' then 'Volunteer PTO'
					when b.description = '13 garnishment' then 'Garnshiment'
					when b.description = '15 life aft tax' then 'Employee Voluntary Life'
					when b.description in ('Critical Illness', '17 cancer prem') then 'Critical Illness'
					when b.description in ('401k','Accident','Accounts Receivable','Dental Pre-Tax','Employee Voluntary Life',
																	'Flex Fee','FSA Dependent Care','FSA Medical','Hospital','HSA Pre-Tax Individual','Long Term Disability',
																	'Medical Pre-Tax','Rydell Cares','Short Term Disability','Uniforms','Vision Pre-Tax','Whole Life') then b.description
					when b.description = 'Roth 401k' then 'ROTH'
					when b.description in ('ER CONTR- 91B','ER CONTR- 91C') then '401k'
					when b.description in ('ER CONTR- 99','ER CONTR- 99B','ER CONTR- 99C') then 'ROTH'
					else '**************' || b.description 
				end as description, a.amount -- sum(amount) as amount
		from arkona.ext_pyhscdta a
		join jon.payroll_codes b on a.code_type = b.code_type
			and a.code_id = b.code_id
			and a.company_number = b.store
		join arkona.ext_pyhshdta c on a.company_number = c.company_number
			and a.payroll_run_number = c.payroll_run_number
			and a.employee_number = c.employee_
			and c.check_year = 21
			and c.check_month between 1 and 3
			and c.seq_void <> '0J' -- void checks
			and a.amount <> 0
-- 		where a.company_number = 'RY1' 
		left join arkona.ext_pymast d on a.employee_number = d.pymast_employee_number;
		
-- yep, 2 different code_types: 2 & 5
ukg.ph_first_quarter_Adds_deducts_base
select description, count(*) from ukg.ph_first_quarter_Adds_deducts_base group by description order by description

select distinct code_type from ukg.ph_first_quarter_Adds_deducts_base -- 1,2,5,0
1: compensation
2: deductions
5: emplr contr
0: compensation

select distinct code_type from arkona.ext_pypcodes  -- only code types 1 & 2

select distinct code_type, description from ukg.ph_first_quarter_Adds_deducts_base where code_type not in ('1','2')
select * from ukg.ph_first_quarter_Adds_deducts_base

drop table if exists ukg.ph_first_quarter_Adds_deducts cascade;
create unlogged table ukg.ph_first_quarter_Adds_deducts as
select check_date, company_number, employee_number, description, sum(amount) as amount
from ukg.ph_first_quarter_Adds_deducts_base
group by check_date, company_number, employee_number, description;
alter table ukg.ph_first_quarter_Adds_deducts
add primary key(check_date,employee_number,description);

select * from ukg.ph_first_quarter_Adds_deducts where check_date = '01/08/2021'  and employee_number = '168573'
select * from ukg.ph_first_quarter_Adds_deducts where description like '%401%' and employee_number = '1130420'



-- insert this result into a table with mapped field names
drop table if exists first_quarter_adds_deducts_pivot;
select jon.colpivot (
	'first_quarter_adds_deducts_pivot',
	$$
	  select * 
	  from ukg.ph_first_quarter_adds_deducts
	$$,
	array['company_number','check_date','employee_number'], -- non pivoted column headers
	array['description'], -- pivot headers
	'#.amount', -- data for the pivoted columns
	'description'); -- sort order (left to right) of the pivoted headers
alter table first_quarter_adds_deducts_pivot
add primary key(employee_number, check_date);

select * from first_quarter_adds_deducts_pivot where employee_number = '1130420'

-- remove the single quotes from the column names
select string_agg('alter table first_quarter_adds_deducts_pivot rename column "'||column_name||'" to "'||replace(column_name,'''','')||'"', ';' order by ordinal_position)
from information_schema.columns
where table_name = 'first_quarter_adds_deducts_pivot'
  and column_name not in ('company_number','check_date','employee_number')

alter table first_quarter_adds_deducts_pivot rename column "'0-Holiday'" to "0-Holiday";alter table first_quarter_adds_deducts_pivot rename column "'0-Overtime'" to "0-Overtime";alter table first_quarter_adds_deducts_pivot rename column "'0-PTO'" to "0-PTO";alter table first_quarter_adds_deducts_pivot rename column "'1-Biweekly Commission'" to "1-Biweekly Commission";alter table first_quarter_adds_deducts_pivot rename column "'1-Cash Spiff'" to "1-Cash Spiff";alter table first_quarter_adds_deducts_pivot rename column "'1-FFCRA FMLA 10 Weeks'" to "1-FFCRA FMLA 10 Weeks";alter table first_quarter_adds_deducts_pivot rename column "'1-FFCRA Sick Leave 10 Days'" to "1-FFCRA Sick Leave 10 Days";alter table first_quarter_adds_deducts_pivot rename column "'1-F&I Commission'" to "1-F&I Commission";alter table first_quarter_adds_deducts_pivot rename column "'1-Flat Rate'" to "1-Flat Rate";alter table first_quarter_adds_deducts_pivot rename column "'1-Flat Rate Premium Pay'" to "1-Flat Rate Premium Pay";alter table first_quarter_adds_deducts_pivot rename column "'1-Holiday'" to "1-Holiday";alter table first_quarter_adds_deducts_pivot rename column "'1-Hourly Rate Adjust'" to "1-Hourly Rate Adjust";alter table first_quarter_adds_deducts_pivot rename column "'1-Lease Program'" to "1-Lease Program";alter table first_quarter_adds_deducts_pivot rename column "'1-Monthly Commission'" to "1-Monthly Commission";alter table first_quarter_adds_deducts_pivot rename column "'1-PTO'" to "1-PTO";alter table first_quarter_adds_deducts_pivot rename column "'1-PTO Payout'" to "1-PTO Payout";alter table first_quarter_adds_deducts_pivot rename column "'1-Pulse Pay'" to "1-Pulse Pay";alter table first_quarter_adds_deducts_pivot rename column "'1-Salary'" to "1-Salary";alter table first_quarter_adds_deducts_pivot rename column "'1-Sales Volume Commission'" to "1-Sales Volume Commission";alter table first_quarter_adds_deducts_pivot rename column "'1-Volunteer PTO'" to "1-Volunteer PTO";alter table first_quarter_adds_deducts_pivot rename column "'2-401k'" to "2-401k";alter table first_quarter_adds_deducts_pivot rename column "'2-Accident'" to "2-Accident";alter table first_quarter_adds_deducts_pivot rename column "'2-Accounts Receivable'" to "2-Accounts Receivable";alter table first_quarter_adds_deducts_pivot rename column "'2-Critical Illness'" to "2-Critical Illness";alter table first_quarter_adds_deducts_pivot rename column "'2-Dental Pre-Tax'" to "2-Dental Pre-Tax";alter table first_quarter_adds_deducts_pivot rename column "'2-Employee Voluntary Life'" to "2-Employee Voluntary Life";alter table first_quarter_adds_deducts_pivot rename column "'2-Flex Fee'" to "2-Flex Fee";alter table first_quarter_adds_deducts_pivot rename column "'2-FSA Dependent Care'" to "2-FSA Dependent Care";alter table first_quarter_adds_deducts_pivot rename column "'2-FSA Medical'" to "2-FSA Medical";alter table first_quarter_adds_deducts_pivot rename column "'2-Garnshiment'" to "2-Garnshiment";alter table first_quarter_adds_deducts_pivot rename column "'2-Hospital'" to "2-Hospital";alter table first_quarter_adds_deducts_pivot rename column "'2-HSA Pre-Tax Individual'" to "2-HSA Pre-Tax Individual";alter table first_quarter_adds_deducts_pivot rename column "'2-Long Term Disability'" to "2-Long Term Disability";alter table first_quarter_adds_deducts_pivot rename column "'2-Medical Pre-Tax'" to "2-Medical Pre-Tax";alter table first_quarter_adds_deducts_pivot rename column "'2-ROTH'" to "2-ROTH";alter table first_quarter_adds_deducts_pivot rename column "'2-Rydell Cares'" to "2-Rydell Cares";alter table first_quarter_adds_deducts_pivot rename column "'2-Short Term Disability'" to "2-Short Term Disability";alter table first_quarter_adds_deducts_pivot rename column "'2-Uniforms'" to "2-Uniforms";alter table first_quarter_adds_deducts_pivot rename column "'2-Vision Pre-Tax'" to "2-Vision Pre-Tax";alter table first_quarter_adds_deducts_pivot rename column "'2-Whole Life'" to "2-Whole Life";alter table first_quarter_adds_deducts_pivot rename column "'5-401k'" to "5-401k";alter table first_quarter_adds_deducts_pivot rename column "'5-ROTH'" to "5-ROTH"

-- remove the code type from the column names
select string_agg('alter table first_quarter_adds_deducts_pivot rename column "'||column_name||'" to "'||substring(column_name,3)||'"', ';' order by ordinal_position)
from information_schema.columns
where table_name = 'first_quarter_adds_deducts_pivot'
  and column_name not in ('company_number','check_date','employee_number')

--= must rename first instance of holiday to holiday_1 and pto to pto_1
-- and last instances of 401k and roth as _ec
alter table first_quarter_adds_deducts_pivot rename column "0-Holiday" to "Holiday_1";alter table first_quarter_adds_deducts_pivot rename column "0-Overtime" to "Overtime";alter table first_quarter_adds_deducts_pivot rename column "0-PTO" to "PTO_1";alter table first_quarter_adds_deducts_pivot rename column "1-Biweekly Commission" to "Biweekly Commission";alter table first_quarter_adds_deducts_pivot rename column "1-Cash Spiff" to "Cash Spiff";alter table first_quarter_adds_deducts_pivot rename column "1-FFCRA FMLA 10 Weeks" to "FFCRA FMLA 10 Weeks";alter table first_quarter_adds_deducts_pivot rename column "1-FFCRA Sick Leave 10 Days" to "FFCRA Sick Leave 10 Days";alter table first_quarter_adds_deducts_pivot rename column "1-F&I Commission" to "F&I Commission";alter table first_quarter_adds_deducts_pivot rename column "1-Flat Rate" to "Flat Rate";alter table first_quarter_adds_deducts_pivot rename column "1-Flat Rate Premium Pay" to "Flat Rate Premium Pay";alter table first_quarter_adds_deducts_pivot rename column "1-Holiday" to "Holiday";alter table first_quarter_adds_deducts_pivot rename column "1-Hourly Rate Adjust" to "Hourly Rate Adjust";alter table first_quarter_adds_deducts_pivot rename column "1-Lease Program" to "Lease Program";alter table first_quarter_adds_deducts_pivot rename column "1-Monthly Commission" to "Monthly Commission";alter table first_quarter_adds_deducts_pivot rename column "1-PTO" to "PTO";alter table first_quarter_adds_deducts_pivot rename column "1-PTO Payout" to "PTO Payout";alter table first_quarter_adds_deducts_pivot rename column "1-Pulse Pay" to "Pulse Pay";alter table first_quarter_adds_deducts_pivot rename column "1-Salary" to "Salary";alter table first_quarter_adds_deducts_pivot rename column "1-Sales Volume Commission" to "Sales Volume Commission";alter table first_quarter_adds_deducts_pivot rename column "1-Volunteer PTO" to "Volunteer PTO";alter table first_quarter_adds_deducts_pivot rename column "2-401k" to "401k";alter table first_quarter_adds_deducts_pivot rename column "2-Accident" to "Accident";alter table first_quarter_adds_deducts_pivot rename column "2-Accounts Receivable" to "Accounts Receivable";alter table first_quarter_adds_deducts_pivot rename column "2-Critical Illness" to "Critical Illness";alter table first_quarter_adds_deducts_pivot rename column "2-Dental Pre-Tax" to "Dental Pre-Tax";alter table first_quarter_adds_deducts_pivot rename column "2-Employee Voluntary Life" to "Employee Voluntary Life";alter table first_quarter_adds_deducts_pivot rename column "2-Flex Fee" to "Flex Fee";alter table first_quarter_adds_deducts_pivot rename column "2-FSA Dependent Care" to "FSA Dependent Care";alter table first_quarter_adds_deducts_pivot rename column "2-FSA Medical" to "FSA Medical";alter table first_quarter_adds_deducts_pivot rename column "2-Garnshiment" to "Garnshiment";alter table first_quarter_adds_deducts_pivot rename column "2-Hospital" to "Hospital";alter table first_quarter_adds_deducts_pivot rename column "2-HSA Pre-Tax Individual" to "HSA Pre-Tax Individual";alter table first_quarter_adds_deducts_pivot rename column "2-Long Term Disability" to "Long Term Disability";alter table first_quarter_adds_deducts_pivot rename column "2-Medical Pre-Tax" to "Medical Pre-Tax";alter table first_quarter_adds_deducts_pivot rename column "2-ROTH" to "ROTH";alter table first_quarter_adds_deducts_pivot rename column "2-Rydell Cares" to "Rydell Cares";alter table first_quarter_adds_deducts_pivot rename column "2-Short Term Disability" to "Short Term Disability";alter table first_quarter_adds_deducts_pivot rename column "2-Uniforms" to "Uniforms";alter table first_quarter_adds_deducts_pivot rename column "2-Vision Pre-Tax" to "Vision Pre-Tax";alter table first_quarter_adds_deducts_pivot rename column "2-Whole Life" to "Whole Life";alter table first_quarter_adds_deducts_pivot rename column "5-401k" to "401k_ec";alter table first_quarter_adds_deducts_pivot rename column "5-ROTH" to "ROTH_ec"

-- list all the column names
select string_agg('"'||column_name||'"', ',' order by ordinal_position)
from information_schema.columns
where table_name = 'first_quarter_adds_deducts_pivot'

drop table if exists ukg.ph_first_quarter_adds_deducts_pivot cascade;
create unlogged table ukg.ph_first_quarter_adds_deducts_pivot as
-- combine the pto and holiday fields into one
select "company_number","check_date","employee_number",coalesce("Holiday_1",0) + coalesce("Holiday",0) as "Holiday",
	"Overtime",coalesce("PTO_1",0) + coalesce("PTO",0) as "PTO","Biweekly Commission",
	"Cash Spiff","FFCRA FMLA 10 Weeks","FFCRA Sick Leave 10 Days","F&I Commission","Flat Rate","Flat Rate Premium Pay",
	"Hourly Rate Adjust","Lease Program","Monthly Commission","PTO Payout","Pulse Pay","Salary",
	"Sales Volume Commission","Volunteer PTO","401k","Accident","Accounts Receivable","Critical Illness",
	"Dental Pre-Tax","Employee Voluntary Life","Flex Fee","FSA Dependent Care","FSA Medical","Garnshiment","Hospital",
	"HSA Pre-Tax Individual","Long Term Disability","Medical Pre-Tax","ROTH","Rydell Cares","Short Term Disability",
	"Uniforms","Vision Pre-Tax","Whole Life","401k_ec","ROTH_ec"
from first_quarter_adds_deducts_pivot;
alter table ukg.ph_first_quarter_adds_deducts_pivot
add primary key(check_date, employee_number);


select * from ukg.ph_first_quarter_adds_deducts_pivot where employee_number = '1130420'



-- -- from V1
-- drop table if exists first_quarter_all_pay;
-- create temp table first_quarter_all_pay as
-- select *
-- from ( -- need to group, multiple checks per payroll_run
-- 	select 
-- 	  dds.db2_integer_to_date(('20'||check_year::text || lpad(check_month::text, 2, '0') ||lpad(check_day::text, 2, '0'))::integer) as the_date,
-- 	  company_number as store, -- payroll_run_number as batch,
-- 		case company_number
-- 			when 'RY1' then '45-0236743' 
-- 			when 'RY2' then '45-0415948'
-- 		end as "EIN Tax Id", 
-- 		case company_number
-- 			when 'RY1' then 'Rydell Auto Center, Inc.' 
-- 			when 'RY2' then 'Honda Nissan of Grand Forks'
-- 		end as "EIN NAME", 	
-- 		employee_ as user_id, employee_name, state_tax_tab_code as withholding_state,
-- 		sum(base_pay) as base_pay, sum(total_gross_pay) as total_gross_pay, sum(curr_federal_tax) as fed_tax, sum(curr_fica) as fica, 
-- 		sum(curr_employee_medicare) as medicare, sum(curr_state_tax) as state_tax, sum(emplr_curr_ret) as emplr_ret, sum(current_net) as net
-- 	from arkona.ext_pyhshdta 
-- -- 	where payroll_run_number = 108210
--   where check_year = 21 and check_month between 1 and 3
-- -- 		and company_number = 'RY1'
-- 	group by dds.db2_integer_to_date(('20'||check_year::text || lpad(check_month::text, 2, '0') ||lpad(check_day::text, 2, '0'))::integer),
-- 	  company_number, --payroll_run_number,
-- 		case company_number
-- 			when 'RY1' then '45-0236743' 
-- 			when 'RY2' then '45-0415948'
-- 		end, 
-- 		case company_number
-- 			when 'RY1' then 'Rydell Auto Center, Inc.' 
-- 			when 'RY2' then 'Honda Nissan of Grand Forks'
-- 		end, 	
-- 		employee_, employee_name, state_tax_tab_code order by employee_name) a
-- left join ( -- 118010 had 2 identical rows
-- 	select distinct * 
-- 	from first_quarter_adds_deducts_pivot) b on a.store = b.company_number
-- 	  and a.user_id = b.employee_number 
-- 	  and a.the_date = b.check_date
-- order by a.employee_name;
-- alter table first_quarter_all_pay
-- add primary key(the_date,user_id);

-- may need to map this out the group 
-- d.distrib_code, d.payroll_class, d.pay_period
-- this needs to be grouped and summed
06/24/21 i believe jeri still has issues with earnings code
regardless, need to finish this up with mapping using richards spreadsheet and jeris pdf doc

-- cant use pymast for payroll class
drop table if exists ukg.ph_first_quarter_paychecks cascade;
create unlogged table ukg.ph_first_quarter_paychecks as
select aa.the_date as "Check Date", aa."EIN Tax Id", aa."EIN NAME", aa.user_id, aa.employee_name, 
	-- compensation
  aa."Regular", coalesce(aa."Salary",0) + coalesce(bb."Salary", 0) as "Salary", bb."Holiday",
  bb."Overtime", bb."PTO", bb."PTO Payout", bb."Monthly Commission", bb."Biweekly Commission", bb."F&I Commission", bb."Sales Volume Commission",
  bb."Lease Program", bb."Pulse Pay", bb."FFCRA Sick Leave 10 Days", bb."FFCRA FMLA 10 Weeks", bb."Hourly Rate Adjust",
  bb."Cash Spiff", bb."Flat Rate", bb."Flat Rate Premium Pay", bb."Volunteer PTO",
  --deductions
  bb."Garnshiment", bb."Accounts Receivable",
  bb."401k", bb."Accident", bb."Critical Illness",
	bb."Dental Pre-Tax", bb."Employee Voluntary Life", bb."Flex Fee", "FSA Dependent Care", bb."FSA Medical", bb."Hospital",
	bb."HSA Pre-Tax Individual", bb."Long Term Disability", bb."Medical Pre-Tax", bb."ROTH", bb."Rydell Cares", bb."Short Term Disability",
	bb."Uniforms", bb."Vision Pre-Tax", bb."Whole Life",
	-- taxes
	aa."FIT", aa."FICA", aa."MEDI", aa."SIT:AZ", aa."SIT:MN", aa."SIT:ND", aa."SIT:OR",
	-- emplr contributions
-- 	coalesce(aa."401K_ec", 0) + coalesce(bb."401k_ec", 0) as "401k_ec", bb."ROTH_ec"
	aa."401K_ec", bb."ROTH_ec"
-- select *
from (
	select 
	  dds.db2_integer_to_date(('20'||a.check_year::text || lpad(a.check_month::text, 2, '0') ||lpad(a.check_day::text, 2, '0'))::integer) as the_date,
	  a.company_number as store, -- payroll_run_number as batch,
		case a.company_number
			when 'RY1' then '45-0236743' 
			when 'RY2' then '45-0415948'
		end as "EIN Tax Id", 
		case a.company_number
			when 'RY1' then 'Rydell Auto Center, Inc.' 
			when 'RY2' then 'Honda Nissan of Grand Forks'
		end as "EIN NAME", 	
		a.employee_ as user_id, a.employee_name, 
		sum(case when a.payroll_class = 'h' then a.base_pay end) as "Regular",
		sum(case when a.payroll_class = 's' then a.base_pay end) as "Salary", 
		-- a.total_gross_pay, 
		sum(a.curr_federal_tax) as "FIT", sum(a.curr_fica) as "FICA", 
		sum(a.curr_employee_medicare) as "MEDI", 
		sum(case when a.state_tax_tab_code = 'AZ' then a.curr_state_tax end) as "SIT:AZ", 
		sum(case when a.state_tax_tab_code = 'MN' then a.curr_state_tax end) as "SIT:MN",
		sum(case when a.state_tax_tab_code = 'ND' then a.curr_state_tax end) as "SIT:ND",
		sum(case when a.state_tax_tab_code = 'OR' then a.curr_state_tax end) as "SIT:OR",
		sum(a.emplr_curr_ret) as "401K_ec"
	from arkona.ext_pyhshdta a
--   left join arkona.ext_pymast b on a.employee_ = b.pymast_employee_number
  where a.check_year = 21 and a.check_month between 1 and 3
  group by 	  dds.db2_integer_to_date(('20'||a.check_year::text || lpad(a.check_month::text, 2, '0') ||lpad(a.check_day::text, 2, '0'))::integer),
	  a.company_number,
		case a.company_number
			when 'RY1' then '45-0236743' 
			when 'RY2' then '45-0415948'
		end, 
		case a.company_number
			when 'RY1' then 'Rydell Auto Center, Inc.' 
			when 'RY2' then 'Honda Nissan of Grand Forks'
		end, 	
		a.employee_, a.employee_name) aa
left join ( -- 118010 had 2 identical rows
	select distinct "company_number","check_date","employee_number","Holiday",
	"Overtime","PTO","Biweekly Commission",
	"Cash Spiff","FFCRA FMLA 10 Weeks","FFCRA Sick Leave 10 Days","F&I Commission","Flat Rate","Flat Rate Premium Pay",
	"Hourly Rate Adjust","Lease Program","Monthly Commission","PTO Payout","Pulse Pay","Salary",
	"Sales Volume Commission","Volunteer PTO","401k","Accident","Accounts Receivable","Critical Illness",
	"Dental Pre-Tax","Employee Voluntary Life","Flex Fee","FSA Dependent Care","FSA Medical","Garnshiment","Hospital",
	"HSA Pre-Tax Individual","Long Term Disability","Medical Pre-Tax","ROTH","Rydell Cares","Short Term Disability",
	"Uniforms","Vision Pre-Tax","Whole Life","401k_ec","ROTH_ec"
	from ukg.ph_first_quarter_adds_deducts_pivot) bb on aa.store = bb.company_number
	  and aa.user_id = bb.employee_number 
	  and aa.the_date = bb.check_date
order by aa.employee_name, check_date;
alter table ukg.ph_first_quarter_paychecks
add primary key(employee_name, "Check Date");

select * from ukg.ph_first_quarter_paychecks where employee_name like 'sorum%'		

-- i believe this is adequate granularity from this table
drop table if exists ukg.ph_first_quarter_payroll_batches cascade;
create unlogged table ukg.ph_first_quarter_payroll_batches as
select distinct company_number, payroll_run_number,
	arkona.db2_integer_to_date(payroll_start_date) as from_date, arkona.db2_integer_to_date(payroll_end_date) as thru_date, 
	dds.db2_integer_to_date(check_date) as check_date
from arkona.ext_pyptbdta
where dds.db2_integer_to_date(check_date) between '01/01/2021' and '03/31/2021'
  and description not like '%void%';
alter table ukg.ph_first_quarter_payroll_batches
add primary key(company_number,payroll_run_number);
create unique index on ukg.ph_first_quarter_payroll_batches(company_number, payroll_run_number,check_date);


drop table if exists ukg.ph_first_quarter_employees cascade;
create unlogged  table ukg.ph_first_quarter_employees as
select company_number, from_date, thru_date, check_date, employee_,
	sum(reg_hours) as reg_hours,
	sum(overtime_hours) as overtime_hours, sum(vacation_taken) as vacation_taken, 
	sum(sick_leave_taken) as sick_leave_taken, sum(holiday_taken) as holiday_taken
from (-- get rid of payroll_run_number
	select a.company_number, a.payroll_run_number, a.from_date, a.thru_date, a.check_date, b.employee_,
		sum(reg_hours) as reg_hours,
		sum(overtime_hours) as overtime_hours, sum(vacation_taken) as vacation_taken, 
		sum(sick_leave_taken) as sick_leave_taken, sum(holiday_taken) as holiday_taken
	from ukg.ph_first_quarter_payroll_batches a
	left join arkona.ext_pyhshdta b on a.company_number = b.company_number
		and a.payroll_run_number = b.payroll_run_number
		and b.seq_void <> '0J'
	group by a.company_number, a.payroll_run_number, a.from_date, a.thru_date, a.check_date, b.employee_) aa
group by company_number, from_date, thru_date, check_date, employee_;
alter table ukg.ph_first_quarter_employees
add primary key(check_date, employee_);

drop table if exists ukg.ph_first_quarter_clock_hours cascade;
create unlogged table ukg.ph_first_quarter_clock_hours as
select a.company_number, a.check_date, a.employee_,
  coalesce(sum(b.reg_hours), a.reg_hours) as "REG", 
  coalesce(sum(b.ot_hours), a.overtime_hours) as "Overtime",
  coalesce(sum(b.vac_hours), a.vacation_taken) + coalesce(sum(b.pto_hours), a.sick_leave_taken) as "PTO",
  coalesce(sum(b.hol_hours), a.holiday_taken) as "Holiday"		
from ukg.ph_first_quarter_employees a
left join arkona.xfm_pypclockin b on a.employee_ = b.employee_number
  and b.the_date between a.from_date and a.thru_date
group by a.company_number, a.check_date, a.employee_;
alter table ukg.ph_first_quarter_clock_hours
add primary key(check_date,employee_);


-- this is what was submitted
select a.*, b."REG", b."Overtime", b."PTO", b."Holiday"
from ukg.ph_first_quarter_paychecks a
join ukg.ph_first_quarter_clock_hours b on a."Check Date" = b.check_date
  and a.user_id = b.employee_
order by employee_name, "Check Date"  

i expect that i will need to manually adjust sorum on the spreadsheet
and rename columns for emplr cont (get rid of the _ec)