﻿-- this is based on the work done in E:\sql\postgresql\kronos\TimeEntries_Template.sql
-- and the spreadsheet sent to ukg on 6/24/21 rydell_time_series_v3.xlsx
-- 
-- excludes automatic lunch, code 666 (negative hours, no in and out, ukg can not process that data)
-- select min(yiclkind), max(yiclkind) from arkona.ext_pypclockin limit 10
-- 
-- arkona.ext_pypclockin_tmp goes back far enough (6/20) 
-- 
-- 
-- select min(yiclkind), max(yiclkind) from arkona.ext_pypclockin_tmp limit 10
-- select * from arkona.ext_pypclockin_tmp limit 10
-- select * from dds.dim_date where the_date = '07/01/2021'  -- pay period 6/20 -> 7/3

-- asked ukg if it is ok to submit entire 3rd quarter, waiting for a response

----------------------------------------------------------------------------------------------------
-- take a snapshot of arkona.ext_pypclockin_tmp to have the data available
-- TODO: drop this table when finished
drop table if exists ukg.tmp_time_clock_data cascade;
create table ukg.tmp_time_clock_data as
select * from arkona.ext_pypclockin_tmp;
----------------------------------------------------------------------------------------------------
select * from dds.dim_date where the_date = '07/15/2021'
the pay period with a check date of 7/23 is 7/1-7/17

-- sent kim the spreadshhet 09/24
select f.employee_name as "Employee Name", "Employee ID", "EIN Tax Id", "EIN Name", "Pay Date",
  "In Date", 
  case 
		when yicode in ('VAC','PTO','HOL') then null
		
		else "Time In"
	end as "Time In", 
  case 
		when yicode in ('VAC','PTO','HOL') then null
		else "Time Out"
	end as "Time Out", 
	clock_hours + hol_hours + pto_hours + vac_hours as "Total Time", 
	case
		when yicode in ('VAC','PTO') then 'PTO'
		when yicode = 'HOL' then 'Holiday'
-- 		when yicode = '666' then 'Auto'
		else null
  end as "Time Off"
from (
	select pymast_employee_number as "Employee ID",
			case yico_
				when 'RY1' then '45-0236743' 
				when 'RY2' then '45-0415948'
			end as "EIN Tax Id", 
			case yico_
				when 'RY1' then 'Rydell Auto Center Inc.' 
				when 'RY2' then 'Honda Nissan of Grand Forks'
			end as "EIN Name", yiclkind as "Pay Date", yiclkind as "In Date", yiclkint as "Time In", yiclkoutt as "Time Out", 
			coalesce(
				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
					/3600.0)::numeric(6,2)) filter (where yicode in ('666','O')), 0) as clock_hours,    
			coalesce(
				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
					/3600.0)::numeric(6,2)) filter (where yicode = 'HOL'), 0) as hol_hours, 
			coalesce(
				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
					/3600.0)::numeric(6,2)) filter (where yicode = 'PTO'), 0) as pto_hours, 
			coalesce(
				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
					/3600.0)::numeric(6,2)) filter (where yicode = 'VAC'), 0) as vac_hours,
			yicode
	from ( -- d: clock hours
		select a.yico_, a.pymast_employee_number, a.yiclkind, a.yiclkint, a.yiclkoutd, a.yiclkoutt, a.yicode
		from arkona.ext_pypclockin a
		where a.yicode in ('HOL','O','PTO','VAC') -- ('HOL','666','O','PTO','VAC')
			and a.yiclkind between '07/04/2021' and '07/17/2021') d
	group by yico_, pymast_employee_number, yiclkint, yiclkoutt, yiclkind, yiclkoutd, yicode) e
left join arkona.ext_pymast f on e."Employee ID" = f.pymast_employee_number
order by "Employee Name", "In Date", "Time In"




-- do a summary of the negative clock hours excluded from v3
-- save this in docs as negative_clock_hours_excluded_from_time_series
select f.employee_name as "Employee Name", "Employee ID", "EIN Tax Id", "EIN Name", "Pay Date",
  "In Date", 
  case 
		when yicode in ('VAC','PTO','HOL') then null
		else "Time In"
	end as "Time In", 
  case 
		when yicode in ('VAC','PTO','HOL') then null
		else "Time Out"
	end as "Time Out", 
	clock_hours + hol_hours + pto_hours + vac_hours as "Total Time", 
	case
		when yicode in ('VAC','PTO') then 'PTO'
		when yicode = 'HOL' then 'Holiday'
		when yicode = '666' then 'Auto'
		else null
  end as "Time Off"
from (
	select pymast_employee_number as "Employee ID",
			case yico_
				when 'RY1' then '45-0236743' 
				when 'RY2' then '45-0415948'
			end as "EIN Tax Id", 
			case yico_
				when 'RY1' then 'Rydell Auto Center Inc.' 
				when 'RY2' then 'Honda Nissan of Grand Forks'
			end as "EIN Name", yiclkind as "Pay Date", yiclkind as "In Date", yiclkint as "Time In", yiclkoutt as "Time Out", 
			coalesce(
				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
					/3600.0)::numeric(6,2)) filter (where yicode in ('666','O')), 0) as clock_hours,    
			coalesce(
				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
					/3600.0)::numeric(6,2)) filter (where yicode = 'HOL'), 0) as hol_hours, 
			coalesce(
				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
					/3600.0)::numeric(6,2)) filter (where yicode = 'PTO'), 0) as pto_hours, 
			coalesce(
				sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
					to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
					/3600.0)::numeric(6,2)) filter (where yicode = 'VAC'), 0) as vac_hours,
			yicode
	from ( -- d: clock hours
		select a.yico_, a.pymast_employee_number, a.yiclkind, a.yiclkint, a.yiclkoutd, a.yiclkoutt, a.yicode
		from arkona.ext_pypclockin_tmp a
		where a.yicode in ('666') -- ('HOL','666','O','PTO','VAC')
			and a.yiclkind between '05/23/2021' and '06/05/2021') d
	group by yico_, pymast_employee_number, yiclkint, yiclkoutt, yiclkind, yiclkoutd, yicode) e
left join arkona.ext_pymast f on e."Employee ID" = f.pymast_employee_number
order by "Employee Name", "In Date", "Time In"
 

