﻿drop table if exists jon.direct_deposit cascade;
create table jon.direct_deposit (
	employee_number citext primary key,
	routing citext not null,
	account citext not null);

needs to include current employees and terms since 1/1/21
05/10, need to add 5 to jon.direct_deposit
insert into jon.direct_Deposit values
('152374','091000022','104784334989'),
('121869','291370918','2057149'),
('147658','071904779','199376075917'),
('184937','091300159','10184093'),
('120318','221979363','2000000468610');

-- 05/07/21 saved as employee_direct_deposit_settings_v1, set to kim as GM_employee_direct_deposit_settings_v1 & HN_employee_direct_deposit_settings_v1
-- 414
-- 05/10/21 leading zeros on account and routing
-- select * from (
select 'Auto Generate' as "Username",a.pymast_employee_number as "Employee ID",b.ssn as "SSN", 
	a.employee_name as "Employee External ID",
  case a.pymast_company_number
		when 'RY1' then '45-0236743'
		when 'RY2' then '45-0415948'
  end as "EIN Tax Id",
  case a.pymast_company_number
		when 'RY1' then 'Rydell GM'
		when 'RY2' then 'Rydell Honda Nissan'
  end as "EIN Name",
  case when c.dep_acct_type = 'C' then 'Checking' else 'Savings' end as "Account Type",
  arkona.db2_integer_to_date(a.hire_date) as "Start Date",
  'Flat $ Amount' as "Calculation Method", 'EveryPay' as "Frequency",
  '="' ||d.routing|| '"' as "ABA", '="' ||d.account|| '"' as "Account Number"
from arkona.ext_pymast a
left join jon.ssn b on a.pymast_employee_number = b.employee_number
left join arkona.ext_pypeddep c on a.pymast_employee_number = c.employee_number
left join jon.direct_deposit d on a.pymast_employee_number = d.employee_number
where a.active_code <> 'T'
  and a.pymast_company_number in ('RY1','RY2')
-- order by "EIN Tax Id", a.employee_name  
union
-- 52 terms
select 'Auto Generate' as "Username",a.pymast_employee_number as "Employee ID",b.ssn as "SSN", 
	a.employee_name as "Employee External ID",
  case a.pymast_company_number
		when 'RY1' then '45-0236743'
		when 'RY2' then '45-0415948'
  end as "EIN Tax Id",
  case a.pymast_company_number
		when 'RY1' then 'Rydell GM'
		when 'RY2' then 'Rydell Honda Nissan'
  end as "EIN Name",
  case when c.dep_acct_type = 'C' then 'Checking' else 'Savings' end as "Account Type",
  arkona.db2_integer_to_date(a.hire_date) as "Start Date",
  'Flat $ Amount' as "Calculation Method", 'EveryPay' as "Frequency",
  '="' ||d.routing|| '"' as "ABA", '="' ||d.account|| '"' as "Account Number"
from arkona.ext_pymast a	
left join jon.ssn b on a.pymast_employee_number = b.employee_number
left join arkona.ext_pypeddep c on a.pymast_employee_number = c.employee_number
left join jon.direct_deposit d on a.pymast_employee_number = d.employee_number
where pymast_company_number in ('RY1','RY2')
	and arkona.db2_integer_to_date(termination_date) between '01/01/2021' and current_date
	and employee_name not in ('AAMODT, MATTHEW','BINA, BENJAMIN','HAGER, DELWYN D', -- exclude xfr between stores
		'HOLLAND, NIKOLAI','LONGORIA, MICHAEL','KIEFER, LUCAS M')
order by "EIN Tax Id", "Employee External ID"
-- ) x where "ABA" is null



