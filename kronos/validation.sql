﻿-- create schema ukg;
-- comment on schema ukg is 'schema for ukg/kronos work';


-----------------------------------------------------------------------------------------------
--< direct deposit
-----------------------------------------------------------------------------------------------
drop table if exists ukg.direct_deposit_report cascade;
create table ukg.direct_deposit_report (
  first_name citext not null,
  last_name citext not null,
  emp_number citext not null,
  account_type citext not null, 
  routing citext not null,
  account_number citext not null,
  primary key(emp_number));
 comment on table ukg.direct_deposit_report is 'used during validation, this table is populated by the data from the ukg direct deposit report';


-- 06/10/21 generate a query for validation
-- data to compare to the data in ukg.direct_deposit_report
-- the ukg has only active employees so
-- this query from ...kronos/direct_deposit.sql
select aa.*, bb.*, cc.active_code, dds.db2_integer_to_date(cc.hire_date) as hire_date, dds.db2_integer_to_date(cc.termination_date) as term_date
from (
	select a.employee_first_name, a.employee_last_name, a.pymast_employee_number,
		case when c.dep_acct_type = 'C' then 'Checking' else 'Savings' end as account_type,
		d.routing as routing, d.account as account_number
	from arkona.ext_pymast a
	-- left join jon.ssn b on a.pymast_employee_number = b.employee_number
	left join arkona.ext_pypeddep c on a.pymast_employee_number = c.employee_number
	left join jon.direct_deposit d on a.pymast_employee_number = d.employee_number
	where a.active_code <> 'T'
		and a.pymast_company_number in ('RY1','RY2')) aa
full outer join ukg.direct_deposit_report bb on aa.pymast_employee_number = bb.emp_number
left join arkona.ext_pymast cc on coalesce(aa.pymast_employee_number, bb.emp_number) = cc.pymast_employee_number
where md5(coalesce(aa::text, 'abc')) <> md5(coalesce(bb::text, 'def'))

-- sent as validation_direct_deposit_20210610 6/10
select aa.*, bb.*, cc.active_code, dds.db2_integer_to_date(cc.hire_date) as hire_date, dds.db2_integer_to_date(cc.termination_date) as term_date
from (
	select a.employee_first_name, a.employee_last_name, a.pymast_employee_number,
		case when c.dep_acct_type = 'C' then 'Checking' else 'Savings' end as account_type,
		d.routing as routing, d.account as account_number
	from arkona.ext_pymast a
	-- left join jon.ssn b on a.pymast_employee_number = b.employee_number
	left join arkona.ext_pypeddep c on a.pymast_employee_number = c.employee_number
	left join jon.direct_deposit d on a.pymast_employee_number = d.employee_number
	where a.active_code <> 'T'
		and a.pymast_company_number in ('RY1','RY2')) aa
full outer join ukg.direct_deposit_report bb on aa.pymast_employee_number = bb.emp_number
left join arkona.ext_pymast cc on coalesce(aa.pymast_employee_number, bb.emp_number) = cc.pymast_employee_number
where (( md5(aa::text) <> md5(bb::text)) or (aa.routing is null) or (bb.routing is null))
order by aa.employee_last_name, bb.last_name

-----------------------------------------------------------------------------------------------
--/> direct deposit
-----------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------
--< scheduled deductions
-----------------------------------------------------------------------------------------------


-----------------------------------------------------------------------------------------------
--/> scheduled deductions
-----------------------------------------------------------------------------------------------
drop table if exists ukg.scheduled_deductions_report cascade;
create table ukg.scheduled_deductions_report (
  first_name citext not null,
  last_name citext not null,
  employee_number citext not null,
  deduction citext not null, 
  ee_percent citext,
  ee_amount citext,
  primary key(employee_number,deduction));
 comment on table ukg.scheduled_deductions_report is 'used during validation, this table is populated by the data from the ukg scheduled deductions report';


select aa.*, bb.*, --md5(aa::text),md5(bb::text)	
  cc.active_code, dds.db2_integer_to_date(cc.hire_date) as hire_date, dds.db2_integer_to_date(cc.termination_date) as term_date
from (
  select first_name, last_name, employee_number, 
    case
      when deduction like 'HSA Pre Tax%' then 'HSA Pre Tax'
      else deduction
    end as deduction, coalesce(nullif(ee_percent, ''), '0') as ee_percent, replace(coalesce(nullif(ee_amount, ''), '0'), ',', '') as ee_amount  -- get rid of empty strings and commas in numbers
  from ukg.scheduled_deductions_report
	order by last_name, first_name, deduction) aa
full outer join (	
-- query from .../kronos/payroll_deductions
-- dean braaton (118010) has 1% to 99 and 1% to 99A, both of which roll up into Roth 401k, so, need to group and sum
	select aa.employee_first_name, aa.employee_last_name, pymast_employee_number, 
		case 
			when bb.ded_pay_code = '111' then 'Dental Pre-Tax'
			when bb.ded_pay_code = '295' then 'Vision Pre-Tax'
			when bb.ded_pay_code = '107' then 'Medical Pre-Tax'
			when bb.ded_pay_code = '96' then 'HSA Pre Tax' -- 'HSA Pre Tax Family' --'HSA Pre-Tax Individual'
			when bb.ded_pay_code = '115' then 'FSA Dependent Care'
			when bb.ded_pay_code = '119' then 'FSA Medical'
			when bb.ded_pay_code in ('91','91b','91c') then '401K'
			when bb.ded_pay_code in ('99','99a','99b','99c') then 'Roth 401k'
			when bb.ded_pay_code = '121' then 'Long Term Disability'
			when bb.ded_pay_code = '120' then 'Short Term Disability'
			when bb.ded_pay_code = '122' then 'Employee Voluntary Life'
			when bb.ded_pay_code = '123' then 'Accident'
			when bb.ded_pay_code = '124' then 'Hospital'
			when bb.ded_pay_code = '125' then 'Critical Illness'
			when bb.ded_pay_code = '103' then 'Rydell Cares'
			when bb.ded_pay_code = '305' then 'Flex Fee'
			when bb.ded_pay_code = '227' then 'Uniforms'
			else 'XXXXXXXXXXXXXXXXXXX'
		end as deduction,
		coalesce(round(sum(case when bb.fixed_ded_pct = 'Y' then bb.fixed_Ded_amt end ), 2)::citext, '0') as ee_percent,
		coalesce(round(sum(case when bb.fixed_ded_pct is null then bb.fixed_ded_amt end), 2)::citext, '0') as ee_dollars
	from (
		select a.employee_first_name, a.employee_last_name, a.pymast_employee_number, 
			arkona.db2_integer_to_date(a.hire_date) as hire_date, a.pay_period
		from arkona.ext_pymast a
		where a.pymast_company_number in ('RY1', 'RY2')
			and a.active_code <> 'T') aa
	join (		
		select a.employee_number, a.ded_pay_code, a.fixed_ded_pct, a.fixed_ded_amt, a.ded_freq, b.description, b.account_number
		from arkona.ext_pydeduct a
		join arkona.ext_pypcodes b on a.company_number = b.company_number
			and a.ded_pay_code = b.ded_pay_code
		where a.ded_pay_code in ('103','107','111','115','119','120','121','122','123','124','125','227',
				'126','299','295','305','91','91B','91C','99','99A','99B','99C','96')
		  and fixed_ded_amt <> 0) bb on aa.pymast_employee_number = bb.employee_number
-- 	where pymast_employee_number = '1111325'
	group by aa.employee_first_name, aa.employee_last_name, pymast_employee_number, deduction
	order by aa.employee_last_name, aa.employee_first_name, deduction) bb on aa.employee_number = bb.pymast_employee_number and aa.deduction = bb.deduction
left join arkona.ext_pymast cc on coalesce(aa.employee_number, bb.pymast_employee_number) = cc.pymast_employee_number	
where (( md5(aa::text) <> md5(bb::text)) or (aa.first_name is null) or (bb.employee_first_name is null))
order by aa.last_name, bb.employee_last_name, cc.active_code


select * from arkona.ext_pydeduct where employee_number = '195826'	

gayla 1111325, 111 & 295 are 0 but show up anyway
she use to have them but no longer does