﻿-- probably need approved requests
select * 
from pto.requests

select * from pto.employee_pto_allocation

-- need to generate current from/thru for 20 year folk
select a.employee_name, a.pymast_employee_number, b.from_date, b.thru_date, b.hours, 
	coalesce(c.used_hours, 0) as used_hours
from arkona.ext_pymast a
left join pto.employee_pto_allocation b on a.pymast_employee_number = b.employee_number
  and current_date between from_date and thru_date
left join lateral(
  select employee_number, sum(hours) as used_hours
  from pto.used_pto  
  where employee_number = b.employee_number
    and the_date between b.from_date and b.thru_date
  group by employee_number) c on a.pymast_employee_number = c.employee_number
where a.active_code <> 'T'
  and a.pymast_company_number in ('RY1','RY2')
order by employee_name

-- balance 
--289
drop table if exists pto_balance;
create temp table pto_balance as
select aa.*, bb.used_hours, cc.hours as adj_hours, cc.reason, cc.notes, aa.hours - coalesce(bb.used_hours, 0) + coalesce(cc.hours, 0) as balance
from (
	select a.employee_name, a.pymast_employee_number, 
		case
			when thru_date = '12/31/9999' then
				(
					select the_date
					from dds.dim_date
					where month_of_year = extract(month from from_date)
						and day_of_month = extract(day from from_date)
						and the_date <= current_date
					order by the_date desc
					limit 1)
			else b.from_date
		end as from_date,
		case 
			when thru_date = '12/31/9999' then
				(
					select (the_date + interval '1 year')::date - 1
					from dds.dim_date
					where month_of_year = extract(month from from_date)
						and day_of_month = extract(day from from_date)
						and the_date <= current_date
					order by the_date desc
					limit 1)    
			else b.thru_date
		end as thru_date,
		b.hours
	from arkona.ext_pymast a
	join pto.employee_pto_allocation b on a.pymast_employee_number = b.employee_number
		and current_date between from_date and thru_date
  left join pto.exclude_employees c on a.pymast_employee_number = c.employee_number		
	where a.active_code <> 'T'
	  and c.employee_number is null
		and a.pymast_company_number in ('RY1','RY2')) aa
left join lateral(
  select employee_number, sum(hours) as used_hours
  from pto.used_pto  
  where employee_number = aa.pymast_employee_number
    and the_date between aa.from_date and aa.thru_date
  group by employee_number) bb on aa.pymast_employee_number = bb.employee_number		
left join pto.adjustments cc on aa.pymast_employee_number = cc.employee_number 
  and cc.from_date between aa.from_date and aa.thru_date
order by employee_name;



kim ran a ukg report, i put the values into jon.ukg_pto_status,
then sent her the results of this query as E:\sql\postgresql\kronos\files\compare_pto_1-11-22.xlsx

select * 
from pto_balance a
full outer join jon.ukg_pto_status b on a.employee_name = b.last_name || ', ' || b.first_name

drop table if exists jon.ukg_pto_status;
create table jon.ukg_pto_status (
  first_name citext,
  last_name citext,
  start_date date,
  hours_earned_ytd citext,
  hours_take_ytd citext,
  hours_scheduled citext, 
  hours_remaining citext);

select * 
from (
select a.*, b.employee_number
from jon.ukg_pto_status a
left join ukg.employees b on a.first_name = b.first_name and a.last_name = b.last_name) aa
full outer join pto_balance bb on aa.employee_number = bb.pymast_employee_number
order by coalescE(aa.last_name || aa.first_name, bb.employee_name)

insert into jon.ukg_pto_status values('MATTHEW','AAMODT','06/03/2019',84,8,0,76);
insert into jon.ukg_pto_status values('AMANDA','AANENSON','06/03/2019',66,0,32,34);
insert into jon.ukg_pto_status values('PATRICK','ADAM','04/25/2016',92,0,0,92);
insert into jon.ukg_pto_status values('OM','ADHIKARI','06/17/2019',2,0,0,2);
insert into jon.ukg_pto_status values('John','Alvarez','09/16/2021',0,0,0,0);
insert into jon.ukg_pto_status values('JON','ANDREWS','04/11/2006',0,0,0,0);
insert into jon.ukg_pto_status values('TREVOR','ATWELL','02/24/2020',16,8,0,8);
insert into jon.ukg_pto_status values('THOMAS','AUBOL','11/09/1998',112,64,0,48);
insert into jon.ukg_pto_status values('DALE','AXTMAN','05/01/2001',112,8,0,104);
insert into jon.ukg_pto_status values('CASPER','BACH','03/24/2020',48,0,0,48);
insert into jon.ukg_pto_status values('AMANDA','BAGLEY','08/26/2019',24,5.5,0,18.5);
insert into jon.ukg_pto_status values('JADE','BARTA','10/08/2014',43,30,0,13);
insert into jon.ukg_pto_status values('CORY','BATZER','11/06/2017',112,8,0,104);
insert into jon.ukg_pto_status values('JEFFERY','BEAR','09/13/2010',32,0,0,32);
insert into jon.ukg_pto_status values('THOMAS','BERGE','10/08/2019',80,0,0,80);
insert into jon.ukg_pto_status values('Brian','Bertsch','11/29/2021',0,0,0,0);
insert into jon.ukg_pto_status values('DAVID','BIES II','01/28/2019',48,0,40,8);
insert into jon.ukg_pto_status values('BENJAMIN','BINA','07/02/2018',28.5,24,0,4.5);
insert into jon.ukg_pto_status values('MARK','BJORNSETH','09/25/1989',192,0,0,192);
insert into jon.ukg_pto_status values('KATHERINE','BLUMHAGEN','08/19/2002',136,8,8,120);
insert into jon.ukg_pto_status values('DOUGLAS','BOHM','03/01/1993',0,0,0,0);
insert into jon.ukg_pto_status values('Dylan','Bork','09/20/2021',0,0,0,0);
insert into jon.ukg_pto_status values('DEAN','BRAATEN','08/10/2011',0,0,0,0);
insert into jon.ukg_pto_status values('Cameron','Bridger','01/10/2022',0,0,0,0);
insert into jon.ukg_pto_status values('KATRINA','BRIONES-BURNETT','04/06/2020',0,0,0,0);
insert into jon.ukg_pto_status values('MARK','BRODEN','08/25/2021',112,0,0,112);
insert into jon.ukg_pto_status values('DREW','BROOKS JR','05/23/2016',76,0,0,76);
insert into jon.ukg_pto_status values('BO','BRORBY','11/23/2015',112,0,0,112);
insert into jon.ukg_pto_status values('AFTON','BRUGGEMAN','08/01/2016',0,0,0,0);
insert into jon.ukg_pto_status values('JUSTIN','BRUNK','02/26/2018',72,0,0,72);
insert into jon.ukg_pto_status values('Skyler','Bruns','12/29/2021',0,0,0,0);
insert into jon.ukg_pto_status values('KYLE','BUCHANAN','05/08/2017',0,0,0,0);
insert into jon.ukg_pto_status values('ZACHARY','BUCKHOLZ','04/10/2017',112,0,0,112);
insert into jon.ukg_pto_status values('BENJAMIN','CAHALAN','08/03/2011',152,0,0,152);
insert into jon.ukg_pto_status values('Cory','Callahan','08/25/2021',0,0,0,0);
insert into jon.ukg_pto_status values('ROBERT','CARLSON','04/01/1997',0,0,0,0);
insert into jon.ukg_pto_status values('TAYLOR','CARLSTEDT','02/22/2021',0,0,0,0);
insert into jon.ukg_pto_status values('Brendin','Carlstedt','12/29/2021',0,0,0,0);
insert into jon.ukg_pto_status values('Triston','Charette','11/29/2021',0,0,0,0);
insert into jon.ukg_pto_status values('JACOB','CHARLEY','01/11/2021',72,0,0,72);
insert into jon.ukg_pto_status values('Karissa','Christiansen','05/10/2021',0,0,0,0);
insert into jon.ukg_pto_status values('Tyiler','Clay','11/15/2021',0,0,0,0);
insert into jon.ukg_pto_status values('PRESTON','CLOSE','09/12/2019',112,0,0,112);
insert into jon.ukg_pto_status values('MICHELLE','COCHRAN','01/24/2011',152,0,72,152);
insert into jon.ukg_pto_status values('Ethan','Crane','08/25/2021',0,0,0,0);
insert into jon.ukg_pto_status values('CRAIG','CROAKER','01/30/2007',40,40,0,0);
insert into jon.ukg_pto_status values('JOEL','DANGERFIELD','11/29/2001',192,0,0,192);
insert into jon.ukg_pto_status values('GORDON','DAVID','06/07/1995',88,0,0,88);
insert into jon.ukg_pto_status values('Nile','Davidson','11/01/2021',0,0,0,0);
insert into jon.ukg_pto_status values('ERIC','DAVIS','08/11/2014',104,8,24,72);
insert into jon.ukg_pto_status values('XANDER','DEARINGER','04/19/2021',0,0,0,0);
insert into jon.ukg_pto_status values('RUBEN','DELACRUZ','10/28/2020',0,0,0,0);
insert into jon.ukg_pto_status values('MICHAEL','DELOHERY','05/03/2010',152,0,56,96);
insert into jon.ukg_pto_status values('COLTEN','DEMANT','06/01/2020',72,0,0,72);
insert into jon.ukg_pto_status values('DYLAN','DEZIEL','02/21/2019',56,8,0,48);
insert into jon.ukg_pto_status values('NATE','DOCKENDORF','05/18/2015',96,0,0,96);
insert into jon.ukg_pto_status values('TERRANCE','DRISCOLL','11/04/2002',144,12,0,132);
insert into jon.ukg_pto_status values('DYLAN','DUBOIS','08/03/2020',72,40,0,32);
insert into jon.ukg_pto_status values('JARED','DUCKSTAD','09/30/2002',168,32,8,128);
insert into jon.ukg_pto_status values('TIMOTHY','DURAND','07/27/2020',0,0,0,0);
insert into jon.ukg_pto_status values('Crystal','Dvorak','12/13/2021',0,0,0,0);
insert into jon.ukg_pto_status values('Johnny','Earls','12/29/2021',0,0,0,0);
insert into jon.ukg_pto_status values('MARCUS','EBERLE','08/15/2016',17.5,4,0,13.5);
insert into jon.ukg_pto_status values('ERIK','ENGEBRETSON','07/02/2018',104,8,0,96);
insert into jon.ukg_pto_status values('ANTHONY','ERICKSON','08/26/2019',92,0,0,92);
insert into jon.ukg_pto_status values('RONALD','ERICKSON','10/12/2009',152,0,0,152);
insert into jon.ukg_pto_status values('KENNETH','ESPELUND','09/19/2005',104,0,64,40);
insert into jon.ukg_pto_status values('NED','EULISS','11/02/2004',120,8,0,112);
insert into jon.ukg_pto_status values('DANIEL','EVAVOLD','01/23/2007',0,0,0,0);
insert into jon.ukg_pto_status values('ETHAN','FARLEY','12/18/2021',43.59,0,0,43.59);
insert into jon.ukg_pto_status values('LUDWIG','FEIST','01/23/1995',19,8,0,11);
insert into jon.ukg_pto_status values('SIERRA','FERDON','10/28/2020',64,0,0,64);
insert into jon.ukg_pto_status values('GAVIN','FLAAT','04/11/2016',0,0,0,0);
insert into jon.ukg_pto_status values('BENJAMIN','FOSTER','10/21/2013',112,0,0,112);
insert into jon.ukg_pto_status values('KEVIN','FOSTER','06/05/2006',0,0,0,0);
insert into jon.ukg_pto_status values('BRADEN','FRASER','01/05/2017',68,0,0,68);
insert into jon.ukg_pto_status values('ANASTASIA','GAGNON','05/27/2018',18,0,0,18);
insert into jon.ukg_pto_status values('JUNE','GAGNON','02/11/2008',152,0,0,152);
insert into jon.ukg_pto_status values('TRACI','GAGNON','10/30/2008',112,0,0,112);
insert into jon.ukg_pto_status values('CHAD','GARDNER','04/04/2011',108,8,32,68);
insert into jon.ukg_pto_status values('JOHN','GARDNER','09/14/2010',112,0,0,112);
insert into jon.ukg_pto_status values('CLAYTON','GEHRTZ','12/29/2014',112,32,48,32);
insert into jon.ukg_pto_status values('ELIJAH','GOETZ','03/24/2020',0,0,0,0);
insert into jon.ukg_pto_status values('DANIEL','GONZALEZ','12/17/2019',112,0,0,112);
insert into jon.ukg_pto_status values('DEREK','GOODOIEN','07/11/2019',112,0,0,112);
insert into jon.ukg_pto_status values('CONNER','GOTHBERG','08/25/2014',84,0,0,84);
insert into jon.ukg_pto_status values('DESIREE','GRANT','05/19/2014',0,0,0,0);
insert into jon.ukg_pto_status values('NATHAN','GRAY','08/22/2011',48,16,72,032);
insert into jon.ukg_pto_status values('JORDYN','GREER','08/24/2020',40,40,0,0);
insert into jon.ukg_pto_status values('TYLER','GROLLIMUND','08/01/2018',112,0,0,112);
insert into jon.ukg_pto_status values('JONATHAN','GRYSKIEWICZ','12/06/2021',112,0,0,112);
insert into jon.ukg_pto_status values('Viviana','Guerrero','11/04/2021',0,0,0,0);
insert into jon.ukg_pto_status values('BRITTANY','HAARSTAD','08/26/2019',112,0,0,112);
insert into jon.ukg_pto_status values('WILLIAM','HACK','04/19/2021',0,0,0,0);
insert into jon.ukg_pto_status values('TYLER','HAGEN','10/28/2019',88,8,0,80);
insert into jon.ukg_pto_status values('William','Halldorson','06/07/2021',0,0,0,0);
insert into jon.ukg_pto_status values('JADIN','HAMS','05/04/2021',0,0,0,0);
insert into jon.ukg_pto_status values('Eric','Hanson','05/07/2021',0,0,0,0);
insert into jon.ukg_pto_status values('TERRENCE','HARMON','04/21/1993',24,8,0,16);
insert into jon.ukg_pto_status values('DAWN','HASTINGS','01/06/1986',192,0,16,176);
insert into jon.ukg_pto_status values('JOSH','HEFFERNAN','07/15/1995',136,16,88,72);
insert into jon.ukg_pto_status values('JACOB','HEIN','09/03/2020',0,0,0,0);
insert into jon.ukg_pto_status values('Talasia','Hendry-Robinson','09/07/2021',0,0,0,0);
insert into jon.ukg_pto_status values('JUDY','HENRY','02/01/1994',0,0,0,0);
insert into jon.ukg_pto_status values('MORGAN','HIBMA','08/01/2016',112,0,0,112);
insert into jon.ukg_pto_status values('BRIAN','HILL','08/28/2006',112,0,0,112);
insert into jon.ukg_pto_status values('Sebastain','Hipsak','11/15/2021',0,0,0,0);
insert into jon.ukg_pto_status values('KIRBY','HOLWERDA','05/18/2011',48,16,16,16);
insert into jon.ukg_pto_status values('LIZ','HOWARD','07/27/2020',0,0,0,0);
insert into jon.ukg_pto_status values('DAVID','HULL','04/27/2015',16,16,0,0);
insert into jon.ukg_pto_status values('PETER','JACOBSON','12/13/2010',152,8,0,144);
insert into jon.ukg_pto_status values('Gabriel','Jarvi','07/12/2021',0,0,0,0);
insert into jon.ukg_pto_status values('Patrick','Jensen','07/21/2021',0,0,0,0);
insert into jon.ukg_pto_status values('BRANDON','JOHNSON','07/20/2004',32,16,0,16);
insert into jon.ukg_pto_status values('CHRISTIE','JOHNSON','07/29/2019',104,0,0,104);
insert into jon.ukg_pto_status values('DUSTIN','JOHNSON','12/17/2007',152,32,0,120);
insert into jon.ukg_pto_status values('BENJAMIN','JOHNSON','10/08/2014',0,0,0,0);
insert into jon.ukg_pto_status values('CARISSA','JOHNSON','04/19/2021',0,0,0,0);
insert into jon.ukg_pto_status values('ISAIAH','JOHNSON','06/29/2020',46,16,0,30);
insert into jon.ukg_pto_status values('JARED','JOHNSON','01/25/2021',0,0,0,0);
insert into jon.ukg_pto_status values('Carol','Kallias','10/18/2021',0,0,0,0);
insert into jon.ukg_pto_status values('JUSTIN','KILMER','02/01/2007',152,0,0,152);
insert into jon.ukg_pto_status values('HALEY','KLOETY','04/29/2019',0,0,0,0);
insert into jon.ukg_pto_status values('BENJAMIN','KNUDSON','08/12/2009',140,0,0,140);
insert into jon.ukg_pto_status values('KENNETH','KNUDSON','10/06/1980',184,8,0,176);
insert into jon.ukg_pto_status values('DEBRA','KOENEN','08/04/2008',104,24,0,80);
insert into jon.ukg_pto_status values('KIRK','KOLSTAD','03/01/1980',24,8,0,16);
insert into jon.ukg_pto_status values('KELSEY','KOZEL','05/18/2020',29,20,64,055);
insert into jon.ukg_pto_status values('Jacey','Kuersteiner','11/04/2021',0,0,0,0);
insert into jon.ukg_pto_status values('Katie','Kuester','08/09/2021',112,0,0,112);
insert into jon.ukg_pto_status values('VALERIE','KUGLIN','12/28/2020',72,0,0,72);
insert into jon.ukg_pto_status values('Mason','Kurtz','11/29/2021',0,0,0,0);
insert into jon.ukg_pto_status values('KODI','LABELLE','12/17/2018',112,0,0,112);
insert into jon.ukg_pto_status values('REBECCA','LALLIER','09/08/2020',40,0,0,40);
insert into jon.ukg_pto_status values('BRANDON','LAMONT','10/12/2015',52,32,32,012);
insert into jon.ukg_pto_status values('CODY','LARSON','08/15/2016',80,48,0,32);
insert into jon.ukg_pto_status values('LARRY','LAUGHLIN','01/21/1996',32,16,16,0);
insert into jon.ukg_pto_status values('PAUL','LEAVY','08/25/2014',48,24,0,24);
insert into jon.ukg_pto_status values('AUSTIN','LECLAIR','11/19/2018',112,0,0,112);
insert into jon.ukg_pto_status values('KATHERINE','LIND','11/22/1983',192,0,0,192);
insert into jon.ukg_pto_status values('TASHA','LINDEMANN','09/11/2017',56,8,0,48);
insert into jon.ukg_pto_status values('Jeremiah','Lindgren','10/04/2021',0,0,0,0);
insert into jon.ukg_pto_status values('TRENTON','LONG','02/22/2021',0,8,0,08);
insert into jon.ukg_pto_status values('BEVERLEY','LONGORIA','02/21/2005',96,0,56,88);
insert into jon.ukg_pto_status values('MICHAEL','LONGORIA','07/30/2018',0,0,0,0);
insert into jon.ukg_pto_status values('Diandra','Lopez','06/21/2021',0,0,0,0);
insert into jon.ukg_pto_status values('ZAKERY','LOUDEN','05/29/2019',0,0,0,0);
insert into jon.ukg_pto_status values('ARDEN','LOVEN','03/08/2010',72,40,0,32);
insert into jon.ukg_pto_status values('DAVID','LUEKER','06/16/2003',48,16,0,32);
insert into jon.ukg_pto_status values('SAVANAH','MAGENAU','09/07/2021',0,0,0,0);
insert into jon.ukg_pto_status values('TYLER','MAGNUSON','09/09/2019',96,8,0,88);
insert into jon.ukg_pto_status values('RICHARD','MANGAN','07/03/1997',120,8,0,112);
insert into jon.ukg_pto_status values('THOMAS','MARCOTTE','07/28/1985',88,0,0,88);
insert into jon.ukg_pto_status values('DAYTON','MAREK','09/12/2021',96,0,0,96);
insert into jon.ukg_pto_status values('Lacey','Markel','07/27/2021',0,0,0,0);
insert into jon.ukg_pto_status values('NICHOLAS','MARO','09/08/2009',21,21,0,0);
insert into jon.ukg_pto_status values('Kyle','Martens','11/29/2021',0,0,0,0);
insert into jon.ukg_pto_status values('AMBE','MCKENZIE','01/11/2021',72,0,0,72);
insert into jon.ukg_pto_status values('JOHN','MCLAUGHLIN','12/11/2017',0,0,0,0);
insert into jon.ukg_pto_status values('DENNIS','MCVEIGH','11/13/2007',152,24,16,112);
insert into jon.ukg_pto_status values('David','McVeigh','11/02/2021',0,0,0,0);
insert into jon.ukg_pto_status values('Jonathon','Melander','07/12/2021',0,0,0,0);
insert into jon.ukg_pto_status values('MICHAEL','MENARD','02/12/2018',48,8,0,40);
insert into jon.ukg_pto_status values('Misty','Messenger','07/12/2021',0,0,0,0);
insert into jon.ukg_pto_status values('JUSTIN','METZGER','05/24/2010',136,48,0,88);
insert into jon.ukg_pto_status values('KAYLE','METZGER','11/20/2013',94,4,0,90);
insert into jon.ukg_pto_status values('Kerry','Meyers','06/01/2021',0,0,0,0);
insert into jon.ukg_pto_status values('NICHOLAS','MICHAEL','07/27/2021',0,0,0,0);
insert into jon.ukg_pto_status values('ANTHONY','MICHAEL','10/25/2010',152,8,0,144);
insert into jon.ukg_pto_status values('PAUL','MILLER','10/28/2020',56,16,0,40);
insert into jon.ukg_pto_status values('KIM','MILLER','01/02/1985',192,0,0,192);
insert into jon.ukg_pto_status values('JESSIE','MONREAL','09/25/2017',112,0,0,112);
insert into jon.ukg_pto_status values('Kane','Moon','11/04/2021',0,0,0,0);
insert into jon.ukg_pto_status values('GREG','MORRIS','04/19/2010',152,0,0,152);
insert into jon.ukg_pto_status values('Vince','Morrotto','10/18/2021',112,0,0,112);
insert into jon.ukg_pto_status values('KYLE','MURDOCH','06/29/2020',72,0,0,72);
insert into jon.ukg_pto_status values('LAUREN','NELSON','05/04/2020',0,0,0,0);
insert into jon.ukg_pto_status values('NICKOLAS','NEUMANN','08/03/2015',12,0,0,12);
insert into jon.ukg_pto_status values('ANDREW','NEUMANN','03/03/2003',152,0,0,152);
insert into jon.ukg_pto_status values('NANCY','NEUMANN','06/11/2018',102,0,0,102);
insert into jon.ukg_pto_status values('Nathan','Nichols','08/04/2021',0,0,0,0);
insert into jon.ukg_pto_status values('RONALD','NOKELBY','05/05/1978',0,0,0,0);
insert into jon.ukg_pto_status values('JOSHUA','NORTHAGEN','03/12/2019',18,8,0,10);
insert into jon.ukg_pto_status values('KENT','NYGAARD','01/26/2015',8,8,0,0);
insert into jon.ukg_pto_status values('JOHN','O''BRYAN','09/08/2020',72,0,0,72);
insert into jon.ukg_pto_status values('MATTHEW','O''LEARY','12/31/2019',112,0,0,112);
insert into jon.ukg_pto_status values('TIMOTHY','O''NEIL','08/11/2014',52,12,112,8);
insert into jon.ukg_pto_status values('JOHN','OLDERBAK','02/07/2005',56,0,0,56);
insert into jon.ukg_pto_status values('CODEY','OLSON','07/20/2015',48,20,0,28);
insert into jon.ukg_pto_status values('JAY','OLSON','08/19/1994',104,0,48,56);
insert into jon.ukg_pto_status values('JUSTIN','OLSON','11/12/2012',72,8,0,64);
insert into jon.ukg_pto_status values('Emily','Olson','06/01/2021',0,0,0,0);
insert into jon.ukg_pto_status values('CHAD','OLSON','09/09/2002',152,0,0,152);
insert into jon.ukg_pto_status values('Joshua','Olson','11/15/2021',0,0,0,0);
insert into jon.ukg_pto_status values('Jamie','Olson','10/11/2021',0,0,0,0);
insert into jon.ukg_pto_status values('GUADALUPE','ORTIZ','11/12/2012',104,32,0,72);
insert into jon.ukg_pto_status values('DENISE','OSOWSKI','07/27/2020',6.17,6.28,0,00.11);
insert into jon.ukg_pto_status values('FEDERICO','PALACIOS','08/12/2020',16,8,8,0);
insert into jon.ukg_pto_status values('ERIC','PALACIOS-FOLEY','01/13/2020',0,0,0,0);
insert into jon.ukg_pto_status values('JOSHUA','PARKER','09/12/2019',64,23,0,41);
insert into jon.ukg_pto_status values('MATTHEW','PASCHKE','08/26/2019',96,8,40,48);
insert into jon.ukg_pto_status values('DOUGLAS','PEDE','05/04/2020',40,0,0,40);
insert into jon.ukg_pto_status values('DAVID','PEDERSON','12/16/2011',152,8,0,144);
insert into jon.ukg_pto_status values('LIL CHRIS','PERALES','10/21/2019',72,0,0,72);
insert into jon.ukg_pto_status values('BRIAN','PETERSON','03/15/1999',48,8,0,40);
insert into jon.ukg_pto_status values('DOUG','PETERSON','12/04/1995',0,0,0,0);
insert into jon.ukg_pto_status values('WHITNEY','PICKENS','03/24/2020',72,48,24,0);
insert into jon.ukg_pto_status values('CHANDRA','POKHREL','12/17/2018',112,0,0,112);
insert into jon.ukg_pto_status values('Megan','Polley','11/15/2021',0,0,0,0);
insert into jon.ukg_pto_status values('Tricia','Pope','08/02/2021',0,0,0,0);
insert into jon.ukg_pto_status values('ELBERT','POPE','09/21/2020',53.02,0,0,53.02);
insert into jon.ukg_pto_status values('DEVON','POPE','11/16/2021',0,0,0,0);
insert into jon.ukg_pto_status values('GAYLA','POWELL','08/29/2011',152,0,96,56);
insert into jon.ukg_pto_status values('Evin','Prock','12/29/2021',0,0,0,0);
insert into jon.ukg_pto_status values('JACK','RADKE','04/05/2021',0,0,0,0);
insert into jon.ukg_pto_status values('SANTI','RAI','04/23/2018',0,0,0,0);
insert into jon.ukg_pto_status values('VALERIE','RAUNER','06/19/2017',60,0,56,4);
insert into jon.ukg_pto_status values('TIMOTHY','REUTER','01/26/2015',12,0,0,12);
insert into jon.ukg_pto_status values('DARRICK','RICHARDSON','04/11/2011',80,64,8,8);
insert into jon.ukg_pto_status values('REBECCA','RISBERG','01/07/2020',112,0,0,112);
insert into jon.ukg_pto_status values('RUDOLPH','ROBLES','07/12/2000',111,0,0,111);
insert into jon.ukg_pto_status values('JUSTIN','RODRIGUEZ','11/23/2015',88,16,0,72);
insert into jon.ukg_pto_status values('MITCH','ROGERS','05/29/2012',8,0,0,8);
insert into jon.ukg_pto_status values('CRAIG','ROGNE','06/13/1988',16,0,0,16);
insert into jon.ukg_pto_status values('Cole','Rolf','10/04/2021',0,0,0,0);
insert into jon.ukg_pto_status values('BRITTANY','ROSE','10/07/2019',72,0,0,72);
insert into jon.ukg_pto_status values('LAURA','ROTH','11/20/2017',112,0,0,112);
insert into jon.ukg_pto_status values('ROBERT','RUMEN','05/07/2018',80,0,0,80);
insert into jon.ukg_pto_status values('ANTONIO','RYAN','03/29/2012',112,0,0,112);
insert into jon.ukg_pto_status values('BRIAN','RYDELL','06/28/1999',192,0,0,192);
insert into jon.ukg_pto_status values('WESLEY','RYDELL','06/01/1962',0,0,0,0);
insert into jon.ukg_pto_status values('RANDY','SATTLER','08/26/2002',152,0,0,152);
insert into jon.ukg_pto_status values('TARA','Sattler','09/10/2018',68,20,48,0);
insert into jon.ukg_pto_status values('THOMAS','SAULS','12/13/2021',112,0,0,112);
insert into jon.ukg_pto_status values('MICHAEL','SCHADT','06/25/2020',0,0,0,0);
insert into jon.ukg_pto_status values('John','Schlosser','11/15/2021',0,0,0,0);
insert into jon.ukg_pto_status values('JERI','SCHMIESS PENAS','05/12/2009',152,0,0,152);
insert into jon.ukg_pto_status values('BRIAN','SCHNEIDER','06/05/2017',0,8,0,08);
insert into jon.ukg_pto_status values('BRADLEY','SCHUMACHER','08/03/2020',48,8,0,40);
insert into jon.ukg_pto_status values('KODEY','SCHUPPERT','04/27/2015',19,0,0,19);
insert into jon.ukg_pto_status values('MICHAEL','SCHWAN','05/16/2005',68,0,52,16);
insert into jon.ukg_pto_status values('BRADLEY','SEEBA','04/11/1977',0,8,0,08);
insert into jon.ukg_pto_status values('SCOTT','SEVIGNY','09/29/2008',144,4,0,140);
insert into jon.ukg_pto_status values('LOREN','SHERECK','10/30/2006',136,16,0,120);
insert into jon.ukg_pto_status values('NICHOLAS','SHIREK','09/20/2010',152,0,0,152);
insert into jon.ukg_pto_status values('DUSTIN','SHOWER','08/20/2018',24,0,0,24);
insert into jon.ukg_pto_status values('RYAN','SHROYER','04/10/2013',112,0,0,112);
insert into jon.ukg_pto_status values('MARTIN','SHUMAKER','07/01/2013',61,24,0,37);
insert into jon.ukg_pto_status values('JACOB','SIEDSCHLAG','09/10/2020',72,0,0,72);
insert into jon.ukg_pto_status values('TRAVONTE','SIMMONS','10/28/2020',0,0,0,0);
insert into jon.ukg_pto_status values('Mitchell','Simon','06/03/2021',0,0,0,0);
insert into jon.ukg_pto_status values('JASON','SMERER','05/20/2019',55,55,0,0);
insert into jon.ukg_pto_status values('CONNOR','SMITH','03/09/2021',0,0,0,0);
insert into jon.ukg_pto_status values('TANER','SMITH','05/07/2018',64,0,0,64);
insert into jon.ukg_pto_status values('Natassja','Smith','12/02/2021',0,0,0,0);
insert into jon.ukg_pto_status values('NICHOLAS','SOBERG','08/27/2018',84,16,0,68);
insert into jon.ukg_pto_status values('PAUL','SOBOLIK','03/23/1992',43.5,12,0,31.5);
insert into jon.ukg_pto_status values('ARIK','SOLHEIM','04/15/2019',112,0,0,112);
insert into jon.ukg_pto_status values('ABIGAIL','SORUM','10/26/2010',112,0,0,112);
insert into jon.ukg_pto_status values('JACOB','SORUM','06/03/2019',112,0,0,112);
insert into jon.ukg_pto_status values('MICHAEL','SORUM','11/04/2002',0,0,0,0);
insert into jon.ukg_pto_status values('GREG','SORUM','01/01/1996',192,0,0,192);
insert into jon.ukg_pto_status values('FRED','SPENCER','02/01/2005',16,16,48,0);
insert into jon.ukg_pto_status values('RICHARD','STALLMO','08/04/2009',144,8,0,136);
insert into jon.ukg_pto_status values('DONALD','STEIN','08/13/2012',28,64,0,036);
insert into jon.ukg_pto_status values('RICK','STOUT','08/29/2016',88,16,0,72);
insert into jon.ukg_pto_status values('TIMOTHY','STREETER','03/28/2007',64,24,0,40);
insert into jon.ukg_pto_status values('BROOKE','SUTHERLAND','03/27/2017',80,8,40,32);
insert into jon.ukg_pto_status values('ELIZABETH','SYMONS','12/27/2012',0,0,0,0);
insert into jon.ukg_pto_status values('STEVEN','SYMONS','05/10/2010',152,0,0,152);
insert into jon.ukg_pto_status values('JOSHUA','SYVERSON','10/27/2005',120,72,0,48);
insert into jon.ukg_pto_status values('ASHLEY','Syverson','11/18/2019',84,4,48,32);
insert into jon.ukg_pto_status values('JEFF','TARR','11/01/1993',192,48,0,144);
insert into jon.ukg_pto_status values('WYATT','THOMPSON','05/14/2007',28,0,0,28);
insert into jon.ukg_pto_status values('ERIC','THOMPSON','06/14/2021',0,0,0,0);
insert into jon.ukg_pto_status values('RODNEY','TROFTGRUBEN','08/03/1992',136,24,0,112);
insert into jon.ukg_pto_status values('RICHARD','TURNER','12/31/2018',112,0,0,112);
insert into jon.ukg_pto_status values('FRED','VAN HESTE II','05/15/2007',40,40,0,0);
insert into jon.ukg_pto_status values('DAVID','VANYO','12/18/2017',112,0,0,112);
insert into jon.ukg_pto_status values('Laura','Vasko','10/18/2021',0,0,0,0);
insert into jon.ukg_pto_status values('Caleb','Vigil','08/25/2021',0,0,0,0);
insert into jon.ukg_pto_status values('AARON','VONASEK','11/16/2020',64,24,0,40);
insert into jon.ukg_pto_status values('SAMUEL','WAGENER','01/01/2014',0,0,0,0);
insert into jon.ukg_pto_status values('ALEX','WALDBAUER','08/29/2007',88,8,0,80);
insert into jon.ukg_pto_status values('JAMES','WALDECK','10/21/2019',112,40,0,72);
insert into jon.ukg_pto_status values('CHRIS','WALDEN','09/06/1994',160,4,0,156);
insert into jon.ukg_pto_status values('John','Walsh','05/19/2021',0,0,0,0);
insert into jon.ukg_pto_status values('JOSHUA','WALTON','12/27/2006',112,8,0,104);
insert into jon.ukg_pto_status values('JAMES','WARMACK','03/01/1985',32,24,0,8);
insert into jon.ukg_pto_status values('JAMES','WEBER','10/10/2011',56,32,0,24);
insert into jon.ukg_pto_status values('COREY','WILDE','08/06/2012',104,8,0,96);
insert into jon.ukg_pto_status values('DAVID','WILKIE','05/17/1978',192,0,0,192);
insert into jon.ukg_pto_status values('BRAYDON','WILLIAMS','05/04/2020',0,0,0,0);
insert into jon.ukg_pto_status values('Tyler','Wilson','09/01/2021',0,0,0,0);
insert into jon.ukg_pto_status values('PATRICK','WITTER','02/24/2020',0,0,0,0);
insert into jon.ukg_pto_status values('Christopher','Wofford','12/29/2021',0,0,0,0);
insert into jon.ukg_pto_status values('EMILEE','WOINAROWICZ','07/29/2013',0,0,0,0);
insert into jon.ukg_pto_status values('SCOTT','WOODS','06/24/2019',48,24,0,24);
insert into jon.ukg_pto_status values('RITHY','YEM','04/29/2013',112,0,0,112);
insert into jon.ukg_pto_status values('STEVEN','YOUNG','03/29/2021',0,0,0,0);
insert into jon.ukg_pto_status values('STEVE','ZOELLICK','04/01/2004',0,0,0,0);
