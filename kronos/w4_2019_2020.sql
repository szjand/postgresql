﻿create table arkona.ext_pymastex (
	pymastex_company_number citext,
	pymastex_employee_number citext, 
	dependt_amt numeric(9,2),
	other_income_adjt numeric(9,2),
	other_deductn numeric(9,2),
	extra_wh numeric(9,2),
	credit_tax_method citext,
	user_crt citext,
	user_upd_last citext,
	timestamp_upd_last TIMESTAMPTZ,
	timestamp_crt TIMESTAMPTZ);


     select string_agg(column_name, ',' order by ordinal_position)
     from information_schema.columns
     where table_schema = 'arkona'
       and table_name = 'ext_pymastex'	

pymastex_company_number,pymastex_employee_number,dependt_amt,other_income_adjt,other_deductn,extra_wh,credit_tax_method,user_crt,user_upd_last,timestamp_upd_last,timestamp_crt       


select * 
from arkona.ext_pymastex
order by pymastex_employee_number

select employee_name as "Name", pymast_employee_number as "Emp#", 
  case 
    when b.pymastex_company_number is null then '2019'
    else '2020'
  end as "W4",
  b.dependt_amt as "Dependent Amount", b.other_income_adjt as "Other Income Adj", 
  b.other_deductn as "Other Deductions", b.extra_wh as "Extra Withholding",
  b.credit_tax_method as "Credit Tax Method"
from arkona.ext_pymast a
left join arkona.ext_PYMASTEX b on trim(a.pymast_employee_number) = trim(pymastex_employee_number)
where a.active_code <> 'T'
order by a.employee_name