﻿/*
09/28/21
 Hi Jon
Can you pull this info for me?
This spreadsheet  is what I am going to import into  UKG to make the LTD and STD work, because UKG only uses the hourly rate by 2080 hours or salary The benefit salary 1 column we are going to use the year to date wage from
2020  and the effective date will be 7-1-2021 Let me know if you have questions Thanks Kim

in response to my questions:
Yes total gross for 2020,  and current employees, the ones hired after 2020 let's leave those blank
*/
select pymast_employee_number as "Employee Id", employee_first_name as first_name, employee_last_name as last_name, b.total as "Benefit Salary 1", '07/01/2021' as "Benefit Salary 1 Effective From"
from arkona.ext_pymast a
left join (
	select employee_, sum(total_gross_pay) as total
	from arkona.ext_pyhshdta
	where check_year = 20
	group by employee_) b on a.pymast_employee_number = b.employee_
where active_code <> 'T'
  and pymast_company_number in ('ry1','ry2')
  and a.pymast_employee_number not in ('290910','179380')
order by employee_last_name  


