﻿
select aa.name, aa."emp #", 
  case
    when coalesce(aa.hours, 0) = 0 then bb.hours
    else aa.hours
  end as clock_hours
from (
	select a.employee_name as name, a.pymast_employee_number as "emp #", b.hours
	from arkona.ext_pymast a
	left join (
		select employee_number, sum(clock_hours) as hours 
		from arkona.xfm_pypclockin a
		where a.the_date between '01/01/2021' and '12/31/2021'
		group by employee_number) b on a.pymast_employee_number = b.employee_number
	where a.active_code <> 'T'
		and a.pymast_company_number in ('RY1','RY2')) aa
-- order by a.employee_name
-- order by hours nulls first
left join (
	select name, "emp #", sum(reg_hours) as hours
	from (
	select a.employee_name as name, a.pymast_employee_number as "emp #", b.reg_hours 
	from arkona.ext_pymast a
	join arkona.ext_pyhshdta b on a.pymast_employee_number = b.employee_
		and payroll_cen_year = 121
	where a.active_code <> 'T') c
	group by name, "emp #") bb on aa."emp #" = bb."emp #"