﻿
-- aftermarket
select stock_number, sum(aftermarket) as aftermarket
from (
 select *
 from (
   select a.control as stock_number, sum(amount) as aftermarket
   from fin.fact_gl a
   join dds.dim_date aa on a.date_key = aa.date_key
   join (
     select inpmast_stock_number, inpmast_vin
     from arkona.xfm_inpmast
     where status = 'I'
       and current_row = true
       and type_n_u = 'N'
       and left(inventory_account, 1) = '1') b on a.control = b.inpmast_stock_number
   join fin.dim_account c on a.account_key = c.account_key
   join fin.dim_journal d on a.journal_key = d.journal_key
   join fin.dim_gl_Description e on a.gl_description_key = e.gl_description_key
   where d.journal_code = 'AFM' and post_Status = 'Y'
   group by a.control) a
   union
   select a.control as stock_number, amount as aftermarket
   from fin.fact_gl a
   join dds.dim_date aa on a.date_key = aa.date_key
   join (
     select inpmast_stock_number, inpmast_vin
     from arkona.xfm_inpmast
     where status = 'I'
       and current_row = true
       and type_n_u = 'N'
       and left(inventory_account, 1) = '1') b on a.control = b.inpmast_stock_number
   join fin.dim_account c on a.account_key = c.account_key
   join fin.dim_journal d on a.journal_key = d.journal_key
   join fin.dim_gl_Description e on a.gl_description_key = e.gl_description_key
   where d.journal_code = 'CRC'
     and e.description like '%ACCES%' and post_Status = 'Y') aa
group by stock_number
order by aftermarket desc

-- pending boards
select *
from board.pending_boards
where not is_deleted
  and not is_delivered