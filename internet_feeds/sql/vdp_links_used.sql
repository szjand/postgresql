﻿drop table if exists ads.vdp_vins;
CREATE TABLE ads.vdp_vins
(
  vi_id citext NOT NULL,
  vin citext not null,
  PRIMARY KEY (vin));

drop table if exists ads.vdp_vii;
CREATE TABLE ads.vdp_vii
(
  vii_id citext NOT NULL,
  stock_number citext not null,
  from_date date not null,
  thru_date date not null default '12/31/9999',
  vi_id citext not null,
  owning_location_id citext not null,
  PRIMARY KEY (stock_number));

drop table if exists ads.vdp_vii_statuses;
CREATE TABLE ads.vdp_vii_statuses
(
  vii_id citext NOT NULL,
  status citext not null,
  from_date date not null,
  thru_date date not null default '12/31/9999',
  PRIMARY KEY (vii_id, status, from_date));  

SELECT * from ads.vdp_vins	
select * from ads.vdp_vii
select * from ads.vdp_vii_statuses

select * from ops.ads_extract where task like 'vdp%';
delete from ops.ads_Extract where task like 'vdp%';

select website_url
from scrapes.most_recent_rydell_scrape a
inner join arkona.ext_inpmast b on a.vin = b.inpmast_vin 
where vin = '3GCUYGED6KG156590' -- _vin
and case when b.inpmast_company_number = 'RY1' then website_title = 'rydellcars'
when  b.inpmast_company_number = 'RY2' then website_title = 'gfhonda'
when b.inpmast_company_number = 'RY8' then website_title = 'rydelltoyota' end;

drop table if exists ifd.used_vdp_links;
create table ifd.used_vdp_links (
  stock_number citext not null,
  vin citext not null,
  vdp_link citext,
  primary key(vin));

create or replace function ifd.update_used_car_vdp_links()
returns void as
$BODY$
/*
select ifd.update_used_car_vdp_links();
*/
truncate ifd.used_vdp_links; 

insert into ifd.used_vdp_links(stock_number,vin,vdp_link)
select a.stock_number, b.vin, (select scrapes.get_vdp_by_vin(b.vin))
from ads.vdp_vii a
join ads.vdp_vins b on a.vi_id = b.vi_id
where a.thru_date > current_date
  and a.owning_location_id in ( -- grand forks locations
    select partyid2
    from ads.ext_party_relationships
    where typ = 'PartyRelationship_MarketLocations'
      and partyid1 = 'A4847DFC-E00E-42E7-89EE-CD4368445A82')
  and exists (
    select 1
    from ads.vdp_vii_statuses   
    where vii_id = a.vii_id
    and status = 'RawMaterials_RawMaterials'
    and thru_date > current_date) 
  and not exists (
    select 1
    from ads.vdp_vii_statuses   
    where vii_id = a.vii_id
    and status in ('RMFlagSB_SalesBuffer','RMFlagWSB_WholesaleBuffer','RMFlagPIT_PurchaseInTransit',
      'RMFlagTNA_TradeNotAvailable','RMFlagIP_InspectionPending','RMFlagWP_WalkPending')
    and thru_date > current_date);
$BODY$
language sql;    

delete from ifd.used_vdp_links;

select * from ifd.used_vdp_links;

select distinct length(vdp_link) from ifd.used_vdp_links;

-- in advantage dps 
CREATE TABLE vdp_links(
  stock_number cichar(20),
	vin cichar(17),
	vdp_link memo);
		