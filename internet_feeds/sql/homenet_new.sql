﻿SELECT 'Rydell GM Auto Center' as DEALERSHIPNAME, 
trim(inpmast_vin) as VIN, trim(inpmast_stock_number) as STOCK, 'NEW' as "NEW-USED", year as YEAR, make as MAKE, 
  model as MODEL, model_code as MODELNUMBER, replace(body_Style, '"', '') as BODY, '' as TRANSMISSION, '' as TRIM, 
  '' as DOORS, odometer as MILES, '' as CYLINDERS, '' as DISPLACEMENT, '' AS DRIVETRAIN, 
  '' as trimlevel, color as EXTCOLOR, color_code,
  trim as INTCOLOR, 
  --'' as INVOICE, 
  coalesce((-- ad invoice per t arrington request
    select f.num_field_value 
    from arkona.ext_inpoptf f 
    inner join arkona.ext_inpoptd d on f.company_number = d.company_number
      and f.vin_number = i.inpmast_vin
      and f.seq_number = d.seq_number
      and d.field_descrip = 'Flooring/Payoff'), 0) as "INVOICE", 
  list_price as MSRP,
  '' as BOOKVALUE, 
  coalesce((  
    select f.num_field_value
    from arkona.ext_inpoptf f 
    inner join arkona.ext_inpoptd d on f.company_number = d.company_number
      and f.vin_number = i.inpmast_vin 
      and f.seq_number = d.seq_number
      and d.field_descrip = 'Internet Price'), 0) as "SELLINGPRICE", -- opt field Internet Price
  '' as "DATE-IN-STOCK", '' as CERTIFIED, '' as DESCRIPTION, '' as OPTIONS
-- select count(*)
FROM arkona.xfm_INPMAST i
-- inner join (
--   select gtctl#, sum(gttamt)as net
--   from rydedata.glptrns
--   where trim(gtacct) in (-- Inventory accounts only
--     '123100', -- Chev
--     '123101', -- Cad
--     '123105', -- Buick
--     '123700', -- Chev Lt Trk
--     '123701', -- Cad Trk
--     '123705', -- Buick Trk
--     '123706', -- GMC Trk
--     '123707', -- MV-1
--     '223000', -- Honda Trk
--     '223100', -- Honda
--     '223105', -- Nissan
--     '223700', -- Nissan Trk
--     '323102', -- Buick
--     '323702', -- Buick Trk
--     '323703') -- GMC Trk) 
--   and trim(gtpost) <> 'V'  -- Ignore VOIDS
--   group by gtctl#
--   having sum(coalesce(gttamt,0)) > 1) ia on trim(i.imstk#) = trim(ia.gtctl#)
WHERE i.status = 'I'
and i.type_n_u = 'N'
  and i.current_row
  and i.make = 'gmc'

select a.vin, b.inpmast_stock_number, b.status, b.type_n_u, b.current_row, count(*)
from nrv.new_vehicle_pictures a
left join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
group by a.vin, b.inpmast_stock_number, b.status, b.type_n_u, b.current_row  
order by stock  

-- subset of 4 vins, 3 of which have pictures
SELECT 'Rydell GM Auto Center' as DEALERSHIPNAME, 
trim(inpmast_vin) as VIN, trim(inpmast_stock_number) as STOCK, 'NEW' as "NEW-USED", year as YEAR, make as MAKE, 
  model as MODEL, model_code as MODELNUMBER, body_Style as BODY, '' as TRANSMISSION, '' as TRIM, 
  '' as DOORS, odometer as MILES, '' as CYLINDERS, '' as DISPLACEMENT, '' AS DRIVETRAIN, 
  '' as trimlevel, color as EXTCOLOR, color_code,
  trim as INTCOLOR, 
  --'' as INVOICE, 
  coalesce((-- ad invoice per t arrington request
    select f.num_field_value 
    from arkona.ext_inpoptf f 
    inner join arkona.ext_inpoptd d on f.company_number = d.company_number
      and f.vin_number = i.inpmast_vin
      and f.seq_number = d.seq_number
      and d.field_descrip = 'Flooring/Payoff'), 0) as "INVOICE", 
  list_price as MSRP,
  '' as BOOKVALUE, 
  coalesce((  
    select f.num_field_value
    from arkona.ext_inpoptf f 
    inner join arkona.ext_inpoptd d on f.company_number = d.company_number
      and f.vin_number = i.inpmast_vin 
      and f.seq_number = d.seq_number
      and d.field_descrip = 'Internet Price'), 0) as "SELLINGPRICE", -- opt field Internet Price
  '' as "DATE-IN-STOCK", '' as CERTIFIED, '' as DESCRIPTION, '' as OPTIONS,
  (select string_agg(url, ',')
  from (
    select * 
    from nrv.new_vehicle_pictures 
    where vin = i.inpmast_vin 
      and thru_ts > now() order by sequence_number) a) 
-- select count(*)
FROM arkona.xfm_INPMAST i
where inpmast_vin in ('1GT42WEY2KF109184','3GTU2NEC5JG463079','W04GV8SX5J1068678','1GNSKHKC7JR405186')
  and i.current_row


-- faster
SELECT 'Rydell GM Auto Center' as DEALERSHIPNAME, 
trim(inpmast_vin) as VIN, trim(inpmast_stock_number) as STOCK, 'NEW' as "NEW-USED", year as YEAR, make as MAKE, 
  model as MODEL, model_code as MODELNUMBER, replace(body_Style, '"', '') as BODY, '' as TRANSMISSION, '' as TRIM, 
--   '' as DOORS, odometer as MILES, '' as CYLINDERS, '' as DISPLACEMENT, '' AS DRIVETRAIN, 
--   '' as trimlevel, color as EXTCOLOR, color_code,
--   trim as INTCOLOR,
--   b.num_field_value as internet_price,
--   e.num_field_value as invoice,
--   (select string_agg(url, ',')
--   from (
--     select * 
--     from nrv.new_vehicle_pictures 
--     where vin = a.inpmast_vin 
--       and thru_ts > now() order by sequence_number) z),
  g.*, h.*
FROM arkona.ext_INPMAST a
left join arkona.ext_inpoptf b on a.inpmast_vin = b.vin_number
join arkona.ext_inpoptd c on b.company_number = c.company_number
  and b.seq_number = c.seq_number
  and c.field_descrip = 'Internet Price'
left join arkona.ext_inpoptf e on a.inpmast_vin = e.vin_number
join arkona.ext_inpoptd f on e.company_number = f.company_number
  and e.seq_number = f.seq_number
  and f.field_descrip = 'Flooring/Payoff' 
left join (
    select d.vin, min(b.the_date) as the_Date
    from ads.ext_fact_repair_order a
    join dds.dim_date b on a.opendatekey = b.date_key
    join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
--       and vin  in ('1GT42WEY2KF109184','3GTU2NEC5JG463079','W04GV8SX5J1068678','1GNSKHKC7JR405186')
    join arkona.ext_inpmast e on d.vin = e.inpmast_vin
      and e.status = 'I'
      and e.type_n_u = 'N'
    group by d.vin) g on a.inpmast_vin = g.vin  
left join keys.ext_keyper h on a.inpmast_stock_number = h.stock_number     
where a.status = 'I'
  and a.type_n_u = 'N'
  and model = 'equinox'


    select d.vin, min(b.the_date) as the_Date
    from ads.ext_fact_repair_order a
    join dds.dim_date b on a.opendatekey = b.date_key
    join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
--       and vin  in ('1GT42WEY2KF109184','3GTU2NEC5JG463079','W04GV8SX5J1068678','1GNSKHKC7JR405186')
    join arkona.ext_inpmast e on d.vin = e.inpmast_vin
      and e.status = 'I'
      and e.type_n_u = 'N'
    group by d.vin


          
  left join ads.keyper_creation_dates cc on aa.stock_number = cc.stock_number  
  where aa.source = 'dealer trade'




check G35312 for ro type - yep as i thought, transport


G35401 keyper 11/2  no ro

select * from gmgl.vehicle_orders where vin = '2GNAXSEV9K6119800'
select * from gmgl.vehicle_order_events where order_number = 'WFNWGM'

select * from gmgl.vehicle_event_codes


start with a table of stocknumbers/vins of current inventory


-- faster
drop table if exists inventory;
create temp table inventory as
SELECT 'Rydell GM Auto Center' as DEALERSHIPNAME, 
trim(inpmast_vin) as VIN, trim(inpmast_stock_number) as STOCK, 'NEW' as "NEW-USED", year as YEAR, make as MAKE, 
  model as MODEL, model_code as MODELNUMBER, replace(body_Style, '"', '') as BODY, '' as TRANSMISSION, '' as TRIM,
  '' as DOORS, odometer as MILES, '' as CYLINDERS, '' as DISPLACEMENT, '' AS DRIVETRAIN, 
  '' as trimlevel, color as EXTCOLOR, color_code,
  trim as INTCOLOR,
  b.num_field_value as internet_price,
  e.num_field_value as invoice,
  (select string_agg(url, ',')
  from (
    select * 
    from nrv.new_vehicle_pictures 
    where vin = a.inpmast_vin 
      and thru_ts > now() order by sequence_number) z) as "photoURLs"
FROM arkona.ext_INPMAST a
left join arkona.ext_inpoptf b on a.inpmast_vin = b.vin_number
join arkona.ext_inpoptd c on b.company_number = c.company_number
  and b.seq_number = c.seq_number
  and c.field_descrip = 'Internet Price'
left join arkona.ext_inpoptf e on a.inpmast_vin = e.vin_number
join arkona.ext_inpoptd f on e.company_number = f.company_number
  and e.seq_number = f.seq_number
  and f.field_descrip = 'Flooring/Payoff'  
where a.status = 'I'
  and a.type_n_u = 'N';

select string_agg(column_name, '|')
from information_schema.columns
where table_name = 'inventory'

dealershipname|vin|stock|NEW-USED|year|make|model|modelnumber|body|transmission|trim|doors|miles|cylinders|displacement|drivetrain|trimlevel|extcolor|color_code|intcolor|internet_price|invoice|photoURLs


create schema ifd;
comment on schema ifd is 'tables for generating internet feeds';

create table ifd.homenet_new (
  dealershipname citext,
  vin citext primary key,
  stock citext,
  new_used citext,
  year integer,
  make citext,
  model citext,
  modelnumber citext,
  body citext,
  transmission citext,
  trim citext,
  doors citext,
  miles citext,
  cylinders citext,
  displacement citext,
  drivetrain citext,
  extcolor citext,
  intcolor citext,
  invoice numeric(8,2),
  msrp numeric(8,2),
  bookvalue citext,
  sellingprice numeric(8,2),
  date_in_stock citext,
  certified citext,
  description citext,
  options citext,
  color_code citext,
  photourls citext,
  intransit boolean);
comment on table ifd.homenet_new is 'new vehicle feed for homenet,  alias fields new_used,date_in_stock & color code with a dash instead of underscore';  

truncate ifd.homenet_new;
insert into ifd.homenet_new (dealershipname,vin,stock,new_used,year,make,model,
  modelnumber,body,extcolor,intcolor,invoice,msrp,sellingprice,color_code,photourls,intransit)
SELECT 'Rydell GM Auto Center', inpmast_vin, inpmast_stock_number, 
  'NEW', year, make, model, model_code, replace(body_Style, '"', ''), 
  color as EXTCOLOR, 
  trim as INTCOLOR,
  e.num_field_value, --invoice
  list_price, -- msrp
  b.num_field_value, -- internet price
  color_code,
  (select string_agg(url, ',')
  from (
    select * 
    from nrv.new_vehicle_pictures 
    where vin = a.inpmast_vin 
      and thru_ts > now() order by sequence_number) z) as "photoURLs",
  false
FROM arkona.ext_INPMAST a
left join arkona.ext_inpoptf b on a.inpmast_vin = b.vin_number
join arkona.ext_inpoptd c on b.company_number = c.company_number
  and b.seq_number = c.seq_number
  and c.field_descrip = 'Internet Price'
left join arkona.ext_inpoptf e on a.inpmast_vin = e.vin_number
join arkona.ext_inpoptd f on e.company_number = f.company_number
  and e.seq_number = f.seq_number
  and f.field_descrip = 'Flooring/Payoff'  
where a.status = 'I'
  and a.type_n_u = 'N';  

select *
from ifd.homenet_new  

select replace(string_agg(upper(column_name), '|'), '_','-')
from information_schema.columns
where table_schema = 'ifd'
  and table_name = 'homenet_new'

DEALERSHIPNAME|VIN|STOCK|NEW-USED|YEAR|MAKE|MODEL|MODELNUMBER|BODY|TRANSMISSION|TRIM|DOORS|MILES|CYLINDERS|DISPLACEMENT|DRIVETRAIN|EXTCOLOR|INTCOLOR|INVOICE|MSRP|BOOKVALUE|SELLINGPRICE|DATE-IN-STOCK|CERTIFIED|DESCRIPTION|OPTIONS|COLOR-CODE|PHOTOURLS|INTRANSIT


-- in transit
update ifd.homenet_new
set intransit = true
where stock in (
  select a.stock --, a.vin, b.ro_date, d.keyper_date
  from ifd.homenet_new a
  left join (
    select d.vin, min(b.the_date) as ro_date
    from ads.ext_fact_repair_order a
    join dds.dim_date b on a.opendatekey = b.date_key
    join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
    join ifd.homenet_new e on d.vin = e.vin
    group by d.vin) b on a.vin = b.vin
  left join (
    select c.stock_number, c.keyper_date
    from (
      select a.stock_number, transaction_date::date as keyper_date, 
        row_number() over (partition by a.stock_number order by a.transaction_date desc)
      from keys.ext_keyper a
      join ifd.homenet_new b on a.stock_number = b.stock) c
    where row_number = 1) d on a.stock = d.stock_number
  where ro_date is null
    and keyper_date is null) 



-- 8/24/19
somewhere in there, price_coming_soon was added as an attribute to ifd.homenet_new
now need to add price_before_incentives



select *
from ifd.ext_new_data

-- this is the query from the luigi module
SELECT 'Rydell GM Auto Center', aa.vin, aa.stock_number, 
  'NEW', aa.model_year, aa.make, aa.model, aa.model_code, 
  replace(aa.body_Style, '"', ''), aa.odometer,
  aa.ext_color as EXTCOLOR, aa.int_color as INTCOLOR,
  aa.invoice, aa.msrp, aa.internet_price, aa.color_code,
  (select string_agg(url, ',')
  from (
    select * 
    from nrv.new_vehicle_pictures 
    where vin = aa.vin 
      and thru_ts > now() order by sequence_number) z) as "photoURLs",
  case when bb.vin is null then 'intransit' else 'onlot' end,
  case when aa.internet_price = 0 then true else false end
FROM ifd.ext_new_data  aa
left join nc.vehicle_acquisitions bb on aa.vin = bb.vin 
  and bb.thru_date > current_date
where aa.vin not in ( --exclude list, pending board, sold orders, fleet orders
  select vin
  from gmgl.daily_inventory
  where is_fleet = true
    or is_sold = true
    or stock_number in (
      select stock_number
      from board.pending_boards
      where is_delivered = false
      and is_deleted = false));


select * 
FROM ifd.ext_new_data  aa




