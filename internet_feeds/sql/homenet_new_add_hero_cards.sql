﻿-- 08/25/20
-- no pix
3GCUYDET6LG395595
3GNCJNSB8LL319537
1G1ZD5ST0LF149700

-- have pix
3N1CN8DV5LL800810
5N1AT2MV4LC765417
1N4AL4CVXLN313242

drop table if exists vins;
create temp table vins as 
select '3GCUYDET6LG395595' as vin
union
select '3GNCJNSB8LL319537'
union
select '1G1ZD5ST0LF149700'
union
select '3N1CN8DV5LL800810'
union
select '5N1AT2MV4LC765417'
union
select '1N4AL4CVXLN313242'

-- current subquery
(select string_agg(url, ',')
from (
  select * 
  from nrv.new_vehicle_pictures 
  where vin = aa.vin 
    and thru_ts > now() order by sequence_number) z) as "photoURLs",

-- aftons query
select *
from nrv.new_vehicle_hero_cards
union
select url, sequence_number
from nrv.new_vehicle_pictures
where vin = '2GNAXJEV5L6106845'
  and thru_ts > now() 
order by sequence_number

select null as vin, url, sequence_number
from nrv.new_vehicle_hero_cards
union 
select vin, url, sequence_number
from nrv.new_vehicle_pictures
where vin = '2GNAXJEV5L6106845'
  and thru_ts > now() 
order by sequence_number



-- ok this is what i want, nulls as photourls where there are no pictures
select a.vin,
  (select string_agg(url, ',')
  from (
    select * 
    from nrv.new_vehicle_pictures 
    where vin = a.vin 
      and thru_ts > now() order by sequence_number) z) as "photoURLs"
from ifd.ext_new_data a 
join vins b on a.vin = b.vin

-- this appears to work, if there are no pictures i want a null response
select string_agg(url, ',' order by sequence_number)
from (
  select null as vin, url, sequence_number
  from nrv.new_vehicle_hero_cards
  union 
  select vin, url, sequence_number
  from nrv.new_vehicle_pictures
  where vin in ('2GNAXJEV5L6106845', '3GCUYDET6LG395595')
    and thru_ts > now() 
  order by sequence_number) z

select distinct a.vin from nrv.new_vehicle_pictures a join vins b on a.vin = b.vin

-- ok this is what i want, nulls as photourls where there are no pictures
-- add the union to it
-- and what i get is rows for vins with no pictures and a string_agg of just hero pix
select a.vin, 
  (select string_agg(url, ',')
  from (
    select url, sequence_number
    from nrv.new_vehicle_hero_cards
    union
    select url, sequence_number
    from nrv.new_vehicle_pictures 
    where vin = a.vin 
      and thru_ts > now() order by sequence_number) z) as "photoURLs"
from ifd.ext_new_data a 
join vins b on a.vin = b.vin

only want hero cards for vins that have pictures
the problem is the union gives me hero cards whether there is a picture or not
-- no pix
3GCUYDET6LG395595
3GNCJNSB8LL319537
1G1ZD5ST0LF149700

-- have pix
3N1CN8DV5LL800810
5N1AT2MV4LC765417
1N4AL4CVXLN313242

-- ok this is what i want, nulls as photourls where there are no pictures
select a.vin,
  (select string_agg(url, ',')
  from (
    select * 
    from nrv.new_vehicle_pictures 
    where vin = a.vin 
      and thru_ts > now() order by sequence_number) z) as "photoURLs"
from ifd.ext_new_data a 
join vins b on a.vin = b.vin

-- ok this is what i want, nulls as photourls where there are no pictures
-- so lets try a case that only generates urls if there is pictures
-- finally, this appears to be it
select a.vin,
  case 
    when exists (select 1 from nrv.new_vehicle_pictures where vin = a.vin) then
      (select string_agg(url, ',')
      from (
        select *
        from nrv.new_vehicle_hero_cards
        union       
        select url, sequence_number
        from nrv.new_vehicle_pictures 
        where vin = a.vin 
          and thru_ts > now() order by sequence_number) z) 
    else null
  end as "photoURLs"
from ifd.ext_new_data a 
join vins b on a.vin = b.vin
----------------------------------------------------------------------------------------------

-- current homenet query
                    SELECT 'Rydell GM Auto Center', aa.vin, aa.stock_number, 
--                       'NEW', aa.model_year, aa.make, aa.model, aa.model_code, 
--                       replace(aa.body_Style, '"', ''), aa.odometer,
--                       aa.ext_color as EXTCOLOR, aa.int_color as INTCOLOR,
--                       aa.invoice, aa.msrp, aa.internet_price, aa.color_code,
                      (select string_agg(url, ',')
                      from (
                        select * 
                        from nrv.new_vehicle_pictures 
                        where vin = aa.vin 
                          and thru_ts > now() order by sequence_number) z) as "photoURLs",
                      case when bb.vin is null then 'intransit' else 'onlot' end,
                      case when aa.internet_price = 0 then true else false end,
                      -- (aa.internet_price + coalesce(cc.total_cash, 0))::integer as price_before_incentives
                      case
                        when aa.internet_price = 0 then aa.msrp::integer
                        else (aa.internet_price + coalesce(cc.total_cash, 0))::integer 
                      end as price_before_incentives                      
                    FROM ifd.ext_new_data  aa
                    left join nc.vehicle_acquisitions bb on aa.vin = bb.vin 
                      and bb.thru_date > current_date
                    left join gmgl.vin_incentive_cash cc on aa.vin = cc.vin
                      and cc.thru_ts > now()
                      and cc.is_best_incentive
                    where aa.vin not in ( --exclude list, pending board, sold orders, fleet orders
                      select vin
                      from gmgl.daily_inventory
                      where is_fleet = true
                        or is_sold = true
                        or stock_number in (
                          select stock_number
                          from board.pending_boards
                          where is_delivered = false
                            and is_deleted = false));





                    SELECT 'Rydell GM Auto Center', aa.vin, aa.stock_number, 
                      'NEW', aa.model_year, aa.make, aa.model, aa.model_code, 
                      replace(aa.body_Style, '"', ''), aa.odometer,
                      aa.ext_color as EXTCOLOR, aa.int_color as INTCOLOR,
                      aa.invoice, aa.msrp, aa.internet_price, aa.color_code,
                      case 
                        when exists (select 1 from nrv.new_vehicle_pictures where vin = aa.vin) then
                          (select string_agg(url, ',')
                          from (
                            select *
                            from nrv.new_vehicle_hero_cards
                            union       
                            select url, sequence_number
                            from nrv.new_vehicle_pictures 
                            where vin = aa.vin 
                              and thru_ts > now() order by sequence_number) z) 
                        else null
                        end as "photoURLs",
                        case when bb.vin is null then 'intransit' else 'onlot' end,
                        case when aa.internet_price = 0 then true else false end,
                        case
                          when aa.internet_price = 0 then aa.msrp::integer
                          else (aa.internet_price + coalesce(cc.total_cash, 0))::integer 
                        end as price_before_incentives                      
                    FROM ifd.ext_new_data  aa
                    left join nc.vehicle_acquisitions bb on aa.vin = bb.vin 
                      and bb.thru_date > current_date
                    left join gmgl.vin_incentive_cash cc on aa.vin = cc.vin
                      and cc.thru_ts > now()
                      and cc.is_best_incentive
                    where aa.vin not in ( --exclude list, pending board, sold orders, fleet orders
                      select vin
                      from gmgl.daily_inventory
                      where is_fleet = true
                        or is_sold = true
                        or stock_number in (
                          select stock_number
                          from board.pending_boards
                          where is_delivered = false
                            and is_deleted = false));                            