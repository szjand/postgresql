﻿-- -- original query from afton
-- select 'Rydell GM Auto Center', vin, stock_number, 'NEW', model_year, make, model, model_code, trim_level, odometer, best_price
-- from (
-- 	select a.vin, a.stock_number, a.model_year, make, model, model_code, trim_level, odometer, 
-- 	case
-- 	  when retirement_status = 'Early Retirement'
-- 	  then early_retirement_before_incentives
-- 	  when retirement_status = 'Full Retirement' 
-- 	  then full_retirement_before_incentives
-- 	  else forfeited_before_incentives end as best_price,
-- 	case 
-- 		when a.has_recall = true then 'Recall' 
-- 		when hh.vin is not null then 'On Sold Board' 
-- 		when ff.stock_number is not null then 'On Pending Board' 
-- 		else 'CTP ' || a.ctp_type 
-- 	end as status_description
-- 	from ontrac.get_current_ctp_pricing() a
-- 	 left join board.sales_board hh on a.vin = hh.vin and a.stock_number = hh.stock_number
-- 	      and hh.is_deleted = false 
-- 	      and is_backed_on = false
-- 	      and board_type_key not in (19,11, 12)
-- 	      and hh.stock_number not in (
-- 		select stock_number
-- 		from board.sales_board
-- 		where board_type_key in (19,11,12))
-- 	    left join board.board_types hhh on hh.board_type_key = hhh.board_type_key
-- 	    left join board.pending_boards ff on a.stock_number = ff.stock_number
-- 	      and ff.is_deleted = false
-- 	      and is_delivered = false
-- ) A
-- where status_description like 'CTP%'
-- 
-- -- and the modified 2 price query from afton
-- select vin, stock_number, model_year, make, model, trim_level, odometer, best_price_before_incentives, best_price
-- from (
-- 	select a.vin, a.stock_number, a.model_year, make, model, trim_level, odometer, 
-- 	case
-- 	  when retirement_status = 'Early Retirement'
-- 	  then early_retirement_before_incentives
-- 	  when retirement_status = 'Full Retirement' 
-- 	  then full_retirement_before_incentives
-- 	  else forfeited_before_incentives end as best_price_before_incentives,
-- 	case
-- 	  when retirement_status = 'Early Retirement'
-- 	  then early_retirement
-- 	  when retirement_status = 'Full Retirement' 
-- 	  then full_retirement
-- 	  else forfeited end as best_price,
-- 	case when a.has_recall = true then 'Recall' when hh.vin is not null then 'On Sold Board' when ff.stock_number is not null then 'On Pending Board' else 'CTP ' || a.ctp_type end as status_description
-- 	from ontrac.get_current_ctp_pricing() a
-- 	 left join board.sales_board hh on a.vin = hh.vin and a.stock_number = hh.stock_number
-- 	      and hh.is_deleted = false 
-- 	      and is_backed_on = false
-- 	      and board_type_key not in (19,11, 12)
-- 	      and hh.stock_number not in (
-- 		select stock_number
-- 		from board.sales_board
-- 		where board_type_key in (19,11,12))
-- 	    left join board.board_types hhh on hh.board_type_key = hhh.board_type_key
-- 	    left join board.pending_boards ff on a.stock_number = ff.stock_number
-- 	      and ff.is_deleted = false
-- 	      and is_delivered = false
-- ) A
-- where status_description like 'CTP%'
/*
Selling price should be the 'conditional best price' and price before incentives should be the other

Mike Yem
*/
-- believe this will be the query i union in the python file
-- or maybe just put them both in a function
-- yep FUNCTION ifd.update_homenet_new_vehicle_feed()

07/02
current state of affairs,
this query as is returns 2 rows for  3GCUYDET8MG275458 which is bad
it has been taken out of ctp, so it should not be returning a row
so the plan is to exempt vins from the ctp portion of the feed that exist in the regular feed

the regular inventory part of the feed is updated hourly and is derived from inpmast
truncate ifd.homenet_new;
insert into ifd.homenet_new
select * from ifd.homenet_new_regular_inventory
union
select * from ifd.homenet_new_ctp_inventory;
select * from ifd.homenet_new;



drop table if exists test;
create temp table test as

-- the regular feed
-- which is based on ifd.ext_new_data, which is an estract of the needed fields from inpmast, inpoptf, inpoptd
insert into ifd.homenet_new_regular_inventory (dealershipname,vin,stock,new_used,year,make,model,
		modelnumber,body,miles,extcolor,intcolor,invoice,msrp,sellingprice,color_code,
		photourls,intransit,price_coming_soon,price_before_incentives)
SELECT 'Rydell GM Auto Center' as store, aa.vin, aa.stock_number, 
		'NEW', aa.model_year, aa.make, aa.model, aa.model_code, 
		replace(aa.body_Style, '"', ''), aa.odometer,
		aa.ext_color as EXTCOLOR, aa.int_color as INTCOLOR,
		aa.invoice, aa.msrp, aa.internet_price, aa.color_code,
		case 
				when exists (select 1 from nrv.new_vehicle_pictures where vin = aa.vin) then
						case
								when aa.make = 'cadillac' then 
										(select string_agg(url, ',')
										from (   
												select url, sequence_number
												from nrv.new_vehicle_pictures 
												where vin = aa.vin 
														and thru_ts > now() order by sequence_number) y) 	
								else													
										(select string_agg(url, ',')
										from (
												select *
												from nrv.new_vehicle_hero_cards
												union       
												select url, sequence_number
												from nrv.new_vehicle_pictures 
												where vin = aa.vin 
														and thru_ts > now() order by sequence_number) z) 
								end
				else null
		end as "photoURLs",
		case when bb.vin is null then 'intransit' else 'onlot' end as intransit,
		case when aa.internet_price = 0 then true else false end as price_coming_coon,
		case
				when aa.make = 'cadillac' then aa.internet_price::integer
				when aa.internet_price = 0 then aa.msrp::integer
				when dd.has_emp_inc then dd.testing_price
				else (aa.internet_price + coalesce(cc.total_cash, 0))::integer 
		end as price_before_incentives  
FROM ifd.ext_new_data  aa
left join nc.vehicle_acquisitions bb on aa.vin = bb.vin 
		and bb.thru_date > current_date
left join gmgl.vin_incentive_cash cc on aa.vin = cc.vin
		and cc.thru_ts > now()
		and cc.is_best_incentive
left join gmgl.get_vehicle_prices() dd on aa.stock_number = dd.stock_number
		and aa.vin = dd.vin
		and dd.has_emp_inc
where aa.vin not in ( --exclude list, pending board, sold orders, fleet orders
		select vin
		from gmgl.daily_inventory
		where is_fleet = true
				or is_sold = true
				or stock_number in (
						select stock_number
						from board.pending_boards
						where is_delivered = false
								and is_deleted = false));   
-- ctp
insert into ifd.homenet_new_ctp_inventory (dealershipname,vin,stock,new_used,year,make,model,
		modelnumber,body,miles,extcolor,intcolor,invoice,msrp,sellingprice,color_code,
		photourls,intransit,price_coming_soon,price_before_incentives)                                  
select 'Rydell GM Auto Center' as store, a.vin, a.stock_number, 'NEW', a.model_year, a.make, a.model, 
	a.model_code, a.trim_level, a.odometer, 
	f.color, null as intcolor, null as invoice, a.total_msrp as msrp, 
		case
			when retirement_status = 'Early Retirement' then early_retirement
			when retirement_status = 'Full Retirement'  then full_retirement
			else forfeited 
	  end as best_price, 
	  g.color_code, 
	case 
		when exists (select 1 from nrv.new_vehicle_pictures where vin = a.vin) then
			case
				when a.make = 'cadillac' then (
					select string_agg(url, ',')
					from (   
						select url, sequence_number
						from nrv.new_vehicle_pictures 
						where vin = a.vin 
								and thru_ts > now() order by sequence_number) y) 	
				else (												
					select string_agg(url, ',')
					from (
						select *
						from nrv.new_vehicle_hero_cards
						union       
						select url, sequence_number
						from nrv.new_vehicle_pictures 
						where vin = a.vin 
								and thru_ts > now() order by sequence_number) z) 
				end
		else null
	end as "photoURLs",	
	'onlot' as intransit, false as price_coming_soon, 
	case
		when a.retirement_status = 'Early Retirement' then a.early_retirement_before_incentives
		when a.retirement_status = 'Full Retirement' then full_retirement_before_incentives
		else a.forfeited_before_incentives 
	end as price_before_incentives
from ontrac.get_current_ctp_pricing() a
left join board.sales_board b on a.vin = b.vin and a.stock_number = b.stock_number
	and b.is_deleted = false 
	and b.is_backed_on = false
	and b.board_type_key not in (19,11, 12)
	and b.stock_number not in (
		select stock_number
		from board.sales_board
		where board_type_key in (19,11,12))
left join board.board_types c on b.board_type_key = c.board_type_key
left join board.pending_boards e on a.stock_number = e.stock_number
	and e.is_deleted = false
	and e.is_delivered = false
left join nc.vehicles f on a.vin = f.vin	
left join jon.exterior_colors g on f.chrome_style_id = g.chrome_style_id
  and f.color = g.color
where not a.has_recall
  and b.vin is null -- sold board
  and e.stock_number is null -- pending board  
  and not exists (
    select 1
    from ifd.homenet_new_regular_inventory
    where vin = a.vin);


