﻿/*

9/11/19 
I am following up with the CDK issue of displaying vehicles online for very very very low pricing. 
After speaking with CDK there is no way for us to display "Price Coming Soon" as they cannot 
create rules on their end like DealerOn, so are we able to default the MSRP to the Rydell Best Price 
in vision IF there is NO MARGIN set. This series would make the price before rebates the MSRP instead of $0, 
which in turn would send the to CDK and we would have an over priced vehicle vs a $2700 2020 Silverado.

Please let me know if this is feasible, 
Mike Yem

*/

reiterating several times with yem:
if the margin is 0 send msrp as price_before_incentives

well, the margin is nowhere in the feed but, the price_before_incentives = internet_price + total_cash

so, one of the reasons internet price could be 0 is no margin, so, 
if the internet price is 0, then send msrp
case
  when aa.internet_price = 0 then aa.msrp
  else (aa.internet_price + coalesce(cc.total_cash, 0))::integer 
end as price_before_incentives

                    SELECT 'Rydell GM Auto Center', aa.vin, aa.stock_number, 
                      'NEW', aa.model_year, aa.make, aa.model, aa.model_code, 
                      replace(aa.body_Style, '"', ''), aa.odometer,
                      aa.ext_color as EXTCOLOR, aa.int_color as INTCOLOR,
                      aa.invoice, aa.msrp, aa.internet_price, aa.color_code,
--                       (select string_agg(url, ',')
--                       from (
--                         select * 
--                         from nrv.new_vehicle_pictures 
--                         where vin = aa.vin 
--                           and thru_ts > now() order by sequence_number) z) as "photoURLs",
                      case when bb.vin is null then 'intransit' else 'onlot' end,
                      case when aa.internet_price = 0 then true else false end,
--                       (aa.internet_price + coalesce(cc.total_cash, 0))::integer as price_before_incentives

                      case
                        when aa.internet_price = 0 then aa.msrp::integer
                        else (aa.internet_price + coalesce(cc.total_cash, 0))::integer 
                      end as price_before_incentives

                    FROM ifd.ext_new_data  aa
                    left join nc.vehicle_acquisitions bb on aa.vin = bb.vin 
                      and bb.thru_date > current_date
                    left join gmgl.vin_incentive_cash cc on aa.vin = cc.vin
                      and cc.thru_ts > now()
                      and cc.is_best_incentive
                    where aa.vin not in ( --exclude list, pending board, sold orders, fleet orders
                      select vin
                      from gmgl.daily_inventory
                      where is_fleet = true
                        or is_sold = true
                        or stock_number in (
                          select stock_number
                          from board.pending_boards
                          where is_delivered = false
                            and is_deleted = false)) 
and aa.internet_price = 0                            

                            

                            
