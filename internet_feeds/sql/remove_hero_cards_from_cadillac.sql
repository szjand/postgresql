﻿SELECT 'Rydell GM Auto Center', aa.vin, aa.stock_number, 
	'NEW', aa.model_year, aa.make, aa.model, aa.model_code, 
	replace(aa.body_Style, '"', ''), aa.odometer,
	aa.ext_color as EXTCOLOR, aa.int_color as INTCOLOR,
	aa.invoice, aa.msrp, aa.internet_price, aa.color_code,
	case 
		--  when there are pictures for the vin
		when exists (select 1 from nrv.new_vehicle_pictures where vin = aa.vin) then
			case
				when aa.make = 'cadillac' then 
					(select string_agg(url, ',')
					from (   
						select url, sequence_number
						from nrv.new_vehicle_pictures 
						where vin = aa.vin 
							and thru_ts > now() order by sequence_number) cad) 	
				else													
					(select string_agg(url, ',')
					from (
						select *
						from nrv.new_vehicle_hero_cards
						union       
						select url, sequence_number
						from nrv.new_vehicle_pictures 
						where vin = aa.vin 
							and thru_ts > now() order by sequence_number) z) 
				end
		else null
	end as "photoURLs",
	case when bb.vin is null then 'intransit' else 'onlot' end,
	case when aa.internet_price = 0 then true else false end,
	case
		when aa.make = 'cadillac' then aa.internet_price::integer
		when aa.internet_price = 0 then aa.msrp::integer
		when dd.has_emp_inc then dd.testing_price
		else (aa.internet_price + coalesce(cc.total_cash, 0))::integer 
	end as price_before_incentives  
FROM ifd.ext_new_data  aa
left join nc.vehicle_acquisitions bb on aa.vin = bb.vin 
	and bb.thru_date > current_date
left join gmgl.vin_incentive_cash cc on aa.vin = cc.vin
	and cc.thru_ts > now()
	and cc.is_best_incentive
left join gmgl.get_vehicle_prices() dd on aa.stock_number = dd.stock_number
	and aa.vin = dd.vin
	and dd.has_emp_inc
where aa.vin not in ( --exclude list, pending board, sold orders, fleet orders
	select vin
	from gmgl.daily_inventory
	where is_fleet = true
		or is_sold = true
		or stock_number in (
			select stock_number
			from board.pending_boards
			where is_delivered = false
				and is_deleted = false))
and aa.make = 'cadillac'                            




https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_1.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_2.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_3.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_4.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_5.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_6.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_7.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_8.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_9.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_10.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_11.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_12.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_13.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_14.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_15.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_16.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_17.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_18.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_19.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_20.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_21.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_22.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_23.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_24.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_25.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_26.jpg,https://beta.rydellvision.com:8888/vision/new-vehicle-picture/1GYKPDRS2MZ185311_27.jpg