﻿-- -- 8/25/19
-- -- while this looked promising yesterday it is fucked up in so many ways
-- -- fundamentally does not account for is_best_incentive
-- 
-- with
--   majors as (
--     select aa.vin, -- array_agg(has_sup_inc),
--       coalesce((select true = any(array_agg(has_sup_inc))), false) as has_maj_supinc,
--       coalesce((select true = any(array_agg(has_emp_inc))), false) as has_maj_empinc,
--       sum(program_dollar_amount) as maj_amount
--     from ifd.homenet_new   aa
--     left join (
--         select a.vin, a.program_dollar_amount, has_sup_inc, has_emp_inc
--         from gmgl.major_incentives a
--         inner join gmgl.incentive_programs b on a.program_key  = b.program_key
--         inner join gmgl.incentive_program_rules c on b.program_key = c.program_key 
--           and current_row = true
--         where a.lookup_date = current_date
--           and a.is_best_incentive) A on aa.vin = a.vin
--     where aa.stock not like 'H%'  
--     group by aa.vin), 
-- 
--   stackables as (
-- -- this looks promising for stackables
--     select gg.vin, 
--       -- true if any program for a vin has supinc/empinc
--       coalesce((select true = any(array_agg(has_sup_inc))), false) as has_sta_supinc,
--       coalesce((select true = any(array_agg(has_emp_inc))), false) as has_sta_empinc,
--       sum(program_dollar_amount) as sta_amount
--     from ifd.homenet_new gg
--     join (
--         select a.vin, a.major_incentive_key
--         from gmgl.major_incentives a
--         inner join gmgl.incentive_programs b on a.program_key  = b.program_key
--         inner join gmgl.incentive_program_rules c on b.program_key = c.program_key and current_row = true
--         where lookup_date = current_date) A on gg.vin = a.vin
--     join (
--         select major_incentive_key, program_dollar_amount, has_sup_inc, has_emp_inc
--         from gmgl.stackable_incentives c 
--         inner join gmgl.incentive_programs d on c.program_key = d.program_key
--         inner join gmgl.incentive_program_rules e on d.program_key = e.program_key 
--           and current_row = true
--         where lookup_date = current_date) c on a.major_incentive_key = c.major_incentive_key
--         inner join gmgl.vehicle_invoices d on gg.vin = d.vin and thru_date > now()
--         where gg.vin is not null
--     group by gg.vin),
--   cadillac as (
--     select case when math_operator = '*' then (the_value * second_value::numeric)::integer end as custom_incentive, vin
--     from (
--     select d.rule_name,
--       case
--         when first_value_type = 'MSRP' 
--       then total_msrp 
--       end as the_value , math_operator, second_value, a.vin
--     from ifd.homenet_new a
--     inner join gmgl.custom_incentive_rule_distributions c on a.make = c.make and c.model_year = a.year
--     inner join gmgl.custom_incentive_rules d on d.rule_key = c.rule_key
--     inner join gmgl.vehicle_invoices e on a.vin = e.vin and e.thru_date > now()) x)       
-- 
-- -- looking good, need supinc/empinc from invoices, cadillac
-- -- remember, trying to produce price before incentives, so: best price + total incentives
-- select a.vin, b.maj_amount, b.has_maj_supinc, has_maj_empinc, 
--   coalesce(c.sta_amount, 0) as sta_amount, coalesce(c.has_sta_supinc, false) as has_sta_supinc, coalesce(has_sta_empinc, false) as has_sta_empinc, 
--   coalesce(d.custom_incentive, 0) as custom_incentive,
--   e.supinc, e.empinc,
--   has_maj_supinc != has_sta_supinc
-- from ifd.homenet_new a
-- left join majors b on a.vin = b.vin
-- left join stackables c on a.vin = c.vin
-- left join cadillac d on a.vin = d.vin
-- left join gmgl.vehicle_invoices e on a.vin = e.vin
-- where a.stock not like 'H%'  


8/25/19 going with vin_incentive_cash, close enough, cadillac is fucked up though

alter table ifd.homenet_new
add column price_before_incentives integer;

select a.vin, a.stock, a.make, a.msrp, a.sellingprice, b.total_cash, 
  (a.sellingprice + coalesce(b.total_cash, 0))::integer as price_before_incentives
-- select b.*
from ifd.homenet_new a
left join gmgl.vin_incentive_cash b on a.vin = b.vin
  and b.thru_ts > now()
  and is_best_incentive

-- now this ^ needs to be added to the query in homenet_new.py 

                    SELECT 'Rydell GM Auto Center', aa.vin, aa.stock_number, 
                      'NEW', aa.model_year, aa.make, aa.model, aa.model_code, 
                      replace(aa.body_Style, '"', ''), aa.odometer,
                      aa.ext_color as EXTCOLOR, aa.int_color as INTCOLOR,
                      aa.invoice, aa.msrp, aa.internet_price, aa.color_code,
                      (select string_agg(url, ',')
                      from (
                        select * 
                        from nrv.new_vehicle_pictures 
                        where vin = aa.vin 
                          and thru_ts > now() order by sequence_number) z) as "photoURLs",
                      case when bb.vin is null then 'intransit' else 'onlot' end,
                      case when aa.internet_price = 0 then true else false end,
                      (aa.internet_price + coalesce(cc.total_cash, 0))::integer as price_before_incentives
                    FROM ifd.ext_new_data  aa
                    left join nc.vehicle_acquisitions bb on aa.vin = bb.vin 
                      and bb.thru_date > current_date
                    left join gmgl.vin_incentive_cash cc on aa.vin = cc.vin
                      and cc.thru_ts > now()
                      and cc.is_best_incentive
                    where aa.vin not in ( --exclude list, pending board, sold orders, fleet orders
                      select vin
                      from gmgl.daily_inventory
                      where is_fleet = true
                        or is_sold = true
                        or stock_number in (
                          select stock_number
                          from board.pending_boards
                          where is_delivered = false
                          and is_deleted = false)); 