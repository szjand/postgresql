﻿select * from fin.fact_fs limit 10
select * from fin.dim_fs limit 100
select * from fin.dim_fs_org

drop table if exists fs cascade;
create temp table fs as
select store, page, line, line_label, control, price,
  case 
    when price between 0 and 14999 then '<15K'
    when price between 15000 and 24999 then '15k-25k'
    when price between 25000 and 34999 then '25k-35k'
    when price between 35000 and 44999 then '35k-45k'
    when price > 45000 then '>45k'
  end as price_band
from (  
	select store, page, line, line_label, control, sum(unit_count) as unit_count, sum(-amount)::integer as price
	from (
		select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
			a.control, a.amount,
			case when a.amount < 0 then 1 else -1 end as unit_count
		from fin.fact_gl a
		inner join dds.dim_date b on a.date_key = b.date_key
			and b.year_month between  202208 and 202302 
		inner join fin.dim_account c on a.account_key = c.account_key
			and c.current_row
		inner join fin.dim_journal aa on a.journal_key = aa.journal_key
			and aa.journal_code in ('VSN','VSU')
		inner join ( -- d: fs gm_account page/line/acct description
			select distinct f.store, d.gl_account, b.page, b.line, b.line_label, e.description
			from fin.fact_fs a
			inner join fin.dim_fs b on a.fs_key = b.fs_key
				and b.year_month between  202208 and 202302 
				and b.page = 16 and b.line between 1 and 14 -- used cars
			inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
			inner join fin.dim_account e on d.gl_account = e.account
				and e.account_type_code = '4'
				and e.current_row
			inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
				and f.store = 'RY8') d on c.account = d.gl_account
		where a.post_status = 'Y'
		and right(control, 1) <> 'L') h
	where line < 8  
	group by store, page, line, line_label, control
	having sum(unit_count) > 0) s
order by control;

aug: 38
sep: 33
oct: 24
nov: 34
dec: 31
jan: 35
feb: 30

select * from board.sales_board limit 10

drop table if exists board;
create temp table board as
select board_type_key, store,stock_number, vin, boarded_Date, board_type, board_sub_type, model_year, make, model, trim  
from (
	select d.arkona_store_key as store,
-- 	  case d.arkona_store_key when 'RY3' then 'RY1' else d.arkona_store_key end as store, 
		a.board_type_key, a.stock_number, a.vin, a.boarded_date, b.board_type, b.board_sub_type, 
		c.vehicle_model_year as model_year, c.vehicle_make as make, c.vehicle_model as model, e.trim
	-- select a.*
	from board.sales_board a
	join board.board_types b on a.board_type_key = b.board_type_key
		and b.board_type = 'Deal'
		and b.board_type_key not in (16)
	join board.daily_board c on a.board_id = c.board_id
		and c.vehicle_make not in ('yamaha','kawasaki','HARLEY','HARLEY DAVIDSON','HARLEY-DAVIDSON','SUZUKI')
		and c.vehicle_type = 'U'
	join onedc.stores d on a.store_key = d.store_key  
	left join ads.ext_vehicle_items e on a.vin = e.vin
	where a.boarded_date between '08/01/2022' and '02/28/2023'
		and not a.is_deleted
		and not is_backed_on
		and right(stock_number, 1) <> 'L'
		and a.vin <> 'CB3602006227') z
group by board_type_key, store,stock_number, vin, boarded_Date, board_type, board_sub_type, model_year, make, model, trim 
order by store, board_sub_type;  

select *
from board a
full outer join fs b on a.stock_number = b.control
where a.store = 'RY8'
  and a.board_sub_type <> 'wholesale'

select a.stock_number, a.vin, a.model_year, a.make, a.model, a.trim, b.price, price_band
from board a
join fs b on a.stock_number = b.control
where a.store = 'RY8'
  and a.board_sub_type <> 'wholesale'
order by a.make, a.model, a.trim
  
select count(*)
from board a
where a.store = 'RY8'
  and a.board_sub_type <> 'wholesale' 
  
select make, count(*)
from board a
where a.store = 'RY8'
  and a.board_sub_type <> 'wholesale' 
group by make  
order by count(*) desc

select age, make, count(*)
from (
-- select * 
select *, 2023 - model_year::integer as age
from board a
where a.store = 'RY8'
  and a.board_sub_type <> 'wholesale') b
group by make, age
order by count(*) desc   

select make, model, count(*)
from board a
where a.store = 'ry8'
  and a.board_sub_type <> 'wholesale'
group by make, model
order by count(*) desc   

select make, price_band, count(*)
from board a
join fs b on a.stock_number = b.control
where a.store = 'RY8'
  and a.board_sub_type <> 'wholesale'
group by make, price_band
order by count(*) desc

select price_band, count(*)
from board a
join fs b on a.stock_number = b.control
where a.store = 'RY8'
  and a.board_sub_type <> 'wholesale'
group by price_band
order by count(*) desc



















select d.gl_account, b.line, e.description
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202301
  and b.page = 16
  and b.line between 1 and 14
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
  and e.account_type_code = '4'
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store = 'ry8'  

drop table if exists front_gross cascade;
create temp table front_gross as
select stock_number, sales - cost as front_gross
from (
	select a.stock_number, 
		sum(-b.amount::integer) filter (where d.account_type = 'sale') as sales,
		sum(b.amount::integer) filter (where d.account_type = 'cogs') as cost
	from board a
	join fin.fact_gl b on a.stock_number = b.control
	join fin.dim_account c on b.account_key = c.account_key
	join (
		select distinct d.gl_account, e.account_type, e.department
		from fin.fact_fs a
		inner join fin.dim_fs b on a.fs_key = b.fs_key
			and b.year_month = 202301
			and b.page = 16
			and b.line between 1 and 14
		inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
		inner join fin.dim_account e on d.gl_account = e.account  
		inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
			and f.store = 'ry8') d on c.account = d.gl_account
	where b.post_status = 'Y'		
		and a.store = 'ry8'
		and a.board_sub_type <> 'wholesale'		
	group by a.stock_number) x


-- most of the deals missing gross are feb end of month
  select *
  from board a
  left join (select distinct stock_number from front_gross) b on a.stock_number = b.stock_number
  where b.stock_number is null
  		and a.store = 'ry8'
		and a.board_sub_type <> 'wholesale'	

-- fuck me, this is missing way more
select * 
from board a
left join sls.deals_accounting_detail b on a.stock_number = b.control
where a.store = 'ry8'
  and a.board_sub_type = 'retail'

