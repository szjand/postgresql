﻿select b.vin, max(a.vehicleevaluationts::date) as the_date, max(c.value) as miles
from ads.ext_vehicle_evaluations a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
left join ads.ext_Vehicle_item_mileages c on a.vehicleitemid = c.vehicleitemid
where currentstatus in ('VehicleEvaluation_Booked','VehicleEvaluation_Finished')
  and vehicleevaluationts::date > current_date - 15
group by b.vin


select 3+5+5+5+4+3+5+5+4+4+4+4+5+4+3+2+2+3+35  -- 105

0 : W/S
1 - 63 : 1
64 - 84 : 2
105 - 85 : 3


SELECT b.* 
FROM ads.ext_Vehicle_Items a
JOIN ads.ext_Vehicle_Item_mileages b on a.VehicleItemID = b.VehicleItemID
AND a.vin = '2HGFB2F8XFH530303'

drop table if exists ads.ext_appeal_scorecards;
CREATE TABLE ads.ext_appeal_scorecards ( 
      TableKey citext primary key,
      AppealMatrixID citext not null,
      Appeal citext,
      BasisTable citext not null,
      PaintBody integer,
      FrameUnibody integer,
      GlassLightsLenses integer,
      Rust integer,
      Tires integer,
      IntExtColor integer,
      WheelLook integer,
      Odor integer,
      UpholsteryCarpet integer,
      EngineTransmission integer,
      SteeringSuspension integer,
      ACHeater integer,
      Brakes integer,
      Title integer,
      MaintenanceRecords integer,
      ServiceContract integer,
      OwnerHistory integer);

drop table if exists ads.ext_appeal_matrix_elements;
CREATE TABLE ads.ext_appeal_matrix_elements ( 
      AppealMatrixID citext,
      AppealElementTypecitext citext,
      AppealElementName citext,
      Rank integer,
      Label citext,
      Score Integer);


select sum(score) from ads.ext_appeal_matrix_elements where rank = 0 
      
select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ads'
and table_name = 'ext_appeal_scorecards'

select aa.*, 
  paintbody,frameunibody,glasslightslenses,rust,tires,intextcolor,wheellook,odor,upholsterycarpet,
  enginetransmission,steeringsuspension,acheater,brakes,title,maintenancerecords,servicecontract,ownerhistory,
  bb.appeal, PaintBody + frameunibody + GlassLightsLenses + Rust + Tires + IntExtColor + 
	WheelLook + Odor + UpholsteryCarpet + EngineTransmission + SteeringSuspension + ACHeater + 
	Brakes + Title + MaintenanceRecords + ServiceContract + OwnerHistory
from (
	select b.vin, a.vehicleevaluationid,  max(a.vehicleevaluationts::date) as the_date, max(c.value) as miles
	from ads.ext_vehicle_evaluations a
	join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
	left join ads.ext_Vehicle_item_mileages c on a.vehicleitemid = c.vehicleitemid
	where currentstatus in ('VehicleEvaluation_Booked','VehicleEvaluation_Finished')
		and vehicleevaluationts::date > current_date - 15
	group by b.vin, a.vehicleevaluationid) aa     
left join ads.ext_appeal_scorecards	bb on aa.vehicleevaluationid = bb.tablekey

-- age at trade
round((date_capped - to_date('09'||'01'||(year - 1)::text,'MMDDYYYY'))/365.0, 1) as age_in_years_at_trade_2
-- miles per year
round(f.value/case when a.age_in_years_at_trade_2 = 0 then 1 else a.age_in_years_at_trade_2 end, 0) as miles_per_year




select aa.*, 
--   paintbody,frameunibody,glasslightslenses,rust,tires,intextcolor,wheellook,odor,upholsterycarpet,
--   enginetransmission,steeringsuspension,acheater,brakes,title,maintenancerecords,servicecontract,ownerhistory,
--   (select score from ads.ext_appeal_matrix_elements where appealelementname = 'paintbody' and rank = bb.paintbody) as paintbody_score,
--   (select score from ads.ext_appeal_matrix_elements where appealelementname = 'frameunibody' and rank = bb.frameunibody) as frameunibody_score,
--   (select score from ads.ext_appeal_matrix_elements where appealelementname = 'glasslightslenses' and rank = bb.glasslightslenses) as glasslightslenses_score,
--   (select score from ads.ext_appeal_matrix_elements where appealelementname = 'rust' and rank = bb.rust) as rust_score,
--   (select score from ads.ext_appeal_matrix_elements where appealelementname = 'tires' and rank = bb.tires) as tires_score,
--   (select score from ads.ext_appeal_matrix_elements where appealelementname = 'intextcolor' and rank = bb.intextcolor) as intextcolor_score,
--   (select score from ads.ext_appeal_matrix_elements where appealelementname = 'wheellook' and rank = bb.wheellook) as wheellook_score,
--   (select score from ads.ext_appeal_matrix_elements where appealelementname = 'odor' and rank = bb.odor) as odor_score,
--   (select score from ads.ext_appeal_matrix_elements where appealelementname = 'upholsterycarpet' and rank = bb.upholsterycarpet) as upholsterycarpet_score,
--   (select score from ads.ext_appeal_matrix_elements where appealelementname = 'enginetransmission' and rank = bb.enginetransmission) as enginetransmission_score,
--   (select score from ads.ext_appeal_matrix_elements where appealelementname = 'steeringsuspension' and rank = bb.steeringsuspension) as steeringsuspension_score,
--   (select score from ads.ext_appeal_matrix_elements where appealelementname = 'acheater' and rank = bb.acheater) as acheater_score,
--   (select score from ads.ext_appeal_matrix_elements where appealelementname = 'brakes' and rank = bb.brakes) as brakes_score,
--   (select score from ads.ext_appeal_matrix_elements where appealelementname = 'title' and rank = bb.title) as title_score,
--   (select score from ads.ext_appeal_matrix_elements where appealelementname = 'maintenancerecords' and rank = bb.maintenancerecords) as maintenancerecords_score,
--   (select score from ads.ext_appeal_matrix_elements where appealelementname = 'servicecontract' and rank = bb.servicecontract) as servicecontract_score,
--   (select score from ads.ext_appeal_matrix_elements where appealelementname = 'ownerhistory' and rank = bb.ownerhistory) as ownerhistory_score
  (select score from ads.ext_appeal_matrix_elements where appealelementname = 'paintbody' and rank = bb.paintbody) +
  (select score from ads.ext_appeal_matrix_elements where appealelementname = 'frameunibody' and rank = bb.frameunibody) +
  (select score from ads.ext_appeal_matrix_elements where appealelementname = 'glasslightslenses' and rank = bb.glasslightslenses) +
  (select score from ads.ext_appeal_matrix_elements where appealelementname = 'rust' and rank = bb.rust) +
  (select score from ads.ext_appeal_matrix_elements where appealelementname = 'tires' and rank = bb.tires) +
  (select score from ads.ext_appeal_matrix_elements where appealelementname = 'intextcolor' and rank = bb.intextcolor) +
  (select score from ads.ext_appeal_matrix_elements where appealelementname = 'wheellook' and rank = bb.wheellook) +
  (select score from ads.ext_appeal_matrix_elements where appealelementname = 'odor' and rank = bb.odor) +
  (select score from ads.ext_appeal_matrix_elements where appealelementname = 'upholsterycarpet' and rank = bb.upholsterycarpet) +
  (select score from ads.ext_appeal_matrix_elements where appealelementname = 'enginetransmission' and rank = bb.enginetransmission) +
  (select score from ads.ext_appeal_matrix_elements where appealelementname = 'steeringsuspension' and rank = bb.steeringsuspension) +
  (select score from ads.ext_appeal_matrix_elements where appealelementname = 'acheater' and rank = bb.acheater) +
  (select score from ads.ext_appeal_matrix_elements where appealelementname = 'brakes' and rank = bb.brakes) +
  (select score from ads.ext_appeal_matrix_elements where appealelementname = 'title' and rank = bb.title) +
  (select score from ads.ext_appeal_matrix_elements where appealelementname = 'maintenancerecords' and rank = bb.maintenancerecords) +
  (select score from ads.ext_appeal_matrix_elements where appealelementname = 'servicecontract' and rank = bb.servicecontract) +
  (select score from ads.ext_appeal_matrix_elements where appealelementname = 'ownerhistory' and rank = bb.ownerhistory) as appeal_score,

	case round(
				 aa.miles/ case 
										 when round((the_date - to_date('09'||'01'||(aa.yearmodel::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
										 else round((the_date - to_date('09'||'01'||(aa.yearmodel::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
									 end, 0) 
	  when < 5000 then 35
	  when between 5000 and 9999 then 25
	  when between 10000 and 14999 then 15
	  when between 15000 and 19999 then -5
	  when between 20000 and 24999 then -15
	  when between 25000 and 999999 then -30
	end
from (
	select b.vin, a.vehicleevaluationid, b.yearmodel, max(a.vehicleevaluationts::date) as the_date, max(c.value) as miles
	from ads.ext_vehicle_evaluations a
	join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
	left join ads.ext_Vehicle_item_mileages c on a.vehicleitemid = c.vehicleitemid
	where currentstatus in ('VehicleEvaluation_Booked','VehicleEvaluation_Finished')
		and vehicleevaluationts::date > current_date - 15
	group by b.vin, a.vehicleevaluationid, b.yearmodel) aa     
left join ads.ext_appeal_scorecards	bb on aa.vehicleevaluationid = bb.tablekey

-- 05/25/21 sent to afton
select *, appeal_score + mileage_score as appeal_perc -- max appeal + mileage scores = 100
from (
	select vin, yearmodel, miles, the_date, 
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'paintbody' and rank = paintbody) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'frameunibody' and rank = frameunibody) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'glasslightslenses' and rank = glasslightslenses) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'rust' and rank = rust) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'tires' and rank = tires) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'intextcolor' and rank = intextcolor) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'wheellook' and rank = wheellook) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'odor' and rank = odor) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'upholsterycarpet' and rank = upholsterycarpet) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'enginetransmission' and rank = enginetransmission) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'steeringsuspension' and rank = steeringsuspension) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'acheater' and rank = acheater) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'brakes' and rank = brakes) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'title' and rank = title) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'maintenancerecords' and rank = maintenancerecords) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'servicecontract' and rank = servicecontract) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'ownerhistory' and rank = ownerhistory) as appeal_score,
		miles_per_year, 
		case
			when miles_per_year < 5000 then 35
			when miles_per_year between 5000 and 9999 then 25
			when miles_per_year between 10000 and 14999 then 15
			when miles_per_year between 15000 and 19999 then -5
			when miles_per_year between 20000 and 24999 then -15
			when miles_per_year between 25000 and 999999 then -30
		end as mileage_score
	from (
		select aa.*, paintbody, frameunibody, glasslightslenses, rust,tires, intextcolor ,wheellook, odor, upholsterycarpet,
			enginetransmission, steeringsuspension, acheater, brakes, title, maintenancerecords, servicecontract, ownerhistory,
			round(
				aa.miles/ case 
									 when round((the_date - to_date('09'||'01'||(aa.yearmodel::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
									 else round((the_date - to_date('09'||'01'||(aa.yearmodel::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
								 end, 0) as miles_per_year
		from (
			select b.vin, a.vehicleevaluationid, b.yearmodel, max(a.vehicleevaluationts::date) as the_date, max(c.value) as miles
			from ads.ext_vehicle_evaluations a
			join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
			left join ads.ext_Vehicle_item_mileages c on a.vehicleitemid = c.vehicleitemid
			where currentstatus in ('VehicleEvaluation_Booked','VehicleEvaluation_Finished')
				and vehicleevaluationts::date > current_date - 15
			group by b.vin, a.vehicleevaluationid, b.yearmodel) aa     
		join ads.ext_appeal_scorecards	bb on aa.vehicleevaluationid = bb.tablekey)  cc) dd   


-- 05/25/21 needs amount paid, use ACV

select *, appeal_score + mileage_score as appeal_perc -- max appeal + mileage scores = 100
from (
	select vin, yearmodel, miles, the_date, acv as amount_paid, 
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'paintbody' and rank = paintbody) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'frameunibody' and rank = frameunibody) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'glasslightslenses' and rank = glasslightslenses) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'rust' and rank = rust) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'tires' and rank = tires) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'intextcolor' and rank = intextcolor) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'wheellook' and rank = wheellook) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'odor' and rank = odor) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'upholsterycarpet' and rank = upholsterycarpet) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'enginetransmission' and rank = enginetransmission) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'steeringsuspension' and rank = steeringsuspension) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'acheater' and rank = acheater) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'brakes' and rank = brakes) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'title' and rank = title) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'maintenancerecords' and rank = maintenancerecords) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'servicecontract' and rank = servicecontract) +
		(select score from ads.ext_appeal_matrix_elements where appealelementname = 'ownerhistory' and rank = ownerhistory) as appeal_score,
		miles_per_year, 
		case
			when miles_per_year < 5000 then 35
			when miles_per_year between 5000 and 9999 then 25
			when miles_per_year between 10000 and 14999 then 15
			when miles_per_year between 15000 and 19999 then -5
			when miles_per_year between 20000 and 24999 then -15
			when miles_per_year between 25000 and 999999 then -30
		end as mileage_score
	from (
		select aa.*, cc.amount::integer as acv, paintbody, frameunibody, glasslightslenses, rust,tires, intextcolor ,wheellook, odor, upholsterycarpet,
			enginetransmission, steeringsuspension, acheater, brakes, title, maintenancerecords, servicecontract, ownerhistory,
			round(
				aa.miles/ case 
									 when round((the_date - to_date('09'||'01'||(aa.yearmodel::integer - 1)::text,'MMDDYYYY'))/365.0, 1) = 0 then 1
									 else round((the_date - to_date('09'||'01'||(aa.yearmodel::integer - 1)::text,'MMDDYYYY'))/365.0, 1)
								 end, 0) as miles_per_year
		from (
			select b.vin, a.vehicleevaluationid, b.yearmodel, max(a.vehicleevaluationts::date) as the_date, max(c.value) as miles
			from ads.ext_vehicle_evaluations a
			join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
			left join ads.ext_Vehicle_item_mileages c on a.vehicleitemid = c.vehicleitemid
			where currentstatus in ('VehicleEvaluation_Booked','VehicleEvaluation_Finished')
				and vehicleevaluationts::date > current_date - 15
			group by b.vin, a.vehicleevaluationid, b.yearmodel) aa     
		join ads.ext_appeal_scorecards	bb on aa.vehicleevaluationid = bb.tablekey
		left join ads.ext_vehicle_price_components cc on aa.vehicleevaluationid = cc.tablekey
		  and cc.thruts is null
		  and cc.typ = 'VehiclePriceComponent_ACV')  ccc) ddd   