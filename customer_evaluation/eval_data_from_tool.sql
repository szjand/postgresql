﻿select c.vehicleevaluationts::date, a.stocknumber, b.vin,
  b.yearmodel, b.make ||' '|| b.model ||' '|| b.trim as vehicle,
  reconmechanicalamount::integer as mechanical, reconbodyamount::integer as body, reconappearanceamount::integer as appearance,
  recontireamount::integer as tires, reconglassamount::integer as glass,
  (reconmechanicalamount + reconbodyamount + reconappearanceamount + recontireamount + reconglassamount)::integer as total_recon,
  d.amount as acv, e.amount as finished, c.amountshown as amount_shown
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
join ads.ext_vehicle_evaluations c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
left join ads.ext_vehicle_price_components d on c.vehicleevaluationid = d.tablekey
	and d.thruts is null
	and d.typ = 'VehiclePriceComponent_ACV'
left join ads.ext_vehicle_price_components e on c.vehicleevaluationid = e.tablekey
	and e.thruts is null
	and e.typ = 'VehiclePriceComponent_ACV'
where a.thruts::date > current_date
  and b.vin = '1GTU9FEL7KZ253909'



create or replace function afton.get_tool_data_for_customer_eval_test(_vin citext)
returns setof json as
$BODY$
/*
select afton.get_tool_data_for_customer_eval_test('1GTU9FEL7KZ253909');
*/
select row_to_json(bb)
from (
	select json_agg(row_to_json(aa)) as tool_data
	from (
		select c.vehicleevaluationts::date, a.stocknumber, b.vin,
			b.yearmodel, b.make ||' '|| b.model ||' '|| b.trim as vehicle,
			reconmechanicalamount::integer as mechanical, reconbodyamount::integer as body, reconappearanceamount::integer as appearance,
			recontireamount::integer as tires, reconglassamount::integer as glass,
			(reconmechanicalamount + reconbodyamount + reconappearanceamount + recontireamount + reconglassamount)::integer as total_recon,
			d.amount as acv, e.amount as finished, c.amountshown as amount_shown
		from ads.ext_vehicle_inventory_items a
		join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
		join ads.ext_vehicle_evaluations c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
		left join ads.ext_vehicle_price_components d on c.vehicleevaluationid = d.tablekey
			and d.thruts is null
			and d.typ = 'VehiclePriceComponent_ACV'
		left join ads.ext_vehicle_price_components e on c.vehicleevaluationid = e.tablekey
			and e.thruts is null
			and e.typ = 'VehiclePriceComponent_ACV'
		where a.thruts::date > current_date
			and b.vin = _vin) aa) bb;

$BODY$
language sql;