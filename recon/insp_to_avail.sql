﻿
drop table if exists jon.insp_to_avail cascade;
create unlogged table jon.insp_to_avail as
select b.stocknumber, d.vin, b.fromts::date as date_acq, c.thruts::date as date_insp, e.thruts::date as date_walk, a.fromts::date as date_avail, b.thruts::date as date_del
from ads.ext_vehicle_inventory_item_statuses a
join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
join ads.ext_vehicle_inventory_item_statuses c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
  and c.status = 'RMFlagIP_InspectionPending'
join ads.ext_vehicle_items d on b.vehicleitemid = d.vehicleitemid  
join ads.ext_vehicle_inventory_item_statuses e on a.vehicleinventoryitemid = e.vehicleinventoryitemid
  and e.status = 'RMFlagWP_WalkPending'
where a.status = 'RMFlagAV_Available'
  and a.fromts::date > '12/31/2022'
order by a.fromts::date;
create index on jon.insp_to_avail(vin); 
create index on jon.insp_to_avail(date_acq); 
create index on jon.insp_to_avail(date_del); 

select * from jon.insp_to_avail order by date_acq

drop table if exists jon.ros cascade;
create unlogged table jon.ros as
select a.ro, a.ro_flag_hours, a.open_date, a.close_date, b.vin
from dds.fact_repair_order a
join dds.dim_vehicle b on a.vehicle_key = b.vehicle_key
where a.open_date > '06/30/2022'
group by a.ro, a.ro_flag_hours, a.open_date, a.close_date, b.vin;
create index on jon.ros(vin);
create index on jon.ros(open_date);
create index on jon.ros(close_date);

select * 
from jon.insp_to_avail a
join jon.ros b on a.vin = b.vin
  and b.open_date between a.date_acq and a.date_del

select a.date_avail, b.*
from (
	select distinct date_avail 
	from jon.insp_to_avail 
	order by date_avail) a
left join jon.insp_to_avail b on a.date_avail = b.date_avail

select a.date_avail, a.stocknumber, a.date_avail-a.date_insp as days_to_lot, sum(b.ro_flag_hours) as flag_hours
from jon.insp_to_avail a
join jon.ros b on a.vin = b.vin
  and b.open_date between a.date_acq and a.date_del
group by a.date_avail, a.stocknumber, a.date_avail-a.date_insp
