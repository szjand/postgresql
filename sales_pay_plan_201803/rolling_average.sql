﻿
rolling avg matters  because it determines the guarantee
if rolling 3 month avg >= 10, guarantee is 3k/month otherwise guarantee is 1.8k/month
3 month avg does not apply to new sales consultants until the 4th full month of employment
so for the current month, for each employee, i need to know full months employment


select seq from sls.months where open_closed = 'open';

select employee_number, 
  (select seq from sls.months where open_closed = 'open') - 
    (select seq from sls.months where year_month = a.first_full_month_emp) as months_employed
from sls.personnel a
order by first_full_month_emp



-- rolling 3 month avg is for the previous 3 complete months, eg for 201803, the avg of 201712 -> 201812
-- so, programatically, what are the previous 3 months

select year_month
from sls.months where seq in (
select seq - 3 from sls.months where open_closed = 'open'
union
select seq - 2 from sls.months where open_closed = 'open'
union
select seq - 1 from sls.months where open_closed = 'open')


 
  select psc_employee_number, round(sum(unit_count)/3, 1) as "3 month rolling avg"
  from (
    select a.psc_employee_number, psc_last_name, b.first_full_month_emp,
      case
        when ssc_last_name = 'none' then unit_count
        else 0.5 * unit_count
      end as unit_count
    from sls.deals_by_month a
    inner join sls.personnel b on a.psc_employee_number = b.employee_number
    where a.year_month in (
        select year_month
        from sls.months where seq in (
        select seq - 3 from sls.months where open_closed = 'open'
        union
        select seq - 2 from sls.months where open_closed = 'open'
        union
        select seq - 1 from sls.months where open_closed = 'open')) 
    union all
    select ssc_employee_number, ssc_last_name, b.first_full_month_emp,
      0.5 * unit_count as unit_count
    from sls.deals_by_month a
    inner join sls.personnel b on a.ssc_employee_number = b.employee_number
    where a.year_month in (
        select year_month
        from sls.months where seq in (
        select seq - 3 from sls.months where open_closed = 'open'
        union
        select seq - 2 from sls.months where open_closed = 'open'
        union
        select seq - 1 from sls.months where open_closed = 'open')) 
      and a.ssc_last_name <> 'none') e
  group by psc_employee_number






select year_month
from sls.months where seq in (
select seq - 3 from sls.months where year_month = 201803
union
select seq - 2 from sls.months where year_month = 201803
union
select seq - 1 from sls.months where year_month = 201803)





















---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
-- this is all the fix for personnel needing to be updated with fi_id's
-- update deals_by_month fi
select stock_number, unit_count, psc_last_name, fi_last_name
from sls.deals_by_month
where (psc_last_name in ('barker','seay','weber') or fi_last_name in ('barker','seay','weber'))
  and year_month = '201802'
order by fi_last_name

select b.*
from sls.deals_by_month a
left join sls.deals b on a.vin = b.vin
where b.year_month = 201802
order by b.fi_manager

select a.stock_number, a.psc_last_name, a.ssc_last_name, a.fi_last_name, b.fi_manager --b.primary_sc, b.secondary_sc, b.fi_manager
from sls.deals_by_month a
left join sls.deals b on a.bopmast_id = b.bopmast_id
-- where b.fi_manager = 'BFO'
where b.year_month = 201802
order by a.fi_last_name

-- there are  deals in deals_by_month where the fi is none because the personnel table had not been updated
-- with fi_id's, it's part of the a-z deal
-- ok, i think this shows the ones that need to be fixed
select a.fi_last_name, b.fi_manager, count(*)
from sls.deals_by_month a
left join sls.deals b on a.bopmast_id = b.bopmast_id
where b.year_month > 201712
group by a.fi_last_name, b.fi_manager
order by a.fi_last_name

select a.stock_number, b.bopmast_id, b.seq, a.psc_first_name, a.psc_last_name, a.psc_employee_number, a.fi_last_name, b.fi_manager 
from sls.deals_by_month a
left join sls.deals b on a.bopmast_id = b.bopmast_id
where b.year_month > 201712
  and a.fi_last_name = 'none' 
  and b.fi_manager in ('JWE','FRD','JOB')
order by b.fi_manager, a.psc_last_name  

select * 
from sls.deals_by_month
where stock_number in (
  select a.stock_number
  from sls.deals_by_month a
  left join sls.deals b on a.bopmast_id = b.bopmast_id
  where b.year_month > 201712
    and a.fi_last_name = 'none' 
    and b.fi_manager in ('JWE','FRD','JOB'))

update sls.deals_by_month
set fi_first_name = 'Frank',
    fi_last_name = 'Decouteau',
    fi_employee_number = '15552'
where bopmast_id = 46588
  and seq = 1;  

update sls.deals_by_month
set fi_first_name = 'Joshua',
    fi_last_name = 'Barker',
    fi_employee_number = '110052'
where bopmast_id in (46520,46492,46619,46607,46423,46622,46568)
  and seq = 1;

update sls.deals_by_month
set fi_first_name = 'James',
    fi_last_name = 'Weber',
    fi_employee_number = '1147810'
where bopmast_id in (46652,46522,46487,46334,46429,46569,46450,46498,46402,46422,46507,46589,46462,46408)
  and seq = 1;

    
select * from arkona.ext_bopslsp where active is null and sales_person_type = 'F' order by company_number, sales_person_name

-- as of now 3/19/18, all the a-z guys have the same fi_id for both stores
select sales_person_name,
  max(case when company_number = 'RY1' then sales_person_id end) as ry1_id,
  max(case when company_number = 'RY2' then sales_person_id end) as ry2_id
from arkona.ext_bopslsp 
where active is null 
  and sales_person_type = 'F' 
  and company_number in ('RY1','RY2')
group by sales_person_name
order by sales_person_name


