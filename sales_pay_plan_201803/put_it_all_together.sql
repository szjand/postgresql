﻿-- select * from sls.personnel
-- select * from sls.deals_gross_by_month where year_month = 201803
-- insert into sls.consultant_payroll
-- replace tmp_consultants, hell, just eliminate it, not needed
with 
  open_month as (
    select year_month
    from sls.months
--     where year_month = 201803)
    where open_closed = 'open')
select (select year_month from open_month),
  jj.*,
  round(
    case
      when total_earned > guarantee then total_earned - draw
      else guarantee - draw
    end, 2) as due_at_month_end,
  now()
from ( -- jj                     
  select  ff.*, coalesce(gg."3 month rolling avg", 0) as "3_month_rolling_avg", hh.months_employed,
    unit_pay + fi_pay + pto_pay as total_earned,
    ii.draw, 
    case
      when hh.months_employed > 4 and coalesce(gg."3 month rolling avg", 0) < 10 then 1800
      else 3000
    end as guarantee   
  from ( --ff
    select aa.*, bb.unit_count,
      (select * from sls.per_unit_earnings (aa.employee_number, bb.unit_count)) as unit_pay, 
      case 
        when aa.payplan = 'executive' then coalesce(cc.fi_gross, 0)
        else 0
      end as fi_gross, 
      case 
        when aa.payplan = 'executive' then coalesce(dd.amount, 0)
        else 0
      end as chargebacks,
      case 
        when aa.payplan = 'executive' then coalesce(dd.amount, 0) + coalesce(cc.fi_gross)
        else 0
      end as fi_total, 
      case 
        when aa.payplan = 'executive' then round(.16 * (coalesce(dd.amount, 0) + coalesce(cc.fi_gross)), 2)
        else 0
      end as fi_pay,
      ee.pto_hours, ee.pto_rate, coalesce(ee.pto_pay, 0) as pto_pay
    from ( -- aa: cons, team, payplan
      select b.team, a.last_name, a.first_name, a.employee_number, c.payplan
      from sls.personnel a
      inner join sls.team_personnel b on a.employee_number = b.employee_number
        and b.thru_date > current_date
      inner join sls.personnel_payplans c on a.employee_number = c.employee_number
        and c.thru_date > current_date
      where a.store_code = 'RY1') aa
    left join ( -- bb: unit count
      select psc_employee_number, sum(unit_count) as unit_count
      from (
      select a.psc_employee_number, 
        case
          when ssc_last_name = 'none' then unit_count
          else 0.5 * unit_count
        end as unit_count
      from sls.deals_by_month a
--       inner join sls.tmp_consultants b on a.psc_employee_number = b.employee_number
--         or a.ssc_employee_number = b.employee_number
      where a.year_month = (select year_month from open_month)
      union all
      select a.ssc_employee_number, 
        0.5 * unit_count as unit_count
      from sls.deals_by_month a
--       inner join sls.tmp_consultants b on a.ssc_employee_number = b.employee_number
      where a.year_month = (select year_month from open_month)
        and a.ssc_last_name <> 'none') x
      group by psc_employee_number) bb  on aa.employee_number = bb.psc_employee_number
    left join ( -- cc: fi gross
      select a.fi_employee_number, sum(b.fi_gross) as fi_gross
      from sls.deals_by_month a
      left join sls.deals_gross_by_month b on a.stock_number = b.control
        and a.year_month = b.year_month
      where a.year_month = (select year_month from open_month)
      group by a.fi_employee_number) cc on aa.employee_number = cc.fi_employee_number
    left join ( -- fi chargebacks
      select fi_employee_number, sum(amount) as amount
      from sls.fi_chargebacks
      where chargeback_year_month = (select year_month from open_month)
      group by fi_employee_number) dd on aa.employee_number = dd.fi_employee_number
    left join (
      select a.employee_number, round(a.pto_hours, 2) as pto_hours, b.pto_rate, round(a.pto_hours * b.pto_rate, 2) as pto_pay
      from sls.clock_hours_by_month a
      left join sls.pto_intervals b on a.employee_number = b.employee_number
        and a.year_month = b.year_month
      where a.year_month = (select year_month from open_month)
        and a.pto_hours <> 0
        and b.pto_rate <> 0) ee on aa.employee_number = ee.employee_number) ff
  left join ( -- gg: 3 month rolling avg
    select psc_employee_number, round(sum(unit_count)/3, 1) as "3 month rolling avg"
    from (
      select a.psc_employee_number, psc_last_name, b.first_full_month_emp,
        case
          when ssc_last_name = 'none' then unit_count
          else 0.5 * unit_count
        end as unit_count
      from sls.deals_by_month a
      inner join sls.personnel b on a.psc_employee_number = b.employee_number
      where a.year_month in (
          select year_month
          from sls.months where seq in (
          select seq - 3 from sls.months where year_month = (select year_month from open_month)
          union
          select seq - 2 from sls.months where year_month = (select year_month from open_month)
          union
          select seq - 1 from sls.months where year_month = (select year_month from open_month))) 
      union all
      select ssc_employee_number, ssc_last_name, b.first_full_month_emp,
        0.5 * unit_count as unit_count
      from sls.deals_by_month a
      inner join sls.personnel b on a.ssc_employee_number = b.employee_number
      where a.year_month in (
          select year_month
          from sls.months where seq in (
          select seq - 3 from sls.months where year_month = (select year_month from open_month)
          union
          select seq - 2 from sls.months where year_month = (select year_month from open_month)
          union
          select seq - 1 from sls.months where year_month = (select year_month from open_month))) 
        and a.ssc_last_name <> 'none') e
    group by psc_employee_number) gg on ff.employee_number = gg.psc_employee_number 
  left join ( -- hh: months employed
    select employee_number, 
      (select seq from sls.months where open_closed = 'open') - 
        (select seq from sls.months where year_month = a.first_full_month_emp) as months_employed
    from sls.personnel a) hh on ff.employee_number = hh.employee_number 
  left join sls.paid_by_month ii on ff.employee_number = ii.employee_number
    and ii.year_month = (select year_month from open_month)) jj
order by last_name




