﻿
1/1/2020
all this has been done
spreadsheets sent to kim and nick
now its time to close december and make january the open month

select sls.update_is_payroll_submitted('RY2')
select sls.update_is_payroll_submitted('RY1')
and we are done
    select year_month
    from sls.months
    where open_closed = 'open'

from jeri 12/18/19
Year-End:  Sales cut-off (and others on monthly payroll) will be Thursday, December 26th.  
Payroll must be turned in the night of December 26th so that Kim can process right away 
on Friday, December 27th.  You will then turn in sales for the 27th – 31st separately.

i dont know what happened to the documentation for the part 1 from last year

1. create tables statically rather than in the script
/*
CREATE TABLE sls.ry1_201912_part_1_payroll_kim(
  consultant text,
  employee_number citext,
  "PTO (74)" numeric(8,2),
  "Other - Adjustments (79)" numeric,
  "Other - Addtl Comp (79)" numeric,
  "Unit Commission less draw (79)" numeric,
  "F&I Comm (79A)" numeric(8,2),
  "Pulse (79B)" integer,
  "Total Month End Payout" numeric);
CREATE TABLE sls.ry1_201912_part_2_payroll_kim(
  consultant text,
  employee_number citext,
  "PTO (74)" numeric(8,2),
  "Other - Adjustments (79)" numeric,
  "Other - Addtl Comp (79)" numeric,
  "Unit Commission less draw (79)" numeric,
  "F&I Comm (79A)" numeric(8,2),
  "Pulse (79B)" integer,
  "Total Month End Payout" numeric);
CREATE TABLE sls.ry2_201912_part_1_payroll_kim(
  consultant text,
  employee_number citext,
  "PTO (74)" numeric(8,2),
  "Other - Adjustments (79)" numeric,
  "Other - Addtl Comp (79)" numeric,
  "Unit Commission less draw (79)" numeric,
  "F&I Comm (79A)" numeric(8,2),
  "Pulse (79B)" integer,
  "Total Month End Payout" numeric);
CREATE TABLE sls.ry2_201912_part_2_payroll_kim(
  consultant text,
  employee_number citext,
  "PTO (74)" numeric(8,2),
  "Other - Adjustments (79)" numeric,
  "Other - Addtl Comp (79)" numeric,
  "Unit Commission less draw (79)" numeric,
  "F&I Comm (79A)" numeric(8,2),
  "Pulse (79B)" integer,
  "Total Month End Payout" numeric);  
CREATE TABLE sls.ry1_ry2_201912_part_1_payroll_mgr(
  store citext,
  last_name citext,
  first_name citext,
  unit_count numeric(3,1),
  unit_pay numeric(8,2),
  fi_gross numeric(8,2),
  chargebacks numeric(8,2),
  fi_total numeric(8,2),
  fi_pay numeric(8,2),
  pto_hours numeric(6,2),
  pto_rate numeric(6,2),
  pto_pay numeric(8,2),
  pulse integer,
  total_earned numeric(8,2),
  draw numeric(8,2),
  guarantee numeric(8,2),
  adjusted_amount numeric,
  additional_comp numeric,
  total_due numeric);
CREATE TABLE sls.ry1_ry2_201912_part_2_payroll_mgr(
  store citext,
  last_name citext,
  first_name citext,
  unit_count numeric(3,1),
  unit_pay numeric(8,2),
  fi_gross numeric(8,2),
  chargebacks numeric(8,2),
  fi_total numeric(8,2),
  fi_pay numeric(8,2),
  pto_hours numeric(6,2),
  pto_rate numeric(6,2),
  pto_pay numeric(8,2),
  pulse integer,
  total_earned numeric(8,2),
  draw numeric(8,2),
  guarantee numeric(8,2),
  adjusted_amount numeric,
  additional_comp numeric,
  total_due numeric);
CREATE TABLE sls.ry1_ry2_201912_part_1_deals(
  store_code citext,
  last_name citext,
  first_name citext,
  stock_number citext,
  unit_count numeric,
  sum numeric);
CREATE TABLE sls.ry1_ry2_201912_part_2_deals(
  store_code citext,
  last_name citext,
  first_name citext,
  stock_number citext,
  unit_count numeric,
  sum numeric);  
*/  


-- sales payroll year end
-- December payroll will be all deals thru 12/26


-- deals for 12/27 - 12/31 will be paid on the Jan 15th check
-- so, for each payroll,  i need to generate a payroll spreadsheet for kim
-- and a detail list for nick
-- 
/*
1/1/2019, updated sls.months and sls.is_payroll_submitted
insert into sls.is_payroll_submitted values
('RY1', 201812),
('RY2', 201812);

update sls.months
set open_closed = 'closed'
where year_month = 201812;

update sls.months
set open_closed = 'open'
where year_month = 201901;

update sls.months set open_closed = 'closed' where  year_month = 201901;
update sls.months set open_closed = 'open' where  year_month = 201812;
delete from sls.is_payroll_submitted where year_month = 201812;

*/

***************** aubol 
---------------------------------------------------------------------
--< PART 1 kim
---------------------------------------------------------------------
  on friday morning, 12/27/19, populate all the part 1 tables
  this is just a snapshot at this point in time, all the necessary reconciliation will be done for part 2
-- 
-- this generates the spreadsheet for kim:   select * from sls.json_get_payroll_for_kim('RY1')
-- create a spreadsheet for her
-- store all data in a table (for generating a diff, later)

select * from sls.ry1_201912_part_1_payroll_kim

-- this is taken verbatim from FUNCTION sls.json_get_payroll_for_kim(_store citext)
do $$
begin 
  delete from sls.ry1_201912_part_1_payroll_kim;
  insert into sls.ry1_201912_part_1_payroll_kim
  with
    open_month as (
      select year_month
      from sls.months
      where open_closed = 'open')
  select a.last_name || ', ' || a.first_name as consultant, a.employee_number, 
    a.pto_pay as "PTO (74)",
    coalesce(d.adjusted_amount, 0) as "Other - Adjustments (79)",
    coalesce(e.additional_comp, 0) as "Other - Addtl Comp (79)",
    round (
      case -- paid spiffs deducted only if base guarantee
        when a.total_earned >= a.guarantee then
          a.unit_pay - coalesce(a.draw, 0)
        else
          (a.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) - coalesce(c.spiffs, 0)
      end, 2) as "Unit Commission less draw (79)",
      a.fi_pay as "F&I Comm (79A)",   
      a.pulse as "Pulse (79B)",
      round( 
        case -- paid spiffs deducted only if base guarantee
          when a.total_earned >= a.guarantee then
            a.total_earned - coalesce(a.draw, 0) + coalesce(d.adjusted_amount, 0) + coalesce(e.additional_comp, 0)
          else
            (a.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) 
                - coalesce(c.spiffs, 0) + coalesce(e.additional_comp, 0) + coalesce(d.adjusted_amount, 0)
          end, 2) as "Total Month End Payout"      
  from sls.consultant_payroll a
  left join sls.pto_intervals b on a.employee_number = b.employee_number
    and a.year_month = b.year_month
  left join sls.paid_by_month c on a.employee_number = c.employee_number
    and a.year_month = c.year_month
  left join (
    select employee_number, sum(amount) as adjusted_amount
    from sls.payroll_adjustments
    where year_month =  (select year_month from open_month)
    group by employee_number) d on a.employee_number = d.employee_number
  left join (
    select employee_number, sum(amount) as additional_comp
    from sls.additional_comp
    where thru_date > (
      select first_of_month
      from sls.months
      where open_closed = 'open')
    group by employee_number) e on a.employee_number = e.employee_number  
  left join sls.personnel f on a.employee_number = f.employee_number
  left join ( -- if this is the first month of employment, multiplier = days worked/days in month
  select a.employee_number, 
    round(wd_of_month_remaining::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as guarantee_multiplier 
  from sls.personnel a
  inner join dds.dim_date b on a.start_date = b.the_date
    and b.year_month = (select year_month from open_month)) g on f.employee_number = g.employee_number      
  left join ( -- if employee termed mid month, multiplier elapsed wd in month at term date/work days in month
    select a.employee_number, 
      round(wd_of_month_elapsed::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as term_guarantee_multiplier
    from sls.personnel a
    inner join dds.dim_date b on a.end_date = b.the_date
      and b.year_month = (select year_month from open_month) ) h on f.employee_number = h.employee_number         
  where a.year_month = (select year_month from open_month)
    and f.store_code = 'RY1'
  order by consultant;
end $$;  


-- select * from sls.ry2_201912_part_1_payroll_kim
do $$
begin  
  delete from sls.ry2_201912_part_1_payroll_kim;
  insert into sls.ry2_201912_part_1_payroll_kim
  with
    open_month as (
      select year_month
      from sls.months
      where open_closed = 'open')
  select a.last_name || ', ' || a.first_name as consultant, a.employee_number, 
    a.pto_pay as "PTO (74)",
    coalesce(d.adjusted_amount, 0) as "Other - Adjustments (79)",
    coalesce(e.additional_comp, 0) as "Other - Addtl Comp (79)",
    round (
      case -- paid spiffs deducted only if base guarantee
        when a.total_earned >= a.guarantee then
          a.unit_pay - coalesce(a.draw, 0)
        else
          (a.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) - coalesce(c.spiffs, 0)
      end, 2) as "Unit Commission less draw (79)",
      a.fi_pay as "F&I Comm (79A)",   
      a.pulse as "Pulse (79B)",
      round( 
        case -- paid spiffs deducted only if base guarantee
          when a.total_earned >= a.guarantee then
            a.total_earned - coalesce(a.draw, 0) + coalesce(d.adjusted_amount, 0) + coalesce(e.additional_comp, 0)
          else
            (a.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) 
                - coalesce(c.spiffs, 0) + coalesce(e.additional_comp, 0) + coalesce(d.adjusted_amount, 0)
          end, 2) as "Total Month End Payout"      
  from sls.consultant_payroll a
  left join sls.pto_intervals b on a.employee_number = b.employee_number
    and a.year_month = b.year_month
  left join sls.paid_by_month c on a.employee_number = c.employee_number
    and a.year_month = c.year_month
  left join (
    select employee_number, sum(amount) as adjusted_amount
    from sls.payroll_adjustments
    where year_month =  (select year_month from open_month)
    group by employee_number) d on a.employee_number = d.employee_number
  left join (
    select employee_number, sum(amount) as additional_comp
    from sls.additional_comp
    where thru_date > (
      select first_of_month
      from sls.months
      where open_closed = 'open')
    group by employee_number) e on a.employee_number = e.employee_number  
  left join sls.personnel f on a.employee_number = f.employee_number
  left join ( -- if this is the first month of employment, multiplier = days worked/days in month
  select a.employee_number, 
    round(wd_of_month_remaining::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as guarantee_multiplier 
  from sls.personnel a
  inner join dds.dim_date b on a.start_date = b.the_date
    and b.year_month = (select year_month from open_month)) g on f.employee_number = g.employee_number      
  left join ( -- if employee termed mid month, multiplier elapsed wd in month at term date/work days in month
    select a.employee_number, 
      round(wd_of_month_elapsed::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as term_guarantee_multiplier
    from sls.personnel a
    inner join dds.dim_date b on a.end_date = b.the_date
      and b.year_month = (select year_month from open_month) ) h on f.employee_number = h.employee_number         
  where a.year_month = (select year_month from open_month)
    and f.store_code = 'RY2'
  order by consultant;
end $$;  


---------------------------------------------------------------------
--/> PART 1 kim
---------------------------------------------------------------------

---------------------------------------------------------------------
--< PART 1 payroll mgr
---------------------------------------------------------------------
-- taken from FUNCTION sls.json_get_payroll_for_submittal()
-- matches the vision payroll submittal page
-- removed a few fields

select * from sls.ry1_ry2_201912_part_1_payroll_mgr

do $$
declare
  _year_month integer := 201912;
begin
  delete from sls.ry1_ry2_201912_part_1_payroll_mgr;
  insert into sls.ry1_ry2_201912_part_1_payroll_mgr
  select 
    case
      when left(a.employee_number, 1) = '1' then 'RY1'
      else 'RY2'
    end as store, a.last_name, a.first_name, a.unit_count as units,
    a.unit_pay, a.fi_gross, a.chargebacks, a.fi_total, a.fi_pay, coalesce(a.pto_hours, 0) as pto_hours,
    coalesce(a.pto_rate, b.pto_rate) as pto_rate, a.pto_pay, a.pulse, --coalesce(c.spiffs, 0) as spiffs, 
    a.total_earned, a.draw, 
    round(a.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1), 2) as guarantee, 
    coalesce(d.adjusted_amount, 0) as adjusted_amount,
    coalesce(e.additional_comp, 0) as additional_comp,
    round (
      case -- paid spiffs deducted only if base guarantee
        when a.total_earned >= a.guarantee then
          a.total_earned - coalesce(a.draw, 0) + coalesce(d.adjusted_amount, 0) + coalesce(e.additional_comp, 0)
        else
          (a.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) 
              - coalesce(c.spiffs, 0) + coalesce(e.additional_comp, 0) + coalesce(d.adjusted_amount, 0)
        end, 2) as total_due
  from sls.consultant_payroll a
  left join sls.pto_intervals b on a.employee_number = b.employee_number
    and a.year_month = b.year_month
  left join sls.paid_by_month c on a.employee_number = c.employee_number
    and a.year_month = c.year_month
  left join (
    select employee_number, sum(amount) as adjusted_amount
    from sls.payroll_adjustments
    where year_month =  _year_month
    group by employee_number) d on a.employee_number = d.employee_number
  left join (
    select employee_number, sum(amount) as additional_comp
    from sls.additional_comp
    where thru_date > (
      select first_of_month
      from sls.months
      where open_closed = 'open')
    group by employee_number) e on a.employee_number = e.employee_number  
  left join sls.personnel f on a.employee_number = f.employee_number
  left join ( -- if this is the first month of employment, multiplier = remaining wd in month since hire date/work days in month
    select a.employee_number, 
      round(wd_of_month_remaining::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as guarantee_multiplier 
    from sls.personnel a
    inner join dds.dim_date b on a.start_date = b.the_date
      and b.year_month = _year_month) g on f.employee_number = g.employee_number    
  left join ( -- if employee termed mid month, multiplier elapsed wd in month at term date/work days in month
    select a.employee_number, 
      round(wd_of_month_elapsed::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as term_guarantee_multiplier
    from sls.personnel a
    inner join dds.dim_date b on a.end_date = b.the_date
      and b.year_month = _year_month ) h on f.employee_number = h.employee_number                   
  where a.year_month = _year_month
  order by left(a.employee_number, 1), a.last_name;
end $$;
     
---------------------------------------------------------------------
--/> PART 1 payroll mgr
---------------------------------------------------------------------
  

---------------------------------------------------------------------
--< PART 1 deals mgr
---------------------------------------------------------------------
-- this gives me the deals & the count
select * from sls.ry1_ry2_201912_part_1_deals

delete from sls.ry1_ry2_201912_part_1_deals;
insert into sls.ry1_ry2_201912_part_1_deals
select a.store_code, a.last_name, a.first_name, b.stock_number, coalesce(b.unit_count, 0), sum(unit_count) over (partition by last_name)
-- select *
from sls.personnel a
left join (
  select a.psc_employee_number, 
    case
      when ssc_last_name = 'none' then unit_count
      else 0.5 * unit_count
    end as unit_count, stock_number
  from sls.deals_by_month a
  where a.year_month = 201912
  union all
  select a.ssc_employee_number, 
    0.5 * unit_count as unit_count, stock_number
  from sls.deals_by_month a
  where a.year_month = 201912
    and a.ssc_last_name <> 'none') b on a.employee_number = b.psc_employee_number 
where end_date > current_Date    
  and last_name not in ('aubol', 'bedney', 'stout', 'HSE' ,'langenstein','Dockendorf','haley','knudson',
    'longoria', 'michael', 'seay','shirek', 'calcaterra', 'tweten')
order by a.store_code, a.last_name;
---------------------------------------------------------------------
--/> PART 1 deals mgr
---------------------------------------------------------------------


---------------------------------------------------------------------
--< PART 2 kim
---------------------------------------------------------------------
  on wednesday morning, 1/1/20, populate all the part 2 tables


select * from sls.ry1_201912_part_2_payroll_kim

-- this is taken verbatim from FUNCTION sls.json_get_payroll_for_kim(_store citext)
do $$
begin 
  delete from sls.ry1_201912_part_2_payroll_kim;
  insert into sls.ry1_201912_part_2_payroll_kim
  with
    open_month as (
      select year_month
      from sls.months
      where open_closed = 'open')
  select a.last_name || ', ' || a.first_name as consultant, a.employee_number, 
    a.pto_pay as "PTO (74)",
    coalesce(d.adjusted_amount, 0) as "Other - Adjustments (79)",
    coalesce(e.additional_comp, 0) as "Other - Addtl Comp (79)",
    round (
      case -- paid spiffs deducted only if base guarantee
        when a.total_earned >= a.guarantee then
          a.unit_pay - coalesce(a.draw, 0)
        else
          (a.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) - coalesce(c.spiffs, 0)
      end, 2) as "Unit Commission less draw (79)",
      a.fi_pay as "F&I Comm (79A)",   
      a.pulse as "Pulse (79B)",
      round( 
        case -- paid spiffs deducted only if base guarantee
          when a.total_earned >= a.guarantee then
            a.total_earned - coalesce(a.draw, 0) + coalesce(d.adjusted_amount, 0) + coalesce(e.additional_comp, 0)
          else
            (a.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) 
                - coalesce(c.spiffs, 0) + coalesce(e.additional_comp, 0) + coalesce(d.adjusted_amount, 0)
          end, 2) as "Total Month End Payout"      
  from sls.consultant_payroll a
  left join sls.pto_intervals b on a.employee_number = b.employee_number
    and a.year_month = b.year_month
  left join sls.paid_by_month c on a.employee_number = c.employee_number
    and a.year_month = c.year_month
  left join (
    select employee_number, sum(amount) as adjusted_amount
    from sls.payroll_adjustments
    where year_month =  (select year_month from open_month)
    group by employee_number) d on a.employee_number = d.employee_number
  left join (
    select employee_number, sum(amount) as additional_comp
    from sls.additional_comp
    where thru_date > (
      select first_of_month
      from sls.months
      where open_closed = 'open')
    group by employee_number) e on a.employee_number = e.employee_number  
  left join sls.personnel f on a.employee_number = f.employee_number
  left join ( -- if this is the first month of employment, multiplier = days worked/days in month
  select a.employee_number, 
    round(wd_of_month_remaining::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as guarantee_multiplier 
  from sls.personnel a
  inner join dds.dim_date b on a.start_date = b.the_date
    and b.year_month = (select year_month from open_month)) g on f.employee_number = g.employee_number      
  left join ( -- if employee termed mid month, multiplier elapsed wd in month at term date/work days in month
    select a.employee_number, 
      round(wd_of_month_elapsed::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as term_guarantee_multiplier
    from sls.personnel a
    inner join dds.dim_date b on a.end_date = b.the_date
      and b.year_month = (select year_month from open_month) ) h on f.employee_number = h.employee_number         
  where a.year_month = (select year_month from open_month)
    and f.store_code = 'RY1'
  order by consultant;
end $$;  


-- select * from sls.ry2_201912_part_2_payroll_kim
do $$
begin  
  delete from sls.ry2_201912_part_2_payroll_kim;
  insert into sls.ry2_201912_part_2_payroll_kim
  with
    open_month as (
      select year_month
      from sls.months
      where open_closed = 'open')
  select a.last_name || ', ' || a.first_name as consultant, a.employee_number, 
    a.pto_pay as "PTO (74)",
    coalesce(d.adjusted_amount, 0) as "Other - Adjustments (79)",
    coalesce(e.additional_comp, 0) as "Other - Addtl Comp (79)",
    round (
      case -- paid spiffs deducted only if base guarantee
        when a.total_earned >= a.guarantee then
          a.unit_pay - coalesce(a.draw, 0)
        else
          (a.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) - coalesce(c.spiffs, 0)
      end, 2) as "Unit Commission less draw (79)",
      a.fi_pay as "F&I Comm (79A)",   
      a.pulse as "Pulse (79B)",
      round( 
        case -- paid spiffs deducted only if base guarantee
          when a.total_earned >= a.guarantee then
            a.total_earned - coalesce(a.draw, 0) + coalesce(d.adjusted_amount, 0) + coalesce(e.additional_comp, 0)
          else
            (a.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) 
                - coalesce(c.spiffs, 0) + coalesce(e.additional_comp, 0) + coalesce(d.adjusted_amount, 0)
          end, 2) as "Total Month End Payout"      
  from sls.consultant_payroll a
  left join sls.pto_intervals b on a.employee_number = b.employee_number
    and a.year_month = b.year_month
  left join sls.paid_by_month c on a.employee_number = c.employee_number
    and a.year_month = c.year_month
  left join (
    select employee_number, sum(amount) as adjusted_amount
    from sls.payroll_adjustments
    where year_month =  (select year_month from open_month)
    group by employee_number) d on a.employee_number = d.employee_number
  left join (
    select employee_number, sum(amount) as additional_comp
    from sls.additional_comp
    where thru_date > (
      select first_of_month
      from sls.months
      where open_closed = 'open')
    group by employee_number) e on a.employee_number = e.employee_number  
  left join sls.personnel f on a.employee_number = f.employee_number
  left join ( -- if this is the first month of employment, multiplier = days worked/days in month
  select a.employee_number, 
    round(wd_of_month_remaining::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as guarantee_multiplier 
  from sls.personnel a
  inner join dds.dim_date b on a.start_date = b.the_date
    and b.year_month = (select year_month from open_month)) g on f.employee_number = g.employee_number      
  left join ( -- if employee termed mid month, multiplier elapsed wd in month at term date/work days in month
    select a.employee_number, 
      round(wd_of_month_elapsed::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as term_guarantee_multiplier
    from sls.personnel a
    inner join dds.dim_date b on a.end_date = b.the_date
      and b.year_month = (select year_month from open_month) ) h on f.employee_number = h.employee_number         
  where a.year_month = (select year_month from open_month)
    and f.store_code = 'RY2'
  order by consultant;
end $$;  

-- create the spreadsheet from this query, 0 out negative month end
select a.consultant, a.employee_number, 
  a."PTO (74)" - b."PTO (74)" as "PTO (74)",
  a."Other - Adjustments (79)" - b."Other - Adjustments (79)" as "Other - Adjustments (79)",
  a."Other - Addtl Comp (79)" - b."Other - Addtl Comp (79)" as "Other - Addtl Comp (79)",
  a."Unit Commission less draw (79)" - b."Unit Commission less draw (79)" as "Unit Commission less draw (79)",
  a."F&I Comm (79A)" - b."F&I Comm (79A)" as "F&I Comm (79A)",
  a."Pulse (79B)" - b."Pulse (79B)" as "Pulse (79B)",
  a."Total Month End Payout" - b."Total Month End Payout"  as "Total Month End Payout"  
from sls.ry1_201912_part_2_payroll_kim a
join sls.ry1_201912_part_1_payroll_kim b on a.consultant = b.consultant

select 1 as part, a.* from sls.ry1_201912_part_1_payroll_kim a 
union all
select 2, b.* from sls.ry1_201912_part_2_payroll_kim b
order by consultant, part

select a.consultant, a.employee_number, 
  a."PTO (74)" - b."PTO (74)" as "PTO (74)",
  a."Other - Adjustments (79)" - b."Other - Adjustments (79)" as "Other - Adjustments (79)",
  a."Other - Addtl Comp (79)" - b."Other - Addtl Comp (79)" as "Other - Addtl Comp (79)",
  a."Unit Commission less draw (79)" - b."Unit Commission less draw (79)" as "Unit Commission less draw (79)",
  a."F&I Comm (79A)" - b."F&I Comm (79A)" as "F&I Comm (79A)",
  a."Pulse (79B)" - b."Pulse (79B)" as "Pulse (79B)",
  a."Total Month End Payout" - b."Total Month End Payout"  as "Total Month End Payout"  
from sls.ry2_201912_part_2_payroll_kim a
join sls.ry2_201912_part_1_payroll_kim b on a.consultant = b.consultant


select 1 as part, a.* from sls.ry2_201912_part_1_payroll_kim a 
union all
select 2, b.* from sls.ry2_201912_part_2_payroll_kim b
order by consultant, part


for both stores, stores the original query output in the csv
modified the xls to make math sense
---------------------------------------------------------------------
--/> PART 2 kim
---------------------------------------------------------------------


---------------------------------------------------------------------
--< PART 2 payroll mgr
---------------------------------------------------------------------
-- taken from FUNCTION sls.json_get_payroll_for_submittal()
-- matches the vision payroll submittal page
-- removed a few fields

select * from sls.ry1_ry2_201912_part_2_payroll_mgr

do $$
declare
  _year_month integer := 201912;
begin
  delete from sls.ry1_ry2_201912_part_2_payroll_mgr;
  insert into sls.ry1_ry2_201912_part_2_payroll_mgr
  select 
    case
      when left(a.employee_number, 1) = '1' then 'RY1'
      else 'RY2'
    end as store, a.last_name, a.first_name, a.unit_count as units,
    a.unit_pay, a.fi_gross, a.chargebacks, a.fi_total, a.fi_pay, coalesce(a.pto_hours, 0) as pto_hours,
    coalesce(a.pto_rate, b.pto_rate) as pto_rate, a.pto_pay, a.pulse, --coalesce(c.spiffs, 0) as spiffs, 
    a.total_earned, a.draw, 
    round(a.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1), 2) as guarantee, 
    coalesce(d.adjusted_amount, 0) as adjusted_amount,
    coalesce(e.additional_comp, 0) as additional_comp,
    round (
      case -- paid spiffs deducted only if base guarantee
        when a.total_earned >= a.guarantee then
          a.total_earned - coalesce(a.draw, 0) + coalesce(d.adjusted_amount, 0) + coalesce(e.additional_comp, 0)
        else
          (a.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) 
              - coalesce(c.spiffs, 0) + coalesce(e.additional_comp, 0) + coalesce(d.adjusted_amount, 0)
        end, 2) as total_due
  from sls.consultant_payroll a
  left join sls.pto_intervals b on a.employee_number = b.employee_number
    and a.year_month = b.year_month
  left join sls.paid_by_month c on a.employee_number = c.employee_number
    and a.year_month = c.year_month
  left join (
    select employee_number, sum(amount) as adjusted_amount
    from sls.payroll_adjustments
    where year_month =  _year_month
    group by employee_number) d on a.employee_number = d.employee_number
  left join (
    select employee_number, sum(amount) as additional_comp
    from sls.additional_comp
    where thru_date > (
      select first_of_month
      from sls.months
      where open_closed = 'open')
    group by employee_number) e on a.employee_number = e.employee_number  
  left join sls.personnel f on a.employee_number = f.employee_number
  left join ( -- if this is the first month of employment, multiplier = remaining wd in month since hire date/work days in month
    select a.employee_number, 
      round(wd_of_month_remaining::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as guarantee_multiplier 
    from sls.personnel a
    inner join dds.dim_date b on a.start_date = b.the_date
      and b.year_month = _year_month) g on f.employee_number = g.employee_number    
  left join ( -- if employee termed mid month, multiplier elapsed wd in month at term date/work days in month
    select a.employee_number, 
      round(wd_of_month_elapsed::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as term_guarantee_multiplier
    from sls.personnel a
    inner join dds.dim_date b on a.end_date = b.the_date
      and b.year_month = _year_month ) h on f.employee_number = h.employee_number                   
  where a.year_month = _year_month
  order by left(a.employee_number, 1), a.last_name;
end $$;


-- just like with kims
-- generate spreadsheet with this, 0 out negative due in january amounts
-- save the original csv
-- modify the xlsx
select a.*, 
  case
    when a.store = 'RY1' then b."Total Month End Payout"
    when a.store = 'RY2' then c."Total Month End Payout"
  end as paid_in_december, 
  case
    when a.store = 'RY1' then a.total_due - b."Total Month End Payout"
    when a.store = 'RY2' then a.total_due - c."Total Month End Payout"
  end as due_in_january
from sls.ry1_ry2_201912_part_2_payroll_mgr a
left join sls.ry1_201912_part_1_payroll_kim b on a.last_name || ', ' || a.first_name = b.consultant   
left join sls.ry2_201912_part_1_payroll_kim c on a.last_name || ', ' || a.first_name = c.consultant  
---------------------------------------------------------------------
--/> PART 2 payroll mgr
---------------------------------------------------------------------
  


---------------------------------------------------------------------
--< PART 1 deals mgr
---------------------------------------------------------------------
-- this gives me the deals & the count
select * from sls.ry1_ry2_201912_part_1_deals

delete from sls.ry1_ry2_201912_part_1_deals;
insert into sls.ry1_ry2_201912_part_1_deals
select a.store_code, a.last_name, a.first_name, b.stock_number, coalesce(b.unit_count, 0), sum(unit_count) over (partition by last_name)
-- select *
from sls.personnel a
left join (
  select a.psc_employee_number, 
    case
      when ssc_last_name = 'none' then unit_count
      else 0.5 * unit_count
    end as unit_count, stock_number
  from sls.deals_by_month a
  where a.year_month = 201912
  union all
  select a.ssc_employee_number, 
    0.5 * unit_count as unit_count, stock_number
  from sls.deals_by_month a
  where a.year_month = 201912
    and a.ssc_last_name <> 'none') b on a.employee_number = b.psc_employee_number 
where end_date > current_Date    
  and last_name not in ('aubol', 'bedney', 'stout', 'HSE' ,'langenstein','Dockendorf','haley','knudson',
    'longoria', 'michael', 'seay','shirek', 'calcaterra', 'tweten')
order by a.store_code, a.last_name;
---------------------------------------------------------------------
--/> PART 1 deals mgr
---------------------------------------------------------------------



---------------------------------------------------------------------
--< PART 2 deals mgr
---------------------------------------------------------------------
insert into sls.ry1_ry2_201912_part_2_deals
select a.store_code, a.last_name, a.first_name, b.stock_number, coalesce(b.unit_count, 0), sum(unit_count) over (partition by last_name)
-- select *
from sls.personnel a
left join (
  select a.psc_employee_number, 
    case
      when ssc_last_name = 'none' then unit_count
      else 0.5 * unit_count
    end as unit_count, stock_number
  from sls.deals_by_month a
  where a.year_month = 201912
  union all
  select a.ssc_employee_number, 
    0.5 * unit_count as unit_count, stock_number
  from sls.deals_by_month a
  where a.year_month = 201912
    and a.ssc_last_name <> 'none') b on a.employee_number = b.psc_employee_number 
where end_date > current_Date    
  and last_name not in ('aubol', 'bedney', 'stout', 'HSE' ,'langenstein','Dockendorf','haley','knudson',
    'longoria', 'michael', 'seay','shirek', 'calcaterra', 'tweten')
order by a.store_code, a.last_name




-- part 1
select distinct 1 as seq, a.store_code, a.last_name, a.first_name, null, null::numeric, sum
from sls.ry1_ry2_201912_part_1_deals a
-- order by store_code, last_name
union
select 2 as seq, a.store_code, a.last_name, null, stock_number, unit_count, null
from sls.ry1_ry2_201912_part_1_deals a
order by store_code, last_name, seq




-- part 2
select distinct 1 as seq, a.store_code, a.last_name, a.first_name, null, null::numeric, sum
from sls.ry1_ry2_201912_part_2_deals a
where not exists (
  select 1
  from sls.ry1_ry2_201912_part_1_deals
  where last_name = a.last_name
    and stock_number = a.stock_number)
union
select 2 as seq, a.store_code, a.last_name, null, stock_number, unit_count, null
from sls.ry1_ry2_201912_part_2_deals a
where not exists (
  select 1
  from sls.ry1_ry2_201912_part_1_deals
  where last_name = a.last_name
    and stock_number = a.stock_number)
order by store_code, last_name, seq

---------------------------------------------------------------------
--/> PART 1 deals mgr
---------------------------------------------------------------------
