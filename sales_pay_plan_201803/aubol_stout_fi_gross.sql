﻿-- 2/1/19  nick wants their f/i gross by month for 2018
-- this is what i sent him
select a.year_month, 
  (sum(a.unit_count * b.fi_gross) filter (where a.fi_last_name = 'Aubol'))::integer as Aubol,
  (sum(a.unit_count * b.fi_gross) filter (where a.fi_last_name = 'Stout'))::integer as Stout
from sls.deals_by_month a
join sls.deals_gross_by_month b on a.stock_number = b.control
where a.year_month between 201801 and 201812
  and a.fi_last_name in ('stout','aubol')
  and b.fi_gross <> 0
group by a.year_month
order by a.year_month

-----------------------------------------------------------
select year_month, fi_last_name, count(*)
from sls.deals_by_month
where year_month between 201801 and 201807
  and fi_last_name in ('aubol','stout')
group by year_month, fi_last_name
order by fi_last_name, year_month

drop table if exists deals;
create temp table deals as
select a.year_month, a.stock_number, a.unit_count, a.fi_last_name
from sls.deals_by_month a
where a.year_month between 201801 and 201807
  and a.fi_last_name in ('aubol','stout');


drop table if exists acct_detail;
create temp table acct_detail as
  select c.year_month, a.control,
    aa.journal_code, b.account_type, b.department,
    d.store_code, d.page, d.line, d.gl_Account, b.description as account_description,
    e.description as trans_description,
    max(
      case
        when page = 17 and line in (3,13) then 'fi_chargeback'
        when page = 17 then 'fi'
        else 'front'
      end) as gross_category,
    sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
  inner join dds.dim_date c on a.date_key = c.date_key
  inner join fin.dim_account b on a.account_key = b.account_key
    and c.the_date between b.row_from_Date and b.row_thru_date
  inner join sls.deals_accounts_routes d on b.account = d.gl_account
    and d.page = 17
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where post_status = 'Y'
    and c.year_month between 201801 and 201807
  group by b.description, c.year_month, a.control,
    aa.journal_code, b.account_type, b.department,
    d.store_code, d.page, d.line, d.gl_Account, e.description;  

select * from acct_detail

drop table if exists chargebacks;
create temp table chargebacks as
  select year_month, control, sum(amount) as amount, store_code, fi_last_name,
    delivery_date, source
  from (
    select a.year_month, a.control, -a.amount as amount, 
      b.store_code, coalesce(d.employee_number, 'N/A') as fi_last_name, 
      b.delivery_date, 'pipeline' as source
    from acct_detail a
    inner join sls.deals b on a.control = b.stock_number
    left join ads.ext_dim_customer c on b.buyer_bopname_id = c.bnkey
    left join sls.personnel d on b.fi_manager = d.fi_id
    where a.gross_category = 'fi_chargeback'
      and not exists (
        select 1
        from sls.deals
        where stock_number = b.stock_number
          and seq > 1)
      and delivery_date >= ( -- 6 month rule     
        select (first_of_month - interval '6 month')::date as the_date
        from sls.months where year_month = a.year_month)
    union
    select distinct year_month, control, amount, store_code, 
      coalesce(employee_number, 'N/A'), delivery_date, 'pipeline'
    from (
      --eliminate same month back on
      -- inner join on the_bopmast_id cte eliminates those deals for which the sum
      -- of the unit count in the same month is 0
      with
        the_bopmast_id as (
          select bopmast_id
          from acct_detail a
          inner join sls.deals b on a.control = b.stock_number
          left join ads.ext_dim_customer c on b.buyer_bopname_id = c.bnkey
          left join sls.personnel d on b.fi_manager = d.fi_id
          where a.gross_category = 'fi_chargeback'
            and exists (
              select 1
              from sls.deals
              where stock_number = b.stock_number
                and seq > 1)
          group by b.bopmast_id, b.year_month
          having sum(unit_count) <> 0)  
      select a.year_month, a.control, -a.amount as amount, 
        b.store_code, b.fi_manager, d.employee_number, 'pipeline',
        b.delivery_date, b.bopmast_id,  unit_count
      from acct_detail a
      inner join sls.deals b on a.control = b.stock_number
      inner join the_bopmast_id bb on b.bopmast_id = bb.bopmast_id
      left join ads.ext_dim_customer c on b.buyer_bopname_id = c.bnkey
      left join sls.personnel d on b.fi_manager = d.fi_id
      where a.year_month = (select year_month from sls.months where open_closed = 'open')
        and a.gross_category = 'fi_chargeback'
        and exists (
          select 1
          from sls.deals
          where stock_number = b.stock_number
            and seq > 1)
        and delivery_date >= ( -- 6 month rule     
          select (first_of_month - interval '6 month')::date as the_date
          from sls.months where year_month = a.year_month)) gl) aa  
  group by year_month, control, store_code, fi_last_name, delivery_date, source;     



select *
from ( 
select a.year_month, a.fi_last_name, 
  sum(-b.amount) filter (where gross_category = 'fi') as fi_gross
from deals a  
left join acct_detail b on a.stock_number = b.control
group by a.year_month, a.fi_last_name) aa
left join (
select a.year_month, a.fi_last_name,  sum(b.amount) as chargebacks
from deals a
left join chargebacks b on a.stock_number = b.control
group by a.year_month, a.fi_last_name) bb on aa.year_month = bb.year_month and aa.fi_last_name = bb.fi_last_name



select aa.fi_last_name as fi, aa.year_month, sum(fi_gross + chargebacks) as fi_gross
from ( 
  select a.year_month, a.fi_last_name, 
    sum(-b.amount) filter (where gross_category = 'fi') as fi_gross
  from deals a  
  left join acct_detail b on a.stock_number = b.control
  group by a.year_month, a.fi_last_name) aa
left join (
  select a.year_month, a.fi_last_name,  sum(b.amount) as chargebacks
  from deals a
  left join chargebacks b on a.stock_number = b.control
  group by a.year_month, a.fi_last_name) bb on aa.year_month = bb.year_month and aa.fi_last_name = bb.fi_last_name
group by aa.fi_last_name, aa.year_month


select aa.year_month, 
  sum(fi_gross + chargebacks) filter (where aa.fi_last_name = 'aubol') as aubol,
  sum(fi_gross + chargebacks) filter (where aa.fi_last_name = 'stout') as stout
from ( 
  select a.year_month, a.fi_last_name, 
    sum(-b.amount) filter (where gross_category = 'fi') as fi_gross
  from deals a  
  left join acct_detail b on a.stock_number = b.control
  group by a.year_month, a.fi_last_name) aa
left join (
  select a.year_month, a.fi_last_name,  sum(b.amount) as chargebacks
  from deals a
  left join chargebacks b on a.stock_number = b.control
  group by a.year_month, a.fi_last_name) bb on aa.year_month = bb.year_month and aa.fi_last_name = bb.fi_last_name
group by aa.year_month


-- 7/31 jeri/nick/ben interested only in FS level gross, no chargebacks, just lines 1,2,7,11,12,17
-- believe this has to with tom/ricks pay plan
-- so, base needs to be accounting, then fetch fi mgr from relevant deals

select * from acct_detail limit 100

select year_month, store_code, line, sum(amount)
from acct_detail
group by year_month, store_code, line
order by store_code, year_month, line

drop table if exists acct_detail;
create temp table acct_detail as
  select c.year_month, a.control, a.doc,
    aa.journal_code, b.account_type, b.department,
    d.store_code, d.page, d.line, d.gl_Account, b.description as account_description,
    e.description as trans_description,
    sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
  inner join dds.dim_date c on a.date_key = c.date_key
  inner join fin.dim_account b on a.account_key = b.account_key
    and c.the_date between b.row_from_Date and b.row_thru_date
  inner join sls.deals_accounts_routes d on b.account = d.gl_account
    and d.page = 17
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where post_status = 'Y'
    and c.year_month = 201807
  group by b.description, c.year_month, a.control, a.doc,
    aa.journal_code, b.account_type, b.department,
    d.store_code, d.page, d.line, d.gl_Account, e.description;  

select line, sum(amount) from acct_detail group by line order by line
-- all the line 6/16 are honda deals
select * 
from acct_detail
where line in (6,16)
  and amount <> 0
  
select a.year_month, 
  (sum(-a.amount) filter (where fi_last_name = 'aubol'))::integer as aubol,
  (sum(-a.amount) filter (where fi_last_name = 'stout'))::integer as stout
from acct_detail a
inner join sls.deals_by_month b on a.control = b.stock_number
where a.line in (1,2,7,11,12,17)
  and b.fi_last_name in ('aubol','stout')
group by a.year_month


    