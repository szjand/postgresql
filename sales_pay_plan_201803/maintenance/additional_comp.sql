﻿create table sls.additional_comp (
  employee_number citext not null,
  thru_date date not null default '12/31/9999'::date,
  category citext not null,
  from_date date not null,
  amount numeric(8,2),
  primary key (employee_number,category,thru_date));

insert into sls.additional_comp (employee_number,category,from_date,amount)
select employee_number, 'mentoring new consultants', '03/01/2018', 700
from sls.personnel
where last_name = 'warmack';  

select * 
from sls.additional_comp

-- per Nick on 4/24, Jim gets $800/month

update sls.additional_comp
set amount = 800
where employee_number = (
  select employee_number
  from sls.personnel
  where last_name = 'warmack');

-- per jared, corey wilde $1000/month  
Hey Jon,
The breakdown for Corey was $600/mo for being Nissan Brand Manager 
and $400/mo for being a floor mentor.  If you need anything else please let me know.
JARED LANGENSTEIN

do
$$
declare
_employee_number citext := (select employee_number from sls.personnel where last_name = 'wilde');
_category citext := 'Nissan Brand Manager';
_from_date date := '03/01/2018';
_amount numeric(8,2) := 600;
begin
insert into sls.additional_comp (employee_number,category,from_date,amount)
values
(_employee_number, _category, _from_date, _amount);
end
$$;