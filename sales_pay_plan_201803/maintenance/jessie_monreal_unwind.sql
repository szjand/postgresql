﻿select *
from sls.deals
where stock_number = 'h11080t'

select *
from sls.deals_by_month
where year_month = 201805  
  and psc_last_name = 'monreal'
  or ssc_last_name = 'monreal'
  order by stock_number



delete
from sls.deals_by_month  
where stock_number = 'h11080t'



    select -- store's units & other store's units sold by store's consultants 
      -- eg RY1 units sold by RY1 consultants & RY2 units sold by RY1 consultants
      sum(unit_count) - sum(unit_count) filter (where deal_store <> sc_store) as total_units,
      sum(unit_count) filter (where deal_store <> sc_store) as total_other_units
    from (  
      select a.psc_employee_number, a.store_code as deal_store, b.store_code as sc_store,
        case
          when ssc_last_name = 'none' then a.unit_count
          else 0.5 * a.unit_count
        end as unit_count
      from sls.deals_by_month a
      inner join sls.personnel b on a.psc_employee_number = b.employee_number
        and b.employee_number <> 'HSE'    
      inner join sls.consultant_payroll c on b.employee_number = c.employee_number  
        and a.year_month = c.year_month    
      where a.year_month = 201805
      union all 
      select a.ssc_employee_number, a.store_code as deal_store, b.store_code as sc_store,
        0.5 * a.unit_count as unit_count
      from sls.deals_by_month a
      inner join sls.personnel b on a.ssc_employee_number = b.employee_number
        and b.employee_number <> 'HSE'     
      inner join sls.consultant_payroll c on b.employee_number = c.employee_number   
        and a.year_month = c.year_month    
      where a.year_month =201805
        and a.ssc_last_name <> 'none') x
    where deal_store = 'ry2'


select *
from sls.consultant_payroll
where year_month = 201805
  and last_name = 'monreal'

update sls.consultant_payroll
  set unit_count = 11.0,
      unit_pay = 3300.00,
      total_earned = 3300.00,
      due_at_month_end = 2800.00
where last_name = 'monreal'
  and year_month = 201805  