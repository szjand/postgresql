﻿the confusion, at least in my mind, is the verbage: pad spiffs deducted only if pay based on base guarantee
6/26/21
	!!! holy shit, it appears that the spiffs entered on the payroll page are not stored anywhere !!!
	afton is vacationing this weekend, looks like we weill have to fix this on monday
	looks like i will move on to accrual
	
06/24/21
	talked to nick, apparently thier are 2 types of spiff
		cash: paid on the spot, taylor notifies kim who only enters taxes into payroll
		other: submitted on vision, also somehow kim is notified, she includes them on the paycheck
-- 			end of may payroll, jordyn greer 200 spiff
			select a.description, a.code_id, a.amount, b.employee_name, c.total_gross_pay
			from arkona.ext_pyhscdta a
			join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
			join arkona.ext_pyhshdta c on a.payroll_run_number = c.payroll_run_number
				and a.employee_number = c.employee_
			where a.payroll_run_number = 531210
				and a.code_id = '82'

06/25/21
	reached out to kim, need to make sure i understand her view as well
	no response, will have to wait until monday

in the meantime, rework existing functions based on this understanding:
		forget the deduction of paid spiffs (cash), have no way of knowing what they are
		if nick chooses to spiff "under performing" consultants, so be it
		include spiffs in output ( 82 Spiff Pay Outs) for kims spreadsheet and in total
  		
functions that include spiff:

sls.json_get_payroll_for_kim(citext)
sls.json_get_payroll_for_submittal(citext)
sls.json_get_previous_payroll(citext, integer)
sls.update_paid_by_month()

spiff is missing from sls.consultant_payroll (does it belong there?)

-- this table is based solely on pyhshdtta & pyhscdta
select * from sls.paid_by_month where year_month = 202105

-- json_get_payroll_for_kim
the inclusion of sls.paid_by_month, used for deducdtion spiffs doesnt make any sense
	that table is built upon pyhshdta and pyhscdta exclusively, so how can the current paycheck
	presumably, in the past, kim would issue a spiff check in the course of the month, therefor,
	at the end of the month, there would be "spiff checks" for the current month which could be deducted

do $$
declare
	_store citext := 'RY1';
begin
drop table if exists kim_test;
create temp table kim_test as
	with open_month as (	
		select 202105 as year_month)
	select a.last_name || ', ' || a.first_name as consultant, a.employee_number, 
		a.pto_pay as "PTO (74)",
		coalesce(d.adjusted_amount, 0) as "Other - Adjustments (79)",
		coalesce(e.additional_comp, 0) as "Other - Addtl Comp (79)",
		round (
			case -- paid spiffs deducted only if base guarantee
				when a.total_earned >= a.guarantee then
					a.unit_pay - coalesce(a.draw, 0)
				else
					(a.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) - coalesce(c.spiffs, 0)
			end, 2) as "Unit Commission less draw (79)",
			a.fi_pay as "F&I Comm (79A)",   
			a.pulse as "Pulse (79B)",
			a.spiff,
			round( 
				case -- paid spiffs deducted only if base guarantee
					when a.total_earned >= a.guarantee then
						a.total_earned - coalesce(a.draw, 0) + coalesce(d.adjusted_amount, 0) + coalesce(e.additional_comp, 0)
					else
						(a.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) 
								- coalesce(c.spiffs, 0) + coalesce(e.additional_comp, 0) + coalesce(d.adjusted_amount, 0)
					end, 2) as "Total Month End Payout"      
	from sls.consultant_payroll a
	left join sls.pto_intervals b on a.employee_number = b.employee_number
		and a.year_month = b.year_month
	left join sls.paid_by_month c on a.employee_number = c.employee_number
		and a.year_month = c.year_month
	left join (
		select employee_number, sum(amount) as adjusted_amount
		from sls.payroll_adjustments
		where year_month =  (select year_month from open_month)
		group by employee_number) d on a.employee_number = d.employee_number
	left join (
		select employee_number, sum(amount) as additional_comp
		from sls.additional_comp
		where thru_date > (
			select first_of_month
			from sls.months
			where open_closed = 'open')
		group by employee_number) e on a.employee_number = e.employee_number  
	left join sls.personnel f on a.employee_number = f.employee_number
	left join ( -- if this is the first month of employment, multiplier = days worked/days in month
	select a.employee_number, 
		round(wd_of_month_remaining::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as guarantee_multiplier 
	from sls.personnel a
	inner join dds.dim_date b on a.start_date = b.the_date
		and b.year_month = (select year_month from open_month)) g on f.employee_number = g.employee_number      
	left join ( -- if employee termed mid month, multiplier elapsed wd in month at term date/work days in month
	select a.employee_number, 
		round(wd_of_month_elapsed::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as term_guarantee_multiplier
	from sls.personnel a
	inner join dds.dim_date b on a.end_date = b.the_date
		and b.year_month = (select year_month from open_month) ) h on f.employee_number = h.employee_number         
	where a.year_month = (select year_month from open_month)
	and f.store_code = _store;
end $$;
select * from kim_test order by consultant;	

