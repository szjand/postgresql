﻿fa person in the role of product genius can sell vehicles, but is paid hourly

need to include them in deal tallies, but not in payroll
therefore
they need to be in  sls.personnel, but excluded from payroll
think i will add an attribute to sls.personnel, product_genius, boolean, that should provide the distinction i need
but then i  have to find all the places where it will matter for payroll
my first guess is sls.update_consultant_payroll()
they will also need to be excluded from payplans
will they also need a team designation?
need to get rid of the whole team  stuff

actually, maybe i can get by with no new attribute, add them to sls.personnel, but not 
to sls.team_personnel or sls.personnel_payplans and that will be enough to eliminate them from payroll

that will be version 1
add the role to the maintenance script

select * from sls.deals where stock_number = 't10288g'

select * from sls.personnel where ry8_id = 'RSA'

the table sls.personnel_roles would have worked, but does not look like i use it anywhere

select * 
from sls.payplans

-- 12/31/2022 ---------------------------------------------------------------------------------------------------

select * 
from ukg.employees
where first_name = 'ethan'

select * 
from ukg.ext_jobs

select * 
from ukg.employee_profiles
limit 43

select * 
from ukg.employees
where primary_Account_id = 12987528185

select * 
from  sls.personnel
where last_name = 'dubois'

select * 
from  sls.consultant_payroll
where last_name = 'dubois'

select * 
from ukg.ext_cost_center_job_details;


select * 
from ukg.pay_statement_earnings
limit 100

select distinct cost_centeR_job_name, cost_center_job_display_name
from ukg.pay_statement_earnings
order by cost_center_job_display_name

select * 
from ukg.pay_statement_earnings
where cost_center_job_name = 'product genius'


select * from ukg.get_cost_center_hierarchy()

select * from ukg.ext_employee_pay_info  order by employee_id

select b.last_name, b.first_name, b.cost_center, c.job_name 
from ukg.ext_employee_pay_info a
left join ukg.employees b on a.employee_id = b.primary_account_id
left join ukg.ext_jobs c on a.default_job_id = c.id
where b.term_date is null
order by b.cost_center, c.job_name


            "id": 55942755388,
            "job_category": "Sales",
            "name": "Product Genius",

select a.last_name, a.first_name, a.cost_center, b.default_job_id, c.*
from ukg.employees a
left join ukg.ext_employee_pay_info b on a.employee_id = b.employee_id
left join ukg.ext_jobs c on b.default_job_id = c.id
-- order by a.last_name, a.first_name
order by a.cost_center, c.job_name

select * 
from luigi.luigi_log
where pipeline = 'ukg'
  and the_date = current_date
  
select * 
from ukg.ext_employee_pay_info
limit 100

select * from ukg.ext_employee_details limit 100

-- 01/01/2023 -----------------------------------------------------------------------------------------------------------------

select * from ukg.employees limit 20

-- current product geniuses
select a.store, a.last_name, a.first_name, a.employee_number, a.cost_center, c.job_name, d.*
from ukg.employees a
left join ukg.ext_employee_pay_info b on a.employee_id = b.employee_id
left join ukg.ext_jobs c on b.default_job_id = c.id
left join sls.personnel d on a.employee_number = d.employee_number
where c.job_name = 'product genius'
  and a.term_date > current_date

select * from sls.personnel_roles  
select * from sls.team_roles
select * from sls.payplans
select * from sls.personnel
