﻿-- these 3 stay on their per unit pay, but now also get 10% gross
select *
from sls.personnel
where last_name in ('warmack','loven','croaker')

Croaker,Craig CRC   G45366
Loven,Arden   LOE   G45433PA
Warmack,James JWA   G45810

update sls.personnel
set fi_id = 'CRC'
where last_name = 'croaker';

update sls.personnel
set fi_id = 'LOE'
where last_name = 'Loven';

select * from sls.deals where stock_number = 'G45366'

-- fi is coalesced to none in sls.update_deals_by_month
select * from sls.deals_by_month where stock_number = 'G45366'

select a.stock_number, a.fi_manager, b.fi_sales, b.fi_cogs, b.fi_gross  
from sls.deals a
left join sls.deals_gross_by_month b on a.stock_number = b.control
where fi_manager in ('crc','loe','jwa')


select * 
from sls.per_unit_matrix

--- this is the part of FUNCTION sls.update_consultant_payroll() that generates the fi

      select aa.*, coalesce(bb.unit_count, 0) as unit_count,
        (select * from sls.per_unit_earnings (aa.employee_number, bb.unit_count)) as unit_pay, 
        case 
          when aa.payplan = 'executive' then coalesce(cc.fi_gross, 0)
          when aa.last_name in ('loven','croaker','warmack') then coalesce(cc.fi_gross, 0)
          else 0
        end as fi_gross, 
        case 
          when aa.payplan = 'executive' then coalesce(dd.amount, 0)
          when aa.last_name in ('loven','croaker','warmack') then coalesce(dd.amount, 0)
          else 0
        end as chargebacks,
        case 
          when aa.payplan = 'executive' then coalesce(dd.amount, 0) + coalesce(cc.fi_gross, 0)
          when aa.last_name in ('loven','croaker','warmack') then coalesce(dd.amount, 0) + coalesce(cc.fi_gross, 0)
          else 0
        end as fi_total, 
        case 
          when aa.payplan = 'executive' then round(.16 * (coalesce(dd.amount, 0) + coalesce(cc.fi_gross, 0)), 2)
          when aa.last_name in ('loven','croaker','warmack') then round(.1 * (coalesce(dd.amount, 0) + coalesce(cc.fi_gross, 0)), 2)
          else 0
        end as fi_pay,
        ee.pto_hours, ee.pto_rate, coalesce(ee.pto_pay, 0) as pto_pay,
        ee.hol_hours, ee.hol_rate, coalesce(ee.hol_pay, 0) as hol_pay
      from ( -- aa: cons, team, payplan
-- *c*      
        select b.team, a.last_name, a.first_name, a.employee_number, c.payplan
        from sls.personnel a
        inner join sls.team_personnel b on a.employee_number = b.employee_number
          and b.thru_date > (select first_of_month from sls.months where open_closed = 'open')
        inner join sls.personnel_payplans c on a.employee_number = c.employee_number
          and c.thru_date > (select first_of_month from sls.months where open_closed = 'open')) aa 
-- *a*
--         where a.store_code = 'RY1') aa
      left join ( -- bb: unit count
        select psc_employee_number, sum(unit_count) as unit_count
        from ( -- x
          select a.psc_employee_number, 
            case
              when ssc_last_name = 'none' then unit_count
              else 0.5 * unit_count
            end as unit_count
          from sls.deals_by_month a
          where a.year_month = 202211
          union all
          select a.ssc_employee_number, 
            0.5 * unit_count as unit_count
          from sls.deals_by_month a
          where a.year_month = 202211
            and a.ssc_last_name <> 'none') x
        group by psc_employee_number) bb  on aa.employee_number = bb.psc_employee_number
-- *d*  
      left join ( -- cc: fi gross
        select fi_employee_number, sum(fi_gross) as fi_gross
        from (    
          select a.fi_employee_number, a.fi_last_name, 
            case
              when c.payplan = 'executive' then b.fi_sales - b.fi_cogs
              else b.fi_sales - b.fi_cogs - b.fi_acq_fee
            end as fi_gross
          from sls.deals_by_month a
          left join sls.deals_gross_by_month b on a.stock_number = b.control
            and a.year_month = b.year_month
          left join sls.personnel_payplans c on a.fi_employee_number = c.employee_number
            and c.thru_date > current_date  
          where a.year_month = 202211) aaa
        group by aaa.fi_employee_number) cc on aa.employee_number = cc.fi_employee_number
      left join ( -- fi chargebacks
        select fi_employee_number, sum(amount) as amount
        from sls.fi_chargebacks
        where chargeback_year_month = 202211
        group by fi_employee_number) dd on aa.employee_number = dd.fi_employee_number
			left join ( -- ee
				select employee_number, pto_hours, hol_hours, coalesce(pto_rate, 0) as pto_rate, coalesce(hol_rate, 0) as hol_rate,
					round(pto_hours * coalesce(pto_rate, 0), 2) as pto_pay, round(hol_hours * hol_rate, 2) as hol_pay
				from (
					select aa.employee_number, aa.pto_hours, aa.holiday_hours as hol_hours, 
					  cc.current_rate as pto_rate, dd.current_rate as hol_rate
					from sls.clock_hours_by_month aa
					left join (
						select a.last_name, a.first_name, a.employee_number, c.current_rate
						from ukg.employees a
						join ukg.employee_profiles b on a.employee_id = b.employee_id
						join ukg.personal_rate_tables c on b.rate_table_id = c.rate_table_id
							and c.counter = 'paid time off') cc on aa.employee_number = cc.employee_number  	
					left join (
						select a.last_name, a.first_name, a.employee_number, c.current_rate
						from ukg.employees a
						join ukg.employee_profiles b on a.employee_id = b.employee_id
						join ukg.personal_rate_tables c on b.rate_table_id = c.rate_table_id
							and c.counter = 'holiday') dd on aa.employee_number = dd.employee_number   
					where aa.year_month = 202211) bb) ee on aa.employee_number = ee.employee_number