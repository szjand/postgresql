﻿/* what's covered, so far
1. new pay plan
2. employee leaves sales for a different dept
3. term
4. new consultant
4a. new product genius
4b. product genius to sales consultant
4c. change pay plan from sales consultant to single point
5. consultant changes teams
6. new team
7. change pay plan
8. consultant becomes team leader
9. consultant returns to sales after having moved from sales to admin
10. termed consultant returns to rydells as consultant
11. consultant changes store, and goes from consultant to manager (Nik Holland)
12. consultant changes store
13. consultant gets an F/I id
14. consultant went to bdc, then came back to sales
15. consultant left one store to become sales manager at another store
*/

select * from sls.payplans

-- current config
select a.store_code, b.team, a.employee_number, a.last_name, a.first_name, c.payplan, d.team_role,
  a.start_date, a.ry1_id, a.ry2_id, a.ry8_id, a.fi_id, a.first_full_month_emp, a.end_date
-- select *
from sls.personnel a
left join sls.team_personnel b on a.employee_number = b.employee_number
  and b.thru_date > (select first_of_month from sls.months where open_closed = 'open') -- current_date 
left join sls.personnel_payplans c on a.employee_number = c.employee_number
  and c.thru_date > (select first_of_month from sls.months where open_closed = 'open') -- current_date 
left join sls.personnel_roles d on a.employee_number = d.employee_number  
  and d.thru_date > (select first_of_month from sls.months where open_closed = 'open') -- current_date 
where a.end_date > (select first_of_month from sls.months where open_closed = 'open') -- current_date 
  and a.store_code = 'RY1'
order by a.store_code, a.last_name
order by a.store_code, b.team, d.team_role, a.last_name

update sls.personnel
set fi_id = 'PYU'
-- select * from sls.personnel
where last_name = 'yunker'

select *
from sls.team_personnel
where employee_number = '174141'

select * from sls.personnel where employee_number = '2100751'

-- create a new payplan for craig and assign him to it
select * from sls.json_get_deals_by_consultant_month('128530', 201804);

select * from sls.personnel where last_name = 'croaker'

insert into sls.payplans values('croaker');

insert into sls.per_unit_matrix values ('croaker',400,400,400,400);

update sls.personnel_payplans
set thru_date = '03/31/2018'
-- select * from sls.personnel_payplans
where employee_number = '128530';
insert into sls.personnel_payplans (employee_number,payplan,from_date)
values('128530','croaker','04/01/2018');

-- create a new payplan for stadstad and assign him to it
select * from sls.json_get_deals_by_consultant_month('128530', 201804);

select * from sls.personnel where last_name = 'stadstad' -- 1130690

insert into sls.payplans values('stadstad');

insert into sls.per_unit_matrix values ('stadstad',0,0,0,0);

update sls.personnel_payplans
set thru_date = '03/31/2018'
-- select * from sls.personnel_payplans
where employee_number = '1130690';
insert into sls.personnel_payplans (employee_number,payplan,from_date)
values('1130690','stadstad','04/01/2018');

-------------------------------------------------------------
--< larry stadstad
-------------------------------------------------------------
/*
1/31/2020
Please adjust Larry Stadstads pay to $350 per car flat. No guarantee. 
He is part time now with no guarantees. 
I noticed on vision page it still showed his $5000 salary.
Please adjust before submitting for January 2020. 
Thanks
Nick 
*/


select * -- 1130690
from sls.personnel
where last_name = 'stadstad'

select * from sls.payplans

select * from sls.per_unit_matrix

insert into sls.payplans values('stadstad_pt');

insert into sls.per_unit_matrix values ('stadstad_pt',350,350,350,350);

update sls.personnel_payplans
set thru_date = '12/31/2019'
-- select * from sls.personnel_payplans
where employee_number = '1130690'
  and thru_date > current_date;
insert into sls.personnel_payplans (employee_number,payplan,from_date)
values('1130690','stadstad_pt','01/01/2020');

weber & rumen new pay plan: single point plus: the plus is $50 more per unit

select * from sls.personnel where last_name in ('weber','rumen');  1147810, 141082

insert into sls.payplans(payplan,fi,chargeback,from_date,thru_date)
select 'single point plus', fi, chargeback, '01/01/2024', thru_date
from sls.payplans
where payplan = 'single point';

insert into sls.per_unit_matrix(payplan,level_1,level_2,level_3,level_4)
values('single point plus', 300, 325, 350, 375);

update sls.personnel_payplans
set thru_date = '12/31/2023'
-- select * from sls.personnel_payplans
where employee_number in ('1147810','141082')
  and thru_date > current_date;

insert into sls.personnel_payplans(employee_number,payplan,from_date,thru_date) values
('1147810','single point plus','01/01/2024','12/31/9999'),
('141082','single point plus','01/01/2024','12/31/9999');  
-------------------------------------------------------------
--/> larry stadstad
-------------------------------------------------------------

select * from dds.dim_date limit 10
-- carter ferry no longer in sales
-- leave him in personnel but close out the roles/teams/payplans tables
-- alan mulhern no longer in sales, remove him from sls.personnel
-- no, i don't like that either, let's add a thru_date to sls.personnel
-- alter table sls.personnel
-- add column end_date date not null default '12/31/9999';
-- is now the same as term 
-- brad longoria leaves sales 6/21/18
select * from arkona.xfm_pymast where employee_last_name = 'belgarde' and current_row = true

select * from sls.personnel where last_name = 'bedney' and first_name = 'jesse'
-------------------------------------------------------------------------
-- 3. term
-- consultants that term in the middle of the month, need to show for current month
-- but guarantee needs to be prorated based on end date, 
-- experiment with jon borgen
-- hopefully, sls.consultant_payroll will show "base" guarantee,
-- payroll page should show adjusted guarantee
-- 2/15/19 
-- james delaurier terms on 2/14, so sls.personnel.end_date = 2/14, 
-- the other thru_dates = 2/28
-- 06/03/20 catching up on some old managers no longer consultants
-- 10/30/20 from nick:
--       Can you delete Sam Foster out of the sales consultant payroll page please. 
--       I don’t want to submit it and have him on the spreadsheet.
--       Morgan is doing his payroll now that he is the BDC sales manager. 
-- to accomodate nick, i will term him on 9/30, though
-- this is what the payroll page showed before i termed him
-- CONSULTANT	    PLAN	    UNITS	UNIT PAY	F&I PAY	PTO PAY	SPIFFS	PULSE	TOT. EARNED	DRAW	GUAR.	    ADJ.	ADD. COMP.	DUE @ M.E.	
-- Foster, Samuel	executive	3	    $750.00	  $155.28	$0.00	  $0.00	  $0	  $905.28	    $0.00	$3,000.00	$0.00	$0.00	      $3,000.00
select sls.update_consultant_payroll() 
-- select * from sls.personnel where last_name = 'loven'
-------------------------------------------------------------------------
do
$$
declare 
   _emp_no citext := (select employee_number from sls.personnel where last_name = 'sabo' and first_name = 'mallorie'); 
   _thru_date date := '12/04/2023';
   _term_date date := '12/04/2023';
begin
  update sls.personnel
  set end_date = _term_date
  where employee_number = _emp_no;
  
  update sls.team_personnel
  set thru_date = _thru_date
  where employee_number = _emp_no
    and thru_date > current_date;

  update sls.personnel_roles
  set thru_date = _thru_date
  where employee_number = _emp_no
    and thru_date > current_date;  

  update sls.personnel_payplans
  set thru_date = _thru_date
  where employee_number = _emp_no
    and thru_date > current_date;    
end
$$;

select sls.update_consultant_payroll()   

select * from sls.personnel_payplans where employee_number = (select employee_number from sls.personnel where last_name = 'miller')

-- 3aa. mid month term, change just sls.personnel.end_date
select * from sls.personnel where last_name = 'vanyo'
update sls.personnel
set end_date = '07/14/2023'
where last_name = 'vanyo';

select * from sls.personnel where last_name = 'bocklage'
update sls.personnel
set end_date = '07/05/2023'
where last_name = 'bocklage';
-------------------------------------------------------------------------
-- 3a. rehire 

6/5/19 deion saldana, recently termed, rehired, this assumes anniversary remains as the original
do
$$
declare 
   _emp_no citext := (select employee_number from sls.personnel where last_name = 'gonzalez' and first_name = 'daniel'); 
   _thru_date date := '08/31/2022';
   _end_date date := '08/04/2022';
begin
  update sls.personnel
  set end_date = _end_date
  where employee_number = _emp_no;
  
  update sls.team_personnel
  set thru_date = _thru_date
  where employee_number = _emp_no;

  update sls.personnel_roles
  set thru_date = _thru_date
  where employee_number = _emp_no;  

  update sls.personnel_payplans
  set thru_date = _thru_date
  where employee_number = _emp_no
    and thru_date = '12/31/9999';    
end
$$;

1/1/21 nicholas nelson, new anniversary, assuming id will remain the same, dont know yet (fucking IT)
1/12/01 nope, also, decided to make first full month 210101
select * from sls.personnel where employee_number = '2100751'
1/13/01 shit, looks like nelson is still using NNE, modified car_deals.py  to handle the 2 ids, emailed nick to fix crm
update sls.personnel
set ry1_id = 'NIN', ry2_id = 'NIN', first_full_month_emp = 202101
where employee_number = '2100751';

select * from sls.deals where stock_number = 'h14108'

do
$$
declare 
   _emp_no citext := (select employee_number from sls.personnel where last_name = 'nelson' and first_name = 'nicholas'); 
   _thru_date date := '12/31/9999';
begin
  update sls.personnel
  set end_date = _thru_date,  start_date = '01/01/2021'
  where employee_number = _emp_no;
  
  update sls.team_personnel
  set thru_date = _thru_date
  where employee_number = _emp_no;

  update sls.personnel_roles
  set thru_date = _thru_date
  where employee_number = _emp_no;  

  update sls.personnel_payplans
  set thru_date = _thru_date
  where employee_number = _emp_no;    
end
$$;

select * from sls.personnel where last_name = 'nelson'
-------------------------------------------------------------------------
-- 4. new consultant
-------------------------------------------------------------------------
select * from sls.personnel where
select * from ukg.employees where last_name = 'brunk' and first_name = 'justin'
select * from pto.employees where employee_number = '258763'
select * from sls.personnel where store_code = 'ry2'
select * from sls.payplans
select * from sls.teams

update sls.personnel
set ry1_id = 'ACA', ry2_id = 'ACA'
-- select * from sls.personnel
where employee_number = '180353'

Sean Trottier - SHT  190254
Kody Schlenvogt - KSC 198373

DO $$
  declare
  _store citext := 'RY1';
  _employee_number citext := '145776';
  _last_name citext := 'Sutton';
  _first_name citext := 'Nathan';
  _start_date date := '01/02/2024'::Date;
  _ry1_id citext := 'NSU';
  _ry2_id citext := 'none';
  _ry8_id citext := 'none';
  _fi_id citext := 'none';
  _anniversary_date date := '01/02/2024'::date;
  _first_full_month_emp integer := 202401;
  _team citext := 'team who_knows';
  _role citext := 'consultant';
  _payplan citext:= 'standard';

begin

insert into sls.personnel(store_code,employee_number,last_name,first_name,start_date,ry1_id,
  ry2_id,fi_id,anniversary_date, first_full_month_emp, ry8_id) values
(_store, _employee_number, _last_name, _first_name, _start_date, _ry1_id, _ry2_id, 
  _fi_id, _anniversary_date, _first_full_month_emp, _ry8_id);

insert into sls.team_personnel (team, employee_number, from_date)
values(_team, _employee_number, _start_date);    

insert into sls.personnel_roles (employee_number, team_role, from_date)
values(_employee_number, _role, _start_date); 

insert into sls.personnel_payplans (employee_number, payplan, from_date)
values(_employee_number, _payplan, _start_date);
end $$;

select sls.update_consultant_payroll()     

select * from sls.personnel where store_code = 'ry8'

select * from sls.personnel_roles

select * from sls.payplans

-------------------------------------------------------------------------
-- 4a. new product genius, can sell cars but paid hourly, personnel only
-------------------------------------------------------------------------
/*
01/03/23
	brad wants these folks to show up in vision one on ones, to do so, they need to be in sls.personnel
	check with afton, is that the only table she needs
	i have been adding them as they show up in a deal

i will continue to simply add them to the sls.personnel table, none of the other associated tables	
*/
-- Robb Sarchet
-- Maximilian Ortiz
-- Ezekiel Bocklage
-- Michael Brooks RY2 not in consultants

-- current product geniuses not in sls.personnel
select a.store, a.last_name, a.first_name, a.employee_number, a.cost_center, a.seniority_date, c.job_name, d.*
from ukg.employees a
left join ukg.ext_employee_pay_info b on a.employee_id = b.employee_id
left join ukg.ext_jobs c on b.default_job_id = c.id
left join sls.personnel d on a.employee_number = d.employee_number
where c.job_name = 'product genius'
  and a.term_date > current_date
  and d.store_code is null

01/12/23
select * from sls.personnel where employee_number = '145711'

Bryce Huot already in sls.personnel, but has no consultant id
now he has sold a vehicle and his ry1_id = BYH
so:
update sls.personnel 
set ry1_id = 'BYH'
where employee_number = '145983';
-- palacios
update sls.personnel
set ry1_id = 'FPA'
where employee_number = '165748';
-- select * from sls.personnel where last_name = 'mccauley'
update sls.personnel
set ry1_id = 'MCY'
where employee_number = '158941';




do $$
  declare
  _store citext := 'RY8';
  _employee_number citext := '8100262';
  _last_name citext := 'Imhof';
  _first_name citext := 'Julian';
  _start_date date := '10/30/2023'::Date;
  _ry1_id citext := 'none';
  _ry2_id citext := 'none';
  _ry8_id citext := 'JIM';
  _fi_id citext := '';
  _anniversary_date date := '10/30/2023'::date;
  _first_full_month_emp integer := 202312;
--   _team citext := 'team who_knows';
--   _role citext := 'product genius';
--   _payplan citext:= 'hourly';

begin
insert into sls.personnel(store_code,employee_number,last_name,first_name,start_date,ry1_id,
  ry2_id,fi_id,anniversary_date, first_full_month_emp, ry8_id) values
(_store, _employee_number, _last_name, _first_name, _start_date, _ry1_id, _ry2_id, 
  _fi_id, _anniversary_date, _first_full_month_emp, _ry8_id);

end $$;


select * from sls.personnel where last_name = 'imhof'
update sls.personnel set ry1_id = 'KCH' where employee_number = '272581' 
update sls.personnel set anniversary_date = '01/06/2023' where employee_number = '136353' 

-- Parker Leigh and Jacy Knock are the same person.
-- Jacy Knock is a birth name; however, he is transitioning to a male and identifies as a male.
-- Parker Leigh is his preferred name.

select * from sls.personnel where employee_number = '245997'
update sls.personnel
set ry2_id = 'PRL',
    first_name = 'Parker',
    last_name = 'Leigh'
where employee_number = '245997'

-------------------------------------------------------------------------------------------------------------------
-- 4b. product genius becomes a sales consultant
-------------------------------------------------------------------------------------------------------------------
need to add to sls.team_personnel, sls.personnel_roles & sls.personnel_payplans 
& update first_full_month

05/05/23
As of 5/1/23 Bryce Huot was promoted from Product Genius to Sales Consultant. This will move him from hourly 
to the Sales Consultant Plan that is based on a per unit pay and 10% FI across the board

-- ok, i did not take an integrated approach to this one, though i should have
-- have already fixed sls.personnel_payplans
-- Please add Robb Sarchet to Single point payroll in Vision so it's ready for the end of month. He made the switch on 8/1/23.
-- 9/25/23 Tyler Hinkle from product genius to outlet consultant (145603)
-- 11/3/23 Nathan Sloop Toytota 8100249
-- 11/3/23 Karissa Christiansen
-- 11/22/23 Federico Palacios
-- 11/30
Caleb Solem 145711  CSO
Jenny Krout 145645  JKY 
Hannah Gregerson  145653  HHG
Robert Cooper  145931 RTC

select a.store_code, b.team, a.employee_number, a.last_name, a.first_name, c.payplan, d.team_role,
  a.start_date, a.ry1_id, a.ry2_id, a.ry8_id, a.fi_id, a.first_full_month_emp, a.end_date
from sls.personnel a
left join sls.team_personnel b on a.employee_number = b.employee_number
  and b.thru_date > (select first_of_month from sls.months where open_closed = 'open') -- current_date 
left join sls.personnel_payplans c on a.employee_number = c.employee_number
  and c.thru_date > (select first_of_month from sls.months where open_closed = 'open') -- current_date 
left join sls.personnel_roles d on a.employee_number = d.employee_number  
  and d.thru_date > (select first_of_month from sls.months where open_closed = 'open') -- current_date 
where a.end_date > (select first_of_month from sls.months where open_closed = 'open') -- current_date 
order by store_code, first_name, last_name

Nathan Sutton & Cody Lind are on the standard sales pay plan
select * from sls.personnel where last_name = 'sutton'

select * from sls.personnel where employee_number in ('145779')
select * from sls.personnel_payplans where employee_number = '145779'
select * from sls.team_personnel where employee_number = '145779'
select * from sls.personnel_roles where employee_number = '145779'
select * from sls.payplans

-- sls.team_personnel
-- select * from sls.teams
insert into sls.team_personnel (team, employee_number, from_date)
values('team who_knows', '145763', '01/01/2024'); 

-- sls.personnel_roles
-- select * from sls.team_roles
insert into sls.personnel_roles (employee_number, team_role, from_date)
values('145763', 'consultant', '01/01/2024'); 

-- sls.personnel_payplans
-- select * from sls.payplans
insert into sls.personnel_payplans (employee_number, payplan, from_date)
values ('145763', 'standard', '01/01/2024');

-- first_full_month & FI id
update sls.personnel
set first_full_month_emp = 202401 -- , fi_id = 'NAS'
where employee_number = '145765';

select sls.update_deals_by_month();
select sls.update_consultant_payroll();


do -- sls.personnel_payplans
$$
  declare
  _employee_number citext := (select employee_number from sls.personnel where last_name = 'palacios' and first_name = 'federico');
  _to_payplan citext := 'sales consultant';
  _from_date date := '10/01/2023';
  _thru_date date := '09/30/2023';
begin  
-- update current_row
update sls.personnel_payplans
set thru_date = _thru_date
where employee_number = _employee_number
  and thru_date > current_Date;
-- insert new row
insert into sls.personnel_payplans (employee_number, payplan, from_date)
values (_employee_number, _to_payplan, _from_date);
end
$$;


DO $$
  declare
  _store citext := 'RY1';
  _employee_number citext := '145754';
  _last_name citext := 'Krause';
  _first_name citext := 'MEaghann';
  _start_date date := '12/01/2023'::Date;
  _ry1_id citext := 'MKR';
  _ry2_id citext := 'none';
  _ry8_id citext := 'none';
  _fi_id citext := '';
  _anniversary_date date := '10/30/2023'::date;
  _first_full_month_emp integer := 202312;
  _team citext := 'team who_knows';
  _role citext := 'consultant';
  _payplan citext:= 'standard'; ---------------------------------------------------------------------------------------

begin
-- insert into sls.personnel(store_code,employee_number,last_name,first_name,start_date,ry1_id,
--   ry2_id,fi_id,anniversary_date, first_full_month_emp, ry8_id) values
-- (_store, _employee_number, _last_name, _first_name, _start_date, _ry1_id, _ry2_id, 
--   _fi_id, _anniversary_date, _first_full_month_emp, _ry8_id);

update sls.personnel
set first_full_month_emp = _first_full_month_emp,
    start_date = _start_date
where employee_number = '145754';

insert into sls.team_personnel (team, employee_number, from_date)
values(_team, _employee_number, _start_date);    

insert into sls.personnel_roles (employee_number, team_role, from_date)
values(_employee_number, _role, _start_date); 

insert into sls.personnel_payplans (employee_number, payplan, from_date)
values(_employee_number, _payplan, _start_date);
end $$;



select sls.update_consultant_payroll()

-------------------------------------------------------------------------------------------------------------------
-- 4c. change pay plan from sales consultant to  single point
--------------------------------------------------------------------------------------------------------------------- 
-- at 9PM on May 31, Brad calls Afton to say he forgot, but ezekiel bocklage should be on single point pay plan
select * from sls.payplans
select * from sls.personnel_payplans where employee_number = '145950'

do -- sls.personnel_payplans
$$
  declare
  _employee_number citext := (select employee_number from sls.personnel where last_name = 'Christiansen' and first_name = 'Ethan');
  _to_payplan citext := 'single point';
  _from_date date := '07/01/2023';
  _thru_date date := '06/30/2023';
begin  
-- update current_row
update sls.personnel_payplans
set thru_date = _thru_date
where employee_number = _employee_number
  and thru_date > current_Date;
-- insert new row
insert into sls.personnel_payplans (employee_number, payplan, from_date)
values (_employee_number, _to_payplan, _from_date);

-- sls.team_personnel
insert into sls.team_personnel (team, employee_number, from_date)
values('team anthony', '8100243', '07/01/2023'); 
end
$$;
-------------------------------------------------------------------------
-- 5. change team
-------------------------------------------------------------------------
-- select * from sls.teams
-- select * from sls.team_personnel where employee_number = (select employee_number from sls.personnel where last_name = 'haley') order by from_date
-- select * from sls.personnel where last_name = 'danzey'

do
$$
  declare
  _employee_number citext := (select employee_number from sls.personnel where last_name = 'rumen');
  _to_team citext := 'team bedney';
  _from_date date := '10/01/2019';
  _thru_date date := '09/30/2019';
begin
update sls.team_personnel
set thru_date = _thru_date
where employee_number = _employee_number
  and thru_date > current_Date;

insert into sls.team_personnel (team, employee_number, from_date)
values (_to_team, _employee_number, _from_date);
end
$$;

select * from sls.teams
189100

select * from sls.team_personnel where employee_number = '189100' order by from_date

select * from sls.personnel where first_name = 'jacob'
-------------------------------------------------------------------------
-- 7. change payplan
--
-- if the change is to executive, the consultant needs an F&I ID
--
-------------------------------------------------------------------------

08/20/22 Paul Miller to executive per Brad Schumacher
10/31/2022 bradd wants kody Schlenvogt changed to executive on the last day of the month
04/29/23
Austin Suedel hit a performance milestone yesterday and I need to move him from the Sales Professional pay plan that 
pays him 10% on FI to the Single Point pay plan that pays 10% on reserve and 20% on product. 
I will submit this in compli right away. I just need to make sure we can make this retro to the first on April

11/30/2023
Rick had conversations with Mallorie Sabo and Bryce Huot about paying them Single Point wage this month instead of Sales Consultant. 
I was not aware of this and thats why its coming in so late. Would you guys be able to help me get them switched in Vision 
from Sales Consultant to Single Point so their pay is correct? I have already submitted in UKG

select sls.update_consultant_payroll() 
  
select * from sls.personnel where last_name in ('mccoy')

-- add fi_id to consultant going to executive payplan
update sls.personnel
set fi_id = 'FRS'
where employee_number = '2130678';

update sls.personnel set fi_id = 'WHA' where last_name = 'halldorson';
update sls.personnel set fi_id = 'QWL' where last_name = 'wolf';

select * from sls.payplans
select * from sls.personnel order by last_name  
select * from sls.personnel_payplans order by employee_number

-- all tables
select a.store_code, a.last_name, a.first_name, a.employee_number, a.ry2_id, a.fi_id,
  b.team_role, c.team, d.payplan
from sls.personnel a
left join sls.personnel_roles b on a.employee_number = b.employee_number
  and b.thru_date > current_date
left join sls.team_personnel c on a.employee_number = c.employee_number
  and c.thru_date > current_date  
left join sls.personnel_payplans d on a.employee_number = d.employee_number
  and d.thru_date > current_date
where a.last_name = 'wilde'

do
$$
  declare
  _employee_number citext := (select employee_number from sls.personnel where last_name = 'wilde' and first_name = 'corey');
  _to_payplan citext := 'single point';
  _from_date date := '12/01/2023';
  _thru_date date := '11/30/2023';
begin  
-- update current_row
update sls.personnel_payplans
set thru_date = _thru_date
where employee_number = _employee_number
  and thru_date > current_Date;
-- insert new row
insert into sls.personnel_payplans (employee_number, payplan, from_date)
values (_employee_number, _to_payplan, _from_date);
end
$$;


--< product genius to single point ------------------------------------------------------
-- Brandon McCoy
select * from sls.personnel where last_name = 'mccoy'
-- update sls.personnel
update sls.personnel
set fi_id = 'BRM', first_full_month_emp = 202305
where last_name = 'mccoy';

select * from sls.personnel_payplans where employee_number = '8100244'
-- create record in sls.personnel_payplans
insert into sls.personnel_payplans(employee_number,payplan,from_date)
values('8100244','single point','05/01/2023');

-- sls.team_personnel
insert into sls.team_personnel (team, employee_number, from_date)
values('team who_knows', '8100244', '05/01/2023'); 

-- sls.personnel_roles
insert into sls.personnel_roles (employee_number, team_role, from_date)
values('8100244', 'consultant', '05/01/2023'); 

--/> product genius to single point ------------------------------------------------------


select * from sls.personnel where last_name = 'brunk'
select * from sls.payplans
select * from sls.personnel_payplans where employee_number in ('259875','243876','258763')
select left(employee_number, 1), payplan, count(*) from sls.personnel_payplans where thru_date > current_date group by left(employee_number, 1),payplan
5/3/19c
current payplans in use:
  stadstad, croaker, outlet, standard, executive (a-z) (single point)

update sls.personnel
set fi_id = 'JNK'
where last_name = 'brunk'  

select sls.update_consultant_payroll()      
-------------------------------------------------------------------------
-- new team
-- 08/04/20 this structure has no real use for teams, so create a dummy team, team who_knows as placeholder
-------------------------------------------------------------------------
insert into sls.teams (team)
values('team who_knows');

insert into sls.teams (team)
values('team haley');

insert into sls.teams (team)
values('team tweten');

-- team eden becomes team seay
update sls.teams
set team = 'team seay'
where team = 'team eden'

-- team foster being taken over by dockendorf
update sls.team_personnel
set team = 'team dockendorf'
-- select * from sls.team_personnel
where team = 'team foster'
  and thru_date > current_date;

-- team lizakowski being taken over by tweten  
update sls.team_personnel
set team = 'team tweten'
-- select * from sls.team_personnel
where team = 'team lizakowski'
  and thru_date > current_date;
-------------------------------------------------------------------------
-- consultant becomes team leader
-------------------------------------------------------------------------
do
$$
declare 
   _emp_no citext := (select employee_number from sls.personnel where last_name = 'haley'); 
   _from_date date := '10/01/2019';
   _thru_date date := '09/30/2019';
   _to_team citext := 'team haley';
begin

  update sls.personnel_roles
  set thru_date = _thru_date
  where employee_number = _emp_no
    and thru_date > current_date;  
  insert into sls.personnel_roles (employee_number, team_role, from_date)
  values(_emp_no, 'team_leader', _from_date);

  
  update sls.personnel_payplans
  set thru_date = _thru_date
  where employee_number = _emp_no
    and thru_date > current_date;    

-- -- if its not on the team to which they are currently assigned, need to change the team
--   update sls.team_personnel
--   set thru_date = _thru_date
--   where employee_number = _emp_no
--     and thru_date > current_Date;
-- 
--   insert into sls.team_personnel (team, employee_number, from_date)
--   values (_to_team, _emp_no, _from_date);
    
end
$$;


------------------------------------------------------------------------
-- consultant returns to sales after having moved from sales to admin
-------------------------------------------------------------------------
-- 201806: Damian Brooks returns to selling
-- 202401: Nate Dockendorf returns to selling
select * 
from sls.personnel a
join sls.team_personnel b on a.employee_number = b.employee_number
where a.last_name = 'dockendorf'

select a.store_code, b.team, a.employee_number, a.last_name, a.first_name, c.payplan, d.team_role,
  a.start_date, a.ry1_id, a.ry2_id, a.ry8_id, a.fi_id, a.first_full_month_emp, a.end_date
-- select *
from sls.personnel a
left join sls.team_personnel b on a.employee_number = b.employee_number
  and b.thru_date > (select first_of_month from sls.months where open_closed = 'open') -- current_date 
left join sls.personnel_payplans c on a.employee_number = c.employee_number
  and c.thru_date > (select first_of_month from sls.months where open_closed = 'open') -- current_date 
left join sls.personnel_roles d on a.employee_number = d.employee_number  
  and d.thru_date > (select first_of_month from sls.months where open_closed = 'open') -- current_date 
where a.end_date > (select first_of_month from sls.months where open_closed = 'open') -- current_date 
  and a.last_name = 'dockendorf'


do
$$
declare
  _emp_no citext := (select employee_number from sls.personnel where last_name = 'dockendorf');
  _team citext := 'team who_knows';
  _from_date date := '01/01/2024';
  _payplan citext := 'single point';
  _role citext := 'consultant';
begin
  insert into sls.personnel_payplans (employee_number,payplan,from_date)
  values(_emp_no,_payplan,_from_date);
  update sls.personnel_roles
  set thru_date = _from_date - 1
  where employee_number = _emp_no
    and thru_date > current_date;
  insert into sls.personnel_roles(employee_number,team_role,from_date)
  values(_emp_no, _role, _from_date);
  update sls.team_personnel
  set thru_date = _from_date - 1
  where employee_number = _emp_no
    and thru_date > current_date;
  insert into sls.team_personnel(team,employee_number,from_date)
  values(_team,_emp_no,_from_date);
end
$$;  


------------------------------------------------------------------------
-- 10. -- termed consultant returns to rydells as consultant
------------------------------------------------------------------------
201809 Amanda Syverson is rehired
10/30/23 justin  brunk is rehired
select * from sls.personnel where last_name = 'syverson' and first_name = 'amanda'
do
$$
declare
  _emp_no citext := (select employee_number from sls.personnel where last_name = 'brunk' and first_name = 'justin');
  _team citext := 'team longoria';
  _from_date date := '11/01/2023';
  _payplan citext := 'standard';
  _role citext := 'consultant';
  _first_month integer := 202311;
begin
  update sls.personnel
  set start_date = _from_date,
      anniversary_date = _from_date,
      end_date = '12/31/9999',
      first_full_month_emp = _first_month
  where employee_number = _emp_no;
  insert into sls.personnel_payplans (employee_number,payplan,from_date)
  values(_emp_no,_payplan,_from_date);
  insert into sls.personnel_roles(employee_number,team_role,from_date)
  values(_emp_no, _role, _from_date);
  insert into sls.team_personnel(team,employee_number,from_date)
  values(_team,_emp_no,_from_date);
end
$$;  

select sls.update_consultant_payroll()
-------------------------------------------------------------------------
-- 11. 
-- consultant changes store, and goes from consultant to manager (Nik Holland)
-- the same as a term, in that just need to close out his records as
-- an RY1 consultant, but since he is to be a manager at RY2,
-- don't need anything here
-------------------------------------------------------------------------
do
$$
declare 
   _emp_no citext := (select employee_number from sls.personnel where last_name = 'holland' and first_name = 'nikolai'); 
   _thru_date date := '03/31/2019';
   _end_date date := '03/24/2019';
begin
  update sls.personnel
  set end_date = _end_date
  where employee_number = _emp_no;
  
  update sls.team_personnel
  set thru_date = _thru_date
  where employee_number = _emp_no
    and thru_date > current_date;

  update sls.personnel_roles
  set thru_date = _thru_date
  where employee_number = _emp_no
    and thru_date > current_date;  

  update sls.personnel_payplans
  set thru_date = _thru_date
  where employee_number = _emp_no
    and thru_date > current_date;    
end
$$;


-------------------------------------------------------------------------
-- 12.
-- consultant changes store
--------------------------------------
select a.store_code, b.team, a.employee_number, a.last_name, a.first_name, c.payplan, d.team_role,
  a.start_date, a.ry1_id, a.ry2_id, a.fi_id, a.first_full_month_emp, a.end_date
-- select *
from sls.personnel a
left join sls.team_personnel b on a.employee_number = b.employee_number
  and b.thru_date > (select first_of_month from sls.months where open_closed = 'open') -- current_date 
left join sls.personnel_payplans c on a.employee_number = c.employee_number
  and c.thru_date > (select first_of_month from sls.months where open_closed = 'open') -- current_date 
left join sls.personnel_roles d on a.employee_number = d.employee_number  
  and d.thru_date > (select first_of_month from sls.months where open_closed = 'open') -- current_date 
where a.end_date > (select first_of_month from sls.months where open_closed = 'open') -- current_date 
and a.last_name in ( 'greer')
order by a.store_code, a.last_name

-- 10/09/2020
bradley schumacher from RY2 to RY2
update sls.personnel
set store_code = 'RY1',
    employee_number = '165470'
where employee_number = '265478';

-- 08/04/2020
Justin Brunk from RY1 to RY2
update sls.personnel
set store_code = 'RY2',
    employee_number = '258763'
where employee_number = '158763';

select * from sls.personnel where last_name = 'greer'
RY1,team bedney,169628,Greer,Jordyn,executive,consultant,2020-08-24,JGR,JGR,JDG,202009,9999-12-31
termed at GM on 7/11, started at toyota on 7/12
so, lets try the new consultant approach on this one

apparently i didnt do the end date when i termed her in GM
update sls.personnel 
set end_date = '07/11/2022'
where employee_number = '169628';

update sls.personnel 
set start_date = '07/12/2022'
where employee_number = '8100231';

update sls.personnel 
set ry8_id = 'JGR'
where employee_number = '8100231';


do
$$
  declare
  _store citext := 'RY8';
  _employee_number citext := '8100231';
  _last_name citext := 'Greer';
  _first_name citext := 'Jordyn';
  _start_date date := '08/24/2020'::Date;
  _ry1_id citext := 'JGR';
  _ry2_id citext := 'JGR';
  _ry8_id citext := 'JGR';
  _fi_id citext := 'JDG';
  _anniversary_date date := '08/24/2020'::date;
  _first_full_month_emp integer := 20209;
  _team citext := 'team who_knows';
  _role citext := 'consultant';
  _payplan citext:= 'executive';

begin
insert into sls.personnel(store_code,employee_number,last_name,first_name,start_date,ry1_id,
  ry2_id,fi_id,anniversary_date, first_full_month_emp) values
(_store, _employee_number, _last_name, _first_name, _start_date, _ry1_id, _ry2_id, 
  _fi_id, _anniversary_date, _first_full_month_emp);

insert into sls.team_personnel (team, employee_number, from_date)
values(_team, _employee_number, _start_date);    

insert into sls.personnel_roles (employee_number, team_role, from_date)
values(_employee_number, _role, _start_date); 

insert into sls.personnel_payplans (employee_number, payplan, from_date)
values(_employee_number, _payplan, _start_date);
end
$$;


-------------------------------------------------------------------------
-- 13. consultant gets an F/I id
-------------------------------------------------------------------------
select * from sls.personnel where employee_number = '198373'
update sls.personnel
set fi_id = 'KSB'
where employee_number = '198373';


------------------------------------------------------------------------
-- 14. consultant went to bdc, then came back to sales
------------------------------------------------------------------------
Dylan Dubois
select * from sls.personnel where last_name = 'dubois' and first_name = 'dylan'
his record in payplans, etc were not closed when he left sales on 1/22/22
do
$$
declare
  _emp_no citext := (select employee_number from sls.personnel where last_name = 'dubois' and first_name = 'dylan');
  _team citext := 'team who_knows';
  _payplan citext := 'standard';
  _role citext := 'consultant';
  _from_date  date := '08/01/2022';
begin
  update sls.personnel
  set start_date = _from_date,
      end_date = '12/31/9999'
  where employee_number = _emp_no;
--   insert into sls.personnel_payplans (employee_number,payplan,from_date)
--   values(_emp_no,_payplan,_from_date);
--   insert into sls.personnel_roles(employee_number,team_role,from_date)
--   values(_emp_no, _role, _from_date);
--   insert into sls.team_personnel(team,employee_number,from_date)
--   values(_team,_emp_no,_from_date);
end
$$;  

------------------------------------------------------------------------
-- 15. consultant left one store to become sales manager at another store
------------------------------------------------------------------------

select * -- 169645
from sls.personnel
where last_name = 'dubois'

update sls.team_personnel
set thru_date = '10/31/2022'
where employee_number = '169645';

update sls.personnel
set end_date = '10/31/2022'
where employee_number = '169645';

insert into sls.personnel (store_code,employee_number,last_name,first_name,start_date,
  ry1_id,ry2_id,fi_id,is_senior,anniversary_date,first_full_month_emp,end_date)
values('RY8','8100242','DuBois','Dylan','11/01/2022', 'DBU', 'DBU', 'DDB', false, '08/03/2020', 202008, '12/31/9999');

select * from sls.personnel_payplans where employee_number = '258763'
