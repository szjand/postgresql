﻿select *
from sls.deals
limit 10

select *
from sls.deals_By_month
where year_month = 201809


-- G34389: in inventory: 130402::59.00
-- G34390: sold: 130402::59.00/-59.00, 144502::-299.00
-- G34385: sold & no pulse (i think)  130402::59.00/059.00
select b.the_date, a.control, c.account, c.account_type, d.journal_code, a.doc, a.ref, a.amount
from fin.fact_gl a
inner join dds.dim_Date b on a.date_key = b.date_key
inner join fin.dim_Account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
where c.account in ('144502','244502','130402','230402')
order by a.control



select b.the_date, a.control, c.account, d.journal_code, a.doc, a.ref, a.amount
from fin.fact_gl a
inner join dds.dim_Date b on a.date_key = b.date_key
inner join fin.dim_Account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
where c.account in ('130402','230402')
order by a.control


select * -- yep
from sls.deals_Accounting_detail
where gl_account in ('144502','244502')
order by control

select * -- nope because the account type is other income, not sales
from sls.ext_accounting_base
where account in ('144502','244502')

select * -- nope
from sls.ext_accounting_deals
where account in ('144502','244502')

select *
from sls.ext_deals
where stock_number in ('g33893','g33905')


trying to use the way gap/service_contract/total_care was persisted, though those are no longer used
thinking the "pulse" data will need to exist at every level
maybe not
look into simply updating sls.deals_by_month, that, i believe is the basis for payroll

see sls.update_consultant_payroll() which is the basis for the managers submit payroll page, sls.json_get_payroll_for_submittal(citext)

                    select a.run_date, a.store_code, a.bopmast_id, a.deal_status,
                      a.deal_type, a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
                      a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
                      a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date, a.approved_date,
                      a.capped_date, a.delivery_date, a.gap, a.service_contract, a.total_care,
                      'new', 1,
                      coalesce(b.gl_date, '12/31/9999'), coalesce(b.unit_count, 0),
                      case when b.account in ('144502','244502') then b.amount end as pulse
                    from sls.ext_deals a
                    left join sls.ext_accounting_deals b on a.stock_number = b.control
                      and b.gl_date = (
                        select max(gl_date)
                        from sls.ext_accounting_deals
                        where control = b.control)
                    where gl_date >= '09/01/2018'
                      and a.vin is not null


-- looks like joining this in Function: sls.update_deals_by_month()
-- should give me what i need
select control, sum(amount)
from sls.deals_Accounting_detail
where gl_account in ('144502','244502')
group by control

alter table sls.deals_by_month 
add column pulse integer;

update sls.deals_by_month
set pulse = 0;

alter table sls.deals_by_month
alter column pulse set not null;


alter table sls.consultant_payroll
add column pulse integer;

update sls.consultant_payroll
set pulse = 0;

alter table sls.consultant_payroll
alter column pulse set not null;



select unit_count,
from sls.deals_by_month
where year_month = 201809


select psc_last_name, psc_employee_number, unit_count
from sls.deals_by_month
where year_month = 201809 
  and pulse = 1
  and psc_employee_number <> 'HSE'




select * 
from sls.consultant_payroll
where year_month = 201809


  with 
    open_month as (
      select year_month
      from sls.months
      where open_closed = 'open')
        select psc_employee_number, sum(unit_count) * 25 as pulse
        from ( -- y
          select a.psc_employee_number, 
            case
              when ssc_last_name = 'none' then unit_count
              else 0.5 * unit_count
            end as unit_count
          from sls.deals_by_month a
          where a.year_month = (select year_month from open_month)
            and a.pulse = 1
          union all
          select a.ssc_employee_number, 
            0.5 * unit_count as unit_count
          from sls.deals_by_month a
          where a.year_month = (select year_month from open_month)
            and a.ssc_last_name <> 'none'
            and a.pulse = 1) y
        group by psc_employee_number