﻿-- chargeback accounts
  select distinct case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code,
    a.fxmpge as the_page, a.fxmlne as line, trim(b.g_l_acct_number) as gl_account, c.description, c.account_type
  from arkona.ext_eisglobal_sypffxmst a
  inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
    and a.fxmcyy = b.factory_financial_year
  left join fin.dim_account c on b.g_l_acct_number = c.account    
    and c.current_row = true
  where a.fxmcyy = 2018
    and trim(a.fxmcde) = 'GM'
    and coalesce(b. consolidation_grp, '1') <> '3'
    and b.factory_code = 'GM'
    and trim(b.factory_account) <> '331A'
    and b.company_number = 'RY1'
    and b.g_l_acct_number <> ''
    and a.fxmpge = 17 
    and a.fxmlne in (3, 13) -- chargebacks

-- per jeri
-- 185003: incentives that are unable to be claimed as a result of ineligibity or missing documentation


-- february chargebacks
select c.store_code, b.the_date, d.line, c.account, c.description, a.control, a.amount, 
  sum(amount) over (partition by c.store_code, line)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201802
inner join fin.dim_account c on a.account_key = c.account_key
inner join (
  select distinct case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code,
    a.fxmpge as the_page, a.fxmlne as line, trim(b.g_l_acct_number) as gl_account
  from arkona.ext_eisglobal_sypffxmst a
  inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
    and a.fxmcyy = b.factory_financial_year
  where a.fxmcyy = 2018
    and trim(a.fxmcde) = 'GM'
    and coalesce(b. consolidation_grp, '1') <> '3'
    and b.factory_code = 'GM'
    and trim(b.factory_account) <> '331A'
    and b.company_number = 'RY1'
    and b.g_l_acct_number <> ''
    and a.fxmpge = 17 
    and a.fxmlne in (3, 13)) d on c.account =  d.gl_account
where a.post_status = 'Y'    
order by store_code, line



--looking at before controlling chargebacks by stocknumber but rather by last 6 of vin
-- shows that as of 201803, stocknumber is being used consistently
select b.the_date, d.line, gl_account,  e.journal_code, f.description, a.control, a.amount, g.year_month, g.bopmast_id, g.vin, g.buyer, g.fi_last_name,
  h.bopmast_id, h.vin, h.capped_date, h.delivery_date, h.fi_manager, 
  'break', i.bopmast_id, i.vin, i.capped_date, i.delivery_date, i.fi_manager
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201803
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal e on a.journal_key = e.journal_key
inner join fin.dim_gl_description f on a.gl_description_key = f.gl_description_key
inner join (
  select distinct case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code,
    a.fxmpge as the_page, a.fxmlne as line, trim(b.g_l_acct_number) as gl_account
  from arkona.ext_eisglobal_sypffxmst a
  inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
    and a.fxmcyy = b.factory_financial_year
  where a.fxmcyy = 2018
    and trim(a.fxmcde) = 'GM'
    and coalesce(b. consolidation_grp, '1') <> '3'
    and b.factory_code = 'GM'
    and trim(b.factory_account) <> '331A'
    and b.company_number = 'RY1'
    and b.g_l_acct_number <> ''
    and a.fxmpge = 17 
    and a.fxmlne in (3, 13) -- chargebacks
    and b.consolidation_grp is null) d on c.account =  d.gl_account
left join sls.deals_by_month g on a.control = g.stock_number    
left join sls.ext_bopmast_partial h on a.control = h.stock_number
left join sls.ext_bopmast_partial i on a.control = right(i.vin, 6)
where a.post_status = 'Y'    
order by f.description



select year_month, count(*)
from sls.deals_by_month
group by year_month
order by year_month


select *
from sls.ext_bopmast_partial
limit 100


select *
from sls.deals_by_month
limit 200

select bopmast_id, count(*) 
from sls.deals
group by bopmast_id
order by count(*) desc 

select *
from sls.deals
where bopmast_id = 42943
order by seq

categorize different fi lines?

select year_month, store_code, page, line, sum(amount) as amount
from sls.deals_Accounting_Detail
group by year_month, store_code, page, line 
order by store_code, page, line, year_month

select *
from sls.deals_accounts_routes
order by store_code, page, line


select * --a.*, b.year_month, b.fi_last_name
from sls.deals_accounting_detail a
inner join sls.deals_By_month b on a.control = b.stock_number
where page = 17 and line in (3,13)


select a.control, count(*)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_year > 2016
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account in (
    select gl_account
    from sls.deals_accounts_routes
    where page = 17 and line in (3,13))
where a.post_status = 'Y'    
group by a.control
order by count(*) desc    


select *
from arkona.xfm_inpmast
where current_row = true
and right(Trim(inpmast_vin), 6) = '393564'

stk 19377a

-- over a series of 3 months, 4 different chargeback transactions
select b.the_date, a.*, c.account, c.description
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_year > 2016
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account in (
    select gl_account
    from sls.deals_accounts_routes
    where page = 17 and line in (3,13))
where a.post_status = 'Y'    
  and a.control = '393564'

-- 3/28/18
-- add attribute fi_chargeback to sls.deals_gross_by_month
-- include it in fi_gross

alter table sls.deals_gross_by_month
add column fi_chargeback numeric(8,2) default 0 not null;

select * from sls.deals_gross_by_month limit 100

ok, got chargebacks showing up in sls.deals_accounting_Detail and sls.deals_gross_by_month
but they only apply to fi_gross if they are within 6 months of the deal
the way the data is configured now does not accomodate that

1. only add to deals_gross_by_month if within the time frame
2. a separate table for chargebacks
take chargebacks out of the deals_gross_by_month table
but use the chargebacks table in the query generating payroll

control
deal date
deal year_month
chargeback_date
chargeback_yearmonth
account
amount

select * from sls.ext_bopmast_partial limit 100

-- ok, here is the start of getting the chargeback stuff
select a.*, b.capped_Date, b.delivery_date, b.gl_date, b.fi_manager, b.unit_count, b.seq, b.deal_status, c.delivery_date, c.capped_date
from sls.deals_accounting_detail a
left join sls.deals b on a.control = b.stock_number
left join sls.ext_bopmast_partial c on a.control = c.stock_number
where a.year_month = 201803
  and a.gross_category = 'fi_chargeback'
order by c.delivery_date desc   

-- 3/29
feel like shit today but i have to get this stuff done
keep wanting to do a mojor refactor
not today
simply add a chargebacks table derived from sls.deals_Accounting_detail

do need to add the date to sls.deals_accounting_detail
-- array of column names from a table
SELECT array_agg(attname)
FROM  pg_attribute
WHERE attrelid = 'sls.deals_accounting_detail'::regclass  -- table name, optionally schema-qualified
  AND attnum > 0
  AND NOT attisdropped 

alter table sls.deals_accounting_detail
add column the_date date;

that did not work out, adding the date requires making the date part of the PK, which is allready too junked up
fuck it, will work out the 6 month deal based on year month

alter table sls.deals_accounting_detail
drop column the_date;




-- ok, here is the start of getting the chargeback stuff
-- select a.*, b.*--  b.capped_Date, b.delivery_date, b.gl_date, b.fi_manager, b.unit_count, b.seq, b.deal_status, b.year_month, b.bopmast_id
select a.year_month, a.control, -a.amount, b.bopmast_id, delivery_date, seq, b.year_month, unit_count, b.fi_manager
from sls.deals_accounting_detail a
inner join sls.deals b on a.control = b.stock_number
where a.year_month = 201803
  and a.gross_category = 'fi_chargeback'
order by b.stock_number, bopmast_id, seq


-- mind fuck 1 back on resold, to which deal does the chargeback apply?  32730B (same stock #, diff bopmast_id)
shit, final sale was wholesale, so the chargeback applies to first deal, the back on
looks like i will need a source attribute, like i did with bs pay, if it is manual, do not update it
select *
from sls.deals_accounting_Detail
where control = '32730b'
-- 2 post sale month correction, 33129, this one seems like going with the delivery_date/year_month works
-- 3 31643B, obvious by looking that it applies to the second deal, but that is not
select *
from sls.deals_accounting_Detail
where control = '31643b'

there is no sure way of knowing 
ohhh, maybe, the desc on the chargeback looks like the customer name may be the link

select *
from sls.deals a
left join ads.ext_dim_customer b on a.buyer_bopname_id = b.bnkey
where stock_number = '31643b'

select *
from sls.deals_accounting_detail
where control = '31643b'

create extension pg_trgm schema public;

-- interesting and helpful, but not conclusive
select a.year_month, a.control, -a.amount, a.trans_description, 
  b.bopmast_id, b.delivery_date, b.seq, b.year_month, b.unit_count, b.fi_manager, c.fullname,
  similarity(a.trans_description,c.fullname)
--   similarity(a.trans_description,replace(c.fullname, 'JOHN', ''))
from sls.deals_accounting_detail a
inner join sls.deals b on a.control = b.stock_number
left join ads.ext_dim_customer c on b.buyer_bopname_id = c.bnkey
where a.year_month = 201803
  and a.gross_category = 'fi_chargeback'
--   and similarity(a.trans_description,c.fullname) < .5
--   and exists (
--     select 1
--     from sls.deals
--     where stock_number = b.stock_number
--       and seq > 1)
order by b.stock_number, bopmast_id, seq


drop table if exists sls.fi_chargebacks;
create table sls.fi_chargebacks (
  chargeback_year_month integer not null,
  stock_number citext not null,
  amount numeric(8,2) not null,
  store_code citext not null,
  fi_employee_number citext not null,
  delivery_date date not null,
  source citext not null default 'pipeline',
  primary key (chargeback_year_month, stock_number));

select sum(amount), count(*) from sls.fi_chargebacks  --2619.03 /  117
  
-- i believe this is good enough to get started
-- so, first cut only deals with single row in sls.deals
truncate sls.fi_chargebacks;
insert into sls.fi_chargebacks
select year_month, control, sum(amount) as amount, store_code, fi_employee_number,
  delivery_date, source
from (
  select a.year_month, a.control, -a.amount as amount, 
    b.store_code, coalesce(d.employee_number, 'N/A') as fi_employee_number, 
    b.delivery_date, 'pipeline' as source
  from sls.deals_accounting_detail a
  inner join sls.deals b on a.control = b.stock_number
  left join ads.ext_dim_customer c on b.buyer_bopname_id = c.bnkey
  left join sls.personnel d on b.fi_manager = d.fi_id
  where a.year_month = (select year_month from sls.months where open_closed = 'open')
    and a.gross_category = 'fi_chargeback'
    and not exists (
      select 1
      from sls.deals
      where stock_number = b.stock_number
        and seq > 1)
    and delivery_date >= ( -- 6 month rule     
      select (first_of_month - interval '6 month')::date as the_date
      from sls.months where year_month = (
        select year_month 
        from sls.months 
        where open_closed = 'open'))
  union
  select distinct year_month, control, amount, store_code, 
    coalesce(employee_number, 'N/A'), delivery_date, 'pipeline'
  from (
    --eliminate same month back on
    -- inner join on the_bopmast_id cte eliminates those deals for which the sum
    -- of the unit count in the same month is 0
    with
      the_bopmast_id as (
        select bopmast_id
        from sls.deals_accounting_detail a
        inner join sls.deals b on a.control = b.stock_number
        left join ads.ext_dim_customer c on b.buyer_bopname_id = c.bnkey
        left join sls.personnel d on b.fi_manager = d.fi_id
        where a.year_month = (select year_month from sls.months where open_closed = 'open')
          and a.gross_category = 'fi_chargeback'
          and exists (
            select 1
            from sls.deals
            where stock_number = b.stock_number
              and seq > 1)
        group by b.bopmast_id, b.year_month
        having sum(unit_count) <> 0)  
    select a.year_month, a.control, -a.amount as amount, 
      b.store_code, b.fi_manager, d.employee_number, 'pipeline',
      b.delivery_date, b.bopmast_id,  unit_count
    from sls.deals_accounting_detail a
    inner join sls.deals b on a.control = b.stock_number
    inner join the_bopmast_id bb on b.bopmast_id = bb.bopmast_id
    left join ads.ext_dim_customer c on b.buyer_bopname_id = c.bnkey
    left join sls.personnel d on b.fi_manager = d.fi_id
    where a.year_month = (select year_month from sls.months where open_closed = 'open')
      and a.gross_category = 'fi_chargeback'
      and exists (
        select 1
        from sls.deals
        where stock_number = b.stock_number
          and seq > 1)
      and delivery_date >= ( -- 6 month rule     
        select (first_of_month - interval '6 month')::date as the_date
        from sls.months where year_month = (
          select year_month 
          from sls.months 
          where open_closed = 'open'))) gl) aa  
group by year_month, control, store_code, fi_employee_number, delivery_date, source;

----------------------------------------------------------------------
-- 5/2/18
----------------------------------------------------------------------
-- failing in luigi
-- 32035ac, 2 different chargeback transactions in the same month, but for different deals
-- PK of sls.fi_chargebacks is currently chargeback_year_month/stock_number
-- so this data fails:
-- 
-- 201805;32035AC;120.00;RY1;110052;2018-05-02;pipeline
-- 201805;32035AC;120.00;RY1;148080;2018-03-30;pipeline
-- 
-- need to add either employee_number or delivery_date to PK
-- 
-- feels like 6 of ...
-- 
-- so, lets make it employeenumber

alter table sls.fi_chargebacks
drop  CONSTRAINT fi_chargebacks_pkey;

alter table sls.fi_chargebacks
add primary key (chargeback_year_month, stock_number, fi_employee_number);

