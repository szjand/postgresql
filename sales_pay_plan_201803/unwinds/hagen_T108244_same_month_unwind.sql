﻿T10824
hagen unwind
sold and unwound in august, unwound on the 31st, system saw it as 202309
need to find how it processes same month unwinds
handled payroll with an adjustment

select * from sls.deals where stock_number = 'T10824'

update sls.deals
set run_Date = '08/31/2023'
where stock_number = 'T10824'
  and deal_status_code = 'deleted'

select * from sls.deals_by_month where stock_number = 'T10824'  

update sls.deals_by_month
set year_month = 202308
where stock_number = 'T10824'
  and seq = 2

select * from sls.deals_by_month where unit_count = -1 order by year_month desc limit 10  

select * from sls.deals_by_month where stock_number = 'H16035'

-- 00/03/23
try board.sales_board.is_backed_on

select * 
from board.sales_board
where is_backed_on
order by boarded_ts desc
limit 20

G47445 capped 8/21
			 unwind 8/22
			 sold 8/28

-- this is what i am looking for
G47955 capped 8/19
			 unwind 8/21

-- no rows
select * from sls.deals_by_month where stock_number = 'G47955'			 
-- 2 rows, 1 for the sale 1 for the unwind both in the same month
select * from sls.deals where stock_number = 'G47955'	

-- sls.deals looks ok, an august run_date for sale and for unwind
select * from sls.deals where stock_number = 'T10824'

select * from sls.deals_by_month where stock_number = 'T10824' 
-- should be no rows in sls.deals_by_month for August
delete from sls.deals_by_month where stock_number = 'T10824'

select sls.update_consultant_payroll()  

ok, looks like hagen is correct for september now
-- 09/06/23
T10824 is again showing as a sep unwind for Hagen

-- this is the subquery on sls.deals from sls.update_deals_by_month
    select store_code, bopmast_id, stock_number, year_month, sum(unit_count)::integer as unit_count
    from sls.deals
    where year_month = 202309
      and store_code in ('RY1', 'RY2', 'RY8')
    group by store_code, bopmast_id, stock_number, year_month
    having sum(unit_count) <> 0

looks like the problem is sls.deals.year_month shows 202309 for the unwind
update sls.deals
set year_month = 202308
where stock_number = 'T10824' 
  and deal_status_code = 'deleted'    

hopefully ok now  