﻿
 H17035L  H17124  H17027  T10994


--< H17035L -------------------------------------------------------------------------------------------------------------------

-- final call, exclude from sls.update_deals_by_month(), both bopmast_ids 25859,25871
--   changed unit count in sls.deals & sls.deals_by_month to be all zero but for 1 in October

select * from sls.deals where stock_number = 'h17035l'
update sls.deals
set unit_count = 0
where stock_number = 'h17035l'
  and gl_date > '10/31/2023'

select * from sls.deals_by_month where stock_number = 'h17035l'
update sls.deals_by_month
set unit_count = 0
where stock_number = 'h17035l'
  and year_month = 202311

select  sls.update_deals_by_month()
select sls.update_consultant_payroll()

Karisssas commission page now no longer shows H17035L and UKG Imports is correct as well


select * from sls.consultant_payroll where stock_number = 'h17035l'

select distinct b.control, b.gl_description, c.record_key
from (
	select a.control, a.gl_description, min(a.gl_date), max(a.gl_date)
	from sls.ext_accounting_deals a
	group by a.control, a.gl_description) b
left join arkona.xfm_bopmast c on b.control = c.bopmast_stock_number
  and b.gl_description = c.bopmast_search_name
where b.control = 'h17035l'
order by b.control




select * 
from sls.deals_by_month 
where bopmast_id in (25859,25871)

select * 
from arkona.xfm_bopmast
where bopmast_stock_number = 'H17035L'


select * 
from arkona.ext_bopmast
where bopmast_stock_number = 'H17035L'

-- this one shows 25871 is the deal, 25859 is deleted, but also
-- shows 2 -1 unit_counts
select * from sls.deals where stock_number = 'H17035L'

select *  
from sls.deals_by_month
where stock_number = 'H17035L'

-- fuck, 2 customer numbers for the same person
select * from arkona.ext_bopname where bopname_record_key in (1187657,1187582)



select stock_number, vin, bopmast_id, buyer_bopname_id, unit_count, gl_date, DELIVERY_DATE from sls.deals where stock_number = 'h17035l'

select * from cu.used_2 where stock_number = 'h17035l'

select * from sls.deals where bopmast_id in (25859,25871)

    select store_code, bopmast_id, stock_number, year_month, sum(unit_count)::integer as unit_count
    from sls.deals
--     where year_month = 202310-- (select year_month from sls.months where open_closed = 'open')
    -- * *
    where store_code in ('RY1', 'RY2', 'RY8')
    -- *d*
      and bopmast_id <> 79660
      and bopmast_id in (25859,25871)
    group by store_code, bopmast_id, stock_number, year_month
    having sum(unit_count) <> 0

--/> H17035L -------------------------------------------------------------------------------------------------------------------  

in all_nc_inventory_nightly.sql, deleting these 3 from nc.stg_sales, l1828, so that it passes the query
at L1844: dup stock numbers

H17124  H17027  T10994


--< T10994 -------------------------------------------------------------------------------------------------------------------

select * from nc.stg_sales where stock_number = 'T10994'

select * from sls.deals_by_month where stock_number = 'T10994'  

can take care of this one with the delete from nc.stg_sales at line 1599, because it looks like an accounting exercise, not an unwind

--< T10994 -------------------------------------------------------------------------------------------------------------------

in fact, all 3 are exactly the same

select * from nc.stg_sales where stock_number = 'H17124'