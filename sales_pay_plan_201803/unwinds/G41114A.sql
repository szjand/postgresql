﻿sold and paid in november by vanyo
unwound and resold to same customer on new deal in december
showing up as -1 on consultant page

select run_date, bopmast_id, deal_Status_code, stock_number, buyer_bopname_id,
  year_month, gl_date, unit_count
from sls.deals where stock_number = 'G41114A'

2020-12-16;63991;deleted;G41114A;1118229;202012;2020-11-27;-1.0
2020-12-16;64244;U;      G41114A;1118229;202011;2020-11-27; 1.0
2020-11-28;63991;U;      G41114A;1118229;202011;2020-11-27; 1.0

should be no count in december
change unit count on both december transactions to 0

update sls.deals
set unit_count = 0
-- select * from sls.deals
where stock_number = 'G41114A'
  and run_date = '12/16/2020';

select sls.update_deals_by_month()