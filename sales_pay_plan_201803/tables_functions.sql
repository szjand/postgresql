﻿select * 
from sls.personnel a
left join sls.personnel_payplans b on a.employee_number = b.employee_number
  and thru_date > current_date
order by anniversary_date desc  
order by a.store_code, b.payplan, a.last_name  



select * from sls.personnel_payplans

do 
$$
declare
_from_date date := '03/01/2018';
_thru_date date := '02/28/2018';
_standard citext[] := array['195632','12654','12242','124120','128530','1212688','159863','145840',
  '186593','187594','189100','165983','151702','1566882','1130690','1135518','175642','178543','1147250'];
_executive citext[] := array['110052','15552','148080','163700','111232','1124625','1147810'];  
_outlet citext[] := array['147741','133017','1106225'];
begin 
-- drop table if exists wtf;
-- create temp table wtf as
-- select *
-- from sls.personnel 
-- where employee_number = any(_standard);
-- end
update sls.personnel_payplans
set thru_date = _thru_date
where employee_number = any(_standard)
  and payplan <> 'standard'
  and thru_date > current_date;
insert into sls.personnel_payplans (employee_number,payplan,from_date)
select employee_number, 'standard',_from_date
from sls.personnel_payplans
where employee_number = any(_standard)
  and payplan <> 'standard'
  and thru_date = _thru_date;

update sls.personnel_payplans
set thru_date = _thru_date
where employee_number = any(_executive)
  and payplan <> 'executive'
  and thru_date > current_date;
insert into sls.personnel_payplans (employee_number,payplan,from_date)
select employee_number, 'executive', _from_date
from sls.personnel_payplans
where employee_number = any(_executive)
  and payplan <> 'executive'
  and thru_date = _thru_date;   

update sls.personnel_payplans
set thru_date = _thru_date
where employee_number = any(_outlet)
  and payplan <> 'outlet'
  and thru_date > current_date;
insert into sls.personnel_payplans (employee_number,payplan,from_date)
select employee_number, 'outlet', _from_date
from sls.personnel_payplans
where employee_number = any(_outlet)
  and payplan <> 'outlet'
  and thru_date = _thru_date;  
  
end  
$$;

-- select * from wtf ;

alter table sls.payroll_guarantees
rename to z_unused_payroll_guarantees;


create table sls.consultant_guarantee (
  three_month_rolling_avg int4range primary key,
  value integer not null);

insert into sls.consultant_guarantee values
('[0,11)', 1800),
('[10,100)',3000);  



create or replace function sls.per_unit_earnings (
  _employee_number citext,
  _unit_count numeric(3,1))
  returns numeric as
$BODY$
/*
select sls.per_unit_earnings('110052', 4)
*/
declare
  _payplan citext := (
    select payplan
    from sls.personnel_payplans
    where employee_number = _employee_number
      and thru_date > current_date);      
begin

  if _unit_count < 15 then return (select _unit_count * level_1 from sls.per_unit_matrix where payplan = _payplan);
  elsif _unit_count < 20 then return
    (select ((14 * level_1) + ((_unit_count - 14) * level_2)) from sls.per_unit_matrix where payplan = _payplan);
  elsif _unit_count < 25 then return
    (select ((14 * level_1) + (5 * level_2) + ((_unit_count - 19) * level_3)) from sls.per_unit_matrix where payplan = _payplan);
  elsif _unit_count >= 25 then return
    (select ((14 * level_1) + (5 * level_2) + (5 * level_3) +((_unit_count - 24) * level_4)) from sls.per_unit_matrix where payplan = _payplan);
  end if;

end; 
$BODY$

language plpgsql;


select 14*250 + 5*275 + 5*300 + 2*325

standard;   300;350;400;450
executive;  250;275;300;325
outlet;     300;300;300;300


-- first_full_month_emp  
ALTER TABLE sls.personnel ADD COLUMN first_full_month_emp integer;

update sls.personnel
set first_full_month_emp = 
  case
  when extract(day from start_date) < 6 then (select year_month from dds.dim_date where the_date = start_date)
  else (select (select extract(year from (start_date + interval '1 month')::date) * 100) + (select extract(month from (start_date + interval '1 month')::date)) ) 
  end;

alter table sls.personnel
alter column first_full_month_emp set not null;

    
select * from sls.personnel

