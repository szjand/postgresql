﻿
select b.the_date, c.account, a.*, d.journal_code, journal
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201804
inner join fin.dim_Account c on a.account_key = c.account_key
  and c.account in ('180600','180800','144300','164300','144400','164400','145400','165400','145500','165500')  
inner join fin.dim_journal d on a.journal_key = d.journal_key  
where a.post_status = 'Y'


select * 
from sls.deals_accounts_routes


select * from sls.deals_by_month where stock_number in ('28739','G33689G','G33204','32951B','32239R','G33378')


-- 5/7/18 where's the product?
-- 2018 non VSN, VSU transactions against fi accounts
select a.control, a.doc, a.ref, a.amount, b.the_date, c.account, c.description, d.journal_code, d.journal, e.description
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month > 201800
inner join fin.dim_account c on a.account_key = c.account_key
and c.account in (
'180600,'
'180800',
'144300','164300',
'144400','164400',
'145400','165400',
'145500 ','165500')
inner join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code not in ('VSN','VSU')
inner join fin.dim_gl_Description e on a.gl_description_key = e.gl_description_key
and abs(a.amount) > 10
order by a.control