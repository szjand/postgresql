﻿/*
11/25/23
realized that i had not uploaded bopsrvs to luigi production, so i did that
looks like jeri has changed the set up, looks like they are now all Rydell Care (SWA)(SWB)(SWC) or (SWD), where SWA is no charge 12 month 15000 miles
need to talk to her

11/29/23
this looks like a descent query
talked to jeri, the hope is to eventually include this in the vision pay page

saved the output of this into E:\sql\postgresql\sales_pay_plan_201803\service_contracts\nov_test_1.csv and formatted that into nov_test_1.xlsx

need to compose the email to managers expressing the desire to incorporate this into the vision page,
but until we can all get on the same page about the data ...
*/

select 
  case b.store_code
    when 'RY1' then 'GM'
    when 'RY2' then 'HONDA NISSAN'
    when 'RY8' then 'TOYOTA'
    else 'XXX'
  end as store, b.fi_first_name || ' ' || b.fi_last_name, 
  count(*) over (partition by b.store_code, b.fi_first_name || ' ' || b.fi_last_name) * 25 as total,
  b.stock_number, 
  a.contract_name
from arkona.ext_bopsrvs a
join sls.deals_by_month b on a.vin = b.vin
  and  b.year_month = 202401 ---------------------------------------------------------------------
  and b.fi_last_name not in ('HSE','none')
join sls.personnel c on b.fi_employee_number = c.employee_number  
  and end_date > current_date
where contract_name like 'RYDELL CARE%' 
  and contract_name <> 'RYDELL CARE (SWA)'
order by b.store_code, b.fi_first_name || ' ' || b.fi_last_name, b.stock_number

------------------------------------------------------------------------------------------------------------------
/*
11/1/23
found a better table
had 2 where the manager said there were multiple service contract companies for the deals,
one of which was Toyota Maintenance
bopsrvc does not expose that, bopsrvs does
but then, bopsrvs does not seem to have all the deals in it
*/

select * from arkona.ext_bopsrvs where deal_key in (1809, 25776)

select * from sls.deals_by_month where bopmast_id = 79682
select * from arkona.ext_bopsrvs where deal_key = 79682


select a.bopmast_stock_number, a.bopmast_vin, a.record_key, c.*
from arkona.ext_bopmast a
join sls.deals_by_month c on a.record_key = c.bopmast_id
  and a.bopmast_company_number = c.store_code
where c.year_month = 202310 
  and a.vehicle_type = 'U'


  
select b.contract_name, a.bopmast_stock_number as stock_number, a.bopmast_vin, a.record_key as bopmast_id, a.date_capped, b.contract_expiration_months, b.contract_expiration_miles,
  c.fi_first_name || ' ' || c.fi_last_name as fi, c.fi_employee_number, d.payplan, e.cost_center
from arkona.ext_bopmast  a
join arkona.ext_bopsrvs b on a.serv_cont_company = b.deal_key
  and a.bopmast_company_number = b.company_number
-- --   and b.record_key <> 0
join sls.deals_by_month c on a.record_key = c.bopmast_id
  and a.bopmast_company_number = c.store_code
join sls.personnel_payplans d on c.fi_employee_number = d.employee_number
  and thru_date > current_date
left join ukg.employees e on c.fi_employee_number = e.employee_number
where c.year_month = 202310
  and b.contract_name = 'TOYOTA MAINTENANCE'
  and a.vehicle_type = 'U'
  and (
    b.contract_expiration_months > 12
    or
    b.contract_expiration_miles > 15000)
order by left(stock_number, 1), fi 



select b.company_name,  bopmast_stock_number, date_capped, serv_cont_months, serv_cont_miles
-- 	serv_cont_company, serv_cont_amount, 
--   serv_cont_deduct, serv_cont_miles, serv_cont_months, serv_cont_cost,
--   serv_cont_str_dat, serv_cont_exp_dat, service_cont_tax
from arkona.ext_bopmast  a
join arkona.ext_bopsrvc b on a.serv_cont_company = b.record_key
  and a.bopmast_company_number = b.company_number
-- --   and b.record_key <> 0
where a.date_capped > '09/30/2023'
  and a.vehicle_type = 'U'
order by a.bopmast_stock_number  

select * from arkona.ext_bopsrvc limit 4

-- 173
-- add fi person and payplan
-- 10/27 looked thru those with fi = none but had sc other that 12/15000, found 2 where personnel needed to be updated, palacios and wilde
select b.company_number, a.bopmast_stock_number as stock_number, a.record_key as bopmast_id, a.date_capped, a.serv_cont_months, a.serv_cont_miles,
  c.fi_first_name || ' ' || c.fi_last_name as fi, c.fi_employee_number, d.payplan, e.cost_center
-- select b.*    
from arkona.ext_bopmast  a
join arkona.ext_bopsrvs b on a.record_key = b.deal_key
  and a.bopmast_company_number = b.company_number
-- --   and b.record_key <> 0
join sls.deals_by_month c on a.record_key = c.bopmast_id
  and a.bopmast_company_number = c.store_code
left join sls.personnel_payplans d on c.fi_employee_number = d.employee_number
  and thru_date > current_date
left join ukg.employees e on c.fi_employee_number = e.employee_number
where c.year_month = 202311
  and b.contract_name = 'TOYOTA MAINTENANCE'
  and a.vehicle_type = 'U'
order by a.bopmast_stock_number  

select * from sls.deals_by_month where stock_number = 'G47751A'


select * from sls.personnel where last_name = 'miller'

update sls.personnel
set fi_id = 'CDE'
where last_name = 'wilde'

select * from sls.personnel where fi_id = 'CDE'



-- potential spiff list, eg service contract > 12/15000
select b.company_name, a.bopmast_stock_number as stock_number, a.record_key as bopmast_id, a.date_capped, a.serv_cont_months, a.serv_cont_miles,
  c.fi_first_name || ' ' || c.fi_last_name as fi, c.fi_employee_number, d.payplan, e.cost_center
from arkona.ext_bopmast  a
join arkona.ext_bopsrvc b on a.serv_cont_company = b.record_key
  and a.bopmast_company_number = b.company_number
join sls.deals_by_month c on a.record_key = c.bopmast_id
  and a.bopmast_company_number = c.store_code
join sls.personnel_payplans d on c.fi_employee_number = d.employee_number
  and thru_date > current_date
left join ukg.employees e on c.fi_employee_number = e.employee_number
where c.year_month = 202310
  and b.company_name = 'TOYOTA MAINTENANCE'
  and a.vehicle_type = 'U'
  and (
    serv_cont_months > 12
    or
    serv_cont_miles > 15000)
order by left(stock_number, 1), fi    

G47751A paul miller, but fi says none

-- hmm, this says house
select * from sls.deals_by_month where stock_number = 'G47751A'

select * from sls.personnel where last_name = 'miller'

select * from sls.deals where stock_number = 'G47751A'
--f_i_manager is null
select * from arkona.ext_bopmast where bopmast_stock_number = 'G47751A'

the other 2 "none" are ok



not sure what to make of this one from longoria, Jeri is on the emails, so...
-- Thank you for the data! The only one that was off for my team was H17091T for Christie Johnson. She sold a 24 month option that did not make the list.

select * from arkona.ext_bopmast where bopmast_stock_number = 'H17091T'  --25776

select b.company_name, a.bopmast_stock_number as stock_number, a.record_key as bopmast_id, a.date_capped, a.serv_cont_months, a.serv_cont_miles
--   c.fi_first_name || ' ' || c.fi_last_name as fi, c.fi_employee_number, d.payplan, e.cost_center
from arkona.ext_bopmast  a
join arkona.ext_bopsrvc b on a.serv_cont_company = b.record_key
  and a.bopmast_company_number = b.company_number
 where a.bopmast_stock_number = 'H17091T' 
-- from mike
--  In the deal, there is an AUL warranty for $3404 and below that is another line for Toyota 
-- Maintenance for $170, bringing the total to $3574. Both are included under the service contract option in DMS

-- from anthony
-- Thanks Jon! I did find one for T10955a and I gave Cole the $25 for it. Can't believe it but also found one on a new vehicle. So that's fun!
select b.company_name, a.bopmast_stock_number as stock_number, a.record_key as bopmast_id, a.date_capped, a.serv_cont_months, a.serv_cont_miles
--   c.fi_first_name || ' ' || c.fi_last_name as fi, c.fi_employee_number, d.payplan, e.cost_center
from arkona.ext_bopmast  a
join arkona.ext_bopsrvc b on a.serv_cont_company = b.record_key
  and a.bopmast_company_number = b.company_number
 where a.bopmast_stock_number = 'T10955a' 

select * from arkona.ext_bopmast where bopmast_stock_number = 'T10955a' 

