﻿/* fi

select stock_number, fi_first_name, fi_last_name, fi_employee_number 
from sls.deals_by_month
where year_month = 202211
  and store_code = 'RY8'

select * 
from sls.deals
where stock_number = 'G45545B'


select stock_number, fi_first_name, fi_last_name, fi_employee_number 
from sls.deals_by_month
where year_month = 202211
  and stock_number = 'G45545B'

select b.*, serv_cont_amount, serv_cont_cost, gap_premium, gap_cost, finance_reserve, total_f_i_reserve, amo_total, amo_reserve
-- select *
from arkona.ext_bopmast a
join (
  select *
  from sls.deals_by_month
  where year_month = 202211
    and store_code = 'RY8') b on a.bopmast_stock_number = b.stock_number
where a.bopmast_stock_number = 'T10257'

*/ 

-- sls.deals_accounts_routes
-- generate some data to compare in the luigi project, toyota_fi.py

trying to state what i need
looks like i need to add toyota to sls.deals_accounts_routes and the rest will flow from that

drop table if exists jon.orig_deals_accounts_routes;
CREATE unlogged TABLE jon.orig_deals_accounts_routes
(
  store_code citext NOT NULL,
  page integer NOT NULL,
  line integer NOT NULL,
  gl_account citext NOT NULL,
  PRIMARY KEY (page, line, gl_account)
);



select store_code, count(*)
from sls.deals_accounts_routes
group by store_code

select store_code, count(*)
from jon.orig_deals_accounts_routes
group by store_code

drop table if exists jon.with_toyota_deals_accounts_routes;
CREATE unlogged TABLE jon.with_toyota_deals_accounts_routes
(
  store_code citext NOT NULL,
  page integer NOT NULL,
  line integer NOT NULL,
  gl_account citext NOT NULL,
  PRIMARY KEY (page, line, gl_account)
);

select store_code, count(*)
from jon.with_toyota_deals_accounts_routes
group by store_code

-- updated original to a.fxmcyy = 2022 and we look good
select *
from jon.with_toyota_deals_accounts_routes a
full outer join jon.orig_deals_accounts_routes b on a.store_code = b.store_code
  and a.page = b.page
  and a.line = b.line
  and a.gl_account = b.gl_account
where b.store_code is null  
order by a.store_code, a.page, a.line, a.gl_account

----------------------------------------------------------------------------------
-- wtf page 14 in toyota, that was caused by 
select 
  case
    when b.g_l_company_ = 'RY1' and b.consolidation_grp = '2' then 'RY2'
    when b.g_l_company_ = 'RY1' and b.consolidation_grp = '' then 'RY1'
    when b.g_l_company_ = 'RY8' and b.consolidation_grp = '' then 'RY8'
  end as store_code,
  a.*--, b.* -- a.fxmpge as the_page, INTEGER(a.fxmlne) as line, trim(b.g_l_acct_number) as gl_account
from eisglobal.sypffxmst a
inner join rydedata.ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
where a.fxmcyy = 2022
  and trim(a.fxmcde) in ('TOY', 'GM')
  and b. consolidation_grp <> '3'
  and b.factory_code in ('TOY', 'GM')
  and trim(b.factory_account) <> '331A'
  and b.company_number in ( 'RY1','RY8')
  and b.g_l_acct_number <> ''
  and (
    (a.fxmpge between 5 and 15) or
    (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
    -- exclude fin comp 
    (a.fxmpge = 17 and a.fxmlne in (1,2,3,6,7,11,12,13,16,17)))
and trim(b.g_l_acct_number) = '6271C'
-- which resulted in this output: ffxmcde for RY8 with GM & TOY
RY8	GM	 2022	850	17	3.0	3	51
RY8	TOY	2022	6270	7	1.0	4	25

select store_code, count(*) from sls.deals_accounts_routes group by store_code

------------------------------------------------------------------
-- next steps from sc_payroll_201803.py
SELECT sls.update_deals_accounting_detail(); pass
select * from sls.update_deals_gross_by_month(); pass
select * from sls.update_fi_chargebacks(); pass
select sls.update_clock_hours(); pass
select * from sls.update_consultant_payroll(); pass
-- still no fi earnings
-- had to add ry8 fi categorization to FUNCTION sls.update_deals_accounting_detail();
that fixed it, now have fi pay in ukg imports
-- still don't know what lines 8 & 9 are in toyota
select * from sls.deals_gross_by_month where year_month = 202211 and control like 'T1%'

select * from sls.deals_accounting_detail where year_month = 202211 and control like 'T1%'

select * from sls.deals_accounts_routes where store_code = 'ry8' order by page, line

select * from sls.deals_accounts_routes where gl_account = '4430'

select * from fin.dim_account where account = '4430'

select * from sls.deals_gross_by_month where control = 't10257'

select * from sls.fi_chargebacks where stock_number = 't10257'

