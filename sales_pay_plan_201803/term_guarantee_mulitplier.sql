﻿with
  open_month as (
    select 201808 as year_month)
  select a.employee_number, 
    round(((select max(wd_in_month)::numeric from dds.dim_date where year_month = (select year_month from open_month)) 
      - (select wd_of_month_elapsed  from dds.dim_date where the_date = a.start_date)::numeric)/
                (select max(wd_in_month)::numeric from dds.dim_date where year_month = b.year_month), 4) as guarantee_multiplier
  from sls.personnel a
  inner join dds.dim_date b on a.start_date = b.the_date
    and b.year_month = (select year_month from open_month)

so what i need is a term multiplier
fraction of month worked


with
  open_month as (
    select 201808 as year_month)
  select a.employee_number, 
    round(((select wd_of_month_elapsed  from dds.dim_date where the_date = a.end_date)::numeric)/
                (select max(wd_in_month)::numeric from dds.dim_date where year_month = b.year_month), 4) as guarantee_multiplier
  from sls.personnel a
  inner join dds.dim_date b on a.end_date = b.the_date
    and b.year_month = (select year_month from open_month) 

-- this is cleaner, don't need to select total working days, we already have it as sum of elapsed + remaining
with
  open_month as (
    select 201808 as year_month)
  select a.employee_number, 
    round(wd_of_month_elapsed::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as term_guarantee_multiplier
  from sls.personnel a
  inner join dds.dim_date b on a.end_date = b.the_date
    and b.year_month = (select year_month from open_month) 


-- do the same thing with the guarantee_multiplier for new hires
with
  open_month as (
    select 201808 as year_month)
select a.employee_number, 
  round(wd_of_month_remaining::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as guarantee_multiplier 
from sls.personnel a
inner join dds.dim_date b on a.start_date = b.the_date
  and b.year_month = (select year_month from open_month)


select *
from dds.dim_date
where year_month = 201808
order by the_date    