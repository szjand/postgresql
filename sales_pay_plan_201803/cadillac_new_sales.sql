﻿select psc_last_name, count(*)
from (
  select stock_number, psc_last_name, count(*)
  from sls.deals_by_month
  where make = 'cadillac'
    and year_month between 201808 and 201901
    and vehicle_type = 'New'
    and psc_last_name  <> 'HSE'
  group by stock_number, psc_last_name
  having sum(unit_count) > 0) X
group by psc_last_name  


-- warmack wants 2019 sales

select a.model_year, a.model, c.bopname_search_name, c.phone_number, c.business_phone, c.cell_phone
from sls.deals_by_month a
left join sls. deals b on a.stock_number = b.stock_number
left join arkona.xfm_bopname c on  b.buyer_bopname_id = c.bopname_record_key
  and c.current_row
where a.make = 'cadillac'
  and a.vehicle_type = 'New'
  and a.year_month > 201812