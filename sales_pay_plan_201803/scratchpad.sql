﻿/*


select a.store_code, a.last_name, a.first_name, ry1_id, ry2_id, fi_id, a.employee_number as id, b.user_key as user
from sls.personnel a
left join nrv.users b on a.employee_number = b.employee_number
order by store_code, last_name

select * from sls.deals_by_month where year_month = 201803 order by psc_last_name

*/

do
$$
declare
  _employee_number citext := '128530';
  _year_month integer := 201803;
  _user_key uuid := (
    select user_key
    from nrv.users
    where employee_number = _employee_number);  
  _payplan citext := (
    select payplan
    from sls.personnel_payplans
    where employee_number = _employee_number
      and thru_date > current_date);

begin

drop table if exists wtf;
create temp table wtf as
  select row_to_json(zzz)
  from (
    select row_to_json(xxx) as sales_pay
    from ( 
      with
        consultant_deals as (
            select stock_number, model_year::citext || ' ' || make || ' ' || model as vehicle, buyer, 
              case 
                when ssc_employee_number = 'none' then unit_count
                when ssc_employee_number <> 'none' then .5 * unit_count
              end as unit_count
            from sls.deals_by_month
            where year_month = _year_month
              and psc_employee_number = _employee_number    
            union all
            select stock_number, model_year::citext || ' ' || make || ' ' || model as vehicle, buyer, .5 * unit_count
            from sls.deals_by_month
            where year_month = _year_month
              and ssc_employee_number = _employee_number),
        the_count as (
          select sum(unit_count) as unit_count
          from consultant_deals),
        fi_deals as (
            select a.stock_number, a.model_year::citext || ' ' || a.make || ' ' || a.model as vehicle, a.buyer, b.fi_gross
            from sls.deals_by_month a
            left join sls.deals_gross_by_month b on a.stock_number = b.control
            where a.year_month = _year_month
              and a.fi_employee_number = _employee_number)         
      select _user_key as user, z.*,
        ( -- consultdeals: an array of deal objects: multiple rows
          select coalesce(array_to_json(array_agg(row_to_json(g))), '[]')
          from ( -- g one row per deal
            select *
            from consultant_deals) g) as consultant_deals,
        ( -- f/i deals: an array of deal objects: multiple rows
          select coalesce(array_to_json(array_agg(row_to_json(gg))), '[]')
          from ( -- gg one row per deal
            select *
            from fi_deals) gg) as fi_deals           
      from (-- z all the individual's pay data + total pay:  single row
        select x.*, coalesce(deal_earnings, 0) + coalesce(fi_earnings, 0) as total_earnings
        from (  -- x all the individuals pay data elements: single row
          select _employee_number as id, 
            (select sls.per_unit_earnings(_employee_number, (select unit_count from the_count))) as deal_earnings,
            coalesce(round(.16 * (select sum(fi_gross) from fi_deals), 2), 0) as fi_earnings, 
            coalesce((select sum(fi_gross) from fi_deals), 0) as fi_gross, 
            (select unit_count from the_count) as total_deal_units) x) z) xxx) zzz;
end
$$;
select * from wtf    