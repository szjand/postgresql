﻿-- sales payroll year end
-- December payroll will be all deals thru 12/26
-- deals for 12/27 - 12/31 will be paid on the Jan 15th check
-- so, for each payroll,  i need to generate a payroll spreadsheet for kim
-- and a detail list for nick and jared
-- 
/*
1/1/2019, updated sls.months and sls.is_payroll_submitted
insert into sls.is_payroll_submitted values
('RY1', 201812),
('RY2', 201812);

update sls.months
set open_closed = 'closed'
where year_month = 201812;

update sls.months
set open_closed = 'open'
where year_month = 201901;
*/
-- 
-- this generates the spreadsheet for kim:   select * from sls.json_get_payroll_for_kim('RY1')
-- create a spreadsheet for her
-- store all data in a table (for generating a diff, later)

update sls.months set open_closed = 'closed' where  year_month = 201901;
update sls.months set open_closed = 'open' where  year_month = 201812;
delete from sls.is_payroll_submitted where year_month = 201812;

drop table sls.ry1_201812_part_2_payroll_kim;
create table sls.ry1_201812_part_2_payroll_kim as
with
  open_month as (
    select year_month
    from sls.months
    where open_closed = 'open')
select a.last_name || ', ' || a.first_name as consultant, a.employee_number, 
  a.pto_pay as "PTO (74)",
  coalesce(d.adjusted_amount, 0) as "Other - Adjustments (79)",
  coalesce(e.additional_comp, 0) as "Other - Addtl Comp (79)",
  round (
    case -- paid spiffs deducted only if base guarantee
      when a.total_earned >= a.guarantee then
        a.unit_pay - coalesce(a.draw, 0)
      else
        (a.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) - coalesce(c.spiffs, 0)
    end, 2) as "Unit Commission less draw (79)",
    a.fi_pay as "F&I Comm (79A)",   
    a.pulse as "Pulse (79B)",
    round( 
      case -- paid spiffs deducted only if base guarantee
        when a.total_earned >= a.guarantee then
          a.total_earned - coalesce(a.draw, 0) + coalesce(d.adjusted_amount, 0) + coalesce(e.additional_comp, 0)
        else
          (a.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) 
              - coalesce(c.spiffs, 0) + coalesce(e.additional_comp, 0) + coalesce(d.adjusted_amount, 0)
        end, 2) as "Total Month End Payout"      
from sls.consultant_payroll a
left join sls.pto_intervals b on a.employee_number = b.employee_number
  and a.year_month = b.year_month
left join sls.paid_by_month c on a.employee_number = c.employee_number
  and a.year_month = c.year_month
left join (
  select employee_number, sum(amount) as adjusted_amount
  from sls.payroll_adjustments
  where year_month =  (select year_month from open_month)
  group by employee_number) d on a.employee_number = d.employee_number
left join (
  select employee_number, sum(amount) as additional_comp
  from sls.additional_comp
  where thru_date > (
    select first_of_month
    from sls.months
    where open_closed = 'open')
  group by employee_number) e on a.employee_number = e.employee_number  
left join sls.personnel f on a.employee_number = f.employee_number
left join ( -- if this is the first month of employment, multiplier = days worked/days in month
select a.employee_number, 
  round(wd_of_month_remaining::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as guarantee_multiplier 
from sls.personnel a
inner join dds.dim_date b on a.start_date = b.the_date
  and b.year_month = (select year_month from open_month)) g on f.employee_number = g.employee_number      
left join ( -- if employee termed mid month, multiplier elapsed wd in month at term date/work days in month
  select a.employee_number, 
    round(wd_of_month_elapsed::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as term_guarantee_multiplier
  from sls.personnel a
  inner join dds.dim_date b on a.end_date = b.the_date
    and b.year_month = (select year_month from open_month) ) h on f.employee_number = h.employee_number         
where a.year_month = (select year_month from open_month)
and f.store_code = 'ry1' order by consultant

drop table sls.ry1_201812_part_2_payroll
  
-- for managers 
-- doesn't handle partial month : delaurier, s/b $1440.00
drop table sls.ry2_201812_part_2_payroll_mgr;
create table sls.ry2_201812_part_2_payroll_mgr as
select a.last_name, a.first_name, a.unit_count, a.unit_pay, a.fi_gross, a.chargebacks, 
  a.fi_total, a.fi_pay, a.pto_hours, a.pto_rate, a.pto_pay,
  a.pulse,  a.total_earned, a.draw, a.guarantee, b.adjusted_amount, e.additional_comp, 
  due_at_month_end + coalesce(b.adjusted_amount,0) + coalesce(e.additional_comp,0) as total_due 
from sls.consultant_payroll a
left join ( -- adjustments
  select employee_number, sum(amount) as adjusted_amount
  from sls.payroll_adjustments
  where year_month =  201812
  group by employee_number) b on a.employee_number = b.employee_number
left join (
  select employee_number, sum(amount) as additional_comp
  from sls.additional_comp
  where thru_date > (
    select first_of_month
    from sls.months
    where open_closed = 'open')
  group by employee_number) e on a.employee_number = e.employee_number 
where a.year_month = 201812 
  and left(a.employee_number, 1) = '2' 
order by a.last_name

select * from sls.ry2_201812_part_2_payroll_mgr

-- this gives me the deals & the count
create table sls.ry1_ry2_201812_part_2_deals as
select a.store_code, a.last_name, a.first_name, b.stock_number, b.unit_count, sum(unit_count) over (partition by last_name)
-- select *
from sls.personnel a
left join (
  select a.psc_employee_number, 
    case
      when ssc_last_name = 'none' then unit_count
      else 0.5 * unit_count
    end as unit_count, stock_number
  from sls.deals_by_month a
  where a.year_month = 201812
  union all
  select a.ssc_employee_number, 
    0.5 * unit_count as unit_count, stock_number
  from sls.deals_by_month a
  where a.year_month = 201812
    and a.ssc_last_name <> 'none') b on a.employee_number = b.psc_employee_number 
where end_date > current_Date    
  and last_name <> 'HSE'
order by a.store_code, a.last_name


-- and part 2 for january
-- kim
-- ? haley, tarr negative commission less draw
-- does it really matter, part 2 exactly equals the payroll page
-- what to do with part 2
select a.consultant, a.employee_number, 
  a."PTO (74)" - b."PTO (74)" as "PTO (74)",
  a."Other - Adjustments (79)" - b."Other - Adjustments (79)" as "Other - Adjustments (79)",
  a."Other - Addtl Comp (79)" - b."Other - Addtl Comp (79)" as "Other - Addtl Comp (79)",
  a."Unit Commission less draw (79)" - b."Unit Commission less draw (79)" as "Unit Commission less draw (79)",
  a."F&I Comm (79A)" - b."F&I Comm (79A)" as "F&I Comm (79A)",
  a."Pulse (79B)" - b."Pulse (79B)" as "Pulse (79B)",
  a."Total Month End Payout" - b."Total Month End Payout"  as "Total Month End Payout"  
from sls.ry1_201812_part_2_payroll_kim a
join sls.ry1_201812_part_1_payroll_kim b on a.consultant = b.consultant

-- gabrielson in part 1 did not exceed guarantee there fore no pulse, in part 2, met guarantee, gets pulse
select a.consultant, a.employee_number, 
  a."PTO (74)" - b."PTO (74)" as "PTO (74)",
  a."Other - Adjustments (79)" - b."Other - Adjustments (79)" as "Other - Adjustments (79)",
  a."Other - Addtl Comp (79)" - b."Other - Addtl Comp (79)" as "Other - Addtl Comp (79)",
  a."Unit Commission less draw (79)" - b."Unit Commission less draw (79)" as "Unit Commission less draw (79)",
  a."F&I Comm (79A)" - b."F&I Comm (79A)" as "F&I Comm (79A)",
  a."Pulse (79B)" - b."Pulse (79B)" as "Pulse (79B)",
  a."Total Month End Payout" - b."Total Month End Payout"  as "Total Month End Payout"  
from sls.ry2_201812_part_2_payroll_kim a
join sls.ry2_201812_part_1_payroll_kim b on a.consultant = b.consultant

-- this is extremely helpful to see what's up
select 1 as part, a.* from sls.ry2_201812_part_1_payroll_kim a 
union all
select 2, b.* from sls.ry2_201812_part_2_payroll_kim b
order by consultant, part

select 1 as part, a.* from sls.ry1_201812_part_1_payroll_kim a 
union all
select 2, b.* from sls.ry1_201812_part_2_payroll_kim b
order by consultant, part

-- managers
-- shit i fucked up sls.ry1_201812_part_1_payroll_mgr, but this will work
-- show entire actual december payroll, amt paid in dec and dif due in jan
select a.*, b."Total Month End Payout", a.total_due - b."Total Month End Payout"
from sls.ry1_201812_part_2_payroll_mgr a
left join sls.ry1_201812_part_1_payroll_kim b on a.last_name || ', ' || a.first_name = b.consultant


select a.*, b.total_due, a.total_due - b.total_due
from sls.ry2_201812_part_2_payroll_mgr a
left join sls.ry2_201812_part_1_payroll_mgr b on a.last_name = b.last_name and a.first_name = b.first_name


select *
from sls.ry1_ry2_201812_part_2_deals a
where store_code = 'ry1'
and not exists (
  select 1
  from sls.ry1_ry2_201812_part_1_deals
  where last_name = a.last_name
    and stock_number = a.stock_number)
order by last_name, stock_number

















  