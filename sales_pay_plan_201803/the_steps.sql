﻿since fi pay has been split between reserve and product for the single point pay plan
it no longer is possible to handle chargebacks
so, afton is taking them off the pages, and i need to remove them from code


start out searching in postgres functions for chargeback

1.
alter schema sls_toy
rename to z_unused_sls_toy

2.
sc_payroll_201803.py
	class Consultant_Payroll() requires FiCharbacks() , this is the only class that requires FiCharbacks()
		FUNCTION sls.update_consultant_payroll()
			hard coded fi_chargebacks to 0, removed fi_chargebacks from all calcs of fi_total

3. drop sls.create_fi_managers_payroll_import_file()
		there already existed a rename (z_unused_) version

4. rename sls.get_fi_managers_payroll_approval_data(integer, citext)
    alter function sls.get_fi_managers_payroll_approval_data(integer, citext)
    rename to z_unused_get_fi_managers_payroll_approval_data

5. rename ukg.get_sales_fi_payroll(citext, integer)
    alter function ukg.get_sales_fi_payroll(citext, integer)
    rename to z_unused_get_sales_fi_payroll   

6. rename ukg.get_sales_fi_payroll(citext)
    alter function ukg.get_sales_fi_payroll(citext)
    rename to z_unused_get_sales_fi_payroll 

7. rename FUNCTION sls.update_fi_chargebacks()
		alter function sls.update_fi_chargebacks()
		rename to z_unused_update_fi_chargebacks
    