﻿select record_key, bopmast_stock_number, bopmast_search_name, primary_salespers, date_capped, date_deleted from arkona.xfm_bopmast where bopmast_vin = '2GCVKPEC7K1127606'

53844;  G35074; UNGER, MATTHEW CARL;   DHA; 2019-03-29; 2019-04-08
54186;  G35074; OMODT, TABITHA ISABEL; MME; 2019-04-18; 9999-12-31

sold by haley in march, unwound in april
sold in april by menard
menards f/i shows -583.84, which was the f/i from the march deal, should show flat $100

select * from sls.deals_gross_by_month where control = 'G35074'

select * from sls.deals where stock_number = 'G35074'

select * from sls.deals_Accounting_detail where control = 'g35074' and gross_category = 'fi' and year_month = 201904 


-- sls.update_Deals_gross_by_month()
    select year_month, control,
      sum(case when gross_category = 'front' and account_type in ('sale', 'Other Income')
        then -amount else 0 end) as front_sales,
      sum(case when gross_category = 'front' and account_type = 'cogs'
        then amount else 0 end) as front_cogs,
      sum(case when gross_category = 'front' then -amount else 0 end) as front_gross,
      sum(case when gross_category = 'fi' and account_type in ('sale', 'Other Income')
        then -amount else 0 end) as fi_sales,
     sum(case when gross_category = 'fi' and account_type = 'cogs' then amount else 0 end) as fi_cogs,
      sum(case when gross_category = 'fi' then -amount else 0 end) as fi_gross
    from sls.deals_accounting_detail
    where year_month = 201904
      and control = 'g35074'
    group by year_month, control;


-- sls.update_deals_accounting_detail()

  select c.year_month, a.control,
    aa.journal_code, b.account_type, b.department,
    d.store_code, d.page, d.line, d.gl_Account, b.description as account_description,
    e.description as trans_description,
    max(
      case
        when page = 17 and line in (3,13) then 'fi_chargeback'
        when page = 17 then 'fi'
        else 'front'
      end) as gross_category,
    sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
  inner join dds.dim_date c on a.date_key = c.date_key
  inner join fin.dim_account b on a.account_key = b.account_key
    and c.the_date between b.row_from_Date and b.row_thru_date
  inner join sls.deals_accounts_routes d on b.account = d.gl_account
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where post_status = 'Y'
    and c.year_month = (select year_month from sls.months where open_closed = 'open') and a.control = 'g35074' 
  group by b.description, c.year_month, a.control,
    aa.journal_code, b.account_type, b.department,
    d.store_code, d.page, d.line, d.gl_Account, e.description;    

-- this is horrible, not sure how else to fix it for now    
-- exclude the transactions not for menard from function sls.update_deals_accounting_detail()
-- this will exclude f/i from the back on but retain the f/i for menard
  select c.year_month, a.control,
    aa.journal_code, b.account_type, b.department,
    d.store_code, d.page, d.line, d.gl_Account, b.description as account_description,
    e.description as trans_description,
    max(
      case
        when page = 17 and line in (3,13) then 'fi_chargeback'
        when page = 17 then 'fi'
        else 'front'
      end) as gross_category,
    sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
  inner join dds.dim_date c on a.date_key = c.date_key
  inner join fin.dim_account b on a.account_key = b.account_key
    and c.the_date between b.row_from_Date and b.row_thru_date
  inner join sls.deals_accounts_routes d on b.account = d.gl_account
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where post_status = 'Y'
    and c.year_month = (select year_month from sls.months where open_closed = 'open') and a.control = 'g35074' 
    and not (a.trans = 4383163 and a.seq = 13)
    and not (a.trans = 4405875 and a.seq = 33)    
  group by b.description, c.year_month, a.control,
    aa.journal_code, b.account_type, b.department,
    d.store_code, d.page, d.line, d.gl_Account, e.description;   

        