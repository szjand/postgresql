﻿create table sls.is_payroll_submitted (
  store_code citext not null,
  year_month integer not null,
  primary key (store_code,year_month));

comment on table sls.is_payroll_submitted is 'for managing open month, current month can not be closed until both stores have
submitted payroll for the current open month';

create or replace function sls.update_is_payroll_submitted(_store citext)
returns void as
$BODY$

/*

select * from sls.is_payroll_submitted

truncate sls.is_payroll_submitted;

select sls.update_is_payroll_submitted('RY2');

select year_month from sls.months where open_closed = 'open';


truncate sls.is_payroll_submitted;
update sls.months set open_closed = 'open' where year_month = 201804;
update sls.months set open_closed = 'closed' where year_month = 201805;

*/
declare
  _year_month integer := (
    select year_month
    from sls.months
    where open_closed = 'open');
    
begin

  insert into sls.is_payroll_submitted (store_code,year_month)
  values(_store, _year_month);

  if (
    select count(*) 
    from sls.is_payroll_submitted
    where year_month = _year_month) = 2 then

    update sls.months
    set open_closed = 'closed'
    where open_closed = 'open';

    update sls.months
    set open_closed = 'open'
    where seq = (
      select seq + 1
      from sls.months
      where year_month = _year_month);
  end if;
     
end;
$BODY$
language plpgsql;


comment on function sls.update_is_payroll_submitted(citext) is 'inserts a row into sls.is_payroll_submitted when the payroll is 
submitted by the GSM. When both payrolls have been submitted, resets the open_month to the new month';