﻿
brad 1/31/2023

Zeke Bocklage and Austin Suedel have successfully completed their transition from Product Genius to selling cars. 
Effective Feb 1 they will no longer be on hourly pay. They will be on the "Sales Consultant" pay plan which 
consists of $300 per unit sold plus 10% of Finance Gross Generated. 
This is the pay plan that Craig, Jim and Arden are currently on as well. 
We will continue to use this pay plan to bridge the time between the product genius 
to Single Point Sales transition, I envision this being a 2-3 month process on average. 
I have submitted this change in Compli for Zeke and Austin. 

We currently have five other Product Genius that have expressed interest in making this transition, 
so I see it being a popular topic in the near future and want to make sure I have the process buttoned 
up to avoid any pain points for everyone involved in making it happen.

Alex Waldeck has been moved to the Outlet and John Olderback is now part of our Inventory team 
here in the GM Building. John will continue to oversee the outlet and can still be utilized for any questions or help needed. 
I have submitted this change for Alex in Compli and will manually adjust his pay for the time he was at the outlet in January. ($50 per unit sold at Outlet)

The outlet pay plan will be changing as well effective Feb.1. Currently they are being paid $300 per unit. 
The volume pay will remain the same but they will now be compensated for finance gross generated at 16%($300 per unit + 16% finance gross generated). 
I have submitted this change in compli for Alex Waldeck and Jacob Charley.

Finally, our Single Point sales pay plan will be changing effective Feb. 1. 
The progressive volume pay will remain the same but the 16% of Finance Gross Generated will be changing. 
It will now be 10% of Finance Reserve Generated and 20% of Finance Gross Generated from Product. 
(Unit Pay + 10% Finance Reserve + 20% Finance Gross Generated from Product). Rick Stout has done an awesome 
job putting this in motion and it has been a very positive thing at the Honda store already. 
With any questions on this please see Rick.

To recap the current pay plans are 
Auto Outlet ($300 per unit + 16% Finance Gross Generated), 16% total fi
Product Genius (hourly), 
Sales Consultant ($300 per unit + 10% Finance Gross Generated) 10% total fi
and Single Point  (Progressive Unit Pay + 10% Finance Reserve + 20% Finance Gross Generated from Product). 
These names will be how we will refer to the pay plans going forward.

also
Croaker pay plan
Tarr pay plan

select *
from sls.consultant_payroll
where year_month = 202301

questions
guarantee based on 3 month rolling <> 10, 24/3000  
is croaker still on $400 per unit

is the "Sales Consultant" payplan (the replacement for standard) no long progressive
current progressive per unit matrix for the standard payplan
	 1-14: 300 per unit
	15-20: 350 per unit
	21-25: 400 per unit
	  >25: 450 per unit
	  
is the "Single Point" payplan (the replacement for executive) progressive per unit matrix still:
	 1-14: 250 per unit
	15-20: 275 per unit
	21-25: 300 per unit
	  >25: 325 per unit


------------------------------------------------------------------------------------------------
Questions sent
1. Is the guarantee still based on the 3 month rolling average 
        Less than 10 units is 2400, 10 or more is 3000? 
        The first 3 months of employment (for non-hourly employees) have the 3000/month guarantee
 
2. is Craig Croaker still to be paid $400 per unit? 
 
3 .  is the "Sales Consultant" pay plan (the replacement for standard)  no long progressive? 
	current progressive per unit matrix for the standard pay plan 
                 1-14: 300 per unit 
                15-20: 350 per unit 
                21-25: 400 per unit 
                   >25: 450 per unit 
 
4. is the "Single Point" pay plan (the replacement for executive)  progressive per unit matrix still: 
                 1-14: 250 per unit 
                15-20: 275 per unit 
                21-25: 300 per unit 
                   >25: 325 per unit 

5. Sales Consultant pay plan, F/I portion is 10% of Finance Gross Generated and 0 (zero) on Finance Reserve.
      That is, the employees on this pay plan are paid nothing on any Finance Reserve on their deals.
      Correct?
-- new questions
Jeff Tarr??

10% total finance for sales consultant pay plan

	 sales consultant no progressive 

------------------------------------------------------------------------
1. new payplans
select * from sls.payplans;
insert into sls.payplans values
('tarr'),
('single point'),
('sales consultant');

-------------------------------------------------------------------------------------------------------
2. split f/i	 
-------------------------------------------------------------------------------------------------------
3. outlet: olderback, waldeck, new pay plan including fi
select * from sls.personnel where last_name = 'olderbak'
select * from sls.team_personnel where employee_number = '1106225'
update sls.team_personnel
set thru_date = '01/31/2023'
where employee_number = '1106225'
  and thru_date > current_date

select sls.update_consultant_payroll()  



-------------------------------------------------------------------------------------------------------
5. FUNCTION sls.update_consultant_payroll()
add attributes to sls.consultant_payroll: fi_product, fi_reserve
the function gets fi from sls.deals_gross_by_month

select * from sls.deals_gross_By_month where year_month = 202302

sls.deals_accounts_routes -> sls.deals_accounting_detail -> sls.deals_gross_by_month

select * from sls.deals_accounts_routes where gl_account = '6271c'

/*
stop the presses, looks like accounts routes has problems says line 1, actually line 3
the issue is in the query that generates sls.deals_accounts_routes in luigi.AccountsRoutes
I am doiong something goofy with toyota in the where clause where i set sypffxmst.fxmcde = 'TOY'
That generates some weird routing, think it must be for the toyota (non gm) statement
BUT this query that i do for routing here cleans it up, it definitely does the GM routing
so the initial issue is simply where do i expose the fi type?
At some point i need to go back to the luigi and figure out what's up with that query
do i add an attribute to sls.deals_accounting_detail, or add the detail in sls.deals_gross_by_month
leaning towards adding it to table sls.deals_gross_by_month
so
fi_sales would become fi_product_sales, fi_reserve_sales
fi_cogs would become fi_products_cogs, fi_reserve_cogs
that way i will have the granular data that can be aggregated based on the relevant pay plan
and the relevant gross would be figured in update_consultant_payroll
*/

gross_category is in sls.deals_Accounting_detail
populated by FUNCTION sls.update_deals_accounting_detail()
-- i can leave chargebacks out of this query, they are figured separately
select a.*, bb.fi_type
from sls.deals_Accounting_detail a
left join (
	select a.fxmact, a.fxmpge, a.fxmlne, 
		case
			when b.consolidation_grp = '2' then 'RY2'
			else company_number
		end as store, 
		b.g_l_acct_number,
		case
			when a.fxmlne in (3, 13) then 'Chargeback'
			when a.fxmlne in (1, 11) then 'Reserve'
			else 'Product'
		end as fi_type    
	from arkona.ext_eisglobal_sypffxmst a
	join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
		and b.factory_financial_year = 2023
	where a.fxmcyy = 2023
		and a.fxmpge = 17
		and a.fxmlne in (1,2,5,6,7,11,12,15,16,17)) bb on a.gl_account = bb.g_l_acct_number -- exclude 3,13
where a.year_month = 202302
  and a.gross_category like 'fi%'
order by control

select * 
from sls.deals_Gross_by_month
where year_month = 202302


-- this is the update query from FUNCTION sls.update_deals_gross_by_month()
--     insert into sls.deals_gross_by_month
-- leave the total fi sales & cogs attributes 
-- not so sure i should delte chargebacks from this query, because of the problem with Toyota,
-- the function sls.update_fi_chargebacks() derives chargebacks from sls.deals_accounting_detail,
-- not sure the 2 sources for fi categorization match up
-- getting a bit late and my mind is a bit fuzzy - need to sort this out so that i am doing chargebacks correctly
create table jon.deals_gross_by_month_temp as
    select year_month, control,
      sum(case when gross_category = 'front' and account_type in ('sale', 'Other Income')
        then -amount else 0 end) as front_sales,
      sum(case when gross_category = 'front' and account_type = 'cogs'
        then amount else 0 end) as front_cogs,
      sum(case when gross_category = 'front' then -amount else 0 end) as front_gross,
      sum(case when gross_category = 'fi' and account_type in ('sale', 'Other Income')
        then -amount else 0 end) as fi_sales,      
      sum(case when gross_category = 'fi' and account_type = 'cogs' then amount else 0 end) as fi_cogs,
      0 as fi_gross,
      coalesce(sum(amount) filter (where gl_account in ('180601','180801','180803')), 0) as acq_fee,
      sum(case when gross_category = 'fi' and account_type in ('sale', 'Other Income') and bb.fi_type = 'Product'
        then -amount else 0 end) as fi_product_sales,
			sum(case when gross_category = 'fi' and account_type = 'cogs' and bb.fi_type = 'Product' then amount else 0 end) as fi_product_cogs,        
      sum(case when gross_category = 'fi' and account_type in ('sale', 'Other Income') and bb.fi_type = 'Reserve'
        then -amount else 0 end) as fi_reserve_sales,   
      sum(case when gross_category = 'fi' and account_type = 'cogs' and bb.fi_type = 'Reserve' then amount else 0 end) as fi_reserve_cogs           
    from sls.deals_accounting_detail a
		left join (
			select a.fxmact, a.fxmpge, a.fxmlne, 
				case
					when b.consolidation_grp = '2' then 'RY2'
					else company_number
				end as store, 
				b.g_l_acct_number,
				case
				  when a.fxmlne in (3,13) then 'fi_chargeback'
					when a.fxmlne in (1, 11) then 'Reserve'
					else 'Product'
				end as fi_type    
			from arkona.ext_eisglobal_sypffxmst a
			join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
				and b.factory_financial_year = 2023
			where a.fxmcyy = 2023
				and a.fxmpge = 17
				and a.fxmlne in (1,2,3,5,6,7,11,12,13,15,16,17)) bb on a.gl_account = bb.g_l_acct_number  
    where a.year_month = 202302
    group by a.year_month, a.control;
      
alter table sls.deals_gross_by_month
add column fi_product_sales numeric(8,2),
add column fi_product_cogs numeric(8,2),
add column fi_reserve_sales numeric(8,2),
add column fi_reserve_cogs numeric(8,2);


--now i am thinking it needs to be in sls.deals_accounting_detail

-- checking out  chargebacks
-- the issue is exclusively with Toyota
-- deals_Accounting_detail has page 7 instead of page 17
-- that becomes A HUGE TODO, figure out why page 7 is getting generated for toyota in sls.deals_accounting_detail
-- it is because of accounts routes
select * 
from sls.deals_accounting_detail a
		left join (
			select a.fxmact, a.fxmpge, a.fxmlne, 
				case
					when b.consolidation_grp = '2' then 'RY2'
					else company_number
				end as store, 
				b.g_l_acct_number,
				case
				  when a.fxmlne in (3,13) then 'fi_chargeback'
					when a.fxmlne in (1, 11) then 'Reserve'
					else 'Product'
				end as fi_type    
			from arkona.ext_eisglobal_sypffxmst a
			join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
				and b.factory_financial_year = 2023
			where a.fxmcyy = 2023
				and a.fxmpge = 17
				and a.fxmlne in (1,2,3,5,6,7,11,12,13,15,16,17)) bb on a.gl_account = bb.g_l_acct_number  
where a.year_month = 202302
  and store_code = 'RY8' and control like 'T1%'
  and a.line <> bb.fxmlne
order by a.gross_category


-- this shows me that accounts_routes has the wrong line numbers
-- comparing to fi_routing.xlsx
-- which is why i need to 
select a.account_number, a.account_desc, a.feb_balance02, b.*
from arkona.ext_glpmast a
join sls.deals_accounts_routes b on a.account_number = b.gl_account
  and b.store_code = 'RY8' and b.page = 7
where a.year = 2023
  and a.company_number = 'RY8'
order by a.account_number


select * from sls.deals_accounts_routes where store_code = 'RY8' and gl_account not in ('4250') order by gl_account

create table jon.accounts_routes_test (
  store citext,
  page citext,
  line citext,
  account citext);

select * 
from sls.deals_accounts_routes a
full outer join jon.accounts_routes_test b on a.gl_account = b.account


select * 
from (
	select a.*, b.store, b.page as page_2, b.line as line_2, b.account 
	from sls.deals_accounts_routes a
	full outer join jon.accounts_routes_test b on a.gl_account = b.account ) aa
left join fin.dim_Account bb on aa.gl_account = bb.account
  and bb.current_row
order by aa.store_code, aa.page, aa.line  

select * 
from (
	select a.*, b.store, b.page::integer as page_2, b.line::integer as line_2, b.account 
	from sls.deals_accounts_routes a
	full outer join jon.accounts_routes_test b on a.gl_account = b.account ) aa
left join fin.dim_Account bb on aa.gl_account = bb.account
  and bb.current_row
where aa.line <> aa.line_2  
order by aa.store_code, aa.page, aa.line  


/* 
		OK I AM CONVINCED, I CAN MAKE THE CHANGE IN THE QUERY THAT GENERATES SLS.DEALS_ACCOUNTS_ROUTES
	  AND I WILL BE RID OF THE TOYOTA CONFLICT
	  FROM THE ABOVE TEST, THE MISSING ACCOUNTS IN THE NEW WAY ARE ALL AFTERMARKET AND FIXED, THIS IS ABOUT DEALS ONLY
	  This also explains why there were so many toyota ros in sls.deals_accounting_detail and
	  subsequently sls.deals_gross_by_month

	  I CAN DO ONE MORE TEST, COMPARE SLS.DEALS_ACCOUNTING_DETAIL BASED ON THE OLD VERSION OF ACCOUNTS_ROUTES VS THE NEW
	  AND IT WORKS

	  generated a table jon.accounts_routes_test from dealertrack using the new where clause for comparing
*/
-- looking good
select * --0- 2877
from sls.deals_accounting_detail a
full outer join (
  select c.year_month, a.control,
    aa.journal_code, b.account_type, b.department,
    d.store, d.page, d.line, d.Account, b.description as account_description,
    e.description as trans_description,
    max(
      case
        when d.store <> 'RY8' and page::integer = 17 and line::integer in (3,13) then 'fi_chargeback'
        when d.store <> 'RY8' and page::integer = 17 then 'fi'
        when d.store = 'RY8' and page::integer = 7 and line::integer in (3,13) then 'fi_chargeback'
        when d.store = 'RY8' and page::integer = 7 then 'fi'
        else 'front'
      end) as gross_category,
    sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
  inner join dds.dim_date c on a.date_key = c.date_key
  inner join fin.dim_account b on a.account_key = b.account_key
    and c.the_date between b.row_from_Date and b.row_thru_date
  inner join jon.accounts_routes_test /*sls.deals_accounts_routes*/ d on b.account = d.account -- d.gl_account
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where post_status = 'Y'
    and c.year_month = (select year_month from sls.months where open_closed = 'open')
    and not (a.trans = 4383163 and a.seq = 13)
    and not (a.trans = 4405875 and a.seq = 33)       
  group by b.description, c.year_month, a.control,
    aa.journal_code, b.account_type, b.department,
    d.store, d.page, d.line, d.Account, e.description) bb on a.control = bb.control and a.gl_Account = bb.account
where a.year_month = 202302 
  and a.control not like '8%' -- ROs
--   and bb.year_month is null


-- ok aggregate at control level and compare 
-- looks great
select * 
from ( --805
	select store_code, control, sum(amount) as amount
	from sls.deals_accounting_detail a
	where a.year_month = 202302 
		and a.control not like '8%'
	group by store_code, control) x  
full outer join ( -- 804
select store, control, sum(amount) as amount
from (
  select c.year_month, a.control,
    aa.journal_code, b.account_type, b.department,
    d.store, d.page, d.line, d.Account, b.description as account_description,
    e.description as trans_description,
    max(
      case
        when d.store <> 'RY8' and page::integer = 17 and line::integer in (3,13) then 'fi_chargeback'
        when d.store <> 'RY8' and page::integer = 17 then 'fi'
        when d.store = 'RY8' and page::integer = 7 and line::integer in (3,13) then 'fi_chargeback'
        when d.store = 'RY8' and page::integer = 7 then 'fi'
        else 'front'
      end) as gross_category,
    sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
  inner join dds.dim_date c on a.date_key = c.date_key
  inner join fin.dim_account b on a.account_key = b.account_key
    and c.the_date between b.row_from_Date and b.row_thru_date
  inner join jon.accounts_routes_test /*sls.deals_accounts_routes*/ d on b.account = d.account -- d.gl_account
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where post_status = 'Y'
    and c.year_month = (select year_month from sls.months where open_closed = 'open')
    and not (a.trans = 4383163 and a.seq = 13)
    and not (a.trans = 4405875 and a.seq = 33)       
  group by b.description, c.year_month, a.control,
    aa.journal_code, b.account_type, b.department,
    d.store, d.page, d.line, d.Account, e.description) aa
group by store, control) y on x.control = y.control 
where x.amount <> y.amount

-------------------------------------------------------------------------------------------------------

So, now to backtrack, start over
1st thing to do is to clean sls.deals_accounts_routes
-- 2/23 2:04
reran class AccountsRoutes locally in luigi, fi routing now looks good
select * from sls.deals_accounts_routes where store_code = 'RY8' and page = 17

-------------------------------------------------------------------------------------------------------

the question has become, at what point do i persist the categorization of fi (product, reserve, chargeback)
i think just like below, expand the gross_category in sls.deals_accounting_detail to front, fi_chargeback, fi_product & fi_reserve
that should work for  sls.update_fi_chargebacks as well
-- 2/23 2:37 
updated the function  sls.update_deals_accounting_detail()
reran class DealsAccountingDetail locally in luigi
   select * from sls.deals_Accounting_Detail where year_month = 202302  and  page = 17 order by store_code, line
   
-------------------------------------------------------------------------------------------------------

now, on to sls.deals_gross_by_month
FUNCTION sls.update_deals_gross_by_month()

select * from sls.deals_gross_by_month where year_month = 202302 and control not like '8%'

select * from jon.deals_gross_by_month_temp
-- this all looks good
-- thinking chargebacks are not a part of this table, that is reserved for a separate table/process
drop table if exists jon.deals_gross_by_month_temp cascade;
create table jon.deals_gross_by_month_temp as
--     insert into sls.deals_gross_by_month
    select year_month, control,
      sum(case when gross_category = 'front' and account_type in ('sale', 'Other Income')
        then -amount else 0 end) as front_sales,
      sum(case when gross_category = 'front' and account_type = 'cogs'
        then amount else 0 end) as front_cogs,
      sum(case when gross_category = 'front' then -amount else 0 end) as front_gross,
      sum(case when gross_category in ('fi_product', 'fi_reserve') and account_type in ('sale', 'Other Income')
        then -amount else 0 end) as fi_sales,
      sum(case when gross_category in ('fi_product', 'fi_reserve') and account_type = 'cogs' then amount else 0 end) as fi_cogs,
      0 as fi_gross,
      coalesce(sum(amount) filter (where gl_account in ('180601','180801','180803')), 0) as acq_fee,
      sum(case when gross_category = 'fi_product' and account_type in ('sale', 'Other Income') then -amount else 0 end) as fi_product_sales,
			sum(case when gross_category = 'fi_product' and account_type = 'cogs' then amount else 0 end) as fi_product_cogs,     
      sum(case when gross_category = 'fi_reserve' and account_type in ('sale', 'Other Income') then -amount else 0 end) as fi_reserve_sales,   
      sum(case when gross_category = 'fi_reserve' and account_type = 'cogs' then amount else 0 end) as fi_reserve_cogs           
    from sls.deals_accounting_detail
    where year_month = (select year_month from sls.months where open_closed = 'open')
    group by year_month, control;


select * 
from sls.deals_gross_by_month a
full outer join jon.deals_gross_by_month_temp b on a.control = b.control
where a.year_month = 202302 
	and a.control not like '8%'
--   and (a.fi_sales <> b.fi_sales or a.fi_cogs <> b.fi_cogs)
  and b.fi_cogs <> b.fi_product_cogs + b.fi_reserve_cogs

-- 2/23 3:22
updated function like above and ran it
select sls.update_deals_gross_by_month()  

select * from sls.deals_gross_by_month where year_month = 202302

-------------------------------------------------------------------------------------------------------
-- 2/23 3:46
function sls.update_fi_chargebacks: already uses sls.deals_accounting_detail::gross_category = fi_chargeback
good here, dont need to do anything
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
but before that, need to update sls.per_unit_matrix for the new pay plans
insert into sls.per_unit_matrix (payplan, level_1, level_2, level_3, level_4) values
('tarr',300,350,400,450),
('sales consultant',300,300,300,300),
('single point',250,275,300,325);

-- add fi info to table sls.payplans
alter table sls.payplans
add column from_date date,
add column thru_date date default '12/31/9999',
add column fi citext,
add column chargeback citext;

update sls.payplans
set fi = '16% on total F/I', chargeback = '10%', from_date = '01/01/2022'
where payplan = 'executive';

update sls.payplans
set fi = '16% on total F/I', chargeback = '10%', from_date = '02/01/2023'
where payplan = 'outlet';

update sls.payplans
set fi = '10% on total F/I', chargeback = '10%', from_date = '02/01/2023'
where payplan = 'sales consultant';

update sls.payplans
set fi = '10% on Finance Reserve plus 20% on Finance Gross from Product', chargeback = '10%', from_date = '02/01/2023'
where payplan = 'single point';

update sls.payplans
set fi = '10% on total F/I', chargeback = '10%', from_date = '02/01/2023'
where payplan = 'croaker';

update sls.payplans
set fi = 'n/a', chargeback = 'n/a', from_date = '01/01/2022'
where fi is null;
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------



first need to get everybody assigned to the proper payplan

select * from sls.personnel where last_name = 'warmack'

select * from sls.personnel_payplans where employee_number = '1147250'

update sls.personnel_payplans
set thru_date = '01/31/2023'
where employee_number = '1147250'
  and thru_date > current_date;
insert into sls.personnel_payplans (employee_number,payplan,from_date)  
values ('1147250', 'sales consultant', '02/01/2023');


-- moving from product genius to sales consultant
Zeke Bocklage and Austin Suedel

select * from sls.personnel where last_name = 'Bocklage'

select * from sls.personnel_payplans where employee_number = '145950'

insert into sls.personnel_payplans (employee_number,payplan,from_date)  
values ('145956', 'sales consultant', '02/01/2023');

-- executive to single point

select store_code, a.last_name, a.first_name, a.employee_number, b.payplan, b.thru_date
from sls.personnel a
join sls.personnel_payplans b on a.employee_number = b.employee_number
  and b.thru_date > current_date
order by store_code, last_name  
--   and b.payplan = 'executive'
  and store_code in ('ry1','ry8')

RY1,Monreal,Jessie,115883,executive
RY1,Weber,James,1147810,executive
RY1,Vanyo,David,178543,executive
RY1,Menard,Michael,165983,executive
RY1,Rumen,Robert,141082,executive
RY1,Miller,Paul,164562,executive
RY1,Turner,Richard,195231,executive
RY1,Michael,Nicholas,195480,executive
RY1,Magenau,Savanah,189768,executive
RY1,Schlenvogt,Kody,198373,executive
RY8,Console,Bradley,8100204,executive
RY8,Suedel,Cole,8100229,executive
RY8,Marion,Jace,8100218,executive
RY8,Hagen,Mason,8100207,executive

update sls.personnel_payplans
set thru_date = '01/31/2023'
where employee_number = '8100207'
  and thru_date > current_date;
insert into sls.personnel_payplans (employee_number,payplan,from_date)  
values ('8100207', 'single point', '02/01/2023');  

-- some cleanup
update sls.personnel_payplans
set thru_date = '01/31/2023'
where employee_number = '1106225'

select * from sls.personnel_payplans where employee_number = '1106225' and thru_date > current_date


!!!!!!!!!!!!!!!!!!!!
Zeke Bocklage and Austin Suedel
when a product genius converts to a sales consultant, need to reset first_full_month_of_emp, 
this sets the 3 months for which the consultant automatically gets a guarantee of $3000

select * from sls.personnel where last_name in ('bocklage','suedel')

update sls.personnel
set first_full_month_emp = 202302
where employee_number in ('145950','145956');


-- 3 honda consultants on single point
select * from sls.personnel where last_name in ('spencer','johnson','rose')

update sls.personnel_payplans
set thru_date = '01/31/2023'
where employee_number = '259875'
  and thru_date > current_date;
insert into sls.personnel_payplans (employee_number,payplan,from_date)  
values ('259875', 'single point', '02/01/2023');  

update sls.personnel_payplans
set thru_date = '01/31/2023'
where employee_number = '2130678'
  and thru_date > current_date;
insert into sls.personnel_payplans (employee_number,payplan,from_date)  
values ('2130678', 'single point', '02/01/2023');  

update sls.personnel_payplans
set thru_date = '01/31/2023'
where employee_number = '243876'
  and thru_date > current_date;
insert into sls.personnel_payplans (employee_number,payplan,from_date)  
values ('243876', 'single point', '02/01/2023');  


select sls.update_consultant_payroll()  
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
-- 02/26 4:59

-- 02/26 6:45 not as bad as i thought, it is done
update_consultant_payroll, think this is going to be a biggy, yep, huge

select * from sls.deals_gross_by_month where year_month = 202302 limit 20

select * from sls.personnel_payplans where employee_number = '1147810'

select * from sls.deals_gross_by_month where

--       left join ( -- cc: fi gross
        select fi_employee_number, fi_last_name, sum(fi_gross) as fi_gross, 
          sum(fi_product_gross) as fi_product_gross, sum(fi_reserve_gross) as fi_reserve_gross, 
          payplan
        from (    
          select a.fi_employee_number, a.fi_last_name, c.payplan,
            case when c.payplan in ('executive','croaker','outlet','sales consultant') then b.fi_sales - b.fi_cogs end as fi_gross,
            case when c.payplan = 'single point' then b.fi_product_sales - b.fi_product_cogs end as fi_product_gross,
            case when c.payplan = 'single point' then b.fi_reserve_sales - b.fi_reserve_cogs end as fi_reserve_gross
          from sls.deals_by_month a
          left join sls.deals_gross_by_month b on a.stock_number = b.control
            and a.year_month = b.year_month
          left join sls.personnel_payplans c on a.fi_employee_number = c.employee_number
            and c.thru_date > current_date  
          where a.year_month = 202302) aaa
        group by aaa.fi_employee_number, fi_last_name, payplan



select fi_employee_number, sum(amount) 
from sls.fi_chargebacks
where chargeback_year_month = 202302
group by fi_employee_number



select * from sls.deals_Accounting_detail where year_month = 202302 and gross_category = 'fi_chargeback'




















-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------




