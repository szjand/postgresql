﻿-- Function: sls.json_get_deals_by_consultant_month(citext, integer)

-- DROP FUNCTION sls.json_get_deals_by_consultant_month(citext, integer);

CREATE OR REPLACE FUNCTION sls.json_get_deals_by_consultant_month(
    _employee_number citext,
    _year_month integer)
  RETURNS SETOF json AS
$BODY$  

/*

select * from sls.json_get_deals_by_consultant_month('110052', 201803);

*/
declare

  _user_key uuid := (
    select user_key
    from nrv.users
    where employee_number = _employee_number);    
  _payplan citext := (
    select payplan
    from sls.personnel_payplans
    where employee_number = _employee_number
      and thru_date > current_date);
          
  begin
  return query
  select row_to_json(zzz)
  from (
    select row_to_json(xxx) as sales_pay
    from ( 
      with
        consultant_deals as (
            select stock_number, model_year::citext || ' ' || make || ' ' || model as vehicle, buyer, 
              case 
                when ssc_employee_number = 'none' then unit_count
                when ssc_employee_number <> 'none' then .5 * unit_count
              end as unit_count
            from sls.deals_by_month
            where year_month = _year_month
              and psc_employee_number = _employee_number    
            union all
            select stock_number, model_year::citext || ' ' || make || ' ' || model as vehicle, buyer, .5 * unit_count
            from sls.deals_by_month
            where year_month = _year_month
              and ssc_employee_number = _employee_number),
        the_count as (
          select sum(unit_count) as unit_count
          from consultant_deals),
        fi_deals as (
            select a.stock_number, a.model_year::citext || ' ' || a.make || ' ' || a.model as vehicle, a.buyer, b.fi_gross
            from sls.deals_by_month a
            left join sls.deals_gross_by_month b on a.stock_number = b.control
            where a.year_month = _year_month
              and a.fi_employee_number = _employee_number)         
      select _user_key as user, z.*,
        ( -- consultdeals: an array of deal objects: multiple rows
          select coalesce(array_to_json(array_agg(row_to_json(g))), '[]')
          from ( -- g one row per deal
            select *
            from consultant_deals) g) as consultant_deals,
        ( -- f/i deals: an array of deal objects: multiple rows
          select coalesce(array_to_json(array_agg(row_to_json(gg))), '[]')
          from ( -- gg one row per deal
            select *
            from fi_deals) gg) as fi_deals           
      from (-- z all the individual's pay data + total pay:  single row
        select x.*, deal_earnings + fi_earnings as total_earnings
        from (  -- x all the individuals pay data elements: single row
          select _employee_number as id, 
            (select sls.per_unit_earnings(_employee_number, (select unit_count from the_count))) as deal_earnings,
            round(.16 * (select sum(fi_gross) from fi_deals), 2) as fi_earnings, 
            (select sum(fi_gross) from fi_deals) as fi_gross, 
            (select unit_count from the_count) as total_deal_units) x) z) xxx) zzz;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sls.json_get_deals_by_consultant_month(citext, integer)
  OWNER TO postgres;
