﻿select *
from sls.paid_by_month


  select company_number, employee_, year_month,
    sum(case when code_id = '79' then amount else 0 end) as commission,
    sum(case when code_id = '78' then amount else 0 end) as draw,
    sum(case when code_id = 'OVT' then amount else 0 end) as overtime,
    sum(case when code_id = '400' then amount else 0 end) as pto_pay_out,
    sum(case when code_id = '500' then amount else 0 end) as lease,
    sum(case when code_id = '74' then amount else 0 end) as vacation,
    sum(case when code_id = '82' then amount else 0 end) as spiffs
  from (-- c: pyhscdta 
    select a.company_number, a.employee_,
      ((2000 + a.check_year) *100) + a.check_month as year_month,
      sum(b.amount) as amount, b.code_id
    from arkona.ext_pyhshdta a
    inner join sls.personnel aa on a.employee_ = aa.employee_number
    inner join arkona.ext_pyhscdta b on a.payroll_run_number = b.payroll_run_number
      and a.company_number = b.company_number
      and a.employee_ = b.employee_number
      and b.code_type in ('0','1') -- 0,1: income, 2: deduction
    where a.payroll_cen_year in (117,118)     
    group by a.company_number, a.employee_name, a.employee_,
      ((2000 + a.check_year) *100) + a.check_month, b.code_id) c
  group by company_number, employee_, year_month      


select * from arkona.ext_pypcodes order by ded_pay_code


select *
from (
  select a.company_number, a.employee_, aa.last_name || ', ' || aa.first_name,
    ((2000 + a.check_year) *100) + a.check_month as year_month,
    sum(b.amount) as amount, b.code_id
  from arkona.ext_pyhshdta a
  inner join sls.personnel aa on a.employee_ = aa.employee_number
  inner join arkona.ext_pyhscdta b on a.payroll_run_number = b.payroll_run_number
    and a.company_number = b.company_number
    and a.employee_ = b.employee_number
    and b.code_type in ('0','1') -- 0,1: income, 2: deduction
  where a.payroll_cen_year in (117,118)     
  group by a.company_number, a.employee_name, a.employee_,
    ((2000 + a.check_year) *100) + a.check_month, b.code_id , aa.last_name || ', ' || aa.first_name) g
left join (
select company_number, ded_pay_code, description
from arkona.ext_pypcodes      
where company_number in ('RY1','RY2')
--   and code_type = '1'
group by company_number, ded_pay_code, description) h on g.company_number = h.company_number
  and g.code_id = h.ded_pay_code   
order by h.ded_pay_code   

select * from sls.consultant_payroll


-- looks good, matches submittal output
with
  open_month as (
    select year_month
    from sls.months
    where open_closed = 'open')
select a.total_earned, a.first_name || ' ' || a.last_name as consultant, a.employee_number, 
  a.pto_pay as "PTO (74)",
  coalesce(d.adjusted_amount, 0) as "Other - Adjustments (79)",
  coalesce(e.additional_comp, 0) as "Other - Addtl Comp (79)",
  round (
    case -- paid spiffs deducted only if base guarantee
      when a.total_earned >= a.guarantee then
        a.unit_pay + a.pulse - coalesce(a.draw, 0)
      else
        (a.guarantee * coalesce(g.guarantee_multiplier, 1)) - coalesce(a.draw, 0) - coalesce(c.spiffs, 0)
    end, 2) as "Unit Commission less draw (79)",
    a.fi_pay as "F&I Comm (79A)",   
    round( 
      case -- paid spiffs deducted only if base guarantee
        when a.total_earned >= a.guarantee then
          a.total_earned - coalesce(a.draw, 0) + coalesce(d.adjusted_amount, 0) + coalesce(e.additional_comp, 0)
        else
          (a.guarantee * coalesce(g.guarantee_multiplier, 1)) - coalesce(a.draw, 0) 
              - coalesce(c.spiffs, 0) + coalesce(e.additional_comp, 0) + coalesce(d.adjusted_amount, 0)
        end, 2) as "Total Month End Payout"      
from sls.consultant_payroll a
left join sls.pto_intervals b on a.employee_number = b.employee_number
  and a.year_month = b.year_month
left join sls.paid_by_month c on a.employee_number = c.employee_number
  and a.year_month = c.year_month
left join (
  select employee_number, sum(amount) as adjusted_amount
  from sls.payroll_adjustments
  where year_month =  (select year_month from open_month)
  group by employee_number) d on a.employee_number = d.employee_number
left join (
  select employee_number, sum(amount) as additional_comp
  from sls.additional_comp
  where thru_date > (
    select first_of_month
    from sls.months
    where open_closed = 'open')
  group by employee_number) e on a.employee_number = e.employee_number  
left join sls.personnel f on a.employee_number = f.employee_number
left join ( -- if this is the first month of employment, multiplier = days worked/days in month
  select a.employee_number, 
    round(extract(day from a.start_date)::numeric 
      / (select max(day_of_month)::numeric from dds.dim_date where year_month = b.year_month), 2) as guarantee_multiplier
  from sls.personnel a
  inner join dds.dim_date b on a.start_date = b.the_date
    and b.year_month = (select year_month from open_month)) g on f.employee_number = g.employee_number        
where a.year_month = (select year_month from open_month)
and f.store_code = 'RY1'





















