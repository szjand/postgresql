﻿drop table if exists sls.payroll_adjustments;
create table sls.payroll_adjustments (
  employee_number citext not null,
  reason citext not null references sls.payroll_adjustment_reasons(reason),
  year_month integer not null,
  amount numeric (8,2) not null check (amount <> 0),
  note citext,
  constraint other_requires_note check (
    reason = 'Other' and note is not null and length(trim(note)) > 3),
  primary key (employee_number, reason, year_month));


  employee_number citext NOT NULL,
  reason citext NOT NULL,
  year_month integer NOT NULL,
  amount numeric(8,2) NOT NULL,
  note citext,
  CONSTRAINT payroll_adjustments_pkey PRIMARY KEY (employee_number, reason, year_month),
  CONSTRAINT payroll_adjustments_reason_fkey FOREIGN KEY (reason)
      REFERENCES sls.payroll_adjustment_reasons (reason) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION

get the constraints working      
then try to handle errors in function returning json
time to learn exception handling in plpgsql


constraints: 
  primary key
  foreign key
  check:
    reason other requires a note
    amount must be non zero

-- yikes, reasons are store specific
drop table if exists sls.payroll_adjustments;
create table sls.payroll_adjustments (
  employee_number citext not null,
  store_code citext not null,
  reason citext not null,
  year_month integer not null,
  amount numeric (8,2) not null,
  note citext,
  constraint other_requires_note check ( -- required the or clause, without it failed every time reason was not other
    (reason = 'Other' and note is not null) or reason <> 'Other'),
  primary key (employee_number, reason, year_month),
  foreign key (store_code,reason) references sls.payroll_adjustment_reasons(store_code,reason));    
COMMENT ON TABLE sls.payroll_adjustments
  IS 'adjustments made to a sales consultants pay by GSM. adjustments are made during the month end process of preparing sales consultant payroll for the payroll clerk';

-- base row
insert into sls.payroll_adjustments (employee_number,store_code,reason,year_month,amount,note) 
values ('12345','RY1','Other',201804, 10, 'the note');

-- test FK: SQL state: 23503: 	foreign_key_violation
insert into sls.payroll_adjustments (employee_number,store_code,reason,year_month,amount,note) 
values ('RY1','148825','abcv',201804, 10, 'the note');

-- -- test check amount <> 0: SQL state: 23514: violates check constraint "payroll_adjustments_amount_check
-- insert into sls.payroll_adjustments (employee_number,reason,year_month,amount,note) 
-- values ('448825','Courtesy delivery',201804, 0, NULL);

-- test PK: state: 23505: violates unique constraint "payroll_adjustments_pkey"
insert into sls.payroll_adjustments (employee_number,reason,year_month,amount,note) 
values ('12345','Other',201804, 10, 'the note');

-- test other requires note
-- test PK: state: 23514: violates check constraint "other_requires_note"
insert into sls.payroll_adjustments (employee_number,reason,year_month,amount,note) 
values ('83455','Other',201804, 99, null);





-- DROP FUNCTION sls.insert_payroll_adjustment(citext,citext, citext, integer, numeric, citext);
CREATE OR REPLACE FUNCTION sls.insert_payroll_adjustment(_store_code citext ,_employee_number citext, _reason citext,
    _year_month integer, _amount numeric, _note citext DEFAULT NULL::citext)
  RETURNS SETOF json AS
$BODY$
/*
select * from sls.payroll_adjustments
select * from sls.insert_payroll_adjustment ('RY1','148825','abcv',201804, 10, 'the note')
*/
begin  
  -- overwrite the adjustment for emp/month/reason
  if exists (
    select 1
    from sls.payroll_adjustments
    where employee_number = _employee_number
      and reason = _reason
      and year_month = _year_month) then
    delete 
    from sls.payroll_adjustments
    where employee_number = _employee_number
      and reason = _reason
      and year_month = _year_month;
  end if;
  
  insert into sls.payroll_adjustments(store_code,employee_number,reason,year_month,amount,note)
  values(_store_code,_employee_number,_reason,_year_month,_amount,_note);     

  return query (
      select json_build_object('result', 'Pass'));
      
  exception
    when SQLSTATE '23503' then
      return query (
        select json_build_object('result', 'The reason you selected is not valid'));  
    when SQLSTATE '23514' then
      return query (
        select json_build_object('result', 'Selecting ''Other'' as the reason requires you to enter a note'));
    when SQLSTATE '23505' then
      return query (
        select json_build_object('result', 'This should not have happened, a primary key failure, see Afton'));   
    when others then     
      return query (
        select json_build_object('result', 'Uh oh, something unexpected happened, please yell at Afton')); 

end;

$BODY$
  LANGUAGE plpgsql;



  -- DROP FUNCTION sls.insert_payroll_adjustment(citext, citext, integer, numeric, citext);
CREATE OR REPLACE FUNCTION sls.insert_payroll_adjustment(_employee_number citext, _reason citext,
    _year_month integer, _amount numeric, _note citext DEFAULT NULL::citext)
  RETURNS setof json as
$BODY$
/*
select * from sls.payroll_adjustments
select * from sls.insert_payroll_adjustment ('12345','abc',201804, 10, 'the note')
*/
DECLARE
    v_state   TEXT;
    v_msg     TEXT;
    v_detail  TEXT;
    v_hint    TEXT;
    v_context TEXT;


  begin
    insert into sls.payroll_adjustments(employee_number,reason,year_month,amount,note)
    values(_employee_number,_reason,_year_month,_amount,_note);     

    EXCEPTION WHEN others THEN
        GET STACKED DIAGNOSTICS
            v_state   = RETURNED_SQLSTATE,
            v_msg     = MESSAGE_TEXT,
            v_detail  = PG_EXCEPTION_DETAIL,
            v_hint    = PG_EXCEPTION_HINT,
            v_context = PG_EXCEPTION_CONTEXT;
--         raise notice E'Got exception:
--             state  : %
--             message: %
--             detail : %
--             hint   : %
--             context: %', v_state, v_msg, v_detail, v_hint, v_context;
        return query (
          select json_build_object('state',v_state, 'message', v_msg, 'detail',v_detail, 'context',v_context));
    end;

$BODY$
  LANGUAGE plpgsql;


