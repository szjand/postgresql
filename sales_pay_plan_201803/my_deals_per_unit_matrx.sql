﻿/*

select * from sls.personnel where last_name = 'croaker'

select * from sls.months

select *
from sls.per_unit_matrix

drop table if exists units_sold_levels;
create temp table units_sold_levels as
select '0 - 14.5'::text as lev_1, '15 - 19.5'::text as lev_2,
  '20 - 24.5'::text as lev_3, '25+'::text as lev_4 ;

select a.payplan, b.lev_1 as units_sold, a.level_1 as per_unit
from sls.per_unit_matrix a
cross join (
  select '0 - 14.5'::text as lev_1, '15 - 19.5'::text as lev_2,
    '20 - 24.5'::text as lev_3, '25+'::text as lev_4 ) b
where a.payplan in ('standard','executive')    
union
select a.payplan, b.lev_2 as units_sold, a.level_2 as per_unit
from sls.per_unit_matrix a
cross join (
  select '0 - 14.5'::text as lev_1, '15 - 19.5'::text as lev_2,
    '20 - 24.5'::text as lev_3, '25+'::text as lev_4 ) b
where a.payplan in ('standard','executive')     
union
select a.payplan, b.lev_3 as units_sold, a.level_3 as per_unit
from sls.per_unit_matrix a
cross join (
  select '0 - 14.5'::text as lev_1, '15 - 19.5'::text as lev_2,
    '20 - 24.5'::text as lev_3, '25+'::text as lev_4 ) b
where a.payplan in ('standard','executive')     
union
select a.payplan, b.lev_4 as units_sold, a.level_4 as per_unit
from sls.per_unit_matrix a
cross join (
  select '0 - 14.5'::text as lev_1, '15 - 19.5'::text as lev_2,
    '20 - 24.5'::text as lev_3, '25+'::text as lev_4 ) b
where a.payplan in ('standard','executive') 
union
select a.payplan, 'Any' as units_sold, a.level_1 as per_unit
from sls.per_unit_matrix a
cross join (
  select '0 - 14.5'::text as lev_1, '15 - 19.5'::text as lev_2,
    '20 - 24.5'::text as lev_3, '25+'::text as lev_4 ) b
where a.payplan in ('outlet','croaker','stadstad')
order by payplan, units_sold


    



    
-- one row for each deal
            select stock_number, model_year::citext || ' ' || make || ' ' || model as vehicle, buyer, 
              case 
                when ssc_employee_number = 'none' then unit_count
                when ssc_employee_number <> 'none' then .5 * unit_count
              end as unit_count
            from sls.deals_by_month
            where year_month = 201804
              and psc_employee_number = '128530'
            union all
            select stock_number, model_year::citext || ' ' || make || ' ' || model as vehicle, buyer, .5 * unit_count
            from sls.deals_by_month
            where year_month = 201804
              and ssc_employee_number = '128530'

so i need one row for each item in matrix
per_unit_matrix
units_sold
pvr
*/
-- this worked, put per_uniit in cte
do
$$
declare
  _employee_number citext := '128530';
  _year_month integer := 201804;
begin  

drop table if exists per_unit;
create temp table per_unit as
          select a.payplan, b.lev_1 as units_sold, a.level_1 as per_unit
          from sls.per_unit_matrix a
          cross join (
            select '0 - 14.5'::text as lev_1, '15 - 19.5'::text as lev_2,
              '20 - 24.5'::text as lev_3, '25+'::text as lev_4 ) b
          where a.payplan in ('standard','executive')    
          union
          select a.payplan, b.lev_2 as units_sold, a.level_2 as per_unit
          from sls.per_unit_matrix a
          cross join (
            select '0 - 14.5'::text as lev_1, '15 - 19.5'::text as lev_2,
              '20 - 24.5'::text as lev_3, '25+'::text as lev_4 ) b
          where a.payplan in ('standard','executive')     
          union
          select a.payplan, b.lev_3 as units_sold, a.level_3 as per_unit
          from sls.per_unit_matrix a
          cross join (
            select '0 - 14.5'::text as lev_1, '15 - 19.5'::text as lev_2,
              '20 - 24.5'::text as lev_3, '25+'::text as lev_4 ) b
          where a.payplan in ('standard','executive')     
          union
          select a.payplan, b.lev_4 as units_sold, a.level_4 as per_unit
          from sls.per_unit_matrix a
          cross join (
            select '0 - 14.5'::text as lev_1, '15 - 19.5'::text as lev_2,
              '20 - 24.5'::text as lev_3, '25+'::text as lev_4 ) b
          where a.payplan in ('standard','executive') 
          union
          select a.payplan, 'Any' as units_sold, a.level_1 as per_unit
          from sls.per_unit_matrix a
          cross join (
            select '0 - 14.5'::text as lev_1, '15 - 19.5'::text as lev_2,
              '20 - 24.5'::text as lev_3, '25+'::text as lev_4 ) b
          where a.payplan in ('outlet','croaker','stadstad')
          order by payplan, units_sold ;  
          
drop table if exists wtf;
create temp table wtf as

  select b.*
  from sls.personnel_payplans a
  left join per_unit b on a.payplan = b.payplan
  where a.employee_number = _employee_number
    and a.from_date <= (select last_of_month from sls.months where year_month = _year_month)
    and a.thru_date >= (select first_of_month from sls.months where year_month = _year_month); 



end;
$$;

select * from wtf;
