﻿drop table if exists sls.payroll_adjustment_reasons;
create table sls.payroll_adjustment_reasons (
  store_code citext not null,
  reason citext not null,
  primary key (store_code, reason));

insert into sls.payroll_adjustment_reasons (store_code,reason) values  
('RY1','Courtesy Delivery'),
('RY1','Spiff'),
('RY1','Other'),
('RY2','Courtesy Delivery'),
('RY2','Spiff'),
('RY2','Other'),
('RY2','Nissan Stair Step');

-- 4/24/18 reasons need to be store specific
-- 4/29/18 sale of fi product outside of deal, eg service contract
--            only applies to executive pay plan, ry1 only at this time
insert into sls.payroll_adjustment_reasons (store_code,reason) values  
('RY1','Sale of F&I Product Only');

-- 4/30/2018
insert into sls.payroll_adjustment_reasons (store_code,reason) values  
('RY1','PTO Payout'),
('RYs','PTO Payout');

************************************************************************************************
most of what follows is cah cah
see payroll_adjustment_constraints
*************************************************************************************************
drop table if exists sls.payroll_adjustments;
create table sls.payroll_adjustments (
  employee_number citext not null,
  reason citext not null references sls.payroll_adjustment_reasons(reason),
  year_month integer not null,
  amount numeric (8,2) not null check (amount <> 0),
  note citext,
  constraint other_requires_note check (
    reason = 'Other' and note is not null and length(trim(note)) > 3),
  primary key (employee_number, reason, year_month));



drop function sls.insert_payroll_adjustment (citext,citext,integer,numeric,citext);
create or replace function sls.insert_payroll_adjustment (_employee_number citext, 
  _reason citext, _year_month integer, _amount numeric(8,2), _note citext default null)
  returns setof json as
$BODY$

/*

select * from sls.insert_payroll_adjustment ('1234','Other',201804, 10, 'the_note')

*/

begin  

-- return query
select
  case
    when _reason = 'Other' and (_note is null or length(_note) < 3) then
      return query (select json_build_object('result', 'When the reason for the adjustment is Other, you must include a note') as msg)
      
    when _amount = 0 then 
      return query (select json_build_object('result', 'The amount must be a non zero value') as msg)
      
    else
      insert into sls.payroll_adjustments(employee_number,reason,year_month,amount,note)
      values(_employee_number,_reason,_year_month,_amount,_note);
      return query (select json_build_object('result', 'Pass') as msg)
  end; 
end

$BODY$

language plpgsql;

truncate sls.payroll_adjustments

      insert into sls.payroll_adjustments(employee_number,reason,year_month,amount,note)
      values('12345','courtesy delivery',201804,0,'this is a note')




drop function sls.insert_payroll_adjustment (citext,citext,integer,numeric,citext);
create or replace function sls.insert_payroll_adjustment (_employee_number citext, 
  _reason citext, _year_month integer, _amount numeric(8,2), _note citext default null)
  returns setof json as
$BODY$

/*
select * from sls.payroll_adjustments
select * from sls.insert_payroll_adjustment ('1234','Other',201804, 0, 'the note')
*/
begin  

  if exists (
    select 1
    from sls.payroll_adjustments
    where employee_number = _employee_number
      and reason = _reason
      and year_month = _year_month) then
    delete 
    from sls.payroll_adjustments
    where employee_number = _employee_number
      and reason = _reason
      and year_month = _year_month;
  end if;
 
--   if  (_reason = 'Other' and _note is not null)
--     or ( _amount <> 0 
--     and exists (
--       select 1
--       from sls.payroll_adjustment_reasons
--       where reason = _reason)) then 
--     insert into sls.payroll_adjustments(employee_number,reason,year_month,amount,note)
--     values(_employee_number,_reason,_year_month,_amount,_note);      

  if exists (
      select 1
      from sls.payroll_adjustment_reasons
      where reason = _reason) then 
    insert into sls.payroll_adjustments(employee_number,reason,year_month,amount,note)
    values(_employee_number,_reason,_year_month,_amount,_note);     
  end if;
   
  if _reason = 'Other' and _note is null then
    return query (
      select json_build_object('result', 'When the reason for the adjustment is Other, you must include a note') as msg);
  else if _amount = 0 then
    return query (
      select json_build_object('result', 'The amount must be a non zero value') as msg);
  else if not exists (
    select 1
    from sls.payroll_adjustment_reasons
    where reason = _reason) then
    return query (
      select json_build_object('result', 'The reason submitted is not valid') as msg);      
  else
    return query (
      select json_build_object('result', 'Pass') as msg);
  end if;
  end if;
  end if;
end;

$BODY$

language plpgsql; 






insert into sls.payroll_Adjustments values ('1234','wtf',201804, 10, 'note')







     