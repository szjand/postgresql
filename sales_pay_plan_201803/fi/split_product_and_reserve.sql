﻿
/*
GM Reserve:  180600, 180601, 180800, 180801
GM Reserve Chargebacks: 185001, 185101
GM Product:  144300 (164300), 144400 (164400), 180700 (185700), 145400 (165400), 145500 (165500), 180900 (185900)
GM Product Chargebacks:  185000, 185100

HN Reserve: 280600, 280601, 280800, 280801, 280802
HN Product: 244300 (264300), 244301 (264301), 244400 (264400), 244401 (264401), 244402 (264402), 280700 (285700), 280701 (285701)
HN Chargebacks: 285010, 285011, 285100, 285101

Chargebacks are not separated into product and reserve at Honda—if we are going to use them, I would recommend deducting them at the lower percentage—I can follow up on this.
I did not include incentive chargebacks.  I need to find out what the intention is here.

Can you tell me how this lines up with the F&I accounts currently being used?
*/
-- all accounts currently used for fi
select * from sls.deals_accounts_routes where page = 17 order by gl_account

drop table if exists sls.jeri_fi_accounts;
create unlogged table sls.jeri_fi_accounts (
  account citext primary key);

insert into sls.jeri_fi_accounts values('144300');
insert into sls.jeri_fi_accounts values('144400');
insert into sls.jeri_fi_accounts values('145400');
insert into sls.jeri_fi_accounts values('145500');
insert into sls.jeri_fi_accounts values('164300');
insert into sls.jeri_fi_accounts values('164400');
insert into sls.jeri_fi_accounts values('165400');
insert into sls.jeri_fi_accounts values('165500');
insert into sls.jeri_fi_accounts values('180600');
insert into sls.jeri_fi_accounts values('180601');
insert into sls.jeri_fi_accounts values('180700');
insert into sls.jeri_fi_accounts values('180800');
insert into sls.jeri_fi_accounts values('180801');
insert into sls.jeri_fi_accounts values('180900');
insert into sls.jeri_fi_accounts values('185000');
insert into sls.jeri_fi_accounts values('185001');
insert into sls.jeri_fi_accounts values('185100');
insert into sls.jeri_fi_accounts values('185101');
insert into sls.jeri_fi_accounts values('185700');
insert into sls.jeri_fi_accounts values('185900');
insert into sls.jeri_fi_accounts values('244300');
insert into sls.jeri_fi_accounts values('244301');
insert into sls.jeri_fi_accounts values('244400');
insert into sls.jeri_fi_accounts values('244401');
insert into sls.jeri_fi_accounts values('244402');
insert into sls.jeri_fi_accounts values('264300');
insert into sls.jeri_fi_accounts values('264301');
insert into sls.jeri_fi_accounts values('264400');
insert into sls.jeri_fi_accounts values('264401');
insert into sls.jeri_fi_accounts values('264402');
insert into sls.jeri_fi_accounts values('280600');
insert into sls.jeri_fi_accounts values('280601');
insert into sls.jeri_fi_accounts values('280700');
insert into sls.jeri_fi_accounts values('280701');
insert into sls.jeri_fi_accounts values('280800');
insert into sls.jeri_fi_accounts values('280801');
insert into sls.jeri_fi_accounts values('280802');
insert into sls.jeri_fi_accounts values('285010');
insert into sls.jeri_fi_accounts values('285011');
insert into sls.jeri_fi_accounts values('285100');
insert into sls.jeri_fi_accounts values('285101');
insert into sls.jeri_fi_accounts values('285700');
insert into sls.jeri_fi_accounts values('285701');




	
select gl_account, sum(amount)
from sls.deals_accounting_detail a
left join fin.dim_account b on a.gl_account = b.account
  and b.current_row
where gross_category = 'fi'
group by gl_account
order by gl_account


select *
from sls.deals_accounting_detail
where year_month = 202302
  and gross_category = 'fi'

select gl_account, sum(amount)
from sls.deals_accounting_detail
where year_month in (202302, 202301)
  and gross_category = 'fi'  
group by gl_account

-- comparing my list to jeri's, exposes accounts with transactions in 2023
-- that are not categorized by jeri as product or reserve
select a.*, b.*, c.active, d.amount
from sls.deals_accounts_routes a
left join sls.jeri_fi_accounts b on a.gl_Account = b.account
left join arkona.ext_glpmast c on a.gl_account = c.account_number
  and year = 2023
left join (
select gl_account, sum(amount) as amount
from sls.deals_accounting_detail
where year_month in (202302, 202301)
  and gross_category = 'fi'  
group by gl_account) d on a.gl_account = d.gl_account  
where page = 17 order by gl_account

-- updated by Jeri on 2/16
GM Reserve:  180600, 180601, 180800, 180801, 180802, 180803
GM Reserve Chargebacks: 185001, 185101
GM Product:  144300 (164300), 144400 (164400), 180700 (185700), 145400 (165400), 145500 (165500), 
	180900 (185900), 145502 (165502), 180902 (185902)
GM Product Chargebacks:  185000, 185100

HN Reserve: 280600, 280601, 280800, 280801, 280802
HN Product: 244300 (264300), 244301 (264301), 244400 (264400), 244401 (264401), 244402 (264402), 
	280700 (285700), 280701 (285701), 245400 (265400), 245401 (265401), 245500 (265500), 
	245501 (265501), 245502 (265502), 285900 (285901)
HN Chargebacks: 285010, 285011, 285100, 285101

from jeri 2/24, Toyota

Im a little worried about the longevity of just using accounts for this as we will soon be changing the Toyota accounts and I occasionally add/change accounts etc.  But as of today, this is how I would separate the accounts:


Reserve:  6180 6280 6282 6380 6382   

Product: 4172 (6172) 4254 (6254) 4270 (6270) 4272 (6272) 4275 (6275) 4292 (6292) 4370 (6370) 4372 (6372) 4374 (6374) 4392 (6392) 4675 (6675) 4745 (6745) 4747 (6747) 4792 (6792) 4870 (6870) 4872 (6872) 4874 (6874) 4875 (6875) 4878 (6878) 4970 (6970) (6290) (6390) 6790)

Reserve Chargebacks: 6181, 6281, 6381, 6781
Product Chargebacks:  6171, 6171C, 6266, 6271, 6271C, 6273, 6276, 6291, 6293, 6371, 6371C, 6373, 6375, 6391, 6393, 6473, 6676, 6746, 6749, 6791, 6871, 6873, 6876, 6877, 6879, 6993

The following accounts are on your list, but I would exclude them:  Aftermarket accounts 4950 (6950) 4350 (6350) 4250 (6250) & vehicle adjustment accounts (6341) (6342) (6343)

ohhh—ok, so if chargebacks dont need to be separated, then I would say financial routing is better.  

Reserve:  GM Page 7, Line 1 & Line 11
Product:  GM Page 7, Line 2, 5, 6, 7, 12, 15, 16, 17
Chargebacks:  GM Page 7, Line 3 & 13

Does that come up with the same list of accounts?

2/25/23
Going with the routing 
here is the query that shows it

select a.fxmact, a.fxmpge, a.fxmlne, 
  case
    when b.consolidation_grp = '2' then 'RY2'
    else company_number
  end as store, 
  b.g_l_acct_number,
  case
    when a.fxmlne in (1, 3) then 'Chargeback'
    when a.fxmlne in (1, 11) then 'Reserve'
    else 'Product'
  end as Type    
from arkona.ext_eisglobal_sypffxmst a
join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
  and b.factory_financial_year = 2023
where a.fxmcyy = 2023
  and a.fxmpge = 17
  and a.fxmlne in (1,2,3,5,6,7,11,12,13,15,16,17)
order by store, a.fxmlne, a.fxmact, b.g_l_acct_number 