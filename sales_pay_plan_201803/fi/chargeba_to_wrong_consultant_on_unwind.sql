﻿08/08/23
Will you please also look at G47403B?  I’m hearing that Bryce Huot was the original salesperson 
and this vehicle backed on.  Bryce was likely paid on $700 of F&I product ($140 total) in July 
and should have had this reversed in August.  Alex is saying that this is going against his August pay. 
Thank you!
Jeri Schmiess

huot: 145983  payplan: sales consultant
waldeck: 165989  payplan: outlet 
select * from sls.deals_By_month where stock_number = 'G47403B'


-- this looks correct   
select * from sls.deals_by_month where stock_number = 'G47403B'

select * from sls.deals_gross_by_month where control = 'G47403B'

select * from sls.deals_accounting_detail where control = 'G47403B'  shows the sale of the fi in july and the reversal (mortenson) in august

was hoping for a bopmast_id to segregate the deals, in deals_accounting_detail, but dont have it
because accounting does not know anything about the operational data
BUT
have the customers name under trans_description
the  july deal and aug unwind for huot was customer mortenson
the aug deal for waldeck was mecham
waldecks vision commission page shows G47403B with mecham but the -700 gross 
so, what i need to do is to track how the fi reversal gets assigned to waldeck instead of just huot

FUNCTION sls.update_consultant_payroll():
        case 
          when aa.payplan in ('outlet', 'executive') then round(.16 * (coalesce(dd.amount, 0) + coalesce(cc.fi_gross, 0)), 2)
          when aa.payplan in ('croaker','sales consultant') then round(.1 * (coalesce(dd.amount, 0) + coalesce(cc.fi_gross, 0)), 2)
          when aa.payplan = 'single point' then 0.1 * coalesce(cc.fi_reserve_gross, 0) + .2 * coalesce(cc.fi_product_gross, 0)
          else 0
        end as fi_pay,
        
      left join ( -- cc: fi gross
        select fi_employee_number, sum(fi_gross) as fi_gross, sum(fi_product_gross) as fi_product_gross, 
					sum(fi_reserve_gross) as fi_reserve_gross
        from (    

--- aaa in FUNCTION sls.update_consultant_payroll() is the source of the -700 being attributed to waldeck  
-- this, perhaps then, is where i can include the buyer in the join      
          select a.year_month, a.fi_employee_number, a.fi_last_name, 
            case when c.payplan in ('executive','croaker','outlet','sales consultant') then b.fi_sales - b.fi_cogs end as fi_gross,
            case when c.payplan = 'single point' then b.fi_product_sales - b.fi_product_cogs end as fi_product_gross,
            case when c.payplan = 'single point' then b.fi_reserve_sales - b.fi_reserve_cogs end as fi_reserve_gross
          from sls.deals_by_month a
          left join sls.deals_gross_by_month b on a.stock_number = b.control
            and a.year_month = b.year_month
          left join sls.personnel_payplans c on a.fi_employee_number = c.employee_number
            and c.thru_date > current_date  
          where a.year_month = 202308 -- (select year_month from open_month)
            and a.bopmast_id <> 76564
            and a.fi_employee_number in ('145983','165989')) aaa -- exclude the vanyo g47000 unwind
        group by aaa.fi_employee_number, aaa.year_month) cc   

--- aaa is the source of the -700 being attributed to waldeck  
-- this, perhaps then, is where i can include the buyer in the join 
--- aaa
select a.year_month, a.fi_employee_number, a.fi_last_name, 
	case when c.payplan in ('executive','croaker','outlet','sales consultant') then b.fi_sales - b.fi_cogs end as fi_gross,
	case when c.payplan = 'single point' then b.fi_product_sales - b.fi_product_cogs end as fi_product_gross,
	case when c.payplan = 'single point' then b.fi_reserve_sales - b.fi_reserve_cogs end as fi_reserve_gross
from sls.deals_by_month a
left join sls.deals_gross_by_month b on a.stock_number = b.control
	and a.year_month = b.year_month
left join sls.personnel_payplans c on a.fi_employee_number = c.employee_number
	and c.thru_date > current_date  
where a.year_month = 202308 -- (select year_month from open_month)
	and a.bopmast_id <> 76564
	and a.fi_employee_number in ('145983','165989')        

sls.deals_accounting_detail has customer in trans_description	    
seems like i need to add that to sls.deals_Gross_by_month -- nah, don't think that is it
-- FUNCTION sls.update_deals_gross_by_month()
    select year_month, control, trans_description,
      sum(case when gross_category = 'front' and account_type in ('sale', 'Other Income')
        then -amount else 0 end) as front_sales,
      sum(case when gross_category = 'front' and account_type = 'cogs'
        then amount else 0 end) as front_cogs,
      sum(case when gross_category = 'front' then -amount else 0 end) as front_gross,
      sum(case when gross_category in ('fi_product', 'fi_reserve') and account_type in ('sale', 'Other Income')
        then -amount else 0 end) as fi_sales,
      sum(case when gross_category in ('fi_product', 'fi_reserve') and account_type = 'cogs' then amount else 0 end) as fi_cogs,
      0 as fi_gross,
      coalesce(sum(amount) filter (where gl_account in ('180601','180801','180803')), 0) as acq_fee,
      sum(case when gross_category = 'fi_product' and account_type in ('sale', 'Other Income') then -amount else 0 end) as fi_product_sales,
			sum(case when gross_category = 'fi_product' and account_type = 'cogs' then amount else 0 end) as fi_product_cogs,     
      sum(case when gross_category = 'fi_reserve' and account_type in ('sale', 'Other Income') then -amount else 0 end) as fi_reserve_sales,   
      sum(case when gross_category = 'fi_reserve' and account_type = 'cogs' then amount else 0 end) as fi_reserve_cogs
    from sls.deals_accounting_detail
    where year_month = 202308 -- (select year_month from sls.months where open_closed = 'open')
      and control = 'G47403B'
    group by year_month, control, trans_description;

select * from sls.deals_by_month where stock_number = 'G47403B'

sls.deals_accounting_detail: acct info only no bopmast_id
			|			select * from sls.deals_accounting_detail where control = 'G47403B'
			|
			V
sls.deals_gross_by_month
			|     select * from sls.deals_gross_by_month where control = 'G47403B'
			|     
			V
sls.consultant_payroll


-- 08/24/23
--- aaa in FUNCTION sls.update_consultant_payroll() 
-- limited to waldeck and huot & stock_number G47403B
-- add stock_number & bopmast_id
-- select * from sls.deals_by_month where stock_number = 'G47403B'
select a.year_month, a.fi_employee_number, a.fi_last_name, 
	case when c.payplan in ('executive','croaker','outlet','sales consultant') then b.fi_sales - b.fi_cogs end as fi_gross,
	case when c.payplan = 'single point' then b.fi_product_sales - b.fi_product_cogs end as fi_product_gross,
	case when c.payplan = 'single point' then b.fi_reserve_sales - b.fi_reserve_cogs end as fi_reserve_gross,
	a.stock_number, a.bopmast_id  -----------------------------
from sls.deals_by_month a
left join sls.deals_gross_by_month b on a.stock_number = b.control
	and a.year_month = b.year_month
left join sls.personnel_payplans c on a.fi_employee_number = c.employee_number
	and c.thru_date > current_date  
where a.year_month = 202308 -- (select year_month from open_month)
-- 	and a.bopmast_id <> 76564
	and a.fi_employee_number in ('145983','165989') --------------------------------------
	-- but i think this only handles the case where the second sale has no fi gross
	-- added and a.unit_count = -1 to the case statement but it did not work 
	and a.stock_number = 'G47403B' ------------------------------------
	and   -- this  does exactly what i need, assigns the fi gross reversal to huot only
	  case
	    when a.stock_number = 'G47403B' and a.year_month = 202308 then a.bopmast_id = 78568
	    else true
	  end

looks like it is also necessary to modify FUNCTION sls.json_get_deals_by_consultant_month(citext, integer)

-- 							from (    
								select a.fi_employee_number, a.fi_last_name, a.stock_number, concat(model_year, ' ', make, ' ', model) as vehicle, 
									buyer as customer,
									case when c.payplan in ('executive','croaker','outlet','sales consultant') then b.fi_sales - b.fi_cogs end as fi_gross,
									case when c.payplan = 'single point' then b.fi_product_sales - b.fi_product_cogs end as fi_product_gross,
									case when c.payplan = 'single point' then b.fi_reserve_sales - b.fi_reserve_cogs end as fi_reserve_gross
								from sls.deals_by_month a
								left join sls.deals_gross_by_month b on a.stock_number = b.control
									and a.year_month = b.year_month
								left join sls.personnel_payplans c on a.fi_employee_number = c.employee_number
									and c.thru_date > current_date  
								where a.year_month = 202308 
            and
              case
                when a.stock_number = 'G47403B' and a.year_month = 202308 then a.bopmast_id = 78568
                else true
              end  order by stock_number -- ) aaa 
								--_year_month) aaa














-- 8/18 distraction, look for any other unwinds in august
select * from sls.deals_by_month where year_month = 202308 and unit_count = -1

select * from sls.deals_by_month where stock_number = 'G47730X'


select a.year_month, store_code, bopmast_id, unit_count, a.stock_number, buyer, fi_last_name 
from sls.deals_by_month a
join (
  select stock_number
  from sls.deals_by_month
  where year_month = 202306
    and unit_count = -1) b on a.stock_number = b.stock_number
order by stock_number, year_month    
    

select * from sls.deals where stock_number = 'H16499A';

select * 
from sls.deals_accounting_detail 
where control = 'G47403B'
  and department = 'finance'















---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
08/04/23
from Jeri
It looks like back in June, Jay Turner was charged for the backon on G46821.  This deal was originally sold in May by someone else 
(I can’t see who), and Jay sold this unit in June.  It looks like the chargeback possibly went against Jay and not the original sales person. 
Would you have some time to take a look at this so I can get an answer back to Jay?  Has this possibly happened on other units 
when the second salesperson is different?

select * 
from sls.fi_chargebacks
where stock_number = 'G46821'

select * 
from sls.deals_by_month
where stock_number = 'G46821'

sold in May by Ezekiel Bocklage (145950), unwound in May, sold in May by Jay (Richard) Turner (195231)

the $120 goes to account 185002

select * from sls.deals_accounts_routes where gl_account = '185002'

-- FUNCTION sls.update_fi_chargebacks();
  select year_month, control, sum(amount) as amount, store_code, fi_employee_number,
    delivery_date, source
  from (
    select a.year_month, a.control, -a.amount as amount, 
      b.store_code, coalesce(d.employee_number, 'N/A') as fi_employee_number, 
      b.delivery_date, 'pipeline' as source
    from sls.deals_accounting_detail a
    inner join sls.deals b on a.control = b.stock_number
    left join ads.ext_dim_customer c on b.buyer_bopname_id = c.bnkey
    left join sls.personnel d on b.fi_manager = d.fi_id
    where a.year_month = (select year_month from sls.months where open_closed = 'open')
      and a.gross_category = 'fi_chargeback'
      and not exists (
        select 1
        from sls.deals
        where stock_number = b.stock_number
          and seq > 1)
      and delivery_date >= ( -- 6 month rule     
        select (first_of_month - interval '6 month')::date as the_date
        from sls.months where year_month = (
          select year_month 
          from sls.months 
          where open_closed = 'open')) 
    union
    select distinct year_month, control, amount, store_code, 
      coalesce(employee_number, 'N/A'), delivery_date, 'pipeline'
    from (
      --eliminate same month back on
      -- inner join on the_bopmast_id cte eliminates those deals for which the sum
      -- of the unit count in the same month is 0
      with
        the_bopmast_id as (
          select bopmast_id
          from sls.deals_accounting_detail a
          inner join sls.deals b on a.control = b.stock_number
          left join ads.ext_dim_customer c on b.buyer_bopname_id = c.bnkey
          left join sls.personnel d on b.fi_manager = d.fi_id
          where a.year_month = (select year_month from sls.months where open_closed = 'open')
            and a.gross_category = 'fi_chargeback'
            and exists (
              select 1
              from sls.deals
              where stock_number = b.stock_number
                and seq > 1)
          group by b.bopmast_id, b.year_month
          having sum(unit_count) <> 0)  
      select a.year_month, a.control, -a.amount as amount, 
        b.store_code, b.fi_manager, d.employee_number, 'pipeline',
        b.delivery_date, b.bopmast_id,  unit_count
      from sls.deals_accounting_detail a
      inner join sls.deals b on a.control = b.stock_number
      inner join the_bopmast_id bb on b.bopmast_id = bb.bopmast_id
      left join ads.ext_dim_customer c on b.buyer_bopname_id = c.bnkey
      left join sls.personnel d on b.fi_manager = d.fi_id
      where a.year_month = (select year_month from sls.months where open_closed = 'open')
        and a.gross_category = 'fi_chargeback'
-- *a*
        and 
          case 
            when a.control = 'G33482' then b.delivery_date > '10/01/2018' 
            when a.control = 'G36991' then b.delivery_date > '05/01/2019'
            when a.control = 'G34900RC' then b.delivery_date > '06/12/2019'
            when a.control = 'G37491' then b.delivery_date > '08/30/2019'
            when a.control = 'G37384A' then b.delivery_date > '10/01/2019'
            when a.control = 'H12342' then b.run_date = '10/23/2019'
            when a.control = 'G38725G' then b.run_date = '01/01/2020'
            when a.control = 'G37783A' then b.run_date = '11/05/2019'
            when a.control = 'G39972' then b.run_date = '10/01/2020'
            when a.control = 'G41393X' then b.run_date = '06/09/2021'
            when a.control = 'G43931' then b.run_date > '02/05/2022'
            when a.control = 'T10439' then b.run_date = '02/15/2023'
            when a.control = 'G44727aa' then b.run_date = '08/14/2022'
            when a.control = 'H16453G' then b.run_date = '03/31/2023'
            when a.control = 'H16548G' then b.run_date = '05/03/2023'
            else 1 = 1 
          end
-- *b*          
        and a.control <> 'G45738A'
        and exists (
          select 1
          from sls.deals
          where stock_number = b.stock_number
            and seq > 1)
        and delivery_date >= ( -- 6 month rule     
          select (first_of_month - interval '6 month')::date as the_date
          from sls.months where year_month = (
            select year_month 
            from sls.months 
            where open_closed = 'open'))) gl) aa  
  group by year_month, control, store_code, fi_employee_number, delivery_date, source;





    select a.year_month, a.control, -a.amount as amount, 
      b.store_code, coalesce(d.employee_number, 'N/A') as fi_employee_number, 
      b.delivery_date, 'pipeline' as source
    from sls.deals_accounting_detail a
    inner join sls.deals b on a.control = b.stock_number
    left join ads.ext_dim_customer c on b.buyer_bopname_id = c.bnkey
    left join sls.personnel d on b.fi_manager = d.fi_id
    where a.year_month = 202306 -- (select year_month from sls.months where open_closed = 'open')
      and a.gross_category = 'fi_chargeback'
      and not exists (
        select 1
        from sls.deals
        where stock_number = b.stock_number
          and seq > 1)
      and a.control = 'G46821'
--       and delivery_date >= ( -- 6 month rule     
--         select (first_of_month - interval '6 month')::date as the_date
--         from sls.months where year_month = (
--           select year_month 
--           from sls.months 
--           where open_closed = 'open')) 
    union
    select distinct year_month, control, amount, store_code, 
      coalesce(employee_number, 'N/A'), delivery_date, 'pipeline'
    from (
      --eliminate same month back on
      -- inner join on the_bopmast_id cte eliminates those deals for which the sum
      -- of the unit count in the same month is 0
      with
        the_bopmast_id as (
          select bopmast_id
          from sls.deals_accounting_detail a
          inner join sls.deals b on a.control = b.stock_number
          left join ads.ext_dim_customer c on b.buyer_bopname_id = c.bnkey
          left join sls.personnel d on b.fi_manager = d.fi_id
          where a.year_month = 202306 -- (select year_month from sls.months where open_closed = 'open')
            and a.gross_category = 'fi_chargeback'
            and exists (
              select 1
              from sls.deals
              where stock_number = b.stock_number
                and seq > 1)
          group by b.bopmast_id, b.year_month
          having sum(unit_count) <> 0)  
      select a.year_month, a.control, -a.amount as amount, 
        b.store_code, b.fi_manager, d.employee_number, 'pipeline',
        b.delivery_date, b.bopmast_id,  unit_count
      from sls.deals_accounting_detail a
      inner join sls.deals b on a.control = b.stock_number
      inner join the_bopmast_id bb on b.bopmast_id = bb.bopmast_id
      left join ads.ext_dim_customer c on b.buyer_bopname_id = c.bnkey
      left join sls.personnel d on b.fi_manager = d.fi_id
      where a.year_month = 202306 -- (select year_month from sls.months where open_closed = 'open')
        and a.gross_category = 'fi_chargeback'
-- *a*
        and 
          case 
            when a.control = 'G33482' then b.delivery_date > '10/01/2018' 
            when a.control = 'G36991' then b.delivery_date > '05/01/2019'
            when a.control = 'G34900RC' then b.delivery_date > '06/12/2019'
            when a.control = 'G37491' then b.delivery_date > '08/30/2019'
            when a.control = 'G37384A' then b.delivery_date > '10/01/2019'
            when a.control = 'H12342' then b.run_date = '10/23/2019'
            when a.control = 'G38725G' then b.run_date = '01/01/2020'
            when a.control = 'G37783A' then b.run_date = '11/05/2019'
            when a.control = 'G39972' then b.run_date = '10/01/2020'
            when a.control = 'G41393X' then b.run_date = '06/09/2021'
            when a.control = 'G43931' then b.run_date > '02/05/2022'
            when a.control = 'T10439' then b.run_date = '02/15/2023'
            when a.control = 'G44727aa' then b.run_date = '08/14/2022'
            when a.control = 'H16453G' then b.run_date = '03/31/2023'
            when a.control = 'H16548G' then b.run_date = '05/03/2023'
            else 1 = 1 
          end
-- *b*          
        and a.control <> 'G45738A'
        and exists (
          select 1
          from sls.deals
          where stock_number = b.stock_number
            and seq > 1)
--         and delivery_date >= ( -- 6 month rule     
--           select (first_of_month - interval '6 month')::date as the_date
--           from sls.months where year_month = (
--             select year_month 
--             from sls.months 
--             where open_closed = 'open'))
            ) gl  



select * from sls.deals_accounting_detail where control = 'G46821'

select * from sls.ext_accounting_base where control = 'G46821'

-- this populates vision ukg imports
select * from sls.get_sales_consultant_payroll(202306,'RY1')
-- driven by:
select * from sls.consultant_payroll where year_month = 202306 and last_name in ( 'bocklage','turner')
-- so look at FUNCTION sls.update_consultant_payroll():
still not making any sense, need to find where the chargeback is figured into fi pay
is account 185002 product or reserve?

-- from FUNCTION sls.update_consultant_payroll() L135
-- dd is chargebacks, applies only to non single point payplans ?!?!?!?!

        case 
          when aa.payplan = 'single point' then coalesce(dd.amount, 0) + coalesce(cc.fi_reserve_gross, 0) + coalesce(cc.fi_product_gross, 0)
--           when aa.last_name in ('loven','croaker','warmack') then coalesce(dd.amount, 0) + coalesce(cc.fi_gross, 0)
          else coalesce(dd.amount, 0) + coalesce(cc.fi_gross, 0)
        end as fi_total,  


-- fi_pay
        case 
          when aa.payplan in ('outlet', 'executive') then round(.16 * (coalesce(dd.amount, 0) + coalesce(cc.fi_gross, 0)), 2)
          when aa.payplan in ('croaker','sales consultant') then round(.1 * (coalesce(dd.amount, 0) + coalesce(cc.fi_gross, 0)), 2)
          when aa.payplan = 'single point' then 0.1 * coalesce(cc.fi_reserve_gross, 0) + .2 * coalesce(cc.fi_product_gross, 0)
          else 0
        end as fi_pay,        


      left join ( -- cc: fi gross
        select fi_employee_number, sum(fi_gross) as fi_gross, sum(fi_product_gross) as fi_product_gross, 
					sum(fi_reserve_gross) as fi_reserve_gross
--         select fi_employee_number, sum(fi_gross) as fi_gross, 
--           case
--             when year_month = 202304 and fi_employee_number = '178543' then sum(fi_product_gross) - 4766.22
--             else sum(fi_product_gross) 
--           end as fi_product_gross,  
--           case
--             when year_month = 202304 and fi_employee_number = '178543' then sum(fi_reserve_gross) - 100       
-- 					  else sum(fi_reserve_gross) 
-- 					end as fi_reserve_gross
        from (    
          select a.year_month, a.fi_employee_number, a.fi_last_name, 
            case when c.payplan in ('executive','croaker','outlet','sales consultant') then b.fi_sales - b.fi_cogs end as fi_gross,
            case when c.payplan = 'single point' then b.fi_product_sales - b.fi_product_cogs end as fi_product_gross,
            case when c.payplan = 'single point' then b.fi_reserve_sales - b.fi_reserve_cogs end as fi_reserve_gross
          from sls.deals_by_month a
          left join sls.deals_gross_by_month b on a.stock_number = b.control
            and a.year_month = b.year_month
          left join sls.personnel_payplans c on a.fi_employee_number = c.employee_number
            and c.thru_date > current_date  
          where a.year_month = 202306 -- (select year_month from open_month)
            and a.bopmast_id <> 76564
            and a.psc_last_name in ('bocklage','turner')) aaa -- exclude the vanyo g47000 unwind
        group by aaa.fi_employee_number, aaa.year_month) cc on aa.employee_number = cc.fi_employee_number        

select * from sls.deals_by_month limit 10

select * from sls.personnel where employee_number = '165983'

-- 08/07/23  it appears that chargeback goes into fi_total, but not fi_pay for single point        


Turner
fi_product_gros  14835.22  * .2 = 2967.04
fi_reserve_gross  3606.98  * .1 =  360.70
                                  3327.74
-- so, these fucking totals don't match the vision ukg import page totals for him or his personal vision page
his vision page: not even close, but it matches the ukg import page for june
	product: 13675 * .2 = 2735
	reserve: 10124 * .1 = 1012.40
	                      3747.40

select * from sls.deals_by_month where year_month = 202306 and psc_last_name = 'turner'         

select * from sls.deals limit 10   

SELECT * FROM SLS.deals_gross_by_month where year_month = 202306 and control = 'g46379'      