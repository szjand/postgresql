﻿in the previous version, it does not make any sense how it ever worked because of the clause:
      where not exists (
        select 1
        from sls.paid_by_month
        where employee_number = d.employee_
          and year_month = d.year_month);
once the mid-month draw check was issued, the second check for the month would be excluded
looks like a changed things to be this way (fucked up) in may of 2017:

  select a.last_name, a.first_name, b.*
  from sls.personnel a 
  left join sls.paid_by_month b on a.employee_number = b.employee_number
  where year_month > 201712
  order by last_name, year_month desc

so, some things
1. move sql from sales_consultant_payroll.py into a function
2. do insert on conflict do update
3. had to add distrib codes: RAO, SALM
4. removed name from queries, decouteau is in pyhshdta with 2 diff names, resulting
in an error: ON CONFLICT DO UPDATE command cannot affect row a second time

payroll_cen_year:
select distinct (the_year - 2000) + 100
from dds.dim_date
where year_month = (
  select year_month
  from sls.months
  where open_closed = 'open')
  

                      
insert into sls.paid_by_month (store_code,employee_number,year_month,total_gross_pay,
  commission,draw,overtime,pto_pay_out,lease,vacation,spiffs)                  
select d.company_number, d.employee_, d.year_month, d.total_gross_pay,
  coalesce(e.commission, 0) as commission, coalesce(e.draw, 0) as draw,
  coalesce(e.overtime, 0) as overtime,
  coalesce(e.pto_pay_out, 0) as pto_pay_out, coalesce(e.lease, 0) as lease,
  coalesce(e.vacation, 0) as vacation,
  coalesce(e.spiffs, 0) as spiffs
from ( -- d
  select company_number, employee_, year_month,
    sum(total_gross_pay) as total_gross_pay
  from (-- b: pyhshdta
    select a.company_number, a.payroll_cen_year, a.payroll_run_number,
      a.employee_name, a.employee_, a.check_year, a.check_month,
      ((2000 + a.check_year) * 100) + a.check_month as year_month, a.total_gross_pay
    from arkona.ext_pyhshdta a
    where trim(a.distrib_code) in ('TEAM','SALE','RAO','SALM')
      and a.payroll_cen_year = (
        select distinct (the_year - 2000) + 100
        from dds.dim_date
        where year_month = (
          select year_month
          from sls.months
          where open_closed = 'open'))      
      and a.seq_void = '00') b
  group by company_number, employee_, year_month) d
left join ( -- e
  select company_number, employee_, year_month,
    sum(case when code_id = '79' then amount else 0 end) as commission,
    sum(case when code_id = '78' then amount else 0 end) as draw,
    sum(case when code_id = 'OVT' then amount else 0 end) as overtime,
    sum(case when code_id = '400' then amount else 0 end) as pto_pay_out,
    sum(case when code_id = '500' then amount else 0 end) as lease,
    sum(case when code_id = '74' then amount else 0 end) as vacation,
    sum(case when code_id = '82' then amount else 0 end) as spiffs
  from (-- c: pyhscdta 
    select a.company_number, a.employee_,
      ((2000 + a.check_year) *100) + a.check_month as year_month,
      sum(b.amount) as amount, b.code_id
    from arkona.ext_pyhshdta a
    inner join arkona.ext_pyhscdta b on a.payroll_run_number = b.payroll_run_number
      and a.company_number = b.company_number
      and a.employee_ = b.employee_number
      and b.code_type in ('0','1') -- 0,1: income, 2: deduction
    where trim(a.distrib_code) in ('TEAM','SALE','RAO','SALM')
      and a.payroll_cen_year = (
        select distinct (the_year - 2000) + 100
        from dds.dim_date
        where year_month = (
          select year_month
          from sls.months
          where open_closed = 'open'))      
    group by a.company_number, a.employee_name, a.employee_,
      ((2000 + a.check_year) *100) + a.check_month, b.code_id) c
  group by company_number, employee_, year_month) e on d.company_number = e.company_number
    and d.employee_ = e.employee_ and d.year_month = e.year_month
on conflict (year_month, employee_number)
do update set (total_gross_pay,commission,draw,overtime,pto_pay_out,lease,vacation,spiffs) 
= (excluded.total_gross_pay, excluded.commission, excluded.draw, excluded.overtime, 
    excluded.pto_pay_out, excluded.lease, excluded.vacation, excluded.spiffs);




  