﻿-- Function: sls.update_deals_accounting_detail()
  select c.year_month, a.control,
    aa.journal_code, b.account_type, b.department,
    d.store_code, d.page, d.line, d.gl_Account, b.description as account_description,
    e.description as trans_description,
    max(
      case
        when d.page = 17 and d.line in (3,13) then 'fi_chargeback'
        when d.page = 17 and d.line in (1,11) then 'fi_reserve'
        when d.page = 17 and d.line in (2,5,6,7,12,15,16,17) then 'fi_product'
        else 'front'
      end) as gross_category,  
    sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
  inner join dds.dim_date c on a.date_key = c.date_key
  inner join fin.dim_account b on a.account_key = b.account_key
    and c.the_date between b.row_from_Date and b.row_thru_date
  inner join sls.deals_accounts_routes d on b.account = d.gl_account
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where post_status = 'Y'
    and c.year_month = 202310 -- (select year_month from sls.months where open_closed = 'open')
    and not (a.trans = 4383163 and a.seq = 13)
    and not (a.trans = 4405875 and a.seq = 33) 
    and not (a.trans = 5691987 and a.seq in (4,5,11)) -- exclude G47000 david vanyo    
    and a.control = 't10827' 
  group by b.description, c.year_month, a.control,
    aa.journal_code, b.account_type, b.department,
    d.store_code, d.page, d.line, d.gl_Account, e.description;    

select * from sls.deals_by_month where year_month = 202310 and sale_type = 'retail'

select * from sls.deals_accounting_detail where year_month = 202310

select control, year_month, trans_Description, gross_Category, sum(amount) 
from sls.deals_accounting_detail where control = 't10827'
group by control, year_month, trans_Description, gross_Category



-- to handle unwinds, need data from before the month in question
select * 
from (
select stock_number, bopmast_id, buyer, psc_last_name, fi_last_name from sls.deals_by_month) a
join (
select control, trans_description, gross_category, amount from sls.deals_accounting_detail) b on a.stock_number = b.control and a.buyer = b.trans_description
-- where a.stock_number = 't10827'
where a.stock_number in (
  select stock_number
  from sls.deals_by_month
  where year_month = 202310)


select * 
from (
select stock_number, bopmast_id, buyer, psc_last_name, fi_last_name from sls.deals_by_month) a
join (
select control, year_month, trans_Description, gross_Category, sum(amount) 
from sls.deals_accounting_detail -- where control = 't10827'
group by control, year_month, trans_Description, gross_Category) b on a.stock_number = b.control and a.buyer = b.trans_description
-- where a.stock_number = 't10827'
where a.stock_number in (
  select stock_number
  from sls.deals_by_month
  where year_month = 202310)  

select * from sls.deals_accounting_detail
where year_month = 202311  

select * from sls.deals_by_month where stock_number = 'G48345H'

----------------------------------------------------------------------------------------------------------------------

drop table if exists t1;
create temp table t1 as
select control, trans_description
from sls.deals_accounting_detail a
where exists (
  select 1
  from arkona.xfm_bopmast
  where bopmast_stock_number = a.control)
and exists (
  select 1 
  from arkona.xfm_bopmast
  where bopmast_search_name = a.trans_description)
group by control, trans_description;

select * from t1 order by control

select * from t1 where control = 't10827'


drop table if exists t1;
create temp table t1 as
select control, trans_description, b.record_key
from sls.deals_accounting_detail a
join arkona.xfm_bopmast b on a.control = b.bopmast_stock_number
  and b.bopmast_search_name = a.trans_description
group by control, trans_description, b.record_key

select * from t1 where control = 't10827'

drop table if exists t2;
create temp table t2 as
select * 
from t1 a
join sls.deals_by_month b on a.control = b.stock_number
  and trans_description = b.buyer
  and a.record_key = b.bopmast_id 
where b.year_month > 202308  
order by a.control

select *
from t2 a
join (
  select control
  from t2
  group by control
  having count(*) > 1) b on a.control = b.control

 -------------------------------------------------------------------------------------------------------------------------------------

11/4/23
as i try to figure out whats happening with stuff not running last night, it brought to my attention the tables sls.ext_accounting_Base and
sls.ext_accounting_deals, aha, another approach to extracting deals ferom accounting
do these tables offer any better opportunity to add the bopmast_id to accounting data ?


  select * from sls.ext_accounting_Base limit 100

select * from sls.ext_accounting_deals limit 100

select * from sls.ext_accounting_deals where gl_date between '10/01/2023' and '10/31/2023' and unit_count <> 1


select * from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
  and b.account = '244000'
where a.control = 'h16784'

select * from sls.ext_accounting_Base where control = 'h16784'


select control
from (
select control, gl_description
	from sls.ext_accounting_deals
	group by control, gl_description) a
	group by control
having count(*) > 1


-- 44368
select control, gl_description, min(gl_date), max(gl_date)
from sls.ext_accounting_deals
group by control, gl_description
order by control

select b.*, c.record_key, c.bopmast_search_name, c.bopmast_stock_number, c.date_capped
from (
	select a.control, a.gl_description, min(a.gl_date), max(a.gl_date)
	from sls.ext_accounting_deals a
	group by a.control, a.gl_description) b
left join arkona.xfm_bopmast c on b.control = c.bopmast_stock_number
  and b.gl_description = c.bopmast_search_name
order by b.control


--< H17035L -------------------------------------------------------------------------------------------------------------------





select * 
from sls.deals_by_month 
where bopmast_id in (25859,25871)

select * 
from arkona.xfm_bopmast
where bopmast_stock_number = 'H17035L'


select * 
from arkona.ext_bopmast
where bopmast_stock_number = 'H17035L'

-- this one shows 25871 is the deal, 25859 is deleted, but also
-- shows 2 -1 unit_counts
select * from sls.deals where stock_number = 'H17035L'

select *  
from sls.deals_by_month
where stock_number = 'H17035L'

-- fuck, 2 customer numbers for the same person
select * from arkona.ext_bopname where bopname_record_key in (1187657,1187582)

--/> H17035L -------------------------------------------------------------------------------------------------------------------


select * from sls.deals_Accounting_detail where year_month = 202311

select * from sls.deals_gross_by_month where year_month = 202311

select a.post_status, a.trans, a.seq, b.the_date, c.account, d.description, a.amount
from fin.fact_gl a 
join dds.dim_date b on a.date_key = b.date_key
join fin.dim_account c on a.account_key = c.account_key
join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
where control = 'H17035L'
order by b.the_date


select distinct control, year_month
from sls.deals_accounting_detail
where control in (
	select control 
	from sls.deals_Accounting_detail 
	where year_month = 202311)
and gl_account not in ('264502','164502')
order by control

	select distinct gl_Account 
	from sls.deals_Accounting_detail 
	where year_month = 202311








