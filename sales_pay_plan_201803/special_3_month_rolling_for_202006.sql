﻿/*
Hey Jon

We told the sales teams at all 3 rooftops, that their 3 month rolling average for the month of June would be from December 2019, January 2020, and February 2020.

EX: Sales person X sold-

10 Cars in December
12 cars in January
11 cars in February

Their 3 month rolling that would show in Vision for June 1st would be 11 cars.

Does this make sense?

Let me know if it is possible

Thanks

Nick
*/
select *
from (

select psc_employee_number, round(sum(unit_count)/3, 1) as "3 month rolling avg", last_name
from (
  select a.psc_employee_number, psc_last_name, b.first_full_month_emp,
    case
      when ssc_last_name = 'none' then unit_count
      else 0.5 * unit_count
    end as unit_count,
    b.last_name
  from sls.deals_by_month a
  inner join sls.personnel b on a.psc_employee_number = b.employee_number
  where a.year_month in (
      select year_month
      from sls.months where seq in (
      select seq - 3 from sls.months where year_month = (select year_month from sls.months where open_closed = 'open')
      union
      select seq - 2 from sls.months where year_month = (select year_month from sls.months where open_closed = 'open')
      union
      select seq - 1 from sls.months where year_month = (select year_month from sls.months where open_closed = 'open'))) 

      
  union all

  
  select ssc_employee_number, ssc_last_name, b.first_full_month_emp,
    0.5 * unit_count as unit_count, b.last_name
  from sls.deals_by_month a
  inner join sls.personnel b on a.ssc_employee_number = b.employee_number
  where a.year_month in (
      select year_month
      from sls.months where seq in (
      select seq - 3 from sls.months where year_month = (select year_month from sls.months where open_closed = 'open')
      union
      select seq - 2 from sls.months where year_month = (select year_month from sls.months where open_closed = 'open')
      union
      select seq - 1 from sls.months where year_month = (select year_month from sls.months where open_closed = 'open'))) 
    and a.ssc_last_name <> 'none') e
group by psc_employee_number, last_name) aa

full outer join (

select psc_employee_number, round(sum(unit_count)/3, 1) as "3 month rolling avg", last_name
from (
  select a.psc_employee_number, psc_last_name, b.first_full_month_emp,
    case
      when ssc_last_name = 'none' then unit_count
      else 0.5 * unit_count
    end as unit_count,
    b.last_name
  from sls.deals_by_month a
  inner join sls.personnel b on a.psc_employee_number = b.employee_number
  where a.year_month in (201912, 202001, 202002) 
  union all
  select ssc_employee_number, ssc_last_name, b.first_full_month_emp,
    0.5 * unit_count as unit_count, b.last_name
  from sls.deals_by_month a
  inner join sls.personnel b on a.ssc_employee_number = b.employee_number
  where a.year_month in (201912, 202001, 202002) 
    and a.ssc_last_name <> 'none') e
group by psc_employee_number, last_name) bb on aa.psc_employee_number = bb.psc_employee_number


-- all right start with just current consultants
drop table if exists consultants;
create temp table consultants as
select a.*
from sls.personnel a
where end_date > current_Date
  and employee_number <> 'HSE'
  and ry1_id <> 'none'
  and last_name not in ('haley','bedney','dockendorf','seay')
order by store_code, last_name; 


drop table if exists sls.june_2020_3_month_rolling_avg;
create table sls.june_2020_3_month_rolling_avg (
  psc_employee_number citext,
  "3 month rolling avg" numeric);

select * from  sls.june_2020_3_month_rolling_avg
  
insert into sls.june_2020_3_month_rolling_avg 
select psc_employee_number, "3 month rolling avg"
from (
  select x.psc_last_name, x.psc_employee_number, may_avg, feb_avg,
    case
      when may_avg > coalesce(feb_avg, 0) then may_avg
      else feb_avg
    end as "3 month rolling avg"
  from (
  -- mar - may
  select psc_last_name, psc_employee_number,
    sum(unit_count) filter (where year_month = 202005) as may,
    sum(unit_count) filter (where year_month = 202004) as apr,
    sum(unit_count) filter (where year_month = 202003) as mar, 
    sum(unit_count) as total,
    round(sum(unit_count)/3, 1) as may_avg
  from (  
    -- psc
    select a.psc_employee_number, psc_last_name, a.year_month,
      case
        when ssc_last_name = 'none' then unit_count
        else 0.5 * unit_count
      end as unit_count
    from sls.deals_by_month a
    inner join consultants b on a.psc_employee_number = b.employee_number
    where a.year_month in (
        select year_month
        from sls.months where seq in (
        select seq - 3 from sls.months where year_month = (select year_month from sls.months where open_closed = 'open')
        union
        select seq - 2 from sls.months where year_month = (select year_month from sls.months where open_closed = 'open')
        union
        select seq - 1 from sls.months where year_month = (select year_month from sls.months where open_closed = 'open'))) 
    union all     
    -- ssc 
    select a.ssc_employee_number, ssc_last_name, a.year_month,
      0.5 * unit_count as unit_count
    from sls.deals_by_month a
    inner join consultants b on a.ssc_employee_number = b.employee_number
    where a.year_month in (
        select year_month
        from sls.months where seq in (
        select seq - 3 from sls.months where year_month = (select year_month from sls.months where open_closed = 'open')
        union
        select seq - 2 from sls.months where year_month = (select year_month from sls.months where open_closed = 'open')
        union
        select seq - 1 from sls.months where year_month = (select year_month from sls.months where open_closed = 'open'))) 
      and a.ssc_last_name <> 'none') aa
  group by psc_employee_number, psc_last_name) x     

  full outer join (
  -- dec - feb
  select psc_last_name, 
    sum(unit_count) filter (where year_month = 202002) as feb,
    sum(unit_count) filter (where year_month = 202001) as jan,
    sum(unit_count) filter (where year_month = 201912) as dec, 
    sum(unit_count) as total,
    round(sum(unit_count)/3, 1) as feb_avg
  from (  
    -- psc
    select a.psc_employee_number, psc_last_name, a.year_month,
      case
        when ssc_last_name = 'none' then unit_count
        else 0.5 * unit_count
      end as unit_count
    from sls.deals_by_month a
    inner join consultants b on a.psc_employee_number = b.employee_number
    where a.year_month in (201912, 202001, 202002)
    union all     
    -- ssc 
    select a.ssc_employee_number, ssc_last_name, a.year_month,
      0.5 * unit_count as unit_count
    from sls.deals_by_month a
    inner join consultants b on a.ssc_employee_number = b.employee_number
    where a.year_month in (201912, 202001, 202002)
      and a.ssc_last_name <> 'none') aa
  group by psc_employee_number, psc_last_name) y on x.psc_last_name = y.psc_last_name ) zz    
order by psc_last_name    