﻿-- DealsChangedRows() fails due to test for questionable uncategorized changes

select * from sls.changed_deals

-- OR  

-- this exposes the problem deal(s)
select *
from (-- query that generates ros for sls.changed_deals
  select c.hash as xfm_hash, d.hash as deal_hash,
    case
      when d.deal_status_code = 'none' and c.deal_status = 'none' then 'ignore'                      
      when c.bopmast_id in (43283,13634,43827,46610,50579,58799,60130,60157,64182,66640,69669) then 'ignore'
      when c.bopmast_id in(15088, 16540, 17178, 21012) and c.store_code = 'RY2' then 'ignore'
      when c.bopmast_id in (1418, 1564) and c.store_code = 'RY8' then 'ignore'
      when c.gl_date <> d.gl_date
        and c.gl_count < 0 and d.unit_count > 0
        and c.buyer_bopname_id = d.buyer_bopname_id
        and c.deal_status = d.deal_status_code
        and d.notes <> 'type_2_a' then 'type_2_a'  --30365 3/6
      when c.deal_status in ('none', 'A') and d.deal_status_code = 'U' then 'type_2_b' -- 30745x 3/6
      when (
            c.deal_status = 'deleted'
              and d.deal_status_code = 'U'
              and c.gl_date <> d.gl_date
              and d.notes <> 'type_2_a')
        or ( -- curious possible anomaly 31135C, 31588XA, no change in gl_date
            c.deal_status = 'deleted'
              and d.deal_status_code in ('none','A')
              and d.notes = 'type_2_b')
        then 'type_2_c' -- 30157A
      when d.notes = 'type_2_a' and c.deal_status = 'deleted' then 'type_2_d'
      when d.notes = 'type_2_a' and c.gl_count > 0 and c.bopmast_id = d.bopmast_id then 'type_2_e'
      when (d.notes = 'type_2_b' or d.notes = 'type_2_h') and c.gl_count > 0 and c.deal_status = 'U'
        then 'type_2_f'
      -- c: most recent xfm rows 1 row per store/bopmast_id
      -- d: most recent deals rows 1 row per store/bopmast_id
      when c.deal_status = 'deleted' and d.deal_status_code = 'U' then 'type_2_g'
      when c.gl_date = d.gl_date and c.deal_status = 'A' and d.deal_status_code = 'A'
        and d.notes = 'type_2_b' and c.gl_count = 1 and d.unit_count = -1 then 'type_2_h'
      when d.notes = 'type_2_b' and d.deal_status_code = 'none' and c.deal_status = 'A' then 'do nothing'
      when c.deal_status <> d.deal_status_code and c.stock_number in ('31126A','31694','31493XX') then
        'type_2_i'
      when c.deal_status <> d.deal_status_code
        or c.gl_count <> d.unit_count
        or (c.gl_date <> d.gl_date
          and -- 29164B: same year_month and no change in status and no change in count = type 1
              (extract(year from c.gl_date) * 100) + extract(month from c.gl_date) <> (extract(year from d.gl_date) * 100) + extract(month from d.gl_date)
              OR c.deal_status <> d.deal_status_code
              or c.gl_count <> d.unit_count)
           then 'DANGER WILL ROBINSON'
      else 'type_1'
    end as change_type
  from ( -- c: most recent xfm rows 1 row per store/bopmast_id
    select *
    from sls.xfm_deals a
    where a.seq = (
      select max(seq)
      from sls.xfm_deals
      where store_code = a.store_code
        and bopmast_id = a.bopmast_id)) c
  inner join (-- d: most recent deals rows 1 row per store/bopmast_id
    select *
    from sls.deals a
    where a.deal_status <> 'deleted'
      and a.seq = (
        select max(seq)
        from sls.deals
        where store_code = a.store_code
          and bopmast_id = a.bopmast_id)) d on c.store_code = d.store_code
      and c.bopmast_id = d.bopmast_id
    and c.hash <> d.hash
    and c.bopmast_id not in (57595, 18539, 60897, 64005,65229,57595, 18539, 60897, 64005, 65229, 24328)) x
  where change_type in ('DANGER WILL ROBINSON',' do nothing');  

--------------------------------------------------------------------------------------------
--< 11/08/23
--------------------------------------------------------------------------------------------
select * from sls.changed_deals
select * from sls.deals where hash = '4abb52ba3af2294d861d4ab4d7650695'
select * from sls.xfm_deals where hash = '30c5546aa53c80908d8f754e1c804786'
select * from sls.deals where stock_number = 'H17027'
select * from arkona.xfm_bopmast where bopmast_stock_number = 'H17027' order by row_from_date

select 'deals', store_code, bopmast_id, deal_status,
  deal_type_code, sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), coalesce(unit_count, 0)
from sls.deals
where stock_number = 'H17027'                                           
union all
select 'xfm_deals', store_code, bopmast_id, deal_status,
  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), gl_count
from sls.xfm_deals
where stock_number = 'H17027' and run_date = '2023-11-08'


not sure what to do, add it to ignore and see what the results are
on Quintins vision page currently shows as 2 unwinds in November, should be 0 impact in November
well, that got it down to 1 unwind
update sls.deals
set unit_count = 0
where bopmast_id = 25851
  and deal_status_code = 'A'

select sls.update_consultant_payroll()

select * from sls.consultant_payroll where last_name = 'wolf' and year_month = 202311

select * from sls.deals_by_month where stock_number = 'h17027'

update sls.deals_by_month
set unit_count = 0
where stock_number = 'h17027'
  and year_month = 202311

  
--------------------------------------------------------------------------------------------
--/> 11/08/23
--------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------
--< 10/29/23
--------------------------------------------------------------------------------------------
select * from sls.changed_deals
select * from sls.deals where hash = '9f8da36dc2fbd604b43df824c82823bd'
select * from sls.xfm_deals where hash = 'c3868395eb652066e7494d7932df95bb'
select * from sls.deals where stock_number = 'H16784'
select * from arkona.xfm_bopmast where bopmast_stock_number = 'H16784' order by row_from_date

select 'deals', store_code, bopmast_id, deal_status,
  deal_type_code, sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), coalesce(unit_count, 0)
from sls.deals
where stock_number = 'H16784'                                           
union all
select 'xfm_deals', store_code, bopmast_id, deal_status,
  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), gl_count
from sls.xfm_deals
where stock_number = 'H16784' and run_date = '2023-10-29'

turns out H16784 was a september deal, but was in and outed a couple times in october, all the same customer
just crossing ts and dotting is

update sls.deals
set unit_count = 0
where stock_number = 'H16784'
  and run_date = '10/27/2023'

add the deal to ignore

and fi for octrober is fucked up  

select * from sls.personnel where ry2_id = 'CJO'  -- CHJ

select * from sls.deals_accounting_detail where control = 'H16784' and gross_category like 'fi%'

select * from sls.deals_Gross_by_month where control = 'H16784'

select * from sls.deals_by_month where stock_number = 'h16784'

select sls.update_consultant_payroll()  

----------- **********  i jumped the gun worrying about all the bad side effects and what i have to rerun
----------- **********  instead of fixing the H16784 ignore issue and then letting luigi finish
----------- **********  that is done now, so lets see how things look
and of course, everything looks fine now
--------------------------------------------------------------------------------------------
--/> 10/29/23
--------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------
--< 10/13/23
--------------------------------------------------------------------------------------------
toyota deal from september that was uncapped and then recapped in october, i think
select * from arkona.xfm_bopmast where bopmast_stock_number = 'T10749A'
select * from sls.deals where stock_number = 'T10749A'
select * from sls.xfm_deals where stock_number = 'T10749A'
select * from arkona.ext_bopname where bopname_record_key = 1028363

select 'deals', store_code, bopmast_id, deal_status,
  deal_type_code, sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), coalesce(unit_count, 0)
from sls.deals
where stock_number = 'T10749A'                                           
union all
select 'xfm_deals', store_code, bopmast_id, deal_status,
  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), gl_count
from sls.xfm_deals
where stock_number = 'T10749A' and run_date = '2023-10-13'


--------------------------------------------------------------------------------------------
--< 09/07/23
--------------------------------------------------------------------------------------------
select * from sls.deals where stock_number = 'G47977P'
select * from sls.changed_deals
select * from sls.changed_Deals
select * from sls.deals where hash = '35ef3693d293830706463c43359a15f1'
select * from sls.xfm_deals where hash = '32de0d40ad716c0c5110b44fea56de49'
select * from sls.xfm_deals where stock_number = 'G47977P'
-- an unwind at toyota 
select 'deals', store_code, bopmast_id, deal_status,
  deal_type_code, sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), coalesce(unit_count, 0)
from sls.deals
where stock_number = 'G47977P'                                           
union all
select 'xfm_deals', store_code, bopmast_id, deal_status,
  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), gl_count
from sls.xfm_deals
where stock_number = 'G47977P' and run_date = '2023-09-11'


select * from sls.deals where hash = '7e4bd4c19a60036a3afb5d627f1ca50d'
select * from sls.xfm_deals where hash = '1e3cfe551594939a72e3aee6fa92640a'



--------------------------------------------------------------------------------------------
--/> 09/07/23
--------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------
--< 07/25/23
--------------------------------------------------------------------------------------------
select * from sls.changed_Deals
select * from arkona.xfm_bopmast where bopmast_stock_number = 'T10632HA'
select * from sls.xfm_deals where stock_number = 'T10632HA'
-- an unwind at toyota that was ws to GM, ignore bopmast_i3 1418
select 'deals', store_code, bopmast_id, deal_status,
  deal_type_code, sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), coalesce(unit_count, 0)
from sls.deals
where stock_number = 'T10632HA'                                           
union all
select 'xfm_deals', store_code, bopmast_id, deal_status,
  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), gl_count
from sls.xfm_deals
where stock_number = 'T10632HA' and run_date = '2023-07-25'


select * from sls.deals where stock_number = 'T10632HA'
--------------------------------------------------------------------------------------------
--/> 07/25/23
--------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------
--< 07/07/23
--------------------------------------------------------------------------------------------
select * from sls.changed_Deals
select * from sls.xfm_deals where hash = '19c66692440d49d6f1cca60e1f78d6c5'
select * from arkona.xfm_bopmast where bopmast_stock_number = 'G47371A'
select * from sls.xfm_deals where stock_number = 'G47371A'
-- only change was adding service contract, ignore bopmast_id 78194
select 'deals', store_code, bopmast_id, deal_status,
  deal_type_code, sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), coalesce(unit_count, 0)
from sls.deals
where stock_number = 'G47371A'                                           
union all
select 'xfm_deals', store_code, bopmast_id, deal_status,
  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), gl_count
from sls.xfm_deals
where stock_number = 'G47371A' and run_date = '2023-07-07'

--------------------------------------------------------------------------------------------
--/> 07/07/23
--------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------
--< 05/13/23
--------------------------------------------------------------------------------------------
select * from sls.changed_Deals
select * from sls.xfm_deals where hash = 'ce5891ec03a73e9eec5ea975cc19d620'
select * from arkona.xfm_bopmast where bopmast_stock_number = 'H16412'
select * from sls.xfm_deals where stock_number = 'H16412'
-- H16412/24930 accounting fiddling with an april fleet deal - ignore
select 'deals', store_code, bopmast_id, deal_status,
  deal_type_code, sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), coalesce(unit_count, 0)
from sls.deals
where stock_number = 'G46816'                                           
union all
select 'xfm_deals', store_code, bopmast_id, deal_status,
  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), gl_count
from sls.xfm_deals
where stock_number = 'G46816' and run_date = '2023-05-31'


--------------------------------------------------------------------------------------------
--/> 05/13/23
--------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------
--< 05/03/23
--------------------------------------------------------------------------------------------
select * from sls.changed_Deals
select * from sls.xfm_deals where hash = 'e8831e8b6ae7ce7d4c47c0f13a75774f'
select * from arkona.xfm_bopmast where bopmast_stock_number = 'G46770'

-- G46770/77134 michelle fiddling with an april fleet deal - ignore
select 'deals', store_code, bopmast_id, deal_status,
  deal_type_code, sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), coalesce(unit_count, 0)
from sls.deals
where stock_number = 'G46770'                                           
union all
select 'xfm_deals', store_code, bopmast_id, deal_status,
  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), gl_count
from sls.xfm_deals
where stock_number = 'G46770' and run_date = '2023-05-03'

select * from sls.xfm_deals where hash = 'b635c73e42136fbcbba471a1db96da95'
-- G46771/77132 michelle fiddling with an april fleet deal - ignore
select 'deals', store_code, bopmast_id, deal_status,
  deal_type_code, sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), coalesce(unit_count, 0)
from sls.deals
where stock_number = 'G46771'                                           
union all
select 'xfm_deals', store_code, bopmast_id, deal_status,
  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), gl_count
from sls.xfm_deals
where stock_number = 'G46771' and run_date = '2023-05-03'

select * from sls.xfm_deals where hash = '343a57b832cc4e276d91043dff150646'
select * from sls.xfm_deals where hash = 'b635c73e42136fbcbba471a1db96da95'
-- G46773/77133 michelle fiddling with an april fleet deal - ignore
select 'deals', store_code, bopmast_id, deal_status,
  deal_type_code, sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), coalesce(unit_count, 0)
from sls.deals
where stock_number = 'G46773'                                           
union all
select 'xfm_deals', store_code, bopmast_id, deal_status,
  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), gl_count
from sls.xfm_deals
where stock_number = 'G46773' and run_date = '2023-05-03'
--------------------------------------------------------------------------------------------
--</> 05/03/23
--------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------
--< 04/28/23
--------------------------------------------------------------------------------------------
-- G46687
-- this was a feb deal, unwound and recapped a couple times, with date changes, per michelle,
-- just a crossing t and dotting i deal, so added it to ignore
select 'deals', store_code, bopmast_id, deal_status,
  deal_type_code, sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), coalesce(unit_count, 0)
from sls.deals
where stock_number = 'G46687'                                           
union all
select 'xfm_deals', store_code, bopmast_id, deal_status,
  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), gl_count
from sls.xfm_deals
where stock_number = 'G46687' and run_date = '2023-04-28'
--------------------------------------------------------------------------------------------
--/> 04/28/23
--------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------
--< 04/14/23
--------------------------------------------------------------------------------------------
select * from sls.changed_deals
select * from sls.deals where hash in ('ee155284616eb68891bf2968c3b9592a')

select * from sls.deals where stock_number = 'G45685' 

select * from sls.xfm_deals where stock_number = 'G45685' 

select 'deals', store_code, bopmast_id, deal_status,
  deal_type_code, sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), coalesce(unit_count, 0)
from sls.deals
where stock_number = 'G45685'                                           
union all
select 'xfm_deals', store_code, bopmast_id, deal_status,
  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), gl_count
from sls.xfm_deals
where stock_number = 'G45685' and run_date = '2023-04-07'

the odometer changed freom 30 to 7
ignore


--------------------------------------------------------------------------------------------
--/> 04/14/23
--------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------
--< 03/08/23
--------------------------------------------------------------------------------------------
select * from sls.changed_deals
select * from sls.deals where hash in ('ed51261dfcf2be019f8c1d37e0df4def')

select * from sls.deals where stock_number = 'G46563' 

select * from sls.xfm_deals where stock_number = 'G46563' 

added it to ignore in the python code

select 'deals', store_code, bopmast_id, deal_status,
  deal_type_code, sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), coalesce(unit_count, 0)
from sls.deals
where stock_number = 'G46563'                                           
union all
select 'xfm_deals', store_code, bopmast_id, deal_status,
  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), gl_count
from sls.xfm_deals
where stock_number = 'G46563' and run_date = '03/08/2023'



select 'xfm' as source, 0 as year_month, run_date, store_code, bopmast_id, deal_status,
  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  gl_date, gl_count, '' as notes, hash
from sls.xfm_deals 
-- where bopmast_id = 43389
where stock_number = 'G46563' and run_date = '03/08/2023' 
union
select 'deal', year_month, run_date, store_code, bopmast_id, deal_status_code,
  deal_type_code, sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  gl_date, unit_count, notes, a.hash
from sls.deals a
-- where bopmast_id = 43389
where stock_number = 'G46563' 
order by source, run_date

--------------------------------------------------------------------------------------------
--< 12/1/21
--------------------------------------------------------------------------------------------
dd36b6652eb97ac67e93e517c8af742c,89a2dd7bac1d690f3006a36842eec964,DANGER WILL ROBINSON

select * from sls.changed_deals

select * from sls.xfm_deals where hash = 'dd36b6652eb97ac67e93e517c8af742c'

select * from sls.deals where stock_number = 'G43180PA'
bopmast_id:
	69669 november deal
	69197 october deal unwound


select * 
from arkona.xfm_bopmast
where bopmast_stock_number = 'G43180PA'
order by bopmast_key

-- this looks like a simple unwind, don't remember what to do with it

select * 
from sls.deals a
where deal_status_code = 'deleted'
and exists (
  select 1
  from sls.deals
  where extract(month from run_date) <> extract(month from a.run_date)
    and stock_number = a.stock_number
    and bopmast_id <> a.bopmast_id)
order by run_date desc  
limit 10

select * from sls.xfm_deals where stock_number = 'G43180PA' order by bopmast_id, run_date

select 'deals', store_code, bopmast_id, deal_status,
  deal_type_code, sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999') as gl_date, coalesce(unit_count, 0) as uniit_count
from sls.deals
where stock_number = 'G43180PA' and bopmast_id = 69669                                                    
union all
select 'xfm_deals', store_code, bopmast_id, deal_status,
  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), gl_count
from sls.xfm_deals
where stock_number = 'G43180PA' and bopmast_id = 69669    and seq = 2

my first shot at understanding this one (given what a cluster fuck yesterday was)
the only diff is gl_date, add 69669 to ignore  
!!! doing it on the server, after done with month end need to clean up car_deals.py !!!

so, updated the ignore in this query, reran and got a clean result
updated the ignore statement on the server, reran and it failed again



on the server added a commit after the execution of the insert into sls.changed_deals query, and there are none with DANGER
select * from sls.changed_deals

-- -- this yielded nothing, never got any rows in jon.failed_deals
-- create table jon.failed_deals (
--   xfm_hash citext,
--   deal_hash citext,
--   change_type citext);
-- 
-- truncate jon.failed_Deals;
-- 
-- insert into jon.failed_deals
-- select * from sls.changed_deals
-- 
-- select * from jon.failed_deals

fixed the flightplan failure with the honda civic accounts
added that fix to ExtAccountingDeals
reran failed again
and again, i see no DANGER
select * 
from sls.changed_deals


select * from sls.xfm_deals
where run_date = current_date
  and row_type = 'new'

select * from sls.xfm_deals
where run_date = current_date
  and row_type = 'update'  

select *
from sls.deals
where run_date = current_Date  

so, leave the 69669 in ignore in DealsChangedRows
comment out the DANGER test
let it run

AND IT FUCKING FAILED WITH DANGER !!!!!!!!!!!!!!!!!!!!!!!

SO, I FAKED AN OUTPUT FILE FOR DEALSCHANGEDROWS AND LET IT RUN

--------------------------------------------------------------------------------------------
--< 06/10/21
--------------------------------------------------------------------------------------------
select *
from sls.xfm_deals where hash = '22350d508d37130fcea17dd7b0b3a0a1'

select * 
from sls.deals
where stock_number = 'G41393X'

select * 
from arkona.xfm_bopmast
where bopmast_stock_number = 'G41393X'
order by bopmast_key

it is all key 66640
delete seq 2 from deals, add to ignore

delete 
-- select *
from sls.deals
where bopmast_id = 66640
  and seq = 2

select * 
from sls.deals 
where run_date = current_date
--------------------------------------------------------------------------------------------
--< 04/13/21 
--------------------------------------------------------------------------------------------
SELECT * FROM SLS.XFM_DEALS WHERE hash = 'e02f8e59671e5054243960cbd19bbcd6'
select * from sls.deals where vin = '3GTP2VE73CG155217'
looks like it is all key 21012, H14388G Brandon Sandberg
select * from arkona.xfm_bopmast where bopmast_Stock_number = 'H14388G'



select 'deals' as source, store_code, bopmast_id, deal_status,
  deal_type_code, sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999') as gl_date, coalesce(unit_count, 0) as uniit_count
from sls.deals
where stock_number = 'H14388G' and bopmast_id = 21012                                                    
union all
select 'xfm_deals', store_code, bopmast_id, deal_status,
  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), gl_count
from sls.xfm_deals
where stock_number = 'H14388G' --and seq = 3
order by source, gl_date

all just paperwork changes, should never have been -1 unit count
so, delete from sls.deals the april entry, seq = 2
and add 21012 to ignore
returns deal to unit_count 1 in march, and nothing else

delete from sls.deals 
where store_code = 'RY2'
  and bopmast_id = 21012
  and seq = 2;


--------------------------------------------------------------------------------------------
--/> 04/13/21
--------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------
--< 03/18/21
--------------------------------------------------------------------------------------------
SELECT * FROM SLS.XFM_DEALS WHERE hash = 'ab56d7c82b838a9c6bcbc7d13674b840'
select * from sls.deals where vin = 'KL4MMCSL0MB037455'
sale of G40805R shows as notes = type_1 seq = 1, i believe it should be notes = new row and in xfm_deals row_type shows update
select * from sls.xfm_deals where vin = 'KL4MMCSL0MB037455'
ah, that was wrong, the change is the cobuyer was removed

approved, capped, gl dates changed but are of no consequence
so, change the deal cobuyer in sls.deals, and add bopmast_id 65229 to ignore

select * from sls.deals where stock_number = 'G40805R'

update sls.deals
set cobuyer_bopname_id = 0
where stock_number = 'G40805R';

--------------------------------------------------------------------------------------------
--/> 03/18/21
--------------------------------------------------------------------------------------------

--< 01/14/2021
SELECT * FROM SLS.XFM_DEALS WHERE hash = '30a6230f5e5ebe080e2334c10dd8713c'

select * from sls.deals where vin = 'KL7CJPSB7LB024562'  



the only diff is gl_date, add 64182 to RY1 ignore
--/ 01/14/2021 ----------------------------------------------------------

G34805A, delevered in 201809, last night michelle unwound and recapped it, same customer                     
select *
from sls.xfm_deals
where hash = 'b70c46f70eb62a7621eeefa22bbd1a65'

select *
from sls.xfm_deals
where stock_number = 'G34805A'

select *
from sls.deals
where stock_number = 'G34805A'

select 'deals', store_code, bopmast_id, deal_status,
  deal_type_code, sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), coalesce(unit_count, 0)
from sls.deals
where stock_number = 'G34805A' and bopmast_id = 64182                                                    
union all
select 'xfm_deals', store_code, bopmast_id, deal_status,
  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), gl_count
from sls.xfm_deals
where stock_number = 'G34805A' and run_date = '01/14/2021'

deal type changed from C to F, 
approved, capped, gl dates changed but are of no consequence
so, change the deal type data in sls.deals, and add bopmast_id 50579 to ignore

update sls.deals
set deal_type_code = 'F'
where stock_number = 'G34805A';


2/13/19
H11735
xfm_deals had an existing row for H11735 in accepted status
2/13 generated another with accepted status
deleted both row with accepted status, and it passed

select 'deals' as source, run_date, store_code, bopmast_id, deal_status,
  deal_type_code, sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), coalesce(unit_count, 0), seq
from sls.deals
where stock_number = 'h11735'                                                        
union all
select 'xfm_deals', run_date, store_code, bopmast_id, deal_status,
  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), gl_count, seq
from sls.xfm_deals
where stock_number = 'h11735' 
order by source, seq


2/14/19
h11735, re-capped again

select *
from sls.xfm_deals
where stock_number = 'h11735'

select *
from sls.deals
where stock_number = 'h11735'

delete from sls.xfm_deals 
where stock_number = 'H11735'
  and seq > 1;

delete from sls.deals 
where stock_number = 'H11735'
  and seq > 1;

2/15/19
H11735 again
nothing changed in dealertrack, but, approved_date, delivery_date & gl_date are all different than the orig info
but those changes were all internal, its all been on bopmast_id 16540, no changes of interest
fix needs to exclude this xfm_deals row
so need to keep it out of ext_deals or xfm_deals
the new rows make it a feb deal instead of a jan deal, so need to fix it
do it in DealsChangedRows: ignore bopmast_id 16540
that will leave a bogus ros in sls.xfm_deals, but that is ok
select * from sls.ext_deals where bopmast_id = 16540