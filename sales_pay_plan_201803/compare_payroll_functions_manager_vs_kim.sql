﻿-- no fucking json
-- compares the ouput of sls.json_get_payroll_for_submittal(citext) and sls.json_get_payroll_for_kim(citext)
-- 8/22 added term_guarantee_multiplier
do
$$
-- declare _store citext := 'RY1';
begin
-- select * from sls.json_get_payroll_for_submittal('RY1')
drop table if exists wtf;
create temp table wtf as 

with
  open_month as (
    select 201808 as year_month)

select a.employee_number as id, a.last_name || ', ' || a.first_name as consultant, a.payplan as plan, a.unit_count as units,
  a.unit_pay, a.fi_pay, a.fi_gross, a.chargebacks, a.fi_total, a.pto_pay, coalesce(a.pto_hours, 0) as pto_hours,
  coalesce(a.pto_rate, b.pto_rate) as pto_rate, coalesce(c.spiffs, 0) as spiffs,
  a.total_earned, a.draw, 
  round(a.guarantee * coalesce(g.guarantee_multiplier, 1 * coalesce(h.term_guarantee_multiplier, 1)), 2) as guarantee, 
  coalesce(d.adjusted_amount, 0) as adjusted_amount,
  coalesce(e.additional_comp, 0) as additional_comp,
  round (
    case -- paid spiffs deducted only if base guarantee
      when a.total_earned >= a.guarantee then
        a.total_earned - coalesce(a.draw, 0) + coalesce(d.adjusted_amount, 0) + coalesce(e.additional_comp, 0)
      else
        (a.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) 
            - coalesce(c.spiffs, 0) + coalesce(e.additional_comp, 0) + coalesce(d.adjusted_amount, 0)
      end, 2) as due,
  case when a.months_employed < 0 then 0 else a.months_employed end as months_employed,
   "3 month rolling_avg" as three_month_average, f.start_date as hire_date
from sls.consultant_payroll a
left join sls.pto_intervals b on a.employee_number = b.employee_number
  and a.year_month = b.year_month
left join sls.paid_by_month c on a.employee_number = c.employee_number
  and a.year_month = c.year_month
left join (
  select employee_number, sum(amount) as adjusted_amount
  from sls.payroll_adjustments
  where year_month =  (select year_month from open_month)
  group by employee_number) d on a.employee_number = d.employee_number
left join (
  select employee_number, sum(amount) as additional_comp
  from sls.additional_comp
  where thru_date > (
    select first_of_month
    from sls.months
    where open_closed = 'open')
  group by employee_number) e on a.employee_number = e.employee_number  
left join sls.personnel f on a.employee_number = f.employee_number
left join ( -- if this is the first month of employment, multiplier = work days in month since hire date/work days in month
  select a.employee_number, 
    round(wd_of_month_remaining::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as guarantee_multiplier 
  from sls.personnel a
  inner join dds.dim_date b on a.start_date = b.the_date
    and b.year_month = (select year_month from open_month)) g on f.employee_number = g.employee_number     
left join ( -- if employee termed mid month, multiplier elapsed wd in month at term date/work days in month
  select a.employee_number, 
    round(wd_of_month_elapsed::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as term_guarantee_multiplier
  from sls.personnel a
  inner join dds.dim_date b on a.end_date = b.the_date
    and b.year_month = (select year_month from open_month) ) h on f.employee_number = h.employee_number       
where a.year_month = (select year_month from open_month)
-- and f.store_code = _store 
order by a.last_name;
end
$$;
select * from wtf;    
--           
-- 
-- 
-- 
do 
$$
declare _store citext := 'RY1';
begin

drop table if exists wtf1;
create temp table wtf1 as 
-- -- 
-- 
with
  open_month as (
    select 201808 as year_month)    
select a.last_name || ', ' || a.first_name as consultant, a.employee_number, 
    a.pto_pay as "PTO (74)",
    coalesce(d.adjusted_amount, 0) as "Other - Adjustments (79)",
    coalesce(e.additional_comp, 0) as "Other - Addtl Comp (79)",
    round (
      case -- paid spiffs deducted only if base guarantee
        when a.total_earned >= a.guarantee then
          a.unit_pay - coalesce(a.draw, 0)
        else
          (a.guarantee * coalesce(g.guarantee_multiplier, 1)  * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) - coalesce(c.spiffs, 0)
      end, 2) as "Unit Commission less draw (79)",
      a.fi_pay as "F&I Comm (79A)",   
      round( 
        case -- paid spiffs deducted only if base guarantee
          when a.total_earned >= a.guarantee then
            a.total_earned - coalesce(a.draw, 0) + coalesce(d.adjusted_amount, 0) + coalesce(e.additional_comp, 0)
          else
            (a.guarantee * coalesce(g.guarantee_multiplier, 1)  * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) 
                - coalesce(c.spiffs, 0) + coalesce(e.additional_comp, 0) + coalesce(d.adjusted_amount, 0)
          end, 2) as "Total Month End Payout"      
  from sls.consultant_payroll a
  left join sls.pto_intervals b on a.employee_number = b.employee_number
    and a.year_month = b.year_month
  left join sls.paid_by_month c on a.employee_number = c.employee_number
    and a.year_month = c.year_month
  left join (
    select employee_number, sum(amount) as adjusted_amount
    from sls.payroll_adjustments
    where year_month =  (select year_month from open_month)
    group by employee_number) d on a.employee_number = d.employee_number
  left join (
    select employee_number, sum(amount) as additional_comp
    from sls.additional_comp
    where thru_date > (
      select first_of_month
      from sls.months
      where open_closed = 'open')
    group by employee_number) e on a.employee_number = e.employee_number  
  left join sls.personnel f on a.employee_number = f.employee_number
  left join ( -- if this is the first month of employment, multiplier = days worked/days in month
    select a.employee_number, 
      round(wd_of_month_remaining::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as guarantee_multiplier 
    from sls.personnel a
    inner join dds.dim_date b on a.start_date = b.the_date
      and b.year_month = (select year_month from open_month)) g on f.employee_number = g.employee_number      
  left join ( -- if employee termed mid month, multiplier elapsed wd in month at term date/work days in month
    select a.employee_number, 
      round(wd_of_month_elapsed::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as term_guarantee_multiplier
    from sls.personnel a
    inner join dds.dim_date b on a.end_date = b.the_date
      and b.year_month = (select year_month from open_month) ) h on f.employee_number = h.employee_number            
  where a.year_month = (select year_month from open_month);
--     and a.last_name = 'rumen'
--   and f.store_code = _store;

end
$$;

select * from wtf1;

-- 
select a.consultant, a.employee_number, "Total Month End Payout", b.due, ("Total Month End Payout" - b.due)::integer
from wtf1 a
left join wtf b on a.employee_number = b.id
where ("Total Month End Payout" - b.due) <> 0    
order by consultant