﻿
do
$$
declare
  _store citext := 'RY1';
begin
  drop table if exists wtf;
  create temp table wtf as 
  with
    open_month as (
      select 201906 as year_month), -----------------------------------------
    store_count as (
      select -- store's units & other store's units sold by store's consultants 
        -- eg RY1 units sold by RY1 consultants & RY2 units sold by RY1 consultants
        sum(unit_count) - sum(unit_count) filter (where deal_store <> sc_store) as total_units,
        sum(unit_count) filter (where deal_store <> sc_store) as total_other_units
      from (  
        select a.psc_employee_number, a.store_code as deal_store, b.store_code as sc_store,
          case
            when ssc_last_name = 'none' then a.unit_count
            else 0.5 * a.unit_count
          end as unit_count
        from sls.deals_by_month a
        inner join sls.personnel b on a.psc_employee_number = b.employee_number
          and b.employee_number <> 'HSE'    
        inner join sls.consultant_payroll c on b.employee_number = c.employee_number  
          and a.year_month = c.year_month    
        where a.year_month = (select year_month from open_month)
        union all 
        select a.ssc_employee_number, a.store_code as deal_store, b.store_code as sc_store,
          0.5 * a.unit_count as unit_count
        from sls.deals_by_month a
        inner join sls.personnel b on a.ssc_employee_number = b.employee_number
          and b.employee_number <> 'HSE'     
        inner join sls.consultant_payroll c on b.employee_number = c.employee_number   
          and a.year_month = c.year_month    
        where a.year_month =(select year_month from open_month)
          and a.ssc_last_name <> 'none') x
      where deal_store = _store)                 
    
  select a.employee_number as id, a.last_name || ', ' || a.first_name as consultant, a.payplan as plan, a.unit_count as units,
    a.unit_pay, a.fi_pay, a.fi_gross, a.chargebacks, a.fi_total, a.pto_pay, coalesce(a.pto_hours, 0) as pto_hours,
    coalesce(a.pto_rate, b.pto_rate) as pto_rate, coalesce(c.spiffs, 0) as spiffs,
    a.total_earned, a.draw, 
    round(a.guarantee * coalesce(g.guarantee_multiplier, 1), 2) as guarantee, 
    coalesce(d.adjusted_amount, 0) as adjusted_amount,
    coalesce(e.additional_comp, 0) as additional_comp,
    round (
      case -- paid spiffs deducted only if base guarantee
        when a.total_earned >= a.guarantee then
          a.total_earned - coalesce(a.draw, 0) + coalesce(d.adjusted_amount, 0) + coalesce(e.additional_comp, 0)
        else
          (a.guarantee * coalesce(g.guarantee_multiplier, 1)) - coalesce(a.draw, 0) 
              - coalesce(c.spiffs, 0) + coalesce(e.additional_comp, 0) + coalesce(d.adjusted_amount, 0)
        end, 2) as due,
    case when a.months_employed < 0 then 0 else a.months_employed end as months_employed,
     "3 month rolling_avg" as three_month_average, f.start_date as hire_date
  from sls.consultant_payroll a
  left join sls.pto_intervals b on a.employee_number = b.employee_number
    and a.year_month = b.year_month
  left join sls.paid_by_month c on a.employee_number = c.employee_number
    and a.year_month = c.year_month
  left join (
    select employee_number, sum(amount) as adjusted_amount
    from sls.payroll_adjustments
    where year_month =  (select year_month from open_month)
    group by employee_number) d on a.employee_number = d.employee_number
  left join (
    select employee_number, sum(amount) as additional_comp
    from sls.additional_comp
    where thru_date > (
      select first_of_month
      from sls.months
      where open_closed = 'open')
    group by employee_number) e on a.employee_number = e.employee_number  
  left join sls.personnel f on a.employee_number = f.employee_number
  left join ( -- if this is the first month of employment, multiplier = days worked/days in month
    select a.employee_number, 
      round(((select max(wd_in_month)::numeric from dds.dim_date where year_month = (select year_month from open_month)) 
        - (select wd_of_month_elapsed  from dds.dim_date where the_date = a.start_date)::numeric)/
                  (select max(wd_in_month)::numeric from dds.dim_date where year_month = b.year_month), 4) as guarantee_multiplier
    from sls.personnel a
    inner join dds.dim_date b on a.start_date = b.the_date
      and b.year_month = (select year_month from open_month)) g on f.employee_number = g.employee_number        
  where a.year_month = (select year_month from open_month)
  and f.store_code = _store;
end
$$; select * from wtf;