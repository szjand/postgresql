﻿-- kim (w/o json): based on sls.consultant_payroll, which is only updated for open month
-- 10/5 need to break out pulse as a separate value 79B
do
$$
declare
  _year_month integer := 201810;
  _store citext := 'RY1';
begin
  drop table if exists wtf;
  create temp table wtf as
    select a.last_name || ', ' || a.first_name as consultant, a.employee_number, 
      a.pto_pay as "PTO (74)",
      coalesce(d.adjusted_amount, 0) as "Other - Adjustments (79)",
      coalesce(e.additional_comp, 0) as "Other - Addtl Comp (79)",
      round (
        case -- paid spiffs deducted only if base guarantee
          when a.total_earned >= a.guarantee then
--             a.unit_pay + a.pulse - coalesce(a.draw, 0)
            a.unit_pay - coalesce(a.draw, 0)
          else
            (a.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) - coalesce(c.spiffs, 0)
        end, 2) as "Unit Commission less draw (79)",
        a.fi_pay as "F&I Comm (79A)",   
        a.pulse as "Pulse (79B)",
        round( 
          case -- paid spiffs deducted only if base guarantee
            when a.total_earned >= a.guarantee then
              a.total_earned - coalesce(a.draw, 0) + coalesce(d.adjusted_amount, 0) + coalesce(e.additional_comp, 0)
            else
              (a.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) 
                  - coalesce(c.spiffs, 0) + coalesce(e.additional_comp, 0) + coalesce(d.adjusted_amount, 0)
            end, 2) as "Total Month End Payout"      
    from sls.consultant_payroll a
    left join sls.pto_intervals b on a.employee_number = b.employee_number
      and a.year_month = b.year_month
    left join sls.paid_by_month c on a.employee_number = c.employee_number
      and a.year_month = c.year_month
    left join (
      select employee_number, sum(amount) as adjusted_amount
      from sls.payroll_adjustments
      where year_month =  _year_month
      group by employee_number) d on a.employee_number = d.employee_number
    left join (
      select employee_number, sum(amount) as additional_comp
      from sls.additional_comp
      where thru_date > (
        select first_of_month
        from sls.months
        where open_closed = 'open')
      group by employee_number) e on a.employee_number = e.employee_number  
    left join sls.personnel f on a.employee_number = f.employee_number
  left join ( -- if this is the first month of employment, multiplier = days worked/days in month
    select a.employee_number, 
      round(wd_of_month_remaining::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as guarantee_multiplier 
    from sls.personnel a
    inner join dds.dim_date b on a.start_date = b.the_date
      and b.year_month = _year_month) g on f.employee_number = g.employee_number      
  left join ( -- if employee termed mid month, multiplier elapsed wd in month at term date/work days in month
    select a.employee_number, 
      round(wd_of_month_elapsed::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as term_guarantee_multiplier
    from sls.personnel a
    inner join dds.dim_date b on a.end_date = b.the_date
      and b.year_month = _year_month) h on f.employee_number = h.employee_number         
    where a.year_month = _year_month
    and f.store_code = _store;  
--     select a.last_name || ', ' || a.first_name as consultant, a.employee_number, 
--       a.pto_pay as "PTO (74)",
--       coalesce(d.adjusted_amount, 0) as "Other - Adjustments (79)",
--       coalesce(e.additional_comp, 0) as "Other - Addtl Comp (79)",
--       round (
--         case -- paid spiffs deducted only if base guarantee
--           when a.total_earned >= a.guarantee then
--             a.unit_pay - coalesce(a.draw, 0)
--           else
--             (a.guarantee * coalesce(g.guarantee_multiplier, 1)) - coalesce(a.draw, 0) - coalesce(c.spiffs, 0)
--         end, 2) as "Unit Commission less draw (79)",
--         a.fi_pay as "F&I Comm (79A)",   
--         round( 
--           case -- paid spiffs deducted only if base guarantee
--             when a.total_earned >= a.guarantee then
--               a.total_earned - coalesce(a.draw, 0) + coalesce(d.adjusted_amount, 0) + coalesce(e.additional_comp, 0)
--             else
--               (a.guarantee * coalesce(g.guarantee_multiplier, 1)) - coalesce(a.draw, 0) 
--                   - coalesce(c.spiffs, 0) + coalesce(e.additional_comp, 0) + coalesce(d.adjusted_amount, 0)
--             end, 2) as "Total Month End Payout"      
--     from sls.consultant_payroll a
--     left join sls.pto_intervals b on a.employee_number = b.employee_number
--       and a.year_month = b.year_month
--     left join sls.paid_by_month c on a.employee_number = c.employee_number
--       and a.year_month = c.year_month
--     left join (
--       select employee_number, sum(amount) as adjusted_amount
--       from sls.payroll_adjustments
--       where year_month =  _year_month
--       group by employee_number) d on a.employee_number = d.employee_number
--     left join (
--       select employee_number, sum(amount) as additional_comp
--       from sls.additional_comp
--       where thru_date > (
--         select first_of_month
--         from sls.months
--         where year_month = _year_month)
--       group by employee_number) e on a.employee_number = e.employee_number  
--     left join sls.personnel f on a.employee_number = f.employee_number
--     left join ( -- if this is the first month of employment, multiplier = days worked/days in month
--       select a.employee_number, 
--         round(((select max(wd_in_month)::numeric from dds.dim_date where year_month = _year_month) 
--           - (select wd_of_month_elapsed  from dds.dim_date where the_date = a.start_date)::numeric)/
--                     (select max(wd_in_month)::numeric from dds.dim_date where year_month = b.year_month), 4) as guarantee_multiplier
--       from sls.personnel a
--       inner join dds.dim_date b on a.start_date = b.the_date
--         and b.year_month = _year_month) g on f.employee_number = g.employee_number        
--     where a.year_month = _year_month
--     and f.store_code = _store;
end
$$;  select * from wtf;  