﻿
-----------------------------------------------------------
--< 2021

-- mail to nick shireck and mike longoria
-----------------------------------------------------------
do
$$
declare 
  _year_month integer := 202111;  -- month just passed ------------------------------------------------
  _month_of_year integer := (
    select distinct month_of_year
    from dds.dim_date
    where year_month = _year_month);
    
begin
drop table if exists monthly;
create temp table monthly as
select a.*, -- b.first_full_month_emp,
  -- (select min(year_month) from sls.deals_by_month where psc_employee_number = b.employee_number or ssc_employee_number = b.employee_number),
  case
    when b.first_full_month_emp < 202111 then _month_of_year -- month just passed ----------------------
    when a.psc_last_name = 'HSE' then _month_of_year
    when b.first_full_month_emp = 202101 then 11 -- month just passed, each month uncomment another line, and increment the count, this is always the top row
    when b.first_full_month_emp = 202102 then 10 -- increment the integer and the year_month
    when b.first_full_month_emp = 202103 then 9
    when b.first_full_month_emp = 202104 then 8
    when b.first_full_month_emp = 202105 then 7
    when b.first_full_month_emp = 202106 then 6
    when b.first_full_month_emp = 202107 then 5
    when b.first_full_month_emp = 202108 then 4 
    when b.first_full_month_emp = 202109 then 3 
    when b.first_full_month_emp = 202010 then 2 
    when b.first_full_month_emp = 202011 then 1 
--     when b.first_full_month_emp = 202012 then 1
  end as months
from (
  select psc_last_name, psc_first_name, psc_employee_number, 
    sum(jan) as jan, sum(feb) as feb, sum(mar) as mar, sum(apr) as apr, sum(may) as may,
    sum(jun) as jun, sum(jul) as jul, sum(aug) as aug, sum(sep) as sep,
    sum(oct) as oct, sum(nov) as nov, sum(dec) as dec,
    sum(jan+feb+mar+apr+may+jun+jul+aug+sep+oct+nov+dec) as ytd_total
    
  from (  
    select  psc_last_name, psc_first_name, psc_employee_number, 
      coalesce(sum(unit_count) filter (where year_month = 202101), 0) as jan,
      coalesce(sum(unit_count) filter (where year_month = 202102), 0) as feb,
      coalesce(sum(unit_count) filter (where year_month = 202103), 0) as mar,
      coalesce(sum(unit_count) filter (where year_month = 202104), 0) as apr,
      coalesce(sum(unit_count) filter (where year_month = 202105), 0) as may,
      coalesce(sum(unit_count) filter (where year_month = 202106), 0) as jun,
      coalesce(sum(unit_count) filter (where year_month = 202107), 0) as jul,
      coalesce(sum(unit_count) filter (where year_month = 202108), 0) as aug,
      coalesce(sum(unit_count) filter (where year_month = 202109), 0) as sep,
      coalesce(sum(unit_count) filter (where year_month = 202110), 0) as oct,
      coalesce(sum(unit_count) filter (where year_month = 202111), 0) as nov,
      coalesce(sum(unit_count) filter (where year_month = 202112), 0) as dec
    from sls.deals_by_month
    where ssc_last_name = 'none' and year_month < 202112 -- month just passed + 1 --------------------------------------
    group by psc_last_name, psc_first_name, psc_employee_number  
    union all
    select  psc_last_name, psc_first_name, psc_employee_number, 
      coalesce(sum(unit_count*.5) filter (where year_month = 202101), 0) as jan,
      coalesce(sum(unit_count*.5) filter (where year_month = 202102), 0) as feb,
      coalesce(sum(unit_count*.5) filter (where year_month = 202103), 0) as mar,
      coalesce(sum(unit_count*.5) filter (where year_month = 202104), 0) as apr,
      coalesce(sum(unit_count*.5) filter (where year_month = 202105), 0) as may,
      coalesce(sum(unit_count*.5) filter (where year_month = 202106), 0) as jun,
      coalesce(sum(unit_count*.5) filter (where year_month = 202107), 0) as jul,
      coalesce(sum(unit_count*.5) filter (where year_month = 202108), 0) as aug,
      coalesce(sum(unit_count*.5) filter (where year_month = 202109), 0) as sep,
      coalesce(sum(unit_count*.5) filter (where year_month = 20210), 0) as oct,
      coalesce(sum(unit_count*.5) filter (where year_month = 202111), 0) as nov,
      coalesce(sum(unit_count*.5) filter (where year_month = 202112), 0) as dec
    from sls.deals_by_month
    where ssc_last_name <> 'none' and year_month < 202112 -- month just passed + 1 ---------------------------------------
    group by psc_last_name, psc_first_name, psc_employee_number  
    union all
    select  ssc_last_name, ssc_first_name, ssc_employee_number, 
      coalesce(sum(unit_count*.5) filter (where year_month = 202101), 0) as jan,
      coalesce(sum(unit_count*.5) filter (where year_month = 202102), 0) as feb,
      coalesce(sum(unit_count*.5) filter (where year_month = 202103), 0) as mar,
      coalesce(sum(unit_count*.5) filter (where year_month = 202104), 0) as apr,
      coalesce(sum(unit_count*.5) filter (where year_month = 202105), 0) as may,
      coalesce(sum(unit_count*.5) filter (where year_month = 202106), 0) as jun,
      coalesce(sum(unit_count*.5) filter (where year_month = 202107), 0) as jul,
      coalesce(sum(unit_count*.5) filter (where year_month = 202108), 0) as aug,
      coalesce(sum(unit_count*.5) filter (where year_month = 202109), 0) as sep,
      coalesce(sum(unit_count*.5) filter (where year_month = 202110), 0) as oct,
      coalesce(sum(unit_count*.5) filter (where year_month = 202111), 0) as nov,
      coalesce(sum(unit_count*.5) filter (where year_month = 202112), 0) as dec
    from sls.deals_by_month
    where ssc_last_name <> 'none' and year_month < 202112 -- month just passed + 1 ------------------------------------------
    group by ssc_last_name, ssc_first_name, ssc_employee_number) x
  group by psc_last_name, psc_first_name, psc_employee_number) a 
inner join sls.personnel b on a.psc_employee_number = b.employee_number  
where b.store_code in ('RY2') ------------------------------------------------------- nick wants it for each store 
  and b.end_date > '10/31/2021' ----------------------------------------------------- end of previous month
  and a.psc_last_name not in ('seay','dockendorf','bedney','kobayashi-dyer','brooks','longoria','haley')
order by psc_last_name, psc_first_name;
end
$$;

select * from monthly;


-- drop table if exists ry2;
-- create temp table ry2 as
select -- sum(jan) over (),
  psc_last_name as last_name, psc_first_name as first_name, 
  jan, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, --dec,
  --  nov, ------------------------------------------
  ytd_total,
  round(ytd_total/coalesce(months, 1), 2) as monthly_avg,
--   (4 * round(ytd_total/months, 2)) + ytd_total as pacing_for,
  round(ytd_total *
    (select wd_in_year from dds.working_days where the_date = current_date and department = 'sales') * 1.0
    /
    (select wd_of_year_elapsed from dds.working_days where the_date = current_date - 1 and department = 'sales'), 1)  as pacing_for  
from monthly
where psc_last_name not in ('seay','dockendorf','bedney','kobayashi-dyer','brooks','haley')
-- order by last_name
order by monthly_avg desc

/*
-- 9/4/19 nick wants each consultants rolling 3 month avg

select psc_last_name as last_name, psc_first_name as first_name, jun, jul, aug, round((jun + jul + aug)/3, 1) as avg
from monthly
where psc_last_name <> 'HSE'
*/

-----------------------------------------------------------
--/> 2021
-----------------------------------------------------------

-----------------------------------------------------------
--< 2020
-----------------------------------------------------------
do
$$
declare 
  _year_month integer := 202012;  -- month just passed ------------------------------------------------
  _month_of_year integer := (
    select distinct month_of_year
    from dds.dim_date
    where year_month = _year_month);
    
begin
drop table if exists monthly;
create temp table monthly as
select a.*, -- b.first_full_month_emp,
  -- (select min(year_month) from sls.deals_by_month where psc_employee_number = b.employee_number or ssc_employee_number = b.employee_number),
  case
    when b.first_full_month_emp < 202012 then _month_of_year -- month just passed ----------------------
    when a.psc_last_name = 'HSE' then _month_of_year
    when b.first_full_month_emp = 202001 then 12
    when b.first_full_month_emp = 202002 then 11
    when b.first_full_month_emp = 202003 then 10
    when b.first_full_month_emp = 202004 then 9
    when b.first_full_month_emp = 202005 then 8
    when b.first_full_month_emp = 202006 then 7
    when b.first_full_month_emp = 202007 then 6
    when b.first_full_month_emp = 202008 then 5 
    when b.first_full_month_emp = 202009 then 4 
    when b.first_full_month_emp = 202010 then 3 
    when b.first_full_month_emp = 202011 then 2 
    when b.first_full_month_emp = 202012 then 1 -- month just passed --------------------------add this for each new month, on 12/1/20 i added 202011
  end as months
from (
  select psc_last_name, psc_first_name, psc_employee_number, 
    sum(jan) as jan, sum(feb) as feb, sum(mar) as mar, sum(apr) as apr, sum(may) as may,
    sum(jun) as jun, sum(jul) as jul, sum(aug) as aug, sum(sep) as sep,
    sum(oct) as oct, sum(nov) as nov, sum(dec) as dec,
    sum(jan+feb+mar+apr+may+jun+jul+aug+sep+oct+nov+dec) as ytd_total
    
  from (  
    select  psc_last_name, psc_first_name, psc_employee_number, 
      coalesce(sum(unit_count) filter (where year_month = 202001), 0) as jan,
      coalesce(sum(unit_count) filter (where year_month = 202002), 0) as feb,
      coalesce(sum(unit_count) filter (where year_month = 202003), 0) as mar,
      coalesce(sum(unit_count) filter (where year_month = 202004), 0) as apr,
      coalesce(sum(unit_count) filter (where year_month = 202005), 0) as may,
      coalesce(sum(unit_count) filter (where year_month = 202006), 0) as jun,
      coalesce(sum(unit_count) filter (where year_month = 202007), 0) as jul,
      coalesce(sum(unit_count) filter (where year_month = 202008), 0) as aug,
      coalesce(sum(unit_count) filter (where year_month = 202009), 0) as sep,
      coalesce(sum(unit_count) filter (where year_month = 202010), 0) as oct,
      coalesce(sum(unit_count) filter (where year_month = 202011), 0) as nov,
      coalesce(sum(unit_count) filter (where year_month = 202012), 0) as dec
    from sls.deals_by_month
    where ssc_last_name = 'none' and year_month < 202101 -- month just passed + 1 --------------------------------------
    group by psc_last_name, psc_first_name, psc_employee_number  
    union all
    select  psc_last_name, psc_first_name, psc_employee_number, 
      coalesce(sum(unit_count*.5) filter (where year_month = 202001), 0) as jan,
      coalesce(sum(unit_count*.5) filter (where year_month = 202002), 0) as feb,
      coalesce(sum(unit_count*.5) filter (where year_month = 202003), 0) as mar,
      coalesce(sum(unit_count*.5) filter (where year_month = 202004), 0) as apr,
      coalesce(sum(unit_count*.5) filter (where year_month = 202005), 0) as may,
      coalesce(sum(unit_count*.5) filter (where year_month = 202006), 0) as jun,
      coalesce(sum(unit_count*.5) filter (where year_month = 202007), 0) as jul,
      coalesce(sum(unit_count*.5) filter (where year_month = 202008), 0) as aug,
      coalesce(sum(unit_count*.5) filter (where year_month = 202009), 0) as sep,
      coalesce(sum(unit_count*.5) filter (where year_month = 202010), 0) as oct,
      coalesce(sum(unit_count*.5) filter (where year_month = 202011), 0) as nov,
      coalesce(sum(unit_count*.5) filter (where year_month = 202012), 0) as dec
    from sls.deals_by_month
    where ssc_last_name <> 'none' and year_month < 202101 -- month just passed + 1 ---------------------------------------
    group by psc_last_name, psc_first_name, psc_employee_number  
    union all
    select  ssc_last_name, ssc_first_name, ssc_employee_number, 
      coalesce(sum(unit_count*.5) filter (where year_month = 202001), 0) as jan,
      coalesce(sum(unit_count*.5) filter (where year_month = 202002), 0) as feb,
      coalesce(sum(unit_count*.5) filter (where year_month = 202003), 0) as mar,
      coalesce(sum(unit_count*.5) filter (where year_month = 202004), 0) as apr,
      coalesce(sum(unit_count*.5) filter (where year_month = 202005), 0) as may,
      coalesce(sum(unit_count*.5) filter (where year_month = 202006), 0) as jun,
      coalesce(sum(unit_count*.5) filter (where year_month = 202007), 0) as jul,
      coalesce(sum(unit_count*.5) filter (where year_month = 202008), 0) as aug,
      coalesce(sum(unit_count*.5) filter (where year_month = 202009), 0) as sep,
      coalesce(sum(unit_count*.5) filter (where year_month = 202010), 0) as oct,
      coalesce(sum(unit_count*.5) filter (where year_month = 202011), 0) as nov,
      coalesce(sum(unit_count*.5) filter (where year_month = 202012), 0) as dec
    from sls.deals_by_month
    where ssc_last_name <> 'none' and year_month < 202101 -- month just passed + 1 ------------------------------------------
    group by ssc_last_name, ssc_first_name, ssc_employee_number) x
  group by psc_last_name, psc_first_name, psc_employee_number) a 
inner join sls.personnel b on a.psc_employee_number = b.employee_number  
where b.store_code in ('RY2') ------------------------------------------------------- nick wants it for each store 
  and b.end_date > '11/30/2020' -------------------------------------------------------------------
  and a.psc_last_name not in ('seay','dockendorf','bedney','kobayashi-dyer','brooks','longoria','haley')
order by psc_last_name, psc_first_name;
end
$$;

select * from monthly;


-- drop table if exists ry2;
-- create temp table ry2 as
select -- sum(jan) over (),
  psc_last_name as last_name, psc_first_name as first_name, 
  jan, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec,
  --  nov, ------------------------------------------
  ytd_total,
  round(ytd_total/coalesce(months, 1), 2) as monthly_avg,
--   (4 * round(ytd_total/months, 2)) + ytd_total as pacing_for,
  round(ytd_total *
    (select wd_in_year from dds.working_days where the_date = current_date and department = 'sales') * 1.0
    /
    (select wd_of_year_elapsed from dds.working_days where the_date = current_date - 1 and department = 'sales'), 1)  as pacing_for  
from monthly
where psc_last_name not in ('seay','dockendorf','bedney','kobayashi-dyer','brooks','haley')
-- order by last_name
order by monthly_avg desc

/*
-- 9/4/19 nick wants each consultants rolling 3 month avg

select psc_last_name as last_name, psc_first_name as first_name, jun, jul, aug, round((jun + jul + aug)/3, 1) as avg
from monthly
where psc_last_name <> 'HSE'
*/

-----------------------------------------------------------
--/> 2020
-----------------------------------------------------------
-----------------------------------------------------------
--< 2019
-----------------------------------------------------------
do
$$
declare 
  _year_month integer := 201912;  -- month just passed ------------------------------------------------
  _month_of_year integer := (
    select distinct month_of_year
    from dds.dim_date
    where year_month = _year_month);
    
begin
drop table if exists monthly;
create temp table monthly as
select a.*, -- b.first_full_month_emp,
  -- (select min(year_month) from sls.deals_by_month where psc_employee_number = b.employee_number or ssc_employee_number = b.employee_number),
  case
    when b.first_full_month_emp < 201912 then _month_of_year -- month just passed ----------------------
    when a.psc_last_name = 'HSE' then _month_of_year
    when b.first_full_month_emp = 201902 then 11
    when b.first_full_month_emp = 201903 then 10
    when b.first_full_month_emp = 201904 then 9
    when b.first_full_month_emp = 201905 then 8  
    when b.first_full_month_emp = 201906 then 7 
    when b.first_full_month_emp = 201907 then 6  
    when b.first_full_month_emp = 201908 then 5  
    when b.first_full_month_emp = 201909 then 4 
    when b.first_full_month_emp = 201910 then 3  
    when b.first_full_month_emp = 201911 then 2  
    when b.first_full_month_emp = 201912 then 1  -- month just passed ----------------------------------
  end as months
from (
  select psc_last_name, psc_first_name, psc_employee_number, 
    sum(jan) as jan, sum(feb) as feb, sum(mar) as mar, sum(apr) as apr, sum(may) as may,
    sum(jun) as jun, sum(jul) as jul, sum(aug) as aug, sum(sep) as sep,
    sum(oct) as oct, sum(nov) as nov, sum(dec) as dec,
    sum(jan+feb+mar+apr+may+jun+jul+aug+sep+oct+nov+dec) as ytd_total
    
  from (  
    select  psc_last_name, psc_first_name, psc_employee_number, 
      coalesce(sum(unit_count) filter (where year_month = 201901), 0) as jan,
      coalesce(sum(unit_count) filter (where year_month = 201902), 0) as feb,
      coalesce(sum(unit_count) filter (where year_month = 201903), 0) as mar,
      coalesce(sum(unit_count) filter (where year_month = 201904), 0) as apr,
      coalesce(sum(unit_count) filter (where year_month = 201905), 0) as may,
      coalesce(sum(unit_count) filter (where year_month = 201906), 0) as jun,
      coalesce(sum(unit_count) filter (where year_month = 201907), 0) as jul,
      coalesce(sum(unit_count) filter (where year_month = 201908), 0) as aug,
      coalesce(sum(unit_count) filter (where year_month = 201909), 0) as sep,
      coalesce(sum(unit_count) filter (where year_month = 201910), 0) as oct,
      coalesce(sum(unit_count) filter (where year_month = 201911), 0) as nov,
      coalesce(sum(unit_count) filter (where year_month = 201912), 0) as dec
    from sls.deals_by_month
    where ssc_last_name = 'none' and year_month < 202001 -- month just passed + 1 --------------------------------------
    group by psc_last_name, psc_first_name, psc_employee_number  
    union all
    select  psc_last_name, psc_first_name, psc_employee_number, 
      coalesce(sum(unit_count*.5) filter (where year_month = 201901), 0) as jan,
      coalesce(sum(unit_count*.5) filter (where year_month = 201902), 0) as feb,
      coalesce(sum(unit_count*.5) filter (where year_month = 201903), 0) as mar,
      coalesce(sum(unit_count*.5) filter (where year_month = 201904), 0) as apr,
      coalesce(sum(unit_count*.5) filter (where year_month = 201905), 0) as may,
      coalesce(sum(unit_count*.5) filter (where year_month = 201906), 0) as jun,
      coalesce(sum(unit_count*.5) filter (where year_month = 201907), 0) as jul,
      coalesce(sum(unit_count*.5) filter (where year_month = 201908), 0) as aug,
      coalesce(sum(unit_count*.5) filter (where year_month = 201909), 0) as sep,
      coalesce(sum(unit_count*.5) filter (where year_month = 201910), 0) as oct,
      coalesce(sum(unit_count*.5) filter (where year_month = 201911), 0) as nov,
      coalesce(sum(unit_count*.5) filter (where year_month = 201912), 0) as dec
    from sls.deals_by_month
    where ssc_last_name <> 'none' and year_month < 202001 -- month just passed + 1 ---------------------------------------
    group by psc_last_name, psc_first_name, psc_employee_number  
    union all
    select  ssc_last_name, ssc_first_name, ssc_employee_number, 
      coalesce(sum(unit_count*.5) filter (where year_month = 201901), 0) as jan,
      coalesce(sum(unit_count*.5) filter (where year_month = 201902), 0) as feb,
      coalesce(sum(unit_count*.5) filter (where year_month = 201903), 0) as mar,
      coalesce(sum(unit_count*.5) filter (where year_month = 201904), 0) as apr,
      coalesce(sum(unit_count*.5) filter (where year_month = 201905), 0) as may,
      coalesce(sum(unit_count*.5) filter (where year_month = 201906), 0) as jun,
      coalesce(sum(unit_count*.5) filter (where year_month = 201907), 0) as jul,
      coalesce(sum(unit_count*.5) filter (where year_month = 201908), 0) as aug,
      coalesce(sum(unit_count*.5) filter (where year_month = 201909), 0) as sep,
      coalesce(sum(unit_count*.5) filter (where year_month = 201910), 0) as oct,
      coalesce(sum(unit_count*.5) filter (where year_month = 201911), 0) as nov,
      coalesce(sum(unit_count*.5) filter (where year_month = 201912), 0) as dec
    from sls.deals_by_month
    where ssc_last_name <> 'none' and year_month < 202001 -- month just passed + 1 ------------------------------------------
    group by ssc_last_name, ssc_first_name, ssc_employee_number) x
  group by psc_last_name, psc_first_name, psc_employee_number) a 
inner join sls.personnel b on a.psc_employee_number = b.employee_number  
where b.store_code in ('RY1') ------------------------------------------------------- nick wants it for each store 
  and b.end_date > '12/31/2019' -------------------------------------------------------------------
  and a.psc_last_name not in ('seay','dockendorf','bedney','kobayashi-dyer','brooks','longoria','monreal')
order by psc_last_name, psc_first_name;
end
$$;

select * from monthly;

select * from monthly where ytd_total > 200  and psc_last_name <> 'HSE'

-- drop table if exists ry2;
-- create temp table ry2 as
select -- sum(jan) over (),
  psc_last_name as last_name, psc_first_name as first_name, 
  jan, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, ------------------------------------------
  ytd_total,
  round(ytd_total/coalesce(months, 1), 2) as monthly_avg,
--   (4 * round(ytd_total/months, 2)) + ytd_total as pacing_for,
  round(ytd_total *
    (select wd_in_year from dds.working_days where the_date = current_date and department = 'sales') * 1.0
    /
    (select wd_of_year_elapsed from dds.working_days where the_date = current_date and department = 'sales'), 1)  as pacing_for  
from monthly
where psc_last_name not in ('seay','dockendorf','bedney','kobayashi-dyer','brooks')
-- order by last_name
order by monthly_avg desc

/*
-- 9/4/19 nick wants each consultants rolling 3 month avg

select psc_last_name as last_name, psc_first_name as first_name, jun, jul, aug, round((jun + jul + aug)/3, 1) as avg
from monthly
where psc_last_name <> 'HSE'
*/

-----------------------------------------------------------
--/> 2019
-----------------------------------------------------------
-- select * from sls.personnel where employee_number in ('169542','186531')
/*

-----------------------------------------------------------
--< 2018
-----------------------------------------------------------
drop table if exists monthly;
create temp table monthly as
select a.*,
  case
    when jan <> 0 then 11
    when jan = 0 and feb <> 0 then 10
    when jan+feb = 0 and mar <> 0 then 9
    when jan+feb+mar = 0 and apr <> 0 then 8
    when jan+feb+mar+apr = 0 and may <> 0 then 7
    when jan+feb+mar+apr+may = 0 and jun <> 0 then 6
    when jan+feb+mar+apr+may+jun = 0 and jul <> 0 then 5
    when jan+feb+mar+apr+may+jun+jul = 0 and aug <> 0 then 4
    when jan+feb+mar+apr+may+jun+jul+aug = 0 and sep <> 0 then 3
    when jan+feb+mar+apr+may+jun+jul+aug+sep = 0 and oct <> 0 then 2
    when jan+feb+mar+apr+may+jun+jul+aug+sep+oct = 0 and nov <> 0 then 1
  end as months
from (
  select psc_last_name, psc_first_name, psc_employee_number, 
    sum(jan) as jan, sum(feb) as feb, sum(mar) as mar, sum(apr) as apr, sum(may) as may,
    sum(jun) as jun, sum(jul) as jul, sum(aug) as aug, sum(sep) as sep,
    sum(oct) as oct, sum(nov) as nov,
    sum(jan+feb+mar+apr+may+jun+jul+aug+sep+oct+nov) as ytd_total
    
  from (  
    select  psc_last_name, psc_first_name, psc_employee_number, 
      coalesce(sum(unit_count) filter (where year_month = 201801), 0) as jan,
      coalesce(sum(unit_count) filter (where year_month = 201802), 0) as feb,
      coalesce(sum(unit_count) filter (where year_month = 201803), 0) as mar,
      coalesce(sum(unit_count) filter (where year_month = 201804), 0) as apr,
      coalesce(sum(unit_count) filter (where year_month = 201805), 0) as may,
      coalesce(sum(unit_count) filter (where year_month = 201806), 0) as jun,
      coalesce(sum(unit_count) filter (where year_month = 201807), 0) as jul,
      coalesce(sum(unit_count) filter (where year_month = 201808), 0) as aug,
      coalesce(sum(unit_count) filter (where year_month = 201809), 0) as sep,
      coalesce(sum(unit_count) filter (where year_month = 201810), 0) as oct,
      coalesce(sum(unit_count) filter (where year_month = 201811), 0) as nov
    from sls.deals_by_month
    where ssc_last_name = 'none'
--       and store_code = 'RY1'
    group by psc_last_name, psc_first_name, psc_employee_number  
    union all
    select  psc_last_name, psc_first_name, psc_employee_number, 
      coalesce(sum(unit_count*.5) filter (where year_month = 201801), 0) as jan,
      coalesce(sum(unit_count*.5) filter (where year_month = 201802), 0) as feb,
      coalesce(sum(unit_count*.5) filter (where year_month = 201803), 0) as mar,
      coalesce(sum(unit_count*.5) filter (where year_month = 201804), 0) as apr,
      coalesce(sum(unit_count*.5) filter (where year_month = 201805), 0) as may,
      coalesce(sum(unit_count*.5) filter (where year_month = 201806), 0) as jun,
      coalesce(sum(unit_count*.5) filter (where year_month = 201807), 0) as jul,
      coalesce(sum(unit_count*.5) filter (where year_month = 201808), 0) as aug,
      coalesce(sum(unit_count*.5) filter (where year_month = 201809), 0) as sep,
      coalesce(sum(unit_count*.5) filter (where year_month = 201810), 0) as oct,
      coalesce(sum(unit_count*.5) filter (where year_month = 201811), 0) as nov
    from sls.deals_by_month
    where ssc_last_name <> 'none'
--       and store_code = 'RY1'
    group by psc_last_name, psc_first_name, psc_employee_number  
    union all
    select  ssc_last_name, ssc_first_name, ssc_employee_number, 
      coalesce(sum(unit_count*.5) filter (where year_month = 201801), 0) as jan,
      coalesce(sum(unit_count*.5) filter (where year_month = 201802), 0) as feb,
      coalesce(sum(unit_count*.5) filter (where year_month = 201803), 0) as mar,
      coalesce(sum(unit_count*.5) filter (where year_month = 201804), 0) as apr,
      coalesce(sum(unit_count*.5) filter (where year_month = 201805), 0) as may,
      coalesce(sum(unit_count*.5) filter (where year_month = 201806), 0) as jun,
      coalesce(sum(unit_count*.5) filter (where year_month = 201807), 0) as jul,
      coalesce(sum(unit_count*.5) filter (where year_month = 201808), 0) as aug,
      coalesce(sum(unit_count*.5) filter (where year_month = 201809), 0) as sep,
      coalesce(sum(unit_count*.5) filter (where year_month = 201810), 0) as oct,
      coalesce(sum(unit_count*.5) filter (where year_month = 201811), 0) as nov
    from sls.deals_by_month
    where ssc_last_name <> 'none'
--       and store_code = 'RY1'
    group by ssc_last_name, ssc_first_name, ssc_employee_number) x
  group by psc_last_name, psc_first_name, psc_employee_number) a 
inner join sls.personnel b on a.psc_employee_number = b.employee_number  
where b.store_code = 'RY1'
  and b.end_date > '11/30/2018'
  and aug > 0
order by psc_last_name, psc_first_name;


select psc_last_name as last_name, psc_first_name as first_name, 
  jan, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, ytd_total,
  round(ytd_total/months, 2) as monthly_avg,
--   (4 * round(ytd_total/months, 2)) + ytd_total as pacing_for,
  round(ytd_total *
    (select wd_in_year from dds.working_days where the_date = current_date and department = 'sales') * 1.0
    /
    (select wd_of_year_elapsed from dds.working_days where the_date = current_date and department = 'sales'), 1)  as pacing_for  
from monthly
-- order by last_name
order by monthly_avg desc

-----------------------------------------------------------
--/> 2018
-----------------------------------------------------------

-- select * from dds.working_days where the_date = current_date

-- select * from monthly

-- 1/2/19
-- need to do year end totals for nick
-- then modify to do pacing in 2019

--------------------------------------------------------------
--< end of year
--------------------------------------------------------------
select b.last_name, b.first_name, a.total
from (
  select psc_last_name, psc_first_name, psc_employee_number, sum(total) as total
  from (  
    select psc_last_name, psc_first_name, psc_employee_number, sum(unit_count) as total
    from sls.deals_by_month
    where ssc_last_name = 'none'
      and year_month between 201801 and 201812
    group by psc_last_name, psc_first_name, psc_employee_number  
    union all
    select  psc_last_name, psc_first_name, psc_employee_number, sum(unit_count)
    from sls.deals_by_month
    where ssc_last_name <> 'none'
      and year_month between 201801 and 201812
    group by psc_last_name, psc_first_name, psc_employee_number  
    union all
    select  ssc_last_name, ssc_first_name, ssc_employee_number, sum(unit_count)
    from sls.deals_by_month
    where ssc_last_name <> 'none'
      and year_month between 201801 and 201812
    group by ssc_last_name, ssc_first_name, ssc_employee_number) x
  group by psc_last_name, psc_first_name, psc_employee_number) a
join sls.personnel b on a.psc_employee_number = b.employee_number  
where b.store_code = 'RY1'
  and b.end_date > '12/31/2018'
  and b.last_name not in ('seay','dockendorf','bedney','kobayashi-dyer')
order by psc_last_name, psc_first_name;  
--------------------------------------------------------------
--./ end of year
--------------------------------------------------------------

*/

/**/
-- select 'ry1' as store, a.last_name, a.first_name, a.may, a.jun, a.jul from ry1 a where last_name <> 'hse'
-- union
-- select 'ry2', b.last_name, b.first_name, b.may, b.jun, b.jul from ry2 b
-- order by store, last_name
-- 
-- 
-- select 'ry1' as store, a.* from ry1 a where last_name <> 'hse'
-- union
-- select 'ry2', b.* from ry2 b
-- order by store, last_name
/**/