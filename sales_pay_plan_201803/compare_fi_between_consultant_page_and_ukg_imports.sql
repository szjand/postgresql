﻿select * 
from sls.deals_by_month
where stock_number = 'g45918l'

select * from sls.personnel where last_name = 'weber'

select * from sls.json_get_deals_by_consultant_month('1147810', 202211)
returns 2416.03 as fi_gross

-- consultant page
-- this is exactly what shows on consultant page
select sum(fi_gross), .16 * sum(fi_gross)
from (
          select distinct a.stock_number, a.model_year::citext || ' ' || a.make || ' ' || a.model as vehicle, a.buyer, b.fi_sales - b.fi_cogs as fi_gross
          from sls.deals_by_month a
          inner join ( -- *a
            select employee_number, payplan
            from sls.personnel_payplans
            where employee_number = '1147810' 
              and payplan = 'executive'
              and thru_date > current_date) aa on a.fi_employee_number =  aa.employee_number              
          left join sls.deals_gross_by_month b on a.stock_number = b.control
            and b.year_month = 202211
          where a.year_month = 202211
            and a.fi_employee_number = '1147810') x

-- ukg import     
-- returns what is on the ukg import page       
select * from sls.get_sales_consultant_payroll(202211,'RY1') order by consultant

sc			page total	page fi			ukg fi      chargeback
weber			9801.15		1568.18			1636.19				425.05
waldeck	 13471.70		2155.47			2193.87				240.00
vanyo	   12216.27		1954.60     1791.89	    -1016.98
rumen    14170.45   2267.27     2129.38      -861.85

select * from sls.consultant_payroll where last_name in ('waldeck', 'weber', 'vanyo', 'rumen') and year_month = 202211
*** looks like the consultant page does not include the chargebacks ***

select * from sls.fi_chargebacks where chargeback_year_month = 202211

-- almost all the chargebacks are a positive value, here are a couple largish negative ones 
select * from sls.deals where stock_number in ('G44839','G45233')
select * from sls.personnel where fi_id in ('dyo','ror')


-- look at toyota
select * from sls.consultant_payroll where last_name in ('suedel', 'console', 'hagen', 'marion') and year_month = 202211
ukg import is right on, of course\
and the individual pages, same thing, dont include chargebacks