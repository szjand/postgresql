﻿-- generate a list of accounts for jeri to categorize
-- -- select * from sls.deals_accounts_routes where page = 17
-- -- 
-- -- select * from sls.deals_accounting_detail limit 100
-- -- 
-- -- select distinct gross_category from sls.deals_accounting_detail
-- -- 
-- -- select * 
-- -- from (
-- -- select distinct store_code, gl_account, gross_category
-- -- from sls.deals_accounting_detail
-- -- where gross_category in ('fi_chargeback','fi')
-- --   and store_code = 'RY2') a
-- -- left join fin.dim_Account b on a.gl_account = b.account  
-- -- order by gl_account  
-- from jeri
-- Reserve accounts:
-- 280600
-- 280601
-- 280800
-- 280801
-- 280802
-- 
-- The issue Im having is knowing where to apply the chargebacks as we do not separate types of chargebacks at Honda.
-- 
-- from longoria
-- The accounts Jeri sent are the ones to be used at 10%
-- All others will be at 20%

-- honda fi experiment

-- december payroll
-- january payroll
drop table if exists fi cascade;
create temp table fi as
select control, category, gross_category, sum(-amount) as amount
from (
	select control, gl_account, gross_category, amount,
		case
			when gl_account in ('280600','280601','280800','280801','280802') then 'reserve'
			else 'product'
		end as category
	from sls.deals_accounting_detail 
	where year_month = 202301 --------------------------------------------
		and gross_category in ('fi_chargeback','fi')
		and store_code = 'RY2') a
group by control, category, gross_category
order by control;

-- this is what i send to longoria
select aa.*, bb.product, bb.reserve, round(.2 * product, 2) as "product_pay(20%)", round(.1 * reserve, 2) as "reserve_pay(10%)", 
  round(.2 * product, 2) + round(.1 * reserve, 2) as "product + reserve"
from (
-- 	select first_name || ' ' || last_name as consultant, employee_number, fi_gross, chargebacks, fi_total, fi_pay,
	select first_name || ' ' || last_name as consultant, employee_number, fi_gross, chargebacks, fi_total, fi_pay,
		round(.16 * fi_gross, 2) as "16% on gross (without chargebacks)" 
	from sls.consultant_payroll
	where year_month = 202301 ----------------------------------------------------------
		and employee_number like '2%'
		and fi_gross <> 0) aa
left join (  
		select fi_employee_number, 
			sum(amount) filter (where category = 'product') as product,
			sum(amount) filter (where category = 'reserve') as reserve
		from fi a
		join (
			select stock_number, fi_last_name, fi_first_name, fi_employee_number
			from sls.deals_by_month 
			where year_month = 202301 --------------------------------------------------
				and store_code = 'ry2') b on a.control = b.stock_number
		group by fi_employee_number) bb on aa.employee_number = bb.fi_employee_number

	
--  november payroll
drop table if exists fi cascade;
create temp table fi as
select control, category, gross_category, sum(-amount) as amount
from (
	select control, gl_account, gross_category, amount,
		case
			when gl_account in ('280600','280601','280800','280801','280802') then 'reserve'
			else 'product'
		end as category
	from sls.deals_accounting_detail 
	where year_month = 202211
		and gross_category in ('fi_chargeback','fi')
		and store_code = 'RY2') a
group by control, category, gross_category
order by control;


select fi_last_name, fi_first_name, 
  case when category = 'reserve' then .1 * amount end as reserve_pay,
  case when category = 'product' then .2 * amount end as product_pay
--   (.1 * amount) filter (where category = 'reserve') as 'reserve',
--   (.2 * amount) filter (where category = 'product') as 'product'
from (
select *, 
  sum(amount) over (partition by fi_last_name) as fi_gross
from (
	select fi_last_name, fi_first_name, category, gross_category, sum(amount) amount
	from fi a
	join (
		select stock_number, fi_last_name, fi_first_name
		from sls.deals_by_month 
		where year_month = 202211 
			and store_code = 'ry2') b on a.control = b.stock_number
	group by fi_last_name, fi_first_name, category, gross_category) c) d

	