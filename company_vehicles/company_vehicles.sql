﻿select b.the_date, a.control, a.doc, a.ref, a.amount, 
  c.account, c.account_type, c.department, c.account_type, c.description,
  d.journal_code, e.description
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
join fin.dim_account c on a.account_key = c.account_key
  and c.current_row
join fin.dim_journal d on a.journal_key = d.journal_key
join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and b.the_year = 2019
  and c.account = '127500'

228500: honda

127500: driver training

127703: service loaners (ctp)

128500:
  30144A  positive amount: 44378 on 5/15: vehicle from inventory to company car (service_rental)

    select * from arkona.ext_inpcmnt where vin = '1G6AX5SX6H0168635' order by transaction_date, transaction_time, sequence_number  
  32098xx
    select * from arkona.xfm_inpmast where inpmast_stock_number = '32098xx' order by inpmast_key 
    11/22/17: 38045.35 VSU becomes coach car
    2/27/19: -38045.35 GJE move to inventory (stock number changed to 32098xxxz, subsequently retailed on 3/29/19)

-- damnit, this pulls up balances due to fact_gl only goes back to 1/1/11
select a.control, sum(amount), min(b.the_date), max(b.the_date)
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
join fin.dim_account c on a.account_key = c.account_key
  and c.current_row
where a.post_status = 'Y'
  and c.account = '128500' 
group by a.control
having sum(amount) <> 0  
order by min(b.the_date)

select * from arkona.xfm_inpmast where inpmast_stock_number = '10138' order by inpmast_key 

select a.control, a.amount, b.the_Date, c.account
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
join fin.dim_account c on a.account_key = c.account_key
  and c.current_row
where a.post_status = 'Y'
  and a.control = '10138'
  

-- first date in
select b.the_Date, a.control, a.amount, b.the_Date, c.account
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
join fin.dim_account c on a.account_key = c.account_key
  and c.current_row
where a.post_status = 'Y'
order by b.the_date
limit 100