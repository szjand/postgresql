﻿-- build vehicle instances
/*
initially tried to do the vins from chrome as a batch file, but that exploded
because, now, with multiple vins, style_id is not unique in styles, and the whole notion
of populating the chrome vehicle data model evaporated immediately
so, i have retreated to doing calls to describe-vehicle-options and populating 
chr2.describe_vehicle_by_vin

***** thought i may need that endpoint with the ShowExtendedTechnicalSpecifications switch
***** i can try a few with zeep and try to determine if it is actually needed
***** first cut, just doing the same thing i have been doing from chrome
test it for trailering capacity and wheelbase, can i get what i need for each vehicle instance from
a combination of the style driven data model and the vin ADS data

*/
-- 284
select vin, yearmodel, make, model, trim, bodystyle, interiorcolor,exteriorcolor,engine,transmission
from ads.ext_vehicle_items
where yearmodel::integer > 2017
  and make not in ('chevrolet','buick','gmc','cadillac','honda','nissan')
order by make, model  


-- 1618
select distinct status, inpmast_vin, year, make, model, body_style, color,chrome_style_id
from arkona.ext_inpmast
where year > 2017
  and make not in ('chevrolet','buick','gmc','cadillac','honda','nissan')
and length(inpmast_vin) = 17  
order by make  

-- 1682 vins for batch_uplaod
-- makes no sense to do batch, data model is irrelevant at the vin level, style_id is not unique

select *
from (                 
  select vin
  from ads.ext_vehicle_items
  where yearmodel::integer > 2017
    and make not in ('chevrolet','buick','gmc','cadillac','honda','nissan')
  union
  select distinct inpmast_vin
  from arkona.ext_inpmast
  where year > 2017
    and make not in ('chevrolet','buick','gmc','cadillac','honda','nissan')
  and length(inpmast_vin) = 17) a
where not exists (
  select 1
  from chr2.describe_vehicle_by_vin
  where vin = a.vin)
limit 1;  

select * from chr2.describe_vehicle_by_vin limit 10



-- trying to get an idea of the attributes to include for a vehicle instance
select distinct table_schema, table_name
from (
with 
  make_tables  as (
    select distinct a.table_schema, a.table_name
    from information_schema.tables a
    inner join information_schema.columns b on b.table_name = a.table_name 
      and b.table_schema = a.table_schema
    where b.column_name = 'make'
      and a.table_schema not in ('information_schema', 'pg_catalog')
      and a.table_type = 'BASE TABLE'), 
  make_model_tables as (
    select distinct a.table_schema, a.table_name
    from make_tables a
    inner join information_schema.columns b on b.table_name = a.table_name 
      and b.table_schema = a.table_schema
    where b.column_name = 'model')
select a.table_schema, a.table_name, a.column_name, a.ordinal_position
from information_schema.columns a
join make_model_tables b on a.table_schema = b.table_schema
  and a.table_name = b.table_name
where a.table_name not like 'z_%'  
order by a.table_schema, a.table_name, a.ordinal_position) x

-- attributes that don't change over time
-- stocknumber, pricing are transaction attributes
order number
invoice [number, date, amount]
msrp
holdback
[build data,options,package]
black book uvc and groupnumber
vin
exterior color
interior color
seat [configuration,material,trim]
engine [size, turbo, cylinders]
transmission
fuel [capacity, type,range]
mpg(s)
towing capacity
gross weight
air conditioning

select a.vin, a.style_count
from chr2.describe_vehicle_by_vin a



select a.vin, a.style_count, c.style_id
from chr2.describe_vehicle_by_vin a
join jsonb_array_elements(a.response->'style') as b on true
left join veh.vehicle_types c on (b -> 'attributes' ->> 'id')::integer = c.style_id

-- style_count = 10, lets look at the json object
select a.*, b -> 'attributes' ->> 'trim' , b -> 'attributes' ->> 'name' 
from chr2.describe_vehicle_by_vin a
join jsonb_array_elements(a.response->'style') as b on true
where vin = '1FT7W2B64JED01059'

select * from chr2.category_definition where category_id in (1350,1304,1228,1227,1213,1203,1202,1176)

select *  from ads.ext_vehicle_items where vin = '1FT7W2B64JED01059'

/*
12/27/20
this is where i lost it
no way to populate my vision of a vehicle instance with what we currently have with chrome
*/

-- 1 style id, see if i can match up with color, engine, etc
-- fuck me, colors at least will probably never work outside of build data
select style_count, count(*)
from (
  select a.vin, a.style_count, c.style_id
  from chr2.describe_vehicle_by_vin a
  join jsonb_array_elements(a.response->'style') as b on true
  left join veh.vehicle_types c on (b -> 'attributes' ->> 'id')::integer = c.style_id) a
group by style_count
order by style_count

-- stg_vehicles uses model_code and a few specific trims to identify the relevant style id
-- this is the query for stg_vehicles for style_count = 1, substituting chr2.describe_vehicle_by_vin (style_count = 2) for nc.stg_availability
drop table if exists sc2 cascade;  -- style count = 2
create temp table sc2 as
select c.vin, (r.style ->'attributes'->>'id')::citext as chr_style_id,
  (r.style ->'attributes'->>'modelYear')::integer as chr_model_year,
  (r.style ->'division'->>'$value')::citext as chr_make,
  (r.style ->'model'->>'$value'::citext)::citext as chr_model,
  (r.style ->'attributes'->>'mfrModelCode')::citext as chr_model_code,
  case
    when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as chr_drive,
  r.style ->'marketClass'->>'$value' as market_class,
  case
    when u.cab->>'$value' like 'Crew%' then 'crew' 
    when u.cab->>'$value' like 'Extended%' then 'double'  
    when u.cab->>'$value' like 'Regular%' then 'reg'  
    else 'n/a'   
  end as chr_cab,
  case -- trim level
    when (r.style ->'model'->>'$value'::citext)::citext = 'equinox'
          and (r.style ->'attributes'->>'mfrModelCode')::citext in ( '1XY26','1XR26')
          and (r.style ->'attributes'->>'modelYear')::integer = 2020 then
      case
        when r.style ->'attributes'->>'name'::citext like '%1LT' then 'LT w/1LT'
        when r.style ->'attributes'->>'name'::citext like '%2LT' then 'LT w/2LT'
        when r.style ->'attributes'->>'name'::citext like '%2FL' then 'LT w/2FL'        
      end
    when (r.style ->'model'->>'$value'::citext)::citext = 'blazer' then 
      case 
        when r.style ->'attributes'->>'name'::citext like '%1LT' then '1LT'
        when r.style ->'attributes'->>'name'::citext like '%2LT' then '2LT'
        when r.style ->'attributes'->>'name'::citext like '%3LT' then '3LT'
        else coalesce(r.style ->'attributes'->>'trim', 'none') 
      end
    when (r.style ->'attributes'->>'modelYear')::integer = '2019' and (r.style ->'model'->>'$value'::citext)::citext = 'Silverado 1500 LD' then
      case
        when r.style ->'attributes'->>'name'::citext like '%1LT' then '1LT'
        when r.style ->'attributes'->>'name'::citext like '%2LT' then '2LT'
        else 'XXXXXXXXXXXXXXXXXXX'
      end 
    when (r.style ->'model'->>'$value'::citext)::citext = 'Express Cargo Van' 
        and (r.style ->'attributes'->>'mfrModelCode')::citext = 'CG33705' then '3500'
    else coalesce(r.style ->'attributes'->>'trim', 'none') 
  end::citext as chr_trim,
  coalesce(t.displacement->>'$value', 'n/a') as engine_displacement, -- a.color, null::integer as configuration_id,
  case
    when v. bed->>'$value' like '%Bed' then substring(bed->>'$value', 1, position(' ' in bed->>'$value') - 1)
    else 'n/a'
  end as box_size    
from chr2.describe_vehicle_by_vin c
-- join chr.describe_vehicle c on a.vin = c.vin
left join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(r.style->'bodyType') with ordinality as v(bed) on true
  and v.ordinality =  1    
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
  and s.engine ? 'installed'
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->'attributes'->>'unit' = 'liters'  
where c.style_count between 3 and 9
  and (r.style ->'attributes'->>'modelYear')::integer > 2017 -- in ('2018','2019','2020')
order by c.vin

-- these 2 don't show up in sc2, model year is 2000 and 1988 !?!?
select *
from chr2.describe_vehicle_by_vin a
where style_count = 2
  and not exists (
    select 1 
    from sc2
    where vin = a.vin)

select a.*, b.trim, b.engine, c.model, c.model_code, c.body_style, c.engine_code, c.transmission_code, key_to_cap_explosion_data, chrome_style_id
from sc2 a
left join ads.ext_vehicle_items b on a.vin = b.vin
left join arkona.ext_inpmast c on a.vin = c.inpmast_vin
where a.vin = '1FTEW1EP2KFB28899'


-- -- wtf vin not in the above, or just the attributes
-- select * from arkona.ext_inpmast where inpmast_vin = '1FTEW1EP2KFB28899'

select *
from nc.vehicle_configurations
limit 100

-- wtf, why can't i decode body type
select a.vin,  c.* --->>'attributes'
-- select a.vin,  c->>'$value'
from chr2.describe_vehicle_by_vin a
left join jsonb_array_elements(a.response->'style') as b on true
left join jsonb_array_elements(b->'bodyType') with ordinality as c on true
  and c.ordinality = 2
where vin = '1C6RR7FT2J2252496'


select bus_off_fran_code, count(*)
from arkona.ext_inpmast
group by bus_off_fran_code