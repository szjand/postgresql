﻿
descriptions for different contexts

each table currently containing vehicle data can be seen (along with its derivation/sourcing)
-- all tables with a make attribute AND a model attribute
with 
  make_tables  as (
    select distinct a.table_schema, a.table_name
    from information_schema.tables a
    inner join information_schema.columns b on b.table_name = a.table_name 
      and b.table_schema = a.table_schema
    where b.column_name = 'make'
      and a.table_schema not in ('information_schema', 'pg_catalog')
      and a.table_type = 'BASE TABLE'), 
  make_model_tables as (
    select distinct a.table_schema, a.table_name
    from make_tables a
    inner join information_schema.columns b on b.table_name = a.table_name 
      and b.table_schema = a.table_schema
    where b.column_name = 'model')
select a.table_schema, a.table_name, a.column_name, a.ordinal_position
from information_schema.columns a
join make_model_tables b on a.table_schema = b.table_schema
  and a.table_name = b.table_name
where a.table_name not like 'z_%'  
order by a.table_schema, a.table_name, a.ordinal_position;

maybe use the service to get the style_ids
then use the styleids for the batch requests

