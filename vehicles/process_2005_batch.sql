﻿/*

1/25/2021
the conclusion of all in this script is a function, chr2.update_veh_tables()
that updates veh.models, veh.make_models, veh.vehicle_types and veh.additional_classification_data

*/

select * 
from chr2.style a
left join chr2.base_1 b on a.style_id = b.style_id
where b.style_id is null
order by a.style_id

1.
in the query that generates chr2.base_1 ( and subsequently veh.vehicle_types) -- \vehicles\build_vehicle_types.sql
so what i am running into with the 2005 batch is that this join :
left join chr2.option f on a.style_id = f.style_id
  and f.header_name = 'PREFERRED EQUIPMENT GROUP'
is resulting in multiple rows for 99 style_ids  
because multiple oem_codes exist for the style_id
try a subquery with a string_agg for the oem

2. 
no body
in build_vehicle_types.sql, a table is created (chr2.bodies) using the 2 tables produced in step_3_update.sql (chr2.body_types &  chr2.body_type_ref)
in the in initial processing, the batch returns a single body_type.csv file, which i broke into 2
tables: chr2.body_types &  chr2.body_type_ref which i then reassembled into 1 table:
  drop table if exists chr2.bodies cascade;
  create table chr2.bodies as 
  select a.style_id, string_agg(distinct body_type_name, '::')::citext as body 
  from chr2.body_type_ref a
  join chr2.body_types b on a.body_type_id = b.body_type_id
  group by a.style_id;
  alter table chr2.bodies add primary key(style_id);
  alter table chr2.bodies add foreign key(style_id) references chr2.style(style_id);

the issue with body seems to be whether or not the is_primary attribute is important
in the initial parsing i decided no, but the result was vehicles with multiple body types
so, if i am ok with that, the function chr2.update_chr2_tables() ( and script ...step_3_update)
i can get rid of the splitting the tmp_body_type table into 2 tables and just go ahead

3. 
no wheelbase, passenger capacity, segment or vehicle_type
these are all based on tech specs

the batch provides 3 files:
  tech_spec.csv
  technical_specification_definition.csv
  tech_spec_value.csv
currently the function chr2.update_chr2_tables() sets the_value & and condition = 'none' where null
  to accomodate a PK
  then populates the table chr2.tech_spec_value
then in build_vehicle_types.sql, chru2.tech_spec_value & chr2.technical_specification_definition
  are combined to produce chr2.tech_specs:
    drop table if exists chr2.tech_specs cascade;
    create table chr2.tech_specs as
    select b.style_id, a.title_id, a.group_name, a.header_name, a.title_name, b.condition, b.the_value
    from chr2.technical_specification_definition a
    join chr2.tech_spec_value b on a.title_id = b.title_id;
    create unique index on chr2.tech_specs(style_id,title_id,condition,the_value);
    alter table chr2.tech_specs add foreign key(style_id) references chr2.style(style_id);
    create index on chr2.tech_specs(title_id);
    create index on chr2.tech_specs(style_id);
which is commented out, but it is that table that was used in the generation of chr2.base_1 (and subsequently veh.vehicle_types)
so, heres the plan
no need to persist with chr2.tech_spec_value, 
alter table chr2.tech_spec_value
rename to z_unused_tech_spec_value;

update the null fields in chr2.tmp_tech_spec_value
then insert then new data into tech_specs with:
-- !!! REMEMBER   THIS IS FOR ONLY ADDING THE NEW (2005-2017) DATA, STILL HAVE TO FIGURE OUT PERIODIC UPDATING !!!
-- when we get to updating, want to avoid ON CONFLICT DO UPDATE, particularly with large tables, 
-- something more likd chr2.xfm_style_ids(), separate queries for new rows and changed rows
insert into chr2.tech_specs(style_id,title_id,group_name, header_name,title_name,condition,the_value)
select b.style_id, a.title_id, a.group_name, a.header_name, a.title_name, b.condition, b.the_value
from chr2.technical_specification_definition a
join chr2.tmp_tech_spec_value b on a.title_id = b.title_id;

--01/23/21 ok, after the peg, body & tech specs (looks like no type/segment in 2005) ordeal on the 2005 batch, this
-- looks ok
-- the question now is, do we need a table chr2.base_1 for anything, why not just go straight to veh.vehicle_types
-- veh.vehicle_types is populated (...\vehicles\ddl.sql) with a straight insert select *
-- so check for where else chr2.base_1 is used
-- ok, nowhere "external" to my stuff, so, we can do away with chr2.base_1

before populating veh.vehicle_types though,
using chr2.divisions & models, populate veh.makes_models
THEN vehicle_types.make & model become foreign keys to veh.makes_models


---------------------------------------------------------------------
--< veh.makes
---------------------------------------------------------------------

drop table if exists chr2.makes_models cascade;
create table chr2.makes_models as
  select b.division as make, d.subdivision, e.model
  from chr2.style a
  join chr2.divisions b on a.division_id = b.division_id
  join veh.makes c on b.division = c.make
  join chr2.subdivisions d on a.subdivision_id = d.subdivision_id
    and d.subdivision not in ('Chevy Kodiak C-Series','Chevy W-Series',
      'GMC TopKick C-Series','GMC T-Series','Chevy T-Series','GMC W-Series',
      'Chevy Medium Duty','Ford Medium-Duty Chassis')
  join chr2.models e on a.model_id = e.model_id
  where b.division in ('Acura','Audi','BMW','Buick','Cadillac','Chevrolet','Chrysler','Dodge','Ford',
                    'GMC','Honda','Hyundai','INFINITI','Jaguar','Jeep','Kia','Land Rover','Lexus','Lincoln','Mazda',
                    'Mercedes-Benz','MINI','Mitsubishi','Nissan','Porsche','Ram','smart','Subaru','Tesla','Toyota',
                    'Volkswagen','Volvo')
  group by b.division, d.subdivision, e.model;
-- GMC Sierra 3500HD can be subdivision GMC Chassis-Cabs or GMC Pickups
alter table chr2.makes_models
add primary key(make,subdivision,model);
create index on chr2.makes_models(make);  
create index on chr2.makes_models(model);
create index on chr2.makes_models(subdivision);


first need a table of veh.makes to limit to the 32 relevant makes
!!! this is a one time task, nothing needs to be done periodically

drop table if exists veh.makes cascade;
create table veh.makes (
  make citext primary key);
comment on table veh.makes is 'the 32 makes deemed to be relevant, this list of makes is what drives the 
task of ads_base_tables.py which drives the process of chrome batch requests which is the basis for
veh.makes_models and veh.vehicle_types. in short, this is the canonical list of makes.
The addition (or deletion) of any makes will have to be an admin task.  
1/25/21 added makes Hummer,Isuzu,Mercury,Pontiac,Saab,Saturn,Scion,Suzuki, for a new total of 40
the excluded makes are:Alfa Romeo,Aston Martin,Bentley,Ferrari,FIAT,Fisker,Freightliner,Genesis,
Karma,Lamborghini,Lotus,Maserati,Maybach,McLaren,Mercury,Panoz,Polestar,Rolls-Royce';

insert into veh.makes(make)  
select make
from chr2.makes_models
group by make;

alter table veh.makes_models
add foreign key (make) references veh.makes(make);

---------------------------------------------------------------------
--/> veh.makes
---------------------------------------------------------------------



---------------------------------------------------------------------
--< veh.models
---------------------------------------------------------------------
drop table if exists veh.models cascade;
create table veh.models (
  model citext primary key);
comment on table veh.models is 'models derived from chr2.makes_models, only the models for the
makes included in veh.makes. this will need to be updated intially for the additional model years, 
subsequently for new makes.';

-- -- first insert, don't include the 2005 models yet, need a separate update query for all the new model_years
-- -- this is a one time query
-- insert into veh.models
-- select model  -- 469
-- from chr2.makes_models a
-- where exists (
--   select 1
--   from veh.makes_models
--   where model = a.model)
-- group by model;

alter table veh.makes_models
add foreign key (model) references veh.models(model);

-- the update queries that need to be in the function
insert into veh.models
select model
from chr2.makes_models a
where not exists (
  select 1
  from veh.makes_models
  where model = a.model)
group by model; 

---------------------------------------------------------------------
--/> veh.models
---------------------------------------------------------------------

---------------------------------------------------------------------
--< veh.makes_models
---------------------------------------------------------------------

insert into veh.makes_models
select make, model
from chr2.makes_models a
where not exists (
  select 1 
  from veh.makes_models
  where make = a.make
    and model = a.model)
group by make, model;    

---------------------------------------------------------------------
--/> veh.makes_models
---------------------------------------------------------------------


---------------------------------------------------------------------
--< veh.vehicle_types
---------------------------------------------------------------------
-- this is part of the adding additional model years, as such, should be all new style_ids
-- select count(*) from (   -- no limits 17353, 16360 with limits

insert into veh.vehicle_types
select a.style_id, b.model_year, b.division, c.subdivision, d.model, a.mfr_model_code, 
  e.body, a.alt_body_type, 
  case
    when a.drivetrain = 'Front Wheel Drive' then 'FWD'
    when a.drivetrain = 'All Wheel Drive' then 'AWD'
    when a.drivetrain = 'Four Wheel Drive' then '4WD'
    when a.drivetrain = 'Rear Wheel Drive' then 'RWD'
  end as drivetrain,
  a.trim_name, a.style_name, a.style_name_wo_trim, a.pass_doors, l.passenger_capacity,
  case
    when a.alt_body_type like '%cab%' then 
    left(a.alt_body_type, position('Cab' in a.alt_body_type) -1)
  end as cab,
  case 
    when a.alt_body_type like '%Bed%' then   
    replace(
      substring(a.alt_body_type, position('-' in a.alt_body_type) + 2, 
        position('Bed' in a.alt_body_type) -2), 'Bed', '')
  end as bed,
  f.peg,
  g.wheelbase, h.overall_length,  
  j.the_value as segment, k.the_value as vehicle_type,
  i.url as image_url
from chr2.style a
join chr2.divisions b on a.division_id = b.division_id and a.model_year = b.model_year
join chr2.subdivisions c on a.subdivision_id = c.subdivision_id and a.model_year = c.model_year
join chr2.models d on a.model_id = d.model_id and a.model_year = d.model_year
join chr2.makes_models aa on b.division = aa.make and d.model = aa.model and c.subdivision = aa.subdivision-- limit based on subdivision & 32 makes
left join chr2.bodies e on a.style_id = e.style_id 
-- left join chr2.option f on a.style_id = f.style_id
--   and f.header_name = 'PREFERRED EQUIPMENT GROUP'
left join ( -- 2005: multiple peg for some models
  select style_id, string_agg(distinct oem_code, ',') as peg
  from chr2.option
  where header_name = 'PREFERRED EQUIPMENT GROUP'
  group by style_id) f on a.style_id = f.style_id
left join ( -- wheelbase 
  select style_id, string_agg(the_value, ',') as wheelbase
  from chr2.tech_specs 
  where title_id = 301
  group by style_id) g on a.style_id = g.style_id
left join ( -- overall_length 
  select style_id, string_agg(the_value, ',') as overall_length
  from chr2.tech_specs 
  where title_id = 304
  group by style_id) h on a.style_id = h.style_id
left join chr2.image i on a.style_id = i.style_id
left join chr2.tech_specs j on a.style_id = j.style_id
  and j.title_id = 1113  -- segment
left join chr2.tech_specs k on a.style_id = k.style_id
  and k.title_id = 1114  -- vehicle_type
left join (
  select style_id, string_agg(distinct the_value, ',') as passenger_capacity
  from chr2.tech_specs 
  where title_name = 'passenger capacity'
  group by style_id) l on a.style_id = l.style_id 
where not exists (
    select 1
    from veh.vehicle_types
    where style_id = a.style_id);

---------------------------------------------------------------------
--/> veh.vehicle_types
---------------------------------------------------------------------


---------------------------------------------------------------------
--< veh.additional_classification_data
---------------------------------------------------------------------
truncate veh.additional_classification_data;
insert into veh.additional_classification_data
select aa.chrome_make, aa.chrome_model, aa.subdivision, aa.wheelbase, aa.overall_length,
  aa.pass_doors, aa.drivetrain, aa.type_segment,
   bb.model as polk_model, bb.polk_segment,
  cc.model as tool_model, cc.tool_shape_size, cc.luxury as tool_luxury, cc.comm as tool_comm, cc.sport as tool_sport,
  aa.image_url_1 as image_one, aa.image_url_2 as image_two
from (
  select a.make as chrome_make, a.model as chrome_model, a.subdivision, 
    case
      when cardinality(array_agg(distinct a.wheelbase)) = 1 then (min(a.wheelbase))::text
      else (min(a.wheelbase))::text || '-' || (max(a.wheelbase))::text
    end as wheelbase,
    case
      when cardinality(array_agg(distinct a.overall_length)) = 1 then (min(a.overall_length))::text
      else (min(a.overall_length))::text || '-' || (max(a.overall_length))::text
    end as overall_length,
    case
      when cardinality(array_agg(distinct a.pass_doors)) = 1 then (min(a.pass_doors))::text
      else (min(a.pass_doors))::text || '-' || (max(a.pass_doors))::text
    end as pass_doors,  string_agg(distinct a.drivetrain, ',') as drivetrain,
    max(b.type_segment) as type_segment, min(image_url) as image_url_1, max(image_url) as image_url_2
  from veh.vehicle_types a 
  left join ( -- generates a vertical likst of types/segments
    select make, model, string_agg(distinct coalesce(vehicle_type, '') ||  ' : ' || coalesce(segment, ''), E'\n') as type_segment 
    from veh.vehicle_types 
    where vehicle_type is not null
      and segment is not null
    group by make, model) b on a.make = b.make and a.model = b.model
  group by a.make, a.subdivision, a.model) aa
left join (
  select a.make, a.model, string_agg(distinct a.segment, ',') as polk_segment
  from polk.vehicle_categories a
  join veh.makes_models b on a.make = b.make
  group by a.make, a.model) bb on aa.chrome_make = bb.make and aa.chrome_model = bb.model
left join (
  select b.make, b.model, string_agg(distinct split_part(b.vehicletype, '_', 2) ||'-'||split_part(b.vehiclesegment, '_', 2),',') as tool_shape_size,
    case when string_agg(distinct luxury::text,',') like '%true%' then 'true' else null end as luxury, 
    case when string_agg(distinct commercial::text,',') like '%true%' then 'true' else null end as comm, 
    case when string_agg(distinct sport::text,',') like '%true%' then 'true' else null end as sport
  from ads.ext_make_model_classifications b
  join veh.makes_models c on b.make = c.make
  group by b.make, b.model) cc on coalesce(aa.chrome_make, bb.make) = cc.make  and coalesce(aa.chrome_model, bb.model) = cc.model
-- where not exists (
--   select 1
--   from veh.additional_classification_data
--   where chrome_make = aa.chrome_make
--     and chrome_model = aa.chrome_model
--     and subdivision = aa.subdivision);

select chrome_make, chrome_model, subdivision
from veh.additional_classification_data
group by chrome_make, chrome_model, subdivision
having count(*) > 1
---------------------------------------------------------------------
--/> veh.additional_classification_data
---------------------------------------------------------------------




select a.*
from veh.additional_classification_Data a
join (
select chrome_make, chrome_model
from veh.additional_classification_data
group by chrome_make, chrome_model 
having count(*) > 1) b on a.chrome_model = b.chrome_model
  and a.chrome_make = b.chrome_make
order by a.chrome_make, a.chrome_model  


select * from veh.shape_size_classifications


select concat(a.make,'_',a.model) as id, a.make, a.model, luxury, sport, shape, size, shape_size, shape_size_additional, case when shape is not null then true else false end as is_classified,
c.*
from veh.makes_models a 
left join veh.shape_size_classifications b on a.make = b.make and a.model = b.model
-- left join additional_data c on a.make = c.chrome_make and a.model = c.chrome_model
left join veh.additional_classification_data c on a.make = c.chrome_make and a.model = c.chrome_model
where a.make = 'chevrolet' and a.model = 'Silverado 3500'