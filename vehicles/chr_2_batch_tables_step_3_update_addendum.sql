﻿/*
02/05/21
to sum it up, i discovered there were quite a few makes and models that did not make it into
veh.makes_models. the data exists in the base tables but did not make it to chr.makes_models
so this script is all about getting those makes and models into the data
*/

-- holy fucking shit 268 more !?!?!?!?
drop table if exists wtf cascade;
creaTE TEMP TABLE wtf as
  select b.division as make, d.subdivision, e.model, min(style_id), max(style_id)
  from chr2.style a
  join chr2.divisions b on a.division_id = b.division_id
  join veh.makes c on b.division = c.make
  join chr2.subdivisions d on a.subdivision_id = d.subdivision_id
    and d.subdivision not in ('Chevy Kodiak C-Series','Chevy W-Series',
      'GMC TopKick C-Series','GMC T-Series','Chevy T-Series','GMC W-Series',
      'Chevy Medium Duty','Ford Medium-Duty Chassis')
  join chr2.models e on a.model_id = e.model_id
  where b.division in ('Acura','Audi','BMW','Buick','Cadillac','Chevrolet','Chrysler','Dodge','Ford',
                    'GMC','Honda','Hyundai','INFINITI','Jaguar','Jeep','Kia','Land Rover','Lexus','Lincoln','Mazda',
                    'Mercedes-Benz','MINI','Mitsubishi','Nissan','Porsche','Ram','smart','Subaru','Tesla','Toyota',
                    'Volkswagen','Volvo','Hummer','Isuzu','Mercury','Pontiac','Saab','Saturn','Scion','Suzuki')
    and not exists (
      select 1 
      from veh.makes_models
      where model = e.model
        and make = b.division)                    
  group by b.division, d.subdivision, e.model    

select * from wtf
select * from chr2.divisions where division = 'mercury'

select * from chr2.models where division_id = 28

ok, i see the missing make_models in chr2.make_models, somehow fucking 268 did not get inserted into it
but is the data in chr2.option, tech_Specs, etc all the tables for vehicle types ?
looks like it
select * from wtf 
select * from chr2.tech_specs where style_id in (290152,363265)

so the hypothesis is, i neglected to do the 
chr2.subdivision |
chr2.style       | -> chr2.makes_models |-> veh.makes | -> veh.makes_models
chr2.divisions   |                      |-> veh.models|
chr2.models      |
which is used in an inner join in the building of veh.vehicle_types & veh.additional_classification_data

so the updating of chr2.makes_models should have happened in chr2.update_chr2_tables()

-- 02/05/21 i will do it here, then also add these queries to  chr2.update_chr2_tables()

insert into chr2.makes_models
select b.division as make, d.subdivision, e.model
from chr2.style a
join chr2.divisions b on a.division_id = b.division_id
join veh.makes c on b.division = c.make
join chr2.subdivisions d on a.subdivision_id = d.subdivision_id
  and d.subdivision not in ('Chevy Kodiak C-Series','Chevy W-Series',
    'GMC TopKick C-Series','GMC T-Series','Chevy T-Series','GMC W-Series',
    'Chevy Medium Duty','Ford Medium-Duty Chassis')
join chr2.models e on a.model_id = e.model_id
where b.division in ('Acura','Audi','BMW','Buick','Cadillac','Chevrolet','Chrysler','Dodge','Ford',
                  'GMC','Honda','Hyundai','INFINITI','Jaguar','Jeep','Kia','Land Rover','Lexus','Lincoln','Mazda',
                  'Mercedes-Benz','MINI','Mitsubishi','Nissan','Porsche','Ram','smart','Subaru','Tesla','Toyota',
                  'Volkswagen','Volvo','Hummer','Isuzu','Mercury','Pontiac','Saab','Saturn','Scion','Suzuki')
  and not exists (
    select 1 
    from chr2.makes_models
    where model = e.model
      and make = b.division)                    
group by b.division, d.subdivision, e.model;     



-- and these to chr2.update_veh_tables()  
-- actually, there is no need to add anything to chr2.update_veh_tables()  
-- i just need to run the queries in that function to add the missing data
-- makes are ok
select * 
from (
  select make 
  from chr2.makes_models 
  group by make) a
left join veh.makes b on a.make = b.make
where b.make is null;

---------------------------------------------------------------------
--< veh.models
---------------------------------------------------------------------
insert into veh.models
select model
from chr2.makes_models a
where not exists (
  select 1
  from veh.makes_models
  where model = a.model)
group by model; 
 ---------------------------------------------------------------------
--< veh.makes_models
--------------------------------------------------------------------- 
insert into veh.makes_models
select make, model
from chr2.makes_models a
where not exists (
  select 1 
  from veh.makes_models
  where make = a.make
    and model = a.model)
group by make, model;  

-- ok, there are 6 more in chr2.makes_models than go into veh.makes models
-- in chr2 there are dup make and models that are differetiated by subdivision
-- select make, model from wtf group by make, model having count(*) > 1
-- select * from wtf a
-- left join (
--   select make, model
--   from chr2.makes_models a
--   where not exists (
--     select 1 
--     from veh.makes_models
--     where make = a.make
--       and model = a.model)
--   group by make, model) b on a.make = b.make and a.model = b.model
--  where b.make is null 
-- 
-- select * from chr2.makes_models where make = 'gmc' and model = 'Sierra 3500 Classic' 

---------------------------------------------------------------------
--< veh.vehicle_types
---------------------------------------------------------------------  
  insert into veh.vehicle_types
  select a.style_id, b.model_year, b.division, c.subdivision, d.model, a.mfr_model_code, 
    e.body, a.alt_body_type, 
    case
      when a.drivetrain = 'Front Wheel Drive' then 'FWD'
      when a.drivetrain = 'All Wheel Drive' then 'AWD'
      when a.drivetrain = 'Four Wheel Drive' then '4WD'
      when a.drivetrain = 'Rear Wheel Drive' then 'RWD'
    end as drivetrain,
    a.trim_name, a.style_name, a.style_name_wo_trim, a.pass_doors, l.passenger_capacity,
    case
      when a.alt_body_type like '%cab%' then 
      left(a.alt_body_type, position('Cab' in a.alt_body_type) -1)
    end as cab,
    case 
      when a.alt_body_type like '%Bed%' then   
      replace(
        substring(a.alt_body_type, position('-' in a.alt_body_type) + 2, 
          position('Bed' in a.alt_body_type) -2), 'Bed', '')
    end as bed,
    f.peg,
    g.wheelbase, h.overall_length,  
    j.the_value as segment, k.the_value as vehicle_type,
    i.url as image_url
  from chr2.style a
  join chr2.divisions b on a.division_id = b.division_id and a.model_year = b.model_year
  join chr2.subdivisions c on a.subdivision_id = c.subdivision_id and a.model_year = c.model_year
  join chr2.models d on a.model_id = d.model_id and a.model_year = d.model_year
  join chr2.makes_models aa on b.division = aa.make and d.model = aa.model and c.subdivision = aa.subdivision-- limit based on subdivision & 32 makes
  left join chr2.bodies e on a.style_id = e.style_id 
  -- left join chr2.option f on a.style_id = f.style_id
  --   and f.header_name = 'PREFERRED EQUIPMENT GROUP'
  left join ( -- 2005: multiple peg for some models
    select style_id, string_agg(distinct oem_code, ',') as peg
    from chr2.option
    where header_name = 'PREFERRED EQUIPMENT GROUP'
    group by style_id) f on a.style_id = f.style_id
  left join ( -- wheelbase 
    select style_id, string_agg(the_value, ',') as wheelbase
    from chr2.tech_specs 
    where title_id = 301
    group by style_id) g on a.style_id = g.style_id
  left join ( -- overall_length 
    select style_id, string_agg(the_value, ',') as overall_length
    from chr2.tech_specs 
    where title_id = 304
    group by style_id) h on a.style_id = h.style_id
  left join chr2.image i on a.style_id = i.style_id
  left join chr2.tech_specs j on a.style_id = j.style_id
    and j.title_id = 1113  -- segment
  left join chr2.tech_specs k on a.style_id = k.style_id
    and k.title_id = 1114  -- vehicle_type
  left join (
    select style_id, string_agg(distinct the_value, ',') as passenger_capacity
    from chr2.tech_specs 
    where title_name = 'passenger capacity'
    group by style_id) l on a.style_id = l.style_id 
  where not exists (
      select 1
      from veh.vehicle_types
      where style_id = a.style_id);

---------------------------------------------------------------------
--< veh.additional_classification_data
---------------------------------------------------------------------      
  truncate veh.additional_classification_data;
  insert into veh.additional_classification_data
  select aa.chrome_make, aa.chrome_model, aa.subdivision, aa.wheelbase, aa.overall_length,
    aa.pass_doors, aa.drivetrain, aa.type_segment,
     bb.model as polk_model, bb.polk_segment,
    cc.model as tool_model, cc.tool_shape_size, cc.luxury as tool_luxury, cc.comm as tool_comm, cc.sport as tool_sport,
    aa.image_url_1 as image_one, aa.image_url_2 as image_two
  from (
    select a.make as chrome_make, a.model as chrome_model, a.subdivision, 
      case
        when cardinality(array_agg(distinct a.wheelbase)) = 1 then (min(a.wheelbase))::text
        else (min(a.wheelbase))::text || '-' || (max(a.wheelbase))::text
      end as wheelbase,
      case
        when cardinality(array_agg(distinct a.overall_length)) = 1 then (min(a.overall_length))::text
        else (min(a.overall_length))::text || '-' || (max(a.overall_length))::text
      end as overall_length,
      case
        when cardinality(array_agg(distinct a.pass_doors)) = 1 then (min(a.pass_doors))::text
        else (min(a.pass_doors))::text || '-' || (max(a.pass_doors))::text
      end as pass_doors,  string_agg(distinct a.drivetrain, ',') as drivetrain,
      max(b.type_segment) as type_segment, min(image_url) as image_url_1, max(image_url) as image_url_2
    from veh.vehicle_types a 
    left join ( -- generates a vertical likst of types/segments
      select make, model, string_agg(distinct coalesce(vehicle_type, '') ||  ' : ' || coalesce(segment, ''), E'\n') as type_segment 
      from veh.vehicle_types 
      where vehicle_type is not null
        and segment is not null
      group by make, model) b on a.make = b.make and a.model = b.model
    group by a.make, a.subdivision, a.model) aa
  left join (
    select a.make, a.model, string_agg(distinct a.segment, ',') as polk_segment
    from polk.vehicle_categories a
    join veh.makes_models b on a.make = b.make
    group by a.make, a.model) bb on aa.chrome_make = bb.make and aa.chrome_model = bb.model
  left join (
    select b.make, b.model, string_agg(distinct split_part(b.vehicletype, '_', 2) ||'-'||split_part(b.vehiclesegment, '_', 2),',') as tool_shape_size,
      case when string_agg(distinct luxury::text,',') like '%true%' then 'true' else null end as luxury, 
      case when string_agg(distinct commercial::text,',') like '%true%' then 'true' else null end as comm, 
      case when string_agg(distinct sport::text,',') like '%true%' then 'true' else null end as sport
    from ads.ext_make_model_classifications b
    join veh.makes_models c on b.make = c.make
    group by b.make, b.model) cc on coalesce(aa.chrome_make, bb.make) = cc.make  and coalesce(aa.chrome_model, bb.model) = cc.model;      