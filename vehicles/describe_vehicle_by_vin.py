# encoding=utf-8
"""
"""
import utilities
import requests

pg_server = '173'

with utilities.pg(pg_server) as pg_con:
    with pg_con.cursor() as pg_cur:

        sql = """
                select *
                from (                 
                  select vin
                  from ads.ext_vehicle_items
                  where yearmodel::integer > 2017
                    and make not in ('chevrolet','buick','gmc','cadillac','honda','nissan')
                  union
                  select distinct inpmast_vin
                  from arkona.ext_inpmast
                  where year > 2017
                    and make not in ('chevrolet','buick','gmc','cadillac','honda','nissan')
                  and length(inpmast_vin) = 17) a
                where not exists (
                  select 1
                  from chr2.describe_vehicle_by_vin
                  where vin = a.vin);  
         """
        pg_cur.execute(sql)
        for row in pg_cur.fetchall():
            vin = row[0]
            url = 'https://beta.rydellvision.com:8888/chrome/describe-vehicle-options/' + vin
            data = requests.get(url)
            resp = data.text
            resp = resp.replace("'", "''")
            with pg_con.cursor() as chr_cur:
                sql = """
                    insert into chr2.describe_vehicle_by_vin(vin,response)
                    values ('{0}','{1}');
                """.format(vin, resp)
                chr_cur.execute(sql)
                pg_con.commit()
        with pg_con.cursor() as chr_cur:
            sql = """
                update chr2.describe_vehicle_by_vin y
                set style_count = x.style_count
                from (
                  select vin, count(*) as style_count
                  from (
                    select vin, jsonb_array_elements(response->'style')
                    from chr2.describe_vehicle_by_vin
                    where style_count is null) a
                  group by vin) x
                where x.vin = y.vin;  
            """
            chr_cur.execute(sql)
