﻿12/10/20
it has been (tentatively) decided that transmission does not belong in the type table
any further work on transmissions should be on a viable snowflake
-----------------------------------------------------------------------------------------------------
--< transmission
-----------------------------------------------------------------------------------------------------

-- a lot of vehicles without trans info in chr2.option
select a.style_id, a.model_year, a.division, a.model, a.trim_name, b.*
from chr2.base_1 a
left join chr2.option b on a.style_id = b.style_id
  and b.header_name = 'transmission'


2020 acura rdx styleid 406972
in factoryoptions there is no transmission
but in standard, there is a transmission


added_cat_ids: Category IDs associated to equipment 
removed_cat_ids: Like category IDs not associated to equipment

that is what i was looking for, cat id 1130 = automatic, 1131 = manual
these category ids are found in the added_cat_ids field
and this corresponds with what i did in python project/ext_chrome/sql/body_types_transmissions.sql
for automatic or manual
so standard_equipment header_id = 1236 (mechanical) and added_Cat_ids 1130 or 1131

for trans descripion
header_id = 1236 and description like 'Transmission:%'

select a.style_id, a.model_year, a.division, a.model, a.trim_name, b.added_cat_ids
from chr2.base_1 a
left join chr2.standard_equipment b on a.style_id = b.style_id 
  and b.header_id = 1236
  and (b.added_cat_ids like '%1130%' or b.added_cat_ids like '%1131%')
where a.division = 'ford'


-- going off on 1130/1131 not matching option.descriptions
select a.style_id, a.model_year, a.division, a.model, a.trim_name, b.descriptions, replace(b.descriptions, 'TRANSMISSION: ', '') as trans, b.oem_code, b.chrome_code,
  c.added_cat_ids, c.removed_cat_ids
from chr2.base_1 a
left join chr2.option b on a.style_id = b.style_id
  and b.header_name = 'transmission'
left join chr2.standard_equipment c on a.style_id = c.style_id 
  and c.header_id = 1236
  and (c.added_cat_ids like '%1130%' or c.added_cat_ids like '%1131%')  
where a.model_year = 2020
  and (
    (b.descriptions like '%manual%' and c.added_cat_ids like '%1130%')
    or
    (b.descriptions like '%auto%' and c.added_Cat_ids like '%1131%'))
order by a.style_id


-- maybe go with 1130/1131 as base in types, but suplement with descriptions as a trans snowflake
-- at least 1130/1131 is internally consistent
select a.style_id, a.model_year, a.division, a.model, a.trim_name, 
  c.added_cat_ids
from chr2.base_1 a
join chr2.standard_equipment c on a.style_id = c.style_id 
  and c.header_id = 1236
  and c.added_cat_ids like '%1130%' 
  and c.added_cat_ids like '%1131%' 

-- maybe go with option where there is only one per style
-- here are the multiple trans per style
select a.style_id, a.model_year, a.division, a.model, a.trim_name, b.descriptions trans, b.oem_code, b.chrome_code
from chr2.base_1 a
left join chr2.option b on a.style_id = b.style_id
  and b.header_name = 'transmission'
where a.style_id in (  
  select style_id
  from (
    select a.style_id, b.descriptions
    from chr2.base_1 a
    join chr2.option b on a.style_id = b.style_id
      and b.header_name = 'transmission'
    group by a.style_id, b.descriptions) c
  group by style_id 
  having count(*) > 1) 
order by a.style_id  

-- here are the one trans per style
-- this is only 3564 of the 14158 styles
select a.style_id, a.model_year, a.division, a.model, a.trim_name, b.descriptions trans, b.oem_code, b.chrome_code
from chr2.base_1 a
left join chr2.option b on a.style_id = b.style_id
  and b.header_name = 'transmission'
where a.style_id in (  
  select style_id
  from (
    select a.style_id, b.descriptions
    from chr2.base_1 a
    join chr2.option b on a.style_id = b.style_id
      and b.header_name = 'transmission'
    group by a.style_id, b.descriptions) c
  group by style_id 
  having count(*) = 1) 
order by a.style_id  

-- here are the one trans per style, now does 1130/1131 agree with these
-- these are golden, only exeption is corvette with discription = TRANSMISSION, 8-SPEED DUAL CLUTCH, INCLUDES MANUAL AND AUTO MODES
select a.style_id, a.model_year, a.division, a.model, a.trim_name, b.descriptions trans, b.oem_code, b.chrome_code, c.added_cat_ids
from chr2.base_1 a
left join chr2.option b on a.style_id = b.style_id
  and b.header_name = 'transmission'
left join chr2.standard_equipment c on a.style_id = c.style_id 
  and c.header_id = 1236
  and (c.added_cat_ids like '%1130%' or c.added_cat_ids like '%1131%')    
where a.style_id in (  
  select style_id
  from (
    select a.style_id, b.descriptions
    from chr2.base_1 a
    join chr2.option b on a.style_id = b.style_id
      and b.header_name = 'transmission'
    group by a.style_id, b.descriptions) c
  group by style_id 
  having count(*) = 1) 
  and (
    (b.descriptions like '%manual%' and c.added_cat_ids like '%1130%')
    or
    (b.descriptions like '%auto%' and c.added_Cat_ids like '%1131%'))  
order by a.style_id  

-- great now , the number of gears from categories?

select a.style_id, a.model_year, a.division, a.model, a.trim_name,
  case
    when b.added_cat_ids ~ '1130' then 'Auto'
    when b.added_cat_ids ~ '1131' then 'Manual'
  end as trans
from chr2.base_1 a  
left join chr2.standard_equipment b on a.style_id = b .style_id 
  and b.header_id = 1236
  and (b.added_cat_ids like '%1130%' or b.added_cat_ids like '%1131%')    
where not exists (
  select 1
  from chr2.option
  where style_id = a.style_id
    and header_name = 'transmission')


-- 12/10/20 
-- one last check 1:1 on style_id - 1130/1131
-- ok, no multiples
select a.style_id, a.model_year, a.division, a.model, a.trim_name,
  case
    when b.added_cat_ids ~ '1130' then 'Auto'
    when b.added_cat_ids ~ '1131' then 'Manual'
  end as trans
from chr2.base_1 a  
left join chr2.standard_equipment b on a.style_id = b .style_id 
  and b.header_id = 1236
  and (b.added_cat_ids like '%1130%' or b.added_cat_ids like '%1131%')    
-- only 6 styleids missing
select a.style_id, a.model_year, a.division, a.model, a.trim_name,
  case
    when b.added_cat_ids ~ '1130' then 'Auto'
    when b.added_cat_ids ~ '1131' then 'Manual'
  end as trans
from chr2.base_1 a  
join chr2.standard_equipment b on a.style_id = b .style_id 
  and b.header_id = 1236
  and (b.added_cat_ids like '%1130%' or b.added_cat_ids like '%1131%')    



-- figure out how to join on 1423;1443;1205
select a.description, a.added_cat_ids, array_length(string_to_array(added_cat_ids, ';'), 1) - 1
from chr2.standard_equipment a
-- join chr2.category_description b on '%' || a.added_cat_ids || '%'
where a.header_id = 1236
  and a.style_id = 409109
  and a.description like 'Transmission:%'   

select a.description, unnest(string_to_array(added_cat_ids, ';'))
from chr2.standard_equipment a
where a.header_id = 1236
  and a.style_id = 409109
  and a.description like 'Transmission:%'     

select *
from (
  select a.description, unnest(string_to_array(added_cat_ids, ';'))::integer as cat_id
  from chr2.standard_equipment a
  where a.header_id = 1236
    and a.style_id = 392617
    and a.description like 'Transmission%') aa  
join (
  select category_id, category_name
  from chr2.category_definition
  where header_id = 15
    and category_id not in (1130,1131,1380, 1134, 1195, 1196)) bb on aa.cat_id = bb.category_id
left join (
  select category_id, category_name
  from chr2.category_definition
  where header_id = 15
    and category_id in (1134, 1195, 1196)) cc on aa.cat_id = cc.category_id    
-----------------------------------------------------------------------------------------------------
--/> transmission
-----------------------------------------------------------------------------------------------------


-- uh oh going down mindfuck alley


-- does this description differ from the option description?
select *
from chr2.standard_equipment
where header_id = 1236
  and description like '%transmission%'
  and (added_cat_ids like '%1130%' or added_cat_ids like '%1131%')    

-- no dups, 14063 of 14158
select style_id from (
select *
from chr2.standard_equipment
where header_id = 1236
  and description like '%transmission%'
  and (added_cat_ids like '%1130%' or added_cat_ids like '%1131%')    
) x group by style_id having count(*) > 1

-- here is a 2020 sierra with values in chr2.option
406623;2020;GMC;Sierra 1500;SLT;TRANSMISSION, 10-SPEED AUTOMATIC, ELECTRONICALLY CONTROLLED;MQB;MQB
406623;2020;GMC;Sierra 1500;SLT;TRANSMISSION, 8-SPEED AUTOMATIC, ELECTRONICALLY CONTROLLED;MQE;MQE

-- holy shit, it is even more verbose in standard equipment
select *
from chr2.standard_equipment
where header_id = 1236
  and style_id = 406623
  and description like '%transmission%'
  and (added_cat_ids like '%1130%' or added_cat_ids like '%1131%')    

 Transmission, 8-speed automatic, electronically controlled with overdrive and tow/haul mode. Includes Cruise Grade Braking and Powertrain Grade Braking (Standard on 2WD models equipped with (L84) 5.3L EcoTec3 V8 engine. Available on 4WD models equipped with (L84) 5.3L EcoTec3 V8 engine.) 


--  so what are the 95 styles missing from standard_equipment
drop table if exists trans_missing_standard;
create temp table trans_missing_standard as
select a.style_id, a.model_year, a.division, a.model, a.trim_name
from chr2.base_1 a
left join chr2.standard_equipment b on a.style_id = b.style_id
  and b.description like '%transmission%'
  and (added_cat_ids like '%1130%' or added_cat_ids like '%1131%')   
where b.style_id is null;  

do they have trans in options?
-- yep, each style_id has one trans, the only exception is 390512, a 2018 370Z Coupe
select a.style_id, a.model_year, a.division, a.model, a.trim_name, b.*
from chr2.base_1 a
join trans_missing_standard aa on a.style_id = aa.style_id
left join chr2.option b on a.style_id = b.style_id
  and b.header_name = 'transmission'


 