﻿drop schema if exists veh cascade;
create schema veh;
comment on schema veh is 'for the consolidated vehicle tables, a project started in dec 2020 as part of the 8:00 meeting with greg, afton and jon.'.


drop table if exists veh.vehicle_types cascade;
CREATE TABLE veh.vehicle_types (
  style_id integer NOT NULL,
  model_year integer not null,
  make citext not null,
  subdivision citext not null,
  model citext not null,
  model_code citext not null,
  body citext not null,
  alt_body_type citext,
  drivetrain citext,
  trim_name citext,
  style_name citext not null,
  style_name_wo_trim citext not null,
  pass_doors integer not null,
  passenger_capacity citext not null,
  cab citext,
  bed citext,
  peg citext,
  wheelbase citext,
  overall_length citext,
  segment citext,
  vehicle_type citext,
  image_url citext,
  CONSTRAINT vehicle_types_pkey PRIMARY KEY (style_id),
  CONSTRAINT vehicle_types_style_id_fkey FOREIGN KEY (style_id)
      REFERENCES chr2.style (style_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION);
create index on veh.vehicle_types(model_year);      
create index on veh.vehicle_types(make);
create index on veh.vehicle_types(model);
comment on table veh.vehicle_types is 'built entirely from tables in schema chr2 populated by data from  chrome style_id batch requests. 
initially populated dec 2020 with 32 makes, 14158 style_ids and model years 2018 - 2022.
1/24/21 added model years 2005 - 2017.  looking for a unique index other than PK,  model_year/make/model/model_code,style_name works for 
everything EXCEPT 2 2019 nissan frontier style_ids (which we have not seen), i am going to exclude them: 408189 & 408190. some months
ago i ran into these, asked chrome about it, they said they are valid, oh well. Never mind, 2005 reveals 72 volvos with the same situatin,
no natural key for now, damnit';


-- -- 1/24/21 no longer populating from chr2.base_1, but by the query that generated chr2.base_1
-- insert into veh.vehicle_types
-- select * from chr2.base_1;

delete
-- select * 
from veh.vehicle_types
where style_id in (408189,408190);

create unique index on veh.vehicle_types(model_year, make, model, model_code, style_name);
drop index veh.vehicle_types_model_year_make_model_model_code_style_name_idx;

