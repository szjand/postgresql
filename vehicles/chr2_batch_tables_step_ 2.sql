﻿create or replace function chr2.drop_and_recreate_tmp_table_for_batch_processing()
returns void as
$BODY$
-- tmp_tables
-- these tables all need to be dropped and recreated for each new set of data

  drop table if exists chr2.tmp_style cascade;
  create table chr2.tmp_style(
    input_row_number integer,
    passthru_id integer, 
    style_id integer primary key,
    division_id	integer not null, 
    subdivision_id	integer not null, -- column name agree with subdivisions table
    model_id integer not null references chr2.models(model_id), -- ref
    base_msrp	numeric,
    base_invoice numeric,	
    dest_chrg	numeric,
    is_price_unknown boolean not null,
    market_class_id	integer not null, -- ref what? marketClass_definition.csv is empty !
    model_year integer not null,
    style_name citext not null,	
    style_name_wo_trim citext not null,
    trim_name	citext, -- some have no trim, am i comfortable with null values?
    mfr_model_code citext not null,
    is_fleet_only	boolean not null,
    is_model_fleet boolean not null,	
    pass_doors integer not null,
    alt_model_name citext,
    alt_style_name citext,
    alt_body_type citext,
    drivetrain citext, -- some have no trim, am i comfortable with null values?
    foreign key (model_year,division_id) references chr2.divisions(model_year,division_id),
    foreign key (model_year,subdivision_id) references chr2.subdivisions(model_year,subdivision_id));
  create unique index on chr2.tmp_style(style_id,trim_name);

  drop table if exists chr2.tmp_technical_specification_definition cascade;
  create table chr2.tmp_technical_specification_definition (
    group_id integer not null,
    group_name citext not null,
    header_id integer not null,
    header_name citext not null,
    title_id integer not null primary key,
    title_name citext not null,
    measurement_unit citext,
    country citext,
    language citext);
  create index on chr2.tmp_technical_specification_definition(group_id);
  create index on chr2.tmp_technical_specification_definition(header_id);

  drop table if exists chr2.tmp_tech_spec_value cascade;
  create table chr2.tmp_tech_spec_value (
    input_row_num integer not null,
    passthru_id integer not null,
    title_id integer not null,
    the_value citext, -- change field name
    condition citext,
    style_id integer not null);

  drop table if exists chr2.tmp_body_type cascade;
  create table chr2.tmp_body_type (
    input_row_number integer,
    passthru_id integer,
    style_id integer not null,
    body_type_id integer not null,
    body_type_name citext not null,
    is_primary boolean not null);
  comment on table chr2.tmp_body_type is 'destination for the body_type.csv from batch results, data is to be subsequently
    broken out into 2 tables: chr2.tmp_body_types & chr2.tmp_body_type_ref';

  drop table if exists chr2.tmp_category_definition cascade;
  create table chr2.tmp_category_definition (
    group_id integer not null,
    group_name citext not null,
    header_id integer not null,
    header_name citext not null,
    category_id integer primary key,
    category_name citext not null,
    type_id integer,
    type_name citext,
    country citext,
    language citext);

  drop table if exists chr2.tmp_option cascade;  -- probably don't need for "vehicle types", no but for a table of packages  & ...
  create table chr2.tmp_option (
    input_row_num	integer,
    passthru_id	integer,
    ambiguous_option_id	integer,
    header_id	integer, -- 2005 has null header_id
    header_name	citext, -- 2005 has null header_name
    descriptions citext, 
    added_cat_ids	citext,  -- array?
    removed_cat_ids	citext,  -- array? this one definitely contains multiple ids
    msrp_low	numeric,
    msrp_high	numeric,
    invoice_low	numeric,
    invoice_high	numeric,
    is_unknown_price	boolean not null,
    style_id	integer not null, -- references chr2.tmp_style(style_id),  -- changed field name
    install_cause	citext,
    install_cause_detail citext,
    chrome_code	citext,
    oem_code	citext,
    alt_optioncode	citext,
    is_standard	boolean not null,
    optionkind_id	integer,
    utf	citext,
    is_fleet_only citext); -- orig was boolean not null, but there are null values (389539)
  --   primary key (style_id,chrome_code));
    -- chrome code has null values

   drop table if exists chr2.tmp_interior_color cascade;
   create table chr2.tmp_interior_color (
     input_row_num	integer,
     passthru_id	integer,
     style_id integer not null references chr2.tmp_style(style_id), -- changed field name 
     install_cause	citext,
     install_cause_detail	citext,
     color_code	citext, -- some nulls (390836)
     color_name	citext not null,
     rgb_value citext);
  --    primary key(style_id,color_code)); nulls in color_code

  drop table if exists chr2.tmp_exterior_color_xref cascade; -- header row in csv has 1 instead of attribute name
  create table chr2.tmp_exterior_color_xref (
    input_row_num integer,
    passthru_id integer,
    exterior_color_name citext not null,
    exterior_color_code citext, -- primary key, Key (exterior_color_code)=(A2A2) already exists, DETAIL:  Failing row contains (170, 390836, Body In Prime, null, Non-Color, t).
    generic_color_name citext not null,
    is_generic_color_primary boolean not null);

  drop table if exists chr2.tmp_exterior_color cascade;
  create table chr2.tmp_exterior_color (
    input_ro_num integer,
    passthru_id integer,
    style_id integer not null, -- changed field name
    install_cause citext,
    install_cause_detail citext,
    color_code citext, -- not null, DETAIL:  Failing row contains (170, 390836, 390836, null, null, null, Body In Prime, null).
    color_name citext, --, same as color_code
    rgb_value citext,
  --   primary key(style_id,color_code),  null color codes
    foreign key (style_id) references chr2.tmp_style(style_id));

  drop table if exists chr2.tmp_engine cascade;
  create table chr2.tmp_engine (
    input_row_num	integer,
    passthru_id	integer,
    engine_type_id	integer not null references chr2.tmp_category_definition(category_id),
    engine_type	citext not null,
    fuel_type_id	integer not null references chr2.tmp_category_definition(category_id),
    fuel_type	citext not null,
    hp_value numeric, -- integer, CONTEXT:  COPY engine, line 2, column hp_value: "354.0"
    hp_rpm	numeric,
    net_torque_value	numeric,
    net_torque_rpm	numeric,
    cylinders	numeric,
    displ_liters	numeric,
    displ_cubic_inch	numeric,
    displ_cubic_cm	numeric,
    fuel_economy_city_low	numeric,
    fuel_economy_city_high	numeric,
    fuel_economy_hwy_low	numeric,
    fuel_economy_hwy_high	numeric,
    fuel_economy_unit	citext,
    fuel_tank_capacity_low	numeric,
    fuel_tank_capacity_high	numeric,
    fuel_tank_capacity_unit	citext,
    forced_induction_id	integer references chr2.tmp_category_definition(category_id),
    forced_induction_type	citext,
    install_cause	citext,
    install_cause_detail	citext,
    is_high_output boolean not null);

  drop table if exists chr2.tmp_image cascade;
  create table chr2.tmp_image (
    input_row_num	integer,
    passthru_id	integer,
    url	citext not null,
    width	integer not null,
    height	integer not null,
    filename citext not null,
    is_view	citext,
    is_colorized citext,
    is_stock boolean not null,
    primary_color_optioncode citext,
    secondary_color_optionCode citext,
    is_match citext,
    shotcode citext,
    style_id integer primary key references chr2.tmp_style(style_id),
    background_description citext,
    primary_rgb_hexcodeRGBHexCode citext,
    secondary_rgb_hexcodeRGBHexCode citext);

  -- turns out vehicle description has no reliably usefull data
  -- drop table if exists chr2.tmp_vehicle_description cascade;  
  -- create table chr2.tmp_vehicle_description (
  --   input_row_num	integer,
  --   passthru_id	integer primary key references chr2.tmp_style(style_id),  -- this is a bit hoaky
  --   vin	citext,
  --   gvwr_low citext,
  --   gvwr_high	citext,
  --   world_manufacturer_identifier	citext,
  --   manufacturer_identification_code citext,
  --   restraint_types	citext,
  --   market_class_id	citext,
  --   model_year citext,
  --   division citext,
  --   model_name citext,
  --   style_name citext,
  --   body_type	citext,
  --   driving_wheels citext,
  --   built	citext,
  --   country	citext,
  --   language citext,
  --   best_make_name citext not null,
  --   best_model_name citext not null,
  --   best_style_name	citext not null,
  --   best_trim_name citext,
  --   base_msrp_low	citext,
  --   base_msrp_high numeric, -- integer, CONTEXT:  COPY vehicle_description, line 2, column base_msrp_high: "51400.0"
  --   base_inv_low numeric,
  --   base_inv_high	numeric,
  --   dest_chrg_low	numeric,
  --   dest_chrg_high numeric,
  --   is_price_unknown boolean not null,
  --   build_data_indicator citext,
  --   built_msrp citext);

  drop table if exists chr2.tmp_standard_equipment cascade;
  create table chr2.tmp_standard_equipment (
    input_row_num integer,
    passthru_id integer,
    header_id integer not null,
    header_name citext not null,
    description citext not null,
    added_cat_ids citext,
    removed_cat_ids citext,
    avail_style_ids integer not null, 
    install_cause citext,
    install_cause_detail citext,
    primary key (header_id,avail_style_ids,description));
   
  -- need to transform the table tmp_body_type into 2 tables: tmp_body_type_ref & tmp_body_types
  -- the ddl for these 2 tables should actually be in chr_batch_tables_step_2
  -- then do the updating of the production tables (body_type_ref & body_types) in chr_2_batch_tables_step_3_update.sql

  drop table if exists chr2.tmp_body_types cascade; -- added s to table name to distinguish from ch2.body_type
  create table chr2.tmp_body_types (
    body_type_id integer primary key,
    body_type_name citext not null);
  create index on chr2.tmp_body_types(body_type_name);

  drop table if exists chr2.tmp_body_type_ref cascade;
  create table chr2.tmp_body_type_ref(
    style_id integer not null references chr2.tmp_style(style_id),
    body_type_id integer not null references chr2.tmp_body_types(body_type_id),
    is_primary boolean not null,
    primary key (style_id,body_type_id,is_primary));
  create index on chr2.tmp_body_type_ref(body_type_id);
  create index on chr2.tmp_body_type_ref(is_primary);  

$BODY$
language SQL;

comment on function chr2.drop_and_recreate_tmp_table_for_batch_processing() is 'in the processing of large batch files, they are initially
loaded into tmp_XXXXXXX tables which need to be purged prior to processing each batch.  part of automating a fundamentally manual process
in batches.py';