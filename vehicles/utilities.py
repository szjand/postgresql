# encoding=utf-8
import psycopg2
import pyodbc
import pypyodbc

# connections ####################################################


def pg(server):
    # if server == '173':  # !!!!!!!!!!!!1 THIS IS FOR TESTING ON LOCAL ONLY, POINTS 173 TO LOCALHOST!!!!!!!!!!!!!!!!!1
    #     # return psycopg2.connect("host='localhost' dbname='cartiva' user='postgres' password='cartiva'")
    #     # 173 is now pointing to pg_standby with a current restored backup from 173
    #     # for testing the deployment of the new python3 luigi
    #     return psycopg2.connect("host='10.130.196.209' dbname='cartiva' user='postgres' password='cartiva'")
    if server == 'local':
        return psycopg2.connect("host='localhost' dbname='cartiva' user='postgres' password='cartiva'")
    elif server == '173':
        return psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'")
    elif server == '174':
        return psycopg2.connect("host='10.130.196.174' dbname='cartiva' user='postgres' password='cartiva'")
    elif server == '88':
        return psycopg2.connect("host='10.130.196.88' dbname='cartiva' user='rydell' password='cartiva'")
    elif server == '73':
        return psycopg2.connect("host='172.17.196.73' dbname='Cartiva' user='postgres' password='cartiva'")
    elif server == '139':  # jon's local vm for luigi dev
        return psycopg2.connect("host='192.168.43.139' dbname='Cartiva' user='postgres' password='cartiva'")


def mysql_shoretel_config():
    return pyodbc.connect("Provider=MSDASQL; DRIVER={MySQL ODBC 5.2a Driver};SERVER=192.168.100.10; "
                          "Port=4308;DATABASE=shoreware;USER=st_configread;PASSWORD=passwordconfigread;OPTION=3;")


def drive_centric():
    return pyodbc.connect('Driver={SQL Server}; Server=52.22.117.38;Database=leadcrumb_daily_copy;'
                          'uid=store-access;pwd=h%Mf4Ze5L#yQ9n*')


def arkona(server):
    if server == 'report':
        return pypyodbc.connect(
            'DRIVER={iSeries Access ODBC Driver};system=REPORT1.ARKONA.COM;uid=rydejon;pwd=fuckyou5')
    elif server == 'production':
        return pypyodbc.connect(
            'DRIVER={iSeries Access ODBC Driver};system=RYDELL.ARKONA.COM;uid=rydejon;pwd=fuckyou5')


def arkona_luigi_27(server):
    if server == 'report':
        return pypyodbc.connect(
            'DRIVER={iSeries Access ODBC Driver 64-bit};system=REPORT1.ARKONA.COM;uid=rydejon;pwd=fuckyou5')
    elif server == 'production':
        return pypyodbc.connect(
            'DRIVER={iSeries Access ODBC Driver 64-bit};system=RYDELL.ARKONA.COM;uid=rydejon;pwd=fuckyou5')


