﻿-- -- line feed as separator
select string_agg(column_name,E'\n')
from information_schema.columns
where table_name = 'base_1' -- enter table name here
  and table_schema= 'chr2' 
-- 
select string_agg(column_name,',')
from information_schema.columns
where table_name = 'base_1' -- enter table name here
  and table_schema= 'chr2'   





-- 7: epa classificxation
-- 8: passentger capacity
-- 11: number of passenger doors
-- 1113: vehicle segment (size)
-- 1114: vehicle_type (shape)
-- for gm, it appears that title_id 40 does have the option code for engines
-- 97 very sparse


-- drop table if exists chr2.tech_specs cascade;
-- create table chr2.tech_specs as
-- select b.style_id, a.title_id, a.group_name, a.header_name, a.title_name, b.condition, b.the_value
-- from chr2.technical_specification_definition a
-- join chr2.tech_spec_value b on a.title_id = b.title_id;
-- create unique index on chr2.tech_specs(style_id,title_id,condition,the_value);
-- alter table chr2.tech_specs add foreign key(style_id) references chr2.style(style_id);
-- create index on chr2.tech_specs(title_id);
-- create index on chr2.tech_specs(style_id);

select * from chr2.tech_specs limit 100

-- 
-- select count(*) from chr2.tech_specs
-- 
-- drop table if exists chr2.bodies cascade;
-- create table chr2.bodies as 
-- select a.style_id, string_agg(distinct body_type_name, '::')::citext as body 
-- from chr2.body_type_ref a
-- join chr2.body_types b on a.body_type_id = b.body_type_id
-- group by a.style_id;
-- alter table chr2.bodies add primary key(style_id);
-- alter table chr2.bodies add foreign key(style_id) references chr2.style(style_id);

-- all style ids have record(s) in tech_spec_value
select a.style_id, b.*
from chr2.style a
left join chr2.tech_spec_value b on a.style_id = b.style_id
where b.style_id is null


select a.style_id, array_agg(distinct b.title_id)
from chr2.style a
left join chr2.tech_spec_value b on a.style_id = b.style_id
group by a.style_id

select count(*)  -- 261891, 527617, 809031, 1070831, 1073807
from chr2.option

select style_id, header_name, descriptions, added_cat_ids, removed_cat_ids, 
  chrome_code, oem_code, is_standard, optionkind_id, utf
-- select *
from chr2.option
order by header_name, utf, optionkind_id
limit 500

select header_name, descriptions, optionkind_id, utf, array_agg(distinct style_id)
-- select *
from chr2.option
group by header_name, descriptions, optionkind_id, utf
order by header_name, utf, optionkind_id

select * from chr2.style limit 10

drop table if exists chr2.base_1 cascade;
create table chr2.base_1 as
select a.style_id, b.model_year, b.division, c.subdivision, d.model, a.mfr_model_code, 
  e.body, a.alt_body_type, 
  case
    when a.drivetrain = 'Front Wheel Drive' then 'FWD'
    when a.drivetrain = 'All Wheel Drive' then 'AWD'
    when a.drivetrain = 'Four Wheel Drive' then '4WD'
    when a.drivetrain = 'Rear Wheel Drive' then 'RWD'
  end as drivetrain,
  a.trim_name, a.style_name, a.style_name_wo_trim, a.pass_doors, l.passenger_capacity,
  case
    when a.alt_body_type like '%cab%' then 
    left(a.alt_body_type, position('Cab' in a.alt_body_type) -1)
  end as cab,
  case 
    when a.alt_body_type like '%Bed%' then   
    replace(
      substring(a.alt_body_type, position('-' in a.alt_body_type) + 2, 
        position('Bed' in a.alt_body_type) -2), 'Bed', '')
  end as bed,
  f.oem_code as peg,
  g.wheelbase, h.overall_length,  
  j.the_value as segment, k.the_value as vehicle_type,
  i.url as image_url
from chr2.style a
join chr2.divisions b on a.division_id = b.division_id and a.model_year = b.model_year
join chr2.subdivisions c on a.subdivision_id = c.subdivision_id and a.model_year = c.model_year
join chr2.models d on a.model_id = d.model_id and a.model_year = d.model_year
left join chr2.bodies e on a.style_id = e.style_id 
left join chr2.option f on a.style_id = f.style_id
  and f.header_name = 'PREFERRED EQUIPMENT GROUP'
left join ( -- wheelbase 
  select style_id, string_agg(the_value, ',') as wheelbase
  from chr2.tech_specs 
  where title_id = 301
  group by style_id) g on a.style_id = g.style_id
left join ( -- overall_length 
  select style_id, string_agg(the_value, ',') as overall_length
  from chr2.tech_specs 
  where title_id = 304
  group by style_id) h on a.style_id = h.style_id
left join chr2.image i on a.style_id = i.style_id
left join chr2.tech_specs j on a.style_id = j.style_id
  and j.title_id = 1113  -- segment
left join chr2.tech_specs k on a.style_id = k.style_id
  and k.title_id = 1114  -- vehicle_type
left join (
  select style_id, string_agg(distinct the_value, ',') as passenger_capacity
  from chr2.tech_specs 
  where title_name = 'passenger capacity'
  group by style_id) l on a.style_id = l.style_id;
alter table chr2.base_1 add primary key(style_id);
alter table chr2.base_1 add foreign key(style_id) references chr2.style(style_id);
create index on chr2.base_1(model_year);
create index on chr2.base_1(model);
create index on chr2.base_1(mfr_model_code);
create index on chr2.base_1(division);

select * from chr2.base_1

select * from chr2.style limit 20

select drivetrain, count(*) from chr2.base_1 group by drivetrain

-- segment/vehicle_type
-- sent chrome an email about missing 2020/20201 data
select a.*, e.the_value as segment, f.the_value as vehicle_type, e.the_value
from chr2.base_1 a
left join chr2.tech_specs e on a.style_id = e.style_id
  and e.title_id = 1113  -- segment
left join chr2.tech_specs f on a.style_id = f.style_id
  and f.title_id = 1114  -- vehicle_type
-- left join chr2.tech_specs g on a.style_id = g.style_id
--   and g.title_id = 7   -- epa classification -- too sporadic to be useful
 where a.division = 'ram'
order by model_year,division, model

-- wheelbase
-- only medium duty ford and chevy have multiple values
select a.style_id, a.subdivision, a.model_year, a.division, a.model, a.trim_name , string_agg(h.the_value, ',') as wheelbase
from chr2.base_1 a
left join chr2.tech_specs h on a.style_id = h.style_id  
  and h.title_id = 301  -- wheelbase adds multiple rows, 14158 -> 14667 
group by a.style_id, a.subdivision, a.model_year, a.division, a.model, a.trim_name  
order by array_length(array_agg(h.the_value), 1) desc

-- overall length
-- many more w/multiple values medium duty, gm 3500, ford super duty
select a.style_id, a.subdivision, a.model_year, a.division, a.model, a.trim_name , string_agg(i.the_value, ',') as length
from chr2.base_1 a
left join chr2.tech_specs i on a.style_id = i.style_id
  and i.title_id = 304  -- more multiple rows 14667 -> 15837
group by a.style_id, a.subdivision, a.model_year, a.division, a.model, a.trim_name    
order by array_length(array_agg(i.the_value), 1) desc 

-- fuel capacity
-- 2046 with multiple values, guessing it has to do with multiple engines available
select a.style_id, a.subdivision, a.model_year, a.division, a.model, a.trim_name , string_agg(j.the_value, ',') as fuel_capacity
from chr2.base_1 a
left join chr2.tech_specs j on a.style_id = j.style_id
  and j.title_id = 206  -- fuel capacity  -- more multiples 15837 -> 20711
group by a.style_id, a.subdivision, a.model_year, a.division, a.model, a.trim_name    
order by array_length(array_agg(j.the_value), 1) desc   

----------------------------------------------------------------------------------------
--< image_url
----------------------------------------------------------------------------------------
-- 478 without a url
select *
from chr2.base_1
where image_url is null

-- scattered accross model_years and makes
select model_year, division, count(*)
from chr2.base_1
where image_url is null
group by model_year, division
order by model_year, division

2021 escalade
select *
from chr2.image
where style_id = '413263'

select * from nc.vehicles a
join chr.describe_vehicle b on a.vin = b.vin
where model = 'escalade' and model_year = 2021


-- a few hondas based on vin, but no pictures in chr.describe vehicle either
select a.style_id, a.model_year, a.division, b.vin, c.vin, f.style -> 'stockImage' -> 'attributes' ->> 'url' as picture_url
from chr2.base_1 a 
left join nc.vehicles b on a.style_id = b.chrome_style_id::integer
left join chr.describe_vehicle c on b.vin = c.vin
left join jsonb_array_elements(c.response->'style') as f(style) on true 
where a.image_url is null
  and c.vin is not null 

-- no pictures from either
select a.style_id, a.model_year, a.division, f.style -> 'stockImage' -> 'attributes' ->> 'url' as picture_url
from chr2.base_1 a 
left join nc.vehicles b on a.style_id = b.chrome_style_id::integer
left join jon.describe_vehicle_by_style_id c on a.style_id = c.chrome_style_id::integer
left join jsonb_array_elements(c.response->'style') as f(style) on true 
where a.image_url is null



  
select model_year, division, model, min(image_url), max(image_url), count(*)
from chr2.base_1
group by model_year, division, model
order by model_year, division, model

----------------------------------------------------------------------------------------
--/> image_url
----------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------
--< rear seats
----------------------------------------------------------------------------------------

select *
from chr2.option
limit 100

select header_name, count(*)
from chr2.option
group by header_name
order by header_name

select *
from chr2.option
where header_name = 'rear seat'

select *
from chr2.standard_equipment
limit 100

select header_name, count(*)
from chr2.standard_Equipment
group by header_name
order by header_name

select description, count(*)
from chr2.standard_equipment
where header_name = 'interior'
  and description like '%rear seat%'
group by description
order by description

select b.division, b.model, a.description, count(*)
from chr2.standard_equipment a
join chr2.base_1 b on a.style_id = b.style_id
where a.header_name = 'interior'
  and a.description like '%rear seat%'
  and a.description not like '%driver%'
group by b.division, b.model, a.description
order by b.division, b.model, a.description



select a.style_id, a.model_year, a.division, a.model, a.trim_name, b.description
from chr2.base_1 a
left join chr2.standard_equipment b on a.style_id = b.style_id
  and b.header_name = 'interior'
  and b.description like '%rear seat%'
  and b.description not like '%tri-zone%'
where a.model = 'suburban'


select *
from chr2.standard_equipment a
where a.header_name = 'interior'
  and exists (
    select 1
    from chr2.base_1
    where style_id = a.style_id
      and model_year = 2018)


select a.division, a.model, b.header_name, count(*)
from chr2.base_1 a
join chr2.standard_equipment b on a.style_id = b.style_id
where a.model_year = 2018
group by a.division, a.model, b.header_name
order by a.division, a.model, b.header_name

----------------------------------------------------------------------------------------
--/< rear seats
----------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------
--< dually
----------------------------------------------------------------------------------------
select *
from chr2.standard_equipment
where header_id = 1176
  and added_cat_ids like '%1044%'

select *
from chr2.category_definition
where category_id = '1044'

select *
from chr2.option
limit 100

select *
from chr2.tech_specs
limit 100

select header_name, title_name, the_value
from chr2.tech_specs
where title_name like '%dual%'
group by header_name, title_name, the_value


----------------------------------------------------------------------------------------
--/< dually
----------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------
--< passenger capacity
----------------------------------------------------------------------------------------

-- the lesson here, is i am combing thru describe_vehicle & describe_vehicle_by_style_id looking for seat, seating,
--   seats, passenger, capacity
-- no luck, finally realized it is in tech specs, and tech specs in the json objects just have the titleId and value
--    not group_name or header_name or title_name, just title_id:
--   "technicalSpecification": [
--     {
--       "titleId": 8,
--       "definition": null,
--       "range": {
--         "min": 5.0,
--         "max": 5.0
--       },
--       "value": [
--         {
--           "styleId": [
--             402210
--           ],
--           "value": "5",
--           "condition": ""
--         }
--       ]
--     }]


    
select *
from chr2.option
limit 100

select distinct header_id, header_name, descriptions, added_Cat_ids from chr2.option where descriptions like '%passenger%'

select * from chr2.category_definition where category_id in (1011,1034,1266,1191,1269,1325)
select * from chr2.category_definition where header_name = 'seats'
select distinct group_name, header_name from chr2.category_definition order by group_name, header_name

select *
from chr2.standard_equipment
limit 100

select distinct header_id, header_name from chr2.standard_equipment
select distinct description from chr2.standard_equipment where header_id = 1220 and description like '%seat%'

select * from chr2.tech_specs limit 10
select group_name, header_name from chr2.tech_specs group by group_name, header_name order by group_name, header_name

select * from chr2.tech_specs where header_name = 'interior dimensions' and title_name = 'passenger capacity' limit 100

select a.style_id, a.division, a.model, string_agg(distinct b.the_value, ',')
from chr2.base_1 a
left join chr2.tech_specs b on a.style_id = b.style_id
  and b.title_name = 'passenger capacity'
group by a.style_id, a.division, a.model
order by a.style_id



----------------------------------------------------------------------------------------
--/> passenger capacity
----------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------
--< towing capacity -- maximum trailering capacity
----------------------------------------------------------------------------------------


select *
from chr2.tech_specs
where header_name = 'frame'


select title_name, the_value
from chr2.tech_specs
where header_name = 'frame'
  and title_name = 'frame type'
group by title_name, the_value


select *
from chr2.tech_specs
where header_name = 'Trailering'
  and title_name = 'maximum trailering capacity'
limit 100

select style_id, string_agg(the_value, ',')
from chr2.tech_specs
where header_name = 'Trailering'
  and title_name = 'maximum trailering capacity'
group by style_id

select *
from chr2.technical_specification_definition
limit 100

----------------------------------------------------------------------------------------
--/> towing capacity -- maximum trailering capacity
----------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------
--< side door type
----------------------------------------------------------------------------------------
select distinct title_name, the_value
from chr2.tech_specs
where header_name = 'doors'

----------------------------------------------------------------------------------------
--/> side door type
----------------------------------------------------------------------------------------

need to look over tech_specs again

select * from chr2.tech_specs limit 10

-- narrow down to what could be included in the type table
select group_name, header_name, count(*)
from chr2.tech_specs
group by group_name, header_name
order by group_name, header_name

-- header: vehicle ----------------------------------------------------
-- model group, segment, type, cab, bed
-- would be good if all the data was there
select title_id, title_name, count(*)
from chr2.tech_specs
where header_name = 'vehicle'
group by title_id, title_name
order by title_id

-- meh 
select title_name, the_value, count(*)
from chr2.tech_specs
where header_name = 'vehicle'
  and title_id in (1113,1114,1115,1116)
group by title_name, the_value  
-- header: vehicle ----------------------------------------------------

--< transmission ------------------------------------------------------
tech specs vs options vs standard equip
jon.transmissions uses options

tech specs: header_name = 'transmisison'


selecT chrome_Style_id, count(*)
from jon.transmissions
group by chrome_Style_id
having count(*) > 1
order by count(*) desc 

select * from jon.transmissions where chrome_style_id = '405309'

select * from chr2.base_1 where style_id = 405309  -- 3500HD

select * from chr2.tech_specs where header_name = 'transmission' and style_id = 405309 order by title_id

-- this matches jon.transmissions
select * from chr2.option where header_name = 'transmission' and style_id = 405309
-- select * from chr2.category_definition where category_id in (1130,1328,1104,1131)

-- no values for install_cause or install_cause_detail
select distinct install_cause from chr2.option where header_name = 'transmission' and install_cause_detail is not null

-- as i would expect, lists a single transmission
select * from chr2.standard_equipment where style_id = 405309 and (description like 'Transmission:%' or description like 'Transmission,%')

-- no missing style_ids
select * 
from chr2.base_1 a
where not exists (
  select 1
  from chr2.option
  where style_id = a.style_id)

drop table if exists chr2.transmissions cascade;
create table chr2.transmissions as
select style_id, coalesce(oem_code, 'none') as oem_code, descriptions as transmission
from chr2.option
where header_name = 'transmission';
alter table chr2.transmissions add foreign key(style_id) references chr2.style(style_id);
alter table chr2.transmissions add primary key(style_id,transmission,oem_code);

--/< transmission ------------------------------------------------------

--< engine ------------------------------------------------------

select * from chr2.option where header_name = 'engine' limit 100

--/< engine ------------------------------------------------------


























