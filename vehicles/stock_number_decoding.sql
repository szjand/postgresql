﻿select -- min(fromts::date), max(fromts::date),
  trim(replace( -- 9
    replace( --8
      replace( -- 7
        replace( -- 6
          replace( -- 5
            replace( -- 4
              replace( -- 3
                replace( -- 2
                  replace( -- 1
                    replace( -- 0
                      right(stocknumber,4),'0',''),'1',''),'2',''),'3',''),'4',''),'5',''),'6',''),'7',''),'8',''),'9',''))::citext
from ads.ext_vehicle_inventory_items
group by   replace( -- 9
    replace( --8
      replace( -- 7
        replace( -- 6
          replace( -- 5
            replace( -- 4
              replace( -- 3
                replace( -- 2
                  replace( -- 1
                    replace( -- 0
                      right(stocknumber,4),'0',''),'1',''),'2',''),'3',''),'4',''),'5',''),'6',''),'7',''),'8',''),'9','')

union
select 
  trim(replace( -- 9
    replace( --8
      replace( -- 7
        replace( -- 6
          replace( -- 5
            replace( -- 4
              replace( -- 3
                replace( -- 2
                  replace( -- 1
                    replace( -- 0
                      right(inpmast_stock_number,4),'0',''),'1',''),'2',''),'3',''),'4',''),'5',''),'6',''),'7',''),'8',''),'9',''))
from arkona.ext_inpmast
where type_n_u = 'U'
group by   replace( -- 9
    replace( --8
      replace( -- 7
        replace( -- 6
          replace( -- 5
            replace( -- 4
              replace( -- 3
                replace( -- 2
                  replace( -- 1
                    replace( -- 0
                      right(inpmast_stock_number,4),'0',''),'1',''),'2',''),'3',''),'4',''),'5',''),'6',''),'7',''),'8',''),'9','')


select 
  replace( -- 9
    replace( --8
      replace( -- 7
        replace( -- 6
          replace( -- 5
            replace( -- 4
              replace( -- 3
                replace( -- 2
                  replace( -- 1
                    replace( -- 0
                      right(stocknumber,4),'0',''),'1',''),'2',''),'3',''),'4',''),'5',''),'6',''),'7',''),'8',''),'9','') as stock_number_suffix, count(*)
from (                                         
  select stocknumber from ads.ext_vehicle_inventory_items
  union 
  select inpmast_stock_number from arkona.ext_inpmast where type_n_u = 'U') a
group by   
  replace( -- 9
    replace( --8
      replace( -- 7
        replace( -- 6
          replace( -- 5
            replace( -- 4
              replace( -- 3
                replace( -- 2
                  replace( -- 1
                    replace( -- 0
                      right(stocknumber,4),'0',''),'1',''),'2',''),'3',''),'4',''),'5',''),'6',''),'7',''),'8',''),'9','')
order by stock_number_suffix                      
         