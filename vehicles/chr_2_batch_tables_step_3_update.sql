﻿ 
-- in this order 'style', 'technical_specification_definition', 'tech_spec_value', 'body_type',
--                   'category_definition', 'option', 'interior_color', 'exterior_color_xref',
--                   'exterior_color', 'engine', 'image', 'vehicle_description' 
-- 
-- update the chr2 tables with data from the chr2.tmp_ tables

-- -- generate list of column names
-- select string_agg('excluded.' || column_name,',')
-- -- select string_agg(column_name,',')
-- from information_schema.columns
-- where table_name = 'standard_equipment' -- enter table name here
--   and table_schema= 'chr2'  

-------------------------------------------------------------------------------
--< alterations for the first load of production tables model year 2018 run once only
-------------------------------------------------------------------------------

-- -- chr2.tech_spec_value
-- update chr2.tech_spec_value
-- set condition = 'none'
-- where condition is null;
-- 
-- update chr2.tech_spec_value
-- set the_value = 'none'
-- where the_value is null;
-- 
-- alter table chr2.tech_spec_value add primary key(style_id,title_id,condition, the_value);
-- 
-- -- chr2.body_type
-- insert into chr2.body_types
-- select body_type_id, body_type_name
-- from chr2.body_type
-- group by body_type_id, body_type_name;
-- 
-- insert into chr2.body_type_ref(style_id,body_type_id,is_primary)
-- select style_id, body_type_id, is_primary
-- from chr2.body_type
-- group by style_id, body_type_id, is_primary;
-- 
-- -- chr2.option
-- update chr2.option
-- set chrome_code = 'none'
-- where chrome_code is null;
-- 
-- update chr2.option
-- set ambiguous_option_id = 0
-- where ambiguous_option_id is null;
-- 
-- alter table chr2.option add primary key(style_id,chrome_code,ambiguous_option_id);
-- 
-- -- chr2.interior_color
-- update chr2.interior_color
-- set color_code = 'none'
-- where color_code is null;

-- alter table chr2.interior_color add primary key(style_id,color_code,color_name);

-- -- chr2.generic_exterior_colors
-- drop table if exists chr2.generic_exterior_colors cascade;
-- create table chr2.generic_exterior_colors (
--   exterior_color_name citext not null,
--   exterior_color_code citext not null,
--   generic_color_name citext not null,
--   is_generic_color_primary boolean not null,
--   primary key(exterior_color_name,exterior_color_code,generic_color_name,is_generic_color_primary));
-- create index on chr2.generic_exterior_colors(exterior_color_name);
-- create index on chr2.generic_exterior_colors(exterior_color_code);
-- 
-- insert into chr2.generic_exterior_colors
-- select exterior_color_name,coalesce(exterior_color_code, 'none'),generic_color_name,is_generic_color_primary
-- from chr2.exterior_color_xref
-- group by exterior_color_name,coalesce(exterior_color_code, 'none'),generic_color_name,is_generic_color_primary;

-- -- chr2.exterior_color
-- update chr2.exterior_color
-- set color_code = 'none'
-- where color_code is null;
-- -- and add the PK
-- alter table chr2.exterior_color add primary key(style_id,color_code);

-------------------------------------------------------------------------------
--/> alterations for the first load of production tables model year 2018 run once only
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--< chr.style
-------------------------------------------------------------------------------

-- style, should be all new rows, until we get into updating existing model years

insert into chr2.style
select input_row_number,passthru_id,style_id,division_id,subdivision_id,model_id,base_msrp,
  base_invoice,dest_chrg,is_price_unknown,market_class_id,model_year,style_name,
  style_name_wo_trim,trim_name,mfr_model_code,is_fleet_only,is_model_fleet,pass_doors,
  alt_model_name,alt_style_name,alt_body_type,drivetrain 
from chr2.tmp_style;


-------------------------------------------------------------------------------
--/> chr.style
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--< chr.technical_specification_definition
-------------------------------------------------------------------------------
  
-- technical_specification_definition, very few if any changes, PK = title_id
insert into chr2.technical_specification_definition
select group_id,group_name,header_id,header_name,title_id,title_name,measurement_unit,country,language
from chr2.tmp_technical_specification_definition
on conflict (title_id)
do update 
  set (
    group_id,group_name,header_id,header_name,title_name,measurement_unit,country,language)
  = (
    excluded.group_id,excluded.group_name,excluded.header_id,excluded.header_name,
    excluded.title_name,excluded.measurement_unit,excluded.country,excluded.language);

-------------------------------------------------------------------------------
--/> chr.technical_specification_definition
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--< chr.tech_spec_value
-------------------------------------------------------------------------------

-- chromes data model specifies condition as part of the PK
-- PG wont let null values be part of the PK
-- so, populate the table w/o a PK
-- update condition where null to some inocuous value ('none')
-- then re-apply the PK
-- turns out that the alledged PK is not unique
-- complain to chrome, but in the meantime, include the_value in the PK
-- shit, fucking the_value has null values too 

update chr2.tmp_tech_spec_value
set condition = 'none'
where condition is null;

update chr2.tmp_tech_spec_value
set the_value = 'none'
where the_value is null;

alter table chr2.tmp_tech_spec_value add primary key(style_id,title_id,condition, the_value);

-- -- todo send this to chrome and ask WTF
-- -- the fucking values are different
-- select *
-- from chr2.tech_spec_value
-- where style_id = 393491
--   and title_id = 33
--   and condition = 'DMF&(WLA&(2FG-R,2FG-F))'
-- -- this is from 2018
-- 1336;393491;33;22520;DMF&(WLA&(2FG-R,2FG-F));393491
-- 1336;393491;33;22550;DMF&(WLA&(2FG-R,2FG-F));393491

-- -- should be all new rows until we get into updating existing model years  
-- insert into chr2.tech_spec_value
-- select input_row_num,passthru_id,title_id,the_value,condition,style_id
-- from chr2.tmp_tech_spec_value;
-- 
-- no need to persist with chr2.tech_spec_value, 
-- update the null fields in chr2.tmp_tech_spec_value
-- then insert then new data into tech_specs with:
-- !!! REMEMBER   THIS IS FOR ONLY ADDING THE NEW (2005-2017) DATA, STILL HAVE TO FIGURE OUT PERIODIC UPDATING !!!
insert into chr2.tech_specs(style_id,title_id,group_name, header_name,title_name,condition,the_value)
select b.style_id, a.title_id, a.group_name, a.header_name, a.title_name, b.condition, b.the_value
from chr2.technical_specification_definition a
join chr2.tmp_tech_spec_value b on a.title_id = b.title_id;

-------------------------------------------------------------------------------
--/> chr.tech_spec_value
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--< chr.body_type
-------------------------------------------------------------------------------
-- -- 1/22/21
-- -- don't need these 2 tables, as they are going to be turned into a single table anyway
-- 
-- -- batch returns a single file body_type.csv that needs to be parsed into 2 tables 
-- insert into chr2.body_types (body_type_id,body_type_name)
-- select body_type_id, body_type_name
-- from chr2.tmp_body_type
-- group by body_type_id, body_type_name
-- on conflict (body_type_id)
--   do update
--     set (body_type_name) = (excluded.body_type_name);
-- 
-- insert into chr2.body_type_ref(style_id,body_type_id,is_primary)
-- select a.*
-- from (
--   select style_id, body_type_id, is_primary
--   from chr2.tmp_body_type
--   group by style_id, body_type_id, is_primary) a
-- where not exists (
--   select 1 
--   from chr2.body_type_ref  
--   where style_id = a.style_id
--     and body_type_id = a.body_type_id
--     and is_primary = a.is_primary);
-- 

-- should not be any conflicts. YET, only doing new model years, so should be
-- unique style_ids
-- also i a freaking about on conflict do update actually (effectively) updating every row
-- on large tables that create bloating and vacuuming issues that i would rather avoid.

insert into chr2.bodies (style_id, body)
select style_id, string_agg(distinct body_type_name, '::')::citext as body 
from chr2.tmp_body_type
group by style_id;
-------------------------------------------------------------------------------
--/> chr.body_type
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--< chr.category_definition
-------------------------------------------------------------------------------

-- category_definition, very few if any changes, PK = category_id
insert into chr2.category_definition
select group_id,group_name,header_id,header_name,category_id,category_name,type_id,type_name,country,language
from chr2.tmp_category_definition
on conflict (category_id)
do update 
  set (
    group_id,group_name,header_id,header_name,category_name,type_id,type_name,country,language)
  = (
    excluded.group_id,excluded.group_name,excluded.header_id,excluded.header_name,excluded.category_name,
    excluded.type_id,excluded.type_name,excluded.country,excluded.language);

  
-------------------------------------------------------------------------------
--/> chr.category_definition
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--< chr.option
-------------------------------------------------------------------------------

-- -- field description in erd is descriptions in file
-- -- erd says PK is style_id/chrome_code, but many null chrome_code, even coalesced this is not unique
-- -- ok, here is a candidate key lets give it a shot
-- select style_id, coalesce(chrome_code, 'none'), coalesce(ambiguous_option_id, 0)
-- from chr2.option
-- group by style_id, coalesce(chrome_code, 'none'), coalesce(ambiguous_option_id, 0)
-- having count(*) > 1
-- 
-- select style_id, coalesce(chrome_code, 'none'), coalesce(ambiguous_option_id, 0)
-- from chr2.tmp_option
-- group by style_id, coalesce(chrome_code, 'none'), coalesce(ambiguous_option_id, 0)
-- having count(*) > 1

-- 01/21/21 processing 2005, found null header_id and header_name, update them to non null values

-- update the tmp tables
update chr2.tmp_option
set chrome_code = 'none'
where chrome_code is null;

update chr2.tmp_option
set ambiguous_option_id = 0
where ambiguous_option_id is null;

update chr2.tmp_option
set header_id = 0
where header_id is null;

update chr2.tmp_option
set header_name = 'none'
where header_name is null;

alter table chr2.tmp_option add primary key(style_id,chrome_code,ambiguous_option_id);

insert into chr2.option
select input_row_num,passthru_id,ambiguous_option_id,header_id,header_name,descriptions,
  added_cat_ids,removed_cat_ids,msrp_low,msrp_high,invoice_low,invoice_high,
  is_unknown_price,style_id,install_cause,install_cause_detail,chrome_code,
  oem_code,alt_optioncode,is_standard,optionkind_id,utf,is_fleet_only
from chr2.tmp_option
on conflict (style_id,chrome_code,ambiguous_option_id)
do update
  set (
    input_row_num,passthru_id,header_id,header_name,descriptions,added_cat_ids,
    removed_cat_ids,msrp_low,msrp_high,invoice_low,invoice_high,is_unknown_price,
    install_cause,install_cause_detail,oem_code,alt_optioncode,is_standard,
    optionkind_id,utf,is_fleet_only)
  = (
    excluded.input_row_num,excluded.passthru_id,excluded.header_id,excluded.header_name,excluded.descriptions,
    excluded.added_cat_ids,excluded.removed_cat_ids,excluded.msrp_low,excluded.msrp_high,excluded.invoice_low,
    excluded.invoice_high,excluded.is_unknown_price,excluded.install_cause,excluded.install_cause_detail,
    excluded.oem_code,excluded.alt_optioncode,excluded.is_standard,excluded.optionkind_id,excluded.utf,
    excluded.is_fleet_only);
    
select min(header_id) from chr2.option  
-------------------------------------------------------------------------------
--/> chr.option
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--< chr.interior_color
-------------------------------------------------------------------------------


-- -- great, no color code, but multiple color names
-- -- at least it is a valid candidate key, dont know how much use it will be
-- select style_id, coalesce(color_code), color_name
-- from chr2.interior_color
-- group by style_id, coalesce(color_code), color_name
-- having count(*) > 1
-- 
-- select style_id, coalesce(color_code), color_name
-- from chr2.tmp_interior_color
-- group by style_id, coalesce(color_code), color_name
-- having count(*) > 1


update chr2.tmp_interior_color
set color_code = 'none'
where color_code is null;

alter table chr2.tmp_interior_color add primary key(style_id,color_code,color_name);

insert into chr2.interior_color
select input_row_num,passthru_id,style_id,install_cause,install_cause_detail,
  color_code,color_name,rgb_value
from chr2.tmp_interior_color
on conflict (style_id,color_code,color_name)
do update 
  set (
    input_row_num,passthru_id,install_cause,install_cause_detail,rgb_value)
  = (
    excluded.input_row_num,excluded.passthru_id,excluded.install_cause,
    excluded.install_cause_detail,excluded.rgb_value);
-------------------------------------------------------------------------------
--/> chr.interior_color
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
--< chr.exterior_color_xref
-------------------------------------------------------------------------------
-- -- so after all of this, decided don't need this table it would only provide a generic color
-- -- which is currently of no use, available if ever needed
-- -- ah shit, this is not about vehicle types, this is about the snowflaked ext_colors table, can see a use for generic
-- -- coalesce the nulls, group
-- select exterior_color_code, exterior_color_name
-- from chr2.exterior_color_xref
-- group by exterior_color_code, exterior_color_name
-- having count(*) > 1  
-- 
-- select * from chr2.exterior_color_xref where exterior_color_code = 'gga'
-- -- the only null color_code is for Body In Prime, whatever that means
-- select * from chr2.exterior_color_xref where exterior_color_code is null
-- 
-- for at least some, when there are multiple generic color for passthru(style_id) and color code, the diff is the primary
-- does this explain the hondas with multiple color codes?
-- nope, multiple code, same is_primary, they just have multiple codes for the same color
-- select a.*, b.model
-- from chr2.exterior_color_xref a
-- join jon.configurations b on a.passthru_id = b.chrome_style_id::integer
--   and b.make = 'honda'
-- order by a.passthru_id, a.exterior_color_code  

-- select coalesce(exterior_color_code, 'none'), exterior_color_name, generic_color_name, is_generic_color_primary, array_agg(distinct passthru_id), count(*)
-- from chr2.exterior_color_xref
-- group by coalesce(exterior_color_code, 'none'), exterior_color_name, generic_color_name, is_generic_color_primary
-- having count(*) > 1  
-- 
-- select * from chr2.exterior_color_xref limit 100
-- there is no PK, so group them the 4 data fields and use that
-- take out the input_row_num and passthru_id and we have a brand new table


-- now update production table with new data

insert into chr2.generic_exterior_colors
select a.*
from (
  select exterior_color_name,coalesce(exterior_color_code, 'none') as exterior_color_code,generic_color_name,is_generic_color_primary
  from chr2.tmp_exterior_color_xref
  group by exterior_color_name,coalesce(exterior_color_code, 'none'),generic_color_name,is_generic_color_primary) a
left join chr2.generic_exterior_colors b on a.exterior_color_name = b.exterior_color_name
  and a.exterior_color_code = b.exterior_color_code
  and a.generic_color_name = b.generic_color_name
  and a.is_generic_color_primary = b.is_generic_color_primary
where b.exterior_color_name is null; 
-------------------------------------------------------------------------------
--/> chr.exterior_color_xref
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--< chr.exterior_color
-------------------------------------------------------------------------------

-- -- the only null color_code is for Body In Prime, whatever that means
-- select * from chr2.exterior_color where color_code is null

insert into chr2.exterior_color
select input_ro_num,passthru_id,style_id,install_cause,install_cause_detail,
  coalesce(color_code, 'none'),color_name,rgb_value
from chr2.exterior_color
on conflict (style_id,color_code)
do update
  set (input_ro_num,passthru_id,install_cause,install_cause_detail,color_name,rgb_value)
  = (
    excluded.input_ro_num,excluded.passthru_id,excluded.install_cause,
    excluded.install_cause_detail,excluded.color_name,excluded.rgb_value);
    
-------------------------------------------------------------------------------
--/> chr.exterior_color
-------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------
-- --< chr.engine
-- -------------------------------------------------------------------------------

-- 
-- 12/6/20
-- not having any luck, in jon.engines, the pk id style_id/option_code, this file has neither value
-- will have to return to engines
-- one possiblity is to group on basic attributes(type,fuel,displ, fuel) but no way to tie to style_id
-- 
-- this is going to be a tough PK to find
-- 
-- select count(*) -- 5321
-- from chr2.engine
-- 
-- select * from chr2.engine order by passthru_id limit 500
-- 
-- select passthru_id from chr2.engine group by passthru_id having count(*) > 1
-- 
-- select * from chr2.engine where engine_type_id is null or fuel_type_id is null
-- 
-- select passthru_id, engine_type_id, fuel_type_id, displ_liters, coalesce(displ_cubic_inch, 0)
-- from chr2.engine
-- group by passthru_id, engine_type_id, fuel_type_id, displ_liters, coalesce(displ_cubic_inch, 0)
-- having count(*) > 1
-- order by passthru_id
-- 
-- select *
-- from chr2.engine
-- where passthru_id = 393337
--   and engine_type_id = 1052
--   and fuel_Type_id = 1059
--   and displ_liters = 6.0
-- 
-- -------------------------------------------------------------------------------
-- --/> chr.engine
-- -------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--< chr.image
-------------------------------------------------------------------------------

insert into chr2.image  
select input_row_num,passthru_id,url,width,height,filename,is_view,is_colorized,
  is_stock,primary_color_optioncode,secondary_color_optioncode,is_match,shotcode,
  style_id,background_description,primary_rgb_hexcodergbhexcode,
  secondary_rgb_hexcodergbhexcode
from chr2.tmp_image
on conflict (style_id)
do update
  set (
    input_row_num,passthru_id,url,width,height,filename,is_view,is_colorized,is_stock,
    primary_color_optioncode,secondary_color_optioncode,is_match,shotcode,background_description,
    primary_rgb_hexcodergbhexcode,secondary_rgb_hexcodergbhexcode)
  = (
    excluded.input_row_num,excluded.passthru_id,excluded.url,excluded.width,excluded.height,
    excluded.filename,excluded.is_view,excluded.is_colorized,excluded.is_stock,
    excluded.primary_color_optioncode,excluded.secondary_color_optioncode,excluded.is_match,
    excluded.shotcode,excluded.background_description,excluded.primary_rgb_hexcodergbhexcode,
    excluded.secondary_rgb_hexcodergbhexcode);

-------------------------------------------------------------------------------
--/> chr.image
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--< chr.vehicle_description
-------------------------------------------------------------------------------
-- i am not convinced this table has anything of use, maybe best_model_name, best_Style_name, best_trim_name
-- then on processing 2020, best_make_name is null, take it out of the parsing all together
-- pass on it for now
-- select * from chr2.vehicle_description 
-- union all
-- select * from chr2.tmp_vehicle_description limit 50

  
-------------------------------------------------------------------------------
--/> chr.vehicle_description
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--< chr.standard_equipment
-------------------------------------------------------------------------------

insert into chr2.standard_equipment
select input_row_num,passthru_id,header_id,header_name,description,added_cat_ids,
  removed_cat_ids,avail_style_ids,install_cause,install_cause_detail
from chr2.tmp_standard_equipment
on conflict (header_id,style_id,description)
do update
  set (header_name,added_cat_ids,removed_cat_ids,install_cause,install_cause_detail)
  = (excluded.header_name,excluded.added_cat_ids,excluded.removed_cat_ids,
    excluded.install_cause,excluded.install_cause_detail);
  

select count(*)  --1133902, 1478443, 1481199
from chr2.standard_equipment



-------------------------------------------------------------------------------
--/> chr.standard_equipment
-------------------------------------------------------------------------------







