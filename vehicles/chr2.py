# encoding=utf-8
"""
"""
import utilities
import requests
import datetime
import csv
import ftplib


pg_server = '173'


def version():
    run_date = datetime.datetime.today().strftime('%m/%d/%Y')
    url = 'https://beta.rydellvision.com:8888/chrome/version-info'
    data = requests.get(url)
    data = data.text.replace("'", "''")  # .encode("utf-8")
    with utilities.pg(pg_server) as pg_con:
        with pg_con.cursor() as table_cur:
            sql = """
                insert into chr2.version_info(the_date,response)
                values ('{0}','{1}');
            """.format(run_date, data)
            table_cur.execute(sql)


def model_years():
    url = 'https://beta.rydellvision.com:8888/chrome/model-years'
    data = requests.get(url)
    with utilities.pg(pg_server) as pg_con:
        with pg_con.cursor() as chr_cur:
            chr_cur.execute("truncate chr2.get_model_years")
            sql = """
                insert into chr2.get_model_years(model_years)
                values ('{}');
            """.format(data.text)
            chr_cur.execute(sql)
            chr_cur.execute("select chr2.xfm_model_years()")


def divisions():
    with utilities.pg(pg_server) as pg_con:
        with pg_con.cursor() as model_year_cur:
            sql = """
                select model_year
                from chr2.model_years
                where model_year > 2017;
            """
            model_year_cur.execute(sql)
            for model_year in model_year_cur:
                url = 'https://beta.rydellvision.com:8888/chrome/divisions/' + str(model_year[0])
                data = requests.get(url)
                with pg_con.cursor() as chr_cur:
                    chr_cur.execute("truncate chr2.get_divisions")
                    sql = """
                        insert into chr2.get_divisions(model_year,divisions)
                        values ({0},'{1}');
                    """.format(model_year[0], data.text)
                    chr_cur.execute(sql)
                    chr_cur.execute("select chr2.xfm_divisions()")


def subdivisions():
    with utilities.pg(pg_server) as pg_con:
        with pg_con.cursor() as model_year_cur:
            sql = """
                select model_year
                from chr2.model_years
                where model_year between 2018 and 2022;
            """
            model_year_cur.execute(sql)
            for model_year in model_year_cur:
                url = 'https://beta.rydellvision.com:8888/chrome/subdivisions/' + str(model_year[0])
                print(url)
                data = requests.get(url)
                with pg_con.cursor() as chr_cur:
                    chr_cur.execute("truncate chr2.get_subdivisions")
                    sql = """
                        insert into chr2.get_subdivisions(model_year,subdivisions)
                        values ({0},'{1}');
                    """.format(model_year[0], data.text)
                    chr_cur.execute(sql)
                    chr_cur.execute("select chr2.xfm_subdivisions()")


def models():
    with utilities.pg(pg_server) as pg_con:
        with pg_con.cursor() as get_models_cur:
            get_models_cur.execute("truncate chr2.get_models")
        with pg_con.cursor() as division_cur:
            sql = """
                select model_year, division_id
                from chr2.divisions
                where division in('Acura','Audi','BMW','Buick','Cadillac','Chevrolet','Chrysler','Dodge','Ford',
                    'GMC','Honda','Hyundai','INFINITI','Jaguar','Jeep','Kia','Land Rover','Lexus','Lincoln','Mazda',
                    'Mercedes-Benz','MINI','Mitsubishi','Nissan','Porsche','Ram','smart','Subaru','Tesla','Toyota',
                    'Volkswagen','Volvo');
            """
            division_cur.execute(sql)
            for division in division_cur:
                payload = {"year": division[0], "divisionId": division[1]}
                url = 'https://beta.rydellvision.com:8888/chrome/models'
                json_data = requests.post(url, data=payload)
                with pg_con.cursor() as table_cur:
                    sql = """
                        insert into chr2.get_models(model_year,division_id,models)
                        values ({0},{1},'{2}');
                    """.format(division[0], division[1], json_data.text)
                    table_cur.execute(sql)
        with pg_con.cursor() as models_cur:
            models_cur.execute("select chr2.xfm_models()")


def style_ids():
    """
    takes about 1/2 hour on the initial test of 4 years and 32 makes
    keeps failing on the function in a nonsensical wway

    """
    i = 0
    with utilities.pg(pg_server) as pg_con:
        with pg_con.cursor() as get_styles_cur:
            get_styles_cur.execute("truncate chr2.get_style_ids")
        with pg_con.cursor() as model_cur:
            model_sql = """
                select model_id
                from chr2.models;
            """
            model_cur.execute(model_sql)
            for model_id in model_cur:
                url = 'https://beta.rydellvision.com:8888/chrome/styles/' + str(model_id[0])
                data = requests.get(url)
                data = data.text.replace("'", "''")

                with pg_con.cursor() as table_cur:
                    sql = """
                        insert into chr2.get_style_ids(model_id,style_ids)
                        values ({0},'{1}');
                    """.format(model_id[0], data)
                    table_cur.execute(sql)
                    # print(model_id[0])
                    i += 1
                    print(i)
        # commit so we don't lose the data in the event of the function failing
        pg_con.commit()
        with pg_con.cursor() as styles_cur:
            styles_cur.execute("select chr2.xfm_style_ids()")


def batch_upload():
    """

    """
    model_year = 2022
    datetime.datetime.now()
    upload_file_path = 'batches/requests/'
    upload_file_name = str('{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now())) + '_' + str(model_year) + '.csv'
    print(upload_file_name)
    with utilities.pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                select d.style_id,null,null,d.style_id,null,null,null,null,null,null,null,null,
                    null,null,null,null,null,null,null,null
                from chr2.model_years a
                join chr2.divisions b on a.model_year = b.model_year
                join chr2.models c on a.model_year = c.model_year
                  and b.division_id = c.division_id
                join chr2.style_ids d on c.model_id = d.model_id
                where a.model_year = {}
            """.format(model_year)
            pg_cur.execute(sql)
            with open(upload_file_path + upload_file_name, 'w') as batch_request:
                csv.writer(batch_request).writerow(['passthru_id', 'vin', 'reducing_style_id', 'style_id',
                                                    'year', 'make', 'model', 'trim_name', 'manufacturer_model_code',
                                                    'wheelbase', 'exterior_color', 'interior_color',
                                                    'options', 'equipment', 'nonfactory_equipment', 'acode',
                                                    'reducing_acode', 'style_name', 'primary_option_names',
                                                    'include_technical_specification_title_id'])
                csv.writer(batch_request).writerows(pg_cur.fetchall())
    server = 'ftp.chromedata.com'
    username = 'rc265491_7'
    password = 'car491'
    ftp = ftplib.FTP(server)
    ftp.login(username, password)
    ftp.cwd('In')
    ftp.storbinary('STOR ' + upload_file_name, open(upload_file_path + upload_file_name, 'rb'))
    ftp.quit()


def batch_upload_non_captive_vins():
    """

    """
    datetime.datetime.now()
    upload_file_path = 'batches/requests/'
    upload_file_name = str('{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now())) + '_non_captive_vins' + '.csv'
    # upload_file_name = '2020-12-15-07-54_non_captive_vins.csv'
    print(upload_file_name)
    with utilities.pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                select model_year::text || ':' || make || ':' || right(vin, 6)::text as passthru, vin,
                    null, NULL,null,null,null,null,null,null,null,null,
                    null,null,null,null,null,null,null,null
                from (
                  select vin, yearmodel::integer as model_year, make
                  from ads.ext_vehicle_items
                  where yearmodel::integer > 2017
                    and make not in ('chevrolet','buick','gmc','cadillac','honda','nissan')
                  union
                  select distinct inpmast_vin, year as model_year, make
                  from arkona.ext_inpmast
                  where year > 2017
                    and make not in ('chevrolet','buick','gmc','cadillac','honda','nissan')
                  and length(inpmast_vin) = 17) a
            """
            pg_cur.execute(sql)
            with open(upload_file_path + upload_file_name, 'w') as batch_request:
                csv.writer(batch_request).writerow(['passthru_id', 'vin', 'reducing_style_id', 'style_id',
                                                    'year', 'make', 'model', 'trim_name', 'manufacturer_model_code',
                                                    'wheelbase', 'exterior_color', 'interior_color',
                                                    'options', 'equipment', 'nonfactory_equipment', 'acode',
                                                    'reducing_acode', 'style_name', 'primary_option_names',
                                                    'include_technical_specification_title_id'])
                csv.writer(batch_request).writerows(pg_cur.fetchall())
    server = 'ftp.chromedata.com'
    username = 'rc265491_7'
    password = 'car491'
    ftp = ftplib.FTP(server)
    ftp.login(username, password)
    ftp.cwd('In')
    ftp.storbinary('STOR ' + upload_file_name, open(upload_file_path + upload_file_name, 'rb'))
    ftp.quit()


def process_results_1():
    """
    initial feed of 2018 data
    """
    file_path = 'batches/results/2020-12-03-11-09_2018/to_parse/'
    file_names = ['style', 'technical_specification_definition', 'tech_spec_value', 'body_type',
                  'category_definition', 'option', 'interior_color', 'exterior_color_xref',
                  'exterior_color', 'engine', 'image', 'vehicle_description' ]
    # file_names = ['tech_spec_value']
    for the_file in file_names:
        with open(file_path + the_file + '.csv', 'r',
                  encoding='latin_1', newline='') as infile, open(
                  file_path + the_file + '_clean.csv', 'w') as outfile:
            reader = csv.reader(infile, delimiter=",", quotechar="~")
            writer = csv.writer(outfile)
            for row in reader:
                writer.writerow(row)
        with utilities.pg(pg_server) as pg_con:
            print(the_file)
            with pg_con.cursor() as pg_cur:
                with open(file_path + the_file + '_clean.csv', 'r', encoding='latin_1', newline='') as io:
                    pg_cur.copy_expert("copy chr2." + the_file + " from stdin with csv header encoding 'latin-1'", io)


def process_results_2():
    """
    subsequent feeds into tmp_tables
    12/6/20 removed vehicle_description, no consistently usable data
    12/9/20 realize that i do need the standard equipment file
    12/15/20 first big vin batch, includ ambiguous_option & vehicle_description
    """
    file_path = 'batches/results/2020-12-15-07-54_non_captive_vins/to_parse/'
    file_names = ['style', 'technical_specification_definition', 'tech_spec_value', 'body_type',
                  'category_definition', 'option', 'ambiguous_option', 'interior_color', 'exterior_color_xref',
                  'exterior_color', 'engine', 'image', 'standard_equipment', 'vehicle_description']
    # file_names = ['standard_equipment']
    for the_file in file_names:
        with open(file_path + the_file + '.csv', 'r',
                  encoding='latin_1', newline='') as infile, open(
                  file_path + the_file + '_clean.csv', 'w') as outfile:
            reader = csv.reader(infile, delimiter=",", quotechar="~")
            writer = csv.writer(outfile)
            for row in reader:
                writer.writerow(row)
        # embedded db into for the_file, to preserve the_file
        with utilities.pg(pg_server) as pg_con:
            print(the_file)
            with pg_con.cursor() as pg_cur:
                with open(file_path + the_file + '_clean.csv', 'r', encoding='latin_1', newline='') as io:
                    pg_cur.copy_expert(
                        "copy chr2." + 'tmp_' + the_file + " from stdin with csv header encoding 'latin-1'", io)


def vin_batch_upload_test_for_chrome():
    """

    """
    datetime.datetime.now()
    upload_file_path = 'batches/requests/'
    upload_file_name = str('{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now())) + '_' + 'vin_segment_test_6' + '.csv'
    print(upload_file_name)
    with utilities.pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                (select model_year::text || '-' || right(vin, 6)::text as passthru, vin, 
                  null, chrome_Style_id,null,null,null,null,null,null,null,null,
                  null,null,null,null,null,null,null,null
                from nc.vehicles 
                where model = 'malibu'
                  and model_year = 2021
                limit 1)   
                union 
                (select model_year::text || '-' || right(vin, 6)::text as passthru, vin, 
                  null, chrome_Style_id,null,null,null,null,null,null,null,null,
                  null,null,null,null,null,null,null,null
                from nc.vehicles 
                where model = 'malibu'
                  and model_year = 2020
                limit 1)
                union
                (select model_year::text || '-' || right(vin, 6)::text as passthru, vin, 
                  null, chrome_Style_id,null,null,null,null,null,null,null,null,
                  null,null,null,null,null,null,null,null
                from nc.vehicles 
                where model = 'malibu'
                  and model_year = 2019
                limit 1) 
                union 
                (select model_year::text || '-' || right(vin, 6)::text as passthru, vin, 
                  null, chrome_Style_id,null,null,null,null,null,null,null,null,
                  null,null,null,null,null,null,null,null
                from nc.vehicles 
                where model = 'colorado'
                  and model_year = 2021
                limit 1)  
                union
                (select model_year::text || '-' || right(vin, 6)::text as passthru, vin, 
                  null, chrome_Style_id,null,null,null,null,null,null,null,null,
                  null,null,null,null,null,null,null,null
                from nc.vehicles 
                where model = 'colorado'
                  and model_year = 2020
                limit 1) 
                union
                (select model_year::text || '-' || right(vin, 6)::text as passthru, vin, 
                  null, chrome_Style_id,null,null,null,null,null,null,null,null,
                  null,null,null,null,null,null,null,null
                from nc.vehicles 
                where model = 'colorado'
                  and model_year = 2019
                limit 1)  
            """
            pg_cur.execute(sql)
            with open(upload_file_path + upload_file_name, 'w') as batch_request:
                csv.writer(batch_request).writerow(['passthru_id', 'vin', 'reducing_style_id', 'style_id',
                                                    'year', 'make', 'model', 'trim_name', 'manufacturer_model_code',
                                                    'wheelbase', 'exterior_color', 'interior_color',
                                                    'options', 'equipment', 'nonfactory_equipment', 'acode',
                                                    'reducing_acode', 'style_name', 'primary_option_names',
                                                    'include_technical_specification_title_id'])
                csv.writer(batch_request).writerows(pg_cur.fetchall())
    server = 'ftp.chromedata.com'
    username = 'rc265491_7'
    password = 'car491'
    ftp = ftplib.FTP(server)
    ftp.login(username, password)
    ftp.cwd('In')
    ftp.storbinary('STOR ' + upload_file_name, open(upload_file_path + upload_file_name, 'rb'))
    ftp.quit()


def single_style_batch_upload():
    """

    """
    datetime.datetime.now()
    upload_file_path = 'batches/requests/'
    upload_file_name = str('{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now())) + '_' + 'single_style_406972' + '.csv'
    print(upload_file_name)
    with utilities.pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                select 406972,null,null,406972,null,null,null,null,null,null,null,null,
                    null,null,null,null,null,null,null,null
            """
            pg_cur.execute(sql)
            with open(upload_file_path + upload_file_name, 'w') as batch_request:
                csv.writer(batch_request).writerow(['passthru_id', 'vin', 'reducing_style_id', 'style_id',
                                                    'year', 'make', 'model', 'trim_name', 'manufacturer_model_code',
                                                    'wheelbase', 'exterior_color', 'interior_color',
                                                    'options', 'equipment', 'nonfactory_equipment', 'acode',
                                                    'reducing_acode', 'style_name', 'primary_option_names',
                                                    'include_technical_specification_title_id'])
                csv.writer(batch_request).writerows(pg_cur.fetchall())
    server = 'ftp.chromedata.com'
    username = 'rc265491_7'
    password = 'car491'
    ftp = ftplib.FTP(server)
    ftp.login(username, password)
    ftp.cwd('In')
    ftp.storbinary('STOR ' + upload_file_name, open(upload_file_path + upload_file_name, 'rb'))
    ftp.quit()


def main():
    # batch_upload()
    # version()
    # model_years()
    # divisions()
    # subdivisions()
    # models()
    # style_ids()
    # process_results_1()
    process_results_2()
    # vin_batch_upload()
    # single_style_batch_upload()
    # batch_upload_non_captive_vins()


if __name__ == '__main__':
    main()
