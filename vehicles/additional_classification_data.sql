﻿drop table if exists veh.additional_classification_data cascade;
create table veh.additional_classification_data as
select aa.chrome_make, aa.chrome_model, aa.subdivision, aa.wheelbase, aa.overall_length,
  aa.pass_doors, aa.drivetrain, aa.type_segment,
   bb.model as polk_model, bb.polk_segment,
  cc.model as tool_model, cc.tool_shape_size, cc.luxury as tool_luxury, cc.comm as tool_comm, cc.sport as tool_sport,
  aa.image_url_1 as image_one, aa.image_url_2 as image_two
from (
  select a.make as chrome_make, a.model as chrome_model, a.subdivision,
--     case 
--       when a.make = a.subdivision then null
--       else a.subdivision
--     end as subdivision,     
    case
      when cardinality(array_agg(distinct a.wheelbase)) = 1 then (min(a.wheelbase))::text
      else (min(a.wheelbase))::text || '-' || (max(a.wheelbase))::text
    end as wheelbase,
    case
      when cardinality(array_agg(distinct a.overall_length)) = 1 then (min(a.overall_length))::text
      else (min(a.overall_length))::text || '-' || (max(a.overall_length))::text
    end as overall_length,
    case
      when cardinality(array_agg(distinct a.pass_doors)) = 1 then (min(a.pass_doors))::text
      else (min(a.pass_doors))::text || '-' || (max(a.pass_doors))::text
    end as pass_doors,  string_agg(distinct a.drivetrain, ',') as drivetrain,
    max(b.type_segment) as type_segment, min(image_url) as image_url_1, max(image_url) as image_url_2
  from veh.vehicle_types a 
  left join ( -- generates a vertical likst of types/segments
    select make, model, string_agg(distinct coalesce(vehicle_type, '') ||  ' : ' || coalesce(segment, ''), E'\n') as type_segment 
    from veh.vehicle_types 
    where vehicle_type is not null
      and segment is not null
    group by make, model) b on a.make = b.make and a.model = b.model
  group by a.make, a.subdivision, a.model) aa
left join (
  select a.make, a.model, string_agg(distinct a.segment, ',') as polk_segment
  from polk.vehicle_categories a
  join veh.makes_models b on a.make = b.make
  group by a.make, a.model) bb on aa.chrome_make = bb.make and aa.chrome_model = bb.model
left join (
  select b.make, b.model, string_agg(distinct split_part(b.vehicletype, '_', 2) ||'-'||split_part(b.vehiclesegment, '_', 2),',') as tool_shape_size,
    case when string_agg(distinct luxury::text,',') like '%true%' then 'true' else null end as luxury, 
    case when string_agg(distinct commercial::text,',') like '%true%' then 'true' else null end as comm, 
    case when string_agg(distinct sport::text,',') like '%true%' then 'true' else null end as sport
  from ads.ext_make_model_classifications b
  join veh.makes_models c on b.make = c.make
  group by b.make, b.model) cc on coalesce(aa.chrome_make, bb.make) = cc.make  and coalesce(aa.chrome_model, bb.model) = cc.model;
alter table veh.additional_classification_data
add primary key(chrome_make, chrome_model, subdivision);
create index on veh.additional_classification_data(chrome_model);

select * from veh.additional_classification_data  where chrome_make = 'gmc' and chrome_model = 'sierra 3500hd'
select chrome_make, chrome_model, subdivision from veh.additional_classification_data group by chrome_make, chrome_model,subdivision having count(*) > 1


