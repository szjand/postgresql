﻿a number of issues
----------------------------------------------------------------------------------------------------
--< 1.
----------------------------------------------------------------------------------------------------
-- Greg  2:14 PM
-- How many used cars did we own on Jan 28, 2019?
-- 2:17
-- Statement says 511 on Jan 31, 2019 running through the graphs for each shape/size I counted over 600. Could be my math in my head as I clicked though.


-- this approach is no good
-- fact_gl is transactions, i need balance
-- how many vehicles had a balance in an inventory account on 1/31/2019
select a.gl_account, b.* 
from fin.dim_fs_account a
left join fin.dim_Account b on a.gl_account = b.account
where gm_account in ('240','241')
  and left(gl_account, 1) = '1'

select control, sum(amount)
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.the_date = '01/31/2019'
join fin.dim_account c on a.account_key = c.account_key
  and c.account in ('124000','124100')  
group by a.control 

select b.the_date, a.control, a.amount
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and year_month = 201901
join fin.dim_account c on a.account_key = c.account_key
  and c.account in ('124000','124100')  
order by a.control, b.the_date  


select * 
from tem.inventory_2019_2020_days
where the_date = '01/31/2019'

show log_statement
----------------------------------------------------------------------------------------------------
--/> 1.
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
--< 2.
----------------------------------------------------------------------------------------------------




-- 03/02/21 afton noticed that these queries do not give a row for every day, 
-- eg, compact car 202005, only have inventory on 15 of the 30 days
-- so, this is what i sent to her
with 
  wtf as (
    select a.*, sum(sales) over (partition by year_month) as month_sales,
      sum(sales) over (order by the_date rows between current row and 14 following) as sales_14,
      sum(sales) over (order by the_date rows between current row and 21 following) as sales_21,
      sum(sales) over (order by the_date rows between current row and 28 following) as sales_28
    from (
      select aa.year_month, aa.the_date, 
        coalesce(bb.total_inv, 0) as total_inv,
        coalesce(bb.green, 0) as green,
        coalesce(bb.yellow, 0) as yellow,
        coalesce(bb.red, 0) as red,
        coalesce(bb.black, 0) as black,
        coalesce(bb.sales, 0) as sales
      from dds.dim_date aa
      left join (
        select year_month, the_date,
          count(*) as total_inv,
          count(*) filter (where age <= 30) as green,
          count(*) filter (where age between 31 and 60) as yellow,
          count(*) filter (where age between 61 and 90) as red,
          count(*) filter (where age > 90) as black,
          count(*) filter (where date_sold = the_date and sale_type = 'retail') as sales
        from tem.inventory_2019_2020_days
        where year_month in ( 202005,202006)
          and shape = 'car'
          and size = 'compact'
        group by year_month, the_date) bb on aa.the_date = bb.the_date
      where aa.year_month in ( 202005,202006)  ) a)
select *
from wtf 
where year_month in(202005,202006)
order by the_date

-- but it did not work out, because as she is using it, she had to add shape and size
-- which resulted in null values on days without inventoury
    select a.*, sum(sales) over (partition by year_month) as month_sales,
      sum(sales) over (order by the_date rows between current row and 14 following) as sales_14,
      sum(sales) over (order by the_date rows between current row and 21 following) as sales_21,
      sum(sales) over (order by the_date rows between current row and 28 following) as sales_28
    from (
      select aa.year_month, aa.the_date, shape, size,
        coalesce(bb.total_inv, 0) as total_inv,
        coalesce(bb.green, 0) as green,
        coalesce(bb.yellow, 0) as yellow,
        coalesce(bb.red, 0) as red,
        coalesce(bb.black, 0) as black,
        coalesce(bb.sales, 0) as sales
      from dds.dim_date aa
      left join (
        select year_month, the_date,shape, size,
          count(*) as total_inv,
          count(*) filter (where age <= 30) as green,
          count(*) filter (where age between 31 and 60) as yellow,
          count(*) filter (where age between 61 and 90) as red,
          count(*) filter (where age > 90) as black,
          count(*) filter (where date_sold = the_date and sale_type = 'retail') as sales
        from tem.inventory_2019_2020_days
        where year_month in ( 202005,202006)
          and shape = 'car'
          and size = 'compact'
        group by year_month, the_date,shape, size) bb on aa.the_date = bb.the_date
      where aa.year_month in ( 202005,202006)) a

-- need this for a base table
drop table if exists tem.days_shapes_sizes cascade;
create table tem.days_shapes_sizes as
select year_month, the_date, shape, size
from dds.dim_date a
cross join (
  select distinct shape, size
  from tem.inventory_2019_2020_days) b
where a.the_year in (2019,2020);
alter table tem.days_shapes_sizes add primary key(the_date,shape,size);
create index on tem.days_shapes_sizes(year_month);
create index on tem.days_shapes_sizes(shape);
create index on tem.days_shapes_sizes(size);


-- slow & the outer sales is fucked up
select c.*, sum(sales) over (partition by year_month) as month_sales,
  sum(sales) over (order by the_date rows between current row and 14 following) as sales_14,
  sum(sales) over (order by the_date rows between current row and 21 following) as sales_21,
  sum(sales) over (order by the_date rows between current row and 28 following) as sales_28
from (
  select a.*,         
    coalesce(b.total_inv, 0) as total_inv,
    coalesce(b.green, 0) as green,
    coalesce(b.yellow, 0) as yellow,
    coalesce(b.red, 0) as red,
    coalesce(b.black, 0) as black,
    coalesce(b.sales, 0) as sales
  from tem.days_shapes_sizes A
  left join lateral(
    select year_month, the_date,shape, size,
      count(*) as total_inv,
      count(*) filter (where age <= 30) as green,
      count(*) filter (where age between 31 and 60) as yellow,
      count(*) filter (where age between 61 and 90) as red,
      count(*) filter (where age > 90) as black,
      count(*) filter (where date_sold = the_date and sale_type = 'retail') as sales
    from tem.inventory_2019_2020_days
    where year_month = a.year_month
      and shape = a.shape
      and size = a.size
    group by year_month, the_date,shape, size) b on a.the_date = b.the_date
      and a.shape = b.shape
      and a.size = b.size
  where a.year_month in (202005,202006)
    and a.shape = 'car'
    and a.size in ('compact','small')) c  
order by shape, size, the_date

-- but for this first graph, this works
select a.*,         
  coalesce(b.total_inv, 0) as total_inv,
  coalesce(b.green, 0) as green,
  coalesce(b.yellow, 0) as yellow,
  coalesce(b.red, 0) as red,
  coalesce(b.black, 0) as black,
  coalesce(b.sales, 0) as sales
from tem.days_shapes_sizes A
left join lateral(
  select year_month, the_date,shape, size,
    count(*) as total_inv,
    count(*) filter (where age <= 30) as green,
    count(*) filter (where age between 31 and 60) as yellow,
    count(*) filter (where age between 61 and 90) as red,
    count(*) filter (where age > 90) as black,
    count(*) filter (where date_sold = the_date and sale_type = 'retail') as sales
  from tem.inventory_2019_2020_days
  where year_month = a.year_month
    and shape = a.shape
    and size = a.size
  group by year_month, the_date,shape, size) b on a.the_date = b.the_date
    and a.shape = b.shape
    and a.size = b.size
where a.year_month in (202005,202006)
  and a.shape = 'car'
  and a.size in ('compact','small')
order by a.shape, a.size, a.the_date











----------------------------------------------------------------------------------------------------
--/> 2.
----------------------------------------------------------------------------------------------------