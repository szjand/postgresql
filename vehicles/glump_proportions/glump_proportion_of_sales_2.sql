﻿/*
01/02/21
after going through some mind fucking and trying to implement this using greg.uc_daily_snapshot_beta_1
decided that arkona is less fucked up than the tool
fundamentally that is, still need tool for glump
*/
-- these 3 queries generate the basis for sales in a temp table: step_3
-- from fin_data_mart/sql/fact_fs/fact_fs_monthly_update_including_page_2.sql
drop table if exists step_1 cascade;
create temp table step_1 as
select year_month, page, line, line_label, control, sum(unit_count) as unit_count, sum(amount) as amount
from (
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201901 and 202012 --------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month between 201901 and 202012 -------------------------------------------------------------------
      and (
        (b.page = 16 and b.line between 1 and 5)) -- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row -- **
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
      and f.store = 'RY1'
    group by f.store, d.gl_account, b.page, b.line, b.line_label, e.description  ) d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by year_month, page, line, line_label, control
order by year_month, page, line;
create index on step_1(control);

-- select * from step_1 limit 100


-- for line chart of sales by day over 2 years, need sale date. go with bopmast_date_capped
-- 1/13/21: i am OK with limiting this to units where unit_count = 1
drop table if exists step_2 cascade;  --4367
create temp table step_2 as
select a.year_month, b.date_capped as sale_date, a.control as stock_number, -a.amount as amount, b.bopmast_vin as vin, b.retail_price 
from step_1 a
left join arkona.ext_bopmast b on a.control = b.bopmast_stock_number
where a.unit_count = 1  -- G37123G is the only one 
  and b.bopmast_vin is not null;
-- group by a.year_month, a.control, -a.amount, b.bopmast_vin, b.retail_price, b.date_capped;
create index on step_2(vin);
create index on step_2(stock_number);
create index on step_2(retail_price);
create index on step_2(sale_date);

drop table if exists tem.price_bands_ext;
create table tem.price_bands_ext as
select * from uc.price_bands where price_band <> '40k+'
union
select * from tem.high_end_price_bands 
order by price_from;
create index on tem.price_bands_ext(price_from);
create index on tem.price_bands_ext(price_thru);

drop table if exists step_3 cascade;
create temp table step_3 as
select a.*, coalesce(c.make, b.make) as make, coalesce(c.model, b.model) as model, --4351
  (split_part(d.vehiclesegment, '_', 2))::citext as size, (split_part(d.vehicletype, '_', 2))::citext as shape,
  e.price_band, -- , f.hi_price_band,
  e.price_from -- needed for sorting on pricebands
from step_2 a
left join arkona.ext_inpmast b on a.vin = b.inpmast_vin
left join ads.ext_vehicle_items c on a.vin = c.vin
join ads.ext_make_model_classifications d on coalesce(c.make, b.make) = d.make and coalesce(c.model, b.model) = d.model
left join price_bands_ext e on a.retail_price between e.price_from and e.price_thru;
-- join uc.price_bands e on a.retail_price between e.price_from and e.price_thru
-- left join tem.high_end_price_bands f on a.retail_price >= 40000
--   and a.retail_price between f.price_from and f.price_thru
-- where d.vehicletype is null;
create index on step_3(size);
create index on step_3(shape);
create index on step_3(price_band);
create index on step_3(year_month);

drop table if exists tem.step_3 cascade;
create table tem.step_3 as
select * from step_3;
create index on tem.step_3(size);
create index on tem.step_3(shape);
create index on tem.step_3(price_band);
create index on tem.step_3(year_month);
select * from step_3 limit 10

-- select count(*) from  step_3  --4477

-- add on a separate over 40 set of price bands
-- drop table if exists tem.high_end_price_bands cascade;
-- create table tem.high_end_price_bands (
--   hi_price_band citext primary key,
--   price_from integer not null,
--   price_thru integer not null);
-- create index on tem.high_end_price_bands(price_from);  
-- create index on tem.high_end_price_bands(price_thru);
-- insert into tem.high_end_price_bands values ('40-50k',40000,49999);
-- insert into tem.high_end_price_bands values ('50-60k',50000,59999);
-- insert into tem.high_end_price_bands values ('60-70k',60000,69999);
-- insert into tem.high_end_price_bands values ('70k+',70000,99999);

-- -- will probably wa to add sequence
-- drop table if exists tem.shapes_sizes cascade;
-- create table tem.shapes_sizes (
--   shape citext not null,
--   size citext not null,
--   primary key (shape,size));
-- create index on tem.shapes_sizes(shape);  
-- create index on tem.shapes_sizes(size);
-- insert into tem.shapes_sizes
-- select distinct split_part(vehicletype, '_', 2) as shape, split_part(vehiclesegment, '_', 2) as size
-- from ads.ext_make_model_classifications;


-- this could work for line chart
select a.shape, a.size, 
  count(c.vin) filter (where c.year_month = 201901) as "201901",
  count(c.vin) filter (where c.year_month = 201902) as "201902",
  count(c.vin) filter (where c.year_month = 201903) as "201903",
  count(c.vin) filter (where c.year_month = 201904) as "201904",
  count(c.vin) filter (where c.year_month = 201905) as "201905",
  count(c.vin) filter (where c.year_month = 201906) as "201906",
  count(c.vin) filter (where c.year_month = 201907) as "201907",
  count(c.vin) filter (where c.year_month = 201908) as "201908",
  count(c.vin) filter (where c.year_month = 201909) as "201909",
  count(c.vin) filter (where c.year_month = 201910) as "201910",
  count(c.vin) filter (where c.year_month = 201911) as "201911",
  count(c.vin) filter (where c.year_month = 201912) as "201912",
  count(c.vin) filter (where c.year_month = 202001) as "202001",
  count(c.vin) filter (where c.year_month = 202002) as "202002",
  count(c.vin) filter (where c.year_month = 202003) as "202003",
  count(c.vin) filter (where c.year_month = 202004) as "202004",
  count(c.vin) filter (where c.year_month = 202005) as "202005",
  count(c.vin) filter (where c.year_month = 202006) as "202006",
  count(c.vin) filter (where c.year_month = 202007) as "202007",
  count(c.vin) filter (where c.year_month = 202008) as "202008",
  count(c.vin) filter (where c.year_month = 202009) as "202009",
  count(c.vin) filter (where c.year_month = 202010) as "202010",
  count(c.vin) filter (where c.year_month = 202011) as "202011",
  count(c.vin) filter (where c.year_month = 202012) as "202012"    
from tem.shapes_sizes a
left join step_3 c on a.shape = c.shape
    and a.size = c.size
group by a.shape, a.size
order by a.shape, a.size

select a.shape, a.size, b.year_month, b.stock_number
from tem.shapes_sizes a
left join step_3 b on a.shape = b.shape
  and a.size = b.size
limit 100

-- shape size proportions
drop table if exists tem.sales_shape_size_proportions cascade;
create table tem.sales_shape_size_proportions as
select aa.shape, aa.size, aa.the_year, aa.year_month, 
  count(vin) over (partition by aa.the_year) as year_total,
  count(vin) over (partition by aa.year_month) as month_total, 
  count(vin) over (partition by aa.the_year, aa.shape, aa.size) as year_shape_size,
  count(vin) over (partition by aa.year_month, aa.shape, aa.size) as month_shape_size
from (
  select *
  from tem.shapes_sizes
  cross join (
    select distinct the_year, year_month
    from dds.dim_date
    where the_year between 2019 and 2020) a) aa
left join tem.step_3 bb on aa.year_month = bb.year_month
  and aa.shape = bb.shape
  and aa.size = bb.size;
create index on tem.sales_shape_size_proportions(shape);
create index on tem.sales_shape_size_proportions(size);
create index on tem.sales_shape_size_proportions(the_year);
create index on tem.sales_shape_size_proportions(year_month);
create index on tem.sales_shape_size_proportions(month_total);
create index on tem.sales_shape_size_proportions(year_total);
create index on tem.sales_shape_size_proportions(year_total);
create index on tem.sales_shape_size_proportions(month_shape_size);

select shape, size, the_year, year_month, month_total, year_total, year_shape_size, month_shape_size,
  round(100.0 * year_shape_size/year_total, 1) as year_propor,
  round(100.0 * month_shape_size/month_total, 1) as month_propor
from tem.sales_shape_size_proportions 
group by shape, size, the_year, year_month, month_total, year_total, year_shape_size, month_shape_size
order by shape, size, year_month;

-- priceband proportions
drop table if exists tem.price_band_proportions cascade;
create table tem.price_band_proportions as
select aa.price_band, aa.the_year, aa.year_month, 
  count(vin) over (partition by aa.the_year) as year_total,
  count(vin) over (partition by aa.year_month) as month_total, 
  count(vin) over (partition by aa.the_year, aa.price_band) as year_price_band,
  count(vin) over (partition by aa.year_month, aa.price_band) as month_price_band
from (
  select *
  from tem.price_bands_ext
  cross join (
    select distinct the_year, year_month
    from dds.dim_date
    where the_year between 2019 and 2020) a) aa
left join tem.step_3 bb on aa.year_month = bb.year_month
  and aa.price_band = bb.price_band;
create index on tem.price_band_proportions(price_band);
create index on tem.price_band_proportions(the_year);
create index on tem.price_band_proportions(year_month);
create index on tem.price_band_proportions(month_total);
create index on tem.price_band_proportions(year_total);
create index on tem.price_band_proportions(year_price_band);
create index on tem.price_band_proportions(month_price_band);

select price_band, the_year, year_month, month_total, year_total, year_price_band, month_price_band,
  round(100.0 * year_price_band/year_total, 1) as year_propor,
  round(100.0 * month_price_band/month_total, 1) as month_propors
from tem.price_band_proportions 
group by price_band, the_year, year_month, month_total, year_total, year_price_band, month_price_band
order by price_band, the_year, year_month, month_total, year_total, year_price_band, month_price_band;
  
/*   
-- do just shape and size for year_months 202004 202012, 202006, 202007
-- proportional_shape_size.csv
select aa.*, bb.year_month, bb.total, bb.count, bb."%",
  cc.year_month, cc.total, cc.count, cc."%",
  dd.year_month, dd.total, dd.count, dd."%",
  ee.year_month, ee.total, ee.count, ee."%"
from (
  select b.shape, b.size, a.total, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (
    select count(*) as total
    from step_3
    where year_month between 202001 and 202012) a
  join (
    select shape, size, count(*) as glump
    from step_3
    where year_month between 202001 and 202012
    group by shape, size) b on true) aa
left join (
  select a.year_month, a.total, b.shape, b.size, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (
    select year_month, count(*) as total -- totals
    from step_3
    where year_month = 202004
    group by year_month) a
  left join (
    select year_month, shape, size, count(*) as glump
    from step_3
    where year_month = 202004
    group by year_month, shape, size) b on a.year_month = b.year_month) bb on aa.shape = bb.shape and aa.size = bb.size
left join (
  select a.year_month, a.total, b.shape, b.size, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (
    select year_month, count(*) as total -- totals
    from step_3
    where year_month = 202006
    group by year_month) a
  left join (
    select year_month, shape, size, count(*) as glump
    from step_3
    where year_month = 202006
    group by year_month, shape, size) b on a.year_month = b.year_month) cc on aa.shape = cc.shape and aa.size = cc.size    
left join (
  select a.year_month, a.total, b.shape, b.size, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (
    select year_month, count(*) as total -- totals
    from step_3
    where year_month = 202007
    group by year_month) a
  left join (
    select year_month, shape, size, count(*) as glump
    from step_3
    where year_month = 202007
    group by year_month, shape, size) b on a.year_month = b.year_month) dd on aa.shape = dd.shape and aa.size = dd.size     
left join (
  select a.year_month, a.total, b.shape, b.size, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (
    select year_month, count(*) as total -- totals
    from step_3
    where year_month = 202012
    group by year_month) a
  left join (
    select year_month, shape, size, count(*) as glump
    from step_3
    where year_month = 202012
    group by year_month, shape, size) b on a.year_month = b.year_month) ee on aa.shape = ee.shape and aa.size = ee.size        
order by aa.count desc  


-- just price band
-- proportional_price_band.csv
select aa.*, bb.year_month, bb.total, bb.count, bb."%",
  cc.year_month, cc.total, cc.count, cc."%",
  dd.year_month, dd.total, dd.count, dd."%",
  ee.year_month, ee.total, ee.count, ee."%"
from (
  select b.price_band, a.total, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (
    select count(*) as total
    from step_3
    where year_month between 202001 and 202012) a
  join (
    select price_band, count(*) as glump
    from step_3
    where year_month between 202001 and 202012
    group by price_band) b on true) aa
left join (
  select a.year_month, a.total, b.price_band, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (
    select year_month, count(*) as total -- totals
    from step_3
    where year_month = 202004
    group by year_month) a
  left join (
    select year_month, price_band, count(*) as glump
    from step_3
    where year_month = 202004
    group by year_month, price_band) b on a.year_month = b.year_month) bb on aa.price_band = bb.price_band 
left join (
  select a.year_month, a.total, b.price_band, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (
    select year_month, count(*) as total -- totals
    from step_3
    where year_month = 202006
    group by year_month) a
  left join (
    select year_month, price_band, count(*) as glump
    from step_3
    where year_month = 202006
    group by year_month, price_band) b on a.year_month = b.year_month) cc on aa.price_band = cc.price_band
left join (
  select a.year_month, a.total, b.price_band, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (
    select year_month, count(*) as total -- totals
    from step_3
    where year_month = 202007
    group by year_month) a
  left join (
    select year_month, price_band, count(*) as glump
    from step_3
    where year_month = 202007
    group by year_month, price_band) b on a.year_month = b.year_month) dd on aa.price_band = dd.price_band 
left join (
  select a.year_month, a.total, price_band, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (
    select year_month, count(*) as total -- totals
    from step_3
    where year_month = 202012
    group by year_month) a
  left join (
    select year_month, price_band, count(*) as glump
    from step_3
    where year_month = 202012
    group by year_month, price_band) b on a.year_month = b.year_month) ee on aa.price_band = ee.price_band 
order by aa.count desc  
*/

--------------------------------------------------------------------
--< inventory
--------------------------------------------------------------------
-- shape and size -----------------------------------------------------------------
drop table if exists inv_1 cascade;
-- inventory by day from inpmast, 2019 - 2020
create temp table inv_1 as  -- 257335
select a.the_date,  b.inpmast_stock_number as stock_number, b.inpmast_vin as vin, b.make, b.model
from dds.dim_date a
join arkona.xfm_inpmast b on a.the_date between b.row_from_date and b.row_thru_date
  and b.type_n_u = 'U'
  and b.status = 'I'
  and left(b.inpmast_stock_number, 1) <> 'H'
where a.year_month between 201901 and 202012
group by a.the_date, b.inpmast_stock_number, b.inpmast_vin, b.make, b.model;
create unique index on inv_1(the_date, vin);

-- this is the base table for inventory, for both shape_size and price_band
-- 1 row for each day a vehicle in inventory
drop table if exists inv_2 cascade;
-- add vehicle_items, make_model_classifications
create temp table inv_2 as
select a.the_date, a.stock_number, a.vin,
  coalesce(b.make, a.make) as make, coalesce(b.model, a.model) as model, --4351
    coalesce(split_part(c.vehiclesegment, '_', 2), 'unknown')::citext as size, 
    coalesce(split_part(c.vehicletype, '_', 2), 'unknown')::citext as shape
from inv_1 a
left join ads.ext_vehicle_items b on a.vin = b.vin
left join ads.ext_make_model_classifications c on coalesce(b.make, a.make) = c.make and coalesce(b.model, a.model) = c.model;
create unique index on inv_2(the_date, vin);
create index on inv_2(shape);
create index on inv_2(size);

-- so lets start with range of inventory by month
-- but need to make sure to account for all days, eg stock outs
-- so need a base table cartesion of date/shape_size
drop table if exists date_shape_size cascade;
create temp table date_shape_size as
select the_date, year_month, shape, size, seq
from dds.dim_date a
cross join (
  select distinct shape, size,
    case size
      when 'small' then 1
      when 'compact' then 2
      when 'midsize' then 3
      when 'large' then 4
      when 'extra' then 5
    end as seq
  from inv_2
    where shape <> 'unknown') b
where a.the_year in (2019, 2020);
create index on date_shape_size(the_date);
create index on date_shape_size(shape);
create index on date_shape_size(size);
create index on date_shape_size(year_month);

drop table if exists inv_shape_size_by_day cascade;
create temp table inv_shape_size_by_day as
select a.*, coalesce(b.units, 0) as units
from date_shape_size a
left join (
  select the_date, shape, size, count(*)::integer as units
  from inv_2
  group by the_date, shape, size) b on a.the_date = b.the_date and a.shape = b.shape and a.size = b.size;
create index on inv_shape_size_by_day(the_date);
create index on inv_shape_size_by_day(year_month);
create index on inv_shape_size_by_day(shape);
create index on inv_shape_size_by_day(size);
    
-- monthly min, max, avg
select year_month, shape, size, min(units), max(units), round(avg(units), 1) as avg -- , int4range(min(units), max(units), '[]')
from inv_shape_size_by_day
group by year_month, shape, size, seq
order by year_month, shape, seq

select * from inv_shape_size_by_day limit 100
need to pivot such that the columns are shape_size, the_date with units as the values
use jon.colpivot
-- -- -- example from taylor project
-- drop table if exists _test_ext_colors;
-- select jon.colpivot('_test_ext_colors', 'select * from ta.colors where model_year = 2019 and forecast_model = ''1500 Reg'' and color not like ''01%''',
--     array['color'], array['chrome_style_id'], '#.perc_sold_current', 'max(seq)');
-- select .01*(row_number() over()), a.* from _test_ext_colors a;  

-- finally, this gives me what i am looking for
-- now see if excel can graph it, it does just fine
drop table if exists _test_inv_2019;
select jon.colpivot('_test_inv_2019', 'select the_date, shape, size, units, seq, ''inv''::citext as source from inv_shape_size_by_day where the_date between ''01/01/2019'' and ''01/31/2019'' order by seq',
  array['shape', 'size', 'seq', 'source'], array['the_date'], '#.units', 'the_date');
select * from _test_inv_2019 a order by shape, seq;  

drop table if exists _test_inv_2019;
select jon.colpivot(
  '_test_inv_2019', 
  $$ 
    select the_date, shape, size, 'inv'::citext as source, units, seq -- , 'inv'::citext as source 
    from inv_shape_size_by_day 
    where the_date between '01/01/2019' and '01/31/2019' 
    order by seq $$,
  array['shape', 'size', 'source', 'seq'], 
  array['the_date'], 
  '#.units', 
  'the_date');
select * from _test_inv_2019 a order by shape, seq;


drop table if exists _test_sales_2019;
select jon.colpivot(
  '_test_sales_2019', 
  $$ 
    select a.the_date, a.shape, a.size, 'sales'::citext as source, count(vin) as units, seq
    from date_shape_size a
    left join step_3 b on a.the_date = b.sale_date
      and a.shape = b.shape
      and a.size = b.size   
    where a.the_date between '01/01/2019' and '01/31/2019' 
    group by a.the_date, a.shape, a.size, seq  $$,
  array['shape', 'size', 'source', 'seq'], 
  array['the_date'], 
  '#.units', 
  'the_date');
select * from _test_sales_2019 a order by shape, source, seq;

-- graph_shape_by_day_sales_vs_inv.csv
select * from _test_inv_2019 
union 
select * from _test_sales_2019
order by shape, seq, source


-- shape and size -----------------------------------------------------------------

-- price band -----------------------------------------------------------------
drop table if exists inv_prices_tmp cascade;
create temp table inv_prices_tmp as
select c.stocknumber as stock_number, d.vin, vehiclepricingts::date as date_priced, max(amount) as best_price
from ads.ext_vehicle_pricings a
join ads.ext_vehicle_pricing_details b on a.vehiclepricingid = b.vehiclepricingid
  and b.typ = 'VehiclePricingDetail_BestPrice'
join ads.ext_vehicle_inventory_items c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
join ads.ext_vehicle_items d on a.vehicleitemid = d.vehicleitemid
group by c.stocknumber, d.vin, vehiclepricingts::date;



-- to get price on a day, think i need to convert price date to from and thru
drop table if exists inv_prices cascade;
create temp table inv_prices as
select a.stock_number, a.vin, date_priced as from_date,
  coalesce(lead(date_priced - 1, 1) over(partition by stock_number, vin order by date_priced), '12/31/9999') as thru_date,
  best_price::integer, b.price_band
from inv_prices_tmp a
left join tem.price_bands_ext b on a.best_price between b.price_from and b.price_thru;
create unique index on inv_prices(stock_number,from_date);
create index on inv_prices(stock_number);
create index on inv_prices(vin);
create index on inv_prices(from_date);
create index on inv_prices(thru_date);
create index on inv_prices(best_price);

-- to best deal with vehicles with no price on a date, do an inner join
select a.the_date, a.stock_number, a.vin, a.make, a.model, b.from_date, b.thru_date, b.best_price  --269199
from inv_2 a
join inv_prices b on a.vin = b.vin
  and a.the_date between b.from_date and b.thru_date


/*
-- !!! inventory not priced, will be days for most vehicles in inventory without a price
select a.the_date, a.stock_number, a.vin, a.make, a.model, b.from_date, b.thru_date, b.best_price  -- 30390
from inv_2 a
left join inv_prices b on a.vin = b.vin
  and a.the_date between b.from_date and b.thru_date
where b.best_price is null

select count(*) from inv_2 -- select count(*) from inv_2  -- 257335

-- vehicles with no price on one or more days : 4258
drop table if exists wtf cascade;
create temp table wtf as
select distinct a.stock_number, a.vin
from inv_2 a
left join inv_prices b on a.vin = b.vin
  and a.the_date between b.from_date and b.thru_date
where b.best_price is null;
create unique index on wtf(stock_number,vin);
create index on wtf(stock_number);
create index on wtf(vin);

select count(*) from (select distinct stock_number, vin from inv_2) a -- a total of 5931 vehicles

select * -- of the 4258 with no price on one or more days, only 259 have no price at all
from wtf a
where not exists (
  select 1
  from inv_prices
  where stock_number = a.stock_number
    and vin = a.vin)
*/

-- so lets start with range of inventory by month
-- but need to make sure to account for all days, eg stock outs
-- so need a base table cartesion of date/shape_size
drop table if exists date_price_bands cascade;
create temp table date_price_bands as
select the_date, year_month, price_from, price_band
from dds.dim_date a
cross join (
  select distinct price_from, price_band
  from tem.price_bands_ext) b
where a.the_year in (2019, 2020)
order by the_date, price_from;
create index on date_price_bands(the_date);
create index on date_price_bands(price_band);
create index on date_price_bands(year_month);    

select * from date_price_bands

drop table if exists inv_price_band_by_day cascade;
create temp table inv_price_band_by_day as
select a.*, coalesce(b.units, 0) as units
from date_price_bands a
left join (
  select a.the_date, b.price_band, count(*) as units
  from inv_2 a
  join inv_prices b on a.vin = b.vin
    and a.the_date between b.from_date and b.thru_date
  group by a.the_date, b.price_band) b on a.the_date = b.the_date and a.price_band = b.price_band;
create index on inv_price_band_by_day(the_date);
create index on inv_price_band_by_day(year_month);
create index on inv_price_band_by_day(price_band);

select * from inv_price_band_by_day

-- monthly min, max, avg
select year_month, price_from, price_band, min(units), max(units), round(avg(units), 1) as avg -- , int4range(min(units), max(units), '[]')
from inv_price_band_by_day
group by year_month, price_from, price_band
order by year_month, price_from
--------------------------------------------------------------------
--/> inventory
--------------------------------------------------------------------

select * from price_bands_ext

select * from step_3 limit 100

line charts, need sales by day
-- no way 1686 values of 1
select sale_date, size, shape, count(*) as sales
from step_3
group by sale_date, size, shape
order by count(*)

-- lets see the graph
-- shape_size_2_years.csv
select year_month,shape || '-' || size as shape_size, count(*) as sales
from step_3
group by year_month, shape || '-' || size
order by year_month, shape || '-' || size


select shape || '-' || size, count(vin) as shape_size from step_3 group by shape || '-' || size order by count(vin) desc

-- shape size
-- first shot at line graphs
-- on the graph separated shapes to make them visible
-- graph_sales_vs_inventory_v1
-- sales
select shape || '-' || size as shape_size, 'sales' as source,
  count(c.vin) filter (where c.year_month = 201901) as "201901",
  count(c.vin) filter (where c.year_month = 201902) as "201902",
  count(c.vin) filter (where c.year_month = 201903) as "201903",
  count(c.vin) filter (where c.year_month = 201904) as "201904",
  count(c.vin) filter (where c.year_month = 201905) as "201905",
  count(c.vin) filter (where c.year_month = 201906) as "201906",
  count(c.vin) filter (where c.year_month = 201907) as "201907",
  count(c.vin) filter (where c.year_month = 201908) as "201908",
  count(c.vin) filter (where c.year_month = 201909) as "201909",
  count(c.vin) filter (where c.year_month = 201910) as "201910",
  count(c.vin) filter (where c.year_month = 201911) as "201911",
  count(c.vin) filter (where c.year_month = 201912) as "201912",
  count(c.vin) filter (where c.year_month = 202001) as "202001",
  count(c.vin) filter (where c.year_month = 202002) as "202002",
  count(c.vin) filter (where c.year_month = 202003) as "202003",
  count(c.vin) filter (where c.year_month = 202004) as "202004",
  count(c.vin) filter (where c.year_month = 202005) as "202005",
  count(c.vin) filter (where c.year_month = 202006) as "202006",
  count(c.vin) filter (where c.year_month = 202007) as "202007",
  count(c.vin) filter (where c.year_month = 202008) as "202008",
  count(c.vin) filter (where c.year_month = 202009) as "202009",
  count(c.vin) filter (where c.year_month = 202010) as "202010",
  count(c.vin) filter (where c.year_month = 202011) as "202011",
  count(c.vin) filter (where c.year_month = 202012) as "202012"    
from step_3 c
group by shape || '-' || size
union
-- inventory
select shape_size, 'inv' as source,
  round("201901", 1) as "201901",round("201902", 1) as "201902",round("201903", 1) as "201903",
  round("201904", 1) as "201904",round("201905", 1) as "201905",round("201906", 1) as "201906",
  round("201907", 1) as "201907",round("201908", 1) as "201908",round("201909", 1) as "201909",
  round("201910", 1) as "201910",round("201911", 1) as "201911",round("201912", 1) as "201912",
  round("202001", 1) as "202001",round("202002", 1) as "202002",round("202003", 1) as "202003",
  round("202004", 1) as "202004",round("202005", 1) as "202005",round("202006", 1) as "202006",
  round("202007", 1) as "202007",round("202008", 1) as "202008",round("202009", 1) as "202009",
  round("202010", 1) as "202010",round("202011", 1) as "202011",round("202012", 1) as "202012"
from (  
  select shape || '-' || size as shape_size,
    avg(units) filter (where c.year_month = 201901) as "201901",
    avg(units) filter (where c.year_month = 201902) as "201902",
    avg(units) filter (where c.year_month = 201903) as "201903",
    avg(units) filter (where c.year_month = 201904) as "201904",
    avg(units) filter (where c.year_month = 201905) as "201905",
    avg(units) filter (where c.year_month = 201906) as "201906",
    avg(units) filter (where c.year_month = 201907) as "201907",
    avg(units) filter (where c.year_month = 201908) as "201908",
    avg(units) filter (where c.year_month = 201909) as "201909",
    avg(units) filter (where c.year_month = 201910) as "201910",
    avg(units) filter (where c.year_month = 201911) as "201911",
    avg(units) filter (where c.year_month = 201912) as "201912",
    avg(units) filter (where c.year_month = 202001) as "202001",
    avg(units) filter (where c.year_month = 202002) as "202002",
    avg(units) filter (where c.year_month = 202003) as "202003",
    avg(units) filter (where c.year_month = 202004) as "202004",
    avg(units) filter (where c.year_month = 202005) as "202005",
    avg(units) filter (where c.year_month = 202006) as "202006",
    avg(units) filter (where c.year_month = 202007) as "202007",
    avg(units) filter (where c.year_month = 202008) as "202008",
    avg(units) filter (where c.year_month = 202009) as "202009",
    avg(units) filter (where c.year_month = 202010) as "202010",
    avg(units) filter (where c.year_month = 202011) as "202011",
    avg(units) filter (where c.year_month = 202012) as "202012"    
  from inv_shape_size_by_day c
  group by shape || '-' || size) x
order by shape_size




select * from step_3 limit 10

-- priceband
-- first shot at line graphs
-- on the graph separated shapes to make them visible
-- graph_price_band_sales_inventory.csv
-- sales
select price_from, price_band, 'sales' as source,
  count(c.vin) filter (where c.year_month = 201901) as "201901",
  count(c.vin) filter (where c.year_month = 201902) as "201902",
  count(c.vin) filter (where c.year_month = 201903) as "201903",
  count(c.vin) filter (where c.year_month = 201904) as "201904",
  count(c.vin) filter (where c.year_month = 201905) as "201905",
  count(c.vin) filter (where c.year_month = 201906) as "201906",
  count(c.vin) filter (where c.year_month = 201907) as "201907",
  count(c.vin) filter (where c.year_month = 201908) as "201908",
  count(c.vin) filter (where c.year_month = 201909) as "201909",
  count(c.vin) filter (where c.year_month = 201910) as "201910",
  count(c.vin) filter (where c.year_month = 201911) as "201911",
  count(c.vin) filter (where c.year_month = 201912) as "201912",
  count(c.vin) filter (where c.year_month = 202001) as "202001",
  count(c.vin) filter (where c.year_month = 202002) as "202002",
  count(c.vin) filter (where c.year_month = 202003) as "202003",
  count(c.vin) filter (where c.year_month = 202004) as "202004",
  count(c.vin) filter (where c.year_month = 202005) as "202005",
  count(c.vin) filter (where c.year_month = 202006) as "202006",
  count(c.vin) filter (where c.year_month = 202007) as "202007",
  count(c.vin) filter (where c.year_month = 202008) as "202008",
  count(c.vin) filter (where c.year_month = 202009) as "202009",
  count(c.vin) filter (where c.year_month = 202010) as "202010",
  count(c.vin) filter (where c.year_month = 202011) as "202011",
  count(c.vin) filter (where c.year_month = 202012) as "202012"    
from step_3 c
group by price_from, price_band
union
-- inventory
select price_from, price_band, 'inv' as source,
  round("201901", 1) as "201901",round("201902", 1) as "201902",round("201903", 1) as "201903",
  round("201904", 1) as "201904",round("201905", 1) as "201905",round("201906", 1) as "201906",
  round("201907", 1) as "201907",round("201908", 1) as "201908",round("201909", 1) as "201909",
  round("201910", 1) as "201910",round("201911", 1) as "201911",round("201912", 1) as "201912",
  round("202001", 1) as "202001",round("202002", 1) as "202002",round("202003", 1) as "202003",
  round("202004", 1) as "202004",round("202005", 1) as "202005",round("202006", 1) as "202006",
  round("202007", 1) as "202007",round("202008", 1) as "202008",round("202009", 1) as "202009",
  round("202010", 1) as "202010",round("202011", 1) as "202011",round("202012", 1) as "202012"
from (  
  select price_from, price_band,
    avg(units) filter (where c.year_month = 201901) as "201901",
    avg(units) filter (where c.year_month = 201902) as "201902",
    avg(units) filter (where c.year_month = 201903) as "201903",
    avg(units) filter (where c.year_month = 201904) as "201904",
    avg(units) filter (where c.year_month = 201905) as "201905",
    avg(units) filter (where c.year_month = 201906) as "201906",
    avg(units) filter (where c.year_month = 201907) as "201907",
    avg(units) filter (where c.year_month = 201908) as "201908",
    avg(units) filter (where c.year_month = 201909) as "201909",
    avg(units) filter (where c.year_month = 201910) as "201910",
    avg(units) filter (where c.year_month = 201911) as "201911",
    avg(units) filter (where c.year_month = 201912) as "201912",
    avg(units) filter (where c.year_month = 202001) as "202001",
    avg(units) filter (where c.year_month = 202002) as "202002",
    avg(units) filter (where c.year_month = 202003) as "202003",
    avg(units) filter (where c.year_month = 202004) as "202004",
    avg(units) filter (where c.year_month = 202005) as "202005",
    avg(units) filter (where c.year_month = 202006) as "202006",
    avg(units) filter (where c.year_month = 202007) as "202007",
    avg(units) filter (where c.year_month = 202008) as "202008",
    avg(units) filter (where c.year_month = 202009) as "202009",
    avg(units) filter (where c.year_month = 202010) as "202010",
    avg(units) filter (where c.year_month = 202011) as "202011",
    avg(units) filter (where c.year_month = 202012) as "202012"    
  from inv_price_band_by_day c
  group by price_from, price_band) x
order by price_from, source;