﻿-- 02/11/21
-- fuck it go with the describe vehicle approach and update

first, update tem.step_3 shape_new, size_new with available data

select *  -- 370
from tem.step_3
where shape_new is null

-- 1. data from chr2.describe_vehicle_by_vin
update tem.step_3 x
set make_new = y.new_make,
    model_new = y.new_model,
    shape_new = y.new_shape,
    size_new = y.new_size
from (
  select distinct a.vin, a.make, a.model, a.shape, a.size,
    c->'division'->>'$value' as new_make, 
    c->'model'->>'$value' as new_model, 
    d.shape as new_shape, d.size as new_size
  from tem.step_3 a
  join chr2.describe_vehicle_by_vin b on a.vin = b.vin
  join jsonb_array_elements(b.response->'style') c on true
  left join veh.shape_size_classifications d on c->'division'->>'$value' = d.make
    and c->'model'->>'$value' = d.model
  where a.shape_new is null) y
where x.vin = y.vin  


-- 2. data from chr2.describe_vehicle_by_vin
update tem.step_3 x
set make_new = y.new_make,
    model_new = y.new_model,
    shape_new = y.new_shape,
    size_new = y.new_size
from (
  select distinct a.vin, a.make, a.model, a.shape, a.size,
    c->'division'->>'$value' as new_make, 
    c->'model'->>'$value' as new_model, 
    d.shape as new_shape, d.size as new_size
  from tem.step_3 a
  join chr.describe_vehicle b on a.vin = b.vin
  join jsonb_array_elements(b.response->'style') c on true
  left join veh.shape_size_classifications d on c->'division'->>'$value' = d.make
    and c->'model'->>'$value' = d.model
  where a.shape_new is null) y
where x.vin = y.vin  

-- 3. data from inpmast
update tem.step_3 x
set make_new = y.new_make,
    model_new = y.new_model,
    shape_new = y.new_shape,
    size_new = y.new_size
from (
  select distinct a.vin, a.make, a.model, a.shape, a.size,
    initcap(d.make) as new_make, 
    initcap(d.model) as new_model, 
    d.shape as new_shape, d.size as new_size
  from tem.step_3 a
  join (
      select a.vin, b.make, b.model, c.shape, c.size
      from tem.step_3 a
      left join arkona.ext_inpmast b on a.vin = b.inpmast_vin
      left join  veh.shape_size_classifications c on b.make = c.make and b.model = c.model
      where a.shape_new is null
        and c.shape is not null) d  on a.vin = d.vin 
    where a.shape_new is null) y
where x.vin = y.vin  


which leaves me with these 166 vehicles for which i need to do a describe vehicle request

select distinct a.vin 
from tem.step_3 a 
where shape_new is null
  and not exists (
    select 1
    from chr2.describe_vehicle_by_vin_with_tech
    where vin = a.vin);

!!! new table describe_vehicle_by_vin_with_tech !!!



1. step will be to update the make and models that can be done from existing data
god damn it think it all the way through this time
2. for vehicle remaining in tem.step_3 with a null shape_new, do a chr2.describe_vehicle_by_vin_with_tech
3. from that response populate all these tables:  (all of which are necessary for veh.vehicle_types)
    chr2.divisions
    chr2.subdivisions
    chr2.models
    chr2.style_ids
    -----
    chr2.style
    chr2.tech_specs
    chr2.bodies
    chr2.option
    chr2.image
    ---
    chr2.makes_models
    veh.makes
    veh.models
    veh.makes_models
    veh.types
so before i jump down the rabbit hole, lets verify that that is all doable using some existing
data from chr2.describe_vehicle_by_vin_with_tech comparing to the functions that currently populate all those tables

use this endpoint: https://beta.rydellvision.com:8888/chrome/describe-vehicle-equip-tech-specs/
it includes available equipment and tech specs


first glitch
looking at the chr2.image, table includes style_id, how is handling multiple style_ids going to work
----------------------------------------------------------------------------------------------
--< the chr2.xfm_functions
    -- this is for style_count = 1
    -- 02/10 thanks to afton, this handles all regardless of style_count
----------------------------------------------------------------------------------------------

DAMNIT I THINK I WANT THE ADS RESPONSE TO GO INTO A NEW TABLE
CHR2.DESCRIBE_VEHICLE_BY_VIN_WITH_TECH

02/13/21: added 304 more vins to handle 2019 - 2020 
2/19/21: added 41 more vins from drill_down_1.sql

-- chr2.xfm_divisions()
-- select * from chr2.divisions
-- alter table chr2.divisions
-- add column row_from_date date;
-- update chr2.divisions
-- set row_from_date = '01/21/2021';
insert into chr2.divisions(model_year,division_id,division,row_from_date)
select aa.model_year,aa.division_id,aa.division,current_date
from (
  select distinct (r.style -> 'attributes' ->> 'modelYear')::integer as model_year,
    (r.style -> 'division' -> 'attributes' ->> 'id')::integer as division_id,
    r.style -> 'division' ->> '$value' as division
  from chr2.describe_vehicle_by_vin_with_tech a
  join jsonb_array_elements(a.response->'style') as r(style) on true) aa
where not exists (
  select 1
  from chr2.divisions
  where model_year = aa.model_year
    and division_id = aa.division_id);

-- chr2.xfm_subdivisions()
-- select * from chr2.subdivisions
-- alter table chr2.subdivisions
-- add column row_from_date date;
-- update chr2.subdivisions
-- set row_from_date = '01/21/2021';
insert into chr2.subdivisions(model_year,subdivision_id,subdivision,row_from_date)
select aa.model_year,aa.subdivision_id,aa.subdivision,current_date
from (
  select distinct (r.style -> 'attributes' ->> 'modelYear')::integer as model_year,
    (r.style -> 'subdivision' -> 'attributes' ->> 'id')::integer as subdivision_id,
    r.style -> 'subdivision' ->> '$value' as subdivision
  from chr2.describe_vehicle_by_vin_with_tech a
  join jsonb_array_elements(a.response->'style') as r(style) on true) aa
where not exists (
  select 1
  from chr2.subdivisions
  where model_year = aa.model_year
    and subdivision_id = aa.subdivision_id);

-- chr2.xfm_models()
-- select * from chr2.models limit 100
-- alter table chr2.models
-- add column row_from_date date;
-- update chr2.models
-- set row_from_date = '01/21/2021';
insert into chr2.models(model_id,model_year,division_id,model,row_from_date)
select aa.model_id,aa.model_year,aa.division_id,aa.model,current_date
from (
  select distinct (r.style -> 'model' -> 'attributes' ->> 'id')::integer as model_id,
    (r.style -> 'attributes' ->> 'modelYear')::integer as model_year,
    (r.style -> 'division' -> 'attributes' ->> 'id')::integer as division_id,
    r.style -> 'model' ->> '$value' as model
  from chr2.describe_vehicle_by_vin_with_tech a
  join jsonb_array_elements(a.response->'style') as r(style) on true) aa
where not exists (
  select 1
  from chr2.models
  where model_id = aa.model_id);


-- chr2.xfm_style_ids
-- select * from chr2.style_ids limit 100
-- alter table chr2.style_ids
-- add column row_from_date date;
-- update chr2.style_ids
-- set row_from_date = '01/21/2021';
insert into chr2.style_ids (style_id,model_id,style_name,row_from_date)
select aa.style_id,aa.model_id,aa.style_name,current_date
from (
  select distinct(r -> 'attributes' ->> 'id')::integer as style_id,
    (r -> 'model' -> 'attributes' ->> 'id')::integer as model_id,
    a.response -> 'vinDescription' -> 'attributes' ->> 'styleName' as style_name
  from chr2.describe_vehicle_by_vin_with_tech a
  join jsonb_array_elements(a.response->'style') as r on true) aa
where not exists (
  select 1
  from chr2.style_ids
  where style_id::integer = aa.style_id);


    
----------------------------------------------------------------------------------------------
--/> the chr2.xfm_functions
----------------------------------------------------------------------------------------------    

----------------------------------------------------------------------------------------------
--< chr2.update_chr2_tables()
----------------------------------------------------------------------------------------------
    chr2.style
    chr2.tech_specs
    chr2.bodies
    chr2.option
    chr2.image
    chr2.makes_models
-- style -----------------------------------------------------------------------------------
-- alter table chr2.style
-- add column row_from_date date;
-- update chr2.style_ids
-- set row_from_date = '01/21/2021';

-- select string_agg('aa.'||column_name, ',' order by ordinal_position)
-- from information_schema.columns
-- where table_schema = 'chr2'
--   and table_name = 'style'

insert into chr2.style (input_row_number,passthru_id,style_id,division_id,subdivision_id,model_id,
  base_msrp,base_invoice,dest_chrg,is_price_unknown,market_class_id,model_year,style_name,
  style_name_wo_trim,trim_name,mfr_model_code,is_fleet_only,is_model_fleet,pass_doors,
  alt_model_name,alt_style_name,alt_body_type,drivetrain,row_from_date)
select aa.input_row_number,aa.passthru_id,aa.style_id,aa.division_id,aa.subdivision_id,aa.model_id,
  aa.base_msrp,aa.base_invoice,aa.dest_chrg,aa.is_price_unknown,aa.market_class_id,aa.model_year,
  aa.style_name,aa.style_name_wo_trim,aa.trim_name,aa.mfr_model_code,aa.is_fleet_only,
  aa.is_model_fleet,aa.pass_doors,aa.alt_model_name,aa.alt_style_name,aa.alt_body_type,
  aa.drivetrain,current_date
from (
  select distinct null::integer as input_row_number, null as passthru_id,
    (r -> 'attributes' ->> 'id')::integer as style_id,
    (r->'division'->'attributes'->>'id')::integer as division_id,
    (r->'subdivision'->'attributes'->>'id')::integer as subdivision_id,
    (r -> 'model' -> 'attributes' ->> 'id')::integer as model_id,
    (r -> 'basePrice' -> 'attributes' ->> 'msrp')::numeric as base_msrp,
    (r -> 'basePrice' -> 'attributes' ->> 'invoice')::numeric as base_invoice,
    (r -> 'basePrice' -> 'attributes' ->> 'destination')::numeric as dest_chrg,
    (r -> 'basePrice' -> 'attributes' ->> 'unknown')::boolean as is_price_unknown,
    (r -> 'marketClass' -> 'attributes' ->> 'id')::integer as market_class_id,
    (r -> 'attributes' ->> 'modelYear')::numeric as model_year,
    r -> 'attributes' ->> 'name' as style_name,
    r -> 'attributes' ->> 'nameWoTrim' as style_name_wo_trim,
    r -> 'attributes' ->> 'trim' as trim_name,
    r -> 'attributes' ->> 'mfrModelCode' as mfr_model_code,
    (r -> 'attributes' ->> 'fleetOnly')::boolean as is_fleet_only,
    (r -> 'attributes' ->> 'modelFleet')::boolean as is_model_fleet,
    (r -> 'attributes' ->> 'passDoors')::integer as pass_doors,
    r -> 'attributes' ->> 'altModelName' as alt_model_name,
    r -> 'attributes' ->> 'altStyleName' as alt_style_name,
    r -> 'attributes' ->> 'altBodyType' as alt_body_type,
    r -> 'attributes' ->> 'drivetrain' as drivetrain
  from chr2.describe_vehicle_by_vin_with_tech a
  join jsonb_array_elements(a.response->'style') as r on true) aa
where not exists (
  select 1
  from chr2.style
  where style_id = aa.style_id);

-- tech_specs -----------------------------------------------------------------------------------
-- alter table chr2.tech_specs
-- add column row_from_date date;
-- update chr2.tech_specs
-- set row_from_date = '01/21/2021';

-- 02/20/21 coalesced the_value to none
insert into chr2.tech_specs (style_id,title_id,group_name,header_name,title_name,condition,the_value,row_from_date)
select distinct aa.style_id,aa.title_id,aa.group_name,aa.header_name,aa.title_name,aa.condition,coalesce(aa.the_value, 'none')::citext, current_date
from (
  with test_table 
    as ( 
      select t.tech_specs ->> 'titleId' as title_id, t.tech_specs -> 'value' as the_array
      from chr2.describe_vehicle_by_vin_with_tech a
      join jsonb_array_elements(a.response->'technicalSpecification') as t(tech_specs) on true) 
  select distinct (jsonb_array_elements_text(styles->'styleId'))::integer as style_id, a.title_id::integer, 
    b.group_name, b.header_name, b.title_name,
    case 
      when styles->'attributes'->>'condition' = '' then 'none'
      else styles->'attributes'->>'condition'
    end::citext as condition,  
    case
      when styles->'attributes'->>'value' = '' then 'none' 
      else styles->'attributes'->>'value' 
    end::citext as the_value
  from test_table a
  join  jsonb_array_elements(the_array) as styles on true
  left join chr2.technical_specification_definition b on a.title_id::integer = b.title_id) aa
where not exists (
  select 1
  from chr2.tech_specs
  where style_id = aa.style_id
    and title_id = aa.title_id
    and condition = aa.condition
    and the_value = aa.the_value)
and not (aa.condition = 'none'::citext and aa.the_value = 'none'::citext);

-- chr2.bodies ---------------------------------------------------------------------------------
-- select * from chr2.bodies where style_id = 398652
-- alter table chr2.bodies
-- add column row_from_date date;
-- update chr2.bodies
-- set row_from_date = '01/21/2021';
insert into chr2.bodies (style_id,body,row_from_date)
select style_id, body, current_date
from (
  select style_id, array_to_string(ARRAY_agg(the_value order by the_id), '::') as body
  from (
    select distinct (r -> 'attributes' ->> 'id')::integer as style_id,
      b ->> '$value' as the_value, (b -> 'attributes' ->> 'id')::integer as the_id
    from chr2.describe_vehicle_by_vin_with_tech a
    join jsonb_array_elements(a.response->'style') as r on true
    join jsonb_array_elements(r -> 'bodyType') as b(bodies) on true) aa
  group by style_id) aa
where not exists (
  select 1
  from chr2.bodies
  where style_id = aa.style_id);

-- old vehicles without a bodyType object in the style object
insert into chr2.bodies (style_id,body,row_from_date)
select distinct aaa.style_id, bbb.response->'vinDescription'->'attributes'->>'bodyType', current_date
from (
  select aa.*
  from (
    select a.vin, (r -> 'attributes' ->> 'id')::integer as style_id
    from chr2.describe_vehicle_by_vin_with_tech a  
    join jsonb_array_elements(a.response->'style') as r on true) aa
  left join chr2.bodies bb on aa.style_id = bb.style_id
  where bb.style_id is null) aaa
left join chr2.describe_vehicle_by_vin_with_tech bbb on aaa.vin = bbb.vin
where not exists (
  select 1
  from chr2.bodies
  where style_id = aaa.style_id);



-- chr2.option ----------------------------------------------------------------------------------
/*
ok, got some serious problems trying to persist data from the describe_vehicle into the existing chr2.option table
the PK is chrome_code, style_id & ambiguous_option_id, the ambiguous_option_id values dont exist in the json
so, iam thinking, get just the values needed fro veh.vehicle_items: PEG
*/
-- alter table chr2.option
-- add column row_from_date date;
-- update chr2.option
-- set row_from_date = '01/21/2021';
-- 
-- select string_agg(column_name, ',' order by ordinal_position)
-- from information_schema.columns
-- where table_schema = 'chr2'
--   and table_name = 'option'

-- 02/19/21 had to add 4 more styleids to exclude
-- 02/20/21 coalesced chrome code to none
insert into chr2.option (input_row_num,passthru_id,ambiguous_option_id,header_id,header_name,descriptions,
  added_cat_ids,removed_cat_ids,msrp_low,msrp_high,invoice_low,invoice_high,is_unknown_price,style_id,
  install_cause,install_cause_detail,chrome_code,oem_code,alt_optioncode,is_standard,optionkind_id,
  utf,is_fleet_only,row_from_date)
select distinct bb.input_row_num,bb.passthru_id,bb.ambiguous_option_id,bb.header_id,bb.header_name,bb.descriptions,
  bb.added_cat_ids,bb.removed_cat_ids,bb.msrp_low,bb.msrp_high,bb.invoice_low,bb.invoice_high,bb.is_unknown_price,
  bb.style_id,bb.install_cause,bb.install_cause_detail,
  coalesce(bb.chrome_code, 'none'), bb.oem_code,bb.alt_optioncode,
  bb.is_standard,bb.optionkind_id,bb.utf,bb.is_fleet_only,current_date
from (
  select null::integer as input_row_num, null::citext as passthru_id, 0 as ambiguous_option_id,
    (c->'header'->'attributes'->>'id')::integer as header_id,
    c->'header'->>'$value' as header_name,
    de->>0 as descriptions,
    g.added_cat_ids, g.removed_cat_ids,
    (c->'price'->'attributes'->>'msrpMin')::numeric as msrp_low,
    (c->'price'->'attributes'->>'msrpMax')::numeric as msrp_high,
    (c->'price'->'attributes'->>'invoiceMin')::numeric as invoice_low,
    (c->'price'->'attributes'->>'invoiceMax')::numeric as invoice_high,
    (c->'price'->'attributes'->>'unknown')::boolean as is_unknown_price,
    (d.value->>0)::integer as style_id,
    c->'installed'->'attributes'->>'cause' as install_cause, null::citext as install_cause_detail,
    c->'attributes'->>'chromeCode' as chrome_code, c->'attributes'->>'oemCode' as oem_code,
    null::citext as alt_optioncode,
    (c->'attributes'->>'standard')::boolean as is_standard, (c->'attributes'->>'optionKindId')::integer as optionkind_id,
    c->'attributes'->>'utf' as utf, (c->'attributes'->>'fleetOnly')::boolean as is_fleet_only
  from chr2.describe_vehicle_by_vin_with_tech a
  join jsonb_array_elements(a.response->'factoryOption') as c on true
  join jsonb_array_elements(c->'styleId') as d on true
  join jsonb_array_elements(c->'description') as de on true
  left join ( -- separate query to aggregate added & removed cat_ids each into a single at tribute
    select style_id, oem_code, 
      string_agg(cat_id, ';') filter (where removed is null) as added_cat_ids,
      string_agg(cat_id, ';') filter (where removed = 'true') as removed_cat_ids
    from (
      select d.value as style_id, c->'attributes'->>'oemCode' as oem_code,
        e->'attributes'->>'id' as cat_id, e->'attributes'->>'removed' as removed
      from chr2.describe_vehicle_by_vin_with_tech a
      join jsonb_array_elements(a.response->'factoryOption') as c on true
      join jsonb_array_elements(c->'styleId') as d on true
      join jsonb_array_elements(c->'category') as e on true
      where c->'header'->>'$value' = 'PREFERRED EQUIPMENT GROUP') f
    group by style_id, oem_code ) g on d.value = g.style_id and c->'attributes'->>'oemCode' = g.oem_code
  where c->'header'->>'$value' = 'PREFERRED EQUIPMENT GROUP') bb    
where not exists (
  select 1
  from chr2.option
  where style_id = bb.style_id 
    and oem_code = bb.oem_code
    and ambiguous_option_id = bb.ambiguous_option_id)
and bb.style_id not in( 103545,103548,105009,104995,5681,4206,282365,282840)   
and chrome_code <> '-BASE1'
and not (chrome_code = '-RETC' and ambiguous_option_id = 0)
and not (chrome_code = '-RETSD' and ambiguous_option_id = 0);

-- chr2.image --------------------------------------------------------------------------------
-- alter table chr2.option
-- add column row_from_date date;
-- update chr2.bodies
-- set row_from_date = '01/21/2021';
insert into chr2.image(url,width,height,filename,is_stock,style_id)
select distinct r->'stockImage'->'attributes'->>'url' as url,
  (r->'stockImage'->'attributes'->>'width')::integer as width,
  (r->'stockImage'->'attributes'->>'height')::integer as height,
  r->'stockImage'->'attributes'->>'filename' as filename,
  TRUE as is_stock,
  (r -> 'attributes' ->> 'id')::integer as style_id
from chr2.describe_vehicle_by_vin_with_tech a
join jsonb_array_elements(a.response->'style') as r on true
where r->'stockImage'->'attributes'->>'url' is not null
  and not exists (
    select 1
    from chr2.image
    where style_id = (r -> 'attributes' ->> 'id')::integer);

  
-- chr2.makes_models -------------------------------------------------------------------------
-- need to add oldsmobile to makes

-- alter table chr2.makes_models
-- add column row_from_date date;
-- update chr2.makes_models
-- set row_from_date = '01/21/2021';

-- don't know if this will hold up as a template for unearthing new makes, but it is a shot
-- this query should return 41 rows
select * 
from (
  select distinct b.division
  from chr2.style a
  join chr2.divisions b on a.division_id = b.division_id
  where division <> 'maserati'
  order by b.division) aa
full outer join veh.makes bb on aa.division = bb.make


insert into chr2.makes_models
select b.division as make, d.subdivision, e.model, current_date
from chr2.style a
join chr2.divisions b on a.division_id = b.division_id
-- join veh.makes c on b.division = c.make
join chr2.subdivisions d on a.subdivision_id = d.subdivision_id
  and d.subdivision not in ('Chevy Kodiak C-Series','Chevy W-Series',
    'GMC TopKick C-Series','GMC T-Series','Chevy T-Series','GMC W-Series',
    'Chevy Medium Duty','Ford Medium-Duty Chassis')
join chr2.models e on a.model_id = e.model_id
where b.division in ('Oldsmobile','Acura','Audi','BMW','Buick','Cadillac','Chevrolet','Chrysler','Dodge','Ford',
                    'GMC','Honda','Hyundai','INFINITI','Jaguar','Jeep','Kia','Land Rover','Lexus','Lincoln','Mazda',
                    'Mercedes-Benz','MINI','Mitsubishi','Nissan','Porsche','Ram','smart','Subaru','Tesla','Toyota',
                    'Volkswagen','Volvo','Hummer','Isuzu','Mercury','Pontiac','Saab','Saturn','Scion','Suzuki')
  and not exists (
    select 1 
    from chr2.makes_models
    where model = e.model
      and make = b.division)                    
group by b.division, d.subdivision, e.model;  

----------------------------------------------------------------------------------------------
--/> chr2.update_chr2_tables()
----------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------
--< chr2.update_veh_tables()
----------------------------------------------------------------------------------------------

-- veh.makes -------------------------------------------------------------------------------------

-- alter table veh.makes
-- add column row_from_date date;
-- update veh.makes
-- set row_from_date = '01/21/2021';

insert into veh.makes
select make, current_date
from chr2.makes_models a
where not exists (
  select 1
  from veh.makes_models
  where make = a.make)
group by make;   

-- veh.models -------------------------------------------------------------------------------------

-- alter table veh.models
-- add column row_from_date date;
-- update veh.models
-- set row_from_date = '01/21/2021';

insert into veh.models
select model, current_date
from chr2.makes_models a
where not exists (
  select 1
  from veh.models
  where model = a.model)
group by model; 


-- alter table veh.makes_models
-- add column row_from_date date;
-- update veh.makes_models
-- set row_from_date = '01/21/2021';

insert into veh.makes_models
select make, model, current_date
from chr2.makes_models a
where not exists (
  select 1 
  from veh.makes_models
  where make = a.make
    and model = a.model)
group by make, model;   


-- veh.vehicle_types --------------------------------------------------------------------------------

-- alter table veh.vehicle_types
-- add column row_from_date date;
-- update veh.vehicle_types
-- set row_from_date = '01/21/2021';

-- 1st issue, query uses alt_body_type to derive cab and bed, these values appear to not exist in pre 1995 vehicles
--   nor is the data in tech_specs, same thing with drivetrain in cars

insert into veh.vehicle_types
-- drop table if exists tem.wtf; create table tem.wtf as
select distinct a.style_id, b.model_year, b.division, c.subdivision, d.model, 
  case
    when b.model_year < 1997 and a.mfr_model_code is null then 'none'
    else a.mfr_model_code
  end as mfr_model_code,
  e.body, a.alt_body_type, 
  coalesce(
    case
      when a.drivetrain = 'Front Wheel Drive' then 'FWD'
      when a.drivetrain = 'All Wheel Drive' then 'AWD'
      when a.drivetrain = 'Four Wheel Drive' then '4WD'
      when a.drivetrain = 'Rear Wheel Drive' then 'RWD'
    end, n.drivetrain) as drivetrain,
  a.trim_name, a.style_name, a.style_name_wo_trim, a.pass_doors, 
  coalesce(l.passenger_capacity, 'unknown'),
  coalesce(
    case
      when a.alt_body_type like '%cab%' then 
      left(a.alt_body_type, position('Cab' in a.alt_body_type) -1)
    end, m.cab) as cab,
  case 
    when a.alt_body_type like '%Bed%' then   
    replace(
      substring(a.alt_body_type, position('-' in a.alt_body_type) + 2, 
        position('Bed' in a.alt_body_type) -2), 'Bed', '')
  end as bed,  f.peg,
  g.wheelbase, h.overall_length,  
  j.the_value as segment, k.the_value as vehicle_type,
  i.url as image_url,
  current_date as row_from_date
from chr2.style a
join chr2.divisions b on a.division_id = b.division_id and a.model_year = b.model_year
join chr2.subdivisions c on a.subdivision_id = c.subdivision_id and a.model_year = c.model_year
join chr2.models d on a.model_id = d.model_id and a.model_year = d.model_year
join chr2.makes_models aa on b.division = aa.make and d.model = aa.model and c.subdivision = aa.subdivision-- limit based on subdivision & 41 makes
left join chr2.bodies e on a.style_id = e.style_id 
left join ( -- 2005: multiple peg for some models
  select style_id, string_agg(distinct oem_code, ',') as peg
  from chr2.option
  where header_name = 'PREFERRED EQUIPMENT GROUP'
  group by style_id) f on a.style_id = f.style_id
left join ( -- wheelbase 
  select style_id, string_agg(the_value, ',') as wheelbase
  from chr2.tech_specs 
  where title_id = 301
  group by style_id) g on a.style_id = g.style_id
left join ( -- overall_length 
  select style_id, string_agg(the_value, ',') as overall_length
  from chr2.tech_specs 
  where title_id = 304
  group by style_id) h on a.style_id = h.style_id
left join chr2.image i on a.style_id = i.style_id
left join chr2.tech_specs j on a.style_id = j.style_id
  and j.title_id = 1113  -- segment
left join chr2.tech_specs k on a.style_id = k.style_id
  and k.title_id = 1114  -- vehicle_type
left join (
  select style_id, string_agg(distinct the_value, ',') as passenger_capacity
  from chr2.tech_specs 
  where title_name = 'passenger capacity'
  group by style_id) l on a.style_id = l.style_id 
left join (-- old vehicles with no alt_body_type in style->attributes, also no relevant tech_specs, hence no cab or bed, this gives cab
  select style_id, left(style_name, position('Cab' in style_name) - 2) as cab, current_date as row_from_date
  from (
    select distinct aaa.style_id, bbb.response->'vinDescription'->'attributes'->>'styleName' as style_name, current_date
    from (
      select aa.*
      from ( -- get the style_ids from the json
        select a.vin, (r -> 'attributes' ->> 'id')::integer as style_id
        from chr2.describe_vehicle_by_vin_with_tech a  
        join jsonb_array_elements(a.response->'style') as r on true) aa
      join ( -- get the style_ids of pickup styles with no alt_body_type
        select style_id 
        from chr2.style f
        join chr2.subdivisions g on f.subdivision_id = g.subdivision_id
        where f.alt_body_type is null 
          and g.subdivision like '%pick%') bb on aa.style_id = bb.style_id) aaa
    left join chr2.describe_vehicle_by_vin_with_tech bbb on aaa.vin = bbb.vin) ccc) m on a.style_id = m.style_id
left join (-- old vehicles with no drivetrain in style->attributes, cars have no drivetrain, but trucks do
  select style_id, right(style_name, 3) as drivetrain, current_date as row_from_date
  from (
    select distinct aaa.style_id, bbb.response->'vinDescription'->'attributes'->>'styleName' as style_name, current_date
    from (
      select aa.*
      from ( -- get the style_ids from the json
        select a.vin, (r -> 'attributes' ->> 'id')::integer as style_id
        from chr2.describe_vehicle_by_vin_with_tech a  
        join jsonb_array_elements(a.response->'style') as r on true) aa
      join ( -- get the style_ids of pickup styles with no drivetrain attribute
        select style_id 
        from chr2.style f
        join chr2.subdivisions g on f.subdivision_id = g.subdivision_id
        where f.drivetrain is null 
          and g.subdivision like '%pick%') bb on aa.style_id = bb.style_id) aaa
    left join chr2.describe_vehicle_by_vin_with_tech bbb on aaa.vin = bbb.vin) ccc) n on a.style_id = n.style_id    
where not exists (
    select 1
    from veh.vehicle_types
    where style_id = a.style_id);
      
      
-- veh.additional_classification_data ---------------------------------------------------------------

-- yikes, this is where make model classification is used, i want to use the new ones, so ...
-- what do i need to do to get the new makes and models to show up on the classifications page
-- got, that, i had not done the veh.makes_models insert
-- now, for images, need this table, 
-- actually i can go ahead and generate it, then once everything is classified, change this query
-- to use the new classifications
-- so, gulp, run it
-- done and everything classifiied

-- 02/19/21 just to reiterate, before running this, i actually need to update the classifications in vision, which, today
-- seems goofy because there is none of the support data available yet, eg wheelbase, image, etc 
-- so i have done the classification, now i will run this
-- and now the support data is available on the vision page, guess if need be i can always reclassify the vehicles

  truncate veh.additional_classification_data;
  insert into veh.additional_classification_data
  select aa.chrome_make, aa.chrome_model, aa.subdivision, aa.wheelbase, aa.overall_length,
    aa.pass_doors, aa.drivetrain, aa.type_segment,
     bb.model as polk_model, bb.polk_segment,
    cc.model as tool_model, cc.tool_shape_size, cc.luxury as tool_luxury, cc.comm as tool_comm, cc.sport as tool_sport,
    aa.image_url_1 as image_one, aa.image_url_2 as image_two
  from (
    select a.make as chrome_make, a.model as chrome_model, a.subdivision, 
      case
        when cardinality(array_agg(distinct a.wheelbase)) = 1 then (min(a.wheelbase))::text
        else (min(a.wheelbase))::text || '-' || (max(a.wheelbase))::text
      end as wheelbase,
      case
        when cardinality(array_agg(distinct a.overall_length)) = 1 then (min(a.overall_length))::text
        else (min(a.overall_length))::text || '-' || (max(a.overall_length))::text
      end as overall_length,
      case
        when cardinality(array_agg(distinct a.pass_doors)) = 1 then (min(a.pass_doors))::text
        else (min(a.pass_doors))::text || '-' || (max(a.pass_doors))::text
      end as pass_doors,  string_agg(distinct a.drivetrain, ',') as drivetrain,
      max(b.type_segment) as type_segment, min(image_url) as image_url_1, max(image_url) as image_url_2
    from veh.vehicle_types a 
    left join ( -- generates a vertical list of types/segments
      select make, model, string_agg(distinct coalesce(vehicle_type, '') ||  ' : ' || coalesce(segment, ''), E'\n') as type_segment 
      from veh.vehicle_types 
      where vehicle_type is not null
        and segment is not null
      group by make, model) b on a.make = b.make and a.model = b.model
    group by a.make, a.subdivision, a.model) aa
  left join (
    select a.make, a.model, string_agg(distinct a.segment, ',') as polk_segment
    from polk.vehicle_categories a
    join veh.makes_models b on a.make = b.make
    group by a.make, a.model) bb on aa.chrome_make = bb.make and aa.chrome_model = bb.model
  left join (
    select b.make, b.model, string_agg(distinct split_part(b.vehicletype, '_', 2) ||'-'||split_part(b.vehiclesegment, '_', 2),',') as tool_shape_size,
      case when string_agg(distinct luxury::text,',') like '%true%' then 'true' else null end as luxury, 
      case when string_agg(distinct commercial::text,',') like '%true%' then 'true' else null end as comm, 
      case when string_agg(distinct sport::text,',') like '%true%' then 'true' else null end as sport
    from ads.ext_make_model_classifications b
    join veh.makes_models c on b.make = c.make
    group by b.make, b.model) cc on coalesce(aa.chrome_make, bb.make) = cc.make  and coalesce(aa.chrome_model, bb.model) = cc.model;

----------------------------------------------------------------------------------------------
--/> chr2.update_veh_tables()
----------------------------------------------------------------------------------------------

