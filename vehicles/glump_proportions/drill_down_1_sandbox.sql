﻿
select count(*) from tem.inventory_2019_2020

select a.*, b.vin, c.stocknumber, d.vehicle_walk_ts::date, e.status, e.fromts::date, e.thruts::date
from tem.inventory_2019_2020 a
left join ads.ext_vehicle_items b on a.vin = b.vin
left join ads.ext_vehicle_inventory_items c on a.stock_number = c.stocknumber
left join ads.ext_vehicle_walks d on c.vehicleinventoryitemid = d.vehicle_inventory_item_id::citext
left join ads.ext_vehicle_inventory_item_statuses e on c.vehicleinventoryitemid = e.vehicleinventoryitemid
  and e.status = 'RMFlagAV_Available'
order by a.stock_number, e.fromts::date
limit 100

select b.* 
from ads.ext_Vehicle_inventory_items a
left join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
where a.stocknumber = 'G37311G'
order by b.fromts 

select * from greg.uc_daily_snapshot_beta_1 a where the_Date = current_Date

select a.*, 
  b.date_priced, b.best_price, b.days_since_priced, 
  b.days_owned, b.status, b.days_in_status, b.sale_type
from tem.inventory_2019_2020_days a
left join greg.uc_daily_snapshot_beta_1 b on a.stock_number = b.stock_number
  and a.the_date = b.the_date
limit 1000 

select a.*, e.fromts::date as date_available
from tem.inventory_2019_2020 a
left join ads.ext_vehicle_inventory_items c on a.stock_number = c.stocknumber
left join ads.ext_vehicle_inventory_item_statuses e on c.vehicleinventoryitemid = e.vehicleinventoryitemid
  and e.status = 'RMFlagAV_Available'

-- 44 with date_sold but no sale type
select a.*, 
  coalesce(split_part(d.typ, '_', 2), case when e.sale_type = 'R' then 'Retail' else 'Wholesale' end),
  case when date_sold = '12/31/9999' then current_date - date_acquired end as age,
  e.date_Capped, e.sale_type
from tem.inventory_2019_2020 a
left join ads.ext_vehicle_inventory_items c on a.stock_number = c.stocknumber
left join ads.ext_vehicle_sales d on c.vehicleinventoryitemid = d.vehicleinventoryitemid
  and d.status <> 'VehicleSale_SaleCanceled'
left join arkona.ext_bopmast e on a.stock_number = e.bopmast_stock_number
  and e.record_status = 'U'  


-- 41 with no viitem
-- the big problem here is intra market, pricing before and after vehicle was at ry1
-- results in 507 without a price, looks like mostly G units
-- set the exclusions, then use bopmast? looks good enough
select x.stock_number, coalesce(y.date_priced, x.date_sold) as date_priced, coalesce(y.best_price, z.retail_price) as best_price  
from tem.inventory_2019_2020 x 
left join ( -- the pricings
  select aa.stock_number, aa.vin, date_acquired, date_sold, aa.date_priced, aa.amount as best_price
  from (
    select a.stock_number, a.vin, date_acquired, a.date_sold, c.vehiclepricingts, c.vehiclepricingts::date date_priced, d.amount::integer
    from tem.inventory_2019_2020 a
    left join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
    left join ads.ext_vehicle_pricings c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
    left join ads.ext_vehicle_pricing_details d on c.vehiclepricingid = d.vehiclepricingid
      and d.typ = 'VehiclePricingDetail_BestPrice') aa
  join ( -- this ensures one price per day
    select a.stock_number, max(c.vehiclepricingts) as vehiclepricingts
    from tem.inventory_2019_2020 a
    left join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
    left join ads.ext_vehicle_pricings c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
    group by a.stock_number, c.vehiclepricingts::date) bb on aa.stock_number = bb.stock_number
      and aa.vehiclepricingts = bb.vehiclepricingts
  where aa.date_priced >= aa.date_acquired
    and aa.date_priced <= aa.date_sold) y on x.stock_number = y.stock_number
left join arkona.ext_bopmast z on x.stock_number = z.bopmast_stock_number;


delete from tem.inventory_2019_2020 where stock_number = 'UNKNOWN'



drop table if exists test_base cascade;
create temp table test_base as
select stock_number, count(*)
from tem.inventory_2019_2020_days a
where exists (
  select 1
  from tem.inventory_2019_2020_days
  where stock_number = a.stock_number
    and status is not null)
group by stock_number
having count(*) < 30  and count(*) > 2
limit 10;

drop table if exists wtf cascade;
create temp table wtf as
select a.* from tem.inventory_2019_2020_days a 
join test_Base b on a.stock_number = b.stock_number
order by a.stock_number, the_date



select * from wtf where disposition is not null

update wtf x
set disposition = y.first_value
from ( 
select the_date, stock_number, status, 
  first_value(status) over (partition by stock_number, status_partition order by the_date)
from (
  select the_date, stock_number, age, status, 
    sum(case when status is null then 0 else 1 end) over (partition by stock_number order by the_date) as status_partition
  from wtf
  order by stock_number, the_date) a) y
where x.stock_number = y.stock_number
  and x.the_date = y.the_date  


drop table if exists wtf cascade;
create temp table wtf as
select * from tem.inventory_2019_2020_days where stock_number in ('31689B','31345C','31048RB');


select the_date, stock_number, 
  case
    when status = 'available' then
      case
        when (the_date + 1) - first_value(the_date) over (partition by stock_number,status order by stock_number, the_date) < 31 then 'avail_fresh'
        else 'avail_aged'
      end
  end as avail_status
from wtf
where status = 'available'
order by stock_number, the_date


drop table if exists wtf_2 cascade;
create temp table wtf_2 as
select a.the_date, a.stock_number, a.status, age, b.date_priced, b.best_price 
from wtf a
left join tem.pricings b on a.stock_number = b.stock_number and a.the_date = b.date_priced
order by a.stock_number, the_date;

select * from wtf_2

select the_date, stock_number, best_price, date_priced, the_date - date_priced as days_since
from (
select the_date, stock_number, status, 
  first_value(date_priced) over (partition by stock_number, date_partition order by the_date) as date_priced,
  first_value(best_price) over (partition by stock_number, price_partition order by the_date) as best_price
from (
  select the_date, stock_number, status, age, date_priced, best_price,
    sum(case when date_priced is null then 0 else 1 end) over (partition by stock_number order by the_date) as date_partition,
    sum(case when best_price is null then 0 else 1 end) over (partition by stock_number order by the_date) as price_partition
  from wtf_2
  order by stock_number, the_date) a) b




select *
  from (
    select vehicleinventoryitemid,date_priced,best_price,
      coalesce(lead(date_priced) over (
        partition by vehicleinventoryitemid order by date_priced) - 1, '12/31/9999') as priced_thru
    from (
      select a.vehicleinventoryitemid, 
        a.vehiclepricingts::date as date_priced, 
        last_value(c.amount) over (
            partition by vehicleinventoryitemid, vehiclepricingts::date 
            order by vehicleinventoryitemid, vehiclepricingts  
            RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as best_price     
      from ads.ext_vehicle_pricings a
      inner join greg.uc_base_vehicles aa on a.vehicleinventoryitemid = aa.vehicle_inventory_item_id
        and aa.sale_date >= current_date
      left join ads.ext_vehicle_pricing_details b on a.vehiclepricingid = b.vehiclepricingid
        and b.typ = 'VehiclePricingDetail_Invoice'
      left join ads.ext_vehicle_pricing_details c on a.vehiclepricingid = c.vehiclepricingid
        and c.typ = 'VehiclePricingDetail_BestPrice'
      where a.vehicleinventoryitemid = '1fd01605-09ad-49b1-a704-132306625837') x
    group by vehicleinventoryitemid,date_priced,best_price) x
order by vehicleinventoryitemid, date_priced    