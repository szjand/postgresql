﻿
---------------------------------------------------------------------------------
--< fucking around with fords
---------------------------------------------------------------------------------

select distinct a.stock_number, a.vin, a.make, a.model, b.style_count, c.year, c.model_code, c.body_style, c.trim, d.bodystyle, d.trim, d.yearmodel,vinresolved,
  (style ->'attributes'->>'id')::integer as style_id, style->'attributes'->>'trim', style->'attributes'->>'mfrModelCode',style->'attributes'->>'altBodyType'
from tem.inventory_2019_2020 a
left join chr2.describe_vehicle_by_vin_with_tech b on a.vin = b.vin
left join jsonb_array_elements(b.response->'style') as style on true
left join arkona.ext_inpmast c on a.vin = c.inpmast_vin
left join ads.ext_vehicle_items d on a.vin = d.vin 
where style_id is null 
  and a.make = 'ford'
--   and a.model like '%150%'
--   and d.trim = (style->'attributes'->>'trim')::citext
--   and a.vin = '1FT7W2B66CEB29300'
order by vin, style_id  

select a.*, b.style_count, c.year, c.model_code, c.body_style, c.trim, d.bodystyle, d.trim, d.yearmodel,vinresolved,
  (style ->'attributes'->>'id')::integer as style_id, style->'attributes'->>'trim', style->'attributes'->>'mfrModelCode',style->'attributes'->>'altBodyType',
  count(*) over(partition by a.vin)
from (
  select vin, make, model 
  from tem.inventory_2019_2020 
  where style_id is null
  group by vin, make, model) a
left join chr2.describe_vehicle_by_vin_with_tech b on a.vin = b.vin
left join jsonb_array_elements(b.response->'style') as style on true
left join arkona.ext_inpmast c on a.vin = c.inpmast_vin
left join ads.ext_vehicle_items d on a.vin = d.vin 
where a.make = 'ford'
  and a.model like '%150%'
  and d.trim = (style->'attributes'->>'trim')::citext
--   and a.vin = '1FTPX14564NC31483'
order by vin  

select * from chr2.describe_vehicle_by_vin_with_tech where vin = '1FTPX14564NC31483'

select * 
from nc.vehicles
limit 100


-- 74 short, 25 standard, 1 long
select round(100.0 * (short * 1.0/total), 0) as short,
  round(100.0 * (long * 1.0/total), 0) as long,
  round(100.0 * (standard * 1.0/total), 0) as standard
from (  
select count(*) as total,
  count(*) filter (where box_size = 'short') as short,
  count(*) filter (where box_size = 'long') as long,
  count(*) filter (where box_size = 'standard') as standard
from nc.vehicle_configurations a
join nc.vehicles b on a.configuration_id = b.configuration_id
where a.model like '%1500%') x

-- 68 standard, 32 long
select round(100.0 * (short * 1.0/total), 0) as short,
  round(100.0 * (long * 1.0/total), 0) as long,
  round(100.0 * (standard * 1.0/total), 0) as standard
from (  
select count(*) as total,
  count(*) filter (where box_size = 'short') as short,
  count(*) filter (where box_size = 'long') as long,
  count(*) filter (where box_size = 'standard') as standard
from nc.vehicle_configurations a
join nc.vehicles b on a.configuration_id = b.configuration_id
where a.model like '%2500%') x

-- 52 standard, 41 long  (n/a)
select round(100.0 * (short * 1.0/total), 0) as short,
  round(100.0 * (long * 1.0/total), 0) as long,
  round(100.0 * (standard * 1.0/total), 0) as standard
from (  
select count(*) as total,
  count(*) filter (where box_size = 'short') as short,
  count(*) filter (where box_size = 'long') as long,
  count(*) filter (where box_size = 'standard') as standard
from nc.vehicle_configurations a
join nc.vehicles b on a.configuration_id = b.configuration_id
where a.model like '%3500%') x

select a.vin, min((style ->'attributes'->>'id')::integer), max((style ->'attributes'->>'id')::integer)
from (
  select vin, make, model 
  from tem.inventory_2019_2020 
  where style_id is null
  group by vin, make, model) a
left join chr2.describe_vehicle_by_vin_with_tech b on a.vin = b.vin
left join jsonb_array_elements(b.response->'style') as style on true
left join arkona.ext_inpmast c on a.vin = c.inpmast_vin
left join ads.ext_vehicle_items d on a.vin = d.vin 
where a.make = 'ford'
--   and a.model like '%150%'
  and d.trim = (style->'attributes'->>'trim')::citext
  and style->'attributes'->>'altBodyType' like '%Pickup%'
group by a.vin

select a.*, b.style_count, c.year, 
  (style ->'attributes'->>'id')::integer as style_id, style->'attributes'->>'altBodyType',
  count(*) over(partition by a.vin)
from (
  select vin, make, model 
  from tem.inventory_2019_2020 
  where style_id is null
  group by vin, make, model) a
left join chr2.describe_vehicle_by_vin_with_tech b on a.vin = b.vin
left join jsonb_array_elements(b.response->'style') as style on true
left join arkona.ext_inpmast c on a.vin = c.inpmast_vin
left join ads.ext_vehicle_items d on a.vin = d.vin 
where a.make = 'ford'
  and d.trim = (style->'attributes'->>'trim')::citext
--   and a.vin = '1FTPX14564NC31483'
order by vin, style_id
---------------------------------------------------------------------------------
--/> fucking around with fords
---------------------------------------------------------------------------------






---------------------------------------------------------------------------------
-- < 02/21
---------------------------------------------------------------------------------
this is about figuring out how i ended up with vehicles in tem.inventory_2019_2929 that are not
in veh.vehicle_types


ok, so the coverage in vehicle_types looks good for step_3 and inv_2
select *
from (
select coalesce(make_new,make) as make, coalesce(model_new, model) as model from tem.step_3 
union
select distinct make, model from tem.inv_2) a
where not exists (
  select 1
  from veh.vehicle_types
  where make = a.make and model = a.model)

but the issue is not step_3 and inv_2
the issue is vehicles in inventory_2019_2020 that are not in vehicle_types
and how did that happen

step_3 and inv_2 _5 done line 403
but those dont even matter, the actual vehicles being used are based on tem.inpmat, greg & tool -> tem.acct -> tem.vehicles line 200
and besides line 343 verifies only 42 from tem.vehicles not in step_3 & inv_2
step_3 and inv_2 _5 done line 403

-- 94 distinct style_ids
select distinct a.style_id
from tem.inventory_2019_2020  a
left join veh.vehicle_types b on a.style_id = b.style_id
where b.style_id is null

-- thinking it is due to using this query to generate the vins,
-- but when i updated tem.inventory i only used describe_vehicle_by_vin_with_tech
-- don't know if this is an issue or not
select distinct vin 
from tem.inventory_2019_2020 a
where a.style_id is null
  and not exists (
    select 1
    from chr2.describe_vehicle_by_vin_with_tech
    where vin = a.vin)
  and not exists (
    select 1
    from chr.describe_vehicle
    where vin = a.vin)
  and not exists (
    select 1
    from chr2.describe_vehicle_by_vin
    where vin = a.vin)
  and a.vin not in ('1HD1GX1336K331300','1HD1MAL15FB855467','1HD4NBB1XGC504180'); 


-- -- 1 fuck up, model code instead of style id
-- only 3 exist in describe_vehicle_by_vin_with_tech
select *
from ( -- 94 distinct style_ids no in vehicle_types
select distinct a.style_id
from tem.inventory_2019_2020  a
left join veh.vehicle_types b on a.style_id = b.style_id
where b.style_id is null) aa
left join (
select (style->'attributes'->>'id')::integer as style_id
from chr2.describe_vehicle_by_vin_with_tech a
join jsonb_array_elements(a.response->'style') as style on true) bb on aa.style_id = bb.style_id


select * from veh.vehicle_types where make = 'nissan' and model = 'titan' order by style_id

select * from chr2.describe_vehicle_by_vin_with_tech where vin = '1N6BA07C28N346519'

update tem.inventory_2019_2020 set style_id = 291762 where vin = '1N6BA07C28N346519'


select * from chr2.describe_vehicle_by_vin_with_tech where vin = '1GCVKREC1HZ325044' 



-- on the missing 93, check for match describe_vehicle_by_vin_with_tech 
select * 
from (
  select a.*
  from tem.inventory_2019_2020  a
  left join veh.vehicle_types b on a.style_id = b.style_id
  where b.style_id is null) aa

this vin, 1GCVKREC1HZ325044 has a style_id in inventory, style_id does not exist in vehicle_types,
how the fuck did it get there??

select * from chr2.describe_vehicle_by_vin_with_tech where vin = '1GCVKREC1HZ325044'
select * from chr2.describe_vehicle_by_vin where vin = '1C4RJFCG0JC212008'
select * from chr.describe_vehicle where vin = '1GCVKREC1HZ325044'  

-- and what about this fucking disaster
-- these are the ones that were covered by 
select * from tem.inventory_2019_2020 a
where not exists (
  select 1
  from chr2.describe_vehicle_by_vin_with_tech
  where vin = a.vin)

select count(*) from tem.inventory_2019_2020; --6150
select * from tem.inventory_2019_2020 where vin = '2D4RN3D11AR430157'
select * from veh.vehicle_types where style_id = 324355
-- holy shit, 2420 with no vin decoding
select a.vin, b.vin as b_vin, c.vin as c_vin, d.vin as d_vin
from tem.inventory_2019_2020 a
left join chr2.describe_vehicle_by_vin_with_tech b on a.vin = b.vin
left join chr2.describe_vehicle_by_vin c on a.vin = c.vin
left join chr.describe_vehicle d on a.vin = d.vin
where b.vin is null and c.vin is null and d.vin is null

02/21 conclusion
for now dont fucking worry about the lack of chrome
all i need to do is make sure all the inventory records are in vehicle_types

-- on the missing 93, check for match describe_vehicle_by_vin_with_tech 
select distinct aa.vin
from (
  select a.*
  from tem.inventory_2019_2020  a
  left join veh.vehicle_types b on a.style_id = b.style_id
  where b.style_id is null) aa
left join chr2.describe_vehicle_by_vin_with_tech bb on aa.vin = bb.vin
where bb.vin is null

---------------------------------------------------------------------------------
-- /> 02/21
---------------------------------------------------------------------------------
even after fucking all that, still end up with 3 not in vehicel_types
-- now i fucking know i did everything but
-- whey the fuck are these not in vehicle_types 
select * 
from tem.inventory_2019_2020 a
left join veh.vehicle_types b on a.style_id = b.style_id
where b.style_id is null 

Chrysler;Sebring
Cadillac;DeVille
Cadillac;DeVille
Chrysler;Town & Country

-- this is the base for veh.vehicle_types
ok, they are ok up to chr2.makes_models
select b.*,c.* ,d.*
from chr2.style a
join chr2.divisions b on a.division_id = b.division_id and a.model_year = b.model_year
join chr2.subdivisions c on a.subdivision_id = c.subdivision_id and a.model_year = c.model_year
join chr2.models d on a.model_id = d.model_id and a.model_year = d.model_year
join chr2.makes_models aa on b.division = aa.make and d.model = aa.model and c.subdivision = aa.subdivision-- limit based on subdivision & 41 makes
where a.style_id in (103627,5632,105095)

-- these subdivisions are different
Cadillac;Cadillac Cars
Chrysler;Chrysler Passenger Vans
Chrysler;Chrysler Cars

select * from chr2.makes_models
where
  (make = 'cadillac' and model = 'deville')
or (make = 'chrysler' and model = 'sebring')
or (make = 'chrysler' and model = 'town & country')

-- than these
select * from chr2.subdivisions where subdivision_id in (128,1529,1530)
1530;Chrysler
128;Cadillac
1529;Chrysler

update chr2.subdivisions
set subdivision = 'Cadillac Cars'
where subdivision_id = 128;
update chr2.subdivisions
set subdivision = 'Chrysler Cars'
where subdivision_id = 1529;
update chr2.subdivisions
set subdivision = 'Chrysler Passenger Vans'
where subdivision_id = 1530;


---------------------------------------------------------------------------------
-- < 02/21 #2
---------------------------------------------------------------------------------



---------------------------------------------------------------------------------
-- /> 02/21 #2
---------------------------------------------------------------------------------
