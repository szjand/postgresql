﻿start with large car 10/19 (18) vs 11/19 (8)

1st iteration, look at daily inventory (stacked bar graph) with inventory age status

-- worried about the veracity of vehicles in inventory 2 years ago, still not sold
none of these are stock#s are in vii, all the vins are in vi
select a.stock_number, a.vin, a.make, a.model, a.date_acquired, b.stocknumber, c.vin
from greg.uc_daily_snapshot_beta_1 a
left join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
left join ads.ext_vehicle_items c on a.vin = c.vin
where a.the_date = '10/01/2019' and a.store_code = 'RY1'
  and a.sale_date = '12/31/9999'
  


select * from greg.uc_daily_snapshot_beta_1 where stock_number = 'H11591A'  

------------------------------------------------------------------
--< compare inventory on 10/01/2019
------------------------------------------------------------------
initially, there seems to be 2 issues
  the existence of a vehicle in inventory
  the period of existence in inventory
  
drop table if exists inpmast_10_01 cascade;
create temp table inpmast_10_01 as
select a.inpmast_stock_number as stock_number, a.inpmast_vin as vin
from arkona.xfm_inpmast a
where a.type_n_u = 'U'
  and a.inpmast_vin <> '3GYFNGE37DS598684'
  and a.status = 'I'
  and left(a.inpmast_stock_number, 1) <> 'H'
  and a.row_from_date <= '10/01/2019'  and a.row_thru_date >= '10/01/2019' 
group by a.inpmast_stock_number, a.inpmast_vin;

-- select * from greg.uc_daily_snapshot_beta_1 limit 10
drop table if exists greg_10_01 cascade;
create temp table greg_10_01 as
select a.stock_number, a.vin
from greg.uc_daily_snapshot_beta_1 a
where a.the_date = '10/01/2019' 
  and a.store_code = 'RY1';

drop table if exists tool_10_01 cascade;
create temp table tool_10_01 as
select a.stocknumber as stock_number, b.vin
from ads.ext_Vehicle_inventory_items a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
where a.fromts::date <= '10/01/2019' 
  and a.thruts::date >= '10/01/2019'
  and left(a.stocknumber, 1) <> 'H';

select * 
from inpmast_10_01 a 
full outer join greg_10_01 b on a.vin = b.vin
full outer join tool_10_01 c on coalesce(a.vin, b.vin) = c.vin
where a.vin is null or b.vin is null or c.vin is null

drop table if exists all_accounting cascade;
create temp table all_accounting as
select * -- 363 
from ( -- all the vehicles
  select * from inpmast_10_01
  union
  select * from greg_10_01
  union
  select * from tool_10_01) a
left join (
  select control
  from fin.fact_gl
  where post_status = 'Y'
  group by control) b on a.stock_number = b.control  

-- 19 controls missing from accounting 
select *
from all_accounting 
where control is null

-- multiple vins
select a.*
from all_accounting a
join (
  select vin
  from all_accounting
  group by vin
  having count(*) > 1) b on a.vin = b.vin
order by a.vin  

-- so my initial thought is, if it exists in accounting, it is a valid vehicle

-- 354 of 363 have inpmast
-- bear in mind, this is for but a single day, but looks promising as a base
select a.stock_number, a.vin,  min(b.row_from_date) as inpmast_in, max(b.row_thru_date) as inpmast_out, 
  min(c.date_acquired) as acq, max(c.sale_date) as sold, max(d.fromts::date) as vii_from, max(d.thruts::date) as vii_thru,
  min(f.date_capped) as bopmast_sold
-- select a.stock_number, a.vin
from all_accounting a
left join arkona.xfm_inpmast b on a.stock_number = b.inpmast_stock_number
  and b.status = 'I'
left join greg.uc_daily_snapshot_beta_1 c on a.stock_number = c.stock_number  
left join ads.ext_Vehicle_inventory_items d on a.stock_number = d.stocknumber
left join ads.ext_vehicle_items e on d.vehicleitemid = e.vehicleitemid
left join arkona.ext_bopmast f on a.stock_number = f.bopmast_stock_number
  and a.vin = f.bopmast_vin
  and f.record_status = 'U'
where a.control is not null
group by a.stock_number, a.vin
order by a.stock_number


-- bear in mind, this is for but a single day, but looks promising as a base
-- least() & greatest() ignore nulls
-- 28019XXZ sold: 9/19/19(2) 10/14/19 (1) 9/20//19(1) greatest = 10/14 obviously not the best 
-- i ama pretty pleased with this, how to expand to 2 years of inventory?
select * 
from (
  select aa.*,least(inpmast_in,acq,vii_from) as in_date, greatest(inpmast_out,sold,vii_thru,bopmast_sold,boarded),least(inpmast_out,sold,vii_thru,bopmast_sold,boarded),
    case
      when greatest(inpmast_out,sold,vii_thru,bopmast_sold,boarded) - least(inpmast_out,sold,vii_thru,bopmast_sold,boarded) < 5 then greatest(inpmast_out,sold,vii_thru,bopmast_sold,boarded)
      else least(inpmast_out,sold,vii_thru,bopmast_sold)
    end as out_date
  from (
    select a.stock_number, a.vin,  min(b.row_from_date) as inpmast_in, max(b.row_thru_date) as inpmast_out, 
      min(c.date_acquired) as acq, max(c.sale_date) as sold, max(d.fromts::date) as vii_from, max(d.thruts::date) as vii_thru,
      min(f.date_capped) as bopmast_sold,
      min(g.boarded_date) as boarded
    from all_accounting a
    left join arkona.xfm_inpmast b on a.stock_number = b.inpmast_stock_number
      and b.status = 'I'
    left join greg.uc_daily_snapshot_beta_1 c on a.stock_number = c.stock_number  
    left join ads.ext_Vehicle_inventory_items d on a.stock_number = d.stocknumber
    left join ads.ext_vehicle_items e on d.vehicleitemid = e.vehicleitemid
    left join arkona.ext_bopmast f on a.stock_number = f.bopmast_stock_number
      and a.vin = f.bopmast_vin
      and f.record_status = 'U'
    left join (
      select stock_number, boarded_date
      from board.sales_board a
      join (
        select * 
        from board.board_types
        where board_type = 'deal') b on a.board_type_key = b.board_type_key) g on a.stock_number = g.stock_number
    where a.control is not null
    group by a.stock_number, a.vin) aa
  where not (sold = '12/31/9999' and bopmast_sold is null and vii_thru is null)) bb
where in_date > out_date  
order by stock_number


-- lets go for 2019 first, shit, we are going to need the inventory on every fucking day
-- well, lets give it a shot
-- first generate the list of "valid" vehicles and the in and out dates, then flesh out the entire year and add statuses
-- first inpmast, lets do phys tables
-- this is going so well, do both years
drop table if exists tem.inpmast_2019_2020 cascade;
create table tem.inpmast_2019_2020 as
select a.inpmast_stock_number as stock_number, a.inpmast_vin as vin
from arkona.xfm_inpmast a
where a.type_n_u = 'U'
  and a.inpmast_vin <> '3GYFNGE37DS598684'
  and a.status = 'I'
  and left(a.inpmast_stock_number, 1) <> 'H'
  and a.row_from_date <= '12/31/2020'  
  and a.row_thru_date >= '01/01/2019' 
group by a.inpmast_stock_number, a.inpmast_vin;
create unique index on tem.inpmast_2019_2020(stock_number,vin);
create index on tem.inpmast_2019_2020(vin);

drop table if exists tem.greg_2019_2020 cascade;
create table tem.greg_2019_2020 as
select a.stock_number, a.vin
from greg.uc_daily_snapshot_beta_1 a
where a.the_date between '01/01/2019' and '12/31/2020' 
  and a.store_code = 'RY1'
group by stock_number, vin  ;
create unique index on tem.greg_2019_2020(stock_number,vin);
create index on tem.greg_2019_2020(vin);  

drop table if exists tem.tool_2019_2020 cascade;
create table tem.tool_2019_2020 as
select a.stocknumber as stock_number, b.vin
from ads.ext_Vehicle_inventory_items a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
where a.fromts::date <= '12/31/2020' 
  and a.thruts::date >= '01/01/2019'
  and left(a.stocknumber, 1) <> 'H';
create unique index on tem.tool_2019_2020(stock_number,vin);
create index on tem.tool_2019_2020(vin);  

-- limits vehicles to those for which the stock_number exists in accounting
drop table if exists tem.acct_2019_2020 cascade;
create table tem.acct_2019_2020 as
select a.* -- 
from ( -- all the vehicles
  select * from tem.inpmast_2019_2020
  union
  select * from tem.greg_2019_2020
  union
  select * from tem.tool_2019_2020) a
join (
  select control
  from fin.fact_gl
  where post_status = 'Y'
  group by control) b on a.stock_number = b.control;
create unique index on tem.acct_2019_2020(stock_number,vin);
create index on tem.acct_2019_2020(vin);
delete from tem.acct_2019_2020 where right(stock_number, 1) in ('1','2','3','4','5','6','7','8','9','0');
delete from tem.acct_2019_2020 where length(vin) <> 17; -- 4
delete from tem.acct_2019_2020 where left(stock_number,1) = 'H'; -- 2

drop table if exists tem.vehicles_2019_2020 cascade;
create table tem.vehicles_2019_2020 as
select stock_number, vin, in_date, out_date 
from (
  select aa.*,least(inpmast_in,acq,vii_from) as in_date, greatest(inpmast_out,sold,vii_thru,bopmast_sold,boarded),least(inpmast_out,sold,vii_thru,bopmast_sold,boarded),
    case
      when greatest(inpmast_out,sold,vii_thru,bopmast_sold,boarded) - least(inpmast_out,sold,vii_thru,bopmast_sold,boarded) < 5 then greatest(inpmast_out,sold,vii_thru,bopmast_sold,boarded)
      else least(inpmast_out,sold,vii_thru,bopmast_sold)
    end as out_date
  from (
    select a.stock_number, a.vin,  min(b.row_from_date) as inpmast_in, max(b.row_thru_date) as inpmast_out, 
      min(c.date_acquired) as acq, max(c.sale_date) as sold, max(d.fromts::date) as vii_from, max(d.thruts::date) as vii_thru,
      min(f.date_capped) as bopmast_sold,
      min(g.boarded_date) as boarded
    from tem.acct_2019_2020 a
    left join arkona.xfm_inpmast b on a.stock_number = b.inpmast_stock_number
      and b.status = 'I'
    left join greg.uc_daily_snapshot_beta_1 c on a.stock_number = c.stock_number  
    left join ads.ext_Vehicle_inventory_items d on a.stock_number = d.stocknumber
    left join ads.ext_vehicle_items e on d.vehicleitemid = e.vehicleitemid
    left join arkona.ext_bopmast f on a.stock_number = f.bopmast_stock_number
      and a.vin = f.bopmast_vin
      and f.record_status = 'U'
    left join (
      select stock_number, boarded_date
      from board.sales_board a
      join (
        select * 
        from board.board_types
        where board_type = 'deal') b on a.board_type_key = b.board_type_key) g on a.stock_number = g.stock_number
    group by a.stock_number, a.vin) aa
  where not (sold = '12/31/9999' and bopmast_sold is null and vii_thru is null)) bb
where in_date <= out_date; -- excludes 20 rows
create unique index on tem.vehicles_2019_2020(stock_number,vin);
create index on tem.vehicles_2019_2020(vin);
create index on tem.vehicles_2019_2020(in_date);
create index on tem.vehicles_2019_2020(out_date);


-- detour dup stock numbers
-- fuck, 56 dup stocknumbers
-- which means different vins 
select stock_number
from tem.vehicles_2019_2020
group by stock_number
having count(*) > 1


select a.*, c.vin as tool_vin, c.vinresolved, d.stocknumber as tool_stock, e.vin as chr2_vin, f.vin as chr_vin
from tem.vehicles_2019_2020 a
join (
  select stock_number
  from tem.vehicles_2019_2020
  group by stock_number
  having count(*) > 1) b on a.stock_number = b.stock_number
left join ads.ext_vehicle_items c on a.vin = c.vin   
left join ads.ext_vehicle_inventory_items d on c.vehicleitemid = d.vehicleitemid
left join chr2.describe_vehicle_by_vin_with_tech e on a.vin = e.vin
left join chr.describe_vehicle f on a.vin = f.vin
order by a.stock_number




-- marginal cleanup of duplicate stocknumbers
delete from tem.vehicles_2019_2020 where vin in ('2C3CCAST9DH558128','2G1WF55K449319048','2GCEK13M871504363',
  '2HGFC1F34HH643663','3GCUKREC1FG176725','3GNAXSEV5JS584400','KM8JUCACXCU397485','1G1YY32G1X5113078');
delete from tem.vehicles_2019_2020 where vin = '2GCEK13M881175908' and stock_number = 'G35790B';
delete from tem.vehicles_2019_2020 where vin = '5FNRL5H47FB043246' and stock_number = 'G36078XA'; 
delete from tem.vehicles_2019_2020 where vin = '2GTEK133281236614' and stock_number = 'G36162GA'; 
delete from tem.vehicles_2019_2020 where vin = '1G2ZG57N294176981' and stock_number = 'G36287B';  
delete from tem.vehicles_2019_2020 where vin = '1G1BF5SM7H7124346' and stock_number = 'G36316A'; 
delete from tem.vehicles_2019_2020 where vin = '1FM5K8D89DGA01932' and stock_number = 'G36352XA'; 
delete from tem.vehicles_2019_2020 where vin = '5XYPHDA51JG396072' and stock_number = 'G36862RA'; 
delete from tem.vehicles_2019_2020 where vin = '1G4HD57248U170969' and stock_number = 'G36961A'; 
delete from tem.vehicles_2019_2020 where vin = '3GNAXXEV6KS559231' and stock_number = 'G37381A'; 
delete from tem.vehicles_2019_2020 where vin = '1G4GB5G32GF189467' and stock_number = 'G37491A'; 
delete from tem.vehicles_2019_2020 where vin = '1FATP8EM4G5305121' and stock_number = 'G37620C'; 
delete from tem.vehicles_2019_2020 where vin = '3GYFK62877G268294' and stock_number = 'G37624B'; 
delete from tem.vehicles_2019_2020 where vin = '2D4GP44L87R198593' and stock_number = 'G37642B'; 
delete from tem.vehicles_2019_2020 where vin = '3GCUKSEC9EG522980' and stock_number = 'G37659A'; 
delete from tem.vehicles_2019_2020 where vin = '1D7HW48N35S252836' and stock_number = 'G37930B'; 
delete from tem.vehicles_2019_2020 where vin = '1FTNE2EW8CDA66776' and stock_number = 'G38024AA'; 
delete from tem.vehicles_2019_2020 where vin = '1GYS4JKJ3HR248138' and stock_number = 'G38050RA'; 
delete from tem.vehicles_2019_2020 where vin = '5GAEV23768J233853' and stock_number = 'G38243XB'; 
delete from tem.vehicles_2019_2020 where vin = '2GKFLUEK2G6333311' and stock_number = 'G38269XA'; 
delete from tem.vehicles_2019_2020 where vin = '1G4HC5EM8BU141991' and stock_number = 'G38302XA'; 
delete from tem.vehicles_2019_2020 where vin = '1G3AJ55M5S6303642' and stock_number = 'G38385C'; 
delete from tem.vehicles_2019_2020 where vin = '1GKS2MEF8DR287150' and stock_number = 'G38416XA'; 
delete from tem.vehicles_2019_2020 where vin = '2G1145S39H9122279' and stock_number = 'G38466MA'; 
delete from tem.vehicles_2019_2020 where vin = '1GCVKREC8FZ327452' and stock_number = 'G38534A'; 
delete from tem.vehicles_2019_2020 where vin = '1GTU9DEL4KZ136647' and stock_number = 'G38631A'; 
delete from tem.vehicles_2019_2020 where vin = '3GTU2VEJ9FG286445' and stock_number = 'G38683A'; 
delete from tem.vehicles_2019_2020 where vin = '3GCPKTE30BG230097' and stock_number = 'G38844B'; 
delete from tem.vehicles_2019_2020 where vin = '3GCUKSEJ1HG390428' and stock_number = 'G38846A'; 
delete from tem.vehicles_2019_2020 where vin = '4S4BSAHC4K3279182' and stock_number = 'G38922RA'; 
delete from tem.vehicles_2019_2020 where vin = '1GCEK19T04Z128985' and stock_number = 'G38948B'; 
delete from tem.vehicles_2019_2020 where vin = '1C4RJFBG8JC492889' and stock_number = 'G39028A'; 
delete from tem.vehicles_2019_2020 where vin = '1GNEVGKW2JJ151054' and stock_number = 'G39165RA'; 
delete from tem.vehicles_2019_2020 where vin = '1GNSKJE7XCR256703' and stock_number = 'G39471RB'; 
delete from tem.vehicles_2019_2020 where vin = '2GCEK13T841370893' and stock_number = 'G39581C'; 
delete from tem.vehicles_2019_2020 where vin = '2GKFLUEK8H6279515' and stock_number = 'G39655A'; 
delete from tem.vehicles_2019_2020 where vin = '1GCEK19Z67Z162051' and stock_number = 'G39709A'; 
delete from tem.vehicles_2019_2020 where vin = '1GCVKREC3HZ354089' and stock_number = 'G39827A'; 
delete from tem.vehicles_2019_2020 where vin = '1GKS2HKJXGR100091' and stock_number = 'G39875A'; 
delete from tem.vehicles_2019_2020 where vin = '5TFDW5F1XHX619222' and stock_number = 'G39972A'; 
delete from tem.vehicles_2019_2020 where vin = '1GNSKJKC6FR209254' and stock_number = 'G40007A'; 
delete from tem.vehicles_2019_2020 where vin = '2FMDK4AK1BBA47635' and stock_number = 'G40072B'; 
delete from tem.vehicles_2019_2020 where vin = '3GTP9EEL3KG203600' and stock_number = 'G40363A'; 
delete from tem.vehicles_2019_2020 where vin = '1GKFK13017R236410' and stock_number = 'G40365B'; 
delete from tem.vehicles_2019_2020 where vin = 'JN8AF5MV8ET366592' and stock_number = 'G40431A'; 
delete from tem.vehicles_2019_2020 where vin = '4S3BNBJ66F3006425' and stock_number = 'G40588B'; 
delete from tem.vehicles_2019_2020 where vin = '1FT7X2B63EEB68345' and stock_number = 'G40655A'; 
delete from tem.vehicles_2019_2020 where vin = '1B7HC16Y5XS306705' and stock_number = 'G40964XAA'; 
delete from tem.vehicles_2019_2020 where vin = '1GYUKBEF3AR136571' and stock_number = 'G40974B'; 
delete from tem.vehicles_2019_2020 where vin = '3GCUKREC9GG162072' and stock_number = 'G41072A'; 



-----------------------------------------------------------------------------------
--< detour are all these vehicles make model shape size normalized?
-----------------------------------------------------------------------------------

--1st are all the vins in tem.step_3 or inv_2
-- 42 are not
select a.*, b.vin, c.vin
from tem.vehicles_2019_2020 a
left join tem.step_3 b on a.vin = b.vin
left join (select distinct vin from tem.inv_2) c on a.vin = c.vin
where b.vin is null
and c.vin is null

-- only 1 not in inpmast
select a.*, b.inpmast_stock_number, b.make, b.model
from (
    select a.*
    from tem.vehicles_2019_2020 a
    left join tem.step_3 b on a.vin = b.vin
    left join (select distinct vin from tem.inv_2) c on a.vin = c.vin
    where b.vin is null
    and c.vin is null)  a
left join arkona.ext_inpmast b on a.vin = b.inpmast_vin
where b.make is null

-- -- this one looks like a typo
-- -- well, they both decode to the same vehicle_type
-- -- so leave the tool version in (to get statuses)
-- G39581C in DT 2GCEK13T841370893
-- select * from tem.vehicles_2019_2020 where vin = '2GCEK13T841370893'  dealertrack
-- select * from tem.vehicles_2019_2020 where vin = '2GTEK13T841370893'  tool
-- neither one decode in black book
-- 
-- select r.* from chr2.describe_vehicle_by_vin_with_tech a
-- join jsonb_array_elements(a.response->'style') r on true
-- where vin = '2GCEK13T841370893'
-- 
-- select r.* from chr2.describe_vehicle_by_vin_with_tech a
-- join jsonb_array_elements(a.response->'style') r on true
-- where vin = '2GTEK13T841370893'

select count(*) from tem.vehicles_2019_2020  -- 6161
select count(distinct vin) from tem.vehicles_2019_2020  -- 5990
select * from tem.inv_2 limit 10
-- so onward on the path to normalization
select a.*, b.vin, coalesce(b.make_new, b.make) as make, coalesce(b.model_new, b.model) as model,
  b.shape_new, b.size_new, c.vin, c.make, c.model, c.shape, c.size
from (
  select distinct vin from tem.vehicles_2019_2020) a
left join tem.step_3 b on a.vin = b.vin
left join (select distinct vin, make, model, shape, size from tem.inv_2) c on a.vin = c.vin
where b.vin is null and c.vin is null

ok, quit fooling around, get the 42 vins taken care of
select aa.vin
from (
  select distinct a.vin
  from tem.vehicles_2019_2020 a
  left join tem.step_3 b on a.vin = b.vin
  left join (select distinct vin from tem.inv_2) c on a.vin = c.vin
  where b.vin is null
  and c.vin is null) aa
left join chr2.describe_vehicle_by_vin_with_tech bb on aa.vin = bb.vin
where bb.vin is null

ok, got the chrome, now need to update the model
go to glump_proportions\glump_proportion_of_sales_5.sql to do this

ok, that is done, the vehicles are classified in vision

time to start adding attributes?
yes, there are 2 types of attributes needed
  those describing the vehicle (make, model, consistent across life of vehicle in inventory)
  those that change over time (age, status, price)


lets go ahead and do make, model, shape and size in this detour

select * from tem.step_3 where vin = '2CNBJ13C3Y6917269'
select vin, coalesce(make_new, make), coalesce(model_new, model),
  shape_new, size_new
from tem.step_3
group by vin, coalesce(make_new, make), coalesce(model_new, model),
  shape_new, size_new

select vin, make, model, shape, size 
from tem.inv_2 
group by vin, make, model, shape, size 

drop table if exists tem.inventory_2019_2020 cascade;
create table tem.inventory_2019_2020 (
  stock_number citext not null,
  vin citext not null check (length(vin) = 17),
  style_id integer,
  date_acquired date not null,
  date_sold date not null,
  make citext,
  model citext,
  shape citext,
  size citext,
  primary key(stock_number,vin));
create index on tem.inventory_2019_2020(date_acquired); 
create index on tem.inventory_2019_2020(date_sold); 
-- motorcycles
delete from tem.inventory_2019_2020 where vin in ('1G3NL52M9WM319554','1HD1ECL17FY118289','1HD1JBB171Y013848',
  '1HD1GX1336K331300','1HD4NBB1XGC504180','JS1GT76A252103098','1HD1MAL15FB855467');
-- bad vins
delete from tem.inventory_2019_2020 where vin in ('1D4HB48238F141487','1FAFP5340XG108367','2S3DB717X96100015');

insert into tem.inventory_2019_2020
select a.stock_number, a.vin, null as style_id, in_date, out_date, coalesce(b.make, c.make) as make,  coalesce(b.model, c.model) as model,
    coalesce(b.shape, c.shape) as shape, coalesce(b.size , c.size) as size
from tem.vehicles_2019_2020 a
left join (
  select vin, make, model, shape, size 
  from tem.inv_2 
  group by vin, make, model, shape, size ) b on a.vin = b.vin 
left join (
  select vin, coalesce(make_new, make) as make, coalesce(model_new, model) as model,
    shape_new as shape, size_new as size
  from tem.step_3
  group by vin, coalesce(make_new, make), coalesce(model_new, model),
    shape_new, size_new) c on a.vin = c.vin;

-- update the rest (42) from chrome
update tem.inventory_2019_2020 x
set make = y.make,
    model = y.model,
    shape = y.shape,
    size = y.size
from (    
  select aa.vin, aa.make, aa.model, bb.shape, bb.size
  from (
    select distinct a.vin, (style->'division'->>'$value')::citext as make, 
      (style->'model'->>'$value')::citext as model
    from tem.inventory_2019_2020 a
    join chr2.describe_vehicle_by_vin_with_tech b on a.vin = b.vin
    join jsonb_array_elements(b.response->'style') as style on true
    where a.make is null) aa
  left join veh.shape_size_classifications bb on aa.make = bb.make
    and aa.model = bb.model) y
where x.vin = y.vin;    

WHAT ARE ALL THE OTHER VEHICLE STATIC ATTRIBUTES 
  make
  model
  shape
  size
  model_year
  body 
  trim
  drive
  cab
  bed
  miles
  sale type
work out the join to vehicle_types?

select * 
from tem.inventory_2019_2020 a
join greg.uc_daily_snapshot_beta_1 b on a.stock_number = b.stock_number
  and a.vin = b.vin
  and b.the_date = b.sale_Date
limit 20

select * from veh.vehicle_types limit 20

select count(*) from veh.vehicle_types  -- 62006


select model_year, make, model, count(*) from veh.vehicle_types group by model_year, make, model order by count(*)




select * from veh.vehicle_types where make = 'gmc' and model = 'terrain'

liking the idea of approaching this from veh.vehicle_types, that is a fixed model entity, so use it
for now, just grab the chrome_style_id

does inpmast have chrome code for used vehicles

-- this gives me about half of them
update tem.inventory_2019_2020 x
set style_id = y.key_to_cap_explosion_data
from (
  select a.stock_number, a.vin, b.key_to_cap_explosion_data::integer
  from tem.inventory_2019_2020 a
  join arkona.ext_inpmast b on a.stock_number = b.inpmast_stock_number
    and a.vin = b.inpmast_vin
  where b.key_to_cap_explosion_data is not null) y
where x.stock_number = y.stock_number
  and x.vin = y.vin; 

fuck it, lets go for chrome, 2193
select distinct vin 
from tem.inventory_2019_2020 a
where a.style_id is null
  and not exists (
    select 1
    from chr2.describe_vehicle_by_vin_with_tech
    where vin = a.vin)
  and not exists (
    select 1
    from chr.describe_vehicle
    where vin = a.vin)
  and not exists (
    select 1
    from chr2.describe_vehicle_by_vin
    where vin = a.vin)
  and a.vin not in ('1HD1GX1336K331300','1HD1MAL15FB855467','1HD4NBB1XGC504180'); 

***********************************************************************************************************************************
***********************************************************************************************************************************
***********************************************************************************************************************************

02/20/21 this is where i fucked up, i added all those vehicles to chr2.describe_vehicle_by_vin_with_tech but did not then do
         glump_proportion_of_sales_5.sql

***********************************************************************************************************************************
***********************************************************************************************************************************
***********************************************************************************************************************************

-- go ahead and update a few style_ids from chr.describe_vehicle
update tem.inventory_2019_2020 x
set style_id = y.style_id
from (
  select a.vin, (style->'attributes'->>'id')::integer as style_id
  from tem.inventory_2019_2020 a
  join chr.describe_vehicle b on a.vin = b.vin
  join jsonb_array_elements(b.response->'style') style on true
  where a.style_id is null 
    and b.style_count = 1) y
where x.vin = y.vin    
    
-- and the rest
update tem.inventory_2019_2020 x
set style_id = y.style_id
from ( -- 2166
  select a.vin, (style->'attributes'->>'id')::integer as style_id
  from tem.inventory_2019_2020 a
  join chr2.describe_vehicle_by_vin_with_tech b on a.vin = b.vin
  join jsonb_array_elements(b.response->'style') style on true
  where a.style_id is null 
    and b.style_count = 1) y
where x.vin = y.vin;    

-- 367 to go
select distinct a.vin
from tem.inventory_2019_2020 a
left join chr2.describe_vehicle_by_vin_with_tech b on a.vin = b.vin
where style_id is null 
-- 
-- select * from ads.ext_vehicle_items limit 100
-- gm may be able to do model_code
select distinct a.stock_number, a.vin, a.make, a.model, b.style_count, c.year, c.model_code, c.body_style, c.trim, d.bodystyle, d.trim, d.yearmodel,vinresolved,d.engine,
  (style ->'attributes'->>'id')::integer as style_id, style->'attributes'->>'trim', style->'attributes'->>'mfrModelCode'
from tem.inventory_2019_2020 a
left join chr2.describe_vehicle_by_vin_with_tech b on a.vin = b.vin
left join jsonb_array_elements(b.response->'style') as style on true
left join arkona.ext_inpmast c on a.vin = c.inpmast_vin
left join ads.ext_vehicle_items d on a.vin = d.vin 
where style_id is null 


-- ok, looks like i can step thru various matching attributes to get some styleids
-- model_code
update tem.inventory_2019_2020 x
set style_id = y.style_id
from ( -- 50
  select a.vin, max((style ->'attributes'->>'id')::integer) as style_id
  from tem.inventory_2019_2020 a
  left join chr2.describe_vehicle_by_vin_with_tech b on a.vin = b.vin
  left join jsonb_array_elements(b.response->'style') as style on true
  left join arkona.ext_inpmast c on a.vin = c.inpmast_vin
  left join ads.ext_vehicle_items d on a.vin = d.vin 
  where style_id is null 
    and c.model_code = (style->'attributes'->>'mfrModelCode')::citext
  group by a.vin
  having count(*) = 1) y
where x.vin = y.vin;  


-- trim 
update tem.inventory_2019_2020 x
set style_id = y.style_id
from ( -- 89
  select a.vin, max((style ->'attributes'->>'id')::integer) as style_id
  from tem.inventory_2019_2020 a
  left join chr2.describe_vehicle_by_vin_with_tech b on a.vin = b.vin
  left join jsonb_array_elements(b.response->'style') as style on true
  left join arkona.ext_inpmast c on a.vin = c.inpmast_vin
  left join ads.ext_vehicle_items d on a.vin = d.vin 
  where style_id is null 
    and d.trim = (style->'attributes'->>'trim')::citext
  group by a.vin
  having count(*) = 1) y
where x.vin = y.vin; 


-- maybe engine NO REMEMBER, STYLEID DOES NOT DETERMINE ENGINE
-- Ford trucks, the distinguishing attributes are trim and bed, unfortunately we have almost no bed data,
-- so, unfortunately legacy ford trucks are going to be arbitrary
-- 3 sizes: 5.5', 6.5', 8'
-- after mindfucking for way too long, realized it's pointless
-- so, fuck  it, minimum style id wins
-- will talk about it with the 8 crew, maybe they will have a better idea
-- ford pickups 
update tem.inventory_2019_2020 x
set style_id = y.style_id
from ( -- 88
select a.vin, min((style ->'attributes'->>'id')::integer) as style_id
from (
  select vin, make, model 
  from tem.inventory_2019_2020 
  where style_id is null
  group by vin, make, model) a
left join chr2.describe_vehicle_by_vin_with_tech b on a.vin = b.vin
left join jsonb_array_elements(b.response->'style') as style on true
left join arkona.ext_inpmast c on a.vin = c.inpmast_vin
left join ads.ext_vehicle_items d on a.vin = d.vin 
where a.make = 'ford'
  and d.trim = (style->'attributes'->>'trim')::citext
  and style->'attributes'->>'altBodyType' like '%Pickup%'
group by a.vin) y
where x.vin = y.vin; 

-- 140 to go
select distinct a.vin
from tem.inventory_2019_2020 a
left join chr2.describe_vehicle_by_vin_with_tech b on a.vin = b.vin
where style_id is null 

-- arkona bodystyle = chrome style name
update tem.inventory_2019_2020 x
set style_id = y.style_id
from ( -- 11
select a.vin, min((style ->'attributes'->>'id')::integer) as style_id
from (
  select vin, make, model 
  from tem.inventory_2019_2020 
  where style_id is null
  group by vin, make, model) a
left join chr2.describe_vehicle_by_vin_with_tech b on a.vin = b.vin
left join jsonb_array_elements(b.response->'style') as style on true
left join arkona.ext_inpmast c on a.vin = c.inpmast_vin
left join ads.ext_vehicle_items d on a.vin = d.vin 
where c.body_style = (style->'attributes'->>'name')::citext
group by a.vin) y
where x.vin = y.vin; 

!!!!!!!!! many of these that i am doing manually (toyota, suzuki, hyundai ...)decode based on transmission

select distinct (style ->'attributes'->>'id')::integer as style_id, a.vin, a.make, a.model, b.style_count, c.year, c.model_code, c.body_style, d.bodystyle, d.trim, d.yearmodel,vinresolved,d.transmission,
  style->'attributes'->>'trim' as trim, style->'attributes'->>'mfrModelCode' as model_code, 
  style->'attributes'->>'drivetrain' as drive,
  style->'attributes'->>'name' as name, style->'attributes'->>'altStyleName' as alt_style, style->'attributes'->>'altBodyType' as alt_body
from (
  select vin, make, model 
  from tem.inventory_2019_2020 
  where style_id is null
  group by vin, make, model) a
left join chr2.describe_vehicle_by_vin_with_tech b on a.vin = b.vin
left join jsonb_array_elements(b.response->'style') as style on true
left join arkona.ext_inpmast c on a.vin = c.inpmast_vin
left join ads.ext_vehicle_items d on a.vin = d.vin 
order by vin

select * from arkona.ext_inpmast where inpmast_vin = '1B3LC46K38N175931'

update tem.inventory_2019_2020 set style_id = 102226 where vin = 'JTEHH20V030249661';
update tem.inventory_2019_2020 set style_id = 300481 where vin = 'KMHCN46C88U258760';
update tem.inventory_2019_2020 set style_id = 128721 where vin = 'KMHFU45E34A303263';
update tem.inventory_2019_2020 set style_id = 331853 where vin = 'WBAKC8C57CC435958';
update tem.inventory_2019_2020 set style_id = 386489 where vin = 'WDDZF4JB5HA081103';
update tem.inventory_2019_2020 set style_id = 4593 where vin = 'WVWRH63B11P143211';
update tem.inventory_2019_2020 set style_id = 327189 where vin = 'YV4952DL5B2192299';
update tem.inventory_2019_2020 set style_id =  where vin = '';
update tem.inventory_2019_2020 set style_id =  where vin = '';
update tem.inventory_2019_2020 set style_id =  where vin = '';
update tem.inventory_2019_2020 set style_id =  where vin = '';
update tem.inventory_2019_2020 set style_id =  where vin = '';
update tem.inventory_2019_2020 set style_id =  where vin = '';
update tem.inventory_2019_2020 set style_id =  where vin = '';
update tem.inventory_2019_2020 set style_id =  where vin = '';
update tem.inventory_2019_2020 set style_id =  where vin = '';



-- base
select distinct a.vin, a.make, a.model, b.style_count, 
  c.year, c.model_code, c.body_style, c.trim, d.bodystyle, 
  d.trim, d.yearmodel,vinresolved,d.engine,
  (style ->'attributes'->>'id')::integer as style_id, style->'attributes'->>'trim' as trim, style->'attributes'->>'mfrModelCode' as model_code, 
  style->'attributes'->>'drivetrain' as drive,
  style->'attributes'->>'name' as name, style->'attributes'->>'altStyleName' as alt_style, style->'attributes'->>'altBodyType' as alt_body
from (
  select vin, make, model 
  from tem.inventory_2019_2020 
  where style_id is null
  group by vin, make, model) a
left join chr2.describe_vehicle_by_vin_with_tech b on a.vin = b.vin
left join jsonb_array_elements(b.response->'style') as style on true
left join arkona.ext_inpmast c on a.vin = c.inpmast_vin
left join ads.ext_vehicle_items d on a.vin = d.vin 
order by vin


02/21/21
see drill_down_troubleshooting.sql
run chrome those vehicles in inventory that have a styleid not in vehicle_types

select distinct aa.vin
from (
  select a.*
  from tem.inventory_2019_2020  a
  left join veh.vehicle_types b on a.style_id = b.style_id
  where b.style_id is null) aa
left join chr2.describe_vehicle_by_vin_with_tech bb on aa.vin = bb.vin
where bb.vin is null

now the glump_proportion_of_sales_5.sql

select * 
from tem.inventory_2019_2020 a
left join veh.vehicle_types b on a.style_id = b.style_id
where b.style_id is null 

ok, i thought i just did everything, how did this happen
well, initially these are clearly fucked up
silverado and cruze with a style_id of 105371
lets zap the style_id and run the vins that need to be run

-- styleid zapped
update tem.inventory_2019_2020
set style_id = null
where vin in (
  select a.vin 
  from tem.inventory_2019_2020 a
  left join veh.vehicle_types b on a.style_id = b.style_id
  where b.style_id is null);
-- all vins in chrome
select aa.vin 
from (
  select a.vin 
  from tem.inventory_2019_2020 a
  left join veh.vehicle_types b on a.style_id = b.style_id
  where b.style_id is null) aa 
left join chr2.describe_vehicle_by_vin_with_tech bb on aa.vin = bb.vin
where bb.vin is null  

-- -- zap maserati
-- delete from tem.inventory_2019_2020 where vin = 'ZAM57RTA0E1126849';

-- where style_count = 1
update tem.inventory_2019_2020 x
set style_id = y.style_id
from (
  select a.vin, (style->'attributes'->>'id')::integer as style_id
  from tem.inventory_2019_2020 a
  join chr2.describe_vehicle_by_vin_with_tech b on a.vin = b.vin
  join jsonb_array_elements(b.response->'style') style on true
  where a.style_id is null 
    and b.style_count = 1) y
where x.vin = y.vin    

-- and the last one (using base query from above) is a cad deville
update tem.inventory_2019_2020
set style_id = 5632
where vin = '1G6KD54Y61U260751';

-----------------------------------------------------------------------------------
--/> detour are all these vehicles make model shape size normalized?
-----------------------------------------------------------------------------------
select * from tem.inventory_2019_2020 limit 10
select count(*) from tem.inventory_2019_2020  -- 6102 vehicles
select count(*) from tem.inventory_2019_2020_days  -- 485226

drop table if exists tem.inventory_2019_2020_days cascade;
create table tem.inventory_2019_2020_days as
select b.the_date, a.*, null::integer as age
from tem.inventory_2019_2020 a
join dds.dim_date b on b.the_date between a.date_acquired and a.date_sold;
create unique index on tem.inventory_2019_2020_days(stock_number,the_date);
create index on tem.inventory_2019_2020_days(the_date);
create index on tem.inventory_2019_2020_days(vin);

drop table if exists tem.inventory_2019_2020_days cascade;
CREATE TABLE tem.inventory_2019_2020_days(
  the_date date,
  stock_number citext,
  vin citext,
  style_id integer,
  date_acquired date,
  date_sold date,
  make citext,
  model citext,
  shape citext,
  size citext,
  age integer,
  status citext,
  days_in_status integer,
  best_price integer,
  date_priced date,
  days_since_priced integer,
  sale_type citext,
  disposition citext,
  year_month integer,
  primary key(stock_number,the_date));
create index on tem.inventory_2019_2020_days(the_date);
create index on tem.inventory_2019_2020_days(vin); 
create index on tem.inventory_2019_2020_days(shape); 
create index on tem.inventory_2019_2020_days(size); 
create index on tem.inventory_2019_2020_days(date_acquired); 
create index on tem.inventory_2019_2020_days(date_sold); 

alter table tem.inventory_2019_2020_days
add column year_month integer;
create index on tem.inventory_2019_2020_days(year_month);
update tem.inventory_2019_2020_days x
set year_month = y.year_month
from (
  select a.the_date, a.year_month 
  from dds.dim_date a
  join tem.inventory_2019_2020_days b on a.the_date = b.the_date) y
where x.the_date = y.the_date;

  
-- 485226
insert into tem.inventory_2019_2020_days(the_date,stock_number,vin,style_id,date_acquired,
  date_sold,make,model,shape,size)
select b.the_date, a.*
from tem.inventory_2019_2020 a
join dds.dim_date b on b.the_date between a.date_acquired and a.date_sold;

-- -- 47 vehicles acquired before 01/01/19
-- delete
-- from tem.inventory_2019_2020 a
-- where date_sold < '01/01/2019'
-- 
-- delete
-- from tem.inventory_2019_2020_days a
-- where date_sold < '01/01/2019'

-- 22 secs to update 483747 rows
-- update tem.inventory_2019_2020_days set age = null
update tem.inventory_2019_2020_days x
set age = y.age
from (
  select stock_number, the_date, row_number() over (partition by stock_number order by stock_number, the_date) as age
  from tem.inventory_2019_2020_days
  order by stock_number, the_date) y
where x.stock_number = y.stock_number
  and x.the_date = y.the_date;

-- date avail
-- from date_acquired - avail is raw
-- after populating, update days in status, and avail to fresh avail and aged avail
update tem.inventory_2019_2020_days x
set status = 'available'
from (
  select a.stock_number, date_acquired, max(e.fromts)::date as date_available, date_sold
  from tem.inventory_2019_2020 a
  left join ads.ext_vehicle_inventory_items c on a.stock_number = c.stocknumber
  left join ads.ext_vehicle_inventory_item_statuses e on c.vehicleinventoryitemid = e.vehicleinventoryitemid
    and e.status = 'RMFlagAV_Available'
  group by a.stock_number, date_acquired, date_sold) y
where x.stock_number = y.stock_number
  and x.the_date = y.date_available;  

-- need to populated all days between avail and last day with avail
-- 45 sec
update tem.inventory_2019_2020_days x
set status = y.first_value
from ( 
select the_date, stock_number, status, 
  first_value(status) over (partition by stock_number, status_partition order by the_date)
from (
  select the_date, stock_number, age, status, 
    sum(case when status is null then 0 else 1 end) over (partition by stock_number order by the_date) as status_partition
  from tem.inventory_2019_2020_days
  order by stock_number, the_date) a) y
where x.stock_number = y.stock_number
  and x.the_date = y.the_date; 

-- raw
update tem.inventory_2019_2020_days
set status = 'raw'
where status is null;

-- avail_fresh, avail_aged
update tem.inventory_2019_2020_days x
set status = y.avail_status
from (
select the_date, stock_number, age, status,
  case
    when status = 'available' then
      case
        when (the_date + 1) - first_value(the_date) over (partition by stock_number,status order by stock_number, the_date) < 31 then 'avail_fresh'
        else 'avail_aged'
      end
  end as avail_status
from tem.inventory_2019_2020_days
where status = 'available') y
where x.stock_number = y.stock_number
  and x.the_date = y.the_date;

-- days in status
update tem.inventory_2019_2020_days x
set days_in_status = y.days_in_status
from (
  select the_date, stock_number, status, 
    count(*) over (partition by stock_number, status order by stock_number, the_date) as days_in_status
  from tem.inventory_2019_2020_days) y
where x.stock_number = y.stock_number
  and x.the_date = y.the_date;  





-- pricings
-- dates with no price are null
drop table if exists tem.pricings cascade;
create table tem.pricings as
select distinct x.stock_number, coalesce(y.date_priced, x.date_sold) as date_priced, coalesce(y.best_price, z.retail_price) as best_price  
from tem.inventory_2019_2020 x 
left join ( -- the pricings
  select aa.stock_number, aa.vin, date_acquired, date_sold, aa.date_priced, aa.amount as best_price
  from (
    select a.stock_number, a.vin, date_acquired, a.date_sold, c.vehiclepricingts, c.vehiclepricingts::date date_priced, d.amount::integer
    from tem.inventory_2019_2020 a
    left join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
    left join ads.ext_vehicle_pricings c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
    left join ads.ext_vehicle_pricing_details d on c.vehiclepricingid = d.vehiclepricingid
      and d.typ = 'VehiclePricingDetail_BestPrice') aa
  join ( -- this ensures one price per day
    select a.stock_number, max(c.vehiclepricingts) as vehiclepricingts
    from tem.inventory_2019_2020 a
    left join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
    left join ads.ext_vehicle_pricings c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
    group by a.stock_number, c.vehiclepricingts::date) bb on aa.stock_number = bb.stock_number
      and aa.vehiclepricingts = bb.vehiclepricingts
  where aa.date_priced >= aa.date_acquired
    and aa.date_priced <= aa.date_sold) y on x.stock_number = y.stock_number
left join arkona.ext_bopmast z on x.stock_number = z.bopmast_stock_number;
create unique index on tem.pricings(stock_number,date_priced);
create index on tem.pricings(date_priced);

-- 49 sec
update tem.inventory_2019_2020_days x
set best_price = y.best_price,
    date_priced = y.date_priced,
    days_since_priced = y.days_since
from (    
  select the_date, stock_number, best_price, date_priced, the_date - date_priced as days_since
  from (
    select the_date, stock_number, 
      first_value(date_priced) over (partition by stock_number, date_partition order by the_date) as date_priced,
      first_value(best_price) over (partition by stock_number, price_partition order by the_date) as best_price
    from (
      select a.the_date, a.stock_number, b.date_priced, b.best_price,
        sum(case when b.date_priced is null then 0 else 1 end) over (partition by a.stock_number order by the_date) as date_partition,
        sum(case when b.best_price is null then 0 else 1 end) over (partition by a.stock_number order by the_date) as price_partition
      from tem.inventory_2019_2020_days a
      left join tem.pricings b on a.stock_number = b.stock_number
        and a.the_date = b.date_priced
      order by a.stock_number, a.the_date) aa) aaa) y
where x.stock_number = y.stock_number
  and x.the_date = y.the_date;      


  
-- sale_type
-- leave unsold as null
update tem.inventory_2019_2020_days x
set sale_type = y.sale_type
from (
  select a.stock_number, 
    coalesce(split_part(d.typ, '_', 2), case when e.sale_type = 'R' then 'Retail' else 'Wholesale' end) as sale_type
  from tem.inventory_2019_2020 a
  left join ads.ext_vehicle_inventory_items c on a.stock_number = c.stocknumber
  left join ads.ext_vehicle_sales d on c.vehicleinventoryitemid = d.vehicleinventoryitemid
    and d.status <> 'VehicleSale_SaleCanceled'
  left join arkona.ext_bopmast e on a.stock_number = e.bopmast_stock_number
    and e.record_status = 'U') y
where x.stock_number = y.stock_number;


select * from tem.inventory_2019_2020_days order by stock_number, the_date limit 500


select * from tem.inventory_2019_2020_days order by stock_number, the_date offset 1000 limit 2090


select * from tem.inventory_2019_2020_days where the_date = '02/22/2019'



select shape, size, status, count(*)
from tem.inventory_2019_2020_days
where the_date = '02/22/2019'
group by shape, size, status


-- look at car large 10/10 (18) vs 11/19 (8)

-- monthly totals by shape size
select shape, size,
  count(*) filter (where date_sold between '10/01/2019' and '10/31/2019') as oct,
  count(*) filter (where date_sold between '11/01/2019' and '11/30/2019') as nov
from tem.inventory_2019_2020_days
where shape = 'car'
  and the_date = date_sold
  and sale_type = 'retail'
group by shape, size

-- this is close to what was described as the first graph for the drill down
-- daily inventory by age of inventory
select a.*, sum(sales) over (partition by year_month) as month_sales
from (
  select year_month, the_date,
    count(*) filter (where age < 30) as green,
    count(*) filter (where age between 31 and 60) as yellow,
    count(*) filter (where age between 61 and 90) as red,
    count(*) filter (where age > 90) as black,
    count(*) filter (where date_sold = the_date and sale_type = 'retail') as sales
  from tem.inventory_2019_2020_days
  where year_month in ( 201902)
    and shape = 'pickup'
    and size = 'large'
  group by year_month, the_date) a
order by the_date 

-- greg wants to see daily inventory by age of inventory to include sales over the next 14, 21, 28 days

with 
  wtf as (
    select a.*, sum(sales) over (partition by year_month) as month_sales,
      sum(sales) over (order by the_date rows between current row and 14 following) as sales_14,
      sum(sales) over (order by the_date rows between current row and 21 following) as sales_21,
      sum(sales) over (order by the_date rows between current row and 28 following) as sales_28
    from (
      select year_month, the_date,
        count(*) as total_inv,
        count(*) filter (where age < 30) as green,
        count(*) filter (where age between 31 and 60) as yellow,
        count(*) filter (where age between 61 and 90) as red,
        count(*) filter (where age > 90) as black,
        count(*) filter (where date_sold = the_date and sale_type = 'retail') as sales
      from tem.inventory_2019_2020_days
      where year_month in ( 201910, 201911,201912)
        and shape = 'car'
        and size = 'large'
      group by year_month, the_date) a)
select * 
from wtf 
where year_month in(201910, 201911)
order by the_date

-- 03/02/21 afton noticed that these queries do not give a row for every day, 
-- eg, compact car 202005, only have inventory on 15 of the 30 days
-- so, this is what i sent to her
with 
  wtf as (
    select a.*, sum(sales) over (partition by year_month) as month_sales,
      sum(sales) over (order by the_date rows between current row and 14 following) as sales_14,
      sum(sales) over (order by the_date rows between current row and 21 following) as sales_21,
      sum(sales) over (order by the_date rows between current row and 28 following) as sales_28
    from (
      select aa.year_month, aa.the_date, 
        coalesce(bb.total_inv, 0) as total_inv,
        coalesce(bb.green, 0) as green,
        coalesce(bb.yellow, 0) as yellow,
        coalesce(bb.red, 0) as red,
        coalesce(bb.black, 0) as black,
        coalesce(bb.sales, 0) as sales
      from dds.dim_date aa
      left join (
        select year_month, the_date,
          count(*) as total_inv,
          count(*) filter (where age <= 30) as green,
          count(*) filter (where age between 31 and 60) as yellow,
          count(*) filter (where age between 61 and 90) as red,
          count(*) filter (where age > 90) as black,
          count(*) filter (where date_sold = the_date and sale_type = 'retail') as sales
        from tem.inventory_2019_2020_days
        where year_month in ( 202005,202006)
          and shape = 'car'
          and size = 'compact'
        group by year_month, the_date) bb on aa.the_date = bb.the_date
      where aa.year_month in ( 202005,202006)  ) a)
select *
from wtf 
where year_month in(202005,202006)
order by the_date

/*
04/08/2021
Comparing Large Pickups
Feb 2019 to Oct 2019
Feb 2019 - Sold 69 units
Oct 2019 - Sold 43 units
This is age of vehicle from acquisition date.
Inital observation
Volume of aged vehicles in inventory in Feb 2019 is higher than Oct 2019
What is the condition of the inventory?
For each month
What is average daily units owned for less than 30 days.
"" 30/60/90/+
For a given date for each category of inventory what was the end result of the vehicle. Sold out of raw @ 30 days, Wholesale, Sold retail ....
Sold Raw 30/60/90/+
Sold Lot 30/60/90/+
Wholesale Raw 30/60/90/+
Wholesale Lot 30/60/90/+
Questions:
How many vehicles did we sell out of raw in each month?
*/
select * from tem.inventory_2019_2020_days limit 100

select * from tem.inventory_2019_2020_days where shape = 'pickup' and size = 'large' and the_date = '02/19/2019'

select a.*
-- select *
from tem.inventory_2019_2020_days a
where a.shape = 'pickup'
  and a.size = 'large'
  and a.the_date = '02/01/2019'
order by a.date_sold 

drop table if exists tem.inventory_dispositions cascade;
create table tem.inventory_dispositions as
select stock_number, vin, shape, size, date_acquired, date_sold, status, days_in_status, sale_type,
  case 
		when status = 'raw' and sale_type = 'retail' and days_in_status between 0 and 30 then 'retail_raw_30'
		when status = 'raw' and sale_type = 'retail' and days_in_status between 31 and 60 then 'retail_raw_60'
		when status = 'raw' and sale_type = 'retail' and days_in_status between 61 and 90 then 'retail_raw_90'
		when status = 'raw' and sale_type = 'retail' and days_in_status > 90 then 'retail_raw_90+'
		when status = 'raw' and sale_type = 'wholesale' and days_in_status between 0 and 30 then 'ws_raw_30'
		when status = 'raw' and sale_type = 'wholesale' and days_in_status between 31 and 60 then 'ws_raw_60'
		when status = 'raw' and sale_type = 'wholesale' and days_in_status between 61 and 90 then 'ws_raw_90'
		when status = 'raw' and sale_type = 'wholesale' and days_in_status > 90 then 'ws_raw_90+'		
		when status like 'avail%' and sale_type = 'retail' and days_in_status between 0 and 30 then 'retail_lot_30'
		when status like 'avail%' and sale_type = 'retail' and days_in_status between 31 and 60 then 'retail_lot_60'
		when status like 'avail%' and sale_type = 'retail' and days_in_status between 61 and 90 then 'retail_lot_90'
		when status like 'avail%' and sale_type = 'retail' and days_in_status > 90 then 'retail_lot_90+'
		when status like 'avail%' and sale_type = 'wholesale' and days_in_status between 0 and 30 then 'ws_lot_30'
		when status like 'avail%' and sale_type = 'wholesale' and days_in_status between 31 and 60 then 'ws_lot_60'
		when status like 'avail%' and sale_type = 'wholesale' and days_in_status between 61 and 90 then 'ws_lot_90'
		when status like 'avail%' and sale_type = 'wholesale' and days_in_status > 90 then 'ws_lot_90+'
	end as disposition
from tem.inventory_2019_2020_days
where the_date = date_sold ;

select date_sold, disposition, count(*)
from tem.inventory_dispositions
group by date_sold, disposition
order by date_sold, disposition


select a.the_date, a.stock_number, a.shape, a.size, 
  a.date_acquired, a.date_sold, b.disposition
-- select *
from tem.inventory_2019_2020_days a
left join tem.inventory_dispositions b
  on a.stock_number = b.stock_number
  and a.vin = b.vin
  and a.date_acquired = b.date_acquired
  and a.date_sold = b.date_sold
where a.shape = 'pickup'
  and a.size = 'large'
  and a.year_month = 201902
order by a.the_date

select a.the_date, b.disposition, count(*)
-- select *
from tem.inventory_2019_2020_days a
left join tem.inventory_dispositions b
  on a.stock_number = b.stock_number
  and a.vin = b.vin
  and a.date_acquired = b.date_acquired
  and a.date_sold = b.date_sold
where a.shape = 'pickup'
  and a.size = 'large'
  and a.year_month in( 201902, 201910)
group by a.the_date, b.disposition
order by a.the_date, b.disposition

