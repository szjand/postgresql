﻿ /*
01/02/21
after going through some mind fucking and trying to implement this using greg.uc_daily_snapshot_beta_1
decided that arkona is less fucked up than the tool
fundamentally that is, still need tool for glump

01/14/2021
narrowed this all down to the tables/queries to be used by afton for the first vision pages
2 years of proportions shape-size and priceband by month
2 years of sales-inventory by month

01/17/2021
decided to use tool to get inventory for the priceband line charts
the arkona inventory was close enough for shape and size, but to get consistent pricing
and to be able to accomodate pre walk null prices it was necessary to go with the tool for all of it
tem.inv_2: arkona based inventory
tem.inv_2t: tool based inventory

this file finally brings all relevant queries together for V1 of used car analysis:
  1. 2019 - 2020 sales proportions by shape_size and by priceband
  2. 2019 - 2020 monthly sales vs inventory
*/
-- these 3 queries generate the basis for sales in a temp table: step_3
-- from fin_data_mart/sql/fact_fs/fact_fs_monthly_update_including_page_2.sql
drop table if exists step_1 cascade;
create temp table step_1 as
select year_month, page, line, line_label, control, sum(unit_count) as unit_count, sum(amount) as amount
from (
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201901 and 202012 --------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month between 201901 and 202012 -------------------------------------------------------------------
      and (
        (b.page = 16 and b.line between 1 and 5)) -- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row -- **
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
      and f.store = 'RY1'
    group by f.store, d.gl_account, b.page, b.line, b.line_label, e.description  ) d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by year_month, page, line, line_label, control
order by year_month, page, line;
create index on step_1(control);

-- select * from step_1 limit 100

-- for line chart of sales by day over 2 years, need sale date. go with bopmast_date_capped
-- 1/13/21: i am OK with limiting this to units where unit_count = 1
drop table if exists step_2 cascade;  --4367
create temp table step_2 as
select a.year_month, b.date_capped as sale_date, a.control as stock_number, -a.amount as amount, b.bopmast_vin as vin, b.retail_price 
from step_1 a
left join arkona.ext_bopmast b on a.control = b.bopmast_stock_number
where a.unit_count = 1  -- G37123G is the only one 
  and b.bopmast_vin is not null;
-- group by a.year_month, a.control, -a.amount, b.bopmast_vin, b.retail_price, b.date_capped;
create index on step_2(vin);
create index on step_2(stock_number);
create index on step_2(retail_price);
create index on step_2(sale_date);

drop table if exists tem.price_bands_ext;
create table tem.price_bands_ext as
select * from uc.price_bands where price_band <> '40k+'
union
select * from tem.high_end_price_bands 
order by price_from;
create index on tem.price_bands_ext(price_from);
create index on tem.price_bands_ext(price_thru);

drop table if exists step_3 cascade;
create temp table step_3 as
select a.*, coalesce(c.make, b.make) as make, coalesce(c.model, b.model) as model, --4351
  (split_part(d.vehiclesegment, '_', 2))::citext as size, (split_part(d.vehicletype, '_', 2))::citext as shape,
  e.price_band, -- , f.hi_price_band,
  e.price_from -- needed for sorting on pricebands
from step_2 a
left join arkona.ext_inpmast b on a.vin = b.inpmast_vin
left join ads.ext_vehicle_items c on a.vin = c.vin
join ads.ext_make_model_classifications d on coalesce(c.make, b.make) = d.make and coalesce(c.model, b.model) = d.model
left join price_bands_ext e on a.retail_price between e.price_from and e.price_thru;
create index on step_3(size);
create index on step_3(shape);
create index on step_3(price_band);
create index on step_3(year_month);

drop table if exists tem.step_3 cascade;
create table tem.step_3 as
select * from step_3;
create index on tem.step_3(size);
create index on tem.step_3(shape);
create index on tem.step_3(price_band);
create index on tem.step_3(year_month);
comment on table tem.step_3 is 'sales data for 2019 & 2020 to be used as the basis for the first vision used car analysis pages,
data based on financial statement retail used car & truck data. vehicle information fleshed out from inpmast, bopmast & ads make_model_classifications.
initiall 2 pages:
  1. proportion of sales by shape-size, proportion of sales by priceband, 
  2. sales vs inventory by shape-size, sales vs inventory by priceband';


-- -- shape size proportions
-- -- 02/13/21, redo with veh.shap_size_classifications
-- drop table if exists tem.sales_shape_size_proportions cascade;
-- create table tem.sales_shape_size_proportions as
-- select aa.shape, aa.size, aa.the_year, aa.year_month, 
--   count(vin) over (partition by aa.the_year) as year_total,
--   count(vin) over (partition by aa.year_month) as month_total, 
--   count(vin) over (partition by aa.the_year, aa.shape, aa.size) as year_shape_size,
--   count(vin) over (partition by aa.year_month, aa.shape, aa.size) as month_shape_size
-- from (
--   select *
--   from tem.shape_size
--   cross join (
--     select distinct the_year, year_month
--     from dds.dim_date
--     where the_year between 2019 and 2020) a) aa
-- left join tem.step_3 bb on aa.year_month = bb.year_month
--   and aa.shape = bb.shape
--   and aa.size = bb.size;
-- create index on tem.sales_shape_size_proportions(shape);
-- create index on tem.sales_shape_size_proportions(size);
-- create index on tem.sales_shape_size_proportions(the_year);
-- create index on tem.sales_shape_size_proportions(year_month);
-- create index on tem.sales_shape_size_proportions(month_total);
-- create index on tem.sales_shape_size_proportions(year_total);
-- create index on tem.sales_shape_size_proportions(year_total);
-- create index on tem.sales_shape_size_proportions(month_shape_size);

-- shape size proportions
-- 02/13/21, redo with veh.shap_size_classifications
drop table if exists tem.sales_shape_size_proportions cascade;
create table tem.sales_shape_size_proportions as
select aa.shape, aa.size, aa.the_year, aa.year_month, 
  count(vin) over (partition by aa.the_year) as year_total,
  count(vin) over (partition by aa.year_month) as month_total, 
  count(vin) over (partition by aa.the_year, aa.shape, aa.size) as year_shape_size,
  count(vin) over (partition by aa.year_month, aa.shape, aa.size) as month_shape_size
from (
  select *
  from tem.shape_size
  cross join (
    select distinct the_year, year_month
    from dds.dim_date
    where the_year between 2019 and 2020) a) aa
left join tem.step_3 bb on aa.year_month = bb.year_month
  and aa.shape = bb.shape_new
  and aa.size = bb.size_new;
create index on tem.sales_shape_size_proportions(shape);
create index on tem.sales_shape_size_proportions(size);
create index on tem.sales_shape_size_proportions(the_year);
create index on tem.sales_shape_size_proportions(year_month);
create index on tem.sales_shape_size_proportions(month_total);
create index on tem.sales_shape_size_proportions(year_total);
create index on tem.sales_shape_size_proportions(year_total);
create index on tem.sales_shape_size_proportions(month_shape_size);


--02/13/21 vision porportion page now shows the new data
-- sent to afton 01/14/2021
select shape, size, the_year, year_month, month_total, year_total, year_shape_size, month_shape_size,
  round(100.0 * year_shape_size/year_total, 1) as year_propor,
  round(100.0 * month_shape_size/month_total, 1) as month_propor
from tem.sales_shape_size_proportions 
group by shape, size, the_year, year_month, month_total, year_total, year_shape_size, month_shape_size
order by shape, size, year_month;



-- priceband proportions
drop table if exists tem.price_band_proportions cascade;
create table tem.price_band_proportions as
select aa.price_band, aa.the_year, aa.year_month, 
  count(vin) over (partition by aa.the_year) as year_total,
  count(vin) over (partition by aa.year_month) as month_total, 
  count(vin) over (partition by aa.the_year, aa.price_band) as year_price_band,
  count(vin) over (partition by aa.year_month, aa.price_band) as month_price_band
from (
  select *
  from tem.price_bands_ext
  cross join (
    select distinct the_year, year_month
    from dds.dim_date
    where the_year between 2019 and 2020) a) aa
left join tem.step_3 bb on aa.year_month = bb.year_month
  and aa.price_band = bb.price_band;
create index on tem.price_band_proportions(price_band);
create index on tem.price_band_proportions(the_year);
create index on tem.price_band_proportions(year_month);
create index on tem.price_band_proportions(month_total);
create index on tem.price_band_proportions(year_total);
create index on tem.price_band_proportions(year_price_band);
create index on tem.price_band_proportions(month_price_band);

-- sent to afton 01/14/2021
select price_band, the_year, year_month, month_total, year_total, year_price_band, month_price_band,
  round(100.0 * year_price_band/year_total, 1) as year_propor,
  round(100.0 * month_price_band/month_total, 1) as month_propors
from tem.price_band_proportions 
group by price_band, the_year, year_month, month_total, year_total, year_price_band, month_price_band
order by price_band, the_year, year_month, month_total, year_total, year_price_band, month_price_band;
 

--------------------------------------------------------------------
--< inventory
for the purpose of pricing inventory, the tool proves to be a substantially more comprehensive approach
--------------------------------------------------------------------
-- not much luck using gl data for used car inventory, going with inpmast
-- next step will probably require needing to know exact dates in inventory, and possible tool statuses
-- for this though, using avg inventory per month

drop table if exists tem.inv_1 cascade;
create table tem.inv_1 as  -- 275562
select b.the_date, a.inpmast_stock_number as stock_number, a.inpmast_vin as vin, a.make, a.model
from arkona.xfm_inpmast a
join dds.dim_date b on b.the_date between a.row_from_date and case when a.row_thru_date = '12/31/9999' then current_date else a.row_thru_date end
where a.type_n_u = 'U'
  and a.inpmast_vin <> '3GYFNGE37DS598684'
  and a.status = 'I'
  and left(a.inpmast_stock_number, 1) <> 'H'
  and a.row_from_date < current_date and a.row_thru_date > '12/31/2018'
group by b.the_date, a.inpmast_stock_number, a.inpmast_vin, a.make, a.model;
create unique index on tem.inv_1(the_date,stock_number,vin); 


-- this is the base table for inventory, for both shape_size and price_band
-- 1 row for each day a vehicle in inventory
drop table if exists tem.inv_2 cascade;
create table tem.inv_2 as -- 275562
select a.the_date, a.stock_number, a.vin,
  coalesce(b.make, a.make) as make, coalesce(b.model, a.model) as model, 
    coalesce(split_part(c.vehiclesegment, '_', 2), 'unknown')::citext as size, 
    coalesce(split_part(c.vehicletype, '_', 2), 'unknown')::citext as shape
from tem.inv_1 a
left join ads.ext_vehicle_items b on a.vin = b.vin
left join ads.ext_make_model_classifications c on coalesce(b.make, a.make) = c.make and coalesce(b.model, a.model) = c.model;
create unique index on tem.inv_2(the_date, vin);
create index on tem.inv_2(shape);
create index on tem.inv_2(size);

-- 02/13/21 update tem.inv_2 with normalized shape, size, make, model
drop table if exists tem.inv_2a cascade;
create table tem.inv_2a as
select vin, make, model, size, shape
from tem.inv_2 a 
where a.vin not in (-- motorcycles, couple of odd anomalies
  'JS1GT76A252103098', '1HD1GX1336K331300', '1HD4NBB1XGC504180',
  '1HD1MAL15FB855467','ZAM57RTA0E1126849','ZAM57RTA0E1126849','1G3NL52M9WM319554',
  '1HD1ECL17FY118289','1HD1JBB171Y013848','F10YPS81146')    
group by vin, make, model, size, shape;

update tem.inv_2 x
  set make = y.make,
      model = y.model,
      shape = y.shape,
      size = y.size
from ( -- updates the needed makes and models from chrome, shapes and sizes from veh.shape_size_classifications    
  select aa.*, bb.shape, bb.size
  from (-- need to do make model first -- if new else old
    select distinct a.vin, 
      coalesce(r->'division'->>'$value', a.make)::citext as make, 
      coalesce(r->'model'->>'$value', a.model)::citext as model
    from tem.inv_2a a
    left join chr2.describe_vehicle_by_vin_with_tech b on a.vin = b.vin
    left join jsonb_array_elements(b.response->'style') as r on true) aa
  -- now all makes and models should decode to shape and size
  left join veh.shape_size_classifications bb on aa.make = bb.make
    and aa.model = bb.model) y
where x.vin = y.vin;

-- --02/13/21 
-- -- redo with veh.shape_size_classifications
-- drop table if exists tem.shape_size cascade;
-- create table tem.shape_size (
--   shape citext not null,
--   size citext not null,
--   primary key (shape,size));
-- create index on tem.shape_size(shape);  
-- create index on tem.shape_size(size);
-- insert into tem.shape_size
-- select distinct split_part(vehicletype, '_', 2) as shape, split_part(vehiclesegment, '_', 2) as size
-- from ads.ext_make_model_classifications;

drop table if exists tem.shape_size cascade;
create table tem.shape_size (
  shape citext not null,
  size citext not null,
  primary key (shape,size));
create index on tem.shape_size(shape);  
create index on tem.shape_size(size);
insert into tem.shape_size
select distinct shape, size
from veh.shape_size_classifications;

-- --02/13/21 
-- -- updated with veh.shape_size_classifications
drop table if exists tem.date_shape_size cascade;
create table tem.date_shape_size as
select the_date, year_month, shape, size, seq
from dds.dim_date a
cross join (
  select distinct shape, size,
    case size
      when 'compact' then 1
      when 'small' then 2
      when 'medium' then 3 -- when 'midsize' then 3
      when 'large' then 4
      when 'extra large' then 5 -- when 'extra' then 5
    end as seq
  from tem.shape_size) b
where a.the_year in (2019, 2020);
create index on tem.date_shape_size(the_date);
create index on tem.date_shape_size(shape);
create index on tem.date_shape_size(size);
create index on tem.date_shape_size(year_month);

-- --02/13/21 
-- -- updated with veh.shape_size_classifications
drop table if exists tem.inv_shape_size_by_day cascade;  --11696
create table tem.inv_shape_size_by_day as
select a.*, coalesce(b.units, 0) as units
from tem.date_shape_size a
left join (
  select the_date, shape, size, count(*)::integer as units
  from tem.inv_2
  group by the_date, shape, size) b on a.the_date = b.the_date and a.shape = b.shape and a.size = b.size;
create index on tem.inv_shape_size_by_day(the_date);
create index on tem.inv_shape_size_by_day(year_month);
create index on tem.inv_shape_size_by_day(shape);
create index on tem.inv_shape_size_by_day(size);

--/> inventory shape and size -----------------------------------------------------------------

--< shape size sales vs inventory -------------------------------------------------------------
-- sent to afton 1/14/21

select 'inv' as source, shape, size, year_month, round(avg(units), 1) as units
from tem.inv_shape_size_by_day
group by year_month, shape, size
union
select 'sales' as source, shape, size, year_month, month_shape_size
from tem.sales_shape_size_proportions 
group by shape, size, year_month, month_shape_size
order by shape, size, year_month, source;
--/> shape size sales vs inventory -------------------------------------------------------------


--< inventory price band -----------------------------------------------------------------
-- this is where the tool inventory starts


drop table if exists tem.inv_2t cascade;  --297000
create table tem.inv_2t as
select b.the_date, a.vehicleinventoryitemid, a.stocknumber as stock_number, c.vin, c.make, c.model,
  coalesce(split_part(d.vehiclesegment, '_', 2), 'unknown')::citext as size, 
  coalesce(split_part(d.vehicletype, '_', 2), 'unknown')::citext as shape
from ads.ext_Vehicle_inventory_items a
join dds.dim_date b on b.the_date between a.fromts::date and case when a.thruts::date = '12/31/9999' then current_date else a.thruts::date end
join ads.ext_vehicle_items c on a.vehicleitemid = c.vehicleitemid
left join ads.ext_make_model_classifications d on c.make = d.make and c.model = d.model
where left(a.stocknumber, 1) <> 'H'
  and right(a.stocknumber, 1) not in ('1','2','3','4','5','6','7','8','9','0')  
  and a.fromts::date < current_date and a.thruts::date > '12/31/2018';
create unique index on tem.inv_2t(the_date,stock_number,vin); 


-- select *
-- from (
-- select vehicleinventoryitemid, stock_number, vin, make, model, shape, size, min(the_date) as from_date, max(the_date) as thru_date
-- from tem.inv_2t 
-- group by vehicleinventoryitemid, stock_number, vin, make, model, shape, size) a
-- full outer join (
-- select stock_number, vin, make, model, shape, size, min(the_date) as from_date, max(the_date) as thru_date
-- from tem.inv_2
-- group by stock_number, vin, make, model, shape, size) b on a.stock_number = b.stock_number and a.vin  = b.vin



drop table if exists tem.inv_shape_size_by_day_tool cascade;  --11696
create table tem.inv_shape_size_by_day_tool as
select a.*, coalesce(b.units, 0) as units
from tem.date_shape_size a
left join (
  select the_date, shape, size, count(*)::integer as units
  from tem.inv_2t
  group by the_date, shape, size) b on a.the_date = b.the_date and a.shape = b.shape and a.size = b.size;
create index on tem.inv_shape_size_by_day_tool(the_date);
create index on tem.inv_shape_size_by_day_tool(year_month);
create index on tem.inv_shape_size_by_day_tool(shape);
create index on tem.inv_shape_size_by_day_tool(size);
 

--< inventory price band -----------------------------------------------------------------

drop table if exists tem.date_price_bands cascade;
create table tem.date_price_bands as
select the_date, year_month, price_from, price_band
from dds.dim_date a
cross join (
  select distinct price_from, price_band
  from tem.price_bands_ext) b
where a.the_year in (2019, 2020)
order by the_date, price_from;
create index on tem.date_price_bands(the_date);
create index on tem.date_price_bands(price_band);
create index on tem.date_price_bands(year_month);   


drop table if exists tem.inv_date_price_tool cascade;  -- 275562
create table tem.inv_date_price_tool as 
select a.the_date, a.stock_number, a.vin, b.best_price 
from tem.inv_2t a
left join tem.inv_prices b on a.vin = b.vin
  and a.stock_number = b.stock_number
  and a.the_date between b.from_date and b.thru_date
order by a.stock_number;
create unique index on tem.inv_date_price_tool(stock_number,the_date);


-- so, if i am going with the tool for inventory for pricing, should i also do it for shape size?
-- no, the avg inventory from the tool was very close to that from inpmast, so leave it be

drop table if exists tem.inv_date_price_tool_2 cascade;  -- fill in the null prices with first price, add price_bands
create table tem.inv_date_price_tool_2  as
select aa.year_month, a.the_date, a.stock_number, a.vin, coalesce(a.best_price, b.first_best_price) as best_price, c.price_band
from tem.inv_date_price_tool a
join dds.dim_date aa on a.the_date = aa.the_date
left join (
  select stock_number, vin, first_best_price
  from (
    select stock_number, vin, first_value(best_price) over (partition by stock_number, vin order by the_date) as first_best_price
    from tem.inv_date_price_tool
    where best_price is not null) a
    group by stock_number, vin, first_best_price) b on a.stock_number = b.stock_number and a.vin = b.vin    
left join tem.price_bands_ext c on coalesce(a.best_price, b.first_best_price) between c.price_from and c.price_thru;
create unique index on tem.inv_date_price_tool_2(the_date,stock_number);



select 'inv'::citext as source, price_band, year_month, round(avg(units), 1) as avg_inv
from (
  select a.year_month, a.the_date, a.price_band, count(b.stock_number) as units
  from tem.date_price_bands a
  left join tem.inv_price_bands_by_day_tool b on a.price_band = b.price_band and a.the_date = b.the_date
  where a.price_band <> 'not priced'
  group by a.year_month, a.the_date, a.price_band) c
group by year_month,  price_band
union
select 'sales'::citext as source, price_band, year_month, sum(units) as sales
from (
  select a.year_month, a.the_date, a.price_band, count(b.stock_number) as units
  from tem.date_price_bands a
  left join tem.step_3 b on a.price_band = b.price_band
    and a.the_date = b.sale_date
  group by a.year_month, a.the_date, a.price_band) c
group by year_month, price_band
order by price_band, year_month, source;

 