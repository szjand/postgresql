﻿-- as i go through glump_proportion_of_sales_5.sql, this is a scratchpad to work out anomalies
-- old vehicles without a bodyType object in the style object
insert into chr2.bodies (style_id,body,row_from_date)
select distinct aaa.style_id, bbb.response->'vinDescription'->'attributes'->>'bodyType', current_date
from (
  select aa.*
  from (
    select a.vin, (r -> 'attributes' ->> 'id')::integer as style_id
    from chr2.describe_vehicle_by_vin_with_tech a  
    join jsonb_array_elements(a.response->'style') as r on true) aa
  left join chr2.bodies bb on aa.style_id = bb.style_id
  where bb.style_id is null) aaa
left join chr2.describe_vehicle_by_vin_with_tech bbb on aaa.vin = bbb.vin
where not exists (
  select 1
  from chr2.bodies
  where style_id = aaa.style_id);


-- old vehicles with no alt_body_type in style->attributes, also no relevant tech_specs, hence no cab or bed
-- this will give me a cab, but no bed available
select style_id, left(style_name, position('Cab' in style_name) - 2) as cab, current_date as row_from_date
from (
  select distinct aaa.style_id, bbb.response->'vinDescription'->'attributes'->>'styleName' as style_name, current_date
  from (
    select aa.*
    from ( -- get the style_ids from the json
      select a.vin, (r -> 'attributes' ->> 'id')::integer as style_id
      from chr2.describe_vehicle_by_vin_with_tech a  
      join jsonb_array_elements(a.response->'style') as r on true) aa
    join ( -- get the style_ids of pickup styles with no alt_body_type
      select style_id 
      from chr2.style f
      join chr2.subdivisions g on f.subdivision_id = g.subdivision_id
      where f.alt_body_type is null 
        and g.subdivision like '%pick%') bb on aa.style_id = bb.style_id) aaa
  left join chr2.describe_vehicle_by_vin_with_tech bbb on aaa.vin = bbb.vin) ccc     
   

-- old vehicles missing drivetrain, cars have none, but trucks do
select style_id, right(style_name, 3) as drivetrain, current_date as row_from_date
from (
  select distinct aaa.style_id, bbb.response->'vinDescription'->'attributes'->>'styleName' as style_name, current_date
  from (
    select aa.*
    from ( -- get the style_ids from the json
      select a.vin, (r -> 'attributes' ->> 'id')::integer as style_id
      from chr2.describe_vehicle_by_vin_with_tech a  
      join jsonb_array_elements(a.response->'style') as r on true) aa
    join ( -- get the style_ids of pickup styles with no alt_body_type
      select style_id 
      from chr2.style f
      join chr2.subdivisions g on f.subdivision_id = g.subdivision_id
      where f.drivetrain is null 
        and g.subdivision like '%pick%') bb on aa.style_id = bb.style_id) aaa
  left join chr2.describe_vehicle_by_vin_with_tech bbb on aaa.vin = bbb.vin) ccc     


 select * from veh.makes_models where model like 'c/k%' 

 select * from tem.wtf where row_from_date = current_date

 -- inventory --------------------------------------------------------------------------------

-- holy shit 275562
select count(*) from tem.inv_2 

-- 398 where make & model don't match
drop table if exists tem.tmp1 cascade;
create table tem.tmp1 as
select a.*
from (
  select distinct vin, make, model 
  from tem.inv_2) a
left join veh.shape_size_classifications b on a.make = b.make 
  and a.model = b.model
where b.make is null;

the most reliable, i believe, way to address these is with the chr2.describe vehicle path
so
-- 304 more vins to process
select distinct a.vin
from tem.tmp1 a
left join chr2.describe_vehicle_by_vin_with_tech b on a.vin = b.vin
where b.vin is null
  and a.make not like 'harley%'

bad vin: JS1GT76A252103098, 1HD1GX1336K331300, 1HD4NBB1XGC504180 does not decode


select * from ads.ext_vehicle_items where vin in ('JS1GT76A252103098', '1HD1GX1336K331300', '1HD4NBB1XGC504180')
aha, motorcycles
select * from arkona.ext_inpmast where inpmast_vin in ('JS1GT76A252103098', '1HD1GX1336K331300', 
  '1HD4NBB1XGC504180','1HD1MAL15FB855467','WDDGJ7HB9DF972943')  

select distinct a.vin
from tem.tmp1 a
join chr2.describe_vehicle_by_vin_with_tech b on a.vin = b.vin
where b.vin is null

drop table if exists tem.wtf;
create table tem.wtf as
select aaa.vin, aaa.make, aaa.model, aaa.shape, aaa.size, aaa.new_make, aaa.new_model, ccc.shape as new_shape, ccc.size as new_size
from (
  select bb.vin, bb.make, bb.model, bb.shape, bb.size, r->'division'->>'$value' as new_make, r->'model'->>'$value' as new_model
  from (
    select vin, make, model, shape, size
    from (
      select a.the_date, a.stock_number, a.vin,
        coalesce(b.make, a.make) as make, coalesce(b.model, a.model) as model, 
          coalesce(split_part(c.vehiclesegment, '_', 2), 'unknown')::citext as size, 
          coalesce(split_part(c.vehicletype, '_', 2), 'unknown')::citext as shape
      from tem.inv_1 a
      left join ads.ext_vehicle_items b on a.vin = b.vin
      left join ads.ext_make_model_classifications c on coalesce(b.make, a.make) = c.make 
        and coalesce(b.model, a.model) = c.model
      where a.vin not in ('JS1GT76A252103098', '1HD1GX1336K331300', '1HD4NBB1XGC504180',
        '1HD1MAL15FB855467','ZAM57RTA0E1126849','ZAM57RTA0E1126849','1G3NL52M9WM319554',
        '1HD1ECL17FY118289','1HD1JBB171Y013848','F10YPS81146')) aa
  group by vin, make, model, shape, size) bb
  left join chr2.describe_vehicle_by_vin_with_tech cc on bb.vin = cc.vin
  left join jsonb_array_elements(cc.response->'style') as r on true) aaa
left join veh.shape_size_classifications ccc on coalesce(aaa.new_make, aaa.make) = ccc.make
  and coalesce(aaa.new_model, aaa.model) = ccc.model;


select a.*, b.vin
from tem.wtf a
left join chr2.describe_vehicle_by_vin_with_tech b on a.vin = b.vin
where new_shape is null

select * from chr2.describe_vehicle_by_vin_with_tech where vin = '5GRGN23U64H106940'

create index on tem.inv_2(vin);


-- 02/14/21  wtf, still a lot of missing shit
drop table if exists tem.inv_2a cascade;
create table tem.inv_2a as
select vin, make, model, size, shape
from tem.inv_2 a 
where a.vin not in ('JS1GT76A252103098', '1HD1GX1336K331300', '1HD4NBB1XGC504180',
        '1HD1MAL15FB855467','ZAM57RTA0E1126849','ZAM57RTA0E1126849','1G3NL52M9WM319554',
        '1HD1ECL17FY118289','1HD1JBB171Y013848','F10YPS81146')    
group by vin, make, model, size, shape;

-- need to do make model first -- if new else old
-- select * from ( -- and we are golden
select aa.*, bb.shape, bb.size
from (-- need to do make model first -- if new else old
  select distinct a.vin, 
    coalesce(r->'division'->>'$value', a.make)::citext as make, 
    coalesce(r->'model'->>'$value', a.model)::citext as model
  from tem.inv_2a a
  left join chr2.describe_vehicle_by_vin_with_tech b on a.vin = b.vin
  left join jsonb_array_elements(b.response->'style') as r on true) aa
-- now all makes and models should decode to shape and size
left join veh.shape_size_classifications bb on aa.make = bb.make
  and aa.model = bb.model
-- ) x where shape is null  