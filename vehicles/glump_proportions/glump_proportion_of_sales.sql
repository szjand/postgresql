﻿-- unit counts
drop table if exists unit_counts cascade;
create temp table unit_counts as
select year_month, page, line, line_label, control, sum(unit_count) as unit_count, sum(amount) as amount
from (
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_year between 2015 and 2020 --------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.the_year between 2015 and 2020 -------------------------------------------------------------------
      and (
        (b.page = 16 and b.line between 1 and 5)) -- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row -- **
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
      and f.store = 'RY1'
    group by f.store, d.gl_account, b.page, b.line, b.line_label, e.description  ) d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by year_month, page, line, line_label, control;
create index on unit_counts(year_month);
create index on unit_counts(line);


-- 01/08 thinking greg.uc_daily_snapshot_beta_1 is good enough
select *
from (
  select year_month, sum(unit_count) as units 
  from unit_counts
  group by year_month) a
join (
  select year_month, sum(unit_count) as units 
  from step_1
  group by year_month) b on a.year_month = b.year_month
join (
  select year_month, count(*)
  from greg.uc_daily_snapshot_beta_1 a
  inner join dds.dim_date b on a.the_date = b.the_date
  where a.sale_date = a.the_date
    and b.year_month between 201901 and 202012
    and a.store_code = 'RY1'
    and a.sale_type = 'Retail'
  group by year_month) c on a.year_month = c.year_month
order by a.year_month;

-- compare proportions
select aa.*, bb.year_month, bb.total, bb.count, bb."%"
from (
  select b.shape, b.size, a.total, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (
    select count(*) as total
    from step_3
    where year_month between 202001 and 202012) a
  join (
    select shape, size, count(*) as glump
    from step_3
    where year_month between 202001 and 202012
    group by shape, size) b on true) aa
left join (
  select a.year_month, a.total, b.shape, b.size, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (
    select year_month, count(*) as total -- totals
    from step_3
    where year_month = 202006
    group by year_month) a
  left join (
    select year_month, shape, size, count(*) as glump
    from step_3
    where year_month = 202006
    group by year_month, shape, size) b on a.year_month = b.year_month) bb on aa.shape = bb.shape and aa.size = bb.size
order by aa.count desc;

-- shape size
with greg as (
  select year_month, shape, size, count(*) as units
  from greg.uc_daily_snapshot_beta_1 a
  inner join dds.dim_date b on a.the_date = b.the_date
  where a.sale_date = a.the_date
    and b.the_year = 2020
    and a.store_code = 'RY1'
    and a.sale_type = 'Retail'
  group by year_month, shape, size)
select aa.*, bb.year_month, bb.total, bb.count, bb."%"
from (  
  select b.shape, b.size, a.total, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (  
    select sum(units) as total
    from greg) a 
  join (
    select shape, size, sum(units) as glump
    from greg
    group by shape, size) b on true) aa
left join (
  select a.year_month, a.total, b.shape, b.size, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (
    select year_month, sum(units) as total -- totals
    from greg
    where year_month = 202006
    group by year_month) a
  left join (
    select year_month, shape, size, sum(units) as glump
    from greg
    where year_month = 202006
    group by year_month, shape, size) b on a.year_month = b.year_month) bb on aa.shape = bb.shape and aa.size = bb.size
order by aa.count desc;    


drop table if exists greg;
create temp table greg as
  select year_month, price_band, count(*) as units
  from greg.uc_daily_snapshot_beta_1 a
  inner join dds.dim_date b on a.the_date = b.the_date
  where a.sale_date = a.the_date
    and a.the_date = a.sale_date
    and b.the_year = 2020
    and a.store_code = 'RY1'
    and a.sale_type = 'Retail'
    and price_band <> 'not priced'
  group by year_month, price_band;

select * from greg
select * from step_3 where year_month between 202001 and 202012

-- why is price band coming up with a total of 1691 units for 2020 when step_3 has 1963

select stock_number, vin, price_band from step_3 where year_month between 202001 and 202012

  select stock_number, vin, price_band
  from greg.uc_daily_snapshot_beta_1 a
  inner join dds.dim_date b on a.the_date = b.the_date
  where a.sale_date = a.the_date
    and b.the_year = 2020
    and a.store_code = 'RY1'
    and a.sale_type = 'Retail'
    and price_band <> 'not priced'


select * from  step_3 where stock_number = 'G30093G'
select * from greg.uc_daily_snapshot_beta_1 where stock_number = 'G34928XZA'















-- priceband
-- 01/09/21 not priced accounts for ~ 250 units ?!?!?
with greg as (
  select year_month, price_band, count(*) as units
  from greg.uc_daily_snapshot_beta_1 a
  inner join dds.dim_date b on a.the_date = b.the_date
  where a.sale_date = a.the_date
    and a.the_date = a.sale_date
    and b.the_year = 2020
    and a.store_code = 'RY1'
    and a.sale_type = 'Retail'
    and price_band <> 'not priced'
  group by year_month, price_band)
select aa.*--, bb.year_month, bb.total, bb.count, bb."%"
from (  
  select b.price_band, a.total, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (  
    select sum(units) as total
    from greg) a 
  join (
    select price_band, sum(units) as glump
    from greg
    group by price_band) b on true) aa
left join (
  select a.year_month, a.total, b.price_band, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (
    select year_month, sum(units) as total -- totals
    from greg
    where year_month = 202006
    group by year_month) a
  left join (
    select year_month, price_band, sum(units) as glump
    from greg
    where year_month = 202006
    group by year_month, price_band) b on a.year_month = b.year_month) bb on aa.price_band = bb.price_band
order by aa.count desc;  

select *
from greg.uc_daily_snapshot_beta_1
where sale_type = 'retail'
  and sale_date between '06/01/2020' and '06/30/2020'
  and the_date = sale_date
  and store_code = 'RY1'

select *
from greg.uc_daily_snapshot_beta_1
where stock_number = 'G31065AZ'
order by the_date

-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------

-- from fin_data_mart/sql/fact_fs/fact_fs_monthly_update_including_page_2.sql
drop table if exists step_1 cascade;
create temp table step_1 as
select year_month, page, line, line_label, control, sum(unit_count) as unit_count, sum(amount) as amount
from (
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201901 and 202012 --------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month between 201901 and 202012 -------------------------------------------------------------------
      and (
        (b.page = 16 and b.line between 1 and 5)) -- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row -- **
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
      and f.store = 'RY1'
    group by f.store, d.gl_account, b.page, b.line, b.line_label, e.description  ) d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by year_month, page, line, line_label, control
order by year_month, page, line;
create index on step_1(control);


-- for line chart of sales by day over 2 years, need sale date. go with bopmast_date_capped
drop table if exists step_2 cascade;  --4367
create temp table step_2 as
select a.year_month, b.date_capped as sale_date, a.control as stock_number, -a.amount as amount, b.bopmast_vin as vin, b.retail_price 
from step_1 a
left join arkona.ext_bopmast b on a.control = b.bopmast_stock_number
where a.unit_count = 1
  and b.bopmast_vin is not null
group by a.year_month, a.control, -a.amount, b.bopmast_vin, b.retail_price, b.date_capped;
create index on step_2(vin);
create index on step_2(stock_number);
create index on step_2(retail_price);
create index on step_2(sale_date);

drop table if exists step_3 cascade;
create temp table step_3 as
select a.*, coalesce(c.make, b.make) as make, coalesce(c.model, b.model) as model, --4351
  split_part(d.vehiclesegment, '_', 2) as size, split_part(d.vehicletype, '_', 2) as shape,
  e.price_band, f.hi_price_band
from step_2 a
left join arkona.ext_inpmast b on a.vin = b.inpmast_vin
left join ads.ext_vehicle_items c on a.vin = c.vin
join ads.ext_make_model_classifications d on coalesce(c.make, b.make) = d.make and coalesce(c.model, b.model) = d.model
join uc.price_bands e on a.retail_price between e.price_from and e.price_thru
left join tem.high_end_price_bands f on a.retail_price >= 40000
  and a.retail_price between f.price_from and f.price_thru
where d.vehicletype is not null;
create index on step_3(size);
create index on step_3(shape);
create index on step_3(price_band);
create index on step_3(year_month);

select *  -- 1 g37123g
from step_2
where vin is null

-- all in different months, OK
select *
from step_2 a
join (
  select stock_number
  from step_2
  group by stock_number
  having count(*) > 1) b on a.stock_number = b.stock_number
order by a.stock_number

-- add on a separate over 40 set of price bands
drop table if exists tem.high_end_price_bands cascade;
create table tem.high_end_price_bands (
  hi_price_band citext primary key,
  price_from integer not null,
  price_thru integer not null);
create index on tem.high_end_price_bands(price_from);  
create index on tem.high_end_price_bands(price_thru);
insert into tem.high_end_price_bands values ('40-50k',40000,49999);
insert into tem.high_end_price_bands values ('50-60k',50000,59999);
insert into tem.high_end_price_bands values ('60-70k',60000,69999);
insert into tem.high_end_price_bands values ('70k+',700000,99999);







select * from step_3

-- will probably wa to add sequence
drop table if exists tem.shapes_sizes cascade;
create table tem.shapes_sizes (
  shape citext not null,
  size citext not null,
  primary key (shape,size));
create index on tem.shapes_sizes(shape);  
create index on tem.shapes_sizes(size);
insert into tem.shapes_sizes
select distinct split_part(vehicletype, '_', 2) as shape, split_part(vehiclesegment, '_', 2) as size
from ads.ext_make_model_classifications;


select *
from (
  select a.*, b.price_band 
  from tem.shapes_sizes a
  cross join uc.price_bands b
  where b.price_band <> 'not priced') aa


select a.shape, a.size, b.price_band,
  count(c.vin) filter (where c.year_month = 201901) as "201901",
  count(c.vin) filter (where c.year_month = 201902) as "201902",
  count(c.vin) filter (where c.year_month = 201903) as "201903",
  count(c.vin) filter (where c.year_month = 201904) as "201904",
  count(c.vin) filter (where c.year_month = 201905) as "201905",
  count(c.vin) filter (where c.year_month = 201906) as "201906",
  count(c.vin) filter (where c.year_month = 201907) as "201907",
  count(c.vin) filter (where c.year_month = 201908) as "201908",
  count(c.vin) filter (where c.year_month = 201909) as "201909",
  count(c.vin) filter (where c.year_month = 201910) as "201910",
  count(c.vin) filter (where c.year_month = 201911) as "201911",
  count(c.vin) filter (where c.year_month = 201912) as "201912",
  count(c.vin) filter (where c.year_month = 202001) as "202001",
  count(c.vin) filter (where c.year_month = 202002) as "202002",
  count(c.vin) filter (where c.year_month = 202003) as "202003",
  count(c.vin) filter (where c.year_month = 202004) as "202004",
  count(c.vin) filter (where c.year_month = 202005) as "202005",
  count(c.vin) filter (where c.year_month = 202006) as "202006",
  count(c.vin) filter (where c.year_month = 202007) as "202007",
  count(c.vin) filter (where c.year_month = 202008) as "202008",
  count(c.vin) filter (where c.year_month = 202009) as "202009",
  count(c.vin) filter (where c.year_month = 202010) as "202010",
  count(c.vin) filter (where c.year_month = 202011) as "202011",
  count(c.vin) filter (where c.year_month = 202012) as "202012"    
from tem.shapes_sizes a
join uc.price_bands b on true
  and b.price_band <> 'not priced'
left join step_3 c on a.shape = c.shape
    and a.size = c.size
    and b.price_band = c.price_band
group by a.shape, a.size, b.price_band
order by a.shape, a.size, b.price_band

-- these are the only glumps with at least one sale
-- and the totals over the 2 years
select a.shape, a.size, a.price_band, count(*)
from step_3 a
group by a.shape, a.size, a.price_band
order by a.shape, a.size, a.price_band

lets narrow this down to a single month and figure out what i want
june of 2020 had 232

select *
from step_3
where year_month = 202006


-- proportional_v2.csv
select a.year_month, a.total, b.shape, b.size, b.price_band, b.glump, 100.0 * b.glump/a.total  
from (
  select year_month, count(*) as total -- totals
  from step_3
  where year_month between 202006 and 202007
  group by year_month) a
left join (
  select year_month, shape, size, price_band, count(*) as glump
  from step_3
  where year_month between 202006 and 202007
  group by year_month, shape, size, price_band) b on a.year_month = b.year_month
order by a.year_month, b.glump -- b.shape, b.size, b.price_band 


select year_month, count(*) from step_3 where year_month between 202001 and 202012group by year_month order by count(*)

-- the number are so small
-- do just shape and size for year_months 202004 202012, 202006, 202007
-- proportional_shape_size.csv
select aa.*, bb.year_month, bb.total, bb.count, bb."%",
  cc.year_month, cc.total, cc.count, cc."%",
  dd.year_month, dd.total, dd.count, dd."%",
  ee.year_month, ee.total, ee.count, ee."%"
from (
  select b.shape, b.size, a.total, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (
    select count(*) as total
    from step_3
    where year_month between 202001 and 202012) a
  join (
    select shape, size, count(*) as glump
    from step_3
    where year_month between 202001 and 202012
    group by shape, size) b on true) aa
left join (
  select a.year_month, a.total, b.shape, b.size, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (
    select year_month, count(*) as total -- totals
    from step_3
    where year_month = 202004
    group by year_month) a
  left join (
    select year_month, shape, size, count(*) as glump
    from step_3
    where year_month = 202004
    group by year_month, shape, size) b on a.year_month = b.year_month) bb on aa.shape = bb.shape and aa.size = bb.size
left join (
  select a.year_month, a.total, b.shape, b.size, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (
    select year_month, count(*) as total -- totals
    from step_3
    where year_month = 202006
    group by year_month) a
  left join (
    select year_month, shape, size, count(*) as glump
    from step_3
    where year_month = 202006
    group by year_month, shape, size) b on a.year_month = b.year_month) cc on aa.shape = cc.shape and aa.size = cc.size    
left join (
  select a.year_month, a.total, b.shape, b.size, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (
    select year_month, count(*) as total -- totals
    from step_3
    where year_month = 202007
    group by year_month) a
  left join (
    select year_month, shape, size, count(*) as glump
    from step_3
    where year_month = 202007
    group by year_month, shape, size) b on a.year_month = b.year_month) dd on aa.shape = dd.shape and aa.size = dd.size     
left join (
  select a.year_month, a.total, b.shape, b.size, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (
    select year_month, count(*) as total -- totals
    from step_3
    where year_month = 202012
    group by year_month) a
  left join (
    select year_month, shape, size, count(*) as glump
    from step_3
    where year_month = 202012
    group by year_month, shape, size) b on a.year_month = b.year_month) ee on aa.shape = ee.shape and aa.size = ee.size        
order by aa.count desc  

-- just price band
-- proportional_price_band.csv
select aa.*, bb.year_month, bb.total, bb.count, bb."%",
  cc.year_month, cc.total, cc.count, cc."%",
  dd.year_month, dd.total, dd.count, dd."%",
  ee.year_month, ee.total, ee.count, ee."%"
from (
  select b.price_band, a.total, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (
    select count(*) as total
    from step_3
    where year_month between 202001 and 202012) a
  join (
    select price_band, count(*) as glump
    from step_3
    where year_month between 202001 and 202012
    group by price_band) b on true) aa
left join (
  select a.year_month, a.total, b.price_band, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (
    select year_month, count(*) as total -- totals
    from step_3
    where year_month = 202004
    group by year_month) a
  left join (
    select year_month, price_band, count(*) as glump
    from step_3
    where year_month = 202004
    group by year_month, price_band) b on a.year_month = b.year_month) bb on aa.price_band = bb.price_band 
left join (
  select a.year_month, a.total, b.price_band, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (
    select year_month, count(*) as total -- totals
    from step_3
    where year_month = 202006
    group by year_month) a
  left join (
    select year_month, price_band, count(*) as glump
    from step_3
    where year_month = 202006
    group by year_month, price_band) b on a.year_month = b.year_month) cc on aa.price_band = cc.price_band
left join (
  select a.year_month, a.total, b.price_band, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (
    select year_month, count(*) as total -- totals
    from step_3
    where year_month = 202007
    group by year_month) a
  left join (
    select year_month, price_band, count(*) as glump
    from step_3
    where year_month = 202007
    group by year_month, price_band) b on a.year_month = b.year_month) dd on aa.price_band = dd.price_band 
left join (
  select a.year_month, a.total, price_band, b.glump as count, round(100.0 * b.glump/a.total, 1) as "%"
  from (
    select year_month, count(*) as total -- totals
    from step_3
    where year_month = 202012
    group by year_month) a
  left join (
    select year_month, price_band, count(*) as glump
    from step_3
    where year_month = 202012
    group by year_month, price_band) b on a.year_month = b.year_month) ee on aa.price_band = ee.price_band 
order by aa.count desc  

--------------------------------------------------------------------
--< inventory
--------------------------------------------------------------------
drop table if exists inv_1 cascade;
-- inventory by day from inpmast, 2019 - 2020
create temp table inv_1 as  -- 257335
select a.the_date,  b.inpmast_stock_number as stock_number, b.inpmast_vin as vin, b.make, b.model
from dds.dim_date a
join arkona.xfm_inpmast b on a.the_date between b.row_from_date and b.row_thru_date
  and b.type_n_u = 'U'
  and b.status = 'I'
  and left(b.inpmast_stock_number, 1) <> 'H'
where a.year_month between 201901 and 202012
group by a.the_date, b.inpmast_stock_number, b.inpmast_vin, b.make, b.model;
create unique index on inv_1(the_date, vin);


drop table if exists inv_2 cascade;
-- add vehicle_items, make_model_classifications
create temp table inv_2 as
select a.the_date, a.stock_number, a.vin,
  coalesce(b.make, a.make) as make, coalesce(b.model, a.model) as model, --4351
    coalesce(split_part(c.vehiclesegment, '_', 2), 'unknown') as size, 
    coalesce(split_part(c.vehicletype, '_', 2), 'unknown') as shape
from inv_1 a
left join ads.ext_vehicle_items b on a.vin = b.vin
left join ads.ext_make_model_classifications c on coalesce(b.make, a.make) = c.make and coalesce(b.model, a.model) = c.model;
create unique index on inv_2(the_date, vin);
create index on inv_2(shape);
create index on inv_2(size);

drop table if exists inv_prices_tmp cascade;
create temp table inv_prices_tmp as
select c.stocknumber as stock_number, d.vin, vehiclepricingts::date as date_priced, max(amount) as best_price
from ads.ext_vehicle_pricings a
join ads.ext_vehicle_pricing_details b on a.vehiclepricingid = b.vehiclepricingid
  and b.typ = 'VehiclePricingDetail_BestPrice'
join ads.ext_vehicle_inventory_items c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
join ads.ext_vehicle_items d on a.vehicleitemid = d.vehicleitemid
-- where vehiclepricingts::date > '01/01/2016'  
group by c.stocknumber, d.vin, vehiclepricingts::date;

-- to get price on a day, think i need to convert price date to from and thru
drop table if exists inv_prices cascade;
create temp table inv_prices as
select a.stock_number, a.vin, date_priced as from_date,
  coalesce(lead(date_priced - 1, 1) over(partition by stock_number, vin order by date_priced), '12/31/9999') as thru_date,
  best_price::integer
from inv_prices_tmp a;
create index on inv_prices(stock_number);
create index on inv_prices(vin);
create index on inv_prices(from_date);
create index on inv_prices(thru_date);
create index on inv_prices(best_price);

-- !!! inventory not priced, will be days for most vehicles in inventory without a price
select a.the_date, a.stock_number, a.vin, a.make, a.model, b.from_date, b.thru_date, b.best_price
from inv_2 a
left join inv_prices b on a.vin = b.vin
  and a.the_date between b.from_date and b.thru_date
where a.vin = '1C3CDFEB7FD410606'
  and b.best_price is not null






--------------------------------------------------------------------
--/> inventory
--------------------------------------------------------------------

select * from step_3 limit 100

line charts, need sales by day
-- no way 1686 values of 1
select sale_date, size, shape, count(*) as sales
from step_3
group by sale_date, size, shape
order by count(*)

-- lets see the graph
-- shape_size_2_years.csv
select year_month,shape || '-' || size as shape_size, count(*) as sales
from step_3
group by year_month, shape || '-' || size
order by year_month, shape || '-' || size




-- first shot at line graphs
-- on the graph separated shapes to make them visible

select shape || '-' || size as shape_size,
  count(c.vin) filter (where c.year_month = 201901) as "201901",
  count(c.vin) filter (where c.year_month = 201902) as "201902",
  count(c.vin) filter (where c.year_month = 201903) as "201903",
  count(c.vin) filter (where c.year_month = 201904) as "201904",
  count(c.vin) filter (where c.year_month = 201905) as "201905",
  count(c.vin) filter (where c.year_month = 201906) as "201906",
  count(c.vin) filter (where c.year_month = 201907) as "201907",
  count(c.vin) filter (where c.year_month = 201908) as "201908",
  count(c.vin) filter (where c.year_month = 201909) as "201909",
  count(c.vin) filter (where c.year_month = 201910) as "201910",
  count(c.vin) filter (where c.year_month = 201911) as "201911",
  count(c.vin) filter (where c.year_month = 201912) as "201912",
  count(c.vin) filter (where c.year_month = 202001) as "202001",
  count(c.vin) filter (where c.year_month = 202002) as "202002",
  count(c.vin) filter (where c.year_month = 202003) as "202003",
  count(c.vin) filter (where c.year_month = 202004) as "202004",
  count(c.vin) filter (where c.year_month = 202005) as "202005",
  count(c.vin) filter (where c.year_month = 202006) as "202006",
  count(c.vin) filter (where c.year_month = 202007) as "202007",
  count(c.vin) filter (where c.year_month = 202008) as "202008",
  count(c.vin) filter (where c.year_month = 202009) as "202009",
  count(c.vin) filter (where c.year_month = 202010) as "202010",
  count(c.vin) filter (where c.year_month = 202011) as "202011",
  count(c.vin) filter (where c.year_month = 202012) as "202012"    
from step_3 c
group by shape || '-' || size
order by shape || '-' || size



select * from step_3 where year_month = 202004 order by stock_number







 
--<-----------------------------------------------------------------------------------------------------------------------------
-- afton  11:52 AM
-- When you have a chance could you gather the max number of used vehicles we had in inventory for the past 6 months? - from yem
-- wanted to do it with accounting, but turned out to be too much of a mind fuck today

select the_date, 
  case
    when store = 'G' then 'RY1'
    when store = 'H' then 'RY2'
  end, count(*) as units
from (  
  select the_date,  b.inpmast_vin, left(inpmast_stock_number, 1) as store
  from dds.dim_date a
  join arkona.xfm_inpmast b on a.the_date between b.row_from_date and b.row_thru_date
    and b.type_n_u = 'U'
    and b.status = 'I'
  where a.the_date between current_date -181 and current_date - 1  
    and left(b.inpmast_stock_number, 1) in ('G','H')
  group by a.the_date, b.inpmast_vin, left(inpmast_stock_number, 1)
  order by a.the_date, left(inpmast_stock_number, 1)) x
group by the_date, store
order by store, units desc

select the_date, count(*) as units
from (  
  select the_date,  b.inpmast_vin, left(inpmast_stock_number, 1) as store
  from dds.dim_date a
  join arkona.xfm_inpmast b on a.the_date between b.row_from_date and b.row_thru_date
    and b.type_n_u = 'U'
    and b.status = 'I'
  where a.the_date between current_date -181 and current_date - 1  
    and left(b.inpmast_stock_number, 1) in ('G','H')
  group by a.the_date, b.inpmast_vin, left(inpmast_stock_number, 1)
  order by a.the_date, left(inpmast_stock_number, 1)) x
group by the_date
order by units desc

GM 403 on 12/31/2020
Honda Nissan 128 on 01/06/2021
Market 526 on 01/03/2021


-- afton  2:50 PM
-- sorry. my terrible explaining. only market and max per month for 6 months
-- 
-- jon  2:51 PM
-- do you mean the max for each of the past 6 months?

select year_month, count(*) as units
from (  
  select a.year_month,  b.inpmast_vin
  from dds.dim_date a
  join arkona.xfm_inpmast b on a.the_date between b.row_from_date and b.row_thru_date
    and b.type_n_u = 'U'
    and b.status = 'I'
  where a.year_month between 202007 and 202012
  group by a.year_month,  b.inpmast_vin) x
group by year_month
order by a.year_month

select year_month, max(units)
from (
  select year_month, the_date, count(*) as units
  from (  
    select a.year_month, a.the_date,  b.inpmast_vin
    from dds.dim_date a
    join arkona.xfm_inpmast b on a.the_date between b.row_from_date and b.row_thru_date
      and b.type_n_u = 'U'
      and b.status = 'I'
    where a.year_month between 202007 and 202012
    group by a.year_month, a.the_date,  b.inpmast_vin) x
  group by year_month, the_date) xx
group by year_month  


202007: 419
202008: 395
202009: 384
202010: 416
202011: 422
202012: 523



--/>-----------------------------------------------------------------------------------------------------------------------------
