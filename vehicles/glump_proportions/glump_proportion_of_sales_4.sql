﻿02/05/21
02/05/21

whats next
need to convert the sales data from 2019 2020 (tem.step_3) to shape and size from veh.shape_size_classifications
then, get vehicle detail information for drilling into the 2 pages (proportions & sales vs inventory)

-- add the new shape size classifications done by wilkie in vision
alter table tem.step_3
add column shape_new citext,
add column size_new citext;
create index on tem.step_3(shape_new);
create index on tem.step_3(size_new);

-- cautious approach to updating make and model
alter table tem.step_3
add column make_new citext,
add column model_new citext;
create index on tem.step_3(make_new);
create index on tem.step_3(model_new);



update tem.step_3 x
set shape_new = y.shape_new,
    size_new = y.size_new
from (
  select a.make, a.model, b.shape as shape_new, b.size as size_new
  from tem.step_3 a
  left join  veh.shape_size_classifications b on a.make = b.make and a.model = b.model) y
where x.make = y.make
  and x.model = y.model;
  
problem 1 the make and model in tem.step_3 do not match the make and model in veh.xxxxx

-- 416 rows
select * from tem.step_3 where shape_new is null

my first impulse is to submit those 416 vins in a batch to chrome, from which we should have matching makes and models

-- now i realize that i have already submitted a bunch of vins to chrome for off brand vins stored in chr2.describe_vehicle_by_vin
-- so using that and chr.describe_vehicle_by_vin which is new car (originally) vins
-- this gives me 48 of 416 with a chrome decoded vin
select a.*, b.vin, b.style_count, c.vin, c.style_count
from tem.step_3 a
left join chr2.describe_vehicle_by_vin b on a.vin = b.vin
left join chr.describe_vehicle c on a.vin = c.vin
where a.shape_new is null
  and (b.vin is not null or c.vin is not null)

the biggest initial concern are the fucking fords with 10 - 13 styles per vin, although, i am guessing make and model
will be consistent across the multiple styleids
lets go ahead and experiment with the data i have and see where it goes

-- yep this narrows them down to a single make/model for each vin
select stock_number, vin, make, model, style_count, chr_model_year, chr_make, chr_model
from (
  select a.stock_number, a.vin, a.make, a.model, b.style_count,
    (r.style ->'attributes'->>'modelYear')::integer as chr_model_year,
    (r.style ->'division'->>'$value')::citext as chr_make,
    (r.style ->'model'->>'$value'::citext)::citext as chr_model
  from tem.step_3 a
  join chr2.describe_vehicle_by_vin b on a.vin = b.vin
  join jsonb_array_elements(b.response->'style') as r(style) on true
  where a.shape_new is null) aa
group by stock_number, vin, make, model, style_count, chr_model_year, chr_make, chr_model



-- i am eager to try some vin batches, but wait, lets see what dealertrack can give us
-- tem.step_3 make and model come from coalesce(tool, dealertrack)
-- lets see what kind of match i can get using just dealertrack
-- this picks up 184 makes and models
 

-- time for a statement of purpose
-- for each vehicle in tem.step_3, i want the shape and size as determined by veh.shape_size_classifications
-- what that means is for those vehicle make and models that are not in veh.shape_size_classifications, i
-- need to run chrome data on them
-- step 1, vehicles in tem.step_3 where shape_new is null
-- 416 vehicles
select * from tem.step_3 where shape_new is null
-- step 2 exclude vehicles for which i already have chrome data
-- this leaves me with 368 vehicles
select a.*
from tem.step_3 a
left join chr2.describe_vehicle_by_vin b on a.vin = b.vin
left join chr.describe_vehicle c on a.vin = c.vin
where a.shape_new is null
  and b.vin is null 
  and c.vin is null
-- step 3 exclude vehicles for which the make and model from inpmast match veh.shape_size_classifications
-- which leaves me with 212 (of 4477) vehicles for which i don't have a matching make and model
-- these are the vins for which i want to submit a chrome batch
select a.*
from tem.step_3 a
left join chr2.describe_vehicle_by_vin b on a.vin = b.vin
left join chr.describe_vehicle c on a.vin = c.vin
left join (
    select a.vin
    from tem.step_3 a
    left join arkona.ext_inpmast b on a.vin = b.inpmast_vin
    left join  veh.shape_size_classifications c on b.make = c.make and b.model = c.model
    where a.shape_new is null
      and c.shape is not null) d  on a.vin = d.vin
where a.shape_new is null
  and b.vin is null 
  and c.vin is null
  and d.vin is null

-- -- step 4 process the vehicle_description file from batch 2020-12-15-07-54_non_captive_vins
-- -- in make_model_normalization.py, table DDL is at end of this script
-- select a.vin, a.model_year, a.division, a.model_name , b.*
-- from chr2.vehicle_description a
-- left join veh.shape_size_classifications b on a.division = b.make and a.model_name = b.model
-- where division is not null
-- 
-- ok, this step is taking me down a different path
-- at least for vehicle_description, but probably for arkona as well, just go ahead and update the shape_new and size_new fields in tem.step_3
-- -- well fuck me, this only gives me the same 11 fords i got from chr2.describe_vehicle_by_vin
-- select a.*, b.model_year, b.division, b.model_name
-- from tem.step_3 a
-- left join chr2.vehicle_description b on a.vin = b.vin
-- where a.shape_new is null


-- step 5 build a vin batch, comprised of vin and stock_numbers as passthru_id
                    
select a.stock_number ||':'|| a.vin , a.vin, null,NULL,null,null,null,
  null,null,null,null,null,null,null,null,null,null,
  null,null,null
from tem.step_3 a
left join chr2.describe_vehicle_by_vin b on a.vin = b.vin
left join chr.describe_vehicle c on a.vin = c.vin
left join (
    select a.vin
    from tem.step_3 a
    left join arkona.ext_inpmast b on a.vin = b.inpmast_vin
    left join  veh.shape_size_classifications c on b.make = c.make and b.model = c.model
    where a.shape_new is null
      and c.shape is not null) d  on a.vin = d.vin
where a.shape_new is null
  and b.vin is null 
  and c.vin is null
  and d.vin is null




select aa.*, bb.model_year, bb.division, bb.model_name, cc.*
from (
  select a.*
  from tem.step_3 a
  left join chr2.describe_vehicle_by_vin b on a.vin = b.vin
  left join chr.describe_vehicle c on a.vin = c.vin
  left join (
      select a.vin
      from tem.step_3 a
      left join arkona.ext_inpmast b on a.vin = b.inpmast_vin
      left join  veh.shape_size_classifications c on b.make = c.make and b.model = c.model
      where a.shape_new is null
        and c.shape is not null) d  on a.vin = d.vin
  where a.shape_new is null
    and b.vin is null 
    and c.vin is null
    and d.vin is null) aa
left join chr2.tmp_vehicle_description bb on aa.vin = bb.vin
left join veh.shape_size_classifications cc on bb.division = cc.make and bb.model_name = cc.model
where aa.shape_new is null
order by model_year


select b.year, b.make, b.model, count(*)
from tem.step_3 a
join arkona.ext_inpmast b on a.vin = b.inpmast_vin
group by b.year, b.make, b.model
order by b.year

-- 255 (252 distinct vins) pre 2005 vehicles in tem.step_3  of 4477 vehicles (4386 distinct vins)
select count(distinct a.vin)
from tem.step_3 a
join arkona.ext_inpmast b on a.vin = b.inpmast_vin
where b.year < 2005

still got a bunch of clean up to do

select * 
from chr2.vehicle_description a limit 10

select * from chr2.tmp_vehicle_description where vin in ('2HGES26763H527414','JA4MT31R32P009691','1G3WH52K2XF301665','2G1WN54TXP9265319','2GCEK19M9V1150308')

select * from (
                select a.stock_number ||':'|| a.vin , a.vin, null,NULL,null,null,null,
                  null,null,null,null,null,null,null,null,null,null,
                  null,null,null
                from tem.step_3 a
                left join chr2.describe_vehicle_by_vin b on a.vin = b.vin
                left join chr.describe_vehicle c on a.vin = c.vin
                left join (
                    select a.vin
                    from tem.step_3 a
                    left join arkona.ext_inpmast b on a.vin = b.inpmast_vin
                    left join  veh.shape_size_classifications c on b.make = c.make and b.model = c.model
                    where a.shape_new is null
                      and c.shape is not null) d  on a.vin = d.vin
                where a.shape_new is null
                  and b.vin is null 
                  and c.vin is null
                  and d.vin is null
) x where vin in ('2HGES26763H527414','JA4MT31R32P009691','1G3WH52K2XF301665','2G1WN54TXP9265319','2GCEK19M9V1150308')                  



-- 02/08/21
where i am angsting is will a vin batch derived chr2 tables well and accurately update the veh tables
so, with the existing vin batch, work thru it and lets see
i can actually step through chr2.update_chr2_tables() & chr2.update_veh_tables()

a vin with multiple styles

basically for each file, what is the difference if generated by vin or by style_id

because there should be some overlap, any of the model year 2005 - 2022 vins should already have data 
in each of the tables based on 

1.  had to  change data type of passthru_id from integer to citext in all tables

1a. move data from chr2.tmp_vehicle_description in to chr2.vehicle_description

select *
from chr2.vehicle_description a
join chr2.tmp_vehicle_description b on a.vin = b.vin

insert into chr2.vehicle_description 
select *
from chr2.tmp_vehicle_description
where passthru_id <> 'G34979C:F10YPS81146';

-- multiple vins
select *
from chr2.tmp_vehicle_description a
join (
  select vin
  from chr2.tmp_vehicle_description
  group by vin 
  having count(*) > 1) b on a.vin = b.vin
where a.passthru_id <> 'G34979C:F10YPS81146'  
order by a.vin  


select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'chr2'
  and table_name = 'tmp_vehicle_description'


insert into chr2.vehicle_description 
select input_row_num,passthru_id,vin,gvwr_low,gvwr_high,world_manufacturer_identifier,
  manufacturer_identification_code,restraint_types,market_class_id,model_year,division,
  model_name,style_name,body_type,driving_wheels,built,country,language,best_make_name,
  best_model_name,best_style_name,best_trim_name,base_msrp_low,base_msrp_high,base_inv_low,
  base_inv_high,dest_chrg_low,dest_chrg_high,is_price_unknown,build_data_indicator,built_msrp
from (  
  select a.*, row_number() over (partition by vin)
  from chr2.tmp_vehicle_Description a
  where a.passthru_id <> 'G34979C:F10YPS81146'
  order by vin) b
where b.row_number = 1



2. chr2.drop_and_recreate_tmp_table_for_batch_processing
    change data type of passthru_id
    bring back chr2.tmp_vehicle_description

select chr2.drop_and_recreate_tmp_table_for_batch_processing()



3. lets look at just style first
ok, fails because pk on chr2.tmp_style is style_id

select * 
from chr2.tmp_style a

ok, want to compare style derived by vin vs style derived by style_id


select *
from chr2.style
where style_id = 332434

332434;8;6913;21310;24505.0;23279.76;825.0;f;16;2012;FWD 4dr LS w/2LS;FWD 4dr;LS w/2LS;1LD26;f;f;4;;;Sport Utility;Front Wheel Drive
332434,8,6913,21310,24505.0,23279.76,825.0,f,16,2012,FWD 4dr LS w/2LS,FWD 4dr,LS w/2LS,1LD26,f,f,4,,,Sport Utility,Front Wheel Drive

on the basis of a single vehicle, the apparent reality is that the 2 are the same

a vin batch does not give me anything more than a styleid batch
all i need from the vin is what is the styleid
and subsequently do those styleids exist in veh.vehicle_types

holy shit why is this style id not in chr2.exterior_colors
select * from chr2.exterior_color where style_id = 332434

select * from veh.vehicle_types where style_id = 332434

i see the problem in chr2.update_chr2_tables()
  insert into chr2.exterior_color
  select input_ro_num,passthru_id,style_id,install_cause,install_cause_detail,
    coalesce(color_code, 'none'),color_name,rgb_value
  from chr2.exterior_color
instead of selecting from chr2.tmp_exterior_colors
that is fixed, but not the data, y et  

-- problem does not exists in style, tech_specs, bodies, 
-- option, 3000 in 2008
select model_year, count(*)
from veh.vehicle_types a
where not exists (
  select 1
  from chr2.exterior_color
  where style_id = a.style_id)
group  by model_year
order by model_year

select * from chr2.exterior_color limit 100

-- 58114 missing 
select style_id
from veh.vehicle_types a
where not exists (
  select 1
  from chr2.exterior_color
  where style_id = a.style_id)

do the exist in jon.exterior_colors ? nope

select model_year, make, count(*) from jon.configurations group by model_year, make

so this has all been an illuminating distraction

get back to the tem.step_3 problem, please

so, what ia m thinking now is that all i need is the style file, that has the vin and the style_id
with that style id, i can normalize the make & model from veh.vehicle_types and also which makes and models
i have to pull yet, eg, pre 2005 vehicles, seems there were over 200 sold in 2019/2020

and that needs to be a separate table, both tmp and final



drop table if exists chr2.tmp_vin_style cascade;
CREATE TABLE chr2.tmp_vin_style (
  input_row_number integer,
  passthru_id citext,
  style_id integer,
  division_id integer,
  subdivision_id integer,
  model_id integer,
  base_msrp numeric,
  base_invoice numeric,
  dest_chrg numeric,
  is_price_unknown boolean ,
  market_class_id integer ,
  model_year integer ,
  style_name citext ,
  style_name_wo_trim citext ,
  trim_name citext,
  mfr_model_code citext ,
  is_fleet_only boolean ,
  is_model_fleet boolean ,
  pass_doors integer ,
  alt_model_name citext,
  alt_style_name citext,
  alt_body_type citext,
  drivetrain citext);
comment on table chr2.tmp_vin_style is 'target table for the style file generated from a vin batch.';  

drop table if exists chr2.vin_style cascade;
CREATE TABLE chr2.vin_style (
  input_row_number integer,
  passthru_id citext,
  style_id integer ,
  division_id integer ,
  subdivision_id integer ,
  model_id integer ,
  base_msrp numeric,
  base_invoice numeric,
  dest_chrg numeric,
  is_price_unknown boolean ,
  market_class_id integer ,
  model_year integer ,
  style_name citext ,
  style_name_wo_trim citext ,
  trim_name citext,
  mfr_model_code citext ,
  is_fleet_only boolean ,
  is_model_fleet boolean ,
  pass_doors integer ,
  alt_model_name citext,
  alt_style_name citext,
  alt_body_type citext,
  drivetrain citext);
comment on table chr2.vin_style is 'populated from chr2.vin_style, this is an interrim table only, used
to provide style_ids on vins to normalize make and model on legacy data & style_ids that do not yet 
exists in veh.vehicle_typesthere is no natural key worth bothering with.'  


ok, using batch 2021-02-06-16-20_vins, lets give this a shot and see where it takes us

select split_part(passthru_id,':',1) as stock_number, split_part(passthru_id,':',2) as vin,
  style_id, model_year, division_id, subdivision_id, model_id, market_class_id
-- select *
from chr2.tmp_vin_style 
where length(split_part(passthru_id,':',2)) = 17
order by model_year


select a.style_id,null,null,a.style_id,null,null,null,null,null,null,null,null,
  null,null,null,null,null,null,null,null
from chr2.tmp_vin_style a
where length(split_part(passthru_id,':',2)) = 17
  and not exists (
    select 1
    from veh.vehicle_types
    where style_id = a.style_id)
group by a.style_id

ran the batch in single_batch_style_ids.py
evertything up to update update_veh_tables()

style failed with null mfr_model_code
alter table chr2.tmp_style
alter column mfr_model_code drop not null;

alter table chr2.style
alter column mfr_model_code drop not null;

yikes failing due to FK constraints, but of course, these vehicles likely dont yet exist in models
and chr2.models is populated based on model_years and makes
so do i do a special version of ads_Base_tables.py for these situations, there will be more
there is a ton of legacy data with no chrome style ids

select b.division, a.model_year, a.* 
from chr2.tmp_vin_style a
left join (
  select division_id, division
  from chr2.divisions
  group by division_id, division) b on a.division_id = b.division_id
where length(split_part(passthru_id,':',2)) = 17
  and not exists (
    select 1
    from veh.vehicle_types
    where style_id = a.style_id)
order by a.model_year


-- 02/09/21
ok, here are the missing make/model_year 
select distinct coalesce(b.division, c.make) as make, a.model_year
from chr2.tmp_vin_style a
left join (
  select division_id, division
  from chr2.divisions
  group by division_id, division) b on a.division_id = b.division_id
left join ads.ext_vehicle_items c on split_part(a.passthru_id,':',2) = c.vin
where length(split_part(passthru_id,':',2)) = 17
  and not exists (
    select 1
    from veh.vehicle_types
    where style_id = a.style_id)
order by a.model_year

the question possibly becomes one of how much history do we need

at this point a could configure an ads_base_tables script to add all these make/models

select count(*) from tem.step_3  -- 4477
select * from tem.step_3 where not exists (select 1 from 


select distinct b.year, a.make, a.model, a.shape, a.size, a.shape_new, a.size_new
from tem.step_3 a
left join arkona.ext_inpmast b on a.vin = b.inpmast_vin
where a.shape_new is null
where a.shape <> coalesce(a.shape_new, 'wtf')


select * from veh.makes order by make

select make, model, string_agg(distinct year::text, ',')
from arkona.ext_inpmast
where make = 'oldsmobile'
group by make, model

we dont want to proceed with part of classification from tool part from chrome

-- 6. ----------------------------------------------------------------------------------

***********************************************************************
***********************************************************************
***********************************************************************

02/11/2021
went with updating the tables with data from describe vehicle vin requests
which is all in glump_proportion_of_sales_5.sql


***********************************************************************
***********************************************************************
***********************************************************************






vin batches also give me market_class_definition.csv (with data), with market_class_id in style.csv,
ah, but the fucking styleid batch style.csv does not have the marke_class_id, fuckers

TODO another potential concern, how many vehcles in tem.step_3 are pre 2005

    
-- this is the remaining 368 vins for non matching make/models
select a.*, d.year, d.make, d.model
from tem.step_3 a
left join chr2.describe_vehicle_by_vin b on a.vin = b.vin
left join chr.describe_vehicle c on a.vin = c.vin
left join arkona.ext_inpmast d on a.vin = d.inpmast_vin
where a.shape_new is null
  and b.vin is null
  and c.vin is null

  
-- 11 fords
-- first glance, combination of trim and box_size looks to differentiate the style ids
select a.stock_number, a.vin, a.make, a.model, b.style_count,
  (r.style ->'attributes'->>'id')::citext as chr_style_id,
  (r.style ->'attributes'->>'modelYear')::integer as chr_model_year,
  (r.style ->'division'->>'$value')::citext as chr_make,
  (r.style ->'model'->>'$value'::citext)::citext as chr_model,
  (r.style ->'attributes'->>'mfrModelCode')::citext as chr_model_code,
  case
    when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as chr_drive,
  case
    when u.cab->>'$value' like 'Crew%' then 'crew' 
    when u.cab->>'$value' like 'Extended%' then 'double'  
    when u.cab->>'$value' like 'Regular%' then 'reg'  
    else 'n/a'   
  end as chr_cab,
  case -- trim level
    when (r.style ->'model'->>'$value'::citext)::citext = 'equinox'
          and (r.style ->'attributes'->>'mfrModelCode')::citext in ( '1XY26','1XR26')
          and (r.style ->'attributes'->>'modelYear')::integer = 2020 then
      case
        when r.style ->'attributes'->>'name'::citext like '%1LT' then 'LT w/1LT'
        when r.style ->'attributes'->>'name'::citext like '%2LT' then 'LT w/2LT'
        when r.style ->'attributes'->>'name'::citext like '%2FL' then 'LT w/2FL'        
      end
    when (r.style ->'model'->>'$value'::citext)::citext = 'blazer' then 
      case 
        when r.style ->'attributes'->>'name'::citext like '%1LT' then '1LT'
        when r.style ->'attributes'->>'name'::citext like '%2LT' then '2LT'
        when r.style ->'attributes'->>'name'::citext like '%3LT' then '3LT'
        else coalesce(r.style ->'attributes'->>'trim', 'none') 
      end
    when (r.style ->'attributes'->>'modelYear')::integer = '2019' and (r.style ->'model'->>'$value'::citext)::citext = 'Silverado 1500 LD' then
      case
        when r.style ->'attributes'->>'name'::citext like '%1LT' then '1LT'
        when r.style ->'attributes'->>'name'::citext like '%2LT' then '2LT'
        else 'XXXXXXXXXXXXXXXXXXX'
      end 
    when (r.style ->'model'->>'$value'::citext)::citext = 'Express Cargo Van' 
        and (r.style ->'attributes'->>'mfrModelCode')::citext = 'CG33705' then '3500'
    else coalesce(r.style ->'attributes'->>'trim', 'none') 
  end::citext as chr_trim,
  case
    when v. bed->>'$value' like '%Bed' then substring(bed->>'$value', 1, position(' ' in bed->>'$value') - 1)
    else 'n/a'
  end as box_size      
from tem.step_3 a
join chr2.describe_vehicle_by_vin b on a.vin = b.vin
join jsonb_array_elements(b.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(r.style->'bodyType') with ordinality as v(bed) on true
  and v.ordinality =  1    
where a.shape_new is null

-- the problem may be that in neither the tool or dealertrack do we have the box size
-- on this vin, the tool and dealertrack don't agree on the vin
select * from ads.ext_vehicle_items where vin = '1FT7W2BT5JEB65739'
select inpmast_stock_number, inpmast_vin, year, make, model_code, model, body_style, 
  color, engine_code, key_to_cap_explosion_data -- this is the chrome styleid  396878
  -- trim = XL, 8' BOX
from arkona.ext_inpmast where inpmast_vin = '1FT7W2BT5JEB65739'




-- so lets resurrect the vehicle_description table, might as well create a tmp version as well
-- drop table if exists chr2.vehicle_description cascade;
-- CREATE TABLE chr2.vehicle_description (
--   input_row_num integer,
--   passthru_id integer NOT NULL,
--   vin citext,
--   gvwr_low citext,
--   gvwr_high citext,
--   world_manufacturer_identifier citext,
--   manufacturer_identification_code citext,
--   restraint_types citext,
--   market_class_id citext,
--   model_year citext,
--   division citext,
--   model_name citext,
--   style_name citext,
--   body_type citext,
--   driving_wheels citext,
--   built citext,
--   country citext,
--   language citext,
--   best_make_name citext NOT NULL,
--   best_model_name citext NOT NULL,
--   best_style_name citext NOT NULL,
--   best_trim_name citext,
--   base_msrp_low citext,
--   base_msrp_high numeric,
--   base_inv_low numeric,
--   base_inv_high numeric,
--   dest_chrg_low numeric,
--   dest_chrg_high numeric,
--   is_price_unknown boolean NOT NULL,
--   build_data_indicator citext,
--   built_msrp citext,
--   CONSTRAINT vehicle_description_pkey PRIMARY KEY (vin));
-- 
-- drop table if exists chr2.tmp_vehicle_description cascade;
-- CREATE TABLE chr2.tmp_vehicle_description (
--   input_row_num integer,
--   passthru_id integer NOT NULL,
--   vin citext,
--   gvwr_low citext,
--   gvwr_high citext,
--   world_manufacturer_identifier citext,
--   manufacturer_identification_code citext,
--   restraint_types citext,
--   market_class_id citext,
--   model_year citext,
--   division citext,
--   model_name citext,
--   style_name citext,
--   body_type citext,
--   driving_wheels citext,
--   built citext,
--   country citext,
--   language citext,
--   best_make_name citext NOT NULL,
--   best_model_name citext NOT NULL,
--   best_style_name citext NOT NULL,
--   best_trim_name citext,
--   base_msrp_low citext,
--   base_msrp_high numeric,
--   base_inv_low numeric,
--   base_inv_high numeric,
--   dest_chrg_low numeric,
--   dest_chrg_high numeric,
--   is_price_unknown boolean NOT NULL,
--   build_data_indicator citext,
--   built_msrp citext,
--   CONSTRAINT tmp_vehicle_description_pkey PRIMARY KEY (vin));

alter table chr2.vehicle_description
alter column passthru_id type citext;
alter table chr2.tmp_vehicle_description
alter column passthru_id type citext;

alter table chr2.vehicle_description 
alter column passthru_id type citext,
alter column is_price_unknown drop not null,
alter column best_make_name drop not null,
alter column best_model_name drop not null,
alter column best_style_name drop not null;

alter table chr2.tmp_vehicle_description 
alter column vin drop not null,
alter column is_price_unknown drop not null,
alter column best_make_name drop not null,
alter column best_model_name drop not null,
alter column best_style_name drop not null;

ALTER TABLE chr2.tmp_vehicle_description DROP CONSTRAINT tmp_vehicle_description_pkey

alter table chr2.tmp_body_type
alter column passthru_id type citext;

alter table chr2.tmp_engine
alter column passthru_id type citext;

alter table chr2.tmp_exterior_color
alter column passthru_id type citext;

alter table chr2.tmp_exterior_color_xref
alter column passthru_id type citext;

alter table chr2.tmp_image
alter column passthru_id type citext;

alter table chr2.tmp_interior_color
alter column passthru_id type citext;

alter table chr2.tmp_option
alter column passthru_id type citext;

alter table chr2.tmp_standard_equipment
alter column passthru_id type citext;

alter table chr2.tmp_style
alter column passthru_id type citext;

alter table chr2.tmp_tech_spec_value
alter column passthru_id type citext;

alter table chr2.tmp_vehicle_description
alter column passthru_id type citext;

---------------


alter table chr2.exterior_color
alter column passthru_id type citext;

alter table chr2.exterior_color_xref
alter column passthru_id type citext;

alter table chr2.image
alter column passthru_id type citext;

alter table chr2.interior_color
alter column passthru_id type citext;

alter table chr2.option
alter column passthru_id type citext;

alter table chr2.standard_equipment
alter column passthru_id type citext;

alter table chr2.style
alter column passthru_id type citext;

