﻿
select * 
from (
select make, count(*), min(year), max(year)
from arkona.ext_inpmast
where make in ('hummer','isuzu','mercury','pontiac','saab','saturn','scion','suzuki')
group by make) a
full outer join (
select make, count(*), min(yearmodel), max(yearmodel)
from ads.ext_vehicle_items
where make in ('hummer','isuzu','mercury','pontiac','saab','saturn','scion','suzuki')
group by make) b on a.make = b.make
order by a.make



perfect
after running ads_base_tables configured for the model years and makes to populate chr2.divisions, 
  chr2.subdivisions and chr2.models ( and the rest in that script)
and then running batches_8_new_makes.py to populate chr2.style (and the rest in that script)
this gives me all the new makes and models

select b.division as make, d.subdivision, e.model
from chr2.style a
join chr2.divisions b on a.division_id = b.division_id
join chr2.subdivisions d on a.subdivision_id = d.subdivision_id
  and d.subdivision not in ('Chevy Kodiak C-Series','Chevy W-Series',
    'GMC TopKick C-Series','GMC T-Series','Chevy T-Series','GMC W-Series',
    'Chevy Medium Duty','Ford Medium-Duty Chassis')
join chr2.models e on a.model_id = e.model_id
where b.division in ('Hummer','Isuzu','Mercury','Pontiac','Saab','Saturn','Scion','Suzuki')
group by b.division, d.subdivision, e.model;


-- step 1 update chr2.makes_models
insert into chr2.makes_models(make,subdivision,model)
select make, subdivision, model
from (
  select b.division as make, d.subdivision, e.model
  from chr2.style a
  join chr2.divisions b on a.division_id = b.division_id
  join chr2.subdivisions d on a.subdivision_id = d.subdivision_id
    and d.subdivision not in ('Chevy Kodiak C-Series','Chevy W-Series',
      'GMC TopKick C-Series','GMC T-Series','Chevy T-Series','GMC W-Series',
      'Chevy Medium Duty','Ford Medium-Duty Chassis')
  join chr2.models e on a.model_id = e.model_id
  where b.division in ('Hummer','Isuzu','Mercury','Pontiac','Saab','Saturn','Scion','Suzuki')
  group by b.division, d.subdivision, e.model) aa
where not exists (
  select 1
  from chr2.makes_models
  where make = aa.make
    and subdivision = aa.subdivision
    and model = aa.model);

-- step 2 update veh.makes
insert into veh.makes(make)  
select make
from chr2.makes_models a
where not exists (
  select 1 
  from veh.makes
  where make = a.make)
group by make;

-- step 3 run chr2.update_veh_tables()
and that is it

-- adding 2005-2017 and these 8 makes: 289 vehicle classifications pending
select *
from veh.makes_models a
left join veh.shape_size_classifications b on a.make = b.make and a.model = b.model
where b.make is null 