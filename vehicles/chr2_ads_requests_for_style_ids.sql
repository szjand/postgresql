﻿

Initial process:
  ADS service for version, model years, divisions, subdivisions, modes & style ids.
  parse these requests into the relevant tables: 2 step process: first into a get_xxx tables for the json data, then the final tables
  data to include only model years 2018 thru 2022 and 32 "relevant" divisions
  this generates (among others) the table chr2.style_ids, which can subsequently be used to generate batch requests from the style_id
  
build out the service requests part first
12/02/20: created and populated these chr2 tables based on ADS requests
-- chrome version info
drop schema if exists chr2 cascade;
create schema chr2;
comment on schema chr2 is 'a fresh schema for starting the vehicle data modeling project, beginning with chrome batch data';

drop table if exists chr2.version_info cascade;
create table chr2.version_info (
  the_date date NOT NULL primary key,
  response jsonb NOT NULL);

-- model years
drop table if exists chr2.get_model_years cascade;  
create table chr2.get_model_years (
  model_years jsonb);
comment on table chr2.get_model_years is 'destination for response from request to chrome ADS at https://beta.rydellvision.com:8888/chrome/model-years';

drop table if exists chr2.model_years cascade;
create table chr2.model_years (
  model_year integer primary key);
comment on table chr2.model_years is 'table of valid model years from chrome';
  
CREATE OR REPLACE FUNCTION chr2.xfm_model_years()
RETURNS void AS
$BODY$
/*
kimball type 1 updates
*/
  insert into chr2.model_years(model_year) 
  select jsonb_array_elements_text(model_years)::integer
  from chr2.get_model_years
  on conflict (model_year)
  do nothing;
$BODY$
LANGUAGE sql;
comment on function chr2.xfm_model_years() is 'parse data from chr2.get_model_years into chr2.model_years';

-- divisions
drop table if exists chr2.get_divisions cascade;
CREATE TABLE chr2.get_divisions(
  model_year integer primary key,
  divisions jsonb);
comment on table chr2.get_divisions is 'destination for response from request to chrome ADS at https://beta.rydellvision.com:8888/chrome/divisions/ + str(model_year[0].  initially getting only data for model years 2018 thru present';  

drop table if exists chr2.divisions cascade;
CREATE TABLE chr2.divisions (
  model_year integer NOT NULL references chr2.model_years(model_year),
  division_id integer NOT NULL,
  division citext NOT NULL,
  CONSTRAINT divisions_pkey PRIMARY KEY (model_year, division_id));
create index on chr2.divisions(division_id); 
create index on chr2.divisions(division);
create index on chr2.divisions(model_year);


CREATE OR REPLACE FUNCTION chr2.xfm_divisions()
  RETURNS void AS
$BODY$
/*
kimball type 1 updates
*/
  -- new rows
  insert into chr2.divisions(model_year, division_id, division) 
  select *
  from (
    select a.model_year, (attributes #>> '{id}')::integer as division_id, b."$value" as division
    from chr2.get_divisions a,
      jsonb_to_recordset(a.divisions) as b("$value" citext, attributes jsonb)) c
  where not exists (
    select 1
    from chr2.divisions
    where model_year = c.model_year
      and division_id = c.division_id);

  -- changed rows
  update chr2.divisions x
  set division = y.division
  from (   
    select a.model_year, (attributes #>> '{id}')::integer as division_id, b."$value" as division
    from chr2.get_divisions a,
      jsonb_to_recordset(a.divisions) as b("$value" citext, attributes jsonb)) y
  where x.model_year = y.model_year
    and x.division_id = y.division_id
    and x.division <> y.division;    
    
$BODY$
LANGUAGE sql;
comment on function chr2.xfm_divisions() is 'parse data from chr2.get_divisions into chr2.divisions';

-- subdivisions
drop table if exists chr2.get_subdivisions cascade;
CREATE TABLE chr2.get_subdivisions(
  model_year integer primary key,
  subdivisions jsonb);
comment on table chr2.get_subdivisions is 'destination for response from request to chrome ADS at https://beta.rydellvision.com:8888/chrome/subdivisions/ + str(model_year[0].  initially getting only data for model years 2018 thru present';  

drop table if exists chr2.subdivisions cascade;
CREATE TABLE chr2.subdivisions (
  model_year integer NOT NULL references chr2.model_years(model_year),
  subdivision_id integer NOT NULL,
  subdivision citext NOT NULL,
  CONSTRAINT subdivisions_pkey PRIMARY KEY (model_year, subdivision_id));
create index on chr2.subdivisions(subdivision_id); 
create index on chr2.subdivisions(subdivision);
create index on chr2.subdivisions(model_year);


CREATE OR REPLACE FUNCTION chr2.xfm_subdivisions()
  RETURNS void AS
$BODY$
/*
kimball type 1 updates
*/
  -- new rows
  insert into chr2.subdivisions(model_year, subdivision_id, subdivision) 
  select *
  from (
    select a.model_year, (attributes #>> '{id}')::integer as subdivision_id, b."$value" as subdivision
    from chr2.get_subdivisions a,
      jsonb_to_recordset(a.subdivisions) as b("$value" citext, attributes jsonb)) c
  where not exists (
    select 1
    from chr2.subdivisions
    where model_year = c.model_year
      and subdivision_id = c.subdivision_id);

  -- changed rows
  update chr2.subdivisions x
  set subdivision = y.subdivision
  from (   
    select a.model_year, (attributes #>> '{id}')::integer as subdivision_id, b."$value" as subdivision
    from chr2.get_subdivisions a,
      jsonb_to_recordset(a.subdivisions) as b("$value" citext, attributes jsonb)) y
  where x.model_year = y.model_year
    and x.subdivision_id = y.subdivision_id
    and x.subdivision <> y.subdivision;    
    
$BODY$
LANGUAGE sql;
comment on function chr2.xfm_subdivisions() is 'parse data from chr2.get_subdivisions into chr2.subdivisions';


      
-- models
drop table if exists chr2.get_models cascade;
CREATE TABLE chr2.get_models(
  model_year integer NOT NULL,
  division_id integer NOT NULL,
  models jsonb);
comment on table chr2.get_divisions is 'destination for response from request to chrome ADS at https://beta.rydellvision.com:8888/chrome/models with payload of model_year and division_id.';  

drop table if exists chr2.models cascade;
CREATE TABLE chr2.models(
  model_id integer primary key,
  model_year integer not null,
  division_id integer not null,
  model citext NOT NULL,
  CONSTRAINT models_model_year_fkey FOREIGN KEY (model_year, division_id)
        REFERENCES chr2.divisions (model_year, division_id));  
create index on chr2.models(model_year);
create index on chr2.models(division_id);
create index on chr2.models(model);


CREATE OR REPLACE FUNCTION chr2.xfm_models()
  RETURNS void AS
$BODY$
/*
kimball type 1 updates
*/
  -- new rows
  insert into chr2.models(model_id,model_year,division_id,model)
  select * 
  from (
    select (attributes #>> '{id}')::integer as model_id, model_year, division_id, b."$value" as model
    from chr2.get_models,
      jsonb_to_recordset(chr2.get_models.models) as b("$value" citext, attributes jsonb)) c
  where not exists (
    select 1
    from chr2.models
    where model_id = c.model_id);

  -- changed rows
  update chr2.models x
  set model = y.model
  from (
    select (attributes #>> '{id}')::integer as model_id, model_year, division_id, b."$value" as model
    from chr2.get_models,
      jsonb_to_recordset(chr2.get_models.models) as b("$value" citext, attributes jsonb)) y
  where x.model_id = y.model_id
    and x.model <> y.model;

$BODY$
LANGUAGE sql;
comment on function chr2.xfm_models() is 'parse data from chr2.get_models into chr2.models';

--styles

drop table if exists chr2.get_style_ids cascade;
CREATE TABLE chr2.get_style_ids (
  model_id integer primary key,
  style_ids jsonb NOT NULL);
comment on table chr2.get_divisions is 'destination for response from request to chrome ADS at https://beta.rydellvision.com:8888/chrome/styles/ + str(model_id[0])';

drop table if exists chr2.style_ids cascade;
CREATE TABLE chr2.style_ids (
  style_id citext primary key,
  model_id integer NOT NULL references chr2.models(model_id),
  style_name citext NOT NULL);
create index on chr2.style_ids(model_id);

-- alter table chr2.styles
-- rename to style_ids;
-- alter table chr2.style_ids
-- rename column chrome_style_id to style_id;



CREATE OR REPLACE FUNCTION chr2.xfm_style_ids()
  RETURNS void AS
$BODY$
-- new rows
  insert into chr2.style_ids (style_id,model_id,style_name)
  select *
    from (select (attributes #>> '{id}') style_id, model_id, b."$value" as style_name
    from chr2.get_style_ids,
      jsonb_to_recordset(chr2.get_style_ids.style_ids) as b("$value" citext, attributes jsonb) order by style_id) c
  where not exists (
    select 1
    from chr2.style_ids
    where style_id = c.style_id);   

-- changed rows
  update chr2.style_ids x
  set style_name = x.style_name
  from (
    select (attributes #>> '{id}') style_id, model_id, b."$value" as style_name
    from chr2.get_style_ids,
      jsonb_to_recordset(chr2.get_style_ids.style_ids) as b("$value" citext, attributes jsonb)) y
  where x.style_id = y.style_id
    and x.style_name <> y.style_name;
    
$BODY$
LANGUAGE sql;


12/3/20
32 division
model year 2018 - 2022
1564 models
14204 style ids


01/18/21
added model years 2005 - 2017, took 2 1/2 hours
-- need to look into this
-- its ok, there are divisions in chr2.division, that are not used
select count(distinct division) from chr2.divisions  57
-- yep, 32 divisions
select count(distinct division) 
from chr2.models a
join chr2.divisions b on a.division_id = b.division_id

select * from chr2.models limit 100
select model_year, count(*)
from chr2.models 
group by model_year
order by model_year

-- 6270 models
select count(distinct model_id) from chr2.models
select count(*) from chr2.models

select * from chr2.style_ids limit 100
select count(*) from chr2.style_ids
--61540 style ids

select * from chr2.models where model  = 'envoy'