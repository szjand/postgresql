﻿select *
from jon.configurations
where model_year = 2020
  and model = 'malibu'

select *
from jon.get_describe_vehicle_by_Style_id
where chrome_style_id = '402208'

select *
from nc.vehicles
where model = 'malibu'
  and model_year = 2019

 2019 malibu: 1G1ZD5ST9KF128116 

 2020 malibu: 1G1ZD5ST7LF141982

select model_year::text as passthru, vin, 
from nc.vehicles
where vin in ('1G1ZD5ST9KF128116','1G1ZD5ST7LF141982')


select *
from nc.vehicles
where model_year = 2021
order by model

-- 20 attributes

(select model_year::text || '-' || right(vin, 6)::text as passthru, vin, 
  null, chrome_Style_id,null,null,null,null,null,null,null,null,
  null,null,null,null,null,null,null,null
from nc.vehicles 
where model = 'malibu'
  and model_year = 2021
limit 1)   
union 
(select model_year::text || '-' || right(vin, 6)::text as passthru, vin, 
  null, chrome_Style_id,null,null,null,null,null,null,null,null,
  null,null,null,null,null,null,null,null
from nc.vehicles 
where model = 'malibu'
  and model_year = 2020
limit 1)
union
(select model_year::text || '-' || right(vin, 6)::text as passthru, vin, 
  null, chrome_Style_id,null,null,null,null,null,null,null,null,
  null,null,null,null,null,null,null,null
from nc.vehicles 
where model = 'malibu'
  and model_year = 2019
limit 1) 
union 
(select model_year::text || '-' || right(vin, 6)::text as passthru, vin, 
  null, chrome_Style_id,null,null,null,null,null,null,null,null,
  null,null,null,null,null,null,null,null
from nc.vehicles 
where model = 'colorado'
  and model_year = 2021
limit 1)  
union
(select model_year::text || '-' || right(vin, 6)::text as passthru, vin, 
  null, chrome_Style_id,null,null,null,null,null,null,null,null,
  null,null,null,null,null,null,null,null
from nc.vehicles 
where model = 'colorado'
  and model_year = 2020
limit 1) 
union
(select model_year::text || '-' || right(vin, 6)::text as passthru, vin, 
  null, chrome_Style_id,null,null,null,null,null,null,null,null,
  null,null,null,null,null,null,null,null
from nc.vehicles 
where model = 'colorado'
  and model_year = 2019
limit 1)  