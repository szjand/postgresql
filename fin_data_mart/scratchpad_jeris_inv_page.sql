﻿08/30/2022
Toyota: (from jeri)
	New Car:  2320
	New Truck:  2325
	Used Car:  2400, 2405, 2407
	Used Truck: 2401, 2406, 2408
	Parts:  2410, 2419, 2411, 2412, 2413, 2415, 2430

	I haven’t been able to get to goals yet (actually, GM and HN are likely outdated, as well), but maybe we can get the inventory numbers first?


new inventory
Car 
    chev  123100
    cad   123101
    buick 123105
    honda 223100
    nissan 223105
truck
    chev  123700
    cad   123701
    buick 123705
    gmc   123706
    honda 223000
    nissan 223700

used inventory
      cars        124000 224000 224005 224006
      cars pack   124001 224002
      truck       124100 224100 224105 224106
      truck pack  124101 224102

parts:
Here are the parts accounts to use for the inventory balances:

GM
124200
124201
124219
124300
124400

HN
224200
224201
224300
224400
224500

select * from fin.dim_fs_org

drop table if exists jeri.inventory_accounts;
create table jeri.inventory_accounts (
  store_code citext not null,
  department citext not null,
  account citext not null primary key,
  factory_account citext not null,
  description citext not null);
comment on table jeri.inventory_accounts is 'subset of accounts to support jeris inventory tracking page in vision';
insert into jeri.inventory_accounts values
  ('RY1','NC','123100','231','Chevrolet New Car'),
  ('RY1','NC','123101','231','Cadillac New Car'),
  ('RY1','NC','123105','231','Buick New Car'),
  ('RY1','NC','123700','237','Chevrolet New Truck'),
  ('RY1','NC','123701','237','Cadillac New Truck'),
  ('RY1','NC','123705','237','Buick New Truck'),
  ('RY1','NC','123706','237','GMC New Truck'),
  ('RY1','UC','124100','241','Used Trucks'),
  ('RY1','UC','124101','241','Used Trucks Pack'),
  ('RY1','UC','124000','240','Used Cars'),
  ('RY1','UC','124001','240','Used Cars Pack'),
  ('RY1','Parts','124200','242','Parts & Acc'),
  ('RY1','Parts','124201','242','Parts Outside Source'),
  ('RY1','Parts','124219','242','Parts Non-GM Non-Stock'),
  ('RY1','Parts','124300','243','Tires'),
  ('RY1','Parts','124400','244','Gas Oil & Grease'),
  ('RY2','NC','223000','237N','Honda New Truck'),
  ('RY2','NC','223100','231N','Honda New Car'),
  ('RY2','NC','223105','231N','Nissan New Car'),
  ('RY2','NC','223700','237N','Nissan New Truck'),
  ('RY2','UC','224000','240','Honda Used Car'),
  ('RY2','UC','224002','240','Used Car Pack'),
  ('RY2','UC','224005','240','Other Used Car'),
  ('RY2','UC','224006','240','Nissan Used Car'),
  ('RY2','NC','224100','241','Honda Used Truck'),
  ('RY2','UC','224102','241','Used Truck Pack'),
  ('RY2','UC','224105','241','Other Used Truck'),
  ('RY2','UC','224106','241','Nissan Used Truck'),
  ('RY2','Parts','224200','242','Honda Parts'),
  ('RY2','Parts','224201','242','Non-OE Parts'),
  ('RY2','Parts','224300','242','Honda Access'),
  ('RY2','Parts','224400','244','Gas Oil & Grease'),
  ('RY2','Parts','224500','242','Nissan & Other Parts');

-- 09/09/22 add toyota 
select * from fin.dim_Account where account in ('2320','2325','2400','2405','2407','2401','2406','2408','2410','2419','2411','2412','2413','2415','2430') order by account
update fin.dim_account set department_code = 'UC', department = 'Used Vehicle' where account = '2408'

select a.account, b.*
from fin.dim_Account a
join fin.dim_fs_account b on a.account = b.gl_account
where account in ('2320','2325','2400','2405','2407','2401','2406','2408','2410','2419','2411','2412','2413','2415','2430')

insert into jeri.inventory_accounts
select store_code, 
  case
    when department_code = 'GN' then 'Parts'
    else department_code
  end as department,
  account, b.gm_account, a.description
from fin.dim_Account a
join fin.dim_fs_account b on a.account = b.gl_account 
where a.account in ('2320','2325','2400','2405','2407','2401','2406','2408','2410','2419','2411','2412','2413','2415','2430');


-- transactions match balances
select *,  beginning_balance + jan_trans + feb_trans + mar_trans + apr_trans + may_trans + jun_trans
from (
  Select c.*, beginning_balance, jan_balance01,feb_balance02,mar_balance03,apr_balance04,
    may_balance05,jun_balance06,jul_balance07
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account
  where a.year = 2019) x
left join (
  select c.account, 
    sum(a.amount) filter (where year_month = 201901) as jan_trans,
    sum(a.amount) filter (where year_month = 201902) as feb_trans,
    sum(a.amount) filter (where year_month = 201903) as mar_trans,
    sum(a.amount) filter (where year_month = 201904) as apr_trans,
    sum(a.amount) filter (where year_month = 201905) as may_trans,
    sum(a.amount) filter (where year_month = 201906) as jun_trans,
    sum(a.amount) filter (where year_month = 201907) as jul_trans
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201901 and 201907
  join fin.dim_account c on a.account_key = c.account_key
  join jeri.inventory_accounts d on c.account = d.account  
  where a.post_status = 'Y'
  group by c.account) y on x.account = y.account
order by x.account

09/09/22
select * from arkona.ext_glpmast where year= 2022 and company_number = 'RY8' and account_number in ('2320','2325','2400','2405','2407','2401','2406','2408','2410','2419','2411','2412','2413','2415','2430')
select * from jeri.inventory_balances

select jeri.update_inventory_balances()
ERROR:  duplicate key value violates unique constraint "inventory_balances_pkey"
DETAIL:  Key (account, the_year, the_month)=(2401, 2022, 1) already exists.

Select c.account,year, beginning_balance::integer, 1, jan_balance01::integer
select a.*
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account
  and c.account = '2401'

select distinct company_number from arkona.ext_glpmast

modified FUNCTION jeri.update_inventory_balances() to be explicit with company_number, Toyota now shows up on Jeris page



	
drop table if exists jeri.inventory_balances;
create table jeri.inventory_balances (
  account citext not null references jeri.inventory_accounts(account),
  the_year integer not null,
  beginning_balance integer not null,
  the_month integer not null,
  balance integer not null,
  primary key (account,the_year,the_month));
-- account balances
insert into jeri.inventory_balances
Select c.account,year, beginning_balance::integer, 1, jan_balance01::integer
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account
union all
Select c.account,year, beginning_balance::integer, 2, feb_balance02::integer
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account
union all
Select c.account,year, beginning_balance::integer, 3, mar_balance03::integer
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account
union all
Select c.account,year, beginning_balance::integer, 4, apr_balance04::integer
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account
union all
Select c.account,year, beginning_balance::integer, 5, may_balance05::integer
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account
union all
Select c.account,year, beginning_balance::integer, 6, jun_balance06::integer
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account
union all
Select c.account,year, beginning_balance::integer, 7, jul_balance07::integer
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account
union all
Select c.account,year, beginning_balance::integer, 8, aug_balance08::integer
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account
union all
Select c.account,year, beginning_balance::integer, 9, sep_balance09::integer
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account
union all
Select c.account,year, beginning_balance::integer, 10, oct_balance10::integer
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account
union all
Select c.account,year, beginning_balance::integer, 11, nov_balance11::integer
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account
union all
Select c.account,year, beginning_balance::integer, 12, dec_balance12::integer
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account;




drop table if exists jeri.inventory_unit_balances;
create table jeri.inventory_unit_balances (
  account citext not null references jeri.inventory_accounts(account),
  the_year integer not null,
  beginning_balance integer not null,
  the_month integer not null,
  units integer not null,
  primary key (account,the_year,the_month));
      
--units balance
insert into jeri.inventory_unit_balances
Select c.account,year, units_beg_balance::integer, 1, jan_units01::integer
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account
union all
Select c.account,year, beginning_balance::integer, 2, feb_units02::integer
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account
union all
Select c.account,year, beginning_balance::integer, 3, mar_units03::integer
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account
union all
Select c.account,year, beginning_balance::integer, 4, apr_units04::integer
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account
union all
Select c.account,year, beginning_balance::integer, 5, may_units05::integer
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account
union all
Select c.account,year, beginning_balance::integer, 6, jun_units06::integer
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account
union all
Select c.account,year, beginning_balance::integer, 7, jul_units07::integer
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account
union all
Select c.account,year, beginning_balance::integer, 8, aug_units08::integer
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account
union all
Select c.account,year, beginning_balance::integer, 9, sep_units09::integer
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account
union all
Select c.account,year, beginning_balance::integer, 10, oct_units10::integer
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account
union all
Select c.account,year, beginning_balance::integer, 11, nov_units11::integer
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account
union all
Select c.account,year, beginning_balance::integer, 12, dec_units12::integer
from arkona.ext_glpmast a
join jeri.inventory_accounts c on a.account_number = c.account;


create or replace function jeri.get_inventory_balances_current_month()
returns table (store_code citext,department citext,balance bigint) as
$BODY$
/*
select * from jeri.get_inventory_balances_current_month();
*/
select store_code, department, sum(balance) 
from jeri.inventory_accounts a
join jeri.inventory_balances b on a.account = b.account
  and b.the_year = (select the_year from dds.dim_date where the_date = current_date)
  and b.the_month = (select month_of_year from dds.dim_Date where the_date = current_date)
group by store_code, department;
$BODY$
language sql;


create or replace function jeri.update_inventory_balances()
returns void as 
$BODY$
  truncate jeri.inventory_balances;
  insert into jeri.inventory_balances
  Select c.account,year, beginning_balance::integer, 1, jan_balance01::integer
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account
  union all
  Select c.account,year, beginning_balance::integer, 2, feb_balance02::integer
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account
  union all
  Select c.account,year, beginning_balance::integer, 3, mar_balance03::integer
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account
  union all
  Select c.account,year, beginning_balance::integer, 4, apr_balance04::integer
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account
  union all
  Select c.account,year, beginning_balance::integer, 5, may_balance05::integer
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account
  union all
  Select c.account,year, beginning_balance::integer, 6, jun_balance06::integer
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account
  union all
  Select c.account,year, beginning_balance::integer, 7, jul_balance07::integer
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account
  union all
  Select c.account,year, beginning_balance::integer, 8, aug_balance08::integer
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account
  union all
  Select c.account,year, beginning_balance::integer, 9, sep_balance09::integer
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account
  union all
  Select c.account,year, beginning_balance::integer, 10, oct_balance10::integer
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account
  union all
  Select c.account,year, beginning_balance::integer, 11, nov_balance11::integer
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account
  union all
  Select c.account,year, beginning_balance::integer, 12, dec_balance12::integer
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account;

  truncate jeri.inventory_unit_balances;
  insert into jeri.inventory_unit_balances
  Select c.account,year, units_beg_balance::integer, 1, jan_units01::integer
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account
  union all
  Select c.account,year, beginning_balance::integer, 2, feb_units02::integer
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account
  union all
  Select c.account,year, beginning_balance::integer, 3, mar_units03::integer
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account
  union all
  Select c.account,year, beginning_balance::integer, 4, apr_units04::integer
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account
  union all
  Select c.account,year, beginning_balance::integer, 5, may_units05::integer
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account
  union all
  Select c.account,year, beginning_balance::integer, 6, jun_units06::integer
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account
  union all
  Select c.account,year, beginning_balance::integer, 7, jul_units07::integer
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account
  union all
  Select c.account,year, beginning_balance::integer, 8, aug_units08::integer
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account
  union all
  Select c.account,year, beginning_balance::integer, 9, sep_units09::integer
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account
  union all
  Select c.account,year, beginning_balance::integer, 10, oct_units10::integer
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account
  union all
  Select c.account,year, beginning_balance::integer, 11, nov_units11::integer
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account
  union all
  Select c.account,year, beginning_balance::integer, 12, dec_units12::integer
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account;  
$BODY$
language sql;



-- 02/06/20
-- data for a graph of monthly parts balance since 201901
select a.the_year, a.the_month, sum(balance)
from jeri.inventory_balances a
join jeri.inventory_accounts b on a.account = b.account
  and b.store_code = 'RY1'
  and b.department = 'parts'
where a.the_year = 2019  
  or (a.the_year = 2020 and a.the_month < 3)
group by a.the_year, a.the_month  
order by a.the_year, a.the_month


select a.the_year, a.the_month, b.store_code, b.department, sum(balance)
from jeri.inventory_balances a
join jeri.inventory_accounts b on a.account = b.account
where a.the_year = 2019  
  or (a.the_year = 2020 and a.the_month < 3)
group by a.the_year, a.the_month, b.store_code, b.department  
order by b.store_code, b.department, a.the_year, a.the_month

select *
from jeri.inventory_balances
limit 100


select 100 * a.the_year + a.the_month as year_month, b.store_code, b.department, sum(balance)
from jeri.inventory_balances a
join jeri.inventory_accounts b on a.account = b.account
where a.the_year = 2019  
  or (a.the_year = 2020 and a.the_month < 3)
group by a.the_year, a.the_month, b.store_code, b.department  
order by b.store_code, b.department, 100 * a.the_year + a.the_month

-- sent this to afton to put graphs on jeri's page
select 100 * a.the_year + a.the_month as year_month, b.store_code, b.department, sum(balance)
from jeri.inventory_balances a
join jeri.inventory_accounts b on a.account = b.account
where 100 * a.the_year + a.the_month between 
  (select year_month from dds.dim_date where the_date = (current_date - INTERVAL '1 year')::date)
  and
  (select year_month from dds.dim_date where the_date = current_date)
group by a.the_year, a.the_month, b.store_code, b.department  
order by store_code, department, 100 * a.the_year + a.the_month


-- 03/24/2022
-- greg wants to know what and where are all these vehicles

select store, sum(inpmast_vehicle_cost)
from (
select
  case
    when left(inpmast_stock_number, 1) = 'G' then 'GM'
    else 'HN'
  end as store, inpmast_stock_number, inpmast_vin, make, model, date_in_invent, inpmast_sale_account, inventory_account, inpmast_vehicle_cost
from arkona.ext_inpmast a
where status = 'I'
  and type_n_u = 'N') a
group by store  

-- difference is ctp 
select sum(balance)
from (
select *
from jeri.inventory_accounts a
join jeri.inventory_balances b on a.account = b.account
  and b.the_year = (select the_year from dds.dim_date where the_date = current_date)
  and b.the_month = (select month_of_year from dds.dim_Date where the_date = current_date)
  where department = 'NC'
  and store_code = 'RY1') x


select
  case
    when left(a.inpmast_stock_number, 1) = 'G' then 'GM'
    else 'HN'
  end as store, a.inpmast_stock_number, a.inpmast_vin, a.make, a.model, a.date_in_invent,
  a.inpmast_vehicle_cost, b.ground_date
from arkona.ext_inpmast a
left join nc.vehicle_acquisitions b on a.inpmast_stock_number = b.stock_number
where a.status = 'I'
  and a.type_n_u = 'N'  
order by b.ground_date  
