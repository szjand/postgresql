﻿
/*
CREATE TABLE fin.ext_budget
(
  store_code citext NOT NULL,
  department citext NOT NULL,
  category citext NOT NULL,
  line_label citext NOT NULL,
  display_seq integer NOT NULL,
  year_month integer NOT NULL,
  budgeted_value integer NOT NULL DEFAULT 0,
  budgeted_percent_of_gross numeric(7,4),
  CONSTRAINT ext_budget_pk PRIMARY KEY (store_code, department, line_label, year_month)
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE fin.ext_budget
  IS 'Raw scrape of spreadsheet Dealership Budget v2';  
*/
-- with_insert_formulas - Copy: sales done, need to do aug - dec for all other depts
insert into fin.ext_budget values
 ('RY2','sales','expense analysis','TOTAL UNITS',1,201609,106,0), 
 ('RY2','sales','expense analysis','GROSS PROFIT',2,201609,188143,0), 
 ('RY2','sales','expense analysis','SALES COMPENSATION',3,201609,30000,15.9453), 
 ('RY2','sales','expense analysis','DELIVERY EXPENSE',4,201609,3500,1.8603), 
 ('RY2','sales','expense analysis','POLICY WORK - VEHICLES',5,201609,2100,1.1162), 
 ('RY2','sales','expense analysis','TOTAL VARIABLE',6,201609,35600,18.9218), 
 ('RY2','sales','expense analysis','SALARIES - OWNERS/EXECUTIVE MANAGERS',7,201609,6500,3.4548), 
 ('RY2','sales','expense analysis','SALARIES - SUPERVISION',8,201609,25000,13.2878), 
 ('RY2','sales','expense analysis','SALARIES - CLERICAL',9,201609,6000,3.1891), 
 ('RY2','sales','expense analysis','OTHER SALARIES & WAGES',10,201609,4500,2.3918), 
 ('RY2','sales','expense analysis','ABSENTEE COMPENSATION',11,201609,342,0.1818), 
 ('RY2','sales','expense analysis','TAXES - PAYROLL',12,201609,6000,3.1891), 
 ('RY2','sales','expense analysis','EMPLOYEE BENEFITS',13,201609,5500,2.9233), 
 ('RY2','sales','expense analysis','RETIREMENT BENEFITS',14,201609,1400,0.7441), 
 ('RY2','sales','expense analysis','TOTAL PERSONNEL',15,201609,55242,29.3617), 
 ('RY2','sales','expense analysis','COMPANY VEHICLE EXPENSE',16,201609,500,0.2658), 
 ('RY2','sales','expense analysis','OFFICE SUPPLIES & EXPENSE',17,201609,750,0.3986), 
 ('RY2','sales','expense analysis','OTHER SUPPLIES',18,201609,2000,1.063), 
 ('RY2','sales','expense analysis','E-COMMERCE ADVERTISING',19,201609,13000,6.9096), 
 ('RY2','sales','expense analysis','ADVERTISING',20,201609,10000,5.3151), 
 ('RY2','sales','expense analysis','ADVERTISING REBATES',21,201609,-8000,-4.2521), 
 ('RY2','sales','expense analysis','CONTRIBUTIONS',22,201609,100,0.0532), 
 ('RY2','sales','expense analysis','POLICY WORK - PARTS & SERVICE',23,201609,0,0), 
 ('RY2','sales','expense analysis','INFORMATION TECH SERVICES',24,201609,12000,6.3781), 
 ('RY2','sales','expense analysis','OUTSIDE SERVICES',25,201609,4000,2.126), 
 ('RY2','sales','expense analysis','TRAVEL & ENTERTAINMENT',26,201609,2500,1.3288), 
 ('RY2','sales','expense analysis','MEMBERSHIP DUES & PUBLICATIONS',27,201609,205,0.109), 
 ('RY2','sales','expense analysis','LEGAL & AUDITING EXPENSE',28,201609,550,0.2923), 
 ('RY2','sales','expense analysis','TELEPHONE',29,201609,1200,0.6378), 
 ('RY2','sales','expense analysis','TRAINING EXPENSE',30,201609,1400,0.7441), 
 ('RY2','sales','expense analysis','INTEREST - FLOORPLAN',31,201609,13000,6.9096), 
 ('RY2','sales','expense analysis','INTEREST - FLOORPLAN CREDIT',32,201609,-12000,-6.3781), 
 ('RY2','sales','expense analysis','INTEREST - NOTES PAYABLE (OTHER)',33,201609,400,0.2126), 
 ('RY2','sales','expense analysis','INSURANCE - INVENTORY',34,201609,1500,0.7973), 
 ('RY2','sales','expense analysis','BAD DEBT EXPENSE',35,201609,17,0.009), 
 ('RY2','sales','expense analysis','FREIGHT',36,201609,600,0.3189), 
 ('RY2','sales','expense analysis','MISCELLANEOUS EXPENSE',37,201609,1200,0.6378), 
 ('RY2','sales','expense analysis','TOTAL SEMI-FIXED EXPENSE',38,201609,44922,23.8765), 
 ('RY2','sales','expense analysis','RENT',39,201609,18725,9.9525), 
 ('RY2','sales','expense analysis','AMORTIZATION - LEASEHOLDS',40,201609,1200,0.6378), 
 ('RY2','sales','expense analysis','REPAIRS - REAL ESTATE',41,201609,1289,0.6851), 
 ('RY2','sales','expense analysis','DEPRECIATION - BUILDINGS & IMPROVEMENTS',42,201609,0,0), 
 ('RY2','sales','expense analysis','TAXES - REAL ESTATE',43,201609,2200,1.1693), 
 ('RY2','sales','expense analysis','INSURANCE - BUILDINGS & IMPROVEMENTS',44,201609,0,0), 
 ('RY2','sales','expense analysis','INTEREST - MORTGAGES',45,201609,0,0), 
 ('RY2','sales','expense analysis','UTILITIES',46,201609,3300,1.754), 
 ('RY2','sales','expense analysis','INSURANCE - OTHER',47,201609,3000,1.5945), 
 ('RY2','sales','expense analysis','TAXES - OTHER',48,201609,0,0), 
 ('RY2','sales','expense analysis','REPAIRS - EQUIPMENT',49,201609,5,0.0027), 
 ('RY2','sales','expense analysis','DEPRECIATION - EQUIPMENT',50,201609,2600,1.3819), 
 ('RY2','sales','expense analysis','EQUIPMENT RENTAL',51,201609,1200,0.6378), 
 ('RY2','sales','expense analysis','TOTAL FIXED EXPENSE',52,201609,33519,17.8157), 
 ('RY2','sales','expense analysis','TOTAL EXPENSES',53,201609,169283,89.9757), 
 ('RY2','sales','expense analysis','NET OPERATING PROFIT',54,201609,18860,10.0243)































































 


















