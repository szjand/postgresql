﻿insert into fin.ext_budget (store_code,department,category,line_label,
  display_seq,year_month,budgeted_value,budgeted_percent_of_gross)
select a.store_code, a.department, a.category, a.line_item, 
  a.display_seq, a.year_month, a.value, 
  case 
    when a.display_seq > 1 then round(100 * (a.value::numeric(13,6)/b.value), 4)
    else 0
  end    
from fin.budget a
left join (
  select year_month, store_code, department, value
  from fin.budget
  where line_item = 'gross profit') b on a.year_month = b.year_month
    and a.store_code = b.store_code
    and a.department = b.department
order by a.store_Code, a.year_month, a.department, a.display_seq    


select a.department, a.line_item, 
  case 
    when a.display_seq > 1 then round(a.value::numeric(13,6)/b.value, 2)
    else 0
  end    
from fin.budget a
left join (
  select year_month, store_code, department, value
  from fin.budget
  where line_item = 'gross profit') b on a.year_month = b.year_month
    and a.store_code = b.store_code
    and a.department = b.department
where a.display_Seq in (6,15,38,52,54)   
  and a.department = 'sales' 
  and a.year_month  = 201606
order by a.store_Code, a.year_month, a.department, a.display_seq    