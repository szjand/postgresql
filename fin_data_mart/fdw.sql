﻿/*
12/31/16
  not sure the schema pg_cartiva is necessary
  ok, in pgadmin file->options->browser-display, selected Foreign Data Wrappers, Foreign Servers,
    User Mappings and Foreign Tables
  now, the the table shows up under foreign_tables in schema from_pg_cartiva
  i like this separation, foreign tables are in a specific schema.
  this is good

4/7/17  
trying to set this up on VM: w10_python against local instance of postgres (9.6)
damn confused about user mapping, noticing the line: -- drop user mapping for postgres server pg_cartiva  
tried to do this without creating it and it failed, so, creatded it

import foreign schema fails:
ERROR:  could not connect to server "pg_cartiva"
DETAIL:  FATAL:  password authentication failed for user "DESKTOP-E46PBHK$"
FATAL:  password authentication failed for user "DESKTOP-E46PBHK$"

ah, found a nice definition for usermapping:
  Create a user mapping, which defines the credentials that a user on the local 
  server will use to make queries against the remote server:

tried
  create user mapping for postgres
    server pg_cartiva
    options (password 'cartiva');
  same failure

tried
  create user mapping for rydell
    server pg_cartiva
    options (password 'cartiva');  
  same failure

tried
  create user mapping for rydell
    server pg_cartiva
    options (password '1rydell$');  
  same failure  
  
hmm, can't find server pg_cartiva in pgadmin, but it shows up as Foreign Server in opject search on local cartiva db
ah, found it:
  cartiva
    Foreign Data Wrappers (2)
      postres_fdw
        Foreign Servers (2)
          pg_cartiva

ok, here is what finally worked:
  drop user mapping for postgres server pg_cartiva; 
  create user mapping for current_user
      server pg_cartiva
      options (user 'rydell', password 'cartiva'); 
bingo      
          
*/  
create extension postgres_fdw;

create server pg_cartiva 
  foreign data wrapper postgres_fdw 
  options (host '10.130.196.173', port '5432', dbname 'cartiva');

-- create user mapping for postgres
--   server pg_cartiva
--   options (password '1rydell$');
-- 
-- create user mapping for rydell
--   server pg_cartiva
--   options (password 'cartiva');  
-- 
-- create user mapping for rydell
--   server pg_cartiva
--   options (password '1rydell$'); 


create user mapping for current_user
    server pg_cartiva
    options (user 'rydell', password 'cartiva')
  
create schema from_pg_cartiva;  

import foreign schema dds limit to (day,dim_date)
  from server pg_cartiva into from_pg_cartiva;

tables show in :
Schemas
  from_pg_cartiva
    Foreign Tables
rather than under tables
    
-- 4/7 bingo
select *
from from_pg_cartiva.dim_date
limit 100

insert into dds.day
select * from from_pg_cartiva.day


import foreign schema arkona limit to (ext_glpdept, ext_glpdept_tmp, ext_glpjrnd, ext_glpjrnd_tmp,
  ext_glpmast, ext_glptrns_tmp, xfm_glpmast, xfm_glptrns)
  from server pg_cartiva into from_pg_cartiva;

insert into arkona.ext_glpdept
select * from from_pg_cartiva.ext_glpdept;
insert into arkona.ext_glpdept_tmp
select * from from_pg_cartiva.ext_glpdept_tmp;
insert into arkona.ext_glpjrnd
select * from from_pg_cartiva.ext_glpjrnd;
insert into arkona.ext_glpjrnd_tmp
select * from from_pg_cartiva.ext_glpjrnd_tmp;
insert into arkona.ext_glpmast
select * from from_pg_cartiva.ext_glpmast;
insert into arkona.ext_glptrns_tmp
select * from from_pg_cartiva.ext_glptrns_tmp;
insert into arkona.xfm_glpmast
select * from from_pg_cartiva.xfm_glpmast;
insert into arkona.xfm_glptrns
select * from from_pg_cartiva.xfm_glptrns;


import foreign schema fin limit to (dim_account, dim_doc_type, dim_gl_description, 
  dim_journal, fact_gl, budget, dim_fs, dim_fs_org, fact_fs, fact_budget_variance)
  from server pg_cartiva into from_pg_cartiva;

insert into fin.dim_account
select * from from_pg_cartiva.dim_account;  

insert into fin.dim_doc_type
select * from from_pg_cartiva.dim_doc_type; 

insert into fin.dim_gl_description
select * from from_pg_cartiva.dim_gl_description; 

insert into fin.dim_journal
select * from from_pg_cartiva.dim_journal; 

insert into fin.fact_gl
select a.* 
from from_pg_cartiva.fact_gl a
inner join from_pg_cartiva.day b on a.date_key = b.datekey
  and b.thedate > '10/31/2016';


-- 4/22/17
-- from pg (10.130.196.173) create a foreign server to ubuntu(172.17.196.73)
-- ok, foreign server already exists:
-- cartiva
--   foreign data wrappers (1)
--     postgres_fdw
--       foreign servers (1)
--         user mappings (1)
--           rydell

-- create a schema
create schema from_ubuntu_test; 

-- get relevant tables
import foreign schema test limit to (ext_bopmast_0228)
from server foreign_server into from_ubuntu_test;

import foreign schema test limit to (ext_bopmast_0306)
from server foreign_server into from_ubuntu_test;
-- tables show in :
-- Schemas
--   from_pg_cartiva
--     Foreign Tables

-- 10/11/17 
-- file_fdw

-- do it on local host first

CREATE EXTENSION file_fdw SCHEMA public;

create server parts foreign data wrapper file_fdw;

create foreign table ignite (
  part_no citext)
server parts options(filename '/home/jon/Desktop/ignite_your_autumn.csv', format 'csv');

-- from http://tjsoftware.co/wp-content/uploads/2017/05/PostgreSQL-FDW-jan-was.pdf
-- and does not work
-- import foreign schema parts from server parts into parts;

select * from ignite
