﻿-- semi-fixed accounts
create temp table accounts as 
select distinct f.department, f.sub_department, f.store, b.line, b.col, b.line_label, d.gl_account, e.description 
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page in (3,4)
  and b.line between 18 and 40
--   and b.col = 5
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store in ('ry1','ry2')
order by f.store, f.department, f.sub_department, b.line, d.gl_account;

create index on accounts(gl_account);
create index on accounts(department);
create index on accounts(sub_department);
create index on accounts(line_label);


select * from accounts 

select *
from arkona.ext_glptrns_tmp a
left join dds.ext_glpcust b on a.gtvnd_ = b.vendor_number
where a.gtvnd_ is not null
  and b.vendor_number is not null 


-- first, get a monthly trend for the accounts 201601 thru 201701
-- this create a nice spreadsheet
select d.line, d.department, d.sub_department, d.line_label,
  sum(case when b.year_month = 201601 then a.amount else 0 end) as "201601",
  sum(case when b.year_month = 201602 then a.amount else 0 end) as "201602",
  sum(case when b.year_month = 201603 then a.amount else 0 end) as "201603",
  sum(case when b.year_month = 201604 then a.amount else 0 end) as "201604",
  sum(case when b.year_month = 201605 then a.amount else 0 end) as "201605",
  sum(case when b.year_month = 201606 then a.amount else 0 end) as "201606",
  sum(case when b.year_month = 201607 then a.amount else 0 end) as "201607",
  sum(case when b.year_month = 201608 then a.amount else 0 end) as "201608",
  sum(case when b.year_month = 201609 then a.amount else 0 end) as "201609",
  sum(case when b.year_month = 201610 then a.amount else 0 end) as "201610",
  sum(case when b.year_month = 201611 then a.amount else 0 end) as "201611",
  sum(case when b.year_month = 201612 then a.amount else 0 end) as "201612",
  sum(case when b.year_month = 201701 then a.amount else 0 end) as "201701"
from fin.fact_gl a 
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month between 201601 and 201701
inner join fin.dim_account c on a.account_key = c.account_key
inner join accounts d on c.account = d.gl_account
group by d.line, d.department, d.sub_department, d.line_label
order by d.department, d.sub_department, d.line

-- lets do it just for department
-- another nice spreadsheet
create temp table dept_semi_fixed as
select d.line, d.department, d.line_label,
  sum(case when b.year_month = 201601 then a.amount else 0 end) as "201601",
  sum(case when b.year_month = 201602 then a.amount else 0 end) as "201602",
  sum(case when b.year_month = 201603 then a.amount else 0 end) as "201603",
  sum(case when b.year_month = 201604 then a.amount else 0 end) as "201604",
  sum(case when b.year_month = 201605 then a.amount else 0 end) as "201605",
  sum(case when b.year_month = 201606 then a.amount else 0 end) as "201606",
  sum(case when b.year_month = 201607 then a.amount else 0 end) as "201607",
  sum(case when b.year_month = 201608 then a.amount else 0 end) as "201608",
  sum(case when b.year_month = 201609 then a.amount else 0 end) as "201609",
  sum(case when b.year_month = 201610 then a.amount else 0 end) as "201610",
  sum(case when b.year_month = 201611 then a.amount else 0 end) as "201611",
  sum(case when b.year_month = 201612 then a.amount else 0 end) as "201612",
  sum(case when b.year_month = 201701 then a.amount else 0 end) as "201701"
from fin.fact_gl a 
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month between 201601 and 201701
inner join fin.dim_account c on a.account_key = c.account_key
inner join accounts d on c.account = d.gl_account
group by d.line, d.department, d.line_label
order by d.department, d.line

-- the subset of accounts for analysis
-- 201601 to 201701 at least 10% increase by department, line
select a.*, "201701" - "201601" as change, round(100*("201701" - "201601")/"201601", 0) as perc_change
from dept_semi_fixed a
where a."201601" <> 0
  and round(100*("201701" - "201601")/"201601", 0) >= 10
  
-- drop table accounts_1;
-- only those lines with an increase of >= 10%
create temp table accounts_1 as
select distinct x.gl_account
-- select *
from accounts x
inner join (
  select a.*, "201701" - "201601" as change, round(100*("201701" - "201601")/"201601", 0) as perc_change
  from dept_semi_fixed a
  where a."201601" <> 0
    and round(100*("201701" - "201601")/"201601", 0) >= 10) y on x.department = y.department and x.line = y.line
where x.store = 'ry1';  
create index on accounts_1(gl_account);

select * from accounts_1    

select a.*, c.*
from fin.fact_gl a 
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month between 201601 and 201701
inner join fin.dim_account c on a.account_key = c.account_key
inner join accounts_1 d on c.account = d.gl_account

-- just department
select x.*, "201701" - "201601" as increase, round(100 * ("201701" - "201601")/"201601", 0) as perc_increase
from (
  select e.department,
    round(sum(case when b.year_month = 201601 then amount else 0 end), 0) as "201601",
    round(sum(case when b.year_month = 201701 then amount else 0 end), 0) as "201701" 
  from fin.fact_gl a 
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month in( 201601, 201701)
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join accounts_1 d on c.account = d.gl_account
  inner join accounts e on d.gl_account = e.gl_account
  group by e.department) x

select x.*, "201701" - "201601" as increase, round(100 * ("201701" - "201601")/"201601", 0) as perc_increase
from (
  select e.department,
    round(sum(case when b.year_month = 201601 then amount else 0 end), 0) as "201601",
    round(sum(case when b.year_month = 201701 then amount else 0 end), 0) as "201701" 
  from fin.fact_gl a 
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month in( 201601, 201701)
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join accounts_1 d on c.account = d.gl_account
  inner join accounts e on d.gl_account = e.gl_account
  group by e.department
  union
  select 'z_total',
    round(sum(case when b.year_month = 201601 then amount else 0 end), 0) as "201601",
    round(sum(case when b.year_month = 201701 then amount else 0 end), 0) as "201701" 
  from fin.fact_gl a 
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month in( 201601, 201701)
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join accounts_1 d on c.account = d.gl_account
  inner join accounts e on d.gl_account = e.gl_account) x  
order by department

-- department/sub__department
select x.*, "201701" - "201601" as increase, round(100 * ("201701" - "201601")/"201601", 0) as perc_increase
from (
  select e.department, e.sub_department,
    sum(case when b.year_month = 201601 then amount else 0 end) as "201601",
    sum(case when b.year_month = 201701 then amount else 0 end) as "201701" 
  from fin.fact_gl a 
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month in( 201601, 201701)
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join accounts_1 d on c.account = d.gl_account
  inner join accounts e on d.gl_account = e.gl_account
  group by e.department, e.sub_department
  union
  select 'z_total', '',
    round(sum(case when b.year_month = 201601 then amount else 0 end), 0) as "201601",
    round(sum(case when b.year_month = 201701 then amount else 0 end), 0) as "201701" 
  from fin.fact_gl a 
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month in( 201601, 201701)
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join accounts_1 d on c.account = d.gl_account
  inner join accounts e on d.gl_account = e.gl_account) x  
order by department, sub_department

select * from accounts  
-- body shop line
select x.*, "201701" - "201601" as increase, 
  case "201601"
    when 0 then 0
    else round(100 * ("201701" - "201601")/"201601", 0) 
  end as perc_increase
from (
  select e.department, e.line, e.line_label,
    round(sum(case when b.year_month = 201601 then amount else 0 end), 0) as "201601",
    round(sum(case when b.year_month = 201701 then amount else 0 end), 0) as "201701" 
  from fin.fact_gl a 
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month in( 201601, 201701)
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join accounts_1 d on c.account = d.gl_account
  inner join accounts e on d.gl_account = e.gl_account
    and e.department = 'body shop'
  group by e.department, e.line, e.line_label) x
order by x.line


-- body shop accounts
select x.*, "201701" - "201601" as increase, 
  case "201601"
    when 0 then 0
    else round(100 * ("201701" - "201601")/"201601", 0) 
  end as perc_increase
from (
  select e.department, e.line, e.line_label, e.gl_account,
    round(sum(case when b.year_month = 201601 then amount else 0 end), 0) as "201601",
    round(sum(case when b.year_month = 201701 then amount else 0 end), 0) as "201701" 
  from fin.fact_gl a 
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month in( 201601, 201701)
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join accounts_1 d on c.account = d.gl_account
  inner join accounts e on d.gl_account = e.gl_account
    and e.department = 'body shop'
    and e.line in (27,32,39)
  group by e.department, e.line, e.line_label, e.gl_account) x
order by x.line, x.gl_account

-- bs account detail
select x.*, "201701" - "201601" as increase, 
  case "201601"
    when 0 then 0
    else round(100 * ("201701" - "201601")/"201601", 0) 
  end as perc_increase
from (
  select e.department, e.line, e.line_label, e.gl_account, a.amount,
    round(sum(case when b.year_month = 201601 then amount else 0 end), 0) as "201601",
    round(sum(case when b.year_month = 201701 then amount else 0 end), 0) as "201701" 
  from fin.fact_gl a 
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month in( 201601, 201701)
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join accounts_1 d on c.account = d.gl_account
  inner join accounts e on d.gl_account = e.gl_account
    and e.department = 'body shop'
    and e.line in (27,32,39)
  group by e.department, e.line, e.line_label, e.gl_account, a.amount) X
order by x.line, x.gl_account  

-- bs acct detail
select x.*, "201701" - "201601" as increase, 
  case "201601"
    when 0 then 0
    else round(100 * ("201701" - "201601")/"201601", 0) 
  end as perc_increase
from (  
  select c.account, d.description,
      round(sum(case when b.year_month = 201601 then amount else 0 end), 0) as "201601",
      round(sum(case when b.year_month = 201701 then amount else 0 end), 0) as "201701" 
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month in( 201601, 201701)
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account = '16905'
  inner join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
  group by c.account, d.description) x
order by increase  


select *
from (
  select gtdate, gtctl_,gtdoc_,gtdesc, gttamt
  from dds.ext_glptrns_201601
  where gtacct = '16905') a
full outer join (
  select gtdate, gtctl_,gtdoc_,gtdesc, gttamt
  from dds.ext_glptrns_201701
  where gtacct = '16905') b
on a.gtctl_

-------------------------------------------------------------------------------------------------------------------------------------------

-- parts line
select x.*, "201701" - "201601" as increase, 
  case "201601"
    when 0 then 0
    else round(100 * ("201701" - "201601")/"201601", 0) 
  end as perc_increase
from (
  select e.department, e.line, e.line_label,
    round(sum(case when b.year_month = 201601 then amount else 0 end), 0) as "201601",
    round(sum(case when b.year_month = 201701 then amount else 0 end), 0) as "201701" 
  from fin.fact_gl a 
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month in( 201601, 201701)
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join accounts_1 d on c.account = d.gl_account
  inner join accounts e on d.gl_account = e.gl_account
    and e.department = 'parts'
  group by e.department, e.line, e.line_label) x
order by x.line


-- parts accounts
select x.*, "201701" - "201601" as increase, 
  case "201601"
    when 0 then 0
    else round(100 * ("201701" - "201601")/"201601", 0) 
  end as perc_increase
from (
  select e.department, e.line, e.line_label, e.gl_account,
    round(sum(case when b.year_month = 201601 then amount else 0 end), 0) as "201601",
    round(sum(case when b.year_month = 201701 then amount else 0 end), 0) as "201701" 
  from fin.fact_gl a 
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month in( 201601, 201701)
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join accounts_1 d on c.account = d.gl_account
  inner join accounts e on d.gl_account = e.gl_account
    and e.department = 'parts'
    and e.line in (18,20,25,26,27,28,39)
  group by e.department, e.line, e.line_label, e.gl_account) x
order by x.line, x.gl_account


-- parts acct detail
select x.*, "201701" - "201601" as increase
from (  
  select c.account, d.description,
      round(sum(case when b.year_month = 201601 then amount else 0 end), 0) as "201601",
      round(sum(case when b.year_month = 201701 then amount else 0 end), 0) as "201701" 
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month in( 201601, 201701)
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account = '17706'
  inner join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
  group by c.account, d.description) x
order by increase  


select *
from (
  select gtdate, gtctl_,gtdoc_,gtdesc, gttamt
  from dds.ext_glptrns_201601
  where gtacct = '16905') a
full outer join (
  select gtdate, gtctl_,gtdoc_,gtdesc, gttamt
  from dds.ext_glptrns_201701
  where gtacct = '16905') b
on a.gtctl_

-------------------------------------------------------------------------------------------------------------------------------------------

-- service mech line
select x.*, "201701" - "201601" as increase, 
  case "201601"
    when 0 then 0
    else round(100 * ("201701" - "201601")/"201601", 0) 
  end as perc_increase
from (
  select e.department, e.line, e.line_label,
    round(sum(case when b.year_month = 201601 then amount else 0 end), 0) as "201601",
    round(sum(case when b.year_month = 201701 then amount else 0 end), 0) as "201701" 
  from fin.fact_gl a 
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month in( 201601, 201701)
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join accounts_1 d on c.account = d.gl_account
  inner join accounts e on d.gl_account = e.gl_account
    and e.department = 'service'
    and e.sub_department = 'mechanical'
  group by e.department, e.line, e.line_label) x
order by x.line


-- mech accounts
select x.*, "201701" - "201601" as increase, 
  case "201601"
    when 0 then 0
    else round(100 * ("201701" - "201601")/"201601", 0) 
  end as perc_increase
from (
  select e.department, e.line, e.line_label, e.gl_account,
    round(sum(case when b.year_month = 201601 then amount else 0 end), 0) as "201601",
    round(sum(case when b.year_month = 201701 then amount else 0 end), 0) as "201701" 
  from fin.fact_gl a 
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month in( 201601, 201701)
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join accounts_1 d on c.account = d.gl_account
  inner join accounts e on d.gl_account = e.gl_account
    and e.department = 'service'
    and e.sub_department = 'mechanical'
    and e.line in (27,32,39)
  group by e.department, e.line, e.line_label, e.gl_account) x
order by x.line, x.gl_account


-- mech acct detail
select x.*, "201701" - "201601" as increase
from (  
  select c.account, d.description,
      round(sum(case when b.year_month = 201601 then amount else 0 end), 0) as "201601",
      round(sum(case when b.year_month = 201701 then amount else 0 end), 0) as "201701" 
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month in( 201601, 201701)
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account = '17704'
  inner join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
  group by c.account, d.description) x
order by increase  


select *
from (
  select gtdate, gtctl_,gtdoc_,gtdesc, gttamt
  from dds.ext_glptrns_201601
  where gtacct = '16905') a
full outer join (
  select gtdate, gtctl_,gtdoc_,gtdesc, gttamt
  from dds.ext_glptrns_201701
  where gtacct = '16905') b
on a.gtctl_

-------------------------------------------------------------------------------------------------------------------------------------------

-- sales new line
select x.*, "201701" - "201601" as increase, 
  case "201601"
    when 0 then 0
    else round(100 * ("201701" - "201601")/"201601", 0) 
  end as perc_increase
from (
  select e.department, e.line, e.line_label,
    round(sum(case when b.year_month = 201601 then amount else 0 end), 0) as "201601",
    round(sum(case when b.year_month = 201701 then amount else 0 end), 0) as "201701" 
  from fin.fact_gl a 
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month in( 201601, 201701)
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join accounts_1 d on c.account = d.gl_account
  inner join accounts e on d.gl_account = e.gl_account
    and e.department = 'sales'
    and e.sub_department = 'new'
  group by e.department, e.line, e.line_label) x
order by x.line


-- sales new accounts
select x.*, "201701" - "201601" as increase, 
  case "201601"
    when 0 then 0
    else round(100 * ("201701" - "201601")/"201601", 0) 
  end as perc_increase
from (
  select e.department, e.line, e.line_label, e.gl_account,
    round(sum(case when b.year_month = 201601 then amount else 0 end), 0) as "201601",
    round(sum(case when b.year_month = 201701 then amount else 0 end), 0) as "201701" 
  from fin.fact_gl a 
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month in( 201601, 201701)
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join accounts_1 d on c.account = d.gl_account
  inner join accounts e on d.gl_account = e.gl_account
    and e.department = 'sales'
    and e.sub_department = 'new'
    and e.line in (20,26,27,33,36,39)
  group by e.department, e.line, e.line_label, e.gl_account) x
order by x.line, x.gl_account


-- sales new acct detail
select x.*, "201701" - "201601" as increase
from (  
  select c.account, d.description,
      round(sum(case when b.year_month = 201601 then amount else 0 end), 0) as "201601",
      round(sum(case when b.year_month = 201701 then amount else 0 end), 0) as "201701" 
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month in( 201601, 201701)
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account = '17701'
  inner join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
  group by c.account, d.description) x
order by increase  


select *
from (
  select gtdate, gtctl_,gtdoc_,gtdesc, gttamt
  from dds.ext_glptrns_201601
  where gtacct = '16905') a
full outer join (
  select gtdate, gtctl_,gtdoc_,gtdesc, gttamt
  from dds.ext_glptrns_201701
  where gtacct = '16905') b
on a.gtctl_

-------------------------------------------------------------------------------------------------------------------------------------------

-- sales used line
select x.*, "201701" - "201601" as increase, 
  case "201601"
    when 0 then 0
    else round(100 * ("201701" - "201601")/"201601", 0) 
  end as perc_increase
from (
  select e.department, e.line, e.line_label,
    round(sum(case when b.year_month = 201601 then amount else 0 end), 0) as "201601",
    round(sum(case when b.year_month = 201701 then amount else 0 end), 0) as "201701" 
  from fin.fact_gl a 
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month in( 201601, 201701)
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join accounts_1 d on c.account = d.gl_account
  inner join accounts e on d.gl_account = e.gl_account
    and e.department = 'sales'
    and e.sub_department = 'used'
  group by e.department, e.line, e.line_label) x
order by x.line


-- sales used accounts
select x.*, "201701" - "201601" as increase, 
  case "201601"
    when 0 then 0
    else round(100 * ("201701" - "201601")/"201601", 0) 
  end as perc_increase
from (
  select e.department, e.line, e.line_label, e.gl_account,
    round(sum(case when b.year_month = 201601 then amount else 0 end), 0) as "201601",
    round(sum(case when b.year_month = 201701 then amount else 0 end), 0) as "201701" 
  from fin.fact_gl a 
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month in( 201601, 201701)
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join accounts_1 d on c.account = d.gl_account
  inner join accounts e on d.gl_account = e.gl_account
    and e.department = 'sales'
    and e.sub_department = 'used'
    and e.line in (20,21,26,27,36,39)
  group by e.department, e.line, e.line_label, e.gl_account) x
order by x.line, x.gl_account


-- sales new acct detail
select x.*, "201701" - "201601" as increase
from (  
  select c.account, d.description,
      round(sum(case when b.year_month = 201601 then amount else 0 end), 0) as "201601",
      round(sum(case when b.year_month = 201701 then amount else 0 end), 0) as "201701" 
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month in( 201601, 201701)
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account = '17702'
  inner join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
  group by c.account, d.description) x
order by increase  


select *
from (
  select gtdate, gtctl_,gtdoc_,gtdesc, gttamt
  from dds.ext_glptrns_201601
  where gtacct = '16905') a
full outer join (
  select gtdate, gtctl_,gtdoc_,gtdesc, gttamt
  from dds.ext_glptrns_201701
  where gtacct = '16905') b
on a.gtctl_