﻿
mechanical
line 19
-- formatted for chart
select store, department, dept, the_month, amount, mr, null, unpl, x_bar, lnpl, url, mr_bar  
from ( -- x
select the_year, store, the_month, department, dept, amount, mr, x_bar, the_count,
	(avg(mr) over (partition by store, department, dept))::integer as mr_bar,
	(x_bar + (avg(mr) over (partition by store, department, dept) * 2.66))::integer as unpl,
	(x_bar - (avg(mr) over (partition by store, department, dept) * 2.66))::integer as lnpl,
	(avg(mr) over (partition by store, department, dept) * 3.268)::integer as url
from (  -- b
	select the_year, year_month, the_month, store, department, dept, amount,
		abs(amount - lag(amount) over (partition by store, department, dept
				order by the_year, the_month, store, department, dept)) as mr,
				round((avg(amount) over (partition by store, department, dept)), 1) as x_bar,
				count(amount) over (partition by store, department, dept) as the_count  -- add the count
	from (	-- a			
		select the_year, year_month, the_month, store, department, dept, sum(amount)as amount			
			from ( 
				select the_year, year_month, the_month, store, department,
					case 
						when sub_department = 'none' then department
						else sub_department
					end as dept, 
					case
						when sub_department = 'none' then dept_line_total
						else sub_dept_line_total
					end as amount
				from fin.xmr_semi_fixed_base
				where year_month between 202001 and 202012
					and line = 19
					and sub_department = 'mechanical'
					and store = 'ry1'
				group by the_year, year_month, store, the_month, line, department,  dept, amount) aa
			group by the_year, year_month, the_month, store, department, dept) a) b) x 
	order by store, department, dept, the_month
