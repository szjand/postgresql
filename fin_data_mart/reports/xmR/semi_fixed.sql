﻿-- semi fixed
select year_month, b.col, sum(a.amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201901 and 202107
  and b.page = 4
  and b.line between 18 and 40
--   and b.col = 13
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store = 'ry1'
group by year_month, b.col

-- store semi fixed via page
-- store_xmr_1.xlsx
select year_month, store 
from fin.page_2 
where year_month between 201901 and 202107
  and store_code = 'ry1'
  and line = 40
order by year_month  
  
-- exclude adv rebates & interest credits
-- store_xmr_2.xlsx
select year_month, sum(a.amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201901 and 202107
  and b.page Between 3 and 4
  and b.line between 18 and 40
  and b.line not in (23,34)
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
  and e.current_row
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store = 'ry1'
group by year_month

/*
8-12 showed the  at the 9:30, consensus was that though interesting, not useful at this level of aggregation
ben c had  a couple of specific requests
what's needed is to drill down to dept/line
greg's helpful suggestion was to, rather than over 200 charts, do the analysis in the code, 
pinpoint those that include a signal
*/

--< Ben's request ------------------------------------------------------------------------------------
-- travel & entertainment, 2017- 2019, 2020 - 2021
-- line 28
-- formatted for the template date column
select b.year_month, (select distinct first_of_month year_month_short from dds.dim_date where year_month = b.year_month) as the_month, sum(a.amount)
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201701 and 202107
  and b.page Between 3 and 4
  and b.line = 28
join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
join fin.dim_account e on d.gl_account = e.account
  and e.current_row
join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store = 'ry1'
group by b.year_month, (select distinct first_of_month from dds.dim_date where year_month = b.year_month)
order by b.year_month

-- sales policy
select the_month, amount, mr, null, unpl, x_bar, lnpl, url, mr_bar  
from (
	select the_month, amount, mr, x_bar, the_count,
		(avg(mr) over ())::integer as mr_bar,
		(x_bar + (avg(mr) over () * 2.66))::integer as unpl,
		case 
			when x_bar - (avg(mr) over () * 2.66) < 0 then 0
			else (x_bar - (avg(mr) over () * 2.66))::integer 
		end as lnpl,
		(avg(mr) over () * 3.268)::integer as url
	from (
		select year_month, the_month, amount,
			abs(amount - lag(amount) over (order by the_month)) as mr,
					round((avg(amount) over ()), 1) as x_bar,
					count(amount) over () as the_count  -- add the count
		from (		
			select b.year_month, (select distinct first_of_month year_month_short from dds.dim_date where year_month = b.year_month) as the_month, sum(a.amount) as amount
			from fin.fact_fs a
			join fin.dim_fs b on a.fs_key = b.fs_key
				and b.year_month between 201701 and 202107
				and b.page = 3
				and b.line = 6
			join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
			join fin.dim_account e on d.gl_account = e.account
				and e.current_row
			join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
				and f.store = 'ry1'
			group by b.year_month, (select distinct first_of_month from dds.dim_date where year_month = b.year_month))aa) bb) cc

--/> Ben's request ------------------------------------------------------------------------------------

-- 2018 - present each line by department as a base table
-- raw data, leave in  lines 23 & 34, again, this is the base table

drop table if exists fin.xmr_semi_fixed_base cascade;
create table fin.xmr_semi_fixed_base as
select aa.the_year, aa.year_month, aa.line::integer as line, aa.line_label, aa.the_month, aa.store, aa.area, aa.department, aa.sub_department, 
  sum(aa.amount) over (partition by aa.year_month, aa.store, aa.line) as p2_store_line_total,
  sum(aa.amount) over (partition by aa.year_month, aa.store, aa.area, aa.line) as p2_area_line_total,
  sum(aa.amount) over (partition by aa.year_month, aa.store, aa.department, aa.line) as dept_line_total,
  sum(aa.amount) over (partition by aa.year_month, aa.store, aa.department, aa.sub_department, aa.line) as sub_dept_line_total
from (	
	select b.year_month, b.line, b.line_label, (select distinct first_of_month from dds.dim_date where year_month = b.year_month) as the_month, 
	  (select distinct the_year from dds.dim_date where year_month = b.year_month) as the_year,
		f.store, f.area, f.department, f.sub_department, sum(a.amount) as amount
	from fin.fact_fs a
	join fin.dim_fs b on a.fs_key = b.fs_key
		and b.year_month between 201801 and 202107
		and b.page Between 3 and 4
		and b.line between 18 and 40
	join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
	join fin.dim_account e on d.gl_account = e.account
		and e.current_row
	join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
	group by b.year_month, b.line, b.line_label, (select distinct first_of_month from dds.dim_date where year_month = b.year_month),
		f.store, f.area, f.department, f.sub_department) aa
order by year_month, store, line, area, department, sub_department;

alter table fin.xmr_semi_fixed_base
add primary key(store, department, sub_department, year_month, line);

select * from fin.xmr_semi_fixed_base limit 100
select * from fin.xmr_semi_fixed_base where sub_department like 'gateway%'

-- this looks good at the store/year (limits/avgs recalc on calendar year: controled by the partition phrase) level
select * from (
select the_year, store, the_month, amount, mr, x_bar, mr_bar, unpl, lnpl, url,
	case
		when amount > unpl
			or amount < lnpl
			or mr > url then true
	  else false
	end as signal
from (
	select the_year, store, the_month, amount, mr, x_bar,
		(avg(mr) over (partition by the_year, store))::integer as mr_bar,
		(x_bar + (avg(mr) over (partition by the_year, store) * 2.66))::integer as unpl,
		case 
			when x_bar - (avg(mr) over (partition by the_year, store) * 2.66) < 0 then 0
			else (x_bar - (avg(mr) over (partition by the_year, store) * 2.66))::integer 
		end as lnpl,
		(avg(mr) over (partition by the_year, store) * 3.268)::integer as url
	from (
		select the_year, store, the_month, amount,
			abs(amount - lag(amount) over (partition by the_year, store order by store, the_year, the_month)) as mr,
			round((avg(amount) over (partition by store, the_year)), 1) as x_bar
		from ( -- this is ok
			select store, the_year, the_month, sum(p2_store_line_total) as amount
			from (
				select store, the_year, the_month,  p2_store_line_total
				from fin.xmr_semi_fixed_base
				where year_month between 201801 and 202107
				group by the_year, store, the_month,  p2_store_line_total) a
			group by the_year, store, the_month) b) c) d
) e where signal			
order by store, the_month	

-- signals at the store level
-- so, i need a version of this query that goes across years
-- remove the_year
-- this is  ok, but, to drill into why the signal at the store level, need line level detail
select * from (
select store, the_month, amount, mr, x_bar, mr_bar, unpl, lnpl, url,
	case
		when amount > unpl
			or amount < lnpl
			or mr > url then true
	  else false
	end as signal
from (
	select store, the_month, amount, mr, x_bar,
		(avg(mr) over (partition by store))::integer as mr_bar,
		(x_bar + (avg(mr) over (partition by store) * 2.66))::integer as unpl,
		case 
			when x_bar - (avg(mr) over (partition by store) * 2.66) < 0 then 0
			else (x_bar - (avg(mr) over (partition by store) * 2.66))::integer 
		end as lnpl,
		(avg(mr) over (partition by store) * 3.268)::integer as url
	from (
		select store, the_month, amount,
			abs(amount - lag(amount) over (partition by store order by store, the_month)) as mr,
			round((avg(amount) over (partition by store)), 1) as x_bar
		from ( -- this is ok
			select store, the_month, sum(p2_store_line_total) as amount
			from (
				select store, the_month,  p2_store_line_total
				from fin.xmr_semi_fixed_base
 				where year_month between 201901 and 201912
				  and store = 'ry2'
				group by store, the_month,  p2_store_line_total) a
			group by store, the_month) b) c) d
) e where signal
-- thinking to start with individual calendar years for limits, like wheeler, with the break between each year
-- add departments/subdepartments first

-- over rows preceding for consecutive values > x_bar

/*
want a base table
ok, what i am running into is that for drill down purposes i expect to be able to cross calendar years
but the path i am on is building data based on calendar years
i want some data that is easy to play with, when i cross years, the mr_bar necessarily resets at the calendar  year
but if i cross calendar years i need mr_bar to be continuous across those years
i think, go ahead with the calendar year/line base data
also thinking will need to be able to roll up, say from sales new & used to just sales

not all sub depts have values for each line, eg line 18 nothing for pdq, detail or car wash
so dop i need a base table, cartesian of lines/sub_departemnts?

select generate_series(18,39)
select store, area, department,
	case 
		when sub_department = 'none' then department
		else sub_department
	end as dept 
from fin.dim_fs_org
where area not in ('none','general')
  and department <> 'national'
*/

-- limits based on store, dept, line
-- narrowed it down, excluded lines & dept based on way less than 43 entries,
-- 	x_bar > 0, and 12 entries for the year
-- -- changes: 
-- 	removed x_bar > 0
-- 	removed setting lnpl to 0 if negative
-- 	cleaned up grouping in table a
select * 
from (
	select *,
		case
			when amount > unpl
				or amount < lnpl
				or case 
						 when mr is not null then mr > url end
					 then true
			else false
		end as signal,
		case when amount > unpl then 'X' end as "> unpl",
		case when amount < lnpl then 'X' end as "< lnpl",
		case when mr is not null and mr > url then 'X' end as "> url",
		case
			when amount > unpl then amount - unpl
			when amount < lnpl then lnpl - amount
			when mr is not null and mr > url then mr - url
	  end as diff
	from (
		select the_year, store, the_month, department, dept, line, line_label, amount, mr, x_bar, the_count,
			(avg(mr) over (partition by the_year, store, department, dept, line ))::integer as mr_bar,
			(x_bar + (avg(mr) over (partition by the_year, store, department, dept, line ) * 2.66))::integer as unpl,
			(x_bar - (avg(mr) over (partition by the_year, store, department, dept, line) * 2.66))::integer  as lnpl,
			(avg(mr) over (partition by the_year, store, department, dept, line) * 3.268)::integer as url
		from (  -- b
			select the_year, year_month, the_month, store, line, line_label, department, dept, amount,
				abs(amount - lag(amount) over (partition by the_year, store, department, dept, line 
						order by the_year, the_month, store, department, dept, line)) as mr,
						round((avg(amount) over (partition by store, the_year, department, dept, line)), 1) as x_bar,
						count(amount) over (partition by store, the_year, department, dept, line) as the_count  -- add the count
			from ( -- a	
				select the_year, year_month, the_month, store, line, max(line_label) as line_label, department,
					case 
						when sub_department = 'none' then department
						else sub_department
					end as dept, 
					case
						when sub_department = 'none' then dept_line_total
						else sub_dept_line_total
					end as amount
				from fin.xmr_semi_fixed_base
				where year_month between 201801 and 202107
-- 				where year_month between 202001 and 202107
				group by the_year, year_month, store, the_month, line, department, dept, amount) a) b) c		  
	where not (the_year = 2018 and department = 'body shop' and line = 21)
	  and not (line = 18 and dept in ('pdq','detail'))
	  and not (line in (21,22,29,30,32,38,39) and dept in ('pdq','detail','car wash'))
	  and not (line in (23,34,33,36,37)))d
	where signal
	  and d.store = 'ry1'
-- 	  and the_year = 2020
	  and the_count = 12
-- 	  and x_bar > 0
order by store, the_year, department, dept, line, the_month
-- 
-- -- now i need to generate data for these with signals
-- 		select the_year, store, the_month, department, dept, line, line_label, amount, mr, x_bar, the_count,
-- 			(avg(mr) over (partition by the_year, store, department, dept, line ))::integer as mr_bar,
-- 			(x_bar + (avg(mr) over (partition by the_year, store, department, dept, line ) * 2.66))::integer as unpl,
-- 			case 
-- 				when x_bar - (avg(mr) over (partition by the_year, store, department, dept, line ) * 2.66) < 0 then 0
-- 				else (x_bar - (avg(mr) over (partition by the_year, store, department, dept, line) * 2.66))::integer 
-- 			end as lnpl,
-- 			(avg(mr) over (partition by the_year, store, department, dept, line) * 3.268)::integer as url
-- 		from (  -- b
-- 			select the_year, year_month, the_month, store, line, line_label, department, dept, amount,
-- 				abs(amount - lag(amount) over (partition by the_year, store, department, dept, line 
-- 						order by the_year, the_month, store, department, dept, line)) as mr,
-- 						round((avg(amount) over (partition by store, the_year, department, dept, line)), 1) as x_bar,
-- 						count(amount) over (partition by store, the_year, department, dept, line) as the_count  -- add the count
-- 			from ( -- a	
-- 				select the_year, year_month, the_month, store, line, max(line_label) as line_label, department,
-- 					case 
-- 						when sub_department = 'none' then department
-- 						else sub_department
-- 					end as dept, 
-- 					case
-- 						when sub_department = 'none' then dept_line_total
-- 						else sub_dept_line_total
-- 					end as amount
-- 				from fin.xmr_semi_fixed_base
-- 				where year_month between 201801 and 202107
-- 				group by the_year, year_month, store, the_month, line, department,
-- 					case 
-- 						when sub_department = 'none' then department
-- 						else sub_department
-- 					end, 
-- 					case
-- 						when sub_department = 'none' then dept_line_total
-- 						else sub_dept_line_total
-- 					end) a) b
-- 


-- -- dept grain, excl 23 & 43 data with a single xmr, removed year from the partitions
-- select * from (
-- select the_year, store, the_month, department, dept, amount, mr, x_bar, the_count,
-- 	(avg(mr) over (partition by store, department, dept))::integer as mr_bar,
-- 	(x_bar + (avg(mr) over (partition by store, department, dept) * 2.66))::integer as unpl,
-- 	case 
-- 		when x_bar - (avg(mr) over (partition by store, department, dept) * 2.66) < 0 then 0
-- 		else (x_bar - (avg(mr) over (partition by store, department, dept) * 2.66))::integer 
-- 	end as lnpl,
-- 	(avg(mr) over (partition by store, department, dept) * 3.268)::integer as url
-- from (  -- b
-- 	select the_year, year_month, the_month, store, department, dept, amount,
-- 		abs(amount - lag(amount) over (partition by store, department, dept
-- 				order by the_year, the_month, store, department, dept)) as mr,
-- 				round((avg(amount) over (partition by store, department, dept)), 1) as x_bar,
-- 				count(amount) over (partition by store, department, dept) as the_count  -- add the count
-- 	from ( -- a	
-- 		select the_year, year_month, the_month, store, department,
-- 			case 
-- 				when sub_department = 'none' then department
-- 				else sub_department
-- 			end as dept, 
-- 			case
-- 				when sub_department = 'none' then dept_line_total
-- 				else sub_dept_line_total
-- 			end as amount
-- 		from fin.xmr_semi_fixed_base
-- 		where year_month between 201801 and 202107
-- 		  and line not in (23,34)
-- 		group by the_year, year_month, store, the_month, line, department,
-- 			case 
-- 				when sub_department = 'none' then department
-- 				else sub_department
-- 			end, 
-- 			case
-- 				when sub_department = 'none' then dept_line_total
-- 				else sub_dept_line_total
-- 			end) a) b
-- ) x 
-- order by store, the_year, the_month, dept


-- dept totals for all 43 months
-- formatted to fit in the chart template
-- single set of limits for all 43 months
-- exclude lines 23, 34
select store, department, dept, the_month, amount, mr, null, unpl, x_bar, lnpl, url, mr_bar  from (
select the_year, store, the_month, department, dept, amount, mr, x_bar, the_count,
	(avg(mr) over (partition by store, department, dept))::integer as mr_bar,
	(x_bar + (avg(mr) over (partition by store, department, dept) * 2.66))::integer as unpl,
	case 
		when x_bar - (avg(mr) over (partition by store, department, dept) * 2.66) < 0 then 0
		else (x_bar - (avg(mr) over (partition by store, department, dept) * 2.66))::integer 
	end as lnpl,
	(avg(mr) over (partition by store, department, dept) * 3.268)::integer as url
from (  -- b
	select the_year, year_month, the_month, store, department, dept, amount,
		abs(amount - lag(amount) over (partition by store, department, dept
				order by the_year, the_month, store, department, dept)) as mr,
				round((avg(amount) over (partition by store, department, dept)), 1) as x_bar,
				count(amount) over (partition by store, department, dept) as the_count  -- add the count
from(			
	select the_year, year_month, the_month, store, department, dept, sum(amount)as amount			
		from ( -- a	
			select the_year, year_month, the_month, store, department,
				case 
					when sub_department = 'none' then department
					else sub_department
				end as dept, 
				case
					when sub_department = 'none' then dept_line_total
					else sub_dept_line_total
				end as amount
			from fin.xmr_semi_fixed_base
			where year_month between 201801 and 202107
				and line not in (23,34)
			group by the_year, year_month, store, the_month, line, department,
				case 
					when sub_department = 'none' then department
					else sub_department
				end, 
				case
					when sub_department = 'none' then dept_line_total
					else sub_dept_line_total
				end) aa
		group by the_year, year_month, the_month, store, department, dept) a) b
) x 
order by store, department, dept, the_month





-- not all sub depts have values for every month
-- nice overview
-- 22 lines making up semi fixed
-- this this works for what to exclude
select store, line, 
  case
    when line = 34 then 'INTEREST CREDITS'
    else max(line_label)
  end as line_label,
  count(*) filter (where department = 'body shop') as bs,
  count(*) filter (where department = 'parts') as parts,
  count(*) filter (where sub_department = 'new') as new,
  count(*) filter (where sub_department = 'used') as used,
  count(*) filter (where sub_department = 'mechanical') as mech,
  count(*) filter (where sub_department = 'pdq') as pdq,
  count(*) filter (where sub_department = 'detail') as detail,
  count(*) filter (where sub_department = 'car wash') as cw,
  count(*) filter (where sub_department = 'gateway wash') as gcw
from fin.xmr_semi_fixed_base
group by store, line
order by store, line


-- the actual count of values per year & line
select store, the_year, department, dept, line, count(*) as the_count
from (
		select the_year, year_month, the_month, store, line, department,
			case 
				when sub_department = 'none' then department
				else sub_department
			end as dept, 
			case
				when sub_department = 'none' then dept_line_total
				else sub_dept_line_total
			end as amount
		from fin.xmr_semi_fixed_base
		where year_month between 201801 and 202007
		group by the_year, year_month, store, the_month, line, department,
			case 
				when sub_department = 'none' then department
				else sub_department
			end, 
			case
				when sub_department = 'none' then dept_line_total
				else sub_dept_line_total
			end) b
 group by store, the_year, department, dept, line			
order by the_count desc, line		

-- this gives me the cartesian for missing values
-- 1 row for each month/store/sub_dept/line
select *   -- 17974
from (
	select a.store, a.area, a.department,
		case 
			when a.sub_department = 'none' then department
			else a.sub_department
		end as dept,
		b.*, c.*
	from fin.dim_fs_org a
	cross join (select generate_series(18,39) as line) b
	cross join (
		select generate_series(201801,201812) as year_month
		union
		select generate_series(201901,201912)
		union
		select generate_series(202001,202012)
		union
		select generate_series(202101,202107))  c
	where area not in ('none','general')
		and department not in('national','finance')) d
order by store, area, department, dept, year_month, line	

this picks up these anomalies
select * from fin.xmr_semi_fixed_base where department = 'none'
	ry2;201803;24;none;none
	ry2;201804;24;none;none
	ry2;202012;26;none;none


select * from (
	select *   -- 17974
	from (
		select a.store, a.area, a.department,
			case 
				when a.sub_department = 'none' then department
				else a.sub_department
			end as dept,
			b.*, c.*
		from fin.dim_fs_org a
		cross join (select generate_series(18,39) as line) b
		cross join (
			select generate_series(201801,201812) as year_month
			union
			select generate_series(201901,201912)
			union
			select generate_series(202001,202012)
			union
			select generate_series(202101,202107))  c
		where area not in ('none','general')
			and department not in ('national','finance')) d) aa
full outer join (				
	select store, year_month, line, department, 
			case 
			when sub_department = 'none' then department
			else sub_department
		end as dept
	from fin.xmr_semi_fixed_base 
	group by store, year_month, line, department, dept) bb on aa.store = bb.store and aa.year_month = bb.year_month and aa.department = bb.department and aa.dept = bb.dept	and aa.line = bb.line
order by aa.store, aa.department, aa.dept, aa.line, bb.store, bb.department, bb.dept, bb.line




