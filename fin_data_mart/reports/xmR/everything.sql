﻿s/*
Ben request: How long would it take your team to put xmr charts on vision of each department 
with the last 12 months with a separate chart of Gross, Personnel, Semi and Fixed expenses

@Greg
 the in previous work i did on generating xmr data for semi-fixed i excluded lines 23 & 34 (advertising rebates & interest credits). 
 since the view we are talking about to get started is totals for semi-fixed, i believe that is a correct decision, what do you think?
Greg
I’d show it both ways

add column for accounts
separate table for gross and expenses
*/

-- this query is from th semi_fixed.sql file
-- think i want this level of detail for everything
-- add column for accounts, include account in the grain
drop table if exists fin.fs_expenses_detail cascade;
create table fin.fs_expenses_detail as
select aa.the_year, aa.year_month, aa.fs_category, aa.line::integer as line, aa.line_label, 
	aa.store, aa.area, aa.department, 
	case
	  when aa.sub_department = 'none' then aa.department
	  else aa.sub_department
	end as sub_dept, account,
--   sum(aa.amount) over (partition by aa.year_month, aa.store, aa.line) as p2_store_line_total,
--   sum(aa.amount) over (partition by aa.year_month, aa.store, aa.area, aa.line) as p2_area_line_total,
--   sum(aa.amount) over (partition by aa.year_month, aa.store, aa.department, aa.line) as dept_line_total,
  sum(aa.amount) over (partition by aa.year_month, aa.store, aa.department, aa.sub_department, aa.line, aa.account) as amount
from (	
	select b.year_month, 
	  case
	    when b.line between 4 and 6 then 'variable'
	    when b.line between 8 and 16 then 'personnel'
	    when b.line between 18 and 39 then 'semi-fixed'
	    when b.line between 41 and 54 then 'fixed'
	  end as fs_category,
	  b.line, b.line_label, b.the_year, --(select distinct first_of_month from dds.dim_date where year_month = b.year_month) as the_month, 
-- 	  (select distinct the_year from dds.dim_date where year_month = b.year_month) as the_year,
		f.store, f.area, f.department, f.sub_department, e.account, sum(a.amount) as amount --, array_agg(distinct e.account) as accounts
	from fin.fact_fs a
	join fin.dim_fs b on a.fs_key = b.fs_key
		and b.year_month between 201201 and 202208
		and b.page Between 3 and 4
		and b.line between 4 and 54
	join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
	join fin.dim_account e on d.gl_account = e.account
	  and (select distinct last_of_month from dds.dim_date where year_month = b.year_month) between e.row_from_date and e.row_thru_date
-- 		and e.current_row
	join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
-- 	where f.store = 'ry1'
	group by b.year_month, b.line, b.line_label, b.the_year,--(select distinct first_of_month from dds.dim_date where year_month = b.year_month),
		f.store, f.area, f.department, f.sub_department, e.account) aa
order by year_month, store, line, area, department, sub_dept;

alter table fin.fs_expenses_detail
add primary key(sub_dept, year_month, line, account);

select * from fin.fs_expenses_detail where store = 'ry1' and year_month = 201208 /*and area = 'variable'*/ order by line, department, sub_dept


select store, line, sum(amount) from fin.fs_expenses_detail where store = 'ry1' and year_month = 201208 group by store, line order by store, line

select line, line_label
from fin.fs_expenses_detail
group by line, line_label
order by line



update fin.fs_expenses_detail
set line_label = 'VEHICLE SALESPERSON COMP/OTH'
where line = 4;
update fin.fs_expenses_detail
set line_label = 'DELIVERY EXPENSE'
where line = 5;
update fin.fs_expenses_detail
set line_label = 'POLICY WORK-VEHICLES'
where line = 6;
update fin.fs_expenses_detail
set line_label = 'SALARIES-OWNERS/EX MGRS'
where line = 8;
update fin.fs_expenses_detail
set line_label = 'SALARIES-SUPERVISION'
where line = 9;
update fin.fs_expenses_detail
set line_label = 'SALARIES-CLERICAL'
where line = 10;
update fin.fs_expenses_detail
set line_label = 'OTHER SALARIES & WAGES'
where line = 11;
update fin.fs_expenses_detail
set line_label = 'EMPLOYEE BENEFITS'
where line = 15;
update fin.fs_expenses_detail
set line_label = 'RETIREMENT BENEFITS'
where line = 16;
update fin.fs_expenses_detail
set line_label = 'COMPANY VEHICLE'
where line = 18;
update fin.fs_expenses_detail
set line_label = 'OFFICE SUPPLIES & EXPENSES'
where line = 19;
update fin.fs_expenses_detail
set line_label = 'OTHER SUPPLIES'
where line = 20;
update fin.fs_expenses_detail
set line_label = 'E-COMMERCE ADVERTISING FEES'
where line = 21;
update fin.fs_expenses_detail
set line_label = 'ADVERTISING REBATES'
where line = 23;
update fin.fs_expenses_detail
set line_label = 'POLICY WORK PARTS & SERVICE'
where line = 25;
update fin.fs_expenses_detail
set line_label = 'INFORMATION TECH'
where line = 26;
update fin.fs_expenses_detail
set line_label = 'OUTSIDE SERVICES OTHER'
where line = 27;
update fin.fs_expenses_detail
set line_label = 'TRAVEL & ENTERTAINMENT'
where line = 28;
update fin.fs_expenses_detail
set line_label = 'MEMBERSHIP DUE & PUBLICATIONS'
where line = 29;
update fin.fs_expenses_detail
set line_label = 'LEGAL & AUDITING EXPENSES'
where line = 30;
update fin.fs_expenses_detail
set line_label = 'TRAINING EXPENSES'
where line = 32;
update fin.fs_expenses_detail
set line_label = 'INTEREST-FLOOR PLAN CREDIT'
where line = 34;
update fin.fs_expenses_detail
set line_label = 'INTEREST-NOTES PAYABLE'
where line = 35;
update fin.fs_expenses_detail
set line_label = 'INSURANCE-INVENTORY'
where line = 36;
update fin.fs_expenses_detail
set line_label = 'BAD DEBTS EXPENSES'
where line = 37;
update fin.fs_expenses_detail
set line_label = 'FREIGHT POSTAGE AND SHIPPING'
where line = 38;
update fin.fs_expenses_detail
set line_label = 'MISCELLANEOUS EXPENSES'
where line = 39;
update fin.fs_expenses_detail
set line_label = 'AMORTIZATION LEASEHOLDS'
where line = 42;
update fin.fs_expenses_detail
set line_label = 'REPAIRS-REAL ESTATE'
where line = 43;
update fin.fs_expenses_detail
set line_label = 'DEPREC BLDS & IMPROVEMENTS'
where line = 44;
update fin.fs_expenses_detail
set line_label = 'TAXES-REAL ESTATE'
where line = 45;
update fin.fs_expenses_detail
set line_label = 'INSURANCE BLDGS & IMPROVEMENTS'
where line = 46;
update fin.fs_expenses_detail
set line_label = 'INTEREST MORTGAGE'
where line = 47;
update fin.fs_expenses_detail
set line_label = 'INSURANCE-OTHER'
where line = 50;
update fin.fs_expenses_detail
set line_label = 'REPAIRS EQUIPMENT'
where line = 52;
update fin.fs_expenses_detail
set line_label = 'DEPRECIATION-EQUIPMENT'
where line = 53;
update fin.fs_expenses_detail
set line_label = 'EQUIPMENT RENTAL'
where line = 54;

select store, line, sum(amount)
from fin.fs_expenses_details
where year_month = 201601
group by store, line
order by store, line

select year_month, fs_category, sum(amount)
from fin.fs_expenses_detail
where store = 'ry1'
group by year_month, fs_category
order by year_month, fs_category

-- and the expenses query for afton
select year_month, sub_dept, amount, mr, x_bar,
	(avg(mr) over (partition by sub_dept))::integer as mr_bar,
	(x_bar + (avg(mr) over (partition by sub_dept) * 2.66))::integer as unpl,
	(x_bar - (avg(mr) over (partition by sub_dept) * 2.66))::integer as lnpl,
	(avg(mr) over (partition by sub_dept) * 3.268)::integer as url
from (  -- b
	select year_month, sub_dept, amount,
		abs(amount - lag(amount) over (partition by sub_dept order by year_month, sub_dept)) as mr,
		round((avg(amount) over (partition by sub_dept)), 1) as x_bar
	from (-- a		
		select year_month, sub_dept, sum(amount) as amount
		from fin.fs_expenses_detail
		where store = 'ry1'
		  and fs_category = 'fixed'
		  and year_month between 202109 and 202208
		group by year_month, sub_dept) a) b
order by sub_dept, year_month


select * from fin.fs_expenses_detail limit 10
select distinct fs_category from fin.fs_expenses_detail
select distinct department, sub_dept from fin.fs_expenses_detail
-- there are no expenses for f/i


-- gross

-- uc & fixed gross
select 
  sum(-amount) filter (where b.page = 16 and b.line between 1 and 12) as uc,
  sum(-amount) filter (where b.page = 16 and b.line between 21 and 33) as mech,
  sum(-amount) filter (where b.page = 16 and b.line between 35 and 42) as bs,
  sum(-amount) filter (where b.page = 16 and b.line between 45 and 59) as parts 
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
  and c.store = 'ry1'
where b.year_month = 202208
--   and b.page = 16 


-- nc gross
select sum(-a.amount)
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
  and c.store = 'ry1'
where b.year_month = 202208
  and b.page between 5 and 15

select store, page, col, sum(-a.amount)
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
  and c.store = 'ry1'
where b.year_month = 202208
  and b.page between 5 and 15  
group by store, col, page
order by store, page, col

-- use dim_Account.typical_balance to aggregate and separate sales accounts and cogs accounts
-- 1426006 shoud be credit
-- select page, array_agg(distinct line), col, account_type, typical_balance, array_agg(distinct account) from (
drop table if exists fin.fs_gross_detail cascade;
create table fin.fs_gross_detail as
	select b.the_year, b.year_month, 
	  b.page, b.line, b.line_label, f.store, 
		case
		  when f.store = 'RY2' and b.page = 7 and area in ('general','fixed') then 'variable'
		  else f.area
		end as area, 
		case
		  when f.store = 'RY2' and b.page = 7 and area in ('general','fixed') then 'sales'		
		  else f.department
		end as department,
		case
		  when f.store = 'RY2' and b.page = 7 and area in ('general','fixed') then 'new'
		  else f.sub_department
		end as sub_dept, 
		sum(-a.amount) filter (where b.col in (1,2,9)) as sales,
		sum(a.amount) filter (where b.col in (3,11)) as cogs,
		sum(-a.amount) as gross,
		array_agg(distinct e.account) as accounts		 
	from fin.fact_fs a
	join fin.dim_fs b on a.fs_key = b.fs_key
		and b.year_month between 201201 and 202208
		and ((b.page between 5 and 15 or b.page in (16, 17)) or (b.page = 4 and b.line = 59))
		and 
		  case
		    when b.page = 17 then b.line between 1 and 19
		    else true
		  end
	join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
	join fin.dim_account e on d.gl_account = e.account
	  and (select distinct last_of_month from dds.dim_date where year_month = b.year_month) between e.row_from_date and e.row_thru_date
	join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
	group by b.the_year, b.year_month, b.page, b.line, b.line_label, --b.col, e.account_type, e.account, --(select distinct first_of_month from dds.dim_date where year_month = b.year_month),
		f.store, 
		case
		  when f.store = 'RY2' and b.page = 7 and area in ('general','fixed') then 'variable'
		  else f.area
		end,	
		case
		  when f.store = 'RY2' and b.page = 7 and area in ('general','fixed') then 'sales'		
		  else f.department
		end,
		case
		  when f.store = 'RY2' and b.page = 7 and area in ('general','fixed') then 'new'
		  else f.sub_department
		end					
order by f.store, b.page, b.line;

alter table fin.fs_gross_detail
add primary key(store, department, sub_dept, year_month, page, line);

-- this now handles matching line 16 to fs
with
	totals as (
	select page, line, sum(sales) as sales, sum(cogs) as cogs, sum(gross) as gross 
	from fin.fs_gross_detail 
	where store = 'ry2'
	and year_month = 202208	
	group by page, line) 
select page, 
  case
    when page = 16 then sum(sales) + (select sum(sales) from totals where page in (7,14)) + (select sum(sales) from totals where page = 17)
    else sum(sales) 
  end as sales,
  case
    when page = 16 then sum(cogs) + (select sum(cogs) from totals where page in (7,14)) + (select sum(cogs) from totals where page = 17)
    else sum(cogs) 
  end as cogs,
  case
    when page = 16 then sum(gross) + (select sum(gross) from totals where page in (7,14)) + (select sum(gross) from totals where page = 17)
    else sum(gross) 
  end as gross  
from fin.fs_gross_detail 
where store = 'ry2'
and year_month = 202208	
group by page	
order by page


with
	totals as (
	select page,line, sum(sales) as sales, sum(cogs) as cogs, sum(gross) as gross 
	from fin.fs_gross_detail 
	where store = 'ry1'
	and year_month = 202208
	group by page, line) 
select page, 
  case
    when page = 16 then sum(sales) + (select sum(sales) from totals where page in (5,8,9,10)) + (select sum(sales) from totals where page = 17)
    else sum(sales) 
  end as sales,
  case
    when page = 16 then sum(cogs) + (select sum(cogs) from totals where page in (5,8,9,10)) + (select sum(cogs) from totals where page = 17)
    else sum(cogs) 
  end as cogs,
  case
    when page = 16 then sum(gross) + (select sum(gross) from totals where page in (5,8,9,10)) + (select sum(gross) from totals where page = 17)
    else sum(gross) 
  end as gross  
from fin.fs_gross_detail 
where store = 'ry1'
and year_month = 202208
group by page	
order by page

it looks like i am not handling parts split correctly
so i added page 4 to detail query
i think the addition of the page 4 data is all i basically need, now it is just a matter of picking and choosing to get the correct gross
for the departments
tomorrow



select * from fin.fs_gross_Detail where year_month = 202208 and store = 'ry1' and line in (49,59) order by page, department

49 fs: 135,776, 94,942, 40,834
49: 47111,32663,14448
select page, line, line_label, sum(sales) as sales, sum(cogs) as cogs, sum(gross) as gross
from fin.fs_gross_Detail
where year_month = 202208
  and department = 'parts'
  and store = 'ry1'
group by page, line, line_label
order by page, line, line_label

49: 88665,62279,26386
select page, line, line_label, sum(sales) as sales, sum(cogs) as cogs, sum(gross) as gross
from fin.fs_gross_Detail
where year_month = 202208
  and department = 'body shop'
  and store = 'ry1'
group by page, line, line_label
order by page, line, line_label

!! 59: 34934,,34934
select page, line, line_label, sum(sales) as sales, sum(cogs) as cogs, sum(gross) as gross
from fin.fs_gross_Detail
where year_month = 202208
  and department = 'service'
  and store = 'ry1'
group by page, line, line_label
order by page, line, line_label  

