﻿
-- ez view
select store, the_year, year_month, line, max(line_label) as line_label,
	sum(amount) as the_store,
  sum(amount) filter (where department = 'sales') as variable,
  sum(amount) filter (where department in ('sales','service','body shop','parts')) as fixed,
  sum(amount) filter (where department = 'sales' and dept = 'new') as new,
  sum(amount) filter (where department = 'sales' and dept = 'used') as used,
  sum(amount) filter (where department = 'service') as mechanical,
	sum(amount) filter (where department = 'body shop') as bs,
	sum(amount) filter (where department = 'parts') as parts,
	sum(amount) filter (where department = 'service' and dept = 'mechanical') as main_shop,
	sum(amount) filter (where department = 'service' and dept = 'car wash') as car_wash,
	sum(amount) filter (where department = 'service' and dept = 'detail') as detail,
	sum(amount) filter (where department = 'service' and dept = 'pdq') as pdq
from (
	select store, the_year, year_month, department, line, max(line_label) as line_label,
		case 
			when sub_department = 'none' then department
			else sub_department
		end as dept, 
		case
			when sub_department = 'none' then dept_line_total
			else sub_dept_line_total
		end as amount
	from fin.xmr_semi_fixed_base
	where year_month between 201801 and 202107
	group by store, the_year, year_month, line, department,
		case 
			when sub_department = 'none' then department
			else sub_department
		end, 
		case
			when sub_department = 'none' then dept_line_total
			else sub_dept_line_total
		end) a
group by a.store, the_year, year_month, line
-- order by a.store, the_year, year_month, line		
union
select store, the_year, year_month, 40, 'TOTAL',
	sum(amount) as the_store,
  sum(amount) filter (where department = 'sales') as variable,
  sum(amount) filter (where department in ('service','body shop','parts')) as fixed,
  sum(amount) filter (where department = 'sales' and dept = 'new') as new,
  sum(amount) filter (where department = 'sales' and dept = 'used') as used,
  sum(amount) filter (where department = 'service') as mechanical,
	sum(amount) filter (where department = 'body shop') as bs,
	sum(amount) filter (where department = 'parts') as parts,
	sum(amount) filter (where department = 'service' and dept = 'mechanical') as main_shop,
	sum(amount) filter (where department = 'service' and dept = 'pdq') as pdq,
	sum(amount) filter (where department = 'service' and dept = 'car wash') as car_wash,
	sum(amount) filter (where department = 'service' and dept = 'detail') as detail
from (
	select store, the_year, year_month, department, line, max(line_label) as line_label,
		case 
			when sub_department = 'none' then department
			else sub_department
		end as dept, 
		case
			when sub_department = 'none' then dept_line_total
			else sub_dept_line_total
		end as amount
	from fin.xmr_semi_fixed_base
	where year_month between 201801 and 202107
	group by store, the_year, year_month, line, department,
		case 
			when sub_department = 'none' then department
			else sub_department
		end, 
		case
			when sub_department = 'none' then dept_line_total
			else sub_dept_line_total
		end) a
group by a.store, the_year, year_month
order by store, the_year, year_month, line