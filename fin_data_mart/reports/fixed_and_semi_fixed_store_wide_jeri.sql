﻿-- semi-fixed and fixed accounts
-- ry1 only
-- store wide, don't need store/dept/sub_dept
-- use page 3 line labels
drop table lines_labels;
create temp table lines_labels as
-- select distinct b.line, b.line_label
-- from fin.fact_fs a
-- inner join fin.dim_fs b on a.fs_key = b.fs_key
--   and b.year_month = 201701
--   and b.page = 2
--   and b.line between 18 and 55
-- inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
--   and f.store = 'ry1'
-- order by b.line
select distinct line, line_label
from fin.dim_fs 
where year_month = 201701
  and page = 2
  and line between 18 and 55
order by line  
  
-- thinking should possibly do a different account list
-- for each month
create temp table accounts as 
select distinct b.line, d.gl_account, e.description 
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page in (3,4)
  and b.line between 18 and 55
--   and b.col = 5
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store = 'ry1'
order by b.line, d.gl_account;

create unique index on lines_labels(line);
create index on accounts(gl_account);
create index on accounts(line);
create unique index on accounts(gl_account,line)

-- first, get a monthly trend for the accounts 201601 thru 201701
-- this create a nice spreadsheet
select d.line, d.line_label,
  sum(case when b.year_month = 201601 then a.amount else 0 end) as "201601",
  sum(case when b.year_month = 201602 then a.amount else 0 end) as "201602",
  sum(case when b.year_month = 201603 then a.amount else 0 end) as "201603",
  sum(case when b.year_month = 201604 then a.amount else 0 end) as "201604",
  sum(case when b.year_month = 201605 then a.amount else 0 end) as "201605",
  sum(case when b.year_month = 201606 then a.amount else 0 end) as "201606",
  sum(case when b.year_month = 201607 then a.amount else 0 end) as "201607",
  sum(case when b.year_month = 201608 then a.amount else 0 end) as "201608",
  sum(case when b.year_month = 201609 then a.amount else 0 end) as "201609",
  sum(case when b.year_month = 201610 then a.amount else 0 end) as "201610",
  sum(case when b.year_month = 201611 then a.amount else 0 end) as "201611",
  sum(case when b.year_month = 201612 then a.amount else 0 end) as "201612",
  sum(case when b.year_month = 201701 then a.amount else 0 end) as "201701"
from fin.fact_gl a 
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month between 201601 and 201701
inner join fin.dim_account c on a.account_key = c.account_key
inner join accounts cc on c.account = cc.gl_account
inner join lines_labels d on cc.line = d.line
where a.post_status = 'Y'
group by d.line, d.line_label
order by d.line


-- lets start out with jeri's vendor report
select * from accounts where gl_account = '16901'
select * from accounts where line = 27

drop table line_27;
create temp table line_27 as
select -- a.control, c.account, 
  coalesce(e.search_name, 
    case 
      when aa.description like 'PAY%' then 'PAYROLL' 
      when aa.description like 'INVENTORY%' then 'INVENTORY'
      when aa.description like 'LYNX%' then 'LYNX'
      else aa.description 
    end), string_agg(distinct control,',') as control, string_agg(distinct c.account, ',') as accounts,
  max(case when e.search_name is not null then 1 else 2 end) as sort,
  sum(case when b.year_month = 201601 then a.amount else 0 end) as "201601",
  sum(case when b.year_month = 201602 then a.amount else 0 end) as "201602",
  sum(case when b.year_month = 201603 then a.amount else 0 end) as "201603",
  sum(case when b.year_month = 201604 then a.amount else 0 end) as "201604",
  sum(case when b.year_month = 201605 then a.amount else 0 end) as "201605",
  sum(case when b.year_month = 201606 then a.amount else 0 end) as "201606",
  sum(case when b.year_month = 201607 then a.amount else 0 end) as "201607",
  sum(case when b.year_month = 201608 then a.amount else 0 end) as "201608",
  sum(case when b.year_month = 201609 then a.amount else 0 end) as "201609",
  sum(case when b.year_month = 201610 then a.amount else 0 end) as "201610",
  sum(case when b.year_month = 201611 then a.amount else 0 end) as "201611",
  sum(case when b.year_month = 201612 then a.amount else 0 end) as "201612",
  sum(case when b.year_month = 201701 then a.amount else 0 end) as "201701"
--   sum(a.amount) as Total
from fin.fact_gl a 
inner join fin.dim_gl_description aa on a.gl_description_key = aa.gl_description_key
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month between 201601 and 201701
inner join fin.dim_account c on a.account_key = c.account_key
inner join accounts cc on c.account = cc.gl_account
inner join lines_labels d on cc.line = d.line
  and d.line = 27
left join dds.ext_glpcust e on a.control = e.vendor_number  
where a.post_status = 'Y'
--   and a.control = '1260'
group by 
coalesce(e.search_name, 
    case 
      when aa.description like 'PAY%' then 'PAYROLL' 
      when aa.description like 'INVENTORY%' then 'INVENTORY'
      when aa.description like 'LYNX%' then 'LYNX'
      else aa.description 
    end)
order by sort,   
coalesce(e.search_name, 
    case 
      when aa.description like 'PAY%' then 'PAYROLL' 
      when aa.description like 'INVENTORY%' then 'INVENTORY'
      when aa.description like 'LYNX%' then 'LYNX'
      else aa.description 
    end);


-- if no vendor number, account
create temp table line_27_compact as
select -- a.control, c.account, 
  coalesce(e.search_name, c.account),
  max(case when e.search_name is not null then 1 else 2 end) as sort,
  sum(case when b.year_month = 201601 then a.amount else 0 end) as "201601",
  sum(case when b.year_month = 201602 then a.amount else 0 end) as "201602",
  sum(case when b.year_month = 201603 then a.amount else 0 end) as "201603",
  sum(case when b.year_month = 201604 then a.amount else 0 end) as "201604",
  sum(case when b.year_month = 201605 then a.amount else 0 end) as "201605",
  sum(case when b.year_month = 201606 then a.amount else 0 end) as "201606",
  sum(case when b.year_month = 201607 then a.amount else 0 end) as "201607",
  sum(case when b.year_month = 201608 then a.amount else 0 end) as "201608",
  sum(case when b.year_month = 201609 then a.amount else 0 end) as "201609",
  sum(case when b.year_month = 201610 then a.amount else 0 end) as "201610",
  sum(case when b.year_month = 201611 then a.amount else 0 end) as "201611",
  sum(case when b.year_month = 201612 then a.amount else 0 end) as "201612",
  sum(case when b.year_month = 201701 then a.amount else 0 end) as "201701"
--   sum(a.amount) as Total
from fin.fact_gl a 
inner join fin.dim_gl_description aa on a.gl_description_key = aa.gl_description_key
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month between 201601 and 201701
inner join fin.dim_account c on a.account_key = c.account_key
inner join accounts cc on c.account = cc.gl_account
inner join lines_labels d on cc.line = d.line
  and d.line = 27
left join dds.ext_glpcust e on a.control = e.vendor_number  
where a.post_status = 'Y'
--   and a.control = '1260'
group by coalesce(e.search_name, c.account)
order by sort, coalesce(e.search_name, c.account)

grouping:'PAY%' then 'PAYROLL', 'INVENTORY%' then 'INVENTORY', 'LYNX%' then 'LYNX'

select * from line_27

select * from line_27_compact

select * from accounts where line = 27 order by gl_account

select * from fin.dim_account where account in ('16901','16927','16924')



select -- a.control, c.account, 
  coalesce(e.search_name, 
    case 
      when aa.description like 'PAY%' then 'PAYROLL' 
      when aa.description like 'INVENTORY%' then 'INVENTORY'
      when aa.description like 'LYNX%' then 'LYNX'
      else aa.description 
    end), string_agg(distinct control,',') as control, string_agg(distinct c.account, ',') as accounts,
  max(case when e.search_name is not null then 1 else 2 end) as sort,
  sum(case when b.year_month = 201601 then a.amount else 0 end) as "201601",
  sum(case when b.year_month = 201602 then a.amount else 0 end) as "201602",
  sum(case when b.year_month = 201603 then a.amount else 0 end) as "201603",
  sum(case when b.year_month = 201604 then a.amount else 0 end) as "201604",
  sum(case when b.year_month = 201605 then a.amount else 0 end) as "201605",
  sum(case when b.year_month = 201606 then a.amount else 0 end) as "201606",
  sum(case when b.year_month = 201607 then a.amount else 0 end) as "201607",
  sum(case when b.year_month = 201608 then a.amount else 0 end) as "201608",
  sum(case when b.year_month = 201609 then a.amount else 0 end) as "201609",
  sum(case when b.year_month = 201610 then a.amount else 0 end) as "201610",
  sum(case when b.year_month = 201611 then a.amount else 0 end) as "201611",
  sum(case when b.year_month = 201612 then a.amount else 0 end) as "201612",
  sum(case when b.year_month = 201701 then a.amount else 0 end) as "201701"
--   sum(a.amount) as Total
from fin.fact_gl a 
inner join fin.dim_gl_description aa on a.gl_description_key = aa.gl_description_key
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month between 201601 and 201701
inner join fin.dim_account c on a.account_key = c.account_key
inner join accounts cc on c.account = cc.gl_account
  and cc.gl_account = '16901'
inner join lines_labels d on cc.line = d.line
  and d.line = 27
left join dds.ext_glpcust e on a.control = e.vendor_number  
where a.post_status = 'Y'
--   and a.control = '1260'
group by 
coalesce(e.search_name, 
    case 
      when aa.description like 'PAY%' then 'PAYROLL' 
      when aa.description like 'INVENTORY%' then 'INVENTORY'
      when aa.description like 'LYNX%' then 'LYNX'
      else aa.description 
    end)
order by sort,   
coalesce(e.search_name, 
    case 
      when aa.description like 'PAY%' then 'PAYROLL' 
      when aa.description like 'INVENTORY%' then 'INVENTORY'
      when aa.description like 'LYNX%' then 'LYNX'
      else aa.description 
    end);


select a.*, d.*
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201701
inner join fin.dim_Account c on  a.account_key = c.account_key
  and c.account = '16901'
inner join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key  

-----------------------------------------------------------------------------------------------------------
-- 2/15/17 ------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------

select d.line,-- a.control, c.account, 
  coalesce(e.search_name, c.account || ' - ' || c.description),
  max(case when e.search_name is not null then 1 else 2 end) as sort,
  round(sum(case when b.year_month = 201601 then a.amount else 0 end), 0) as "201601",
  round(sum(case when b.year_month = 201602 then a.amount else 0 end), 0) as "201602",
  round(sum(case when b.year_month = 201603 then a.amount else 0 end), 0) as "201603",
  round(sum(case when b.year_month = 201604 then a.amount else 0 end), 0) as "201604",
  round(sum(case when b.year_month = 201605 then a.amount else 0 end), 0) as "201605",
  round(sum(case when b.year_month = 201606 then a.amount else 0 end), 0) as "201606",
  round(sum(case when b.year_month = 201607 then a.amount else 0 end), 0) as "201607",
  round(sum(case when b.year_month = 201608 then a.amount else 0 end), 0) as "201608",
  round(sum(case when b.year_month = 201609 then a.amount else 0 end), 0) as "201609",
  round(sum(case when b.year_month = 201610 then a.amount else 0 end), 0) as "201610",
  round(sum(case when b.year_month = 201611 then a.amount else 0 end), 0) as "201611",
  round(sum(case when b.year_month = 201612 then a.amount else 0 end), 0) as "201612",
  round(sum(case when b.year_month between 201601 and 201612 then a.amount else 0 end), 0) as "2016 Total",
  round(sum(case when b.year_month between 201601 and 201612 then a.amount else 0 end)/12, 0) as "2016 Avg per Month",
  round(sum(case when b.year_month = 201701 then a.amount else 0 end), 0) as "201701"
from fin.fact_gl a 
inner join fin.dim_gl_description aa on a.gl_description_key = aa.gl_description_key
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month between 201601 and 201701
inner join fin.dim_account c on a.account_key = c.account_key
inner join accounts cc on c.account = cc.gl_account
inner join lines_labels d on cc.line = d.line
  and d.line between 18 and 54
left join dds.ext_glpcust e on a.control = e.vendor_number  
where a.post_status = 'Y'
group by d.line, coalesce(e.search_name, c.account || ' - ' || c.description)
order by line, sort, coalesce(e.search_name, c.account || ' - ' || c.description)


select * from accounts where gl_account = '16901'
select * from accounts where line = 18
select * from lines_labels

select 'Line ' || a.line::integer::citext || ': ' || a.line_label  as col_1, line as seq_1, 1 as seq_2
from lines_labels a 
-- where line = 18
union
select gl_account || ': ' || description, line as seq_1, 2 as seq_2
from accounts 
-- where line = 18
order by seq_1, seq_2, col_1


select line::integer::citext, line_label from lines_labels order by line
