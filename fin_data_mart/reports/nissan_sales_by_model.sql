﻿drop table if exists accounts_lines cascade;
create temp table accounts_lines as
-- from sales_consultant_payroll.py AccountsRoutes
-- accounts/page/line/store/year
select distinct a.fxmpge as page, a.fxmlne::integer as line, trim(b.g_l_acct_number) as gl_account
from arkona.ext_eisglobal_sypffxmst a
inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
where a.fxmcyy between 2012 and 2017
  and trim(a.fxmcde) = 'GM'
  and coalesce(b.consolidation_grp, '1') = '2'
  and b.factory_code = 'GM'
  and trim(b.factory_account) <> '331A'
  and b.company_number = 'RY1'
  and b.g_l_acct_number <> ''
  and a.fxmpge = 14;
create unique index on accounts_lines(the_year, store_code, gl_account);   
create index on accounts_lines(the_year);
create index on accounts_lines(gl_account);



select line  as fin_stmt_line, model, 
  coalesce(sum(unit_count) filter (where year_month = 201601), 0) as "201601",
  coalesce(sum(unit_count) filter (where year_month = 201602), 0) as "201602",
  coalesce(sum(unit_count) filter (where year_month = 201603), 0) as "201603",
  coalesce(sum(unit_count) filter (where year_month = 201604), 0) as "201604",
  coalesce(sum(unit_count) filter (where year_month = 201605), 0) as "201605",
  coalesce(sum(unit_count) filter (where year_month = 201606), 0) as "201606",
  coalesce(sum(unit_count) filter (where year_month = 201607), 0) as "201607",
  coalesce(sum(unit_count) filter (where year_month = 201608), 0) as "201608",
  coalesce(sum(unit_count) filter (where year_month = 201609), 0) as "201609",
  coalesce(sum(unit_count) filter (where year_month = 201610), 0) as "201610",
  coalesce(sum(unit_count) filter (where year_month = 201611), 0) as "201611",
  coalesce(sum(unit_count) filter (where year_month = 201612), 0) as "201612",
  coalesce(sum(unit_count) filter (where year_month = 201701), 0) as "201701",
  coalesce(sum(unit_count) filter (where year_month = 201702), 0) as "201702",
  coalesce(sum(unit_count) filter (where year_month = 201703), 0) as "201703",
  coalesce(sum(unit_count) filter (where year_month = 201704), 0) as "201704",
  coalesce(sum(unit_count) filter (where year_month = 201705), 0) as "201705",
  coalesce(sum(unit_count) filter (where year_month = 201706), 0) as "201706",
  coalesce(sum(unit_count) filter (where year_month = 201707), 0) as "201707",
  coalesce(sum(unit_count) filter (where year_month = 201708), 0) as "201708",
  coalesce(sum(unit_count) filter (where year_month = 201709), 0) as "201709",
  coalesce(sum(unit_count) filter (where year_month = 201710), 0) as "201710",
  coalesce(sum(unit_count) filter (where year_month = 201711), 0) as "201711",
  coalesce(sum(unit_count) filter (where year_month = 201712), 0) as "201712"         
from (
  select b.year_month, c.store_code, d.page, d.line,
    case 
      when d.line = 15 then 'versa'
      when d.line = 16	then 'sentra'
      when d.line = 17	then 'altima 4dr'
      when d.line = 18	then 'maxima'
      when d.line = 19	then '370z coupe'
      when d.line = 27	then 'juke'
      when d.line = 31	then 'nv200'
      when d.line = 32	then 'varmada'
      when d.line = 33	then 'rogue'
      when d.line = 34	then 'murano'
      when d.line = 35	then 'frontier'
      when d.line = 36	then 'pathfinder'
      when d.line = 37	then 'quest'
      when d.line = 38 then 'xterra'
      when d.line = 39 then 'titan king'
      when d.line = 40	then 'titan crew'
    end as model,   
    sum(case when a.amount < 0 then 1 else -1 end) as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join accounts_lines d on c.account = d.gl_account
  where a.post_status = 'Y'
    and b.year_month between 201601 and 201712
  --   and d.the_year = 2017
    and d.page = 14
    and d.line < 41
    and c.account_type = 'Sale'
  group by b.year_month, c.store_code, d.page, d.line) x
group by line, model
order by line