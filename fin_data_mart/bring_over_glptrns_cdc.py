# encoding=utf-8
import csv
import db_cnx

task = 'ext_glptrns'
fin_con = None
cdc_con = None
run_id = None
file_name = 'files/ext_glptrns.csv'

try:
    # run_id = ops.log_start(task)b
    # if ops.dependency_check(task) != 0:
    #     ops.log_dependency_failure(run_id)
    #     print 'Failed dependency check'
    #     exit()
    with db_cnx.postgres_ubuntu() as cdc_con:
        with cdc_con.cursor() as cdc_cur:
            sql = """
                select
                  gtco_, gttrn_, gtseq_, gtdtyp, gttype, gtpost, gtrsts, gtadjust, gtpsel,
                  trim(gtjrnl), gtdate, gtrdate, gtsdate, trim(gtacct), trim(gtctl_), trim(gtdoc_),
                  trim(gtrdoc_), gtrdtyp, trim(gtodoc_), trim(gtref_), trim(gtvnd_),
                  trim(gtdesc), gttamt, gtcost, gtctlo,gtoco_, rrn
                from test.ext_glptrns_1214
            """
            cdc_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(cdc_cur)
    with db_cnx.pg() as fin_con:
        with fin_con.cursor() as fin_cur:
            fin_cur.execute("truncate fin.ext_glptrns_1214")
            with open(file_name, 'r') as io:
                fin_cur.copy_expert("""copy fin.ext_glptrns_1214 from stdin with csv encoding 'latin-1 '""", io)
    # ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if cdc_con:
        cdc_con.close()
    if fin_con:
        fin_con.close()
