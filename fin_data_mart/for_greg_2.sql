﻿---------------------------------------------------------------
--< financial statement
---------------------------------------------------------------

/*

fact_fs
  dim_fs
  dim_fs_account
  dim_fs_org

*/  
-- page 2 202002
select *
from fin.page_2 
where year_month = 202002
order by store_code, line

-- page 2 2019
select line, line_label,
  max(case when year_month = 201901 then store end) as jan_store,
  max(case when year_month = 201902 then store end) as feb_store,
  max(case when year_month = 201903 then store end) as mar_store,
  max(case when year_month = 201904 then store end) as apr_store,
  max(case when year_month = 201905 then store end) as may_store,
  max(case when year_month = 201906 then store end) as jun_store,
  max(case when year_month = 201907 then store end) as jul_store,
  max(case when year_month = 201908 then store end) as aug_store,
  max(case when year_month = 201909 then store end) as sep_store,
  max(case when year_month = 201910 then store end) as oct_store,
  max(case when year_month = 201911 then store end) as nov_store,
  max(case when year_month = 201912 then store end) as dec_store
from fin.page_2
where the_year = 2019
  and store_code = 'ry1'
group by line, line_label
order by line  


-- page 3 202002
select store, 
  sum(amount) filter (where b.col = 1 and b.line between 3 and 6) as "New Variable",
  sum(amount) filter (where b.col = 5 and b.line between 4 and 6) as "Used Variable",
  sum(amount) filter (where b.col = 1 and b.line between 8 and 16) as "New Personnel",
  sum(amount) filter (where b.col = 5 and b.line between 8 and 16) as "Used Personnel",  
  sum(amount) filter (where b.col = 1 and b.line between 18 and 39) as "New Semi-Fixed",
  sum(amount) filter (where b.col = 5 and b.line between 18 and 39) as "Used Semi-Fixed",  
  sum(amount) filter (WHERE b.col = 1 and b.line between 41 and 54) as "New Fixed",
  sum(amount) filter (where b.col = 5 and b.line between 41 and 54) as "Used Fixed",
  sum(amount) filter (where b.col = 1 and b.line between 1 and 57) as "New Total",
  sum(amount) filter (where col = 5 and b.line between 1 and 57) as "Used Total"
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202002
  and b.page = 3
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
  and c.area = 'variable' 
group by store
having sum(amount) <> 0  
order by store

-- page 4 202002
select year_month, store, department, 
  sum(amount) filter(where line between 8 and 17) as personnel,
  sum(amount) filter (where line between 18 and 40) as semifixed,
  sum(amount) filter (where line between 41 and 56) fixed,
  sum(amount) filter (where line between 8 and 56) total
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202002
  and b.page = 4
  and b.line < 58 -- expenses only, exclude parts split
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
group by year_month, store, department
having sum(amount) <> 0  
order by year_month, store, department


---------------------------------------------------------------
--/> financial statement
---------------------------------------------------------------




---------------------------------------------------------------
--< fin.fact_gl
---------------------------------------------------------------

-- !!!!!!!! when querying against fin.fact_gl, don't forget: WHERE post_status = 'Y' !!!!!!!!!!!!

/*

fact_gl
  dim_account
  dim_doc_type
  dim_gl_description
  dim_journal
  dim_date

*/

-- new car inventory
select b.account, b.description, a.control, sum(a.amount) as amount
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
  and b.account_type = 'asset'
  and b.department_code = 'nc'
  and b.typical_balance = 'debit'
  and b.current_row
  and b.account <> '126104'
where a.post_status = 'Y'
group by b.account, b.description, a.control
having sum(a.amount) > 10000



 ---------------------------------------------------------------
--/> fin.fact_gl
--------------------------------------------------------------- 