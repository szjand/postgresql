﻿09/08/22
-- body shop at the request of ben cahalan
select d.last_name, d.first_name, a.control, d.cost_center, 
  sum(a.amount) filter (where b.year_month = 202207) as july,
  sum(a.amount) filter (where b.year_month = 202208) as august
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month in (202207,202208)
join fin.dim_Account c on a.account_key = c.account_key
  and c.account = '12205a'
join ukg.employees d on a.control = d.employee_number
where a.post_status = 'Y'
group by d.last_name, d.first_name, a.control, d.cost_center


select b.year_month, sum(a.amount)
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month in (202207,202208)
join fin.dim_Account c on a.account_key = c.account_key
  and c.account = '12205a'
join ukg.employees d on a.control = d.employee_number
where a.post_status = 'Y'
group by b.year_month


-- -- from Andrew
-- Other Salaries Payroll Account #12304-A-B-C-D-E-F-G-H-I for the GM Main Shop
-- Jon, 
-- Would you be able to show me a breakdown from all of August 2021 compared to August 2022 please? I would 
-- like to see the people listed in the individual accounts and what we paid them for the month including 
-- any accruals if that is possible. I need to be able to see who is in there that wasn’t last year and 
-- compare to who we added and what the associated personnel cost is for everyone.  I know our service advisor 
-- pay plan was designed around 7 advisors which we have more of now but the BDC has also grown. 

select * from fin.dim_Account where account in ('12304a','12304b','12304c','12304d','12304e','12304f','12304g','12304h','12304i') order by account
select * from fin.dim_account where account in ('12303','12304','12327','12328','12304h','12304j','12304f','12304a','12304b','12304c','12304d','12304e','12304f','12304g','12304h','12304i') order by account

202108,74676.37
202208,122235.18


select b.year_month, sum(a.amount)
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month in (202108,202208)
join fin.dim_Account c on a.account_key = c.account_key
  and c.account in ('12304a','12304b','12304c','12304d','12304e','12304f','12304g','12304h','12304i')
  and c.current_row
-- ukg.employees must be a left join, not all accounting entries are controled by an employeenumber  
left join ukg.employees d on a.control = d.employee_number
where a.post_status = 'Y'
group by b.year_month

-- select sum(coalesce(Aug_2021, 0)), sum(coalesce(Aug_2022, 0)) from (
select a.control, d.last_name, d.first_name, account, d.cost_center, -- e.journal_code,
  sum(a.amount) filter (where b.year_month = 202108) as Aug_2021,
  sum(a.amount) filter (where b.year_month = 202208) as Aug_2022
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month in (202108,202208)
join fin.dim_Account c on a.account_key = c.account_key
  and c.account in ('12304a','12304b','12304c','12304d','12304e','12304f','12304g','12304h','12304i')
left join ukg.employees d on a.control = d.employee_number
join fin.dim_journal e on a.journal_key = e.journal_key
where a.post_status = 'Y'
group by a.control, d.last_name, d.first_name, d.cost_center, c.account -- , e.journal_code
-- ) x
order by account, last_name nulls last
-- order by cost_center, last_name



/*
the diffs:
	acct			fs			gl
	12304B  56177		58240
	12304D  23576   31350
all other accounts match

-- the issue was the join on ukg.employees
-- because there are entries where the control is NOT an employee number, thereby screwing up the total
-- left join is ok
*/
select * 
from ( -- july statement
	select *, sum(amount) over (partition by col) from (
	select b.page, b.line, b.col, c.gl_account, c.gm_account, sum(a.amount) as amount
	from fin.fact_fs a
	join fin.dim_fs b on a.fs_key = b.fs_key
		and b.year_month = 202207
		and b.page = 4
		and line = 11
	join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key  
	join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
		and d.store = 'ry1'
		and d.department = 'service'
	group by b.page, b.line, b.col, c.gl_account, c.gm_account
	) x) aa
left join (	-- july gl
	select account, --e.journal_code,
		sum(a.amount) filter (where b.year_month = 202207) as Aug_2021
	--   sum(a.amount) filter (where b.year_month = 202208) as Aug_2022
	from fin.fact_gl a
	join dds.dim_date b on a.date_key = b.date_key
		and b.year_month = 202207
	join fin.dim_Account c on a.account_key = c.account_key
	--   and c.account in ('12304a','12304b','12304c','12304d','12304e','12304f','12304g','12304h','12304i')
		and c.account in ('12303','12304','12327','12328','12304h','12304j','12304f','12304a','12304b','12304c','12304d','12304e','12304f','12304g','12304h','12304i')
		and c.department_code = 'SD'
	left join ukg.employees d on a.control = d.employee_number
	join fin.dim_journal e on a.journal_key = e.journal_key
	where a.post_status = 'Y'
	group by account) bb on aa.gl_account = bb.account

