﻿the 2 line_label rows seem to be caused by including multiple years and then doing the distinct
for example p2 l2  2011-2021 TOTAL GROSS PROFIT/INCOME  2022-2024 TOTAL GROSS PROFIT/INCO


	delete from fin.test_dim_fs where year_month = 202401;
	insert into fin.test_dim_fs (the_year,year_month,page,line,col,line_label, row_from_date, current_row) 
create temp table wtf as
	select distinct 2024 as the_year,202401 as year_month, flpage, flflne, coalesce(d.fxmcol, -1) as col,
	  case -- multi valued labels with large internal block of spaces
	    when line_label = 'ASSETS                 AMOUNT' 
	      then 'ASSETS' -- adding the dashes fixed the p1 l6
	    when line_label = 'EXPENSES                                                                                                 NEW SALES/PN'
	      then 'EXPENSES'
-- 	    when flpage = 1 and flflne = 11 then 'WARRANTY CLAIM ADVANCE'
-- 	    when flpage = 1 and flflne = 15 then 'DUE FROM FINANCE COMPANIES'
-- 	    when flpage = 1 and flflne = 17 then 'INSURANCE COMMISSION RECEIVABLE'
-- 	    when flpage = 1 and flflne = 34 then 'MISC ASSETS RECEIVED IN TRADE'
-- 	    when flpage = 1 and flflne = 43 then 'L&R ACCUM DEPRECIATION'
-- 	    when flpage = 1 and flflne = 46 and d.fxmcol = 5 then 'ACCOUNT'
-- 	    when flpage = 1 and flflne = 48 then 'BLDS IMPRVM'
-- 	    when flpage = 1 and flflne = 52 then 'M&S EQUIPM'
-- 	    when flpage = 1 and flflne = 53 then 'P&A EQUIPM'
-- 	    when flpage = 1 and flflne = 54 then 'FURN & FIXT'
-- 	    when flpage = 1 and flflne = 60 then 'NOTES & ACCTS RECEIVABLE OFFICERS'
-- 	    when flpage = 1 and flflne = 61 then 'NOTES & ACCTS RECEIVABLE OTHERS'
-- 	    when flpage = 1 and flflne = 62 then 'OTHER INVESTMENTS & MISC ASSETS'
	    else line_label
	  end as line_label, current_date, true
	from (
	-- original
		select distinct flcyy, flpage, flflne, 
			case
				when flcyy = 2024 and flpage = 16 and flflne = 21 then 'CUSTOMER LABOR CARS & LRG TRUCKS'
				when flcyy = 2024 and flpage = 1 and flflne = 6 then '-ASSETS-'
				else line_label
			end as line_label
		from (
				select flcyy, flpage, flflne, trim(left(line_label, position('|' in line_label) - 1))::citext as line_label
				from (
					select flcyy, flpage, flflne,       
					case
						when left(fldata, 1) = '|' then right(fldata, length(fldata) - 1)
						else fldata
					end as line_label
					from arkona.ext_eisglobal_sypfflout
					where flflsq = 0 
						and flflne > 0
						and flpage < 18) a
				group by flcyy, flpage, flflne, trim(left(line_label, position('|' in line_label) - 1))) b where flpage = 2 and flflne = 2) c
	left join ( -- columns
	  select fxmcyy, fxmpge, fxmlne, fxmcol
	  from arkona.ext_eisglobal_sypffxmst
	  group by fxmcyy, fxmpge, fxmlne, fxmcol) d  on c.flcyy = d.fxmcyy and c.flpage = d.fxmpge and c.flflne = d.fxmlne
where flpage = 2 and flflne = 2


select flpage, flflne, col
from wtf
where year_month = 202401
group by flpage, flflne, col
having count(*) > 1

select * from wtf where flpage = 2 and flflne = 2


ERROR:  duplicate key value violates unique constraint "test_dim_fs_nk"
DETAIL:  Key (year_month, page, line, col)=(202401, 1, 6.0, -1) already exists.

|                 -ASSETS-                     |-AMOUNT-||              -LIABILITES-               | -AMOUNT-|

select *
from arkona.ext_eisglobal_sypfflout
where flcyy = 2024
and flpage = 2
and flflne = 2

select *, md5(flcont)
from arkona.ext_eisglobal_sypfflout_tmp
where flcyy > 2020
and flpage = 2
and flflne = 2
order by flcyy, flcont	  

select * from fin.dim_fs where page = 1 and line = 6 order by the_year, year_month