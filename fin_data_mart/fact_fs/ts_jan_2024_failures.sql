﻿DROP TABLE if exists fin.test_dim_fs cascade;
CREATE TABLE fin.test_dim_fs
(
  fs_key integer NOT NULL DEFAULT nextval('fin.dim_fs_fs_key_seq_2'::regclass), -- source: system generated surrogate key
  the_year integer NOT NULL, -- source: script generated
  year_month integer NOT NULL, -- source: script generated
  page integer NOT NULL, -- source: arkona.ext_eisglobal_sypfflout.flpage
  line numeric(4,1) NOT NULL, -- source: arkona.sypfflout.flflne
  col integer NOT NULL, -- source:
  line_label citext NOT NULL, -- source: arkona.sypfflout.fldata
  row_from_date date NOT NULL,
  row_thru_date date NOT NULL DEFAULT '9999-12-31'::date,
  current_row boolean NOT NULL,
  row_reason citext NOT NULL DEFAULT 'New Row'::citext,
  PRIMARY KEY (fs_key),
  CONSTRAINT test_dim_fs_nk UNIQUE (year_month, page, line, col));


!!! the integrity error is because this is returning col 11 for both rows, one should be 9

-- original with year & year month variables hard coded
	delete from fin.test_dim_fs where year_month = 202401;
	insert into fin.test_dim_fs (the_year,year_month,page,line,col,line_label, row_from_date, current_row)  
-- 	select _year, _year_month, flpage, flflne, coalesce(c.fxmcol, -1) as col,
	select distinct 2024 as the_year,202401 as year_month, flpage, flflne, coalesce(c.fxmcol, -1) as col,
	  case -- multi valued labels with large internal block of spaces
	    when line_label = 'ASSETS                 AMOUNT' 
	      then '-ASSETS-'
	    when line_label = 'EXPENSES                                                                                                 NEW SALES/PN'
	      then 'EXPENSES'
	    else line_label
	  end as line_label, current_date, true
	from ( -- year, page, line, line_label
	  select flcyy, flpage, flflne, trim(left(line_label, position('|' in line_label) - 1))::citext as line_label
	  from (
	    select flcyy, flpage, flflne,         
	    case
	      when left(fldata, 1) = '|' then right(fldata, length(fldata) - 1)
	      else fldata
	    end as line_label
	    from arkona.ext_eisglobal_sypfflout
	    where flflsq = 0 
	      and flflne > 0
	      and flpage < 18) a
	  group by flcyy, flpage, flflne, trim(left(line_label, position('|' in line_label) - 1))) b
	left join ( -- columns
	  select fxmcyy, fxmpge, fxmlne, fxmcol
	  from arkona.ext_eisglobal_sypffxmst
	  group by fxmcyy, fxmpge, fxmlne, fxmcol) c  on b.flcyy = c.fxmcyy and b.flpage = c.fxmpge and b.flflne = c.fxmlne
	where b.flcyy = 2024
	and flpage = 2 and flflne = 2

-- adding the distinct was the final touch after casing the line label
	select distinct 2024, 202401, flpage, flflne, coalesce(c.fxmcol, -1) as col,
	  case -- multi valued labels with large internal block of spaces
	    when line_label = 'ASSETS                 AMOUNT' 
	      then 'ASSETS'
	    when line_label = 'EXPENSES                                                                                                 NEW SALES/PN'
	      then 'EXPENSES'
	    else line_label
	  end as line_label, current_date, true
	from ( -- year, page, line, line_label
	  select flcyy, flpage, flflne, 
	    case
	      when flcode = 'GM' and flcyy = 2024 and flline = 27.0 and flflsq = 0 then 'CUSTOMER LABOR CARS & LD TRKS'::citext
	      else trim(left(line_label, position('|' in line_label) - 1))::citext 
	    end as line_label
	  from (
	    select flcode, flcyy, flpage, flline, flflne, flflsq,     
	    case
	      when left(fldata, 1) = '|' then right(fldata, length(fldata) - 1)
	      else fldata
	    end as line_label
	    from arkona.ext_eisglobal_sypfflout
	    where flflsq = 0 
	      and flflne > 0
	      and flpage < 18) a
	  where flcyy = 2024 and flpage = 16 and flflne = 21
	  group by flcode, flcyy, flline, flpage, flflne, flflsq, trim(left(line_label, position('|' in line_label) - 1))	) b
	left join ( -- columns
	  select fxmcyy, fxmpge, fxmlne, fxmcol
	  from arkona.ext_eisglobal_sypffxmst
	  group by fxmcyy, fxmpge, fxmlne, fxmcol) c  on b.flcyy = c.fxmcyy and b.flpage = c.fxmpge and b.flflne = c.fxmlne
	where b.flcyy = 2024
	and flpage = 16 and flflne = 21

-- original
	  select flcyy, flpage, flflne, trim(left(line_label, position('|' in line_label) - 1))::citext as line_label
	  from (
	    select flcyy, flpage, flflne,       
	    case
	      when left(fldata, 1) = '|' then right(fldata, length(fldata) - 1)
	      else fldata
	    end as line_label
	    from arkona.ext_eisglobal_sypfflout
	    where flflsq = 0 
	      and flflne > 0
	      and flpage < 18) a
	  group by flcyy, flpage, flflne, trim(left(line_label, position('|' in line_label) - 1))

	  
-- add the case
	  select flcyy, flpage, flflne, 
	    case
	      when flcode = 'GM' and flcyy = 2024 and flline = 27.0 and flflsq = 0 then 'CUSTOMER LABOR CARS & LD TRKS'::citext
	      else trim(left(line_label, position('|' in line_label) - 1))::citext 
	    end as line_label
	  from (
	    select flcode, flcyy, flpage, flline, flflne, flflsq,     
	    case
	      when left(fldata, 1) = '|' then right(fldata, length(fldata) - 1)
	      else fldata
	    end as line_label
	    from arkona.ext_eisglobal_sypfflout
	    where flflsq = 0 
	      and flflne > 0
	      and flpage < 18) a
	  where flcyy = 2024 and flpage = 16 and flflne = 21
	  group by flcode, flcyy, flline, flpage, flflne, flflsq, trim(left(line_label, position('|' in line_label) - 1))	

select *
from arkona.ext_eisglobal_sypfflout
where flcyy = 2024
and flpage = 16
and flflne = 21

select *, md5(flcont)
from arkona.ext_eisglobal_sypfflout
where flcyy > 2021
and flpage = 16
and flflne = 21
order by flcyy, flcont

|CUSTOMER LABOR CARS & LRG TRUCKS    |#1  |  |#2      |  |#3      |  |#4   |  |#5  |  |#6      |  |#7      |  |#8   |