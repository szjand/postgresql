﻿
-- store/page/line/col/gm_account/gl-account
select e.store_code, page, line::integer, col,
  string_agg(distinct gm_account, ',') as gm_account,
  string_agg(distinct gl_account, ',') as gl_account
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201707
  and b.page = 3
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
group by e.store_code, page, line::integer, col
order by gm_account


/*
this is even better, store doesn't matter, i am just dealing with gm_accounts
this joins on P2C1 = page 3 & 4, P2C7 = page 3 and P2C11 = page 4
*/
-- -- -- drop table if exists p2;
-- -- -- create temp table p2 as
-- -- -- select P2.*, P3_4.gm_account
-- -- -- from (
-- -- --   select a.page ,a.line::integer as line, a.line_label, a.col, b.fxmact
-- -- --   from fin.dim_fs a 
-- -- --   left join arkona.ext_eisglobal_sypffxmst b on a.line = b.fxmlne
-- -- --     and a.col = b.fxmcol
-- -- --     and b.fxmcyy = 2017
-- -- --     and b.fxmpge = 2
-- -- --   where a.year_month = 201707
-- -- --     and a.page = 2) P2
-- -- -- left join ( -- page 3/4
-- -- --   select page, line::integer as line, 
-- -- --     case 
-- -- --       when page = 3 then string_agg(distinct gm_account, ',') 
-- -- --       when page = 4 then string_agg(distinct gm_account, ',') 
-- -- --     end as gm_account
-- -- --   from fin.fact_fs a
-- -- --   inner join fin.dim_fs b on a.fs_key = b.fs_key
-- -- --     and b.year_month = 201707
-- -- --     and b.page between 3 and 4
-- -- --   inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
-- -- --   inner join fin.dim_account e on d.gl_account = e.account
-- -- --   inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
-- -- --   group by page, line::integer
-- -- --   union 
-- -- --   select 2 as page, line::integer as line, 
-- -- --      string_agg(distinct gm_account, ',') as gm_account
-- -- --   from fin.fact_fs a
-- -- --   inner join fin.dim_fs b on a.fs_key = b.fs_key
-- -- --     and b.year_month = 201707
-- -- --     and b.page between 3 and 4
-- -- --   inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
-- -- --   inner join fin.dim_account e on d.gl_account = e.account
-- -- --   inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
-- -- --   group by line::integer  ) P3_4 on P2.line = P3_4.line 
-- -- --     and 
-- -- --       case 
-- -- --         when p2.col = 1 then P3_4.page = 2
-- -- --         when p2.col = 7 then P3_4.page = 3
-- -- --         when p2.col = 11 then P3_4.page = 4
-- -- --       end;


oops, need to blow it up, one line per gm_account


drop table if exists p2 cascade;
create temp table p2 as
select P2.*, P3_4.gm_account
from (
  select a.page ,a.line::integer as line, a.line_label, a.col, b.fxmact
  from fin.dim_fs a 
  left join arkona.ext_eisglobal_sypffxmst b on a.line = b.fxmlne
    and a.col = b.fxmcol
    and b.fxmcyy = 2017
    and b.fxmpge = 2
  where a.year_month = 201708
    and a.page = 2
    and a.line between 4 and 54) P2
left join ( -- page 3/4
select page, line::integer as line, gm_account
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201708
  and b.page between 3 and 4
  and b.line between 4 and 54
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
union 
select 2 as page, line::integer as line, gm_account
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201708
  and b.page between 3 and 4
  and b.line between 4 and 54
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) P3_4 on P2.line = P3_4.line 
  and 
    case 
      when p2.col = 1 then P3_4.page = 2
      when p2.col = 7 then P3_4.page = 3
      when p2.col = 11 then P3_4.page = 4
    end;

create index on p2(gm_account);



select *
from p2

select c.store_code, a.page, a.line, 
  sum(case when a.col = 1 then d.amount else 0 end)::integer as store,
  sum(case when a.col = 7 then d.amount else 0 end)::integer as variable,
  sum(case when a.col = 11 then d.amount else 0 end)::integer as fixed
from p2 a
left join fin.dim_fs_account b on a.gm_account = b.gm_account
left join fin.dim_account c on b.gl_account = c.account
  and c.current_row = true
left join fin.fact_gl d on c.account_key = d.account_key
  and d.post_status = 'Y'
  and d.date_key in (
    select dd.date_key
    from dds.dim_date dd
    where dd.year_month = 201708)
group by c.store_code, a.page, a.line
order by c.store_code, a.line




201708
  lines 1 & 2
  missing col 1 for lines 4 - 6 both stores
  need subtotal lines 7,17,41, etc
  bad lines: 59, 64, 
    ry2 12,20, 27 $10, 59, 64






-- time to build it for real
-- initial load, base values (store, line, line labels, year, year_month) thru 202012
drop table if exists fin.page_2;
create table fin.page_2 (
  store_code citext not null,
  the_year integer not null,
  year_month integer not null,
  line integer not null,
  line_label citext not null,
  store integer not null default 0,
  variable integer not null default 0,
  fixed integer not null default 0,
  primary key(store_code, year_month, line));  
-- drop table if exists test;
-- create temp table test as
insert into fin.page_2
select 'RY1', m.the_year, m.year_month, n.line::integer as line, n.line_label, 
  0 as store, 0 as variable, 0 as fixed
from (
  select a.the_year, a.the_year * 100 + the_month as year_month
  from ( -- 201101 thru 201612
    select generate_series(2011, 2016, 1) as the_year) a
    cross join (
    select * from generate_Series(1, 12, 1) as the_month) b  
  union
  select a.the_year, a.the_year * 100 + the_month as year_month
  from ( --201701 thru 201707
    select 2017 as the_year) a
    cross join (
    select * from generate_Series(1, 7, 1) as the_month) b ) m
left join (
  select *
  from fin.dim_fs
  where year_month = 201707 and page = 2) n on 1 = 1
group by m.the_year, m.year_month, n.line::integer, n.line_label;
insert into fin.page_2
select 'RY2', m.the_year, m.year_month, n.line::integer as line, n.line_label, 
  0 as store, 0 as variable, 0 as fixed
from (
  select a.the_year, a.the_year * 100 + the_month as year_month
  from ( -- 201101 thru 201612
    select generate_series(2011, 2016, 1) as the_year) a
    cross join (
    select * from generate_Series(1, 12, 1) as the_month) b  
  union
  select a.the_year, a.the_year * 100 + the_month as year_month
  from ( --201701 thru 201707
    select 2017 as the_year) a
    cross join (
    select * from generate_Series(1, 7, 1) as the_month) b ) m
left join (
  select *
  from fin.dim_fs
  where year_month = 201707 and page = 2) n on 1 = 1
group by m.the_year, m.year_month, n.line::integer, n.line_label;

/*
9/9/17 this backfill pulls directly from fact_gl using the temp table p2
which defines the gm_account assignment to lines
today stumbled on  a goofy one
Honda P2L12, variable value is wrong, but P3L12 is correct
redo this back fill using P3 & P4 rather than regenerating from fact_gl
-- backfill thru 201707 (then configure monthly update)
update fin.page_2 z
set store = x.store,
    variable = x.variable,
    fixed = x.fixed
from (    
  select b.year_month, c.store_code, e.page, e.line,
    sum(case when e.col = 1 then a.amount else 0 end)::integer as store,
    sum(case when e.col = 7 then a.amount else 0 end)::integer as variable,
    sum(case when e.col = 11 then a.amount else 0 end)::integer as fixed
  -- select *  
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201201 and 201707
  inner join fin.dim_account c on a.account_key = c.account_key 
  inner join fin.dim_fs_account d on c.account = d.gl_account 
  inner join p2 e on d.gm_account = e.gm_account
  where a.post_status = 'Y' 
  group by b.year_month, c.store_code, e.page, e.line) x
where z.year_month = x.year_month 
  and z.store_code = x.store_code
  and z.line = x.line;  
*/

-- back fill based on P3 & P4
update fin.page_2 z
set fixed = x.amount
from (-- p4 fixed
  select year_month, store as store_code, line, sum(amount) as amount
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month between 201201 and 201707
    and b.page = 4
    and line between 4 and 54 
  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  group by year_month, store, line) x
where z.year_month = x.year_month
  and z.store_code = x.store_code
  and z.line = x.line;  

update fin.page_2 z
set variable = x.amount
from (-- p3 variable
  select year_month, store as store_code, line, sum(amount) as amount
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month between 201201 and 201707
    and b.page = 3
    and line between 4 and 54 
  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
  group by year_month, store, line) x
where z.year_month = x.year_month
  and z.store_code = x.store_code
  and z.line = x.line;  

update fin.page_2 z
set store = x.amount
from (-- total of p3 & p4
  select year_month, store as store_code, line, sum(amount) as amount
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month between 201201 and 201707
    and b.page in(3,4)
    and line between 4 and 54 
  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
  group by year_month, store, line) x
where z.year_month = x.year_month
  and z.store_code = x.store_code
  and z.line = x.line;

-- need to update
-- lines 7,17,40,49,55,56, 57 all 3 columns
update fin.page_2 z
set store = x.store,
    variable = x.variable,
    fixed = x.fixed
from (  
  select store_code, year_month, 7 as line,
    sum(store) as store,
    sum(variable) as variable,
    sum(fixed) as fixed
  from fin.page_2
  where year_month < 201708
    and line between 4 and 6
  group by store_code, year_month
  union all
  select store_code, year_month, 17 as line,
    sum(store) as store,
    sum(variable) as variable,
    sum(fixed) as fixed
  from fin.page_2
  where year_month < 201708
    and line between 8 and 16
  group by store_code, year_month
  union all
  select store_code, year_month, 40 as line,
    sum(store) as store,
    sum(variable) as variable,
    sum(fixed) as fixed
  from fin.page_2
  where year_month < 201708
    and line between 18 and 39
  group by store_code, year_month
  union all
  select store_code, year_month, 49 as line,
    sum(store) as store,
    sum(variable) as variable,
    sum(fixed) as fixed
  from fin.page_2
  where year_month < 201708
    and line between 41 and 48
  group by store_code, year_month
  union all
  select store_code, year_month, 55 as line,
    sum(store) as store,
    sum(variable) as variable,
    sum(fixed) as fixed
  from fin.page_2
  where year_month < 201708
    and (
      line between 49 and 54
      or line between 41 and 48)
  group by store_code, year_month
  union all
  select store_code, year_month, 56 as line,
    sum(store) as store,
    sum(variable) as variable,
    sum(fixed) as fixed
  from fin.page_2
  where year_month < 201708
    and (
      line between 49 and 54
      or line between 41 and 48
      or line between 8 and 16
      or line between 18 and 39)
  group by store_code, year_month
  union all
  select store_code, year_month, 57 as line,
    sum(store) as store,
    sum(variable) as variable,
    sum(fixed) as fixed
  from fin.page_2
  where year_month < 201708
    and (
      line between 49 and 54
      or line between 41 and 48
      or line between 8 and 16
      or line between 18 and 39
      or line between 4 and 6)
  group by store_code, year_month) x
where z.store_code = x.store_code
  and z.year_month = x.year_month
  and z.line = x.line;
  

-- need values for 
-- lines 1, 2
-- variable -- ready to go

update fin.page_2 z
set variable = x.amount
from (
  select m.store_code, m.year_month, 1 as line,
    m.fi_sales + n.uc_sales + o.nc_sales as amount
  from (-- ok. fi is some kind of goofy about what counts as sales and what counts as cogs-- this works
    select store as store_code, year_month, -1 * (sales_a + sales_b) as fi_sales, fi_gross
    from (
      select c.store, b.year_month,
      sum(case when line in (1, 2, 11, 12) then amount else 0 end) as sales_a,
      sum(case when line in (6, 7, 17) and amount < 0 then amount else 0 end) as sales_b,
      sum(-1 * amount) as fi_gross
      from fin.fact_fs a
      inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
      inner join fin.dim_account aaa on aa.gl_account = aaa.account
      inner join fin.dim_fs b on a.fs_key = b.fs_key
        and b.year_month between 201201 and 201707
        and b.page = 17 
        and b.line between 1 and 20 -- f/i
      inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
      group by c.store, b.year_month
      having sum(amount) <> 0) x) m
  left join (-- used cars sales off 201707 by what i believe is in line 11, but gross is ok, yep acct 165300
    select c.store as store_code, b.year_month, page,
      sum(case when amount < 0 then -1 * amount else 0 end) as uc_sales,
    --   sum(case when amount > 0 then amount else 0 end) as cogs,
      sum(-1 * amount) as uc_gross
    from fin.fact_fs a
    inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
    inner join fin.dim_account aaa on aa.gl_account = aaa.account
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month between 201201 and 201707
      and b.page = 16 and b.line between 1 and 14 -- used cars
    inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
    group by c.store, b.year_month, page
    having sum(amount) <> 0
    order by c.store, b.year_month) n on m.store_code = n.store_code and m.year_month = n.year_month
  left join (-- new cars
    select c.store as store_code, b.year_month, 
      sum(case when amount < 0 then - 1 * amount else 0 end) as nc_sales,
    --   sum(case when amount > 0 then amount else 0 end) as cogs,
      sum(-1 * amount) as nc_gross
    from fin.fact_fs a
    inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
    inner join fin.dim_account aaa on aa.gl_account = aaa.account
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month between 201201 and 201707
      and b.page between 5 and 15
    inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
    group by c.store, b.year_month
    having sum(amount) <> 0) o on m.store_code = o.store_code and m.year_month = o.year_month
  union
  select m.store_code, m.year_month, 2 as line,
    m.fi_gross + n.uc_gross + o.nc_gross as gross
  from (-- ok. fi is some kind of goofy about what counts as sales and what counts as cogs-- this works
    select store as store_code, year_month, -1 * (sales_a + sales_b) as fi_sales, fi_gross
    from (
      select c.store, b.year_month,
      sum(case when line in (1, 2, 11, 12) then amount else 0 end) as sales_a,
      sum(case when line in (6, 7, 17) and amount < 0 then amount else 0 end) as sales_b,
      sum(-1 * amount) as fi_gross
      from fin.fact_fs a
      inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
      inner join fin.dim_account aaa on aa.gl_account = aaa.account
      inner join fin.dim_fs b on a.fs_key = b.fs_key
        and b.year_month between 201201 and 201707
        and b.page = 17 
        and b.line between 1 and 20 -- f/i
      inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
      group by c.store, b.year_month
      having sum(amount) <> 0) x) m
  left join (-- used cars sales off 201707 by what i believe is in line 11, but gross is ok, yep acct 165300
    select c.store as store_code, b.year_month, page,
      sum(case when amount < 0 then -1 * amount else 0 end) as uc_sales,
    --   sum(case when amount > 0 then amount else 0 end) as cogs,
      sum(-1 * amount) as uc_gross
    from fin.fact_fs a
    inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
    inner join fin.dim_account aaa on aa.gl_account = aaa.account
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month between 201201 and 201707
      and b.page = 16 and b.line between 1 and 14 -- used cars
    inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
    group by c.store, b.year_month, page
    having sum(amount) <> 0
    order by c.store, b.year_month) n on m.store_code = n.store_code and m.year_month = n.year_month
  left join (-- new cars
    select c.store as store_code, b.year_month, 
      sum(case when amount < 0 then - 1 * amount else 0 end) as nc_sales,
    --   sum(case when amount > 0 then amount else 0 end) as cogs,
      sum(-1 * amount) as nc_gross
    from fin.fact_fs a
    inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
    inner join fin.dim_account aaa on aa.gl_account = aaa.account
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month between 201201 and 201707
      and b.page between 5 and 15
    inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
    group by c.store, b.year_month
    having sum(amount) <> 0) o on m.store_code = o.store_code and m.year_month = o.year_month) x
where z.store_code = x.store_code
  and z.year_month = x.year_month
  and z.line = x.line;      
-- 
-- --201708 new cars sales off by 800, gross ok, so what's up
-- -- 500 p5, 300 p10, fuck it, accounting anomalies, good enough
-- -- new cars
-- select c.store, b.year_month, page, line,
--   sum(case when amount < 0 then amount else 0 end) as sales,
--   sum(case when amount > 0 then amount else 0 end) as cogs,
--   sum(amount) as gross
-- from fin.fact_fs a
-- inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
-- inner join fin.dim_account aaa on aa.gl_account = aaa.account
-- inner join fin.dim_fs b on a.fs_key = b.fs_key
--   and b.year_month = 201708
--   and b.page in (5, 10)
-- inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
-- group by c.store, b.year_month, page, line
-- having sum(amount) <> 0
-- order by c.store, b.year_month;

-- fixed
-- ready to go
update fin.page_2 z
set fixed = x.amount
from (
  select b.year_month, c.store as store_code, 1 as line,
    sum(case when col in (1,2) then -1 * amount else 0 end) as amount
  --   sum(case when col = 3 then amount else 0 end) as cogs,
  --   sum(-1 * amount) as gross
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month between 201201 and 201707
    and b.page = 16 
    and b.line between 20 and 59
  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  where store <> 'none'
  group by b.year_month, c.store
  having sum(amount) <> 0
  union
  select b.year_month, c.store, 2 as line,
  --   sum(case when col in (1,2) then -1 * amount else 0 end) as sales,
  --   sum(case when col = 3 then amount else 0 end) as cogs,
    sum(-1 * amount) as gross
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month between 201201 and 201707
    and b.page = 16 
    and b.line between 20 and 59
  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  where store <> 'none'
  group by b.year_month, c.store
  having sum(amount) <> 0) x
where z.store_code = x.store_code
  and z.year_month = x.year_month
  and z.line = x.line;

update fin.page_2 z
set store = variable + fixed
where line between 1 and 2;

-- line 58 operating profit
update fin.page_2 z
set store = x.store,
    variable = x.variable, 
    fixed = x.fixed
from (
  select a.store_code, a.year_month, a.store - b.store as store,
    a.variable - b.variable as variable, 
    a.fixed - b.fixed as fixed
  from (
    select store_code, year_month, store, variable, fixed
    from fin.page_2
    where line = 2) a
  left join (  
    select store_code, year_month, store, variable, fixed
    from fin.page_2
    where line = 57) b on a.store_code = b.store_code 
      and a.year_month = b.year_month) x 
where z.store_code = x.store_code 
  and z.year_month = x.year_month
  and z.line = 58;

  
-- line 59 adds and deduct
update fin.page_2 z
set store = x.amount
from (
  select store as store_code, year_month, sum(-1 * amount) as amount
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201608
    and b.page = 3
    and b.line > 60
  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
  where store in ('ry1','ry2')
  group by store, year_month) x
where z.store_code = x.store_code
  and z.year_month = x.year_month
  and z.line = 59;  

-- lines 60, 63, 65
update fin.page_2 z
set store = x.store
from (
  select a.store_code, a.year_month, a.store + b.store as store
  from (
    select store_code, year_month, store
    from fin.page_2
    where line = 58) a
  left join (  
    select store_code, year_month, store
    from fin.page_2
    where line = 59) b on a.store_code = b.store_code 
      and a.year_month = b.year_month) x 
where z.store_code = x.store_code 
  and z.year_month = x.year_month
  and z.line in (60,63,65);

select *
from fin.page_2
where year_month = 201707
  and store_code = 'ry2'
order by line    