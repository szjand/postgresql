﻿
/*
ok, its time to do monthly update including page 2
start with 201702
now, page 2
from  page_2_v4 lines 1, 2
*/

do
$$
declare 
   _year integer := 2017;
   _year_month integer := 201708;
begin

----------------------------------------------------------------------------
-- DIM_FS from page_2_v5
----------------------------------------------------------------------------
-- these are to be replaced with actual columns
delete from fin.dim_fs where col = -1 and page = 2;
-- store level for which there is no configuration in arkona
insert into fin.dim_fs (the_year, year_month,page,line,col,line_label,row_from_date, current_row)
select the_year, year_month, page, line, 1 as col, line_label, current_date, true
from fin.dim_Fs
where page = 2
  and line in (2,4,5,6,57)
  and the_year = _year
  and year_month = _year_month
group by the_year, year_month, page, line, line_label;
-- sub total lines
insert into fin.dim_fs (the_year, year_month,page,line,col,line_label,row_from_date, current_row)
-- col 1
select _year, _year_month, b.flpage, b.flflne, 1 as col, 
  case -- multi valued labels with large internal block of spaces
    when line_label = 'ASSETS                 AMOUNT' 
      then 'ASSETS'
    when line_label = 'EXPENSES                                                                                                 NEW SALES/PN'
      then 'EXPENSES'
    else line_label
  end as line_label, current_date, true
from ( -- year, page, line, line_label
  select flcyy, flpage, flflne, trim(left(line_label, position('|' in line_label) - 1))::citext as line_label
  from (
    select flcyy, flpage, flflne,       
    case
      when left(fldata, 1) = '|' then right(fldata, length(fldata) - 1)
      else fldata
    end as line_label
    from arkona.ext_eisglobal_sypfflout
    where flflsq = 0 
      and flflne > 0
      and flpage < 18) a
  group by flcyy, flpage, flflne, trim(left(line_label, position('|' in line_label) - 1))) b
where b.flcyy = _year
  and b.flpage = 2
  and b.flflne in (7,17,40,49,55,56,58,60,63,65)
union
-- col 7
select _year, _year_month, b.flpage, b.flflne, 7 as col, 
  case -- multi valued labels with large internal block of spaces
    when line_label = 'ASSETS                 AMOUNT' 
      then 'ASSETS'
    when line_label = 'EXPENSES                                                                                                 NEW SALES/PN'
      then 'EXPENSES'
    else line_label
  end as line_label, current_date, true
from ( -- year, page, line, line_label
  select flcyy, flpage, flflne, trim(left(line_label, position('|' in line_label) - 1))::citext as line_label
  from (
    select flcyy, flpage, flflne,       
    case
      when left(fldata, 1) = '|' then right(fldata, length(fldata) - 1)
      else fldata
    end as line_label
    from arkona.ext_eisglobal_sypfflout
    where flflsq = 0 
      and flflne > 0
      and flpage < 18) a
  group by flcyy, flpage, flflne, trim(left(line_label, position('|' in line_label) - 1))) b
where b.flcyy = _year
  and b.flpage = 2
  and b.flflne in (7,17,40,49,55,56,58)
union
-- col 11
select _year, _year_month, b.flpage, b.flflne, 11 as col, 
  case -- multi valued labels with large internal block of spaces
    when line_label = 'ASSETS                 AMOUNT' 
      then 'ASSETS'
    when line_label = 'EXPENSES                                                                                                 NEW SALES/PN'
      then 'EXPENSES'
    else line_label
  end as line_label, current_date, true
from ( -- year, page, line, line_label
  select flcyy, flpage, flflne, trim(left(line_label, position('|' in line_label) - 1))::citext as line_label
  from (
    select flcyy, flpage, flflne,       
    case
      when left(fldata, 1) = '|' then right(fldata, length(fldata) - 1)
      else fldata
    end as line_label
    from arkona.ext_eisglobal_sypfflout
    where flflsq = 0 
      and flflne > 0
      and flpage < 18) a
  group by flcyy, flpage, flflne, trim(left(line_label, position('|' in line_label) - 1))) b
where b.flcyy = _year
  and b.flpage = 2
  and b.flflne in (17,40,49,55,56,58);

----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------
-- P2L1C11
----------------------------------------------------------------------------
-- drop table if exists wtf;
-- create temp table wtf as
insert into fin.fact_fs(fs_key, fs_org_key, fs_account_key, amount)
select e.fs_key, c.fs_org_key, d.fs_account_key, c.amount
from arkona.ext_eisglobal_sypffxmst a
inner join (
  select b.year_month, a.fs_key, a.fs_org_key, cc.fs_account_key, a.amount, c.gl_account
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  left join fin.dim_fs_Account cc on c.gl_account = cc.gl_account 
    and cc.gm_account = '<TFS'
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where b.page = 16
    and b.line between 21 and 59
    and b.col in (1,2)
--     and d.area = 'fixed') c on aa.year_month = c.year_month  
    and d.area = 'fixed') c on c.year_month = _year_month  
inner join fin.dim_fs_account d on c.gl_account = d.gl_account
  and d.gm_Account = '<TFS'   
-- inner join fin.dim_fs e on aa.year_month = e.year_month
inner join fin.dim_fs e on e.year_month = _year_month
  and e.page = 2
  and e.line = 1
  and e.col = 11
where a.fxmcyy = _year
  and a.fxmact = '<TFS'
  and a.fxmpge = 2
  and a.fxmlne in (1,2);   

----------------------------------------------------------------------------
-- P2L1C7
----------------------------------------------------------------------------
-- insert into wtf
insert into fin.fact_fs(fs_key, fs_org_key, fs_account_key, amount)
select e.fs_key, c.fs_org_key, d.fs_account_key, c.amount
from arkona.ext_eisglobal_sypffxmst a
inner join (
  select b.year_month, a.fs_key, a.fs_org_key, cc.fs_account_key, a.amount, c.gl_account
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  left join fin.dim_fs_Account cc on c.gl_account = cc.gl_account 
    and cc.gm_account = '<TVS'
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where (
    (b.page = 16 and b.line in (1,2,3,4,5,8,10) and b.col = 1) --uc
    or
    (b.page between 5 and 15 and b.col = 1) -- nc
    or
    (b.page = 17 and b.line between 1 and 21 and b.col = 1)) --f&i
    and d.area = 'variable') c on c.year_month = _year_month  
inner join fin.dim_fs_account d on c.gl_account = d.gl_account
  and d.gm_Account = '<TVS'   
inner join fin.dim_fs e on e.year_month = _year_month
  and e.page = 2
  and e.line = 1
  and e.col = 7
where a.fxmcyy = _year
  and a.fxmact = '<TVS'
  and a.fxmpge = 2
  and a.fxmlne in (1,2);   

----------------------------------------------------------------------------
-- P2L1C1
----------------------------------------------------------------------------
-- insert into wtf
insert into fin.fact_fs(fs_key, fs_org_key, fs_account_key, amount)
select e.fs_key, c.fs_org_key, d.fs_account_key, c.amount
from arkona.ext_eisglobal_sypffxmst a
inner join (
  select b.year_month, a.fs_key, a.fs_org_key, cc.fs_account_key, a.amount, c.gl_account
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  left join fin.dim_fs_Account cc on c.gl_account = cc.gl_account 
    and cc.gm_account = '<TNU'
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where ((
    (b.page = 16 and b.line in (1,2,3,4,5,8,10) and b.col = 1) --uc
    or
    (b.page between 5 and 15 and b.col = 1) -- nc
    or
    (b.page = 17 and b.line between 1 and 21 and b.col = 1) --f&i
    and d.area = 'variable')
    or
    (b.page = 16 and b.line between 21 and 59 and b.col in (1,2) and d.area = 'fixed'))) c on c.year_month = _year_month  
inner join fin.dim_fs_account d on c.gl_account = d.gl_account
  and d.gm_Account = '<TNU'   
inner join fin.dim_fs e on e.year_month = _year_month
  and e.page = 2
  and e.line = 1
where a.fxmcyy = _year
  and a.fxmact = '<TNU'
  and a.fxmpge = 2
  and a.fxmlne in (1,2);  

----------------------------------------------------------------------------
-- P2L2C11
----------------------------------------------------------------------------
-- insert into wtf
insert into fin.fact_fs(fs_key, fs_org_key, fs_account_key, amount)
select e.fs_key, c.fs_org_key, d.fs_account_key, c.amount
from arkona.ext_eisglobal_sypffxmst a
inner join (
  select b.year_month, a.fs_key, a.fs_org_key, cc.fs_account_key, a.amount, c.gl_account
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  left join fin.dim_fs_Account cc on c.gl_account = cc.gl_account 
    and cc.gm_account = '<TFEG'
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where b.page = 16
    and b.line between 21 and 59
    and d.area = 'fixed') c on c.year_month = _year_month  
inner join fin.dim_fs_account d on c.gl_account = d.gl_account
  and d.gm_Account = '<TFEG'   
inner join fin.dim_fs e on e.year_month = _year_month
  and e.page = 2
  and e.line = 2
  and e.col = 11
where a.fxmcyy = _year
  and a.fxmact = '<TFEG'
  and a.fxmpge = 2
  and a.fxmlne in (1,2);    

----------------------------------------------------------------------------
-- P2L2C7
----------------------------------------------------------------------------
-- insert into wtf
insert into fin.fact_fs(fs_key, fs_org_key, fs_account_key, amount)
select e.fs_key, c.fs_org_key, d.fs_account_key, c.amount
from arkona.ext_eisglobal_sypffxmst a
inner join (
  select b.year_month, a.fs_key, a.fs_org_key, cc.fs_account_key, a.amount, c.gl_account
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  left join fin.dim_fs_Account cc on c.gl_account = cc.gl_account 
    and cc.gm_account = '<TVEG'
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where (
    (b.page = 16 and b.line in (1,2,3,4,5,8,9,10)) --uc
    or
    (b.page between 5 and 15) -- nc
    or
    (b.page = 17 and b.line between 1 and 21)) --f&i
    and d.area = 'variable') c on c.year_month = _year_month  
inner join fin.dim_fs_account d on c.gl_account = d.gl_account
  and d.gm_Account = '<TVEG'   
inner join fin.dim_fs e on e.year_month = _year_month
  and e.page = 2
  and e.line = 2
  and e.col = 7
where a.fxmcyy = _year
  and a.fxmact = '<TVEG'
  and a.fxmpge = 2
  and a.fxmlne in (1,2);    


----------------------------------------------------------------------------
-- P2 L4-54 Expenses does not include sub total lines
/*
7/4/17 fails with null fs_account_key, it is not picking upt the Auto Outlet account 1602W
8/6/17 fails with 58 rows with null fs_account_key
see fix in /python projects/fin_data_mart/fact_fs/jeris_fs_page/page_2_add_new_accounts.sql
*/
----------------------------------------------------------------------------
-- insert into wtf
insert into fin.fact_fs(fs_key, fs_org_key, fs_account_key, amount)
select c.fs_key, b.fs_org_key, d.fs_account_key, b.amount
from (
  select *  -- base_2
  from arkona.ext_eisglobal_sypffxmst 
  where fxmpge = 2
    and fxmlne between 4 and 54   
    and fxmcyy = _year) a
left join (
  -- expense line of page 3 & 4 (line 4 - 54)
  -- by year_month/store/page/line/col/gl_account::amount
  -- base_1
  select store, page, line, col, gl_account, d.fs_org_key, sum(amount) as amount
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where b.page in (3,4)
    and b.line between 4 and 54
    and d.store <> 'none'
    and b.year_month = _year_month
  group by store, page, line, col, gl_account, d.fs_org_key) b on a.fxmlne = b.line
    and 
      case 
        when a.fxmlne < 8 then 1 = 1
        when a.fxmcol = 1 then 1 = 1
        when a.fxmcol = 7 then b.page = 3
        when a.fxmcol = 11 then b.page = 4
      end
left join fin.dim_fs c on a.fxmpge = c.page
  and a.fxmlne = c.line
  and a.fxmcol = c.col
  and c.year_month = _year_month
left join fin.dim_fs_account d on a.fxmact = d.gm_account
  and b.gl_account = d.gl_account;

----------------------------------------------------------------------------
-- Page 2 Line 59: Net Additions & Deductions, from Page 3 lines 63 - 68
----------------------------------------------------------------------------
-- insert into wtf
insert into fin.fact_fs(fs_key, fs_org_key, fs_account_key, amount)
select g.fs_key, i.fs_org_key, h.fs_account_key, round(e.amount, 0) as amount
from (
  select store, the_year, year_month, gm_account, gl_account, sum(amount) as amount, department
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  inner join fin.dim_fs_account c on b.account = c.gl_account
    and gm_account in ('902', '903', '905','909','910','952','953','955')
  inner join dds.dim_date d on a.date_key = d.date_key
    and d.year_month = _year_month
  where post_status = 'Y'  
  group by store, the_year, year_month, gm_account, gl_account, department) e
left join (
  select store_code, fxmact, g_l_acct_number, fxmpge, fxmlne, fxmcol
  from arkona.ext_eisglobal_sypffxmst a
  left join (
    select factory_financial_year, 
    case
      when coalesce(consolidation_grp, '1')  = '1' then 'Rydell GM'::citext
      when coalesce(consolidation_grp, '1')  = '2' then 'Honda Nissan'::citext
      else 'XXX'::citext
    end as store_code, g_l_acct_number, factory_account, fact_account_
    from arkona.ext_ffpxrefdta) b on a.fxmcyy = b.factory_financial_year and a.fxmact = b.factory_account
  where a.fxmcyy = _year
    and fxmpge = 3
    and fxmlne between 63 and 68) f on e.store = f.store_code  
  and e.gl_account = f.g_l_acct_number  
  and e.gm_Account = f.fxmact 
left join fin.dim_fs g on e.year_month = g.year_month  
  and f.fxmpge = g.page
  and f.fxmcol = g.col
  and f.fxmlne = g.line
left join fin.dim_fs_account h on e.gm_account = h.gm_account
  and e.gl_account = h.gl_account  
left join fin.dim_fs_org i on i.area = 'general'
  and 
  case 
    when e.store = 'Rydell GM' then i.store = 'RY1'
    when e.store = 'Honda Nissan' then i.store = 'RY2'
  end;  

----------------------------------------------------------------------------
-- P2L2C1
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 2 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L2C1' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and (
      (b.page = 16 and b.line between 21 and 61) or -- fixed
      (b.page = 16 and b.line between 1 and 20) or --  uc
      (b.page between 5 and 15) or -- nc
      (b.page = 17 and b.line between 1 and 21)); --f&i  

----------------------------------------------------------------------------
-- P2L4C1
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 4 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L4C1' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and gm_account = '<ex011v';

----------------------------------------------------------------------------
-- P2L5C1
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 5 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L5C1' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and gm_account = '<ex013v';

----------------------------------------------------------------------------
-- P2L6C1
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 6 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L6C1' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and gm_account = '<ex015v';

----------------------------------------------------------------------------
-- P2L7C7
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 7 and col = 7),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L7C7' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and gm_account in ('<ex011v','<ex013v','<ex015v');

----------------------------------------------------------------------------
-- P2L7C1
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 7 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L7C1' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and gm_account in ('<ex011v','<ex013v','<ex015v');

----------------------------------------------------------------------------
-- P2L17C11
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 17 and col = 11),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L17C11' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 8 and 16
  and col = 11;

----------------------------------------------------------------------------
-- P2L17C7
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 17 and col = 7),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L17C7' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 8 and 16
  and col = 7;

----------------------------------------------------------------------------
-- P2L17C1
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 17 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L17C1' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 8 and 16
  and col = 1;

----------------------------------------------------------------------------
-- P2L40C11
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 40 and col = 11),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L40C11' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 18 and 39
  and col = 11;

----------------------------------------------------------------------------
-- P2L40C7
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 40 and col = 7),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L40C7' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month  = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 18 and 39
  and col = 7;


----------------------------------------------------------------------------
-- P2L40C1
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 40 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L40C1' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 18 and 39
  and col = 1;
  
----------------------------------------------------------------------------
-- P2L49C11
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 49 and col = 11),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L49C11' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 41 and 48  
  and col = 11;
  
----------------------------------------------------------------------------
-- P2L49C7
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 49 and col = 7),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L49C7' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 41 and 48 
  and col = 7;
  
----------------------------------------------------------------------------
-- P2L49C1
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 49 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L49C1' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 41 and 48
  and col = 1;
----------------------------------------------------------------------------
-- P2L55C11
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 55 and col = 11),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L55C11' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 41 and 54
  and col = 11;
  
----------------------------------------------------------------------------
-- P2L55C7
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 55 and col = 7),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L55C7' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 41 and 54
  and col = 7;
  
----------------------------------------------------------------------------
-- P2L55C1
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 55 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L55C1' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 41 and 54
  and col = 1;
  
----------------------------------------------------------------------------
-- P2L56C11
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 56 and col = 11),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L56C11' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 8 and 54
  and col = 11;
  
----------------------------------------------------------------------------
-- P2L56C7
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 56 and col = 7),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L56C7' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 8 and 54
  and col = 7;
  
----------------------------------------------------------------------------
-- P2L56C1
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 56 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L56C1' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 8 and 54
  and col = 1;
  
----------------------------------------------------------------------------
-- P2L57C11
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 57 and col = 11),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = '<G&AF' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 4 and 54
  and col = 11;

----------------------------------------------------------------------------
-- P2L57C7
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 57 and col = 7),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = '<G&AV' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 4 and 54
  and col = 7;


----------------------------------------------------------------------------
-- P2L57C1
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 57 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L57C1' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 4 and 54
  and col = 1;
  
----------------------------------------------------------------------------
-- P2L58C1
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 58 and col = 11),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account in ('<TFEG', '<G&AF') and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and gm_account in ('<TFEG', '<G&AF');
  
----------------------------------------------------------------------------
-- P2L58C7
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 58 and col = 7),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account in ('<TVEG', '<G&AV') and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 2 and 54
  and col = 7;
  
----------------------------------------------------------------------------
-- P2L58C1
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 58 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account in ('P2L57C1', 'P2L2C1') and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 2 and 54
  and col = 1;
  
----------------------------------------------------------------------------
-- P2L59C1
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 59 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = '<TOIN' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 3
  and line between 63 and 68;
  
----------------------------------------------------------------------------
-- P2L60C1
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 60 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account in ('P2L57C1', 'P2L2C1', '<TOIN') and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 2 and 59
  and col = 1;
  
----------------------------------------------------------------------------
-- P2L65C1
----------------------------------------------------------------------------
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 65 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account in ('P2L57C1', 'P2L2C1', '<TOIN') and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = _year_month
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 2 and 59
  and col = 1;


    
end
$$;  

select store, page, line, col, abs(sum(amount))
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where year_month = 201707
  and page = 2
  and col < 3
--   and line in (2,7,17,40,49,55,56,57,58,59,60)
group by store, page, line, col
order by store, page, line, col;



