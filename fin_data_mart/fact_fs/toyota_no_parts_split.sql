﻿/*
09/14/22
ugghhh
failed, normal failure, missing as dim_fs_account key
but i chased my tail round and round
turned out (as usual) a simple case of adding account do fin.dim_fs_account
*/
select * 
from arkona.ext_ffpxrefdta
where company_number = 'ry8'
  and g_l_acct_number in ('4700','4700D','4712','4735','4751','6700','6712','6735','6751')

select * 
from arkona.ext_ffpxrefdta
where company_number = 'ry1'
  and factory_account in ('467S','667S', '468S','668S')  
  and factory_financial_year = 2022

select a.*, b.department 
from fin.dim_fs_account a
left join fin.dim_account b on a.gl_account = b.account
where gl_account  in ('4700','4700D','4712','4735','4751','6700','6712','6735','6751')

select *
from fin.dim_account
where account in ('4700','4700D','4712','4735','4751','6700','6712','6735','6751')

select '(' || ''''||market||'''' ||','||'''RY8'','||''''||area||''''||','||''''||department||''''||','||''''||sub_department||''''||'),'
from fin.dim_fs_org
where store = 'RY2'

insert into fin.dim_fs_org (market,store,area,department,sub_department) values
('grand forks','RY8','variable','sales','new'),
('grand forks','RY8','variable','sales','used'),
('grand forks','RY8','variable','finance','new'),
('grand forks','RY8','variable','finance','used'),
('grand forks','RY8','fixed','parts','none'),
('grand forks','RY8','fixed','service','mechanical'),
('grand forks','RY8','fixed','service','pdq'),
('grand forks','RY8','general','none','none'),
('grand forks','RY8','fixed','service','detail');

select * from fin.dim_fs_org

select * 
from fin.dim_fs_account
where gm_account in ('467s','667s','478s','678s','477s','677s','468s','668s')

insert into fin.dim_fs_account (gm_account,gl_account,row_from_Date,row_thru_date,current_row,row_reason)
values
-- ('467S','4700','07/01/2022','12/31/9999',true,'toyota'),
-- ('467S','4700D','07/01/2022','12/31/9999',true,'toyota'),
-- ('467S','4751','07/01/2022','12/31/9999',true,'toyota'),
-- ('667S','6700','07/01/2022','12/31/9999',true,'toyota'),
-- ('667S','6751','07/01/2022','12/31/9999',true,'toyota'),
-- ('468S','4712','07/01/2022','12/31/9999',true,'toyota'),
('468S','4735','07/01/2022','12/31/9999',true,'toyota'),
('668S','6712','07/01/2022','12/31/9999',true,'toyota'),
('668S','6735','07/01/2022','12/31/9999',true,'toyota');

select * from fin.dim_fs_account where gm_account in ('467a','667a')


-- turns out all i had to do was add accounts 467a and 667a to this query
-- 9/14 well, that is not all, still struggling to get it to work in the insert into fin.fact_fs portion
-- still fails with a null dim.dim_fs_account key
-- this is the query that creates the parts split table that gets inserted into fin.xfm_fs_1
-- 	truncate fin.parts_split;
-- 	insert into fin.parts_split
	select a.the_year, a.year_month, 4 as page, 59.0 as line, 
	  case 
	    when a.gm_account in ('477s','677s') then 11 -- body shop
	    else 1
	  end as col, 
	  a.gm_account, replace(a.gl_account,'SPLT','')::citext as gl_account
	-- select *  
	from fin.xfm_fs_1 a
	-- *e*
	where ((a.gm_account in('467s','667s','478s','678s','477s','677s','468s','668s','467a','667a'))
	  or (a.gl_account in ('146700D','246700D','246701D'))) order by gl_Account;  


select * from fin.xfm_fs_1 where gm_account in ('467s','667s','478s','678s','477s','677s','468s','668s')
page = 4 and line = 59 

-- this is taken from monthly_update_fails_with_null_fs_account_key.sql
-- it is where parts split gets
drop table if exists wtf;
create temp table wtf as
select b.fs_key, coalesce(e.fs_org_key, ee.fs_org_key) as fs_org_key, c.fs_account_key, 
  round( -- this is the parts split
    sum(
      case 
        when b.page = 4 and b.line = 59 and c.gl_account not in ('147701','167701') 
          then round(coalesce(f.amount, 0) * .5, 0) 
        else coalesce(f.amount, 0)
      end), 0) as amount  
from fin.xfm_fs_1 a  
left join fin.dim_fs b on a.year_month = b.year_month
  and a.page = b.page
  and a.line = b.line
  and a.col = b.col
  and b.page = 4 and b.line = 59 -----------------------------------------------------
left join fin.dim_fs_account c on a.gm_account = c.gm_account
  and a.gl_account = c.gl_account  
--  and c.gm_account not in ('4700','4700D','4712','4735','4751','6700','6712','6735','6751') -- exclude toyota from parts split
left join fin.dim_account d on c.gl_account = d.account  
-- *c*
  and d.current_row = true
left join fin.dim_Fs_org e on d.store_code = e.store
  and e.fs_org_key <> 18
  and
    case -- parts split
      when b.page = 4 and b.line = 59 /*and e.store <> 'RY8'*/ then 
        case 
	-- *ee*         
          when c.gm_account in ('467S','667S', '468S','668S','467a','667a' ) then e.sub_department = 'mechanical'
          when c.gm_account in ('478s','678s') then e.sub_department = 'pdq'
          when c.gm_account in ('477s','677s') then e.department = 'body shop'
          when c.gl_account = '146700D' then e.sub_department = 'mechanical'
          when c.gl_account = '246700D' then e.sub_department = 'mechanical' 
          when c.gl_account = '246701D' then e.sub_department = 'pdq'
        end 
      else   
        case 
          when d.department = 'Body Shop' then e.department = 'body shop'
          when d.department = 'Car Wash' then e.sub_Department = 'car wash'
          when d.department = 'Detail' then e.sub_department = 'detail'
          when d.department = 'Finance' and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
          when d.department = 'Finance' and b.line > 10 then e.department = 'finance' and e.sub_department = 'used'
          -- next 2 lines  correctly categorize these accounts as finance rather than sales, which is how they are categorized in COA
          when d.department = 'New Vehicle' and b.page = 17 and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
          when d.department = 'Used Vehicle' and b.page = 17 and b.line between 11 and 19 then e.department = 'finance' and e.sub_department = 'used'      
          when d.department = 'New Vehicle' then e.department = 'sales' and e.sub_department = 'new'
          when d.department = 'Parts' then e.department = 'parts'
          when d.department = 'Quick Lane' then e.sub_department = 'pdq'
          when d.department = 'Service' then e.sub_department = 'mechanical'
          when d.department = 'Used Vehicle' then e.department = 'sales' and e.sub_department = 'used'
-- *b*          
          when d.department = 'Auto Outlet' then e.department = 'sales' and e.sub_department = 'used'
-- *e*
          when d.department = 'National' then e.department = 'national'
          when d.department = 'General' then e.area = 'general'
        end
    end
left join (
  select b.yearmonth, aa.account, sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_account aa on a.account_key = aa.account_key
-- *f*
--     and aa.current_row = true
  inner join dds.day b on a.date_key = b.datekey  
  where a.post_status = 'Y'
    and b.yearmonth = 202208
  group by b.yearmonth, aa.account) f on a.gl_account = f.account
    and b.year_month = f.yearmonth    
left join fin.dim_fs_org ee on 1 = 1
  and ee.market = 'none'   
group by a.year_month, b.fs_key, c.fs_account_key, 
  coalesce(e.fs_org_key, ee.fs_org_key) having sum(amount) <> 0;     

select * from wtf

select * from fin.xfm_fs_1 a
left join fin.dim_fs_account b on a.fs_account_key = b.fs_Account_key where fs_key = 1025035  


select * from fin.fact_fs where fs_key = 1025035  