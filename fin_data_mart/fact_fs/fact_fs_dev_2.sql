﻿-- this is just the culled out generation of temp1 - temp4 from fact_fs_dev_1.sql
drop table if exists temp1;
create temp table temp1 as
select c.the_year, c.year_month, d.flpage as page, d.flflne as line, 
  d.fxmcol as col, d.fxmact as gm_account
from ( -- year and year_month
  select a.the_year, a.the_year * 100 + the_month as year_month
  from (
    select generate_series(2011, 2016, 1) as the_year) a
  cross join (
    select * from generate_Series(1, 12, 1) as the_month) b) c
left join ( -- pages and lines
  select a.*, b.fxmcol, b.fxmact
  from (
    select flcyy,flpage,flflne
    from dds.ext_eisglobal_sypfflout
    where trim(flcode) = 'GM'
      and flcyy > 2010
      and flflsq = 0
      and flflne > 0
    group by flcyy,flpage,flflne) a
  left join ( -- columns and gm_accounts  
    select fxmcyy, fxmact, fxmpge, fxmlne, fxmcol
    from dds.ext_eisglobal_sypffxmst
    where fxmcyy > 2010) b  on a.flcyy = b.fxmcyy and a.flpage = b.fxmpge and a.flflne = b.fxmlne) d on c.the_year = d.flcyy   
where d.fxmcol is not null 
  and c.year_month < 201609;

 drop table if exists temp2;
create temp table temp2 as
-- select b.*, c.store_code, c.g_l_acct_number, c.fact_account_, d.department, d.account_key
select b.*, 
  coalesce(c.store_code, 'none') as store_code, coalesce(c.g_l_acct_number, 'none') as gl_account, 
  coalesce(c.fact_account_, 1) as multiplier, d.department as gl_department, d.account_key
from temp1 b
left join (
  select factory_financial_year, 
  case
    when coalesce(consolidation_grp, '1')  = '1' then 'RY1'::citext
    when coalesce(consolidation_grp, '1')  = '2' then 'RY2'::citext
    else 'XXX'::citext
  end as store_code, g_l_acct_number, factory_account, fact_account_
  from dds.ext_ffpxrefdta a
  where coalesce(a.consolidation_grp, '1') <> '3'
    and a.factory_account <> '331A'
    and a.factory_code = 'GM'
    and factory_financial_year > 2010
    -- excludes the goofy XFCOPY factory accounts
    and g_l_acct_number is not null) c on b.the_year = c.factory_financial_year and b.gm_account = c.factory_account
    -- leave out parts splits for now
    -- parts split is part of the etl process not the data model
--     and g_l_acct_number not like 'SPLT%') c on b.the_year = c.factory_financial_year and b.gm_account = c.factory_account
left join fin.dim_account d on coalesce(c.g_l_acct_number, 'none') = d.account;    
-- eliminate the unused gm accounts, leave the formula accounts
delete from temp2 where store_code = 'none' and left(gm_account, 1) NOT in ('<','&','*'); 


 drop table if exists temp3;
create temp table temp3 as
select b.the_year, b.year_month, b.page, b.line, b.col, b.gm_account, b.store_code,
  b.gl_account, b.multiplier, b.gl_department, b.account_key, 
  coalesce(c.fs_org_key, cc.fs_org_key) as fs_org_key
from temp2 b 
left join fin.dim_fs_org c on b.store_code = c.store
  and
    case 
      when b.gl_department = 'Body Shop' then c.department = 'body shop'
      when b.gl_department = 'Car Wash' then c.sub_Department = 'car wash'
      when b.gl_department = 'Detail' then c.sub_department = 'detail'
      when b.gl_department = 'Finance' and b.line < 10 then c.department = 'finance' and c.sub_department = 'new'
      when b.gl_department = 'Finance' and b.line > 10 then c.department = 'finance' and c.sub_department = 'used'
      -- next 2 lines  correctly categorize these accounts as finance rather than sales, which is how they are categorized in COA
      when b.gl_department = 'New Vehicle' and b.page = 17 and b.line < 10 then c.department = 'finance' and c.sub_department = 'new'
      when b.gl_department = 'Used Vehicle' and b.page = 17 and b.line between 11 and 19 then c.department = 'finance' and c.sub_department = 'used'      
      when b.gl_department = 'New Vehicle' then c.department = 'sales' and c.sub_department = 'new'
      when b.gl_department = 'Parts' then c.department = 'parts'
      when b.gl_department = 'Quick Lane' then c.sub_department = 'pdq'
      when b.gl_department = 'Service' then c.sub_department = 'mechanical'
      when b.gl_department = 'Used Vehicle' then c.department = 'sales' and c.sub_department = 'used'
    end
left join fin.dim_fs_org cc on 1 = 1
  and cc.market = 'none';

update temp3
set page = 16,
    line = 49,
    col = 2
where gl_account = '147701';    

update temp3
set page = 16,
    line = 49,
    col = 3
where gl_account = '167701';   

drop table if exists temp4;
create temp table temp4 as
select a.the_year, a.year_month, 4 as page, 59 as line, 
  case 
    when a.gm_account in ('477s','677s') then 11 -- body shop
    else 1
  end as col, 
  a.gm_account, a.store_code, replace(a.gl_account,'SPLT','')::citext as gl_account,
  multiplier, b.department as gl_department, b.account_key, c.fs_org_key
-- select *  
from temp3 a 
left join fin.dim_account b on  replace(a.gl_account,'SPLT','') = b.account
left join fin.dim_fs_org c on a.store_code = c.store
  and
    case
      when a.gm_account in ('467S','667S', '468S','668S' ) then c.sub_department = 'mechanical'
      when a.gm_account in ('478s','678s') then c.sub_department = 'pdq'
      when a.gm_account in ('477s','677s') then c.department = 'body shop'
      when a.gl_account = '246700D' then c.sub_department = 'mechanical' 
      when a.gl_account = '246701D' then c.sub_department = 'pdq'
    end
where ((a.gm_account in('467s','667s','478s','678s','477s','677s','468s','668s'))
  or (a.gl_account in ('246700D','246701D')));

delete from temp3 where page = 4 and line = 59;
insert into temp3
select * from temp4 order by gl_account

select * from temp3 where page = 4 and line = 59

select * from temp3 where page = 16 and line = 48 and year_month = 201606


select the_year, gm_account, gl_account
from (
select the_year, page, line, col, gm_Account, gl_account, fs_org_key 
from temp3
group by the_year, page, line, col, gm_Account, gl_account, fs_org_key 
) x
group by the_year, gm_account, gl_account
having count(*) > 1

select *
from temp3
where gm_account like '477%' --in('147701', '167701', '246701D')
order by year_month, store_code, page


assuming that the gm/gl account routing only changes on an annual basis
not sure if that is true
i do believe it is true that line labels (sypfflout) and gm acct to P/L/C (sypffxmst) are annual

select gm_account
from (
select the_year, gl_account, gm_account
from dds.xfm_ffpxrefdta
group by the_year, gl_account, gm_account
) x
group by gm_account
having count(*) > 1

select * from dds.xfm_ffpxrefdta
where gm_account = '430j'
order by the_year


-- more than one gm_account per month/page/line/col
-- many are formula accounts, but not all
select m.*, n.*, o.gl_account
-- select *
from (
  select fxmcyy, fxmpge, fxmlne, fxmcol
  from (
    select fxmcyy,fxmact, fxmpge,fxmlne,fxmcol
    from dds.ext_eisglobal_sypffxmst
    where fxmcyy = 2016
      and fxmpge <> 1
    group by fxmcyy,fxmact, fxmpge,fxmlne,fxmcol) x
  group by fxmcyy, fxmpge, fxmlne, fxmcol
  having count(*) > 1) m
left join dds.ext_eisglobal_sypffxmst n on m.fxmcyy = n.fxmcyy  
  and m.fxmpge = n.fxmpge
  and m.fxmlne = n.fxmlne
  and m.fxmcol = n.fxmcol
left join dds.xfm_Ffpxrefdta o on m.fxmcyy = o.the_year
  and n.fxmact = o.gm_account  
order by m.fxmcyy, m.fxmpge, m.fxmlne, m.fxmcol, n.fxmact

-- and of course that persists to temp1
-- select year_month, page, line, col
-- from (
-- select year_month, page, line, col, gm_account 
-- from temp1
-- group by year_month, page, line, col, gm_account 
-- ) x
-- group by year_month,page,line,col
-- having count(*) > 1

select * from temp3
where year_month = 201608
limit 100

select count(*) from temp3

--280392
select the_year, year_month, page, line, col, gm_account, gl_account
from temp3
group by the_year, year_month, page, line, col, gm_account, gl_account

select page, line
from (
select page, line, trim(line_label)
from fin.fact_fs_layout
group by page,line,trim(line_label) order by page, line
) x
group by page, line
having count(*) > 1
order by page, line

-- oh duh, of course the pages are going to have more lines than account assignments
-- there are may lines with no account assignments 
select *
from (
  select the_year, page, count(*)
  from fin.fact_fs_layout
  group by the_year, page) e
left join (
select fxmcyy, fxmpge, count(*)
from (
  select fxmcyy, fxmpge, fxmlne
  from dds.ext_eisglobal_sypffxmst 
  group by fxmcyy, fxmpge, fxmlne) a
  group by fxmcyy, fxmpge) f on e.the_year = f.fxmcyy and e.page = f.fxmpge
order by e.page, e.the_year


-- 9/9/16
-- here i go again, starting over without a clear sense of where i am going
-- so god damnit, stop 
-- i so fucking smuggly tell afton that we need to take a breath and think things thru
-- so
select * from temp3 
limit 100

select page, line, col, gm_account, gl_account 
from temp3 
where year_month = 201608
  and page < 18
group by page, line, col, gm_account, gl_account 
order by page, line, col

-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
9/10 starting over with new model, 

select * 
from temp1
where year_month = 201608
order by page, line, col

select *
from temp3
where year_month = 201606
and page = 4
and line = 59
and col = 11


select * from temp3 where store_code = 'ry2' and page= 4 and line = 59 and year_month = 201606

select * from temp3 where gl_account like 'SP%'

drop table if exists june;
create temp table june as
select a.*, 
  round( -- this is the parts split
    case 
      when a.page = 4 and a.line = 59 and a.gl_account not in ('147701','167701') then round(coalesce(c.amount, 0) * multiplier, 0) 
      else coalesce(c.amount, 0)
    end, 0) as amount  
from temp3 a
left join fin.br_fs_account b on a.gm_account = b.gm_account
  and a.account_key = b.account_key
left join (
  select b.yearmonth, a.account_key, sum(a.amount) as amount
  from fin.fact_gl a
  inner join dds.day b on a.date_key = b.datekey  
    and b.yearmonth = 201606
  where a.post_status = 'Y'
  group by b.yearmonth, a.account_key) c on b.account_key = c.account_key and a.year_month = c.yearmonth
where a.year_month = 201606  
where gl_account not like 'SPLT%'  ; 

select sum(amount) from june where page = 4 and store_code = 'ry2' and line = 59

select store_code, col, sum(amount) from june where page = 4 and line = 59 group by store_code, col

select * from june where page = 4 and store_code = 'ry2' and line = 59 order by gl_account

select * from temp1 where year_month = 201606 and page = 4 and line = 59

select * from temp2 where year_month  = 201606 and page = 4 and line = 59 and store_code = 'ry2'


select col, sum(amount) from june where page = 16 and store_code = 'ry2' and line = 48 group by col 

select * from june where gl_account in ('246700d','246701d')
