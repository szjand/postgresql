﻿-- 202305 toyota new, fs = 29, step_1 = 28
-- fs includes 1 vehicle on line 10 (418J), query does not
-- looks like it is a crown T10673
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count, count(*) over (partition by line)
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 202305-----------------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 202305 ---------------------------------------------------------------------
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
--         or
--         (b.page = 17 and b.line between 1 and 20))-- f/i counts don't make sense
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y'
		and d.store = 'ry8'
		and page = 13
  order by control page, line

select * from fin.dim_fs_account where gm_account = '418j'
418J,4139
418J,4137
418J,4136
418J,4134
select * from fin.dim_Account where account in ('4139','4137','4136','4134')

the issue appears to be account 4134, deal is on 5/18/23, type 2 update on 5/27/23, so account not current at time of deal 
-- thought this would fix it, it did not
update fin.dim_account
set row_from_date = '05/01/2023'
where account_key = 4309;
update fin.dim_account
set row_thru_date = '04/30/2023'
where account_key = 3476;

select c.the_date, a.*, b.*
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
  and b.account = '4134'
join dds.dim_date c on a.date_key = c.date_key


  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count, count(*) over (partition by line)
select a.*, c.account, b.the_date   
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 202305-----------------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
-- -- add journal
--   inner join fin.dim_journal aa on a.journal_key = aa.journal_key
--     and aa.journal_code in ('VSN','VSU')
--   inner join ( -- d: fs gm_account page/line/acct description
--     select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
--     from fin.fact_fs a
--     inner join fin.dim_fs b on a.fs_key = b.fs_key
--       and b.year_month = 202305 ---------------------------------------------------------------------
--       and (
--         (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
--         or
--         (b.page = 16 and b.line between 1 and 14)) -- used cars
-- --         or
-- --         (b.page = 17 and b.line between 1 and 20))-- f/i counts don't make sense
--     inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
--     inner join fin.dim_account e on d.gl_account = e.account
--       and e.account_type_code = '4'
--       and e.current_row
--     inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y'
		and a.control = 'T10673'

-- fact_gl does not return account 4134 ?!?!?  trans 1032350 seq 9
the issue is when i updated dim_account, the account_key 3476 (which is in fact_gl) is no longer the current row
so, i am going to change the account key in fact_gl

and that fixed it
update fin.fact_gl
set account_key = 4309
where trans = 1032350
  and seq = 9
  
select * from fin.dim_account where account = '4134'






select * from arkona.ext_ffpxrefdta where factory_financial_year = 2023 and factory_account = '418j'

select store, page, line, line_label, control, sum(unit_count) as unit_count
  from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 202305-----------------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 202305 ---------------------------------------------------------------------
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
--         or
--         (b.page = 17 and b.line between 1 and 20))-- f/i counts don't make sense
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y'
		and d.store = 'ry8'
		and page = 13  ) h
group by store, page, line, line_label, control
order by store, page, line;