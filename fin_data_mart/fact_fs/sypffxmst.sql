﻿---------------------------------------------------------------
--< 03/22/22
---------------------------------------------------------------
PRIMARY KEY(fxmcyy, fxmact, fxmpge, fxmlne)

select 'tmp', * from arkona.ext_eisglobal_sypffxmst_tmp where fxmcyy = 2022 and fxmpge = 16 and fxmlne = 20.0
union all
select 'base', * from arkona.ext_eisglobal_sypffxmst where fxmcyy = 2022 and fxmpge = 16 and fxmlne = 20.0


update arkona.ext_eisglobal_sypffxmst z
set fxmstr = x.fxmstr, fxmcol = x.fxmcol
from (
	select *
	from arkona.ext_eisglobal_sypffxmst_tmp
	except
	select *
	from arkona.ext_eisglobal_sypffxmst) x
where z.fxmcyy = x.fxmcyy
  and z.fxmact = x.fxmact
  and z.fxmpge = x.fxmpge
  and z.fxmlne = x.fxmlne;	

delete 
-- select *
from arkona.ext_eisglobal_sypffxmst
where fxmcyy = 2022
  and fxmact in ('660A')
  and fxmpge = 16
  and fxmlne = 20.0

---------------------------------------------------------------
--< 03/19/22
---------------------------------------------------------------
created this new file with bits and pieces relevant to arkona.ext_eisglobal_sypffxmst from sypffxmst_sypfflout.sql
because today, this query generated 3971 !?!? (same kind of thing with sypfflout)
this file will have the most recent queries on top
select count (*)
from ((
    select *
    from arkona.ext_eisglobal_sypffxmst
    except
    select *
    from arkona.ext_eisglobal_sypffxmst_tmp)
  union (
    select *
    from arkona.ext_eisglobal_sypffxmst_tmp
    except
    select *
    from arkona.ext_eisglobal_sypffxmst)) x

-- looks like fxmstr is the value that has changed
select * 
from ((
    select *
    from arkona.ext_eisglobal_sypffxmst
    except
    select *
    from arkona.ext_eisglobal_sypffxmst_tmp)
  union (
    select *
    from arkona.ext_eisglobal_sypffxmst_tmp
    except
    select *
    from arkona.ext_eisglobal_sypffxmst)) x
order by fxmcyy, fxmact,fxmpge,fxmlne

-- this fixes the fxmstr issue, 1985 rows
update arkona.ext_eisglobal_sypffxmst z
set fxmstr = x.fxmstr
from (
	select *
	from arkona.ext_eisglobal_sypffxmst_tmp
	except
	select *
	from arkona.ext_eisglobal_sypffxmst) x
where z.fxmcyy = x.fxmcyy
  and z.fxmact = x.fxmact
  and z.fxmpge = x.fxmpge
  and z.fxmlne = x.fxmlne;	

-- leaving 1 new row
select *
from arkona.ext_eisglobal_sypffxmst_tmp
except
select *
from arkona.ext_eisglobal_sypffxmst     

insert into arkona.ext_eisglobal_sypffxmst    
select *
from arkona.ext_eisglobal_sypffxmst_tmp
except
select *
from arkona.ext_eisglobal_sypffxmst;  

---------------------------------------------------------------
--< 03/19/22
---------------------------------------------------------------
    
-- 1/3/2020  new year new rows
-- 1/8/2021  new year new rows
-- 1/3/2022
select fxmcyy, count(*) from arkona.ext_eisglobal_sypffxmst group by fxmcyy order by fxmcyy

insert into arkona.ext_eisglobal_sypffxmst
select *
from arkona.ext_eisglobal_sypffxmst_tmp a
where not exists (
  select 1
  from arkona.ext_eisglobal_sypffxmst 
  where fxmcyy = a.fxmcyy
    and fxmact = a.fxmact
    and a.fxmpge = a.fxmpge 
    and fxmlne = a.fxmlne)

---------------------------------------------------------------
--< 6/11/19 sypffxmst
---------------------------------------------------------------
select count (*)
from ((
    select *
    from arkona.ext_eisglobal_sypffxmst
    except
    select *
    from arkona.ext_eisglobal_sypffxmst_tmp)
  union (
    select *
    from arkona.ext_eisglobal_sypffxmst_tmp
    except
    select *
    from arkona.ext_eisglobal_sypffxmst)) x

-- fxmact <M220CD fxmlne change from 57.0 to 55.0
select 'new', *
from arkona.ext_eisglobal_sypffxmst_tmp
where fxmact = '<M220CD'
and fxmcyy = 2019
union
select 'old', *
from arkona.ext_eisglobal_sypffxmst
where fxmact = '<M220CD'
and fxmcyy = 2019

update arkona.ext_eisglobal_sypffxmst
set fxmlne = 55.0
where fxmact = '<M220CD'
  and fxmcyy = 2019;
---------------------------------------------------------------
--/> 6/11/19 sypffxmst
---------------------------------------------------------------    

---------------------------------------------------------------
--< 2/14/19 sypffxmst
---------------------------------------------------------------
select count (*)
-- select * 
from ((
    select *
    from arkona.ext_eisglobal_sypffxmst
    except
    select *
    from arkona.ext_eisglobal_sypffxmst_tmp)
  union (
    select *
    from arkona.ext_eisglobal_sypffxmst_tmp
    except
    select *
    from arkona.ext_eisglobal_sypffxmst)) x

-- count = 1
-- think this means a row has been removed
select *
from arkona.ext_eisglobal_sypffxmst
except
select *
from arkona.ext_eisglobal_sypffxmst_tmp

select * -- no rows
from arkona.ext_eisglobal_sypffxmst_tmp
where fxmcyy = 2019
  and fxmact = '<TFG'

select * -- yep, row no longer exists
from arkona.ext_eisglobal_sypffxmst_tmp
where fxmcyy = 2019
  and fxmpge = 3
  and fxmlne = 12.0
  
delete from arkona.ext_eisglobal_sypffxmst
where fxmcyy = 2019
  and fxmact = '<TFG'
  and fxmpge = 3 
  and fxmlne = 12.0;
---------------------------------------------------------------
--/> 2/14/19 sypffxmst
---------------------------------------------------------------