﻿fact_fs
  fs_key ------------- dim_fs
                         fs_key
                         the_year
                         year_month
                         page
                         line
                         col
                         line_label
  fs_org_key --------- dim_fs_org
                         fs_org_key
                         market
                         store
                         area
                         department
                         sub_department
  fs_account_key ----- dim_fs_account
                         fs_account_key
                         gm_account
                         gl_account
  amount

loads fin.fact_fs with data 201101 -> 201606


  
--insert into fin.dim_fs_account_1 (the_year,year_month,page,line,col,gm_account,gl_account)
-- this is the big fucking query that i thought would be used to generate the account dimension
-- drop table if exists test;
-- create temp table test as
drop table if exists fin.xfm_fs_1 cascade;
create table if not exists fin.xfm_fs_1 (
  the_year integer not null,
  year_month integer not null,
  page integer not null,
  line numeric (4,1) not null,
  col integer not null,
  gm_account citext not null,
  gl_account citext not null,
  constraint xfm_fs_1_nk unique(year_month,gm_account,gl_account,page,line,col));
insert into fin.xfm_fs_1  
select aa.the_year, aa.year_month,--, b.store_code,
  case -- 147701/167701 100% body shop, no parts split
    when b.g_l_acct_number in ('147701', '167701') then 16
    else a.fxmpge
  end as page, 
  case 
    when b.g_l_acct_number in ('147701', '167701') then 49
    else a.fxmlne
  end as line, 
  case 
    when b.g_l_acct_number = '147701' then 2
    when b.g_l_acct_number = '167701' then 3
    else a.fxmcol
  end as col, a.fxmact, 
  coalesce(
    case 
      when b.g_l_acct_number like '%SPLT%' then replace(b.g_l_acct_number,'SPLT','')::citext
      else b.g_l_acct_number
    end, 'none') as gl_account
from ( -- year, year_month 201101 thru 201606
  select a.the_year, a.the_year * 100 + the_month as year_month
  from ( -- 201101 thru 201512
    select generate_series(2011, 2015, 1) as the_year) a
    cross join (
    select * from generate_Series(1, 12, 1) as the_month) b  
  union
  select a.the_year, a.the_year * 100 + the_month as year_month
  from ( --201601 thru 201606
    select 2016 as the_year) a
    cross join (
    select * from generate_Series(1, 6, 1) as the_month) b ) aa    
left join arkona.ext_eisglobal_sypffxmst a on aa.the_year = a.fxmcyy
left join (
  select factory_financial_year, 
  case
    when coalesce(consolidation_grp, '1')  = '1' then 'RY1'::citext
    when coalesce(consolidation_grp, '1')  = '2' then 'RY2'::citext
    else 'XXX'::citext
  end as store_code, g_l_acct_number, factory_account, fact_account_
  from arkona.ext_ffpxrefdta) b on a.fxmcyy = b.factory_financial_year and a.fxmact = b.factory_account
where a.fxmpge < 18;

-- parts split

drop table if exists parts_split;
create temp table parts_split as  
select a.the_year, a.year_month, 4 as page, 59.0 as line, 
  case 
    when a.gm_account in ('477s','677s') then 11 -- body shop
    else 1
  end as col, 
  a.gm_account, replace(a.gl_account,'SPLT','')::citext as gl_account
-- select *  
from fin.xfm_fs_1 a
where ((a.gm_account in('467s','667s','478s','678s','477s','677s','468s','668s'))
  or (a.gl_account in ('246700D','246701D'))) order by gl_Account;  

delete from fin.xfm_fs_1
where page = 4 
  and line = 59;

insert into fin.xfm_fs_1 (the_year,year_month,page,line,col,gm_account,gl_account) 
select *
from parts_split;  

-- -- -- drop table if exists fin.xfm_fs_2 cascade;
-- -- -- create table if not exists fin.xfm_fs_2 (
-- -- --   fs_key integer not null,
-- -- --   fs_account_key integer not null,
-- -- --   fs_org_key integer not null,
-- -- --   constraint xfm_fs_2_nk unique(fs_key,fs_account_key,fs_org_key));
-- -- -- insert into fin.xfm_fs_2  
-- -- -- select b.fs_key, c.fs_account_key, coalesce(e.fs_org_key, ee.fs_org_key) as fs_org_key
-- -- -- from fin.xfm_fs_1 a
-- -- -- left join fin.dim_fs b on a.year_month = b.year_month
-- -- --   and a.page = b.page
-- -- --   and a.line = b.line
-- -- --   and a.col = b.col
-- -- -- left join fin.dim_fs_account c on a.gm_account = c.gm_account 
-- -- --   and a.gl_account = c.gl_account  
-- -- -- left join fin.dim_account d on c.gl_account = d.account 
-- -- -- left join fin.dim_Fs_org e on d.store_code = e.store
-- -- --   and
-- -- --     case -- parts split
-- -- --       when b.page = 4 and b.line = 59 then 
-- -- --         case
-- -- --           when c.gm_account in ('467S','667S', '468S','668S' ) then e.sub_department = 'mechanical'
-- -- --           when c.gm_account in ('478s','678s') then e.sub_department = 'pdq'
-- -- --           when c.gm_account in ('477s','677s') then e.department = 'body shop'
-- -- --           when c.gl_account = '246700D' then e.sub_department = 'mechanical' 
-- -- --           when c.gl_account = '246701D' then e.sub_department = 'pdq'
-- -- --         end 
-- -- --       else   
-- -- --         case 
-- -- --           when d.department = 'Body Shop' then e.department = 'body shop'
-- -- --           when d.department = 'Car Wash' then e.sub_Department = 'car wash'
-- -- --           when d.department = 'Detail' then e.sub_department = 'detail'
-- -- --           when d.department = 'Finance' and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
-- -- --           when d.department = 'Finance' and b.line > 10 then e.department = 'finance' and e.sub_department = 'used'
-- -- --           -- next 2 lines  correctly categorize these accounts as finance rather than sales, which is how they are categorized in COA
-- -- --           when d.department = 'New Vehicle' and b.page = 17 and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
-- -- --           when d.department = 'Used Vehicle' and b.page = 17 and b.line between 11 and 19 then e.department = 'finance' and e.sub_department = 'used'      
-- -- --           when d.department = 'New Vehicle' then e.department = 'sales' and e.sub_department = 'new'
-- -- --           when d.department = 'Parts' then e.department = 'parts'
-- -- --           when d.department = 'Quick Lane' then e.sub_department = 'pdq'
-- -- --           when d.department = 'Service' then e.sub_department = 'mechanical'
-- -- --           when d.department = 'Used Vehicle' then e.department = 'sales' and e.sub_department = 'used'
-- -- --         end
-- -- --     end
-- -- -- left join fin.dim_fs_org ee on 1 = 1
-- -- --   and ee.market = 'none';
-- 
--
-- 
-- this is working
-- select * from fin.xfm_fs_1 limit 100
drop table if exists fin.fact_fs;
create table fin.fact_fs (
  fs_key integer not null references fin.dim_fs(fs_key),
  fs_org_key integer not null references fin.dim_fs_org(fs_org_key),
  fs_account_key integer not null references fin.dim_fs_account(fs_account_key),
  amount integer not null,
  constraint fact_fs_nk unique(fs_key,fs_org_key,fs_account_key));
create index on fin.fact_fs(fs_key);
create index on fin.fact_fs(fs_org_key);
create index on fin.fact_fs(fs_account_key);
  
-- select --a.*, 
-- --   d.store_code, d.department, e.store, e.area, e.department, e.sub_department,
-- --   b.fs_key, c.fs_account_key, e.fs_org_key,
-- select * from fs limit 100
-- drop table if exists fs;
-- create temp table fs as 
insert into fin.fact_fs
-- drop table if exists fs;
-- create temp table fs as 
select b.fs_key, coalesce(e.fs_org_key, ee.fs_org_key) as fs_org_key, c.fs_account_key, 
  round( -- this is the parts split
    sum(
      case 
        when b.page = 4 and b.line = 59 and c.gl_account not in ('147701','167701') then round(coalesce(f.amount, 0) * .5, 0) 
        else coalesce(f.amount, 0)
      end), 0) as amount  
from fin.xfm_fs_1 a
left join fin.dim_fs b on a.year_month = b.year_month
  and a.page = b.page
  and a.line = b.line
  and a.col = b.col
left join fin.dim_fs_account c on a.gm_account = c.gm_account
  and a.gl_account = c.gl_account  
left join fin.dim_account d on c.gl_account = d.account  
left join fin.dim_Fs_org e on d.store_code = e.store
  and
    case -- parts split
      when b.page = 4 and b.line = 59 then 
        case
          when c.gm_account in ('467S','667S', '468S','668S' ) then e.sub_department = 'mechanical'
          when c.gm_account in ('478s','678s') then e.sub_department = 'pdq'
          when c.gm_account in ('477s','677s') then e.department = 'body shop'
          when c.gl_account = '246700D' then e.sub_department = 'mechanical' 
          when c.gl_account = '246701D' then e.sub_department = 'pdq'
        end 
      else   
        case 
          when d.department = 'Body Shop' then e.department = 'body shop'
          when d.department = 'Car Wash' then e.sub_Department = 'car wash'
          when d.department = 'Detail' then e.sub_department = 'detail'
          when d.department = 'Finance' and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
          when d.department = 'Finance' and b.line > 10 then e.department = 'finance' and e.sub_department = 'used'
          -- next 2 lines  correctly categorize these accounts as finance rather than sales, which is how they are categorized in COA
          when d.department = 'New Vehicle' and b.page = 17 and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
          when d.department = 'Used Vehicle' and b.page = 17 and b.line between 11 and 19 then e.department = 'finance' and e.sub_department = 'used'      
          when d.department = 'New Vehicle' then e.department = 'sales' and e.sub_department = 'new'
          when d.department = 'Parts' then e.department = 'parts'
          when d.department = 'Quick Lane' then e.sub_department = 'pdq'
          when d.department = 'Service' then e.sub_department = 'mechanical'
          when d.department = 'Used Vehicle' then e.department = 'sales' and e.sub_department = 'used'
        end
    end
left join (
  select b.yearmonth, aa.account, sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_account aa on a.account_key = aa.account_key
  inner join dds.day b on a.date_key = b.datekey  
  where a.post_status = 'Y'
    and b.yearmonth between 201101 and 201606
  group by b.yearmonth, aa.account) f on a.gl_account = f.account
    and b.year_month = f.yearmonth    
left join fin.dim_fs_org ee on 1 = 1
  and ee.market = 'none'    
where a.year_month between 201101 and 201606
group by a.year_month, b.fs_key, c.fs_account_key, 
  coalesce(e.fs_org_key, ee.fs_org_key); 


-- 201606 parts split ok
select b.year_month, c.store, b.col, sum(a.amount) june
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201606
  and b.page = 4
  and b.line = 59
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
group by b.year_month, c.store, b.col
having sum(a.amount) <> 0

-- budget comparison
-- ry1 2016 sales total expenses
select year_month, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201601 and 201606
  and b.page = 3
--   and b.line = 59
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
  and c.store = 'ry1' 
  and c.area = 'variable' 
group by year_month
having sum(amount) <> 0  
order by year_month

-- ry2 2016 sales total expenses
select year_month, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201601 and 201606
  and b.page = 3
--   and b.line = 59
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
  and c.store = 'ry2' 
  and c.area = 'variable' 
group by year_month
having sum(amount) <> 0  
order by year_month



-- fucking A it all looks good
select year_month, store, department, sub_department,
  sum(case when line between 8 and 17 then amount end) as personnel,
  sum(case when line between 18 and 40 then amount end) as semifixed,
  sum(case when line between 41 and 56 then amount end) fixed,
  sum(case when line between 8 and 56 then amount end) total
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201601 and 201606
  and b.page = 4
  and b.line < 58 -- expenses only, exclude parts split
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
group by year_month, store, department, sub_department
having sum(amount) <> 0  
order by year_month, store, department, sub_department

-- budget gross
-- sales 
-- GOOD
select c.store, b.year_month, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201601 and 201606
  and (
    (b.page between 5 and 15) -- new cars
    or
    (b.page = 16 and b.line between 1 and 14) -- used cars
    or
    (b.page = 17 and b.line between 1 and 20))-- f/i
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
group by c.store, b.year_month
having sum(amount) <> 0
order by c.store, b.year_month;

-- budget gross
-- fixed
-- all except parts
select b.year_month, c.store, c.department, c.sub_department, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201601 and 201606
  and (
    (b.page = 16 and b.line between 20 and 42)
    or
    (b.page = 4 and b.line = 59)) -- parts split
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.area = 'fixed'
group by b.year_month, c.store, c.department, c.sub_department
having sum(amount) <> 0
order by c.store, c.department, c.sub_department, b.year_month;   

-- parts
select b.year_month, c.store, 
  sum(case when b.page = 4 then -1 * a.amount else a.amount end)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201601 and 201606
  and (
    (b.page = 16 and b.line between 45 and 61)
    or
    (b.page = 4 and b.line = 59)) -- parts split
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
group by b.year_month, c.store
having sum(amount) <> 0
order by c.store, b.year_month

-- -- -- 9/27 why this
-- -- most have 0 amounts, but several have amounts
-- -- P3 L64 - L67
-- -- select *
-- -- from june
-- -- where store = 'none'
-- -- the issue is the department code in glpmast, the relevant accounts are department General
-- -- for now, this goes in the formula accounts bin, to be modeled at a later date
-- -- select *
-- -- from fin.xfm_fs_1 a
-- -- left join fin.dim_account b on a.gl_account = b.account
-- -- where page = 3 and line between 64 and 67
-- -- and year_month = 201606
