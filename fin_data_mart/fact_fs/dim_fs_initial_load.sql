﻿
-- dim_fs 
-- line_label = '' when there is no label
-- col = -1 when there is no column, ie, no row in sypffxmst for that page/line, no gm account assigned 
-- this replaces fact_fs_layout
delete from ops.task_log where task = 'fact_fs_layout';
delete from ops.tasks cascade where task = 'fact_fs_layout' ;
insert into ops.tasks values('eisglobal_sypfflout', 'Daily');


drop table if exists fin.dim_fs cascade;
create table if not exists fin.dim_fs (
  fs_key serial primary key,
  the_year integer not null,
  year_month integer not null, 
  page integer not null,
  line numeric(4,1) not null,
  col integer not null, 
  line_label citext not null,
  row_from_date date not null,
  row_thru_date date default '12/31/9999' not null,
  current_row boolean not null,
  row_reason citext not null default 'New Row',
constraint dim_fs_nk unique(year_month, page, line, col));
create index on fin.dim_fs(the_year); 
create index on fin.dim_fs(year_month) ;
create index on fin.dim_fs(page);
create index on fin.dim_fs(line);
create index on fin.dim_fs(col);
create index on fin.dim_fs(line_label);
comment on table fin.dim_fs is
'line_label = an empty string when there is no label
col = -1 when there is no column, ie, no row in sypffxmst for that page/line, no gm account assigned
gm financial statement page 1 thru 17';

comment on column fin.dim_fs.fs_key is 'source: system generated surrogate key';
comment on column fin.dim_fs.the_year is 'source: script generated';
comment on column fin.dim_fs.year_month is 'source: script generated';
comment on column fin.dim_fs.page is 'source: arkona.ext_eisglobal_sypfflout.flpage';
comment on column fin.dim_fs.line is 'source: arkona.ext_eisglobal_sypfflout.flflne';
comment on column fin.dim_fs.col is 'source: arkona.ext_eisglobal_sypffxmst.fxmcol';
comment on column fin.dim_fs.line_label is 'source: arkona.ext_eisglobal_sypfflout.fldata';

-- initial load 201101 -> 201606
insert into fin.dim_fs (the_year,year_month,page,line,col,line_label, row_from_date, current_row)  
select aa.the_year, aa.year_month, flpage, flflne, coalesce(c.fxmcol, -1) as col,
  case -- multi valued labels with large internal block of spaces
    when line_label = 'ASSETS                 AMOUNT' 
      then 'ASSETS'
    when line_label = 'EXPENSES                                                                                                 NEW SALES/PN'
      then 'EXPENSES'
    else line_label
  end as line_label, current_date, true
from ( -- year, year_month 201101 thru 201607
  select a.the_year, a.the_year * 100 + the_month as year_month
  from ( -- 201101 thru 201512
    select generate_series(2011, 2015, 1) as the_year) a
    cross join (
    select * from generate_Series(1, 12, 1) as the_month) b  
  union
  select a.the_year, a.the_year * 100 + the_month as year_month
  from ( --201601 thru 201606
    select 2016 as the_year) a
    cross join (
    select * from generate_Series(1, 6, 1) as the_month) b ) aa
left join ( -- year, page, line, line_label
  select flcyy, flpage, flflne, trim(left(line_label, position('|' in line_label) - 1))::citext as line_label
  from (
    select flcyy, flpage, flflne,       
    case
      when left(fldata, 1) = '|' then right(fldata, length(fldata) - 1)
      else fldata
    end as line_label
    from arkona.ext_eisglobal_sypfflout
--     where flcode = 'GM'
--       and flcyy > 2010 
    where flflsq = 0 
      and flflne > 0
      and flpage < 18) a
  group by flcyy, flpage, flflne, trim(left(line_label, position('|' in line_label) - 1))) b on aa.the_year = b.flcyy
left join ( -- columns
  select fxmcyy, fxmpge, fxmlne, fxmcol
  from arkona.ext_eisglobal_sypffxmst
--   where fxmcyy > 2010
  group by fxmcyy, fxmpge, fxmlne, fxmcol) c  on b.flcyy = c.fxmcyy and b.flpage = c.fxmpge and b.flflne = c.fxmlne;
