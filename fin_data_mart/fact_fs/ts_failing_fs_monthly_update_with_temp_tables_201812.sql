﻿drop table if exists dim_fs;
create temp table dim_fs as
select 20918 as the_year, 201812 as year_month, flpage as page, flflne as line, 
  coalesce(c.fxmcol, -1) as col,
  case -- multi valued labels with large internal block of spaces
    when line_label = 'ASSETS                 AMOUNT' 
      then 'ASSETS'
    when line_label = 'EXPENSES                                                                                                 NEW SALES/PN'
      then 'EXPENSES'
    else line_label
  end as line_label, current_date, true
from ( -- year, page, line, line_label
  select flcyy, flpage, flflne, trim(left(line_label, position('|' in line_label) - 1))::citext as line_label
  from (
    select flcyy, flpage, flflne,       
    case
      when left(fldata, 1) = '|' then right(fldata, length(fldata) - 1)
      else fldata
    end as line_label
    from arkona.ext_eisglobal_sypfflout
    where flflsq = 0 
      and flflne > 0
      and flpage < 18) a
  group by flcyy, flpage, flflne, trim(left(line_label, position('|' in line_label) - 1))) b
left join ( -- columns
  select fxmcyy, fxmpge, fxmlne, fxmcol
  from arkona.ext_eisglobal_sypffxmst
--   where fxmcyy > 2010
  group by fxmcyy, fxmpge, fxmlne, fxmcol) c  on b.flcyy = c.fxmcyy and b.flpage = c.fxmpge and b.flflne = c.fxmlne
where b.flcyy = 2018; 




-- insert into fin.xfm_fs_1  
-- -- *d*
drop table if exists xfm_fs_1;
create temp table xfm_Fs_1 as
select x._year,x._year_month, x.page, x.line, x.col, x.fxmact as gm_account, x.gl_account
from (
  select 2018 as _year, 201812 as _year_month,
    case -- 147701/167701 100% body shop, no parts split
      when b.g_l_acct_number in ('147701', '167701') then 16
      else a.fxmpge
    end as page, 
    case 
      when b.g_l_acct_number in ('147701', '167701') then 49
      else a.fxmlne
    end as line, 
    case 
      when b.g_l_acct_number = '147701' then 2
      when b.g_l_acct_number = '167701' then 3
      else a.fxmcol
    end as col, a.fxmact, 
    coalesce(
      case 
        when b.g_l_acct_number like '%SPLT%' then replace(b.g_l_acct_number,'SPLT','')::citext
        else b.g_l_acct_number
      end, 'none') as gl_account
  from arkona.ext_eisglobal_sypffxmst a
  -- *a*
  inner join (
    select factory_financial_year, 
    case
      when coalesce(consolidation_grp, '1')  = '1' then 'RY1'::citext
      when coalesce(consolidation_grp, '1')  = '2' then 'RY2'::citext
      else 'XXX'::citext
    end as store_code, g_l_acct_number, factory_account, fact_account_
    from arkona.ext_ffpxrefdta) b on a.fxmcyy = b.factory_financial_year and a.fxmact = b.factory_account
  where a.fxmpge < 18
    and a.fxmcyy = 2018) x
group by x._year,x._year_month, x.page, x.line, x.col, x.fxmact, x.gl_account;

select * from xfm_Fs_1

drop table if exists parts_split;
create temp table parts_split as
select a._year, a._year_month, 4 as page, 59.0 as line, 
  case 
    when a.gm_account in ('477s','677s') then 11 -- body shop
    else 1
  end as col, 
  a.gm_account, replace(a.gl_account,'SPLT','')::citext as gl_account
-- select *  
from xfm_fs_1 a
-- *e*
where ((a.gm_account in('467s','667s','478s','678s','477s','677s','468s','668s'))
  or (a.gl_account in ('146700D','246700D','246701D'))) order by gl_Account;  

delete from xfm_fs_1
where page = 4 
  and line = 59;


insert into xfm_fs_1 (_year,_year_month,page,line,col,gm_account,gl_account) 
select *
from parts_split;  





drop table if exists fact_fs;
create temp table fact_fs as



select b.fs_key, coalesce(e.fs_org_key, ee.fs_org_key) as fs_org_key, c.fs_account_key, 
  round( -- this is the parts split
    sum(
      case 
        when b.page = 4 and b.line = 59 and c.gl_account not in ('147701','167701') 
          then round(coalesce(f.amount, 0) * .5, 0) 
        else coalesce(f.amount, 0)
      end), 0) as amount  
from xfm_fs_1 a
left join fin.dim_fs b on a._year_month = b.year_month
  and a.page = b.page
  and a.line = b.line
  and a.col = b.col
left join fin.dim_fs_account c on a.gm_account = c.gm_account
  and a.gl_account = c.gl_account  
left join fin.dim_account d on c.gl_account = d.account  
-- *c*
  and d.current_row = true
left join fin.dim_Fs_org e on d.store_code = e.store
  and
    case -- parts split
      when b.page = 4 and b.line = 59 then 
        case
          when c.gm_account in ('467S','667S', '468S','668S' ) then e.sub_department = 'mechanical'
          when c.gm_account in ('478s','678s') then e.sub_department = 'pdq'
          when c.gm_account in ('477s','677s') then e.department = 'body shop'
          when c.gl_account = '146700D' then e.sub_department = 'mechanical'
          when c.gl_account = '246700D' then e.sub_department = 'mechanical' 
          when c.gl_account = '246701D' then e.sub_department = 'pdq'
        end 
      else   
        case 
          when d.department = 'Body Shop' then e.department = 'body shop'
          when d.department = 'Car Wash' then e.sub_Department = 'car wash'
          when d.department = 'Detail' then e.sub_department = 'detail'
          when d.department = 'Finance' and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
          when d.department = 'Finance' and b.line > 10 then e.department = 'finance' and e.sub_department = 'used'
          -- next 2 lines  correctly categorize these accounts as finance rather than sales, which is how they are categorized in COA
          when d.department = 'New Vehicle' and b.page = 17 and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
          when d.department = 'Used Vehicle' and b.page = 17 and b.line between 11 and 19 then e.department = 'finance' and e.sub_department = 'used'      
          when d.department = 'New Vehicle' then e.department = 'sales' and e.sub_department = 'new'
          when d.department = 'Parts' then e.department = 'parts'
          when d.department = 'Quick Lane' then e.sub_department = 'pdq'
          when d.department = 'Service' then e.sub_department = 'mechanical'
          when d.department = 'Used Vehicle' then e.department = 'sales' and e.sub_department = 'used'
-- *b*          
          when d.department = 'Auto Outlet' then e.department = 'sales' and e.sub_department = 'used'
-- *e*
          when d.department = 'National' then e.department = 'national'
          when d.department = 'General' then e.area = 'general'
        end
    end
left join (
  select b.yearmonth, aa.account, sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_account aa on a.account_key = aa.account_key
-- *f*
--     and aa.current_row = true
  inner join dds.day b on a.date_key = b.datekey  
  where a.post_status = 'Y'
    and b.yearmonth = 201812
  group by b.yearmonth, aa.account) f on a.gl_account = f.account
    and b.year_month = f.yearmonth    
left join fin.dim_fs_org ee on 1 = 1
  and ee.market = 'none'    
group by a._year_month, b.fs_key, c.fs_account_key, 
  coalesce(e.fs_org_key, ee.fs_org_key) having sum(amount) <> 0; 


select * 
from fact_fs 
where amount = -250969

select *
from fin.dim_fs
where fs_key = 790748


page 1 line 55 col1 company vehicles


  