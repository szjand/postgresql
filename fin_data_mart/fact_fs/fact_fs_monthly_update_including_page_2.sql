﻿/*
02/28/2021
  on 02/04/21 i let pgadmin overwrite the contents of this script without noticing and before it was in git
  found the contents on 173 in /var/log/postgresql/postgresql-9.6-main.log.3.gz
  the "only" thing lost was a ton of comments
  found the queries at the end from e\sql\postgresql\fin_data_mart\fact_fs\P2_monthly_update.sql

09/03/2022
 
*/
	do
	$$
	declare 
	   _year integer := 2023; --------------------------------------------------------------
	   _year_month integer := 202312; ------------------------------------------------------
	begin
	
	delete from fin.fact_fs
	where fs_key in (
	  select fs_key
	  from fin.dim_fs 
	  where year_month = _year_month);
	  
	delete from fin.dim_fs where year_month = _year_month;
	
	insert into fin.dim_fs (the_year,year_month,page,line,col,line_label, row_from_date, current_row)  
	select _year, _year_month, flpage, flflne, coalesce(c.fxmcol, -1) as col,
	  case -- multi valued labels with large internal block of spaces
	    when line_label = 'ASSETS                 AMOUNT' 
	      then 'ASSETS'
	    when line_label = 'EXPENSES                                                                                                 NEW SALES/PN'
	      then 'EXPENSES'
	    else line_label
	  end as line_label, current_date, true
	from ( -- year, page, line, line_label
	  select flcyy, flpage, flflne, trim(left(line_label, position('|' in line_label) - 1))::citext as line_label
	  from (
	    select flcyy, flpage, flflne,       
	    case
	      when left(fldata, 1) = '|' then right(fldata, length(fldata) - 1)
	      else fldata
	    end as line_label
	    from arkona.ext_eisglobal_sypfflout
	    where flflsq = 0 
	      and flflne > 0
	      and flpage < 18) a
	  group by flcyy, flpage, flflne, trim(left(line_label, position('|' in line_label) - 1))) b
	left join ( -- columns
	  select fxmcyy, fxmpge, fxmlne, fxmcol
	  from arkona.ext_eisglobal_sypffxmst
	--   where fxmcyy > 2010
	  group by fxmcyy, fxmpge, fxmlne, fxmcol) c  on b.flcyy = c.fxmcyy and b.flpage = c.fxmpge and b.flflne = c.fxmlne
	where b.flcyy = _year; 

	truncate fin.xfm_fs_1;  
	insert into fin.xfm_fs_1  
	-- *d*
	select x._year,x._year_month, x.page, x.line, x.col, x.fxmact, x.gl_account
	from (
	  select _year, _year_month,
	    case -- 147701/167701 100% body shop, no parts split
	      when b.g_l_acct_number in ('147701', '167701') then 16
	      else a.fxmpge
	    end as page, 
	    case 
	      when b.g_l_acct_number in ('147701', '167701') then 49
	      else a.fxmlne
	    end as line, 
	    case 
	      when b.g_l_acct_number = '147701' then 2
	      when b.g_l_acct_number = '167701' then 3
	      else a.fxmcol
	    end as col, a.fxmact, 
	    coalesce(
	      case 
	        when b.g_l_acct_number like '%SPLT%' then replace(b.g_l_acct_number,'SPLT','')::citext
	        else b.g_l_acct_number
	      end, 'none') as gl_account
	  from arkona.ext_eisglobal_sypffxmst a
	  -- *a*
	  inner join (
	    select factory_financial_year, 
	    case
	      when coalesce(consolidation_grp, '1')  = '1' then 'RY1'::citext
	      when coalesce(consolidation_grp, '1')  = '2' then 'RY2'::citext
	      else 'XXX'::citext
	    end as store_code, g_l_acct_number, factory_account, fact_account_
	    from arkona.ext_ffpxrefdta) b on a.fxmcyy = b.factory_financial_year and a.fxmact = b.factory_account
	  where a.fxmpge < 18
	    and a.fxmcyy = _year) x
	group by x._year,x._year_month, x.page, x.line, x.col, x.fxmact, x.gl_account;
	
	-- parts split
	-- drop table if exists parts_split;
	truncate fin.parts_split;
	-- create temp table parts_split as  
	insert into fin.parts_split
	select a.the_year, a.year_month, 4 as page, 59.0 as line, 
	  case 
	    when a.gm_account in ('477s','677s') then 11 -- body shop
	    else 1
	  end as col, 
	  a.gm_account, replace(a.gl_account,'SPLT','')::citext as gl_account
	-- select *  
	from fin.xfm_fs_1 a
	where ((a.gm_account in('467s','667s','478s','678s','477s','677s','468s','668s'))
	  or (a.gl_account in ('146700D','246700D','246701D'))) order by gl_Account;  
	
	delete from fin.xfm_fs_1
	where page = 4 
	  and line = 59;
	
	
	insert into fin.xfm_fs_1 (the_year,year_month,page,line,col,gm_account,gl_account) 
	select *
	from fin.parts_split;  
	
	insert into fin.fact_fs
	select b.fs_key, coalesce(e.fs_org_key, ee.fs_org_key) as fs_org_key, c.fs_account_key, 
	  round( -- this is the parts split
	    sum(
	      case 
	        when b.page = 4 and b.line = 59 and c.gl_account not in ('147701','167701') 
	          then round(coalesce(f.amount, 0) * .5, 0) 
	        else coalesce(f.amount, 0)
	      end), 0) as amount  
	from fin.xfm_fs_1 a
	left join fin.dim_fs b on a.year_month = b.year_month
	  and a.page = b.page
	  and a.line = b.line
	  and a.col = b.col
	left join fin.dim_fs_account c on a.gm_account = c.gm_account
	  and a.gl_account = c.gl_account  
	left join fin.dim_account d on c.gl_account = d.account  
	-- *c*
	  and d.current_row = true
	left join fin.dim_Fs_org e on d.store_code = e.store
	  and
	    case -- parts split
	      when b.page = 4 and b.line = 59 then 
	        case
	          when c.gm_account in ('467S','667S', '468S','668S' ) then e.sub_department = 'mechanical'
	          when c.gm_account in ('478s','678s') then e.sub_department = 'pdq'
	          when c.gm_account in ('477s','677s') then e.department = 'body shop'
	          when c.gl_account = '146700D' then e.sub_department = 'mechanical'
	          when c.gl_account = '246700D' then e.sub_department = 'mechanical' 
	          when c.gl_account = '246701D' then e.sub_department = 'pdq'
	        end 
	      else   
	        case 
	          when d.department = 'Body Shop' then e.department = 'body shop'
	          when d.department = 'Car Wash' then e.sub_Department = 'car wash'
	          when d.department = 'Gateway Wash' then e.sub_Department = 'gateway wash'
	          when d.department = 'Detail' then e.sub_department = 'detail'
	          when d.department = 'Finance' and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
	          when d.department = 'Finance' and b.line > 10 then e.department = 'finance' and e.sub_department = 'used'
	          -- next 2 lines  correctly categorize these accounts as finance rather than sales, which is how they are categorized in COA
	          when d.department = 'New Vehicle' and b.page = 17 and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
	          when d.department = 'Used Vehicle' and b.page = 17 and b.line between 11 and 19 then e.department = 'finance' and e.sub_department = 'used'      
	          when d.department = 'New Vehicle' then e.department = 'sales' and e.sub_department = 'new'
	          when d.department = 'Parts' then e.department = 'parts'
	          when d.department = 'Quick Lane' then e.sub_department = 'pdq'
	          when d.department = 'Service' then e.sub_department = 'mechanical'
	          when d.department = 'Used Vehicle' then e.department = 'sales' and e.sub_department = 'used'
	-- *b*          
	          when d.department = 'Auto Outlet' then e.department = 'sales' and e.sub_department = 'used'
	-- *e*
	          when d.department = 'National' then e.department = 'national'
	          when d.department = 'General' then e.area = 'general'
	        end
	    end
	left join (
	  select b.year_month, aa.account, sum(a.amount) as amount
	  from fin.fact_gl a
	  inner join fin.dim_account aa on a.account_key = aa.account_key
	-- *f*
	--     and aa.current_row = true
	  inner join dds.dim_date b on a.date_key = b.date_key  
	  where a.post_status = 'Y'
	    and b.year_month = _year_month
	  group by b.year_month, aa.account) f on a.gl_account = f.account
	    and b.year_month = f.year_month    
	left join fin.dim_fs_org ee on 1 = 1
	  and ee.market = 'none'    
	group by a.year_month, b.fs_key, c.fs_account_key, 
	  coalesce(e.fs_org_key, ee.fs_org_key) having sum(amount) <> 0; 
	
	
	-------------------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------------------
	------------------------------------------  PAGE 2 ----------------------------------------------------
	-------------------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------------------
	delete 
	from fin.page_2
	where year_month = _year_month;
	-- base rows RY1
	insert into fin.page_2
	select 'RY1', _year, _year_month, n.line::integer as line, n.line_label, 
	  0 as store, 0 as variable, 0 as fixed
	from (
	  select *
	  from fin.dim_fs
	  where year_month = _year_month and page = 2) n
	group by n.line::integer, n.line_label;
	-- base rows RY2
	insert into fin.page_2
	select 'RY2', _year, _year_month, n.line::integer as line, n.line_label, 
	  0 as store, 0 as variable, 0 as fixed
	from (
	  select *
	  from fin.dim_fs
	  where year_month = _year_month and page = 2) n
	group by n.line::integer, n.line_label;
	-- base rows RY8
	insert into fin.page_2
	select 'RY8', _year, _year_month, n.line::integer as line, n.line_label, 
	  0 as store, 0 as variable, 0 as fixed
	from (
	  select *
	  from fin.dim_fs
	  where year_month = _year_month and page = 2) n
	group by n.line::integer, n.line_label;	
	-- lines 4 - 54
	update fin.page_2 z
	set fixed = x.amount
	from (-- p4 fixed
	  select year_month, store as store_code, line, sum(amount) as amount
	  from fin.fact_fs a
	  inner join fin.dim_fs b on a.fs_key = b.fs_key
	    and b.year_month = _year_month
	    and b.page = 4
	    and line between 4 and 54 
	  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
	  group by year_month, store, line) x
	where z.year_month = x.year_month
	  and z.store_code = x.store_code
	  and z.line = x.line;  
	-- lines 4 - 54  
	update fin.page_2 z
	set variable = x.amount
	from (-- p3 variable
	  select year_month, store as store_code, line, sum(amount) as amount
	  from fin.fact_fs a
	  inner join fin.dim_fs b on a.fs_key = b.fs_key
	    and b.year_month = _year_month
	    and b.page = 3
	    and line between 4 and 54 
	  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
	  group by year_month, store, line) x
	where z.year_month = x.year_month
	  and z.store_code = x.store_code
	  and z.line = x.line;  
	-- lines 4 - 54
	update fin.page_2 z
	set store = x.amount
	from (-- total of p3 & p4
	  select year_month, store as store_code, line, sum(amount) as amount
	  from fin.fact_fs a
	  inner join fin.dim_fs b on a.fs_key = b.fs_key
	    and b.year_month = _year_month
	    and b.page in(3,4)
	    and line between 4 and 54 
	  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
	  group by year_month, store, line) x
	where z.year_month = x.year_month
	  and z.store_code = x.store_code
	  and z.line = x.line;  
	-- lines 7,17,40,49,55,56, 57 all 3 columns
	update fin.page_2 z
	set store = x.store,
	    variable = x.variable,
	    fixed = x.fixed
	from (  
	  select store_code, year_month, 7 as line,
	    sum(store) as store,
	    sum(variable) as variable,
	    sum(fixed) as fixed
	  from fin.page_2
	  where year_month = _year_month
	    and line between 4 and 6
	  group by store_code, year_month
	  union all
	  select store_code, year_month, 17 as line,
	    sum(store) as store,
	    sum(variable) as variable,
	    sum(fixed) as fixed
	  from fin.page_2
	  where year_month = _year_month
	    and line between 8 and 16
	  group by store_code, year_month
	  union all
	  select store_code, year_month, 40 as line,
	    sum(store) as store,
	    sum(variable) as variable,
	    sum(fixed) as fixed
	  from fin.page_2
	  where year_month = _year_month
	    and line between 18 and 39
	  group by store_code, year_month
	  union all
	  select store_code, year_month, 49 as line,
	    sum(store) as store,
	    sum(variable) as variable,
	    sum(fixed) as fixed
	  from fin.page_2
	  where year_month = _year_month
	    and line between 41 and 48
	  group by store_code, year_month
	  union all
	  select store_code, year_month, 55 as line,
	    sum(store) as store,
	    sum(variable) as variable,
	    sum(fixed) as fixed
	  from fin.page_2
	  where year_month = _year_month
	    and (
	      line between 49 and 54
	      or line between 41 and 48)
	  group by store_code, year_month
	  union all
	  select store_code, year_month, 56 as line,
	    sum(store) as store,
	    sum(variable) as variable,
	    sum(fixed) as fixed
	  from fin.page_2
	  where year_month = _year_month
	    and (
	      line between 49 and 54
	      or line between 41 and 48
	      or line between 8 and 16
	      or line between 18 and 39)
	  group by store_code, year_month
	  union all
	  select store_code, year_month, 57 as line,
	    sum(store) as store,
	    sum(variable) as variable,
	    sum(fixed) as fixed
	  from fin.page_2
	  where year_month = _year_month
	    and (
	      line between 49 and 54
	      or line between 41 and 48
	      or line between 8 and 16
	      or line between 18 and 39
	      or line between 4 and 6)
	  group by store_code, year_month) x
	where z.store_code = x.store_code
	  and z.year_month = x.year_month
	  and z.line = x.line;  
	  -- variable lines 1 & 2
	update fin.page_2 z
	set variable = x.amount
	from (
	  select m.store_code, m.year_month, 1 as line,
	    m.fi_sales + n.uc_sales + o.nc_sales as amount
	  from (-- ok. fi is some kind of goofy about what counts as sales and what counts as cogs-- this works
	    select store as store_code, year_month, -1 * (sales_a + sales_b) as fi_sales, fi_gross
	    from (
	      select c.store, b.year_month,
	      sum(case when line in (1, 2, 11, 12) then amount else 0 end) as sales_a,
	      -- 11/3/17 added line 16
	      sum(case when line in (6, 7, 16, 17) and amount < 0 then amount else 0 end) as sales_b,
	      sum(-1 * amount) as fi_gross
	      from fin.fact_fs a
	      inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
	      inner join fin.dim_account aaa on aa.gl_account = aaa.account
	        and aaa.current_row = true
	      inner join fin.dim_fs b on a.fs_key = b.fs_key
	        and b.year_month = _year_month
	        and b.page = 17 
	        and b.line between 1 and 20 -- f/i
	      inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
	      group by c.store, b.year_month
	      having sum(amount) <> 0) x) m
	  left join (-- used cars sales off 201707 by what i believe is in line 11, but gross is ok, yep acct 165300
	    select c.store as store_code, b.year_month, page,
	      sum(case when amount < 0 then -1 * amount else 0 end) as uc_sales,
	      sum(-1 * amount) as uc_gross
	    from fin.fact_fs a
	    inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
	    inner join fin.dim_account aaa on aa.gl_account = aaa.account
	      and aaa.current_row = true
	    inner join fin.dim_fs b on a.fs_key = b.fs_key
	      and b.year_month = _year_month
	      and b.page = 16 and b.line between 1 and 14 -- used cars
	    inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
	    group by c.store, b.year_month, page
	    having sum(amount) <> 0
	    order by c.store, b.year_month) n on m.store_code = n.store_code and m.year_month = n.year_month
	  left join (-- new cars
	    select c.store as store_code, b.year_month, 
	      -- 11/3/17 line 40 had cost & no sales which was being totaled into sales
	--       sum(case when amount < 0 and col = 1 then - 1 * amount else 0 end) as nc_sales,
	--       --  05/03/19 this was fucking up the page 2 variable sales total, so
	      sum(-1 * amount) filter (where col = 1) as nc_sales,
	      sum(-1 * amount) as nc_gross
	    from fin.fact_fs a
	    inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
	    inner join fin.dim_account aaa on aa.gl_account = aaa.account
	      and aaa.current_row = true
	    inner join fin.dim_fs b on a.fs_key = b.fs_key
	      and b.year_month = _year_month
	      and b.page between 5 and 15
	    inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
	    group by c.store, b.year_month
	    having sum(amount) <> 0) o on m.store_code = o.store_code and m.year_month = o.year_month
	  union
	  select m.store_code, m.year_month, 2 as line,
	    m.fi_gross + n.uc_gross + o.nc_gross as gross
	  from (-- ok. fi is some kind of goofy about what counts as sales and what counts as cogs-- this works
	    select store as store_code, year_month, -1 * (sales_a + sales_b) as fi_sales, fi_gross
	    from (
	      select c.store, b.year_month,
	      sum(case when line in (1, 2, 11, 12) then amount else 0 end) as sales_a,
	      sum(case when line in (6, 7, 17) and amount < 0 then amount else 0 end) as sales_b,
	      sum(-1 * amount) as fi_gross
	      from fin.fact_fs a
	      inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
	      inner join fin.dim_account aaa on aa.gl_account = aaa.account
	        and aaa.current_row = true
	      inner join fin.dim_fs b on a.fs_key = b.fs_key
	        and b.year_month = _year_month
	        and b.page = 17 
	        and b.line between 1 and 20 -- f/i
	      inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
	      group by c.store, b.year_month
	      having sum(amount) <> 0) x) m
	  left join (-- used cars sales off 201707 by what i believe is in line 11, but gross is ok, yep acct 165300
	    select c.store as store_code, b.year_month, page,
	      sum(case when amount < 0 then -1 * amount else 0 end) as uc_sales,
	      sum(-1 * amount) as uc_gross
	    from fin.fact_fs a
	    inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
	    inner join fin.dim_account aaa on aa.gl_account = aaa.account
	      and aaa.current_row = true
	    inner join fin.dim_fs b on a.fs_key = b.fs_key
	      and b.year_month = _year_month
	      and b.page = 16 and b.line between 1 and 14 -- used cars
	    inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
	    group by c.store, b.year_month, page
	    having sum(amount) <> 0
	    order by c.store, b.year_month) n on m.store_code = n.store_code and m.year_month = n.year_month
	  left join (-- new cars
	    select c.store as store_code, b.year_month, 
	      sum(case when amount < 0 then - 1 * amount else 0 end) as nc_sales,
	      sum(-1 * amount) as nc_gross
	    from fin.fact_fs a
	    inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
	    inner join fin.dim_account aaa on aa.gl_account = aaa.account
	      and aaa.current_row = true
	    inner join fin.dim_fs b on a.fs_key = b.fs_key
	      and b.year_month = _year_month
	      and b.page between 5 and 15
	    inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
	    group by c.store, b.year_month
	    having sum(amount) <> 0) o on m.store_code = o.store_code and m.year_month = o.year_month) x
	where z.store_code = x.store_code
	  and z.year_month = x.year_month
	  and z.line = x.line;    
	-- fixed lines 1 & 2
	update fin.page_2 z
	set fixed = x.amount
	from (
	  select b.year_month, c.store as store_code, 1 as line,
	    sum(case when col in (1,2) then -1 * amount else 0 end) as amount
	  from fin.fact_fs a
	  inner join fin.dim_fs b on a.fs_key = b.fs_key
	    and b.year_month = _year_month
	    and b.page = 16 
	    and b.line between 20 and 59
	  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
	  where store <> 'none'
	  group by b.year_month, c.store
	  having sum(amount) <> 0
	  union
	  select b.year_month, c.store, 2 as line,
	    sum(-1 * amount) as gross
	  from fin.fact_fs a
	  inner join fin.dim_fs b on a.fs_key = b.fs_key
	    and b.year_month = _year_month
	    and b.page = 16 
	    and b.line between 20 and 59
	  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
	  where store <> 'none'
	  group by b.year_month, c.store
	  having sum(amount) <> 0) x
	where z.store_code = x.store_code
	  and z.year_month = x.year_month
	  and z.line = x.line;
	-- lines 1 & 2 store
	update fin.page_2 z
	set store = variable + fixed
	where line between 1 and 2
	  and year_month = _year_month;  
	-- line 58 operating profit
	update fin.page_2 z
	set store = x.store,
	    variable = x.variable, 
	    fixed = x.fixed
	from (
	  select a.store_code, a.year_month, a.store - b.store as store,
	    a.variable - b.variable as variable, 
	    a.fixed - b.fixed as fixed
	  from (
	    select store_code, year_month, store, variable, fixed
	    from fin.page_2
	    where line = 2
	      and year_month = _year_month) a
	  left join (  
	    select store_code, year_month, store, variable, fixed
	    from fin.page_2
	    where line = 57
	      and year_month = _year_month) b on a.store_code = b.store_code 
	      and a.year_month = b.year_month) x 
	where z.store_code = x.store_code 
	  and z.year_month = x.year_month
	  and z.line = 58;     
	-- line 59 adds and deduct
	update fin.page_2 z
	set store = x.amount
	from (
	  select store as store_code, year_month, sum(-1 * amount) as amount
	  from fin.fact_fs a
	  inner join fin.dim_fs b on a.fs_key = b.fs_key
	    and b.year_month = _year_month
	    and b.page = 3
	    and b.line > 62
	  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
	  where store in ('ry1','ry2', 'ry8')
	  group by store, year_month) x
	where z.store_code = x.store_code
	  and z.year_month = x.year_month
	  and z.line = 59;    
	-- lines 60, 63, 65
	update fin.page_2 z
	set store = x.store
	from (
	  select a.store_code, a.year_month, a.store + b.store as store
	  from (
	    select store_code, year_month, store
	    from fin.page_2
	    where line = 58
	      and year_month = _year_month) a
	  left join (  
	    select store_code, year_month, store
	    from fin.page_2
	    where line = 59
	      and year_month = _year_month) b on a.store_code = b.store_code and a.year_month = b.year_month) x 
	where z.store_code = x.store_code 
	  and z.year_month = x.year_month
	  and z.line in (60,63,65);  
	  
	end
	$$;

select * 
from fin.page_2
where year_month = 202312
order by store_code, line


-- parts split
select c.store, c.department, sum(a.amount)
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202312
  and b.page = 4 
  and b.line = 59
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
group by c.store, c.department

-- 
-- -- parts semi fixed
-- select b.line::integer, b.line_label, 
-- 	sum(a.amount) filter (where year_month = 201901) as "Jan 2019",
-- 	sum(a.amount) filter (where year_month = 201902) as "Feb 2019",
-- 	sum(a.amount) filter (where year_month = 201903) as "Mar 2019",
-- 	sum(a.amount) filter (where year_month = 201904) as "Apr 2019",
-- 	sum(a.amount) filter (where year_month = 201905) as "May 2019",
-- 	sum(a.amount) filter (where year_month = 201906) as "Jun 2019",
-- 	sum(a.amount) filter (where year_month = 201907) as "Jul 2019",
-- 	sum(a.amount) filter (where year_month = 201908) as "Aug 2019",
-- 	sum(a.amount) filter (where year_month = 201909) as "Sep 2019",
-- 	sum(a.amount) filter (where year_month = 201910) as "Oct 2019",
-- 	sum(a.amount) filter (where year_month = 201911) as "Nov 2019",
-- 	sum(a.amount) filter (where year_month = 201912) as "Dec 2019",
-- 
-- 	sum(a.amount) filter (where year_month = 202001) as "Jan 2020",
-- 	sum(a.amount) filter (where year_month = 202002) as "Feb 2020",
-- 	sum(a.amount) filter (where year_month = 202003) as "Mar 2020",
-- 	sum(a.amount) filter (where year_month = 202004) as "Apr 2020",
-- 	sum(a.amount) filter (where year_month = 202005) as "May 2020",
-- 	sum(a.amount) filter (where year_month = 202006) as "Jun 2020",
-- 	sum(a.amount) filter (where year_month = 202007) as "Jul 2020",
-- 	sum(a.amount) filter (where year_month = 202008) as "Aug 2020",
-- 	sum(a.amount) filter (where year_month = 202009) as "Sep 2020",
-- 	sum(a.amount) filter (where year_month = 202010) as "Oct 2020",
-- 	sum(a.amount) filter (where year_month = 202011) as "Nov 2020",
-- 	sum(a.amount) filter (where year_month = 202012) as "Dec 2020",
-- 	sum(a.amount) filter (where year_month = 202101) as "Jan 2021",
-- 	sum(a.amount) filter (where year_month = 202102) as "Feb 2021",
-- 	sum(a.amount) filter (where year_month = 202103) as "Mar 2021",
-- 	sum(a.amount) filter (where year_month = 202104) as "Apr 2021",
-- 	sum(a.amount) filter (where year_month = 202105) as "May 2021",
-- 	sum(a.amount) filter (where year_month = 202106) as "Jun 2021",
-- 	sum(a.amount) filter (where year_month = 202107) as "Jul 2021"
-- from fin.fact_fs a
-- inner join fin.dim_fs b on a.fs_key = b.fs_key
--   and b.year_month between 201901 and 202107
--   and b.page = 4
--   and b.line between 18 and 40
--   and b.col = 13
-- inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
-- inner join fin.dim_account e on d.gl_account = e.account
-- inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
--   and f.store = 'ry1'
-- group by b.line, b.line_label  
-- union
-- select 40, 'Total Semi-Fixed',
-- 	sum(a.amount) filter (where year_month = 201901) as "Jan 2019",
-- 	sum(a.amount) filter (where year_month = 201902) as "Feb 2019",
-- 	sum(a.amount) filter (where year_month = 201903) as "Mar 2019",
-- 	sum(a.amount) filter (where year_month = 201904) as "Apr 2019",
-- 	sum(a.amount) filter (where year_month = 201905) as "May 2019",
-- 	sum(a.amount) filter (where year_month = 201906) as "Jun 2019",
-- 	sum(a.amount) filter (where year_month = 201907) as "Jul 2019",
-- 	sum(a.amount) filter (where year_month = 201908) as "Aug 2019",
-- 	sum(a.amount) filter (where year_month = 201909) as "Sep 2019",
-- 	sum(a.amount) filter (where year_month = 201910) as "Oct 2019",
-- 	sum(a.amount) filter (where year_month = 201911) as "Nov 2019",
-- 	sum(a.amount) filter (where year_month = 201912) as "Dec 2019",
-- 	sum(a.amount) filter (where year_month = 202001) as "Jan 2020",
-- 	sum(a.amount) filter (where year_month = 202002) as "Feb 2020",
-- 	sum(a.amount) filter (where year_month = 202003) as "Mar 2020",
-- 	sum(a.amount) filter (where year_month = 202004) as "Apr 2020",
-- 	sum(a.amount) filter (where year_month = 202005) as "May 2020",
-- 	sum(a.amount) filter (where year_month = 202006) as "Jun 2020",
-- 	sum(a.amount) filter (where year_month = 202007) as "Jul 2020",
-- 	sum(a.amount) filter (where year_month = 202008) as "Aug 2020",
-- 	sum(a.amount) filter (where year_month = 202009) as "Sep 2020",
-- 	sum(a.amount) filter (where year_month = 202010) as "Oct 2020",
-- 	sum(a.amount) filter (where year_month = 202011) as "Nov 2020",
-- 	sum(a.amount) filter (where year_month = 202012) as "Dec 2020",	
-- 	sum(a.amount) filter (where year_month = 202101) as "Jan 2021",
-- 	sum(a.amount) filter (where year_month = 202102) as "Feb 2021",
-- 	sum(a.amount) filter (where year_month = 202103) as "Mar 2021",
-- 	sum(a.amount) filter (where year_month = 202104) as "Apr 2021",
-- 	sum(a.amount) filter (where year_month = 202105) as "May 2021",
-- 	sum(a.amount) filter (where year_month = 202106) as "Jun 2021",
-- 	sum(a.amount) filter (where year_month = 202107) as "Jul 2021"
-- from fin.fact_fs a
-- inner join fin.dim_fs b on a.fs_key = b.fs_key
--   and b.year_month between 201901 and 202107
--   and b.page = 4
--   and b.line between 18 and 40
--   and b.col = 13
-- inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
-- inner join fin.dim_account e on d.gl_account = e.account
-- inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
--   and f.store = 'ry1'
-- order by line  
 

/*

June 2021 
p2 L38
statement: 13620
query: 12733

select b.the_date, a.amount, c.account
-- select d.col, sum(amount)
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 202106
join fin.dim_account c on a.account_key = c.account_key
join (
	select distinct d.gl_account, b.col
	from fin.fact_fs a
	inner join fin.dim_fs b on a.fs_key = b.fs_key
		and b.year_month = 202106
		and b.page = 4
		and b.line = 38
	  and b.col = 1
	inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
	inner join fin.dim_account e on d.gl_account = e.account
	inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
		and f.store = 'ry1') d on c.account = d.gl_account
where a.post_Status = 'Y'
-- group by d.col

i am missing accounts 13327 and 13328

select * from fin.dim_fs_account where gl_account in ( '13327','13328')

select * from fin.dim_account where account in  ( '13327','13328')

--------------------------------------------------------------------------------------------
adj cost of labor
-- where are 165504 and 16504a doubled up
select d.gl_account, b.line, e.description, a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202102
  and b.page = 16
  and b.line in (30,39)
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
--   and e.account_type_code = '4'
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store = 'ry1'  




  
-- used car gross accounts
-- ry1 by line, sales only
select d.gl_account, b.line, e.description
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202312
  and b.page = 16
  and b.line between 1 and 14
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
  and e.account_type_code = '4'
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store = 'ry1'  
  

-- what are the new car gross accounts
select distinct d.gl_account, e.account_type, e.department
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201612
  and b.page between 5 and 15 -- new cars
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
union
-- used car gross accounts
select distinct d.gl_account, e.account_type, e.department
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201612
  and b.page = 16
  and b.line between 1 and 14
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
union
-- f/i accounts
select distinct d.gl_account, e.account_type, e.department
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201612
  and b.page = 17
  and b.line between 1 and 20
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account

-- new car expense accounts
select distinct f.store, b.line, b.line_label, d.gl_account, e.description
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page = 3
  and b.col = 1
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store in ('ry1','ry2')
order by f.store, b.line, d.gl_account


-- new car semi-fixed accounts
select distinct f.store, b.line, b.col, b.line_label, d.gl_account, e.description
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202106
  and b.page = 3
  and b.line between 18 and 40
  and b.col = 1
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store in ('ry1','ry2')
order by f.store, b.line, d.gl_account

-- used car semi-fixed accounts
select distinct f.store, b.line, b.col, b.line_label, d.gl_account, e.description
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page = 3
  and b.line between 18 and 40
  and b.col = 5
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store in ('ry1','ry2')
order by f.store, b.line, d.gl_account

-- fixed semi-fixed accounts
select distinct f.store, b.line, b.col, b.line_label, d.gl_account, e.description, f.*
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page = 4
  and b.line between 18 and 40
--   and b.col = 5
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store in ('ry1','ry2')
order by f.store, f.department, f.sub_department, b.line, d.gl_account







-- semi-fixed accounts
select distinct f.department, f.sub_department, f.store, b.line, b.col, b.line_label, d.gl_account, e.description 
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page in (3,4)
  and b.line between 18 and 40
--   and b.col = 5
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store in ('ry1','ry2')
order by f.store, f.department, f.sub_department, b.line, d.gl_account



-- just the sales accounts for body shop page 6
select distinct d.gl_account
-- select sum(a.amount) -- total bs sales P6L43
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page =16
  and b.line between 35 and 43
   and b.col in (1,2)
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store = 'ry1'
union
-- parts split sales: 147701: GM bs gets 1/2, 147700: non-gm body shop gets all
select distinct d.gl_account
-- select sum(a.amount) -- total bs sales P6L43
-- select *
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page = 4
  and b.line = 59
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store = 'ry1'
  and f.department = 'body shop'
inner join fin.dim_account e on d.gl_account = e.account  
  and e.account_type = 'sale'

*** parts???
-- body shop sales
select b.page, b.line, b.line_label,-- d.gl_account, d.gm_account,
  sum(case when e.account_type = 'Sale' then amount else 0 end) as sales,
  sum(case when e.account_type in ('cogs','expense') then amount else 0 end) as cogs
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202101
  and (
    (b.page = 16 and b.line between 20 and 42)
    or
    (b.page = 4 and b.line = 59)) -- parts split
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.department = 'body shop'
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key  
inner join fin.dim_account e on d.gl_account = e.account
  and e.current_row
group by b.page, b.line, b.line_label
order by b.page, b.line

*** good
-- bs sales totals (without parts)
select 
  sum(case when e.account_type = 'Sale' then amount else 0 end) as sales,
  sum(case when e.account_type in ('cogs','expense') then amount else 0 end) as cogs
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202101
  and (
    (b.page = 16 and b.line between 20 and 42))
--     or
--     (b.page = 4 and b.line = 59)) -- parts split
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.department = 'body shop'
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key  
inner join fin.dim_account e on d.gl_account = e.account

*/
*** good
-- unit counts -----------------------------------------------------------------------------------------------------------------
drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select store, page, line, line_label, control, sum(unit_count) as unit_count
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 202312-----------------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 202312 ---------------------------------------------------------------------
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
--         or
--         (b.page = 17 and b.line between 1 and 20))-- f/i counts don't make sense
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by store, page, line, line_label, control
order by store, page, line;

select * from step_1


-- ok, count is still good
select store, page, line, line_label, sum(unit_count)
from (
  select *
  from step_1) a
group by store, page, line, line_label
order by store, page, line, line_label

-- by store, page
select store, page, sum(unit_count)
from (
  select *
  from step_1) a
group by store, page
order by store, page

-- retail
select store, page, sum(unit_count)
from (
  select *
  from step_1) a
where line between 25 and 40  
group by store, page
order by store, page


-- count
select page, sum(unit_count)
from step_1
where store = 'ry1' 
	and line between 25 and 40
group by page

-- gross
select sum(-a.amount)
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
where b.year_month = 202310
  and page in (5,8,9,10)
  and line between 25 and 40
  and c.store = 'ry1'

-- 202106 new truck pvr
select (
  (
		select sum(-a.amount)
		from fin.fact_fs a
		join fin.dim_fs b on a.fs_key = b.fs_key
		join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
		where b.year_month = 202106
			and page in (5,8,9,10)
			and line between 25 and 40
			and c.store = 'ry1') 
	/
	(
		select sum(unit_count)
		from step_1
		where store = 'ry1' 
			and line between 25 and 40))::integer

drop table if exists the_counts;
create temp table the_counts as
-- include stocknumber on each row
select year_month, sum(unit_count) as unit_count
from (
  select distinct b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201801 and 202106
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month between 201801 and 202106
      and (
        (b.page in (5,8,9,10) and b.line between 25 and 40))
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y'
    and d.store = 'ry1' order by control) h
group by year_month

select * from the_counts

drop table if exists the_gross;
create temp table the_gross as
select b.year_month, sum(-a.amount) as gross
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
where b.year_month between 201801 and 202106
  and page in (5,8,9,10)
  and line between 25 and 40
  and c.store = 'ry1'	
group by year_month;  

select * from the_gross

select a.year_month, a.unit_count, b.gross, (b.gross/a.unit_count)::integer
from the_counts a
join the_Gross b on a.year_month = b.year_month