﻿drop table if exists jon.fs_service_gross_accounts cascade;
create table jon.fs_service_gross_accounts as
select distinct b.year_month, d.store, sub_department, b.page, b.line_label, b.line, b.col, c.gl_account, c.gm_account,
  case when page = 4 then .5 else 1 end as multiplier
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201001 and 202103
  and ((b.page = 16 and b.line between 21 and 34)
    or
      (b.page = 4 and b.line = 59))  -- parts split
join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key  
join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  and d.store = 'ry1'
  and d.area = 'fixed'
  and d.department = 'service';
create unique index on jon.fs_service_gross_accounts(year_month,gl_account); 
create index on jon.fs_service_gross_accounts(gl_account);
create index on jon.fs_service_gross_accounts(page);
create index on jon.fs_service_gross_accounts(sub_department);
create index on jon.fs_service_gross_accounts(year_month);

  
select line, department, sub_department,  sum(amount)::integer 
from (
select d.*, a.amount, c.department 
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
join fin.dim_account c on a.account_key = c.account_key
join fs_service_gross_accounts d on c.account = d.gl_account
  and b.year_month = d.year_month
where a.post_status = 'Y'
  and b.year_month = 201901) aa
group by line, department, sub_department
order by line  

select b.year_month, a.amount, d.multiplier -- sum(-a.amount * d.multiplier)::integer as gross
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
join fin.dim_account c on a.account_key = c.account_key
join jon.fs_service_gross_accounts d on c.account = d.gl_account
  and b.year_month = d.year_month
where a.post_status = 'Y'
  and b.year_month in( 201901,201902,201903) -- between 201001 and 202102
  and d.sub_department = 'mechanical'
group by b.year_month
order by year_month

--04/03/21
-- this really suprised me, running this query with all the months was MUCH faster
-- than running it for just 3 months, a little reading, seems to be about the number
-- 18 sec vs

https://www.depesz.com/2013/04/16/explaining-the-unexplainable/

https://explain.depesz.com/s/Dx4j
EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS, TIMING) --, FORMAT JSON)
select b.year_month, sum(-a.amount * d.multiplier)::integer as gross
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month between 201101 and 202103
join fin.dim_account c on a.account_key = c.account_key
join jon.fs_service_gross_accounts d on c.account = d.gl_account
  and b.year_month = d.year_month
  and d.sub_department = 'mechanical'
where a.post_status = 'Y'
group by b.year_month  

https://explain.depesz.com/s/fKir
EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS,format json)
select b.year_month, sum(-a.amount * d.multiplier)::integer as gross
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
	and b.year_month between 201901 and 201903
join fin.dim_account c on a.account_key = c.account_key
join jon.fs_service_gross_accounts d on c.account = d.gl_account
	and b.year_month = d.year_month
	and d.sub_department = 'mechanical'
where a.post_status = 'Y'  
group by b.year_month  

-- start 3:41, shit 28 sec
-- and analyze took 4:48 min
-- depesz fact_gl 3 months_1, even a little worse
reindex index fin.fact_gl_account_key_idx;

i think the problem isnt the speed of the index scan, it is the fact that it is done 2818 times in a nested loop
rearrange the query, order of the tables:
  
-- #QwQW : 6 35.952, loops = 2,247
-- reindex index fin.dim_account_account_idx
-- #PyXg : Optimization for: plan #QwQW 6 20.223 sec
EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS) --,format json)
-- select b.year_month, c.account , sum(-a.amount * d.multiplier)::integer as gross
select b.year_month, c.account_key
from dds.dim_date b
join jon.fs_service_gross_Accounts d on b.year_month = d.year_month
  and d.sub_department = 'mechanical'
join fin.dim_account c on d.gl_account = c.account
where b.year_month between 201901 and 201903
-- order by b.year_month, c.account
group by b.year_month, c.account_key

am i getting
-- RN3YA
EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS)
select aa.year_month, sum(-bb.amount * aa.multiplier)::integer as gross
from (
	select b.year_month, b.date_key, c.account_key, d.multiplier
	from dds.dim_date b
	join jon.fs_service_gross_Accounts d on b.year_month = d.year_month
		and d.sub_department = 'mechanical'
	join fin.dim_account c on d.gl_account = c.account
	where b.year_month between 201901 and 201903
	group by b.year_month, b.date_key, c.account_key, d.multiplier) aa
join fin.fact_gl bb on aa.date_key = bb.date_key
  and aa.account_key = bb.account_key
  and bb.post_status = 'Y'	
group by aa.year_month  


drop table if exists jon.rearrange_join cascade;
create table jon.rearrange_join as
select b.year_month, b.date_key, c.account_key, d.multiplier
from dds.dim_date b
join jon.fs_service_gross_Accounts d on b.year_month = d.year_month
	and d.sub_department = 'mechanical'
join fin.dim_account c on d.gl_account = c.account
where b.year_month between 201901 and 201903
group by b.year_month, b.date_key, c.account_key, d.multiplier;
create unique index on jon.rearrange_join(year_month,date_key,account_key);
create index on jon.rearrange_join(date_key);
create index on jon.rearrange_join(account_key);
create index on jon.rearrange_join(year_month);

-- OfEm
EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS)
select aa.year_month, sum(-bb.amount * aa.multiplier)::integer as gross
from jon.rearrange_join aa
join fin.fact_gl bb on aa.date_key = bb.date_key
  and aa.account_key = bb.account_key
  and bb.post_status = 'Y'	
group by aa.year_month  

creating the physical table had no effect whatever

analyze verbose fin.fact_gl

https://andreigridnev.com/blog/2016-04-01-analyze-reindex-vacuum-in-postgresql/

-- 04/06
-- only a couple of seconds for dim_Account
VACUUM FULL VERBOSE ANALYZE fin.fact_gl
INFO:  vacuuming "fin.fact_gl"
INFO:  "fact_gl": found 361029 removable, 23552136 nonremovable row versions in 290599 pages
DETAIL:  0 dead row versions cannot be removed yet.
CPU 3.73s/11.12u sec elapsed 27.88 sec.

--no improvement on this query at all, still 15 sec
EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS)
select aa.year_month, sum(-bb.amount * aa.multiplier)::integer as gross
from jon.rearrange_join aa
join fin.fact_gl bb on aa.date_key = bb.date_key
  and aa.account_key = bb.account_key
  and bb.post_status = 'Y'	
group by aa.year_month  

reindex table fin.fact_gl;

-- 22 sec
EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS, TIMING) --, FORMAT JSON)
select b.year_month, sum(-a.amount * d.multiplier)::integer as gross
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month between 201101 and 202103
join fin.dim_account c on a.account_key = c.account_key
join jon.fs_service_gross_accounts d on c.account = d.gl_account
  and b.year_month = d.year_month
  and d.sub_department = 'mechanical'
where a.post_status = 'Y'
group by b.year_month  

reaarange 19 sec
select aa.year_month, sum(-bb.amount * aa.multiplier)::integer as gross
from (
	select b.year_month, b.date_key, c.account_key, d.multiplier
	from dds.dim_date b
	join jon.fs_service_gross_Accounts d on b.year_month = d.year_month
		and d.sub_department = 'mechanical'
	join fin.dim_account c on d.gl_account = c.account
	where b.year_month between 201101 and 201903
	group by b.year_month, b.date_key, c.account_key, d.multiplier) aa
join fin.fact_gl bb on aa.date_key = bb.date_key
  and aa.account_key = bb.account_key
  and bb.post_status = 'Y'	
group by aa.year_month  	

-- the same 19 sec
EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS, TIMING) 
with 
  smaller_table as (
		select b.year_month, b.date_key, c.account_key, d.multiplier
		from dds.dim_date b
		join jon.fs_service_gross_Accounts d on b.year_month = d.year_month
			and d.sub_department = 'mechanical'
		join fin.dim_account c on d.gl_account = c.account
		where b.year_month between 201101 and 201903
		group by b.year_month, b.date_key, c.account_key, d.multiplier) 
select aa.year_month, sum(-bb.amount * aa.multiplier)::integer as gross
from smaller_table aa
join fin.fact_gl bb on aa.date_key = bb.date_key
  and aa.account_key = bb.account_key
  and bb.post_status = 'Y'	
group by aa.year_month  	

EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS)
drop table if exists wtf;
create temp table wtf as -- 371 msec
select a.control, a.amount
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month between 202101 and 202103

EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS)
create temp table wtf as -- 344 ms
select a.control, a.amount
from dds.dim_date b
join fin.fact_gl a on a.date_key = b.date_key
where b.year_month between 202101 and 202103
				