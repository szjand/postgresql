﻿/*

this entire script is about restoring the data in fin.fact_fs for 202101 that i inadvertently
deleted while testing the script ...recovery/fact_fs_monthly_update.sql, which deleted the current month
data from fin.fact_fs because i was not paying close enough attention
*/

-- source (209) is the remote postgres server from where the tables are accessed by the destination database server as foreign tables.
-- destination (173) is another postgres server where the foreign tables are created which is referring tables in source database server.

create schema test_fdw;
comment on schema test_fdw is 'testing import foreign schema, destination schema for importing schema fin from pg_standby_fdw,
  which is a restored backup of 173';

create server pg_standby_fdw
foreign data wrapper postgres_fdw
options(dbname 'cartiva', host '10.130.196.209', port '5432')

create user mapping for current_user
server pg_standby_fdw
options(user 'rydell', password 'cartiva');

import foreign schema fin from server pg_standby_fdw into test_fdw;

-- schema test_fdw -> Foreign Tables is how to access the tables on pg_standby

-- ok, looks like nothing got fucked up beyond the deletion of 202101 when trying
-- to test an old version of fact_fs_monthly_update
select * 
from (
  select b.page, b.line, b.line_label, sum(a.amount) as amount
  from fin.fact_fs a
  join fin.dim_Fs b on a.fs_key = b.fs_key
  join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
  join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  where b.year_month = 202012
  group by b.page, b.line, b.line_label) aa
left join (
  select b.page, b.line, b.line_label, sum(a.amount) as amount
  from test_fdw.fact_fs a
  join test_fdw.dim_Fs b on a.fs_key = b.fs_key
  join test_fdw.dim_fs_org c on a.fs_org_key = c.fs_org_key
  join test_fdw.dim_fs_account d on a.fs_account_key = d.fs_account_key
  where b.year_month = 202012
  group by b.page, b.line, b.line_label) bb on aa.page = bb.page and aa.line = bb.line 
where aa.amount <> bb.amount


select  * -- 1318 rows
from test_fdw.fact_fs a
where not exists (
  select 1
  from fin.fact_fs
  where fs_key = a.fs_key
    and fs_org_key = a.fs_org_key
    and fs_account_key = a.fs_account_key);

select * -- 1318
from test_fdw.fact_fs a
join test_fdw.dim_fs b on a.fs_key = b.fs_key    
where b.year_month = 202101

-- ok, it also looks like i will have to replace the data for 202101 in dim_fs
-- let me check if fs_key is referenced anywhere else, looks like it is only in fact_fs
select * 
from (
select * 
from test_fdw.dim_fs
where year_month = 202101) a
full outer join (
select * 
from fin.dim_fs
where year_month = 202101) b
on a.fs_key = b.fs_key

ok, heres the plan
0.1
  ERROR:  cannot alter type of a column used by a view or rule
  DETAIL:  rule _RETURN on materialized view fin.fact_fs_all depends on column "fs_key"
1.
  delete the data from fin.dim_fs where year_month = 202101
2.
  because dim_fs.fs_key is serial, i will have to change the data type of fin.dim_fs.fs_key to 
  select max(fs_key) from test_fdw.dim_fs  --931644
  select max(fs_key) from fin.dim_fs  where year_month <> 202101 -- 925602
  integer to add the values for the fs_key from test_fdw.dim_fs
  this tests for any potential collisions: (none)
  select fs_key from test_fdw.dim_fs a where year_month = 202101
  and exists (
    select 1 from fin.fact_fs where fs_key = a.fs_key);
  select fs_key from test_fdw.dim_fs a where year_month = 202101
  and exists (
    select 1 from fin.dim_fs where fs_key = a.fs_key);  
3.
  insert into fin.dim_fs 202101 data from test_fdw.dim_fs 
4.    
  insert into fin.fact_fs 202101 data from test_fdw.fact_fs
5.
  change fin.dim_fs.fs_key back to serial

--0.1
DROP MATERIALIZED VIEW fin.fact_fs_all cascade;
 --1
delete 
from fin.dim_fs where year_month = 202101;
--2
alter table fin.dim_fs
alter column fs_key type integer;
-- 3
insert into fin.dim_fs
select * 
from test_fdw.dim_fs
where year_month = 202101;
-- 4
insert into fin.fact_fs
select a.* -- 1318
from test_fdw.fact_fs a
join test_fdw.dim_fs b on a.fs_key = b.fs_key    
where b.year_month = 202101;
--5
CREATE SEQUENCE fin.dim_fs_fs_key_seq_2 increment 1 minvalue 933659;
alter table fin.dim_fs
alter column fs_key set default nextval('fin.dim_fs_fs_key_seq_2');

-- no nulls, we are good
select * 
from (
  select c.store, b.page, b.line, b.line_label, b.col, sum(a.amount) as amount
  from fin.fact_fs a
  join fin.dim_Fs b on a.fs_key = b.fs_key
  join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
  join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  where b.year_month = 202101 
  group by c.store, b.page, b.line, b.col, b.line_label
  order by c.store, b.page, b.line, b.col) aa
full outer join (
  select c.store, b.page, b.line, b.line_label, b.col, sum(a.amount) as amount
  from test_fdw.fact_fs a
  join test_fdw.dim_Fs b on a.fs_key = b.fs_key
  join test_fdw.dim_fs_org c on a.fs_org_key = c.fs_org_key
  join test_fdw.dim_fs_account d on a.fs_account_key = d.fs_account_key
  where b.year_month = 202101 
  group by c.store, b.page, b.line, b.col, b.line_label
  order by c.store, b.page, b.line, b.col) bb on aa.page= bb.page
    and aa.line = bb.line 
    and aa.col = bb.col
    and aa.amount = bb.amount




-- -- it works
-- select *
-- from fin.dim_fs
-- order by fs_key desc
-- limit 3
-- 
-- insert into fin.dim_fs(the_year,year_month,page,line,col,line_label,row_from_Date,current_row)
-- select 2021,202102,17,75,4,'test',current_date,true
-- 
-- delete from fin.dim_fs where line_label = 'test'