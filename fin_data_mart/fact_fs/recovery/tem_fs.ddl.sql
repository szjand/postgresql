﻿-- create schema tem_fs;
-- comment on schema tem_fs is 'a safe workspace in which to play as i try to resucitate all my financial statement work';
-- Table: tem_fs.dim_fs

-- DROP TABLE tem_fs.dim_fs;

CREATE TABLE tem_fs.dim_fs
(
  fs_key serial NOT NULL, -- source: system generated surrogate key
  the_year integer NOT NULL, -- source: script generated
  year_month integer NOT NULL, -- source: script generated
  page integer NOT NULL, -- source: arkona.ext_eisglobal_sypfflout.flpage
  line numeric(4,1) NOT NULL, -- source: arkona.sypfflout.flflne
  col integer NOT NULL, -- source:
  line_label citext NOT NULL, -- source: arkona.sypfflout.fldata
  row_from_date date NOT NULL,
  row_thru_date date NOT NULL DEFAULT '9999-12-31'::date,
  current_row boolean NOT NULL,
  row_reason citext NOT NULL DEFAULT 'New Row'::citext,
  CONSTRAINT dim_fs_pkey PRIMARY KEY (fs_key),
  CONSTRAINT dim_fs_nk UNIQUE (year_month, page, line, col)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tem_fs.dim_fs
  OWNER TO rydell;
GRANT ALL ON TABLE tem_fs.dim_fs TO rydell;
COMMENT ON TABLE tem_fs.dim_fs
  IS 'line_label = an empty string when there is no label
col = -1 when there is no column, ie, no row in sypffxmst for that page/line, no gm account assigned
gm financial statement page 1 thru 17';
COMMENT ON COLUMN tem_fs.dim_fs.fs_key IS 'source: system generated surrogate key';
COMMENT ON COLUMN tem_fs.dim_fs.the_year IS 'source: script generated';
COMMENT ON COLUMN tem_fs.dim_fs.year_month IS 'source: script generated';
COMMENT ON COLUMN tem_fs.dim_fs.page IS 'source: arkona.ext_eisglobal_sypfflout.flpage';
COMMENT ON COLUMN tem_fs.dim_fs.line IS 'source: arkona.sypfflout.flflne';
COMMENT ON COLUMN tem_fs.dim_fs.col IS 'source: ';
COMMENT ON COLUMN tem_fs.dim_fs.line_label IS 'source: arkona.sypfflout.fldata';


-- Index: tem_fs.dim_fs_col_idx

-- DROP INDEX tem_fs.dim_fs_col_idx;

CREATE INDEX dim_fs_col_idx
  ON tem_fs.dim_fs
  USING btree
  (col);

-- Index: tem_fs.dim_fs_fs_key_idx

-- DROP INDEX tem_fs.dim_fs_fs_key_idx;

CREATE INDEX dim_fs_fs_key_idx
  ON tem_fs.dim_fs
  USING btree
  (fs_key);

-- Index: tem_fs.dim_fs_line_idx

-- DROP INDEX tem_fs.dim_fs_line_idx;

CREATE INDEX dim_fs_line_idx
  ON tem_fs.dim_fs
  USING btree
  (line);

-- Index: tem_fs.dim_fs_line_label_idx

-- DROP INDEX tem_fs.dim_fs_line_label_idx;

CREATE INDEX dim_fs_line_label_idx
  ON tem_fs.dim_fs
  USING btree
  (line_label COLLATE pg_catalog."default");

-- Index: tem_fs.dim_fs_page_idx

-- DROP INDEX tem_fs.dim_fs_page_idx;

CREATE INDEX dim_fs_page_idx
  ON tem_fs.dim_fs
  USING btree
  (page);

-- Index: tem_fs.dim_fs_the_year_idx

-- DROP INDEX tem_fs.dim_fs_the_year_idx;

CREATE INDEX dim_fs_the_year_idx
  ON tem_fs.dim_fs
  USING btree
  (the_year);

-- Index: tem_fs.dim_fs_year_month_idx

-- DROP INDEX tem_fs.dim_fs_year_month_idx;

CREATE INDEX dim_fs_year_month_idx
  ON tem_fs.dim_fs
  USING btree
  (year_month);

-- Table: tem_fs.dim_fs_account

-- DROP TABLE tem_fs.dim_fs_account;

CREATE TABLE tem_fs.dim_fs_account
(
  fs_account_key serial NOT NULL,
  gm_account citext NOT NULL, -- source: arkona.ext_eis_global_sypffxmst.fxmact
  gl_account citext NOT NULL, -- source: arkona.ext_ffpxrefdta.g_l_acct_number
  row_from_date date NOT NULL,
  row_thru_date date NOT NULL DEFAULT '9999-12-31'::date,
  current_row boolean NOT NULL,
  row_reason citext NOT NULL DEFAULT 'New Row'::citext,
  CONSTRAINT dim_fs_account_pkey PRIMARY KEY (fs_account_key),
  CONSTRAINT dim_fs_account_nk UNIQUE (gm_account, gl_account)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tem_fs.dim_fs_account
  OWNER TO rydell;
GRANT ALL ON TABLE tem_fs.dim_fs_account TO rydell;
COMMENT ON TABLE tem_fs.dim_fs_account
  IS 'where gl_account = none:
  if the first char is <, & or * it is a "formula" account
  if not it is a gm account not routed in ffpxrefdta';
COMMENT ON COLUMN tem_fs.dim_fs_account.gm_account IS 'source: arkona.ext_eis_global_sypffxmst.fxmact';
COMMENT ON COLUMN tem_fs.dim_fs_account.gl_account IS 'source: arkona.ext_ffpxrefdta.g_l_acct_number';


-- Index: tem_fs.dim_fs_account_fs_account_key_idx

-- DROP INDEX tem_fs.dim_fs_account_fs_account_key_idx;

CREATE INDEX dim_fs_account_fs_account_key_idx
  ON tem_fs.dim_fs_account
  USING btree
  (fs_account_key);

-- Index: tem_fs.dim_fs_account_gl_account_idx

-- DROP INDEX tem_fs.dim_fs_account_gl_account_idx;

CREATE INDEX dim_fs_account_gl_account_idx
  ON tem_fs.dim_fs_account
  USING btree
  (gl_account COLLATE pg_catalog."default");

-- Index: tem_fs.dim_fs_account_gm_account_idx

-- DROP INDEX tem_fs.dim_fs_account_gm_account_idx;

CREATE INDEX dim_fs_account_gm_account_idx
  ON tem_fs.dim_fs_account
  USING btree
  (gm_account COLLATE pg_catalog."default");


-- Table: tem_fs.dim_fs_org

-- DROP TABLE tem_fs.dim_fs_org;

CREATE TABLE tem_fs.dim_fs_org
(
  fs_org_key serial NOT NULL,
  market citext NOT NULL,
  store citext NOT NULL,
  area citext NOT NULL,
  department citext NOT NULL,
  sub_department citext NOT NULL DEFAULT 'none'::citext,
  CONSTRAINT dim_fs_org_pkey PRIMARY KEY (fs_org_key),
  CONSTRAINT dim_fs_org_nk UNIQUE (market, store, area, department, sub_department)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tem_fs.dim_fs_org
  OWNER TO rydell;
GRANT ALL ON TABLE tem_fs.dim_fs_org TO rydell;

-- Index: tem_fs.dim_fs_org_area_idx

-- DROP INDEX tem_fs.dim_fs_org_area_idx;

CREATE INDEX dim_fs_org_area_idx
  ON tem_fs.dim_fs_org
  USING btree
  (area COLLATE pg_catalog."default");

-- Index: tem_fs.dim_fs_org_department_idx

-- DROP INDEX tem_fs.dim_fs_org_department_idx;

CREATE INDEX dim_fs_org_department_idx
  ON tem_fs.dim_fs_org
  USING btree
  (department COLLATE pg_catalog."default");

-- Index: tem_fs.dim_fs_org_fs_org_key_idx

-- DROP INDEX tem_fs.dim_fs_org_fs_org_key_idx;

CREATE INDEX dim_fs_org_fs_org_key_idx
  ON tem_fs.dim_fs_org
  USING btree
  (fs_org_key);

-- Index: tem_fs.dim_fs_org_store_idx

-- DROP INDEX tem_fs.dim_fs_org_store_idx;

CREATE INDEX dim_fs_org_store_idx
  ON tem_fs.dim_fs_org
  USING btree
  (store COLLATE pg_catalog."default");

-- Index: tem_fs.dim_fs_org_sub_department_idx

-- DROP INDEX tem_fs.dim_fs_org_sub_department_idx;

CREATE INDEX dim_fs_org_sub_department_idx
  ON tem_fs.dim_fs_org
  USING btree
  (sub_department COLLATE pg_catalog."default");


-- Table: tem_fs.xfm_fs_1

-- DROP TABLE tem_fs.xfm_fs_1;

CREATE TABLE tem_fs.xfm_fs_1
(
  the_year integer NOT NULL,
  year_month integer NOT NULL,
  page integer NOT NULL,
  line numeric(4,1) NOT NULL,
  col integer NOT NULL,
  gm_account citext NOT NULL,
  gl_account citext NOT NULL,
  CONSTRAINT xfm_fs_1_nk UNIQUE (year_month, gm_account, gl_account, page, line, col)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tem_fs.xfm_fs_1
  OWNER TO rydell;
GRANT ALL ON TABLE tem_fs.xfm_fs_1 TO rydell;


-- Table: tem_fs.fact_fs

-- DROP TABLE tem_fs.fact_fs;

CREATE TABLE tem_fs.fact_fs
(
  fs_key integer NOT NULL,
  fs_org_key integer NOT NULL,
  fs_account_key integer NOT NULL,
  amount integer NOT NULL,
  CONSTRAINT fact_fs_fs_account_key_fkey FOREIGN KEY (fs_account_key)
      REFERENCES tem_fs.dim_fs_account (fs_account_key) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fact_fs_fs_key_fkey FOREIGN KEY (fs_key)
      REFERENCES tem_fs.dim_fs (fs_key) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fact_fs_fs_org_key_fkey FOREIGN KEY (fs_org_key)
      REFERENCES tem_fs.dim_fs_org (fs_org_key) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fact_fs_nk UNIQUE (fs_key, fs_org_key, fs_account_key)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tem_fs.fact_fs
  OWNER TO rydell;
GRANT ALL ON TABLE tem_fs.fact_fs TO rydell;

-- Index: tem_fs.fact_fs_fs_account_key_idx

-- DROP INDEX tem_fs.fact_fs_fs_account_key_idx;

CREATE INDEX fact_fs_fs_account_key_idx
  ON tem_fs.fact_fs
  USING btree
  (fs_account_key);

-- Index: tem_fs.fact_fs_fs_key_idx

-- DROP INDEX tem_fs.fact_fs_fs_key_idx;

CREATE INDEX fact_fs_fs_key_idx
  ON tem_fs.fact_fs
  USING btree
  (fs_key);

-- Index: tem_fs.fact_fs_fs_org_key_idx

-- DROP INDEX tem_fs.fact_fs_fs_org_key_idx;

CREATE INDEX fact_fs_fs_org_key_idx
  ON tem_fs.fact_fs
  USING btree
  (fs_org_key);


