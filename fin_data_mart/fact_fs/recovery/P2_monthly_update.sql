﻿/*
02/27/21
crossing my  fingers that this works after pgadmin ate my work (fact_fs_monthly_update_including_page_2.sql)
experimenting on new schema tem_fs
looking at 202101 
  expenses look ok
  sales & income no good variable is high fixed is fine

added and aaa.current_row to each join on fin.dim_account
which takes me back to, variable sales line 1 is a bit high, but gross line 2 is correct
line 1 is 10763 high ( select 11636356 - 11625593 ) 
as well as line 60 is correct
i remember seeing this before, seems it made no sense, but is good enuf (at least for now)

line 7 variable is missing
oh shit, thats because the variable column is showing fixed values, store is ok though
everything the same on RY2

other than that, this should be good to go


TODO: change the schema for table page_2 to fin when ready for production
*/
do
$$
declare 
   _year integer := 2021;
   _year_month integer := 202101;
begin

delete 
from tem_fs.page_2
where year_month = _year_month;
-- base rows RY1
insert into tem_fs.page_2
select 'RY1', _year, _year_month, n.line::integer as line, n.line_label, 
  0 as store, 0 as variable, 0 as fixed
from (
  select *
  from fin.dim_fs
  where year_month = _year_month and page = 2) n
group by n.line::integer, n.line_label;
-- base rows RY2
insert into tem_fs.page_2
select 'RY2', _year, _year_month, n.line::integer as line, n.line_label, 
  0 as store, 0 as variable, 0 as fixed
from (
  select *
  from fin.dim_fs
  where year_month = _year_month and page = 2) n
group by n.line::integer, n.line_label;
-- lines 4 - 54
update tem_fs.page_2 z
set fixed = x.amount
from (-- p4 fixed
  select year_month, store as store_code, line, sum(amount) as amount
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = _year_month
    and b.page = 4
    and line between 4 and 54 
  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  group by year_month, store, line) x
where z.year_month = x.year_month
  and z.store_code = x.store_code
  and z.line = x.line;  
-- lines 4 - 54  
update tem_fs.page_2 z
set variable = x.amount
from (-- p3 variable
  select year_month, store as store_code, line, sum(amount) as amount
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = _year_month
    and b.page = 4
    and line between 4 and 54 
  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  group by year_month, store, line) x
where z.year_month = x.year_month
  and z.store_code = x.store_code
  and z.line = x.line;  
-- lines 4 - 54
update tem_fs.page_2 z
set store = x.amount
from (-- total of p3 & p4
  select year_month, store as store_code, line, sum(amount) as amount
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = _year_month
    and b.page in(3,4)
    and line between 4 and 54 
  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
  group by year_month, store, line) x
where z.year_month = x.year_month
  and z.store_code = x.store_code
  and z.line = x.line;  
-- lines 7,17,40,49,55,56, 57 all 3 columns
update tem_fs.page_2 z
set store = x.store,
    variable = x.variable,
    fixed = x.fixed
from (  
  select store_code, year_month, 7 as line,
    sum(store) as store,
    sum(variable) as variable,
    sum(fixed) as fixed
  from tem_fs.page_2
  where year_month = _year_month
    and line between 4 and 6
  group by store_code, year_month
  union all
  select store_code, year_month, 17 as line,
    sum(store) as store,
    sum(variable) as variable,
    sum(fixed) as fixed
  from tem_fs.page_2
  where year_month = _year_month
    and line between 8 and 16
  group by store_code, year_month
  union all
  select store_code, year_month, 40 as line,
    sum(store) as store,
    sum(variable) as variable,
    sum(fixed) as fixed
  from tem_fs.page_2
  where year_month = _year_month
    and line between 18 and 39
  group by store_code, year_month
  union all
  select store_code, year_month, 49 as line,
    sum(store) as store,
    sum(variable) as variable,
    sum(fixed) as fixed
  from tem_fs.page_2
  where year_month = _year_month
    and line between 41 and 48
  group by store_code, year_month
  union all
  select store_code, year_month, 55 as line,
    sum(store) as store,
    sum(variable) as variable,
    sum(fixed) as fixed
  from tem_fs.page_2
  where year_month = _year_month
    and (
      line between 49 and 54
      or line between 41 and 48)
  group by store_code, year_month
  union all
  select store_code, year_month, 56 as line,
    sum(store) as store,
    sum(variable) as variable,
    sum(fixed) as fixed
  from tem_fs.page_2
  where year_month = _year_month
    and (
      line between 49 and 54
      or line between 41 and 48
      or line between 8 and 16
      or line between 18 and 39)
  group by store_code, year_month
  union all
  select store_code, year_month, 57 as line,
    sum(store) as store,
    sum(variable) as variable,
    sum(fixed) as fixed
  from tem_fs.page_2
  where year_month = _year_month
    and (
      line between 49 and 54
      or line between 41 and 48
      or line between 8 and 16
      or line between 18 and 39
      or line between 4 and 6)
  group by store_code, year_month) x
where z.store_code = x.store_code
  and z.year_month = x.year_month
  and z.line = x.line;  
  -- variable lines 1 & 2
update tem_fs.page_2 z
set variable = x.amount
from (
  select m.store_code, m.year_month, 1 as line,
    m.fi_sales + n.uc_sales + o.nc_sales as amount
  from (-- ok. fi is some kind of goofy about what counts as sales and what counts as cogs-- this works
    select store as store_code, year_month, -1 * (sales_a + sales_b) as fi_sales, fi_gross
    from (
      select c.store, b.year_month,
      sum(case when line in (1, 2, 11, 12) then amount else 0 end) as sales_a,
      sum(case when line in (6, 7, 17) and amount < 0 then amount else 0 end) as sales_b,
      sum(-1 * amount) as fi_gross
      from fin.fact_fs a
      inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
      inner join fin.dim_account aaa on aa.gl_account = aaa.account
        and aaa.current_row
      inner join fin.dim_fs b on a.fs_key = b.fs_key
        and b.year_month = _year_month
        and b.page = 17 
        and b.line between 1 and 20 -- f/i
      inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
      group by c.store, b.year_month
      having sum(amount) <> 0) x) m
  left join (-- used cars sales off 201707 by what i believe is in line 11, but gross is ok, yep acct 165300
    select c.store as store_code, b.year_month, page,
      sum(case when amount < 0 then -1 * amount else 0 end) as uc_sales,
      sum(-1 * amount) as uc_gross
    from fin.fact_fs a
    inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
    inner join fin.dim_account aaa on aa.gl_account = aaa.account
      and aaa.current_row
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = _year_month
      and b.page = 16 and b.line between 1 and 14 -- used cars
    inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
    group by c.store, b.year_month, page
    having sum(amount) <> 0
    order by c.store, b.year_month) n on m.store_code = n.store_code and m.year_month = n.year_month
  left join (-- new cars
    select c.store as store_code, b.year_month, 
      sum(case when amount < 0 then - 1 * amount else 0 end) as nc_sales,
      sum(-1 * amount) as nc_gross
    from fin.fact_fs a
    inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
    inner join fin.dim_account aaa on aa.gl_account = aaa.account
      and aaa.current_row
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = _year_month
      and b.page between 5 and 15
    inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
    group by c.store, b.year_month
    having sum(amount) <> 0) o on m.store_code = o.store_code and m.year_month = o.year_month
  union
  select m.store_code, m.year_month, 2 as line,
    m.fi_gross + n.uc_gross + o.nc_gross as gross
  from (-- ok. fi is some kind of goofy about what counts as sales and what counts as cogs-- this works
    select store as store_code, year_month, -1 * (sales_a + sales_b) as fi_sales, fi_gross
    from (
      select c.store, b.year_month,
      sum(case when line in (1, 2, 11, 12) then amount else 0 end) as sales_a,
      sum(case when line in (6, 7, 17) and amount < 0 then amount else 0 end) as sales_b,
      sum(-1 * amount) as fi_gross
      from fin.fact_fs a
      inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
      inner join fin.dim_account aaa on aa.gl_account = aaa.account
        and aaa.current_row
      inner join fin.dim_fs b on a.fs_key = b.fs_key
        and b.year_month = _year_month
        and b.page = 17 
        and b.line between 1 and 20 -- f/i
      inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
      group by c.store, b.year_month
      having sum(amount) <> 0) x) m
  left join (-- used cars sales off 201707 by what i believe is in line 11, but gross is ok, yep acct 165300
    select c.store as store_code, b.year_month, page,
      sum(case when amount < 0 then -1 * amount else 0 end) as uc_sales,
      sum(-1 * amount) as uc_gross
    from fin.fact_fs a
    inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
    inner join fin.dim_account aaa on aa.gl_account = aaa.account
      and aaa.current_row
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = _year_month
      and b.page = 16 and b.line between 1 and 14 -- used cars
    inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
    group by c.store, b.year_month, page
    having sum(amount) <> 0
    order by c.store, b.year_month) n on m.store_code = n.store_code and m.year_month = n.year_month
  left join (-- new cars
    select c.store as store_code, b.year_month, 
      sum(case when amount < 0 then - 1 * amount else 0 end) as nc_sales,
      sum(-1 * amount) as nc_gross
    from fin.fact_fs a
    inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
    inner join fin.dim_account aaa on aa.gl_account = aaa.account
      and aaa.current_row
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = _year_month
      and b.page between 5 and 15
    inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
    group by c.store, b.year_month
    having sum(amount) <> 0) o on m.store_code = o.store_code and m.year_month = o.year_month) x
where z.store_code = x.store_code
  and z.year_month = x.year_month
  and z.line = x.line;    
-- fixed lines 1 & 2
update tem_fs.page_2 z
set fixed = x.amount
from (
  select b.year_month, c.store as store_code, 1 as line,
    sum(case when col in (1,2) then -1 * amount else 0 end) as amount
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = _year_month
    and b.page = 16 
    and b.line between 20 and 59
  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  where store <> 'none'
  group by b.year_month, c.store
  having sum(amount) <> 0
  union
  select b.year_month, c.store, 2 as line,
    sum(-1 * amount) as gross
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = _year_month
    and b.page = 16 
    and b.line between 20 and 59
  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  where store <> 'none'
  group by b.year_month, c.store
  having sum(amount) <> 0) x
where z.store_code = x.store_code
  and z.year_month = x.year_month
  and z.line = x.line;
-- lines 1 & 2 store
update tem_fs.page_2 z
set store = variable + fixed
where line between 1 and 2
  and year_month = _year_month;  
-- line 58 operating profit
update tem_fs.page_2 z
set store = x.store,
    variable = x.variable, 
    fixed = x.fixed
from (
  select a.store_code, a.year_month, a.store - b.store as store,
    a.variable - b.variable as variable, 
    a.fixed - b.fixed as fixed
  from (
    select store_code, year_month, store, variable, fixed
    from tem_fs.page_2
    where line = 2
      and year_month = _year_month) a
  left join (  
    select store_code, year_month, store, variable, fixed
    from tem_fs.page_2
    where line = 57
      and year_month = _year_month) b on a.store_code = b.store_code 
      and a.year_month = b.year_month) x 
where z.store_code = x.store_code 
  and z.year_month = x.year_month
  and z.line = 58;     
-- line 59 adds and deduct
update tem_fs.page_2 z
set store = x.amount
from (
  select store as store_code, year_month, sum(-1 * amount) as amount
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = _year_month
    and b.page = 3
    and b.line > 62
  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
  where store in ('ry1','ry2')
  group by store, year_month) x
where z.store_code = x.store_code
  and z.year_month = x.year_month
  and z.line = 59;    
-- lines 60, 63, 65
update tem_fs.page_2 z
set store = x.store
from (
  select a.store_code, a.year_month, a.store + b.store as store
  from (
    select store_code, year_month, store
    from tem_fs.page_2
    where line = 58
      and year_month = _year_month) a
  left join (  
    select store_code, year_month, store
    from tem_fs.page_2
    where line = 59
      and year_month = _year_month) b on a.store_code = b.store_code and a.year_month = b.year_month) x 
where z.store_code = x.store_code 
  and z.year_month = x.year_month
  and z.line in (60,63,65);  
end
$$;


select * 
from tem_fs.page_2 
where year_month = 202101
order by store_code, line;