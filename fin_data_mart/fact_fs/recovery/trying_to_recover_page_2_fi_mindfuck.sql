﻿2/27/21
feels like a mind fuck
looking at ...recovery\p2_monthly_update.sql, generated tem_fs_page_2 for 202101 to see if it is correct
202101 variable sales is high compared to fs
so drilling into what makes up Page 2 lines 1 & 2 referencing ...recovery\p2_monthly_update.sql

started out with fi and lo and behold 202101, 202012, 202006 fi disagrees with the statement
for all 3 months
but for 202012 & 202006 variable sales is correct
which makes no sense, but it appears that

select line, col, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
inner join fin.dim_account aaa on aa.gl_account = aaa.account
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202006
  and b.page = 17 
  and b.line between 1 and 20 -- f/i
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.store = 'ry1'
group by line, col  
order by line, col
  
used lines 11,12,17,19 are off

col 1  query       fs  
11   -77807    77,339
12   -17365    16,624
17  -252749   248,348

select line,  col, string_agg(distinct gm_account, ','), string_agg(distinct gl_account, ',')
from fin.fact_fs a
inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
inner join fin.dim_account aaa on aa.gl_account = aaa.account
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202101
  and b.page = 17 
  and b.line between 1 and 20 -- f/i
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.store = 'ry1'
group by line,  col  

select * 
from fin.page_2
where year_month = 202012
order by store_code,line

select line, col, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
inner join fin.dim_account aaa on aa.gl_account = aaa.account
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202012
  and b.page = 17 
  and b.line between 1 and 20 -- f/i
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.store = 'ry1'
group by line, col  
order by line, col

select * 
from arkona.ext_ffpxrefdta 
where factory_financial_year = 2021
limit 100

select *
from arkona.ext_eisglobal_sypffxmst a
where a.fxmcyy = 2021
and a.fxmpge = 17
  AND a.fxmlne between 11 and 17

  
select a.fxmact, a.fxmlne, a.fxmcol, string_agg(b.g_l_acct_number, ',')
from arkona.ext_eisglobal_sypffxmst a
left join arkona.ext_ffpxrefdta b on a.fxmcyy = b.factory_financial_year
  and a.fxmact = b.factory_account
  and left(b.g_l_acct_number, 1) = '1'
where a.fxmcyy = 2021
and a.fxmpge = 17
  AND a.fxmlne between 11 and 17
group by a.fxmact, a.fxmlne, a.fxmcol
order by a.fxmlne, a.fxmcol

-- well hell yes, this is right on
select c.account, sum(a.amount)
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 202101
join fin.dim_account c on a.account_key = c.account_key
  and c.account in ('180802','180800','180803','180801')  
where a.post_status = 'Y'
group by c.account

the difference is acct 180802 (-2061/=4122) & 180803(797/2391)
-- but this is wrong
select gl_account, gm_account, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs_account aa on a.fs_account_key = aa.fs_account_key
inner join fin.dim_account aaa on aa.gl_account = aaa.account
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202006
  and b.page = 17 
  and b.line = 11
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.store = 'ry1'
group by gl_account, gm_account

select * from fin.dim_fs_account where gl_account in ('180802','180803')