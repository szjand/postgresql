﻿-- yikes
select *
from (
  select c.account, sum(a.amount) as amount
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201703
    and b.the_date < current_date
  inner join fin.dim_account c on a.account_key = c.account_key
  where post_status = 'Y'
  group by c.account) e  
full outer join (  
  select account, sum(amount) as amount
  from arkona.xfm_glptrns
  where post_status = 'Y'
    and the_date between '03/01/2017' and current_date - 1
  group by account) f on e.account = f.account
where e.amount <> f.amount

-- year to date acct balances
-- shit, can't do it don't store all of glptrns
-- need to do this in code, something like ext_glptrns_2016, ext_glptrs_2017 ...
select * 
from (
  select c.account, sum(a.amount) as amount
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_year = 2017
    and b.the_date < current_date
  inner join fin.dim_account c on a.account_key = c.account_key
  where post_status = 'Y'
  group by c.account) e  
full outer join (  
  select account, sum(amount) as amount
  from arkona.xfm_glptrns
  where post_status = 'Y'
    and the_date between '03/01/2017' and current_date - 1
  group by account) f on e.account = f.account
where e.amount <> f.amount

/*

select the_Date, change, count(*)
from fin.fact_gl_change_log 
group by the_date,change
order by the_date

*/
