﻿/*
1/26/17
  i have been making the from_date the first of the month for any new account
2/8/17
    oops, forgot to update fin.dim_fs_account with new account info
this checks for missing accounts in fin.dim_fs_account
select a.*, c.*
from fin.dim_account a 
left join fin.dim_fs_account b on a.account = b.gl_account
left join arkona.ext_ffpxrefdta c on a.account = c.g_l_acct_number
where b.gl_account is null
  and c.g_l_acct_number is not null
order by a.account    

2/25/17
every time the scrip fails, i am reinventing trying to figure out what is different
that does not bode well for the notion of minimizing manual maintenance once i get the 
fucking pipelines working
in addition i have not been updating dim_account as type 2, just overwriting
easy to rationalize, going type 2 means every query involving , no wait, joins to dim_Account
will always be on account key, so that is not an issue,
the issue is in the etl, providing the appropriate account key for each transaction
and , oh shit, an account changes in the middle of the month, i have been perceiving accounts as
having a grain of month, so if i update an account with some new type 2 info on 2/25/17, i then 
have to retroactively update the fact_gl table for every row in february with the old account key
to the new account key, and just to make it more interesting, what if an account changes 
multiple times during the month
yikes
would love to run this by the group and get some input


5/8/17
the luigi script for fact_gl has been processing all changes in glpmast as type 2 changes and populating
dim_account accordingly 
(as opposed to production, pg 173, where i have been flagging changes and doing type 1 updates in this script)
is it good enuf, i can't decide
most of the changes are department changes
the script is cool and seems to work ok, i just can't decide whether i need to intervene and establish
whether a change is a legitimate type 2 change or a correction and subsequently type 1


this is the all in one type 2 script: do the update in the cte returning *

with upd as (
  update fin.dim_account
  set current_row = false,
      row_thru_date = current_date - 1
  where account in ( -- the account(s) that have changed
    select c.account
    from ( -- changed accounts, any attribute
      select account, md5(a::text) as hash
      from arkona.xfm_glpmast a) c
    inner join (
      select account, md5(a::text) as hash
      from (
        select account,account_type_code,description,store_code,department_code,typical_balance
        from fin.dim_account
        where current_row = true) a) d on c.account = d.account and c.hash <> d.hash)
  and current_row = true
  returning *)
insert into fin.dim_account (account, account_type_code, account_type, description,
  store_code, store, department_code, department, typical_balance, current_row,
  row_from_date, row_reason)
select a.account, a.account_type_code, c.account_type,
  a.description, a.store_code,
  case a.store_code
    when 'RY1' then 'Rydell GM'
    when 'RY2' then 'Honda Nissan'
  end,
  a.department_code, b.dept_description,
  a.typical_balance,
  true, current_date, 'type 2 update'
from (
  select z.account, z.account_type_code, z.description, z.store_code, z.department_code, z.typical_balance
  from arkona.xfm_glpmast z
  inner join upd on z.account = upd.account) a
left join arkona.ext_glpdept b on a.department_code = b.department_code
  and a.store_code = b.company_number
left join dds.lkp_account_types c on a.account_type_code = c.account_type_code;

*/  
10/4/16
pussied out on automating change processing for dim_account
so do it here

-- differences

select account, account_type, description, store_code, department_code, typical_balance
from arkona.xfm_glpmast  
except
select account, account_type_code, description, store_code, department_code, typical_balance
from fin.dim_account



select 'xfm' as source, account, account_type, description, store_code, department_code, typical_balance
from arkona.xfm_glpmast  
where account in ('245400','244400','245401','244300','244301')
union 
select 'dim', account, account_type_code, description, store_code, department_code, typical_balance
from fin.dim_account
where account in ('245400','244400','245401','244300','244301')
order by account, source

-- new account

insert into fin.dim_account (account, account_type_code, account_type, description,
  store_code, store, department_code, department, typical_balance, current_row,
  from_date, thru_date, row_reason) 
select a.account, a.account_type,
  case account_type
    when '1' then 'Asset'
    when '2' then 'Liability'
    when '3' then 'Equity'
    when '4' then 'Sale'
    when '5' then 'COGS'
    when '6' then 'Income'
    when '7' then 'Other Income'
    when '8' then 'Expense'
    when '9' then 'Other Expense'
  end as account_type,
  a.description, a.store_code,
  case a.store_code
    when 'RY1' then 'Rydell GM'
    when 'RY2' then 'Honda Nissan'
  end, 
  a.department_code, b.department,
  a.typical_balance, 
  true, '09/12/2016', '12/31/9999', 'new account created by controller'
from arkona.xfm_glpmast a -- new rows in glpmast
left join dds.xfm_glpdept b on a.department_code = b.department_code
where a.account = '16603';


-- new account
-- 12/16/16 285003
-- glpdept now arkona.ext_glpdept
insert into fin.dim_account (account, account_type_code, account_type, description,
  store_code, store, department_code, department, typical_balance, current_row,
  row_from_date, row_thru_date, row_reason) 
select a.account, a.account_type,
  case account_type
    when '1' then 'Asset'
    when '2' then 'Liability'
    when '3' then 'Equity'
    when '4' then 'Sale'
    when '5' then 'COGS'
    when '6' then 'Income'
    when '7' then 'Other Income'
    when '8' then 'Expense'
    when '9' then 'Other Expense'
  end as account_type,
  a.description, a.store_code,
  case a.store_code
    when 'RY1' then 'Rydell GM'
    when 'RY2' then 'Honda Nissan'
  end, 
  a.department_code, b.dept_description,
  a.typical_balance, 
  true, '12/15/2016', '12/31/9999', 'new account created by controller'
from arkona.xfm_glpmast a -- new rows in glpmast
-- left join dds.xfm_glpdept b on a.department_code = b.department_code
left join arkona.ext_glpdept b on a.department_code = b.department_code
  and a.store_code = b.company_number
where a.account = '285003';


-- changed accounts
select 'xfm' as source, account, account_type, description, store_code, department_code, typical_balance
from arkona.xfm_glpmast  
where account in ('126105','231201')
union 
select 'dim', account, account_type_code, description, store_code, department_code, typical_balance
from fin.dim_account
where account in ('126105','231201')
order by account

select *
from fin.dim_fs_account
where gl_Account = '231201'

select *
from fin.fact_fs a
inner join fin.dim_Fs b on a.fs_key = b.fs_key
where fs_Account_key in (216,3352)

select *
from fin.dim_account
where account = '126105'


-- update old row
update fin.dim_account
  set current_row = false,
      thru_date = '08/31/2016'      
where account = '126105';
-- new row
insert into fin.dim_account (account, account_type_code, account_type, description,
  store_code, store, department_code, department, typical_balance, current_row,
  from_date, thru_date, row_reason) 
select a.account, a.account_type,
  case account_type
    when '1' then 'Asset'
    when '2' then 'Liability'
    when '3' then 'Equity'
    when '4' then 'Sale'
    when '5' then 'COGS'
    when '6' then 'Income'
    when '7' then 'Other Income'
    when '8' then 'Expense'
    when '9' then 'Other Expense'
  end as account_type,
  a.description, a.store_code,
  case a.store_code
    when 'RY1' then 'Rydell GM'
    when 'RY2' then 'Honda Nissan'
  end, 
  a.department_code, b.department,
  a.typical_balance, 
  true, '09/01/2016', '12/31/9999', 'description changed'
from arkona.xfm_glpmast a -- new rows in glpmast
left join dds.xfm_glpdept b on a.department_code = b.department_code
where a.account = '126105';


-- new account
-- 12/30/16 130700, 230700
-- glpdept now arkona.ext_glpdept
insert into fin.dim_account (account, account_type_code, account_type, description,
  store_code, store, department_code, department, typical_balance, current_row,
  row_from_date, row_thru_date, row_reason) 
select a.account, a.account_type,
  case account_type
    when '1' then 'Asset'
    when '2' then 'Liability'
    when '3' then 'Equity'
    when '4' then 'Sale'
    when '5' then 'COGS'
    when '6' then 'Income'
    when '7' then 'Other Income'
    when '8' then 'Expense'
    when '9' then 'Other Expense'
  end as account_type,
  a.description, a.store_code,
  case a.store_code
    when 'RY1' then 'Rydell GM'
    when 'RY2' then 'Honda Nissan'
  end, 
  a.department_code, b.dept_description,
  a.typical_balance, 
  true, '12/30/2016', '12/31/9999', 'new account created by controller'
from arkona.xfm_glpmast a -- new rows in glpmast
-- left join dds.xfm_glpdept b on a.department_code = b.department_code
left join arkona.ext_glpdept b on a.department_code = b.department_code
  and a.store_code = b.company_number
where a.account = '230700';



-- new account
-- 1/13/17 180601, 180801
-- glpdept now arkona.ext_glpdept
insert into fin.dim_account (account, account_type_code, account_type, description,
  store_code, store, department_code, department, typical_balance, current_row,
  row_from_date, row_thru_date, row_reason) 
select a.account, a.account_type,
  case account_type
    when '1' then 'Asset'
    when '2' then 'Liability'
    when '3' then 'Equity'
    when '4' then 'Sale'
    when '5' then 'COGS'
    when '6' then 'Income'
    when '7' then 'Other Income'
    when '8' then 'Expense'
    when '9' then 'Other Expense'
  end as account_type,
  a.description, a.store_code,
  case a.store_code
    when 'RY1' then 'Rydell GM'
    when 'RY2' then 'Honda Nissan'
  end, 
  a.department_code, b.dept_description,
  a.typical_balance, 
  true, '01/01/2017', '12/31/9999', 'new account created by controller'
from arkona.xfm_glpmast a -- new rows in glpmast
-- left join dds.xfm_glpdept b on a.department_code = b.department_code
left join arkona.ext_glpdept b on a.department_code = b.department_code
  and a.store_code = b.company_number
where a.account in ('180601','180801');


-- 1/25/17
-- new accounts 25104D,I,T,R,M,F,P
insert into fin.dim_account (account, account_type_code, account_type, description,
  store_code, store, department_code, department, typical_balance, current_row,
  row_from_date, row_thru_date, row_reason) 
select a.account, a.account_type,
  case account_type
    when '1' then 'Asset'
    when '2' then 'Liability'
    when '3' then 'Equity'
    when '4' then 'Sale'
    when '5' then 'COGS'
    when '6' then 'Income'
    when '7' then 'Other Income'
    when '8' then 'Expense'
    when '9' then 'Other Expense'
  end as account_type,
  a.description, a.store_code,
  case a.store_code
    when 'RY1' then 'Rydell GM'
    when 'RY2' then 'Honda Nissan'
  end, 
  a.department_code, b.dept_description,
  a.typical_balance, 
  true, '01/01/2017', '12/31/9999', 'new account created by controller'
from (
  select account, account_type, description, store_code, department_code, typical_balance
  from arkona.xfm_glpmast
  except
  select account, account_type_code, description, store_code, department_code, typical_balance
  from fin.dim_account) a
 left join arkona.ext_glpdept b on a.department_code = b.department_code
   and a.store_code = b.company_number;

  -- 2/2/17 
select *
from (
  (select account, account_type, description, store_code, department_code, typical_balance
  from arkona.xfm_glpmast
  except
  select account, account_type_code, description, store_code, department_code, typical_balance
  from fin.dim_account)
union
  (select account, account_type_code, description, store_code, department_code, typical_balance
  from fin.dim_account
  where current_row = true
      and account not in ('none', '*E*', '*VOID', '*SPORD')
  except
  select account, account_type, description, store_code, department_code, typical_balance
  from arkona.xfm_glpmast)) x
order by account  

select * from fin.dim_Account where account = '130201'

-- new accounts: pto accrual accounts 
-- 2/15/17: 164501 aftermarket policy
insert into fin.dim_account (account, account_type_code, account_type, description,
  store_code, store, department_code, department, typical_balance, current_row,
  row_from_date, row_thru_date, row_reason) 
select a.account, a.account_type,
  case account_type
    when '1' then 'Asset'
    when '2' then 'Liability'
    when '3' then 'Equity'
    when '4' then 'Sale'
    when '5' then 'COGS'
    when '6' then 'Income'
    when '7' then 'Other Income'
    when '8' then 'Expense'
    when '9' then 'Other Expense'
  end as account_type,
  a.description, a.store_code,
  case a.store_code
    when 'RY1' then 'Rydell GM'
    when 'RY2' then 'Honda Nissan'
  end, 
  a.department_code, b.dept_description,
  a.typical_balance, 
  true, '02/01/2017', '12/31/9999', 'new account created by controller'
from (
  select *
  from (
    select account, account_type, description, store_code, department_code, typical_balance
    from arkona.xfm_glpmast 
    except
    select account, account_type_code, description, store_code, department_code, typical_balance
    from fin.dim_account) z 
  where not exists ( -- some of the differences are changes, not new rows
    select 1
    from fin.dim_account
    where account = z.account)) a
left join arkona.ext_glpdept b on a.department_code = b.department_code
  and a.store_code = b.company_number;

-- changed accounts, change description on pto accrual accounts: 130201,130203,130205,130224

update fin.dim_account y
set description = x.description,
    department_code = x.department_code,
    department = (select dept_description from arkona.ext_glpdept where department_code = x.department_code and company_number = 'RY1')
from (
 select b.account, b.description, b.department_code
 from fin.dim_account a
 inner join arkona.xfm_glpmast b on a.account = b.account
 where (a.description <> b.description or a.department_code <> b.department_code)
   and a.current_row = true) x
where y.account = x.account 

-- 2/8/17
-- new account to be added to fin.dim_fs_account
insert into fin.dim_fs_account (gm_account,gl_account,row_from_date,current_row,row_reason)
select c.factory_account, a.account, '2017-01-01', True, 'new gl_account'
from fin.dim_account a 
left join fin.dim_fs_account b on a.account = b.gl_account
left join arkona.ext_ffpxrefdta c on a.account = c.g_l_acct_number
where b.gl_account is null
  and c.g_l_acct_number is not null;

select * from arkona.ext_ffpxrefdta where g_l_acct_number = '164501'


-- 2/22/17
-- changed account type from 7 to 4 (other income to income)
select 'xfm' as source, account, account_type, description, store_code, department_code, typical_balance
from arkona.xfm_glpmast  
where account in ('245400','244400','245401','244300','244301')
union 
select 'dim', account, account_type_code, description, store_code, department_code, typical_balance
from fin.dim_account
where account in ('245400','244400','245401','244300','244301')

update fin.dim_account
set account_type_code = '4',
    account_type = 'Income'
-- select * from fin.dim_Account
where account in ('245400','244400','245401','244300','244301')


-- 2/25/17 ---------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------
every time the scrip fails, i am reinventing trying to figure out what is different
that does not bode well for the notion of minimizing manual maintenance once i get the 
fucking pipelines working
in addition i have not been updating dim_account as type 2, just overwriting
easy to rationalize, going type 2 means every query involving , no wait, joins to dim_Account
will always be on account key, so that is not an issue,
the issue is in the etl, providing the appropriate account key for each transaction
and , oh shit, an account changes in the middle of the month, i have been perceiving accounts as
having a grain of month, so if i update an account with some new type 2 info on 2/25/17, i then 
have to retroactively update the fact_gl table for every row in february with the old account key
to the new account key, and just to make it more interesting, what if an account changes 
multiple times during the month
yikes
would love to run this by the group and get some input
so

-- new accounts
select *
from arkona.xfm_glpmast a
where not exists (
  select 1
  from fin.dim_account
  where account = a.account)

-- removed accounts
select *
from fin.dim_account a
where account not in ('none','*E*','*VOID','*SPORD')
  AND not exists (
    select 1
    from arkona.xfm_glpmast
    where account = a.account);  

-- changed accounts
select *
from (
  select 'xfm', account, md5(a::text) as hash
  from (
    select account, account_type_code, description, store_code, department_code, typical_balance
    from arkona.xfm_glpmast) a) c
inner join (
  select 'dim', account, md5(b::text) as hash
  from (
    select account, account_type_code, description, store_code, department_code, typical_balance
    from fin.dim_account
    where current_row = true) b) d on c.account = d.account and c.hash <> d.hash

so, in accodance with the rant at the beginning of this section
233204, 233203, 233205, 133204 all have changed
are there any rows in fact_gl for those accounts?    

select *
from fin.fact_gl a
inner join dds.dim_date b on a.date_key= b.date_key
  and b.year_month = 201702
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account in ('233204','233203','233205','133204')  

lucked out here, no existing rows for the changed accounts


-- 2/25/17: 133211, 233211: UNUM VOLUNTARY INS PAYABLE
insert into fin.dim_account (account, account_type_code, account_type, description,
  store_code, store, department_code, department, typical_balance, current_row,
  row_from_date, row_thru_date, row_reason) 
select a.account, a.account_type,
  case account_type
    when '1' then 'Asset'
    when '2' then 'Liability'
    when '3' then 'Equity'
    when '4' then 'Sale'
    when '5' then 'COGS'
    when '6' then 'Income'
    when '7' then 'Other Income'
    when '8' then 'Expense'
    when '9' then 'Other Expense'
  end as account_type,
  a.description, a.store_code,
  case a.store_code
    when 'RY1' then 'Rydell GM'
    when 'RY2' then 'Honda Nissan'
  end, 
  a.department_code, b.dept_description,
  a.typical_balance, 
  true, '02/01/2017', '12/31/9999', 'new account created by controller'
from (
  select account, account_type, description, store_code, department_code, typical_balance
  from arkona.xfm_glpmast z
  where not exists ( -- some of the differences are changes, not new rows
    select 1
    from fin.dim_account
    where account = z.account))a
left join arkona.ext_glpdept b on a.department_code = b.department_code
  and a.store_code = b.company_number;

-- changed accounts
select *
from (
  select 'xfm', account, md5(a::text) as hash
  from (
    select account, account_type, description, store_code, department_code, typical_balance
    from arkona.xfm_glpmast) a) c
inner join (
  select 'dim', account, md5(b::text) as hash
  from (
    select account, account_type_code, description, store_code, department_code, typical_balance
    from fin.dim_account
    where current_row = true) b) d on c.account = d.account and c.hash <> d.hash

select 'xfm' as source, account, account_type, description, store_code, department_code, typical_balance
from arkona.xfm_glpmast
where account in ('133204','233204','233203','233205')
union
select 'dim', account, account_type_code, description, store_code, department_code, typical_balance
from fin.dim_account
where current_row = true
  and account in ('133204','233204','233203','233205')
order by account, source  

update fin.dim_account a
set description = x.description
from (
  select account, account_type, description, store_code, department_code, typical_balance
  from arkona.xfm_glpmast
  where account in ('133204','233204','233203','233205')) x
where a.account = x.account  


-- 2/28/17 -------------------------------------------------------------------------------------
the question of removed accounts: 233211 was added this month and is now being removed
does not show up in chart of accounts as an inactive account
???
seems that my thought all along has been, i do not care about inactive accounts, an account
either has transactions against it or it does not
but still, apparently jeri found a was to delete an account, and it does not exists in
rydedata.glpmast
per Jeri, if there have been no postings to an account it can be deleted

-- new accounts: 133212, 233212
insert into fin.dim_account (account, account_type_code, account_type, description,
  store_code, store, department_code, department, typical_balance, current_row,
  row_from_date, row_thru_date, row_reason) 
select a.account, a.account_type,
  case account_type
    when '1' then 'Asset'
    when '2' then 'Liability'
    when '3' then 'Equity'
    when '4' then 'Sale'
    when '5' then 'COGS'
    when '6' then 'Income'
    when '7' then 'Other Income'
    when '8' then 'Expense'
    when '9' then 'Other Expense'
  end as account_type,
  a.description, a.store_code,
  case a.store_code
    when 'RY1' then 'Rydell GM'
    when 'RY2' then 'Honda Nissan'
  end, 
  a.department_code, b.dept_description,
  a.typical_balance, 
  true, '02/01/2017', '12/31/9999', 'new account created by controller'
from (
  select account, account_type, description, store_code, department_code, typical_balance
  from arkona.xfm_glpmast z
  where not exists ( -- some of the differences are changes, not new rows
    select 1
    from fin.dim_account
    where account = z.account))a
left join arkona.ext_glpdept b on a.department_code = b.department_code
  and a.store_code = b.company_number;

-- removed account 233211 - whack it
-- removed accounts
select *
from fin.dim_account a
where account not in ('none','*E*','*VOID','*SPORD')
  AND not exists (
    select 1
    from arkona.xfm_glpmast
    where account = a.account);  

select * from fin.fact_gl where account_key = (select account_key from fin.dim_Account where account = '233211')

delete
from fin.dim_account 
where account = '233211';

-- changed accounts
select *
from (
  select 'xfm', account, md5(a::text) as hash
  from (
    select account, account_type_code, description, store_code, department_code, typical_balance
    from arkona.xfm_glpmast) a) c
inner join (
  select 'dim', account, md5(b::text) as hash
  from (
    select account, account_type_code, description, store_code, department_code, typical_balance
    from fin.dim_account
    where current_row = true) b) d on c.account = d.account and c.hash <> d.hash

select 'xfm' as source, account, account_type_code, description, store_code, department_code, typical_balance
from arkona.xfm_glpmast
where account in ('133211')
union
select 'dim', account, account_type_code, description, store_code, department_code, typical_balance
from fin.dim_account
where current_row = true
  and account in ('133211')
order by account, source  
-- description changes from UNUM VOLUNTARY INS PAYABLE to N/C ADVERTISING PACK
update fin.dim_account a
set description = x.description
from (
  select account, account_type, description, store_code, department_code, typical_balance
  from arkona.xfm_glpmast
  where account in ('133211')) x
where a.account = x.account  

---------------------------------------------------------------------------------------------------------

2457001: 'NC' 'New Vehicle'
145504: 'FI' 'Finance'
select * from arkona.xfm_glpmast where account in ('2457001','145504')
select * from fin.dim_Account where account in ('2457001','145504')

select * from fin.dim_Account where department_code = 'sd'  'SD'  'Service'
-- 
-- update fin.dim_account
-- set department_code = 'SD',
--     department = 'Service'
-- where account in ('2457001','145504');
-- 
-- update fin.dim_account
-- set department_code = 'NC',
--     department = 'New Vehicle'
-- where account = '2457001';
-- 
-- update fin.dim_account
-- set department_code = 'FI',
--     department = 'Finance'
-- where account = '145504';

-- 5-2-17
-- changed accounts
select *
from (
  select 'xfm', account, md5(a::text) as hash
  from (
    select account, account_type_code, description, store_code, department_code, typical_balance
    from arkona.xfm_glpmast) a) c
inner join (
  select 'dim', account, md5(b::text) as hash
  from (
    select account, account_type_code, description, store_code, department_code, typical_balance
    from fin.dim_account
    where current_row = true) b) d on c.account = d.account and c.hash <> d.hash


select 'xfm' as source, account, account_type_code, description, store_code, department_code, typical_balance
from arkona.xfm_glpmast
where account in ('132306','2657001')
union
select 'dim', account, account_type_code, description, store_code, department_code, typical_balance
from fin.dim_account
where current_row = true
  and account in ('132306','2657001')
order by account, source  

-- 132306 description changes from UNUM VOLUNTARY INS PAYABLE to N/C ADVERTISING PACK
update fin.dim_account a
set description = x.description
from (
  select account, account_type_code, description, store_code, department_code, typical_balance
  from arkona.xfm_glpmast
  where account in ('132306')) x
where a.account = x.account  

update fin.dim_account
set department_code = 'SD',
    department = 'Service'
where account = '2657001'    
