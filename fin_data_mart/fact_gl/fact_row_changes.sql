﻿-- run this first
-- expose rows that have changed -----------------------------------
-- if this returns nothing, do the 10/02/20 section to expose the changed rows
drop table if exists trans_seq;
create temp table trans_seq as
-- these are the changed rows
select e.trans, e.seq
from arkona.xfm_glptrns e
inner join (
  select 'fact', trans, seq, post_status, md5(a::text) as hash
  from (
    select a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code,
      d.thedate, e.account, a.control, a.doc, a.ref, a.amount, ee.description
    from fin.fact_gl a
    inner join arkona.xfm_glptrns aa on a.trans = aa.trans
      and a.seq = aa.seq
      and a.post_status = aa.post_status
    inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
    inner join fin.dim_journal c on a.journal_key = c.journal_key
    inner join dds.day d on a.date_key = d.datekey
    inner join fin.dim_account e on a.account_key = e.account_key
    inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key) a) f
      on e.trans = f.trans
        and e.seq = f.seq
        and e.post_status = f.post_status
        and e.hash <> f.hash;
        
drop table if exists jon.transactions cascade;
create table jon.transactions as
-- select distinct doc, trans from (
select 'xfm' as source, a.trans, a.seq, a.doc_type_code, a.post_status, a.journal_code,
  a.the_date, a.account, a.control, a.doc, a.ref, a.amount, a.description   
from arkona.xfm_glptrns a                           
inner join trans_seq b on a.trans = b.trans and a.seq = b.seq
union           
select 'fact', a.trans, a.seq, doc_type_code, post_status, journal_code,
  the_date, account, control, doc, ref, amount, ee.description   
from fin.fact_gl a
inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.dim_date d on a.date_key = d.date_key
inner join fin.dim_account e on a.account_key = e.account_key
inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key
inner join trans_seq aa on a.trans = aa.trans and a.seq = aa.seq        
-- ) x            
order by trans, seq, source

----------------------------------------------------------------
--< 1/04/24 control number correction: 265478 -> 265470
----------------------------------------------------------------
select * from jon.transactions

insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(5909546,14,current_date,'265478 to 265470'),
(5912066,65,current_date,'265478 to 265470');

-- make the change
update fin.fact_gl
set control = '265470'
-- select * from fin.fact_gl
where trans = 5909546 and seq = 14;

update fin.fact_gl
set control = '265470'
-- select * from fin.fact_gl
where trans = 5912066 and seq = 65;
----------------------------------------------------------------
--/> 01/04/24 control number correction: 265478 -> 265470
----------------------------------------------------------------


----------------------------------------------------------------
--< 09/06/23 control number correction: 2A9218575 -> 23530
----------------------------------------------------------------
select * from jon.transactions

insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(5789298,1,current_date,'2A9218575 to 23530'),
(5795349,1,current_date,'2A9218575 to 23530'),
(5795349,2,current_date,'2A9218575 to 23530'),
(5813213,2,current_date,'2A9218575 to 23530');

-- make the change
update fin.fact_gl
set control = '23530'
-- select * from fin.fact_gl
where trans = 5789298 and seq = 1;

update fin.fact_gl
set control = '23530'
-- select * from fin.fact_gl
where trans = 5795349 and seq in(1, 2);

-- make the change
update fin.fact_gl
set control = '23530'
-- select * from fin.fact_gl
where trans = 5813213 and seq = 2;

----------------------------------------------------------------
--/> 09/06/23 control number correction: 2A9218575 -> 23530
----------------------------------------------------------------
----------------------------------------------------------------
--< 08/26/23 control number correction: 6          154235 -> 154235
----------------------------------------------------------------
select * from jon.transactions

insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(5801872,9,current_date,'control 6          154235 to 154235');


-- make the change
update fin.fact_gl
set control = '154235'
-- select * from fin.fact_gl
where trans = 5801872 and seq = 9;

----------------------------------------------------------------
--/> 08/26/23 control number correction: 6          154235 -> 154235
----------------------------------------------------------------

----------------------------------------------------------------
--< 07/01/23 control number correction, 120325 -> 1120325
----------------------------------------------------------------
select * from jon.transactions

insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(5743697,17,current_date,'control 120325 to 1120325');


-- make the change
update fin.fact_gl
set control = '1120325'
-- select * from fin.fact_gl
where trans = 5743697 and seq = 17;

----------------------------------------------------------------
--/> 07/01/23 control number correction, 120325 -> 1120325
----------------------------------------------------------------


----------------------------------------------------------------
--< 06/07/23 control number correction, 830268 -> 8300291
----------------------------------------------------------------
select * from jon.transactions

insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(1029546,1,current_date,'control 830268 to 8300291');
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(1029546,2,current_date,'control 830268 to 8300291');
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(1033663,4,current_date,'control 830268 to 8300291');

-- make the change
update fin.fact_gl
set control = '8300291'
-- select * from fin.fact_gl
where trans = 1029546 and seq in(1, 2);
update fin.fact_gl
set control = '8300291'
-- select * from fin.fact_gl
where trans = 1033663 and seq = 4;
----------------------------------------------------------------
--/> 06/07/23 control number correction, 830268 -> 8300291
----------------------------------------------------------------

----------------------------------------------------------------
--< 06/01/23 control number correction, 8100300 -> 8300205
----------------------------------------------------------------
select * from jon.transactions

insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(1031353,677,current_date,'control 8100300 to 8300205');

-- make the change
update fin.fact_gl
set control = '8300205'
-- select * from fin.fact_gl
where trans = 1031353 and seq = 677;
----------------------------------------------------------------
--/> 06/01/23 
----------------------------------------------------------------


----------------------------------------------------------------
--< 05/24/23 control number correction  145949 -> 146949
----------------------------------------------------------------

insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(5721049,15,current_date,'control 145949 to 146949');

-- make the change
update fin.fact_gl
set control = '146949'
-- select * from fin.fact_gl
where trans = 5721049 and seq = 15;

----------------------------------------------------------------
--/> 05/24/23 control number correction  145949 -> 146949
----------------------------------------------------------------


----------------------------------------------------------------
--< 04/12/23 control number correction  145619 -> 145919
----------------------------------------------------------------
select * from fin.fact_gl where trans = 5689659 and seq = 4

insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(5689659,4,current_date,'control 145619 to 145919');

-- make the change
update fin.fact_gl
set control = '145919'
-- select * from fin.fact_gl
where trans = 5689659 and seq = 4;

----------------------------------------------------------------
--< 04/12/23 control number corrction  145919 ->145619
----------------------------------------------------------------
-------------------------------------------------------------------
--< 3/24/23  29 rows changed !!!  wtf is going on
--  transaction 5678973,   G45807A
-------------------------------------------------------------------
select * 
-- delete
from fin.fact_gl
where trans in (5678973)
order by trans, seq
-------------------------------------------------------------------
--< 3/24/23  29 rows changed !!!  wtf is going on
--  transaction 5678973,   G45807A
-------------------------------------------------------------------


-------------------------------------------------------------------
--< 3/23/23  226 rows changed !!!  wtf is going on
--  transaction 5664904,5672627,5678081,5674756,5658115,5658113,5673776,5677011,56771645675611,5665220
--  G45894,,G46292,G46294,G46305,G46626,G46627,G46635A,G46713,G46745,G46752,H16331
--  then after those, failed on 5675611, 5677164  G46752, G46745
-------------------------------------------------------------------
select * 
-- delete
from fin.fact_gl
where trans in (5675611, 5677164)
order by trans, seq
-------------------------------------------------------------------
--< 3/23/23  226 rows changed !!!  wtf is going on
--  transaction 5664904,5672627,5678081,5674756,5658115,5658113,5673776,5677011,56771645675611,5665220
--  G45894,,G46292,G46294,G46305,G46626,G46627,G46635A,G46713,G46745,G46752,H16331
--  then after those, failed on 5675611, 5677164  G46752, G46745
-------------------------------------------------------------------

-------------------------------------------------------------------
--< 3/18/23  29 rows changed !!!  wtf is going on
--  transaction 5667218,   G39797XZ
-------------------------------------------------------------------
select * 
-- delete
from fin.fact_gl
where trans in (5667218)
order by trans, seq
-------------------------------------------------------------------
--< 3/18/23  29 rows changed !!!  wtf is going on
--  transaction 5667218,   G39797XZ
-------------------------------------------------------------------

-------------------------------------------------------------------
--< 3/17/23  29 rows changed !!!  wtf is going on
--  transaction 5672453,   H16395G
-------------------------------------------------------------------
select * 
-- delete
from fin.fact_gl
where trans in (5672453)
order by trans, seq
-------------------------------------------------------------------
--< 3/17/23  29 rows changed !!!  wtf is going on
--  transaction 5672453,   H16395G
-------------------------------------------------------------------

-------------------------------------------------------------------
--< 3/16/23  90 rows changed !!!  wtf is going on
--  transaction 5671720,5672437,5672625   G46616A,H16250,G46747
-------------------------------------------------------------------
select * 
-- delete
from fin.fact_gl
where trans in (5671720,5672437,5672625)
order by trans, seq
-------------------------------------------------------------------
--< 3/16/23  90 rows changed !!!  wtf is going on
--  transaction 5671720,5672437,5672625   G46616A,H16250,G46747
-------------------------------------------------------------------

-------------------------------------------------------------------
--< 3/15/23  20 rows changed !!!  wtf is going on
--  transaction 5658096,5663737   G46637A,G46191XA
-------------------------------------------------------------------
select * 
-- delete
from fin.fact_gl
where trans in (5658096,5663737)
order by trans, seq
-------------------------------------------------------------------
--< 3/15/23  20 rows changed !!!  wtf is going on
--  transaction 5658096,5663737   G46637A,G46191XA
-------------------------------------------------------------------

-------------------------------------------------------------------
--< 3/14/23  23 rows changed !!!  wtf is going on
--  transaction 5670366   G46640
-------------------------------------------------------------------
select * 
-- delete
from fin.fact_gl
where trans in (5670366)
order by trans, seq
-------------------------------------------------------------------
--< 3/14/23  23 rows changed !!!  wtf is going on
--  transaction 5670366   G46640
-------------------------------------------------------------------

-------------------------------------------------------------------
--< 3/10/23  19 rows changed !!!  wtf is going on
--  transaction 5667847   G45798A
-------------------------------------------------------------------

select * 
-- delete
from fin.fact_gl
where trans in (5667847)
order by trans, seq

-------------------------------------------------------------------
--< 3/10/23  19 rows changed !!!  wtf is going on
--  transaction 5667847   G45798A
-------------------------------------------------------------------

-------------------------------------------------------------------
--< 3/09/23  72 rows changed !!!  wtf is going on
--  transactions 5667234, 5661442   G46352, G46295
-------------------------------------------------------------------

select * 
-- delete
from fin.fact_gl
where trans in (5667234, 5661442)
order by trans, seq

-------------------------------------------------------------------
--< 3/09/23  72 rows changed !!!  wtf is going on
--  transactions 5667234, 5661442   G46352, G46295
-------------------------------------------------------------------


-------------------------------------------------------------------
--< 3/08/23  2 rows changed !!!  wtf is going on
-- 1 transactions 5664710   G46563
-------------------------------------------------------------------

select * 
-- delete
from fin.fact_gl
where trans in (5664710)
order by trans, seq

-------------------------------------------------------------------
--< 3/08/23  27 rows changed !!!  wtf is going on
-- 1 transactions 5664710   G46563
-------------------------------------------------------------------

-------------------------------------------------------------------
--< 3/07/23  27 rows changed !!!  wtf is going on
-- 1 transactions 5664718   G46486
-------------------------------------------------------------------

select * 
-- delete
from fin.fact_gl
where trans in (5664718)
order by trans, seq

-------------------------------------------------------------------
--< 3/07/23  27 rows changed !!!  wtf is going on
-- 1 transactions 5664718   G46486
-------------------------------------------------------------------

-------------------------------------------------------------------
--< 3/04/23  125 rows changed !!!  wtf is going on
-- 2 transactions 5663747, 5659983   G46601,G46326
-------------------------------------------------------------------

select * 
-- delete
from fin.fact_gl
where trans in (5663747, 5659983)
order by trans, seq

-------------------------------------------------------------------
--< 3/04/23  125 rows changed !!!  wtf is going on
-- 2 transactions 5663747, 5659983   G46601,G46326
-------------------------------------------------------------------

-------------------------------------------------------------------
--< 3/03/23  56 rows changed !!!  wtf is going on
-- 1 transactions 5658158  G46386A
-------------------------------------------------------------------

select * 
-- delete
from fin.fact_gl
where trans = 5658158
order by trans, seq

-------------------------------------------------------------------
--/> 3/03/23  56 rows changed !!!  wtf is going on
-- 1 transactions 5658158  G46386A
-------------------------------------------------------------------


-------------------------------------------------------------------
--< 3/02/23  194 rows changed !!!  wtf is going on
-- 4 transactions 5658116,5658751,5660006,5661432  G46307, G46652, H16251, H16352G
-------------------------------------------------------------------
select * 
-- delete
from fin.fact_gl
where trans in(5658116,5658751,5660006,5661432)
order by trans, seq

-------------------------------------------------------------------
--/> 3/02/23  194 rows changed !!!  wtf is going on
-- 4 transactions 5658116,5658751,5660006,5661432  G46307, G46652, H16251, H16352G
-------------------------------------------------------------------
-------------------------------------------------------------------
--< 3/01/23  146 rows changed !!!  wtf is going on
-- actually only 3 transactions, G45800, G45563, H16238
-------------------------------------------------------------------
create table fin.transaction_5657048_5658149_5660035 as
select * from fin.fact_gl where trans in (5657048,5658149,5660035) order by trans, seq;
comment on table fin.transaction_5657048_5658149_5660035 is 'a snapshot of table fin.fact_gl for transactions 5657048,5658149,5660035, 3/01/23, the
transaction has gone thru a massive change, i contemplate deleting the transaction from fact_gl rather than
trying to update, hoping normal processing will then simply replace it with the correct data';

-- here is a snapshot of the extent of the changes
create table fin.transaction_5657048_5658149_5660035_details as
select 'xfm' as source, a.trans, a.seq, a.doc_type_code, a.post_status, a.journal_code,
  a.the_date, a.account, a.control, a.doc, a.ref, a.amount, a.description   
from arkona.xfm_glptrns a                           
inner join trans_seq b on a.trans = b.trans and a.seq = b.seq
union           
select 'fact', a.trans, a.seq, doc_type_code, post_status, journal_code,
  the_date, account, control, doc, ref, amount, ee.description   
from fin.fact_gl a
inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.dim_date d on a.date_key = d.date_key
inner join fin.dim_account e on a.account_key = e.account_key
inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key
inner join trans_seq aa on a.trans = aa.trans and a.seq = aa.seq                    
order by trans, seq, source;

select the_date from dds.dim_date where date_key in (6994,6996,6998)

select * 
-- delete
from fin.fact_gl
where trans in(5657048,5658149,5660035)

-------------------------------------------------------------------
--/> 3/01/23  146 rows changed !!!  wtf is going on
-- actually only 3 transactions, G45800, G45563, H16238
-------------------------------------------------------------------

-------------------------------------------------------------------
--< 2/28/23  6 rows changed on T10470L, accouts, journals, doc_types, dates
-- a single transaction, 1022685, seq 2 - 7
-------------------------------------------------------------------
create table fin.transaction_1022685 as
select * from fin.fact_gl where trans = 1022685;
comment on table fin.transaction_1022685 is 'a snapshot of table fin.fact_gl for transaction 1022685, 2/28/23, the
transaction has gone thru a massive change, i contemplate deleting the transaction from fact_gl rather than
trying to update, hoping normal processing will then simply replace it with the correct data';

-- here is a snapshot of the extent of the change
create table fin.transaction_1022685_details as
select 'xfm' as source, a.trans, a.seq, a.doc_type_code, a.post_status, a.journal_code,
  a.the_date, a.account, a.control, a.doc, a.ref, a.amount, a.description   
from arkona.xfm_glptrns a                           
inner join trans_seq b on a.trans = b.trans and a.seq = b.seq
union           
select 'fact', a.trans, a.seq, doc_type_code, post_status, journal_code,
  the_date, account, control, doc, ref, amount, ee.description   
from fin.fact_gl a
inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.dim_date d on a.date_key = d.date_key
inner join fin.dim_account e on a.account_key = e.account_key
inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key
inner join trans_seq aa on a.trans = aa.trans and a.seq = aa.seq                    
order by trans, seq, source;

select * 
-- delete
from fin.fact_gl
where trans = 1022685

looks like it worked





-------------------------------------------------------------------
--/> 2/28/23  6 rows changed on T10470L, accouts, journals, doc_types, dates
-------------------------------------------------------------------
-------------------------------------------------------------------
--< 12/15/22  a simple one, control change from 0         1140870 to 1140870
-------------------------------------------------------------------
select * from fin.fact_gl where trans = 5584555 and seq = 20

insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(5584555,20,current_date,'control 0         1140870 to 1140870');

-- make the change
update fin.fact_gl
set control = '1140870'
-- select * from fin.fact_gl
where trans = 5584555 and seq = 20;


-------------------------------------------------------------------
--/> 12/15/22  a simple one, control change from 0         1140870 to 1140870
-------------------------------------------------------------------

-------------------------------------------------------------------
--< 12/02/22
-------------------------------------------------------------------
another one with a "crookston" account, 368700
-- first, from the 10/20
-- and take a look at them
select 'xfm' as source, a.trans, a.seq, a.doc_type_code, a.post_status, a.journal_code,
  a.the_date, a.account, a.control, a.doc, a.ref, a.amount, a.description   
from arkona.xfm_glptrns a                           
inner join trans_seq b on a.trans = b.trans and a.seq = b.seq
union           
select 'fact', a.trans, a.seq, doc_type_code, post_status, journal_code,
  the_date, account, control, doc, ref, amount, ee.description   
from new_rows a  --------------- the temp table
inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.dim_date d on a.date_key = d.date_key
inner join fin.dim_account e on a.account_key = e.account_key
inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key
inner join trans_seq aa on a.trans = aa.trans and a.seq = aa.seq                    
order by trans, seq, source 

select * from fin.dim_account where account = '368700'

select * from arkona.ext_glpmast where account_number in('368700')

select * from arkona.xfm_glpmast where account in( '368700')

-- first populate xfm_glpmast with these 2 accounts
truncate arkona.xfm_glpmast;
insert into arkona.xfm_glpmast
						-- this query is failing, returns 2 rows for 32301, one with store = ry3, one with xxx
            select account_number, account_type, account_desc,
              case -- this handles the lack of intercompany, eg, no account_sub_type for toyota
                when account_sub_type = 'A' then 'RY1'
                when account_sub_type = 'B' then 'RY2'
                when company_number = 'RY8' then 'RY8'
                When account_sub_type = 'C' then 'RY3'
                else 'XXX'
              end as store_code,
              department,
              case typical_balance
                when 'D' then 'Debit'
                when 'C' then 'Credit'
                else 'X'
              end
            from arkona.ext_glpmast a
            where account_number in ('368700')
              and account_type = '5' -- limit to the ry3 verson of 32301
              and not exists (
                select 1
                from fin.dim_account
                where account = a.account_number)
            group by company_number,  account_number, account_type, account_desc, account_sub_type,
              department, typical_balance 
-- then insert them into fin.dim_account
            insert into fin.dim_account (account, account_type_code, account_type, description,
              store_code, store, department_code, department, typical_balance, current_row,
              row_from_date, row_reason)
            select a.account, a.account_type_code, c.account_type,
              a.description, a.store_code,
              case a.store_code
                when 'RY1' then 'Rydell GM'
                when 'RY2' then 'Honda Nissan'
                When 'RY3' Then 'Crookston'
              end,
              a.department_code, 
              case
                when a.store_code = 'RY2' and a.department_code = 'RE' then 'Detail'
                else b.dept_description
              end,
              a.typical_balance,
              true,
              (select first_of_month from dds.dim_date where the_date = current_date - 1),
              'new account created by controller'
            from (
              select account, account_type_code, description, store_code, department_code, typical_balance
              from arkona.xfm_glpmast z
              where not exists (
                select 1
                from fin.dim_account
                where account = z.account)) a          
            left join arkona.ext_glpdept b on a.department_code = b.department_code
              and a.store_code = b.company_number
            left join dds.lkp_account_types c on a.account_type_code = c.account_type_code; 

ended up deleting the files on the server for xfmglptrns and dimgldescription and that didnt work

the problem turned out to be the row_from_date for the "new" account, 368700, it was 12/1/22, the transactions were in november
so the account for the transactions kept coming out as none
changed the row_from_date in fin.dim_account to 11/01/2022
-------------------------------------------------------------------
--/> 12/02/22
-------------------------------------------------------------------
-------------------------------------------------------------------
--< 10/28/22
-------------------------------------------------------------------
another one with a "crookston" account, 32301

select * from fin.dim_Account where account = '32301'

-- first, from the 10/20
-- and take a look at them
select 'xfm' as source, a.trans, a.seq, a.doc_type_code, a.post_status, a.journal_code,
  a.the_date, a.account, a.control, a.doc, a.ref, a.amount, a.description   
from arkona.xfm_glptrns a                           
inner join trans_seq b on a.trans = b.trans and a.seq = b.seq
union           
select 'fact', a.trans, a.seq, doc_type_code, post_status, journal_code,
  the_date, account, control, doc, ref, amount, ee.description   
from new_rows a  --------------- the temp table
inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.dim_date d on a.date_key = d.date_key
inner join fin.dim_account e on a.account_key = e.account_key
inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key
inner join trans_seq aa on a.trans = aa.trans and a.seq = aa.seq                    
order by trans, seq, source            


/*
it's the fucking crookston accounts, 2 of them with 48 transactions in June
and they don't exist in fin.dim_account
it's just my guess, but they all look like reversals of errors
again, guessing, but typo on toyota accounts that are actually crookston accounts
crookston accounts are still active

*/
-- xfm,5552941,41,J,Y,PAA,2022-10-28,32301,22B,RPA102822,RPA102822,63458.56,10.28.22 PAYROLL,4287
select 'xfm' as source, a.trans, a.seq, a.doc_type_code, a.post_status, a.journal_code,
  a.the_date, a.account, a.control, a.doc, a.ref, a.amount, a.description,
  (select account_key from fin.dim_account where account = a.account)
from arkona.xfm_glptrns a                           
inner join trans_seq b on a.trans = b.trans and a.seq = b.seq
union           
select 'fact', a.trans, a.seq, doc_type_code, post_status, journal_code,
  the_date, account, control, doc, ref, amount, ee.description, e.account_key   
from fin.fact_gl a
inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.dim_date d on a.date_key = d.date_key
inner join fin.dim_account e on a.account_key = e.account_key
inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key
inner join trans_seq aa on a.trans = aa.trans and a.seq = aa.seq                    
order by account, trans, seq, source

select * from fin.dim_account where account = '32301'

select * from arkona.ext_glpmast where account_number in('32301')

select * from arkona.xfm_glpmast where account in( '32301')

-- first populate xfm_glpmast with these 2 accounts
truncate arkona.xfm_glpmast;
insert into arkona.xfm_glpmast
						-- this query is failing, returns 2 rows for 32301, one with store = ry3, one with xxx
            select account_number, account_type, account_desc,
              case -- this handles the lack of intercompany, eg, no account_sub_type for toyota
                when account_sub_type = 'A' then 'RY1'
                when account_sub_type = 'B' then 'RY2'
                when company_number = 'RY8' then 'RY8'
                When account_sub_type = 'C' then 'RY3'
                else 'XXX'
              end as store_code,
              department,
              case typical_balance
                when 'D' then 'Debit'
                when 'C' then 'Credit'
                else 'X'
              end
            from arkona.ext_glpmast a
            where account_number in ('32301')
              and account_type = '8' -- limit to the ry3 verson of 32301
              and not exists (
                select 1
                from fin.dim_account
                where account = a.account_number)
            group by company_number,  account_number, account_type, account_desc, account_sub_type,
              department, typical_balance  
-- then insert them into fin.dim_account
            insert into fin.dim_account (account, account_type_code, account_type, description,
              store_code, store, department_code, department, typical_balance, current_row,
              row_from_date, row_reason)
            select a.account, a.account_type_code, c.account_type,
              a.description, a.store_code,
              case a.store_code
                when 'RY1' then 'Rydell GM'
                when 'RY2' then 'Honda Nissan'
                When 'RY3' Then 'Crookston'
              end,
              a.department_code, 
              case
                when a.store_code = 'RY2' and a.department_code = 'RE' then 'Detail'
                else b.dept_description
              end,
              a.typical_balance,
              true,
              (select first_of_month from dds.dim_date where the_date = current_date - 1),
              'new account created by controller'
            from (
              select account, account_type_code, description, store_code, department_code, typical_balance
              from arkona.xfm_glpmast z
              where not exists (
                select 1
                from fin.dim_account
                where account = z.account)) a          
            left join arkona.ext_glpdept b on a.department_code = b.department_code
              and a.store_code = b.company_number
            left join dds.lkp_account_types c on a.account_type_code = c.account_type_code; 

-- now try to update fin.fact_gl
update fin.fact_gl a
set account_key = b.account_key
from (
	select 'xfm' as source, a.trans, a.seq, a.doc_type_code, a.post_status, a.journal_code,
		a.the_date, a.account, a.control, a.doc, a.ref, a.amount, a.description,
		(select account_key from fin.dim_account where account = a.account)
	from arkona.xfm_glptrns a                           
	inner join trans_seq b on a.trans = b.trans and a.seq = b.seq) b
where a.trans = b.trans
  and a.seq = b.seq

and it worked, fact_gl is passing
-------------------------------------------------------------------
--/> 10/28/22
-------------------------------------------------------------------
-------------------------------------------------------------------
--< 08/01/22
-------------------------------------------------------------------
-- 2 new toyota acounts  2248, 2249
select 'xfm' as source, a.trans, a.seq, a.doc_type_code, a.post_status, a.journal_code,
  a.the_date, a.account, a.control, a.doc, a.ref, a.amount, a.description,
  (select account_key from fin.dim_account where account = a.account)
from arkona.xfm_glptrns a                           
inner join trans_seq b on a.trans = b.trans and a.seq = b.seq
union           
select 'fact', a.trans, a.seq, doc_type_code, post_status, journal_code,
  the_date, account, control, doc, ref, amount, ee.description, e.account_key   
from fin.fact_gl a
inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.dim_date d on a.date_key = d.date_key
inner join fin.dim_account e on a.account_key = e.account_key
inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key
inner join trans_seq aa on a.trans = aa.trans and a.seq = aa.seq                    
order by account, trans, seq, source
-- don't yet exist in fin.fact_gl
select * from fin.fact_gl where trans = 1001459 and seq in (4,6)

-- did this
insert into fin.fact_gl (trans,seq,doc_type_key,post_Status,journal_key,date_key,
  account_key,control,doc,ref,amount,gl_description_key)
select e.trans, e.seq, coalesce(f.doc_type_key, ff.doc_type_key),
  e.post_status, g.journal_key, h.datekey,
  coalesce(i.account_key, j.account_key) as account_key,
  e.control, e.doc, coalesce(e.ref, ''), e.amount, m.gl_description_key
from arkona.xfm_glptrns e
left join fin.fact_gl ee on e.trans = ee.trans
  and e.seq = ee.seq
left join fin.dim_doc_type f on e.doc_type_code = f.doc_type_code
  and e.the_date between f.row_from_date and f.row_thru_date
left join fin.dim_doc_type ff on 1 = 1
  and ff.doc_type_code = 'none'
left join fin.dim_journal g on e.journal_code = g.journal_code
  and e.the_date between g.row_from_date and g.row_thru_date
left join dds.day h on e.the_date = h.thedate
left join fin.dim_account i on e.account = i.account
  and e.the_date between i.row_from_date and i.row_thru_date
left join fin.dim_account j on 1 = 1
  and j.account = 'none'
left join fin.dim_gl_description m on e.description = m.description
where e.trans = 1001459 and e.seq in (4,6)

-- but on rerun failed
select * from fin.fact_gl where trans = 1001459 and seq in(4,6)
select * from fin.dim_Account where account_key = 2933
select * from fin.dim_Account where account in ('2248','2249')  -- 2248: 4232, 2249: 4231

update fin.fact_gl
set account_key = 4232
where trans = 1001459
  and seq = 4;

update fin.fact_gl
set account_key = 4231
where trans = 1001459
  and seq = 6;  
-------------------------------------------------------------------
--/> 08/01/22
-------------------------------------------------------------------
-------------------------------------------------------------------
--< 07/25/22
-------------------------------------------------------------------
-- the difference in all 48 is the account changed from none to 3xxxx
-- these are the 48 rows in xfm_glptrns

/*
it's the fucking crookston accounts, 2 of them with 48 transactions in June
and they don't exist in fin.dim_account
it's just my guess, but they all look like reversals of errors
again, guessing, but typo on toyota accounts that are actually crookston accounts
crookston accounts are still active

*/
select 'xfm' as source, a.trans, a.seq, a.doc_type_code, a.post_status, a.journal_code,
  a.the_date, a.account, a.control, a.doc, a.ref, a.amount, a.description,
  (select account_key from fin.dim_account where account = a.account)
from arkona.xfm_glptrns a                           
inner join trans_seq b on a.trans = b.trans and a.seq = b.seq
union           
select 'fact', a.trans, a.seq, doc_type_code, post_status, journal_code,
  the_date, account, control, doc, ref, amount, ee.description, e.account_key   
from fin.fact_gl a
inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.dim_date d on a.date_key = d.date_key
inner join fin.dim_account e on a.account_key = e.account_key
inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key
inner join trans_seq aa on a.trans = aa.trans and a.seq = aa.seq                    
order by account, trans, seq, source

select * from fin.dim_account where account = '32301'

select * from arkona.ext_glpmast where account_number in( '331900','332400')

select * from arkona.xfm_glpmast where account in( '331900','332400')

-- first populate xfm_glpmast with these 2 accounts
truncate arkona.xfm_glpmast;
insert into arkona.xfm_glpmast
            select account_number, account_type, account_desc,
              case -- this handles the lack of intercompany, eg, no account_sub_type for toyota
                when account_sub_type = 'A' then 'RY1'
                when account_sub_type = 'B' then 'RY2'
                when company_number = 'RY8' then 'RY8'
                When account_sub_type = 'C' then 'RY3'
                else 'XXX'
              end as store_code,
              department,
              case typical_balance
                when 'D' then 'Debit'
                when 'C' then 'Credit'
                else 'X'
              end
            from arkona.ext_glpmast a
            where account_number in ('331900','332400')
              and not exists (
                select 1
                from fin.dim_account
                where account = a.account_number)
            group by company_number,  account_number, account_type, account_desc, account_sub_type,
              department, typical_balance  
-- then insert them into fin.dim_account
            insert into fin.dim_account (account, account_type_code, account_type, description,
              store_code, store, department_code, department, typical_balance, current_row,
              row_from_date, row_reason)
            select a.account, a.account_type_code, c.account_type,
              a.description, a.store_code,
              case a.store_code
                when 'RY1' then 'Rydell GM'
                when 'RY2' then 'Honda Nissan'
                When 'RY3' Then 'Crookston'
              end,
              a.department_code, 
              case
                when a.store_code = 'RY2' and a.department_code = 'RE' then 'Detail'
                else b.dept_description
              end,
              a.typical_balance,
              true,
              (select first_of_month from dds.dim_date where the_date = current_date - 1),
              'new account created by controller'
            from (
              select account, account_type_code, description, store_code, department_code, typical_balance
              from arkona.xfm_glpmast z
              where not exists (
                select 1
                from fin.dim_account
                where account = z.account)) a          
            left join arkona.ext_glpdept b on a.department_code = b.department_code
              and a.store_code = b.company_number
            left join dds.lkp_account_types c on a.account_type_code = c.account_type_code; 

-- now try to update fin.fact_gl
update fin.fact_gl a
set account_key = b.account_key
from (
	select 'xfm' as source, a.trans, a.seq, a.doc_type_code, a.post_status, a.journal_code,
		a.the_date, a.account, a.control, a.doc, a.ref, a.amount, a.description,
		(select account_key from fin.dim_account where account = a.account)
	from arkona.xfm_glptrns a                           
	inner join trans_seq b on a.trans = b.trans and a.seq = b.seq) b
where a.trans = b.trans
  and a.seq = b.seq

-- looks like it worked
select * from fin.fact_gl where trans = 5440066
-------------------------------------------------------------------
--/> 07/25/22
-------------------------------------------------------------------

-------------------------------------------------------------------
--< 02/17/22
-------------------------------------------------------------------

select 'xfm' as source, a.trans, a.seq, a.doc_type_code, a.post_status, a.journal_code,
  a.the_date, a.account, a.control, a.doc, a.ref, a.amount, a.description   
from arkona.xfm_glptrns a                           
inner join trans_seq b on a.trans = b.trans and a.seq = b.seq
union           
select 'fact', a.trans, a.seq, doc_type_code, post_status, journal_code,
  the_date, account, control, doc, ref, amount, ee.description   
from fin.fact_gl a
inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.dim_date d on a.date_key = d.date_key
inner join fin.dim_account e on a.account_key = e.account_key
inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key
inner join trans_seq aa on a.trans = aa.trans and a.seq = aa.seq                    
order by trans, seq, source

select * from fin.fact_gl where trans = 5322288 and seq = 2

insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(5322288,4,current_date,'control 124109 to 158033');

-- make the change
update fin.fact_gl
set control = '158033'
-- select * from fin.fact_gl
where trans = 5322288 and seq = 4;

-------------------------------------------------------------------
--/> 02/17/22
-------------------------------------------------------------------

  
-------------------------------------------------------------------
--< 02/04/22 new account 230224, transaction does not exist in fact_gl
-------------------------------------------------------------------
select * from fin.dim_account where account = '230224'
select * from fin.fact_gl where trans = 5333700
after inserting the row in the 10/2 still failed

delete from fin.fact_gl where trans = 5333700 and seq = 1;

select * from fin.dim_account where account_key in (1572,3237,2933)
update fin.dim_account
set row_from_date = '01/01/2022'
where account = '230224';

insert into fin.fact_gl (trans,seq,doc_type_key,post_Status,journal_key,date_key,
  account_key,control,doc,ref,amount,gl_description_key)
select e.trans, e.seq, coalesce(f.doc_type_key, ff.doc_type_key),
  e.post_status, g.journal_key, h.datekey,
  coalesce(i.account_key, j.account_key) as account_key,
  e.control, e.doc, coalesce(e.ref, ''), e.amount, m.gl_description_key
from arkona.xfm_glptrns e
left join fin.fact_gl ee on e.trans = ee.trans
  and e.seq = ee.seq
left join fin.dim_doc_type f on e.doc_type_code = f.doc_type_code
  and e.the_date between f.row_from_date and f.row_thru_date
left join fin.dim_doc_type ff on 1 = 1
  and ff.doc_type_code = 'none'
left join fin.dim_journal g on e.journal_code = g.journal_code
  and e.the_date between g.row_from_date and g.row_thru_date
left join dds.day h on e.the_date = h.thedate
left join fin.dim_account i on e.account = i.account
  and e.the_date between i.row_from_date and i.row_thru_date
left join fin.dim_account j on 1 = 1
  and j.account = 'none'
left join fin.dim_gl_description m on e.description = m.description
where e.trans = 5333700 and e.seq = 1
-------------------------------------------------------------------
--/> 02/04/22 new account 230224, transaction does not exist in fact_gl
-------------------------------------------------------------------  

-------------------------------------------------------------------
--< 01/05/2021  another case of new accounts added
-------------------------------------------------------------------
-- after generating the temp tables new_lines and trans_seq (10/02/20), here are the diffs
select 'xfm' as source, a.trans, a.seq, a.doc_type_code, a.post_status, a.journal_code,
  a.the_date, a.account, a.control, a.doc, a.ref, a.amount, a.description   
from arkona.xfm_glptrns a                           
inner join trans_seq b on a.trans = b.trans and a.seq = b.seq
union           
select 'fact', a.trans, a.seq, doc_type_code, post_status, journal_code,
  the_date, account, control, doc, ref, amount, ee.description   
from new_rows a  --------------- the temp table
inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.dim_date d on a.date_key = d.date_key
inner join fin.dim_account e on a.account_key = e.account_key
inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key
inner join trans_seq aa on a.trans = aa.trans and a.seq = aa.seq                    
order by trans, seq, source   

-- 3 new accounts: 15127, 15128, 18428
all 3 added in january, but affect the december statement, so, update the row_from_date in dim account

update fin.dim_account
set row_from_date = '12/01/2020'
-- select * from fin.dim_account
where account in ('15127','15128','18428');

-- after updating dim_acccount, this query generates the correct data (including account keys)
-- to insert into fact_gl

insert into fin.fact_gl (trans,seq,doc_type_key,post_Status,journal_key,date_key,
  account_key,control,doc,ref,amount,gl_description_key)
select e.trans, e.seq, coalesce(f.doc_type_key, ff.doc_type_key),
  e.post_status, g.journal_key, h.datekey,
  coalesce(i.account_key, j.account_key) as account_key,
  e.control, e.doc, coalesce(e.ref, ''), e.amount, m.gl_description_key
from arkona.xfm_glptrns e
left join fin.fact_gl ee on e.trans = ee.trans
  and e.seq = ee.seq
left join fin.dim_doc_type f on e.doc_type_code = f.doc_type_code
  and e.the_date between f.row_from_date and f.row_thru_date
left join fin.dim_doc_type ff on 1 = 1
  and ff.doc_type_code = 'none'
left join fin.dim_journal g on e.journal_code = g.journal_code
  and e.the_date between g.row_from_date and g.row_thru_date
left join dds.day h on e.the_date = h.thedate
left join fin.dim_account i on e.account = i.account
  and e.the_date between i.row_from_date and i.row_thru_date
left join fin.dim_account j on 1 = 1
  and j.account = 'none'
left join fin.dim_gl_description m on e.description = m.description
where (
  (e.trans = 4985903 and e.seq in (2,3))
  or
  (e.trans = 4986392 and e.seq = 10));
  
-- log the changes
insert into fin.fact_gl_change_log(trans,seq,the_date,change)
values(4985903, 2, current_date, 'new account');
insert into fin.fact_gl_change_log(trans,seq,the_date,change)
values(4985903, 3, current_date, 'new account');
insert into fin.fact_gl_change_log(trans,seq,the_date,change)
values(4986392, 10, current_date, 'new account');
-------------------------------------------------------------------
--/> 01/05/2021
-------------------------------------------------------------------
-------------------------------------------------------------------
--< 10/02/20
-------------------------------------------------------------------
suprised this has not come up before
the changed row doesnt make it into fin.fact_gl, i believe because the exception is raised and the
transaction does not get posted
so the task fails, but after the fact, no changed rows are detected
so to find the changed row

-- put the new rows into a temp table
drop table if exists new_rows;
create temp table new_rows as
select e.trans, e.seq, coalesce(f.doc_type_key, ff.doc_type_key) as doc_type_key,
  e.post_status, g.journal_key, h.datekey as date_key,
  coalesce(i.account_key, j.account_key) as account_key,
  e.control, e.doc, coalesce(e.ref, '') as ref, e.amount, m.gl_description_key
from arkona.xfm_glptrns e
left join fin.fact_gl ee on e.trans = ee.trans
  and e.seq = ee.seq
left join fin.dim_doc_type f on e.doc_type_code = f.doc_type_code
  and e.the_date between f.row_from_date and f.row_thru_date
left join fin.dim_doc_type ff on 1 = 1
  and ff.doc_type_code = 'none'
left join fin.dim_journal g on e.journal_code = g.journal_code
  and e.the_date between g.row_from_date and g.row_thru_date
left join dds.day h on e.the_date = h.thedate
left join fin.dim_account i on e.account = i.account
  and e.the_date between i.row_from_date and i.row_thru_date
left join fin.dim_account j on 1 = 1
  and j.account = 'none'
left join fin.dim_gl_description m on e.description = m.description
where ee.trans is null;

-- now, using the temp table new_rows i can generate the diffs
drop table if exists trans_seq;
create temp table trans_seq as
select e.trans, e.seq
from arkona.xfm_glptrns e
inner join (
  select 'fact', trans, seq, post_status, md5(a::text) as hash
  from (
    select a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code,
      d.thedate, e.account, a.control, a.doc, a.ref, a.amount, ee.description
    from new_rows a  -------------------- the temp table
    inner join arkona.xfm_glptrns aa on a.trans = aa.trans
      and a.seq = aa.seq
      and a.post_status = aa.post_status
    inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
    inner join fin.dim_journal c on a.journal_key = c.journal_key
    inner join dds.day d on a.date_key = d.datekey
    inner join fin.dim_account e on a.account_key = e.account_key
    inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key) a) f
      on e.trans = f.trans
        and e.seq = f.seq
        and e.post_status = f.post_status
        and e.hash <> f.hash;
        
-- and take a look at them
select 'xfm' as source, a.trans, a.seq, a.doc_type_code, a.post_status, a.journal_code,
  a.the_date, a.account, a.control, a.doc, a.ref, a.amount, a.description   
from arkona.xfm_glptrns a                           
inner join trans_seq b on a.trans = b.trans and a.seq = b.seq
union           
select 'fact', a.trans, a.seq, doc_type_code, post_status, journal_code,
  the_date, account, control, doc, ref, amount, ee.description   
from new_rows a  --------------- the temp table
inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.dim_date d on a.date_key = d.date_key
inner join fin.dim_account e on a.account_key = e.account_key
inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key
inner join trans_seq aa on a.trans = aa.trans and a.seq = aa.seq                    
order by trans, seq, source            




--< 02/04/22 new account 230224, transaction does not exist in fact_gl
select * from fin.dim_account where account = '230224'
select * from fin.fact_gl where trans = 5333700

insert into fin.fact_gl (trans,seq,doc_type_key,post_Status,journal_key,date_key,
  account_key,control,doc,ref,amount,gl_description_key)
select e.trans, e.seq, coalesce(f.doc_type_key, ff.doc_type_key),
  e.post_status, g.journal_key, h.datekey,
  coalesce(i.account_key, j.account_key) as account_key,
  e.control, e.doc, coalesce(e.ref, ''), e.amount, m.gl_description_key
from arkona.xfm_glptrns e
left join fin.fact_gl ee on e.trans = ee.trans
  and e.seq = ee.seq
left join fin.dim_doc_type f on e.doc_type_code = f.doc_type_code
  and e.the_date between f.row_from_date and f.row_thru_date
left join fin.dim_doc_type ff on 1 = 1
  and ff.doc_type_code = 'none'
left join fin.dim_journal g on e.journal_code = g.journal_code
  and e.the_date between g.row_from_date and g.row_thru_date
left join dds.day h on e.the_date = h.thedate
left join fin.dim_account i on e.account = i.account
  and e.the_date between i.row_from_date and i.row_thru_date
left join fin.dim_account j on 1 = 1
  and j.account = 'none'
left join fin.dim_gl_description m on e.description = m.description
where e.trans = 5333700;
--/> 02/04/22 ---------------------------------------------


-- makes sense, there is a new account this morning 18228
select * from fin.dim_account where account = '19228'

select * from fin.dim_account where account_key in (2933,2873)

select * from new_rows where trans = 4906094

select * from fin.fact_gl where trans = 4906094

select * from arkona.xfm_glptrns where trans = 4906094
-- could not get it to run successfully even after deleting the output files
-- for ExtGlpjrnd,DimAccount,ExtGlpdept,DimGlDescription & XfmGlptrns
-- so i manually inserted the rows with the bad account key
insert into fin.fact_gl (trans,seq,doc_type_key,post_Status,journal_key,date_key,
  account_key,control,doc,ref,amount,gl_description_key)
select e.trans, e.seq, coalesce(f.doc_type_key, ff.doc_type_key),
  e.post_status, g.journal_key, h.datekey,
  coalesce(i.account_key, j.account_key) as account_key,
  e.control, e.doc, coalesce(e.ref, ''), e.amount, m.gl_description_key
from arkona.xfm_glptrns e
left join fin.fact_gl ee on e.trans = ee.trans
  and e.seq = ee.seq
left join fin.dim_doc_type f on e.doc_type_code = f.doc_type_code
  and e.the_date between f.row_from_date and f.row_thru_date
left join fin.dim_doc_type ff on 1 = 1
  and ff.doc_type_code = 'none'
left join fin.dim_journal g on e.journal_code = g.journal_code
  and e.the_date between g.row_from_date and g.row_thru_date
left join dds.day h on e.the_date = h.thedate
left join fin.dim_account i on e.account = i.account
  and e.the_date between i.row_from_date and i.row_thru_date
left join fin.dim_account j on 1 = 1
  and j.account = 'none'
left join fin.dim_gl_description m on e.description = m.description
where e.trans = 5307689;
-- and then fixed it                    
update fin.fact_gl
set account_key = 3235
-- select * from fin.fact_gl
where trans = 5307689
  and seq = 2;

insert into fin.fact_gl_change_log(trans,seq,the_date,change)
values(5307689, 2, current_date, 'new account');

select * from fin.dim_account where account_key = 3235
-------------------------------------------------------------------
--< 10/02/20
-------------------------------------------------------------------



--07/17/20
-- again, 3 lines with journal = none
-- set journal to GJE on all those sfe entries that showed no journal
insert into fin.fact_gl_change_log(trans,seq,the_date,change)
select trans, seq, current_date, 'no journal to GJE' from trans_seq;

select * -- 20, none = 34
from fin.dim_journal
where journal_code = 'GJE'


update fin.fact_gl
set journal_key = 20
-- select * from fin.fact_gl
where journal_key = 34
  and trans in (select trans from trans_seq);



--06/03/20
-- set journal to GJE on all those sfe entries that showed no journal
insert into fin.fact_gl_change_log(trans,seq,the_date,change)
select trans, seq, current_date, 'no journal to GJE' from trans_seq;

select * -- 20, none = 34
from fin.dim_journal
where journal_code = 'GJE'


update fin.fact_gl
set journal_key = 20
-- select * from fin.fact_gl
where journal_key = 34
  and trans in (select trans from trans_seq);


-- 02/07/20
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4697944,14,current_date,'9             ANN to ANN');
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4697944,15,current_date,'9             1F40794 to 1F40794');

-- make the changes
update fin.fact_gl
set control = 'ANN'
-- select * from fin.fact_gl
where trans = 4697944 and seq = 14;

update fin.fact_gl
set control = '1F40794'
-- select * from fin.fact_gl
where trans = 4697944 and seq = 15;


  

-- 11/08/19
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4560083,1,current_date,'1ALCENTER to 154500');
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4560084,1,current_date,'1ALCENTER to 154500');
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4560084,2,current_date,'1ALCENTER to 154500');
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4567149,3,current_date,'13670 to 154500');
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4591174,1,current_date,'13670 to 154500');
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4591174,3,current_date,'13670 to 154500');
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4596124,4,current_date,'13670 to 154500');

-- make the changes
update fin.fact_gl
set control = '154500'
-- select * from fin.fact_gl
where (
  trans = 4560083 and seq = 1
  or trans = 4560084 and seq = 1
  or trans = 4560084 and seq = 2
  or trans = 4567149 and seq = 3
  or trans = 4591174 and seq = 1
  or trans = 4591174 and seq = 3
  or trans = 4596124 and seq = 4);




-- 11/07/19
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4568607,4,current_date,'298337 to 298370');
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4569852,4,current_date,'298337 to 298370');

-- make the change
update fin.fact_gl
set control = '298370'
where (
  trans = 4568607 and seq = 4
  or trans = 4569852 and seq = 4);

  
-- 9/27/19
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4560524,4,current_date,'1L30701 to 130701');

-- make the change
update fin.fact_gl
set control = '130701'
where trans = 4560524 and seq = 4;


-- 8/9/19
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4500602,4,current_date,'118300 to 119200');
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4508464,1,current_date,'118300 to 119200');
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4509036,4,current_date,'118300 to 119200');

-- make the changes
update fin.fact_gl
set control = '119200'
where (
  trans = 4500602 and seq = 4
  or trans = 4508464 and seq = 1
  or trans = 4509036 and seq = 4);


  
-- 8/6/19
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4442433,9,current_date,'1103344 to 1103410');

-- make the change
update fin.fact_gl
set control = '1103410'
where trans = 4476600 and seq = 6;



-- 7/11/19
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4476600,6,current_date,'16502134 to 1139150');
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4449584,3,current_date,'16502134 to 1139150');
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4449584,2,current_date,'16502134 to 1139150');
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4449584,1,current_date,'16502134 to 1139150');
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4454302,9,current_date,'16502134 to 1139150');
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4480636,9,current_date,'16502134 to 1139150');


-- make the change
update fin.fact_gl
set control = '1139150'
where (
  trans = 4442433 and seq = 9
  or trans = 4449584 and seq in( 1, 2, 3)
  or trans = 4454302 and seq = 9
  or trans = 4480636 and seq = 9);


  
7/2/19
shit, fails with Exception: fact_gl has some changed rows 1
but the above query returns nothing
if i run the count(*) query, it returns 0
so
uncomment the ts section lines 483 - 508, on the server, (writes the output to a file) and lets see what we get
fuck me, it generates an empty file
deleted all fact_gl dependency output files, tried it again
aha got it
the transaction in the file is: trans = 4475508 and seq = 2
ah, it fails goofy like this because
  the count query is in the same class (FactGl)
  the class fails, so the data with the discrepancy does not get inserted into fact_gl
  the count fails, everything gets backed out and the diff is no longer visible
so
inserted the data for that transaction into fact_gl
               insert into fin.fact_gl (trans,seq,doc_type_key,post_Status,journal_key,date_key,
                      account_key,control,doc,ref,amount,gl_description_key)
                    select e.trans, e.seq, coalesce(f.doc_type_key, ff.doc_type_key),
                      e.post_status, g.journal_key, h.datekey,
                      coalesce(i.account_key, j.account_key) as account_key,
                      e.control, e.doc, coalesce(e.ref, ''), e.amount, m.gl_description_key
                    from arkona.xfm_glptrns e
                    left join fin.fact_gl ee on e.trans = ee.trans
                      and e.seq = ee.seq
                    left join fin.dim_doc_type f on e.doc_type_code = f.doc_type_code
                      and e.the_date between f.row_from_date and f.row_thru_date
                    left join fin.dim_doc_type ff on 1 = 1
                      and ff.doc_type_code = 'none'
                    left join fin.dim_journal g on e.journal_code = g.journal_code
                      and e.the_date between g.row_from_date and g.row_thru_date
                    left join dds.day h on e.the_date = h.thedate
                    left join fin.dim_account i on e.account = i.account
                      and e.the_date between i.row_from_date and i.row_thru_date
                    left join fin.dim_account j on 1 = 1
                      and j.account = 'none'
                    left join fin.dim_gl_description m on e.description = m.description
                    where ee.trans is null
                      and e.trans = 4475508  
reran luigi, and
select * from trans_seq, now tells me the problem is trans 4475508 seq 2
and the problem is the account in fact_gl is none and should be 231404
so, log it:
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4475508,2,current_date,'account changed from none to 231404');     
and fix it
select * from fin.dim_account where account = '231404'  
account_key = 3130
update fin.fact_gl
set account_key = 3130
where trans = 4475508
  and seq = 2;            



3/13/19
-- this was weird, failed on luigi, would not fail from the query,
-- had to do the insert into fact_gl from xfm_glptrns manually then rerun luigi,
-- now the failure shows up
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4360366,1,current_date,'account changed from none to 246024');
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4360368,1,current_date,'account changed from none to 246024');

select * from fin.dim_account where account_key = 2933
select * from fin.dim_account where account = '246024'
-- uhoh, no account 246024 in dim_account
-- oh shit, i bet this is the ry2 detail, yep
-- dim_account did not fail because i hard coded around it
-- ok, if have fixed dim_Account to handle RY2 Detail
update fin.fact_gl
set account_key = 3117
where trans in (4360366,4360368)
  and seq = 1; 

3/1/19
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4338384,9,current_date,'27905 to 27850');
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4345535,3,current_date,'27905 to 27850');

-- make the change
update fin.fact_gl
set control = '27850'
where trans = 4338384
  and seq = 9; 
update fin.fact_gl
set control = '27850'
where trans = 4345535
  and seq = 3; 


  
2/6/19
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4293404,1,current_date,'184540 to 184505');
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4317353,11,current_date,'184540 to 184505');
-- make the change
update fin.fact_gl
set control = '184505'
where trans = 4293404
  and seq = 1; 
update fin.fact_gl
set control = '184505'
where trans = 4317353
  and seq = 11; 
  
2/1/19
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4296563,7,current_date,'control changed from "3         1130720" to 1130720');
-- make the change
update fin.fact_gl
set control = '1130720'
where trans in (4296563)
  and seq = 7; 


-- 01/3/19
-- 10 transactions where control changed from 11799 to 113363
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4256686,3,current_date,'control changed from 11799 to 113363'),
(4265281,10,current_date,'control changed from 11799 to 113363'),
(4268840,9,current_date,'control changed from 11799 to 113363'),
(4269689,9,current_date,'control changed from 11799 to 113363'),
(4270433,9,current_date,'control changed from 11799 to 113363'),
(4277339,9,current_date,'control changed from 11799 to 113363'),
(4280805,1,current_date,'control changed from 11799 to 113363'),
(4280809,1,current_date,'control changed from 11799 to 113363'),
(4280810,2,current_date,'control changed from 11799 to 113363'),
(4280810,3,current_date,'control changed from 11799 to 113363');

-- make the change
update fin.fact_gl
set control = '113363'
where trans in (4256686)
  and seq = 3; 

update fin.fact_gl
set control = '113363'
where trans = 4265281
  and seq in (10); 

update fin.fact_gl
set control = '113363'
where trans in ( 4268840,4269689,4270433,4277339)
  and seq in (9);  

update fin.fact_gl
set control = '113363'
where trans in (4280805,4280809)
  and seq in (1);   

update fin.fact_gl
set control = '113363'
where trans in (4280810)
  and seq in (2,3);   
---------------------------------------------------------------------------------------
--< 10/03 ruh roh
---------------------------------------------------------------------------------------
fails with changed rows, but when i run the queries i see no changed rows
cant figure out how to trouble shoot it
ran luigi fact_gl locally and same thing
finally
substitued into the query that generates a count of mismatched rows
replaced : from fin.fact_gl a
with the insert new rows query, so ended up with:

                    select e.*, f.*
                    from arkona.xfm_glptrns e
                    inner join (
                      select 'fact', trans, seq, post_status, md5(aa::text) as hash
                      from (
                        select a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code,
                          d.thedate, e.account, a.control, a.doc, a.ref, a.amount, ee.description
--                         from fin.fact_gl a
                        from (
          select e.trans, e.seq, coalesce(f.doc_type_key, ff.doc_type_key) as doc_type_key,
            e.post_status, g.journal_key, h.datekey as date_key,
            coalesce(i.account_key, j.account_key) as account_key,
            e.control, e.doc, coalesce(e.ref, '') as ref, e.amount, m.gl_description_key
          from arkona.xfm_glptrns e
          left join fin.fact_gl ee on e.trans = ee.trans
            and e.seq = ee.seq
          left join fin.dim_doc_type f on e.doc_type_code = f.doc_type_code
            and e.the_date between f.row_from_date and f.row_thru_date
          left join fin.dim_doc_type ff on 1 = 1
            and ff.doc_type_code = 'none'
          left join fin.dim_journal g on e.journal_code = g.journal_code
            and e.the_date between g.row_from_date and g.row_thru_date
          left join dds.day h on e.the_date = h.thedate
          left join fin.dim_account i on e.account = i.account
            and e.the_date between i.row_from_date and i.row_thru_date
          left join fin.dim_account j on 1 = 1
            and j.account = 'none'
          left join fin.dim_gl_description m on e.description = m.description
          where ee.trans is null) a                        
                        inner join arkona.xfm_glptrns aa on a.trans = aa.trans
                          and a.seq = aa.seq
                          and a.post_status = aa.post_status
                        inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
                        inner join fin.dim_journal c on a.journal_key = c.journal_key
                        inner join dds.day d on a.date_key = d.datekey
                        inner join fin.dim_account e on a.account_key = e.account_key
                        inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key) aa) f
                          on e.trans = f.trans
                            and e.seq = f.seq
                            and e.post_status = f.post_status
                            and e.hash <> f.hash

which finally, gives me the changed rows (i think)    
trans/seq:
4198233;2
4198232;2
4198228;2
4198225;2

ok my head hurts, these dont exists in fin.fact_gl yet, so how can i have changes? change from what
its like its saying i dont know what

select *
from arkona.xfm_glptrns
where trans in ( 4198233,4198232,4198228,4198225)
  and seq = 2

select *
from (
          select e.trans, e.seq, coalesce(f.doc_type_key, ff.doc_type_key) as doc_type_key,
            e.post_status, g.journal_key, h.datekey as date_key,
            coalesce(i.account_key, j.account_key) as account_key,
            e.control, e.doc, coalesce(e.ref, '') as ref, e.amount, m.gl_description_key
          from arkona.xfm_glptrns e
          left join fin.fact_gl ee on e.trans = ee.trans
            and e.seq = ee.seq
          left join fin.dim_doc_type f on e.doc_type_code = f.doc_type_code
            and e.the_date between f.row_from_date and f.row_thru_date
          left join fin.dim_doc_type ff on 1 = 1
            and ff.doc_type_code = 'none'
          left join fin.dim_journal g on e.journal_code = g.journal_code
            and e.the_date between g.row_from_date and g.row_thru_date
          left join dds.day h on e.the_date = h.thedate
          left join fin.dim_account i on e.account = i.account
            and e.the_date between i.row_from_date and i.row_thru_date
          left join fin.dim_account j on 1 = 1
            and j.account = 'none'
          left join fin.dim_gl_description m on e.description = m.description
          where ee.trans is null
) x
where trans in ( 4198233,4198232,4198228,4198225)
  and seq = 2



select * from fin.dim_Account where account_key = 2933

-- this query returns none as the account for all 4 rows
select *
from (
                        select a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code,
                          d.thedate, e.account, a.control, a.doc, a.ref, a.amount, ee.description
--                         from fin.fact_gl a
                        from (
          select e.trans, e.seq, coalesce(f.doc_type_key, ff.doc_type_key) as doc_type_key,
            e.post_status, g.journal_key, h.datekey as date_key,
            coalesce(i.account_key, j.account_key) as account_key,
            e.control, e.doc, coalesce(e.ref, '') as ref, e.amount, m.gl_description_key
          from arkona.xfm_glptrns e
          left join fin.fact_gl ee on e.trans = ee.trans
            and e.seq = ee.seq
          left join fin.dim_doc_type f on e.doc_type_code = f.doc_type_code
            and e.the_date between f.row_from_date and f.row_thru_date
          left join fin.dim_doc_type ff on 1 = 1
            and ff.doc_type_code = 'none'
          left join fin.dim_journal g on e.journal_code = g.journal_code
            and e.the_date between g.row_from_date and g.row_thru_date
          left join dds.day h on e.the_date = h.thedate
          left join fin.dim_account i on e.account = i.account
            and e.the_date between i.row_from_date and i.row_thru_date
          left join fin.dim_account j on 1 = 1
            and j.account = 'none'
          left join fin.dim_gl_description m on e.description = m.description
          where ee.trans is null) a                        
                        inner join arkona.xfm_glptrns aa on a.trans = aa.trans
                          and a.seq = aa.seq
                          and a.post_status = aa.post_status
                        inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
                        inner join fin.dim_journal c on a.journal_key = c.journal_key
                        inner join dds.day d on a.date_key = d.datekey
                        inner join fin.dim_account e on a.account_key = e.account_key
                        inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key
) x
where trans in ( 4198233,4198232,4198228,4198225)
  and seq = 2


looks like we are missing accounts 1658001, 2658001

select * from fin.dim_Account where account in ('1658001','2658001')

finally, here is the issue
these transactions are dated 9/30/18
the accounts are new and have a from_Date of 10/01
therefor the accounts are not being picked up

update fin.dim_account
set row_from_date = '09/30/2018'
where account in ('1658001','2658001');

yep, that fixed fact_gl

whew

---------------------------------------------------------------------------------------
--/> 10/03 ruh roh
---------------------------------------------------------------------------------------



                   
-- 09/20/18
-- dyslectic entries control 267431 / 276431
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4148954,7,current_date,'control changed from 267431 to 276431'),
(4148954,8,current_date,'control changed from 267431 to 276431'),
(4171514,11,current_date,'control changed from 267431 to 276431'),
(4180808,11,current_date,'control changed from 267431 to 276431');

-- make the change
update fin.fact_gl
set control = '276431'
where trans in (4171514,4180808)
  and seq = 11; 

update fin.fact_gl
set control = '276431'
where trans = 4148954
  and seq in (7,8); 
  
-- 8/22/18
-- dyslectic entries control 159683 / 159863
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4116572,9,current_date,'control changed from 159683 to 159863'),
(4130274,9,current_date,'control changed from 159683 to 159863'),
(4151800,9,current_date,'control changed from 159683 to 159863');

-- make the change
update fin.fact_gl
set control = '159863'
where trans in (4116572,4130274,4151800)
  and seq = 9; 

  

-- 8/14/18
-- control 118510 changed to 1134841 on several transactions
insert into fin.fact_gl_change_log(trans,seq,the_date, change) values
(4105393,4,current_date,'control changed from 118510 to 1134841'),
(4113244,1,current_date,'control changed from 118510 to 1134841'),
(4113244,2,current_date,'control changed from 118510 to 1134841'),
(4113244,3,current_date,'control changed from 118510 to 1134841'),
(4113244,4,current_date,'control changed from 118510 to 1134841'),
(4113244,5,current_date,'control changed from 118510 to 1134841'),
(4124437,4,current_date,'control changed from 118510 to 1134841');

-- make the change
update fin.fact_gl
set control = '1134841'
where trans in (4105393,4113244,4124437)
  and seq in(4); 
update fin.fact_gl
set control = '1134841'
where trans = 4113244
  and seq in(1,2,3,5); 

  
-- 6/15/18
-- control 244658 changed to 21650 on doc 2538124
-- log the fix
insert into fin.fact_gl_change_log(trans,seq,the_date,change)
select a.trans, a.seq, current_date, 'control changed from 244658 to 21650'
from fin.fact_gl a
where trans = 4069225 
  and seq = 1; 
-- make the change
update fin.fact_gl
set control = '21650'
where trans = 4069225 
  and seq = 1; 
  
-- 06/07/2018
-- control 1C40949 changed to 140949 on ro 16311204
select * from fin.fact_gl_change_log
-- first update the fact_gl_log
insert into fin.fact_gl_change_log(trans,seq,the_date,change)
select a.trans, a.seq, current_date, 'control changed from 1C40949 to 140949'
from fin.fact_gl a
where trans = 4049497 
  and seq = 4; 
insert into fin.fact_gl_change_log(trans,seq,the_date,change)
select a.trans, a.seq, current_date, 'control changed from 1C40949 to 140949'
from fin.fact_gl a
where trans = 4049497 
  and seq = 7;   
insert into fin.fact_gl_change_log(trans,seq,the_date,change)
select a.trans, a.seq, current_date, 'control changed from 1C40949 to 140949'
from fin.fact_gl a
where trans = 4049497 
  and seq = 15;  
  
insert into fin.fact_gl_change_log(trans,seq,the_date,change)
select a.trans, a.seq, current_date, 'control changed from 1C40949 to 140949'
from fin.fact_gl a
where trans = 4076138 
  and seq = 4;  

insert into fin.fact_gl_change_log(trans,seq,the_date,change)
select a.trans, a.seq, current_date, 'control changed from 1C40949 to 140949'
from fin.fact_gl a
where trans = 4077671 
  and seq = 7;  
  
insert into fin.fact_gl_change_log(trans,seq,the_date,change)
select a.trans, a.seq, current_date, 'control changed from 140949 to 16311204'
from fin.fact_gl a
where trans = 4049497 
  and seq = 4;    

insert into fin.fact_gl_change_log(trans,seq,the_date,change)
select a.trans, a.seq, current_date, 'control changed from 140949 to 16311204'
from fin.fact_gl a
where trans = 4049497 
  and seq = 7;    
    
-- make the change
update fin.fact_gl
set control = '16311204'
where trans = 4049497 
  and seq in(4,7); 
update fin.fact_gl
set control = '140949'
where trans = 4076138 
  and seq in(4); 
update fin.fact_gl
set control = '140949'
where trans = 4077671 
  and seq in(7); 
  


    
-- 01/09/18 changed fact_gl rows -----------------------------------
-- first update the fact_gl_log
insert into fin.fact_gl_change_log(trans,seq,the_date,change)
select a.trans, a.seq, current_date, 'control changed from 126590 to 126575'
from fin.fact_gl a
where trans = 3895219 
  and seq = 9; 

-- make the change
update fin.fact_gl
set control = '126575'
where trans = 3895219 
  and seq = 9; 


-- 12/19/17 changed fact_gl rows -----------------------------------
-- first update the fact_gl_log
insert into fin.fact_gl_change_log(trans,seq,the_date,change)
select a.trans, a.seq, current_date, 'control changed from 1G4 to 1G40843'
from fin.fact_gl a
where (
  (trans = 3896866 and seq = 24) or   
  (trans = 3904949 and seq = 5)); 

-- make the change
update fin.fact_gl
set control = '1G40843'
where (
  (trans = 3896866 and seq = 24) or   
  (trans = 3904949 and seq = 5)); 

  
-- 12/8/17 changed fact_gl rows -----------------------------------
-- first update the fact_gl_log
insert into fin.fact_gl_change_log(trans,seq,the_date,change)
select a.trans, a.seq, current_date, 'control changed from 138410 to 138420'
from fin.fact_gl a
where (
  (trans = 3852957 and seq = 9) or   
  (trans = 3853210 and seq = 9) or
  (trans = 3863124 and seq between 1 and 6) or
  (trans = 3887816 and seq = 9)); 

-- make the change
update fin.fact_gl
set control = '138420'
where (
  (trans = 3852957 and seq = 9) or   
  (trans = 3853210 and seq = 9) or
  (trans = 3863124 and seq between 1 and 6) or
  (trans = 3887816 and seq = 9)); 

  
-- 12/6/17 changed fact_gl rows -----------------------------------
-- first update the fact_gl_log
insert into fin.fact_gl_change_log(trans,seq,the_date,change)
select a.trans, a.seq, current_date, 'control changed from 1T0538 to 1T40722'
from fin.fact_gl a
where (
  (trans = 3883387 and seq = 22) or   
  (trans = 3885029 and seq = 1) or
  (trans = 3891378 and seq = 8)); 
-- make the change
update fin.fact_gl
set control = '1T40722'
where (
  (trans = 3883387 and seq = 22) or   
  (trans = 3885029 and seq = 1) or
  (trans = 3891378 and seq = 8)); 

  
  
-- 12/1/17 changed fact_gl rows -----------------------------------
-- first update the fact_gl_log
insert into fin.fact_gl_change_log(trans,seq,the_date,change)
select a.trans, a.seq, current_date, 'control changed from 1M41154 to 194460'
from fin.fact_gl a
where (
  (trans = 3858350 and seq = 1) or   
  (trans = 3878668 and seq = 9)); 

update fin.fact_gl
set control = '194460'
where (
  (trans = 3858350 and seq = 1) or   
  (trans = 3878668 and seq = 9));



--< 6/6/17, changed fact_gl rows ------------------------------------------------------------------------------------
create temp table trans_seq as
-- these are the changed rows
select e.trans, e.seq
from arkona.xfm_glptrns e
inner join (
  select 'fact', trans, seq, post_status, md5(a::text) as hash
  from (
    select a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code,
      d.thedate, e.account, a.control, a.doc, a.ref, a.amount, ee.description
    from fin.fact_gl a
    inner join arkona.xfm_glptrns aa on a.trans = aa.trans
      and a.seq = aa.seq
      and a.post_status = aa.post_status
    inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
    inner join fin.dim_journal c on a.journal_key = c.journal_key
    inner join dds.day d on a.date_key = d.datekey
    inner join fin.dim_account e on a.account_key = e.account_key
    inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key) a) f
      on e.trans = f.trans
        and e.seq = f.seq
        and e.post_status = f.post_status
        and e.hash <> f.hash;

select 'xfm' as source, a.trans, a.seq, a.doc_type_code, a.post_status, a.journal_code,
  a.the_date, a.account, a.control, a.doc, a.ref, a.amount, a.description   
from arkona.xfm_glptrns a                           
inner join trans_seq b on a.trans = b.trans and a.seq = b.seq
union           
select 'fact', a.trans, a.seq, doc_type_code, post_status, journal_code,
  the_date, account, control, doc, ref, amount, ee.description   
from fin.fact_gl a
inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.dim_date d on a.date_key = d.date_key
inner join fin.dim_account e on a.account_key = e.account_key
inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key
inner join trans_seq aa on a.trans = aa.trans and a.seq = aa.seq                    
order by trans, seq, source

select * from trans_seq

-- first update the fact_gl_log
insert into fin.fact_gl_change_log(trans,seq,the_date,change)
select a.trans, a.seq, current_date, 'control changed from 17875 to 1997'
from fin.fact_gl a
where trans = 3764579 and seq = 1;
--   (trans = 3702110 and seq = 9) or
--   (trans = 3703875 and seq = 9) or
--   (trans = 3707930 and seq = 11) or
--   (trans = 3707931 and seq = 11) or
--   (trans = 3711020 and seq = 4) or
--   (trans = 3711022 and seq = 6));

update fin.fact_gl
set control = '1997'
where trans = 3764579 and seq = 1;
--   (trans = 3693988 and seq = 21) or   
--   (trans = 3702110 and seq = 9) or
--   (trans = 3703875 and seq = 9) or
--   (trans = 3707930 and seq = 11) or
--   (trans = 3707931 and seq = 11) or
--   (trans = 3711020 and seq = 4) or
--   (trans = 3711022 and seq = 6)); 
--   
-- reran it all and came up with 2 more
update fin.fact_gl
set control = '1117500'
where trans = 3711022 and seq = 5;   

update fin.fact_gl
set control = '2117500'
where trans = 3711022 and seq = 6;  

-- shit keeps changing
update fin.fact_gl
set control = '2117180'
where trans = 3711022 and seq = 6; 

--/> 6/6/17, changed fact_gl rows ------------------------------------------------------------------------------------

-- 5/9/17, changed fact_gl rows ------------------------------------------------------------------------------------
select 'xfm', a.*
from arkona.xfm_glptrns a
where (
  (trans = 3661731 and seq = 4)
  or (trans = 3663517 and seq = 11))       
union all
select 'fact', f.*,  md5(f::text) 
from (
  select a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code,
    d.thedate, e.account, a.control, a.doc, a.ref, a.amount, ee.description
  from fin.fact_gl a
  inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
  inner join fin.dim_journal c on a.journal_key = c.journal_key
  inner join dds.day d on a.date_key = d.datekey
  inner join fin.dim_account e on a.account_key = e.account_key
  inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key
  where (
  (trans = 3661731 and seq = 4)
  or (trans = 3663517 and seq = 11)) ) f 
order by trans, seq 



-- first update the fact_gl_log
insert into fin.fact_gl_change_log(trans,seq,the_date,change)
select a.trans, a.seq, current_date, 'control changed from 113361 to 113363'
from fin.fact_gl a
where (
  (trans = 3661731 and seq = 4) or   
  (trans = 3663517 and seq = 11)) ;


update fin.fact_gl
set control = '113363'
where (
  (trans = 3661731 and seq = 4) or   
  (trans = 3663517 and seq = 11)) ;



  

-- 5/9/17, changed fact_gl rows ------------------------------------------------------------------------------------



-- 4/5/17, changed fact_gl rows ------------------------------------------------------------------------------------
-- detected prior to updating fact_fs for march
select 'xfm' as source, a.*
from arkona.xfm_glptrns a
where (
  (trans = 3628950 and seq = 4) or   
  (trans = 3630027 and seq = 4) or
  (trans = 3630028 and seq = 4) or
  (trans = 3631134 and seq = 4) or
  (trans = 3643539 and seq = 6) or
  (trans = 3648075 and seq = 4))          
union
select 'fact',a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code,
  d.thedate, e.account, a.control, a.doc, a.ref, a.amount, ee.description, ''
from fin.fact_gl a
inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.day d on a.date_key = d.datekey
inner join fin.dim_account e on a.account_key = e.account_key
inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key        
where (
  (trans = 3628950 and seq = 4) or   
  (trans = 3630027 and seq = 4) or
  (trans = 3630028 and seq = 4) or
  (trans = 3631134 and seq = 4) or
  (trans = 3643539 and seq = 6) or
  (trans = 3648075 and seq = 4))     
order by trans, seq, source  



-- first update the fact_gl_log
insert into fin.fact_gl_change_log(trans,seq,the_date,change)
select a.trans, a.seq, current_date, 'control changed from 127065 to 127190'
from fin.fact_gl a
where (
  (trans = 3628950 and seq = 4) or   
  (trans = 3630027 and seq = 4) or
  (trans = 3630028 and seq = 4) or
  (trans = 3631134 and seq = 4) or
  (trans = 3643539 and seq = 6) or
  (trans = 3648075 and seq = 4)) ;


update fin.fact_gl
set control = '127190'
where (
  (trans = 3628950 and seq = 4) or   
  (trans = 3630027 and seq = 4) or
  (trans = 3630028 and seq = 4) or
  (trans = 3631134 and seq = 4) or
  (trans = 3643539 and seq = 6) or
  (trans = 3648075 and seq = 4)); 

-- 4/5/17, changed fact_gl rows ------------------------------------------------------------------------------------

2/10/17 
-- think i finally have the void sitch correct, 43 void rows today
-- these are the rows that have changed (xfm_glptrns different than the same trans/seq row of fact_gl)

-- attributes that are being monitored for change based on trans/seq being equal: 
--   doc_type_code, post_status, journal_code, the_Date, account, control, doc, ref, amount, description
-- drop table trans_seq;
create temp table trans_seq as
select e.trans, e.seq, e.post_status
from arkona.xfm_glptrns e
inner join (
  select 'fact', trans, seq, post_status, md5(a::text) as hash
  from (
    select a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code,
      d.thedate, e.account, a.control, a.doc, a.ref, a.amount, ee.description
    from fin.fact_gl a
    inner join arkona.xfm_glptrns aa on a.trans = aa.trans
      and a.seq = aa.seq -- and a.post_status = aa.post_status
    inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
    inner join fin.dim_journal c on a.journal_key = c.journal_key
    inner join dds.day d on a.date_key = d.datekey
    inner join fin.dim_account e on a.account_key = e.account_key
    inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key) a) f
      on e.trans = f.trans
        and e.seq = f.seq
        -- and e.post_status = f.post_status
        and e.hash <> f.hash;

-- union xfm_glptrs and fact_gl for those rows that have changed
select 'xfm' as source, a.*
from arkona.xfm_glptrns a
inner join trans_seq b on a.trans = b.trans
  and a.seq = b.seq
union                      
select 'fact', a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code, 
  d.the_date, e.account, a.control, a.doc, a.ref, a.amount, f.description, ''
from fin.fact_gl a
inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.dim_date d on a.date_key = d.date_key
inner join fin.dim_account e on a.account_key = e.account_key
inner join fin.dim_gl_description f on a.gl_description_key = f.gl_description_key
inner join trans_seq g on a.trans = g.trans
  and a.seq = g.seq
order by trans, seq, source

-- the only changed attribute is post_status
select j.trans, j.seq, 
  case when j.doc_type_code = k.doc_type_code then '' else 'X' end as doc_type_code,
  case when j.post_status = k.post_status then '' else 'X' end as post_status,
  case when j.journal_code = k.journal_code then '' else 'X' end as journal_code,
  case when j.the_date = k.the_date then '' else 'X' end as the_date,
  case when j.account = k.account then '' else 'X' end as account,
  case when j.control = k.control then '' else 'X' end as control,
  case when j.doc = k.doc then '' else 'X' end as v,
  case when j.ref = k.ref then '' else 'X' end as ref,
  case when j.amount = k.amount then '' else 'X' end as amount,
  case when j.description = k.description then '' else 'X' end as description
from (
  select 'xfm' as source, a.*
  from arkona.xfm_glptrns a
  inner join trans_seq b on a.trans = b.trans
    and a.seq = b.seq) j
inner join (
  select 'fact', a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code, 
    d.the_date, e.account, a.control, a.doc, a.ref, a.amount, f.description, ''
  from fin.fact_gl a
  inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
  inner join fin.dim_journal c on a.journal_key = c.journal_key
  inner join dds.dim_date d on a.date_key = d.date_key
  inner join fin.dim_account e on a.account_key = e.account_key
  inner join fin.dim_gl_description f on a.gl_description_key = f.gl_description_key
  inner join trans_seq g on a.trans = g.trans
    and a.seq = g.seq) k on j.trans = k.trans and j.seq = k.seq 


update fin.fact_gl a
set post_status = x.post_status
from (
  select *
  from trans_seq) X
where a.trans = x.trans
  and a.seq = x.seq;  

 -- 2/12/17 ---------------------------------------------------------------------------
the nightly script has to process void rows
only send an email if another attribute has changed
Due to the horror of actually modifying fact rows, i would like to keep a log of those
rows that get changed

create table fin.fact_gl_change_log (
  trans bigint not null, 
  seq integer not null, 
  the_date date not null,
  change citext not null,
  constraint fact_gl_change_log_pkey primary Key(trans,seq,the_date,change));

void rows:  

create index on arkona.xfm_glptrns(trans);
create index on arkona.xfm_glptrns(seq);
create index on arkona.xfm_glptrns(post_status);

create index on fin.fact_gl(trans);
create index on fin.fact_gl(seq);
create index on fin.fact_gl(post_status);

select e.trans, e.seq, e.post_status
from arkona.xfm_glptrns e
inner join (
  select 'fact', trans, seq, post_status, md5(a::text) as hash
  from (
    select a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code,
      d.thedate, e.account, a.control, a.doc, a.ref, a.amount, ee.description
    from fin.fact_gl a
    inner join arkona.xfm_glptrns aa on a.trans = aa.trans
      and a.seq = aa.seq -- and a.post_status = aa.post_status
    inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
    inner join fin.dim_journal c on a.journal_key = c.journal_key
    inner join dds.day d on a.date_key = d.datekey
    inner join fin.dim_account e on a.account_key = e.account_key
    inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key) a) f
      on e.trans = f.trans
        and e.seq = f.seq
        -- and e.post_status = f.post_status
        and e.hash <> f.hash;


-- bizzaro test, goes from void to not void ---------------------
select b.trans, b.seq, b.post_status
from fin.fact_gl a
inner join arkona.xfm_glptrns b on a.trans = b.trans
    and a.seq = b.seq
    and a.post_status = 'V'
    and b.post_status <> 'V'


-- first update the fact_gl_log
insert into fin.fact_gl_change_log(trans,seq,the_date,change
select b.trans, b.seq, b.post_status, current_date, 'post_status to V'
from fin.fact_gl a
inner join arkona.xfm_glptrns b on a.trans = b.trans
  and a.seq = b.seq
  and a.post_status = 'Y'
  and b.post_status <> 'Y';
  
-- then update the voids
update fin.fact_gl a
set post_status = x.post_status
from (
  select b.trans, b.seq, b.post_status
  from fin.fact_gl a
  inner join arkona.xfm_glptrns b on a.trans = b.trans
      and a.seq = b.seq
      and a.post_status = 'Y'
      and b.post_status <> 'Y') x
where a.trans = x.trans
  and a seq = x.seq;      

    