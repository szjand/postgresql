﻿-- 2011 50 minutes/329,013 kb/
-- 2,266,344
select count(*) from dds.ext_glptrns_2011
-- 8 rows
-- so, what i am thinking, is that everything goes into ext_glptrns, no constraints
-- but that these are excluded from fact_gl
select rrn
from dds.ext_glptrns_2011 a
inner join (
  select gttrn_, gtseq_
  from dds.ext_glptrns_2011
  where gtpost = 'Y'
  group by gttrn_, gtseq_
  having count(*) > 1) b on a.gttrn_ = b.gttrn_ and a.gtseq_ = b.gtseq_
   
-- 2,192,166
select count(*) from dds.ext_glptrns_2012
-- 28 rows
select rrn
from dds.ext_glptrns_2012 a
inner join (
  select gttrn_, gtseq_
  from dds.ext_glptrns_2012
  where gtpost = 'Y'
  group by gttrn_, gtseq_
  having count(*) > 1) b on a.gttrn_ = b.gttrn_ and a.gtseq_ = b.gtseq_

-- 2,187,076
select count(*) from dds.ext_glptrns_2013
-- 36 rows
select rrn
from dds.ext_glptrns_2013 a
inner join (
  select gttrn_, gtseq_
  from dds.ext_glptrns_2013
  where gtpost = 'Y'
  group by gttrn_, gtseq_
  having count(*) > 1) b on a.gttrn_ = b.gttrn_ and a.gtseq_ = b.gtseq_

-- 2,247,858
select count(*) from dds.ext_glptrns_2014
-- 50 rows
select rrn
from dds.ext_glptrns_2014 a
inner join (
  select gttrn_, gtseq_
  from dds.ext_glptrns_2014
  where gtpost = 'Y'
  group by gttrn_, gtseq_
  having count(*) > 1) b on a.gttrn_ = b.gttrn_ and a.gtseq_ = b.gtseq_

-- 2,369,097
select count(*) from dds.ext_glptrns_2015
-- 8 rows
select rrn
from dds.ext_glptrns_2015 a
inner join (
  select gttrn_, gtseq_
  from dds.ext_glptrns_2015
  where gtpost = 'Y'
  group by gttrn_, gtseq_
  having count(*) > 1) b on a.gttrn_ = b.gttrn_ and a.gtseq_ = b.gtseq_


-- 1,2232,481 dds.ext_glptrns_2016 
-- 22 rows
select *
from dds.ext_glptrns_2016 a
inner join (
  select gttrn_, gtseq_ 
  from dds.ext_glptrns_2016 
  where gtpost = 'Y' 
  group by gttrn_, gtseq_ 
  having count(*) > 1) b on a.gttrn_ = b.gttrn_ and a.gtseq_ = b.gtseq_


select * from dds.ext_glptrns_2016
where gttrn_ = 3371202
order by gtseq_


select * from dds.ext_glptrns_2016
where gttamt = 189.04
order by gtseq_



    