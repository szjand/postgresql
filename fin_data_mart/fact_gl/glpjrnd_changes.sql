﻿
select 'jrnl' as source, *, (select md5(a::text) as hash) from arkona.ext_glpjrnd a where journal_code in('paa') and company_number = 'RY8'
union
select 'tmp' as source, *, (select md5(a::text) as hash) from arkona.ext_glpjrnd_tmp a where journal_code in('paa') and company_number = 'RY8'
order by journal_code, source

update arkona.ext_glpjrnd
set transaction_desc = 'BAL'
where company_number = 'ry8'
  and journal_code in ('GJE');
---------------------------------------------------------------------
--< 07/15/22  
---------------------------------------------------------------------
                select count (*)
                from (
                    select * from arkona.ext_glpjrnd where TRIM(COMPANY_NUMBER) in ('RY1', 'RY2', 'RY8')
                    except
                    select * from arkona.ext_glpjrnd_tmp where TRIM(COMPANY_NUMBER) in ('RY1', 'RY2', 'RY8')
                union all (
                    select * from arkona.ext_glpjrnd_tmp where TRIM(COMPANY_NUMBER) in ('RY1', 'RY2', 'RY8')
                    except
                    select * from arkona.ext_glpjrnd where TRIM(COMPANY_NUMBER) in ('RY1', 'RY2', 'RY8'))) x
		
select count (*)
from (
		select * from arkona.ext_glpjrnd where company_number = 'ry8'
		except
		select * from arkona.ext_glpjrnd_tmp where company_number = 'ry8'
union all (
		select * from arkona.ext_glpjrnd_tmp where company_number = 'ry8'
		except
		select * from arkona.ext_glpjrnd where company_number = 'ry8')) x

-- RY8 changed rows
select *
from (
	select 'jrnl', a.*,
		(select md5(a::text) as hash)
	from arkona.ext_glpjrnd a
	where company_number = 'RY8') aa
join (
	select 'tmp', a.*,
		(select md5(a::text) as hash)
	from arkona.ext_glpjrnd_tmp a
	where company_number = 'RY8') bb on aa.journal_code = bb.journal_code
	  and aa.hash <> bb.hash

select 'jrnl' as source, *, (select md5(a::text) as hash) from arkona.ext_glpjrnd a where journal_code in('vsu','vsn') and company_number = 'RY8'
union
select 'tmp' as source, *, (select md5(a::text) as hash) from arkona.ext_glpjrnd_tmp a where journal_code in('vsu','vsn') and company_number = 'RY8'
order by journal_code, source

update arkona.ext_glpjrnd
set transaction_desc = 'BAL'
where company_number = 'ry8'
  and journal_code in ('VSN','VSU');
---------------------------------------------------------------------
--/> 07/15/22  
---------------------------------------------------------------------
---------------------------------------------------------------------
--< 07/03/22  the addition of RY8
---------------------------------------------------------------------
-- lots of changes after having added RY8, kind of a  mess
select count (*)
from (
		select * from arkona.ext_glpjrnd where company_number = 'ry8'
		except
		select * from arkona.ext_glpjrnd_tmp where company_number = 'ry8'
union all (
		select * from arkona.ext_glpjrnd_tmp where company_number = 'ry8'
		except
		select * from arkona.ext_glpjrnd where company_number = 'ry8')) x

-- journals missing from ry8
select * 
from arkona.ext_glpjrnd_tmp a
where company_number = 'ry8'
  and not exists (
    select 1
    from arkona.ext_glpjrnd
    where journal_code = a.journal_code
      and company_number = 'ry8')
order by journal_code		

insert into arkona.ext_glpjrnd
select *
from arkona.ext_glpjrnd_tmp
where company_number = 'ry8'
  and journal_code in ('paa');

-- ry1, gli add transaction_desc  
update arkona.ext_glpjrnd
set transaction_desc = 'BAL'
where company_number = 'RY1'
  and journal_code = 'GLI';


		
-- RY8 changed rows
select *
from (
	select 'jrnl', a.*,
		(select md5(a::text) as hash)
	from arkona.ext_glpjrnd a
	where company_number = 'RY8') aa
join (
	select 'tmp', a.*,
		(select md5(a::text) as hash)
	from arkona.ext_glpjrnd_tmp a
	where company_number = 'RY8') bb on aa.journal_code = bb.journal_code
	  and aa.hash <> bb.hash
	  
-- a mishmash of changed rows
select 'jrnl' as source, *, (select md5(a::text) as hash) from arkona.ext_glpjrnd a where journal_code in('pca','pcc','pot','sca','svi','wcm') and company_number = 'RY8'
union
select 'tmp' as source, *, (select md5(a::text) as hash) from arkona.ext_glpjrnd_tmp a where journal_code in('pca','pcc','pot','sca','svi','wcm') and company_number = 'RY8'
order by journal_code, source	  

update arkona.ext_glpjrnd
set transaction_desc = 'BAL'
where company_number = 'ry8'
  and journal_code = 'POT';
  
update arkona.ext_glpjrnd
set process_by_batch = 'Y'
where company_number = 'ry8'
  and journal_code in ('pca','pcc','pot','sca','svi');

update arkona.ext_glpjrnd
set journal_desc = 'Warranty Credit Memo'
where company_number = 'RY8'
  and journal_code = 'WCM';
---------------------------------------------------------------------
--/> 07/03/22  the addition of RY8
---------------------------------------------------------------------
---------------------------------------------------------------------
--< 07/02/22  the addition of RY8
---------------------------------------------------------------------

-- add row to arkona.ext_glpjrnd
insert into arkona.ext_glpjrnd
select * from arkona.ext_glpjrnd_tmp
except
select * from arkona.ext_glpjrnd   

---------------------------------------------------------------------
--/> 07/02/22  the addition of RY8
---------------------------------------------------------------------

---------------------------------------------------------------------
--< 06/28/22  changes to attributes control_prompt, document_prompt, override_prompt !?!?!?
---------------------------------------------------------------------
                select count (*)
                from (
                    select * from arkona.ext_glpjrnd
                    except
                    select * from arkona.ext_glpjrnd_tmp
                union all (
                    select * from arkona.ext_glpjrnd_tmp
                    except
                    select * from arkona.ext_glpjrnd)) x
select 'tmp' as source, * from arkona.ext_glpjrnd_tmp where journal_code in('POT','PVU','WTD','DRI','STD','EFT','GJE','PVI','WCM') and company_number = 'RY1'
union
select 'base', * from arkona.ext_glpjrnd where journal_code in('POT','PVU','WTD','DRI','STD','EFT','GJE','PVI','WCM') and company_number = 'RY1'                  
order by journal_code, source

update arkona.ext_glpjrnd
set transaction_desc = 'BAL'
where company_number = 'RY1'
  and journal_code in('POT','PVU','WTD','DRI','STD','EFT','GJE','PVI','WCM')

update arkona.ext_glpjrnd
set document_prompt = 'Document Number', override_prompt = 'Override CTL#'
where company_number = 'RY1'
  and journal_code in('WTD','STD')  

update arkona.ext_glpjrnd
set control_prompt = 'Control Number'
where company_number = 'RY1'
  and journal_code in('STD')    

---------------------------------------------------------------------
--/> 06/28/22
---------------------------------------------------------------------



-- 01/14/2020, new journal: DEP
                select count (*)
                from (
                    select * from arkona.ext_glpjrnd
                    except
                    select * from arkona.ext_glpjrnd_tmp
                union all (
                    select * from arkona.ext_glpjrnd_tmp
                    except
                    select * from arkona.ext_glpjrnd)) x

select * from arkona.ext_glpjrnd order by journal_code          

-- add row to arkona.ext_glpjrnd
insert into arkona.ext_glpjrnd
select * from arkona.ext_glpjrnd_tmp
except
select * from arkona.ext_glpjrnd         

-- add row to fin.dim_journal
-- yikes, RY5 !?!?!
select *
from arkona.ext_glpjrnd a
where not exists (
  select 1
  from fin.dim_journal
  where journal_code = a.journal_code)

-- add row to fin.dim_journal
insert into fin.dim_journal(journal_code,journal, row_from_date,current_row,row_reason)
select journal_code, journal_desc, current_date - 1, true, 'new journal created by controller'
from arkona.ext_glpjrnd
where journal_code = 'DEP'
