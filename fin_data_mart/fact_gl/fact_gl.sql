﻿select min(gtdate) from dds.ext_glptrns

select count(*) from dds.ext_glptrns

-- do the voids go into the fact table?
-- 
-- i am thinking  yes, doesn't hurt anything and they are transactions and 
-- have a separate rrn

thinking it is going to be desirable to keep  a version of the raw data

-- 7/6
-- 3,579,394
select count(*) from dds.ext_glptrns

-- no dups
select rrn from dds.ext_Glptrns group by rrn having count(*) > 1

select gttrn_, gtseq_ from dds.ext_glptrns where gtpost = 'Y' group by gttrn_, gtseq_ having count(*) > 1

-- the only fucked up (dup trn/seq)
-- exclude these rrns: 12664356, 12664357, 12664353, 12664355
select * from dds.ext_glptrns where gttrn_ = '2928213' order by gtseq_
select * from dds.ext_glptrns where gttrn_ = '2928213' order by rrn


voids

select *
from dds.ext_glptrns a
inner join (
  select gttrn_, gtseq_ from dds.ext_glptrns group by gttrn_, gtseq_ having count(*) > 1) b on a.gttrn_ = b.gttrn_ and a.gtseq_ = b.gtseq_
order by a.gttrn_, a.gtseq_  

so, group on the subset of attributes being used and use that has a basis for adding new rows
i have chosen to decide, that that subset of attributes does not change over time

hmmm, so the only subset on which i need to group is grn/seq/post

play with it in the cdc stuff (ubuntu server)

-- oh shit it doesn't really matter anyway, voids whether dup or not will excluded from all queries of interest anyway

select gtacct, count(*)
from dds.ext_glptrns
where left(gtacct, 1) not in ('1','2','3','4','5','6','7','8','9')
group by gtacct 

select * from dds.ext_glptrns where gtacct = '*E*'

select * from dds.ext_glptrns where gttrn_ = 3319972

select * from dds.ext_glptrns where gtctl_ = '19238041' order by gttamt


select * from dds.ext_glptrns where gtacct = '*SPORD'

-- i am on the verge of fuck it, i don't care how many bogus rows exist, queries will always be such
-- that the bogus rows have no effect, either gtpost = 'Y', or account is valid
-- i am ok with that rather than fabricating some bullshit rules governing what i don't understand

-- which leads me to using the rrn for cdc
-- transactions don't get modified, they are either added or voided.

-- sketch it out in CDC (Ubuntu server)s

drop table if exists fin.fact_gl cascade;
create table fin.fact_gl (
  trans bigint not null,
  seq integer not null,
  doc_type_key integer not null references fin.dim_doc_type(doc_type_key),
  post_status citext not null,
  journal_key integer not null references fin.dim_journal(journal_key),
  date_key integer not null references dds.day(datekey),
  account_key integer not null references fin.dim_account(account_key),
  control citext not null,
  doc citext not null,
  ref citext not null,
  amount numeric(12,2) not null,
  rrn bigint not null unique);
create unique index on fin.fact_gl(trans,seq) where post_status = 'Y';
create index on fin.fact_gl(doc_type_key);
create index on fin.fact_gl(post_status);
create index on fin.fact_gl(journal_key);
create index on fin.fact_gl(date_key);
create index on fin.fact_gl(account_key);



-- truncate dds.ext_glptrns;

insert into dds.ext_glptrns
select *
from dds.ext_glptrns_2016 a;

7/11/16 have only entered transactions from jan thru may for 2016
wanted jeri to see the dup transaction/seq rows and see if she could discern
anything about them
nope, she does not
as far as she can tell they are all valed entries, she has no notion of why 
they are dup trans/seq
oh well, 
so what i am thinking is rather than simply excluding them
analyze/modify them
1. exclude 0 amount transactions
2. 2016 these could be segregated by the 
-- select * from dds.ext_glptrns_2016 limit 100
-- select count(*) from fin.fact_gl
insert into fin.fact_gl
select gttrn_, gtseq_, f.doc_type_key, e.gtpost,
  coalesce(g.journal_key, gg.journal_key), 
  h.datekey,
  coalesce(i.account_key, j.account_key),
  e.gtctl_, e.gtdoc_, e.gtref_,
  e.gttamt, e.rrn
from dds.ext_glptrns_2016 e
left join fin.dim_doc_type f on e.gtdtyp = f.doc_type_code
left join fin.dim_journal g on e.gtjrnl = g.journal_code
left join fin.dim_journal gg on 1 = 1 
  and gg.journal_code = 'none'
left join dds.day h on e.gtdate = h.thedate
left join fin.dim_account i on e.gtacct = i.account
left join fin.dim_account j on 1 = 1 
  and j.account = 'none'
where e.rrn not in (
  select rrn
  from dds.ext_glptrns_2016 a
  inner join (
    select gttrn_, gtseq_
    from dds.ext_glptrns_2016
    where gtpost = 'Y'
    group by gttrn_, gtseq_
    having count(*) > 1) b on a.gttrn_ = b.gttrn_ and a.gtseq_ = b.gtseq_)
and gtdate < '06/01/2016'; 

-- 2016 (7/18) the offenders
select a.*
from dds.ext_glptrns_2016 a
inner join (
select gttrn_, gtseq_
from dds.ext_glptrns_2016
where gtpost = 'Y'
group by gttrn_, gtseq_
having count(*) > 1) b on a.gttrn_ = b.gttrn_ and a.gtseq_ = b.gtseq_
-- the fix
update dds.ext_glptrns_2016
set gtseq_ = gtseq_ + 100
-- select * from dds.ext_glptrns_2016
where gttrn_ = 3371202
  and gtseq_ < 12
  and gtdesc = 'GM RESERVES/CHGBKS'
-- voila
select gttrn_, gtseq_
from dds.ext_glptrns_2016
where gtpost = 'Y'
group by gttrn_, gtseq_
having count(*) > 1

-- now go ahead and update fact_gl with post 5/31 data
select * -- 315,392 rows (3 minutes)
from dds.ext_glptrns_2016 e
where not exists (
  select 1
  from fin.fact_gl
  where rrn = e.rrn)

insert into fin.fact_gl
select gttrn_, gtseq_, f.doc_type_key, e.gtpost,
  coalesce(g.journal_key, gg.journal_key), 
  h.datekey,
  coalesce(i.account_key, j.account_key),
  e.gtctl_, e.gtdoc_, e.gtref_,
  e.gttamt, e.rrn
from dds.ext_glptrns_2016 e
left join fin.dim_doc_type f on e.gtdtyp = f.doc_type_code
left join fin.dim_journal g on e.gtjrnl = g.journal_code
left join fin.dim_journal gg on 1 = 1 
  and gg.journal_code = 'none'
left join dds.day h on e.gtdate = h.thedate
left join fin.dim_account i on e.gtacct = i.account
left join fin.dim_account j on 1 = 1 
  and j.account = 'none'
where not exists (
  select 1
  from fin.fact_gl
  where rrn = e.rrn)  

-- 7/25 -- ok, let's do the task -----------------------------

select * from ops.tasks 
select * from ops.task_dependencies 

insert into ops.tasks values ('fact_gl', 'Daily');
insert into ops.task_dependencies values 
  ('dim_journal','fact_gl'),
  ('dim_account','fact_gl');


select count(*) from (
select *
from dds.ext_glptrns_tmp
except 
select *
from dds.ext_glptrns) x


select *
from dds.ext_Glptrns_tmp a
where not exists ( 
  select 1
  from dds.ext_glptrns
  where rrn = a.rrn)
limit 110000


insert into dds.ext_glptrns (gtco_,gttrn_,gtseq_,gtdtyp,gttype,
  gtpost,gtrsts,gtadjust,gtpsel,gtjrnl,gtdate,gtrdate,gtsdate, 
  gtacct,gtctl_,gtdoc_,gtrdoc_,gtrdtyp,gtodoc_,gtref_,gtvnd_,
  gtdesc,gttamt,gtcost,gtctlo,gtoco_,rrn)
select gtco_,gttrn_,gtseq_,gtdtyp,gttype,
  gtpost,gtrsts,gtadjust,gtpsel,gtjrnl,gtdate,gtrdate,gtsdate, 
  gtacct,gtctl_,gtdoc_,gtrdoc_,gtrdtyp,gtodoc_,gtref_,gtvnd_,
  gtdesc,gttamt,gtcost,gtctlo,gtoco_,rrn
from dds.ext_glptrns_tmp a
where not exists (
  select 1
  from dds.ext_glptrns
  where rrn = a.rrn)



select gtco_,gttrn_,gtseq_,gtdtyp,gttype,
  gtpost,gtrsts,gtadjust,gtpsel,gtjrnl,gtdate,gtrdate,gtsdate, 
  gtacct,gtctl_,gtdoc_,gtrdoc_,gtrdtyp,gtodoc_,gtref_,gtvnd_,
  gtdesc,gttamt,gtcost,gtctlo,gtoco_,rrn
from dds.ext_glptrns_tmp a
where not exists (
  select 1
  from dds.ext_glptrns
  where rrn = a.rrn)


select gttrn_, gtseq_
from (  
select gtco_,gttrn_,gtseq_,gtdtyp,gttype,
  gtpost,gtrsts,gtadjust,gtpsel,gtjrnl,gtdate,gtrdate,gtsdate, 
  gtacct,gtctl_,gtdoc_,gtrdoc_,gtrdtyp,gtodoc_,gtref_,gtvnd_,
  gtdesc,gttamt,gtcost,gtctlo,gtoco_,rrn
from dds.ext_glptrns_tmp a
where gtpost = 'Y'
  AND not exists (
    select 1
    from fin.fact_gl
    where rrn = a.rrn)) x
group by gttrn_, gtseq_     
having count(*) > 1


select count(1) 
from (
  select gttrn_, gtseq_
  from dds.ext_glptrns_tmp a
  where gtpost = 'Y'
    AND not exists (
      select 1
      from fin.fact_gl
      where rrn = a.rrn)
  group by gttrn_, gtseq_     
  having count(*) > 1) x;


alter table fin.z_unused_dim_doc_type
rename to dim_doc_type

insert into fin.fact_gl
select gttrn_, gtseq_, f.doc_type_key, e.gtpost,
  coalesce(g.journal_key, gg.journal_key), 
  h.datekey,
  coalesce(i.account_key, j.account_key),
  e.gtctl_, e.gtdoc_, e.gtref_,
  e.gttamt, e.rrn
from dds.ext_glptrns_tmp e
left join fin.dim_doc_type f on e.gtdtyp = f.doc_type_code
left join fin.dim_journal g on e.gtjrnl = g.journal_code
left join fin.dim_journal gg on 1 = 1 
  and gg.journal_code = 'none'
left join dds.day h on e.gtdate = h.thedate
left join fin.dim_account i on e.gtacct = i.account
left join fin.dim_account j on 1 = 1 
  and j.account = 'none'
where not exists (
  select 1
  from fin.fact_gl
  where rrn = e.rrn)    

-- 8/28/16 ------------------------------------------------------------------------------------------------------
VOIDS  
rows that get updated to void retroactively

select *
from dds.ext_glptrns_tmp
where gtpost <> 'Y'

create table dds.glptrns_voids (
  gttrn_ bigint,
  gtseq_ integer, 
  gtpost citext,
  rrn bigint);

select count(*) from dds.glptrns_voids  

select * from dds.glptrns_voids limit 1000

select gtpost, count(*) from dds.glptrns_voids group by gtpost

select post_status, count(*) from fin.fact_gl group by post_status

select coalesce(gtpost, 'xx'), count(*) from dds.ext_glptrns where gtpost <> 'Y' group by gtpost

update   

select aa.thedate, a.*, b.* 
from fin.fact_gl a
inner join dds.glptrns_voids b on a.trans = b.gttrn_ 
  and a.seq = b.gtseq_
  and a.rrn = b.rrn
where a.post_status <> b.gtpost  

update dds.ext_glptrns aa
set gtpost = x.gtpost
from (
  select a.gttrn_, a.gtseq_, a.rrn, b.gtpost
  from dds.ext_glptrns a
  inner join dds.glptrns_voids b on a.gttrn_ = b.gttrn_
    and a.gtseq_ = b.gtseq_
    and a.rrn = b.rrn
  where a.gtpost <> b.gtpost) x
where aa.gttrn_ = x.gttrn_ 
  and aa.gtseq_ = x.gtseq_
  and aa.rrn = x.rrn;

update fin.fact_gl aa
set post_status = x.gtpost
from (
  select a.trans, a.seq, a.rrn, b.gtpost
  from fin.fact_gl a
  inner join dds.glptrns_voids b on a.trans = b.gttrn_
    and a.seq = b.gtseq_
    and a.rrn = b.rrn
  where a.post_status <> b.gtpost) x
where aa.trans = x.trans
  and aa.seq = x.seq
  and aa.rrn = x.rrn;   

-- and the update queries for fact_gl.py
update dds.ext_glptrns aa
set gtpost = x.gtpost
from (
  select a.gttrn_, a.gtseq_, a.rrn, b.gtpost
  from dds.ext_glptrns a
  inner join dds.ext_glptrns_tmp b on a.gttrn_ = b.gttrn_
    and a.gtseq_ = b.gtseq_
    and a.rrn = b.rrn
  where a.gtpost <> b.gtpost) x
where aa.gttrn_ = x.gttrn_ 
  and aa.gtseq_ = x.gtseq_
  and aa.rrn = x.rrn;

update fin.fact_gl aa
set post_status = x.gtpost
from (
  select a.trans, a.seq, a.rrn, b.gtpost
  from fin.fact_gl a
  inner join dds.ext_glptrns_tmp b on a.trans = b.gttrn_
    and a.seq = b.gtseq_
    and a.rrn = b.rrn
  where a.post_status <> b.gtpost) x
where aa.trans = x.trans
  and aa.seq = x.seq
  and aa.rrn = x.rrn;   

---------------------------------------------------------------------------------------------------------------- 
-- 10/5/16 add the join to dim_doc_type for none


select gttrn_, gtseq_, coalesce(f.doc_type_key, ff.doc_type_key), e.gtpost,
  coalesce(g.journal_key, gg.journal_key),
  h.datekey,
  coalesce(i.account_key, j.account_key),
  e.gtctl_, e.gtdoc_, e.gtref_,
  e.gttamt, e.rrn
from dds.ext_glptrns_tmp e
left join fin.dim_doc_type f on e.gtdtyp = f.doc_type_code
left join fin.dim_doc_type ff on 1 = 1 
  and ff.doc_type_code = 'none'
left join fin.dim_journal g on e.gtjrnl = g.journal_code
left join fin.dim_journal gg on 1 = 1
  and gg.journal_code = 'none'
left join dds.day h on e.gtdate = h.thedate
left join fin.dim_account i on e.gtacct = i.account
left join fin.dim_account j on 1 = 1
  and j.account = 'none'
where not exists (
  select 1
  from fin.fact_gl
  where rrn = e.rrn)