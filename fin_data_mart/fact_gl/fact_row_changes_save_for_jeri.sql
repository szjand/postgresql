﻿select * from jon.transactions
order by source, trans, seq

drop table if exists jon.transactions_complete cascade;
create table jon.transactions_complete as
select 'xfm' as source, a.trans, a.seq, a.doc_type_code, a.post_status, a.journal_code,
  a.the_date, a.account, a.control, a.doc, a.ref, a.amount, a.description   
from arkona.xfm_glptrns a                           
where trans = 5678973
union           
select 'fact', a.trans, a.seq, doc_type_code, post_status, journal_code,
  the_date, account, control, doc, ref, amount, ee.description   
from fin.fact_gl a
inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.dim_date d on a.date_key = d.date_key
inner join fin.dim_account e on a.account_key = e.account_key
inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key
-- inner join trans_seq aa on a.trans = aa.trans and a.seq = aa.seq h
where trans = 5678973
order by trans, seq, source

select 
  case
    when source = 'fact' then 'original'
    else 'modified'
  end as source,
  trans,seq,doc_type_code as doc_code, post_Status,journal_code,
  the_date, account, replace(control, ',', ' ') as control, doc, ref, amount, replace(description, ',', ' ') as description
from jon.transactions_complete
order by seq, source desc
