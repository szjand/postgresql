﻿-- i may eventually want to log what the changes are
-- but for now, just update the fact table
-- this is what the task returns
select *
from arkona.xfm_glptrns e
inner join (
  select 'fact', trans, seq, post_status, md5(a::text) as hash
  from (
    select a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code,
      d.thedate, e.account, a.control, a.doc, a.ref, a.amount, ee.description
    from fin.fact_gl a
    inner join arkona.xfm_glptrns aa on a.trans = aa.trans
      and a.seq = aa.seq and a.post_status = aa.post_status
    inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
    inner join fin.dim_journal c on a.journal_key = c.journal_key
    inner join dds.day d on a.date_key = d.datekey
    inner join fin.dim_account e on a.account_key = e.account_key
    inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key) a) f
      on e.trans = f.trans
        and e.seq = f.seq
        -- and e.post_status = f.post_status
        and e.hash <> f.hash

-- union arkona.xfm_glptrns with fin.fact_gl
select 'xfm', a.*
from arkona.xfm_glptrns a
where (
  (trans = 3530397 and seq = 3)
  or (trans = 3528452 and seq = 3)
  or (trans = 3530397 and seq = 2)
  or (trans = 3528452 and seq = 2))       
union all
select 'fact', f.*,  md5(f::text) 
from (
  select a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code,
    d.thedate, e.account, a.control, a.doc, a.ref, a.amount, ee.description
  from fin.fact_gl a
  inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
  inner join fin.dim_journal c on a.journal_key = c.journal_key
  inner join dds.day d on a.date_key = d.datekey
  inner join fin.dim_account e on a.account_key = e.account_key
  inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key
  where (
    (trans = 3530397 and seq = 3)
    or (trans = 3528452 and seq = 3)
    or (trans = 3530397 and seq = 2)
    or (trans = 3528452 and seq = 2))) f 
order by trans, seq   

update fin.fact_gl
set control = '1112800'
-- select * from fin.fact_gl
  where (
    (trans = 3530397 and seq = 3)
    or (trans = 3528452 and seq = 3)
    or (trans = 3530397 and seq = 2)
    or (trans = 3528452 and seq = 2));

