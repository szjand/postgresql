﻿
-- although this is strictly a type 1 dimension, the same metadata
-- rows should be present in ALL dimension tables
drop table fin.dim_gl_description cascade;
create table fin.dim_gl_description (
  gl_description_key serial primary key,
  description citext not null,
  row_from_date date not null,
  row_thru_date date not null DEFAULT '9999-12-31'::date,
  current_row boolean not null,
  row_reason citext not null,
  constraint dim_gl_description_nk unique(description));
COMMENT ON COLUMN fin.dim_gl_description.gl_description_key IS 'source: system generated surrogate key';
COMMENT ON COLUMN fin.dim_gl_description.description IS 'source: arkona.glptrns.gtdesc';

insert into fin.dim_gl_description (description, row_from_date,current_row,row_reason)
values ('none', current_date, true, 'table creation');

-- start with 2016, so i can figure out what the nightly update looks like
-- 2016, 2011, 2012, 2013, 2014, 2015
-- 1210, 1211, 1212, 1213, 1214
drop table fin.xfm_gl_description;
create table fin.xfm_gl_description (
  trans bigint not null,
  seq integer not null,
  post_status citext not null,
  description citext not null,
  constraint xfm_gl_description_pkey primary key (trans,seq,post_status,description));

-- 12/19/16 in production, don't need to group ext_glptrns_tmp (again)
-- use arkona.xfm_glptrns
-- oops, can't, ext_glptrns_tmp does not have the description in it
-- shit when it comes to fact_gl, if gtdesc is not in xfm_glptrns, we won't be checking for 
-- changed comments: once a row is inserted into fact_gl it is assigned a gl_descritpion_key and
-- that's it for the life of the row
-- do i care if comments change?
-- that might best be answered by including gtdesc in the hash, since i am stopping at 
-- generating an email for now if a row changes, this will give me the opportunity to 
-- determine what fucking actually changes.
-- no longer even need the xfm_gl_description table, ahh, keep it, the interim RI could be usedful
-- and it is needed for joining to fact_gl (trans,seq,post_status) to assign dim_gl_description_key
truncate fin.xfm_gl_description;
insert into fin.xfm_gl_description
select gttrn_, gtseq_, gtpost, coalesce(gtdesc, 'none') -- this will take care of trans with no description
from arkona.xfm_glptrns
where gtco_ = 'RY1'
  and gtpost in ('V','Y')
  and left(gtacct, 1) <> '3'
--   and gtdesc is not null
group by gttrn_, gtseq_, gtpost, gtdesc;   

  
-- update the dim
insert into fin.dim_gl_description (description, row_from_date, current_row, row_reason)
select distinct description, current_date, true, 'table creation' 
from fin.xfm_gl_description a-- 157070
where not exists (
  select 1 
  from fin.dim_gl_description
  where description = a.description);

-- update fact_gl
-- same old stupid problem of not knowing how to get the appropriate subset of fact_gl
-- inner join ahhh wait, if xfm_gl_description does not exclude transactions with no 
-- description, i will be ok
-- 12/17 going home, think we are good ext_2016 done
-- doing the update based on the 45 day scrape, if the description has changed, this updates it
-- 12/19 but in production, will be generating new rows, not updating
update fin.fact_gl x
set gl_description_key = z.gl_description_key
from (
  select a.trans, a.seq, a.post_status, c.gl_description_key
  from fin.fact_gl a
  inner join fin.xfm_gl_description b on a.trans = b.trans
    and a.seq = b.seq
    and a.post_status = b.post_status
  left join fin.dim_gl_description c on b.description = c.description) z
where x.trans = z.trans
  and x.seq = z.seq
  and x.post_status = z.post_status;  

