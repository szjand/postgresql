﻿/*
table fin.budget
  "redimentary" storage for the data from jeri's spreadsheet (RAC 2016 Dealership Budget v2.xlsx)
  departments:
      'body shop'
      'detail'
      'pdq'
      'sales'
      'carwash'
      'main shop'
      'parts'
      
currently contains data for jan - jul 2016, ry1 only    
*/
-- sales 1st quarter budgeted gross -- by month
select a.department, 
  a.value as "201601",
  b.value as "201602",
  c.value as "201603"
from fin.budget a
left join fin.budget b on a.department = b.department
  and a.display_seq = b.display_seq
  and b.year_month = 201602
left join fin.budget c on a.department = c.department
  and a.display_seq = c.display_seq
  and c.year_month = 201603
where a.department = 'sales'
  and a.year_month = 201601
  and a.display_seq = 2;

-- sales 1st quarter budgeted gross -- for entire quarter
select a.department, a.value + b.value + c.value
from fin.budget a
left join fin.budget b on a.department = b.department
  and a.display_seq = b.display_seq
  and b.year_month = 201602
left join fin.budget c on a.department = c.department
  and a.display_seq = c.display_seq
  and c.year_month = 201603
where a.department = 'sales'
  and a.year_month = 201601
  and a.display_seq = 2;

/*
table fin.fact_gl
  just what it sounds like
  1 row per gl entry :: rrn (relative row number) a degenerate dimension from glptrns 
  transaction/sequence are unique for non void entries
  update nightly, all gl transactions since 01/01/2011
*/  

-- all non-void entries to RY1 Used Car Sales accounts on 7/25/16

select a.*, c.account, c.description
from fin.fact_gl a
inner join dds.day b on a.date_key = b.datekey
  and b.thedate = '07/25/2016'
inner join fin.dim_account c on a.account_key = c.account_key
  and c.store_code = 'RY1'
  and c.department_code = 'UC'
  and c.account_type = 'Sale'
where a.post_status = 'Y';


/*
table fin.fact_fs
  all financial statement data from 01/01/2011 thru 06/30/2011
*/

-- Expenses, page 4, at cigar box level
select b.area, a.page, a.line, c.line_label,
  sum(case when department = 'body shop' then amount end) as "Body Shop",
  sum(case when department = 'parts' then amount end) as Parts,
  sum(case when sub_department = 'mechanical' then amount end) as "Main Shop",
  sum(case when sub_department = 'quick lane' then amount end) as "PDQ",
  sum(case when sub_department = 'detail' then amount end) as "Detail",
  sum(case when sub_department = 'car wash' then amount end) as Carwash
from fin.fact_fs a
inner join fin.dim_fs_org b on a.fs_org_key = b.fs_org_key
  and b.store = 'RY1'
  and b.area = 'Fixed'
inner join fin.fact_fs_layout c on a.page = c.page and a.line = c.line and left(a.year_month::text, 4)::integer = c.the_year 
where a.year_month = 201605
  and a.page = 4
group by b.area,a.page, a.line, c.line_label
order by a.page, a.line

-- Expenses, page 4, at fs level
select b.area, a.page, a.line, c.line_label,
  sum(case when department = 'service' then amount end) as Mechanical,
  sum(case when department = 'body shop' then amount end) as "Body Shop",
  sum(case when department = 'parts' then amount end) as Parts
from fin.fact_fs a
inner join fin.dim_fs_org b on a.fs_org_key = b.fs_org_key
  and b.store = 'RY1'
  and b.area = 'Fixed'
inner join fin.fact_fs_layout c on a.page = c.page and a.line = c.line and left(a.year_month::text, 4)::integer = c.the_year 
where a.year_month = 201605
  and a.page = 4
group by b.area,a.page, a.line, c.line_label
order by a.page, a.line

-- Expenses, Fixed, page 2, at fs level
select b.area, a.page, a.line, c.line_label, sum(amount) as "Fixed Operations"
from fin.fact_fs a
inner join fin.dim_fs_org b on a.fs_org_key = b.fs_org_key
  and b.store = 'RY1'
  and b.area = 'Fixed'
inner join fin.fact_fs_layout c on a.page = c.page and a.line = c.line and left(a.year_month::text, 4)::integer = c.the_year 
where a.year_month = 201605
  and a.page = 4
group by b.area,a.page, a.line, c.line_label
order by a.page, a.line

-- All Page 2 expenses
select b.area, a.page, a.line, c.line_label, 
  sum(amount) as Dealership,
  sum(case when b.area = 'variable' then amount end) as "Variable Operations",
  sum(case when b.area = 'fixed' then amount end) as "Fixed Operations"
from fin.fact_fs a
inner join fin.dim_fs_org b on a.fs_org_key = b.fs_org_key
  and b.store = 'RY1'
inner join fin.fact_fs_layout c on a.page = c.page and a.line = c.line and left(a.year_month::text, 4)::integer = c.the_year 
where a.year_month = 201605
  and a.page in (3,4)
group by b.area,a.page, a.line, c.line_label
order by a.page, a.line

