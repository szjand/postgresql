﻿
12/12/22
Ben Cahalans request for detail on Jeris Page in Vision

select * 
from jeri.inventory_accounts

select *
from jeri.inventory_balances
limit 10

select *
from jeri.inventory_unit_balances a
join jeri.inventory_accounts b on a.account = b.account
  and b.department = 'NC'
where a.the_year = 2022
  and a.the_month = 12


select d.the_date, c.journal_code, c.journal, a.control, a.amount 
from fin.fact_gl a
join fin.dim_Account b on a.account_key = b.account_key
  and b.account = '123100'
join fin.dim_journal c on a.journal_key = c.journal_key  
join dds.dim_date d on a.date_key = d.date_key
where a.post_status = 'Y'
order by d.the_date desc
limit 100  




select *
from arkona.ext_inpmast
where model = 'malibu'
  and type_n_u  = 'N'
  and status = 'I'



-- Function: jeri.get_inventory_balances_current_month()"
select store_code, department, sum(balance) 
from jeri.inventory_accounts a
join jeri.inventory_balances b on a.account = b.account
  and b.the_year = 2022 -- (select the_year from dds.dim_date where the_date = current_date)
  and b.the_month = 12 -- (select month_of_year from dds.dim_Date where the_date = current_date)
group by store_code, department;  

-- RY1 NC
select *, sum(balance) over () from (
select store_code, department, a.account, sum(balance)  as balance
from jeri.inventory_accounts a
join jeri.inventory_balances b on a.account = b.account
  and b.the_year = 2022 -- (select the_year from dds.dim_date where the_date = current_date)
  and b.the_month = 12 -- (select month_of_year from dds.dim_Date where the_date = current_date)
where store_code = 'RY1'
  and department = 'NC'  
group by store_code, department, a.account) c  
 


  Select c.account,year, beginning_balance::integer, 12, dec_balance12::integer
  select * 
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account
  where a.company_number in ('RY1','RY2','RY8')
    and year = 2022
    and a.department = 'NC'

--- this is giving me way too much weird detail
select sum(amount) 
from (
	select aa.control, account, journal, sum(aa.amount) as amount
	from (
		select a.control, b.account, c.journal, a.amount
		from fin.fact_gl a
		join fin.dim_Account b on a.account_key = b.account_key
		join jeri.inventory_accounts bb on b.account = bb.account
			and bb.department = 'NC'
		join fin.dim_journal c on a.journal_key = c.journal_key 
		join dds.dim_date d on a.date_key = d.date_key
		where a.post_status = 'Y') aa
	group by aa.control, account, journal
	having abs(sum(aa.amount)) > 10000) b

trim it down to just the account	    

	select aa.control, account, sum(aa.amount) as amount
	from (
		select a.control, b.account, c.journal, a.amount
		from fin.fact_gl a
		join fin.dim_Account b on a.account_key = b.account_key
		join jeri.inventory_accounts bb on b.account = bb.account
			and bb.department = 'NC'
		join fin.dim_journal c on a.journal_key = c.journal_key 
		join dds.dim_date d on a.date_key = d.date_key
		  and 
		    case
		      when c.journal_code = 'PVI' then d.the_date > '01/01/2017'
		      else d.the_date > '01/01/2018'
		    end
		where a.post_status = 'Y') aa
	group by aa.control, account
	having abs(sum(aa.amount)) > 10000


-- ok, i sent below to ben, but then thought, maybe i can use accounting if i ensure a PVI entry
-- yep, this is much better, but still, 20 more rows than below
	select aa.control, account, sum(aa.amount) as amount
	from (
		select a.control, b.account, c.journal, a.amount
		from fin.fact_gl a
		join fin.dim_Account b on a.account_key = b.account_key
		join jeri.inventory_accounts bb on b.account = bb.account
			and bb.department = 'NC'
		join fin.dim_journal c on a.journal_key = c.journal_key 
		join dds.dim_date d on a.date_key = d.date_key
		where a.post_status = 'Y'
		  and exists (
		    select 1
		    from fin.fact_gl e
		    join fin.dim_journal f on e.journal_key = f.journal_key
		      and f.journal_code = 'PVI'
		    where e.control = a.control
		      and e.amount > 10000)) aa
	group by aa.control, account
	having abs(sum(aa.amount)) > 10000

---------------------------------------------------------------------
ok, heres the deal
select the_date
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
order by b.the_date
limit 1000  
fact_gl doesnt start until 2011

so, try inpmast for the inventory	
-- looks good
-- this does not include account 127703, which is rentals and ok, i believe

select b.store_code, a.inpmast_stock_number, a.inpmast_vin, a.make, a.model, 
	a.inventory_account, a.inpmast_vehicle_cost, b.description,
	sum(a.inpmast_vehicle_cost) over (partition by a.inventory_account), c.vin
from arkona.ext_inpmast a
join jeri.inventory_accounts b on a.inventory_account = b.account
left join nc.vehicles c on a.inpmast_vin = c.vin
where a.type_n_u = 'N'
  and a.status = 'I'
order by b.store_code, a.inventory_account::integer  

-- clean it up and send it to ben for his approval 

select b.store_code as store, a.inpmast_stock_number as "stock #", a.inpmast_vin as vin, a.make, a.model, 
	a.inpmast_vehicle_cost::integer as cost, 
  case when c.vin is not null then 'here' else 'not here' end as status
from arkona.ext_inpmast a
join jeri.inventory_accounts b on a.inventory_account = b.account
left join nc.vehicles c on a.inpmast_vin = c.vin
where a.type_n_u = 'N'
  and a.status = 'I'
order by b.store_code, a.make, a.model


-- sent to afton 12/13/22
create or replace function jeri.get_current_nc_detail()
returns setof json as
$BODY$
/*
select * from jeri.get_current_nc_detail()
*/
select json_agg(row_to_json(d))
from (
select b.store_code as store, a.inpmast_stock_number as "stock #", a.inpmast_vin as vin, a.make, a.model, 
	a.inpmast_vehicle_cost::integer as cost, 
  case when c.vin is not null then 'here' else 'not here' end as status
from arkona.ext_inpmast a
join jeri.inventory_accounts b on a.inventory_account = b.account
left join nc.vehicles c on a.inpmast_vin = c.vin
where a.type_n_u = 'N'
  and a.status = 'I'
order by b.store_code, a.make, a.model) d;
$BODY$
language sql;


