﻿initial fs lines for categorization

outside services
P2L27  spreadsheet does not include tire credit

it services
P2L26

office supplies
P2L19

other supplies
P2L20  spreadsheet does not include the C accounts, ie 16104C, 16105C, etc

advertising
P2L22

e-commerce advertising
P2L21

miscellaneous
P2L39


-- semi-fixed accounts
drop table if exists semi_fixed_accounts;
create temp table semi_fixed_accounts as
select distinct f.department, f.sub_department, b.line, b.col, b.line_label, d.gl_account, e.description, e.typical_balance
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201903
  and b.page in (3,4)
  and b.line in (39,21,22,20,19,26,27)
--   and b.col = 5
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store in ('ry1')
order by b.line, d.gl_account;
create index on semi_fixed_accounts(gl_account)

select * from semi_fixed_accounts;

-------------------------------------------------------------------------------------------------------------------
--< other supplies L20
-------------------------------------------------------------------------------------------------------------------
select d.*, a.control, a.doc, a.ref, a.amount, e.description,
  sum(amount) over (partition by line)
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201903
join fin.dim_account c on a.account_key = c.account_key
--   and right(trim(c.account), 1) <> 'C' -- just this results in 50,017, thinking on some accounts C means credit on some it means dept
--   and c.typical_balance = 'credit' only 16103C is categorized as Debit
join semi_fixed_accounts d on c.account = d.gl_account
join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and d.line = 20
  and a.amount > 0  -- this gives me a total of 91677, still can't match jeri
order by amount -- line, gl_account


select * from fin.dim_Account where right(Trim(account), 1) = 'C' order by account


-- other supplies
select d.department, d.sub_department, d.gl_account, d.description, e.description, 
  sum(a.amount), count(*), string_agg(distinct a.control, ',') as control
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201903
join fin.dim_account c on a.account_key = c.account_key
join semi_fixed_accounts d on c.account = d.gl_account
join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and d.line = 20
  and a.amount > 0  -- this gives me a total of 91677, still can't match jeri
group by d.department, d.sub_department, d.gl_account, d.description, e.description  


select * from jeri.vendors where vendor_number in ('13600', '12650')

-- other supplies vendors
-- other supplies
select aa.*, bb.vendor_name 
from (
  select a.control, count(*), sum(amount)
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201903
  join fin.dim_account c on a.account_key = c.account_key
  join semi_fixed_accounts d on c.account = d.gl_account
  join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where a.post_status = 'Y'
    and d.line = 20
    and a.amount > 0  -- this gives me a total of 91677, still can't match jeri
  group by a.control) aa
left join jeri.vendors bb on aa.control = bb.vendor_number  
order by count desc

-------------------------------------------------------------------------------------------------------------------
--/> other supplies L20
-------------------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------------------
--< other supplies L19
-------------------------------------------------------------------------------------------------------------------
select d.*, a.control, a.doc, a.ref, a.amount, e.description,
  sum(amount) over (partition by line)
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201903
join fin.dim_account c on a.account_key = c.account_key
join semi_fixed_accounts d on c.account = d.gl_account
join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and d.line = 19
  and a.amount > 0  -- this gives me a total of 91677, still can't match jeri
order by amount -- line, gl_account

select aa.*, bb.vendor_name 
from (
  select a.control, count(*), sum(amount)
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201903
  join fin.dim_account c on a.account_key = c.account_key
  join semi_fixed_accounts d on c.account = d.gl_account
  join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where a.post_status = 'Y'
    and d.line = 19
    and a.amount > 0
  group by a.control) aa
left join jeri.vendors bb on aa.control = bb.vendor_number  
order by count desc

select 2596.86 + 4984.75 + 2407.49

select 100.0*450/10440
-------------------------------------------------------------------------------------------------------------------
--/> other supplies L19
-------------------------------------------------------------------------------------------------------------------