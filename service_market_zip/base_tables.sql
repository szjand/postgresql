﻿drop table if exists tem.service_market_1;
create table tem.service_market_1 as
select a.store_code as store, a.ro, a.open_date, a.close_date, a.final_close_date, a.service_writer_key, a.customer_key, a.vehicle_key, 
  b.service_type_code as service_type, c.payment_type_code as payment_type, sum(flag_hours) as flag_hours,
  sum(labor_sales) as labor_sales
from dds.fact_repair_order a
join dds.dim_service_type b on a.service_type_key = b.service_type_key
  and b.service_type_code <> 'OT'
join dds.dim_payment_type c on a.payment_type_key = c.payment_type_key
  and c.payment_type_code <> 'I'
where a.open_date > current_date - 366
group by a.store_code, a.ro, a.open_date, a.close_date, a.final_close_date, a.service_writer_key, a.customer_key, a.vehicle_key, 
  b.service_type_code, c.payment_type_code;
create unique index on tem.service_market_1(ro,service_type,payment_type);
create index on tem.service_market_1(service_type);
create index on tem.service_market_1(payment_type);
create index on tem.service_market_1(open_date);
create index on tem.service_market_1(service_Writer_key);
create index on tem.service_market_1(customer_key);
create index on tem.service_market_1(vehicle_key);
create index on tem.service_market_1(store);
comment on table tem.service_market_1 is '1 years worth of non internal and non other (service type) ro data, 1 row per ro, service_type, payment_type';
select count(*) from tem.service_market_1;  -- 97646



select * from 
select * from dds.dim_Service_type
select * from dds.fact_repair_order limit 20
select * from dds.dim_customer where customer_key in (1,2)
select * from dds.dim_Customer where full_name like '%invento%'
-- -- customer pay OT is rental, exclude OT
-- select *
-- from dds.fact_repair_order a
-- join dds.dim_service_type b on a.service_type_key = b.service_type_key
--   and b.service_type_code = 'OT'
-- join dds.dim_payment_type c on a.payment_type_key = c.payment_type_key
--   and c.payment_Type_code = 'C'
-- where a.open_date > '11/01/2020'


drop table if exists tem.gm_main_shop_totals;
create table tem.gm_main_shop_totals as
select zip, count(*) as the_count
from ( -- multiple instances of ro in base table due to combinations of service & pay types on individual lines
  select b.zip, a.ro
  from tem.service_market_1 a
  join dds.dim_customer b on a.customer_key = b.customer_key
    and b.customer_key <> 1 -- exclude customer unknown
    and b.full_name not like '%INVENTORY%'
  join dds.dim_Vehicle c on a.vehicle_key = c.vehicle_key  
  where a.service_type not in ('BS','QL','QS')
    and a.store = 'RY1'
  group by b.zip, a.ro) aa
group by zip
order by count(*) desc;



/*
11/19/2020
Greg  9:16 AM
Was just on a call with our SEO company SearchLab. I'd like to get rough service maps done for GM and Honda/Nissan. 
Count of non-internal ROs for the past 365 days by zip code. SearchLab is asking for screenshots of the sales maps. 
I'm thinking we could give them a Vision logon with only access to the maps 
then they can zoom around to understand our territory. Would like them by the end of the month if it works.

jon  9:45 AM
including quick lube and quick service?  (although i don't think i can separate those from honda/nissan)

Greg  9:46 AM
Yes -- all non-internal service ROs
*/

drop table if exists tem.gm_market_area;
create table tem.gm_market_area as
select zip, count(*) as the_count
from ( -- multiple instances of ro in base table due to combinations of service & pay types on individual lines, therefor group on ro level
  select a.store, b.zip, a.ro
  from tem.service_market_1 a
  join dds.dim_customer b on a.customer_key = b.customer_key
    and b.customer_key <> 1 -- exclude customer unknown
    and b.full_name not like '%INVENTORY%'
  join dds.dim_Vehicle c on a.vehicle_key = c.vehicle_key  
  where a.service_type not in ('BS')
    and store = 'RY1'
  group by a.store, b.zip, a.ro) aa
group by zip
order by count(*) desc;
comment on table tem.gm_market_area is 'based on tem.gm_market_area, count of ros by zip code for the gm store';


drop table if exists tem.honda_nissan_market_area;
create table tem.honda_nissan_market_area as
select zip, count(*) as the_count
from ( -- multiple instances of ro in base table due to combinations of service & pay types on individual lines, therefor group on ro level
  select a.store, b.zip, a.ro
  from tem.service_market_1 a
  join dds.dim_customer b on a.customer_key = b.customer_key
    and b.customer_key <> 1 -- exclude customer unknown
    and b.full_name not like '%INVENTORY%'
  join dds.dim_Vehicle c on a.vehicle_key = c.vehicle_key  
  where a.service_type not in ('BS')
    and store = 'RY2'
  group by a.store, b.zip, a.ro) aa
group by zip
order by count(*) desc;

comment on table tem.honda_nissan_market_area is 'based on tem.gm_market_area, count of ros by zip code for the honda nissan store';


select * from tem.gm_market_area;

select * from tem.honda_nissan_market_area;