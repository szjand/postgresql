﻿

/*
jeri:
This balance also includes an adjustment for shop/training time flagged in account 166504A.  
The op codes used are either SHOP or TRAIN.  Balance in July was $13635.50

sent Andrew the detail on shop time
his reply:
After looking at it that number still does not make any sense to me. The range that I expect training and 
shop time to fall is typically between 10,000 on the low end and $15,000 for a max. 
I have Ken manage training enrollment and tech time to be no more than $15,000 each month. 
There is still at or around $16,000 in tech cost expense that is not accounted for in my opinion. 
I was thinking something similar to the UKG issue with Toyota oil change techs being costed 
to the GM store to be honest but I do not have access to that in order to prove my assumption. 

*/
drop table if exists wtf;
create temp table wtf as
select d.year_month, d.the_date, b.store_code, 
  case
    when b.account in ('124700', '124704', '224700', '224704') then 'main_shop'
    when b.account in ('124701','224701', '124723', '224723') then 'main_shop-pdq'
    when b.account in ('124705','124703') then 'bs'
    when b.account in ('124702','124724','224702','224724') then 'main_shop-detail'
  end as department, b.account, a.amount,
  c.journal_code, c.journal, a.control, a.doc, a.ref
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
  and b.current_row
join fin.dim_journal c on a.journal_key = c.journal_key
  and c.journal_code <> 'std'
join dds.dim_date d on a.date_key = d.date_key
  and d.year_month between 202201 and 202207
join fin.dim_fs_account e on b.account = gl_account
  and e.gm_account = '247'
where a.post_status = 'Y'  ;

select * from wtf order by account

select * from fin.dim_fs_account limit 10  
select * from fin.fact_gl limit 10
select * from fin.dim_account where account in ('224724')

select a.* , paid + costed
from (
select year_month, store_code, left(department, 2) as dept, --department,
  SUM(CASE WHEN journal_code IN ('sca','svi','swa') THEN amount ELSE 0 END) AS costed,
  SUM(CASE WHEN journal_code IN ('pay','gli') THEN amount ELSE 0 END) AS paid
FROM wtf
-- where year_month = 202207
GROUP BY year_month, store_code, left(department, 2)) a
order by store_code, year_month -- department

select * from wtf where department is null


--------------------------------------------------------------------------
-- try to pin it down from the financial statement
-- 2016 is close-ish
-- 202207 not close, but honda is right on!?!?!?
-- 08/22 sent jeri an email on this
select e.store, b.line, b.col, a.amount, c.gl_account, d.description, d.department, 
  sum(amount) over (partition by e.store, d.department),
  sum(amount) over (partition by e.store, b.line)
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202207
  and b.page = 16
  and b.line in (39, 30)
join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key  
join fin.dim_account d on c.gl_account = d.account
  and d.current_row
--   and d.account not in ('166504A','167501') -- training time, should be included per jeri
join fin.dim_fs_org e on a.fs_org_key = e.fs_org_key
order by store, b.line, d.department, c.gl_account

select * 
from fin.dim_fs_Account
where gm_account = '247'

select * 
from fin.dim_account
where account in ('166524','166503','166504','167500','266524','266503','266500')
  and current_row
order by account

fncl statement from GM
page 6
select 1112152 - 363922  -- L31
select 1132252 - 379218 -- L34
damnit it does not include acl in the totals
-----------------------------------------------------------------------------
now the paid/costed 
try to mimic what was done in scotest
all the functions, acl_get_Department, acl_get_people, etc based on table acl_v1
what populates that
sql/advantage/DDS/Accounting/financial statement/scratchpad_2.sql
all pulled from accounting

drop table if exists step_1;
create temp table step_1 as
select b.store_code, c.the_date, a.control, a.amount, b.account, d.journal_code,
  case 
		when b.department_code = 'GN' then
			case
			  when account in ('124700','124704') then 'SD'
			  when account in ('124701','124723') then 'PDQ'
			  when account in ('124702','124724') then 'RE'
			  when account in ('124703','124705') then 'BS'
			end
		else b.department_code
  end
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
join dds.dim_date c on a.date_key = c.date_key
  and c.year_month = 202207
join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code <> 'std'
join fin.dim_fs_account e on b.account = e.gl_account
  and e.gm_account = '247'
where a.post_status = 'Y'  

-------------------------------------------------------------
--< these are me fumbling around, not knowing wtf i am doing
-------------------------------------------------------------
select *, sum(amount) over (partition by store_code, department_code)
from (
select store_code, journal_code, department_code, account, sum(amount) as amount
from step_1
group by store_code, journal_code, account, department_code) a
order by store_code, department_code, journal_code, account

select costed + paid
from (
select sum(amount) filter (where journal_code in ('swa','svi','sca')) as costed,
  sum(amount) filter (where journal_code in ('pay','gli')) as paid
from step_1
where department_code <> 'BS'
  and store_code = 'RY1') a

select distinct account, department_code from step_1 order by account

-- wtf
select distinct b.employee_number, tech_name, sum(amount)
from step_1 a
left join dds.dim_tech b on a.control = b.employee_number
where a.department_code <> 'BS'
  and a.store_code = 'RY1'
group by b.employee_number, tech_name

-------------------------------------------------------------
--/> these are me fumbling around, not knowing wtf i am doing
-------------------------------------------------------------
-- shop time
select flag_department_code, sum(cost)
from (
select a.store_code, b.opcode, a.ro, a.flag_hours, d.tech_number, d.tech_name, d.flag_department_code, d.labor_cost, c.service_type_code,
  a.flag_hours * d.labor_cost as cost
from dds.fact_repair_order a
join dds.dim_opcode b on a.opcode_key = b.opcode_key
  and b.opcode in ('shop','train','st','tr')
join dds.dim_service_type c on a.service_type_key = c.service_type_key
  and service_type_code = 'OT'
join dds.dim_tech d on a.tech_key = d.tech_key
  and flag_department_code = 'MR'
join dds.dim_date e on a.flag_date = e.the_date
  and e.year_month = 202207) x
group by flag_department_code  

select a.* 
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
  and b.account = '166504A'
join dds.dim_date c on a.date_key = c.date_key
  and c.year_month = 202207

-- 08/29/22 this is what i sent to Andrew
-- this matches 166504 for shop/training time
select d.tech_name, d.employee_number, d.labor_cost, sum(flag_hours) as flag_hours, 
  round(sum(a.flag_hours * d.labor_cost), 2) as cost
from dds.fact_repair_order a
join dds.dim_opcode b on a.opcode_key = b.opcode_key
  and b.opcode in ('shop','train','st','tr')
join dds.dim_service_type c on a.service_type_key = c.service_type_key
  and service_type_code = 'OT'
join dds.dim_tech d on a.tech_key = d.tech_key
  and flag_department_code = 'MR'
join dds.dim_date e on a.flag_date = e.the_date
  and e.year_month = 202207  
group by d.tech_name, d.employee_number, d.flag_department_code, d.labor_cost  

-- try to expose andrew's assumption from above

from the advantage side, i can derive paid and costed by journals

select distinct journal_code from step_1
-- how much is pot & gje
-- not much
-- but this may be enough that it is what andre is concerned abount
select *, sum(amount) over ()
from step_1
where department_code <> 'BS'
  and store_code = 'RY1'
  and journal_code in ('pot','gje')

-- any bad employee numbers?
-- nope
select control
from step_1
where department_code <> 'BS'
  and store_code = 'RY1'
  and journal_code not in ('pot','gje')  
  and left(control,1) <> '1'

-- try to match the statement ?
-- first match up dept and account
-- looks good
select journal_code, department_code, account
from step_1
where department_code <> 'BS'
  and store_code = 'RY1'
  and journal_code not in ('pot','gje') 
group by journal_code, department_code, account
order by department_code, journal_code

-- add totals
select journal_code, department_code, account, sum(amount) as amount
from step_1
where department_code <> 'BS'
  and store_code = 'RY1'
  and journal_code not in ('pot','gje') 
group by journal_code, department_code, account
order by department_code, journal_code

-- and the total: 4043
-- but when i add pot & gje back in, the total is 2174, that + 13635 from shop/training = 15808, statement is 15810
select sum(amount)
from (
select journal_code, department_code, account, sum(amount) as amount
from step_1
where department_code <> 'BS'
  and store_code = 'RY1'
--   and journal_code not in ('pot','gje') 
group by journal_code, department_code, account
order by department_code, journal_code) a

-- total by dept
-- and the total: 4043
select department_code, sum(amount)
from (
select journal_code, department_code, account, sum(amount) as amount
from step_1
where department_code <> 'BS'
  and store_code = 'RY1'
  and journal_code not in ('pot','gje') 
group by journal_code, department_code, account) a
group by department_code


-- not sure what this tells me
select journal_code, department_code, sum(amount)
from (
select control, journal_code, department_code, sum(amount) as amount
from step_1
where department_code <> 'BS'
  and store_code = 'RY1'
  and journal_code not in ('pot','gje')  
group by control, journal_code, department_code) a
group by journal_code, department_code
order by department_code, journal_code, control