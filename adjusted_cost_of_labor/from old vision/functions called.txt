execute procedure acl_Get_Dates()
execute procedure acl_Get_Market( :yearMonth )
execute procedure acl_Get_Store( :yearMonth, :storeCode )
execute procedure acl_Get_Department( :yearMonth, :storeCode, :department )
execute procedure acl_Get_People( :yearMonth, :storeCode, :department )
