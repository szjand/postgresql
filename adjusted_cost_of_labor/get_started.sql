﻿-- 201812 mech acl -20,091
select a.amount, b.line, b.line_label, c.gl_account
-- select sum(amount)
from fin.fact_Fs a
join fin.dim_Fs b on a.fs_key = b.fs_key
join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where year_month = 201812
  and page = 16
  and line = 30
--   and line between 20 and 34
  and col = 3
  and store = 'ry1'

165504 is the big one
gm acct 665 offset by 247a

select *
from fin.dim_fs_account
where gm_account like '247%'
order by gl_account

select a.*, b.account, c.the_date, d.journal_code, e.description
-- select journal_code, control, account, sum(amount)
select journal_code, account, sum(amount)
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
  and b.account like '1247%' -- ('124704','124703','124723','124724')
join dds.dim_date c on a.date_key =  c.date_key 
  and c.year_month = 201812
join fin.dim_journal d on a.journal_key = d.journal_key  
join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y' 
group by journal_code, account
order by control, account


