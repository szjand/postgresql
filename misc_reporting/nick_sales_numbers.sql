﻿/*
Hey Jon

Need your help boss!

Wondering if you could get me some data?

Total year for 
2018
2019
2020

How many of each

Tahoe
Suburban
Yukon
Yukon XL
Escalade 
Escalade ESV 

Cool?

Nick Shirek
*/
drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select year_month, store, page, line, line_label, gl_account, control, sum(unit_count) as unit_count
from (
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201801 and 202012 --------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select distinct f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month between 201801 and 202012 -------------------------------------------------------------------
      and b.page between 5 and 15 and b.line between 1 and 45
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y') h
where line_label in ('ESCALADE','ESCALADE ESV','SUBURBAN','TAHOE','YUKON','YUKON XL')  
group by year_month, store, page, line, line_label, gl_account, control, year_month
order by store, page, line;


select *, sum(unit_count) over (partition by year, line_label) 
from (
select left(year_month::text, 4)::integer as year, year_month, line_label, gl_account,sum(unit_count) as unit_count
from step_1 
where store = 'ry1' 
group by year_month, line_label, gl_account
order by line_label,year_month) x


select line_label as model,
  sum(unit_count) filter (where year = 2018) as "2018",
  sum(unit_count) filter (where year = 2019) as "2019",
  sum(unit_count) filter (where year = 2020) as "2020"
from (
  select left(year_month::text, 4)::integer as year, year_month, line_label, gl_account,sum(unit_count) as unit_count
  from step_1 
  where store = 'ry1' 
  group by year_month, line_label, gl_account
  order by line_label,year_month) x
group by line_label