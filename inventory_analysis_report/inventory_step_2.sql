﻿sales data with no inventory record for the vehicle
fucked in the foundation


-- yikes nearly 1/3 with no inventory record  1011 rows
select * 
from iar.sales_data_v1 a
left join (
	select b.stock_number, b.avail_category
	from (
		select stock_number, max(the_date) as the_date
		from iar.inventory_detail
		group by stock_number) a
	left join iar.inventory_detail b on a.stock_number = b.stock_number
		and a.the_date = b.the_date) c on a.stock_number = c.stock_number
-- where c.avail_category is null	
order by a.delivery_date	

-- 1/25/24 put together a query for afton to generate sales data that ias missing inventory data for her to dig intlo

select *
from iar.sales_data_v1 a
left join (
	select b.stock_number, b.avail_category
	from (
		select stock_number, max(the_date) as the_date
		from iar.inventory_detail
		group by stock_number) a
	left join iar.inventory_detail b on a.stock_number = b.stock_number
		and a.the_date = b.the_date) c on a.stock_number = c.stock_number
where a.delivery_date between '06/01/2023' and '06/30/2023'
--   and c.avail_category is null
order by right(a.stock_number, 1), a.stock_number



-- accounting
-- i don't think this works (simulating a loop by joining on a dataset of dates)
-- would rather do it in a python loop, feeding the query a single date at a time
select aa.the_date, bb.*
from dds.dim_date aa
join lateral(
		select d.the_date, a.control, sum(a.amount)::integer as inventory_dollars
		from fin.fact_gl a
		join fin.dim_account b on a.account_key = b.account_key
			and b.account not in ('2400P','2401P','224002','224102','124001','124101') -- exclude pack accounts
		join jeri.inventory_accounts c on b.account = c.account
			and c.department = 'UC'
		join dds.dim_date	d on a.date_key = d.date_key
			and d.the_date < aa.the_date + 1
			and d.the_date > (aa.the_date - '3 years'::interval)::date
		where a.post_status = 'Y'  
			and a.control <> '21'
		group by d.the_date, a.control
		having sum(a.amount) > 0) bb on aa.the_date = bb.the_date
where aa.the_date between '01/20/2024' and current_date - 1		

drop table if exists iar.inventory_accounting_1 cascade;
create table iar.inventory_accounting_1 (
  the_date date not null,
  control citext not null,
--   inventory_account citext, 
  inventory_dollars integer not null,
  primary key (the_date,control));
comment on table iar.inventory_accounting_1 is 'inventory dollars for an owned vehicle for each day it is owned';


		select _the_date, a.control, sum(a.amount)::integer as inventory_dollars
		from fin.fact_gl a
		join fin.dim_account b on a.account_key = b.account_key
			and b.account not in ('2400P','2401P','224002','224102','124001','124101') -- exclude pack accounts
		join jeri.inventory_accounts c on b.account = c.account
			and c.department = 'UC'
		join dds.dim_date	d on a.date_key = d.date_key
			and d.the_date < _the_date + 1
			and d.the_date > (_the_date - '3 years'::interval)::date
		where a.post_status = 'Y'  
			and a.control <> '21'
		group by a.control
		having sum(a.amount) > 0

create or replace function iar.update_inventory_accounting_1(_the_date date)		
  returns void as 
$BODY$
/*

*/
begin
  insert into iar.inventory_accounting_1
	select _the_date, a.control, sum(a.amount)::integer as inventory_dollars
	from fin.fact_gl a
	join fin.dim_account b on a.account_key = b.account_key
		and b.account not in ('2400P','2401P','224002','224102','124001','124101') -- exclude pack accounts
	join jeri.inventory_accounts c on b.account = c.account
		and c.department = 'UC'
	join dds.dim_date	d on a.date_key = d.date_key
		and d.the_date < _the_date + 1
		and d.the_date > (_the_date - '3 years'::interval)::date
	where a.post_status = 'Y'  
		and a.control <> '21'
	group by a.control
	having sum(a.amount) > 0;  
end
$BODY$
language plpgsql;	

select count(*) from iar.inventory_accounting_1


-- of the 141 vehicles in iar.sales_data_v1 in 2024, 45 have no record in accounting
-- to be more precise (in the case of intra market ws) there is no single day in wich there is an outstanding balnce
-- in inventory accounts
select *
from iar.sales_data_v1 a
left join (
	select b.control, 'avail_category'
	from (
		select control, max(the_date) as the_date
		from iar.inventory_accounting_1
		group by control) a
	left join iar.inventory_accounting_1 b on a.control = b.control
		and a.the_date = b.the_date) c on a.stock_number = c.control
where a.delivery_date > '12/31/2023'		
--   and control is null
order by right(stock_number, 1), a.delivery_date	

-- select stock_number from (
select *
from iar.sales_data_v1 a
left join (
	select b.control, 'avail_category', a.inventory_account
	from (
		select control, inventory_account, max(the_date) as the_date
		from iar.inventory_accounting_1
		group by control,inventory_account) a
	left join iar.inventory_accounting_1 b on a.control = b.control
		and a.the_date = b.the_date) c on a.stock_number = c.control
where a.delivery_date > '12/31/2023'
  and stock_number in ('G47989B','G48172A','G48523AB','G48750X','G49376TA')
-- ) x group by stock_number having count(*) > 1
order by stock_number  
order by right(stock_number, 1), a.delivery_date	

G48523AB: 2 del dates

in the case of L/H/T/G: journal = PVU
               A/B/C/D: journal = VSN, VSU

--================================================================================================================
FUNCTION iar.update_inventory_detail(date)  
  in the first section (insert into iar.inventory_detail ... ) 
  at the very end, only require c.stocknumber to be not null

-- remove all the jhanuary rows, then re add them using then new version of iar.update.inventory_detail(date)
select * from iar.inventory_detail where the_date >= '01/01/2024'       

delete from iar.inventory_detail where the_date >= '01/01/2024'  -- 8464  

select * from iar.inventory_detail where the_date >= '01/01/2024' 

-- after having run the updated versions of iar.update.inventory_detail(date)
-- no dups
select stock_number, the_date from iar.inventory_detail where the_date >= '01/01/2024' group by stock_number, the_date having count(*) > 1

-- 332 rows of i don't know why
select * from iar.inventory_detail where the_date >= '01/01/2024' and store = 'XXX'

-- no rows here, i thiunk that is a :)
select * from iar.inventory_detail where the_date >= '01/01/2024' and avail_category is null
