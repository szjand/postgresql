﻿drop table if exists stock_numbers cascade;
create temp table stock_numbers as
select control
from iar.inventory_accounting_1
group by control;


-- drop table if exists iar.sale_accounts cascade;
-- create table iar.sale_accounts as
-- select d.gl_account as account, b.line, e.description, 
--   case
--     when b.line < 7 then 'Retail'
--     when b.line > 7 then 'Wholesale'
--     else 'XXX'
--   end as sale_type
-- from fin.fact_fs a
-- inner join fin.dim_fs b on a.fs_key = b.fs_key
--   and b.year_month = 202312
--   and b.page = 16
--   and b.line between 1 and 14
-- inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
-- inner join fin.dim_account e on d.gl_account = e.account
--   and e.account_type_code = '4'
--   and e.current_row
-- inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
-- order by d.gl_account;
-- alter table iar.sale_accounts add primary key (account);


drop table if exists iar.sale_accounts cascade;
create table iar.sale_accounts as
select a.*, b.debit_offset_acct, c.description as deb_descr, b.cred_offset_acct, d.description as cred_descr
from (
	select d.gl_account as account, b.line, e.description, 
		case
			when b.line < 7 then 'Retail'
			when b.line > 7 then 'Wholesale'
			else 'XXX'
		end as sale_type
	from fin.fact_fs a
	inner join fin.dim_fs b on a.fs_key = b.fs_key
		and b.year_month = 202312
		and b.page = 16
		and b.line between 1 and 14
	inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
	inner join fin.dim_account e on d.gl_account = e.account
		and e.account_type_code = '4'
		and e.current_row
	inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) a
left join arkona.ext_glpmast b on a.account = b.account_number
  and b.year = 2024
  and b.company_number in ('ry1','ry2','ry8')
left join fin.dim_account c on b.debit_offset_acct = c.account 
  and c.current_row 
left join fin.dim_account d on b.cred_offset_acct = d.account 
  and c.current_row 
order by a.account;
alter table iar.sale_accounts add primary key (account);
comment on table iar.sale_accounts is 'the used vehicle sale accounts from Dealertrack general ledger, includes the debit (COGS) and credit (inventory) offset accounts as well';
select * from iar.sale_accounts
  	
-- -- no i don't want to limit sales to what is in accounting
-- select a.control, c.account, e.the_date, sum(b.amount), c.description
-- from stock_numbers a
-- join fin.fact_gl b on a.control = b.control
-- join fin.dim_account c on b.account_key = c.account_key
--   and c.current_row
-- join sale_accounts d on c.account = d.gl_account
-- join dds.dim_date e on b.date_key = e.date_key
--   and e.the_date between '01/01/2023' and current_date
-- where b.post_status = 'Y'
-- group by a.control, c.account, e.the_date, c.description

drop table if exists iar.tmp_sales cascade;
create table iar.tmp_sales as
select b.control, c.account, e.the_date, sum(b.amount) as amount, c.description, d.sale_type
from fin.fact_gl b 
join fin.dim_account c on b.account_key = c.account_key
  and c.current_row
join iar.sale_accounts d on c.account = d.account
join dds.dim_date e on b.date_key = e.date_key
  and e.the_date between '01/01/2023' and current_date - 1
where b.post_status = 'Y'
group by b.control, c.account, e.the_date, c.description, d.sale_type
order by e.the_date;

-- only 3 vehicles with dups
select *
-- delete 
from iar.tmp_sales
where (
	control = 'G46612P' and account = '145000'
	or																																																																																																												
	control = 'H16019B' and account = '244600'
	or
	control = 'T10261PB' and account = '4385');

	
create unique index on iar.tmp_sales(control, the_date);
comment on table iar.tmp_sales is 'first shot at a scrape of used vehicle sales transactions from fact_gl for the past year';

-- 5134 rows
select * from iar.tmp_sales

-- only 3 vehicles with dups
select control, the_date
iar.tmp_salesgroup by control, the_date
having count(*) > 1

select * from iar.tmp_sales where control in ('G46612P','H16019B','T10261PB')

select *
-- delete 
from iar.tmp_sales
where (
	control = 'G46612P' and account = '145000'
	or																																																																																																												
	control = 'H16019B' and account = '244600'
	or
	control = 'T10261PB' and account = '4385');

-- records with a 0 amount
select * from iar.tmp_sales where amount = 0


select * 
from iar.tmp_sales a
where control in (
  select control
  from iar.tmp_sales
  where amount = 0)
order by a.control  



select record_key, record_status, bopmast_search_name, row_from_date, sale_type 
from arkona.xfm_bopmast where bopmast_stock_number = 'T10261PB' 
order by row_from_date


-- these are all deals that were sold, then the deals were later opened and reclosed
-- same customer, same deal id
select *
-- delete
from iar.tmp_sales
where (
  control = 'G46186AA' and the_date = '06/05/2023' or
  control = 'G46621A' and the_date = '03/04/2023' or
  control = 'G47464P' and the_date = '07/05/2023' or
  control = 'G47544B' and the_date = '09/11/2023' or
  control = 'G47777X' and the_date = '2023-08-11' or
  control = 'G47906A' and the_date = '2023-09-11' or
  control = 'G48218A' and the_date = '2023-12-19' or
  control = 'G48283A' and the_date = '2023-11-02' or
  control = 'G48345H' and the_date = '2023-11-02' or
  control = 'G48628X' and the_date = '2024-01-04' or
  control = 'H16377A' and the_date = '2023-05-12' or
  control = 'H17150L' and the_date = '2023-11-30' or
  control = 'T10636G' and the_date = '2023-06-12' or
  control = 'T10669A' and the_date = '2023-06-02')




select record_key, record_status, bopmast_search_name, row_from_date, sale_type 
-- select * 
from arkona.xfm_bopmast where bopmast_stock_number = 'G48628X' 
order by row_from_date


-- remaining balance 0 in sale account
G46265A declared a total loss xfred to Progressive, 

G48172A retailed twice and unwound twice
delete from iar.tmp_sales where control = 'G48172A';

T10628GA is an actual unwind
do i just delete the reversal of the sale from the first sale
not sure what this looks like
4385          VSU   5/25/23   T10628GA   T10628GA     -15999.00 
4385          VSU   6/02/23   T10628GA   T10628GA      15999.00 
4385          VSU   6/02/23   T10628GA   T10628GA     -15999.00







--< 2/1/24 from afton --------------------------------------------------------------------------
iar.tmp_inventory_accounting_from_sales

iar.tmp_inventory_timeline

iar.tmp_vehicles_2 -- all vins minus 200

-- the vast majority of these are IMWS
select a.*, c.sale_type
from iar.tmp_vehicles_2 a
join (
  select vin
  from iar.tmp_vehicles_2
  group by vin
  having count(*) > 1) b on a.vin = b.vin
join iar.sale_accounts c on a.sale_account = c.account  
 order by a.vin 

-- returns 0 rows, ie, no unwinds !?!? but afton did say she set unwinds aside
select a.*
from iar.tmp_vehicles_2 a
join (
	select control, vin
	from iar.tmp_vehicles_2
	group by control, vin
	having count(*) > 1) b on a.control = b.control
  and a.vin = b.vin
order by vin, control  

-- a known unwind, not in this table
select * from iar.tmp_vehicles_2 where control = 'T10628GA'

select * from iar.tmp_sales where control = 'T10628GA'


select a.*, c.sale_type
from iar.tmp_vehicles_2 a
join iar.sale_accounts c on a.sale_account = c.account  


--/> 2/1/24 from afton --------------------------------------------------------------------------


select * from arkona.ext_bopmast limit 3
-- about 400 wholesale deals with no record in bopmast
select a.*, b.record_key, b.buyer_number, b.bopmast_search_name, aa.sale_type
from iar.tmp_vehicles_2 a
join iar.sale_accounts aa on a.sale_account = aa.account  
left join arkona.ext_bopmast b on a.control = b.bopmast_stock_number
  and a.vin = b.bopmast_vin

select * 
from ads.ext_vehicle_sales


drop table if exists no_deals cascade;
create temp table no_deals as
select a.* -- , b.record_key, b.buyer_number, b.bopmast_search_name, aa.sale_type
from iar.tmp_vehicles_2 a
join iar.sale_accounts aa on a.sale_account = aa.account  
left join arkona.ext_bopmast b on a.control = b.bopmast_stock_number
  and a.vin = b.bopmast_vin
where b.record_key is null 
order by a.control;


select c.*, a.soldts::date, a.soldamount, a.typ, a.status, d.fullname
from ads.ext_vehicle_sales a
join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
join no_deals c on b.stocknumber = c.control
left join ads.ext_organizations d on a.soldto = d.partyid
order by d.fullname

select * from ads.ext_organizations where partyid = 'B6183892-C79D-4489-A58C-B526DF948B06'

select * from ads.ext_vehicle_sales limit 100


-- multiple instances of control being sold
select a.* 
from iar.tmp_sales a
join (
  select control
  from iar.tmp_sales
  group by control
  having count(*) > 1) b on a.control = b.control
order by a.control  