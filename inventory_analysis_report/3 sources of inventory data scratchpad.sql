﻿
-- G46048A

-- acct
do $$
declare
  _the_date date := '03/13/2023';
begin
  drop table if exists iar.wtf cascade;
  create table iar.wtf as
-- 	select _the_date as the_date, inpmast_stock_number as stock_number, inpmast_vin as vin, inventory_dollars, sale_account, 
-- 		inv_account, vehicleinventoryitemid,
-- 		case  -- use stock_number to establish the store
-- 			when left(inpmast_stock_number, 1) = 'G' then 'RY1'
-- 			when left(inpmast_stock_number, 1) = 'H' then 'RY2'
-- 			when left(inpmast_stock_number, 1) = 'T' then 'RY8'
-- 			else 'XXX'
-- 		end as store	
select a.control as acct, b.inpmast_stock_number as inpmast, c.stocknumber as tool, b.sale_account		
	from (
		select a.control, sum(a.amount)::integer as inventory_dollars
		from fin.fact_gl a
		join fin.dim_account b on a.account_key = b.account_key
			and b.account not in ('2400P','2401P','224002','224102','124001','124101') -- exclude pack accounts
		join jeri.inventory_accounts c on b.account = c.account
			and c.department = 'UC'
		join dds.dim_date	d on a.date_key = d.date_key
			and d.the_date < _the_date + 1
			and d.the_date > (_the_date - '3 years'::interval)::date
		where a.post_status = 'Y'  
			and a.control <> '21'
		group by a.control
		having sum(a.amount) > 0) a
	full outer join (
		select distinct inpmast_stock_number, inpmast_vin, inpmast_sale_account as sale_account, inventory_account as inv_account
		from arkona.xfm_inpmast  
		where _the_date between row_From_date and row_thru_date 
			and status = 'I'
			and (
				type_n_u = 'U'
				or (left(inpmast_stock_number, 1) = 'H' and right(inpmast_stock_number, 1) = 'R'))
			and 
			  case
			    when left(inpmast_stock_number, 1) = 'T' then inpmast_company_number = 'RY8'
			    else true
			  end) b on a.control = b.inpmast_stock_number
	full outer join (			
		select d.stocknumber, e.vin, d.vehicleinventoryitemid
		from ads.ext_vehicle_inventory_items d
		join ads.ext_vehicle_items e on d.vehicleitemid = e.vehicleitemid
		where _the_date between d.fromts::date and d.thruts::date)  c on coalesce(a.control, b.inpmast_stock_number) =  c.stocknumber
			and b.inpmast_vin = c.vin
	where a.control is not null
		and b.inpmast_stock_number is not null
		and c.stocknumber is not null
		and b.sale_account is not null;
end $$;		


-- inventory from accounting only
FUNCTION iar.update_inventory_accounting_1(date)
which populates iar.inventory_accounting_1


--   insert into iar.inventory_accounting_1
	select /*_the_date, */a.control, sum(a.amount)::integer as inventory_dollars
	from fin.fact_gl a
	join fin.dim_account b on a.account_key = b.account_key
		and b.account not in ('2400P','2401P','224002','224102','124001','124101') -- exclude pack accounts
	join jeri.inventory_accounts c on b.account = c.account
		and c.department = 'UC'
	join dds.dim_date	d on a.date_key = d.date_key
		and d.the_date < '01/26/2024' -- _the_date + 1
		and d.the_date > '01/25/2023' -- (_the_date - '3 years'::interval)::date
	where a.post_status = 'Y'  
		and a.control <> '21'
		AND A.CONTROL = 'G46640A'
	group by a.control
	having sum(a.amount) > 0; 

select * from iar.inventory_accounting_1		

select * from iar.inventory_detail where the_date = current_date - 1

select * from jeri.inventory_accounts where department = 'UC'