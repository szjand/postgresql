﻿drop table if exists iar.sale_accounts cascade;
create table iar.sale_accounts as
select a.*, b.debit_offset_acct, c.description as deb_descr, b.cred_offset_acct, d.description as cred_descr
from (
	select d.gl_account as account, b.line, e.description, 
		case
			when b.line < 7 then 'Retail'
			when b.line > 7 then 'Wholesale'
			else 'XXX'
		end as sale_type
	from fin.fact_fs a
	inner join fin.dim_fs b on a.fs_key = b.fs_key
		and b.year_month = 202312
		and b.page = 16
		and b.line between 1 and 14
	inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
	inner join fin.dim_account e on d.gl_account = e.account
		and e.account_type_code = '4'
		and e.current_row
	inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) a
left join arkona.ext_glpmast b on a.account = b.account_number
  and b.year = 2024
  and b.company_number in ('ry1','ry2','ry8')
left join fin.dim_account c on b.debit_offset_acct = c.account 
  and c.current_row 
left join fin.dim_account d on b.cred_offset_acct = d.account 
  and c.current_row 
order by a.account;
alter table iar.sale_accounts add primary key (account);
comment on table iar.sale_accounts is 'the used vehicle sale accounts from Dealertrack general ledger, includes the debit (COGS) and credit (inventory) offset accounts as well,';

select * from iar.sale_accounts;
  	

drop table if exists iar.tmp_sales cascade;
create table iar.tmp_sales as
select b.control, c.account, e.the_date, sum(b.amount) as amount, c.description, d.sale_type
from fin.fact_gl b 
join fin.dim_account c on b.account_key = c.account_key
  and c.current_row
join iar.sale_accounts d on c.account = d.account
join dds.dim_date e on b.date_key = e.date_key
  and e.the_date between '01/01/2023' and current_date - 1
where b.post_status = 'Y'
group by b.control, c.account, e.the_date, c.description, d.sale_type
order by e.the_date;

-- only 3 vehicles with dups
select *
-- delete 
from iar.tmp_sales
where (
	control = 'G46612P' and account = '145000'
	or																																																																																																												
	control = 'H16019B' and account = '244600'
	or
	control = 'T10261PB' and account = '4385');

	
create unique index on iar.tmp_sales(control, the_date);
comment on table iar.tmp_sales is 'first shot at a scrape of used vehicle sales transactions from fact_gl for the past year, turns out
  the purpose of this table is to house the basis for backfill sales data for the Inventory Analysis project.  it is used to generate
  a dataset of all controls with a sale account transaction between 01/01/2023 and now';

-- 5134 rows
select * from iar.tmp_sales

-- only 3 vehicles with dups
select control, the_date
iar.tmp_salesgroup by control, the_date
having count(*) > 1

select * from iar.tmp_sales where control in ('G46612P','H16019B','T10261PB')

select *
-- delete 
from iar.tmp_sales
where (
	control = 'G46612P' and account = '145000'
	or																																																																																																												
	control = 'H16019B' and account = '244600'
	or
	control = 'T10261PB' and account = '4385');

/*	
-- i am concerned that in iar.tmp_sales, i aggregated, did i lose any control numbers?
drop table if exists iar.tmp_sales_2 cascade;
create table iar.tmp_sales_2 as
select b.control, c.account, e.the_date, b.amount, c.description, d.sale_type
from fin.fact_gl b 
join fin.dim_account c on b.account_key = c.account_key
  and c.current_row
join iar.sale_accounts d on c.account = d.account
join dds.dim_date e on b.date_key = e.date_key
  and e.the_date between '01/01/2023' and current_date - 1
where b.post_status = 'Y';

-- only 3 vehicles with dups
select *
-- delete 
from iar.tmp_sales_2
where (
	control = 'G46612P' and account = '145000'
	or																																																																																																												
	control = 'H16019B' and account = '244600'
	or
	control = 'T10261PB' and account = '4385');

-- no control numbers lost
-- 5236 rows
select * 
from iar.tmp_sales_2 a
where not exists (
  select 1
  from iar.tmp_sales
  where control = a.control)

also looks good against iar.control_vins:

select * from iar.control_vins a
where not exists (
  select 1
  from temp_sales
  where control = a.control)

select control
from iar.vehicle_sale_gl_transactions a
where not exists (
  select 1
  from iar.control_vins
  where control = a.control)  
  
 */ 


-- just need a list of all the controls
-- 5067
create temp table temp_sales as
select distinct control from iar.tmp_sales

-- create index on iar.control_vins(control);

-- 5248 rows, with distinct 5241
drop table if exists iar.vehicle_sale_gl_transactions cascade;
create table iar.vehicle_sale_gl_transactions as
select b.control, g.vin, e.the_date, c.account, c.description, f.journal_code, f.journal, b.amount, b.doc as document, b.ref as reference, d.sale_type
from temp_sales a
join fin.fact_gl b on a.control = b.control
join fin.dim_account c on b.account_key = c.account_key
  and c.current_row
join iar.sale_accounts d on c.account = d.account
join dds.dim_date e on b.date_key = e.date_key
--   and e.the_date between '01/01/2023' and current_date - 1  i also need sales transactions on these controls from before 1/1/23, if it had a trx in 23 , i need all of its trxs
join fin.dim_journal f on b.journal_key = f.journal_key  
join iar.control_vins g on a.control = g.control
where b.post_status = 'Y';
comment on table iar.vehicle_sale_gl_transactions is 'the base sale transaction table for the Inventory Analysis project';

select a.*, c.vin
from iar.vehicle_sale_gl_transactions a
join ( -- 34 stock #s that violate control, date PK
	select control, the_date
	from iar.vehicle_sale_gl_transactions
	group by  control, the_date
	having count(*) > 1) b on a.control = b.control
		and a.the_date = b.the_date
join iar.control_vins c on a.control = c.control		
order by control

select *
-- delete 
from iar.vehicle_sale_gl_transactions
where (
  control = 'G44734M' and abs(amount) = 22499.00 or
  control = 'G46186AA' and the_date = '06/05/2023' or
  control = 'G46621A' and the_date = '03/04/2023' or
  control = 'G47464P' and the_date = '07/05/2023' or
  control = 'G47544B' and the_date = '09/11/2023' or
  control = 'G47777X' and the_date = '2023-08-11' or
  control = 'G47906A' and the_date = '2023-09-11' or
  control = 'G48218A' and the_date = '2023-12-19' or
  control = 'G48283A' and the_date = '2023-11-02' or
  control = 'G48345H' and the_date = '2023-11-02' or
  control = 'G48628X' and the_date = '2024-01-04' or
  control = 'H16377A' and the_date = '2023-05-12' or
  control = 'H17150L' and the_date = '2023-11-30' or
  control = 'T10636G' and the_date = '2023-06-12' or
  control = 'T10669A' and the_date = '2023-06-02' OR
  control = 'G46612P' and account = '145000' OR
  control = 'G47099P' and abs(amount) = 1000 OR
  control = 'G47371A' and the_date <> '06/30/2023' OR
  control = 'G47754X' and the_date = '2023-07-14' OR
  control = 'G48797X' and the_date = '2024-01-06' OR
  control = 'G48977A' and abs(amount) = 36655.00 OR
  control = 'G49139T' and abs(amount) = 165.00 OR
  control = 'H16019B' and account = '244600' OR
  control = 'H16278A' and journal_code = 'EFT' OR
  control = 'H16352GA' and abs(amount) = 7705.23 OR
  control = 'H17234LA' and abs(amount) = 5481.35 OR
  control = 'T10261PB' and account = '4385' OR
  control = 'T10628GA' and the_date = '2023-06-02' OR
  control = 'T11171A' and the_date = '2024-01-12');

delete from iar.vehicle_sale_gl_transactions where control = 'H17337A';
insert into iar.vehicle_sale_gl_transactions values('H17337A','2HKRW2H85MH630245','2024-01-29','245200','SLS U/T-WHOLESALE','VSU','Vehicle Sales - Used',-27330.08,'H17337A','Wholesale');

delete from iar.vehicle_sale_gl_transactions where control = 'G44734M';
insert into iar.vehicle_sale_gl_transactions values('G44734M','2GNAXTEV9M6138873','2023-05-03','145200','SLS USED TRK WHSL','VSU','Vehicle Sales - Used',-22499.00,'G44734M','Wholesale');

delete from iar.vehicle_sale_gl_transactions where control = 'G47102A';
insert into iar.vehicle_sale_gl_transactions values('G47102A','5FNRL6H82MB021984','2023-06-29','145200','SLS USED TRK WHSL','VSU','Vehicle Sales - Used',-37017.77,'G47102A','Wholesale');

delete from iar.vehicle_sale_gl_transactions where control = 'G43567A';
insert into iar.vehicle_sale_gl_transactions values('G43567A','1N6AA1ED1LN502510','2023-01-06','145200','SLS USED TRK WHSL','VSU','Vehicle Sales - Used',-44999.00,'G43567A','Wholesale');

delete from iar.vehicle_sale_gl_transactions where control = 'G47501A';
insert into iar.vehicle_sale_gl_transactions values('G47501A','JN1BJ1BW2MW428736','2023-07-21','145200','SLS USED TRK WHSL','VSU','Vehicle Sales - Used',-22818.69,'G47501A','Wholesale');


-- VehicleSales (tool)
-- status saleCanceled = unwind
select a.soldts::date, a.soldamount, a.typ, a.status, d.fullname
from ads.ext_vehicle_sales a
join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and b.stocknumber = 'T10628GA'
left join ads.ext_organizations d on a.soldto = d.partyid
order by d.fullname

-- bopmast
select record_key, record_status, bopmast_Stock_number, bopmast_vin, bopmast_search_name, row_from_date, sale_type, date_capped
from arkona.xfm_bopmast 
-- where bopmast_vin = '2GNAXTEV9M6138873'
where bopmast_stock_number = 'T11171A' 
order by row_from_date

select * from iar.vehicle_sale_gl_transactions where control = 'G47501A'

-- select * from iar.tmp_inventory_gl_transactions where control = 'G47102A' order by the_date

maybe add a seq attribute

unwind						
G45451C	this one is a mess retail 4/17/23 to Dil Rai, whsl to driv training whsl 8/11/23	gl does not show the wholesale
G47371A												
G48172A retailed and unwound twice, currently in inventory


imws	
G44734M		
G47501A to honda
to toyota
G48797X to honda
H16278A to GM
H17234LA to Toyota	
H17337A to toyota									
												
uncap/recap
G46186AA
G46621A
G47464P
G47371A

total loss to Progressive  
G46265A

so, this all leaves me with control G45451C, G46265A & G48172A that will not comply with a primary key of control/the_date

so, lets see, 
re-create iar.vehicle_sale_gl_transactions
run the clean up code above
delete these 3 controls

delete from iar.vehicle_sale_gl_transactions where control in ('G45451C','G46265A','G48172A');
now, see if i can do a unique index
YESSSSSS , it worked
create unique index on iar.vehicle_sale_gl_transactions(control, the_date);
will leave it like this for now, when figured out, these 3 controls can be added back in

select * from iar.vehicle_sale_gl_transactions where control = 'G43567A'

-- additional cleanup discovered along the way
-- SVI post to a new car against uc account
delete from iar.vehicle_sale_gl_transactions where control = 'G46253';
-----------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------
-- 02/07/24 other attributes required for sales data

-- 5165 rows
select count(*) from iar.vehicle_sale_gl_transactions 
-- 5063 distinct controls
select count(distinct control) from iar.vehicle_sale_gl_transactions 

-- vin
select a.*, b.vin
from iar.vehicle_sale_gl_transactions a
left join iar.control_vins b on a.control = b.control
where b.vin is null -- full coverage, no missing vins

-- bopmast
-- ~ 400 mostly wholesale with no bopmast record
select a.*, b.record_key, b.buyer_number, b.bopmast_search_name
from iar.vehicle_sale_gl_transactions a
join iar.control_vins aa on a.control = aa.control
left join arkona.ext_bopmast b on a.control = b.bopmast_stock_number
  and aa.vin = b.bopmast_vin

-- vehicleSales (tool)
select c.*, a.soldts::date, a.soldamount, a.typ, a.status, d.fullname
from ads.ext_vehicle_sales a
join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
join iar.vehicle_sale_gl_transactions c on b.stocknumber = c.control
left join ads.ext_organizations d on a.soldto = d.partyid
order by d.fullname  


-- board data
drop table if exists iar.tmp_board_sales cascade;
create table iar.tmp_board_sales as
select a.*, b.deal_number, b.boarded_date, c.board_sub_type
from iar.vehicle_sale_gl_transactions a
join board.sales_board b on a.control = b.stock_number
  and not b.is_deleted
join board.board_types c on b.board_type_key = c.board_type_key
  and c.board_type = 'Deal';

create unique index on iar.tmp_board_sales(control, the_date)  

-- yikes this returns 364 rows after updating with pre 2023 transactions
select a.*
from iar.tmp_board_sales a
join (
	select control, the_date
	from iar.tmp_board_sales
	group by control, the_date
	having count(*) > 1) b on a.control = b.control
		and a.the_date = b.the_date
order by a.control, a.the_date		


select * from iar.vehicle_sale_gl_transactions where the_date < '01/01/2023'  


-- 5004 controls with a single row
select control
from iar.vehicle_sale_gl_transactions
group by control having count(*) = 1

-- all the single row controls
select a.* 
from iar.vehicle_sale_gl_transactions a
join (
	select control
	from iar.vehicle_sale_gl_transactions
	group by control having count(*) = 1) b on a.control = b.control
order by the_date	





-- only 59 controls with multiple rows
-- that just feels like too few
select count(distinct control)
from iar.vehicle_sale_gl_transactions m
where not exists (
	select a.control
	from iar.vehicle_sale_gl_transactions a
	join (
		select control
		from iar.vehicle_sale_gl_transactions
		group by control having count(*) = 1) b on a.control = b.control
	where a.control = m.control)

-- start with these "clean" transactions to explore adding the additional attributes needed
-- i believe they should all be straight forward sales
drop table if exists iar.tmp_single_sale_trans cascade;
create table iar.tmp_single_sale_trans as 
select a.* 
from iar.vehicle_sale_gl_transactions a
join (
	select control
	from iar.vehicle_sale_gl_transactions
	group by control having count(*) = 1) b on a.control = b.control
order by the_date;
create unique index on iar.tmp_single_sale_trans(control, the_date);

drop table if exists iar.tmp_single_sale_trans cascade;
create table iar.tmp_single_sale_trans as 
select a.* 
from iar.vehicle_sale_gl_transactions a
join (
	select control
	from iar.vehicle_sale_gl_transactions
	group by control having count(*) > 1) b on a.control = b.control
order by the_date;
create unique index on iar.tmp_single_sale_trans(control, the_date);


select * 
from iar.tmp_single_sale_trans m
left join ( -- bopmast
	select a.control, b.record_key, b.buyer_number, b.bopmast_search_name
	from iar.vehicle_sale_gl_transactions a
	join iar.control_vins aa on a.control = aa.control
	left join arkona.ext_bopmast b on a.control = b.bopmast_stock_number
		and aa.vin = b.bopmast_vin) n on m.control = n.control
order by m.sale_type, bopmast_search_name



select m.*, o.vin 
from iar.tmp_single_sale_trans m
left join ( -- bopmast
	select a.control, b.record_key, b.buyer_number, b.bopmast_search_name
	from iar.vehicle_sale_gl_transactions a
	join iar.control_vins aa on a.control = aa.control
	left join arkona.ext_bopmast b on a.control = b.bopmast_stock_number
		and aa.vin = b.bopmast_vin) n on m.control = n.control
left join iar.control_vins o on m.control = o.control		
where right(m.control, 1) = 'L'		
order by m.sale_type, bopmast_search_name










/*
TODO what are K & M vehicles
select * from iar.vehicle_sale_gl_transactions where right(control, 1) in( 'K', 'M')





*/