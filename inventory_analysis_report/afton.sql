﻿select *
--, ads.get_intra_market_wholesale_old_stock_number(a.stock_number)
from iar.sales_data_v1 a
left join (
    select b.stock_number, b.avail_category
    from (
        select stock_number, max(the_date) as the_date
        from iar.inventory_detail
        group by stock_number) a
    left join iar.inventory_detail b on a.stock_number = b.stock_number
        and a.the_date = b.the_date) c on a.stock_number = c.stock_number
        --ads.get_intra_market_wholesale_old_stock_number(a.stock_number) = c.stock_number
where a.delivery_date between '06/01/2023' and '06/30/2023'
   --and c.avail_category is null -- BAD DEALS ONLY
   --and right(a.stock_number,1) in ('G','H','T') -- ONLY IMWS
order by right(a.stock_number, 1), a.stock_number
/* 
ok taking the bad deals only and any stock number that ends with G, H or T. 
70 out of 72 come back with an "old" stock number.
After I add the function ads.get_intra_market_wholesale_old_stock_number(a.stock_number)
to the left join I get 4 that come back as not in inventory.. hmmm
*/
/*
select *, ads.get_intra_market_wholesale_old_stock_number(a.stock_number)
from iar.sales_data_v1 a
left join (
    select b.stock_number, b.avail_category
    from (
        select stock_number, max(the_date) as the_date
        from iar.inventory_detail
        group by stock_number) a
    left join iar.inventory_detail b on a.stock_number = b.stock_number
        and a.the_date = b.the_date) c on a.stock_number = c.stock_number
        --ads.get_intra_market_wholesale_old_stock_number(a.stock_number) = c.stock_number
where a.delivery_date between '06/01/2023' and '06/30/2023'
   and c.avail_category is null -- BAD DEALS ONLY
   and right(a.stock_number,1) in ('G','H','T') -- ONLY IMWS
order by right(a.stock_number, 1), a.stock_number
*/
/*
  -- Lease BUYOUT IF
  -- -25 ish front gross
  -- HOUSE sales person?
select *
from iar.sales_data_v1 a
left join (
    select b.stock_number, b.avail_category
    from (
        select stock_number, max(the_date) as the_date
        from iar.inventory_detail
        group by stock_number) a
    left join iar.inventory_detail b on a.stock_number = b.stock_number
        and a.the_date = b.the_date) c on a.stock_number = c.stock_number
where a.delivery_date between '06/01/2023' and '06/30/2023'
   and c.avail_category is null -- BAD DEALS ONLY
   and right(a.stock_number,1) in ('L') -- LEASE BUYOUTS AND PURCHASES
   order by right(a.stock_number, 1), a.stock_number
*/
select *
from iar.sales_data_v1 a
left join (
    select b.stock_number, b.avail_category
    from (
        select stock_number, max(the_date) as the_date
        from iar.inventory_detail
        group by stock_number) a
    left join iar.inventory_detail b on a.stock_number = b.stock_number
        and a.the_date = b.the_date) c on a.stock_number = c.stock_number
where a.delivery_date between '06/01/2023' and '06/30/2023'
   and c.avail_category is null -- BAD DEALS ONLY
   and right(a.stock_number,1) not in ('G','H','T','L') -- LEASE BUYOUTS AND PURCHASES
   order by right(a.stock_number, 1), a.stock_number
   
   
   select * 
   from iar.inventory_detail
   where stock_number = 'G47409A'
   
   
   -- OK SO WHY 'G47409A' not showing use 06/03/2023
    
 -- tool   
select d.stocknumber, e.vin, d.vehicleinventoryitemid
        from ads.ext_vehicle_inventory_items d
        join ads.ext_vehicle_items e on d.vehicleitemid = e.vehicleitemid
        where '06/03/2023' between d.fromts::date and d.thruts::date
        and d.stocknumber = 'G47409A'
-- ACCT        
select a.control, sum(a.amount)::integer as inventory_dollars
        from fin.fact_gl a
        join fin.dim_account b on a.account_key = b.account_key
            and b.account not in ('2400P','2401P','224002','224102','124001','124101') -- exclude pack accounts
        join jeri.inventory_accounts c on b.account = c.account
            and c.department = 'UC'
        join dds.dim_date   d on a.date_key = d.date_key
            and d.the_date < '06/03/2023'::Date + 1
            and d.the_date > ('06/03/2023'::date - '3 years'::interval)::date
        where a.post_status = 'Y'  
            and a.control <> '21'
            and a.control = 'G47409A'
        group by a.control
        having sum(a.amount) > 0        
        
select distinct inpmast_stock_number, inpmast_vin, inpmast_sale_account as sale_account, inventory_account as inv_account
from arkona.xfm_inpmast  
where '06/03/2023' between row_From_date and row_thru_date 
    and status = 'I'
    and (
        type_n_u = 'U'
        or (left(inpmast_stock_number, 1) = 'H' and right(inpmast_stock_number, 1) = 'R'))
    and 
      case
        when left(inpmast_stock_number, 1) = 'T' then inpmast_company_number = 'RY8'
        else true
      end 
    and inpmast_stock_number = 'G47409A'
        
/*
--JON QUERY        
   
   select '06/03/2023' as the_date, coalesce(inpmast_stock_number, a.control, c.stocknumber) as stock_number, coalesce(inpmast_vin, c.vin) as vin, inventory_dollars, sale_account,     
        inv_account, vehicleinventoryitemid,
        case  -- use stock_number to establish the store
            when left(inpmast_stock_number, 1) = 'G' then 'RY1'
            when left(inpmast_stock_number, 1) = 'H' then 'RY2'
            when left(inpmast_stock_number, 1) = 'T' then 'RY8'
            else 'XXX'
        end as store        
    from (
        select a.control, sum(a.amount)::integer as inventory_dollars
        from fin.fact_gl a
        join fin.dim_account b on a.account_key = b.account_key
            and b.account not in ('2400P','2401P','224002','224102','124001','124101') -- exclude pack accounts
        join jeri.inventory_accounts c on b.account = c.account
            and c.department = 'UC'
        join dds.dim_date   d on a.date_key = d.date_key
            and d.the_date < '06/03/2023'::Date + 1
            and d.the_date > ('06/03/2023'::date - '3 years'::interval)::date
        where a.post_status = 'Y'  
            and a.control <> '21'
        group by a.control
        having sum(a.amount) > 0) a
    full outer join (
        select distinct inpmast_stock_number, inpmast_vin, inpmast_sale_account as sale_account, inventory_account as inv_account
        from arkona.xfm_inpmast  
        where '06/03/2023' between row_From_date and row_thru_date 
            and status = 'I'
            and (
                type_n_u = 'U'
                or (left(inpmast_stock_number, 1) = 'H' and right(inpmast_stock_number, 1) = 'R'))
            and 
              case
                when left(inpmast_stock_number, 1) = 'T' then inpmast_company_number = 'RY8'
                else true
              end) b on a.control = b.inpmast_stock_number
    full outer join (           
        select d.stocknumber, e.vin, d.vehicleinventoryitemid
        from ads.ext_vehicle_inventory_items d
        join ads.ext_vehicle_items e on d.vehicleitemid = e.vehicleitemid
        where '06/03/2023' between d.fromts::date and d.thruts::date)  c on coalesce(a.control, b.inpmast_stock_number) =  c.stocknumber
            and b.inpmast_vin = c.vin
where a.control = 'G47409A'            
 where a.control is not null
     and b.inpmast_stock_number is not null
     and c.stocknumber is not null
     and b.sale_account is not null
    and c.stocknumber is not null;    
    */
        