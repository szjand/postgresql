﻿select * from cu.used_3
where delivery_date between '12/01/2023' and '12/31/2023'
  and sale_type = 'Retail'

select * from iar.inventory_detail where stock_number = 'G48628X' and the_date = '12/29/2023'

-- used car gross accounts
-- ry1 by line, sales only
select f.store, d.gl_account, e.account_type, d.gm_account, b.page, b.line, e.description
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202312
  and (
		b.page = 16 and b.line between 1 and 14
		or
		b.page = 17 and b.line between 11 and 20)
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
order by f.store, d.gl_account




-- sale amounts that match the statement
select b.account, sum(a.amount)
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
  and b.account in ('144601','144602','145000','145001','145002','144800','145200')
join dds.dim_date c on a.date_key = c.date_key
  and c.year_month = 202312   
where a.post_status = 'Y'
group by b.account    


-- matches the statement, both pages 16 & 17, all 3 stores
select d.store, page, line, d.account_type, sum(a.amount)
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
  and b.current_row
join dds.dim_date c on a.date_key = c.date_key
  and c.year_month = 202312  
join ( -- fs data
	select f.store, d.gl_account, e.account_type, d.gm_account, b.page, b.line, e.description
	from fin.fact_fs a
	inner join fin.dim_fs b on a.fs_key = b.fs_key
		and b.year_month = 202312
		and (
			b.page = 16 and b.line between 1 and 14
			or
			b.page = 17 and b.line between 11 and 20)
	inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
	inner join fin.dim_account e on d.gl_account = e.account
	  and e.current_row
	inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on b.account = d.gl_account
where a.post_status = 'Y'
group by d.store, d.page, d.line, d.account_type    

-- table sales is based on cu.used_3 delivery dates between 12/01 and 12/31/2023
do $$
	declare 
		_start_date date := '12/01/2023';	
		_end_date date := '12/31/2023';
begin
  drop table if exists sales;
  create temp table sales as
  select 
    case
      when left(stock_number, 1) = 'G' then 'GM'
      when left(stock_number, 1) = 'H' then 'HN'
      when left(stock_number, 1) = 'T' then 'TY'
      else 'XXX'
    end as store, 
    a.stock_number, a.delivery_date
  from cu.used_3 a
  where a.delivery_date between _start_date and _end_date
    and a.sale_type = 'Retail';
end	$$;

select * from sales;

do $$
	declare 
		_start_date date := '12/01/2023';	
		_end_date date := '12/31/2023';
begin
  drop table if exists sales_detail;
  create temp table sales_detail as
select d.store, a.control, d.page, d.line, d.account_type, b.account, sum(a.amount) as amount
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
  and b.current_row
join dds.dim_date c on a.date_key = c.date_key
  and c.the_date between _start_date and _end_date
join ( -- fs data
	select f.store, d.gl_account, e.account_type, d.gm_account, b.page, b.line, e.description
	from fin.fact_fs a
	inner join fin.dim_fs b on a.fs_key = b.fs_key
		and b.year_month = (select year_month from dds.dim_date where the_date = _start_date)
		and (  
			b.page = 16 and b.line between 1 and 6 -- retail only
			or
			b.page = 17 and b.line between 11 and 20)
	inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
	inner join fin.dim_account e on d.gl_account = e.account
	  and e.current_row
	inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on b.account = d.gl_account
where a.post_status = 'Y'
group by a.control, d.store, d.page, d.line, d.account_type, b.account; 
end	$$;
alter table sales_detail add primary key(control, page, line, account_type, account);


select * from sales_detail;

select store, page, line,
  (sum(b.amount) filter (where b.account_type in ('COGS','Other Expense') and page = 16))::integer as front_cogs,
  (sum(-b.amount) filter (where b.account_type in ('Sale','Other Income') and page = 16))::integer as front_sales,
  (sum(-b.amount) filter (where page = 16))::integer as front_gross,
  (sum(b.amount) filter (where b.account_type in ('COGS','Other Expense') and page = 17))::integer as fi_cogs,
  (sum(-b.amount) filter (where b.account_type in ('Sale','Other Income') and page = 17))::integer as fi_sales,
  (sum(-b.amount) filter (where page = 17))::integer as fi_gross  
from sales_detail b
group by store, page, line
order by store, page, line




 

-- gm gross is way off, statement: -71881, query: 4337
select store, sum(front_gross), sum(fi_gross)
from iar.test_sales_dec_23
group by store

-- add line & page
select store, page, line, sum(front_gross), sum(fi_gross)
from (
	select a.store, a.stock_number, a.delivery_date, b.page, b.line,
		(sum(b.amount) filter (where b.account_type in ('COGS','Other Expense') and page = 16))::integer as front_cogs,
		(sum(-b.amount) filter (where b.account_type in ('Sale','Other Income') and page = 16))::integer as front_sales,
		(sum(-b.amount) filter (where page = 16))::integer as front_gross,
		(sum(b.amount) filter (where b.account_type in ('COGS','Other Expense') and page = 17))::integer as fi_cogs,
		(sum(-b.amount) filter (where b.account_type in ('Sale','Other Income') and page = 17))::integer as fi_sales,
		(sum(-b.amount) filter (where page = 17))::integer as fi_gross
	from sales a
	join sales_detail b on a.stock_number = b.control
	group by a.store, a.stock_number, a.delivery_date, b.page, b.line) x
group by store, page, line
order by store, page, line

*** start here ***************************************************************************************************

select * from sales_detail  -- gl transactions in dec 23 in used car cogs, sales & fi accounts based on routing in financial statements
select * from sales  -- stock numbers of vehicles delivered on retail sales in dec 23, based on  cu.used_3

-- inner join on sales & sales_detail on stock_number/control
-- this is the data that i submitted to afton, good enough for sales side?
drop table if exists iar.test_sales_dec_23 cascade;
create unlogged table iar.test_sales_dec_23 as
select a.store, a.stock_number, a.delivery_date,
  (sum(b.amount) filter (where b.account_type in ('COGS','Other Expense') and page = 16))::integer as front_cogs,
  (sum(-b.amount) filter (where b.account_type in ('Sale','Other Income') and page = 16))::integer as front_sales,
  (sum(-b.amount) filter (where page = 16))::integer as front_gross,
  (sum(b.amount) filter (where b.account_type in ('COGS','Other Expense') and page = 17))::integer as fi_cogs,
  (sum(-b.amount) filter (where b.account_type in ('Sale','Other Income') and page = 17))::integer as fi_sales,
  (sum(-b.amount) filter (where page = 17))::integer as fi_gross
from sales a
join sales_detail b on a.stock_number = b.control
group by a.store, a.stock_number, a.delivery_date
order by left(a.stock_number, 1), a.delivery_date;
alter table iar.test_sales_dec_23 add primary key(delivery_date, stock_number);

select * from iar.test_sales_dec_23

the numbers are badly off compared to the statement

-- 329 rows with no corresponding row in sales
-- implying that those control numbers that are stocknumbers are for vehicles that did not deliver in December
select * 
from sales a
full outer join sales_detail b on a.stock_number = b.control
where a.stock_number is null



--  these are the numbers for dec 23 based on vehicle delivered in dec (cu.used_3)
select store, sum(front_cogs) as front_cogs, sum(front_sales) as front_sales, sum(front_gross) as front_gross,
  sum(fi_cogs) as fi_cogs,  sum(fi_sales) as fi_sales, sum(fi_gross) as fi_gross
from iar.test_sales_dec_23 aa
group by store

-- these are the numbers from the statement that will not show up because they are for control numbers
-- that are not vehicle delivered in december 23

-- this is the financial transactions that are on the statement, but are not represented in the sales data
 

-- add them both up and we are now in synch with the fs
select *, x.front_cogs + y.front_cogs as total_cogs, x.front_sales + y.front_sales as total_front_sales, x.front_gross + y.front_gross as total_front_gross,
  x.fi_cogs + y.fi_cogs as total_fi_cogs, x.fi_sales + y.fi_sales as total_fi_sales, x.fi_gross + y.fi_gross as total_fi_gross
from (--  these are the numbers for dec 23 based on vehicle delivered in dec (cu.used_3)
	select store, sum(front_cogs) as front_cogs, sum(front_sales) as front_sales, sum(front_gross) as front_gross,
		sum(fi_cogs) as fi_cogs,  sum(fi_sales) as fi_sales, sum(fi_gross) as fi_gross
	from iar.test_sales_dec_23 aa
	group by store) x
join (
select case b.store when 'RY1' then 'GM' when  'RY2' then 'HN' when 'RY8' then 'TY' end as store,
		(sum(b.amount) filter (where b.account_type in ('COGS','Other Expense') and page = 16))::integer as front_cogs,
		(sum(-b.amount) filter (where b.account_type in ('Sale','Other Income') and page = 16))::integer as front_sales,
		(sum(-b.amount) filter (where page = 16))::integer as front_gross,
		(sum(b.amount) filter (where b.account_type in ('COGS','Other Expense') and page = 17))::integer as fi_cogs,
		(sum(-b.amount) filter (where b.account_type in ('Sale','Other Income') and page = 17))::integer as fi_sales,
		(sum(-b.amount) filter (where page = 17))::integer as fi_gross
from sales a
full outer join sales_detail b on a.stock_number = b.control
where a.stock_number is null		
group by b.store) y on x.store = y.store

--===================================================================================================================================================
-- ok, Afton and i are on the same page on sales, time to generate the data for iar.sales_detail

havent gotten to it yet, but anticipating nightly update, this is different than inventory, 
alright, already confusing myself, 
so get to the point
need to add bopmast_id to sales and gl_description to gl transactions, remember what i was trying to do with (i dont know what project now)
but trying to to the gl_transaction to an operational deal
TODO ask greg about post deal transactions
so go ahead and add thge attributes now

Well turns out most everything i did below was mind fucking
going with the sales/sales detail approach for backloading

Thne will have to see about what the nightly update looks like
so, i am going to comment out all the mind fucking and ssend it to the bottom of the file
and proceed with backloading sales/sales detail with 2023 data
--===================================================================================================================================================


create or replace function iar.update_sales(_start_date date, _end_date date)
  returns void as 
$BODY$  
/*

select iar.update_sales('01/23/2024', '01/23/2024');

TODO when this goes to production, iar.ext_sales will also populate (cumulatively) the table iar.sales and then emptied
     same with iar.ext_sales_dollars into iar.sales_dollars

TODO hard coded year_month in iar.ext_sales_dollars     
   
*/
-- sales has to change, can no longer use cu.used_3, but will use board instead
		insert into iar.ext_sales(store,bopmast_id,stock_number,delivery_date,customer)
		select distinct
			case 
				when store_key in (39,41) then 'RY1' 
				when store_key = 40 then 'RY2'
				when store_key = 42 then 'RY8' 
			end as store,
			e.record_key as bopmast_id, a.stock_number, boarded_date as delivery_date, bopmast_search_name as customer
		from board.sales_board a
		join board.daily_board b on a.board_id = b.board_id 
			and vehicle_type = 'U'
		join board.board_types c on a.board_type_key = c.board_type_key 
			and c.board_type ='Deal'
			and c.board_sub_type = 'Retail'
		join dds.dim_date d on a.boarded_date = d.the_date
		join arkona.ext_bopmast e on a.stock_number = e.bopmast_stock_number
		where boarded_date between _start_date and _end_date
				and is_deleted = false
				and store_key in (39,41,40, 42); 

		insert into iar.ext_sales_dollars(the_date,store,control,page,line,account_type,account,amount,customer)
		select c.the_date, d.store, a.control, d.page, d.line, d.account_type, b.account, sum(a.amount) as amount, string_agg(distinct bb.description, '|')
		from fin.fact_gl a
		join fin.dim_account b on a.account_key = b.account_key
			and b.current_row
		join dds.dim_date c on a.date_key = c.date_key  -- transaction date of any date in the range
			and c.the_date between _start_date and _end_date
		left join iar.ext_sales aa on a.control = aa.stock_number
			and c.the_date <= aa.delivery_date  
		left join fin.dim_gl_description bb on a.gl_description_key = bb.gl_description_key  
		join ( -- fs data -- there is no
			select f.store, d.gl_account, e.account_type, d.gm_account, b.page, b.line, e.description
			from fin.fact_fs a
			inner join fin.dim_fs b on a.fs_key = b.fs_key
				and b.year_month = 202312 -- (select year_month from dds.dim_date where the_date = _start_date)
				and (
					b.page = 16 and b.line between 1 and 6 -- retail only
					or
					b.page = 17 and b.line between 11 and 20)
			inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
			inner join fin.dim_account e on d.gl_account = e.account
				and e.current_row
			inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on b.account = d.gl_account
		where a.post_status = 'Y'
		group by c.the_date, a.control, d.store, d.page, d.line, d.account_type, b.account; 

		insert into iar.sales_data_v1
		select a.store, a.stock_number, a.delivery_date,
			coalesce((sum(b.amount) filter (where b.account_type in ('COGS','Other Expense') and page = 16))::integer, 0) as front_cogs,
			coalesce((sum(-b.amount) filter (where b.account_type in ('Sale','Other Income') and page = 16))::integer, 0) as front_sales,
			coalesce((sum(-b.amount) filter (where page = 16))::integer, 0) as front_gross,
			coalesce((sum(b.amount) filter (where b.account_type in ('COGS','Other Expense') and page = 17))::integer, 0) as fi_cogs,
			coalesce((sum(-b.amount) filter (where b.account_type in ('Sale','Other Income') and page = 17))::integer, 0) as fi_sales,
			coalesce((sum(-b.amount) filter (where page = 17))::integer, 0) as fi_gross
		from iar.ext_sales a
		join iar.ext_sales_dollars b on a.stock_number = b.control
		where not exists (
			select 1
			from iar.sales_data_v1
			where stock_number = a.stock_number
				and delivery_date = a.delivery_date)
		group by a.store, a.stock_number, a.delivery_date
		order by left(a.stock_number, 1), a.delivery_date;

$BODY$
language sql;

select max(delivery_date) from iar.ext_sales  -- 1/20
select max(the_date) from  iar.ext_sales_dollars -- 1/20

select max(delivery_date) from iar.sales_data_v1  -- 1/20

select * from iar.sales_data_v1
where stock_number = 'G48908A'
order by delivery_date

select * from iar.inventory_detail where stock_number = 'G48908A' order by the_Date


-- drop table if exists iar.sales_detail cascade;
-- create table iar.sales_detail (
--   store citext not null,
--   stock_number citext not null,
--   delivery_date date not null,
--   front_cogs integer not null,
--   front_sales integer not null,
--   front_gross integer not null,
--   fi_cogs integer not null,
--   fi_sales integer not null,
--   fi_gross integer not null,
--   primary key (delivery_date, stock_number));

drop table if exists iar.sales_data_v1 cascade;
create table iar.sales_data_v1 (
  store citext not null,
  stock_number citext not null,
  delivery_date date not null,
  front_cogs integer not null,
  front_sales integer not null,
  front_gross integer not null,
  fi_cogs integer not null,
  fi_sales integer not null,
  fi_gross integer not null,
  primary key(delivery_date, stock_number));
comment on table iar.sales_data_v1 is 'this table consolidates the sales data, both units and dollars for populating the inventory analysis page';   

drop table if exists iar.ext_sales cascade;
create table iar.ext_sales (
  store citext not null,
  bopmast_id citext not null,
  stock_number citext not null,
  delivery_date date not null,
  customer citext,
  primary key(delivery_date, stock_number));
  create index on iar.ext_sales(bopmast_id);
comment on table iar.ext_sales is 'in setting this project up, this table is used to hold data from cu.used_3 for the backfill of historical data,
  subsequently, this table is populated nightly with daily sales data from the sales board';  

drop table if exists iar.ext_sales_dollars cascade;
create table iar.ext_sales_dollars (
  the_date date not null,
  store citext not null,
  control citext not null,
  page integer not null,
  line numeric(4,1) not null,
  account_type citext not null,
  account citext not null,
  amount numeric(8,2) not null,
  customer citext,
  primary key(the_date, control, page, line, account_type, account));
comment on table iar.ext_sales_dollars is 'in setting this project up, this table is used to hold data from fact_gl for the backfill of historical data,
  subsequently, this table is populated nightly with data from fat_gl for the daily up date'; 

-- select * from cu.used_3 where delivery_date = '12/22/2023'
-- 
-- select count(*) from  iar.ext_sales
-- 
-- 
-- create or replace function iar.update_sales(_start_date date, _end_date date)
--   returns void as 
-- $BODY$  
-- /*
-- 1/21/24 add bopmast_id to iar.ext_sales
-- 
-- select iar.update_sales('12/01/2023', '12/31/2023');
-- 
-- TODO when this goes to production, iar.ext_sales will also populate (cumulatively) the table iar.sales and then emptied
--      same with iar.ext_sales_dollars into iar.sales_dollars
--    
-- */
-- 
-- -- insert into iar.ext_sales
-- -- select 
-- -- 	case
-- -- 		when left(stock_number, 1) = 'G' then 'RY1'
-- -- 		when left(stock_number, 1) = 'H' then 'RY2'
-- -- 		when left(stock_number, 1) = 'T' then 'RY8'
-- -- 		else 'XXX'
-- -- 	end as store, 
-- -- 	a.bopmast_id, a.stock_number, a.delivery_date
-- -- from cu.used_3 a
-- -- where a.delivery_date between _start_date and _end_date
-- -- 	and a.sale_type = 'Retail';
-- 
-- insert into iar.ext_sales
-- select 
-- 	case
-- 		when left(stock_number, 1) = 'G' then 'RY1'
-- 		when left(stock_number, 1) = 'H' then 'RY2'
-- 		when left(stock_number, 1) = 'T' then 'RY8'
-- 		else 'XXX'
-- 	end as store, 
-- 	a.bopmast_id, a.stock_number, a.delivery_date, b.bopmast_search_name
-- from cu.used_3 a
-- left join arkona.ext_bopmast b on a.bopmast_id = b.record_key::citext 
--   and
-- 		case left(a.stock_number, 1) 
-- 			when 'G' then b.bopmast_company_number = 'RY1'
-- 			when 'H' then b.bopmast_company_number = 'RY2'
-- 			when 'T' then b.bopmast_company_number = 'RY8'
-- 			else true
-- 		end
-- where a.delivery_date between _start_date and _end_date
-- 	and a.sale_type = 'Retail';
-- 
-- 		
-- 
-- -- somehow, at least for the initial run, the accounting transaction needs to be on or before the delivery date
-- -- add join to iar.ext_sales to fact_gl
-- -- it might be easier to do the backlog catch up month by month
-- 
-- -- 1/21/24 the changes i need to make here are to include the dim_gl_description as a source for the customer's name
-- --		for disambiguating unwinds from crossing t's and dotting i's on deals
-- -- also at some level, believe i will want to have available those transactions that are for other than vehicles delivered in
-- --    the month
-- 
-- insert into iar.ext_sales_dollars
-- select c.the_date, d.store, a.control, d.page, d.line, d.account_type, b.account, sum(a.amount) as amount, string_agg(distinct bb.description, '|')
-- from fin.fact_gl a
-- join fin.dim_account b on a.account_key = b.account_key
--   and b.current_row
-- join dds.dim_date c on a.date_key = c.date_key  -- transaction date of any date in the range
--   and c.the_date between _start_date and _end_date
-- left join iar.ext_sales aa on a.control = aa.stock_number
--   and c.the_date <= aa.delivery_date  
-- left join fin.dim_gl_description bb on a.gl_description_key = bb.gl_description_key
-- join ( -- fs data
-- 	select f.store, d.gl_account, e.account_type, d.gm_account, b.page, b.line, e.description
-- 	from fin.fact_fs a
-- 	inner join fin.dim_fs b on a.fs_key = b.fs_key
-- 		and b.year_month = (select year_month from dds.dim_date where the_date = '12/01/2023') -- _start_date)
-- 		and (
-- 			b.page = 16 and b.line between 1 and 6 -- retail only
-- 			or
-- 			b.page = 17 and b.line between 11 and 20)
-- 	inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
-- 	inner join fin.dim_account e on d.gl_account = e.account
-- 	  and e.current_row
-- 	inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on b.account = d.gl_account
-- where a.post_status = 'Y'
-- group by c.the_date, a.control, d.store, d.page, d.line, d.account_type, b.account; 
-- 
-- $BODY$
-- language sql;
-- 
-- select a.store, b.page, sum(b.amount) as total 
-- from iar.ext_sales a
-- join iar.ext_sales_dollars b on a.stock_number = b.control
--   and b.the_date < '12/31/2023' -- no transactions after month end
-- where a.delivery_date between '12/01/2023' and '12/31/2023'
-- group by a.store, b.page 
-- order by a.store, b.page
-- 
-- -- this is the layout of the data afton needs for the report
-- drop table if exists iar.test_sales_dec_23_2 cascade;
-- create unlogged table iar.test_sales_dec_23_2 as
-- select a.store, a.stock_number, a.delivery_date,
--   (sum(b.amount) filter (where b.account_type in ('COGS','Other Expense') and page = 16))::integer as front_cogs,
--   (sum(-b.amount) filter (where b.account_type in ('Sale','Other Income') and page = 16))::integer as front_sales,
--   (sum(-b.amount) filter (where page = 16))::integer as front_gross,
--   (sum(b.amount) filter (where b.account_type in ('COGS','Other Expense') and page = 17))::integer as fi_cogs,
--   (sum(-b.amount) filter (where b.account_type in ('Sale','Other Income') and page = 17))::integer as fi_sales,
--   (sum(-b.amount) filter (where page = 17))::integer as fi_gross
-- from iar.ext_sales a
-- join iar.ext_sales_dollars b on a.stock_number = b.control
-- where a.deliverY_date between '12/01/2023' and '12/31/2023'
-- group by a.store, a.stock_number, a.delivery_date
-- order by left(a.stock_number, 1), a.delivery_date;
-- alter table iar.test_sales_dec_23_2 add primary key(delivery_date, stock_number);
-- 
-- select *, x.front_cogs + y.front_cogs as total_cogs, x.front_sales + y.front_sales as total_front_sales, x.front_gross + y.front_gross as total_front_gross,
--   x.fi_cogs + y.fi_cogs as total_fi_cogs, x.fi_sales + y.fi_sales as total_fi_sales, x.fi_gross + y.fi_gross as total_fi_gross
-- from (--  these are the numbers for dec 23 based on vehicle delivered in dec (cu.used_3) this section the same
-- 	select store, sum(front_cogs) as front_cogs, sum(front_sales) as front_sales, sum(front_gross) as front_gross,
-- 		sum(fi_cogs) as fi_cogs,  sum(fi_sales) as fi_sales, sum(fi_gross) as fi_gross
-- 	from iar.test_sales_dec_23_2 aa
-- 	group by store) x
-- join (
-- 	select case b.store when 'RY1' then 'GM' when  'RY2' then 'HN' when 'RY8' then 'TY' end as store,
-- 			(sum(b.amount) filter (where b.account_type in ('COGS','Other Expense') and page = 16))::integer as front_cogs,
-- 			(sum(-b.amount) filter (where b.account_type in ('Sale','Other Income') and page = 16))::integer as front_sales,
-- 			(sum(-b.amount) filter (where page = 16))::integer as front_gross,
-- 			(sum(b.amount) filter (where b.account_type in ('COGS','Other Expense') and page = 17))::integer as fi_cogs,
-- 			(sum(-b.amount) filter (where b.account_type in ('Sale','Other Income') and page = 17))::integer as fi_sales,
-- 			(sum(-b.amount) filter (where page = 17))::integer as fi_gross
-- 	from iar.ext_sales a
-- 	full outer join iar.ext_sales_dollars b on a.stock_number = b.control
-- 	where a.stock_number is null		
-- 	group by b.store) y on x.store = y.store
-- -- 
-- -- select * 
-- -- 
-- -- select * from iar.ext_sales where delivery_date between '12/01/2023' and '12/31/2023'
-- -- 
-- -- select * from iar.ext_sales_dollars where the_date between '12/01/2023' and '12/31/2023' order by control
-- -- 
-- -- 
-- -- select count(distinct a.stock_number) 
-- -- from iar.ext_sales a
-- -- join iar.ext_sales_dollars b on a.stock_number = b.control
-- -- where a.delivery_date between '01/01/2023' and '01/31/2023'
-- -- 
-- -- 
-- -- select * from sales_detail  -- gl transactions in dec 23 in used car cogs, sales & fi accounts based on routing in financial statements
-- -- select * from sales where delivery_date between '01/01/2023' and '01/31/2023'
-- -- 
-- -- select * 
-- -- from (
-- -- select * from sales where delivery_date between '01/01/2023' and '01/31/2023') a
-- -- full outer join (
-- -- select * from iar.ext_sales where delivery_date between '01/01/2023' and '01/31/2023') b on a.stock_number = b.stock_number
-- 
-- -- 
-- select * 
-- from (
-- select * from iar.ext_sales 	where delivery_date between '01/01/2023' and '01/31/2023') a
-- full outer join (
-- select * from iar.ext_sales_dollars where the_date between '01/01/2023' and '01/31/2023') b on a.stock_number = b.control
-- 
-- 
-- select case b.store when 'RY1' then 'GM' when  'RY2' then 'HN' when 'RY8' then 'TY' end as store,
-- 		(sum(b.amount) filter (where b.account_type in ('COGS','Other Expense') and page = 16))::integer as front_cogs,
-- 		(sum(-b.amount) filter (where b.account_type in ('Sale','Other Income') and page = 16))::integer as front_sales,
-- 		(sum(-b.amount) filter (where page = 16))::integer as front_gross,
-- 		(sum(b.amount) filter (where b.account_type in ('COGS','Other Expense') and page = 17))::integer as fi_cogs,
-- 		(sum(-b.amount) filter (where b.account_type in ('Sale','Other Income') and page = 17))::integer as fi_sales,
-- 		(sum(-b.amount) filter (where page = 17))::integer as fi_gross
-- from iar.ext_sales a
-- join iar.ext_sales_dollars b on a.stock_number = b.control
-- where a.delivery_date between '12/01/2023' and '12/31/2023'
-- group by b.store
--  
-- 
-- select *, x.front_cogs + y.front_cogs as total_cogs, x.front_sales + y.front_sales as total_front_sales, x.front_gross + y.front_gross as total_front_gross,
--   x.fi_cogs + y.fi_cogs as total_fi_cogs, x.fi_sales + y.fi_sales as total_fi_sales, x.fi_gross + y.fi_gross as total_fi_gross
-- from (--  these are the numbers for dec 23 based on vehicle delivered in dec (cu.used_3)
-- 	select store, sum(front_cogs) as front_cogs, sum(front_sales) as front_sales, sum(front_gross) as front_gross,
-- 		sum(fi_cogs) as fi_cogs,  sum(fi_sales) as fi_sales, sum(fi_gross) as fi_gross
-- 	from iar.test_sales_dec_23_2 aa
-- 	group by store) x
-- join (
-- select b.store,
-- 		(sum(b.amount) filter (where b.account_type in ('COGS','Other Expense') and page = 16))::integer as front_cogs,
-- 		(sum(-b.amount) filter (where b.account_type in ('Sale','Other Income') and page = 16))::integer as front_sales,
-- 		(sum(-b.amount) filter (where page = 16))::integer as front_gross,
-- 		(sum(b.amount) filter (where b.account_type in ('COGS','Other Expense') and page = 17))::integer as fi_cogs,
-- 		(sum(-b.amount) filter (where b.account_type in ('Sale','Other Income') and page = 17))::integer as fi_sales,
-- 		(sum(-b.amount) filter (where page = 17))::integer as fi_gross
-- from sales a
-- full outer join sales_detail b on a.stock_number = b.control
-- where a.stock_number is null		
-- group by b.store) y on x.store = y.store
-- 
-- 
-- select count(*)  -- 1070
-- from sales a
-- join sales_detail b on a.stock_number = b.control
-- where a.delivery_date between '12/01/2023' and '12/31/2023'
-- -- off by 126
-- select count(*)  -- 1196
-- from iar.ext_sales a
-- join iar.ext_sales_dollars b on a.stock_number = b.control
-- where a.delivery_date between '12/01/2023' and '12/31/2023'
-- 
-- -- select count(*) 
-- -- from sales a
-- -- join sales_detail b on a.stock_number = b.control
-- -- where a.delivery_date between '12/01/2023' and '12/31/2023'
-- -- -- off by 26
-- -- select count(*) 
-- -- from iar.ext_sales a
-- -- join iar.ext_sales_dollars b on a.stock_number = b.control
-- -- where a.delivery_date between '12/01/2023' and '12/31/2023'
-- 
-- select * 
-- from (
-- select distinct stock_number
-- from iar.ext_sales 
-- where delivery_date between '12/01/2023' and '12/31/2023') a
-- full outer join (
-- select distinct control
-- from iar.ext_sales_dollars
-- where the_date between '12/01/2023' and '12/31/2023') b on a.stock_number = b.control
-- order by coalesce(a.stock_number, b.control)
-- 
-- select a.* , b.*
-- from iar.ext_sales_dollars a
-- left join iar.ext_sales b on a.control = b.stock_number
-- where the_date between '12/01/2023' and '12/31/2023'
-- order by a.control, a.the_date
-- 
-- 
-- select *  --235
-- from iar.test_sales_dec_23
-- 
-- select *
-- from iar.test_sales_dec_23_2 a
-- where not exists (
-- select 1 from iar.test_sales_dec_23 where stock_number = a.stock_number)
-- 
-- select * from sales
-- 
-- select * from fin.dim_fs where year_month = 202312
-- 
-- -- i think the revelation is here
-- -- the target table for the project is the data that i sent to afton for the sales side
-- -- and this query is the basis for that, need ot look it over and see what, if anything else, 
-- -- needs to be added
-- -- i do want to add the transaction detail table, tut this is not it, at least, not yet
-- select b.year_month, c.store, b.page, b.line, b.col, sum(-a.amount)
-- from fin.fact_fs a
-- join fin.dim_fs b on a.fs_key = b.fs_key
-- join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
-- where b.year_month between 202301 and 202312
--   and (
--     b.page = 16 and line between 1 and 6
-- 		or
-- 		b.page = 17 and b.line between 11 and 20)    
-- group by b.year_month, c.store, b.page, b.line, b.col
-- order by c.store, b.year_month, b.page, b.line, b.col
-- 
-- -- need to add the delivery_date and then break out the
-- -- uh oh, wait a minute, what i sent here was at the stock number level
-- -- this does not give me stock_numbers
-- -- also, this gives me the fs statement numbers, so, i am thinking this is a bad work around
-- -- the inference of the page is that these are the sales numbers for the vehicles sold in the date range
-- -- and we all know that that is no the same as the fs numbers,
-- -- so for the first cut i need to do sales numbers on just those sold as specified by cu.used_3, i think
-- select b.year_month, c.store, b.page, b.line, b.col, sum(-a.amount)
-- from fin.fact_fs a
-- join fin.dim_fs b on a.fs_key = b.fs_key
-- join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
-- where b.year_month between 202301 and 202312
--   and (
--     b.page = 16 and line between 1 and 6
-- 		or
-- 		b.page = 17 and b.line between 11 and 20)    
-- group by b.year_month, c.store, b.page, b.line, b.col
-- order by c.store, b.year_month, b.page, b.line, b.col
-- 
-- -- so, what does that look like
-- -- iar.ext_sales gives me the cu.used_3 sales, just need to get all the accounting on those
-- -- ah and this may be an issue, the accounting on those that was available at the end of the period (month for the backfill)
-- -- for this first version, trying to match the ouput of iar.test_sales_dec_23: store, stock#, del date,front_cogs, front_sales, front_gross, fi_cogs, fio_sales _fi_gross
-- 
-- 
-- -- maybe i have to do cost and sale separately
-- -- cost is any cost transaction since owning the vehilcle
-- -- sale is any sale transaction in the month of delivery
-- unwind
-- create or replace function iar.update_sales(_start_date date, _end_date date)
--   returns void as 
-- $BODY$  
-- /*
-- 1/21/24 add bopmast_id to iar.ext_sales
-- 
-- select iar.update_sales('12/01/2023', '12/31/2023');
-- 
-- TODO when this goes to production, iar.ext_sales will also populate (cumulatively) the table iar.sales and then emptied
--      same with iar.ext_sales_dollars into iar.sales_dollars
--    
-- */
-- 
-- -- insert into iar.ext_sales
-- -- select 
-- -- 	case
-- -- 		when left(stock_number, 1) = 'G' then 'RY1'
-- -- 		when left(stock_number, 1) = 'H' then 'RY2'
-- -- 		when left(stock_number, 1) = 'T' then 'RY8'
-- -- 		else 'XXX'
-- -- 	end as store, 
-- -- 	a.bopmast_id, a.stock_number, a.delivery_date
-- -- from cu.used_3 a
-- -- where a.delivery_date between _start_date and _end_date
-- -- 	and a.sale_type = 'Retail';
-- 
-- -- insert into iar.ext_sales
-- select b.store, b.stock_number, b.delivery_date, 
-- from iar.ext_sales b
-- left join(
--   select a.* 
--   from fin.fact_gl a
--   join iar.ext_sales aa on a.control = aa.stock_number -- only those vehicles delivered in month
--     and aa.delivery_date between '12/01/2023' and '12/31/2023'
--   join fin.dim_accouant ) bb on b.stock_number = bb.control
--  
-- where b.delivery_date between '12/01/2023' and '12/31/2023'
-- 
-- 
-- 
-- 
-- insert into iar.ext_sales_dollars
-- select c.the_date, d.store, a.control, d.page, d.line, d.account_type, b.account, sum(a.amount) as amount, string_agg(distinct bb.description, '|')
-- from fin.fact_gl a
-- join fin.dim_account b on a.account_key = b.account_key
--   and b.current_row
-- join dds.dim_date c on a.date_key = c.date_key  -- transaction date of any date in the range
--   and c.the_date between _start_date and _end_date
-- left join iar.ext_sales aa on a.control = aa.stock_number
--   and c.the_date <= aa.delivery_date  
-- left join fin.dim_gl_description bb on a.gl_description_key = bb.gl_description_key
-- join ( -- fs accounts cogs/sales/fi 
-- 	select f.store, d.gl_account, e.account_type, d.gm_account, b.page, b.line, e.description
-- 	from fin.fact_fs a
-- 	inner join fin.dim_fs b on a.fs_key = b.fs_key
-- 		and b.year_month = (select year_month from dds.dim_date where the_date = _start_date)
-- 		and (
-- 			b.page = 16 and b.line between 1 and 6 -- retail only
-- 			or
-- 			b.page = 17 and b.line between 11 and 20)
-- 	inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
-- 	inner join fin.dim_account e on d.gl_account = e.account
-- 	  and e.current_row
-- 	inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on b.account = d.gl_account
-- where a.post_status = 'Y'
-- group by c.the_date, a.control, d.store, d.page, d.line, d.account_type, b.account; 
-- 
-- 
-- 
-- 
-- do $$
-- 	declare 
-- 		_start_date date := '12/01/2023';	
-- 		_end_date date := '12/31/2023';
-- begin
--   drop table if exists sales_detail;
--   create temp table sales_detail as
-- select d.store, a.control, d.page, d.line, d.account_type, b.account, sum(a.amount) as amount
-- from fin.fact_gl a
-- join fin.dim_account b on a.account_key = b.account_key
--   and b.current_row
-- join dds.dim_date c on a.date_key = c.date_key
--   and c.the_date between _start_date and _end_date
-- join ( -- fs data
-- 	select f.store, d.gl_account, e.account_type, d.gm_account, b.page, b.line, e.description
-- 	from fin.fact_fs a
-- 	inner join fin.dim_fs b on a.fs_key = b.fs_key
-- 		and b.year_month = (select year_month from dds.dim_date where the_date = _start_date)
-- 		and (
-- 			b.page = 16 and b.line between 1 and 6 -- retail only
-- 			or
-- 			b.page = 17 and b.line between 11 and 20)
-- 	inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
-- 	inner join fin.dim_account e on d.gl_account = e.account
-- 	  and e.current_row
-- 	inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on b.account = d.gl_account
-- where a.post_status = 'Y'
-- group by a.control, d.store, d.page, d.line, d.account_type, b.account; 
-- end	$$;
-- alter table sales_detail add primary key(control, page, line, account_type, account);
-- 
-- 
-- a deal consists of a vehicle and a customer
-- 
-- the challenge is to identify that "deal" in accounting
-- 
-- 
