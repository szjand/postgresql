﻿drop table if exists wtf;
create temp table wtf as
select a.inpmast_stock_number as stock, a.inpmast_vin as vin, a.date_in_invent, a.inpmast_sale_account as sale_account, a.inventory_account as inv_account, a.inpmast_vehicle_cost as cost, a.stocked_company
-- select *
from arkona.ext_inpmast a
-- where inpmast_stock_number = 'G48172A'
where a.status = 'I'
  and a.type_n_u = 'U'
order by a.inpmast_vin;  


select * from wtf where cost < 1

select inpmast_vin
from wtf
group by inpmast_vin
having count(*) > 1



select * from ads.potential_lot_gross_report order by the_date desc limit 100
select * from arkona.ext_inpmast where inpmast_vin = 'JTEBU5JR9L5828218'

-- 1/12/24 afton's request, point in time inventory numbers

-- glpmast only good for 1st and last days of the month
  Select c.account,year, units_beg_balance::integer, 1, jan_units01::integer
-- select *  
  from arkona.ext_glpmast a
  join jeri.inventory_accounts c on a.account_number = c.account
    and c.department = 'UC'
  where a.company_number in ('RY1')
    and a.year = 2024

  


-- this says 295, 459 for market
select distinct inpmast_stock_number, inpmast_vin
from arkona.xfm_inpmast 
where '01/01/2024' between row_From_date and row_thru_date    
  and status = 'I'
  and type_n_u = 'U'
  and stocked_company = 'RY1'

-- 296, 469 for market
select stocknumber
from ads.ext_vehicle_inventory_items
where '01/01/2024' between fromts::date and thruts::date 
  and left(stocknumber, 1) = 'G'

-- sold not delivered viitems not closed out
 select * from ads.ext_vehicle_inventory_items where stocknumber = 'G49102A' 

-- YUCK!!!!!!!!!!!!!!!!!!!!!!
select *
from (
	select distinct inpmast_stock_number, inpmast_vin
	from arkona.xfm_inpmast 
	where '01/01/2024' between row_From_date and row_thru_date    
	and status = 'I'
	and type_n_u = 'U') a
full outer join (
	select b.stocknumber, c.vin
	from ads.ext_vehicle_inventory_items b
	join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
	where '01/01/2024' between b.fromts::date and b.thruts::date) c on a.inpmast_stock_number = c.stocknumber
where inpmast_stock_number is null
  or stocknumber is null	


select * from arkona.xfm_inpmast where inpmast_vin = '1C4PJMCS4EW247946' order by row_from_date	

-- YUCK!!!!!!!!!!!!!!!!!!!!!!
select *
from (
	select distinct inpmast_stock_number, inpmast_vin
	from arkona.xfm_inpmast 
	where '11/01/2023' between row_From_date and row_thru_date    
	and status = 'I'
	and type_n_u = 'U') a
full outer join (
	select b.stocknumber, c.vin
	from ads.ext_vehicle_inventory_items b
	join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
	where '11/01/2023' between b.fromts::date and b.thruts::date) c on a.inpmast_stock_number = c.stocknumber
where inpmast_stock_number is null
  or stocknumber is null	

-- use accounting as base, then, use inpmast/tool to identify actual inventory vehicles 

drop table if exists gl;
create temp table gl as
-- 543, 474 (exc pack accts), 469 (going back to 1/1/21)
select a.control, b.account, sum(a.amount) as amount
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
  and b.account not in ('2400P','2401P','224002','224102','124001','124101') -- exclude pack accounts
join jeri.inventory_accounts c on b.account = c.account
	and c.department = 'UC'
join dds.dim_date	d on a.date_key = d.date_key
  and d.the_date < '01/02/2024'
  and d.the_date > '01/01/2021'
where a.post_status = 'Y'  
  and a.control <> '21'
--   and control not like 'R%'
--   and a.control not like 'HG%'
group by a.control, b.account
having sum(a.amount) > 0; 


select distinct a.*, b.fromts::date, b.thruts::date, c.inventory_account, c.status
from gl a
left join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
left join arkona.xfm_inpmast c on a.control = c.inpmast_stock_number
  and c.status = 'I'
  and '01/01/2024' between c.row_from_date and c.row_thru_date
order by b.thruts::date


select * from fin.dim_account where account = '124101' -- pack

select c.the_date, a.*
-- select sum(a.amount)  -- 66205.88
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
  and b.account = '124100'
join dds.dim_date c on a.date_key = c.date_key  
where a.post_status = 'Y'
  and a.control = 'G49050A'


select * from arkona.ext_inpmast where inpmast_stock_number = 'G49050A'


      select * --b.account
      from fin.dim_account b
      where b.account_type = 'asset'
        and b.department_code = 'uc'
        and b.typical_balance = 'debit'
        and b.current_row
order by b.account       

select account
from jeri.inventory_accounts
where department = 'uc'
order by account

select * from fin.dim_account where account in('2400','2401','2401p')

select * from fin.dim_account where department_code = 'uc' and description like '%pack%'

select * from fin.dim_account where account like '124%' and description like '%pack%'

-- -- give up on figuring out the pack account balance on ~15 vehicles
-- select b.account, c.journal_code, d.the_date, a.amount
-- from fin.fact_gl a
-- join fin.dim_account b on a.account_key = b.account_key
-- join fin.dim_journal c on a.journal_key = c.journal_key
-- join dds.dim_date d on a.date_key = d.date_key
-- where a.post_status = 'Y'
--   and a.control = 'G46193X'
-- order by b.account, d.the_date


do gl with outer join on inpmast & tool    
-- for 1/1/24
-- 452 with no nulls
select '01/01/2024'::date, * 
from (
	select a.control, sum(a.amount) as amount
	from fin.fact_gl a
	join fin.dim_account b on a.account_key = b.account_key
		and b.account not in ('2400P','2401P','224002','224102','124001','124101') -- exclude pack accounts
	join jeri.inventory_accounts c on b.account = c.account
		and c.department = 'UC'
	join dds.dim_date	d on a.date_key = d.date_key
		and d.the_date < '01/02/2024'
		and d.the_date > '01/01/2021'
	where a.post_status = 'Y'  
		and a.control <> '21'
	group by a.control
	having sum(a.amount) > 0) a
full outer join (
	select distinct inpmast_stock_number, inpmast_vin, inpmast_vehicle_cost as cost, stocked_company
	from arkona.xfm_inpmast  
	where '01/01/2024' between row_From_date and row_thru_date 
		and status = 'I'
		and (
			type_n_u = 'U'
			or (left(inpmast_stock_number, 1) = 'H' and right(inpmast_stock_number, 1) = 'R'))) b on a.control = b.inpmast_stock_number
full outer join (			
	select d.stocknumber, e.vin
	from ads.ext_vehicle_inventory_items d
	join ads.ext_vehicle_items e on d.vehicleitemid = e.vehicleitemid
	where '01/01/2024' between d.fromts::date and d.thruts::date)  c on coalesce(a.control, b.inpmast_stock_number) =  c.stocknumber
where a.control is not null
  and b.inpmast_stock_number is not null
  and c.stocknumber is not null
  
====================================================================================================================================
1/16/24, time to get serious

create schema iar;
comment on schema iar is 'schema for inventory analysis report data/functions, for now';

drop table if exists iar.inventory_detail cascade;
create table iar.inventory_detail(
  the_date date not null, 
  stock_number citext not null,
  vin citext not null,
  inventory_dollars integer,
  sale_account citext,
  inventory_account citext,
  vehicle_inventory_item_id citext, 
  best_price integer,
  avail_category citext,
  primary key(the_date, stock_number));

comment on table iar.inventory_detail is 'stock number/date level detail of used car inventory, updated nightly in luigi with the end of the previous
  days data.  Updated with a call to function iar.update_inventory_detail. initially data is used for the inventory half of the Inventory Analysis Report';
  
alter table iar.inventory_detail
add column store citext;

update iar.inventory_detail
set store = 
  case
    when left(stock_number, 1) = 'G' then 'RY1'
    when left(stock_number, 1) = 'H' then 'RY2'
    when left(stock_number, 1) = 'T' then 'RY8'
    else 'XXX'
  end


select (current_date - '3 years'::interval)::date

select * from iar.inventory_detail order by stock_number, the_date limit 100

select stock_number, count(*) from iar.inventory_detail group by stock_number order by count(*) desc

select the_Date, count(*) from iar.inventory_detail group by the_date order by the_date

select count(*) from iar.inventory_detail  -- 135833 since 1/1/2023

3 mintutes for a month, 17977 rows

delete from luigi.luigi_log where pipeline = 'fact_repair_order_today'
select * from luigi.luigi_log where pipeline = 'inventory_analysis'
select * from luigi.luigi_log where task = 'InventoryDetail'

select max(the_date) from iar.inventory_detail

select * from ops.ads_extract order by the_date desc limit 100

delete from iar.inventory_detail where the_date = current_date - 1

select * from iar.inventory_detail where the_date = current_date - 1


create or replace function iar.update_inventory_detail(_the_date date)
  returns void as 
$BODY$  
/*

select iar.update_inventory_detail('03/17/2023');
*/
begin
	insert into iar.inventory_detail(the_date, stock_number, vin, inventory_dollars, sale_account, inventory_account,  
		vehicle_inventory_item_id, store)
	select _the_date as the_date, inpmast_stock_number as stock_number, inpmast_vin as vin, inventory_dollars, sale_account, 
		inv_account, vehicleinventoryitemid,
		case  -- use stock_number to establish the store
			when left(inpmast_stock_number, 1) = 'G' then 'RY1'
			when left(inpmast_stock_number, 1) = 'H' then 'RY2'
			when left(inpmast_stock_number, 1) = 'T' then 'RY8'
			else 'XXX'
		end as store		
	from (
		select a.control, sum(a.amount)::integer as inventory_dollars
		from fin.fact_gl a
		join fin.dim_account b on a.account_key = b.account_key
			and b.account not in ('2400P','2401P','224002','224102','124001','124101') -- exclude pack accounts
		join jeri.inventory_accounts c on b.account = c.account
			and c.department = 'UC'
		join dds.dim_date	d on a.date_key = d.date_key
			and d.the_date < _the_date + 1
			and d.the_date > (_the_date - '3 years'::interval)::date -- be sure to pick up old inventory
		where a.post_status = 'Y'  
			and a.control <> '21'
		group by a.control
		having sum(a.amount) > 0) a
	full outer join (
		select distinct inpmast_stock_number, inpmast_vin, inpmast_sale_account as sale_account, inventory_account as inv_account
		from arkona.xfm_inpmast  
		where _the_date between row_From_date and row_thru_date 
			and status = 'I'
			and (
				type_n_u = 'U'
				or (left(inpmast_stock_number, 1) = 'H' and right(inpmast_stock_number, 1) = 'R'))
			and 
			  case
			    when left(inpmast_stock_number, 1) = 'T' then inpmast_company_number = 'RY8'
			    else true
			  end) b on a.control = b.inpmast_stock_number
	full outer join (			
		select d.stocknumber, e.vin, d.vehicleinventoryitemid
		from ads.ext_vehicle_inventory_items d
		join ads.ext_vehicle_items e on d.vehicleitemid = e.vehicleitemid
		where _the_date between d.fromts::date and d.thruts::date)  c on coalesce(a.control, b.inpmast_stock_number) =  c.stocknumber
			and b.inpmast_vin = c.vin
	where a.control is not null
		and b.inpmast_stock_number is not null
		and c.stocknumber is not null
		and b.sale_account is not null;

	update iar.inventory_detail x
	set best_price = y.best_price
	from (
		select the_date, stock_number, best_price
		from (
			select the_date, stock_number, 
				(select ads.get_best_price_by_stock_number_date(a.stock_number, a.the_date)) as best_price
			from iar.inventory_detail a
			where the_date = _the_date) b
		where best_price is not null) y
	where x.stock_number = y.stock_number
		and x.the_date = y.the_date;

	delete 
	from iar.inventory_detail
	where best_price is null;

	assert (
		select count(*)
		from iar.inventory_detail
		where best_price is null) = 0, 'ERROR: Null Best Price'; 

	update iar.inventory_detail x
	set avail_category = y.avail_category
	from (
		select the_date, stock_number, 
			case 
				when days_avail = 0 then 'Raw'
				when days_avail between 1 and 30 then '1-30'
				when days_avail between 31 and 60 then '31-60'
				when days_avail between 61 and 90 then '61-90'
				when days_avail > 90 then '90+'
			end as avail_category
		from (  
			select the_date, a.stock_number, a.the_date - (coalesce(min(b.fromts::date), a.the_date)) as days_avail
			from iar.inventory_detail a
			left join ads.ext_vehicle_inventory_item_statuses b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
				and b.status = 'RMFlagAV_Available'
				and b.fromts::date <= a.the_date
			where a.the_date = _the_date
			group by a.stock_number, a.the_date) z) y
	where x.stock_number = y.stock_number
		and x.the_date = y.the_date;		

  -- for now, avail_cat Other simply means Wholesale Buffer
  update iar.inventory_detail x
  set avail_category = 'Other'
  from (
		select a.the_date, a.stock_number, b.status, b.fromts::date, b.thruts::date
		from iar.inventory_detail a
		left join ads.ext_vehicle_inventory_item_statuses b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
			and b.status = 'RMFlagWSB_WholesaleBuffer'
			and b.fromts::date <= a.the_date
			and b.thruts::date > the_date
		where b.status is not null) y  	
where x.stock_number = y.stock_number
  and x.the_date = y.the_date;
  			
end  
	$BODY$  
		language plpgsql;

comment on function iar.update_inventory_detail(date) is 'called nightly from luigi to update the table iar.inventory_detail';

/*
need to add the Other avail_category
for now, it is anything in Wholesale Buffer
*/

select * from ads.ext_Vehicle_inventory_item_statuses where vehicleinventoryitemid = '6ae14aaa-9a15-4f1a-9a49-6fa4faf197c7'
RMFlagWSB_WholesaleBuffer

	select a.the_date, a.stock_number, b.status, b.fromts::date, b.thruts::date
	from iar.inventory_detail a
	left join ads.ext_vehicle_inventory_item_statuses b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
		and b.status = 'RMFlagWSB_WholesaleBuffer'
		and b.fromts::date <= a.the_date
		and b.thruts::date > the_date
	where b.status is not null

select count(distinct a.stock_number)
from iar.inventory_detail a
left join ads.ext_vehicle_inventory_item_statuses b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
  and b.status = 'RMFlagWSB_WholesaleBuffer'
  and b.fromts::date <= a.the_date
  and b.thruts::date > the_date
where b.status is not null
-- update existing data
update iar.inventory_detail x
set avail_category = 'Other'
from (
	select a.the_date, a.stock_number, b.status, b.fromts::date, b.thruts::date
	from iar.inventory_detail a
	left join ads.ext_vehicle_inventory_item_statuses b on a.vehicle_inventory_item_id = b.vehicleinventoryitemid
		and b.status = 'RMFlagWSB_WholesaleBuffer'
		and b.fromts::date <= a.the_date
		and b.thruts::date > the_date
	where b.status is not null) y
where x.stock_number = y.stock_number
  and x.the_date = y.the_date; 


select * from iar.inventory_detail where the_date = '01/15/24' order by stock_number

select store, avail_category, count(*) from iar.inventory_detail where the_date = '01/15/24' group by store, avail_category order by store, avail_category

/*
ok, got the existing data updated, now to integrate other in the daily update

*/



-- TS query
	select inpmast_stock_number as stock_number, inpmast_vin as vin, inventory_dollars, sale_account, inv_account, vehicleinventoryitemid
	from (
		select a.control, sum(a.amount)::integer as inventory_dollars
		from fin.fact_gl a
		join fin.dim_account b on a.account_key = b.account_key
			and b.account not in ('2400P','2401P','224002','224102','124001','124101') -- exclude pack accounts
		join jeri.inventory_accounts c on b.account = c.account
			and c.department = 'UC'
		join dds.dim_date	d on a.date_key = d.date_key
			and d.the_date < '03/18/2023' -- _the_date + 1
			and d.the_date > '03/17/2020' -- (_the_date - '3 years'::interval)::date
		where a.post_status = 'Y'  
			and a.control <> '21'
		group by a.control
		having sum(a.amount) > 0) a
	full outer join (
		select distinct inpmast_stock_number, inpmast_vin, inpmast_sale_account as sale_account, inventory_account as inv_account
		from arkona.xfm_inpmast  
		where '03/17/2023' between row_From_date and row_thru_date 
			and status = 'I'
			and (
				type_n_u = 'U'
				or (left(inpmast_stock_number, 1) = 'H' and right(inpmast_stock_number, 1) = 'R'))
			and 
			  case
			    when left(inpmast_stock_number, 1) = 'T' then inpmast_company_number = 'RY8'
			    else true
			  end) b on a.control = b.inpmast_stock_number
	full outer join (			
		select d.stocknumber, e.vin, d.vehicleinventoryitemid
		from ads.ext_vehicle_inventory_items d
		join ads.ext_vehicle_items e on d.vehicleitemid = e.vehicleitemid
		where '03/17/2023' between d.fromts::date and d.thruts::date)  c on coalesce(a.control, b.inpmast_stock_number) =  c.stocknumber
			and b.inpmast_vin = c.vin
	where a.control is not null
		and b.inpmast_stock_number is not null
		and c.stocknumber is not null
		and b.sale_account is not null;	




create or replace function iar.get_inventory_numbers(_the_date date)
  returns table (units integer, inventory_dollars integer, total_potential_sales_gross integer, 
		average_potential_sales_gross integer, average_potential_sales_gross_spread text)  as 
$BODY$  
/*
select iar.update_inventory_detail('03/17/2023');
*/

























select * from iar.base_1
-- alter table base_1 add primary key(the_date, inpmast_stock_number);
	
-- select the_date, inpmast_stock_number as stock_number, inpmast_vin as vin, inventory_dollars, sale_account, inv_account, vehicleinventoryitemid
-- from base_1

-- do the inventory side first
--52 without a price on 11/1/23, the several i checked were not priced until after 11/1
-- base_2 add price, vehicles without a price excluded, down to 512

drop table if exists iar.base_2 cascade;
create unlogged table iar.base_2 (
  the_date date not null,
  stock_number citext not null,
  vin citext not null,
  inventory_dollars integer,
  sale_account citext,
  inventory_account citext,
  vehicle_inventory_item_id citext,
  best_price integer not null,
  primary key(the_date, stock_number));

  
-- drop table if exists base_2 cascade;
-- create temp table base_2 as
create or replace function iar.update_base_2()
  returns void as 
$BODY$  
/*
select iar.update_base_2('11/03/2023');
*/
delete 
from iar.base_1 
where the_date = _the_date;

select *
from (
	select the_date, inpmast_stock_number as stock_number, inpmast_vin as vin, inventory_dollars, sale_account, inv_account, vehicleinventoryitemid,
		(select ads.get_best_price_by_stock_number_date(a.control, a.the_date)) as best_price
	from base_1 a) b
where best_price is not null;
alter table base_2 add primary key(the_date, stock_number);

select * from base_2
	
-- availability per greg, from 1st instance of available
-- base_3 add avail category
drop table if exists base_3 cascade;
create temp table base_3 as
select aa.*, bb.avail_category
from base_2 aa
left join (
	select the_date, stock_number, 
		(case 
			when days_avail = 0 then 'Raw'
			when days_avail between 1 and 30 then '1-30'
			when days_avail between 31 and 60 then '31-60'
			when days_avail between 61 and 90 then '61-90'
			when days_avail > 90 then '90+'
		end)::citext as avail_category
	from (  
		select the_date, a.stock_number, a.the_date - (coalesce(min(b.fromts::date), a.the_date)) as days_avail
		from base_2 a
		left join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
			and b.status = 'RMFlagAV_Available'
			and b.fromts::date <= a.the_date
		group by a.stock_number, a.the_date) d) bb on aa.the_date = bb.the_date
			and aa.stock_number = bb.stock_number

select * from base_3
	
select '11/01/2023'::date - '10/23/2023'::date



select
select * from ads.ext_vehicle_inventory_items where vehicleinventoryitemid = 'f552656d-ff82-4408-aab4-24482d27209b'

-- cu.used_3 sale date
select * 
from cu.used_3
where delivery_date = '11/01/2023'
  and sale_type = 'retail'

  
need to add 
	price
	cost same as inventory dollars
	days available/category
	actual sold amount
	sales
		for closed months cu.used_3
		for open month board for vehicle and sale date, accounting for amounts
aggregated numbers not in the same table as units


  
inpmast.stocked_company is not consistently correct, eg, 	RY1,H17268P

select * from cu.used_3 limit 110

