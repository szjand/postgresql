﻿/*
generates insert statements into AccrualFlatRateGross to run against ads

10/01/18
  removed date from grouping in pto_hours
*/


-- august is all fucked up
-- need to fix it
-- pto is doubled
-- prois is ouf of this workd 11032 gross
-- johnson does not include bonus pay
-- zapzalka gross is doubled

do
$$
declare
  _from_date date := '12/19/2021';
  _thru_date date := '12/31/2021';

begin
drop table if exists wtf;
create temp table wtf as
with 
  clock_hours as (
    select d.employee_number, sum(clock_hours) as clock_hours
    from dds.dim_date a
    inner join arkona.xfm_pypclockin b on a.the_date = b.the_date
    inner join hs.main_shop_flat_rate_techs d on b.employee_number = d.employee_number
      and d.thru_date > _from_date
    where a.the_date between _from_date and _thru_date
    group by d.employee_number), 
  pto_hours as (
--     select a.the_date, d.employee_number, 
    select d.employee_number,
      sum(pto_hours + vac_hours + hol_hours) as pto_hours
    from dds.dim_date a
    left join arkona.xfm_pypclockin b on a.the_date = b.the_date
    inner join hs.main_shop_flat_rate_techs d on b.employee_number = d.employee_number
      and d.thru_date > _thru_date
    where a.the_date between _from_date and _thru_date
--     group by a.the_date, d.employee_number
    group by d.employee_number
    having sum(pto_hours + vac_hours + hol_hours) > 0),     
  flag_hours as (
    select d.employee_number, sum(b.flaghours) as flag_hours
    from dds.dim_date a
    inner join ads.ext_fact_repair_order b on a.date_key = b.flagdatekey
    inner join ads.ext_dim_tech c on b.techkey = c.techkey
    inner join hs.main_shop_flat_rate_techs d on c.employeenumber = d.employee_number
      and d.thru_date > _from_date
    where a.the_date between _from_date and _thru_date
    group by d.employee_number),
  adjustments as (
    select a.tech_number, a.employee_number, 
--       b.ro_number as ro, 'Adjust after Close' as type, 
      sum(labor_hours) as flag_hours
    from hs.main_shop_flat_rate_techs a
    inner join arkona.ext_sdpxtim b on a.tech_number = b.technician_id
    where arkona.db2_integer_to_date_long(b.trans_date) between _from_date and _thru_date
      and a.thru_date > _thru_date
    group by a.tech_number, a.employee_number-- , b.ro_number
    having sum(labor_hours) <> 0),
  production as (
    select a.employee_number, coalesce(sum(c.flag_hours),0) as total_flag_hours, 
      coalesce(sum(d.clock_hours),0) as total_clock_hours, coalesce(sum(e.flag_hours), 0) as total_adjustments,
      case 
        when sum(d.clock_hours) = 0 
          then 0 
      else  round((sum(c.flag_hours) + coalesce(sum(e.flag_hours), 0))/sum(d.clock_hours), 2)  end as proficiency
    from hs.main_shop_flat_rate_techs a
    left join flag_hours c on a.employee_number = c.employee_number
    left join clock_hours d on a.employee_number = d.employee_number
    left join adjustments e on a.employee_number = e.employee_number
    where a.thru_date > _from_date
    group by a.employee_number)    

select 'RY2'::citext as storecode, a.employee_number, aa.lastname, aa.firstname, _from_date, _thru_date, 
  (total_flag_hours * flat_rate)::numeric(8,2) as gross, aa.distcode, (coalesce(c.pto_hours, 0) * pto_rate)::numeric(6,2) as ptopay--, total_flag_hours
from hs.main_shop_flat_rate_techs a
left join ads.ext_edw_employee_dim aa on a.employee_number = aa.employeenumber
  and aa.currentrow = true
left join production b on a.employee_number = b.employee_number
left join pto_hours c on a.employee_number = c.employee_number
where a.thru_date > _thru_date;

end
$$;

select 'insert into AccrualFlatRateGross values ('''|| storecode || ''',''' || employee_number || ''',''' || lastname || ''',''' || firstname
  || ''',''' || _from_date || ''',''' || _thru_date || ''',' || gross || ',''' || distcode || ''',' || ptopay || ');'
from wtf
order by lastname;



/*

insert into AccrualFlatRateGross values ('RY2','276431','Engebretson','Erik','2021-12-19','2021-12-31',1758.04,'TECH',308.16);
insert into AccrualFlatRateGross values ('RY2','273315','Johnson','Brandon','2021-12-19','2021-12-31',2030.80,'TECH',1117.92);
insert into AccrualFlatRateGross values ('RY2','279630','Knudson','Kenneth','2021-12-19','2021-12-31',2321.78,'TECH',516.16);
insert into AccrualFlatRateGross values ('RY2','2130150','Sobolik','Paul','2021-12-19','2021-12-31',2826.44,'TECH',761.80);








*/
-- 03/01 add in the snap cell bonus

add scott wade & zakery louden directly to the accrualcommissions table (non team pay techs)
select 1649.70 +  108.34 1758.04
select 1934.80 + 96  2030.80
select 2160.90 + 160.88  2321.78
select 2696.40 + 130.04  2826.44


