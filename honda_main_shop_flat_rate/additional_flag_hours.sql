﻿

------------------------------------------------------------------------
--< 07/07/20
------------------------------------------------------------------------
/*
Jon,

I was out of town last weekend and totally forgot to record technician w-time…. 

CAN YOU ADD TO FLAG HOURS FOR THE FOLLOWING-

Brandon Johnson-           .9
Wyatt Zapzalka-                5.0
Paul Sobolik-                      2.8
Garrett Evenson-             1.5
Ken Knudson-                    1.5
Erik Engebetson-              .5
Josiah Keller                       1.5

PLEASE SUBTRACT FROM FLAG TIME

Sam Summers-                 3.7




Joel T. Dangerfield
07/07/2020
for pay period 06/21 -> 07/04
*/
select min(the_date), max(the_date) from dds.dim_date where biweekly_pay_period_sequence = 301;

insert into hs.additional_flag_hours values 
(301, '273315', .9, 'joel forgot to do adjustments before payperiod ended'), --brandon
(301, '256984', 5, 'joel forgot to do adjustments before payperiod ended'), --wyatt
(301, '2130150', 2.8, 'joel forgot to do adjustments before payperiod ended'), --paul
(301, '241085', 1.5, 'joel forgot to do adjustments before payperiod ended'), --garrett
(301, '279630', 1.5, 'joel forgot to do adjustments before payperiod ended'), --ken
(301, '276431', .5, 'joel forgot to do adjustments before payperiod ended'), --erik
(301, '295148', 1.5, 'joel forgot to do adjustments before payperiod ended') --josiah
------------------------------------------------------------------------
--/> 07/07/20
------------------------------------------------------------------------

drop table if exists hs.additional_flag_hours;
create table hs.additional_flag_hours (
  biweekly_pay_period_sequence integer not null,
  employee_number citext not null,
  flag_hours numeric(6,2) not null,
  reason citext not null);

select * from hs.additional_flag_hours

select distinct biweekly_pay_period_sequence
from dds.dim_date
where the_date = current_date - 7

insert into hs.additional_flag_hours values 
(248, (select employee_number from hs.main_shop_flat_rate_techs where tech_number = '15'), 4, 'joel forgot to do adjustments before payperiod ended'),
(248, (select employee_number from hs.main_shop_flat_rate_techs where tech_number = '31'), 4.8, 'joel forgot to do adjustments before payperiod ended'),
(248, (select employee_number from hs.main_shop_flat_rate_techs where tech_number = '642'), 1.5, 'joel forgot to do adjustments before payperiod ended'),
(248, (select employee_number from hs.main_shop_flat_rate_techs where tech_number = '600'), 1.5, 'joel forgot to do adjustments before payperiod ended'),
(248, (select employee_number from hs.main_shop_flat_rate_techs where tech_number = '130'), 2.8, 'joel forgot to do adjustments before payperiod ended'),
(248, (select employee_number from hs.main_shop_flat_rate_techs where tech_number = '69'), 1.5, 'joel forgot to do adjustments before payperiod ended')



insert into hs.additional_flag_hours values 
(256, (select employee_number from hs.main_shop_flat_rate_techs where tech_number = '130'), 1, 'joel forgot to do adjustments before payperiod ended'),
(256, (select employee_number from hs.main_shop_flat_rate_techs where tech_number = '15'), 6.7, 'joel forgot to do adjustments before payperiod ended'),
(256, (select employee_number from hs.main_shop_flat_rate_techs where tech_number = '31'), 4.8, 'joel forgot to do adjustments before payperiod ended'),
(256, (select employee_number from hs.main_shop_flat_rate_techs where tech_number = '600'), 1.5, 'joel forgot to do adjustments before payperiod ended'),
(256, (select employee_number from hs.main_shop_flat_rate_techs where tech_number = '642'), 6, 'joel forgot to do adjustments before payperiod ended'),
(256, (select employee_number from hs.main_shop_flat_rate_techs where tech_number = '645'), 1.5, 'joel forgot to do adjustments before payperiod ended'),
(256, (select employee_number from hs.main_shop_flat_rate_techs where tech_number = '646'), 1.5, 'joel forgot to do adjustments before payperiod ended'),
(256, (select employee_number from hs.main_shop_flat_rate_techs where tech_number = '68'), 1.5, 'joel forgot to do adjustments before payperiod ended'),
(256, (select employee_number from hs.main_shop_flat_rate_techs where tech_number = '69'), .5, 'joel forgot to do adjustments before payperiod ended')


insert into hs.additional_flag_hours values 
(260, (select employee_number from hs.main_shop_flat_rate_techs where tech_number = '31'), 1.5, 'joel forgot to do adjustments before payperiod ended'),
(260, (select employee_number from hs.main_shop_flat_rate_techs where tech_number = '15'), 2.3, 'joel forgot to do adjustments before payperiod ended'),
(260, (select employee_number from hs.main_shop_flat_rate_techs where tech_number = '646'), 1.5, 'joel forgot to do adjustments before payperiod ended'),
(260, (select employee_number from hs.main_shop_flat_rate_techs where tech_number = '69'), 0.5, 'joel forgot to do adjustments before payperiod ended'),
(260, (select employee_number from hs.main_shop_flat_rate_techs where tech_number = '130'), 0.5, 'joel forgot to do adjustments before payperiod ended'),
(260, (select employee_number from hs.main_shop_flat_rate_techs where tech_number = '642'), 1.5, 'joel forgot to do adjustments before payperiod ended'),
(260, (select employee_number from hs.main_shop_flat_rate_techs where tech_number = '68'), 2.3, 'joel forgot to do adjustments before payperiod ended'),
(260, (select employee_number from hs.main_shop_flat_rate_techs where tech_number = '600'), 1.5, 'joel forgot to do adjustments before payperiod ended'),
(260, (select employee_number from hs.main_shop_flat_rate_techs where tech_number = '645'), 9.5, 'joel forgot to do adjustments before payperiod ended');


select a.*, b.employee_first_name
from hs.main_shop_flat_rate_Techs a
join arkona.xfm_pymast b on a.employee_number = b.pymast_employee_number
  and b.current_row
where thru_date > current_date
order by  b.employee_first_name;

do
$$
declare 
  _tech citext := 'brandon';
  _hours numeric(3,1) := 2.7;
  _message citext := 'joel had computer problems';
  _pay_period_seq integer := (
    select distinct biweekly_pay_period_sequence
    from dds.dim_date
    where the_date = current_date - 7);  
  _employee_number citext := (   
    select a.employee_number
    from hs.main_shop_flat_rate_Techs a
    join arkona.xfm_pymast b on a.employee_number = b.pymast_employee_number
      and b.current_row
    where thru_date > current_date
      and b.employee_first_name = _tech);
begin      
  insert into hs.additional_flag_hours values (_pay_period_seq, _employee_number, _hours, _message); 
end
$$;   


select * from hs.additional_flag_hours where biweekly_pay_period_sequence = 269

select sum(flag_hours) from hs.additional_flag_hours where biweekly_pay_period_sequence = 269

select b.employee_first_name, a.flag_hours 
from hs.additional_flag_hours a
join arkona.xfm_pymast b on a.employee_number = b.pymast_employee_number
  and b.current_row
where a.biweekly_pay_period_sequence = 269


select * from hs.additional_flag_hours

delete from hs.additional_flag_hours where biweekly_pay_period_sequence = 269 and flag_hours = 2.7