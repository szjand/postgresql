﻿

do $$
declare
  _from_date date;
  _thru_date date;
  _current_pay_period_seq integer;
  _pay_period_indicator integer := 0;
begin
  _current_pay_period_seq = (
    select biweekly_pay_period_sequence
    from dds.dim_date
    where the_Date = current_date);  
  _from_date = (
    select distinct biweekly_pay_period_start_date
    from dds.dim_date
    where biweekly_pay_period_sequence = _current_pay_period_seq + _pay_period_indicator);
  _thru_date = (
    select distinct biweekly_pay_period_end_date
    from dds.dim_date
    where biweekly_pay_period_sequence = _current_pay_period_seq + _pay_period_indicator);    
drop table if exists wtf;
create temp table wtf as
with 
  clock_hours as (
    select d.employee_number, sum(coalesce(clock_hours, 0)) as clock_hours
    from dds.dim_date a
    left join arkona.xfm_pypclockin b on a.the_date = b.the_date
    inner join hs.main_shop_flat_rate_techs d on b.employee_number = d.employee_number
      and d.thru_date > _from_date
    where a.the_date between _from_date and _thru_date
    group by d.employee_number),
  flag_hours as (
    select d.employee_number, sum(b.flaghours) as flag_hours
    from dds.dim_date a
    inner join ads.ext_fact_repair_order b on a.date_key = b.flagdatekey
    inner join ads.ext_dim_tech c on b.techkey = c.techkey
    inner join hs.main_shop_flat_rate_techs d on c.employeenumber = d.employee_number
      and d.thru_date > _from_date
    where a.the_date between _from_date and _thru_date
    group by d.employee_number),
  adjustments as (
    select a.tech_number, a.employee_number, arkona.db2_integer_to_date_long(b.trans_date) as date, 
      b.ro_number as ro, 'Adjust after Close' as type, sum(labor_hours) as flag_hours
    from hs.main_shop_flat_rate_techs a
    inner join arkona.ext_sdpxtim b on a.tech_number = b.technician_id
    where arkona.db2_integer_to_date_long(b.trans_date) between _from_date and _thru_date
      and a.thru_date > _from_date
    group by a.tech_number, a.employee_number, arkona.db2_integer_to_date_long(b.trans_date), b.ro_number
    having sum(labor_hours) <> 0),
  production as (
    select b.user_key as id, b.user_key as employee, sum(c.flag_hours) as flag_hours, 
      sum(d.clock_hours) as clock_hours, coalesce(sum(e.flag_hours), 0) as adjustments,
      round((sum(c.flag_hours) + coalesce(sum(e.flag_hours), 0))/sum(d.clock_hours), 2) as proficiency
    from hs.main_shop_flat_rate_techs a
    inner join nrv.users b on a.employee_number = b.employee_number
    left join flag_hours c on a.employee_number = c.employee_number
    left join clock_hours d on a.employee_number = d.employee_number
    left join adjustments e on a.employee_number = e.employee_number

    where a.thru_date > _from_date
    
    group by user_key)    

select * from production;
--   select (
--     select row_to_json(y) as honda_main_shop_flat_rate_manager
--     from (
--       select 'RY2' as id, _pay_period_indicator as pay_period_indicator,
--         (select sum(flag_hours) from flag_hours) as total_flag_hours,
--         (select sum(clock_hours) from clock_hours) as total_clock_hours,
--         coalesce((select sum(flag_hours) from adjustments), 0) as total_adjustments,
--         round((select sum(flag_hours) from flag_hours)/(select sum(clock_hours) from clock_hours), 2) as proficiency,
--         array( 
--           select b.user_key
--           from hs.main_shop_flat_rate_techs a
--           inner join nrv.users b on a.employee_number = b.employee_number) as techs) y) as honda_main_shop_flat_rate_manager,
--         ( -- array of json objects, one row per tech
--           select coalesce(json_agg(row_to_json(x)), '[]')
--           from (
--             select *
--             from production) x) as honda_main_shop_flat_rate_techs;
end $$;             


select * from wtf;