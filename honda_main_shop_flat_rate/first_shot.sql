﻿drop table if exists techs;
create temp table techs as
select a.employeenumber, a.techkey, b.name, a.technumber, b.*
-- select b.name,  b.employeenumber
from ads.ext_dim_tech a
left join ads.ext_edw_employee_dim b on a.employeenumber = b.employeenumber
  and b.currentrow = true
where a.technumber in ('600','642','31','69','130','15','643','644')
  and a.currentrow = true
  and a.storecode = 'RY2';

drop table if exists hs.main_shop_flat_rate_techs;
create table hs.main_shop_flat_rate_techs (
  employee_number citext not null,
  tech_number citext not null,
  flat_rate numeric(6,2) not null,
  pto_rate numeric(6,2) not null,
  from_date date not null, 
  thru_date date not null default '12/31/9999'::date,
  primary key (employee_number, thru_date));
create index on hs.main_shop_flat_rate_techs(tech_number);
insert into hs.main_shop_flat_rate_techs (employee_number,tech_number,flat_rate,pto_rate,from_date)values
('210073','600',18,18,'05/27/2018'),
('211201','642',20,20,'05/27/2018'),
('273315','31',27,27,'05/27/2018'),
('295148','69',18,18,'05/27/2018'),
('279630','130',23,23,'05/27/2018'),
('2130150','15',26.5,26.5,'05/27/2018'),
('265348','643',20,20,'05/27/2018'),
('298543','644',20,29,'05/27/2018');

-- need technumbers to get adjustments from arkona.ext_sdpxtim

create schema hs;
comment on schema hs is 'Honda Service, initially for main shop flat rate data and functions';

comment on function hs.get_pay_periods() is 'returns bi weekly pay periods as of a fixed start date in the format of
  mmm dd - mmm dd, yyyy and the sequence as the pay_period_indicator';

-- need some indexes on ads tables
create index on ads.ext_fact_repair_order(ro);  
create index on ads.ext_fact_repair_order(flagdatekey);  
create index on ads.ext_fact_repair_order(techkey);  
create index on ads.ext_dim_tech(employeenumber);
create index on ads.ext_dim_tech(techkey);
create index on hs.main_shop_flat_rate_techs(employee_number);

-- add PK to arkona.ext_xtim
create unique index on arkona.ext_sdpxtim(company_number, technician_id, trans_date, table_sequence);
create index on arkona.ext_sdpxtim(technician_id);
create index on arkona.ext_sdpxtim(arkona.db2_integer_to_date_long(trans_date));
create index on arkona.ext_sdpxtim(ro_number);

select * from arkona.ext_sdpxtim limit 100

select user_key
from hs.main_shop_flat_rate_techs a
inner join nrv.users b on a.employee_number = b.employee_number

select x.the_date , y.*, z.*
from dds.dim_date x
left join ( -- clock hours
  select b.the_date, c.employeenumber, sum(clockhours) as clockhours, sum(ptohours + vacationhours + holidayhours) as pto
  from ads.ext_edw_clock_hours_fact a  
  inner join dds.dim_Date b on a.datekey = b.date_key
    and b.the_date between '05/14/2018' and current_date
  inner join ads.ext_edw_employee_dim c on a.employeekey = c.employeekey
  inner join techs d on c.employeenumber = d.employeenumber
  group by b.the_date, c.employeenumber) y on x.the_date = y.the_date
left join ( -- flag hours
  select employeenumber, the_date, sum(flaghours) as flag_hours
  -- select b.the_date, a.ro, a.flaghours, c.*
  from ads.ext_fact_repair_order a
  inner join dds.dim_Date b on a.flagdatekey = b.date_key
    and b.the_date between '05/14/2018' and current_Date
  inner join techs c on a.techkey = c.techkey  
  group by employeenumber, the_date) z on x.the_date = z.the_date and y.employeenumber = z.employeenumber
where x.the_date between '05/14/2018' and current_date 
order by 



select * from hs.get_pay_periods()


KNUDSON, KENNETH;279630
BARON, CORY;210073
SOBOLIK, PAUL A;2130150
JOHNSON, BRANDON J;273315
KELLER, JOSIAH;295148
PROIS, JUSTIN;211201
PIERCE, BRETT;265348


select *
from nrv.users
where last_name = 'PIERCE'




SCHINDLER, TIM;298543
