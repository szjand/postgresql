﻿/*
3/13/23
Good morning!
Zak Louden has had a flat rate pay change form $22.00/hr to $24.00 hr that has been approved
Thanks,
Nickolas

Nick
If you want it to be effective for the last pay period, 2/26 -> 3/11, it will have to be 
an adjustment on the next pay period.  I will figure out what that adjustment should be and 
email it to you before 3/27 so you can add it to his payroll then
OK?
jon


*/

select b.first_name, b.last_name, a.* 
from hs.main_shop_flat_rate_techs a
join ukg.employees b on a.employee_number = b.employee_number
where last_name in ('louden')
order by last_name, from_date

update hs.main_shop_flat_rate_techs
set thru_date = '03/11/2023'
where employee_number = '275342'
  and thru_date > current_date;

insert into hs.main_shop_flat_rate_techs(employee_number,tech_number,flat_rate, pto_rate,from_date)
values('275342', '651', 24, 19, '03/12/2023');

-- now figure out the adjustment for pay period 2/26 - 3/11
select * from dds.dim_date where the_date = '03/10/2023'
select * from hs.get_hn_flat_rate_managers_payroll_approval_data(371) order by tech

commission paid for 2/26 -> 3/11/23  1390.18
commission due for 2/26 -> 3/11/23   1516.56
adjustment due in payroll for 3/12 -> 3/25/2023  126.38

/*
08/30/22
Jon,
I am sorry about the late notice but I put a raise in for Brandon Johnson that was suppose to take effect this past pay period.  I didn’t send you anything because Ben C. had not signed off on it.  He just did today.   Can you fix his payroll for the past 2 weeks?   35/hour flat rate.
Thank you,
Joel T. Dangerfield
*/

select b.first_name, b.last_name, a.* 
from hs.main_shop_flat_rate_techs a
join ukg.employees b on a.employee_number = b.employee_number
where last_name in ('johnson')
order by last_name, from_date

select * from hs.main_shop_flat_rate_techs where employee_number = '273315' order by from_date

update hs.main_shop_flat_rate_techs
set thru_date = '08/13/2022'
where employee_number = '273315'
  and thru_date > current_date;

insert into hs.main_shop_flat_rate_techs values('273315','31',35.00,46.58,'2022-08-14','9999-12-31');
  
select hs.update_hn_main_shop_flat_rate_payroll_data();



/*
Jon,
I was just informed that Dennis McVeigh should be 31.50/hr.   he is in vision at 24.00/hr.
Thank you,
7/18/22
Joel T. Dangerfield
*/
select b.first_name, b.last_name, a.* 
from hs.main_shop_flat_rate_techs a
join ukg.employees b on a.employee_number = b.employee_number
where last_name in ('mcveigh','sobolik','knudson','johnson')
order by last_name, from_date

update hs.main_shop_flat_rate_techs
set flat_rate = 31.5
where thru_date > current_date
  and employee_number = '194675';
  
select hs.update_hn_main_shop_flat_rate_payroll_data();

/*
Jon,

I am sorry for the late notice but we changed pay for 3 techs last week.

Brandon Johnson is now 32/hour flate rate
Ken Knudson is now 30/hr flate rate

Paul Sobolik is no longer flat rate.  He will be $45/hour.


Thank you,


Joel T. Dangerfield
*/
select b.first_name, b.last_name, a.* 
from hs.main_shop_flat_rate_techs a
join ukg.employees b on a.employee_number = b.employee_number
where last_name in ('mcveigh','sobolik','knudson','johnson')
order by last_name, from_date

-- johnson
update hs.main_shop_flat_rate_techs
set thru_date = '07/02/02022'
where thru_date > current_date
  and employee_number = '273315';

insert into hs.main_shop_flat_rate_techs values('273315','31',32,46.58,'07/03/2022','12/31/9999'); 

-- knudson
update hs.main_shop_flat_rate_techs
set thru_date = '07/02/02022'
where thru_date > current_date
  and employee_number = '279630';

insert into hs.main_shop_flat_rate_techs values('279630','130',30,32.26,'07/03/2022','12/31/9999'); 

-- sobolik
update hs.main_shop_flat_rate_techs
set thru_date = '07/16/02022'
where thru_date > current_date
  and employee_number = '2130150';




/*
01/18/21
Jon,

Brandon Johnson & Erik Engebretson should have had pay increases on their last payroll.  
I sent everything through compli and it was approved but I did not remember to send to you.  
Can you please send Kim the information needed to back pay them and current pay period at the following dollar amounts.

Brandon J-    was 28.50 but increases to  $30.00/hr
Erik E.-      was 19.50 but increases to  $21.50/hr

Joel T. Dangerfield

for the retroactive part see sql\postgresql\honda_main_shop_flat_rate_\maintenance\knudson_sobolik_retroactive_raises

*/

select b.last_name, b.first_name, a.* 
from hs.main_shop_flat_rate_techs a
join ukg.employees b on a.employee_number = b.employee_number
where a.thru_date > current_date

johnson: 273315
engebretson 276431

update hs.main_shop_flat_rate_techs
set thru_date = '12/18/2021'
where employee_number = '273315'
  and thru_date = '12/31/9999';
-- create the new row
insert into hs.main_shop_flat_rate_techs values('273315','31',30,46.58,'12/19/2021');

update hs.main_shop_flat_rate_techs
set thru_date = '12/18/2021'
where employee_number = '276431'
  and thru_date = '12/31/9999';
-- create the new row
insert into hs.main_shop_flat_rate_techs values('276431','646',21.50,19.26,'12/19/2021');

/*
Samantha Summers was given a $1.00/hr raise.   
That should be reflected on her paycheck this pay period.  
Sorry about the late notice.
Joel T. Dangerfield
12/07/20
*/
-- select * from arkona.ext_pymast where employee_last_name = 'summers'
select b.employee_name, a.*
from hs.main_shop_flat_rate_techs a
join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
 where employee_number in ('264852')
order by employee_number, from_date; 

-- update the current row
update hs.main_shop_flat_rate_techs
set thru_date = '11/21/2020'
where employee_number = '264852'
  and thru_date = '12/31/9999';
-- create the new row
insert into hs.main_shop_flat_rate_techs values('264852','652',20,20,'11/22/2020');

/*
Good Morning Jon-

We’ve had some confusion with raises for Ken and Paul at Honda that should have been effective 8/16/2020.  
Paul is moving to $28.00 and Ken is moving to $24.50.  Both of these guys are expecting this raise on their paychecks this week.  

Since the paychecks have already been printed, can you please help us figure out what they are back owed 
for everything and then Kim will cut them additional checks?

Thanks!

Laura 
*/
select b.employee_name, a.*
from hs.main_shop_flat_rate_techs a
join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
 where employee_number in ('279630','2130150')
order by employee_number, from_date; 

-- update the current row
update hs.main_shop_flat_rate_techs
set thru_date = '08/15/2020'
where employee_number = '279630'
  and thru_date = '12/31/9999';
-- create the new row
insert into hs.main_shop_flat_rate_techs values('279630','130',24.5,32.26,'08/16/2020');

update hs.main_shop_flat_rate_techs
set thru_date = '08/15/2020'
where employee_number = '2130150'
  and thru_date = '12/31/9999';
-- create the new row
insert into hs.main_shop_flat_rate_techs values('2130150','15',28,38.09,'08/16/2020');  


09/02/20
brandon johnson gets a raise from 27 to 28 backdated to 08/02/2020

select * 
from hs.main_shop_flat_rate_techs
where thru_date > current_date

update hs.main_shop_flat_rate_techs
set thru_date = '08/01/2020'
where thru_date > current_date
  and employee_number = '273315';

insert into hs.main_shop_flat_rate_techs values('273315','31',28,46.58,'08/02/2020','12/31/9999'); 