﻿/*
Good Morning Jon-

We’ve had some confusion with raises for Ken and Paul at Honda that should have been effective 8/16/2020.  
Paul is moving to $28.00 and Ken is moving to $24.50.  Both of these guys are expecting this raise on their paychecks this week.  

Since the paychecks have already been printed, can you please help us figure out what they are back owed 
for everything and then Kim will cut them additional checks?

Thanks!

Laura Roth 
*/

----------------------------------------------------------------------------------------
--< 10/08/20
--shit, forgot to update hs.main_shop_flat_rate_techs
----------------------------------------------------------------------------------------
see ... python projects/honda_main_shop_flat_rate/rate_change.sql
----------------------------------------------------------------------------------------
--/> 10/08/20
--shit, forgot to update hs.main_shop_flat_rate_techs
----------------------------------------------------------------------------------------

select b.employee_name, a.*
from hs.main_shop_flat_rate_techs a
join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
 where employee_number in ('279630','2130150');

select the_date, biweekly_pay_period_start_date,biweekly_pay_period_end_date, biweekly_pay_period_sequence
from dds.dim_date 
where the_Date between '12/15/2021' and current_date

8/16 -8/29  305
8/30 - 9/12 306
9/13 - 9/26 307


/*
01/18/21
Jon,

Brandon Johnson & Erik Engebretson should have had pay increases on their last payroll.  
I sent everything through compli and it was approved but I did not remember to send to you.  
Can you please send Kim the information needed to back pay them and current pay period at the following dollar amounts.

Brandon J-    was 28.50 but increases to  $30.00/hr
Erik E.-      was 19.50 but increases to  $21.50/hr

Joel T. Dangerfield

for the retroactive part see sql\postgresql\honda_main_shop_flat_rate_\maintenance\knudson_sobolik_retroactive_raises

*/

if this comes up again:
	1. get what was actually paid from ukg
	2. update hs.main_shop_flat_rate_techs with the new retroactive rates
	3. run this script for the appropriate pay period to see what should have been paid

do
$$
declare
  _from_date date;
  _thru_date date;
  _current_pay_period_seq integer;
  _pay_period_indicator integer := 0;
begin
  _current_pay_period_seq = 341;  
  _from_date = (
    select distinct biweekly_pay_period_start_date
    from dds.dim_date
    where biweekly_pay_period_sequence = _current_pay_period_seq + _pay_period_indicator);
  _thru_date = (
    select distinct biweekly_pay_period_end_date
    from dds.dim_date
    where biweekly_pay_period_sequence = _current_pay_period_seq + _pay_period_indicator);    
drop table if exists wtf;
create temp table wtf as
with 
  clock_hours as ( -- from kim, add pto_hours for mgr
    select e.*,
--       case when e.total_hours > 90 then e.total_hours - 90 else 0 end as bonus_hours,
      pto_vac_hours + hol_hours as pto_hours -- for  mgr only
    from (
      select d.employee_number,
        sum(coalesce(clock_hours, 0)) as clock_hours, 
        sum(coalesce(pto_hours, 0) + coalesce(vac_hours, 0)) as pto_vac_hours,
        sum(coalesce(hol_hours, 0)) as hol_hours,
        sum(coalesce(clock_hours, 0) + coalesce(pto_hours, 0) + coalesce(vac_hours, 0) + coalesce(hol_hours, 0)) as total_hours
      from dds.dim_date a
    left join arkona.xfm_pypclockin b on a.the_date = b.the_date
    inner join hs.main_shop_flat_rate_techs d on b.employee_number = d.employee_number
      and d.thru_date > _thru_date
    where a.the_date between _from_date and _thru_date
    group by d.employee_number) e),
  flag_hours as (
    select d.employee_number, sum(b.flaghours) as flag_hours
    from dds.dim_date a
    inner join ads.ext_fact_repair_order b on a.date_key = b.flagdatekey
    inner join ads.ext_dim_tech c on b.techkey = c.techkey
    inner join hs.main_shop_flat_rate_techs d on c.employeenumber = d.employee_number
      and d.thru_date > _thru_date
    where a.the_date between _from_date and _thru_date
    group by d.employee_number),
  adjustments as (
    select a.tech_number, a.employee_number, sum(labor_hours) as flag_hours
    from hs.main_shop_flat_rate_techs a
    inner join arkona.ext_sdpxtim b on a.tech_number = b.technician_id
    where arkona.db2_integer_to_date_long(b.trans_date) between _from_date and _thru_date
      and a.thru_date > _thru_date
    group by a.tech_number, a.employee_number
    having sum(labor_hours) <> 0),
  additional_flag_hours as (    
    select a.employee_number, coalesce(sum(b.flag_hours), 0) as flag_hours
    from hs.main_shop_flat_rate_techs a
    left join hs.additional_flag_hours b on a.employee_number = b.employee_number
      and b.biweekly_pay_period_sequence = _current_pay_period_seq + _pay_period_indicator
    where a.thru_date > _thru_date
    group by a.employee_number),        
  production as (
    select a.employee_number, coalesce(sum(c.flag_hours),0) as total_flag_hours, 
      coalesce(sum(d.clock_hours),0) as total_clock_hours, coalesce(sum(e.flag_hours), 0) as total_adjustments,
      case 
        when coalesce(sum(d.clock_hours), 0) = 0 
          then 0 
      else  round((sum(c.flag_hours + f.flag_hours) + coalesce(sum(e.flag_hours), 0))/sum(d.clock_hours), 2)  end as proficiency
    from hs.main_shop_flat_rate_techs a
    left join flag_hours c on a.employee_number = c.employee_number
    left join clock_hours d on a.employee_number = d.employee_number
    left join adjustments e on a.employee_number = e.employee_number
    left join additional_flag_hours f on a.employee_number = f.employee_number
    where a.thru_date > _thru_date
    group by a.employee_number)
--   corona_guarantee as (
--     select *
--     from hs.corona_guarantee)   
  -- wtf
  select y.*, 
    round(flat_rate * flag_hours, 2) as flat_rate_pay, bonus_hours * bonus_rate as bonus_pay, pto_rate * pto_hours as pto_pay,
    round((flat_rate * flag_hours) + (bonus_hours * bonus_rate) + (pto_rate * pto_hours), 2) as total_pay,
    round(pto_rate * hol_hours, 2) as hol_pay,
    round(pto_vac_hours * pto_rate, 2) as pto_vac_pay
  from (
    select x.*, 
      case 
        when proficiency < 1 then 0
        else 
          case 
            when x.total_hours > 90 then x.total_hours - 90 
            else 0
          end
      end as bonus_hours
    from (
      select c.name, /*c.employee_name,*/ a.employee_number, a.flat_rate, 2 * a.flat_rate as bonus_rate, a.pto_rate , 
        b.total_flag_hours + b.total_adjustments + aa.flag_hours as flag_hours, 
        coalesce((select clock_hours from clock_hours where employee_number = a.employee_number), 0) as clock_hours,
        coalesce((select pto_hours from clock_hours where employee_number = a.employee_number), 0) as pto_hours,
        coalesce((select clock_hours from clock_hours where employee_number = a.employee_number), 0)
        +
        coalesce((select pto_hours from clock_hours where employee_number = a.employee_number), 0)  as total_hours,
        b.proficiency,
        coalesce((select hol_hours from clock_hours where employee_number = a.employee_number), 0) as hol_hours,
        coalesce((select pto_vac_hours from clock_hours where employee_number = a.employee_number), 0) as pto_vac_hours
--         (select guarantee from corona_guarantee where employee_number = a.employee_number) as guarantee
      from hs.main_shop_flat_rate_techs a
      left join production b on a.employee_number = b.employee_number
      left join additional_flag_hours aa on a.employee_number = aa.employee_number
      left join ads.ext_edw_employee_dim c on b.employee_number = c.employeenumber and c.currentrow = true
      where a.thru_date > _thru_date
        and a.employee_number <> '210073'
    order by name) x) y;



drop table if exists manager;
create temp table manager as
select name,employee_number,flat_rate,bonus_rate,pto_rate,flag_hours,clock_hours,pto_hours,total_hours,
  proficiency,bonus_hours,flat_rate_pay,bonus_pay,pto_pay,total_pay as total_earned --, guarantee,
--   case 
--     when guarantee > total_pay then guarantee
--     else total_pay
--   end as actual_pay
from wtf;

drop table if exists payroll;
create temp table payroll as
select name,employee_number,flat_rate,flag_hours,flat_rate_pay,bonus_rate,bonus_hours,bonus_pay,pto_rate,pto_vac_hours,
  pto_vac_pay,hol_hours,hol_pay,total_pay as total_earned --, guarantee,
--   case 
--     when guarantee > total_pay then guarantee
--     else total_pay
--   end as actual_pay  
from wtf;    

end $$;


select * from payroll where employee_number in ('273315','276431');

select * from ukg.pay_plan_biweekly_commissions_snapshots where employee_number in ('273315','276431');

brandon			paid				due          diff
flat rate: 2032.80     2178.00      145.20
bonus: 	    414.96      444.60       29.64
           2447.76     2622.60      174.84
eric
flat rate: 1669.20     1840.40      171.20
bonus:	    280.41      309.17       28.76
           1949.61     2149.57      199.96

fuck me, dont know how it happened, but erik and brandon got under paid for the pay period of 1/2 -> 1/15
brandon			paid				due          diff
flat rate: 3262.00     3495.00      233.00
bonus: 	    186.48      199.80       13.32
           3448.48     3694.80      246.32

eric
flat rate: 1380.60     1522.20      171.20
bonus:	    