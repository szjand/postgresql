﻿-- had already populated hs.hn_main_shop_flat_rate_payroll_data with data for pay seq 342, get rid of that data

delete 
-- select * 
from hs.hn_main_shop_flat_rate_payroll_data where pay_period_seq = 342;

-- save production data in a temp table
create temp table the_data as
select * from hs.hn_main_shop_flat_rate_payroll_data;

-- all the column names from real table
select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'hs'
  and table_name = 'hn_main_shop_flat_rate_payroll_data';


-- drop and recreate the production table with the tool allow attribute
-- DROP TABLE hs.hn_main_shop_flat_rate_payroll_data;

CREATE TABLE hs.hn_main_shop_flat_rate_payroll_data
(
  pay_period_start date NOT NULL,
  pay_period_end date NOT NULL,
  pay_period_seq integer NOT NULL,
  pay_period_select_format citext NOT NULL,
  first_saturday date NOT NULL,
  last_name citext NOT NULL,
  first_name citext NOT NULL,
  employee_number citext NOT NULL,
  hire_date date NOT NULL,
  term_date date NOT NULL,
  flag_hours numeric NOT NULL,
  adjustments numeric NOT NULL,
  total_flag_hours numeric NOT NULL,
  clock_hours numeric NOT NULL,
  pto_hours numeric NOT NULL,
  hol_hours numeric NOT NULL,
  total_hours numeric NOT NULL,
  prof numeric NOT NULL,
  flat_rate numeric NOT NULL,
  bonus_rate numeric NOT NULL,
  comm_hours numeric NOT NULL,
  bonus_hours numeric NOT NULL,
  comm_pay numeric NOT NULL,
  bonus_pay numeric NOT NULL,
  tool_allow numeric not null,
  total_pay numeric NOT NULL,
  CONSTRAINT hn_main_shop_flat_rate_payroll_data_pkey PRIMARY KEY (pay_period_seq, employee_number));
COMMENT ON TABLE hs.hn_main_shop_flat_rate_payroll_data
  IS 'updated every sunday by a call to function tp.update_hn_main_shop_flat_rate_payroll_data() in luigi ukg_payroll_import_data.py. 
  table for storing all necessary hn main shop flat rate data to generate ukg import files and manager approval pages';

-- insert the real data into the reconstructed production table
insert into hs.hn_main_shop_flat_rate_payroll_data
select pay_period_start,pay_period_end,pay_period_seq,pay_period_select_format,first_saturday,last_name,
	first_name,employee_number,hire_date,term_date,flag_hours,adjustments,total_flag_hours,clock_hours,
	pto_hours,hol_hours,total_hours,prof,flat_rate,bonus_rate,comm_hours,bonus_hours,comm_pay,
	bonus_pay, 0 as tool_allow,total_pay
from the_data;  



do $$
declare 
	_pay_period_seq integer := (
	  select distinct biweekly_pay_period_sequence
	  from dds.dim_date
	  where the_date = current_date - 1);
	_from_date date := (
	  select distinct biweekly_pay_period_start_date
	  from dds.dim_date 
	  where biweekly_pay_period_sequence = _pay_period_seq);
	_thru_date date := (
	  select distinct biweekly_pay_period_end_date
	  from dds.dim_date 
	  where biweekly_pay_period_sequence = _pay_period_seq);	
begin 		
--   delete 
--   from  hs.hn_main_shop_flat_rate_payroll_data
--   where pay_period_seq = _pay_period_seq;
--   
-- 	insert into hs.hn_main_shop_flat_rate_payroll_data


drop table if exists wtf;
create temp table wtf as
	select aa.*, 
-- 		aa.flat_rate * aa.comm_hours * aa.prof as comm_pay,
		aa.flat_rate * flag_hours as comm_pay,
-- 		aa.bonus_rate * aa.bonus_hours * aa.prof as bonus_pay,
		aa.bonus_rate * aa.bonus_hours as bonus_pay, 68.42::numeric as tool_allow,
		(aa.flat_rate * aa.comm_hours * aa.prof) + (aa.bonus_rate * aa.bonus_hours * aa.prof) + 68.42
	from (
		with 
			clock_hours as ( 
				select d.employee_number,
					sum(coalesce(clock_hours, 0)) as clock_hours, 
					sum(coalesce(pto_hours, 0)) as pto_hours,
					sum(coalesce(hol_hours, 0)) as hol_hours,
					sum(coalesce(clock_hours, 0) + coalesce(pto_hours, 0) + coalesce(hol_hours, 0)) as total_hours
				from dds.dim_date a
				join ukg.clock_hours b on a.the_date = b.the_date
				inner join hs.main_shop_flat_rate_techs d on b.employee_number = d.employee_number
					and d.thru_date > _thru_date
				where a.the_date between _from_date and _thru_date
				group by d.employee_number),
			flag_hours as (
		--     create temp table flag_hours as 
				select d.employee_number, sum(b.flaghours) as flag_hours
				from dds.dim_date a
				inner join ads.ext_fact_repair_order b on a.date_key = b.flagdatekey
				inner join ads.ext_dim_tech c on b.techkey = c.techkey
				inner join hs.main_shop_flat_rate_techs d on c.employeenumber = d.employee_number
					and d.thru_date > _thru_date
				where a.the_date between _from_date and _thru_date
				group by d.employee_number),
			adjustments as (
		--     create temp table adjustments as
				select a.tech_number, a.employee_number, sum(labor_hours) as flag_hours
				from hs.main_shop_flat_rate_techs a
				inner join arkona.ext_sdpxtim b on a.tech_number = b.technician_id
				where arkona.db2_integer_to_date_long(b.trans_date) between _from_date and _thru_date
					and a.thru_date > _thru_date
				group by a.tech_number, a.employee_number
				having sum(labor_hours) <> 0)    
		select 
			_from_date, _thru_date, _pay_period_seq,
			trim(to_char(_from_date, 'Month')) || ' ' || extract(day from _from_date) || ' - ' || trim(to_char(_thru_date, 'Month')) || ' ' || extract(day from _thru_date) || ', ' || extract(year from _from_date) as whatever,
			_from_date + 6 as first_saturday,
			aa.last_name, aa.first_name, a.employee_number, 
			aa.hire_date, aa.term_date as term_date,  
			c.flag_hours, coalesce(d.flag_hours, 0) as adjustments,
			c.flag_hours + coalesce(d.flag_hours,0) as total_flag_hours,
			b.clock_hours, b.pto_hours, b.hol_hours, b.total_hours,
			case
				when b.clock_hours = 0 then 0
				else coalesce(c.flag_hours + coalesce(d.flag_hours,0), 0) / b.clock_hours 
			end as prof,
			a.flat_rate, 2 * flat_rate as bonus_rate,
			case
				when b.total_hours > 90 
					and  
						case
							when b.clock_hours = 0 then 0
							else coalesce(c.flag_hours + coalesce(d.flag_hours,0), 0) / b.clock_hours 
						end >= 1 then b.clock_hours - (b.total_hours - 90)
				else b.clock_hours
			end as comm_hours,
			case
				when b.total_hours > 90 
					and  
						case
							when b.clock_hours = 0 then 0
							else coalesce(c.flag_hours + coalesce(d.flag_hours,0), 0) / b.clock_hours 
						end >= 1 then b.total_hours - 90
				else 0
			end as bonus_hours
		-- select a.*  
		from hs.main_shop_flat_rate_techs a
		join ukg.employees aa on a.employee_number = aa.employee_number
		join clock_hours b on a.employee_number = b.employee_number
		join flag_hours c on a.employee_number = c.employee_number
		left join adjustments d on a.employee_number = d.employee_number
		where a.thru_date > _thru_date
			and a.employee_number <> '210073') aa;
end $$;
select * from wtf;


