﻿
-- original
		select pay_period_select_format, (last_name || ', ' || first_name)::citext as tech, 
			employee_number, flat_rate,bonus_rate, round(prof, 4) as prof,
			flag_hours,round(adjustments,2) as adjustments, 
			round(total_flag_hours,2) as total_flag_hours,round(clock_hours,4) as clock_hours, 
			pto_hours, hol_hours, total_hours,
			round(comm_hours, 4) as comm_hours, round(comm_pay, 2) as comm_pay,
			bonus_hours, round(bonus_pay, 2) as bonus_pay, round(comm_pay + bonus_pay, 2) as total_pay, tool_allow
		from hs.hn_main_shop_flat_rate_payroll_data
		where pay_period_seq = 388 --_pay_period_seq
		order by tech; 


			select pay_period_select_format, (last_name || ', ' || first_name)::citext as tech, 
				employee_number, flat_rate,bonus_rate, round(prof, 4) as prof,
				flag_hours,round(adjustments,2) as adjustments, 
				round(total_flag_hours,2) as total_flag_hours,round(clock_hours,4) as clock_hours, 
				pto_hours, hol_hours, total_hours,
				round(comm_hours, 4) as comm_hours, round(comm_pay, 2) as comm_pay,
				bonus_hours, round(bonus_pay, 2) as bonus_pay, 
				case
					when employee_number = '168753' then -- and round(prof, 4) < 1.3 then (1.3 * round(comm_pay, 2))
						case
							when round(prof, 4) < 1.3 then (1.3 * round(comm_pay, 2))
							else 0
						end
				end  as guarantee,
				tool_allow
			from hs.hn_main_shop_flat_rate_payroll_data
			where pay_period_seq = 388  --_pay_period_seq


			select pay_period_select_format, (last_name || ', ' || first_name)::citext as tech, 
				employee_number, flat_rate,bonus_rate, round(prof, 4) as prof,
				flag_hours,round(adjustments,2) as adjustments, 
				round(total_flag_hours,2) as total_flag_hours,round(clock_hours,4) as clock_hours, 
				pto_hours, hol_hours, total_hours,
				round(comm_hours, 4) as comm_hours, round(comm_pay, 2) as comm_pay,
				bonus_hours, round(bonus_pay, 2) as bonus_pay, 
				case
					when employee_number = '168753' and round(prof, 4) < 1.3 then (1.3 * round(comm_pay, 2))
					else 0
				end  as guarantee,
				tool_allow
			from hs.hn_main_shop_flat_rate_payroll_data
			where pay_period_seq = 388  --_pay_period_seq			


    select a.*, 
      case
        when employee_number = '168753' and prof < 1.3 then round(bonus_pay + guarantee, 2) 
        else round(comm_pay + bonus_pay, 2) 
      end as total_pay
    from (
			select pay_period_select_format, (last_name || ', ' || first_name)::citext as tech, 
				employee_number, flat_rate,bonus_rate, round(prof, 4) as prof,
				flag_hours,round(adjustments,2) as adjustments, 
				round(total_flag_hours,2) as total_flag_hours,round(clock_hours,4) as clock_hours, 
				pto_hours, hol_hours, total_hours,
				round(comm_hours, 4) as comm_hours, round(comm_pay, 2) as comm_pay,
				bonus_hours, round(bonus_pay, 2) as bonus_pay, 
				case
					when employee_number = '168753' and round(prof, 4) < 1.3 then (1.3 * round(comm_pay, 2))
					else 0
				end  as guarantee,
				tool_allow
			from hs.hn_main_shop_flat_rate_payroll_data			
			where pay_period_seq = 388) a --_pay_period_seq			
			order by tech

    select pay_period_select_format,tech,employee_number,flat_rate,bonus_rate,prof,flag_hours
      adjustments,total_flag_hours,clock_hours,pto_hours,hol_hours,total_hours,comm_hours,
      comm_pay,bonus_hours,bonus_pay,guarantee,
      case
        when employee_number = '168753' and prof < 1.3 then round(bonus_pay + guarantee, 2) 
        else round(comm_pay + bonus_pay, 2) 
      end as total_pay,
      tool_allow
    from (
			select pay_period_select_format, (last_name || ', ' || first_name)::citext as tech, 
				employee_number, flat_rate,bonus_rate, round(prof, 4) as prof,
				flag_hours,round(adjustments,2) as adjustments, 
				round(total_flag_hours,2) as total_flag_hours,round(clock_hours,4) as clock_hours, 
				pto_hours, hol_hours, total_hours,
				round(comm_hours, 4) as comm_hours, round(comm_pay, 2) as comm_pay,
				bonus_hours, round(bonus_pay, 2) as bonus_pay, 
				case
					when employee_number = '168753' and round(prof, 4) < 1.3 then (1.3 * round(comm_pay, 2))
					else 0
				end  as guarantee,
				tool_allow
			from hs.hn_main_shop_flat_rate_payroll_data			
			where pay_period_seq = 388) a --_pay_period_seq			
			order by tech			