﻿/*
11/2/23
Joshua Northagen is being loaned to Honda for 3 months
*/
select * from hs.main_shop_flat_rate_techs

insert into hs.main_shop_flat_rate_techs(employee_number,tech_number,flat_Rate,pto_rate,from_date,thru_date) 
values('168753','294',23.40,23.40,'10/22/2023','12/31/9999');

need to figure his 130% guarantee
        path: '/hn-main-shop-flat-rate-payrolls',
        options: {
          tags: tags,
          description: 'Get HN main shop estimator payroll',
          handler: async function(request, h) {
            const query = 'select * from  ukg.get_hn_main_shop_flat_rate_payroll($1) as result';

	inner join hs.get_hn_flat_rate_managers_payroll_approval_data(_pay_period_ind)
-- and this is the final version of that function implementing the guarantee
    select pay_period_select_format,tech,employee_number,flat_rate,bonus_rate,prof,flag_hours,
      adjustments,total_flag_hours,clock_hours,pto_hours,hol_hours,total_hours,comm_hours,
      comm_pay,bonus_hours,bonus_pay,guarantee,
      case
        when employee_number = '168753' and prof < 1.3 then round(bonus_pay + guarantee, 2) 
        else round(comm_pay + bonus_pay, 2) 
      end as total,
      tool_allow
    from (
			select pay_period_select_format, (last_name || ', ' || first_name)::citext as tech, 
				employee_number, flat_rate,bonus_rate, round(prof, 4) as prof,
				flag_hours,round(adjustments,2) as adjustments, 
				round(total_flag_hours,2) as total_flag_hours,round(clock_hours,4) as clock_hours, 
				pto_hours, hol_hours, total_hours,
				round(comm_hours, 4) as comm_hours, round(comm_pay, 2) as comm_pay,
				bonus_hours, round(bonus_pay, 2) as bonus_pay, 
				case
					when employee_number = '168753' and round(prof, 4) < 1.3 then round(1.3 * comm_pay, 2)
					else 0
				end  as guarantee,
				tool_allow
			from hs.hn_main_shop_flat_rate_payroll_data
			where pay_period_seq = 388) a --_pay_period_seq) a
		order by tech; 	

/*
11/03/22
Good afternoon,
I talked with Andy and he suggested I email you because I have 4 tech’s going flat rate next pay period….
Zak Louden 22.00/hr
Elijah Goetz  21.00/hr
Josh Olson  32.00/hr
Cory Batzer  22.00/hr
*/
select * from dds.dim_tech limit 20

select * from hs.main_shop_flat_rate_techs where thru_date > current_date

insert into hs.main_shop_flat_rate_techs
select -- a.last_name, a.first_name, 
	a.employee_number, b.tech_number,
  case a.last_name
    when 'louden' then 22
    when 'goetz' then 21
    when 'olson' then 32
    when 'batzer' then 22
  end as flat_rate,
  case a.last_name
    when 'louden' then 19
    when 'goetz' then 19
    when 'olson' then 30
    when 'batzer' then 20
  end as pto_rate, 
  '11/06/2022', '12/31/9999'
from ukg.employees a
join dds.dim_tech b on a.employee_number = b.employee_number
  and b.row_thru_date > current_date
  and b.active
  and b.current_row
where last_name in ('louden','goetz','batzer') or (last_name = 'olson' and first_name = 'joshua');

/*
07/05/2022
Dennis McVeigh is "temporarily" to honda nissan
guaranteed 130%, like axtman
*/

select b.last_name, b.first_name, a.* from hs.main_shop_flat_rate_techs a
left join ukg.employees b on a.employee_number = b.employee_number
where thru_date > current_date

insert into hs.main_shop_flat_rate_techs values('194675','194675',24,24,'07/03/2022','12/31/9999');

/*
Just a heads up…………pat barta will be going to flat rate next pay period………24/hour     8/30/20

Joel T. Dangerfield
08/20/2020
*/

insert into hs.main_shop_flat_rate_techs values('210123','650',31.50,32.07,'07/30/2020','12/31/9999');

select * from dds.dim_tech where tech_name like 'barta%'

/*
NEED SAM SUMMERS ON THIS ALSO…..    19/HR FLAT RATE

Joel T. Dangerfield
08/04/20
*/

select * 
from hs.main_shop_flat_rate_techs a 
where thru_date > current_date

insert into hs.main_shop_flat_rate_techs values('264852','652',19,19,'07/19/2020','12/31/9999')

/*
Wyatt Zapzalka termed on 07/28/2020

select b.employee_name, a.*
from hs.main_shop_flat_rate_techs a
join arkona.xfm_pymast b on a.employee_number = b.pymast_employee_number
  and b.current_row 
where a.thru_date > current_Date  
order by a.employee_number  
*/
update hs.main_shop_flat_rate_techs
set thru_date = '07/28/2020'
-- select * from hs.main_shop_flat_rate_techs
where employee_number = '256984'
  and thru_date > current_date;

  
/*
Juston Prois termed on 05/29/2020

select b.employee_name, a.*
from hs.main_shop_flat_rate_techs a
join arkona.xfm_pymast b on a.employee_number = b.pymast_employee_number
  and b.current_row 
where a.thru_date > current_Date  
order by a.employee_number  

*/

update hs.main_shop_flat_rate_techs
set thru_date = '05/29/2020'
-- select * from hs.main_shop_flat_rate_techs
where employee_number = '211201'
  and thru_date > current_date;
/*
Josiah Keller termed 5/1/2020

select b.employee_name, a.*
from hs.main_shop_flat_rate_techs a
join arkona.xfm_pymast b on a.employee_number = b.pymast_employee_number
  and b.current_row 
where a.thru_date > current_Date  
order by a.employee_number  

*/

update hs.main_shop_flat_rate_techs
set thru_date = '04/30/2020'
-- select * from hs.main_shop_flat_rate_techs
where employee_number = '295148'
  and thru_date > current_date;

/*
Erik will be 19.50 for all rates & Wyatt will be 19.00 for all rates

Thanks,


Joel T. Dangerfield
Service Manager
Rydell Honda / Nissan of Grand Forks
3220 South Washington St.
(P) 701-746-2020
(F) 701-775-2355

From: Jon Andrews [mailto:jandrews@cartiva.com] 
Sent: Monday, October 01, 2018 6:22 AM
To: 'Joel Dangerfield' <jdangerfield@gfhonda.com>
Subject: RE: flat rate

All I need from you is their flat rate & pto/holiday rate

From: Joel Dangerfield <jdangerfield@gfhonda.com> 
Sent: Sunday, September 30, 2018 5:34 PM
To: Jon Andrews <jandrews@cartiva.com>
Subject: flat rate

Jon,

Both Wyatt Zapzalka & Erik Engebretson are going to flat rate in October.   I put in request to compli but speaking to Laura the other day, she said I need to send you an email.  What do you need from me?

Thanks,


Joel T. Dangerfield

*/

new techs

select *
from ads.ext_dim_tech
where description like '%zapzal%'
  or description like '%engebret%'

delete 
from hs.main_shop_flat_rate_techs  
where employee_number in ('256984','276431');

insert into hs.main_shop_flat_rate_techs values 
('256984','645',19,19,'09/30/2018','12/31/9999'),
('276431','646',19.5,19.5,'09/30/2018','12/31/9999');