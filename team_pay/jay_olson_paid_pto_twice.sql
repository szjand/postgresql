﻿-- FUNCTION tp.get_gm_flat_rate_managers_payroll_approval_data(integer)
-- queries table tp.gm_main_shop_flat_rate_payroll_data
-- which is updated by FUNCTION tp.update_gm_main_shop_flat_rate_payroll_data()
-- which is recreated here:
-- the problem was jay olson was paid his pto of 1679.20 twice, the tpo was included in the commission

-- CREATE OR REPLACE FUNCTION tp.update_gm_main_shop_flat_rate_payroll_data()
--   RETURNS void AS
-- $BODY$
-- go ahead and hard code all the dates, need to see what the subqueries are returning

the fix is when the end result is the guarantee, subtract the pto pay

do $$
declare 
	_pay_period_seq integer := 342; --(
-- 	  select distinct biweekly_pay_period_sequence
-- 	  from dds.dim_date
-- 	  where the_date = current_date - 1);
	_from_date date := (
	  select distinct biweekly_pay_period_start_date
	  from dds.dim_date 
	  where biweekly_pay_period_sequence = _pay_period_seq);
	_thru_date date := (
	  select distinct biweekly_pay_period_end_date
	  from dds.dim_date 
	  where biweekly_pay_period_sequence = _pay_period_seq);	
begin 		
--   delete 
--   from  tp.gm_main_shop_flat_rate_payroll_data
--   where pay_period_seq = _pay_period_seq;
  
-- 	insert into tp.gm_main_shop_flat_rate_payroll_data

drop table if exists wtf;
create temp table wtf as
	select dd.*,
		round(comm_hours * team_prof/100 * flat_rate, 2) as comm_pay,
		round(bonus_hours * team_prof/100 * bonus_rate, 2) as bonus_pay,
		round(pto_rate * (pto_hours + hol_hours), 2) as pto_pay,
		68.42::numeric as tool_allow,
		case
			when coalesce(guarantee, 0) > round(comm_hours * team_prof/100 * flat_rate, 2) 
				+ round(bonus_hours * team_prof/100 * bonus_rate, 2)
				+ round(pto_rate * (pto_hours + hol_hours), 2) then guarantee + 68.42 - round(pto_rate * (pto_hours + hol_hours), 2)
			else round(comm_hours * team_prof/100 * flat_rate, 2) 
				+ round(bonus_hours * team_prof/100 * bonus_rate, 2) + 68.42
		end as total_pay
	from (
		select cc.*,
			case
				when cc.total_hours > 90 and cc.team_prof >= 100 then cc.clock_hours - (total_hours -90)
				else cc.clock_hours
			end as comm_hours,
			round(
				case
					when cc.total_hours > 90 and team_prof >= 100 then total_hours - 90
					else 0
				 end, 4) as bonus_hours
		from (
			select aa.*, bb.guarantee 
			from ( -- tech data
			  -- jay's team flat rate earnings:  select .7751 * 28.996 * 56.76  = 1275.67, assume that is half since he only worked ~40 hours, earnings would be 2551.34
			  -- but, that is still less than the guarantee figured below without the pto
				select pay_period_start, pay_period_end, pay_period_seq, pay_period_select_format,
					pay_period_start + 6 as first_saturday,
					c.team_name AS Team, a.last_name, a.first_name, a.employee_number, 
					d.hire_date, d.term_date,
					tech_hourly_rate AS pto_rate, round(tech_tfr_rate, 4) AS flat_rate, 
					2 * round(tech_tfr_rate, 4) AS bonus_rate,
					tech_flag_hours_pptd as flag_hours, tech_Flag_Hour_Adjustments_PPTD as Adjustments,
					tech_flag_hours_pptd + tech_Flag_Hour_Adjustments_PPTD as total_flag_hours,
					team_prof_pptd AS team_prof,
					tech_clock_hours_pptd AS clock_hours, 
					tech_pto_hours_pptd AS pto_hours, 
					tech_holiday_hours_pptd AS hol_hours,
					round(tech_clock_hours_pptd + tech_pto_hours_pptd + tech_holiday_hours_pptd, 2) AS total_hours            
				FROM tp.data a
				INNER JOIN tp.team_techs b on a.tech_key = b.tech_key
					AND a.team_key = b.team_key
				  AND '01/29/2022' BETWEEN b.from_date AND b.thru_date
				LEFT JOIN tp.teams c on b.team_key = c.team_key
				left join ukg.employees d on a.employee_number = d.employee_number
				WHERE a.the_date = '01/29/2022' -- current_date - 1 -- _thru_date -------------------------------------------------------------
					AND a.department_Key = 18) aa
			left join (-- jay's guarantee: 3624.80, guarantee should not have included the pto, so it should have been 3624.80 - 1679.20 = 1945.6
				SELECT employee_number, reg + ot + pto + bonus AS guarantee  
				FROM ( -- blueprint team guarantees
					SELECT a.name, a.employee_number, round(a.hourly_rate * b.clock, 2) AS reg, 
						round(a.hourly_rate * 1.5 * b.ot, 2) AS ot, 
						round(a.pto_rate * (pto + hol), 2) AS pto,
						case
							when b.clock + b.ot + b.pto > 90 THEN  ((b.clock + b.ot + b.pto) - 90) * 2 * a.hourly_rate
						ELSE 0
						END AS bonus
					FROM tp.blueprint_guarantee_rates a
					JOIN (			 
						select c.employee_number, SUM(a.reg_hours) AS clock, SUM(a.ot_hours) AS ot,
							SUM(a.pto_hours) AS pto, SUM(a.hol_hours) AS hol 
						FROM ukg.clock_hours a
						JOIN dds.dim_date b on a.the_date = b.the_date
							AND b.the_date BETWEEN '01/16/2022' and '01/29/2022'
						join tp.blueprint_guarantee_rates c on a.employee_number = c.employee_number
							and c.thru_date > current_date
						GROUP BY c.employee_number) b on a.employee_number = b.employee_number
					WHERE a.thru_date = '12/31/9999') d) bb on aa.employee_number = bb.employee_number) cc) dd;
end $$;

select * from wtf where team = 'pre-diagnosis';				



-- this shows 40 pto hours for jay
				select pay_period_start, pay_period_end, pay_period_seq, pay_period_select_format,
					pay_period_start + 6 as first_saturday,
					c.team_name AS Team, a.last_name, a.first_name, a.employee_number, 
					d.hire_date, d.term_date,
					tech_hourly_rate AS pto_rate, round(tech_tfr_rate, 4) AS flat_rate, 
					2 * round(tech_tfr_rate, 4) AS bonus_rate,
					tech_flag_hours_pptd as flag_hours, tech_Flag_Hour_Adjustments_PPTD as Adjustments,
					tech_flag_hours_pptd + tech_Flag_Hour_Adjustments_PPTD as total_flag_hours,
					team_prof_pptd AS team_prof,
					tech_clock_hours_pptd AS clock_hours, 
					tech_pto_hours_pptd AS pto_hours, 
					tech_holiday_hours_pptd AS hol_hours,
					round(tech_clock_hours_pptd + tech_pto_hours_pptd + tech_holiday_hours_pptd, 2) AS total_hours            
				FROM tp.data a
				INNER JOIN tp.team_techs b on a.tech_key = b.tech_key
					AND a.team_key = b.team_key
				  AND '01/29/2022' BETWEEN b.from_date AND b.thru_date
				LEFT JOIN tp.teams c on b.team_key = c.team_key
				left join ukg.employees d on a.employee_number = d.employee_number
				WHERE a.the_date = '01/29/2022' -- current_date - 1 -- _thru_date -------------------------------------------------------------
					AND a.department_Key = 18	
-- and this shows 40 hours pto for jay
						select c.employee_number, SUM(a.reg_hours) AS clock, SUM(a.ot_hours) AS ot,
							SUM(a.pto_hours) AS pto, SUM(a.hol_hours) AS hol 
						FROM ukg.clock_hours a
						JOIN dds.dim_date b on a.the_date = b.the_date
							AND b.the_date BETWEEN '01/16/2022' and '01/29/2022' -- _from_date and _thru_date
						join tp.blueprint_guarantee_rates c on a.employee_number = c.employee_number
							and c.thru_date > current_date
						GROUP BY c.employee_number					