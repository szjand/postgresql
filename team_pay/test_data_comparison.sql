﻿---------------------------------------------------------
--</ 07/25/22 all this previous stuff looks like overkill
---------------------------------------------------------
-- all i really want to compare is tech_flag_hours_day
-- perfect
select sum(tech_clock_hours_day) as clock, sum(tech_flag_hours_day) as flag  -- 35895.49,50722.59
from tp.data
where the_date between '02/01/2022' and '06/30/2022'

select sum(tech_clock_hours_day) as clock, sum(tech_flag_hours_day) as flag  -- 6802.76,10133.26
from tp.test_data
where the_date between '02/01/2022' and '06/30/2022'

select max(thedate) from ads.ext_tp_data

so. what is the transition
full_shop.py -- uses some ads.ext
sc_payroll_201803.py -- uses some ads.ext_

so, currently, scraping advantage into tp.data
and nightly running team_pay.py DataUpdateNightly to populate tp.test_data

this also means that i now need to reconstruct updating team pay values in postgresql (teams,values, etc)

so it looks like all i need to do is to
	1.stop scraping advantage team pay data into postgresql
	2. refactor function tp.test_data_update_nightly() to update tp.data

	
1. does anything anywhere use ads.ext_tp_data
	functions: no
	luigi: no
		alter TABLE ads.ext_tp_data
		rename to z_unused_ext_tp_data;
		alter TABLE ads.ext_tp_team_techs
		rename to z_unused_ext_tp_team_techs;
		alter table ads.ext_tp_team_values
		rename to z_unused_ext_tp_team_values;
		alter table ads.ext_tp_teams
		rename to z_unused_ext_tp_teams;
		alter table ads.ext_tp_tech_values
		rename to z_unused_ext_tp_tech_values;
		alter table ads.ext_tp_techs
		rename to z_unused_ext_tp_techs;

2. discontinue scraping advantage data into postgresql (tp.data, tp.techs, etc)
		
---------------------------------------------------------
--/> 07/25/22 all this previous stuff looks like overkill
---------------------------------------------------------
select aa.the_date, aa.department_key, aa.tech_number
from (
	select a.*,
		( --generate the hash
		select md5(x::text) as hash 
		from (
			select *
			from tp.data
			where the_date = '02/12/2022'
				and department_key = a.department_key
				and tech_number = a.tech_number) x)
	from tp.data a 
	where the_date = current_date) aa
join (
	select a.*,
		( --generate the hash
		select md5(x::text) as hash 
		from (
			select *
			from tp.test_data
			where the_date = '02/12/2022'
				and department_key = a.department_key
				and tech_number = a.tech_number) x)
	from tp.test_data a 
	where the_date = current_date) bb on aa.the_date = bb.the_date
	  and aa.department_key = bb.department_key
	  and aa.tech_number = bb.tech_number
	  and aa.hash <> bb.hash


select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'tp'
and table_name = 'test_data'

-- union to figure out which fields need to be rounded
	select 'data' as source, the_date,pay_period_start,pay_period_end,pay_period_seq,day_of_pay_period,
		day_name,department_key,tech_key,tech_number,employee_number,first_name,last_name,tech_team_percentage,
		tech_hourly_rate,round(tech_tfr_rate,2),round(tech_clock_hours_day,2),round(tech_clock_hours_pptd,2),round(tech_flag_hours_day,2),
		round(tech_flag_hours_pptd,2),round(tech_flag_hour_adjustments_day,2),round(tech_flag_hour_adjustments_pptd,2),team_key,
		team_name,team_census,round(team_budget,2),team_clock_hours_day,team_clock_hours_pptd,round(team_flag_hours_day,2),
		round(team_flag_hours_pptd,2),round(team_prof_day,2),round(team_prof_pptd,2),round(tech_vacation_hours_day,2),round(tech_vacation_hours_pptd,2),
		round(tech_pto_hours_day,2),round(tech_pto_hours_pptd,2),round(tech_holiday_hours_day,2),round(tech_holiday_hours_pptd,2),tech_other_hours_day,
		tech_other_hours_pptd,tech_training_hours_day,tech_training_hours_pptd,team_other_hours_day,team_other_hours_pptd,
		team_training_hours_day,team_training_hours_pptd,round(team_flag_hour_adjustments_day,2),
		round(team_flag_hour_adjustments_pptd,2),pay_period_select_format,
		( --generate the hash
		select md5(x::text) as hash 
		from (
			select the_date,pay_period_start,pay_period_end,pay_period_seq,day_of_pay_period,
		day_name,department_key,tech_key,tech_number,employee_number,first_name,last_name,tech_team_percentage,
		tech_hourly_rate,round(tech_tfr_rate,2),round(tech_clock_hours_day,2),round(tech_clock_hours_pptd,2),round(tech_flag_hours_day,2),
		round(tech_flag_hours_pptd,2),round(tech_flag_hour_adjustments_day,2),round(tech_flag_hour_adjustments_pptd,2),team_key,
		team_name,team_census,round(team_budget,2),team_clock_hours_day,team_clock_hours_pptd,round(team_flag_hours_day,2),
		round(team_flag_hours_pptd,2),round(team_prof_day,2),round(team_prof_pptd,2),round(tech_vacation_hours_day,2),round(tech_vacation_hours_pptd,2),
		round(tech_pto_hours_day,2),round(tech_pto_hours_pptd,2),round(tech_holiday_hours_day,2),round(tech_holiday_hours_pptd,2),tech_other_hours_day,
		tech_other_hours_pptd,tech_training_hours_day,tech_training_hours_pptd,team_other_hours_day,team_other_hours_pptd,
		team_training_hours_day,team_training_hours_pptd,round(team_flag_hour_adjustments_day,2),
		round(team_flag_hour_adjustments_pptd,2),pay_period_select_format		
			from tp.data
			where the_date = '02/12/2022'
				and department_key = a.department_key
				and tech_number = a.tech_number) x)
	from tp.data a 
	where the_date = '02/12/2022'
	union
	select 'test_data' as source, the_date,pay_period_start,pay_period_end,pay_period_seq,day_of_pay_period,
		day_name,department_key,tech_key,tech_number,employee_number,first_name,last_name,tech_team_percentage,
		tech_hourly_rate,round(tech_tfr_rate,2),round(tech_clock_hours_day,2),round(tech_clock_hours_pptd,2),round(tech_flag_hours_day,2),
		round(tech_flag_hours_pptd,2),round(tech_flag_hour_adjustments_day,2),round(tech_flag_hour_adjustments_pptd,2),team_key,
		team_name,team_census,round(team_budget,2),round(team_clock_hours_day,2),round(team_clock_hours_pptd,2),round(team_flag_hours_day,2),
		round(team_flag_hours_pptd,2),round(team_prof_day,2),round(team_prof_pptd,2),round(tech_vacation_hours_day,2),round(tech_vacation_hours_pptd,2),
		round(tech_pto_hours_day,2),round(tech_pto_hours_pptd,2),round(tech_holiday_hours_day,2),round(tech_holiday_hours_pptd,2),tech_other_hours_day,
		tech_other_hours_pptd,tech_training_hours_day,tech_training_hours_pptd,team_other_hours_day,team_other_hours_pptd,
		team_training_hours_day,team_training_hours_pptd,round(team_flag_hour_adjustments_day,2),
		round(team_flag_hour_adjustments_pptd,2),pay_period_select_format,
		( --generate the hash
		select md5(x::text) as hash 
		from (
			select the_date,pay_period_start,pay_period_end,pay_period_seq,day_of_pay_period,
		day_name,department_key,tech_key,tech_number,employee_number,first_name,last_name,tech_team_percentage,
		tech_hourly_rate,round(tech_tfr_rate,2),round(tech_clock_hours_day,2),round(tech_clock_hours_pptd,2),round(tech_flag_hours_day,2),
		round(tech_flag_hours_pptd,2),round(tech_flag_hour_adjustments_day,2),round(tech_flag_hour_adjustments_pptd,2),team_key,
		team_name,team_census,round(team_budget,2),round(team_clock_hours_day,2),round(team_clock_hours_pptd,2),round(team_flag_hours_day,2),
		round(team_flag_hours_pptd,2),round(team_prof_day,2),round(team_prof_pptd,2),round(tech_vacation_hours_day,2),round(tech_vacation_hours_pptd,2),
		round(tech_pto_hours_day,2),round(tech_pto_hours_pptd,2),round(tech_holiday_hours_day,2),round(tech_holiday_hours_pptd,2),tech_other_hours_day,
		tech_other_hours_pptd,tech_training_hours_day,tech_training_hours_pptd,team_other_hours_day,team_other_hours_pptd,
		team_training_hours_day,team_training_hours_pptd,round(team_flag_hour_adjustments_day,2),
		round(team_flag_hour_adjustments_pptd,2),pay_period_select_format		
			from tp.test_data
			where the_date = '02/12/2022'
				and department_key = a.department_key
				and tech_number = a.tech_number) x)
	from tp.test_data a 
	where the_date = '02/12/2022'
order by tech_number, source

-- now that all the rounding is done, inner join showing hash diffs
select aa.the_date, aa.department_key, aa.tech_number
from (
	select 'data' as source, the_date,pay_period_start,pay_period_end,pay_period_seq,day_of_pay_period,
		day_name,department_key,tech_key,tech_number,employee_number,first_name,last_name,tech_team_percentage,
		tech_hourly_rate,round(tech_tfr_rate,2),round(tech_clock_hours_day,2),round(tech_clock_hours_pptd,2),round(tech_flag_hours_day,2),
		round(tech_flag_hours_pptd,2),round(tech_flag_hour_adjustments_day,2),round(tech_flag_hour_adjustments_pptd,2),team_key,
		team_name,team_census,round(team_budget,2),round(team_clock_hours_day,2),round(team_clock_hours_pptd,2),round(team_flag_hours_day,2),
		round(team_flag_hours_pptd,2),round(team_prof_day,2),round(team_prof_pptd,2),round(tech_vacation_hours_day,2),round(tech_vacation_hours_pptd,2),
		round(tech_pto_hours_day,2),round(tech_pto_hours_pptd,2),round(tech_holiday_hours_day,2),round(tech_holiday_hours_pptd,2),tech_other_hours_day,
		tech_other_hours_pptd,tech_training_hours_day,tech_training_hours_pptd,team_other_hours_day,team_other_hours_pptd,
		team_training_hours_day,team_training_hours_pptd,round(team_flag_hour_adjustments_day,2),
		round(team_flag_hour_adjustments_pptd,2),pay_period_select_format,
		( --generate the hash
		select md5(x::text) as hash 
		from (
			select the_date,pay_period_start,pay_period_end,pay_period_seq,day_of_pay_period,
		day_name,department_key,tech_key,tech_number,employee_number,first_name,last_name,tech_team_percentage,
		tech_hourly_rate,round(tech_tfr_rate,2),round(tech_clock_hours_day,2),round(tech_clock_hours_pptd,2),round(tech_flag_hours_day,2),
		round(tech_flag_hours_pptd,2),round(tech_flag_hour_adjustments_day,2),round(tech_flag_hour_adjustments_pptd,2),team_key,
		team_name,team_census,round(team_budget,2),round(team_clock_hours_day,2),round(team_clock_hours_pptd,2),round(team_flag_hours_day,2),
		round(team_flag_hours_pptd,2),round(team_prof_day,2),round(team_prof_pptd,2),round(tech_vacation_hours_day,2),round(tech_vacation_hours_pptd,2),
		round(tech_pto_hours_day,2),round(tech_pto_hours_pptd,2),round(tech_holiday_hours_day,2),round(tech_holiday_hours_pptd,2),tech_other_hours_day,
		tech_other_hours_pptd,tech_training_hours_day,tech_training_hours_pptd,team_other_hours_day,team_other_hours_pptd,
		team_training_hours_day,team_training_hours_pptd,round(team_flag_hour_adjustments_day,2),
		round(team_flag_hour_adjustments_pptd,2),pay_period_select_format		
			from tp.data
			where the_date = '02/14/2022'
				and department_key = a.department_key
				and tech_number = a.tech_number) x)
	from tp.data a 
	where the_date = '02/14/2022') aa
join (
	select 'test_data' as source, the_date,pay_period_start,pay_period_end,pay_period_seq,day_of_pay_period,
		day_name,department_key,tech_key,tech_number,employee_number,first_name,last_name,tech_team_percentage,
		tech_hourly_rate,round(tech_tfr_rate,2),round(tech_clock_hours_day,2),round(tech_clock_hours_pptd,2),round(tech_flag_hours_day,2),
		round(tech_flag_hours_pptd,2),round(tech_flag_hour_adjustments_day,2),round(tech_flag_hour_adjustments_pptd,2),team_key,
		team_name,team_census,round(team_budget,2),round(team_clock_hours_day,2),round(team_clock_hours_pptd,2),round(team_flag_hours_day,2),
		round(team_flag_hours_pptd,2),round(team_prof_day,2),round(team_prof_pptd,2),round(tech_vacation_hours_day,2),round(tech_vacation_hours_pptd,2),
		round(tech_pto_hours_day,2),round(tech_pto_hours_pptd,2),round(tech_holiday_hours_day,2),round(tech_holiday_hours_pptd,2),tech_other_hours_day,
		tech_other_hours_pptd,tech_training_hours_day,tech_training_hours_pptd,team_other_hours_day,team_other_hours_pptd,
		team_training_hours_day,team_training_hours_pptd,round(team_flag_hour_adjustments_day,2),
		round(team_flag_hour_adjustments_pptd,2),pay_period_select_format,
		( --generate the hash
		select md5(x::text) as hash 
		from (
			select the_date,pay_period_start,pay_period_end,pay_period_seq,day_of_pay_period,
		day_name,department_key,tech_key,tech_number,employee_number,first_name,last_name,tech_team_percentage,
		tech_hourly_rate,round(tech_tfr_rate,2),round(tech_clock_hours_day,2),round(tech_clock_hours_pptd,2),round(tech_flag_hours_day,2),
		round(tech_flag_hours_pptd,2),round(tech_flag_hour_adjustments_day,2),round(tech_flag_hour_adjustments_pptd,2),team_key,
		team_name,team_census,round(team_budget,2),round(team_clock_hours_day,2),round(team_clock_hours_pptd,2),round(team_flag_hours_day,2),
		round(team_flag_hours_pptd,2),round(team_prof_day,2),round(team_prof_pptd,2),round(tech_vacation_hours_day,2),round(tech_vacation_hours_pptd,2),
		round(tech_pto_hours_day,2),round(tech_pto_hours_pptd,2),round(tech_holiday_hours_day,2),round(tech_holiday_hours_pptd,2),tech_other_hours_day,
		tech_other_hours_pptd,tech_training_hours_day,tech_training_hours_pptd,team_other_hours_day,team_other_hours_pptd,
		team_training_hours_day,team_training_hours_pptd,round(team_flag_hour_adjustments_day,2),
		round(team_flag_hour_adjustments_pptd,2),pay_period_select_format		
			from tp.test_data
			where the_date = '02/14/2022'
				and department_key = a.department_key
				and tech_number = a.tech_number) x)
	from tp.test_data a 
	where the_date = '02/14/2022') bb on aa.the_date = bb.the_date
	  and aa.department_key = bb.department_key
	  and aa.tech_number = bb.tech_number
	  and aa.hash <> bb.hash


-- look at the diffs
select * from (
	select 'data' as source, the_date,pay_period_start,pay_period_end,pay_period_seq,day_of_pay_period,
		day_name,department_key,tech_key,tech_number,employee_number,first_name,last_name,tech_team_percentage,
		tech_hourly_rate,round(tech_tfr_rate,2),round(tech_clock_hours_day,2),round(tech_clock_hours_pptd,2),round(tech_flag_hours_day,2),
		round(tech_flag_hours_pptd,2),round(tech_flag_hour_adjustments_day,2),round(tech_flag_hour_adjustments_pptd,2),team_key,
		team_name,team_census,round(team_budget,2),round(team_clock_hours_day,2),round(team_clock_hours_pptd,2),round(team_flag_hours_day,2),
		round(team_flag_hours_pptd,2),round(team_prof_day,2),round(team_prof_pptd,2),round(tech_vacation_hours_day,2),round(tech_vacation_hours_pptd,2),
		round(tech_pto_hours_day,2),round(tech_pto_hours_pptd,2),round(tech_holiday_hours_day,2),round(tech_holiday_hours_pptd,2),tech_other_hours_day,
		tech_other_hours_pptd,tech_training_hours_day,tech_training_hours_pptd,team_other_hours_day,team_other_hours_pptd,
		team_training_hours_day,team_training_hours_pptd,round(team_flag_hour_adjustments_day,2),
		round(team_flag_hour_adjustments_pptd,2),pay_period_select_format,
		( --generate the hash
		select md5(x::text) as hash 
		from (
			select the_date,pay_period_start,pay_period_end,pay_period_seq,day_of_pay_period,
				day_name,department_key,tech_key,tech_number,employee_number,first_name,last_name,tech_team_percentage,
				tech_hourly_rate,round(tech_tfr_rate,2),round(tech_clock_hours_day,2),round(tech_clock_hours_pptd,2),round(tech_flag_hours_day,2),
				round(tech_flag_hours_pptd,2),round(tech_flag_hour_adjustments_day,2),round(tech_flag_hour_adjustments_pptd,2),team_key,
				team_name,team_census,round(team_budget,2),round(team_clock_hours_day,2),round(team_clock_hours_pptd,2),round(team_flag_hours_day,2),
				round(team_flag_hours_pptd,2),round(team_prof_day,2),round(team_prof_pptd,2),round(tech_vacation_hours_day,2),round(tech_vacation_hours_pptd,2),
				round(tech_pto_hours_day,2),round(tech_pto_hours_pptd,2),round(tech_holiday_hours_day,2),round(tech_holiday_hours_pptd,2),tech_other_hours_day,
				tech_other_hours_pptd,tech_training_hours_day,tech_training_hours_pptd,team_other_hours_day,team_other_hours_pptd,
				team_training_hours_day,team_training_hours_pptd,round(team_flag_hour_adjustments_day,2),
				round(team_flag_hour_adjustments_pptd,2),pay_period_select_format		
			from tp.data
			where the_date = '02/14/2022'
				and department_key = a.department_key
				and tech_number = a.tech_number) x)
	from tp.data a 
	where the_date = '02/14/2022'
	union
	select 'test_data' as source, the_date,pay_period_start,pay_period_end,pay_period_seq,day_of_pay_period,
		day_name,department_key,tech_key,tech_number,employee_number,first_name,last_name,tech_team_percentage,
		tech_hourly_rate,round(tech_tfr_rate,2),round(tech_clock_hours_day,2),round(tech_clock_hours_pptd,2),round(tech_flag_hours_day,2),
		round(tech_flag_hours_pptd,2),round(tech_flag_hour_adjustments_day,2),round(tech_flag_hour_adjustments_pptd,2),team_key,
		team_name,team_census,round(team_budget,2),round(team_clock_hours_day,2),round(team_clock_hours_pptd,2),round(team_flag_hours_day,2),
		round(team_flag_hours_pptd,2),round(team_prof_day,2),round(team_prof_pptd,2),round(tech_vacation_hours_day,2),round(tech_vacation_hours_pptd,2),
		round(tech_pto_hours_day,2),round(tech_pto_hours_pptd,2),round(tech_holiday_hours_day,2),round(tech_holiday_hours_pptd,2),tech_other_hours_day,
		tech_other_hours_pptd,tech_training_hours_day,tech_training_hours_pptd,team_other_hours_day,team_other_hours_pptd,
		team_training_hours_day,team_training_hours_pptd,round(team_flag_hour_adjustments_day,2),
		round(team_flag_hour_adjustments_pptd,2),pay_period_select_format,
		( --generate the hash
		select md5(x::text) as hash 
		from (
			select the_date,pay_period_start,pay_period_end,pay_period_seq,day_of_pay_period,
				day_name,department_key,tech_key,tech_number,employee_number,first_name,last_name,tech_team_percentage,
				tech_hourly_rate,round(tech_tfr_rate,2),round(tech_clock_hours_day,2),round(tech_clock_hours_pptd,2),round(tech_flag_hours_day,2),
				round(tech_flag_hours_pptd,2),round(tech_flag_hour_adjustments_day,2),round(tech_flag_hour_adjustments_pptd,2),team_key,
				team_name,team_census,round(team_budget,2),round(team_clock_hours_day,2),round(team_clock_hours_pptd,2),round(team_flag_hours_day,2),
				round(team_flag_hours_pptd,2),round(team_prof_day,2),round(team_prof_pptd,2),round(tech_vacation_hours_day,2),round(tech_vacation_hours_pptd,2),
				round(tech_pto_hours_day,2),round(tech_pto_hours_pptd,2),round(tech_holiday_hours_day,2),round(tech_holiday_hours_pptd,2),tech_other_hours_day,
				tech_other_hours_pptd,tech_training_hours_day,tech_training_hours_pptd,team_other_hours_day,team_other_hours_pptd,
				team_training_hours_day,team_training_hours_pptd,round(team_flag_hour_adjustments_day,2),
				round(team_flag_hour_adjustments_pptd,2),pay_period_select_format		
			from tp.test_data
			where the_date = '02/14/2022'
				and department_key = a.department_key
				and tech_number = a.tech_number) x)
	from tp.test_data a 
	where the_date = '02/14/2022'
) s where department_key = 18 and tech_number = '294'	
order by tech_number, source

select a.team_name, c.first_name || ' ' || c.last_name
-- select * 
from tp.teams a
join tp.team_techs b on a.team_key = b.team_key
  and b.thru_date > current_date
join tp.techs c on b.tech_key = c.tech_key
  and c.thru_date > current_date 
where a.thru_date > current_date  
order by a.team_name, c.first_name || ' ' || c.last_name  



drop table if exists test_data;
create temp table test_data as select * from tp.test_data limit 1;
truncate test_data;
insert into test_data
    SELECT a.the_date, a.biweekly_pay_period_start_date, a.biweekly_pay_period_end_date, 
      a.biweekly_pay_period_sequence, a.day_in_biweekly_pay_period, a.day_name,
      b.department_key, -- main shop, not implementing zDepartments from  ads
      b.tech_key, b.tech_number, b.employee_number, b.first_name, b.last_name
    FROM dds.dim_date a, tp.techs b 
    WHERE a.the_date BETWEEN '02/13/2022' AND current_date
	    AND b.department_key in (13,18) -- no need for separate queries for main and body shop
      AND NOT EXISTS (
        SELECT 1
        FROM tp.test_data
        WHERE the_date = a.the_date
          AND tech_key = b.tech_key)
      AND EXISTS (-- 12/5, techs gone, but still exist IN tpTechs, don't want them IN tpData
        SELECT 1
        FROM tp.team_techs
        WHERE tech_key = b.tech_key
          AND a.the_date BETWEEN '02/13/2022' AND '02/26/2022'); 

    UPDATE test_data aa
    SET Team_Key = x.team_key,
        team_name = x.team_name
    FROM (    
      SELECT *
      FROM ( 
        SELECT the_date
        FROM test_data
        GROUP BY the_date) a
      INNER JOIN (  
        SELECT a.tech_key, c.team_key, c.team_name, b.from_date, b.thru_date
        FROM tp.techs a
        INNER JOIN tp.team_techs b on a.tech_key = b.tech_key
        INNER JOIN tp.teams c on b.team_key = c.team_key) b on a.the_date BETWEEN b.from_date AND b.thru_date) x
    WHERE aa.Tech_Key = x.tech_key
      AND aa.the_Date = x.the_Date;             
    UPDATE test_data aa
      SET team_Census = x.census,
          team_Budget = x.budget
      FROM tp.Team_Values x
      WHERE (aa.team_Census IS NULL OR aa.team_Budget IS NULL) 
        AND aa.team_Key = x.team_Key
        AND aa.the_Date BETWEEN x.from_Date AND x.thru_Date; 	           

select * from test_data