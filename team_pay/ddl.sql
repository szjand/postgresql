﻿-- create schema tp;
-- comment on schema tp is 'repository for team pay data and functions';


-------------------------------------------------------------------------
--< convert tp.teams.team_key & tp.techs.tech_key to serial
-------------------------------------------------------------------------
-- 10/28/2020
create sequence tp.teams_team_key_seq
  owned by tp.teams.team_key;
alter table tp.teams
alter column team_key set default nextval('tp.teams_team_key_seq');
select setval('tp.teams_team_key_seq', (select max(team_key)from tp.teams));

-- insert into tp.teams (department_key,team_name,from_date) values (18,'team wtf',current_date)

create sequence tp.techs_tech_key_seq
owned by tp.techs.tech_key;
alter table tp.techs
alter column tech_key set default nextval('tp.techs_tech_key_seq');
select setval('tp.techs_tech_key_seq', (select max(tech_key)from tp.techs));

-------------------------------------------------------------------------
--/> convert tp.teams.team_key & tp.techs.tech_key to serial
-------------------------------------------------------------------------


-- -- don't need date_key, but to simplify importing data from ads, included it in the initial table creation
alter table tp.data
drop column date_key;

-- select * from tp.data where the_date = current_date;


DROP TABLE if exists tp.data cascade;
CREATE TABLE tp.data (
  date_key integer not null,
  the_date date NOT NULL,
  pay_period_start date NOT NULL,
  pay_period_end date NOT NULL,
  pay_period_seq integer NOT NULL,
  day_of_pay_period integer NOT NULL,
  day_name citext NOT NULL,
  department_key integer NOT NULL,
  tech_key integer,
  tech_number citext NOT NULL,
  employee_number citext,
  first_name citext,
  last_name citext,
  tech_team_percentage numeric,
  tech_hourly_rate numeric,
  tech_tfr_rate numeric,
  tech_clock_hours_day numeric,
  tech_clock_hours_pptd numeric,
  tech_flag_hours_day numeric,
  tech_flag_hours_pptd numeric,
  tech_flag_hour_adjustments_day numeric,
  tech_flag_hour_adjustments_pptd numeric,
  team_key integer,
  team_name citext,
  team_census integer,
  team_budget numeric,
  team_clock_hours_day numeric,
  team_clock_hours_pptd numeric,
  team_flag_hours_day numeric,
  team_flag_hours_pptd numeric,
  team_prof_day numeric,
  team_prof_pptd numeric,
  tech_vacation_hours_day numeric,
  tech_vacation_hours_pptd numeric,
  tech_pto_hours_day numeric,
  tech_pto_hours_pptd numeric,
  tech_holiday_hours_day numeric,
  tech_holiday_hours_pptd numeric,
  tech_other_hours_day integer DEFAULT 0,
  tech_other_hours_pptd integer DEFAULT 0,
  tech_training_hours_day integer DEFAULT 0,
  tech_training_hours_pptd integer DEFAULT 0,
  team_other_hours_day integer DEFAULT 0,
  team_other_hours_pptd integer DEFAULT 0,
  team_training_hours_day integer DEFAULT 0,
  team_training_hours_pptd integer DEFAULT 0,
  team_flag_hour_adjustments_day numeric,
  team_flag_hour_adjustments_pptd numeric,
  pay_period_select_format citext,
  PRIMARY KEY (the_date, department_key, tech_number));

drop table if exists tp.team_techs cascade;  
CREATE TABLE tp.team_techs(
  team_key integer NOT NULL,
  tech_key integer NOT NULL,
  from_date date NOT NULL,
  thru_date date NOT NULL DEFAULT '9999-12-31'::date,
  PRIMARY KEY (team_key, tech_key, thru_date));


drop table if exists tp.team_values cascade;
 CREATE TABLE tp.team_values(
  team_key integer NOT NULL,
  census integer NOT NULL,
  budget numeric(12,4) NOT NULL,
  pool_percentage numeric,
  from_date date NOT NULL,
  thru_date date NOT NULL DEFAULT '9999-12-31'::date,
  PRIMARY KEY (team_key, thru_date)); 

drop table if exists tp.teams cascade;
CREATE TABLE tp.teams(
  team_key integer NOT NULL,
  department_key integer NOT NULL,
  team_name citext NOT NULL,
  from_date date NOT NULL,
  thru_date date DEFAULT '9999-12-31'::date,
  PRIMARY KEY (team_key));

drop table if exists tp.tech_values cascade;
CREATE TABLE tp.tech_values(
  tech_key integer NOT NULL,
  from_date date NOT NULL,
  thru_date date NOT NULL DEFAULT '9999-12-31'::date,
--   tech_team_percentage numeric(12,4) NOT NULL,
  tech_team_percentage numeric NOT NULL,
  hourly_rate numeric,
  PRIMARY KEY (tech_key, thru_date));  

drop table if exists tp.techs cascade;
 CREATE TABLE tp.techs(
  tech_key integer NOT NULL DEFAULT nextval('ads.ext_tptechs_techkey_seq'::regclass),
  department_key integer NOT NULL,
  tech_number citext NOT NULL,
  first_name citext NOT NULL,
  last_name citext NOT NULL,
  employee_number citext NOT NULL,
  from_date date NOT NULL,
  thru_date date DEFAULT '9999-12-31'::date,
  PRIMARY KEY (tech_key)); 
