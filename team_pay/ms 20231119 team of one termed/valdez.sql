﻿-- 11/5/23  Mario Valdez termed.

select aa.team_name, 
	aa.team_key, 
	aa.census, aa.budget, aa.pool_percentage as "pool %", 
	cc.last_name, cc.first_name, 
	cc.tech_key, 
	cc.tech_number, cc.employee_number as emp,
  cc.tech_team_percentage as "tech %", round(aa.budget * cc.tech_team_percentage, 2) as tech_flat_rate
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 18) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date) cc on bb.tech_key = cc.tech_key 
-- where aa.team_key in (54)	
order by aa.team_name, cc.last_name

do $$
declare 
-- 	_eff_date date := '08/13/2023';
	_thru_date date:= '11/18/2023';
	_tech_key integer := 95; 
	_team_key integer := 54; 
begin	
  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key;

  update tp.team_values
  set thru_date = _thru_date
  where team_key = _team_key;

  update tp.team_techs
  set thru_date = _thru_date
  where team_key = _team_key
    and tech_key = _tech_key;

  update tp.techs
  set thru_date = _thru_date
  where tech_key = _tech_key;

  update tp.teams
  set thru_date = _thru_date
  where team_key = _team_key;

  delete
--   select *
  from tp.data
  where team_key = 54 --_team_key
    and the_date > '11/18/2023';
end $$;		