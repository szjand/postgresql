﻿-- create a test table with the tool_allow attribute
drop table tp.test_gm_main_shop_flat_rate_payroll_data;
CREATE TABLE tp.test_gm_main_shop_flat_rate_payroll_data
(
  pay_period_start date NOT NULL,
  pay_period_end date NOT NULL,
  pay_period_seq integer NOT NULL,
  pay_period_select_format citext NOT NULL,
  first_saturday date NOT NULL,
  team citext NOT NULL,
  last_name citext NOT NULL,
  first_name citext NOT NULL,
  employee_number citext NOT NULL,
  hire_date date NOT NULL,
  term_date date NOT NULL,
  pto_rate numeric NOT NULL,
  flat_rate numeric NOT NULL,
  bonus_rate numeric NOT NULL,
  flag_hours numeric NOT NULL,
  adjustments numeric NOT NULL,
  total_flag_hours numeric NOT NULL,
  team_prof numeric NOT NULL,
  clock_hours numeric NOT NULL,
  pto_hours numeric NOT NULL,
  hol_hours numeric NOT NULL,
  total_hours numeric NOT NULL,
  guarantee numeric,
  comm_hours numeric NOT NULL,
  bonus_hours numeric NOT NULL,
  comm_pay numeric NOT NULL,
  bonus_pay numeric NOT NULL,
  pto_pay numeric NOT NULL,
  tool_allow numeric not null,
  total_pay numeric NOT NULL);

-- all the column names from real table
select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'tp'
  and table_name = 'gm_main_shop_flat_rate_payroll_data';

-- store the production data in a temp table
drop table if exists the_test;
create temp table the_test as select * from tp.gm_main_shop_flat_rate_payroll_data

-- insert the real data into the test table, it works, add the tool allowance to the attributes in the select
insert into tp.test_gm_main_shop_flat_rate_payroll_data
select pay_period_start,pay_period_end,pay_period_seq,pay_period_select_format,first_saturday,team,last_name,
	first_name,employee_number,hire_date,term_date,pto_rate,flat_rate,bonus_rate,flag_hours,adjustments,
	total_flag_hours,team_prof,clock_hours,pto_hours,hol_hours,total_hours,guarantee,comm_hours,bonus_hours,
	comm_pay,bonus_pay,pto_pay,0 as tool_allow, total_pay
from the_test;

drop and recreate the production table with the tool allow attribute
-- DROP TABLE tp.gm_main_shop_flat_rate_payroll_data cascade;
CREATE TABLE tp.gm_main_shop_flat_rate_payroll_data
(
  pay_period_start date NOT NULL,
  pay_period_end date NOT NULL,
  pay_period_seq integer NOT NULL,
  pay_period_select_format citext NOT NULL,
  first_saturday date NOT NULL,
  team citext NOT NULL,
  last_name citext NOT NULL,
  first_name citext NOT NULL,
  employee_number citext NOT NULL,
  hire_date date NOT NULL,
  term_date date NOT NULL,
  pto_rate numeric NOT NULL,
  flat_rate numeric NOT NULL,
  bonus_rate numeric NOT NULL,
  flag_hours numeric NOT NULL,
  adjustments numeric NOT NULL,
  total_flag_hours numeric NOT NULL,
  team_prof numeric NOT NULL,
  clock_hours numeric NOT NULL,
  pto_hours numeric NOT NULL,
  hol_hours numeric NOT NULL,
  total_hours numeric NOT NULL,
  guarantee numeric,
  comm_hours numeric NOT NULL,
  bonus_hours numeric NOT NULL,
  comm_pay numeric NOT NULL,
  bonus_pay numeric NOT NULL,
  pto_pay numeric NOT NULL,
  tool_allow numeric not null,
  total_pay numeric NOT NULL,
  CONSTRAINT gm_main_shop_flat_rate_payroll_data_pkey PRIMARY KEY (pay_period_seq, employee_number));
COMMENT ON TABLE tp.gm_main_shop_flat_rate_payroll_data
  IS 'updated every sunday by a call to function tp.update_gm_main_shop_flat_rate_payroll_data() in luigi ukg_payroll_import_data.py. 
  table for storing all necessary gm main shop flat rate data to generate ukg import files and manager approval pages';

-- insert the real data into the reconstructed production table
insert into tp.gm_main_shop_flat_rate_payroll_data
select pay_period_start,pay_period_end,pay_period_seq,pay_period_select_format,first_saturday,team,last_name,
	first_name,employee_number,hire_date,term_date,pto_rate,flat_rate,bonus_rate,flag_hours,adjustments,
	total_flag_hours,team_prof,clock_hours,pto_hours,hol_hours,total_hours,guarantee,comm_hours,bonus_hours,
	comm_pay,bonus_pay,pto_pay,0 as tool_allow, total_pay
from the_test;  

and it works
tested the function for current pay period successully (then deleted the data for pay seq 342)


-- need to add tool allowance 68.24 and hol pay for calculating total earned
-- no need for a holiday pay col, hol included in pto
do $$
declare 
	_pay_period_seq integer := (
	  select distinct biweekly_pay_period_sequence
	  from dds.dim_date
	  where the_date = current_date - 1);
	_from_date date := (
	  select distinct biweekly_pay_period_start_date
	  from dds.dim_date 
	  where biweekly_pay_period_sequence = _pay_period_seq);
	_thru_date date := (
	  select distinct biweekly_pay_period_end_date
	  from dds.dim_date 
	  where biweekly_pay_period_sequence = _pay_period_seq);	
begin 		
--   delete 
--   from  tp.gm_main_shop_flat_rate_payroll_data
--   where pay_period_seq = _pay_period_seq;
--   
-- 	insert into tp.gm_main_shop_flat_rate_payroll_data
drop table if exists wtf;
create temp table wtf as
	select dd.*,
		round(comm_hours * team_prof/100 * flat_rate, 2) as comm_pay,
		round(bonus_hours * team_prof/100 * bonus_rate, 2) as bonus_pay,
		round(pto_rate * (pto_hours + hol_hours), 2) as pto_pay,
		68.42::numeric as tool_allow,
		case
			when coalesce(guarantee, 0) > round(comm_hours * team_prof/100 * flat_rate, 2) 
				+ round(bonus_hours * team_prof/100 * bonus_rate, 2)
				+ round(pto_rate * (pto_hours + hol_hours), 2) then guarantee + 68.42
			else round(comm_hours * team_prof/100 * flat_rate, 2) 
				+ round(bonus_hours * team_prof/100 * bonus_rate, 2) + 68.42
		end as total_pay
	from (
		select cc.*,
			case
				when cc.total_hours > 90 and cc.team_prof >= 100 then cc.clock_hours - (total_hours -90)
				else cc.clock_hours
			end as comm_hours,
			round(
				case
					when cc.total_hours > 90 and team_prof >= 100 then total_hours - 90
					else 0
				 end, 4) as bonus_hours
		from (
			select aa.*, bb.guarantee 
			from ( -- tech data
				select pay_period_start, pay_period_end, pay_period_seq, pay_period_select_format,
					pay_period_start + 6 as first_saturday,
					c.team_name AS Team, a.last_name, a.first_name, a.employee_number, 
					d.hire_date, d.term_date,
					tech_hourly_rate AS pto_rate, round(tech_tfr_rate, 4) AS flat_rate, 
					2 * round(tech_tfr_rate, 4) AS bonus_rate,
					tech_flag_hours_pptd as flag_hours, tech_Flag_Hour_Adjustments_PPTD as Adjustments,
					tech_flag_hours_pptd + tech_Flag_Hour_Adjustments_PPTD as total_flag_hours,
					team_prof_pptd AS team_prof,
					tech_clock_hours_pptd AS clock_hours, 
					tech_pto_hours_pptd AS pto_hours, 
					tech_holiday_hours_pptd AS hol_hours,
					round(tech_clock_hours_pptd + tech_pto_hours_pptd + tech_holiday_hours_pptd, 2) AS total_hours            
				FROM tp.data a
				INNER JOIN tp.team_techs b on a.tech_key = b.tech_key
					AND a.team_key = b.team_key
				  AND _thru_date BETWEEN b.from_date AND b.thru_date
				LEFT JOIN tp.teams c on b.team_key = c.team_key
				left join ukg.employees d on a.employee_number = d.employee_number
				WHERE a.the_date = current_date - 1 -- _thru_date -------------------------------------------------------------
					AND a.department_Key = 18) aa
			left join (		
				SELECT employee_number, reg + ot + pto + bonus AS guarantee
				FROM ( -- blueprint team guarantees
					SELECT a.name, a.employee_number, round(a.hourly_rate * b.clock, 2) AS reg, 
						round(a.hourly_rate * 1.5 * b.ot, 2) AS ot, 
						round(a.pto_rate * (pto + hol), 2) AS pto,
						case
							when b.clock + b.ot + b.pto > 90 THEN  ((b.clock + b.ot + b.pto) - 90) * 2 * a.hourly_rate
						ELSE 0
						END AS bonus
					FROM tp.blueprint_guarantee_rates a
					JOIN (			 
						select c.employee_number, SUM(a.reg_hours) AS clock, SUM(a.ot_hours) AS ot,
							SUM(a.pto_hours) AS pto, SUM(a.hol_hours) AS hol 
						FROM ukg.clock_hours a
						JOIN dds.dim_date b on a.the_date = b.the_date
							AND b.the_date BETWEEN _from_date and _thru_date
						join tp.blueprint_guarantee_rates c on a.employee_number = c.employee_number
							and c.thru_date > current_date
						GROUP BY c.employee_number) b on a.employee_number = b.employee_number
					WHERE a.thru_date = '12/31/9999') d) bb on aa.employee_number = bb.employee_number) cc) dd;
end $$;
select * from wtf;					