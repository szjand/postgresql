﻿
/*
Jon,
we are looking at doing away with the 90 hour bonus pay for the flat rate techs at the request of Jeri and Ben. 
We are trying to come up with a % based bonus that they would be eligible for based off productivity. 
Is there a way you would be able to pull the team % for 2019,2020 and 2021 so far? I know some people have
moved around and such but I believe it will give us enough data to see if this idea is feasible. I would 
just be looking for the final team % at the end of the pay period for the past 2 years and what we have 
for 2021 so far.  
Andrew Neumann

*/

used this query in ads.sccotest
SELECT thedate, payperiodstart, payperiodend, teamname, teamprofpptd, trim(firstname) + ' ' + trim(lastname)
FROM tpdata
WHERE thedate = payperiodend
  AND departmentkey = 18
union
SELECT thedate, payperiodstart, payperiodend, teamname, teamprofpptd, trim(firstname) + ' ' + trim(lastname)
FROM tpdata_archive_thru_2019
WHERE thedate = payperiodend
  AND departmentkey = 18
ORDER BY thedate

to generate a spreadsheet, from which i generated insert statements

drop table if exists jon.tmp_tp_data;
create table jon.tmp_tp_data (
  the_date date,
  team citext,
  team_prof numeric,
  techs citext);

insert into jon.tmp_tp_data values('01/05/2019','Anderson',137.36,'Carter Ferry');
insert into jon.tmp_tp_data values('01/05/2019','Anderson',137.36,'Clayton Gehrtz');
insert into jon.tmp_tp_data values('01/05/2019','Anderson',137.36,'Gavin Flaat');
insert into jon.tmp_tp_data values('01/05/2019','Anderson',137.36,'Shawn Anderson');

select the_date, team, team_prof,string_agg(techs, ',')
from jon.tmp_tp_data
group by the_date, team, team_prof
order by team, the_date