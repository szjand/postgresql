﻿/*
Ken has submitted the the rate increases for Gordon David (team mcveigh) and Matt Aamodt (team oneil) as well as moving Carter Ferry (team mcveigh)
to Team Dennis and switching him to flat rate as well. We are hoping to implement the changes 
starting the next pay period 1-29-23. Let me know if you see anything off. Thanks!
Andrew Neumann 

-- current state, a summary for ken and andrew
-- for my use, add team key & tech key
select aa.team_name, 
	-- aa.team_key, 
	aa.census, aa.budget, aa.pool_percentage as "pool %", 
	cc.last_name, cc.first_name, 
	-- cc.tech_key, 
	cc.tech_number, cc.employee_number as emp,
  cc.tech_team_percentage as "tech %", round(aa.budget * cc.tech_team_percentage, 2) as tech_flat_rate
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 18) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date) cc on bb.tech_key = cc.tech_key 
order by aa.team_name, cc.last_name

*/
-- doing these updates on sunday, the 29th, tp.data will update tonight, don't need to do it here
-- aamodt raise
-- does not effect budget or pool
do $$
declare 
	_eff_date date := '01/29/2023';
	_thru_date date:= '01/28/2022';
	_tech_key integer := (select tech_key from tp.techs where last_name = 'aamodt' and thru_date > current_date);
	_tech_perc numeric := 0.19661; -- select 20.14 / 102.4352
	_hourly_rate numeric := 18.34;
begin

  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;

	insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
	values(_tech_key, _eff_date, _tech_perc, _hourly_rate);

end $$;


-- team mcveigh david gets a raise add carter ferry

do $$
declare 
	_eff_date date := '01/01/2023';
	_thru_date date:= '12/31/2022';
	_tech_number citext;
	_dept_key integer := 18;
	_team_key integer := (select team_key from tp.teams where team_name = 'mcveigh' and thru_date > current_date);
	_tech_key integer;
	_elr numeric := 91.46;
	_census integer := 4;
	_pool_perc numeric := .3;
	_tech_perc numeric;
	_budget numeric := 109.752; -- select _census * _elr * _pool_perc;
begin
-- carter ferry, already exists in tp.techs, update the tech_number
		_tech_number = '672';
		_tech_key = (select tech_key from tp.techs where last_name = 'ferry' and first_name = 'carter' and thru_date > current_date);
		update tp.techs
		set thru_date = _thru_date
		where tech_key = _tech_key;
		
		insert into tp.techs(department_key,tech_number,first_name,last_name,employee_number,from_date)
		values(_dept_key, _tech_number, 'Carter', 'Ferry', '159863', _eff_date);

		_tech_key = (select tech_key from tp.techs where last_name = 'ferry' and first_name = 'carter' and thru_date > current_date);
		
		insert into tp.team_techs(team_key,tech_key,from_date)
		values(_team_key, _tech_key, _eff_date);

		update tp.team_values
		set thru_date = _thru_date
		where team_key = _team_key
		  and thru_date > current_date;

		insert into tp.team_values(team_key,census,budget,pool_percentage,from_date)
		values(_team_key, _census, _budget, _pool_perc, _eff_date);

		-- ferry
		_tech_perc = 0.209563; -- select 23/109.752
		insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
		values(_tech_key, _eff_date, _tech_perc, 23);
		
-- have to update everybody because the census is changing from 3 to 4
		-- gordon
		_tech_perc = 0.20045193; -- select 22/109.752
		_tech_key = (select tech_key from tp.techs where last_name = 'david' and first_name = 'gordon' and thru_date > current_date);

    update tp.tech_values
    set thru_date = _thru_date
    where tech_key = _tech_key
      and thru_date > current_date;

		insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
		values(_tech_key, _eff_date, _tech_perc, 22);

		-- mcveigh
		_tech_perc = 0.287011; -- select 31.50/109.752
		_tech_key = (select tech_key from tp.techs where last_name = 'mcveigh' and first_name = 'dennis' and thru_date > current_date);

    update tp.tech_values
    set thru_date = _thru_date
    where tech_key = _tech_key
      and thru_date > current_date;

		insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
		values(_tech_key, _eff_date, _tech_perc, 31.50);

		-- Schuppert
		_tech_perc = 0.20045193; -- select 22/109.752
		_tech_key = (select tech_key from tp.techs where last_name = 'schuppert' and first_name = 'kodey' and thru_date > current_date);

    update tp.tech_values
    set thru_date = _thru_date
    where tech_key = _tech_key
      and thru_date > current_date;

		insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
		values(_tech_key, _eff_date, _tech_perc, 31.50);	
		
end $$;    


