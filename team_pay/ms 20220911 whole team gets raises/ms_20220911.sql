﻿/*
09/18/22
this is the first team pay update since moving everything to postgresql

This pay rate change will go into effect on the pay period starting 9/11/22. 
The changes are for Mike Schwan, Wyatt Thompson and Joshua Syverson. 
It was delayed in getting to you because it sat in a few Compli inboxes for too long. 
Can you make this adjustment before we run payroll next week please?
*/

create table tp.shop_values (
  department_key integer not null,
  from_date date not null,
  thru_date date not null default '12/31/9999',
  effective_labor_rate numeric(5,2),
  primary key(department_key,thru_date));

insert into tp.shop_values(department_key,from_date,effective_labor_rate) values
(13,'05/04/2014',60),  
(18,'01/10/2016',91.46);

-- rename hefffernan to schwan
update tp.teams
set team_name = 'Schwan'
where team_key = 3
  and department_key = 18;

-- current configuration
SELECT team_Name, 
  (SELECT effective_Labor_Rate 
    FROM tp.Shop_Values 
    WHERE department_Key = 18 AND thru_Date > current_date) AS ELR,
    b.pool_Percentage, a.team_Census, a.team_Budget,  
  TRIM(first_Name) || ' ' || last_Name, a.tech_key, tech_Team_Percentage, 
  round(tech_TFR_Rate, 2) AS tech_rate, a.team_Key
--SELECT *
FROM tp.data a
LEFT JOIN tp.Team_Values b on a.team_Key = b.team_Key
  AND b.thru_Date > current_date
WHERE the_date = current_date
  AND department_Key = 18
  AND a.team_key = 3  -- Heffernan/schwann
ORDER BY team_name, first_name  

select * from tp.team_values where team_key = 3 order by from_date
delete from tp.team_values where team_key = 3 and thru_date > current_date

do $$
declare
  _eff_date date := '09/11/2022';
  _thru_date date := '09/10/2022';
  _dept_key integer := 18;
  _team_key integer := 3; -- all team schwann
  _elr numeric(5,2) := 91.46;
  _census integer := 3;
  _pool_perc numeric := .29;
  _tech_perc numeric;
  _tech_key integer;


begin
/*
*/
-- team values
--   update tp.team_values
--   set thru_date = _thru_date
--   where team_key = _team_key
--     and thru_date > current_date;
--     
--   insert into tp.team_values(team_key,census,budget,pool_percentage,from_date)
--   values(_team_key, _census, _census * _elr * _pool_perc, _pool_perc, _eff_date);

-- tech_values  
-- syverson select * from tp.techs where last_name = 'syverson' and thru_date > current_Date
  _tech_key = 18;
  _tech_perc = .323614;
  
  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;

  insert into tp.tech_values(tech_key,from_date,tech_team_percentage)
  values(_tech_key, _eff_date, _tech_perc);
  
-- schwan select * from tp.techs where last_name = 'schwan' and thru_date > current_date
  _tech_key = 1;
  _tech_perc = .377;

  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;

  insert into tp.tech_values(tech_key,from_date,tech_team_percentage)
  values(_tech_key, _eff_date, _tech_perc);  
  
-- thompson  select * from tp.techs where last_name = 'thompson' and thru_date > current_date
  _tech_key = 13;
  _tech_perc = .333;

  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;

  insert into tp.tech_values(tech_key,from_date,tech_team_percentage)
  values(_tech_key, _eff_date, _tech_perc);  

	delete from tp.data
	where the_date > '09/10/2022'
		and team_key = 3;

	select tp.data_update_nightly()    
end $$;
	
delete from tp.data
where the_date > '09/10/2022'
  and team_key = 3;

select tp.data_update_nightly()  


-- turns out need the hourly rate for tp.gm_main_shop_flat_rate_payroll_data;
-- which drives me nuts, don't actually need it, all pto is in ukg, but in that table i have not null constraints
-- so for now ...
select * from tp.tech_values where tech_key in (1,13,18) order by tech_key, from_date

update tp.tech_values
set hourly_rate = 43.57
where tech_key = 1
  and thru_date > current_date;

update tp.tech_values
set hourly_rate = 32.05
where tech_key = 13
  and thru_date > current_date;

update tp.tech_values
set hourly_rate = 31.66
where tech_key = 18
  and thru_date > current_date;   

select *
from tp.data
where tech_key in (1,13,18)
  and the_date = current_date - 1 

update tp.data
set tech_hourly_rate = 43.57
where tech_key = 1
  and the_date = current_date - 1;

update tp.data
set tech_hourly_rate = 32.05
where tech_key = 13
  and the_date = current_date - 1;

update tp.data
set tech_hourly_rate = 31.66
where tech_key = 18
  and the_date = current_date - 1;