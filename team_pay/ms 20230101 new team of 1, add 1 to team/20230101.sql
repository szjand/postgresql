﻿/*Jon,
 We will be moving 2 technicians from hourly to flat rate effective 01/01/2023.
 
Brody Bailly will go to Team Alignment.   His hourly is $19 hr moving to $19 flat rate
Mariano Valdez will also be moving to flat rate. He will be on his own team from $26 hr to $26 hr flat rate.


-- current state, a summary for ken and andrew
select aa.team_name, aa.census, aa.budget, aa.pool_percentage as "pool %", cc.last_name, cc.first_name, cc.tech_number, cc.employee_number as emp,
  cc.tech_team_percentage as "tech %", round(aa.budget * cc.tech_team_percentage, 2) as tech_flat_rate
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 18) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date) cc on bb.tech_key = cc.tech_key 
order by aa.team_name, cc.last_name
*/

************ doing these on Sunday 1/1, no need to update the data, that will happen tonight **********
-- new team of 1 Mariano Valdez will also be moving to flat rate. He will be on his own team from $26 hr to $26 hr flat rate.
-- select * from dds.dim_tech where tech_name like '%valdez%'
-- select * from tp.techs where tech_number = '312'
do $$
declare 
	_eff_date date := '01/01/2023';
	_team_name citext := 'Valdez';
	_employee_number citext := '174297';
	_tech_number citext := '312';
	_dept_key integer := 18;
	_first_name citext := 'Mariano';
	_last_name citext := 'Valdez';
	_team_key integer;
	_tech_key integer;
begin
  insert into tp.teams (department_key,team_name,from_date)
  values(_dept_key,_team_name,_eff_date);

  _team_key = (select team_key from tp.teams where team_name = _team_name);
	
	insert into tp.techs(department_key,tech_number,first_name,last_name,employee_number, from_date)
	values(_dept_key, _tech_number, _first_name, _last_name, _employee_number, _eff_date);

	_tech_key = (select tech_key from tp.techs where last_name = _last_name and first_name = _first_name);

	insert into tp.team_techs(team_key,tech_key,from_date)
	values(_team_key,_tech_key,_eff_date);

	insert into tp.team_values(team_key,census,budget,pool_percentage,from_date)
	values(_team_key, 1, 26, 1, _eff_date); 

  insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
  values(_tech_key, _eff_date, 1, 26);
END $$;  
	
=======================================================================================================================
-- Brody Bailly will go to Team Alignment.   His hourly is $19 hr moving to $19 flat rate
-- select * from dds.dim_tech where tech_name like '%bail%'
-- select * from tp.techs where tech_number = '669'
do $$
declare 
	_eff_date date := '01/01/2023';
	_thru_date date:= '12/31/2022';
	_employee_number citext := '166873';
	_tech_number citext := '669';
	_dept_key integer := 18;
	_first_name citext := 'Brody';
	_last_name citext := 'Bailly';
	_team_key integer := (select team_key from tp.teams where team_name = 'alignment');
	_tech_key integer;
	_elr numeric := 91.46;
	_census integer := 3;
	_pool_perc numeric := .24;
	_tech_perc numeric;
	_budget numeric := 65.8512; -- select _census * _elr * _pool_perc;
begin
	
	insert into tp.techs(department_key,tech_number,first_name,last_name,employee_number, from_date)
	values(_dept_key, _tech_number, _first_name, _last_name, _employee_number, _eff_date);

	_tech_key = (select tech_key from tp.techs where last_name = _last_name and first_name = _first_name);

	insert into tp.team_techs(team_key,tech_key,from_date)
	values(_team_key,_tech_key,_eff_date);

	update tp.team_values
	set thru_date = _thru_date
	where team_key = _team_key
	  and thru_date > current_date;
	insert into tp.team_values(team_key,census,budget,pool_percentage,from_date)
	values(_team_key, _census, _budget, _pool_perc, _eff_date); 

  --brody
  _tech_perc = .28853; -- select 19/65.8512
  insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
  values(_tech_key, _eff_date, _tech_perc, 19); 	
  
  --josh
  _tech_key = (select tech_key from tp.techs where last_name = 'northagen' and first_name = 'joshua');
  _tech_perc = .32497; -- select 21.4/65.8512
  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;
    
  insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
  values(_tech_key, _eff_date, _tech_perc, 21.4);
  
  --skyler
  _tech_key = (select tech_key from tp.techs where last_name = 'bruns' and first_name = 'skyler');
  _tech_perc = .28853; -- select 19/65.8512
  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;  
    
  insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
  values(_tech_key, _eff_date, _tech_perc, 19);
   
END $$;  

select * from tp.techs order by tech_key