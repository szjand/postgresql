﻿-- Within a pay period for a tech I need:
-- Team Prof
-- Total Team Flag Hours
-- Total Team Clock Hours
-- Employee Commission hours
-- Employee Bonus Hours
-- Comm Pay Rate
-- Bonus Rate
-- Effective Labor Rate
-- (This should be updated nightly to match what was in old vision)
select a.the_date, a.team_prof_pptd, team_flag_hours_pptd + team_flag_hour_adjustments_pptd as flag_hours, team_clock_hours_pptd,
  (tech_clock_hours_pptd + tech_pto_hours_pptd + tech_holiday_hours_pptd) as tech_total_clock_hours,
  case
    when (tech_clock_hours_pptd + tech_pto_hours_pptd + tech_holiday_hours_pptd) > 90 then (tech_clock_hours_pptd + tech_pto_hours_pptd + tech_holiday_hours_pptd) - 90
    else 0
  end as tech_bonus_hours,
  case
    when (tech_clock_hours_pptd + tech_pto_hours_pptd + tech_holiday_hours_pptd) > 90 then
      tech_clock_hours_pptd - ((tech_clock_hours_pptd + tech_pto_hours_pptd + tech_holiday_hours_pptd) - 90)
    else tech_clock_hours_pptd
  end as tech_comm_hours,
  a.tech_tfr_rate as tech_comm_pay_rate,
  2 * a.tech_tfr_rate as tech_bonus_rate,
  91.46 as elr
-- select *
from tp.data a
where a.pay_period_seq = 344
  and a.employee_number = '1110425'
  and a.the_date = current_date

 
-- For each day within a pay period for a tech I need:
-- the date
-- employee flag hours
-- employee clock hours
-- team prof
-- (This should also be updated nightly) (edited)   
select a.the_date, a.first_name || ' ' || a.last_name as tech,
  tech_flag_hours_day, coalesce(tech_clock_hours_day, 0) as tech_clock_hours, team_prof_pptd
-- select *
from tp.data a
where pay_period_seq = 344
  and employee_number = '1110425'
order by a.the_date

-- lists ros/flag hours for a tech for a pay period
-- parameters are: tech employee number, biweekly pay period sequence
select b.tech_name, a.ro, a.flag_date, sum(a.flag_hours) as flag_hours
from dds.fact_repair_order a
join dds.dim_tech b on a.tech_key = b.tech_key
  and b.employee_number = '1110425'  -------------------------------------
where flag_date between 
	(select distinct biweekly_pay_period_start_date from dds.dim_date where biweekly_pay_period_sequence = 344) --------------------
	and 
	(select distinct biweekly_pay_period_end_date from dds.dim_date where biweekly_pay_period_sequence = 344)  ---------------------
group by a.ro, a.flag_date, b.tech_name;

-- lists adjustments/hours for a tech for a pay period
-- parameters are: tech employee number, biweekly pay period sequence
select b.tech_name, a.ro_number, dds.db2_integer_to_date(a.trans_date) as adj_date, sum(labor_hours) as labor_hours
from arkona.ext_sdpxtim a
join dds.dim_tech b on a.technician_id = b.tech_number
  and b.current_row
  and b.employee_number = '1110425' ----------------------------------
where dds.db2_integer_to_date(a.trans_date) between
	(select distinct biweekly_pay_period_start_date from dds.dim_date where biweekly_pay_period_sequence = 344) --------------------------
	and 
	(select distinct biweekly_pay_period_end_date from dds.dim_date where biweekly_pay_period_sequence = 344) 	--------------------------
group by b.tech_name, a.ro_number, dds.db2_integer_to_date(a.trans_date)
having sum(labor_hours) <> 0;


-- team lead view of team performance
-- parameters are: team leader employee number
select b.team_name, b.first_name || ' ' || b.last_name as tech, b.tech_flag_hours_pptd as flag_hours, 
	b.tech_flag_hour_adjustments_pptd as adjustments,
  round(tech_clock_hours_pptd, 2) as clock_hours, team_prof_pptd as team_prof
from tp.team_leaders a
join tp.data b on a.team_key = b.team_key
  and b.the_date = current_date
where a.employee_number = '164015' ---------------------