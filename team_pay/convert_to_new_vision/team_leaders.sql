﻿select * 
from tp.teams 
where thru_date > current_Date
order by team_name

put this together in advantage just to see who has team leader access
have not and do not plan on importing employeeappauthorization
SELECT b.fullname, b.employeenumber, e.teamname, e.teamkey, f.census
FROM employeeappauthorization a
JOIN  tpemployees b on a.username = b.username
JOIN tptechs c on b.employeenumber = c.employeenumber
JOIN tpteamtechs d on c.techkey = d.techkey
  AND d.thrudate > curdate()
JOIN tpteams e on d.teamkey = e.teamkey	
  AND e.thrudate > curdate()
JOIN tpteamvalues f on e.teamkey = f.teamkey
  AND f.thrudate > curdate()	
where approle = 'teamlead'
AND functionality = 'store / team totals'
ORDER BY e.teamname

-- this is what it shows
fullname				employeenumber	teamname				teamkey	census
Kirby Holwerda				167580		Alignment				8					3
Dale Axtman						18005			Axtman					48				2
Jeffery Bear					111210		Bear						22				4
Clayton Gehrtz				150126		Gehrtz					65				3
Gordon David					131370		Gordy						64				1
Wyatt Thompson				1137590		Heffernan				3					4
Josh Heffernan				164015		Heffernan				3					4
Dennis Mcveigh				194675		McVeigh					63				1
Timothy O''neil				1106421		O''Neil					21				2
Chris Walden					1146991		Paint Team			29				4
Peter Jacobson				171055		Paint Team			29				4
Patrick Adam					11660			Paint Team			29				4
Jay Olson						`	1106399		Pre-Diagnosis		51				4
Nathan Gray						152410		Pre-Diagnosis		51				4
Terrance Driscoll			135770		Team 12					41				1
Kyle Buchanan					145789		Team 14					43				1
Codey Olson						1100625		Team 15					44				1
Marcus Eberle					137101		Team 16					45				1
David Bies ii					157953		Team 19					49				1
Brandon Lamont				184614		Team 20					50				1
Benjamin Johnson			184620		Team 21					57				1
Brian Peterson				1110425		Team 22					58				1
Nathan Nichols				160432		Team 23					59				1
Timothy Reuter				1114921		Team 24					61				1
Jonathan Gryskiewicz	122558		Team 25					62				1
Chad Gardner					150105		Team 3					32				2 -- actually 1, i didn't update team values when walton termed
Justin Olson					1106400		Team 6					35				1
Scott Sevigny					1125565		Team 7					36				1


drop table if exists tp.team_leaders cascade;
create table tp.team_leaders (
  team_key integer primary key,
  department_key integer not null, -- 18 main shop, 13 body shop
  team_name citext not null,
  employee_number citext not null);
comment on table tp.team_leaders is 'not all teams will have a team leader, teams of 1 have no team leader,
  this is a hack to get around the advantage access system which is not being imported into new vision/postgresq';

insert into tp.team_leaders values
(8,18,'Alignment','167580'), 
(48,18,'Axtman','18005'), 
(22,18,'Bear','111210'), 
(65,18,'Gehrtz','150126'), 
(3,18,'Heffernan','164015'), 
(21,18,'O''Neil','1106421');

-- 02/13/22 northagen is now team leader for alignment
update tp.team_leaders
set employee_number = '168753'
where team_name = 'alignment';
  