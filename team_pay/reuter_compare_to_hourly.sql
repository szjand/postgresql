﻿techkey 99
team_key = 61  11/21/21 - 2/12/22  team of 1
emp# 1114921
pay periods
04/10 - 04/23
04/24 - 05/07
05/08 - 05/21

select * from tp.tech_values where tech_key = 99
ttperc = 1

select * from tp.team_values where team_key = 61
rate 23.50

select * 
from tp.techs
where last_name = 'reuter'

select * from tp.team_techs where team_key = 61

select * from dds.dim_date where the_date between '04/11/2022' and current_date order by the_date

select * from dds.dim_tech where tech_name like '%reuter%'
tech number 268
tech key
until 4/13 692
after 4/13 1134

select 
  sum(flag_hours) filter (where close_date between '04/10/2022' and '04/23/22') as pp1,
  sum(flag_hours) filter (where close_date between '04/24/2022' and '05/07/22') as pp2,
  sum(flag_hours) filter (where close_date between '05/08/2022' and '05/21/22') as pp3,
  23.50 * sum(flag_hours) filter (where close_date between '04/10/2022' and '04/23/22') as e1,
  23.50 * sum(flag_hours) filter (where close_date between '04/24/2022' and '05/07/22') as e2,
  23.50 * sum(flag_hours) filter (where close_date between '05/08/2022' and '05/21/22') as e3  

from dds.fact_repair_order
where tech_key in (692,1134)
  and close_date between '04/10/2022' and '05/21/2022'

select 
  sum(clock_hours) filter (where the_Date between '04/10/2022' and '04/23/22') as pp1,
  sum(clock_hours) filter (where the_Date between '04/24/2022' and '05/07/22') as pp2,
  sum(clock_hours) filter (where the_Date between '05/08/2022' and '05/21/22') as pp3,
  sum(clock_hours) as total
from ukg.clock_hours
where employee_number = '1114921'
  and the_date between '04/10/2022' and '05/21/2022'

05/08 - 05/21
select 159.27+2120+79.64 = 2358.91
04/24 - 05/07
select 273.75 + 2120 + 136.87  2530.62
04/10 - 04/23
select 383.73 + 2120 + 191.86  2695.59
  