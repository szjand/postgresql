﻿
Jon, 

Dylan Brusseau is currently an hourly tech but I would like to move him to flat rate, starting next pay period at $19. 
The only reason I am sending this e-mail is because its two things at once. A raise from $17 to $19 and move him to flat rate. 
Is a compli submission all I would need to do?
Tim


select aa.team_name, aa.census, aa.budget, aa.pool_percentage as "pool %", cc.last_name, cc.first_name, cc.tech_number, cc.employee_number as emp,
  cc.tech_team_percentage as "tech %", round(aa.budget * cc.tech_team_percentage, 2) as tech_flat_rate, aa.team_key, cc.tech_key
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 13) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date) cc on bb.tech_key = cc.tech_key 
-- where aa.team_name = 'paint team'	
order by aa.team_name, cc.last_name


select * from dds.dim_tech where tech_name like 'brusseau%'

179535  313

-- select * from tp.teams
insert into tp.teams(department_key, team_name, from_date)
values (13, 'Team Brusseau', '02/26/2023');

insert into tp.techs (department_key, tech_number, first_name, last_name, employee_number, from_date)
values(13, '313','Dylan', 'Brusseau', '179535', '02/26/2023');

-- select * from tp.teams where team_name = 'team brusseau' -- 55
-- select * from tp.techs where tech_number = '313'  -- 121
insert into tp.team_techs(team_key, tech_key, from_date)
values(55, 121, '02/26/2023');

-- select * from tp.team_values
insert into tp.team_values(team_key,census,budget,pool_percentage,from_date)
values(55, 1, 19, 1, '02/26/2023');


insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
values (121, '02/26/2023', 1, 19);

