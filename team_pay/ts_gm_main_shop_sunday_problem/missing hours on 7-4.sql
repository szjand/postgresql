﻿/*
7/4/22 from ken:
Sorry for the late notice but it doesn’t look like flag hrs scraped again for Dale,Jared,and Tim

subsequent thread:
No I opened them Saturday 

Sent from my iPhone

> On Jul 4, 2022, at 9:06 AM, Ken Espelund <kespelund@rydellcars.com> wrote:
> 
> 30hr to Tim on ro 16528888
> Dale 61 hr Jared 62 hr on ro 16528889
> 
> Sent from my iPhone
> 
>> On Jul 4, 2022, at 8:05 AM, jandrews@cartiva.com wrote:
>> 
>> Fixed
>> Trying to figure out why 
>> My only question for you is, did those hours get added on Sunday during the day?
>> Thanks
>> jon
>> 
>> -----Original Message-----
>> From: Ken Espelund <kespelund@rydellcars.com> 
>> Sent: Monday, July 4, 2022 5:33 AM
>> To: Jon Andrews <jandrews@cartiva.com>
>> Subject: UKG did not scrape again 
>> 
>> Sorry for the late notice but it doesn’t look like flag hrs scraped again for Dale,Jared,and Tim
>> 
>> Sent from my iPhone
>> 

*/

turns out most teams were short on their hours
reran Function: tp.update_gm_main_shop_flat_rate_payroll_data(), changing the date generation to be
for the pay period 6/19 - 7/2
and got all the hours
axtman/duckstad i understand, ro 16528889 was missing
the others, i dont know what is going on
see the pics in this folder of the ukg import page before and after running the function on the morning of 7/4 (monday)

the data all existed in tpdata

check body shop proficiencies they all look ok
select *
from tp.data
where the_date = '07/02/2022'
  and Department_key = 13
order by team_name  


select *
from tp.data
where the_date = '07/02/2022'
  and Department_key = 18
order by team_name  

select * from dds.dim_tech where tech_name like '%Goodoien%'

select * 
from dds.fact_repair_order a
join dds.dim_tech b on a.tech_key = b.tech_key
  and b.current_row
  and b.tech_name like '%grant%'
where flag_date = '07/02/2022'
  or close_date = '07/02/2022'



select *
from tp.data
where the_date = '07/02/2022'
  and last_name = 'axtman'

  select *
from tp.test_data
where the_date = '07/02/2022'
  and last_name = 'axtman'

select * 
from tp.gm_main_shop_flat_rate_payroll_data
where pay_period_end = '07/02/2022' 
  and last_name = 'axtman'

select * from dds.dim_tech where tech_name like '%duckstad%' and current_row  --956/961  537/595

select *
from dds.fact_repair_order 
where open_date = '07/02/2022'
  and opcode_key = 22107
order by ro_flag_hours  

select  * -- 22017
from dds.dim_opcode
where description like '%shop%'
order by opcode_key

select *
from arkona.ext_sdprhdr limit 10
where ro_technician_id

select * 
from arkona.ext_sdprdet where ro_number = '16528889'
where technician_id in ('537','595')
  and transaction_date > 20220601
order by labor_hours desc  

select * 
from dds.fact_repair_order 
where ro = '16528889'

select * from dds.dim_date where the_date = current_date

select * from dds.dim_date where biweekly_pay_period_sequence = 353

select * from tp.gm_main_shop_flat_rate_payroll_data where pay_period_end = '07/02/2022' and last_name in ('axtman','duckstad')\

				select pay_period_start, pay_period_end, pay_period_seq, pay_period_select_format,
					pay_period_start + 6 as first_saturday,
					c.team_name AS Team, a.last_name, a.first_name, a.employee_number, 
					d.hire_date, d.term_date,
					tech_hourly_rate AS pto_rate, round(tech_tfr_rate, 4) AS flat_rate, 
					2 * round(tech_tfr_rate, 4) AS bonus_rate,
					tech_flag_hours_pptd as flag_hours, tech_Flag_Hour_Adjustments_PPTD as Adjustments,
					tech_flag_hours_pptd + tech_Flag_Hour_Adjustments_PPTD as total_flag_hours,
					team_prof_pptd AS team_prof,
					tech_clock_hours_pptd AS clock_hours, 
					tech_pto_hours_pptd AS pto_hours, 
					tech_holiday_hours_pptd AS hol_hours,
					round(tech_clock_hours_pptd + tech_pto_hours_pptd + tech_holiday_hours_pptd, 2) AS total_hours            
				FROM tp.data a
				INNER JOIN tp.team_techs b on a.tech_key = b.tech_key
					AND a.team_key = b.team_key
				  AND '07/02/2022' BETWEEN b.from_date AND b.thru_date
				LEFT JOIN tp.teams c on b.team_key = c.team_key
				left join ukg.employees d on a.employee_number = d.employee_number
				WHERE a.the_date = '07/02/2022' 
					AND a.department_Key = 18