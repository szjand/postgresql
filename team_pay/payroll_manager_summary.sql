﻿-- select * from tp.data limit 10
-- select distinct pay_period_start, pay_period_end from tp.data order by pay_period_start

DO $$
DECLARE 
  _department integer := 18;
  _payroll_end date := '03/13/2021'; 
begin
drop table if exists wtf;
create temp table wtf as
SELECT x.*, 
  round(comm_Hours * team_Prof/100 * comm_Rate, 2) AS comm_Pay,
  round(bonus_Hours * team_Prof/100 * bonus_Rate, 2) AS bonus_Pay,
--   round(pto_Rate * (vacation_Hours + pto_Hours + holiday_Hours), 2) AS pto_Pay,
  round(comm_Hours * team_Prof/100 * comm_Rate, 2) +  
  round(bonus_Hours * team_Prof/100 * bonus_Rate, 2)  AS total_Pay  
--   round(pto_Rate * (vacation_Hours + pto_Hours + holiday_Hours), 2) AS total_Pay  
FROM (  
  select d.team, d.last_name, d.first_name, d.employee_number, d.pto_Rate, d.comm_Rate, 
    d.bonus_Rate, d.team_Prof, round(d.flag_Hours, 2) as flag_hours, d.adjustments, 
    round(d.flag_hours + d.adjustments, 2) as total_flag_hours, round(e.clock_Hours, 2) as clock_hours, e.vacation_Hours, 
    e.pto_Hours, e.holiday_Hours, round(e.total_Hours, 2) as total_hours,
  -- *a*  
    round(
      CASE 
        WHEN e.total_Hours > 90 and team_prof >= 100 THEN e.clock_hours - (total_hours - 90) 
        else e.clock_Hours 
      end, 2) AS comm_Hours,
    round(
      CASE 
        WHEN total_Hours > 90 and team_prof >= 100 THEN total_Hours - 90 
        ELSE 0 
      END, 2) AS bonus_Hours  
  FROM (
    select c.team_name AS Team, last_name, first_name, employee_number, 
      tech_hourly_rate AS pto_rate, round(tech_tfr_rate, 2) AS comm_rate, 
      2 * round(tech_tfr_rate, 2) AS bonus_rate, -- teamprofpptd AS teamProf, 
      Tech_Flag_Hours_PPTD as Flag_Hours, tech_Flag_Hour_Adjustments_PPTD as Adjustments,
      team_prof_pptd AS Team_Prof   
    FROM tp.data a
    INNER JOIN tp.team_techs b on a.tech_key = b.tech_key
      AND a.team_key = b.team_key
    AND _payroll_end BETWEEN b.from_date AND b.thru_date
    LEFT JOIN tp.Teams c on b.team_Key = c.team_Key
    WHERE the_date = _payroll_end
      AND a.department_Key = 18) d
  LEFT JOIN (
    select employee_number, tech_clock_hours_pptd AS clock_Hours, 
      tech_vacation_hours_pptd AS vacation_Hours,  tech_pto_hours_pptd AS pto_Hours, 
      tech_holiday_hours_pptd AS holiday_Hours,
      tech_clock_hours_pptd + tech_vacation_hours_pptd + tech_pto_hours_pptd + tech_holiday_hours_pptd AS total_Hours  
    FROM tp.data a
    INNER JOIN tp.team_techs b on a.tech_key = b.tech_key
      AND a.team_key = b.team_key
    AND _payroll_end BETWEEN b.from_date AND b.thru_date
  --    AND b.thrudate > curdate()
    LEFT JOIN tp.Teams c on b.team_Key = c.team_Key
    WHERE the_date = _payRoll_End
      AND a.department_Key = _department) e on d.employee_number = e.employee_number) x
ORDER BY team, first_name, last_name;
end $$;

select * from wtf order by last_name