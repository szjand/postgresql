﻿************ doing these on Saturday 10/21, no need to update the data, that will happen tonight **********
-- new team of 1 Jon  Mostad  He will be on his own team $18 hr flat rate.
-- select * from dds.dim_tech where tech_name like '%mostad%'  145675  675, $18/hr 
-- select * from dds.dim_date where the_date = '10/08/2023'
-- select * from tp.teams where department_key = 13
select aa.team_name, 
	aa.team_key, 
	aa.census, aa.budget, aa.pool_percentage as "pool %", 
	cc.last_name, cc.first_name, 
	cc.tech_key, 
	cc.tech_number, cc.employee_number as emp,
  cc.tech_team_percentage as "tech %", round(aa.budget * cc.tech_team_percentage, 2) as tech_flat_rate
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 13) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date) cc on bb.tech_key = cc.tech_key 
where aa.team_name = 'team mostad'	
order by aa.team_name, cc.last_name

-- start value for the sequence was at 58, made no sense
-- ALTER SEQUENCE tp.teams_team_key_seq RESTART WITH 69;
-- made that change 2 weeks ago, seems to have stuck, this script generated a team_key = 70

do $$
declare 
	_eff_date date := '10/22/2023';
	_team_name citext := 'Team Mostad';
	_employee_number citext := '145675';
	_tech_number citext := '675';
	_dept_key integer := 13;
	_first_name citext := 'Jonathon';
	_last_name citext := 'Mostad';
	_team_key integer;
	_tech_key integer;
begin
  insert into tp.teams (department_key,team_name,from_date)
  values(_dept_key,_team_name,_eff_date);

  _team_key = (select max(team_key) from tp.teams where team_name = _team_name);
	
	insert into tp.techs(department_key,tech_number,first_name,last_name,employee_number, from_date)
	values(_dept_key, _tech_number, _first_name, _last_name, _employee_number, _eff_date);

	_tech_key = (select tech_key from tp.techs where last_name = _last_name and first_name = _first_name);

	insert into tp.team_techs(team_key,tech_key,from_date)
	values(_team_key,_tech_key,_eff_date);

	insert into tp.team_values(team_key,census,budget,pool_percentage,from_date)
	values(_team_key, 1, 18, 1, _eff_date); 

  insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
  values(_tech_key, _eff_date, 1, 17);
END $$;  
