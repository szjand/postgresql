﻿select last_name, pay_period_start, pay_period_end, tech_tfr_rate, team_flag_hours_pptd, team_clock_hours_pptd, tech_clock_hours_pptd, team_prof_pptd
-- select * 
from tp.data a
where last_name = 'axtman'
  and the_date = pay_period_end
order by the_date desc
limit 10  

select * 
from dds.dim_opcode
where description like '%shop%'
22107



select a.employee_number, a.last_name, a.pay_period_start, a.pay_period_end, a.tech_tfr_rate, a.team_flag_hours_pptd, a.team_clock_hours_pptd,a.tech_clock_hours_pptd, a.team_prof_pptd, shop_time
from tp.data a
left join (
	select biweekly_pay_period_start_date, biweekly_pay_period_end_date, last_name, sum(shop_time) as shop_time
	from prs.data
	where last_name = 'axtman'
		and the_date between '01/30/2022' and '06/04/2022' -- = biweekly_pay_period_end_date
	group by biweekly_pay_period_start_date, biweekly_pay_period_end_date, last_name) b on a.pay_period_start = b.biweekly_pay_period_start_date
where a.last_name = 'axtman'
  and a.the_date = a.pay_period_end
order by a.the_date desc
limit 20 
  
select biweekly_pay_period_start_date, biweekly_pay_period_end_date, last_name, sum(shop_time)
from prs.data
where last_name = 'axtman'
  and the_date between '01/30/2022' and '06/04/2022' -- = biweekly_pay_period_end_date
group by biweekly_pay_period_start_date, biweekly_pay_period_end_date, last_name  
order by biweekly_pay_period_start_date desc
limit 10  