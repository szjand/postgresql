﻿select the_date, last_name, tech_flag_hours_pptd, team_flag_hours_pptd
from tp.data 
where team_name = 'paint team'
-- where last_name in ('adam','jacobson','walden')
  and the_date in ('02/11/2023', '01/28/2023', '01/14/2023', '12/31/2022', '12/17/2022')
order by the_date desc, last_name  


select sum(flag_hours)  -- 249.2
from dds.fact_repair_order a
join dds.dim_tech b on a.tech_key = b.tech_key
  and b.tech_number = '311'
where a.close_date between '01/29/2023' and  '02/11/2023'


select sum(flag_hours)  -- 101.2
from dds.fact_repair_order a
join dds.dim_tech b on a.tech_key = b.tech_key
  and b.tech_number = '311'
where a.close_date between '01/15/2023' and  '01/28/2023'

select sum(clock_hours) -- 88.12
from ukg.clock_hours  
where employee_number = '1122352'
  and the_date between '01/29/2023' and  '02/11/2023'

select sum(clock_hours)  -- 69.51
from ukg.clock_hours  
where employee_number = '1122352'
  and the_date between '01/15/2023' and  '01/28/2023' 