﻿-- update team leaders
-- this table is require3d for giving team leaders adccess to their entire teams stats

select aa.*, aa.census * 91.46 * aa.pool_percentage as budget_calc, cc.*, aa.budget * cc.tech_team_percentage as tech_rate
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 18) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date) cc on bb.tech_key = cc.tech_key 
order by aa.team_name, cc.last_name


3,18,Heffernan,164015
8,18,Alignment,168753
21,18,O''Neil,1106421
22,18,Bear,111210
48,18,Axtman,18005
65,18,Gehrtz,150126


delete 
from tp.team_leaders
where team_key = 48;

insert into tp.team_leaders values (63, 18, 'McVeigh', '194675');

update tp.team_leaders
set team_name = 'Schwan', employee_number = '1124436'
where team_key = 3;