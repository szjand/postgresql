﻿I would like to move Matt Wingels in the GM main shop to flat rate 1/15/24 as part of Mike Schwans team, 
there will be no rate changes required with the move. I sent Afton a message as well to have the vision 
page also reflect the changes. let me know if that is possible or if you need anything else from me
Mitch Rogers

-- current state, a summary for ken and andrew
-- for my use, add team key & tech key
select aa.team_name, 
	aa.team_key, 
	aa.census, aa.budget, aa.pool_percentage as "pool %", 
	cc.last_name, cc.first_name, 
	cc.tech_key, 
	cc.tech_number, cc.employee_number as emp,
  cc.tech_team_percentage as "tech %", round(aa.budget * cc.tech_team_percentage, 2) as tech_flat_rate
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 18) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date) cc on bb.tech_key = cc.tech_key 
where aa.team_key in (3)	
order by aa.team_name, cc.last_name


-- add Matt Wingels to tp.techs
select * from dds.dim_tech where tech_name like '%wingel%'  14749, 678
SELECT * FROM TP.techs limit 4
insert into tp.techs(department_key,tech_number,first_name,last_name,employee_number,from_date)
values(18,'678','Matthew','Wingels','145749','01/14/2024');

-- add Wingels to tp.team_techs
select * from tp.team_techs where team_key = 3
select * from tp.techs where tech_number = '678'
insert into tp.team_techs(team_key,tech_key,from_date)
values(3,125,'01/14/2024');

-- update tp.team_values for team schwan
select * from tp.team_values where team_key = 3 and thru_date > current_date
-- census is already 4
--   budget = census * ELR * Pool % = select 4 * 91.46 * .29 = 106.0936
-- no changes needed 

-- add wingels to tp.tech_values
select * from tp.tech_values limit 5
tech_team_percentage: flat_rate/budget = select 21/106.0936  
insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
values(125,'01/14/2024',0.19794,21);