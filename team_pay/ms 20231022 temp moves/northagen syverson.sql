﻿Josh Northagens flat rate for Honda is $23.40/hr.  
I would like to guarantee him 130%.  Is there anything I need to do?
Thanks!
Nickolas Neumann

Sorry that it wasnt made clear to you. Josh Syverson will need to be moved from 
Team Mike Schwan over to take the team lead spot with Brody Baily and Skylar Bruns. 
Josh Syversons pay rate will stay the same amount as before the move. Does that make more sense?
25.75
Andrew

-- current state, a summary for ken and andrew
-- for my use, add team key & tech key
select aa.team_name, 
	aa.team_key, 
	aa.census, aa.budget, aa.pool_percentage as "pool %", 
	cc.last_name, cc.first_name, 
	cc.tech_key, 
	cc.tech_number, cc.employee_number as emp,
  cc.tech_team_percentage as "tech %", round(aa.budget * cc.tech_team_percentage, 2) as tech_flat_rate
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 18) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date) cc on bb.tech_key = cc.tech_key 
order by aa.team_name, cc.last_name

1. remove northagen from GM alignment team
2. setup northagen as a honda tech
3. remove syverson from schwann team 
4. move syverson to alignment team

-- 1. remove northagen from GM alignment team -----------------------------------------------------------

do $$
declare
  _tech_key integer := (select tech_key from tp.techs where last_name = 'Northagen' and first_name = 'Joshua');  -- 84
  _team_key integer := (select team_key from tp.teams where team_name = 'Alignment');  -- 8
  _thru_date date := '10/21/2023';
begin
  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;

--   update tp.team_values
--   set thru_date = _thru_date
--   where team_key = _team_key
--     and thru_date > current_date;

  update tp.team_techs
  set thru_date = _thru_date
  where team_key = _team_key
    and tech_key = _tech_key
    and thru_date > current_date;

  update tp.techs
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;

--   update tp.teams
--   set thru_date = _thru_date
--   where team_key = _team_key
--     and thru_date > current_date;

  delete 
--   select *
  from tp.data
  where the_date > _thru_date  -- may or may not want to hard code this date
    and tech_key = _tech_key;
end $$;


select hs.update_hn_main_shop_flat_rate_payroll_data();

select * from hs.hn_main_shop_flat_rate_payroll_data where last_name = 'northagen'

-- 2. setup northagen as a honda tech -----------------------------------------------------------
select * from hs.main_shop_flat_rate_techs

insert into hs.main_shop_flat_rate_techs(employee_number,tech_number,flat_Rate,pto_rate,from_date,thru_date) 
values('168753','294',23.40,23.40,'10/22/2023','12/31/9999');


-- 3. remove syverson from schwann team  -----------------------------------------------------------
-- just remove syverson in tp.team techs, i am assuming that he will be returning to the team

update tp.team_techs
set thru_date = '10/21/2023'
-- select * from tp.team_techs
where team_key = 3
  and tech_key = 18
  and thru_date > current_date;

-- 4. move syverson to alignment team ----------------------------------------------------------------

select * from tp.team_techs where tech_key = 18

insert into tp.team_techs (team_key,tech_key,from_date)
values(8,18,'10/22/2023');

select * from tp.tech_values where tech_key = 18 and thru_date > current_date

alignment budget: 65.8512

select 25.75/65.8512

update tp.tech_values
set thru_date = '10/21/2023'
where tech_key = 18
  and thru_date > current_date;

insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
values(18, '10/22/2023', 0.391033, 31.66) ;

-- remove the tp data for syverson for the current pay period
delete 
select *
from tp.data 
where tech_key = 18
  and the_date > '10/21/2023'

-- fuck me i deleted all the data for the current pay period by mistake
-- should be ok in the morning