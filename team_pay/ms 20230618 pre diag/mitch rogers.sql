﻿7/5/23
Jon, 
We have Mitch on a salary pay system now so could you please remove him from vision completly? 
I am having Kim still pay his tool allowance on his bi-weekly salary check. 
Andrew Neumann 

select aa.team_name, 
	aa.team_key, 
	aa.census, aa.budget, aa.pool_percentage as "pool %", 
	cc.last_name, cc.first_name, 
	cc.tech_key, 
	cc.tech_number, cc.employee_number as emp,
  cc.tech_team_percentage as "tech %", round(aa.budget * cc.tech_team_percentage, 2) as tech_flat_rate
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 18) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date) cc on bb.tech_key = cc.tech_key 
where team_name = 'Pre-Diagnosis'	
order by aa.team_name, cc.last_name


update tp.techs
set thru_date = '07/01/2023'
where tech_key = '79';

update tp.team_techs
set thru_date = '07/01/2023'
-- select * from tp.team_techs
where tech_key = 79
  and team_key = 51
  and thru_date > current_date;

update tp.tech_values 
set thru_date = '07/01/2023'
where tech_key = 79
  and thru_date > current_date;