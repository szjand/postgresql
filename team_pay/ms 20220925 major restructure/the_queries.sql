﻿/*
We have moved several techs around in the shop and put new people with different teams. Long-story short, lots of changes. 
Here are all the latest pay rates for everyone and where they should be. 
All these techs should be on flat rate team based pay except for the following new employees to their role.

William Russell – New Midas Tech
Brody Bailly – New Quick Service Tech
Jared Johnson – New Maintenance Tech 

The Pre-Diagnosis Team and Team Dale A. are still on the hourly guarantee listed on the page. We are working on a 
performance based pay plan for those folks based off how many cars they diagnose on a weekly average. 

Please feel free to ask any questions as this was a lot to do at once but we felt it to be necessary with who best works together. 

Let me know if you see any MISTAKES. 
*/

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  using the tp_test schema !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

-- team budget = census * 91.46 * pool_percentage
select aa.*, aa.census * 91.46 * aa.pool_percentage as budget_calc, cc.*, aa.budget * cc.tech_team_percentage as tech_rate
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 18) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date) cc on bb.tech_key = cc.tech_key 
order by aa.team_name, cc.last_name


-- a summary for ken and andrew
select aa.team_name, aa.census, aa.budget, aa.pool_percentage as "pool %", cc.last_name, cc.first_name, cc.tech_number, cc.employee_number as emp,
  cc.tech_team_percentage as "tech %", round(aa.budget * cc.tech_team_percentage, 2) as tech_flat_rate
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 18) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date) cc on bb.tech_key = cc.tech_key 
order by aa.team_name, cc.last_name


-- refresh
truncate tp.tech_values;
truncate tp.team_values;
truncate tp.team_techs;
truncate tp.teams;
truncate tp.techs;

insert into tp_test.techs 
select * from tp.techs;
insert into tp_test.teams 
select * from tp.teams;
insert into tp_test.team_techs 
select * from tp.team_techs;
insert into tp_test.team_values 
select * from tp.team_values;
insert into tp_test.tech_values 
select * from tp.tech_values;

-- the changes
       ONEIL
Tim O				Tim O
Matt				Matt
Mitchell		Mitchell
Kirby				Lucas K   -----

     SCHWAN
mike s			mike s
wyatt t			wyatt t
josh s			josh s
						alex w --------

   ALIGNMENT
josh n			josh n   						
derrick			skylar b --------
						brody b ---------

    BEAR - MIDAS
jeff b			jeff b	
desiree			billy r ---------- 
						kirby h ----------

			CLAYTON
clayton g		clayton g
nick s			nick s
kodey s     brian g -------
						derek g -------

   PRE DIAG
jay					jay
nate				nate
mitch				maitch
josh h			josh h --------   

	  McVEIGH  --------------
						dennis
						gordon d
						kodey s
*************************************************************************************************
questions
gordy ???  he is on mcveigh
josh h diag guarantee				

Dale Axtman - currently being paid straight hourly
*************************************************************************************************
/*
       ONEIL
Tim O				Tim O
Matt				Matt
Mitchell		Mitchell
Kirby				Lucas K   -----		
*/
-- 1. tech leaving team
-- select * from tp.team_techs where tech_key = 24  and team_key = 21
do $$
  declare
    _eff_date date := '09/25/2022';  
    _thru_date date := '09/24/2022';
		_tech_key integer := (select tech_key from tp.techs where last_name = 'holwerda' and thru_date > current_date);
		_team_key integer := (select team_key from tp.teams where team_name = 'o''neil' and thru_date > current_date);
  begin
    update tp.team_techs
    set thru_date = _thru_date
    where tech_key = _tech_key
      and team_key = _team_key
      and thru_date > current_date;
end $$;
-- 2. add a new tech      
-- select * from tp.techs where first_name = 'lucas'
-- Lucas already exists, just need to add him to team_techs
-- select * from tp.tech_values where tech_key = 50
do $$
  declare
    _eff_date date := '09/25/2022';  
    _thru_date date := '09/24/2022';
		_tech_key integer := (select tech_key from tp.techs where last_name = 'kiefer' and thru_date > current_date);
		_team_key integer := (select team_key from tp.teams where team_name = 'o''neil' and thru_date > current_date);
		_tech_perc numeric;
		_hourly_rate numeric;
  begin
-- lucas kiefer
    -- assign him to team o'neil
    insert into tp.team_techs (team_key,tech_key,from_date) values
    (_team_key, _tech_key, _eff_date);
-- update his tech_values
    -- team budget = select 91.46 * 4 * .28  = 102.4352
    -- tech % = select 26/102.4352 = .25382
    update tp.tech_values
    set thru_date = _thru_date
    where tech_key = _tech_key
      and thru_date > current_date;
    _tech_perc = .25382;
    _hourly_rate = 26;
    insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate) values
    (_tech_key,_eff_date, _tech_perc, _hourly_rate);
-- and the tech_values of the remaining team members
-- timothy o'neil
    _tech_key = (select tech_key from tp.techs where last_name = 'o''neil' and thru_date > current_date);
    _tech_perc = .248938;  -- select 25.5/102.4352
    _hourly_rate = (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
    update tp.tech_values
    set thru_date = _thru_date
    where tech_key = _tech_key
      and thru_date > current_date;
    insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate) values
    (_tech_key,_eff_date, _tech_perc, _hourly_rate);    
-- matthew aamodt    
    _tech_key = (select tech_key from tp.techs where last_name = 'aamodt' and thru_date > current_date);
    _tech_perc = .185484;  -- select 19/102.4352
    _hourly_rate = (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
    update tp.tech_values
    set thru_date = _thru_date
    where tech_key = _tech_key
      and thru_date > current_date;
    insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate) values
    (_tech_key,_eff_date, _tech_perc, _hourly_rate); 
-- mitchell simon  
    _tech_key = (select tech_key from tp.techs where last_name = 'simon' and thru_date > current_date);
    _tech_perc = .24406;  -- select 25/102.4352
    _hourly_rate = (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
    update tp.tech_values
    set thru_date = _thru_date
    where tech_key = _tech_key
      and thru_date > current_date;
    insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate) values
    (_tech_key,_eff_date, _tech_perc, _hourly_rate);     
end $$;

/*
     SCHWAN
mike s			mike s
wyatt t			wyatt t
josh s			josh s
						alex w --------
*/	

-- 1. add a new tech      
-- select * from tp.techs where first_name = 'alex'
-- alex waldbauer already exists, just need to add him to team_techs
-- select * from tp.tech_values where tech_key = 50
do $$
  declare
    _eff_date date := '09/25/2022';  
    _thru_date date := '09/24/2022';
		_team_key integer := (select team_key from tp.teams where team_name = 'schwan' and thru_date > current_date);
		_tech_key integer;
		_tech_perc numeric;
		_hourly_rate numeric;
  begin
-- team budget changes
    update tp.team_values
    set thru_date = _thru_date
    where team_key = _team_key 
      and thru_date > current_date;
    -- budget = 4 * 91.46 * .29 = 106.0936
    insert into tp.team_values(team_key,census,budget,pool_percentage,from_date) values
    (_team_key,4,106.0936,.29,_eff_date);
-- alex waldbauer, already exists in tp.techs  
    _tech_key = (select tech_key from tp.techs where last_name = 'waldbauer' and thru_date > current_date);
    -- is he assigned to a team? select * from tp.team_techs where tech_key = 19 and thru_date > current_date nope
    -- assign him to team schwan
    insert into tp.team_techs(team_key,tech_key,from_date) values
    (_team_key, _tech_key, _eff_date);
  -- and update his tech_values  select * from tp.tech_values where tech_key = 19 and thru_date > current_date
    _tech_perc = .22622;  -- select 24/106.0936
    _hourly_rate = (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
    update tp.tech_values
    set thru_date = _thru_date
    where tech_key = _tech_key
      and thru_date > current_date;
    insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate) values
    (_tech_key,_eff_date, _tech_perc, _hourly_rate);   
-- michael schwan
    _tech_key = (select tech_key from tp.techs where last_name = 'schwan' and thru_date > current_date);
    _tech_perc = .28277;  -- select 30/106.0936
    _hourly_rate = (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
    update tp.tech_values
    set thru_date = _thru_date
    where tech_key = _tech_key
      and thru_date > current_date;
    insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate) values
    (_tech_key,_eff_date, _tech_perc, _hourly_rate);   
-- wyatt thompson
    _tech_key = (select tech_key from tp.techs where last_name = 'thompson' and thru_date > current_date);
    _tech_perc = .24978;  -- select 26.5/106.0936
    _hourly_rate = (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
    update tp.tech_values
    set thru_date = _thru_date
    where tech_key = _tech_key
      and thru_date > current_date;
    insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate) values
    (_tech_key,_eff_date, _tech_perc, _hourly_rate);   
-- joshua syverson
    _tech_key = (select tech_key from tp.techs where last_name = 'syverson' and thru_date > current_date);
    _tech_perc = .242711;  -- select 25.75/106.0936
    _hourly_rate = (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
    update tp.tech_values
    set thru_date = _thru_date
    where tech_key = _tech_key
      and thru_date > current_date;
    insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate) values
    (_tech_key,_eff_date, _tech_perc, _hourly_rate);                    
end $$;

 /*
    ALIGNMENT
josh n			josh n   						
derrick			skylar b --------
						brody b ---------
*/			
-- separate  blocks for adding the techs
even that does not work
add the techs manually, then run the bloclk
very weird

insert into tp.techs(department_key,tech_number,first_name,last_name,employee_number,from_date) 
values(18, '663','Skyler','Bruns','153442', '09/25/2022');  
	
do $$
  declare
    _eff_date date := '09/25/2022';  
    _thru_date date := '09/24/2022';
		_team_key integer := (select team_key from tp.teams where team_name = 'alignment' and thru_date > current_date);
		_tech_key integer;
		_tech_perc numeric;
		_hourly_rate numeric;
  begin
-- remove Derek Goodoien from the team
		_tech_key = (select tech_key from tp.techs where last_name = 'goodoien' and thru_date > current_date);
		update tp.team_techs
		set thru_date = _thru_date
		where team_key = _team_key
		  and tech_key = _tech_key
		  and thru_date > current_date;
-- add 1 new tech skyler bruns
		_tech_key = (select tech_key from tp.techs where last_name = 'bruns' and thru_date > current_date);
		insert into tp.team_techs(team_key,tech_key,from_date)
		values(_team_key, _tech_key, _eff_date);			

-- team budget changes
    update tp.team_values
    set thru_date = _thru_date
    where team_key = _team_key 
      and thru_date > current_date;
    -- budget = select 2 * 91.46 * .24 = 43.9008
    insert into tp.team_values(team_key,census,budget,pool_percentage,from_date) values
    (_team_key, 2, 43.9008, .24, _eff_date);			 
-- joshua northagen
    _tech_key = (select tech_key from tp.techs where last_name = 'northagen' and thru_date > current_date);
    _tech_perc = .487463;  -- select 21.4/43.9008
    _hourly_rate = (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
    update tp.tech_values
    set thru_date = _thru_date
    where tech_key = _tech_key
      and thru_date > current_date;
    insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate) values
    (_tech_key,_eff_date, _tech_perc, _hourly_rate);   
-- skyler bruns
    _tech_key = (select tech_key from tp.techs where last_name = 'bruns' and thru_date > current_date);
    _tech_perc = .432794;  -- select 19/43.9008
--     _hourly_rate = (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
    update tp.tech_values
    set thru_date = _thru_date
    where tech_key = _tech_key
      and thru_date > current_date;
    insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate) values
    (_tech_key,_eff_date, _tech_perc, 19); 
   
end $$;    

/*
    BEAR - MIDAS
jeff b			jeff b	
desiree			billy r ---------- 
						kirby h ----------
*/				

do $$
  declare
    _eff_date date := '09/25/2022';  
    _thru_date date := '09/24/2022';
		_team_key integer := (select team_key from tp.teams where team_name = 'bear' and thru_date > current_date);
		_tech_key integer;
		_tech_perc numeric;
		_hourly_rate numeric;
  begin
-- remove desiree grant from the team
		_tech_key = (select tech_key from tp.techs where last_name = 'grant' and thru_date > current_date);
		update tp.team_techs
		set thru_date = _thru_date
		where team_key = _team_key
		  and tech_key = _tech_key
		  and thru_date > current_date;		
-- add 1 new tech: kirby holwerda
		_tech_key = (select tech_key from tp.techs where last_name = 'holwerda' and thru_date > current_date);
		insert into tp.team_techs(team_key,tech_key,from_date)
		values(_team_key, _tech_key, _eff_date);			
-- team budget changes
    update tp.team_values
    set thru_date = _thru_date
    where team_key = _team_key 
      and thru_date > current_date;
    -- budget = select 2 * 91.46 * .24 = 43.9008
    insert into tp.team_values(team_key,census,budget,pool_percentage,from_date) values
    (_team_key,2,43.9008,.24 ,_eff_date);				
-- jeff bear
    _tech_key = (select tech_key from tp.techs where last_name = 'bear' and thru_date > current_date);
    _tech_perc = .544182;  -- select 23.89/43.9008
    _hourly_rate = (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
    update tp.tech_values
    set thru_date = _thru_date
    where tech_key = _tech_key
      and thru_date > current_date;
    insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate) values
    (_tech_key,_eff_date, _tech_perc, _hourly_rate);    
-- kirby holwerda
    _tech_key = (select tech_key from tp.techs where last_name = 'holwerda' and thru_date > current_date);
    _tech_perc = .478352;  -- select 21/43.9008
    _hourly_rate = (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
    update tp.tech_values
    set thru_date = _thru_date
    where tech_key = _tech_key
      and thru_date > current_date;
    insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate) values
    (_tech_key,_eff_date, _tech_perc, _hourly_rate);     
end $$;        		  

/*
			CLAYTON
clayton g		clayton g
nick s			nick s
kodey s     brian g -------
						derek g -------
*/		

-- first add the new tech manually
insert into tp.techs(department_key,tech_number,first_name,last_name,employee_number,from_date) 
values(18, '662','Bryan','Gowen','153659', '09/25/2022'); 			

do $$
  declare
    _eff_date date := '09/25/2022';  
    _thru_date date := '09/24/2022';
		_team_key integer := (select team_key from tp.teams where team_name = 'gehrtz' and thru_date > current_date);
		_tech_key integer;
		_tech_perc numeric;
		_hourly_rate numeric;
  begin
-- remove kodey schuppert from the team
		_tech_key = (select tech_key from tp.techs where last_name = 'schuppert' and thru_date > current_date);
		update tp.team_techs
		set thru_date = _thru_date
		where team_key = _team_key
		  and tech_key = _tech_key
		  and thru_date > current_date;		
-- add 2 new techs to the team: bryan gowen & derek goodoien
		_tech_key = (select tech_key from tp.techs where last_name = 'gowen' and thru_date > current_date);
		insert into tp.team_techs(team_key,tech_key,from_date)
		values(_team_key, _tech_key, _eff_date);			
		_tech_key = (select tech_key from tp.techs where last_name = 'goodoien' and thru_date > current_date);
		insert into tp.team_techs(team_key,tech_key,from_date)
		values(_team_key, _tech_key, _eff_date);	
-- team budget changes
    update tp.team_values
    set thru_date = _thru_date
    where team_key = _team_key 
      and thru_date > current_date;
    -- budget = select 4 * 91.46 * .3 = 109.752
    insert into tp.team_values(team_key,census,budget,pool_percentage,from_date) values
    (_team_key,4,109.752,.3 ,_eff_date);				
-- clayton gehrtz
    _tech_key = (select tech_key from tp.techs where last_name = 'gehrtz' and thru_date > current_date);
    _tech_perc = .287011;  -- select 31.5/109.752
    _hourly_rate = (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
    update tp.tech_values
    set thru_date = _thru_date
    where tech_key = _tech_key
      and thru_date > current_date;
    insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate) values
    (_tech_key,_eff_date, _tech_perc, _hourly_rate);  
-- nicholas soberg
    _tech_key = (select tech_key from tp.techs where last_name = 'soberg' and thru_date > current_date);
    _tech_perc = .218675;  -- select 24/109.752
    _hourly_rate = (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
    update tp.tech_values
    set thru_date = _thru_date
    where tech_key = _tech_key
      and thru_date > current_date;
    insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate) values
    (_tech_key,_eff_date, _tech_perc, _hourly_rate);      
-- derek goodoien
    _tech_key = (select tech_key from tp.techs where last_name = 'goodoien' and thru_date > current_date);
    _tech_perc = .17312;  -- select 19/109.752
    _hourly_rate = (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
    update tp.tech_values
    set thru_date = _thru_date
    where tech_key = _tech_key
      and thru_date > current_date;
    insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate) values
    (_tech_key,_eff_date, _tech_perc, _hourly_rate);    
-- bryan gowen
    _tech_key = (select tech_key from tp.techs where last_name = 'gowen' and thru_date > current_date);
    _tech_perc = .18223;  -- select 20/109.752
    insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate) values
    (_tech_key,_eff_date, _tech_perc, 20);    
end $$;        

/*
	  McVEIGH  --------------
						dennis
						gordon d
						kodey s
*/						

do $$
  declare
    _eff_date date := '09/25/2022';  
    _thru_date date := '09/24/2022';
		_team_key integer;
		_tech_key integer;
		_tech_perc numeric;
		_hourly_rate numeric;
  begin
-- close out team gordy
    _team_key = (select team_key from tp.teams where team_name = 'gordy' and thru_date > current_date);
    update tp.teams
    set thru_date = _thru_date
    where team_key = _team_key;
-- close out team techs for team gordy
    update tp.team_techs
    set thru_date = _thru_date
    where team_key = _team_key
      and thru_date > current_date;
-- add 2 new techs to the team: kody schuppert & gordon david
    _team_key = (select team_key from tp.teams where team_name = 'mcveigh' and thru_date > current_date);
		_tech_key = (select tech_key from tp.techs where last_name = 'schuppert' and thru_date > current_date);
		insert into tp.team_techs(team_key,tech_key,from_date)
		values(_team_key, _tech_key, _eff_date);			
		_tech_key = (select tech_key from tp.techs where last_name = 'david' and thru_date > current_date);
		insert into tp.team_techs(team_key,tech_key,from_date)
		values(_team_key, _tech_key, _eff_date);	
-- team budget changes
    update tp.team_values
    set thru_date = _thru_date
    where team_key = _team_key 
      and thru_date > current_date;
    -- budget = select 3 * 91.46 * .3 = 82.314
    insert into tp.team_values(team_key,census,budget,pool_percentage,from_date) values
    (_team_key,3,82.314,.3 ,_eff_date);				
-- dennis mcveigh
    _tech_key = (select tech_key from tp.techs where last_name = 'mcveigh' and thru_date > current_date);
    _tech_perc = .382681;  -- select 31.5/82.314
    _hourly_rate = (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
    update tp.tech_values
    set thru_date = _thru_date
    where tech_key = _tech_key
      and thru_date > current_date;
    insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate) values
    (_tech_key,_eff_date, _tech_perc, _hourly_rate);  
-- kody schuppert
    _tech_key = (select tech_key from tp.techs where last_name = 'schuppert' and thru_date > current_date);
    _tech_perc = .26727;  -- select 22/82.314
    _hourly_rate = (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
    update tp.tech_values
    set thru_date = _thru_date
    where tech_key = _tech_key
      and thru_date > current_date;
    insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate) values
    (_tech_key,_eff_date, _tech_perc, _hourly_rate);      
-- gordon david
    _tech_key = (select tech_key from tp.techs where last_name = 'david' and thru_date > current_date);
    _tech_perc = .247346;  -- select 20.36/82.314
    _hourly_rate = (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
    update tp.tech_values
    set thru_date = _thru_date
    where tech_key = _tech_key
      and thru_date > current_date;
    insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate) values
    (_tech_key,_eff_date, _tech_perc, _hourly_rate);  
end $$;  

/*
   PRE DIAG
jay					jay
nate				nate
mitch				maitch
josh h			josh h --------   
add josh to tp.blueprint_guarantee_rates
select * from tp.blueprint_guarantee_rates where thru_date > current_date
insert into tp.blueprint_guarantee_rates(employee_number, name,hourly_rate,pto_rate,from_date,thru_date)
values('164015','heffernan, josh',35.13,35.41,'09/25/2022','12/31/9999');
*/

do $$
  declare
    _eff_date date := '09/25/2022';  
    _thru_date date := '09/24/2022';
		_team_key integer;
		_tech_key integer;
		_tech_perc numeric;
		_hourly_rate numeric;
  begin
-- close out team josh
    _team_key = (select team_key from tp.teams where team_name = 'josh' and thru_date > current_date);
    update tp.teams
    set thru_date = _thru_date
    where team_key = _team_key;
-- close out team techs for team josh
    update tp.team_techs
    set thru_date = _thru_date
    where team_key = _team_key
      and thru_date > current_date;
-- add josh heffernan to the team
    _team_key = (select team_key from tp.teams where team_name = 'pre-diagnosis' and thru_date > current_date);
		_tech_key = (select tech_key from tp.techs where last_name = 'heffernan' and thru_date > current_date);
		insert into tp.team_techs(team_key,tech_key,from_date)
		values(_team_key, _tech_key, _eff_date);		
-- team budget changes
    update tp.team_values
    set thru_date = _thru_date
    where team_key = _team_key 
      and thru_date > current_date;
    -- budget = select 4 * 91.46 * .4 = 146.336
    insert into tp.team_values(team_key,census,budget,pool_percentage,from_date) values
    (_team_key,4,146.336,.4 ,_eff_date);		
-- jay olson
    _tech_key = (select tech_key from tp.techs where last_name = 'olson' and first_name = 'jay' and thru_date > current_date);
    _tech_perc = .263709;  -- select 38.59/146.336
    _hourly_rate = (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
    update tp.tech_values
    set thru_date = _thru_date
    where tech_key = _tech_key
      and thru_date > current_date;
    insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate) values
    (_tech_key,_eff_date, _tech_perc, _hourly_rate);     	      
-- nathan gray
    _tech_key = (select tech_key from tp.techs where last_name = 'gray' and thru_date > current_date);
    _tech_perc = .20248;  -- select 29.63/146.336
    _hourly_rate = (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
    update tp.tech_values
    set thru_date = _thru_date
    where tech_key = _tech_key
      and thru_date > current_date;
    insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate) values
    (_tech_key,_eff_date, _tech_perc, _hourly_rate);     
-- josh heffernan
    _tech_key = (select tech_key from tp.techs where last_name = 'heffernan' and thru_date > current_date);
    _tech_perc = .240064;  -- select 35.13/146.336
    _hourly_rate = (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
    update tp.tech_values
    set thru_date = _thru_date
    where tech_key = _tech_key
      and thru_date > current_date;
    insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate) values
    (_tech_key,_eff_date, _tech_perc, _hourly_rate);      
-- mitch rogers
    _tech_key = (select tech_key from tp.techs where last_name = 'rogers' and thru_date > current_date);
    _tech_perc = .20501;  -- select 30/146.336
    _hourly_rate = (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
    update tp.tech_values
    set thru_date = _thru_date
    where tech_key = _tech_key
      and thru_date > current_date;
    insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate) values
    (_tech_key,_eff_date, _tech_perc, _hourly_rate); 
end $$;       


delete 
-- select *
from tp.data
where the_date > '09/24/2022'
  and department_key = 18;

select tp.data_update_nightly(); 

