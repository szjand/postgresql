﻿do $$
declare
  _current_date date := (select current_date);
  _day_in_pp integer := (
    SELECT day_in_biweekly_pay_period
    FROM dds.dim_date
    WHERE the_date = current_date);  
  _cur_pp_from_date date := (
    select biweekly_pay_period_start_date
    from dds.dim_date
    where the_date = current_date);
  _prev_pp_from_date date := (
    select distinct biweekly_pay_period_start_date
    from dds.dim_date
    where biweekly_pay_period_sequence = (
      select biweekly_pay_period_sequence - 1
      from dds.dim_date
      where the_date = _current_date));
  _from_date date := (
    select 
      case
        when _day_in_pp > 5 then _cur_pp_from_date
        else _prev_pp_from_date
      end);
begin      

----------------------------------------------------------------------------------------------------------

-- delete from tp.data where the_date between current_Date - 1 and current_date;

-- delete from tp.data where pay_period_seq in (309,310);

-- delete from tp.data where the_date = current_date;
----------------------------------------------------------------------------------------------------------

-- base values for a new row
    INSERT INTO tp.data(the_date,pay_period_start,pay_period_end,pay_period_seq,
      day_of_pay_period, day_name, 
      department_key, tech_key, tech_number, employee_number, first_name, last_name)  
    SELECT a.the_date, a.biweekly_pay_period_start_date, a.biweekly_pay_period_end_date, 
      a.biweekly_pay_period_sequence, a.day_in_biweekly_pay_period, a.day_name,
      b.department_key, -- main shop, not implementing zDepartments from  ads
      b.tech_key, b.tech_number, b.employee_number, b.first_name, b.last_name
    FROM dds.dim_date a, tp.techs b 
    WHERE a.the_date BETWEEN _from_date AND current_date
	    AND b.department_key in (13,18) -- no need for separate queries for main and body shop
      AND NOT EXISTS (
        SELECT 1
        FROM tp.data
        WHERE the_date = a.the_date
          AND tech_key = b.tech_key)
      AND EXISTS (-- 12/5, techs gone, but still exist IN tpTechs, don't want them IN tpData
        SELECT 1
        FROM tp.team_techs
        WHERE tech_key = b.tech_key
          AND a.the_date BETWEEN from_date AND thru_date); 

  -- pay_period_select_format for new row
    UPDATE tp.data a
    SET pay_period_select_format = z.ppsel
    FROM (
      SELECT pay_period_seq, TRIM(pt1) || ' ' || TRIM(pt2) AS ppsel
        FROM (  
        SELECT distinct pay_period_start, pay_period_end, pay_period_seq, 
          (SELECT trim(month_name) || ' '
            || trim(day_of_month::text) || ' - '
            FROM dds.dim_date
            WHERE the_date = a.pay_period_start) AS pt1,
          (SELECT trim(month_name) || ' ' 
            || TRIM(day_of_month::text) || ', ' 
            || TRIM(the_year::text)
            FROM dds.dim_date
            WHERE the_date = a.pay_period_end) AS pt2
        FROM tp.data a) y) z
    WHERE a.pay_period_seq = z.pay_period_seq;    

  -- teamname/key for new row
    UPDATE tp.Data aa
    SET Team_Key = x.team_key,
        team_name = x.team_name
    FROM (    
      SELECT *
      FROM ( 
        SELECT the_date
        FROM tp.Data
        GROUP BY the_date) a
      INNER JOIN (  
        SELECT a.tech_key, c.team_key, c.team_name, b.from_date, b.thru_date
        FROM tp.techs a
        INNER JOIN tp.team_techs b on a.tech_key = b.tech_key
        INNER JOIN tp.teams c on b.team_key = c.team_key) b on a.the_date BETWEEN b.from_date AND b.thru_date) x
    WHERE aa.Tech_Key = x.tech_key
      AND aa.the_Date = x.the_Date;             
    UPDATE tp.Data aa
      SET team_Census = x.census,
          team_Budget = x.budget
      FROM tp.Team_Values x
      WHERE (aa.team_Census IS NULL OR aa.team_Budget IS NULL) 
        AND aa.team_Key = x.team_Key
        AND aa.the_Date BETWEEN x.from_Date AND x.thru_Date; 	 

  -- techteamPercentage AND techHourlyRate: FROM tpTechValues for new row
    UPDATE tp.data
    SET Tech_Team_Percentage = x.Tech_Team_Percentage,
	    tech_Hourly_Rate = x.Hourly_Rate
    FROM (
      SELECT a.the_date, b.tech_key, b.tech_Team_Percentage, b.hourly_Rate
      FROM tp.Data a
      INNER JOIN tp.Tech_Values b on a.tech_Key = b.tech_Key
        AND a.the_date BETWEEN b.from_date AND b.thru_date) x
    WHERE tp.Data.Tech_Key = x.Tech_Key
      AND tp.Data.the_date = x.the_Date;    
      
  -- TechTFRRate for new row 
    UPDATE tp.data
    SET Tech_TFR_Rate = tech_team_percentage * team_budget;      

-- techs
  -- ClockHoursDay    
    UPDATE tp.data aa
    SET Tech_Clock_hours_Day = x.clock_hours,
        Tech_Vacation_Hours_Day = x.Vacation,
        Tech_PTO_Hours_Day = x.PTO,
        Tech_Holiday_Hours_Day = x.Holiday
    FROM (    
      SELECT a.the_date, c.pymast_employee_number, SUM(clock_hours) AS clock_hours,
        SUM(coalesce(vac_hours,0)) AS vacation,
        SUM(coalesce(PTO_Hours,0)) AS pto,
        SUM(coalesce(hol_hours,0)) AS holiday
      FROM arkona.xfm_pypclockin a  -- no clock hours fact
--       INNER JOIN dds.dim_date b on a.the_date = b.the_date
--         AND b.the_date BETWEEN _from_date AND current_date
      LEFT JOIN arkona.ext_pymast c on a.employee_number = c.pymast_employee_number -- no employee dim
      where a.the_Date between _from_date and current_date
      GROUP BY a.the_date, c.pymast_employee_number) x 
    WHERE aa.the_date = x.the_date
      AND aa.employee_number = x.pymast_employee_number;    

  -- ClockHoursPPTD
    UPDATE tp.Data aa
    SET Tech_Clock_Hours_PPTD = x.PPTD
    FROM (
      SELECT a.the_date, a.tech_key, a.pay_period_seq, a.tech_clock_hours_day, SUM(b.tech_clock_hours_day) AS pptd
      FROM tp.Data a, tp.data b
      WHERE a.pay_period_seq = b.pay_period_seq
        AND a.the_date BETWEEN _from_date AND current_Date
        AND a.tech_key = b.tech_key
        AND b.the_date <= a.the_date
      GROUP BY a.the_date, a.tech_key, a.pay_period_seq, a.tech_clock_hours_day) x
    WHERE aa.the_date = x.the_date
      AND aa.tech_key = x.tech_key;   

    -- TechVacationPPTD  
    UPDATE tp.Data aa
    SET Tech_Vacation_Hours_PPTD = x.PPTD
    FROM (
      SELECT a.the_date, a.tech_key, a.pay_period_seq, a.Tech_Vacation_Hours_Day, SUM(b.Tech_Vacation_Hours_Day) AS pptd
      FROM tp.Data a, tp.data b
      WHERE a.pay_period_seq = b.pay_period_seq
        AND a.the_date BETWEEN _from_date AND current_date    
        AND a.tech_key = b.tech_key
        AND b.the_date <= a.the_date
      GROUP BY a.the_date, a.tech_key, a.pay_period_seq, a.Tech_Vacation_Hours_Day) x
    WHERE aa.the_date = x.the_date
      AND aa.tech_key = x.tech_key;   
      
    -- TechPTOHoursPPTD
    UPDATE tp.Data aa
    SET Tech_PTO_Hours_PPTD = x.PPTD
    FROM (
      SELECT a.the_date, a.tech_key, a.pay_period_seq, a.Tech_PTO_Hours_Day, SUM(b.Tech_PTO_Hours_Day) AS pptd
      FROM tp.Data a, tp.data b
      WHERE a.pay_period_seq = b.pay_period_seq
        AND a.the_date BETWEEN _from_date AND current_date     
        AND a.tech_key = b.tech_key
        AND b.the_date <= a.the_date
      GROUP BY a.the_date, a.tech_key, a.pay_period_seq, a.Tech_PTO_Hours_Day) x
    WHERE aa.the_date = x.the_date
      AND aa.tech_key = x.tech_key;  
      
    -- TechHolidayHoursPPTD
    UPDATE tp.Data aa
    SET Tech_Holiday_Hours_PPTD = x.PPTD
    FROM (
      SELECT a.the_date, a.tech_key, a.pay_period_seq, a.Tech_Holiday_Hours_Day, SUM(b.Tech_Holiday_Hours_Day) AS pptd
      FROM tp.Data a, tp.data b
      WHERE a.pay_period_seq = b.pay_period_seq
        AND a.the_date BETWEEN _from_date AND current_date
        AND a.tech_key = b.tech_key
        AND b.the_date <= a.the_date
      GROUP BY a.the_date, a.tech_key, a.pay_period_seq, a.Tech_Holiday_Hours_Day) x
    WHERE aa.the_date = x.the_date
      AND aa.tech_key = x.tech_key;      

-- flag hours  
    -- TechFlagHoursDay need to do main and body shop separately, main based on flag date, body on close date
    -- main shop
    UPDATE tp.Data aa
    SET Tech_Flag_Hours_Day = x.flag_hours
    FROM (
      SELECT a.tech_number, a.the_date, round(sum(coalesce(b.flag_hours, 0)), 2) AS flag_hours
      FROM tp.Data a
      LEFT JOIN (
        SELECT a.flag_date, a.ro, a.line, c.tech_number, a.flag_hours
        FROM dds.fact_Repair_Order a  
        JOIN dds.dim_Tech c on a.tech_key = c.tech_key
--         INNER JOIN dds.day d on a.flagdatekey = d.datekey
        WHERE a.flag_hours <> 0
          AND a.store_code = 'ry1'
          and a.flag_date between _from_date and current_date
--           AND a.flagdatekey IN (
--             SELECT datekey
--             FROM dds.day
-- --*c*            
-- --            WHERE thedate BETWEEN '01/11/2015' AND curdate())
--             WHERE thedate BETWEEN @from_date AND curdate())     
          AND c.tech_number IN (
            SELECT tech_number
            FROM tp.Techs)) b on a.tech_number = b.tech_number AND a.the_date = b.flag_date   
      WHERE a.the_date BETWEEN _from_date AND current_date
      GROUP BY a.tech_number, a.the_date) x
    WHERE aa.department_key = 18 -- (SELECT departmentkey FROM zDepartments WHERE dl2 = 'ry1' AND dl4 = 'mainshop' AND dl5 = 'Shop')
      AND aa.the_date = x.the_date
      AND aa.tech_number = x.tech_number;    

    -- body shop
    UPDATE tp.Data aa
    SET Tech_Flag_Hours_Day = x.flag_hours
    FROM (
      SELECT a.tech_number, a.the_date, round(sum(coalesce(b.flag_hours, 0)), 2) AS flag_hours
      FROM tp.Data a
      LEFT JOIN (
        SELECT a.close_date, a.ro, a.line, c.tech_number, a.flag_hours
        FROM dds.fact_Repair_Order a  
        JOIN dds.dim_Tech c on a.tech_key = c.tech_key
--         INNER JOIN dds.day d on a.flagdatekey = d.datekey
        WHERE a.flag_hours <> 0
          AND a.store_code = 'ry1'
          and a.close_date between _from_date and current_date
--           AND a.flagdatekey IN (
--             SELECT datekey
--             FROM dds.day
-- --*c*            
-- --            WHERE thedate BETWEEN '01/11/2015' AND curdate())
--             WHERE thedate BETWEEN @from_date AND curdate())     
          AND c.tech_number IN (
            SELECT tech_number
            FROM tp.Techs)) b on a.tech_number = b.tech_number AND a.the_date = b.close_date   
      WHERE a.the_date BETWEEN _from_date AND current_date
      GROUP BY a.tech_number, a.the_date) x
    WHERE aa.department_key = 13 -- (SELECT departmentkey FROM zDepartments WHERE dl2 = 'ry1' AND dl4 = 'bodyshop')
      AND aa.the_date = x.the_date
      AND aa.tech_number = x.tech_number;     

    -- TechFlagHoursPPTD  
    UPDATE tp.Data aa
    SET Tech_Flag_Hours_PPTD = x.PPTD
    FROM (
      SELECT a.the_date, a.tech_key, a.pay_period_seq, a.tech_flag_hours_day, SUM(b.tech_flag_hours_day) AS pptd
      FROM tp.Data a, tp.data b
      WHERE a.pay_period_seq = b.pay_period_seq
        AND a.tech_key = b.tech_key
        AND b.the_date <= a.the_date
      GROUP BY a.the_date, a.tech_key, a.pay_period_seq, a.tech_flag_hours_day) x
    WHERE aa.the_date = x.the_date
      AND aa.tech_key = x.tech_key;        

-- adjustments
    UPDATE tp.Data aa
    SET Tech_Flag_Hour_Adjustments_Day = x.Adj
    FROM (
      SELECT a.tech_number, a.last_name, a.the_date, coalesce(Adj, 0) AS Adj
      FROM tp.Data a
      LEFT JOIN (  
        select a.technician_id, dds.db2_integer_to_date(a.trans_date) as trans_Date, round(SUM(a.labor_hours), 2) AS adj
        FROM arkona.ext_sdpxtim a       
        jOIN tp.Techs b on a.technician_id = b.tech_number
        WHERE dds.db2_integer_to_date(a.trans_date) BETWEEN _from_date AND current_date
        GROUP BY a.technician_id, dds.db2_integer_to_date(a.trans_date)) b on a.tech_number = b.technician_id AND a.the_date = b.trans_date
      WHERE a.the_date BETWEEN _from_date AND current_date) x    
    WHERE aa.tech_number = x.tech_number
      AND aa.the_date = x.the_date;     

    -- tech flag time adjustments pptd 
    UPDATE tp.Data aa
    SET Tech_Flag_Hour_Adjustments_PPTD = x.pptd
    FROM (
      SELECT a.the_date, a.tech_key, a.pay_period_seq, a.Tech_Flag_Hour_Adjustments_Day, SUM(b.Tech_Flag_Hour_Adjustments_Day) AS pptd
      FROM tp.Data a, tp.data b
      WHERE a.pay_period_seq = b.pay_period_seq
        AND a.the_date BETWEEN _from_date AND current_Date    
        AND a.tech_key = b.tech_key
        AND b.the_date <= a.the_date
      GROUP BY a.the_date, a.tech_key, a.pay_period_seq, a.Tech_Flag_Hour_Adjustments_Day) x
    WHERE aa.the_date = x.the_date
      AND aa.tech_key = x.tech_key;         

-- teams
    -- teamclockhoursDay
    UPDATE tp.Data aa
    SET team_clock_hours_day = x.team_clock_hours_day
    FROM (
      SELECT the_date, team_key, SUM(tech_clock_hours_day) AS team_clock_hours_day
      FROM tp.data  
      WHERE the_date BETWEEN _from_date AND current_date
      GROUP BY the_date, team_key) x 
    WHERE aa.the_date = x.the_date
      AND aa.team_key = x.team_key;  

    -- teamclockhours pptd
    UPDATE tp.Data aa
    SET team_clock_hours_pptd = x.pptd
    FROM (
      SELECT a.the_date, a.team_key, a.pay_period_seq, a.day_hours, round(SUM(b.day_hours), 2) AS pptd
      FROM (
        SELECT the_date, team_key, team_name, pay_period_seq, team_clock_hours_day AS day_hours
        FROM tp.data
        GROUP BY the_date, team_key, team_name, pay_period_seq, team_clock_hours_day) a,
        (
          SELECT the_date, team_key, team_name, pay_period_seq, team_clock_hours_day AS day_hours
          FROM tp.data
          GROUP BY the_date, team_key, team_name, pay_period_seq, team_clock_hours_day) b  
      WHERE a.pay_period_seq = b.pay_period_seq
        AND a.the_date BETWEEN _from_date AND current_date      
        AND a.team_key = b.team_key
        AND b.the_date <= a.the_date
      GROUP BY a.the_date, a.team_key, a.pay_period_seq, a.day_hours) x
    WHERE aa.the_date = x.the_date
      AND aa.team_key = x.team_key; 

    -- teamflaghoursday
    UPDATE tp.Data aa
    SET Team_Flag_Hours_Day = x.hours
    FROM (
      SELECT the_date, team_key, SUM(tech_flag_hours_day) AS hours
      FROM tp.data 
      WHERE the_date BETWEEN _from_date AND current_Date  
      GROUP BY the_date, team_key) x
    WHERE aa.the_date = x.the_date
      AND aa.team_key = x.team_key;

    -- teamflaghour pptd  
    UPDATE tp.Data aa
    SET team_flag_hours_pptd = x.pptd
    FROM (
      SELECT a.the_date, a.team_key, a.pay_period_seq, a.day_hours, round(SUM(b.day_hours), 2) AS pptd
      FROM (
        SELECT the_date, team_key, team_name, pay_period_seq, team_flag_hours_day AS day_hours
        FROM tp.data
        GROUP BY the_date, team_key, team_name, pay_period_seq, team_flag_hours_day) a,
        (
        SELECT the_date, team_key, team_name, pay_period_seq, team_flag_hours_day AS day_hours
        FROM tp.data
        GROUP BY the_date, team_key, team_name, pay_period_seq, team_flag_hours_day) b  
      WHERE a.pay_period_seq = b.pay_period_seq
        AND a.the_date BETWEEN _from_date AND current_date  
        AND a.team_key = b.team_key
        AND b.the_date <= a.the_date
      GROUP BY a.the_date, a.team_key, a.pay_period_seq, a.day_hours) x
    WHERE aa.the_date = x.the_date
      AND aa.team_key = x.team_key;  

    -- TeamFlagHourAdjustmentsDay
    UPDATE tp.Data aa
    SET Team_Flag_Hour_Adjustments_Day = x.hours
    FROM (
      SELECT the_date, team_key, SUM(Tech_Flag_Hour_Adjustments_Day) AS hours
      FROM tp.data 
      WHERE the_date BETWEEN _from_date AND current_date     
      GROUP BY the_date, team_key) x
    WHERE aa.the_date = x.the_date
      AND aa.team_key = x.team_key; 
      
    -- TeamFlagHourAdjustmentsPPTD 
    UPDATE tp.Data aa
    SET Team_Flag_Hour_Adjustments_PPTD = x.pptd
    FROM (
      SELECT a.the_date, a.team_key, a.pay_period_seq, a.day_hours, round(SUM(b.day_hours), 2) AS pptd
      FROM (
        SELECT the_date, team_key, team_name, pay_period_seq, Team_Flag_Hour_Adjustments_Day AS day_hours
        FROM tp.data
        GROUP BY the_date, team_key, team_name, pay_period_seq, Team_Flag_Hour_Adjustments_Day) a,
        (
        SELECT the_date, team_key, team_name, pay_period_seq, Team_Flag_Hour_Adjustments_Day AS day_hours
        FROM tp.data
        GROUP BY the_date, team_key, team_name, pay_period_seq, Team_Flag_Hour_Adjustments_Day) b  
      WHERE a.pay_period_seq = b.pay_period_seq
        AND a.the_date BETWEEN _from_date AND current_date 
        AND a.team_key = b.team_key
        AND b.the_date <= a.the_date
      GROUP BY a.the_date, a.team_key, a.pay_period_seq, a.day_hours) x 
    WHERE aa.the_date = x.the_date
      AND aa.team_key = x.team_key;   

    -- team prof day          
    UPDATE tp.data aa 
    SET team_prof_day = x.prof
    FROM (
      SELECT a.*,
        CASE 
          WHEN Total_Flag = 0 OR team_clock_hours_day = 0 THEN 0
          ELSE round(100 * Total_Flag/team_clock_hours_day, 2)
        END AS prof
      FROM (
        SELECT the_date, team_key, 
          (team_flag_hours_day + team_Other_Hours_Day + team_Training_Hours_Day + Team_Flag_Hour_Adjustments_Day) AS Total_Flag, 
          team_clock_hours_day
        FROM tp.data
        WHERE the_date BETWEEN _from_date AND current_Date      
        GROUP BY the_date, team_key, 
          (team_flag_hours_day + team_Other_Hours_Day + team_Training_Hours_Day + Team_Flag_Hour_Adjustments_Day), 
          team_clock_hours_day) a) x
    WHERE aa.the_date = x.the_date
      AND aa.team_key = x.team_key;      
      
    -- team prof pptd
    UPDATE tp.data aa
    SET team_prof_pptd = x.prof
    FROM (
      SELECT a.*,
        CASE 
          WHEN Total_Flag = 0 OR team_clock_hours_pptd = 0 THEN 0
          ELSE round(100 * Total_Flag/team_clock_hours_pptd, 2)
        END AS prof
      FROM (
        SELECT the_date, team_key, 
          (team_flag_hours_pptd + team_Other_Hours_PPTD + team_Training_Hours_PPTD + Team_Flag_Hour_Adjustments_PPTD) AS Total_Flag, 
          team_clock_hours_pptd
        FROM tp.data
        WHERE the_date BETWEEN _from_date AND current_Date       
        GROUP BY the_date, team_key, 
          (team_flag_hours_pptd + team_Other_Hours_PPTD + team_Training_Hours_PPTD + Team_Flag_Hour_Adjustments_PPTD), 
          team_clock_hours_pptd) a) x
    WHERE aa.the_date = x.the_date
      AND aa.team_key = x.team_key; 


                                                                 
end $$;


/*

select * from tp.data where the_date = current_date order by department_key, last_name;
*/

