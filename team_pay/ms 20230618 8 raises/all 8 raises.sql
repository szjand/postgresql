﻿I have attached the main shop flat rate spreadsheet and would like to make the changes effective 6/18/23 at the start of the new pay period if possible. 
They have gone through Laura, Ashley, Ben and Jeri already, so they are all approved. The folks impacted are listed below. 
Is this something you can adjust for me please?

Wyatt T.
Mike S.
Alex W.
Tim O.
Matt A.
Nick S.
Bryan G.
Derek G. 
-- current state, a summary for ken and andrew
-- for my use, add team key & tech key
select aa.team_name, 
	aa.team_key, 
	aa.census, aa.budget, aa.pool_percentage as "pool %", 
	cc.last_name, cc.first_name, 
	cc.tech_key, 
	cc.tech_number, cc.employee_number as emp,
  cc.tech_team_percentage as "tech %", round(aa.budget * cc.tech_team_percentage, 2) as tech_flat_rate
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 18) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date) cc on bb.tech_key = cc.tech_key 
-- order by first_name, last_names
order by aa.team_name, cc.last_name

-- team schwan
-- over budget, do not change the budget
Wyatt T.  26.50 -> 28.00
Mike S.   30.00 -> 31.50
Alex W.   24.00 -> 25.25
-- team oneil
-- within budget
Tim O.    25.50 -> 28.00
Matt A.   20.14 -> 21.50
-- team gehrtz
--  within budget
Nick S.   24.00 -> 25.50
Bryan G.  20.00 -> 22.00
Derek G.  19.00 -> 20.50

-- doing these updates on sunday, the 6/18, tp.data will update tonight, don't need to do it here
-- does not effect budget or pool
-- team budget = census * 91.46 * pool_percentage
-- tech % = tech_rate/team budget


do $$
declare 
	_eff_date date := '06/18/2023';
	_thru_date date:= '06/17/2023';
	_tech_key integer; 
	_tech_perc numeric; 
	_hourly_rate numeric; 
begin
-- team schwan budget: 106.0936
  -- wyatt
  _tech_key := (select tech_key from tp.techs where last_name = 'thompson' and first_name = 'wyatt' and thru_date > current_date);
  _hourly_rate := (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
  _tech_perc := 0.2639; -- select 28/106.0936
  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;
	insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
	values(_tech_key, _eff_date, _tech_perc, _hourly_rate);
  -- mike
  _tech_key := (select tech_key from tp.techs where last_name = 'schwan' and first_name = 'michael' and thru_date > current_date);
  _hourly_rate := (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
  _tech_perc := 0.2969; -- select 31.5/106.0936
  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;
	insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
	values(_tech_key, _eff_date, _tech_perc, _hourly_rate);  
  -- alex
  _tech_key := (select tech_key from tp.techs where last_name = 'waldbauer' and first_name = 'alex' and thru_date > current_date);
  _hourly_rate := (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
  _tech_perc := 0.238; -- select 25.25/106.0936
  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;
	insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
	values(_tech_key, _eff_date, _tech_perc, _hourly_rate);  	
	
-- team oneil	budget: 102.4352
  -- tim
  _tech_key := (select tech_key from tp.techs where last_name = 'o''neil' and first_name = 'timothy' and thru_date > current_date);
  _hourly_rate := (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
  _tech_perc := 0.27334; -- select 28/102.4352
  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;
	insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
	values(_tech_key, _eff_date, _tech_perc, _hourly_rate);  
	 -- matt
  _tech_key := (select tech_key from tp.techs where last_name = 'aamodt' and first_name = 'matthew' and thru_date > current_date);
  _hourly_rate := (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
  _tech_perc := 0.2099; -- select 21.50/102.4352
  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;
	insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
	values(_tech_key, _eff_date, _tech_perc, _hourly_rate); 
	
-- team gehrtz	budget: 109.7520
  -- nick
  _tech_key := (select tech_key from tp.techs where last_name = 'soberg' and first_name = 'nicholas' and thru_date > current_date);
  _hourly_rate := (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
  _tech_perc := 0.23234; -- select 25.5/109.7520
  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;
	insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
	values(_tech_key, _eff_date, _tech_perc, _hourly_rate); 	
  -- bryan
  _tech_key := (select tech_key from tp.techs where last_name = 'gowen' and first_name = 'bryan' and thru_date > current_date);
  _hourly_rate := (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
  _tech_perc := 0.20045; -- select 22/109.7520
  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;
	insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
	values(_tech_key, _eff_date, _tech_perc, _hourly_rate); 	
  -- derek
  _tech_key := (select tech_key from tp.techs where last_name = 'goodoien' and first_name = 'derek' and thru_date > current_date);
  _hourly_rate := (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
  _tech_perc := 0.18678; -- select 20.5/109.7520
  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;
	insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
	values(_tech_key, _eff_date, _tech_perc, _hourly_rate); 

end $$;


