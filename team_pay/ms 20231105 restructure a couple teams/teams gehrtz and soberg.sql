﻿Good afternoon!

   I have made some changes in the shop to the teams that will go into effect starting Monday the 20th.  
   Clayton Gehrtz is no longer a team leader and moves to Tim O'neil's team. 
   Nick Soberg will now be a team leader and will have Jared Johnson and Bryan Gowen on his team. 
   Thanks for your help and have a great weekend!

Gavin Flaat

select * from tp.team_leaders

-- current state, a summary for ken and andrew
-- for my use, add team key & tech key
select aa.team_name, 
	aa.team_key, 
	aa.census, aa.budget, aa.pool_percentage as "pool %", 
	cc.last_name, cc.first_name, 
	cc.tech_key, 
	cc.tech_number, cc.employee_number as emp,
  cc.tech_team_percentage as "tech %", round(aa.budget * cc.tech_team_percentage, 2) as tech_flat_rate
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 18) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date) cc on bb.tech_key = cc.tech_key 
where aa.team_key in (72,21)	
order by aa.team_name, cc.last_name


1. create a new team: Soberg
2. Populate team soberg
3. move gehrtz to team oneil
4. delete team gehrtz
5. remove gehrtz from team_leaders, add soberg

tech			flat rate
gehrtz		31.50
gowen			22
soberg		27
aamodt		21.50
kiefer		26
oneil			28

-- 1. create a new team: Soberg, team_key = 72
insert into tp.teams(department_key,team_name,from_date)
values (18, 'Soberg', '11/19/2023');


-- 2. remover gehrtz from team gehrtz & add him to team oneil & update his tech_values 
----------------------------
-- this is a change put in place by gavin and puts the team over budget, 107 vs 102.4352
----------------------------
update tp.team_techs
set thru_date = '11/18/2023'
where team_key = 65
  and tech_key = 58;

insert into tp.team_techs(team_key,tech_key,from_date)
values(21, 58, '11/19/2023');

update tp.tech_values
set thru_date = '11/18/2023'
where tech_key = 58
  and thru_date > current_Date;

-- select 31.50/102.4352

insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
values (58, '11/19/2023', 0.30751, 49.65);

-- 3. move gowen and soberg to team soberg
	-- tp.team_techs
	update tp.team_techs
	set thru_date = '11/18/2023'
	-- select * from tp.team_techs
	where tech_key in (89, 94)
	  and team_key = 65
	  and thru_date > current_date;

	insert into tp.team_techs(team_key,tech_key,from_date)
	values(72, 89, '11/19/2023');

	insert into tp.team_techs(team_key,tech_key,from_date)
	values(72, 94, '11/19/2023');	

-- 4. create team_values for team soberg
  -- making up a pool percentage
  --   budget = census * ELR * Pool % = 2 * 91.46 * .3 = 54.876
  insert into tp.team_values(team_key,census,budget,pool_percentage,from_date)
  values (72, 2, 54.876, .3, '11/19/2023');

-- 5.  tech values for soberg and gowen
  -- 89 -> 19.06
  -- 94 -> 20
  update tp.tech_values   
  set thru_date = '11/18/2023'
  -- select * from tp.tech_values
  where tech_key in (89, 94)
    and thru_date > current_date;
  -- tech_team_percentage:  89: select 27/54.876 = 0.49202
  --                        94  select 22/54.876 = 0.40090
  insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
  values(89, '11/19/2023', 0.49202, 19.06),
        (94, '11/19/2023', 0.40090, 20);
	
-- 6. close team gehrtz
  update tp.teams
  set thru_date = '11/18/2023'
  where team_key = 65
    and thru_date > current_date;
    
-- 7. remove gehrtz from team_leaders, add soberg
	delete 
	from tp.team_leaders
	where employee_number = '150126';

	insert into tp.team_leaders (team_key,department_key,team_name,employee_number)
	values (72, 18, 'Soberg', '152964');

delete
-- select * 
from tp.data
where the_date > '11/18/2023'
  and tech_key in (58,94,89,90,50,54)	