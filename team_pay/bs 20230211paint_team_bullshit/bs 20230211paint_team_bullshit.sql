﻿select aa.team_name, aa.team_key, aa.census, aa.budget, aa.pool_percentage as "pool %", cc.last_name, cc.first_name, cc.tech_number, cc.employee_number as emp, cc.tech_key,
  cc.tech_team_percentage as "tech %", round(aa.budget * cc.tech_team_percentage, 2) as tech_flat_rate
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 13) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date) cc on bb.tech_key = cc.tech_key
where aa.team_name = 'paint team'
order by aa.team_name, cc.last_name


select the_date, last_name, tech_flag_hours_pptd, team_flag_hours_pptd
from tp.data 
where team_name = 'paint team'
-- where last_name in ('adam','jacobson','walden')
  and the_date in ('02/11/2023', '01/28/2023', '01/14/2023', '12/31/2022', '12/17/2022')
order by the_date desc, last_name  


select sum(flag_hours)  -- 249.2
from dds.fact_repair_order a
join dds.dim_tech b on a.tech_key = b.tech_key
  and b.tech_number = '311'
where a.close_date between '01/29/2023' and  '02/11/2023'


select sum(flag_hours)  -- 101.2
from dds.fact_repair_order a
join dds.dim_tech b on a.tech_key = b.tech_key
  and b.tech_number = '311'
where a.close_date between '01/15/2023' and  '01/28/2023'

select sum(clock_hours) -- 88.12
from ukg.clock_hours  
where employee_number = '1122352'
  and the_date between '01/29/2023' and  '02/11/2023'

select sum(clock_hours)  -- 69.51
from ukg.clock_hours  
where employee_number = '1122352'
  and the_date between '01/15/2023' and  '01/28/2023' 

ukg.get_body_shop_flat_rate_payroll($1)  
tp.get_body_shop_flat_rate_managers_payroll_approval_data(_pay_period_ind)

 		select pay_period_select_format,  team, (last_name || ', ' || first_name)::citext as tech, employee_number,
			flat_rate,team_prof, round(flag_hours, 2) as flag_hours,
			round(clock_hours, 4) as clock_hours, round(comm_hours, 4) as comm_hours, 
			round(comm_pay, 2) as comm_ppay
		from tp.body_shop_flat_rate_payroll_data
		where pay_period_seq = 360
		order by team, last_name; 

tp.body_shop_flat_rate_payroll_data		
tp.update_body_shop_flat_rate_payroll_data()


do $$
declare 
	_pay_period_seq integer := (
	  select distinct biweekly_pay_period_sequence
	  from dds.dim_date
	  where the_date = '02/11/2023'); -- current_date - 5);
	_from_date date := (
	  select distinct biweekly_pay_period_start_date
	  from dds.dim_date 
	  where biweekly_pay_period_sequence = _pay_period_seq);
	_thru_date date := (
	  select distinct biweekly_pay_period_end_date
	  from dds.dim_date 
	  where biweekly_pay_period_sequence = _pay_period_seq);
begin 		
--   delete 
--   from  tp.body_shop_flat_rate_payroll_data
--   where pay_period_seq = _pay_period_seq;

--   insert into tp.body_shop_flat_rate_payroll_data
drop table if exists wtf;
create temp table wtf as
	select a.pay_period_start, a.pay_period_end, a.pay_period_seq, a.pay_period_select_format,
		a.pay_period_start + 6 as first_saturday,
		c.team_name AS team, a.last_name, a.first_name, a.employee_number, 
		d.hire_date, d.term_date,      
		a.tech_hourly_rate AS pto_rate, round(a.tech_tfr_rate, 4) AS flat_rate,
		a.Tech_Flag_Hours_PPTD as flag_hours, a.team_prof_pptd AS team_prof,
		coalesce(a.tech_clock_hours_pptd, 0) AS clock_hours,   
		coalesce(a.tech_clock_hours_pptd, 0) * a.Team_Prof_PPTD/100 as comm_hours, 
		a.tech_tfr_rate * a.Team_Prof_PPTD * coalesce(a.tech_clock_hours_pptd, 0)/100 AS comm_pay
	FROM tp.data a
	INNER JOIN tp.team_techs b on a.tech_key = b.tech_key
		AND a.team_key = b.team_key
		AND b.thru_Date >= _thru_date ------------------------------------------------
	LEFT JOIN tp.Teams c on b.team_Key = c.team_Key
	left join ukg.employees d on a.employee_number = d.employee_number
-- *a*	
	WHERE the_date = '02/11/2023' -- current_date - 1
		AND a.department_Key = 13
		and c.team_name = 'paint team';
end $$;

select * from wtf;		


---------------------------------------------------------------------------------------------------------
2/12 - 2/25

fucking again, left out sauls

do $$
declare 
	_pay_period_seq integer := (
	  select distinct biweekly_pay_period_sequence
	  from dds.dim_date
	  where the_date = '02/25/2023'); -- current_date - 5);
	_from_date date := (
	  select distinct biweekly_pay_period_start_date
	  from dds.dim_date 
	  where biweekly_pay_period_sequence = _pay_period_seq);
	_thru_date date := (
	  select distinct biweekly_pay_period_end_date
	  from dds.dim_date 
	  where biweekly_pay_period_sequence = _pay_period_seq);
begin 		
--   delete 
--   from  tp.body_shop_flat_rate_payroll_data
--   where pay_period_seq = _pay_period_seq;

--   insert into tp.body_shop_flat_rate_payroll_data
drop table if exists wtf;
create temp table wtf as
	select a.pay_period_start, a.pay_period_end, a.pay_period_seq, a.pay_period_select_format,
		a.pay_period_start + 6 as first_saturday,
		c.team_name AS team, a.last_name, a.first_name, a.employee_number, 
		d.hire_date, d.term_date,      
		a.tech_hourly_rate AS pto_rate, round(a.tech_tfr_rate, 4) AS flat_rate,
		a.Tech_Flag_Hours_PPTD as flag_hours, 229.26 /*a.team_prof_pptd*/ AS team_prof,
		coalesce(a.tech_clock_hours_pptd, 0) AS clock_hours,   
		coalesce(a.tech_clock_hours_pptd, 0) * 229.26/*a.Team_Prof_PPTD*//100 as comm_hours, 
		a.tech_tfr_rate * 229.26 /*a.Team_Prof_PPTD*/ * coalesce(a.tech_clock_hours_pptd, 0)/100 AS comm_pay
	FROM tp.data a
	INNER JOIN tp.team_techs b on a.tech_key = b.tech_key
		AND a.team_key = b.team_key
		AND b.thru_Date >= _thru_date ------------------------------------------------
	LEFT JOIN tp.Teams c on b.team_Key = c.team_Key
	left join ukg.employees d on a.employee_number = d.employee_number
-- *a*	
	WHERE the_date = '02/25/2023' -- current_date - 1
		AND a.department_Key = 13
		and c.team_name = 'paint team';
end $$;

select * from wtf;		





select * from ukg.employees where last_name = 'sauls'
1122352

select sum(clock_hours)  -- 97.86
from ukg.clock_hours
where employee_number = '1122352'
  and the_date between  '02/12/2023' and '02/25/2023' 

select sum(flag_hours)  -- 17.8
from dds.fact_repair_order a
join dds.dim_tech b on a.tech_key = b.tech_key
  and b.tech_number = '311'
where a.close_date between '02/12/2023' and '02/25/2023'  

select * 
from tp.data
where the_date = '02/25/2023'
  and team_name = 'paint team'

           flag     clock
adam        .2        96.18
jacobson   473        90.74
walden     332.5      74.42
           805.7     261.34  308.30%
sauls       17.8      97.86
           823.5     359.2   229.26 %

pay based on incorrect data
adam       4,818.62
jacobson   7,004.92
walden     4,542.85

pay based on data including Sauls
adam       3,378.18
jacobson   5,209.94
walden     3,583.25

Adjustments
adam        -1440.44
jacobson    -1701.92
walden      - 959.60