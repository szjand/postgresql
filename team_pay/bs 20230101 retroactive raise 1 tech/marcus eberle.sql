﻿/*
Jon, 
 
Still leaning the process of how all this works. I submitted a pay raise for Marcus Eberle on 11/6 
and did not know that I had to also let you or Afton know. 
In either case his current flat rate is $19.50 and I would like to get him to $21.00. 
 
If you have any questions please let me know. 
 
Thank you, 
Tim


select aa.team_name, aa.census, aa.budget, aa.pool_percentage as "pool %", cc.last_name, cc.first_name, cc.tech_number, cc.employee_number as emp,
  cc.tech_team_percentage as "tech %", round(aa.budget * cc.tech_team_percentage, 2) as tech_flat_rate, aa.team_key, cc.tech_key
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 13) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date) cc on bb.tech_key = cc.tech_key 
where cc.last_name = 'eberle'	
order by aa.team_name, cc.last_name


*/
select pay_period_start, pay_period_end, team_key, tech_key, team_clock_hours_pptd, team_flag_hours_pptd, team_prof_pptd,
  round(19.5 * team_clock_hours_pptd * team_prof_pptd/100, 2) as paid,
  round(21 * team_clock_hours_pptd * team_prof_pptd/100, 2) as due,
  round(21 * team_clock_hours_pptd * team_prof_pptd/100, 2) - round(19.5 * team_clock_hours_pptd * team_prof_pptd/100, 2) as back_pay
-- select * 
from tp.data
where last_name = 'eberle'
  and the_date in ('11/19/2022','12/03/22','12/17/2022','12/31/2022')
order by the_date  

select pay_period_start, pay_period_end, 
  round(19.5 * team_clock_hours_pptd * team_prof_pptd/100, 2) as paid,
  round(21 * team_clock_hours_pptd * team_prof_pptd/100, 2) as due,
  round(21 * team_clock_hours_pptd * team_prof_pptd/100, 2) - round(19.5 * team_clock_hours_pptd * team_prof_pptd/100, 2) as back_pay
-- select * 
from tp.data
where last_name = 'eberle'
  and the_date in ('11/19/2022','12/03/22','12/17/2022','12/31/2022')
order by the_date 


-- select * from tp.team_values where team_key = 45

update tp.team_values
set thru_date = '12/31/2022'
where team_key = 45
  and thru_date > current_date;

insert into tp.team_values(team_key,census,budget,pool_percentage,from_date)
values(45,1,21.5,1,'01/01/2023');  

-- -- no need to change tech_values, tech_team_percentage remains at 1
-- select * from tp.tech_values where tech_key = 78
-- 
-- update tp.tech_values
-- set thru_date = '12/31/2022'
-- where tech_key = 78
--   and thru_date > current_date;

