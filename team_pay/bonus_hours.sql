﻿/*
Bonus, after mindfucking myself INTO a frenzy, ben clarified it 
total hours (pto + vac + clock) > 90

clock hours = 80
pto hours = 20
comm pay = 70 * commRate * teamProf
bonus pay = 10 * bonusRate * teamProf
pto pay = 20 * ptoRate

clock hours = 100
pto hours = 16
comm pay = 74 * commRate * teamProf
bonus pay = 26 * bonusRate * teamProf
pto pay = 16 * ptoRate

summary: 
IF total hours > 90
bonus hours = total hours - 90
commission hours = clock hours - bonus hours  
ELSE commision hours = clock hours

eg
  CASE WHEN e.totalHours > 90 THEN e.clockhours - (totalhours - 90) else e.clockHours end AS commHours,
  round(CASE WHEN totalHours > 90 THEN totalHours - 90 ELSE 0 END, 2) AS bonusHours   

!!! this is actually misleading, comm_hours is clock_hours * prof
  but that gets confusing, clock hours is just flat clock hours, comm_hours now becomes the result
  of figuring bonus hours
  so when there are no bonus hours, comm_hours = clock_hours
*/


gm
total_hours: tech_clock_hours_pptd + tech_vacation_hours_pptd + tech_pto_hours_pptd + tech_holiday_hours_pptd
comm_hours :
			case
				when cc.total_hours > 90 and cc.team_prof >= 100 then cc.clock_hours - (total_hours -90)
				else cc.clock_hours
			end as comm_hours
bonus hours:
			case
				when cc.total_hours > 90 and team_prof >= 100 then total_hours - 90
				else 0
		  end as bonus_hours			

honda 338		
brandon
	clock: 81.47
	pto: 8
	holiday: 8
	total: 89.47 should be 97.47
which should have generated bonus hours but did not because total hours is wrong

so
comm_hours = 81.47 - (97.47 - 90) = 74
comm_pay = 74hrs * 28 * 2.04 = 4226.88
bonus_hours = 7.47
bonus_pay = 7.47 * 56 = 418.32
total_pay = 4645.20


  example: (includes pto, holiday & bonus, oneil 1106421, pp_seq = 338)
	select * from tp.gm_main_shop_flat_rate_payroll_data where pay_period_seq = 338 and employee_number = '1106421'
	clock  88.60
	pto     4.00
	hol     8.00
	total 100.60
	comm hours = 88.6 - (100.6 - 90) = 78 (when there is no bonus, clock_hours = comm_hours)
	bonus hours = 100.6 - 90 = 10.6
	comm_pay = comm_hours * flat_rate * prof = 78 * 23.50 * 1.3692 = 2509.74
	bonus pay = bonus_hours * bonus_rate * prof = 10.6 * 47 * 1.3692 = 682.14
	total for tlm = comm_pay + bonus_pay = 3191.88   