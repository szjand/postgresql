﻿-- 09/14/2023
-- Jon, 
-- Ben Johnson was given a raise. He went from $16/Flat to $18/Flat. Please back date to the start of current pay period. 
-- 
-- Thank you, 
-- Tim 

select aa.team_name, 
	aa.team_key, 
	aa.census, aa.budget, aa.pool_percentage as "pool %", 
	cc.last_name, cc.first_name, 
	cc.tech_key, 
	cc.tech_number, cc.employee_number as emp,
  cc.tech_team_percentage as "tech %", round(aa.budget * cc.tech_team_percentage, 2) as tech_flat_rate
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 13) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date) cc on bb.tech_key = cc.tech_key 
where aa.team_name = 'team 21'	
order by aa.team_name, cc.last_name

-- select * from tp.team_values where team_key = 57 order by from_Date

update tp.team_values
set thru_date = '09/09/2023'
where team_key = 57
  and thru_date > current_date;

-- no need to change tech_values, tech_team_percentage remains at 1  

insert into tp.team_values(team_key,census,budget,pool_percentage,from_date)
values(57,1,18,1,'09/10/2023');  


-- the next day, 9/15, vision still showed 16 rate in ukg import
-- 
select * from tp.data where tech_key = 96 and the_date between '09/10/2023' and current_date
-- tp.data was not updated for previous days in the pay period
update  tp.data
set tech_tfr_rate = 18
where tech_key = 96 and the_date between '09/10/2023' and current_date

select * from  ukg.get_body_shop_flat_rate_payroll(0)

	select a.pay_period_start, a.pay_period_end, a.pay_period_seq, a.pay_period_select_format,
		a.pay_period_start + 6 as first_saturday,
		c.team_name AS team, a.last_name, a.first_name, a.employee_number, 
		d.hire_date, d.term_date,      
		a.tech_hourly_rate AS pto_rate, round(a.tech_tfr_rate, 4) AS flat_rate,
		a.Tech_Flag_Hours_PPTD as flag_hours, 
		coalesce(a.team_prof_pptd, 0) AS team_prof,
		coalesce(a.tech_clock_hours_pptd, 0) AS clock_hours,   
		coalesce(a.tech_clock_hours_pptd, 0) * coalesce(a.Team_Prof_PPTD, 0)/100 as comm_hours, 
		a.tech_tfr_rate * coalesce(a.Team_Prof_PPTD, 0) * coalesce(a.tech_clock_hours_pptd, 0)/100 AS comm_pay
	FROM tp.data a
	INNER JOIN tp.team_techs b on a.tech_key = b.tech_key
		AND a.team_key = b.team_key
		AND b.thru_Date >= '09/23/2023' ------------------------------------------------
	LEFT JOIN tp.Teams c on b.team_Key = c.team_Key
	left join ukg.employees d on a.employee_number = d.employee_number
-- *a*	
	WHERE the_date = current_date - 1
		AND a.department_Key = 13;

-- ran this and now ukg import is correct
select tp.update_body_shop_flat_rate_payroll_data()		