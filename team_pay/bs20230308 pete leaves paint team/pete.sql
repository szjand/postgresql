﻿/*
Jon, 

Have to bother you one more time about Pete. He was switched from commission to salary.  
It took a while for everything to get approved and his UKG to be updated. 
But it was submitted for his salary to start for the current pay period. 

If you have any questions please let me know. 

Thank you, 
Tim



select aa.team_name, 
	aa.team_key, 
	aa.census, aa.budget, aa.pool_percentage as "pool %", 
	cc.last_name, cc.first_name, 
	cc.tech_key, 
	cc.tech_number, cc.employee_number as emp,
  cc.tech_team_percentage as "tech %", round(aa.budget * cc.tech_team_percentage, 2) as tech_flat_rate
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 13) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (                                                                                                                                                                                                            
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date) cc on bb.tech_key = cc.tech_key 
where aa.team_name = 'paint team'	
order by aa.team_name, cc.last_name

*/


select the_date, day_name, last_name, tech_clock_hours_pptd, tech_flag_hours_pptd, team_clock_hours_pptd, team_flag_hours_pptd, team_prof_pptd
-- select * 
from tp.data
where the_date > '02/25/2023'
  and team_name = 'paint team'
  and the_date = '03/07/2023'
order by the_date, last_name 
 
values ((1.4 + 23.3 + 119.7 + 157.6)/(68.41 + 61.8 + 68.04 + 58.96))  = 1.17

values ((1.4 + 119.7 + 157.6)/(68.41 + 68.04 + 58.96))  = 1.42

select * from tp.tech_values where tech_key = 72

update tp.tech_values
set from_date = '02/26/2023'
-- select * from tp.tech_values
where tech_key = 72
  and thru_date = '12/31/9999'

-- so, remove pete from the paint team
-- i think this is good enough
select * from tp.team_techs where team_key = 29 and tech_key = 43
update tp.team_techs
set thru_date = '02/25/2023'
where team_key = 29 
  and tech_key = 43
  and thru_date > current_date;


  