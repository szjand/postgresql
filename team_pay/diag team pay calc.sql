﻿-- select * from dds.dim_date where the_date = '10/21/2023'
-- FUNCTION tp.get_gm_flat_rate_managers_payroll_approval_data(integer)
		select pay_period_select_format,  team, (last_name || ', ' || first_name)::citext as tech, 
			employee_number, flat_rate,bonus_rate,team_prof,
-- 			flag_hours,round(adjustments,2) as adjustments, 
			round(total_flag_hours,2) as flag_hours,round(clock_hours,4) as clock_hours, 
			pto_hours, hol_hours, total_hours,
			round(comm_hours, 4), comm_pay, bonus_hours, bonus_pay, round(guarantee,2),
			round(
					case
					when guarantee is null then comm_pay + bonus_pay
					when guarantee > comm_pay + bonus_pay + pto_pay then guarantee - pto_pay
					else comm_pay + bonus_pay
				end, 2) as total, tool_allow 
		from tp.gm_main_shop_flat_rate_payroll_data
		where pay_period_seq = 387 -- _pay_period_seq
		  and employee_number in ('164015','152410','1106399')
		order by team, last_name; 

select * from ukg.employees where employee_number = '145801'		

		case
			when coalesce(guarantee, 0) > round(comm_hours * team_prof/100 * flat_rate, 2) 
				+ round(bonus_hours * team_prof/100 * bonus_rate, 2)
				+ round(pto_rate * (pto_hours + hol_hours), 2) then coalesce(guarantee, 0) + 69.42 - round(pto_rate * (pto_hours + hol_hours), 2)
			else round(comm_hours * team_prof/100 * flat_rate, 2) 
				+ round(bonus_hours * team_prof/100 * bonus_rate, 2) + 69.42 
		end as total_pay

select * from tp.gm_main_shop_flat_rate_payroll_data
where employee_number in ('164015','152410','1106399')	
and pay_period_seq = 387


select coalesce(guarantee, 0),  round(comm_hours * team_prof/100 * flat_rate, 2) + round(bonus_hours * team_prof/100 * bonus_rate, 2) + round(pto_rate * (pto_hours + hol_hours), 2),
  coalesce(guarantee, 0) + 69.42 - round(pto_rate * (pto_hours + hol_hours), 2)
from tp.gm_main_shop_flat_rate_payroll_data
where employee_number in ('152410')	
and pay_period_seq = 387

select coalesce(guarantee, 0),  round(comm_hours * team_prof/100 * flat_rate, 2) + round(bonus_hours * team_prof/100 * bonus_rate, 2) + round(pto_rate * (pto_hours + hol_hours), 2),
  coalesce(guarantee, 0) + 69.42 - round(pto_rate * (pto_hours + hol_hours), 2)
from tp.gm_main_shop_flat_rate_payroll_data
where employee_number in ('164015')	
and pay_period_seq = 387