﻿/*
Jon, 

So basically, all of last year Thomas was "in training" for the paint team. He was selling hours, technically, but his clock was never counted against the paint team. A few weeks back is when this all came to light. The paint team was getting paid as it was just a 3-person operation but really there were 4. For 2022 their proficiency was way high because of that. Now for 2023, adding Thomas to the paint team their proficiency took an obvious dive. Which adjusted their pay. In order to counterbalance the addition of Thomas I proposed a pay raise for Pat and Chris. 

Ben came to my office and brought up that the raise I gave Pat and Chris was too much based off of what they made last year. 

In either case what I am specifically asking for is essentially a breakdown of Pat, Chris, Thomas, and Pete for last year of what they would have made for 2022 if it was a four-person paint team, as it should have been. 

Paint team overall proficiency for 2022 with 3 people (Chris, Pete, Pat)
Paint team overall proficiency for 2022 with 4 people (Chris Pete, Pat, adding Thomas) 

What salary did Chris, Pete, Pat pull for 2022 with the 3-person team proficiency
What salary would have Chris, Pete, and Pat pulled if Thomas was part of the team (Including his clock/flag)

This is just to determine what their true salary would have been if Thomas was added like he should had been to adjust their salarys for this year.   

The second part is I know I kept Thomas too high when switching him to flat rate at $20. I should have adjusted his pay based off his skill set when moving him to flat rate. He should be $17. There is no way he should be making almost as much as Chris. 

As this process has unfolded, I have become more and more familiar with Vision and how to use it. 

Thank you, 
Tim
*/

select min(the_date) from tp.data

select * 
from tp.data
where last_name = 'sauls'
  and the_date < '01/01/2023'


select last_name, first_name, 
	round(sum(tech_clock_hours_pptd), 1) as tech_clock_hours , 
	round(sum(tech_flag_hours_pptd), 1) as tech_flag_hours ,
  round(sum(team_clock_hours_pptd), 1) as team_clock_hours, 
  round(sum(team_flag_hours_pptd), 1) as team_flag_hours,
  round(sum(team_flag_hours_pptd)/sum(team_clock_hours_pptd), 3) as team_prof
from tp.data
where team_name = 'paint team'
  and the_date between '01/02/2022' and '12/31/2022'  
  and the_date = pay_period_end
group by last_name, first_name


select distinct pay_period_start, pay_period_end  -- 26 pay periods
from tp.data
where team_name = 'paint team'
  and the_date between '01/02/2022' and '12/31/2022'  
  and the_date = pay_period_end

  
select  * 
from ukg.employees
where last_name = 'sauls'

select 
  sum(clock_hours) filter (where the_date between '01/02/2022' and '12/31/2022' and employee_number = '1122352')  as pp_1
from ukg.clock_hours 
where employee_number = '1122352'

select * from ukg.employees where last_name in ('sauls','walden','adam','jacobson')

select * from ukg.pay_Statement_earnings where employee_Account_id in (12987528823,12987528823,12987528823,12987528823)

select * from ukg.pay_Statement_earnings limit 10

select c.last_name, c.first_name, sum(b.ee_amount) -- a.payroll_id, a.payroll_start_date, a.payroll_end_date,
--   b.*
from ukg.payrolls a
join ukg.pay_Statement_earnings b on a.payroll_id = b.payroll_id
  and  employee_Account_id in (12987528823,12987528884,12987528674,12987528134)
join ukg.employees c on b.employee_account_id = c.primary_account_id
where
  case 
    when b.employee_Account_id in (12987528884,12987528674,12987528134) then earning_name = 'flat rate compensation'
    when b.employee_account_id = 12987528823 then earning_code in ('Average Overtime (0.5)','Overtime Straight','Regular')
  end
group by c.last_name, c.first_name


/*
select aa.team_name, 
	aa.team_key, 
	aa.census, aa.budget, aa.pool_percentage as "pool %", 
	cc.last_name, cc.first_name, 
	cc.tech_key, 
	cc.tech_number, cc.employee_number as emp,
  cc.tech_team_percentage as "tech %", round(aa.budget * cc.tech_team_percentage, 2) as tech_flat_rate
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 13) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date) cc on bb.tech_key = cc.tech_key 
where aa.team_name = 'paint team'	
order by aa.team_name, cc.last_name
*/
and change Sauls flat rate
select * from tp.techs where last_name = 'sauls'
select 15.50/90.0

update tp.tech_values
set tech_team_percentage = .17223
-- select * from tp.tech_values
where tech_key = 120

of course, thats not teh end off the story

From Tim
I definitely overshot their raises. based off the information you gave me. 

One final adjustment

Pat - $17.25
Chris - $20.8

Hopefully this will settle everything. 

select * from tp.techs where last_name = 'adam'
values (17.25/90)

update tp.tech_values
set tech_team_percentage = .19167
-- select * from tp.tech_values
where tech_key = 72
  and thru_date > current_date

select * from tp.techs where last_name = 'walden'
values (20.8/90)

update tp.tech_values
set tech_team_percentage = .23112
-- select * from tp.tech_values
where tech_key = 31
  and thru_date > current_date