﻿/*
3/9/23
Jon, 

Just submitted a compli form for Artem to switch him from hourly to commission starting on 3/12 @ $27. 

If you have any questions, please let me know. 

Thank you, 
Tim

3/29/23
Jon, 

Artem was not on the vision page for flat rate this pay period. Any way I can get him added for next pay period and a calculation for what he should have gotten paid for this past pay period? 

Thank you, 
Tim
*/


select * 
from dds.dim_tech
where employee_number = '145999'


select sum(a.flag_hours)  -- 173.4
from dds.fact_repair_order a
join dds.dim_tech c on a.tech_key = c.tech_key
  and c.tech_number = '673'
where a.close_date between '03/12/2023' and '03/25/2023'  

select sum(a.flag_hours)  -- 173.4
from dds.fact_repair_order a
join dds.dim_tech c on a.tech_key = c.tech_key
  and c.tech_number = '673'
where a.close_date between '02/26/2023' and '03/11/2023'

select sum(clock_hours) -- 75.17
from ukg.clock_hours a
where a.employee_number = '145999'
  and the_date between '03/12/2023' and '03/25/2023'  

select 173.4 * 27  4681.80

select 4681.80 - 2468.15  2213.65

select * from tp.data where team_name = 'team 12' and the_date = '03/25/2023'  

-----------------------------------------------------------------------------------
select aa.team_name, aa.census, aa.budget, aa.pool_percentage as "pool %", cc.last_name, cc.first_name, cc.tech_number, cc.employee_number as emp,
  cc.tech_team_percentage as "tech %", round(aa.budget * cc.tech_team_percentage, 2) as tech_flat_rate, aa.team_key, cc.tech_key
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 13) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date) cc on bb.tech_key = cc.tech_key 
-- where aa.team_name = 'paint team'	
order by aa.team_name, cc.last_name


145999    673    Bazhyna, Artem

-- select * from tp.teams
insert into tp.teams(department_key, team_name, from_date)
values (13, 'Team Bazhyna', '03/26/2023');

-- select * from tp.techs where tech_number = '673'
insert into tp.techs (department_key, tech_number, first_name, last_name, employee_number, from_date)
values(13, '673', 'Artem', 'Bazhyna', '145999', '03/26/2023');

insert into tp.team_techs(team_key, tech_key, from_date)
values(56, 122, '03/26/2023');

insert into tp.team_values(team_key,census,budget,pool_percentage,from_date)
values(56, 1, 27, 1, '03/26/2023');

insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
values (122, '03/26/2023', 1, 27);