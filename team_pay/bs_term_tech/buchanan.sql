﻿-- -	Kyle Buchanan – Metal Technician – 1/13/2023
-- chad gardner, 8/28/23
select aa.team_name, aa.team_key, aa.census, aa.budget, aa.pool_percentage as "pool %", cc.last_name, cc.first_name, cc.tech_number, cc.employee_number as emp, cc.tech_key,
  cc.tech_team_percentage as "tech %", round(aa.budget * cc.tech_team_percentage, 2) as tech_flat_rate
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 13) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date --) cc on bb.tech_key = cc.tech_key
	  and last_name = 'gardner') cc on bb.tech_key = cc.tech_key -------------------------------------------------
order by aa.team_name, cc.last_name



do $$
declare
  _tech_key integer := (select tech_key from tp.techs where last_name = 'Gardner' and first_name = 'Chad');
  _team_key integer := (select team_key from tp.teams where team_name = 'Team 3');
  _thru_date date := '08/28/2023';
begin
  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;

  update tp.team_values
  set thru_date = _thru_date
  where team_key = _team_key
    and thru_date > current_date;

  update tp.team_techs
  set thru_date = _thru_date
  where team_key = _team_key
    and tech_key = _tech_key
    and thru_date > current_date;

  update tp.techs
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;

  update tp.teams
  set thru_date = _thru_date
  where team_key = _team_key
    and thru_date > current_date;

  delete 
--   select *
  from tp.data
  where the_date > '08/28/2023'  -- may or may not want to hard code this date
    and tech_key = 30;
end $$;


