﻿-- gavin has taken over, knows nothing, i asked for the spreadsheet, no response, the raise numbers come from laura
-- Hey Afton-
-- Sorry for the late changes but thank you for working with us!
-- Dennis McVeigh - $31.50 > $33.00
-- Brody Bailly - $19.00 > $21.00
-- Skyler Bruns - $19.00 > $21.00
-- Nicholas Soberg - $25.50 > $27.00 
-- Gordon David - $22.00 > $24.00 
-- Joshua Northagen - $21.40 > $23.40 
-- Kodey Schuppert - $22.00 > $23.00
select aa.team_name, 
	aa.team_key, 
	aa.census, aa.budget, aa.pool_percentage as "pool %", 
	cc.last_name, cc.first_name, 
	cc.tech_key, 
	cc.tech_number, cc.employee_number as emp,
  cc.tech_team_percentage as "tech %", round(aa.budget * cc.tech_team_percentage, 2) as tech_flat_rate
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 18) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date) cc on bb.tech_key = cc.tech_key 
-- order by first_name, last_names
order by aa.team_name, cc.last_name

mcveigh  Dennis McVeigh - $31.50 > $33.00  
mcveigh  Gordon David - $22.00 > $24.00
mcveigh  Kodey Schuppert - $22.00 > $23.00

alignment  Brody Bailly - $19.00 > $21.00
alignment  Joshua Northagen - $21.40 > $23.40
alignment  Skyler Bruns - $19.00 > $21.00

gehrtz  Nicholas Soberg - $25.50 > $27.00

/* 
08/22/23 figure out the adjustment required for pay period 7/30 -> 8/12

*/
 -- see the adjustments spreadsheet 
 
select team_name, last_name, first_name, employee_number, tech_tfr_rate, tech_clock_hours_pptd, tech_pto_hours_pptd, team_prof_pptd 
-- select *
from tp.data 
where the_date = '08/12/2023' 
  and last_name in ('McVeigh','David','Schuppert','Bailly','Northagen','Bruns','Soberg')
order by team_name, last_name  


select * from tp.gm_main_shop_flat_rate_payroll_data where pay_period_seq = 382 and employee_number = '194675' --'166873'



do $$
declare 
	_eff_date date := '08/13/2023';
	_thru_date date:= '08/12/2023';
	_tech_key integer; 
	_tech_perc numeric; 
	_hourly_rate numeric; 
begin
-- team mcveigh budget 109.7520
	-- dennis
  _tech_key := (select tech_key from tp.techs where last_name = 'mcveigh' and first_name = 'dennis' and thru_date > current_date);
  _hourly_rate := (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
  _tech_perc := 0.30068; -- select 33/109.7520
  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;
	insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
	values(_tech_key, _eff_date, _tech_perc, _hourly_rate);	
	-- gordon
  _tech_key := (select tech_key from tp.techs where last_name = 'david' and first_name = 'gordon' and thru_date > current_date);
  _hourly_rate := (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
  _tech_perc := 0.218675; -- select 24/109.7520
  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;
	insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
	values(_tech_key, _eff_date, _tech_perc, _hourly_rate);	
	-- kodey
  _tech_key := (select tech_key from tp.techs where last_name = 'schuppert' and first_name = 'kodey' and thru_date > current_date);
  _hourly_rate := (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
  _tech_perc := 0.209563; -- select 23/109.7520
  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;
	insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
	values(_tech_key, _eff_date, _tech_perc, _hourly_rate);			

-- team alignment	budget 65.8512
  -- brody
  _tech_key := (select tech_key from tp.techs where last_name = 'bailly' and first_name = 'brody' and thru_date > current_date);
  _hourly_rate := (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
  _tech_perc := 0.318901; -- select 21/65.8512
  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;
	insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
	values(_tech_key, _eff_date, _tech_perc, _hourly_rate);	 
  -- joshua
  _tech_key := (select tech_key from tp.techs where last_name = 'northagen' and first_name = 'joshua' and thru_date > current_date);
  _hourly_rate := (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
  _tech_perc := 0.355347; -- select 23.4/65.8512
  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;
	insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
	values(_tech_key, _eff_date, _tech_perc, _hourly_rate);	 	
  -- skyler
  _tech_key := (select tech_key from tp.techs where last_name = 'bruns' and first_name = 'skyler' and thru_date > current_date);
  _hourly_rate := (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
  _tech_perc := 0.318901; -- select 21/65.8512
  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;
	insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
	values(_tech_key, _eff_date, _tech_perc, _hourly_rate);		

-- team gehrtz	budget 109.7520
  -- nicholas
  _tech_key := (select tech_key from tp.techs where last_name = 'soberg' and first_name = 'nicholas' and thru_date > current_date);
  _hourly_rate := (select hourly_rate from tp.tech_values where tech_key = _tech_key and thru_date > current_date);
  _tech_perc := 0.24601; -- select 27/109.752
  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;
	insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
	values(_tech_key, _eff_date, _tech_perc, _hourly_rate);	  

end $$;	