﻿
Jon, 

I would like to move Thomas to flat rate starting 2/26. He will stay at his current rate of $20. 

I submitted it in compli as well. 

Thank you, 
Tim


select aa.team_name, aa.census, aa.budget, aa.pool_percentage as "pool %", cc.last_name, cc.first_name, cc.tech_number, cc.employee_number as emp,
  cc.tech_team_percentage as "tech %", round(aa.budget * cc.tech_team_percentage, 2) as tech_flat_rate, aa.team_key, cc.tech_key
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 13) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date) cc on bb.tech_key = cc.tech_key 
where aa.team_name = 'paint team'	
order by aa.team_name, cc.last_name

select * from tp.data
where the_date = '02/25/2023'
  and team_name = 'paint team'

select * 
from tp.techs  
where last_name = 'sauls'

select * from tp.shop_values
select * from tp.team_values where team_key = 29

select 60 * 4 * .375  budget = 90

select 90 * .18056

select * from dds.dim_tech where tech_name like 'saul%'  
311
1122352

insert into tp.techs (department_key, tech_number, first_name, last_name, employee_number, from_date)
values(13, '311','Thomas', 'Sauls', '1122352', '02/26/2023');

insert into tp.team_techs(team_key, tech_key, from_date)
values(29, 120, '02/26/2023');

-- select 20.0/90.0
insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
values (120, '02/26/2023', 0.22222, 20);

