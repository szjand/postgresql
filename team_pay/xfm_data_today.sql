﻿do $$
begin
-- techs
  -- ClockHoursDay    
    UPDATE tp.data aa
    SET Tech_Clock_hours_Day = x.clock_hours
    FROM (    
      SELECT a.the_date, c.pymast_employee_number, SUM(a.clock_hours) AS clock_hours
      FROM arkona.xfm_pypclockin_today a  -- no clock hours fact
      LEFT JOIN arkona.ext_pymast c on a.employee_number = c.pymast_employee_number -- no employee dim
      where a.the_date = current_date
      GROUP BY a.the_date, c.pymast_employee_number) x 
    WHERE aa.the_date = x.the_date
      AND aa.employee_number = x.pymast_employee_number; 

  -- ClockHoursPPTD
    UPDATE tp.Data aa
    SET Tech_Clock_Hours_PPTD = x.PPTD
    FROM (
      SELECT a.the_date, a.tech_key, a.pay_period_seq, a.tech_clock_hours_day, SUM(b.tech_clock_hours_day) AS pptd
      FROM tp.Data a, tp.data b
      WHERE a.pay_period_seq = b.pay_period_seq
        AND a.tech_key = b.tech_key
        AND b.the_date <= a.the_date
        and a.pay_period_seq = (
          select biweekly_pay_period_sequence
          from dds.dim_date
          where the_date = current_Date)
      GROUP BY a.the_date, a.tech_key, a.pay_period_seq, a.tech_clock_hours_day) x
    WHERE aa.the_date = x.the_date
      AND aa.tech_key = x.tech_key;   

-- teams
    -- teamclockhoursDay
    UPDATE tp.Data aa
    SET team_clock_hours_day = x.team_clock_hours_day
    FROM (
      SELECT the_date, team_key, SUM(tech_clock_hours_day) AS team_clock_hours_day
      FROM tp.data  
      WHERE pay_period_seq = (
        select biweekly_pay_period_sequence
        from dds.dim_date
        where the_date = current_Date)
      GROUP BY the_date, team_key) x 
    WHERE aa.the_date = x.the_date
      AND aa.team_key = x.team_key;  

    -- teamclockhours pptd
    UPDATE tp.Data aa
    SET team_clock_hours_pptd = x.pptd
    FROM (
      SELECT a.the_date, a.team_key, a.pay_period_seq, a.day_hours, round(SUM(b.day_hours), 2) AS pptd
      FROM (
        SELECT the_date, team_key, team_name, pay_period_seq, team_clock_hours_day AS day_hours
        FROM tp.data
        GROUP BY the_date, team_key, team_name, pay_period_seq, team_clock_hours_day) a,
        (
          SELECT the_date, team_key, team_name, pay_period_seq, team_clock_hours_day AS day_hours
          FROM tp.data
          GROUP BY the_date, team_key, team_name, pay_period_seq, team_clock_hours_day) b  
      WHERE a.pay_period_seq = b.pay_period_seq
        AND a.team_key = b.team_key
        AND b.the_date <= a.the_date
        and a.pay_period_seq = (
          select biweekly_pay_period_sequence
          from dds.dim_date
          where the_date = current_Date)        
      GROUP BY a.the_date, a.team_key, a.pay_period_seq, a.day_hours) x
    WHERE aa.the_date = x.the_date
      AND aa.team_key = x.team_key;     

-- techs
  -- flag hours  
    -- TechFlagHoursDay need to do main and body shop separately, main based on flag date, body on close date
    -- main shop
    UPDATE tp.Data aa
    SET Tech_Flag_Hours_Day = x.flag_hours
    FROM (
      SELECT a.tech_number, a.the_date, round(sum(coalesce(b.flag_hours, 0)), 2) AS flag_hours
      FROM tp.Data a
      LEFT JOIN (
        SELECT a.flag_date, a.ro, a.line, c.tech_number, a.flag_hours
        FROM dds.fact_repair_order_today a  
        JOIN dds.dim_tech c on a.tech_key = c.tech_key
        WHERE a.flag_hours <> 0
          AND a.store_code = 'ry1'
          and a.flag_date = current_date
          AND c.tech_number IN (
            SELECT tech_number
            FROM tp.Techs)) b on a.tech_number = b.tech_number AND a.the_date = b.flag_date   
      WHERE a.the_date = current_date
      GROUP BY a.tech_number, a.the_date) x
    WHERE aa.department_key = 18 -- (SELECT departmentkey FROM zDepartments WHERE dl2 = 'ry1' AND dl4 = 'mainshop' AND dl5 = 'Shop')
      AND aa.the_date = x.the_date
      AND aa.tech_number = x.tech_number;           

    -- body shop
    UPDATE tp.Data aa
    SET Tech_Flag_Hours_Day = x.flag_hours
    FROM (
      SELECT a.tech_number, a.the_date, round(sum(coalesce(b.flag_hours, 0)), 2) AS flag_hours
      FROM tp.Data a
      LEFT JOIN (
        SELECT a.close_date, a.ro, a.line, c.tech_number, a.flag_hours
        FROM dds.fact_repair_order_today a  
        JOIN dds.dim_Tech c on a.tech_key = c.tech_key
        WHERE a.flag_hours <> 0
          AND a.store_code = 'ry1'
          and a.close_date = current_date
          AND c.tech_number IN (
            SELECT tech_number
            FROM tp.Techs)) b on a.tech_number = b.tech_number AND a.the_date = b.close_date   
      WHERE a.the_date = current_date
      GROUP BY a.tech_number, a.the_date) x
    WHERE aa.department_key = 13 -- (SELECT departmentkey FROM zDepartments WHERE dl2 = 'ry1' AND dl4 = 'bodyshop')
      AND aa.the_date = x.the_date
      AND aa.tech_number = x.tech_number;        

    -- TechFlagHoursPPTD  
    UPDATE tp.Data aa
    SET Tech_Flag_Hours_PPTD = x.PPTD
    FROM (
      SELECT a.the_date, a.tech_key, a.pay_period_seq, a.tech_flag_hours_day, SUM(b.tech_flag_hours_day) AS pptd
      FROM tp.Data a, tp.data b
      WHERE a.pay_period_seq = b.pay_period_seq
        AND a.tech_key = b.tech_key
        AND b.the_date <= a.the_date
        and a.pay_period_seq = (
          select biweekly_pay_period_sequence
          from dds.dim_date
          where the_date = current_Date)         
      GROUP BY a.the_date, a.tech_key, a.pay_period_seq, a.tech_flag_hours_day) x
    WHERE aa.the_date = x.the_date
      AND aa.tech_key = x.tech_key;      
      
-- teams
    -- teamflaghoursday
    UPDATE tp.Data aa
    SET Team_Flag_Hours_Day = x.hours
    FROM (
      SELECT the_date, team_key, SUM(tech_flag_hours_day) AS hours
      FROM tp.data 
      WHERE the_date = current_Date  
      GROUP BY the_date, team_key) x
    WHERE aa.the_date = x.the_date
      AND aa.team_key = x.team_key;

    -- teamflaghour pptd  
    UPDATE tp.Data aa
    SET team_flag_hours_pptd = x.pptd
    FROM (
      SELECT a.the_date, a.team_key, a.pay_period_seq, a.day_hours, round(SUM(b.day_hours), 2) AS pptd
      FROM (
        SELECT the_date, team_key, team_name, pay_period_seq, team_flag_hours_day AS day_hours
        FROM tp.data
        GROUP BY the_date, team_key, team_name, pay_period_seq, team_flag_hours_day) a,
        (
        SELECT the_date, team_key, team_name, pay_period_seq, team_flag_hours_day AS day_hours
        FROM tp.data
        GROUP BY the_date, team_key, team_name, pay_period_seq, team_flag_hours_day) b  
      WHERE a.pay_period_seq = b.pay_period_seq
        AND a.team_key = b.team_key
        AND b.the_date <= a.the_date
        and a.pay_period_seq = (
          select biweekly_pay_period_sequence
          from dds.dim_date
          where the_date = current_Date)           
      GROUP BY a.the_date, a.team_key, a.pay_period_seq, a.day_hours) x
    WHERE aa.the_date = x.the_date
      AND aa.team_key = x.team_key;  

    -- team prof day          
    UPDATE tp.data aa 
    SET team_prof_day = x.prof
    FROM (
      SELECT a.*,
        CASE 
          WHEN Total_Flag = 0 OR team_clock_hours_day = 0 THEN 0
          ELSE round(100 * Total_Flag/team_clock_hours_day, 2)
        END AS prof
      FROM (
        SELECT the_date, team_key, 
          (team_flag_hours_day + team_Other_Hours_Day + team_Training_Hours_Day + Team_Flag_Hour_Adjustments_Day) AS Total_Flag, 
          team_clock_hours_day
        FROM tp.data
        WHERE the_date = current_Date      
        GROUP BY the_date, team_key, 
          (team_flag_hours_day + team_Other_Hours_Day + team_Training_Hours_Day + Team_Flag_Hour_Adjustments_Day), 
          team_clock_hours_day) a) x
    WHERE aa.the_date = x.the_date
      AND aa.team_key = x.team_key;  

    -- team prof pptd
    UPDATE tp.data aa
    SET team_prof_pptd = x.prof
    FROM (
      SELECT a.*,
        CASE 
          WHEN Total_Flag = 0 OR team_clock_hours_pptd = 0 THEN 0
          ELSE round(100 * Total_Flag/team_clock_hours_pptd, 2)
        END AS prof
      FROM (
        SELECT the_date, team_key, 
          (team_flag_hours_pptd + team_Other_Hours_PPTD + team_Training_Hours_PPTD + Team_Flag_Hour_Adjustments_PPTD) AS Total_Flag, 
          team_clock_hours_pptd
        FROM tp.data
        WHERE pay_period_seq = (
          select biweekly_pay_period_sequence
          from dds.dim_date
          where the_date = current_Date)    
        GROUP BY the_date, team_key, 
          (team_flag_hours_pptd + team_Other_Hours_PPTD + team_Training_Hours_PPTD + Team_Flag_Hour_Adjustments_PPTD), 
          team_clock_hours_pptd) a) x
    WHERE aa.the_date = x.the_date
      AND aa.team_key = x.team_key; 

                  
end $$;


select * from tp.data where the_date = current_date and department_key = 18 order by team_name, first_name