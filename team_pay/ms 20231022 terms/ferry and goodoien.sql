﻿-- carter ferry and derek goodoien termed
-- for now, just close out the tp.team_techs records, suspect they will be replaced on the respective teams

Terms
Carter Ferry (team McVeigh) 11/01/23
Derek Goodoien (team Gehrtz) 10/27/23

select * from tp.teams where team_name in ('mcveigh','gehrtz')
department_key: 18
McVeigh: 63
Gehrtz: 65

select * from tp.techs where last_name in ('ferry','goodoien') and thru_date > current_date
Goodoien: 87
Ferry: 119

select 
  the_date, last_name, tech_key, tech_clock_hours_day, tech_clock_hours_pptd, tech_flag_hours_day, tech_flag_hours_pptd 
from tp.data where tech_key in (87,119) 
  and the_date > '10/21/2023' 
order by tech_key, the_date

delete 
-- select the_date, last_name, tech_key, tech_clock_hours_day, tech_clock_hours_pptd, tech_flag_hours_day, tech_flag_hours_pptd  
from tp.data
where tech_key in (87,119)
  and the_date > '11/04/2023'

select * from tp.team_techs
where team_key in (63, 65)
  and tech_key in (87, 119)  

update tp.team_techs
set thru_date = '11/05/2023'
where team_key = 65
  and tech_key = 87;

update tp.team_techs
set thru_date = '11/05/2023'
where team_key = 63 
  and tech_key = 119;


delete from tp.gm_main_shop_flat_rate_payroll_data where pay_period_start = '11/05/2023' and last_name = 'goodoien';

delete from tp.gm_main_shop_flat_rate_payroll_data where pay_period_start = '11/05/2023' and last_name = 'ferry';