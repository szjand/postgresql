﻿select aa.team_name, 
	aa.team_key, 
	aa.census, aa.budget, aa.pool_percentage as "pool %", 
	cc.last_name, cc.first_name, 
	cc.tech_key, 
	cc.tech_number, cc.employee_number as emp,
  cc.tech_team_percentage as "tech %", round(aa.budget * cc.tech_team_percentage, 2) as tech_flat_rate
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 13) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date) cc on bb.tech_key = cc.tech_key 
where aa.team_name = 'team 16'	
order by aa.team_name, cc.last_name


do $$
declare 
	_thru_date date:= '09/16/2023';
	_tech_key integer := 78; 
	_team_key integer := 45; 
	_last_of_pp date:= '09/23/2023';
begin
  update tp.teams
  set thru_date = _thru_date
  where team_key = _team_key
    and thru_date = '12/31/9999';
  update tp.techs
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date = '12/31/9999';
  update tp.team_techs
  set thru_date = _thru_date
  where tech_key = _tech_key
    and team_key = _team_key
    and thru_date = '12/31/9999';    
  update tp.team_values
  set thru_date = _thru_date
  where team_key = _team_key
    and thru_date = '12/31/9999';
  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date = '12/31/9999';

  delete 
  from tp.data
  where tech_key = _tech_key
    and the_date > _last_of_pp;

end $$;	


select * from tp.teams order by team_name
select * from tp.techs where tech_key = 78
select * from tp.team_techs where tech_key = 78
select * from tp.team_values where team_key = 45
select * from tp.tech_values where tech_key = 78
select * from tp.data where tech_key = 78 and the_date > '09/13/2023' order by the_date