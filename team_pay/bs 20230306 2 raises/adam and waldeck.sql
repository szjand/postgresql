﻿/*
03/06/23
Jon, 

Can you put a pay bump in for both Chris Walden and Pat Adams?

Chris will go from $19.80 to $21.80
Pat will go from $16.25 to $19.00

This pay will be for this pay period so it should start on 2/26

If you have any questions, please let me know. 

Thank you, 
Tim 

select aa.team_name, 
	aa.team_key, 
	aa.census, aa.budget, aa.pool_percentage as "pool %", 
	cc.last_name, cc.first_name, 
	cc.tech_key, 
	cc.tech_number, cc.employee_number as emp,
  cc.tech_team_percentage as "tech %", round(aa.budget * cc.tech_team_percentage, 2) as tech_flat_rate
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 13) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date) cc on bb.tech_key = cc.tech_key 
where aa.team_name = 'paint team'	
order by aa.team_name, cc.last_name
*/

02/26 - 03/11
-- adam: values (19/90.0)  .21112

update tp.tech_values
set thru_date = '02/25/2023'
where tech_key = 72
  and thru_date > current_date;
insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
values(72, '03/26/2023', .21112, 28.81);

-- walden values (21.8/90) .24223

update tp.tech_values
set thru_date = '02/25/2023'
where tech_key = 31
  and thru_date > current_date;
insert into tp.tech_values(tech_key,from_date,tech_team_percentage,hourly_rate)
values(31, '03/26/2023', .24223, 34.86);