﻿Jon, 

Cody Olsen, team 15 was given a raise. Flat rate is now $27  (was 25.50)
Nathan Nichols Team 23 was given a raise. Flat rate is now $25 ( was 23 )

These raises will take effect 8/13.

If you have any questions, please let me know. 

Thank you, 
Tim 


select aa.team_name, 
	aa.team_key, 
	aa.census, aa.budget, aa.pool_percentage as "pool %", 
	cc.last_name, cc.first_name, 
	cc.tech_key, 
	cc.tech_number, cc.employee_number as emp,
  cc.tech_team_percentage as "tech %", round(aa.budget * cc.tech_team_percentage, 2) as tech_flat_rate
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 13) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date) cc on bb.tech_key = cc.tech_key 
where aa.team_name in ( 'team 15', 'team 23')
order by aa.team_name, cc.last_name

-- select * from tp.team_values where team_key = 44

update tp.team_values
set thru_date = '08/12/2023'
where team_key = 44
  and thru_date > current_date;

-- no need to change tech_values, tech_team_percentage remains at 1  

insert into tp.team_values(team_key,census,budget,pool_percentage,from_date)
values(44,1,27.00,1,'08/13/2023');  


-- select * from tp.team_values where team_key = 59

update tp.team_values
set thru_date = '08/12/2023'
where team_key = 59
  and thru_date > current_date;

-- no need to change tech_values, tech_team_percentage remains at 1  

insert into tp.team_values(team_key,census,budget,pool_percentage,from_date)
values(59,1,25.00,1,'08/13/2023');  