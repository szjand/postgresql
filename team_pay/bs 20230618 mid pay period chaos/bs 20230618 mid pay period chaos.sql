﻿Got kind of a situation with the paint team. The late notice is all my fault, as I have been on PTO this last week. 
Chris Walden had to leave to go take care of his mom. His last day was 6/23. 
Dave Bies will be replacing Chris as a Paint Tech on the paint team starting Monday, 6/26. 
I understand that this is right in the middle of the pay period and Dave has already closed metal hours, 
this past week. I put a compli notice in for the change. Additionally, Dave is currently getting paid $28/flat rate. 
He will be moving to $19.80/flat rate on the paint team. 

select aa.team_name, 
	aa.team_key, 
	aa.census, aa.budget, aa.pool_percentage as "pool %", 
	cc.last_name, cc.first_name, 
	cc.tech_key, 
	cc.tech_number, cc.employee_number as emp,
  cc.tech_team_percentage as "tech %", round(aa.budget * cc.tech_team_percentage, 2) as tech_flat_rate
from (
	select a.team_key, a.team_name, b.census, b.budget, b.pool_percentage 
	from tp.teams a  -- 9 teams
	join tp.team_values b on a.team_key = b.team_key
		and b.thru_date > current_date
	where a.thru_date > current_date
		and a.department_key = 13) aa
join tp.team_techs bb on aa.team_key = bb.team_key
  and bb.thru_date > current_date
join (
	select a.tech_key, a.tech_number, a.employee_number, a.last_name, a.first_name, b.tech_team_percentage, b.hourly_rate
	from tp.techs a 
	join tp.tech_values b on a.tech_key = b.tech_key
		and b.thru_date > current_date
	where a.thru_date > current_date) cc on bb.tech_key = cc.tech_key 
where aa.team_name = 'paint team'	
-- where aa.team_name = 'team 19'	
order by aa.team_name, cc.last_name

----------------------------------------------
--< TODO
----------------------------------------------

need paint team pay calculated (show work) for 6/18 -> 6/23 while walden was on the team
update paint team to 3 techs (currently shows 4)

----------------------------------------------
--/> TODO
----------------------------------------------
-- check for last flag/clock hours for walden
select * from dds.dim_tech where tech_name like '%walden%'

pay period: 6/18 - 7/1
paint team

David Bies II		293		1.0        28        team 19
Thomas Sauls    311   .17223     15.5007   paint team
Patrick Adam    273   .19167     17.2503   paint team
Chris Walden    204   .23112     20.8008   paint team

select * from tp.data where last_name in ('adam','sauls','walden','bies ii') and the_date = current_Date - 1

select the_date, employee_number, first_name, last_name, tech_number, tech_flag_hours_day, coalesce(tech_clock_hours_day, 0) as clock_hours
from tp.data
where the_date between '06/18/2023' and '07/01/2023'
  and tech_number in ('293','311','273','204')
order by the_date, last_name

-- proficiency for paint team wil include sauls,adam,walden thru 6/25, from 6/26 thru 7/1 also bies
-- bies proficiency will be thru 6/25 as team 19 @ 28, then as part of the paint team @ 19.80
select the_date, employee_number, first_name, last_name, tech_number, sum(tech_flag_hours_day) as flag_hours, sum(tech_clock_hours_day) as clock_hours
from tp.data
where the_date between '06/18/2023' and '06/25/2023'
  and tech_number in ('293','311','273','204')
group by the_date, employee_number, first_name, last_name, tech_number  
order by the_date, last_name

-- individual stats 6/18 - 6/25
select employee_number, first_name, last_name, tech_number, sum(tech_flag_hours_day) as flag_hours, sum(tech_clock_hours_day) as clock_hours
from tp.data
where the_date between '06/18/2023' and '06/25/2023'
  and tech_number in ('293','311','273','204')
group by employee_number, first_name, last_name, tech_number  

-- individual stats 6/26 - 7/1
-- walden has flag hours but no clock hours
select employee_number, first_name, last_name, tech_number, sum(tech_flag_hours_day) as flag_hours, sum(tech_clock_hours_day) as clock_hours
from tp.data
where the_date between '06/26/2023' and '07/01/2023'
  and tech_number in ('293','311','273','204')
group by employee_number, first_name, last_name, tech_number  

-- paint team proficiency 6/18 - 6/25m -- excludes bies
select sum(tech_flag_hours_day) as flag_hours, sum(tech_clock_hours_day) as clock_hours,
  sum(tech_flag_hours_day)/sum(tech_clock_hours_day)
from tp.data
where the_date between '06/18/2023' and '06/25/2023'
  and tech_number in ('311','273','204')
 -- yuck, this is going to be ugly, proficiency gets padded with flag hours at the end of the pay period

select sum(tech_flag_hours_day) as flag_hours, sum(tech_clock_hours_day) as clock_hours,
  sum(tech_flag_hours_day)/sum(tech_clock_hours_day)
from tp.data
where the_date between '06/26/2023' and '07/01/2023'
  and tech_number in ('293','311','273','204')

-- pay generation -----------------------------------------------------------------------------------------------------------
ukg.get_body_shop_flat_rate_payroll()
|
V
tp.get_body_shop_flat_rate_managers_payroll_approval_data(_pay_period_ind)
|
V
tp.body_shop_flat_rate_payroll_data

-- tp.get_body_shop_flat_rate_managers_payroll_approval_data(_pay_period_ind)
-- pp seq for 6/4-6/17 is 378
-- comm hours = team prof * tech clock_hours
		select pay_period_select_format,  team, (last_name || ', ' || first_name)::citext as tech, employee_number,
			flat_rate,team_prof, round(flag_hours, 2) as flag_hours,
			round(clock_hours, 4) as clock_hours, round(comm_hours, 4) as comm_hours, 
			round(comm_pay, 2) as comm_ppay
		from tp.body_shop_flat_rate_payroll_data
		where pay_period_seq = 378
		order by team, last_name; 

select * From dds.dim_date where the_date = '06/15/2023'		

-- pay generation -----------------------------------------------------------------------------------------------------------

-- pay history per day--------------------------------------------------------------------------------------------------------------
pay period	adam	sauls	walden	bies paint team prof
6/4 - 6/17	384	  304		400			452    235
5/21 - 6/3	305		260		301			266    217
5/7 - 5/20	400		304		424			325    246
4/23 - 5/6	424		362		443			337    258
4/9 - /4/22	412		331		383			291    252


-- pay history --------------------------------------------------------------------------------------------------------------

-- different approach -------------------------------------------------------------------------------------------------------
leave paint team alone, leave all of walden in just add bies from 6/26 on

-- 360.90,202.23,1.7846016911437472
select sum(tech_flag_hours_day) as flag_hours, sum(tech_clock_hours_day) as clock_hours,
  sum(tech_flag_hours_day)/sum(tech_clock_hours_day)
from tp.data
where the_date between '06/18/2023' and '07/01/2023'
  and tech_number in ('311','273','204')

65.50,31.96,2.0494367959949937
-- turns out, the 65.5 flagged on 6/26 actually closed on the 24th, so whatever this total
-- turns out to be, will need to subtract 65.5 hours
select sum(tech_flag_hours_day) as flag_hours, sum(tech_clock_hours_day) as clock_hours,
  sum(tech_flag_hours_day)/sum(tech_clock_hours_day)
from tp.data
where the_date between '06/18/2023' and '06/25/2023'
  and tech_number in ('293')  

add those 2 up, comes out to 182.07
select (360.90 + 65.5), (202.23 + 31.96), (360.90 + 65.5)/(202.23 + 31.96)

-- removing the bies 65.5 flag hours
-- results in a prof of 154.11% (friday morning)
select (360.90 + 0), (202.23 + 31.96), (360.90 + 0)/(202.23 + 31.96)


-- also need bies at the 28 rate
select * from tp.data where last_name = 'bies ii' and the_date between '06/18/2023' and '06/25/2023'

which only yields 18.4 flag hours
prof: 18.4/44.31 = 41.52%


-- turns out, the actual flag hours for bies pre paint team is 83.9 hours
-- so, with 44.31 clock hours, prof = select 83.9/44.31 = 1.89348
-- there fore, his pre paint team earnings are 
select sum(tech_flag_hours_day) as flag_hours, sum(tech_clock_hours_day) as clock_hours,
  sum(tech_flag_hours_day)/sum(tech_clock_hours_day)
from tp.data
where the_date between '06/18/2023' and '06/25/2023'
  and tech_number in ('293')  

  
-- different approach -------------------------------------------------------------------------------------------------------

-- pay calc ---------------------------------------------------------------------------------------------------------

tech% = tech_flat_rate/budget
bies:  select 19.8/90  .2200

pay = tech_flat_rate * team_prof * tech_clock_hours

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
the remaining question, for the guys already on the paint team, do i split the proficiency in two? a prof without bies and a prof with bies?

at this point think no, a single proficiency

select * from tp.body_shop_flat_Rate_payroll_data 
where team in ('paint team', 'team 19') 
  and pay_period_start = '06/18/2023'
-- pay calc ---------------------------------------------------------------------------------------------------------

-- bies pre paint team flag hours ------------------------------------------------------------------------------------
from tim

He has closed 4 ROs before moving to the paint team. 
18096865 - 40.7hrs
18096955 - 17.4hrs
18097873 - 24.8hrs
18098021 - 1.0hrs
Total - 83.9hrs

He had two jobs he finished up with on 6/24 what wouldnt have been closed until 6/26

and
Jon, 

Add RO 18098187 - 18.2hrs
We dont do shop time until Friday and he is owed those hours for training, as a metal tech. 

select * from dds.fact_repair_order where ro in ('18096865','18096955','18097873','18098021') and tech_key = 1146
select * from dds.dim_tech where tech_name like '%bies%'

-- 7/2
so, the total flag hours for bies, pre paint team, per Tim is 83.9 + 18.2 - 7.8 (adj) = 99.7

select 83.9 + 18.2 - 7.8

select * from tp.data where tech_number = '293' and the_date = '07/01/2023'

select * from dds.fact_repair_order where ro = '18097566'

select * from arkona.ext_sdpxtim where technician_id = '293' order by trans_date desc limit 100
select * from dds.dim_tech where tech_number = '293'  -- tech_key = 1146
-- Bies no flag hours for the paint team, these all match up with what Tim sent me
select ro, close_date, flag_hours
from dds.fact_repair_order 
where tech_key = 1146
  and close_date between '06/18/2023' and '07/01/2023'
order by ro  

select the_date, tech_clock_hours_day from tp.data where tech_number = '293' and the_date between '06/18/2023' and '07/01/2023' and coalesce(tech_clock_hours_day, 0) > 0

-- bies pre paint team flag hours ------------------------------------------------------------------------------------

-- paint team --------------------------------------------------------------------------------------------------------

select first_name || ' ' || last_name, tech_tfr_rate, tech_clock_hours_pptd, tech_flag_hours_pptd, team_flag_hours_pptd, team_clock_hours_pptd, team_prof_pptd 
-- select * 
from tp.data where team_name = 'paint team' and  the_date = '07/01/2023' --between '06/18/2023' and '07/01/2023'


-- paint team --------------------------------------------------------------------------------------------------------













--add bies to paint team eff 7/2--------------------------------------------------------------------------------------

do $$
declare 
	_eff_date date := '07/02/2023';
	_thru_date date:= '07/01/2023';
	_team_key integer;
	_tech_key integer;

begin
-- close out team 19 (bies team of one)
  _team_key = (select team_key from tp.teams where team_name = 'team 19' and thru_date > current_date);  -- 49
  _tech_key = (select tech_key from tp.techs where tech_number = '293' and thru_date > current_date);  -- 85
  update tp.teams
  set thru_date = _thru_date
  where team_key = _team_key
    and thru_date > current_date;
  update tp.team_techs
  set thru_date = _thru_date
  where team_key = _team_key
    and tech_key = _tech_key
    and thru_date > current_date;
  update tp.team_values
  set thru_date = _thru_date
  where team_key = _team_key
    and thru_date > current_Date;

-- remove chris walden
  _team_key = (select team_key from tp.teams where team_name = 'paint team' and thru_date > current_date);  -- 29
  _tech_key = (select tech_key from tp.techs where tech_number = '204' and thru_date > current_date);  -- 31
  update tp.techs
  set thru_date = thru_date
  where tech_key = _tech_key
    and thru_date > current_date;
    
  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;
  
  update tp.team_techs
  set thru_date = _thru_date
  where team_key = _team_key
    and tech_key = _tech_key
    and thru_date > current_date;

-- add bies to paint team   
  -- select * from tp.tech_values where tech_key = 85 
  _tech_key = (select tech_key from tp.techs where tech_number = '293' and thru_date > current_date);  -- 85
  
  insert into tp.team_techs(team_key,tech_key,from_date)
  values(_team_key, _tech_key, _eff_date); 

  update tp.tech_values
  set thru_date = _thru_date
  where tech_key = _tech_key
    and thru_date > current_date;

  insert into tp.tech_values(tech_key, from_date, tech_team_percentage, hourly_rate)
  values(_tech_key, _eff_date, 0.22, 34.91);

end $$;      




--add bies to paint team eff 7/2--------------------------------------------------------------------------------------