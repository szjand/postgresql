﻿/*
from the "meeting" 9/9
	what's the deal with recon gross proxy? as opposed to service gross?

*/

select * from ads.ext_vehicle_evaluations where vehicleevaluationts::date > current_date - 31 limit 20
select * from ads.ext_Vehicle_item_statuses where vehicleitemid = 'c45ec35f-403d-7749-8e03-cd6f3d32a272'
select * from ads.ext_vehicle_acquisitions where vehicleinventoryitemid = '4f1f6bb4-8363-4d99-8970-84399f30c0c6'
select * from ads.ext_Vehicle_item_statuses where vehicleitemid = 'e923fb92-c410-354a-8bdf-a2de3a451d82'
/*
TODO:  looks like we are going to need a category of eval started but not complete, because for most of those there is no evaluator

TODO: instead of fucking pvr, maybe something long paid vs price

the evalTS is updated in the eval table when the status changes
*/

drop table if exists jon.evals cascade;
create unlogged table jon.evals as
select b.yearmodel as model_year, b.make, b.model, g.stocknumber, b.vin, a.vehicleevaluationts::date as eval_date, g.fromts::date as book_date,
  case c.fullname
    when 'GF-Rydells' then 'GM'
    when 'GF-Toyota' then 'Toyota'
    when 'GF-Honda Cartiva' then 'HN'
  end as store, d.fullname as consultant, e.fullname as evaluator,
  case
    when h.description = 'auction' and e.fullname  = 'Eric Hanson' then 'Street'
    else h.description 
  end as source, i.description as status
-- -- select a.* 
from ads.ext_vehicle_evaluations a  -- limit 10
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
join ads.ext_organizations c on a.locationid = c.partyid
left join ads.ext_people d on a.salesconsultantid = d.partyid
left join ads.ext_people e on a.vehicleevaluatorid =  e.partyid
join ads.ext_typ_descriptions h on a.typ = h.typ
-- left join ads.ext_vehicle_third_party_values f on a.vehicleitemid = f.vehicleitemid
left join ads.ext_vehicle_inventory_items g on a.vehicleinventoryitemid = g.vehicleinventoryitemid
left join ads.ext_status_descriptions i on a.currentstatus = i.status
where a.vehicleevaluationts::date > current_date - 30
order by eval_date 

select * from jon.evals

/*
9/13/22
putting together a vision page for documenting missed trades
query in advantage for afton

select 
  case c.fullname
    when 'GF-Rydells' then 'GM'
    when 'GF-Toyota' then 'Toyota'
    when 'GF-Honda Cartiva' then 'HN'
  end as store,  
	b.yearmodel, b.make, b.model, b.vin, 
  cast(a.vehicleevaluationts AS sql_date) as eval_date, 
 d.fullname as consultant, e.fullname as evaluator, amountshown AS amount_shown
from VehicleEvaluations  a  -- limit 10
join VehicleItems  b on a.vehicleitemid = b.vehicleitemid
join organizations c on a.locationid = c.partyid
left join people d on a.salesconsultantid = d.partyid
left join people e on a.vehicleevaluatorid =  e.partyid
left join VehicleInventoryItems g on a.vehicleinventoryitemid = g.vehicleinventoryitemid
where cast(a.vehicleevaluationts AS sql_date) = curdate() - 1
  AND a.currentstatus = 'VehicleEvaluation_Finished'

  
*/


/*
09/11  considered using board data to clarify source
don't feel like fucking with those anomalies
sales without acquisitions, G45396P,
multiple acquisitions of the same stock number, G45023B? 


*/
-- these are the "not even get to the evaluator" vehicles
select store, count(*)
from jon.evals
where evaluator is null
  and store is not null
group by store

select store, consultant, count(*)
from jon.evals
where evaluator is null 
group by store, consultant
order by count(*) desc 

select
  count(*) filter (where evaluator is null) as no_evaluator,
  count(*) filter (where evaluator is not null) as with_evaluator,
  count(*) filter (where evaluator is not null and status = 'Booked') as booked,
  count(*) filter (where evaluator is not null and status = 'Finished') as finished,
  count(*) filter (where evaluator is not null and status = 'In Appraisal') as in_appraisal
from jon.evals  

select * from jon.evals where evaluator is not null

-- the statuses:
-- 	Booked
-- 	In Appraisal
-- 	Finished

select store, evaluator, status, count(*)
-- select count(*)
from jon.evals
where evaluator is not null
group by store, evaluator, status

select * from jon.evals where evaluator = 'casper bach' and status <> 'booked'

drop table if exists _pivot_1;
select jon.colpivot (
	'_pivot_1', -- temp table name
	$$
		select store, evaluator, status, count(*) as the_count
		from jon.evals
		where evaluator is not null
		group by store, evaluator, status 
	$$,
	array['store','evaluator'], -- data headers 
	array['status'], -- pivoted header
	'#.the_count', -- data
	'status'); -- sort order of pivoted headers
select * from _pivot_1 order by evaluator

select string_agg('alter table _pivot_1 rename column "'||column_name||'" to "'||replace(column_name,'''','')||'"', ';' order by ordinal_position)
from information_schema.columns
where table_name = '_pivot_1'
  and column_name not in ('store','evaluator')

alter table _pivot_1 rename column "'Booked'" to Booked;
alter table _pivot_1 rename column "'Finished'" to Finished;
alter table _pivot_1 rename column "'In Appraisal'" to In_Appraisal

select sum("Booked"), sum("Finished"), sum("In_Appraisal")
from _pivot_1


select a.*, b.Data_Only 
from _pivot_1 a
left join (
	select store, consultant, count(*) as Data_Only
	from jon.evals
	where evaluator is null 
	group by store, consultant) b on a.evaluator = b.consultant
order by a.finished desc nulls last

/* start of a slack about _pivot_1
what bothers me about the apple report is that i don't readily perceive actionable information, it strikes as yet another dashboard view
for contrast look at the attached spreadsheet
over the last 30 days, of the 536 evaluations that were at least started by a consultant, 76 did not make to an evaluator, of the remaining 460, 246 were booked
drop table if exists _pivot_2;

what annoys me is the lack of attention to detail
triggered by at auction vehicle actually sold in july, sold and not ddelivered not being maintained
that contributes to the lack of believability of tool numbers
so fuck it
focus on reports that show immediate actionable information
fix that shit, and the rest will get better

show the eval numbers, and point ot the immediately apparent actionable item:
i want a reason for each one of those unbooked evals

in my mind starting to see 3 basic kinds of reporting
1. actionable
2. dashboard
3. greg

the difference between actionable and dashboard
look to book
dashboard:
48% oh we are not doing so great
52% oh we are right where we always are
week after week after weekd ad infinitum
actionable
i want starting today an explanation for every started eval not booked
an analysis of appraisals that require follow up
a decision on who is and who is not authorized to give a number to a customer
so, with follow up and authorized only evaluators, does it get better?

our tool meeting was not a tool meeting
it was yet another brushfire meeting
contributed absolutely nothing to a spec enabling progress on tool 2.0
*/

drop table if exists _pivot_2;
select jon.colpivot (
	'_pivot_2', -- temp table name
	$$
		select store, evaluator, source || '_' || status as source_status, count(*) as the_count
		from jon.evals
		where evaluator is not null
		group by store, evaluator, source || '_' || status
		order by store, evaluator, source || '_' || status
	$$,
	array['store','evaluator'], -- data headers 
	array['source_status'], -- pivoted header
	'#.the_count', -- data
	'source_status'); -- sort order of pivoted headers


select string_agg('alter table _pivot_2 rename column "'||column_name||'" to "'||replace(column_name,'''','')||'"', ';' order by ordinal_position)
from information_schema.columns
where table_name = '_pivot_2'
  and column_name not in ('store','evaluator')

alter table _pivot_2 rename column "'Auction_Booked'" to Auction_Booked;
alter table _pivot_2 rename column "'Street_Booked'" to Street_Booked;
alter table _pivot_2 rename column "'Street_Finished'" to Street_Finished;
alter table _pivot_2 rename column "'Trade_Booked'" to Trade_Booked;
alter table _pivot_2 rename column "'Trade_Finished'" to Trade_Finished;
alter table _pivot_2 rename column "'Trade_In Appraisal'" to Trade_In_Appraisal

select * from _pivot_2 order by evaluator


select store, evaluator, source || ':' || status as source_status, count(*)
from jon.evals
where evaluator is not null
group by store, evaluator, source || ':' || status
order by store, evaluator, source || ':' || status


-------------------------------------------------------------------------------
--< Of the cars we currently own - who brought them in, what the recon status is and what is the potential gross based on price.		
-------------------------------------------------------------------------------
-- 406 total, 351 w/price
-- from > minute to 7 sec for recon status
-- 9/11 V2 include recon est
-- 9/12 V3 include recon so far
-- 10/4 updated table for greg
drop table if exists jon.current_priced cascade;
create table jon.current_priced as
select a.vehicleinventoryitemid, b.yearmodel as model_year, a.stocknumber, b.vin, b.make, b.model, b.trim, 
  f.fullname as evaluator, d.amount::integer as price, g.cost_no_recon::integer, gg.recon_so_far::integer, -- d.amount::integer - g.the_cost::integer as pot_gross,
  current_date - a.fromts:: date as age,
  h.mechanical, h.body, h.appearance,
  coalesce(i.recon_est , 0) as recon_est
from ads.ext_Vehicle_inventory_items a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
join (
  select vehicleinventoryitemid, vehiclepricingid, vehiclepricingts, row_number() over (partition by vehicleinventoryitemid order by vehiclepricingts desc) as seq
  from ads.ext_vehicle_pricings 
  order by vehicleinventoryitemid, vehiclepricingts desc) c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
    and c.seq = 1
join ads.ext_Vehicle_pricing_details d on c.vehiclepricingid = d.vehiclepricingid
  and d.typ = 'VehiclePricingDetail_BestPrice'
join ads.ext_vehicle_evaluations e on a.vehicleinventoryitemid = e.vehicleinventoryitemid
join ads.ext_people f on e.vehicleevaluatorid = f.partyid  
left join (
  select a.control, sum(a.amount) cost_no_recon
  from fin.fact_gl a
  join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
    and b.thruts::date > current_date
  join fin.dim_account c on a.account_key = c.account_key
  join jeri.inventory_accounts d on c.account = d.account
    and d.department = 'uc'
  join fin.dim_journal e on a.journal_key = e.journal_key
    and e.journal_code not in ('SVI','SCA','SWA','POT')
  where a.post_status = 'Y'
  group by a.control) g on a.stocknumber = g.control  
left join (
  select a.control, sum(a.amount) recon_so_far
  from fin.fact_gl a
  join ads.ext_vehicle_inventory_items b on a.control = b.stocknumber
    and b.thruts::date > current_date
  join fin.dim_account c on a.account_key = c.account_key
  join jeri.inventory_accounts d on c.account = d.account
    and d.department = 'uc'
  join fin.dim_journal e on a.journal_key = e.journal_key
    and e.journal_code in ('SVI','SCA','SWA','POT')
  where a.post_status = 'Y'
  group by a.control) gg on a.stocknumber = gg.control    
left join (
    SELECT viix.vehicleinventoryitemid,
      CASE (
          SELECT status
          FROM ads.ext_Vehicle_Inventory_Item_Statuses 
          WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
          AND category = 'MechanicalReconProcess'
          AND ThruTS > now())
        WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
        WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
        WHEN 'MechanicalReconProcess_NotStarted' THEN 
          CASE 
            WHEN (
              SELECT COUNT(VehicleInventoryItemID)
              FROM ads.ext_Vehicle_Inventory_Item_Statuses
              WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
              AND category = 'MechanicalReconDispatched'
              AND ThruTS  > now()) = 0 THEN 'Not Started'
            ELSE
              'Dispatched'
            END
		ELSE 'None' 
      END AS mechanical,
      CASE (
          SELECT status
          FROM ads.ext_Vehicle_Inventory_Item_Statuses 
          WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
          AND category = 'BodyReconProcess'
          AND ThruTS  > now())
        WHEN 'BodyReconProcess_InProcess' THEN 'WIP'
        WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
        WHEN 'BodyReconProcess_NotStarted' THEN 
          CASE 
            WHEN (
              SELECT COUNT(VehicleInventoryItemID)
              FROM ads.ext_Vehicle_Inventory_Item_Statuses
              WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
              AND category = 'BodyReconDispatched'
              AND ThruTS  > now()) = 0 THEN 'Not Started'
            ELSE
              'Dispatched'
            END
	    ELSE 'None' 
      END AS body,
      CASE (
          SELECT status
          FROM ads.ext_Vehicle_Inventory_Item_Statuses 
          WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
          AND category = 'AppearanceReconProcess'
          AND ThruTS  > now())
        WHEN 'AppearanceReconProcess_InProcess' THEN 'WIP'
        WHEN 'AppearanceReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
        WHEN 'AppearanceReconProcess_NotStarted' THEN 
          CASE 
            WHEN (
              SELECT COUNT(VehicleInventoryItemID)
              FROM ads.ext_Vehicle_Inventory_Item_Statuses
              WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
              AND category = 'AppearanceReconDispatched'
              AND ThruTS  > now()) = 0 THEN 'Not Started'
            ELSE
              'Dispatched'
            END
	    ELSE 'None' 
      END AS appearance    
    FROM ads.ext_Vehicle_Inventory_Items viix
    where viix.thruts > now()) h on a.vehicleinventoryitemid = h.vehicleinventoryitemid
left join (
	select stocknumber, sum(recon_est) as recon_est
	from (
	-- open items
		SELECT vii.stocknumber, coalesce(sum(vri.TotalPartsAmount + vri.LaborAmount), 0) as recon_est
		FROM ads.ext_vehicle_recon_items vri
		join ads.ext_vehicle_inventory_items vii on vri.vehicleinventoryitemid = vii.vehicleinventoryitemid
			and vii.thruts > now()
		INNER JOIN ads.ext_Authorized_Recon_Items ari ON ari.VehicleReconItemID = vri.VehicleReconItemID
			AND ari.CompleteTS IS null
			AND ari.Status <> 'AuthorizedReconItem_Complete'
			AND ari.ReconAuthorizationID = (
				SELECT ra.ReconAuthorizationID 
				FROM ads.ext_Recon_Authorizations ra 
				WHERE ra.VehicleInventoryItemID = vii.vehicleinventoryitemid -- 'bc82f13b-d00f-4680-a671-3c196a9037d3'
					AND ra.ThruTS IS null)
		group by vii.stocknumber
		having coalesce(sum(vri.TotalPartsAmount + vri.LaborAmount), 0) <> 0
	union
	-- completed items
		SELECT vii.stocknumber, coalesce(sum(vri.TotalPartsAmount + vri.LaborAmount), 0) as recon_est
		FROM ads.ext_Vehicle_Recon_Items vri
		join ads.ext_vehicle_inventory_items vii on vri.vehicleinventoryitemid = vii.vehicleinventoryitemid
			and vii.thruts > now()
		INNER JOIN ads.ext_Authorized_Recon_Items ari ON ari.VehicleReconItemID = vri.VehicleReconItemID
			AND ari.Status = 'AuthorizedReconItem_Complete'
		group by vii.stocknumber
		having coalesce(sum(vri.TotalPartsAmount + vri.LaborAmount), 0) <> 0) a
	group by stocknumber) i	on a.stocknumber = i.stocknumber   
where a.thruts::date > current_date
  and cost_no_recon > 100;


create unique index on jon.current_priced(vehicleinventoryitemid);
create unique index on jon.current_priced(stocknumber);

select * from jon.current_priced order by stocknumber limit 500
-------------------------------------------------------------------------------
--/> Of the cars we currently own- who brought them in, what the recon status is & what is the potential gross based on price.		
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--< accounting
-------------------------------------------------------------------------------
select * 
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202207
  and b.page = 16 
  and b.line = 1
join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key


select * from fin.dim_Account where account in ('144600','164700','164600')

-- G43223P
select c.account, c.description, c.account_type, a.amount, a.control, a.doc, a.ref, d.description, sum(a.amount) over (partition by c.account_type)
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 202207
join fin.dim_Account c on a.account_key = c.account_key
  and c.account in ('144600','164700','164600')
join fin.dim_gl_description d on a.gl_Description_key = d.gl_description_key 
 
-- here are the ros
select unnest(doc) as ro
from (
select a.control, array_agg(a.doc) as doc, array_agg(a.ref) as ref, array_agg(distinct b.journal_code) as journal, 
	array_agg(distinct c.account) as account, sum(a.amount) as body_recon
from fin.fact_gl a 
join fin.dim_journal b on a.journal_key = b.journal_key
  and b.journal_code in ('SVI','SWA','SCA','POT')
join fin.dim_account c on a.account_key = c.account_key
where a.post_Status = 'Y'
  and control = 'G43223P'
group by a.control) a


drop table if exists cost_accounts cascade;
create temp table cost_accounts as
select distinct d.account, 
  case 
    when description like '%recon%' then 'recon'
    else 'cogs'
  end as account_type
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 202207
  and b.page = 16 
  and b.line between 1 and 11
join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
join fin.dim_account d on c.gl_account = d.account
  and d.current_row
  and d.account_type = 'cogs';

-------------------------------------------------------------------------------
--/> accounting
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--< recon estimates
-------------------------------------------------------------------------------
query based on ads PROCEDURE GetCurrentReconItemsForVehicleInventoryItem

-- this works for a single viiid
select sum(recon_est) as recon_est
from (
-- open items
  SELECT coalesce(sum(vri.TotalPartsAmount + vri.LaborAmount), 0) as recon_est
  FROM ads.ext_vehicle_recon_items vri
  INNER JOIN ads.ext_Authorized_Recon_Items ari ON ari.VehicleReconItemID = vri.VehicleReconItemID
    AND ari.CompleteTS IS null
    AND ari.Status <> 'AuthorizedReconItem_Complete'
    AND ari.ReconAuthorizationID = (
      SELECT ra.ReconAuthorizationID 
	    FROM ads.ext_Recon_Authorizations ra 
	    WHERE ra.VehicleInventoryItemID = 'bc82f13b-d00f-4680-a671-3c196a9037d3'
	      AND ra.ThruTS IS null)
union
-- completed items
  SELECT coalesce(sum(vri.TotalPartsAmount + vri.LaborAmount), 0) as recon_est
  FROM ads.ext_Vehicle_Recon_Items vri
  INNER JOIN ads.ext_Authorized_Recon_Items ari ON ari.VehicleReconItemID = vri.VehicleReconItemID
    AND ari.Status = 'AuthorizedReconItem_Complete'
  WHERE vri.VehicleInventoryItemID = 'bc82f13b-d00f-4680-a671-3c196a9037d3') aa

-- this works for all of them
select stocknumber, sum(recon_est) as recon_est
from (
-- open items
  SELECT vii.stocknumber, coalesce(sum(vri.TotalPartsAmount + vri.LaborAmount), 0) as recon_est
  FROM ads.ext_vehicle_recon_items vri
  join ads.ext_vehicle_inventory_items vii on vri.vehicleinventoryitemid = vii.vehicleinventoryitemid
    and vii.thruts > now()
  INNER JOIN ads.ext_Authorized_Recon_Items ari ON ari.VehicleReconItemID = vri.VehicleReconItemID
    AND ari.CompleteTS IS null
    AND ari.Status <> 'AuthorizedReconItem_Complete'
    AND ari.ReconAuthorizationID = (
      SELECT ra.ReconAuthorizationID 
	    FROM ads.ext_Recon_Authorizations ra 
	    WHERE ra.VehicleInventoryItemID = vii.vehicleinventoryitemid -- 'bc82f13b-d00f-4680-a671-3c196a9037d3'
	      AND ra.ThruTS IS null)
	group by vii.stocknumber
	having coalesce(sum(vri.TotalPartsAmount + vri.LaborAmount), 0) <> 0
union
-- completed items
  SELECT vii.stocknumber, coalesce(sum(vri.TotalPartsAmount + vri.LaborAmount), 0) as recon_est
  FROM ads.ext_Vehicle_Recon_Items vri
  join ads.ext_vehicle_inventory_items vii on vri.vehicleinventoryitemid = vii.vehicleinventoryitemid
    and vii.thruts > now()
  INNER JOIN ads.ext_Authorized_Recon_Items ari ON ari.VehicleReconItemID = vri.VehicleReconItemID
    AND ari.Status = 'AuthorizedReconItem_Complete'
  group by vii.stocknumber
	having coalesce(sum(vri.TotalPartsAmount + vri.LaborAmount), 0) <> 0) a
group by stocknumber	








-- TODO  
-- any g units in current priced, nope
-- the ads function accounts for those, i am not going to do it yet
select stock_number, right(trim(stock_number), 1) from current_priced order by  right(trim(stock_number), 1) 
select * from ads.ext_recon_authorizations  order by thruts desc nulls last limit 1000 
select distinct status from ads.ext_authorized_Recon_items
select * from ads.ext_authorized_Recon_items where completets is null limit 1000

SELECT aa.stock_number, a.vehicleinventoryitemid,
	a.description, a.totalpartsamount as parts, a.laboramount as labor,
	totalpartsamount + laboramount as line_total,
	sum(totalpartsamount + laboramount) over (partition by a.vehicleinventoryitemid) as vehicle_total
FROM ads.ext_Vehicle_Recon_Items a
join current_priced aa on a.vehicleinventoryitemid = aa.vehicleinventoryitemid
JOIN ads.ext_Authorized_Recon_Items b ON a.VehicleReconItemID = b.VehicleReconItemID
	AND b.Status = 'AuthorizedReconItem_Complete'
WHERE a.typ like 'body%';

G43372A doubling on appearance and body
select a.vehicleinventoryitemid, a.stock_number, b.vehiclereconitemid,  b.typ, b.description, sum(b.totalpartsamount + b.laboramount)
from current_priced a
join ads.ext_Vehicle_recon_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
join ads.ext_authorized_Recon_items c on b.vehiclereconitemid = c.vehiclereconitemid
  and c.typ <> 'AuthorizedReconItem_Removed'
group by a.vehicleinventoryitemid, a.stock_number, b.vehiclereconitemid, b.typ, b.description
order by stock_number
limit 100

select * from ads.ext_authorized_recon_items where vehiclereconitemid = 'b60087f0-9115-0c40-8fca-f94d01f7fb33'
select distinct typ from ads.ext_authorized_recon_items

select *
from ads.ext_vehicle_recon_items
-------------------------------------------------------------------------------
--/> recon estimates
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--< misc
-------------------------------------------------------------------------------

CREATE TABLE STATUSDESCRIPTIONS ( 
      FromTS TimeStamp,
      ThruTS TimeStamp,
      Description CIChar( 50 ),
      Status Char( 60 ),
      Sequence Integer) IN DATABASE;

drop table if exists ads.ext_status_descriptions cascade;
create table ads.ext_status_descriptions (
  fromts timestamp,
  thruts timestamp,
  description citext not null,
  status citext not null,
  sequence integer);

select * from ads.ext_status_descriptions 

create index on ads.ext_Vehicle_evaluations(vehicleitemid);
create index on ads.ext_Vehicle_evaluations(locationid);
create index on ads.ext_Vehicle_evaluations(salesconsultantid);
create index on ads.ext_Vehicle_evaluations(vehicleevaluatorid);
create index on ads.ext_Vehicle_evaluations(vehicleinventoryitemid);
create index on ads.ext_Vehicle_evaluations(typ);
create index on ads.ext_Vehicle_evaluations(typ);

create index on ads.ext_authorized_recon_items(status);
create index on ads.ext_authorized_recon_items(completets);
create index on ads.ext_authorized_recon_items(reconauthorizationid);

create index on ads.ext_Recon_Authorizations(vehicleinventoryitemid);
create index on ads.ext_Recon_Authorizations(reconauthorizationid);
create index on ads.ext_Recon_Authorizations(thruts);

create index on ads.ext_Vehicle_Recon_Items(status);
-------------------------------------------------------------------------------
--< misc
-------------------------------------------------------------------------------      

-------------------------------------------------------------------------------
--< current recon status
-------------------------------------------------------------------------------
-- based on ads PROCEDURE GetCurrentReconStatusByVehicleInventoryItemID
'27a0bcf9-1ac7-44c3-9f77-6123d9cb8c87'

create or replace function ads.get_current_recon_status_by_vehicleinventoryitemid(_vehicle_inventory_itemi_id citext)
returns table (mechanical citext, body citext, appearance citext)
as 
$BODY$
/*
select * from ads.get_current_recon_status_by_vehicleinventoryitemid('7573e233-91b8-4257-ac02-b672fda73df0')
*/
SELECT 
--   CASE WHEN ra.reconauthorizationid IS NULL THEN ''::citext 
--     ELSE 
      CASE (SELECT viis1.Status
            FROM ads.ext_Vehicle_Inventory_Item_Statuses viis1 
            WHERE viis1.VehicleInventoryItemID = vii.VehicleINventoryItemID 
            AND viis1.Category = 'MechanicalReconProcess'
            AND viis1.ThruTS > now())
        WHEN 'MechanicalReconProcess_NotStarted' THEN 'Not Started'::citext
        WHEN 'MechanicalReconProcess_InProcess' THEN 'In Process'::citext
        WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Incomplete'::citext 
      ELSE 'WTF'::citext
      END as mechanical,
--   END AS Mechanical,
--   CASE WHEN ra.reconauthorizationid IS NULL THEN ''::citext 
--     ELSE 
      CASE (SELECT viis1.Status
            FROM ads.ext_Vehicle_Inventory_Item_Statuses viis1 
            WHERE viis1.VehicleInventoryItemID = vii.VehicleINventoryItemID 
            AND viis1.Category = 'BodyReconProcess'
            AND viis1.ThruTS > now())
        WHEN 'BodyReconProcess_NotStarted' THEN 'Not Started'::citext
        WHEN 'BodyReconProcess_InProcess' THEN 'In Process'::citext
        WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'No Incomplete'::citext 
      ELSE 'WTF'::citext
      END as body,
--   END AS Body,
--   CASE WHEN ra.reconauthorizationid IS NULL THEN ''::citext 
--     ELSE 
      CASE (SELECT viis1.Status
            FROM ads.ext_Vehicle_Inventory_Item_Statuses viis1  
            WHERE viis1.VehicleInventoryItemID = vii.VehicleINventoryItemID 
            AND viis1.Category = 'AppearanceReconProcess'
            AND viis1.ThruTS > now())
        WHEN 'AppearanceReconProcess_NotStarted' THEN 'Not Started'::citext
        WHEN 'AppearanceReconProcess_InProcess' THEN 'InProcess'::citext
        WHEN 'AppearanceReconProcess_NoIncompleteReconItems' THEN 'No Incomplete'::citext 
      ELSE 'WTF'::citext
      END as appearance
--   END AS Appearance
FROM ads.ext_Vehicle_Inventory_Items vii
LEFT OUTER JOIN ads.ext_Recon_Authorizations ra ON ra.VehicleInventoryItemID = vii.vehicleinventoryitemid
WHERE vii.VehicleInventoryItemID = _vehicle_inventory_itemi_id
	AND vii.thruts > now()
	AND ra.ThruTS is null;  
$BODY$	
language sql;	

select * from ads.ext_vehicle_inventory_items where vehicleinventoryitemid = '27a0bcf9-1ac7-44c3-9f77-6123d9cb8c87'
select * from ads.ext_Recon_Authorizations where vehicleinventoryitemid = '27a0bcf9-1ac7-44c3-9f77-6123d9cb8c87'
select * from ads.ext_vehicle_inventory_item_statuses where coalesce(thruts, now()) > now() limit 100

-- this looks like a better function
create or replace function ads.get_current_recon_statusd(_vehicle_inventory_itemi_id citext)
yep, way faster, dont need a function just join to this query
    SELECT viix.vehicleinventoryitemid,
      CASE (
          SELECT status
          FROM ads.ext_Vehicle_Inventory_Item_Statuses 
          WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
          AND category = 'MechanicalReconProcess'
          AND ThruTS > now())
        WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
        WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
        WHEN 'MechanicalReconProcess_NotStarted' THEN 
          CASE 
            WHEN (
              SELECT COUNT(VehicleInventoryItemID)
              FROM ads.ext_Vehicle_Inventory_Item_Statuses
              WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
              AND category = 'MechanicalReconDispatched'
              AND ThruTS  > now()) = 0 THEN 'Not Started'
            ELSE
              'Dispatched'
            END
		ELSE 'None' 
      END AS mechanical,
      CASE (
          SELECT status
          FROM ads.ext_Vehicle_Inventory_Item_Statuses 
          WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
          AND category = 'BodyReconProcess'
          AND ThruTS  > now())
        WHEN 'BodyReconProcess_InProcess' THEN 'WIP'
        WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
        WHEN 'BodyReconProcess_NotStarted' THEN 
          CASE 
            WHEN (
              SELECT COUNT(VehicleInventoryItemID)
              FROM ads.ext_Vehicle_Inventory_Item_Statuses
              WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
              AND category = 'BodyReconDispatched'
              AND ThruTS  > now()) = 0 THEN 'Not Started'
            ELSE
              'Dispatched'
            END
	    ELSE 'None' 
      END AS body,
      CASE (
          SELECT status
          FROM ads.ext_Vehicle_Inventory_Item_Statuses 
          WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
          AND category = 'AppearanceReconProcess'
          AND ThruTS  > now())
        WHEN 'AppearanceReconProcess_InProcess' THEN 'WIP'
        WHEN 'AppearanceReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
        WHEN 'AppearanceReconProcess_NotStarted' THEN 
          CASE 
            WHEN (
              SELECT COUNT(VehicleInventoryItemID)
              FROM ads.ext_Vehicle_Inventory_Item_Statuses
              WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
              AND category = 'AppearanceReconDispatched'
              AND ThruTS  > now()) = 0 THEN 'Not Started'
            ELSE
              'Dispatched'
            END
	    ELSE 'None' 
      END AS appearance    
    FROM ads.ext_Vehicle_Inventory_Items viix
    where viix.thruts > now()
-------------------------------------------------------------------------------
--/> current recon status
-------------------------------------------------------------------------------