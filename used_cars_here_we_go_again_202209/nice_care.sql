﻿select * 
from arkona.ext_inpcmnt
where comment like '%contract%'
limit 100
  and arkona.db2_integer_to_date(transaction_date) > '12/31/2021'

create index on arkona.ext_inpcmnt(vin);
create index on arkona.ext_inpcmnt(comment);

select a.thruts::date, a.stocknumber, b.vin, bopname_search_name as customer, c.comment 
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
join arkona.ext_inpcmnt c on b.vin = c.vin  
  and comment like '%nice%'
join arkona.ext_bopmast d on a.stocknumber = d.bopmast_stock_number
join arkona.ext_bopname e on d.bopmast_company_number = e.bopname_company_number
  and d.buyer_number = e.bopname_record_key
  and e.company_individ is not null
where a.thruts::date > '12/31/2021'  
order by a.thruts::date desc


with nice_care as (select a.thruts::date, a.stocknumber, b.vin, bopname_search_name as customer, c.comment
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
join arkona.ext_inpcmnt c on b.vin = c.vin
  and comment like '%nice%'
join arkona.ext_bopmast d on a.stocknumber = d.bopmast_stock_number
join arkona.ext_bopname e on d.bopmast_company_number = e.bopname_company_number
  and d.buyer_number = e.bopname_record_key
  and e.company_individ is not null
where a.thruts::date > '12/31/2021'
order by a.thruts::date desc ) ,
boards_nice_care as (
select distinct stock_number, boarded_date
from board.sales_board a
where board_type_key = 5
    and boarded_date > '12/31/2021'
    and is_deleted = false
    and is_backed_on = false
    and used_vehicle_package = 'Nice Care')

select *
from boards_nice_care a
left join nice_care b on a.stock_number = b.stocknumber
where b.stocknumber is null