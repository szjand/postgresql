﻿drop table if exists tem.taylor_hn_sale_history;
create table tem.taylor_hn_sale_history as
select c.year_month, a.stock_number, a.vin, a.delivery_date, a.ground_date, b.model_year, 
  b.make, b.model, b.model_code, b.trim_level, b.drive, b.engine, b.color,
  a.delivery_date - ground_date as turn, d.list_price as msrp
from nc.vehicle_sales a 
join nc.vehicles b on a.vin = b.vin
join dds.dim_date c on a.delivery_date = c.the_date
  and c.the_year > 2017
left join arkona.ext_inpmast d on a.vin = d.inpmast_vin  
where a.sale_type = 'retail'
  and b.make in ('honda','nissan')
order by delivery_date;


-- -- 34 2017 vehicles sold in 2018, not in vehcle/acq/sale structure, 
-- -- leave them out for now
-- select a.bopmast_stock_number, a.bopmast_vin, a.date_capped, b.make, b.year, c.vin, d.vin
-- from arkona.ext_bopmast a 
-- join arkona.ext_inpmast b on a.bopmast_vin = b.inpmast_vin
--   and b.make in ('honda','nissan')
--   and b.year = 2017
-- left join chr.describe_vehicle c on a.bopmast_vin = c.vin  
-- left join nc.vehicles d on c.vin = d.vin
-- where sale_type = 'R'
--   and vehicle_type = 'N'
--   and record_status = 'U'
--   and a.date_capped > '12/31/2017'

-- including msrp is too fine grained, numbers too small

-- taylor_honda_sales_history_agg_1
select year_month, model_year, make, model, trim_level, color,
  avg(turn)::integer as avg_turn, min(msrp::integer) as min_msrp, max(msrp::integer) as max_msrp, count(*)
from tem.taylor_hn_sale_history
group by year_month, model_year, make, model, trim_level, color
order by make, count(*) desc

-- 11/16/20
talked to taylor, she is having a tough time articulating what she needs
any further than by make, by year

so lets do a an iterated drill down starting by year

select * from tem.taylor_hn_sale_history limit 10

-- 1 by make andd sale year
select extract(year from delivery_date) as sale_year, make, count(*)
from tem.taylor_hn_sale_history
group by extract(year from delivery_date), make
order by make, extract(year from delivery_date)

-- add model_year, this just looks like noise to me
select extract(year from delivery_date) as sale_year, make, model_year, count(*)
from tem.taylor_hn_sale_history
group by extract(year from delivery_date), make, model_year
order by make, extract(year from delivery_date), model_year

-- removel model_year, add model
-- generate model count
-- don't remember why i excluded fit and titan xd ...
drop table if exists tem.taylor_hn_models cascade;
create table tem.taylor_hn_models as
select extract(year from delivery_date) as sale_year, make, model, count(*) as model_count
from tem.taylor_hn_sale_history
where model not in ('fit','titan xd')
group by extract(year from delivery_date), make, model
having count(*) > 5
order by make, model, extract(year from delivery_date);


select *
from (
  select a.*, b.trim_level, b.trim_count
  from tem.taylor_hn_models a
  left join (
    select extract(year from delivery_date) as sale_year, make, model, trim_level, count(*) as trim_count
    from tem.taylor_hn_sale_history
    group by extract(year from delivery_date), make, model, trim_level) b on a.sale_year = b.sale_year
      and a.make = b.make
      and a.model = b.model
  where a.sale_year = 2018    
  order by a.sale_year, a.make, a.model, b.trim_count desc) aa
full outer join (
  select a.*, b.trim_level, b.trim_count
  from tem.taylor_hn_models a
  left join (
    select extract(year from delivery_date) as sale_year, make, model, trim_level, count(*) as trim_count
    from tem.taylor_hn_sale_history
    group by extract(year from delivery_date), make, model, trim_level) b on a.sale_year = b.sale_year
      and a.make = b.make
      and a.model = b.model
  where a.sale_year = 2019   
  order by a.sale_year, a.make, a.model, b.trim_count desc) bb on aa.make = bb.make and aa.model = bb.model and aa.trim_level = bb.trim_level
-- order by coalesce(aa.make || aa.model || aa.trim_level, bb.make || bb.model || bb.trim_level)  -- this looks good for 2018/2019
order by coalesce(aa.make, bb.make), coalesce(aa.model, bb.model), coalesce(aa.trim_level, bb.trim_level)-- this also good for 2018/2019, this is from vehicle_categorization

-- now add 2020
-- set to taylor in a spreadsheet with some formatting
select *
from ( -- 2018
  select a.*, b.trim_level, b.trim_count
  from tem.taylor_hn_models a
  left join (
    select extract(year from delivery_date) as sale_year, make, model, trim_level, count(*) as trim_count
    from tem.taylor_hn_sale_history
    group by extract(year from delivery_date), make, model, trim_level) b on a.sale_year = b.sale_year
      and a.make = b.make
      and a.model = b.model
  where a.sale_year = 2018    
  order by a.sale_year, a.make, a.model, b.trim_count desc) aa
full outer join ( -- 2019
  select a.*, b.trim_level, b.trim_count
  from tem.taylor_hn_models a
  left join (
    select extract(year from delivery_date) as sale_year, make, model, trim_level, count(*) as trim_count
    from tem.taylor_hn_sale_history
    group by extract(year from delivery_date), make, model, trim_level) b on a.sale_year = b.sale_year
      and a.make = b.make
      and a.model = b.model
  where a.sale_year = 2019   
  order by a.sale_year, a.make, a.model, b.trim_count desc) bb on aa.make = bb.make and aa.model = bb.model and aa.trim_level = bb.trim_level
full outer join ( -- 2020
  select a.*, b.trim_level, b.trim_count
  from tem.taylor_hn_models a
  left join (
    select extract(year from delivery_date) as sale_year, make, model, trim_level, count(*) as trim_count
    from tem.taylor_hn_sale_history
    group by extract(year from delivery_date), make, model, trim_level) b on a.sale_year = b.sale_year
      and a.make = b.make
      and a.model = b.model
  where a.sale_year = 2020   
  order by a.sale_year, a.make, a.model, b.trim_count desc) cc on coalesce(aa.make,bb.make) = cc.make
    and coalesce(aa.model, bb.model) = cc.model 
    and coalesce(aa.trim_level, bb.trim_level) = cc.trim_level 
order by coalesce(aa.make, bb.make, cc.make), coalesce(aa.model, bb.model, cc.model), coalesce(aa.trim_level, bb.trim_level, cc.trim_level)   

/*
11/24/20
Hey Jon,
Could I ask that drivetrain be added to the list for these? 
I added some colors and removed some redundant numbers from mine if you could plug it into there but no biggy 
if it’s more work and you just send what you had sent before. 
Taylor Monson

*/

-- is she thinking a separate column like trim?

  select a.*, b.trim_level, b.trim_count
  from tem.taylor_hn_models a
  left join (
    select extract(year from delivery_date) as sale_year, make, model, trim_level, count(*) as trim_count
    from tem.taylor_hn_sale_history
    group by extract(year from delivery_date), make, model, trim_level) b on a.sale_year = b.sale_year
      and a.make = b.make
      and a.model = b.model
  where a.sale_year = 2019   

-- ok, drive is in the base table
select *
from tem.taylor_hn_sale_history
limit 10


-- exported this to taylor_honda_sales_history_add_drive.csv
-- then copy pasted the values into taylors version of the spreadsheet
-- sent her taylor_honda_sales_history_V3.xlsx
    select extract(year from delivery_date) as sale_year, make, model, trim_level,
    '="' || coalesce((count(*) filter (where drive = 'FWD'))::text, '0') || ' / ' ||  coalesce((count(*) filter (where drive in ('4WD','AWD')))::text, '0') || '"'
    from tem.taylor_hn_sale_history
    group by extract(year from delivery_date), make, model, trim_level
    order by make, extract(year from delivery_date), model, trim_level

    
  