﻿/*
01/05/21
nissan has changed the table structure, added 2 fields campaign_recall, ppo.
need to restructure the 2 vehicle_status tables
*/
drop table if exists hn.ext_hin_vehicle_statuses cascade;
create table hn.ext_hin_vehicle_statuses (
  status citext,
  model_code citext,                                                       
  trim_level citext,                                                      
  transmission citext,                                                     
  emission citext,                                                       
  option_code citext,                                                       
  exterior_interior_color_code citext,                                                      
  source_warehouse citext,                                                      
  vin citext primary key,                                                      
  stop_sale citext,   
  campaign_recall citext,                                                   
  order_reference_type citext,      
  ppo citext,                                                
  from_dealer citext,                                                       
  vehicle_assign_date citext,                                                     
  shipping_date citext,                                                      
  shipping_location citext,                                                      
  shipping_eta citext,                                                       
  received_date citext,                                                       
  demo_date citext,                                                       
  demo_special citext,                                                       
  wholesale citext,                                                       
  inventory citext,                                                      
  security_code citext,                                                       
  stock_number citext,                                                       
  presold citext,                                                        
  customer citext);

comment on table hn.ext_hin_vehicle_statuses is 'raw extract from honda interactive network, truncated and popluated daily. 
  to preserve leading 0''s in the MMDD fields, csv had to enable quoting all, therefore integer fields needed to be citext';
comment on column hn.ext_hin_vehicle_statuses.vehicle_assign_date is 'date in MMDD format';
comment on column hn.ext_hin_vehicle_statuses.shipping_date is 'date shipped from port to dealership in MMDD format';
comment on column hn.ext_hin_vehicle_statuses.shipping_location is 'last known location of in transit vehicle, state codes';
comment on column hn.ext_hin_vehicle_statuses.shipping_eta is 'estimated date of arrival at dealership in MMDD format';
comment on column hn.ext_hin_vehicle_statuses.demo_date is 'date vehicle is placed in demo status';
comment on column hn.ext_hin_vehicle_statuses.demo_special is 'is vehicle a special demo';
comment on column hn.ext_hin_vehicle_statuses.wholesale is 'number of days since the first dealer received the vehicle';
comment on column hn.ext_hin_vehicle_statuses.inventory is 'number of days in the current dealers inventory';
comment on column hn.ext_hin_vehicle_statuses.presold is 'is the vehicle presold';
comment on column hn.ext_hin_vehicle_statuses.customer is 'used to identify a specific customer name, vehicle stock number, or other comment';


/*
01/05/2020
on this table need to keep the data before restructuring, then re-insert it
*/

drop table if exists honda_vehicle_statuses cascade;
create temp table honda_vehicle_statuses as
select * from hn.hin_vehicle_statuses;


drop table if exists hn.hin_vehicle_statuses cascade;
create table hn.hin_vehicle_statuses (
  status citext,
  model_code citext,                                                       
  trim_level citext,                                                      
  transmission citext,                                                     
  emission citext,                                                       
  option_code citext,                                                       
  exterior_color_code citext, 
  interior_color_code citext,                                                    
  source_warehouse citext,                                                      
  vin citext primary key,                                                      
  stop_sale citext,                                                      
  campaign_recall citext,                                                   
  order_reference_type citext,      
  ppo citext,                                                       
  from_dealer citext,                                                       
  vehicle_assign_date citext,                                                     
  shipping_date citext,                                                      
  shipping_location citext,                                                      
  shipping_eta citext,                                                       
  received_date citext,                                                       
  demo_date citext,                                                       
  demo_special citext,                                                       
  wholesale citext,                                                       
  inventory citext,                                                      
  security_code citext,                                                       
  stock_number citext,                                                       
  presold citext,                                                        
  customer citext);

comment on table hn.hin_vehicle_statuses is 'populated nightly from hn.ext_hin_vehicle_statuse, the only structrual 
  differenc, broke out exterior & interior color codes into separate fields, and wholesale and inventory dates
  are converted to integers. new rows (vin) are inserted, existing rows are updated in such a was as to preserve 
  date data that in hin gets removed, eg, vehicle_assignment_date, shipping_eta, etc';
comment on column hn.hin_vehicle_statuses.vehicle_assign_date is 'date in MMDD format';
comment on column hn.hin_vehicle_statuses.shipping_date is 'date shipped from port to dealership in MMDD format';
comment on column hn.hin_vehicle_statuses.shipping_location is 'last known location of in transit vehicle, state codes';
comment on column hn.hin_vehicle_statuses.shipping_eta is 'estimated date of arrival at dealership in MMDD format';
comment on column hn.hin_vehicle_statuses.demo_date is 'date vehicle is placed in demo status';
comment on column hn.hin_vehicle_statuses.demo_special is 'is vehicle a special demo';
comment on column hn.hin_vehicle_statuses.wholesale is 'number of days since the first dealer received the vehicle';
comment on column hn.hin_vehicle_statuses.inventory is 'number of days in the current dealers inventory';
comment on column hn.hin_vehicle_statuses.presold is 'is the vehicle presold';
comment on column hn.hin_vehicle_statuses.customer is 'used to identify a specific customer name, vehicle stock number, or other comment';

insert into hn.hin_vehicle_statuses (status,model_code,trim_level,transmission,emission,option_code,
  exterior_color_code,interior_color_code,source_warehouse,vin,stop_sale,campaign_recall,order_reference_type,
  ppo,from_dealer,vehicle_assign_date,shipping_date,shipping_location,shipping_eta,received_date,
  demo_date,demo_special,wholesale,inventory,security_code,stock_number,presold,customer)
select status,model_code,trim_level,transmission,emission,option_code,
  exterior_color_code,interior_color_code,source_warehouse,vin,stop_sale,null as campaign_recall,order_reference_type,
  null as ppo,from_dealer,vehicle_assign_date,shipping_date,shipping_location,shipping_eta,received_date,
  demo_date,demo_special,wholesale,inventory,security_code,stock_number,presold,customer  
from honda_vehicle_statuses;


select * from hn.hin_vehicle_statuses
select * from hn.hin_vehicle_invoices
select * from hn.hin_vehicle_incentives



when i dropped cascade hin.vehicle_statuses, a couple of unintended side effects:  
NOTICE:  drop cascades to 2 other objects
DETAIL:  drop cascades to constraint hin_vehicle_invoices_vin_fkey on table hn.hin_vehicle_invoices
drop cascades to constraint hin_vehicle_incentives_vin_fkey on table hn.hin_vehicle_incentives
                   




select *
from hn.ext_hin_vehicle_statuses order by status
-- are the values actually null
select * from hn.ext_hin_vehicle_statuses where vehicle_assign_date is null
-- nope, due to the quoting, the empty attributes are an empty string

what fields to not update:
  vehicle_assign_date
  shipping_date
  shipping_location
  shipping_eta

-- query to get column names
select string_agg('excluded.' || column_name,',')
from information_schema.columns
where table_name = 'hin_vehicle_statuses' -- enter table name here
  and table_schema= 'hn'  
  
-- first feed
-- replace empty strings with null
insert into hn.hin_vehicle_statuses
select status,model_code,trim_level,transmission,emission,
  nullif(option_code, '') as option_code,
  trim(left(exterior_interior_color_code, position('/' in exterior_interior_color_code) - 1)) as exterior_color_code,
  trim(substring(exterior_interior_color_code, position('/' in exterior_interior_color_code) + 1)) as interior_color_code,
  nullif(source_warehouse, '') as source_warehouse, vin,
  nullif(stop_sale, '') as stop_sale,
  order_reference_type,
  nullif(from_dealer, '') as from_dealer, nullif(vehicle_assign_date, '') as vehicle_assign_date,
  nullif(shipping_date, '') as shipping_date, nullif(shipping_location, '') as shipping_location,
  nullif(shipping_eta, '') as shipping_eta, received_date,
  demo_date,demo_special,
  nullif(wholesale, '') as wholesale, nullif(inventory, '') as inventory,
  security_code, stock_number,presold,customer
from hn.ext_hin_vehicle_statuses;



CREATE OR REPLACE FUNCTION hn.update_hin_vehicle_statuses()
  RETURNS void AS
$BODY$
/*
*/

insert into hn.hin_vehicle_statuses
select status,model_code,trim_level,transmission,emission,
  nullif(option_code, '') as option_code,
  trim(left(exterior_interior_color_code, position('/' in exterior_interior_color_code) - 1)) as exterior_color_code,
  trim(substring(exterior_interior_color_code, position('/' in exterior_interior_color_code) + 1)) as interior_color_code,
  nullif(source_warehouse, '') as source_warehouse, vin,
  nullif(stop_sale, '') as stop_sale,
  order_reference_type,
  nullif(from_dealer, '') as from_dealer, nullif(vehicle_assign_date, '') as vehicle_assign_date,
  nullif(shipping_date, '') as shipping_date, nullif(shipping_location, '') as shipping_location,
  nullif(shipping_eta, '') as shipping_eta, received_date,
  demo_date,demo_special,
  nullif(wholesale, '') as wholesale, nullif(inventory, '') as inventory,
  security_code, stock_number,presold,customer
from hn.ext_hin_vehicle_statuses
on conflict (vin)
do update 
  set (
    status,model_code,trim_level,transmission,emission,option_code,exterior_color_code,
    interior_color_code,source_warehouse,stop_sale,order_reference_type,from_dealer,
    vehicle_assign_date,shipping_date,shipping_location,shipping_eta,received_date,
    demo_date,demo_special,wholesale,inventory,security_code,stock_number,presold,customer)
  = (
    excluded.status,excluded.model_code,excluded.trim_level,excluded.transmission,
    excluded.emission,excluded.option_code,excluded.exterior_color_code,
    excluded.interior_color_code,excluded.source_warehouse,excluded.stop_sale,
    excluded.order_reference_type,excluded.from_dealer,
    coalesce(excluded.vehicle_assign_date, hin_vehicle_statuses.vehicle_assign_date),
    coalesce(excluded.shipping_date, hin_vehicle_statuses.shipping_date),
    coalesce(excluded.shipping_location, hin_vehicle_statuses.shipping_location),
    coalesce(excluded.shipping_eta, hin_vehicle_statuses.shipping_eta),
    excluded.received_date,excluded.demo_date,excluded.demo_special,
    excluded.wholesale,excluded.inventory,excluded.security_code,
    excluded.stock_number,excluded.presold,excluded.customer);
$BODY$
LANGUAGE sql;
comment on function hn.update_hin_vehicle_statuses() is 'nightly update, breaks out exterior and interior codes into separate attributes, 
  converts empty strings to nulls, persists values that hin routinely delete: shipping_date, shipping_eta, wholesale
  and inventory; thereby creating a rough timeline. inserts new rows (based on vin) and updates current rows.'



------------------------------------------------------------
invoices
invoices exist for statuses onhand and shipped
select vin from hn.hin_vehicle_statuses where status in ('onhand','shipped')

thinking from the invoice i want invoice number, invoice date, msrp and invoice total

drop table if exists hn.ext_hin_vehicle_invoices cascade;
create table hn.ext_hin_vehicle_invoices (
  vin citext,
  invoice_date citext,
  invoice_number citext, 
  msrp citext,
  total_invoice citext);

select * 
from hn.ext_hin_vehicle_invoices  

-- getting started, using stg table
drop table if exists hn.stg_hin_invoices cascade;
create table hn.stg_hin_invoices (
  vin citext primary key,
  invoice_date date,
  invoice_number citext,
  msrp numeric(8,2),
  total_invoice numeric(8,2));

insert into hn.stg_hin_invoices  
select trim(vin) as vin, invoice_date::date, trim(invoice_number) as invoice_number, 
  (replace(trim(left(split_part(msrp, '$', 2), position('      ' in msrp))), ',',''))::numeric as msrp,
  (replace(replace(total_invoice, '$',''),',',''))::numeric as total_invoice
from hn.ext_hin_invoices a
where not exists (
  select 1
  from hn.stg_hin_invoices 
  where vin = a.vin)






-- production, not going to worry about history or changes
drop table if exists hn.hin_vehicle_invoices cascade;
create table hn.hin_vehicle_invoices (
  vin citext primary key references hn.hin_vehicle_statuses(vin),
  invoice_date date,
  invoice_number citext,
  msrp numeric(8,2),
  total_invoice numeric(8,2));


CREATE OR REPLACE FUNCTION hn.hin_vehicle_invoices()
  RETURNS void AS
$BODY$
/*
*/
insert into hn.hin_vehicle_invoices  
select trim(vin) as vin, invoice_date::date, trim(invoice_number) as invoice_number, 
  (replace(trim(left(split_part(msrp, '$', 2), position('      ' in msrp))), ',',''))::numeric as msrp,
  (replace(replace(total_invoice, '$',''),',',''))::numeric as total_invoice
from hn.ext_hin_vehicle_invoices a
where not exists (
  select 1
  from hn.hin_vehicle_invoices 
  where vin = a.vin);
$BODY$
LANGUAGE sql;
comment on function hn.hin_vehicle_invoices() is 'cleans up hin.ext_vehicle_invoice data and inserts int into
  hn.hin_vehicle_invoices.  No history, no updating, initial assumption is that honda invoices don''t change';
  

drop table if exists hn.ext_hin_incentives;
create table hn.ext_hin_incentives (
  model citext,
  special_captive_lease citext,
  captive_lease_cash citext, 
  conjuction_1 citext,
  dealer_finance_cash citext,
  conjuction_2 citext,
  special_captive_apr citext);
  
select * from hn.ext_hin_incentives



-- 05/01 need to add additional fields to invoices

select *
from hn.ext_hin_vehicle_invoices

alter table hn.ext_hin_vehicle_invoices
add column base_invoice citext,
add column destination_charge citext;


select trim(vin) as vin, invoice_date::date, trim(invoice_number) as invoice_number, 
  (replace(trim(left(split_part(msrp, '$', 2), position('      ' in msrp))), ',',''))::numeric as msrp,
  (replace(replace(total_invoice, '$',''),',',''))::numeric as total_invoice,
  (replace(replace(base_invoice, '$',''),',',''))::numeric as base_invoice,
  (replace(replace(destination_charge, '$',''),',',''))::numeric as destination_charge,
--   trim(right(msrp, 8)) as hold_back,
--   length(trim(right(msrp, 8))),
  (left(trim(right(msrp, 8)), length(trim(right(msrp, 8))) - 2) || '.' || right(trim(right(msrp, 8)), 2))::numeric as hold_back
from hn.ext_hin_vehicle_invoices a


-- query to generate a scrape for the existing invoices
select vin 
from hn.hin_vehicle_invoices

something weird going on

select * from hn.ext_hin_vehicle_invoices

truncate hn.ext_hin_vehicle_invoices


alter table hn.hin_vehicle_invoices 
add column base_invoice numeric(8,2),
add column destination_charge numeric(8,2),
add column hold_back numeric(8,2);

update hn.hin_vehicle_invoices x
set base_invoice = y.base_invoice,
    destination_charge = y.destination_charge,
    hold_back = y.hold_back
from (    
  select a.vin, b.base_invoice, b.destination_charge, b.hold_back
  from hn.hin_vehicle_invoices a
  join (
    select trim(vin) as vin, invoice_date::date, trim(invoice_number) as invoice_number, 
      (replace(trim(left(split_part(msrp, '$', 2), position('      ' in msrp))), ',',''))::numeric as msrp,
      (replace(replace(total_invoice, '$',''),',',''))::numeric as total_invoice,
      (replace(replace(base_invoice, '$',''),',',''))::numeric as base_invoice,
      (replace(replace(destination_charge, '$',''),',',''))::numeric as destination_charge,
      (left(trim(right(msrp, 8)), length(trim(right(msrp, 8))) - 2) || '.' || right(trim(right(msrp, 8)), 2))::numeric as hold_back
    from hn.ext_hin_vehicle_invoices) b on a.vin = b.vin
  where a.base_invoice is null) y
where x.vin = y.vin  


select *
from hn.hin_vehicle_invoices a
where not exists (
  select 1
  from hn.hin_vehicle_statuses
  where vin = a.vin)

select vin
from hn.hin_vehicle_invoices
where base_invoice is null;  


select vin
from hn.hin_vehicle_statuses a
where status in ('onhand','shipped')
  and not exists (
    select 1
    from hn.hin_vehicle_invoices
    where vin = a.vin)


select *
from hn.hin_vehicle_invoices a
order by invoice_date desc 

delete from hn.hin_vehicle_invoices where vin = 'SHHFK7H8XLU208091'

CREATE OR REPLACE FUNCTION hn.hin_vehicle_invoices()
  RETURNS void AS
$BODY$
/*
  05/02: include base_invoice, destination_charge & hold_back
*/
insert into hn.hin_vehicle_invoices  
select trim(vin) as vin, invoice_date::date, trim(invoice_number) as invoice_number, 
  (replace(trim(left(split_part(msrp, '$', 2), position('      ' in msrp))), ',',''))::numeric as msrp,
  (replace(replace(total_invoice, '$',''),',',''))::numeric as total_invoice,
  (replace(replace(base_invoice, '$',''),',',''))::numeric as base_invoice,
  (replace(replace(destination_charge, '$',''),',',''))::numeric as destination_charge,
  (left(trim(right(msrp, 8)), length(trim(right(msrp, 8))) - 2) || '.' || right(trim(right(msrp, 8)), 2))::numeric as hold_back
from hn.ext_hin_vehicle_invoices a
where not exists (
  select 1
  from hn.hin_vehicle_invoices 
  where vin = a.vin);
$BODY$
LANGUAGE sql;
comment on function hn.hin_vehicle_invoices() is 'cleans up hin.ext_vehicle_invoice data and inserts int into
  hn.hin_vehicle_invoices.  No history, no updating, initial assumption is that honda invoices don''t change';