﻿-----------------------------------------------------------------------------
--< 09/09/21
-----------------------------------------------------------------------------
new incentives today
but since i so rarely have recourse to this script, i am going to move the most recent activities to the top of this file

-- insert the new program into hn.hin_incentive_bulletins
insert into hn.hin_incentive_bulletins
select row_number() over (partition by a.program_id order by line_number), a.*
from hn.hin_incentive_bulletins_ext a
where not exists (
  select 1
  from hn.hin_incentive_bulletins
  where program_id = a.program_id);

-----------------------------------------------------------------------------
--/> 09/09/21
-----------------------------------------------------------------------------


select * from hn.hin_incentives_check()

drop table if exists hn.hin_incentives_active;
create table hn.hin_incentives_active (
  program_id citext primary key,
  from_date date not null,
  thru_date date not null);
comment on table hn.hin_incentives_active is 'list of currently active honda incentives used for scraping the relevant bulletins';
insert into hn.hin_incentives_active values
('HP-Z04','09/01/2020','11/02/2020'),  
('HP-Z12','09/01/2020','09/08/2020'),
('HP-Z14','09/01/2020','09/08/2020'),
('HP-Z15','09/01/2020','09/08/2020'),
('HP-Z17','09/01/2020','11/02/2020'),
('HP-Z21','09/01/2020','11/02/2020'),
('HP-Z24','09/01/2020','11/02/2020'),
('HP-Z26','09/01/2020','11/02/2020'),
('HP-Z27','09/01/2020','11/02/2020'),
('HP-Z30','09/01/2020','09/08/2020'),
('HP-Y90','07/17/2020','09/08/2020'),
('HP-Y68','07/17/2020','09/08/2020'),
('HP-Y92','08/04/2020','09/08/2020'),
('HP-Z42','09/01/2020','09/08/2020'),
('HP-Z44','09/01/2020','09/08/2020'),
('HP-Z45','09/01/2020','09/08/2020'),
('HP-Z49','09/01/2020','11/02/2020'),
('HP-Z50','09/01/2020','11/02/2020'),
('HP-Z57','09/01/2020','11/02/2020'),
('HP-Z59','09/01/2020','11/30/2020');



select program_id
from hn.hin_incentives_active
where current_date between from_date and thru_date;


DROP TABLE hn.hp_z15 cascade;


/*
thought i would try creating a separate table for each progra, that would work but don't believe it is the best idea
instead, create a single table for all the extract
*/

drop table if exists hn.hin_incentive_bulletins_ext cascade;
create table hn.hin_incentive_bulletins_ext (
  program_id citext,
  line_number integer,
  the_line citext,
  the_date date,
  primary key(program_id,line_number,the_date));
comment on table hn.hin_incentive_bulletins_ext is 'daily scrape of honda incentive bulletins';



select program_id, line_number, the_line, regexp_replace(the_line, '[^[:ascii:]]', '"XXX"', 'g') 
from hn.incentive_programs_ext
where the_line SIMILAR TO '%[^[:ascii:]]%'


select *, aa.the_line = b.the_lines
from (
select row_number() over () , a.*
from hn.incentive_programs_ext a
where program_id = 'HP-Z30') aa
full outer join (
select row_number() over() as row_number, the_lines
from hn.incentive_test_1) b on aa.row_number = b.row_number






drop table if exists hn.hin_incentive_bulletins cascade;
create table hn.hin_incentive_bulletins (
  row_number integer,
  program_id citext,
  line_number integer,
  the_line citext,
  the_date date,
  primary key(program_id,line_number));
comment on table hn.hin_incentive_bulletins is 'the current version of honda incentive bulletins';



insert into hn.hin_incentive_bulletins
select row_number() over (partition by a.program_id order by line_number), a.*
from hn.hin_incentive_bulletins_ext a
join hn.hin_incentives_active b on a.program_id = b.program_id;

-- 09/08/2020
/*
not sure of the work flow
yesterday, hn.hin_incentive_bulletins was populated
today, populated hn.hin_incentive_bulletins_ext

now, what do we do
*/

select *
from (
  select a.program_id, a.line_number, a.the_line as old_line, b.the_line as new_line
  from (
    select *
    from hn.hin_incentive_bulletins
    order by program_id, line_number) a
  full outer join (
    select *
    from hn.hin_incentive_bulletins_ext
    order by program_id, line_number) b on a.program_id = b.program_id
      and a.line_number = b.line_number) c  
where trim(new_line) <> trim(old_line)
  and line_number not in (2,3)


select program_id, line_number, old_line, new_line
from (
  select a.program_id, a.line_number, a.the_line as old_line, b.the_line as new_line, old_line_hash, new_line_hash
  from (
    select *, md5(the_line) as old_line_hash
    from hn.hin_incentive_bulletins
    order by program_id, line_number) a
  full outer join (
    select *, md5(the_line) as new_line_hash
    from hn.hin_incentive_bulletins_ext
    order by program_id, line_number) b on a.program_id = b.program_id
      and a.line_number = b.line_number) c  
where new_line_hash <> old_line_hash
  and line_number not in (2,3)


09/09/20
some of the above are gone, some are changed:

delete from hn.hin_incentives_active
where program_id in ('HP-Z30','HP-Y90','HP-Y68','HP-Y92','HP-Z42','HP-Z44','HP-Z45');

insert into hn.hin_incentives_active values
('HP-Z32','09/09/2020','11/02/2020'),
('HP-Z33','09/09/2020','11/02/2020'),
('HP-Z34','09/09/2020','11/02/2020'),
('HP-Z38','09/09/2020','11/02/2020'),
('HP-Z43','09/09/2020','11/02/2020');

-- 09/09/2020
ok, got the current incentives in hn.hin_incentive_bulletins_ext
what do i do now ????

1. new programs
2. removed programs
3. changed programs

-- new programs
select distinct a.program_id
from hn.hin_incentive_bulletins_ext a
left join  hn.hin_incentive_bulletins b on a.program_id = b.program_id
where b.program_id is null

-- expired/missing programs
select distinct a.program_id
from hn.hin_incentive_bulletins a
left join  hn.hin_incentive_bulletins_ext b on a.program_id = b.program_id
where b.program_id is null

-- changes in existing programs
select program_id, line_number, old_line, new_line
from (
  select a.program_id, a.line_number, a.the_line as old_line, b.the_line as new_line, old_line_hash, new_line_hash
  from (
    select *, md5(the_line) as old_line_hash
    from hn.hin_incentive_bulletins
    order by program_id, line_number) a
  full outer join (
    select *, md5(the_line) as new_line_hash
    from hn.hin_incentive_bulletins_ext
    order by program_id, line_number) b on a.program_id = b.program_id
      and a.line_number = b.line_number) c  
where new_line_hash <> old_line_hash
  and line_number not in (2,3)


-- 09/10/2020
the only incentive selected by ry2 is HP-Z32

select program_id, line_number, old_line, new_line
from (
  select a.program_id, a.line_number, a.the_line as old_line, b.the_line as new_line, old_line_hash, new_line_hash
  from (
    select *, md5(the_line) as old_line_hash
    from hn.hin_incentive_bulletins
    order by program_id, line_number) a
  full outer join (
    select *, md5(the_line) as new_line_hash
    from hn.hin_incentive_bulletins_ext
    order by program_id, line_number) b on a.program_id = b.program_id
      and a.line_number = b.line_number) c  
where new_line_hash <> old_line_hash
  and line_number not in (2,3)

 damnit, i fucked up yesterday and did not update  hn.hin_incentive_bulletins with the values from hn.hin_incentive_bulletins_ext
 went ahead and ran honda_incentive_testbed, which of course deleted all the files from yesterday, and truncated hn.hin_incentive_bulletins_ext,
 so, dont know if if HP-Z32 changed or not
 so, the process needs to be updated to include updating hn.hin_incentive_bulletins

 need to map out the process

 lets clean up some unused tables
 
alter table hn.ext_hin_incentives
rename to z_unused_ext_hin_incentives;

alter table hn.ext_hin_vehicle_incentives
rename to z_unused_ext_hin_vehicle_incentives;

alter table hn.hin_incentives_active
rename to z_unused_ext_hin_incentives_active;

drop table hn.incentive_test_1 cascade;
drop table hn.incentive_test_1a cascade;
drop table hn.incentive_test_2 cascade;
drop table hn.incentive_test_2a cascade;

it looks like afton is using hn.hin_incentives as the table to house the currently selected incentives

need a table to expose changed incentives

drop table if exists hn.hin_incentive_changes;
create table hn.hin_incentive_changes (
  program_id citext not null references hn.hin_incentives(program_id),
  the_date date not null,
  acknowledged boolean not null default false,
  primary key (program_id,the_date));

select * from hn.hin_incentive_changes

-- 09/11


                        select program_id
                        from hn.hin_incentives
                        where thru_date >= current_date

                        

select * from hn.hin_incentives

selecT * from hn.hin_incentive_bulletins where the_date = current_date order by program_id, line_number

selecT * from hn.hin_incentive_bulletins_ext

from yesterday, the only incentive in play is HP-Z32

select *
truncate hn.hin_incentive_bulletins;

insert into hn.hin_incentive_bulletins
select row_number() over (partition by a.program_id order by line_number), a.*
from hn.hin_incentive_bulletins_ext a;

now it is ok to run the program using this query:
    select program_id
    from hn.hin_incentives;
which, today, returns only HP-Z32    

and today, no changes.
-- changes in existing programs
select program_id, line_number, old_line, new_line
from (
  select a.program_id, a.line_number, a.the_line as old_line, b.the_line as new_line, old_line_hash, new_line_hash
  from (
    select *, md5(the_line) as old_line_hash
    from hn.hin_incentive_bulletins
    order by program_id, line_number) a
  full outer join (
    select *, md5(the_line) as new_line_hash
    from hn.hin_incentive_bulletins_ext
    order by program_id, line_number) b on a.program_id = b.program_id
      and a.line_number = b.line_number) c  
where new_line_hash <> old_line_hash
  and line_number not in (2,3)

                        
---------------------------------------------------------------------------------
--< 10/26/20  new incentive, 7/7/21
---------------------------------------------------------------------------------
-- insert the new program into hn.hin_incentive_bulletins
insert into hn.hin_incentive_bulletins
select row_number() over (partition by a.program_id order by line_number), a.*
from hn.hin_incentive_bulletins_ext a
where not exists (
  select 1
  from hn.hin_incentive_bulletins
  where program_id = a.program_id);

select * from hn.hin_incentive_bulletins order by program_id, line_number

delete from hn.hin_incentive_bulletins_ext where program_id = 'HP-53A'
---------------------------------------------------------------------------------
--/> 10/26/20  new incentive
---------------------------------------------------------------------------------

---------------------------------------------------------------------------------
--< 11/19/2020 non superficial incentive change
---------------------------------------------------------------------------------
HP-53A changed, with a re-issue date and an additional clause under vehicles
what i forgot was, after updating hn.hin_incentive_bulletins, i also need to insert data
into hn.hin_incentive_changes, that should trigger the incentive page in vision to highlight that incentive
so
1. 
-- actually this was a goof, updated both current incentives, not just the changed one,
-- not a big deal, just updated lines 2 & 3 on the unchanged incentive
insert into hn.hin_incentive_bulletins(program_id,line_number,the_line,the_date)
select program_id,line_number,the_line,the_date
from hn.hin_incentive_bulletins_ext
on conflict (program_id,line_number)
do update
  set (the_line,the_date) = (excluded.the_line,excluded.the_date);

2.
insert into hn.hin_incentive_changes(program_id, the_date) values('HP-53A', current_date);

---------------------------------------------------------------------------------
--/> 11/19/2020 non superficial incentive change
---------------------------------------------------------------------------------

---------------------------------------------------------------------------------
--< 10/12/2020 incentive change
---------------------------------------------------------------------------------
line 6 has 1 space added, sheesh, that is the only difference, still, it now needs to be the basis

11/02/2020 same thing again, spaces in line 6 for both HP-Z32 & HP-37A
& 11/09/20, HP-37A
select *
from (
  select *, md5(the_line) as old_line_hash, length(the_line)
  from hn.hin_incentive_bulletins
  order by program_id, line_number) a
full outer join (
  select length(the_line), *, md5(the_line) as new_line_hash
  from hn.hin_incentive_bulletins_ext
  order by program_id, line_number) b on a.line_number = b.line_number   
    and a.program_id = b.program_id 
where a.old_line_hash <> b.new_line_hash    

so, i am thinking i can do an insert with update on conflict

-- update hn.hin_incentive_bulletins to the most recent version
insert into hn.hin_incentive_bulletins(program_id,line_number,the_line,the_date)
select program_id,line_number,the_line,the_date
from hn.hin_incentive_bulletins_ext
on conflict (program_id,line_number)
do update
  set (the_line,the_date) = (excluded.the_line,excluded.the_date);

---------------------------------------------------------------------------------
--/> 10/12/2020 incentive change
--------------------------------------------------------------------------------- 

---------------------------------------------------------------------------------
--< 03/10/2021 non superficial incentive change
---------------------------------------------------------------------------------
HP-03C, HP-09C, HP-82B
some changes, with a re-issue date and an additional clause under vehicles
what i forgot was, after updating hn.hin_incentive_bulletins, i also need to insert data
into hn.hin_incentive_changes, that should trigger the incentive page in vision to highlight that incentive
so
1. 
-- actually this was a goof, updated both current incentives, not just the changed one,
-- not a big deal, just updated lines 2 & 3 on the unchanged incentive
insert into hn.hin_incentive_bulletins(program_id,line_number,the_line,the_date)
select program_id,line_number,the_line,the_date
from hn.hin_incentive_bulletins_ext
where program_id = 'HP-82B'
on conflict (program_id,line_number)
do update
  set (the_line,the_date) = (excluded.the_line,excluded.the_date);

2.
insert into hn.hin_incentive_changes(program_id, the_date) values('HP-82B', current_date);

---------------------------------------------------------------------------------
--/> 03/10/2021 non superficial incentive change
---------------------------------------------------------------------------------
--< 11/13/2020 deploying to production, need function(s)
---------------------------------------------------------------------------------
create or replace function hn.hin_incentives_check()
returns integer as 
$BODY$
/*
check for new or changed honda incentives

select * from hn.hin_incentives_check();
*/
select (
  ( -- changed function
    select count(*)::integer
    -- select *
    from (
      select a.program_id, a.line_number, a.the_line as old_line, b.the_line as new_line, old_line_hash, new_line_hash
      from (
        select *, md5(the_line) as old_line_hash
        from hn.hin_incentive_bulletins
        order by program_id, line_number) a
      full outer join (
        select *, md5(the_line) as new_line_hash
        from hn.hin_incentive_bulletins_ext
        order by program_id, line_number) b on a.program_id = b.program_id
          and a.line_number = b.line_number) c  
    where new_line_hash <> old_line_hash
      and line_number not in (2,3))
+
  ( -- new incentive
    select count(*)::integer
    from hn.hin_incentive_bulletins_ext a
    where not exists (
      select 1
      from hn.hin_incentive_bulletins
      where program_id = a.program_id)));
$BODY$
LANGUAGE sql;      
comment on function hn.hin_incentives_check() is 'checks for new or changed honda incentives. invoked from luigi.honda_incentives.py which is run monday thru saturday at 9AM';
---------------------------------------------------------------------------------
--/> 11/13/2020 deploying to production, need function(s)
---------------------------------------------------------------------------------