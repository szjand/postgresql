﻿  select b.year_month, c.store_code, d.g_l_acct_number, 
    a.control, a.amount, fxmpge, fxmlne,
  case fxmlne::integer
    when 34 then 'cr-v'
    when 35 then 'pilot'

  end as model, 
    case when a.amount < 0 then 1 else -1 end as unit_count, 
    
select b.the_date as sale_date, a.control as stock_number, bb.model, bb.body_style, bb.color,
  model_code
  from fin.fact_gl a   
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201701 and 201709
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account_type = 'Sale'
  inner join (
    select distinct g_l_acct_number, fxmpge, fxmlne
    from arkona.ext_eisglobal_sypffxmst a
    inner join (
      select factory_financial_year, g_l_acct_number, factory_account, fact_account_
      from arkona.ext_ffpxrefdta) b on a.fxmcyy = b.factory_financial_year and a.fxmact = b.factory_account
    where a.fxmcyy = 2017
      and fxmpge = 7
      and fxmlne between 34 and 35) d on c.account = d.g_l_acct_number
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
    and aa.journal_code in ('VSN')   
left join arkona.xfm_inpmast bb on a.control = bb.inpmast_stock_number
  and bb.current_row = true
  where a.post_status = 'Y'