﻿1/14/24
all (most) of this is put is put into function sls.ext_honda_nissan_water_report()
which is called from luigi on Monday mornings from misc.py

`
drop table if exists wtf;
create temp table wtf as
select a.inpmast_stock_number as "stock #", a.inpmast_vin as vin, a.year, a.make, a.model, 
	current_date - b.fromts::date as days_owned,
	d.days_avail,
	inpmast_vehicle_cost::integer as cost, 
  (select ads.get_current_best_price_by_stock_number_used(a.inpmast_stock_number)) as best_price,
  case 
    when (
      select count(*)
      from ads.ext_recon_authorizations m
      where vehicleinventoryitemid  = b.vehicleinventoryitemid
        and thruts is null
        and exists (
          select 1
          from ads.ext_authorized_recon_items
          where reconauthorizationid = m.reconauthorizationid
            and status <> 'AuthorizedReconItem_Complete')
          ) > 0 then 'unfinished'
    else
      'finished'
  end as recon_status
from arkona.ext_inpmast a
left join ads.ext_vehicle_inventory_items b on a.inpmast_stock_number = b.stocknumber
left join ( -- this feels wonky, but it works
	select c.vehicleinventoryitemid, current_date - min(c.fromts::date) as days_avail
	from arkona.ext_inpmast a
	left join ads.ext_vehicle_inventory_items b on a.inpmast_stock_number = b.stocknumber
	left join ads.ext_vehicle_inventory_item_statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
		and c.status = 'RMFlagAV_Available'
	where stocked_company = 'RY2'
		and a.status = 'I' 
		and a.type_n_u = 'U'
		and a.inpmast_stock_number like 'H%'
		and a.inpmast_vehicle_cost > 0
		and (select ads.get_current_best_price_by_stock_number_used(a.inpmast_stock_number)) is not null 
	group by c.vehicleinventoryitemid) d on b.vehicleinventoryitemid = d.vehicleinventoryitemid
where stocked_company = 'RY2'
  and a.status = 'I' 
  and a.type_n_u = 'U'
  and a.inpmast_stock_number like 'H%'
  and a.inpmast_vehicle_cost > 0
  and (select ads.get_current_best_price_by_stock_number_used(a.inpmast_stock_number)) is not null 
order by a.inpmast_stock_number;



-- this resulted in avail > owned, because each instance of available is measured
-- from start of status to current date, instead of the duration of the the status, when there
--  are multiple instances of the status         

select c.vehicleinventoryitemid, sum(current_date - c.fromts::date)
from arkona.ext_inpmast a
left join ads.ext_vehicle_inventory_items b on a.inpmast_stock_number = b.stocknumber
left join ads.ext_vehicle_inventory_item_statuses c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
  and c.status = 'RMFlagAV_Available'
where stocked_company = 'RY2'
  and a.status = 'I' 
  and a.type_n_u = 'U'
  and a.inpmast_stock_number like 'H%'
  and a.inpmast_vehicle_cost > 0
  and (select ads.get_current_best_price_by_stock_number_used(a.inpmast_stock_number)) is not null 
--   and a.inpmast_stock_number in ('H17185A','H17177T')
group by c.vehicleinventoryitemid

H17185A  26 + 48 = 74

H17177T  82 + 1 = 83

delete from  sls.honda_nissan_water_report;
insert into sls.honda_nissan_water_report
select 1 as seq, 'RECON FINISHED' as "stock #", null as vin, NULL as year, NULL as make, NULL as model,null as "days owned", null as "days avail", NULL as cost,NULL as "best price",NULL as "profit/loss"
UNION
select 2 as seq, "stock #", vin, year, make, model, days_owned, days_avail,cost, best_price, best_price - cost as "profit/loss"
from wtf
where recon_status = 'finished'
union
select 3 as seq, 'TOTAL', null, NULL, NULL, count(vin)::text || ' ' || 'vehicles', null, null, sum(cost), sum(best_price), sum(best_price - cost)
from wtf
where recon_status = 'finished'
union
select 4 as seq, 'RECON NOT FINISHED', null, NULL, NULL, NULL,NULL,NULL,NULL,null,null
union
select 5 as seq, "stock #", vin, year, make, model, days_owned, days_avail, cost, best_price, best_price - cost as "profit/loss"
from wtf
where recon_status = 'unfinished'
union
select 6 as seq, 'TOTAL', null, NULL, NULL, count(vin)::text || ' ' || 'vehicles', null, null, sum(cost), sum(best_price), sum(best_price - cost)
from wtf
where recon_status = 'unfinished'
order by seq, "stock #";

select "stock #", vin, "year", make, model, days_owned, days_avail, "cost", "best price", "profit/loss"  from sls.honda_nissan_water_report

drop table if exists sls.honda_nissan_water_report cascade;
create table sls.honda_nissan_water_report (
  seq integer,
  "stock #" citext,
  vin citext,
  "year" integer,
  make citext,
  model citext,
  days_owned integer,
  days_avail integer,
  "cost" bigint,
  "best price" bigint,
  "profit/loss" bigint);
create index on sls.honda_nissan_water_report(seq, "stock #");  

drop table if exists sls.ext_honda_nissan_water_report;
create table sls.ext_honda_nissan_water_report (
  "stock #" citext,
  vin citext,
  year integer,
  make citext,
  model citext,
  days_owned integer,
  days_avail integer,
  cost bigint,
  best_price bigint,
  recon_status citext);
-- 
-- 
-- select * 
-- from ads.ext_recon_authorizations
-- where vehicleinventoryitemid = '491dcc3d-f00e-41f9-8acc-510d6a9d58e5'
-- 
-- select * from ads.ext_recon_authorizations where vehicleinventoryitemid = '491dcc3d-f00e-41f9-8acc-510d6a9d58e5'
-- 
-- select * from ads.ext_authorized_recon_items where reconauthorizationid = '6d7cebbf-74ba-dc43-bb8b-bcc077c766db'
-- 
-- select * from ads.ext_vehicle_recon_items where vehiclereconitemid = 'ba048130-2eaf-564e-a2b1-8eca300dd1af'
-- 
-- 
-- select * from ads.ext_recon_authorizations where vehicleinventoryitemid = '491dcc3d-f00e-41f9-8acc-510d6a9d58e5'
-- 
-- select a.inpmast_stock_number
-- from arkona.ext_inpmast a
-- left join ads.ext_vehicle_inventory_items b on a.inpmast_stock_number = b.stocknumber
-- where stocked_company = 'RY2'
--   and status = 'I' 
--   and type_n_u = 'U'
--   and inpmast_stock_number like 'H%'
--   and inpmast_vehicle_cost > 0
--   and (select ads.get_current_best_price_by_stock_number_used(a.inpmast_stock_number)) is not null 
-- order by a.inpmast_stock_number;
-- 
-- 
--       select m.*
--       from ads.ext_recon_authorizations m
--       where vehicleinventoryitemid  = '955c3815-3d53-4472-a16a-8e28583f18c2'
--         and thruts is null
--         and exists (
--           select 1
--           from ads.ext_authorized_recon_items
--           where reconauthorizationid = m.reconauthorizationid
--             and status <> 'AuthorizedReconItem_Complete')
-- 
-- 
-- select * from ads.ext_vehicle_recon_items
-- where vehicleinventoryitemid = '491dcc3d-f00e-41f9-8acc-510d6a9d58e5'