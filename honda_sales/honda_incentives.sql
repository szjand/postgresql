﻿********* this is not the script you are looking for ******************
********* you want incentive_diffs.sql ********************************

-- this is from function hn.check_for_incentive_changes() and shows where the changes are
select 'stg' as source, x.* from (
  select * from  hn.stg_hin_incentives
  except all -- any rows in hn.stg_hin_incentives not in hn.ext_hin_incentives
  select a.*,
    md5(a::text) as hash
  from (  
    select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3) ||  split_part(model, E'\n', 4), -- split_part(model, E'\n', 3),
      split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
      split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
      split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9) as field_7,
      split_part(dealer_finance_cash, E'\n', 10) as field_8, split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12),
      split_part(dealer_finance_cash, E'\n', 13),split_part(dealer_finance_cash, E'\n', 14),split_part(dealer_finance_cash, E'\n', 15),split_part(dealer_finance_cash, E'\n', 16)
    from hn.ext_hin_incentives
    where left(model, 4) in('2020', '2021')
      and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A') a) x
union all
select 'ext', y.* from (      
  select a.*,
    md5(a::text) as hash
  from (  
    select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3) ||  split_part(model, E'\n', 4), -- split_part(model, E'\n', 3),
      split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
      split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
      split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9) as field_7,
      split_part(dealer_finance_cash, E'\n', 10) , split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12),
      split_part(dealer_finance_cash, E'\n', 13),split_part(dealer_finance_cash, E'\n', 14),split_part(dealer_finance_cash, E'\n', 15),split_part(dealer_finance_cash, E'\n', 16)
    from hn.ext_hin_incentives
    where left(model, 4) in('2020', '2021')
      and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A') a 
  except all -- any rows in hn.ext_hin_incentives not in hn.stg_hin_incentives
  select * from  hn.stg_hin_incentives) y
order by model, source        


-----------------------------------------------------------------------------------
--< 09/01/2020  FUNCTION hn.check_for_incentive_changes() failed all new incentives
-----------------------------------------------------------------------------------


truncate hn.stg_hin_incentives;
insert into hn.stg_hin_incentives
select a.*,
  md5(a::text) as hash
from (  
select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3) ||  split_part(model, E'\n', 4), -- split_part(model, E'\n', 3),
  split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
  split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
  split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9) as field_7,
  split_part(dealer_finance_cash, E'\n', 10) as field_8, split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12)
from hn.ext_hin_incentives
where left(model, 4) in ('2020','2021') 
  and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A') a;

update hn.stg_hin_incentives x
set hash = y.hash
from (
  select program_id, md5(a::text) as hash
  from (
  select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3) ||  split_part(model, E'\n', 4), -- split_part(model, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
    split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9) as field_7,
    split_part(dealer_finance_cash, E'\n', 10), split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12),
    split_part(dealer_finance_cash, E'\n', 13),split_part(dealer_finance_cash, E'\n', 14),split_part(dealer_finance_cash, E'\n', 15),split_part(dealer_finance_cash, E'\n', 16)
  from hn.ext_hin_incentives
  where left(model, 4) in('2020', '2021')
    and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A') a) y
where x.program_id = y.program_id;    

!!!!!!!!!!!!!!!!!!! looks like this is all fucked up !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

-- drop table if exists hn.hin_incentives;
-- create table hn.hin_incentives (
--   program_id citext primary key,
--   program_name citext not null,
--   amount integer not null, 
--   exclusions citext,
--   from_date date not null,
--   thru_date date not null);

!!!!!!!!!!!!!!!!!!! lets try to at least get this part right !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
work from the compatability pdf

have to change PK because accord Z14 & Z15, same program ID different amounts
no good cant do it, aftons tables rely on this PK
so, what i will do is change the  progam id,eg, Z14 & Z14h, Z15 & Z15h

and have edit not add the existing programs that already exists (Y90, Y68, Y92)

insert into hn.hin_incentives values
('HP-Z04','2020 Fit Dealer, Lease, Finance Cash',500, null, '09/01/2020','11/02/2020'),  
('HP-Z12','2020 Accord Labor Day Dealer, Lease, Finance Cash', 750, 'Hybrid', '09/01/2020','09/08/2020'),
('HP-Z14','2020 Accord Loyalty',1000, 'Hybrid Only', '09/01/2020','11/02/2020'),  
('HP-Z14h','2020 Accord Loyalty',500, 'Hybrid', '09/01/2020','11/02/2020'),
('HP-Z15','2020 Accord Nissan and Toyota Conquest',1000, 'Hybrid Only', '09/01/2020','11/02/2020'),
('HP-Z15h','2020 Accord Conquest',500, 'Hybrid', '09/01/2020','11/02/2020'),
('HP-Z17','2020 Insight Dealer, Lease, Finance Cash',1000, null, '09/01/2020','11/02/2020'),
('HP-Z24','2020 HR-V Dealer, Lease, Finance Cash',500, null, '09/01/2020','11/02/2020'),
('HP-Z27','2020 HR-V Conquest',500, null, '09/01/2020','11/02/2020'),
('HP-Z26','2020 HR-V Loyalty',500, null, '09/01/2020','11/02/2020'),
('HP-Z30','2020 CR-V Labor Day Dealer, Lease, Finance Cash',750, 'AWD LX Only, Exc Hybrid','09/01/2020','09/08/2020'),
('HP-Z42','2020 Pilot Dealer, Lease, Finance Cash',1750, null, '09/01/2020','09/08/2020'),
('HP-Z44','2020 Pilot Loyalty',1000, null, '09/01/2020','09/08/2020'),
('HP-Z45','2020 Pilot Conquest',1000, null, '09/01/2020','09/08/2020'),
('HP-Z49','2020 Pilot Loyalty',1000, null, '09/01/2020','11/02/2020'),  
('HP-Z50','2020 Pilot Conquest',1000, null, '09/01/2020','11/02/2020'),  
('HP-Z57','2020 Odyssey Dealer, Lease, Finance Cash', 1000, null, '09/01/2020','11/02/2020'),  
('HP-Z59','2020 Odyssey Loyalty', 1000, null, '09/01/2020','11/30/2020'); 


update hn.hin_incentives
set exclusions = 'AWD LX Only, Exc Hybrid',
    from_date = '07/17/2020',
    thru_date = '09/08/2020'
where program_id = 'HP-Y90';

update hn.hin_incentives
set thru_date = '09/08/2020'
where program_id = 'HP-Y68';

update hn.hin_incentives
set thru_date = '09/08/2020'
where program_id = 'HP-Y92';

-----------------------------------------------------------------------------------
--/> 09/01/2020  FUNCTION hn.check_for_incentive_changes() failed all new incentives
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
--< 08/21/2020  FUNCTION hn.check_for_incentive_changes() failed
-----------------------------------------------------------------------------------
-- the failure
CR-V effective date change
update hn.stg_hin_incentives
set valid_range = '7/7/2020 - 9/8/2020', field_9 = 'Effective: 7/17/2020 - 9/8/2020', field_14 = 'Effective: 8/4/2020 - 9/8/2020'
where model = 'CR-V';

-- and update the hash
update hn.stg_hin_incentives
set hash = (
  select md5(a::text)
  from (
  select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3) ||  split_part(model, E'\n', 4), -- split_part(model, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
    split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9) as field_7,
    split_part(dealer_finance_cash, E'\n', 10) as field_8, split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12),
    split_part(dealer_finance_cash, E'\n', 13),split_part(dealer_finance_cash, E'\n', 14),split_part(dealer_finance_cash, E'\n', 15),split_part(dealer_finance_cash, E'\n', 16)
  from hn.ext_hin_incentives
  where left(model, 4) = '2020' 
    and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A'
    and split_part(model, E'\n', 2) = 'CR-V') a)
where model = 'CR-V';    

-----------------------------------------------------------------------------------
--/> 08/21/2020  FUNCTION hn.check_for_incentive_changes() failed
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
--< 08/16/2020  FUNCTION hn.check_for_incentive_changes() failed
-----------------------------------------------------------------------------------
-- the failure
CR-V:  * added to program_id for 2 of the programs
ext: HP-Y68*   HP-Y92*
stg: HP-Y68     HP-Y92
stupid re-arrangement of applicable states
-- the fix, modify hn.stg_hin_incentives program name for accord to include the extra space
update hn.stg_hin_incentives
set program_id = 'HP-Y68*'
where model = 'CR-V'
  and program_id = 'HP-Y68';
  
update hn.stg_hin_incentives
set field_11 = 'HP-Y92*'
where model = 'CR-V'
  and field_11 = 'HP-Y92';  

update hn.stg_hin_incentives
set field_3 = 'MA, ME, MT, ND, NH, NJ, NM, NY, OR, ',
    field_4 = 'PA, RI, SD, UT,VT, WA, WY'
where model = 'CR-V';    
  
-- and update the hash
update hn.stg_hin_incentives
set hash = (
  select md5(a::text)
  from (
  select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3) ||  split_part(model, E'\n', 4), -- split_part(model, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
    split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9) as field_7,
    split_part(dealer_finance_cash, E'\n', 10) as field_8, split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12),
    split_part(dealer_finance_cash, E'\n', 13),split_part(dealer_finance_cash, E'\n', 14),split_part(dealer_finance_cash, E'\n', 15),split_part(dealer_finance_cash, E'\n', 16)
  from hn.ext_hin_incentives
  where left(model, 4) = '2020' 
    and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A'
    and split_part(model, E'\n', 2) = 'CR-V') a)
where model = 'CR-V'    

-----------------------------------------------------------------------------------
--/> 08/16/2020  FUNCTION hn.check_for_incentive_changes() failed
-----------------------------------------------------------------------------------





      
-----------------------------------------------------------------------------------
--< 08/04/2020  FUNCTION hn.check_for_incentive_changes() failed
-----------------------------------------------------------------------------------
-- the failures
add model year 2021 Pilot
cr-v: add HP-Y92, the third "line" of programs for a single model

1. insert the 2 new programs into hn.hin_incentives

insert into hn.hin_incentives values
('HP-Y92','2020 CR-V Loyalty',500, 'Excludes Hybrid', '08/04/2020','08/31/2020'),
('HP-Y95','2021 Pilot Loyalty',1000, null,'08/04/2020','08/31/2020');

2. need to accomodate the 3rd program in hn.stg_hin_incentives, which means adding 4 additional attributes
-- select * from hn.stg_hin_incentives 
alter table hn.stg_hin_incentives
add column field_11 citext,
add column field_12 citext,
add column field_13 citext,
add column field_14 citext;

-- rearranged to keep has as the last field
drop table if exists hn.stg_hin_incentives cascade;
CREATE TABLE hn.stg_hin_incentives(
  model_year citext NOT NULL,
  model citext NOT NULL,
  valid_range citext NOT NULL,
  program_id citext NOT NULL,
  program_amount_name citext NOT NULL,
  field_1 citext,
  field_2 citext,
  field_3 citext,
  field_4 citext,
  field_5 citext,
  field_6 citext,
  field_7 citext,
  field_8 citext,
  field_9 citext,
  field_10 citext,
  field_11 citext,
  field_12 citext,
  field_13 citext,
  field_14 citext,
  hash citext,
  CONSTRAINT stg_hin_incentives_pkey PRIMARY KEY (program_id));

3. update hn.stg_hin_incentives with the new CR-V data
delete 
-- select *
from hn.stg_hin_incentives where model = 'CR-V';

insert into hn.stg_hin_incentives
select a.*,
  md5(a::text) as hash
from (  
select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3) ||  split_part(model, E'\n', 4), -- split_part(model, E'\n', 3),
  split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
  split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
  split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9) as field_7,
  split_part(dealer_finance_cash, E'\n', 10) as field_8, split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12),
  split_part(dealer_finance_cash, E'\n', 13) as field_11, split_part(dealer_finance_cash, E'\n', 14), split_part(dealer_finance_cash, E'\n', 15),
  split_part(dealer_finance_cash, E'\n', 16) as field_14
from hn.ext_hin_incentives
where left(model, 4) = '2020' 
  and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A'
  and split_part(model, E'\n', 2) = 'CR-V') a;

4. update hn.stg_hin_incentives with the new 2021 Pilot data
insert into hn.stg_hin_incentives
select a.*,
  md5(a::text) as hash
from (  
select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3) ||  split_part(model, E'\n', 4), -- split_part(model, E'\n', 3),
  split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
  split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
  split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9) as field_7,
  split_part(dealer_finance_cash, E'\n', 10) as field_8, split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12),
  split_part(dealer_finance_cash, E'\n', 13) as field_11, split_part(dealer_finance_cash, E'\n', 14), split_part(dealer_finance_cash, E'\n', 15),
  split_part(dealer_finance_cash, E'\n', 16) as field_14
from hn.ext_hin_incentives
where left(model, 4) = '2021' 
  and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A'
  and split_part(model, E'\n', 2) = 'Pilot') a;  

5. update all the hashes
update hn.stg_hin_incentives x
set hash = y.hash
from (
  select program_id, md5(a::text) as hash
  from (
  select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3) ||  split_part(model, E'\n', 4), -- split_part(model, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
    split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9) as field_7,
    split_part(dealer_finance_cash, E'\n', 10), split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12),
    split_part(dealer_finance_cash, E'\n', 13),split_part(dealer_finance_cash, E'\n', 14),split_part(dealer_finance_cash, E'\n', 15),split_part(dealer_finance_cash, E'\n', 16)
  from hn.ext_hin_incentives
  where left(model, 4) in('2020', '2021')
    and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A') a) y
where x.program_id = y.program_id;    

6. updated function hn.check_for_incentive_changes() to include the additional attributes
-----------------------------------------------------------------------------------
--/> 08/04/2020  FUNCTION hn.check_for_incentive_changes() failed
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
--< 07/22/2020  FUNCTION hn.check_for_incentive_changes() failed
-----------------------------------------------------------------------------------
-- the failures
cr-v: HP-Y90 adds Excludes Hybrid
-- the fix, modify hn.stg_hin_incentives program name for CR-V to add the exclusion
update hn.stg_hin_incentives
set field_8 = '(AWD LX Only. Excludes Hybrid)'
-- select * from hn.stg_hin_incentives 
where model = 'CR-V';

-- and update the hash
update hn.stg_hin_incentives
set hash = (
  select md5(a::text)
  from (
  select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3) ||  split_part(model, E'\n', 4), -- split_part(model, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
    split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9) as field_7,
    split_part(dealer_finance_cash, E'\n', 10) as field_8, split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12)
  from hn.ext_hin_incentives
  where left(model, 4) = '2020' 
    and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A'
    and split_part(model, E'\n', 2) = 'CR-V') a)
where model = 'CR-V';  
-----------------------------------------------------------------------------------
--/> 07/22/2020  FUNCTION hn.check_for_incentive_changes() failed
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
--< 07/17/2020  FUNCTION hn.check_for_incentive_changes() failed
-----------------------------------------------------------------------------------
-- the failures
cr-v: added HP-Y90

insert into hn.hin_incentives values
('HP-Y90','2020 CR-V Captive Finance Cash',500, 'AWD LX Only', '07/07/2020','08/31/2020');  

delete 
-- select *
from hn.stg_hin_incentives where model = 'CR-V';

insert into hn.stg_hin_incentives
select a.*,
  md5(a::text) as hash
from (  
select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3) ||  split_part(model, E'\n', 4), -- split_part(model, E'\n', 3),
  split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
  split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
  split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9) as field_7,
  split_part(dealer_finance_cash, E'\n', 10) as field_8, split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12)
from hn.ext_hin_incentives
where left(model, 4) = '2020' 
  and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A'
  and split_part(model, E'\n', 2) = 'CR-V') a;
-----------------------------------------------------------------------------------
--/> 07/17/2020  FUNCTION hn.check_for_incentive_changes() failed
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
--< 07/14/2020  FUNCTION hn.check_for_incentive_changes() failed
-----------------------------------------------------------------------------------
-- the failures
accord program_amount name removes the extra space today
ext: '$1,000 Loyalty  OR  $1,000 Conquest'
stg: '$1,000 Loyalty  OR   $1,000 Conquest'
-- accord
-- the fix, modify hn.stg_hin_incentives program name for accord to include the extra space
update hn.stg_hin_incentives
set program_amount_name = '$1,000 Loyalty  OR  $1,000 Conquest'
where model = 'accord';
-- and update the hash
update hn.stg_hin_incentives
set hash = (
  select md5(a::text)
  from (
  select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3) ||  split_part(model, E'\n', 4), -- split_part(model, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
    split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9) as field_7,
    split_part(dealer_finance_cash, E'\n', 10) as field_8, split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12)
  from hn.ext_hin_incentives
  where left(model, 4) = '2020' 
    and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A'
    and split_part(model, E'\n', 2) = 'Accord') a)
where model = 'accord'   

-- the failures
fit: program_id changes from HP-X35 to HP-Y35
-- accord
-- the fix, modify hn.stg_hin_incentives program name for accord to include the extra space
update hn.stg_hin_incentives
set program_id = 'HP-Y35'
-- select * from hn.stg_hin_incentives
where model = 'fit';
-- and update the hash
update hn.stg_hin_incentives
set hash = (
  select md5(a::text)
  from (
  select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3) ||  split_part(model, E'\n', 4), -- split_part(model, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
    split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9) as field_7,
    split_part(dealer_finance_cash, E'\n', 10) as field_8, split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12)
  from hn.ext_hin_incentives
  where left(model, 4) = '2020' 
    and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A'
    and split_part(model, E'\n', 2) = 'Fit') a)
where model = 'Fit'  
-----------------------------------------------------------------------------------
--/> 07/14/2020  FUNCTION hn.check_for_incentive_changes() failed
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
--< 07/08/2020  FUNCTION hn.check_for_incentive_changes() failed
-----------------------------------------------------------------------------------
-- the failure
accord program_amount name has an extra space today
ext: '$1,000 Loyalty  OR   $1,000 Conquest'
stg: '$1,000 Loyalty  OR  $1,000 Conquest'

-- the fix, modify hn.stg_hin_incentives program name for accord to include the extra space
update hn.stg_hin_incentives
set program_amount_name = '$1,000 Loyalty  OR   $1,000 Conquest'
where model = 'accord';
-- and update the hash
update hn.stg_hin_incentives
set hash = (
  select md5(a::text)
  from (
  select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3) ||  split_part(model, E'\n', 4), -- split_part(model, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
    split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9) as field_7,
    split_part(dealer_finance_cash, E'\n', 10) as field_8, split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12)
  from hn.ext_hin_incentives
  where left(model, 4) = '2020' 
    and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A'
    and split_part(model, E'\n', 2) = 'Accord') a)
where model = 'accord'    
-----------------------------------------------------------------------------------
--/> 07/08/2020  FUNCTION hn.check_for_incentive_changes() failed
-----------------------------------------------------------------------------------


-----------------------------------------------------------------------------------
--< 07/07/2020 all new incentives
-----------------------------------------------------------------------------------
select * from hn.stg_hin_incentives


truncate hn.stg_hin_incentives;
insert into hn.stg_hin_incentives
select a.*,
  md5(a::text) as hash
from (  
select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3) ||  split_part(model, E'\n', 4), -- split_part(model, E'\n', 3),
  split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
  split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
  split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9) as field_7,
  split_part(dealer_finance_cash, E'\n', 10) as field_8, split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12)
from hn.ext_hin_incentives
where left(model, 4) = '2020' 
  and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A') a;

-- drop table if exists hn.hin_incentives;
-- create table hn.hin_incentives (
--   program_id citext primary key,
--   program_name citext not null,
--   amount integer not null, 
--   exclusions citext,
--   from_date date not null,
--   thru_date date not null);

insert into hn.hin_incentives values
('HP-X35','2020 Fit Dealer, Lease, Finance Cash',500, null, '07/07/2020','08/31/2020'),  
('HP-Y47','2020 Accord Dealer, Lease, Finance Cash',500, null, '07/07/2020','08/31/2020'), 
('HP-Y42','2020 Accord Loyalty',1000, 'Hybrid Only', '07/07/2020','08/31/2020'), 
('HP-Y43','2020 Accord Conquest',1000, 'Hybrid Only', '07/07/2020','08/31/2020'), 
('HP-Y49','2020 Insight Dealer, Lease, Finance Cash',1000, null, '07/07/2020','08/31/2020'), 
('HP-Y60','2020 HR-V Dealer, Lease, Finance Cash',500, null, '07/07/2020','08/31/2020'), 
('HP-Y61','2020 HR-V Conquest',750, null, '07/07/2020','08/31/2020'),
('HP-Y68','2020 CR-V Subaru and Toyota Conquest',750, 'Excludes Hybrid', '07/07/2020','08/31/2020'),
('HP-Y76','2020 Pilot Dealer, Lease, Finance Cash',1750, null, '07/07/2020','08/31/2020'), 
('HP-Y77','2020 Pilot Loyalty',1000, null, '07/07/2020','08/31/2020'), 
('HP-Y78','2020 Pilot Conquest',1000, null, '07/07/2020','08/31/2020'),  
('HP-Y85','2020 Odyssey Dealer, Lease, Finance Cash', 750, null, '07/07/2020','08/31/2020'),
('HP-Y86','2020 Odyssey Loyalty', 1000, null, '07/07/2020','08/31/2020');


select * from  hn.hin_incentives where thru_Date > current_date







-----------------------------------------------------------------------------------
--/> 07/07/2020 all new incentives
-----------------------------------------------------------------------------------


-----------------------------------------------------------------------------------
--< 06/02/2020 all new incentives
-----------------------------------------------------------------------------------
select * from hn.stg_hin_incentives


truncate hn.stg_hin_incentives;
insert into hn.stg_hin_incentives
select a.*,
  md5(a::text) as hash
from (  
select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3) ||  split_part(model, E'\n', 4), -- split_part(model, E'\n', 3),
  split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
  split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
  split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9) as field_7,
  split_part(dealer_finance_cash, E'\n', 10) as field_8, split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12)
from hn.ext_hin_incentives
where left(model, 4) = '2020' 
  and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A') a;

-- drop table if exists hn.hin_incentives;
-- create table hn.hin_incentives (
  program_id citext primary key,
  program_name citext not null,
  amount integer not null, 
  exclusions citext,
  from_date date not null,
  thru_date date not null);

insert into hn.hin_incentives values
('HP-X80','2020 Fit Dealer, Lease, Finance Cash',500, null, '06/02/2020','07/06/2020'),  
('HP-X91','2020 Accord Dealer, Lease, Finance Cash',500, null, '06/02/2020','07/06/2020'),  
('HP-X93','2020 Insight Dealer, Lease, Finance Cash',500, null, '06/02/2020','07/06/2020'), 
('HP-Y03','2020 HR-V Dealer, Lease, Finance Cash',500, null, '06/02/2020','07/06/2020'), 
('HP-Y04','2020 HR-V Conquest',750, null, '06/02/2020','07/06/2020'), 
('HP-Y11','2020 CR-V Dealer, Lease, Finance Cash',500, 'AWD only excluding Hybrid', '06/02/2020','07/06/2020'),
('HP-Y14','2020 Pilot Dealer, Lease, Finance Cash',1750, null, '06/02/2020','07/06/2020'),  
('HP-Y15','2020 Pilot Loyalty',1000, null, '06/02/2020','07/06/2020'), 
('HP-Y16','2020 Pilot Conquest',1000, null, '06/02/2020','07/06/2020'), 
('HP-Y26','2020 Odyssey Dealer, Lease, Finance Cash', 750, null, '06/02/2020','07/06/2020'),
('HP-Y27','2020 Odyssey Loyalty', 1000, null, '06/02/2020','07/06/2020');


select * from  hn.hin_incentives where thru_Date > current_date







-----------------------------------------------------------------------------------
--/> 06/02/2020 all new incentives
-----------------------------------------------------------------------------------
CREATE TABLE hn.ext_hin_incentives (
  model citext,
  special_captive_lease citext,
  captive_lease_cash citext,
  conjuction_1 citext,
  dealer_finance_cash citext,
  conjuction_2 citext,
  special_captive_apr citext);
comment on table hn.ext_hin_incentives is 'table for the raw columnar parse of the
ACTIVE INCENTIVE PROGRAM SUMMARY pdf from HIN, column contain multiple rows delimited
by new lines'

select *
from hn.ext_hin_incentives
where dealer_finance_cash like 'hp-w98%'

-------------------------------------------------------------------------------------------
--< https://stackoverflow.com/questions/53705488/to-find-new-lines-character-in-postgres
-------------------------------------------------------------------------------------------
1. Splitting the string at the \n chars. Every split part is now one row in a temporary table.
2. Adding a row_count to assure the right order of the split parts
3. This counts the length of all single split parts. 
    The (length + 1) gives the position of the \n. 
    The SUM window function sums up all values within a group (your original text). 
    That''s why the order is relevant. For example: The first two parts of "abc\nde\nfgh" have the lengths of 3 and 2. 
    So the breaks are at 4 (abc = 3, + 1) and 3 (de = 2, + 1). 
    But the 3 of the second part is no real index, but if you sum up these values you get the right indexes: 4 and 7.
4. Aggregating these results
5. If (as in my example) the last char is always a \n and you are only interested in the \n chars the 
    string you could remove the last entry of the aggregated array.

SELECT
    dealer_finance_cash,
    array_remove(                -- 5
        (array_agg(sum))::int[], -- 4
        length(dealer_finance_cash) + 1        
    )
FROM (
    -- 3
    SELECT 
        dealer_finance_cash, 
        SUM(length(lines) + 1) OVER (PARTITION BY dealer_finance_cash ORDER BY row_number)
    FROM (
        -- 2
        SELECT 
            *,
            row_number() OVER ()
        FROM (
            -- 1
            SELECT 
                dealer_finance_cash, 
                regexp_split_to_table(dealer_finance_cash, '\n') as lines 
            FROM hn.ext_hin_incentives
        )s
    )s
) s
GROUP BY dealer_finance_cash


-- replace the new line with :::
SELECT
    dealer_finance_cash,
trim(regexp_replace(dealer_finance_cash, E'\n', ':::', 'g'))
from hn.ext_hin_incentives
where dealer_finance_cash like 'hp-w70%'


SELECT 
    dealer_finance_cash, 
    regexp_split_to_table(dealer_finance_cash, '\n') 
FROM hn.ext_hin_incentives

-------------------------------------------------------------------------------------------
--/> https://stackoverflow.com/questions/53705488/to-find-new-lines-character-in-postgres
-------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------
--< https://dba.stackexchange.com/questions/27279/how-to-preserve-the-original-order-of-elements-in-an-unnested-array
--------------------------------------------------------------------------------------------
select a.model, a.dealer_finance_cash, y.*, x.*
from hn.ext_hin_incentives a,
regexp_split_to_table(dealer_finance_cash, '\n') with ordinality x(word,inc_seq),
regexp_split_to_table(model, '\n') with ordinality y(word,mod_seq)

select *
from (
select a.model, a.dealer_finance_cash, y.*, x.*
from hn.ext_hin_incentives a,
regexp_split_to_table(dealer_finance_cash, '\n') with ordinality x(word,inc_seq),
regexp_split_to_table(model, '\n') with ordinality y(word,mod_seq)) aa
where mod_seq = 2

-- step 1, just the relevant rows
-- multiple programs
select model, dealer_finance_cash
from hn.ext_hin_incentives
where model like '2020%%'
  and dealer_finance_cash like '%AND%'
union
select model, dealer_finance_cash
from hn.ext_hin_incentives
where model like '2020%%'
  and dealer_finance_cash like '%All%'
  and dealer_finance_cash not like '%AND%'
union
select model, dealer_finance_cash
from hn.ext_hin_incentives
where model like '2020%%'
  and dealer_finance_cash like '%N/A%'
union
select model, dealer_finance_cash
from hn.ext_hin_incentives
where model like '2020%%'
  and dealer_finance_cash like '%Exclude%'  
union
select model, dealer_finance_cash
from hn.ext_hin_incentives
where model like '2020%%'
  and dealer_finance_cash like '%Resident%'    

!!!!!!!!!!!!!!!!!! get the published date !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  


select model, dealer_finance_cash, 
  split_part(model, E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3),
  split_part(dealer_finance_cash, E'\n', 1), split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
  split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
  split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9),
  split_part(dealer_finance_cash, E'\n', 10), split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12)
from hn.ext_hin_incentives
where left(model, 4) = '2020'
  and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A'

drop table if exists hn.hin_incentives;
create table hn.hin_incentives (
  program_id citext primary key,
  program_name citext not null,
  amount integer not null, 
  exclusions citext,
  from_date date not null,
  thru_date date not null);

insert into hn.hin_incentives values
('HP-W70','2020 Fit Dealer, Lease, Finance Cash',500, null, '04/01/2020','05/04/2020'),  
('HP-W79','2020 Accord Dealer, Lease, Finance Cash',500, 'Hybrid', '04/01/2020','05/04/2020'),  
('HP-W87','2020 HR-V Dealer, Lease, Finance Cash',500, null, '04/01/2020','05/04/2020'),  
('HP-W93','2020 CR-V Conquest',500, 'Hybrid', '04/01/2020','05/04/2020'),  
('HP-W96','2020 Pilot Dealer, Lease, Finance Cash',1750, null, '04/01/2020','05/04/2020'),  
('HP-W97','2020 Pilot Conquest',1750, null, '04/01/2020','05/04/2020'),  
('HP-X08','2020 Odyssey Conquest', 1000, null, '04/01/2020','05/04/2020'); 

-- 05/05/20
insert into hn.hin_incentives values
('HP-X23','2020 Fit Dealer, Lease, Finance Cash',500, null, '05/05/2020','06/01/2020'),  
('HP-X34','2020 Accord Dealer, Lease, Finance Cash',500, null, '05/05/2020','06/01/2020'),  
('HP-X44','2020 HR-V Dealer, Lease, Finance Cash',500, null, '05/05/2020','06/01/2020'),  
('HP-X45','2020 HR-V Conquest',750, 'ND Only, Excludes MN', '05/05/2020','06/01/2020'),
('HP-X50','2020 CR-V Conquest',500, 'Hybrid', '05/05/2020','06/01/2020'),  
('HP-X54','2020 Pilot Dealer, Lease, Finance Cash',1750, null, '05/05/2020','06/01/2020'),  
('HP-X55','2020 Pilot Conquest',1000, null, '05/05/2020','06/01/2020'),  
('HP-X66','2020 Odyssey  Dealer, Lease, Finance Cash', 750, null, '05/05/2020','06/01/2020'); 

update hn.hin_incentives
set exclusions = 'Minnesota'
where program_id = 'HP-X45'


select * from hn.hin_incentives where current_date between from_date and thru_Date

select a.vin
from hn.hin_vehicle_statuses a
join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
  and b.status = 'I'

select * from hn.ext_hin_vehicle_incentives                  

drop table if exists hn.ext_hin_vehicle_incentives;
create table hn.ext_hin_vehicle_incentives (
  vin citext not null,
  program_id citext not null,
  compatible citext,
  status citext,
  program_type citext,
  program_name citext,
  model citext,
  start_date citext,
  end_date citext);
comment on table hn.ext_hin_vehicle_incentives is 'scrape of HIN program search for each vin in inventory';


select * from hn.hin_incentives

select * from hn.ext_hin_vehicle_incentives limit 100;  

select *
from (
select * from hn.hin_vehicle_incentives) a
left join (
select b.vin, a.program_id
from hn.hin_incentives a
join hn.ext_hin_vehicle_incentives b on a.program_id = b.program_id
where current_date between a.from_date and a.thru_date) b on a.vin = b.vin
where b.vin is null

select b.vin, a.*
from hn.hin_incentives a
join hn.ext_hin_vehicle_incentives b on a.program_id = b.program_id
order by b.vin

drop table if exists hn.hin_vehicle_incentives;
create table hn.hin_vehicle_incentives (
  vin citext not null references hn.hin_vehicle_statuses(vin),
  program_id citext not null references hn.hin_incentives(program_id),
  primary key(vin,program_id));

insert into hn.hin_vehicle_incentives
select b.vin, a.program_id
from hn.hin_incentives a
join hn.ext_hin_vehicle_incentives b on a.program_id = b.program_id
where current_date between a.from_date and a.thru_date

delete from hn.hin_vehicle_incentives where program_id = 'HP-X50'

select * from hn.hin_vehicle_incentives

comment on function hn.update_hin_vehicle_incentives() is 'truncate and repopulate hn.hin_vehicle_incentives nightly';
create or replace function hn.update_hin_vehicle_incentives()
returns void as
$BODY$

truncate hn.hin_vehicle_incentives;

insert into hn.hin_vehicle_incentives
select b.vin, a.program_id
from hn.hin_incentives a
join hn.ext_hin_vehicle_incentives b on a.program_id = b.program_id
where current_date between a.from_date and a.thru_date;

$BODY$
language sql;

select a.vin, b.*
from hn.hin_vehicle_incentives a
join hn.hin_incentives b on a.program_id = b.program_id


drop table if exists hn.ext_incentives_publish_date;
create table hn.ext_incentives_publish_date (
  publish_date citext primary key);

select publish_date, trim(replace(publish_date, 'Published', ''))::date
from hn.ext_incentives_publish_date

drop table if exists hn.ext_incentives_publish_date;
create table hn.ext_incentives_publish_date (
  publish_date citext primary key);

drop table if exists hn.incentives_publish_date;
create table hn.incentives_publish_date (
  publish_date date primary key);
insert into hn.incentives_publish_date values('05/05/2020');  

create or replace function hn.check_hin_incentive_publish_date()
returns void as
$BODY$
/*
  select hn.check_hin_incentive_publish_date();
*/
begin

  assert ( -- ftp_inv.stpautos_wbl
    (
      select trim(replace(publish_date, 'Published', ''))::date
      from hn.ext_incentives_publish_date)
      =
    (
      select publish_date from hn.incentives_publish_date)) , 'Honda incentive publish date has changed';
    
end
$BODY$
LANGUAGE plpgsql VOLATILE;


-- 04/29, got a new publish date, lets use this to establish the process
-- 05/02 invoice data has expanded
/*
production deploy
create directories
  /home/rydell/projects/luigi_1804_36/honda
  /home/rydell/projects/luigi_1804_36/honda/vehicle_status
  /home/rydell/projects/luigi_1804_36/honda/clean_vehicle_status
  /home/rydell/projects/luigi_1804_36/honda/invoice_download
  /home/rydell/projects/luigi_1804_36/honda/invoice_csv
  /home/rydell/projects/luigi_1804_36/honda/bad_invoices
  /home/rydell/projects/luigi_1804_36/honda/incentives
  /home/rydell/projects/luigi_1804_36/honda/vehicle_incentives_xlsx
  /home/rydell/projects/luigi_1804_36/honda/vehicle_incentives_csv
  
*/
in luigi
  download and parse vehicle status
    populate hn.ext_hin_vehicle_statuses
    function hn.update_hin_vehicle_statuses() insert/update hn.hin_vehicle_statuses
  download and parse invoices 
    populate hin.ext_vehicle_invoices
    function hn.hin_vehicle_invoices populates hn.hin_vehicle_invoices 

select a.vin, b.*
from hn.hin_vehicle_incentives a
join hn.hin_incentives b on a.program_id = b.program_id


--------------------------------------------------------------------
5/7/20 
incentives
publish date turns out to be unreliable for scraping
so, daily, download the active incentive program summary
parse it into a csv
store the current version in hn.hin_stg_incentives
scrape daily into hn.hin_ext_incentives
compare ext to stg and error if there are changes
so, i currently have 4 versions, 4/7, 4/24, 5/5 & 5/6

camelot parses it nicely, within each column the rows are separated by line feeds

select *
from hn.ext_hin_incentives

select -- model, dealer_finance_cash, 
  split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3),
  split_part(dealer_finance_cash, E'\n', 1), split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
  split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
  split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9),
  split_part(dealer_finance_cash, E'\n', 10), split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12)
from hn.ext_hin_incentives
where left(model, 4) = '2020'
  and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A'

do 16 attributes (pad for unanticipated length of description), 1st 4 are consistent, after that they are ambiguous
at this level, just do all citext, only the first 5 not null

drop table if exists hn.stg_hin_incentives;
create table hn.stg_hin_incentives (
  model_year citext not null,
  model citext not null,
  valid_range citext not null,
  program_id citext primary key,
  program_amount_name citext not null,
  field_1 citext,
  field_2 citext,
  field_3 citext,
  field_4 citext,
  field_5 citext,
  field_6 citext,
  field_7 citext,
  field_8 citext,
  field_9 citext,
  field_10 citext,
  hash citext);
comment on table hn.stg_hin_incentives is 'table to hold the most recent version of HIN incentives, 
parsed from hn.ext_hin_incentives with the multi row columns parsed into separate columns. Added a
hash column to facilitate identifying changes in programs';



populate the stg table with data from 4/7

insert into hn.stg_hin_incentives
select a.*,
  md5(a::text) as hash
from (  
  select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 1), split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
    split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9),
    split_part(dealer_finance_cash, E'\n', 10), split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12)
  from hn.ext_hin_incentives
  where left(model, 4) = '2020' 
    and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A') a;

select * from hn.stg_hin_incentives    

now populate ext with 4/24
-- no differences between 4/7 and 4/24
select *
from hn.stg_hin_incentives aa
join (
select a.*,
  md5(a::text) as hash
from (  
  select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
    split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9),
    split_part(dealer_finance_cash, E'\n', 10), split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12)
  from hn.ext_hin_incentives
  where left(model, 4) = '2020' 
    and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A') a) bb on aa.program_id = bb.program_id
and aa.hash <> bb.hash


now populate ext with 5/5

-- this is fine but does not expose new programs
select *
from hn.stg_hin_incentives aa
join (
select a.*,
  md5(a::text) as hash
from (  
  select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
    split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9),
    split_part(dealer_finance_cash, E'\n', 10), split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12)
  from hn.ext_hin_incentives
  where left(model, 4) = '2020' 
    and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A') a) bb on aa.program_id = bb.program_id
and aa.hash <> bb.hash      

-- perfect
select aa.*
from hn.stg_hin_incentives aa
left join (
  select a.*,
    md5(a::text) as hash
  from (  
    select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3),
      split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
      split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
      split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9),
      split_part(dealer_finance_cash, E'\n', 10), split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12)
    from hn.ext_hin_incentives
    where left(model, 4) = '2020' 
      and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A') a) bb on aa.program_id = bb.program_id
where bb.program_id is null     

-- so between these 2, can detect changed programs or new programs
 replace stg with the data from 5/5

truncate hn.stg_hin_incentives;
insert into hn.stg_hin_incentives
select a.*,
  md5(a::text) as hash
from (  
  select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 1), split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
    split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9),
    split_part(dealer_finance_cash, E'\n', 10), split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12)
  from hn.ext_hin_incentives
  where left(model, 4) = '2020' 
    and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A') a;

now populate with 5/6    
-- changed rows
select *
from hn.stg_hin_incentives aa
join (
select a.*,
  md5(a::text) as hash
from (  
  select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
    split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9),
    split_part(dealer_finance_cash, E'\n', 10), split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12)
  from hn.ext_hin_incentives
  where left(model, 4) = '2020' 
    and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A') a) bb on aa.program_id = bb.program_id
and aa.hash <> bb.hash   


ok, one program HP-X45 has changed
which in this goofy layout world, is part of HP-X44 (multiple programs in one cell, field_2 = "AND")
the only difference is add states in the Residents clause.
no affect on the data stored in hn.hin_incentives
so
update the data in the stg table, but no changes in hn.hin_incentives
-- 
select 'stg', x.* from hn.stg_hin_incentives x where program_id = 'HP-X44'
union
select 'ext', a.*,
  md5(a::text) as hash
from (  
  select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
    split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
    split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9),
    split_part(dealer_finance_cash, E'\n', 10), split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12)
  from hn.ext_hin_incentives
  where left(model, 4) = '2020' 
    and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A') a
where program_id = 'HP-X44'    

-- update the stg table
update hn.stg_hin_incentives x
set field_7 = y.field_7,
    field_8 = y.field_8,
    hash = y.hash
from (
  select a.*,
    md5(a::text) as hash
  from (  
    select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3),
      split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
      split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
      split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9) as field_7,
      split_part(dealer_finance_cash, E'\n', 10) as field_8, split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12)
    from hn.ext_hin_incentives
    where left(model, 4) = '2020' 
      and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A') a
  where program_id = 'HP-X44') y    
where x.program_id = 'HP-X44';

ok, i think this is good, so now, i have to put it in python
also double check how i am handling vehicle incentives, should be checking for unlisted programs?
add check for bad invoices


-- 05/10
-- have not been parsing out the full date range
-- this fixes that
    select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3) ||  split_part(model, E'\n', 4),
      split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
      split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
      split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9) as field_7,
      split_part(dealer_finance_cash, E'\n', 10) as field_8, split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12)
    from hn.ext_hin_incentives
    where left(model, 4) = '2020' 
      and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A'

-- fix the data in stg table
update hn.stg_hin_incentives x
set valid_range = y.valid_range,
    hash = y.hash
from (
  select a.*,
    md5(a::text) as hash
  from (  
    select split_part(model,E'\n', 1), split_part(model, E'\n', 2), trim(split_part(model, E'\n', 3) ||  split_part(model, E'\n', 4)) as valid_range,
      split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
      split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
      split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9) as field_7,
      split_part(dealer_finance_cash, E'\n', 10) as field_8, split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12)
    from hn.ext_hin_incentives
    where left(model, 4) = '2020' 
      and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A') a) y
where x.program_id = y.program_id

select * from hn.stg_hin_incentives
      
-- new rows      
-- this should detect any changes
comment on function hn.check_for_incentive_changes() is 'compare the contents of hn.ext_hin_incentives with the contents
  of hn.stg_hin_incentives, throw an error if there are any differences. Unfortunately, due to the goofy nature of the 
  data, updating hn.hin_incentives is a manual process, but requires minimal work.'
create or replace function hn.check_for_incentive_changes()
returns void as 
$BODY$
/*
select hn.check_for_incentive_changes()
*/
begin
  assert (
    select count(*)
    from (
      select * from (
        select * from  hn.stg_hin_incentives
        except all -- any rows in hn.stg_hin_incentives not in hn.ext_hin_incentives
        select a.*,
          md5(a::text) as hash
        from (  
          select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3) ||  split_part(model, E'\n', 4), -- split_part(model, E'\n', 3),
            split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
            split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
            split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9) as field_7,
            split_part(dealer_finance_cash, E'\n', 10) as field_8, split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12)
          from hn.ext_hin_incentives
          where left(model, 4) = '2020' 
            and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A') a) x
      union all
      select * from (      
        select a.*,
          md5(a::text) as hash
        from (  
          select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3) ||  split_part(model, E'\n', 4), -- split_part(model, E'\n', 3),
            split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
            split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
            split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9) as field_7,
            split_part(dealer_finance_cash, E'\n', 10) as field_8, split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12)
          from hn.ext_hin_incentives
          where left(model, 4) = '2020' 
            and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A') a 
        except all -- any rows in hn.ext_hin_incentives not in hn.stg_hin_incentives
        select * from  hn.stg_hin_incentives) y) xx) = 0, 'Incentives have changed';
end    
$BODY$   
language plpgsql;      

update hn.stg_hin_incentives
set program_amount_name = '$500 Dealer, Lease, Finance Cash'
-- select * from hn.stg_hin_incentives
where program_id = 'HP-X23'

