﻿-- Function: sls.update_consultant_payroll()


  with 
    open_month as (
      select year_month
      from sls.months
      where open_closed = 'open')
  select (select year_month from open_month),
    team,last_name,first_name,employee_number, payplan,
    unit_count, unit_pay, fi_gross, chargebacks, fi_total, fi_pay, pto_hours, pto_rate, pto_pay,
    "3_month_rolling_avg", months_employed, total_Earned, draw, guarantee, 
    round(
      case
        when total_earned > guarantee then total_earned - draw
        else guarantee - draw
      end, 2) as due_at_month_end,
    now(), coalesce(pulse, 0), hol_hours, hol_rate, hol_pay   
  from ( -- jj                     
    select  ff.*, coalesce(gg."3 month rolling avg", 0) as "3_month_rolling_avg", hh.months_employed,
      ff.unit_pay + ff.fi_pay + ff.pto_pay + ff.hol_pay + coalesce(kk.pulse, 0) as total_earned,
      coalesce(ii.draw, 0) as draw,
      case
        -- *e*
        when ff.employee_number = (select employee_number from sls.personnel where last_name = 'stadstad') then 0
        when hh.months_employed > 3 and coalesce(gg."3 month rolling avg", 0) < 10 then 2400
        else 3000
      end as guarantee, kk.pulse  
    from ( --ff
      select aa.*, coalesce(bb.unit_count, 0) as unit_count,
        (select * from sls.per_unit_earnings (aa.employee_number, bb.unit_count)) as unit_pay, 
        case 
          when aa.payplan = 'executive' then coalesce(cc.fi_gross, 0)
          else 0
        end as fi_gross, 
        case 
          when aa.payplan = 'executive' then coalesce(dd.amount, 0)
          else 0
        end as chargebacks,
        case 
          when aa.payplan = 'executive' then coalesce(dd.amount, 0) + coalesce(cc.fi_gross, 0)
          else 0
        end as fi_total, 
        case 
          when aa.payplan = 'executive' then round(.16 * (coalesce(dd.amount, 0) + coalesce(cc.fi_gross, 0)), 2)
          else 0
        end as fi_pay,
        ee.pto_hours, ee.pto_rate, coalesce(ee.pto_pay, 0) as pto_pay,
        ee.hol_hours, ee.hol_rate, coalesce(ee.hol_pay, 0) as hol_pay
      from ( -- aa: cons, team, payplan
        select b.team, a.last_name, a.first_name, a.employee_number, c.payplan
        from sls.personnel a
        inner join sls.team_personnel b on a.employee_number = b.employee_number
          and b.thru_date > (select first_of_month from sls.months where open_closed = 'open')
        inner join sls.personnel_payplans c on a.employee_number = c.employee_number
          and c.thru_date > (select first_of_month from sls.months where open_closed = 'open')) aa 
-- *a*
--         where a.store_code = 'RY1') aa
      left join ( -- bb: unit count
        select psc_employee_number, sum(unit_count) as unit_count
        from ( -- x
          select a.psc_employee_number, 
            case
              when ssc_last_name = 'none' then unit_count
              else 0.5 * unit_count
            end as unit_count
          from sls.deals_by_month a
          where a.year_month = (select year_month from open_month)
          union all
          select a.ssc_employee_number, 
            0.5 * unit_count as unit_count
          from sls.deals_by_month a
          where a.year_month = (select year_month from open_month)
            and a.ssc_last_name <> 'none') x
        group by psc_employee_number) bb  on aa.employee_number = bb.psc_employee_number
      left join ( -- cc: fi gross
        select fi_employee_number, sum(fi_gross) as fi_gross
        from (    
          select a.fi_employee_number, a.fi_last_name, 
            case
              when c.payplan = 'executive' then b.fi_sales - b.fi_cogs
              else b.fi_sales - b.fi_cogs - b.fi_acq_fee
            end as fi_gross
          from sls.deals_by_month a
          left join sls.deals_gross_by_month b on a.stock_number = b.control
            and a.year_month = b.year_month
          left join sls.personnel_payplans c on a.fi_employee_number = c.employee_number
            and c.thru_date > current_date  
          where a.year_month = (select year_month from open_month)) aaa
        group by aaa.fi_employee_number) cc on aa.employee_number = cc.fi_employee_number
      left join ( -- fi chargebacks
        select fi_employee_number, sum(amount) as amount
        from sls.fi_chargebacks
        where chargeback_year_month = (select year_month from open_month)
        group by fi_employee_number) dd on aa.employee_number = dd.fi_employee_number
			left join ( -- ee
				select employee_number, pto_hours, hol_hours, coalesce(pto_rate, 0) as pto_rate, coalesce(hol_rate, 0) as hol_rate,
					round(pto_hours * coalesce(pto_rate, 0), 2) as pto_pay, round(hol_hours * hol_rate, 2) as hol_pay
				from (
					select aa.employee_number, aa.pto_hours, aa.holiday_hours as hol_hours, 
					  cc.current_rate as pto_rate, dd.current_rate as hol_rate
					from sls.clock_hours_by_month aa
					left join (
						select a.last_name, a.first_name, a.employee_number, c.current_rate
						from ukg.employees a
						join ukg.employee_profiles b on a.employee_id = b.employee_id
						join ukg.personal_rate_tables c on b.rate_table_id = c.rate_table_id
							and c.counter = 'paid time off') cc on aa.employee_number = cc.employee_number  	
					left join (
						select a.last_name, a.first_name, a.employee_number, c.current_rate
						from ukg.employees a
						join ukg.employee_profiles b on a.employee_id = b.employee_id
						join ukg.personal_rate_tables c on b.rate_table_id = c.rate_table_id
							and c.counter = 'holiday') dd on aa.employee_number = dd.employee_number   
					where aa.year_month = (select year_month from open_month)) bb) ee on aa.employee_number = ee.employee_number) ff
    left join ( -- gg: 3 month rolling avg
      select psc_employee_number, round(sum(unit_count)/3, 1) as "3 month rolling avg"
      from (
         select a.psc_employee_number, psc_last_name, b.first_full_month_emp,
           case
             when ssc_last_name = 'none' then unit_count
             else 0.5 * unit_count
           end as unit_count
         from sls.deals_by_month a
         inner join sls.personnel b on a.psc_employee_number = b.employee_number
         where a.year_month in (
             select year_month
             from sls.months where seq in (
             select seq - 3 from sls.months where year_month = (select year_month from open_month)
             union
             select seq - 2 from sls.months where year_month = (select year_month from open_month)
             union
             select seq - 1 from sls.months where year_month = (select year_month from open_month))) 
         union all
         select ssc_employee_number, ssc_last_name, b.first_full_month_emp,
           0.5 * unit_count as unit_count
         from sls.deals_by_month a
         inner join sls.personnel b on a.ssc_employee_number = b.employee_number
         where a.year_month in (
             select year_month
            from sls.months where seq in (
             select seq - 3 from sls.months where year_month = (select year_month from open_month)
             union
             select seq - 2 from sls.months where year_month = (select year_month from open_month)
             union
             select seq - 1 from sls.months where year_month = (select year_month from open_month))) 
           and a.ssc_last_name <> 'none') e
       group by psc_employee_number) gg on ff.employee_number = gg.psc_employee_number 
    left join ( -- hh: months employed
      select employee_number, 
        ((select seq from sls.months where open_closed = 'open') - 
          (select seq from sls.months where year_month = a.first_full_month_emp)) + 1 as months_employed
      from sls.personnel a) hh on ff.employee_number = hh.employee_number 
		left join ( -- ii draw
		select c.employee_number, d.ee_amount as draw, 
			(select year_month from dds.dim_date where the_date = a.payroll_end_date) as year_month
		from ukg.payrolls a
		join ukg.pay_statements b on a.payroll_id = b.payroll_id
		join ukg.employees c on b.employee_account_id = c.employee_id
		join ukg.pay_statement_earnings d on a.payroll_id = d.payroll_id
			and b.pay_statement_id = d.pay_statement_id
			and d.earning_code = 'sales volume commission'
			and d.type_name = 'Earning (Auto)'
		where 
			case
				when a.ein_name = 'Rydell Toyota of Grand Forks' then a.payroll_name like 'Rydell Toyota Semi-Monthly Regular%'
				else a.payroll_name like '%semi-monthly NEW regular%'
			end) ii on ff.employee_number = ii.employee_number 
			and ii.year_month = (select year_month from open_month)
    left join ( -- kk: pulse
        select psc_employee_number, sum(unit_count) * 25 as pulse
        from ( -- y
          select a.psc_employee_number, 
            case
              when ssc_last_name = 'none' then unit_count
              else 0.5 * unit_count
            end as unit_count
          from sls.deals_by_month a
          where a.year_month = (select year_month from open_month)
            and a.pulse = 1
          union all
          select a.ssc_employee_number, 
            0.5 * unit_count as unit_count
          from sls.deals_by_month a
          where a.year_month = (select year_month from open_month)
            and a.ssc_last_name <> 'none'
            and a.pulse = 1) y
        group by psc_employee_number) kk on ff.employee_number = kk.psc_employee_number) jj
where employee_number like '8%'        

/*
1. get rid of riddle
	select * from sls.team_personnel where employee_number = '8100222'

	select * from sls.personnel_payplans  where employee_number = '8100222'
	update sls.personnel
	set end_date = '10/15/2022'
	where last_name = 'riddle';

	update sls.team_personnel
	set thru_date = '10/15/2022'
	where employee_number = '8100222';

	update sls.personnel_payplans
	set thru_date = '10/15/2022'
	where employee_number = '8100222';		

2. months employed is showing 3 for all the toyota consultants	
select * from sls.personnel where employee_number like '8%' and end_date > current_date
all show August as first full month employed

select * from sls.months where year_month in (202208, 202209, 202210, 202211) order by seq
  so this the quey for months employed
      select employee_number, 
        (select seq from sls.months where open_closed = 'open') - 
          (select seq from sls.months where year_month = a.first_full_month_emp) as months_employed
      from sls.personnel a
      where employee_number like '8%'
  which translates to 815 (nov) - 812 (aug), which equals 3, but is actuall 4 full months of employement  
  so, need to make it that formula + 1    
      select employee_number, 
        ((select seq from sls.months where open_closed = 'open') - 
          (select seq from sls.months where year_month = a.first_full_month_emp)) + 1 as months_employed
      from sls.personnel a
      where employee_number like '8%'  

  also changed 
		when hh.months_employed > 4 and coalesce(gg."3 month rolling avg", 0) < 10 then 2400
		to
		when hh.months_employed > 3 and coalesce(gg."3 month rolling avg", 0) < 10 then 2400   

3. i think that is all i need to do to make sls.consulant payroll work for toyota		   
*/