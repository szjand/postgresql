﻿tables team_personnel & personnel_payplans are needed for function sls.update_consultant_payroll()


        select b.team, a.last_name, a.first_name, a.employee_number, c.payplan
        from sls.personnel a
        inner join sls.team_personnel b on a.employee_number = b.employee_number
          and b.thru_date > '08/01/2022'
        inner join sls.personnel_payplans c on a.employee_number = c.employee_number
          and c.thru_date > '08/01/2022'
where a.employee_number like '8%'          

select * from sls.personnel_payplans where employee_number like '8%'
-- remove anthony and jordyn
delete from sls.personnel_payplans
where employee_number in ('8100200','8100231');

select * from sls.team_personnel where employee_number like '8%'
-- melton termed
update sls.team_personnel
set thru_date = '08/26/2022'
where employee_number = '8100219';
update sls.personnel
set end_date = '08/26/2022'
where employee_number = '8100219';

-- close out jordyn at end of july, add all the others
update sls.personnel
set end_date = '07/31/2022'
where employee_number = '8100231';
update sls.team_personnel
set thru_date = '07/31/2022'
where employee_number = '8100231';

-- now, add the remaining 6 consultants to team_personnel & personnel_payplans
-- select * from sls.team_personnel where employee_number like '8%'
-- select * from sls.teams
insert into sls.team_personnel
select 'team anthony', employee_number, '07/12/2022', '12/31/9999'
from sls.personnel
where store_code = 'ry8'
  and last_name <> 'michael'
  and end_date > current_date;

-- select * from sls.personnel_payplans where employee_number like '8%'
insert into sls.personnel_payplans
select employee_number, 'standard', '07/12/2022', '12/31/9999'
from sls.personnel
where store_code = 'ry8'
  and last_name <> 'michael'
  and end_date > current_date;

select * 
from sls.consultant_payroll
where year_month = 202208
  and employee_number like '8%'

select psc_last_name, ssc_last_name, stock_number
from sls.deals_by_month
where year_month = 202208  
  and store_code = 'ry8'
  and psc_last_name = 'marion'
order by psc_last_name, stock_number

select * from sls.deals where stock_number = 't10136p'