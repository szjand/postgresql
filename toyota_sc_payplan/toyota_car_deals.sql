﻿/*
bopmast has RY8 deal records going back to 2016
for this payroll cut, i am limiting to deals since the acquisition
*/

------------------------------------------------------------------------------------------------------------------

CREATE TABLE sls_toy.ext_bopmast_partial
(
  store citext NOT NULL,
  bopmast_id integer NOT NULL,
  deal_status citext,
  deal_type citext,
  sale_type citext,
  sale_group citext,
  vehicle_type citext,
  stock_number citext,
  vin citext,
  odometer_at_sale integer,
  buyer_bopname_id integer,
  cobuyer_bopname_id integer,
  primary_sc citext,
  secondary_sc citext,
  fi_manager citext,
  origination_date integer,
  approved_date date,
  capped_date date,
  delivery_date date,
  gap numeric(8,2),
  service_contract numeric(8,2),
  total_care numeric(8,2),
  trade_allowance numeric(9,2),
  trade_acv numeric(9,2),
  over_allow numeric(9,2),
  leasing_source integer,
  lending_source integer,
  PRIMARY KEY (store, bopmast_id));
comment on table sls_toy.ext_bopmast_partial is 'truncated and populated nightly from luigi toyota_car_deals.py with db2 queries';
create index on sls_toy.ext_bopmast_partial(buyer_bopname_id);
create index on sls_toy.ext_bopmast_partial(capped_date);
create index on sls_toy.ext_bopmast_partial(cobuyer_bopname_id);
create index on sls_toy.ext_bopmast_partial(deal_status);
create index on sls_toy.ext_bopmast_partial(store);
create index on sls_toy.ext_bopmast_partial(vin);




------------------------------------------------------------------------------------------------------------------
drop table if exists sls_toy.ext_deals cascade;
CREATE TABLE sls_toy.ext_deals
(
  run_date date NOT NULL,
  store_code citext NOT NULL, -- source: bopmast.bopmast_company_number
  bopmast_id integer NOT NULL, -- source: bopmast.record_key
  deal_status citext NOT NULL, -- source: bopmast.record_status
  deal_type citext NOT NULL, -- source: bopmast.record_type
  sale_type citext NOT NULL, -- source: bopmast.sale_type
  sale_group citext NOT NULL, -- source: bopmast.franchise_code
  vehicle_type citext NOT NULL, -- source: bopmast.vehicle_type
  stock_number citext, -- source: bopmast.bopmast_stock_number
  vin citext, -- source: bopmast.bopmast_vin
  odometer_at_sale integer NOT NULL, -- source: bopmast.odometer_at_sale
  buyer_bopname_id integer NOT NULL, -- source: bopmast.buyer_number
  cobuyer_bopname_id integer NOT NULL, -- source: bopmast.co_buyer_number
  primary_sc citext NOT NULL, -- source: bopmast.primary_salespers
  secondary_sc citext NOT NULL, -- source: bopmast.secondary_slspers2
  fi_manager citext NOT NULL, -- source: bopmast.pd_policy_number
  origination_date date NOT NULL, -- source: bopmast.origination_date
  approved_date date NOT NULL, -- source: bopmast.date_approved
  capped_date date NOT NULL, -- source: bopmast.date_capped
  delivery_date date NOT NULL, -- source: opmast.delivery_date
  gap numeric(8,2) NOT NULL, -- source: bopmast.gap_premium
  service_contract numeric(8,2) NOT NULL, -- source: bopmast.serv_cont_amount
  total_care numeric(8,2) NOT NULL, -- source: bopmast.amo_total
  PRIMARY KEY (store_code, bopmast_id));
comment on table sls_toy.ext_deals is 'truncated and populated nightly from luigi toyota_car_deals.py from sls_toy.ext_bopmast_partial';
COMMENT ON COLUMN sls_toy.ext_deals.store_code IS 'source: bopmast.bopmast_company_number';
COMMENT ON COLUMN sls_toy.ext_deals.bopmast_id IS 'source: bopmast.record_key';
COMMENT ON COLUMN sls_toy.ext_deals.deal_status IS 'source: bopmast.record_status';
COMMENT ON COLUMN sls_toy.ext_deals.deal_type IS 'source: bopmast.record_type';
COMMENT ON COLUMN sls_toy.ext_deals.sale_type IS 'source: bopmast.sale_type';
COMMENT ON COLUMN sls_toy.ext_deals.sale_group IS 'source: bopmast.franchise_code';
COMMENT ON COLUMN sls_toy.ext_deals.vehicle_type IS 'source: bopmast.vehicle_type';
COMMENT ON COLUMN sls_toy.ext_deals.stock_number IS 'source: bopmast.bopmast_stock_number';
COMMENT ON COLUMN sls_toy.ext_deals.vin IS 'source: bopmast.bopmast_vin';
COMMENT ON COLUMN sls_toy.ext_deals.odometer_at_sale IS 'source: bopmast.odometer_at_sale';
COMMENT ON COLUMN sls_toy.ext_deals.buyer_bopname_id IS 'source: bopmast.buyer_number';
COMMENT ON COLUMN sls_toy.ext_deals.cobuyer_bopname_id IS 'source: bopmast.co_buyer_number';
COMMENT ON COLUMN sls_toy.ext_deals.primary_sc IS 'source: bopmast.primary_salespers';
COMMENT ON COLUMN sls_toy.ext_deals.secondary_sc IS 'source: bopmast.secondary_slspers2';
COMMENT ON COLUMN sls_toy.ext_deals.fi_manager IS 'source: bopmast.pd_policy_number';
COMMENT ON COLUMN sls_toy.ext_deals.origination_date IS 'source: bopmast.origination_date';
COMMENT ON COLUMN sls_toy.ext_deals.approved_date IS 'source: bopmast.date_approved';
COMMENT ON COLUMN sls_toy.ext_deals.capped_date IS 'source: bopmast.date_capped';
COMMENT ON COLUMN sls_toy.ext_deals.delivery_date IS 'source: opmast.delivery_date';
COMMENT ON COLUMN sls_toy.ext_deals.gap IS 'source: bopmast.gap_premium';
COMMENT ON COLUMN sls_toy.ext_deals.service_contract IS 'source: bopmast.serv_cont_amount';
COMMENT ON COLUMN sls_toy.ext_deals.total_care IS 'source: bopmast.amo_total';

select * from sls_toy.ext_deals
------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------
CREATE TABLE sls_toy.ext_accounting_base
(
  the_date date NOT NULL,
  control citext NOT NULL,
  amount numeric(8,2) NOT NULL,
  account citext NOT NULL,
  account_description citext NOT NULL,
  description citext NOT NULL);
comment on table sls_toy.ext_accounting_base is 'truncated and populated nightly from luigi toyota_car_deals.py with 
call to sls_toy.update_ext_accounting_base()';
create index on sls_toy.ext_accounting_base(control);  
create index on sls_toy.ext_accounting_base(the_date);

------------------------------------------------------------------------------------------------------------------

create or replace function sls_toy.update_ext_accounting_base()
returns void as
$BODY$
/*
not sure about this
GM excludes acct 144500: 144500,4,Sale,SLS AFTERMARKET NEW,RY1,Rydell GM,NC,New Vehicle,Credit
the closest i can find in toyota is 
select * 
from fin.dim_Account
where store_code = 'ry8'
  and description like '%AFTERM%'
4250,4,Sale,AFTERMARKET NEW TOYOTA F&I,RY8,Toyota,FI,Finance,Credit
4950,4,Sale,AFTERMKT CERTIFIED USED,RY8,Toyota,FI,Finance,Credit
6250,5,COGS,C/S AFTERMARKET NEW TOYOTA F&I,RY8,Toyota,FI,Finance,Debit
6950,5,COGS,C/S AFTERMKT CERTIFIED USED,RY8,Toyota,FI,Finance,Debit
have to check with jeri  
  
*/
truncate sls_toy.ext_accounting_base;

insert into sls_toy.ext_accounting_base
select d.the_date, a.control, amount,
	b.account, b.description as account_description, e.description
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
	and b.current_row = true
	and store_code = 'RY8' -- limit to toyota
-- and b.account <> '144500'
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.dim_date d on a.date_key = d.date_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where b.account_type = 'sale'
	and b.department_code in ('nc','uc','ao')
	and c.journal_code in ('vsn','vsu')
	 and a.post_status = 'Y'
	and d.the_date between '07/01/2022' and current_date;
$BODY$	
language sql;
comment on function sls_toy.update_ext_accounting_base() is 'run nightly from luigi called from toyota_car_deals.py ExtAccountingDealsTy()';
------------------------------------------------------------------------------------------------------------------
CREATE TABLE sls_toy.ext_accounting_deals
(
  run_date date NOT NULL,
  gl_date date NOT NULL,
  control citext NOT NULL,
  amount numeric(12,2),
  unit_count integer NOT NULL,
  account citext NOT NULL,
  account_description citext NOT NULL,
  gl_description citext NOT NULL,
  PRIMARY KEY (gl_date, control));
comment on table sls_toy.ext_accounting_deals is 'new rows added nightly from luigi with call to sls_toy.update_ext_accounting_deals()';

select * from sls_toy.ext_accounting_deals
------------------------------------------------------------------------------------------------------------------
create or replace function sls_toy.update_ext_accounting_deals()
returns void as
$BODY$
insert into sls_toy.ext_accounting_deals
select current_date, the_date, control, amount, unit_count,
	account, account_description, gl_description
from (  -- ccc all rows
	select aa.the_date, aa.control, sum(aa.amount) as amount, aa.account,
		aa.account_description, max(aa.description) as gl_description,
		sum(case when aa.amount < 0 then 1 else -1 end) as unit_count
	from sls_toy.ext_accounting_base aa
	left join (-- multiple rows per date to exclude
		select x.control, x.the_date
		from ( -- assign unit count to rows w/multiple rows per date
			select a.control, a.the_date,
				case when a.amount < 0 then 1 else -1 end as the_count
			from sls_toy.ext_accounting_base a
			inner join ( -- multiple rows per date
				select control, the_date
				from sls_toy.ext_accounting_base
				group by control, the_date
				having count(*) > 1) b on a.control = b.control and a.the_date = b.the_date) x
		group by x.control, x.the_date
		having sum(the_count) = 0) bb on aa.the_date = bb.the_date and aa.control = bb.control
	where bb.control is null -- this is the line that excludes the multiple entry vehicles
	group by aa.the_date, aa.control, aa.account, aa.account_description) ccc
where not exists (
	select 1
	from sls_toy.ext_accounting_deals
	where control = ccc.control
		and gl_date = ccc.the_date);
$BODY$
language sql;
comment on function sls_toy.update_ext_accounting_deals() is 'run nightly from luigi called from toyota_car_deals.py ExtAccountingDealsTy()';                   
------------------------------------------------------------------------------------------------------------------

CREATE TABLE sls_toy.xfm_deals
(
  run_date date NOT NULL, -- script run date - 1
  store_code citext NOT NULL, -- source: sls_toy.ext_deals
  bopmast_id integer NOT NULL, -- source: sls_toy.ext_deals
  deal_status citext NOT NULL, -- source: sls_toy.ext_deals
  deal_type citext NOT NULL, -- source: sls_toy.ext_deals
  sale_type citext NOT NULL, -- source: sls_toy.ext_deals
  sale_group citext NOT NULL, -- source: sls_toy.ext_deals
  vehicle_type citext NOT NULL, -- source: sls_toy.ext_deals
  stock_number citext NOT NULL, -- source: sls_toy.ext_deals
  vin citext NOT NULL, -- source: sls_toy.ext_deals
  odometer_at_sale integer NOT NULL, -- source: sls_toy.ext_deals
  buyer_bopname_id integer NOT NULL, -- source: sls_toy.ext_deals
  cobuyer_bopname_id integer NOT NULL, -- source: sls_toy.ext_deals
  primary_sc citext NOT NULL, -- source: sls_toy.ext_deals
  secondary_sc citext NOT NULL, -- source: sls_toy.ext_deals
  fi_manager citext NOT NULL, -- source: sls_toy.ext_deals
  origination_date date NOT NULL, -- source: sls_toy.ext_deals
  approved_date date NOT NULL, -- source: sls_toy.ext_deals
  capped_date date NOT NULL, -- source: sls_toy.ext_deals
  delivery_date date NOT NULL, -- source: sls_toy.ext_deals
  gap numeric(8,2) NOT NULL, -- source: sls_toy.ext_deals
  service_contract numeric(8,2) NOT NULL, -- source: sls_toy.ext_deals
  total_care numeric(8,2) NOT NULL, -- source: sls_toy.ext_deals
  row_type citext NOT NULL, -- new or update
  seq integer NOT NULL DEFAULT 1,
  gl_date date,
  gl_count integer DEFAULT 0,
  hash citext NOT NULL,
  PRIMARY KEY (store_code, bopmast_id, seq));
comment on table sls_toy.xfm_deals is 'updated nightly from luigi toyota_car_deals.py with call to sls_toy.update_xfm_deals()';
COMMENT ON COLUMN sls_toy.xfm_deals.run_date IS 'script run date - 1';
COMMENT ON COLUMN sls_toy.xfm_deals.store_code IS 'source: sls_toy.ext_deals';
COMMENT ON COLUMN sls_toy.xfm_deals.bopmast_id IS 'source: sls_toy.ext_deals';
COMMENT ON COLUMN sls_toy.xfm_deals.deal_status IS 'source: sls_toy.ext_deals';
COMMENT ON COLUMN sls_toy.xfm_deals.deal_type IS 'source: sls_toy.ext_deals';
COMMENT ON COLUMN sls_toy.xfm_deals.sale_type IS 'source: sls_toy.ext_deals';
COMMENT ON COLUMN sls_toy.xfm_deals.sale_group IS 'source: sls_toy.ext_deals';
COMMENT ON COLUMN sls_toy.xfm_deals.vehicle_type IS 'source: sls_toy.ext_deals';
COMMENT ON COLUMN sls_toy.xfm_deals.stock_number IS 'source: sls_toy.ext_deals';
COMMENT ON COLUMN sls_toy.xfm_deals.vin IS 'source: sls_toy.ext_deals';
COMMENT ON COLUMN sls_toy.xfm_deals.odometer_at_sale IS 'source: sls_toy.ext_deals';
COMMENT ON COLUMN sls_toy.xfm_deals.buyer_bopname_id IS 'source: sls_toy.ext_deals';
COMMENT ON COLUMN sls_toy.xfm_deals.cobuyer_bopname_id IS 'source: sls_toy.ext_deals';
COMMENT ON COLUMN sls_toy.xfm_deals.primary_sc IS 'source: sls_toy.ext_deals';
COMMENT ON COLUMN sls_toy.xfm_deals.secondary_sc IS 'source: sls_toy.ext_deals';
COMMENT ON COLUMN sls_toy.xfm_deals.fi_manager IS 'source: sls_toy.ext_deals';
COMMENT ON COLUMN sls_toy.xfm_deals.origination_date IS 'source: sls_toy.ext_deals';
COMMENT ON COLUMN sls_toy.xfm_deals.approved_date IS 'source: sls_toy.ext_deals';
COMMENT ON COLUMN sls_toy.xfm_deals.capped_date IS 'source: sls_toy.ext_deals';
COMMENT ON COLUMN sls_toy.xfm_deals.delivery_date IS 'source: sls_toy.ext_deals';
COMMENT ON COLUMN sls_toy.xfm_deals.gap IS 'source: sls_toy.ext_deals';
COMMENT ON COLUMN sls_toy.xfm_deals.service_contract IS 'source: sls_toy.ext_deals';
COMMENT ON COLUMN sls_toy.xfm_deals.total_care IS 'source: sls_toy.ext_deals';
COMMENT ON COLUMN sls_toy.xfm_deals.row_type IS 'new or update';

CREATE INDEX ON sls_toy.xfm_deals(hash);



------------------------------------------------------------------------------------------------------------------

create or replace function sls_toy.update_xfm_deals_new_rows()
returns void as
$BODY$
/*
TODO: hard coded gl_date
*/
insert into sls_toy.xfm_deals (run_date, store_code, bopmast_id, deal_status,
	deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
	odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
	primary_sc, secondary_sc, fi_manager, origination_Date, approved_date,
	capped_date, delivery_date, gap, service_contract, total_care,
	row_type, seq,
	gl_date, gl_count, hash)
select a.run_date, a.store_code, a.bopmast_id, a.deal_status,
	a.deal_type, a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
	a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
	a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date, a.approved_date,
	a.capped_date, a.delivery_date, a.gap, a.service_contract, a.total_care,
	'new', 1,
	coalesce(b.gl_date, '12/31/9999'), coalesce(b.unit_count, 0),
	( -- generate the hash
		select md5(z::text) as hash
		from (
			select store_code, bopmast_id, deal_status,
				deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
				odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
				primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
				capped_date, delivery_date, gap, service_contract,total_care,
				coalesce(gl_date, '12/31/9999'), coalesce(unit_count, 0)
			from sls_toy.ext_deals x
			left join sls_toy.ext_accounting_deals y on x.stock_number = y.control
				and y.gl_date = (
					select max(gl_date)
					from sls_toy.ext_accounting_deals
					where control = y.control)
			where x.store_code = a.store_code
				and x.bopmast_id = a.bopmast_id) z)
from sls_toy.ext_deals a
left join sls_toy.ext_accounting_deals b on a.stock_number = b.control
	and b.gl_date = (
		select max(gl_date)
		from sls_toy.ext_accounting_deals
		where control = b.control)
where gl_date >= '07/01/2022'
	and a.vin is not null
	and not exists (
		select *
		from sls_toy.xfm_deals
		where store_code = a.store_code
			and bopmast_id = a.bopmast_id) order by stock_number;
$BODY$
language sql;
comment on function sls_toy.update_xfm_deals_new_rows() is 'adds new rows to sls_toy.xfm_deals nightly from luigi toyota_car_deals.py';
------------------------------------------------------------------------------------------------------------------

create or replace function sls_toy.update_xfm_deals_changed_rows()
returns void as
$BODY$
/*

*/
insert into sls_toy.xfm_deals (run_date, store_code, bopmast_id, deal_status,
	deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
	odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
	primary_sc, secondary_sc, fi_manager, origination_Date, approved_date,
	capped_date, delivery_date, gap, service_contract, total_care,
	row_type, seq,
	gl_date, gl_count, hash)
select current_date, e.store_code, e.bopmast_id, e.deal_status, e.deal_type,
	e.sale_type, e.sale_group, e.vehicle_type,
	-- e.stock_number, e.vin,
	coalesce(e.stock_number, f.stock_number), coalesce(e.vin, f.vin),
	e.odometer_at_sale, e.buyer_bopname_id, e.cobuyer_bopname_id,
	e.primary_sc, e.secondary_sc, e.fi_manager, e.origination_date,
	e.approved_date, e.capped_date, e.delivery_date, e.gap,
	e.service_contract, e.total_care,
	'update'::citext,
	(
		select max(seq) + 1
		from sls_toy.xfm_deals
		where store_code = e.store_code
			and bopmast_id = e.bopmast_id) as seq,
	 e.gl_date, e.gl_count, e.hash
from (
	select z.*, md5(z::text) as hash
	from (
		select store_code, bopmast_id, deal_status,
			deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
			odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
			primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
			capped_date, delivery_date, gap, service_contract,total_care,
			coalesce(gl_date, '12/31/9999') as gl_date, coalesce(unit_count, 0) as gl_count
		from sls_toy.ext_deals x
		left join sls_toy.ext_accounting_deals y on x.stock_number = y.control
			and y.gl_date = (
				select max(gl_date)
				from sls_toy.ext_accounting_deals
				where control = y.control)) z) e
inner join sls_toy.xfm_deals f on e.store_code = f.store_code
	and f.seq = (
		select max(seq)
		from sls_toy.xfm_deals
		where store_code = f.store_code
			and bopmast_id = f.bopmast_id)
	and e.bopmast_id = f.bopmast_id
	and e.hash <> f.hash;
$BODY$
language sql;
comment on function sls_toy.update_xfm_deals_changed_rows() is 'adds changed rows to sls_toy.xfm_deals nightly from luigi toyota_car_deals.py';
------------------------------------------------------------------------------------------------------------------
create or replace function sls_toy.update_deals_deleted_rows()
returns void as
$BODY$
/*

*/
insert into sls_toy.xfm_deals (run_date, store_code, bopmast_id, deal_status,
	deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
	odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
	primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
	capped_date, delivery_date, gap, service_contract, total_care,
	row_type, seq,
	gl_date, gl_count, hash)
select current_date, a.store_code, a.bopmast_id, 'deleted'::citext as deal_status,
	a.deal_type, a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
	a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
	a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date, a.approved_date,
	a.capped_date, a.delivery_date, a.gap, a.service_contract, a.total_care,
	'update'::citext as row_type,
	(
		select max(seq) + 1
		from sls_toy.xfm_deals
		 where store_code = a.store_code
			 and bopmast_id = a.bopmast_id) as seq,
	c.gl_date, c.unit_count,
	( -- generate a new hash
		select md5(z::text) as hash
		from (
			select store_code, bopmast_id, 'deleted',
				deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
				odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
				primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
				capped_date, delivery_date, gap, service_contract,total_care,
				gl_date, gl_count
			from sls_toy.xfm_deals k
			where store_code = a.store_code
				and bopmast_id = a.bopmast_id
				and seq = (
					select max(seq)
					from sls_toy.xfm_deals
					where store_code = k.store_code
						and bopmast_id = k.bopmast_id)) z)
from sls_toy.xfm_deals a
left join sls_toy.ext_deals b on a.store_code = b.store_code
	and a.bopmast_id = b.bopmast_id
left join sls_toy.ext_accounting_deals c on a.stock_number = c.control
	and c.gl_date = (
		select max(gl_date)
		from sls_toy.ext_accounting_deals
		where control = c.control)
where b.store_code is null -- no longer in extract
	and a.seq = (
		select max(seq)
		from sls_toy.xfm_deals
		where store_code = a.store_code
			and bopmast_id = a.bopmast_id)
	and a.deal_status <> 'deleted';
$BODY$
language sql;
comment on function sls_toy.update_xfm_deals_deleted_rows() is 'adds deleted rows to sls_toy.xfm_deals nightly from luigi toyota_car_deals.py';
------------------------------------------------------------------------------------------------------------------

CREATE TABLE sls_toy.deals
(
  run_date date NOT NULL,
  store_code citext NOT NULL,
  bopmast_id integer NOT NULL,
  deal_status_code citext NOT NULL,
  deal_type_code citext NOT NULL,
  sale_type_code citext NOT NULL,
  sale_group_code citext NOT NULL,
  vehicle_type_code citext NOT NULL,
  stock_number citext NOT NULL,
  vin citext NOT NULL,
  odometer_at_sale integer NOT NULL,
  buyer_bopname_id integer NOT NULL,
  cobuyer_bopname_id integer NOT NULL,
  primary_sc citext NOT NULL,
  secondary_sc citext NOT NULL,
  fi_manager citext NOT NULL,
  origination_date date NOT NULL,
  approved_date date NOT NULL,
  capped_date date NOT NULL,
  delivery_date date NOT NULL,
  gap numeric(8,2) NOT NULL,
  service_contract numeric(8,2) NOT NULL,
  total_care numeric(8,2) NOT NULL,
  seq integer NOT NULL,
  year_month integer NOT NULL,
  unit_count numeric(3,1) NOT NULL,
  gl_date date NOT NULL,
  deal_status citext NOT NULL,
  notes citext,
  hash citext,
  PRIMARY KEY (bopmast_id, seq));
comment on table sls_toy.deals is 'updated nightly from luigi toyota_car_deals.py';
create index on sls_toy.deals(hash);
create index on sls_toy.deals(seq);
create index on sls_toy.deals(stock_number);

------------------------------------------------------------------------------------------------------------------

create or replace function sls_toy.update_deals_new_rows()
returns void as
$BODY$
/*

*/
insert into sls_toy.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
	sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
	odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
	primary_sc, secondary_sc, fi_manager, origination_date,
	approved_date, capped_date, delivery_date, gap, service_contract,
	total_care, seq, year_month, unit_count,
	gl_date, deal_status, notes, hash)
select a.run_date, a.store_code, a.bopmast_id, a.deal_status, a.deal_type,
	a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
	a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
	a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
	a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
	a.total_care,
	1 as seq,
	case
		when a.gl_date is null then
			(100* extract(year from a.capped_date) + extract(month from a.capped_date)):: integer
		else
			(100* extract(year from a.gl_date) + extract(month from a.gl_date)):: integer
	end as year_month,
	a.gl_count,
	case -- delivery date may figure into this, eventually
		when a.gl_date is null then a.capped_date
		else a.gl_date
	end as gl_date,
	case
		when a.deal_status = 'U' then 'capped'
		when a.deal_status = 'A' then 'accepted'
		else 'none'
	end as deal_status, 'new row', a.hash
from sls_toy.xfm_deals a
left join sls_toy.deals b on a.store_code = b.store_code
	and a.bopmast_id = b.bopmast_id
where a.deal_status = 'U'
	and coalesce(a.gl_date, a.capped_date) > '12/31/2021' 
	and b.store_code is null;
$BODY$
language sql;
comment on function sls_toy.update_deals_new_rows() is 'adds new rows to sls_toy.deals nightly from luigi toyota_car_deals.py';
------------------------------------------------------------------------------------------------------------------

CREATE unlogged TABLE sls_toy.changed_deals
(
  xfm_hash citext NOT NULL,
  deal_hash citext NOT NULL,
  change_type citext NOT NULL,
  PRIMARY KEY (xfm_hash, deal_hash));
comment on table sls_toy.changed_deals is 'truncated and populated nightly from luigi toyota_car_deals.py';

create unique index on sls_toy.changed_deals(deal_hash);
create unique index on sls_toy.changed_deals(xfm_hash);

------------------------------------------------------------------------------------------------------------------

create or replace function sls_toy.update_changed_deals()
returns void as
$BODY$
/*
this goofy function categorizes all the different deal changes that i have encountered
*/
truncate sls_toy.changed_deals;
insert into sls_toy.changed_deals
select c.hash as xfm_hash, d.hash as deal_hash,
	case
		-- 9/18/17: when deal status is none/none ignore
		when d.deal_status_code = 'none' and c.deal_status = 'none' then 'ignore'                      
		-- 8/8/17: exceptional anomalies to ignore, 31112A, see danger_will_robinson.sql
		when c.gl_date <> d.gl_date
			and c.gl_count < 0 and d.unit_count > 0
			and c.buyer_bopname_id = d.buyer_bopname_id
			and c.deal_status = d.deal_status_code
			and d.notes <> 'type_2_a' then 'type_2_a'  
		when c.deal_status in ('none', 'A') and d.deal_status_code = 'U' then 'type_2_b' 
		when (
					c.deal_status = 'deleted'
						and d.deal_status_code = 'U'
						and c.gl_date <> d.gl_date
						and d.notes <> 'type_2_a')
			or ( -- curious possible anomaly 31135C, 31588XA, no change in gl_date
					c.deal_status = 'deleted'
						and d.deal_status_code in ('none','A')
						and d.notes = 'type_2_b')
			then 'type_2_c' 
		when d.notes = 'type_2_a' and c.deal_status = 'deleted' then 'type_2_d'
		when d.notes = 'type_2_a' and c.gl_count > 0 and c.bopmast_id = d.bopmast_id then 'type_2_e'
		when (d.notes = 'type_2_b' or d.notes = 'type_2_h') and c.gl_count > 0 and c.deal_status = 'U' then 'type_2_f'
		when c.deal_status = 'deleted' and d.deal_status_code = 'U' then 'type_2_g'
		when c.gl_date = d.gl_date and c.deal_status = 'A' and d.deal_status_code = 'A'
			and d.notes = 'type_2_b' and c.gl_count = 1 and d.unit_count = -1 then 'type_2_h'
		when d.notes = 'type_2_b' and d.deal_status_code = 'none' 
				and c.deal_status = 'A' then 'do nothing'
		when c.deal_status <> d.deal_status_code
			or c.gl_count <> d.unit_count
			or (c.gl_date <> d.gl_date
				and -- 29164B: same year_month and no change in status and no change in count = type 1
						(extract(year from c.gl_date) * 100) + extract(month from c.gl_date) <> 
								(extract(year from d.gl_date) * 100) + extract(month from d.gl_date)
						OR c.deal_status <> d.deal_status_code
						or c.gl_count <> d.unit_count)
				 then 'DANGER WILL ROBINSON'
		else 'type_1'
	end as change_type
from ( -- c: most recent xfm rows 1 row per store/bopmast_id
	select *
	from sls_toy.xfm_deals a
	where a.seq = (
		select max(seq)
		from sls_toy.xfm_deals
		where store_code = a.store_code
			and bopmast_id = a.bopmast_id)) c
inner join (-- d: most recent deals rows 1 row per store/bopmast_id
	select *
	from sls_toy.deals a
	where a.deal_status <> 'deleted'
		and a.seq = (
			select max(seq)
			from sls_toy.deals
			where store_code = a.store_code
				and bopmast_id = a.bopmast_id)) d on c.store_code = d.store_code
		and c.bopmast_id = d.bopmast_id
	and c.hash <> d.hash;
$BODY$
language sql;
comment on function sls_toy.update_changed_deals() is 'luigi toyota_car_deals.py, nightly truncates and populates a table of deal change categorizations which is 
  subsequently used to update sls_toy.deals';
------------------------------------------------------------------------------------------------------------------
create or replace function sls_toy.implement_deal_changes()
returns void as
$BODY$
/*

*/
-- type_1 changes
update sls_toy.deals w
set run_date = x.run_date,
		store_code = x.store_code,
		bopmast_id = x.bopmast_id,
		deal_status_code = x.deal_status_code,
		deal_type_code = x.deal_type,
		sale_type_code = x.sale_type,
		sale_group_code = x.sale_group,
		vehicle_type_code = x.vehicle_type,
		stock_number = x.stock_number,
		vin = x.vin,
		odometer_at_sale = x.odometer_at_sale,
		buyer_bopname_id = x.buyer_bopname_id,
		cobuyer_bopname_id = x.cobuyer_bopname_id,
		primary_sc = x.primary_sc,
		secondary_sc = x.secondary_sc,
		fi_manager = x.fi_manager,
		origination_date = x.origination_date,
		approved_date = x.approved_date,
		capped_date = x.capped_date,
		delivery_date = x.delivery_date,
		gap = x.gap,
		service_contract = x.service_contract,
		total_care = x.total_care,
		seq = x.seq,
		year_month = x.year_month,
		unit_count = x.gl_count,
		gl_date = x.gl_date,
		deal_status = x.deal_status,
		notes = x.notes,
		hash = x.hash
from (
	select a.run_date, a.store_code, a.bopmast_id, a.deal_status as deal_status_code, a.deal_type,
		a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
		a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
		a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
		a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
		a.total_care,
		b.seq,
		b.year_month, a.gl_count, a.gl_date, b.deal_status, 'type_1' as notes, a.hash
	from sls_toy.changed_deals z
	inner join  sls_toy.xfm_deals a on z.xfm_hash = a.hash
		and z.change_type = 'type_1'
	inner join sls_toy.deals b on z.deal_hash = b.hash
		and z.change_type = 'type_1') x
where w.store_code = x.store_code
	and w.bopmast_id = x.bopmast_id
	and w.seq = x.seq;
	
-- type_2_a
insert into sls_toy.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
	sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
	odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
	primary_sc, secondary_sc, fi_manager, origination_date,
	approved_date, capped_date, delivery_date, gap, service_contract,
	total_care, seq, year_month, unit_count,
	gl_date, deal_status, notes, hash)
select a.run_date, a.store_code, a.bopmast_id, a.deal_status as deal_status_code, a.deal_type,
	a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
	a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
	a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
	a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
	a.total_care,
	(select max(seq) + 1 from sls_toy.deals where store_code = b.store_code
		and bopmast_id = b.bopmast_id) as seq,
	(select year_month from dds.dim_date where the_date = a.gl_date) as year_month,
	a.gl_count,
	a.gl_date, a.deal_status, 'type_2_a' as notes,
	a.hash
from sls_toy.changed_deals z
inner join  sls_toy.xfm_deals a on z.xfm_hash = a.hash
	and z.change_type = 'type_2_a'
inner join sls_toy.deals b on z.deal_hash = b.hash
	and z.change_type = 'type_2_a';	
	
-- type_2_b
insert into sls_toy.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
	sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
	odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
	primary_sc, secondary_sc, fi_manager, origination_date,
	approved_date, capped_date, delivery_date, gap, service_contract,
	total_care, seq, year_month, unit_count,
	gl_date, deal_status, notes, hash)
select a.run_date, a.store_code, a.bopmast_id, a.deal_status as deal_status_code, a.deal_type,
	a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
	a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
	a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
	a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
	a.total_care,
	(select max(seq) + 1 from sls_toy.deals where store_code = b.store_code and
		bopmast_id = b.bopmast_id) as seq,
	-- (select year_month from dds.dim_date where the_date = a.gl_date),
	(select year_month
		from dds.dim_date
		where the_date =
			case
				when a.gl_date = '12/31/9999' then a.run_date
				else a.gl_date
			end),
	-1 as unit_count,
	a.gl_date,
	case
		when a.deal_status = 'U' then 'capped'
		when a.deal_status = 'A' then 'accepted'
		else 'none'
	end as deal_status, 'type_2_b' as notes,
	a.hash
from sls_toy.changed_deals z
inner join  sls_toy.xfm_deals a on z.xfm_hash = a.hash
	and z.change_type = 'type_2_b'
inner join sls_toy.deals b on z.deal_hash = b.hash
	and z.change_type = 'type_2_b';
	
	-- type_2_c
insert into sls_toy.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
	sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
	odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
	primary_sc, secondary_sc, fi_manager, origination_date,
	approved_date, capped_date, delivery_date, gap, service_contract,
	total_care, seq, year_month, unit_count,
	gl_date, deal_status, notes, hash)
select a.run_date, a.store_code, a.bopmast_id, a.deal_status as deal_status_code, a.deal_type,
	a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
	a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
	a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
	a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
	a.total_care,
	(select max(seq) + 1 from sls_toy.deals where store_code = b.store_code
		and bopmast_id = b.bopmast_id) as seq,
	(100* extract(year from a.gl_date) + extract(month from a.gl_date)):: integer as year_month,
	-1 as unit_count,
	a.gl_date, 'deleted' as deal_status, 'type_2_c' as notes,
	a.hash
from sls_toy.changed_deals z
inner join  sls_toy.xfm_deals a on z.xfm_hash = a.hash
	and z.change_type = 'type_2_c'
inner join sls_toy.deals b on z.deal_hash = b.hash
	and z.change_type = 'type_2_c';	
	
-- type_2_d
update sls_toy.deals a
set run_date = x.run_date,
		deal_status_code = 'deleted',
		deal_status = 'deleted',
		notes = 'type_2_d'
from (
	select n.run_date, m.deal_hash
	from sls_toy.changed_deals m
	inner join sls_toy.xfm_deals n on m.xfm_hash = n.hash
	where m.change_type = 'type_2_d') x
where a.hash = x.deal_hash;	

-- type_2_e
insert into sls_toy.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
	sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
	odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
	primary_sc, secondary_sc, fi_manager, origination_date,
	approved_date, capped_date, delivery_date, gap, service_contract,
	total_care, seq, year_month, unit_count,
	gl_date, deal_status, notes, hash)
select a.run_date, a.store_code, a.bopmast_id, a.deal_status as deal_status_code, a.deal_type,
	a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
	a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
	a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
	a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
	a.total_care,
	(select max(seq) + 1 from sls_toy.deals where store_code = b.store_code
		and bopmast_id = b.bopmast_id) as seq,
	(select year_month from dds.dim_date where the_date = a.gl_date) as year_month,
	a.gl_count,
	a.gl_date,
	case
		when a.deal_status = 'U' then 'capped'
		when a.deal_status = 'A' then 'accepted'
		else 'none'
	end as deal_status,
	'type_2_e' as notes,
	a.hash
from sls_toy.changed_deals z
inner join  sls_toy.xfm_deals a on z.xfm_hash = a.hash
	and z.change_type = 'type_2_e'
inner join sls_toy.deals b on z.deal_hash = b.hash
	and z.change_type = 'type_2_e';
                      
-- type_2_f
insert into sls_toy.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
	sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
	odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
	primary_sc, secondary_sc, fi_manager, origination_date,
	approved_date, capped_date, delivery_date, gap, service_contract,
	total_care, seq, year_month, unit_count,
	gl_date, deal_status, notes, hash)
select a.run_date, a.store_code, a.bopmast_id, a.deal_status as deal_status_code, a.deal_type,
	a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
	a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
	a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
	a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
	a.total_care,
	(select max(seq) + 1 from sls_toy.deals where store_code = b.store_code
		and bopmast_id = b.bopmast_id) as seq,
	(select year_month from dds.dim_date where the_date = a.gl_date) as year_month,
	a.gl_count,
	a.gl_date,
	case
		when a.deal_status = 'U' then 'capped'
		when a.deal_status = 'A' then 'accepted'
		else 'none'
	end as deal_status,
	'type_2_f' as notes,
	a.hash
from sls_toy.changed_deals z
inner join  sls_toy.xfm_deals a on z.xfm_hash = a.hash
	and z.change_type = 'type_2_f'
	and a.run_date = (
		select max(run_date)
		from sls_toy.xfm_deals
		where store_code = a.store_code
			and bopmast_id = a.bopmast_id)
inner join sls_toy.deals b on z.deal_hash = b.hash
	and z.change_type = 'type_2_f';
                      
-- type_2_g
insert into sls_toy.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
	sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
	odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
	primary_sc, secondary_sc, fi_manager, origination_date,
	approved_date, capped_date, delivery_date, gap, service_contract,
	total_care, seq, year_month, unit_count,
	gl_date, deal_status, notes, hash)
select a.run_date, a.store_code, a.bopmast_id, a.deal_status as deal_status_code, a.deal_type,
	a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
	a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
	a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
	a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
	a.total_care,
	(select max(seq) + 1 from sls_toy.deals where store_code = b.store_code
		and bopmast_id = b.bopmast_id) as seq,
	(select year_month from dds.dim_date where the_date = a.run_date),
	-1 as unit_count,
	a.gl_date, 'deleted' as deal_status,
	'type_2_g' as notes,
	a.hash
from sls_toy.changed_deals z
inner join  sls_toy.xfm_deals a on z.xfm_hash = a.hash
	and z.change_type = 'type_2_g'
	and a.run_date = (
		select max(run_date)
		from sls_toy.xfm_deals
		where store_code = a.store_code
			and bopmast_id = a.bopmast_id)
inner join sls_toy.deals b on z.deal_hash = b.hash
	and z.change_type = 'type_2_g';
                      
-- type_2_h
insert into sls_toy.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
	sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
	odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
	primary_sc, secondary_sc, fi_manager, origination_date,
	approved_date, capped_date, delivery_date, gap, service_contract,
	total_care, seq, year_month, unit_count,
	gl_date, deal_status, notes, hash)
select a.run_date, a.store_code, a.bopmast_id, b.deal_status_code, a.deal_type,
	a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
	a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
	a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
	a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
	a.total_care,
	(select max(seq) + 1 from sls_toy.deals where store_code = b.store_code
		and bopmast_id = b.bopmast_id) as seq,
	b.year_month,
	0 as unit_count,
	b.gl_date, b.deal_status,
	'type_2_h' as notes,
	a.hash
from sls_toy.changed_deals z
inner join  sls_toy.xfm_deals a on z.xfm_hash = a.hash
	and z.change_type = 'type_2_h'
	and a.run_date = (
		select max(run_date)
		from sls_toy.xfm_deals
		where store_code = a.store_code
			and bopmast_id = a.bopmast_id)
inner join sls_toy.deals b on z.deal_hash = b.hash
	and z.change_type = 'type_2_h';
                      
-- type_2_i
insert into sls_toy.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
	sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
	odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
	primary_sc, secondary_sc, fi_manager, origination_date,
	approved_date, capped_date, delivery_date, gap, service_contract,
	total_care, seq, year_month, unit_count,
	gl_date, deal_status, notes, hash)
select a.run_date, a.store_code, a.bopmast_id, a.deal_status, a.deal_type,
	a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
	a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
	a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
	a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
	a.total_care,
	(select max(seq) + 1 from sls_toy.deals where store_code = b.store_code
		and bopmast_id = b.bopmast_id) as seq,
	b.year_month,
	1 as unit_count,
	b.gl_date,
	'capped' as deal_status,
	'type_2_i' as notes,
	a.hash
from sls_toy.changed_deals z
inner join  sls_toy.xfm_deals a on z.xfm_hash = a.hash
	and z.change_type = 'type_2_i'
	and a.run_date = (
		select max(run_date)
		from sls_toy.xfm_deals
		where store_code = a.store_code
			and bopmast_id = a.bopmast_id)
inner join sls_toy.deals b on z.deal_hash = b.hash
	and z.change_type = 'type_2_i';
                      
$BODY$
language sql;
comment on function sls_toy.implement_deal_changes() is 'runs nightly from luigi toytoa_car_deals.py class DealsImplementChangesTy';
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------

