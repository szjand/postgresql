﻿TOYOTA SALES CONSULTANTS UKG IMPORT :: https://beta.rydellvision.com/pay-roll/ukg-imports/???
^
|
rydell@betarydellvision:~/new-vision/lib$ cat plugins/ukg-imports.js
        path: '/gm-sales-consultant-payrolls',
        const query = 'select * from  ukg.get_gm_sales_payroll($1) as result';
^
|
sls.get_sales_consultant_payroll(_pay_period_ind,'RY1')


select * from sls.get_sales_consultant_payroll(202208,'RY8') order by consultant

-- it appears that they have been paid a $1250 draw on 8/15
select c.pay_date, c.payroll_name, b.last_name, b.first_name, b.employee_number, a.earning_code, a.earning_name, a.ee_amount, a.type_name, a.cost_center_name   
from ukg.pay_statement_earnings a
join ukg.employees b on a.employee_account_id = b.employee_id
  and b.employee_number in ('8100204','8100207','8100218','8100220','8100222','8100229')      
join ukg.payrolls c on a.payroll_id = c.payroll_id  
order by b.last_name, c.pay_date  

select * from ukg.payrolls limit 10
select distinct ein_name from ukg.payrolls

comment on table sls.consultant_payroll is 'updated nightly from luigi RunAll->sc_payroll_201803.ConsultantPayroll()';

select * 
from sls.consultant_payroll
where employee_number like '1%'
  and year_month = 202208
  
-- rework function sls.get_sales_consultant_payroll(202208,'RY1') 
-- guarantee is different, based on daily guarantee in august & september
/*
from cahalan 8/3/22
Cole Suedel - Standard - $266.36

Jace Marion - Standard - $339.46

Bradley Console - Standard - $173.22

Jordyn Greer - Executive - no guarantee

Thomas Moore - Standard - $185.82

David Riddle - Standard - $233.64

Mason Hagen - Standard - $170.66
*/
select * from sls.get_sales_consultant_payroll(202208,'RY8') order by consultant  

create table sls.toyota_sales_consultant_guarantee (
  employee_number citext not null,
  daily_guarantee numeric not null,
  primary key (employee_number));
create table sls.toyota_days_worked (
  employee_number citext not null,
  year_month integer not null,
  days_worked integer not null,
  primary key(employee_number,year_month));

insert into sls.toyota_Sales_consultant_guarantee values
('8100229',266.36),
('8100218',339.46),
('8100204',173.22),
('8100220',185.82),
('8100222',233.64),
('8100207',170.66);

select first_name, last_name, daily_guarantee
from ukg.employees a
join sls.toyota_sales_consultant_guarantee b on a.employee_number = b.employee_number

insert into sls.toyota_days_worked values
('8100229',202208,23),    
('8100218',202208,23), 
('8100204',202208,23), 
('8100220',202208,23), 
('8100222',202208,23), 
('8100207',202208,23);

create or replace function sls.get_toyota_sales_consultant_payroll(_year_month integer)
  RETURNS TABLE(employee_number citext, consultant text, plan citext, units numeric, unit_pay numeric, fi_pay numeric, 
		total_earned numeric, draw numeric, additional_comp numeric, guarantee numeric, due numeric, due_with_pto numeric, 
		pto_pay numeric, hol_pay numeric, pulse numeric) AS
$BODY$
/*
select * from sls.get_toyota_sales_consultant_payroll(202208) order by consultant
*/
select a.employee_number as id, a.last_name || ', ' || a.first_name as consultant, a.payplan as plan, a.unit_count as units,
			a.unit_pay, a.fi_pay, a.total_earned, a.draw, e.additional_comp,
			round(i.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1), 2) as guarantee, 
			round (
				case 
					when a.total_earned >= i.guarantee then
						a.total_earned - coalesce(a.draw, 0)  + coalesce(e.additional_comp, 0)  - coalesce(a.pto_pay,0) - coalesce(a.hol_pay)
					else
						(i.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) 
							 + coalesce(e.additional_comp, 0)  - coalesce(a.pto_pay,0) -  coalesce(a.hol_pay)
					end, 2) as due, 
			 round (
				case 
					when a.total_earned >= i.guarantee then
						a.total_earned - coalesce(a.draw, 0)  + coalesce(e.additional_comp, 0) 
					else
						(i.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) 
								 + coalesce(e.additional_comp, 0) 
					end, 2) as due_with_pto, 
					coalesce(a.pto_pay,0) as pto_pay, coalesce(a.hol_pay, 0) as hol_pay, pulse::numeric
		from sls.consultant_payroll a
		left join (
			select employee_number, sum(amount) as adjusted_amount
			from sls.payroll_adjustments
			where year_month = _year_month
			group by employee_number) d on a.employee_number = d.employee_number
		left join (
			select employee_number, sum(amount) as additional_comp
			from sls.additional_comp
			where thru_date > (
				select first_of_month
				from sls.months
				where open_closed = 'open')
			group by employee_number) e on a.employee_number = e.employee_number  
		left join sls.personnel f on a.employee_number = f.employee_number
		left join ( -- if this is the first month of employment, multiplier = remaining wd in month since hire date/work days in month
			select a.employee_number, 
				round(wd_of_month_remaining::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as guarantee_multiplier 
			from sls.personnel a
			inner join dds.dim_date b on a.start_date = b.the_date
				and b.year_month = _year_month) g on f.employee_number = g.employee_number    
		left join ( -- if employee termed mid month, multiplier elapsed wd in month at term date/work days in month
			select a.employee_number, 
				round(wd_of_month_elapsed::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as term_guarantee_multiplier
			from sls.personnel a
			inner join dds.dim_date b on a.end_date = b.the_date
				and b.year_month = _year_month ) h on f.employee_number = h.employee_number 
		left join (
			select a.employee_number, a.daily_guarantee * b.days_worked as guarantee
			from sls.toyota_Sales_consultant_guarantee a
			join sls.toyota_days_worked b on a.employee_number = b.employee_number
				and b.year_month = _year_month) i on a.employee_number = i.employee_number                 
		where a.year_month = _year_month
		and f.store_code = 'RY8';
$BODY$
language sql;
comment on function sls.get_toyota_sales_consultant_payroll(integer) is 'called from ukg.get_toyota_sales_payroll() to populate the ukg imports page';



-- and now the function to populate the page
CREATE OR REPLACE FUNCTION ukg.get_toyota_sales_payroll()
  RETURNS SETOF jsonb AS
$BODY$
/*
select * from ukg.get_toyota_sales_payroll()
*/

declare
	_pay_period_ind integer :=(
	select current_pay_period
	from ukg.get_current_pay_periods()
	where pay_plan_type = 'Payroll');

begin
	return query
	select jsonb_build_object('gm_sales_consultant_payroll', jsonb_agg(to_jsonb(A)), 'ukg_adjustment', (
	select jsonb_agg(to_jsonb(B))
	from ( --- B
		select adjustment_id as id,a.*, (select ukg.get_pretty_payroll_names(time_period,pay_plan_type)) as pretty_payroll_name
		from ukg.pay_plan_adjustments a
		inner join sls.get_toyota_sales_consultant_payroll(_pay_period_ind) b on a.employee_number = b.employee_number
		where a.pay_period_indicator = _pay_period_ind
		) B
	))
	from (

	select b.user_key as employee, a.employee_number as id, plan, units, unit_pay, fi_pay, total_earned + coalesce(sum(amount),0 ) + coalesce(additional_comp,0) as total_earned, 
		draw, additional_comp, guarantee, due, pulse, 
		pto_pay, 
		hol_pay as holiday_pay,
		due_with_pto + coalesce(sum(amount),0) + coalesce(additional_comp,0) as due_with_pto, 
		case 
			when (total_earned + coalesce(sum(amount),0) + coalesce(additional_comp,0)) >= guarantee then 0 
			else (guarantee - (total_earned + coalesce(sum(amount),0) + coalesce(additional_comp,0))) 
		end as guarantee_diff, -- spiffs,
		case 
			when count(adjustment_id) = 0 then '{}' 
			else coalesce(array_agg(adjustment_id),'{}') 
		end as adjustments, sum(amount) as adjustment_amount,
		case 
			when  (total_earned + coalesce(sum(amount),0) + coalesce(additional_comp,0)) > guarantee then (total_earned + coalesce(sum(amount),0) + coalesce(additional_comp,0) ) - pto_pay - draw - hol_pay
			else guarantee - pto_pay - draw - hol_pay
		end as ukg_due,
		case 
			when  (total_earned + coalesce(sum(amount),0) + coalesce(additional_comp,0) ) > guarantee then total_earned + coalesce(additional_comp,0) - pto_pay - draw - hol_pay
			else guarantee - pto_pay - draw - hol_pay - coalesce(sum(amount),0)
		end as ukg_due_without_adjustments
	from sls.get_toyota_sales_consultant_payroll(_pay_period_ind) a
	inner join nrv.users b on a.employee_number = b.employee_number
	left join ukg.pay_plan_adjustments c on a.employee_number = c.employee_number and c.pay_period_indicator = _pay_period_ind
	group by b.user_key, a.employee_number, plan, units, unit_pay, fi_pay, total_earned, draw, additional_comp, guarantee,pto_pay, hol_pay, due,pulse, due_with_pto) A;
end
$BODY$
  LANGUAGE plpgsql;
COMMENT ON FUNCTION ukg.get_toyota_sales_payroll() IS 'Populates the Toyota Sales Consultants page in UKG Imports';
