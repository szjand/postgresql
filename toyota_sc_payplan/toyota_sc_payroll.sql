﻿/*
08/26
	recreating sales for toyota sales consultants, don't want to fuck with existing stuff for GM & HN
	will model this on their same structure, but data will all be separate
	will try to put it all in toyota_sc_payroll.py

08/27/22
  single file didn't work out
  needs car deals, which is all the new/changed deal stuff
  so, this file will be for everything in toyota_sc_payroll.py
  
*/
create schema sls_toy;
comment on schema sls_toy is 'schema for toyota sales data, starting with consultant payroll';
------------------------------------------------------------------------------------------------------------------

CREATE TABLE sls_toy.deals_accounts_routes
(
  store_code citext NOT NULL,
  page integer NOT NULL,
  line integer NOT NULL,
  gl_account citext NOT NULL,
  PRIMARY KEY (page, line, gl_account));
create index on sls_toy.deals_accounts_routes(gl_account);
comment on table sls_toy.deals_accounts_routes is 'truncated and repopulated nightly in luigi toyota_sc_payroll.py with a query to db2';

select * 
from sls_toy.deals_accounts_routes a
join fin.dim_Account b on a.gl_account = b.account
where a.page in (6,7,8)
order by a.page, a.line

select * from sls_toy.deals_accounts_routes
------------------------------------------------------------------------------------------------------------------

-- ot uc after recon
-- in toyota factory account = gl account
this is for gm account 446B OT used cars
so gm statement P6 L2 = toy statement P6 L47 & 56

select * 
from fin.dim_account
where account in ( '4359','4369','4368','4384','4314','4396','4366','4394','4377','4364','4362')

select a.control, a.amount, c.account, sum(amount) over ()
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 202207
join fin.dim_Account c on a.account_key = c.account_key
  and c.account in  ( '4359','4369','4368','4384','4314','4396','4366','4394','4377','4364','4362')
where a.post_status = 'Y'
   
so it looks like i need to do a couple step translation to get toyota account/gm fncl line match up
the page and line numbers in eisglobal.sypffxmst are for the toyota statement

will try to do it in the db2 query that generates sls_toy.deals_accounts_routes
ffpxrefdta has 2 rows for each toyota account, one with factory_code = GM where the factory_account is the gm account, 
  eg, g_l_acct_number = 4384, factory_account = 446B
  and one with factory_code = TOY where the factory_account = gl_account, eg, 4384

ok, i think ive got it
------------------------------------------------------------------------------------------------------------------


CREATE TABLE sls_toy.deals_accounting_detail
(
  year_month integer NOT NULL,
  control citext NOT NULL,
  journal_code citext NOT NULL,
  account_type citext NOT NULL,
  department citext NOT NULL,
  store_code citext NOT NULL,
  page integer NOT NULL,
  line integer NOT NULL,
  gl_account citext NOT NULL,
  account_description citext NOT NULL,
  trans_description citext NOT NULL,
  gross_category citext NOT NULL,
  amount numeric(12,2) NOT NULL,
  PRIMARY KEY (year_month, control, gl_account, journal_code, trans_description));
comment on table sls_toy.deals_accounting_detail is 'updated nightly from luigi with a call to sls_toy.update_deals_accounting_detail()';
------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION sls_toy.update_deals_accounting_detail()
  RETURNS void AS
$BODY$

/*

*/
  delete 
  from sls_toy.deals_accounting_detail
  where year_month = (
    select year_month 
    from sls.months
    where open_closed = 'open');

  insert into sls_toy.deals_accounting_detail
  select c.year_month, a.control,
    aa.journal_code, b.account_type, b.department,
    d.store_code, d.page, d.line, d.gl_Account, b.description as account_description,
    e.description as trans_description,
    max(
      case
        when page = 17 and line in (3,13) then 'fi_chargeback'
        when page = 17 then 'fi'
        else 'front'
      end) as gross_category,
    sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
  inner join dds.dim_date c on a.date_key = c.date_key
  inner join fin.dim_account b on a.account_key = b.account_key
    and c.the_date between b.row_from_Date and b.row_thru_date
  inner join sls_toy.deals_accounts_routes d on b.account = d.gl_account
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where post_status = 'Y'
    and c.year_month = (select year_month from sls.months where open_closed = 'open')  
  group by b.description, c.year_month, a.control,
    aa.journal_code, b.account_type, b.department,
    d.store_code, d.page, d.line, d.gl_Account, e.description;    
    
$BODY$
  LANGUAGE sql;

COMMENT ON FUNCTION sls_toy.update_deals_accounting_detail() IS 'update accounting data for current month deals based on financial statement accounts, called nightly
from luigi toyota_sc_payroll.py';

select * from sls_toy.deals_accounting_detail where year_month = 202207 and page = 14

------------------------------------------------------------------------------------------------------------------

CREATE TABLE sls_toy.deals_gross_by_month
(
  year_month integer NOT NULL,
  control citext NOT NULL,
  front_sales numeric(8,2) NOT NULL,
  front_cogs numeric(8,2) NOT NULL,
  front_gross numeric(8,2) NOT NULL,
  fi_sales numeric(8,2) NOT NULL,
  fi_cogs numeric(8,2) NOT NULL,
  fi_gross numeric(8,2) NOT NULL,
  fi_acq_fee numeric(8,2) NOT NULL DEFAULT 0,
  PRIMARY KEY (year_month, control));
create index on sls_toy.deals_gross_by_month(control);
comment on table sls_toy.deals_gross_by_month is 'updated nightly from luigi with a call to sls_toy.update_deals_gross_by_month()';
------------------------------------------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION sls_toy.update_deals_gross_by_month()
  RETURNS void AS
$BODY$

/*

6/23/19: 
  turns out this is calculating fi_gross incorreclty, there are transactions in accounts 180601, 180801, 180803
  that are acquisition fees. for sub prime deals, these fees are large, per ben, a-z (executive pay plan)
  consultants should not be charged for these fees. So fi gross becomes contigent upon who does the
  fi for a deal. 
  add attribute sls.deals_gross_by_month.fi_acq_fee, set fi_gross to zero.
  gross will be calculated where needed, eg sls.update_consultant_payroll()
08/26/22
	-- what is the toyota version os accounts 180601,180801,180803
	select * from sls.deals_accounts_routes where gl_account in ('180601','180801','180803')
	P17 Lines 1, 11
	select * from sls.deals_accounts_routes where page = 17 and line in (1,11) 
	select * from sls_toy.deals_accounts_routes where page = 17 and line in (1,11) 

	select * from fin.dim_Account where account in ('180601','180801','180803') and current_row
	union
	select * from fin.dim_Account where account in ('6280','6282','6180','6380','6382') and current_row
	order by store_code, account
	-- looking for toyota accounts with dealer acquisition fee as description
	-- !!! there are none and in addition there are no page 17 accounts where type is Other Expense
	select * 
	from sls_toy.deals_accounts_routes a
	join fin.dim_account b on a.gl_account = b.account
	--   and b.description like '%acq%'
	where a.page = 17
	order by a.line  

	select sls_toy.update_deals_gross_by_month();

	select * from sls_toy.deals_gross_by_month
*/

  delete 
  from sls_toy.deals_gross_by_month
  where year_month = (
    select year_month 
    from sls.months
    where open_closed = 'open');

    insert into sls_toy.deals_gross_by_month
    select year_month, control,
      sum(case when gross_category = 'front' and account_type in ('sale', 'Other Income')
        then -amount else 0 end) as front_sales,
      sum(case when gross_category = 'front' and account_type = 'cogs'
        then amount else 0 end) as front_cogs,
      sum(case when gross_category = 'front' then -amount else 0 end) as front_gross,
      sum(case when gross_category = 'fi' and account_type in ('sale', 'Other Income')
        then -amount else 0 end) as fi_sales,
      sum(case when gross_category = 'fi' and account_type = 'cogs' then amount else 0 end) as fi_cogs,
      0 as fi_gross
-- see the comments, no toyota accounts match these 3 from GM      
--       coalesce(sum(amount) filter (where gl_account in ('180601','180801','180803')), 0) as acq_fee
    from sls_toy.deals_accounting_detail
    where year_month = (select year_month from sls.months where open_closed = 'open')
    group by year_month, control;
  
$BODY$
  LANGUAGE sql;
COMMENT ON FUNCTION sls_toy.update_deals_gross_by_month() IS 'aggregate sls_toy.deals_accounting_detail by control/year_month into sls_toy.deals_gross_by_month, called nightly
from luigi toyota_sc_payroll.py';

select * from sls_toy.deals_gross_by_month

delete from sls_toy.deals_gross_by_month where year_month = 202208
------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
