﻿select 'ads', tech, writer, customer, vehicle, ccc, ro_comment, opcode
from (
  select max(techkey) as tech from ads.ext_dim_tech) a
join (
  select max(servicewriterkey) as writer from ads.ext_dim_service_writer) b on true  
join (
  select max(customerkey) as customer from ads.ext_dim_customer) c on true    
join (
  select max(vehiclekey) as vehicle from ads.ext_dim_vehicle) d on true     
join (
  select max(ccckey) as ccc from ads.ext_dim_ccc) e on true   
join (
  select max(rocommentkey) as ro_comment from ads.ext_dim_ro_comment) f on true   
join (
  select max(opcodekey) as opcode from ads.ext_dim_opcode) g on true    
union
select 'dds', tech, writer, customer, vehicle, ccc, ro_comment, opcode
from (
  select max(tech_key) as tech from dds.dim_tech) aa 
join (
  select max(service_writer_key) as writer from dds.dim_service_writer) bb on true
join (
  select max(customer_key) as customer from dds.dim_customer) cc on true
join (
  select max(vehicle_key) as vehicle from dds.dim_vehicle) dd on true
join (
  select max(ccc_key) as ccc from dds.dim_ccc) ee on true
join (
  select max(ro_comment_key) as ro_comment from dds.dim_ro_comment) ff on true    
join (
  select max(opcode_key) as opcode from dds.dim_opcode) gg on true 

/*  SDPRDET */
Column: PTLTYP
Values:                 A  is a Header Record
                        I  Internal Deductible
                        L  Labor Line                           
                        M  Paint/Materials
                        N  Sublet Line
                        Q  Discount
                        W  Hazardous Materials

Column:  PTCODE
Values:                 SC  Service Contract Info Line
                        SL  Sublet Sale Type
                        PM  Paint/Materials Line
                        PO  Purchase Order Line                         
                        CP  Customer Pay Info Line
                        PR  Estimate Data
                        CR  Correction Data
                        WS  Warranty Info Pay Line
                        IS  Internal Flag Info          
                        TT  Tech Time Flag
                        HZ  Hazardous Materials Line

Column:  PTDBAS
Values:                 X   Exclude from Tech Time Report
                        V   Voided Tech time


-- 7/15/20
-- turns out dds.dim_ccc and dds.dim_ro_comment were missing some rows
-- re defined the table with those fields as integer, added the missing rows and restored structure                        
drop table if exists dds.fact_repair_order cascade;
CREATE TABLE dds.fact_repair_order ( 
      store_code citext not null,
      ro citext not null,
      line Integer not null,
      tag citext not null,
      open_date date not null references dds.dim_date(the_date),
      close_date date not null references dds.dim_date(the_date),
      final_close_date date not null references dds.dim_date(the_date),
      line_date date not null references dds.dim_date(the_date),
      flag_date date not null references dds.dim_date(the_date),
      service_writer_key Integer not null references dds.dim_service_writer(service_writer_key),
      customer_key Integer not null references dds.dim_customer(customer_key),
      vehicle_key Integer not null references dds.dim_vehicle(vehicle_key),
      service_type_key Integer not null references dds.dim_service_type(service_type_key),
      payment_type_key Integer not null references dds.dim_payment_type(payment_type_key),
      ccc_key Integer, -- not null references dds.dim_ccc(ccc_key),
      ro_comment_key Integer, --  not null references dds.dim_ro_comment(ro_comment_key),
      opcode_key Integer not null references dds.dim_opcode(opcode_key),
      ro_status_key Integer not null references dds.dim_ro_status(ro_status_key),
      line_status_key Integer not null references dds.dim_line_status(line_status_key),
      ro_labor_sales numeric(8,2) not null,
      ro_parts_sales numeric(8,2) not null,
      flag_hours numeric(8,2) not null,
      ro_flag_hours numeric(8,2) not null,
      miles Integer not null,
      ro_created_ts TimeStamptz not null,
      sublet numeric(8,2) not null,
      ro_shop_supplies numeric(8,2) not null,
      ro_discount numeric(8,2) not null,
      ro_hazardous_materials numeric(8,2) not null,
      ro_paint_materials numeric(8,2) not null,
      cor_code_key Integer not null,
      tech_key Integer not null,
      labor_sales numeric(8,2) not null ); 
create index on dds.fact_repair_order(store_code);
create index on dds.fact_repair_order(ro);
create index on dds.fact_repair_order(line);
create index on dds.fact_repair_order(open_date);
create index on dds.fact_repair_order(close_date);
create index on dds.fact_repair_order(final_close_date);
create index on dds.fact_repair_order(line_date);
create index on dds.fact_repair_order(flag_date);
create index on dds.fact_repair_order(service_writer_key);
create index on dds.fact_repair_order(customer_key);
create index on dds.fact_repair_order(vehicle_key);
create index on dds.fact_repair_order(service_type_key);
create index on dds.fact_repair_order(payment_type_key);
create index on dds.fact_repair_order(ccc_key);
create index on dds.fact_repair_order(ro_comment_key);
create index on dds.fact_repair_order(opcode_key);
create index on dds.fact_repair_order(ro_status_key);
create index on dds.fact_repair_order(line_status_key);


drop table if exists arkona.ext_void_ros;
create table arkona.ext_void_ros (
  ro citext primary key);
create index on arkona.ext_void_ros(ro);

/*
need indexes
index after all the inserting
*/

/*
the issue of old ros (> 45 days old) that get closed and don't get updated to closed
generate a list of open ros > 45 days old, check those specifically for being closed
a targeted query of sdprhdr


*/
-- -- pre 2015 ros have null laborsales 
-- select count(*) filter (where laborsales is null) as null_ls,
--        count(*) filter (where laborsales is not null) as not_null
-- from ads.ext_fact_repair_order

SELECT string_agg(column_name, ',') FROM information_schema.columns WHERE table_schema = 'ads'  AND table_name = 'ext_fact_repair_order'; 

-- 15 sec
drop table if exists fact_ro;
create temp table fact_ro as 
select storecode,ro,line,tag,
  (select the_date from dds.dim_date where date_key = a.opendatekey) as open_date,
  (select the_date from dds.dim_date where date_key = a.closedatekey) as close_date,
  (select the_date from dds.dim_date where date_key = a.finalclosedatekey) as final_close_date,
  (select the_date from dds.dim_date where date_key = a.linedatekey) as line_date,
  (select the_date from dds.dim_date where date_key = a.flagdatekey) as flag_date,
  servicewriterkey,customerkey,vehiclekey,servicetypekey,paymenttypekey,ccckey,
  rocommentkey,opcodekey,rostatuskey,linestatuskey,rolaborsales,ropartssales,flaghours,
  roflaghours,miles,rocreatedts,sublet,roshopsupplies,rodiscount,rohazardousmaterials,
  ropaintmaterials,corcodekey,techkey,laborsales
from ads.ext_fact_repair_order a
join dds.dim_date b on a.opendatekey = b.date_key
  and b.the_date > '12/31/2014';


-- 3.5 min
insert into dds.fact_repair_order
select storecode,ro,line,tag, open_date, close_date, final_close_date, line_date, flag_date,
  servicewriterkey,customerkey,vehiclekey,servicetypekey,paymenttypekey,
  ccckey,
  rocommentkey,opcodekey,rostatuskey,linestatuskey,rolaborsales,ropartssales,flaghours,
  roflaghours,miles,rocreatedts,sublet,roshopsupplies,rodiscount,rohazardousmaterials,
  ropaintmaterials,corcodekey,techkey,laborsales 
from fact_ro
where open_date < '06/22/2020';

nulls: dim_ccc, dim_ro_comment
select * from fact_ro where ccckey = 928671


-- missing dim_ccc rows
select distinct array_agg(a.ccc_key)
from dds.fact_repair_order a
left join dds.dim_ccc b on a.ccc_key = b.ccc_key
where b.ccc_key is null

-- repopulate dds.dim_ccc with ccc_key as integer
drop table if exists dds.dim_ccc cascade;
create table dds.dim_ccc (
  ccc_key integer primary key,
  store_code citext,
  ro citext,
  line integer,
  complaint citext,
  cause citext,
  correction citext);
create unique index on dds.dim_ccc(ro,line) ;

insert into dds.dim_ccc(ccc_key,store_code,ro,line,complaint,cause,correction)
select ccckey,storecode,ro,line,complaint,cause,correction
from ads.ext_dim_ccc;

="insert into dds.dim_ccc values("&A2&",'"&B2&"','"&C2&"',"&D2&",'"&E2&"','"&F2&"','"&G2&"');"
-- insert the values from advantage
insert into dds.dim_ccc values(810644,'RY2','2744888',1,'N/A','N/A','LOF .3');
insert into dds.dim_ccc values(810645,'RY2','2744884',2,'N/A','N/A','24Z');
insert into dds.dim_ccc values(810646,'RY2','2744884',1,'DRIVERS REAR ONLY CUSTOMER IS AWARE THAT REPLACING ONLY ONE TIRE AT A TIME CAN CAUSE DAMAGE TO AWD SYSTEM','N/A','1NOT. .5HRS PW9999 PROGRAMMED TIRE SENSORS. .3HRS');
...

-- restore dim_ccc.ccc_key to serial data type
create sequence dds.dim_ccc_ccc_key_seq
  owned by dds.dim_ccc.ccc_key; 
alter table dds.dim_ccc
alter column ccc_key set default nextval('dds.dim_ccc_ccc_key_seq');

select setval('dds.dim_ccc_ccc_key_seq', (select max(ccc_key)from dds.dim_ccc));

-- now restore the not null constraint and foreign key to fact_repair_order.ccc_key;
-- actually should not have removed the not null constraint, the ccc_key exists in factrepairorder, what
-- was missing was the row in dds.dim_ccc

alter table dds.fact_repair_order alter column ccc_key set not null;
alter table dds.fact_repair_order
add foreign key (ccc_key) references dds.dim_ccc(ccc_key);

-- now the same thing with dds.dim_ro_comment
-- missing dim_ro_comment rows
select array_agg(distinct a.ro_comment_key)
-- select distinct a.ro_comment_key
from dds.fact_repair_order a
left join dds.dim_ro_comment b on a.ro_comment_key = b.ro_comment_key
where b.ro_comment_key is null

-- repopulate dds.dim_ro_comment_key with ro_comment_key as integer
drop table if exists dds.dim_ro_comment cascade;
create table dds.dim_ro_comment(
    ro_comment_key integer primary key,
    store_code citext,
    ro citext,
    comment_type citext,
    ro_comment citext);
create unique index on dds.dim_ro_comment(store_code, ro);

insert into dds.dim_ro_comment(ro_comment_key,store_code,ro,comment_type,ro_comment)
select rocommentkey,storecode,ro,commenttype,comment
from ads.ext_dim_ro_comment;

-- insert the values from advantage
insert into dds.dim_ro_comment values(339062,'RY1','16370447','Please Note','am #1147712 customer pay 15480442');
insert into dds.dim_ro_comment values(339063,'RY1','16370430','Please Note','am #1147711 charge a/r #129618 15480445');
...
-- restore dim_ro_comment.ro_comment_key to serial data type
create sequence dds.ro_comment_ro_comment_key_seq
  owned by dds.dim_ro_comment.ro_comment_key; 
alter table dds.dim_ro_comment
alter column ro_comment_key set default nextval('dds.ro_comment_ro_comment_key_seq');

select setval('dds.ro_comment_ro_comment_key_seq', (select max(ro_comment_key) from dds.dim_ro_comment));

-- now restore the not null constraint and foreign key to fact_repair_order.ccc_key;
alter table dds.fact_repair_order alter column ro_comment_key set not null;
alter table dds.fact_repair_order
add foreign key (ro_comment_key) references dds.dim_ro_comment(ro_comment_key);



-- SP xfmFactRepairOrder


----------------------------------------------------------------------------------------------------------
II.
delete
-- select ro
from dds.fact_repair_order
where ro in (
  select ro
  from arkona.ext_void_ros);
----------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------
--< III. tmpRO
----------------------------------------------------------------------------------------------------------
drop table if exists dds.tmp_ro cascade;
create TABLE dds.tmp_ro(
    store_code citext,
    ro citext,
    writer citext,
    customer integer,
    open_date date,
    close_date date,
    final_close_date date,
    vin citext,
    miles integer,
    ro_parts numeric(12,4),
    ro_labor numeric(12,4),
    discount numeric(12,4),
    hazardous numeric(12,4),
    shop_supplies numeric(12,4),
    tag citext,
    create_ts timestamp with time zone,
    ro_status citext);
-- SDPRHDR    
insert into dds.tmp_ro 
select company_number, ro_number, service_writer_id, customer_key, dds.db2_integer_to_date(open_date), 
  dds.db2_integer_to_date(close_date), dds.db2_integer_to_date(final_close_date), vin, 
  case
    WHEN odometer_in < 0 THEN
      CASE 
        WHEN odometer_out < 0 THEN 0
        ELSE odometer_out
      END
    WHEN odometer_in = odometer_out THEN odometer_in
    WHEN odometer_in < odometer_out THEN odometer_out
    WHEN odometer_in > odometer_out THEN odometer_in
  END, parts_total, labor_total, coupon_discount, cust_pay_hzrd_mat, 
  (cust_pay_shop_sup + warranty_shop_sup + internal_shop_sup + svc_cont_shop_sup), coalesce(tag_number, 'N/A'),
  doc_create_timestamp, 'Closed'
-- select *    
from arkona.ext_sdprhdr
where cust_name <> '*VOIDED REPAIR ORDER*'
  and company_number in ('RY1', 'RY2')
  and ro_number not in ('412955','413514','413620','413629');
-- PDPPHDR where ro does not already exists in dds.tmp_ro
insert into dds.tmp_ro 
select company_number, document_number, service_writer_id, customer_key, dds.db2_integer_to_date(open_tran_date), 
  '12/31/9999','12/31/9999', vin, 
  CASE 
    WHEN odometer_in < 0 THEN
      CASE 
        WHEN odometer_out < 0 THEN 0
        ELSE odometer_out
      END
    WHEN odometer_in = odometer_out THEN odometer_in
    WHEN odometer_in < odometer_out THEN odometer_out
    WHEN odometer_in > odometer_out THEN odometer_in
  END, parts_total, labor_total, coupon_discount, cust_pay_hzrd_mat, 
  (cust_pay_shop_sup + warranty_shop_sup + internal_shop_sup + svc_cont_shop_sup), coalesce(tag_number, 'N/A'),
  doc_create_timestamp, c.ro_status
-- select *    
from arkona.ext_pdpphdr b
left join dds.dim_ro_status c on b.ro_status = c.ro_status_code
where b.document_type = 'RO'
  and b.company_number in ('RY1','RY2')
  and b.document_number is not null
  and exists ( -- exclude ros with no lines
    select 1
    from arkona.ext_pdppdet
    where pending_key = b.pending_key)
  and cust_name <> '*VOIDED REPAIR ORDER*'
  and not exists ( -- exclude ros already in dds.tmp_ro
    select 1
    from dds.tmp_ro
    where ro = b.document_number);

-- UPDATE values IN tmpRO WHERE ro EXISTS IN both SDPRHDR & PDPPHDR
-- a: SDP values FROM tmpRO, d: PDP values
-- i am afraid this is going to come up a lot, i find myself questioning the manner in which this update
-- is done, not sure if it is correct in selecting which values to use
-- but i can't think of any real world situation in which this data is actually being used
-- so if it's wrong "it doesn't matter"
-- the rationale becoming, the purpose of this exercise is to move the existing process to 
-- postgresql, not rewrite it

-- so, the algorithm is: the only time they are D is if A = 0

update dds.tmp_ro aa
  set ro_parts = bb.ro_parts,
      ro_labor = bb.ro_labor,
      discount = bb.discount,
      hazardous = bb.hazardous,
      shop_supplies = bb.shop_supplies,
      ro_status = bb.ro_status
from ( 
  select a.ro,
    case when a.ro_parts = 0 then d.parts_total else a.ro_parts end as ro_parts,
    case when a.ro_labor = 0 then d.labor_total else a.ro_labor end as ro_labor,
    case when a.discount = 0 then d.coupon_discount else a.discount end as discount, 
    case when a.hazardous = 0 then d.cust_pay_hzrd_mat else a.hazardous end as hazardous,
    case when a.shop_supplies = 0 then d.shop_supplies else a.shop_supplies end as shop_supplies, 
    coalesce(d.ro_status, a.ro_status) as ro_status  
  from dds.tmp_ro a
  join (
    select company_number, document_number, parts_total, labor_total, coupon_discount, cust_pay_hzrd_mat, 
      (cust_pay_shop_sup + warranty_shop_sup + internal_shop_sup + svc_cont_shop_sup) as shop_supplies, c.ro_status
    from arkona.ext_pdpphdr b
    left join dds.dim_ro_status c on b.ro_status = c.ro_status_code
    where b.document_type = 'RO'
      and b.company_number in ('RY1','RY2')
      and b.document_number is not null
      and exists ( -- exclude ros with no lines
        select 1
        from arkona.ext_pdppdet
        where pending_key = b.pending_key)
      and cust_name <> '*VOIDED REPAIR ORDER*') d on a.ro = d.document_number
    WHERE (a.ro_parts <> d.parts_total
      OR a.ro_labor <> d.labor_total 
      OR a.discount <> d.coupon_discount
      OR a.hazardous <> d.cust_pay_hzrd_mat
      OR a.shop_supplies <> d.shop_supplies
      OR a.ro_status <> d.ro_status)) bb 
where aa.ro = bb.ro;    




/*
check for the correct bnkey for inventory customer
the minumum bopname.bopname_record_key is 4
i use dimcustomer.bnkey = 0 for inventory ros
so, when ros are created for inventory vehicles but are given a customer number,
i need to update that value to 0
*/

update dds.tmp_ro
set customer = 0
where ro in (
  select a.ro
  from dds.tmp_ro a
  join arkona.ext_bopname b on a.customer = b.bopname_record_key
    and b.last_company_name like '%INVENTORY%');

----------------------------------------------------------------------------------------------------------
--/> III. tmpRO
----------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------
--< IV. tmpROKeys
----------------------------------------------------------------------------------------------------------
drop table if exists dds.tmp_ro_keys cascade;
create TABLE dds.tmp_ro_keys(
    store_code citext NOT NULL,
    ro citext NOT NULL,
    tag citext NOT NULL,
    open_date date NOT NULL,
    close_date date NOT NULL,
    final_close_date date NOT NULL,
    service_writer_key integer NOT NULL,
    customer_key integer NOT NULL,
    vehicle_key integer NOT NULL,
    ro_comment_key integer NOT NULL,
    ro_status_key integer NOT NULL,
    ro_labor_sales numeric (8,2) DEFAULT 0,
    ro_parts_sales numeric(12,4) DEFAULT 0,
    miles integer DEFAULT 0,
    create_ts timestamp with time zone DEFAULT '12/31/9999 00:00:01',
    ro_shop_supplies numeric(12,4) DEFAULT 0,
    ro_discount numeric(12,4) DEFAULT 0,
    ro_hazardous numeric(12,4) DEFAULT 0,
    PRIMARY KEY (store_code,ro));

-- SELECT string_agg(column_name, ',') FROM information_schema.columns WHERE table_schema = 'dds'  AND table_name = 'tmp_ro_keys';   
insert into dds.tmp_ro_keys (store_code,ro,tag,open_date,close_date,final_close_date,
  service_writer_key,customer_key,vehicle_key,ro_comment_key,ro_status_key,ro_labor_sales,
  ro_parts_sales,miles,create_ts,ro_shop_supplies,ro_discount,ro_hazardous)
select a.store_code, a.ro, a.tag, a.open_date, a.close_date, a.final_close_date,
  coalesce(e.service_writer_key, m.service_writer_key) as service_writer_key, 
  coalesce(f.customer_key, k.customer_key) as customer_key, 
  coalesce(g.vehicle_key, n.vehicle_key) as vehicle_key,
  coalesce(h.ro_comment_key, i.ro_comment_key) as ro_comment_key, ro_status_key, 
  a.ro_labor, a.ro_parts, a.miles, a.create_ts, a.shop_supplies, a.discount,
  a.hazardous
FROM dds.tmp_ro  a
-- LEFT JOIN dds.dim_date b ON a.open_date = b.the_date
-- LEFT JOIN dds.dim_date c ON a.close_date = c.the_date
-- LEFT JOIN dds.dim_date d ON a.final_close_date = d.the_date
LEFT JOIN dds.dim_service_writer e ON a.store_code = e.store_code
  AND a.writer = e.writer_number
  AND e.active = true
  AND a.open_date BETWEEN e.row_from_date AND e.row_thru_date
LEFT JOIN dds.dim_service_writer m ON a.store_code = m.store_code
  AND m.writer_number = 'UNK'  
LEFT JOIN dds.dim_customer f ON a.customer = f.bnkey  
LEFT JOIN dds.dim_customer k ON 1 = 1
  AND k.full_name = 'unknown'
LEFT JOIN dds.dim_vehicle g ON a.vin = g.vin 
LEFT JOIN dds.dim_vehicle n ON 1 = 1 
  AND n.vin = 'UNKNOWN'
LEFT JOIN dds.dim_ro_comment h ON a.store_code = h.store_code AND a.ro = h.ro
LEFT JOIN dds.dim_ro_comment i ON a.store_code = i.store_code
  AND i.ro = 'N/A'
LEFT JOIN dds.dim_ro_status j ON a.ro_status = j.ro_status;

----------------------------------------------------------------------------------------------------------
--< V. tmpROLine
----------------------------------------------------------------------------------------------------------
drop table if exists dds.tmp_ro_line cascade;
create TABLE dds.tmp_ro_line(
    store_code citext NOT NULL,
    ro citext NOT NULL,
    line integer NOT NULL,
    line_date date NOT NULL,
    sublet numeric(12,4) NOT NULL DEFAULT 0,
    paint_materials numeric(12,4) NOT NULL DEFAULT 0,
    serv_type citext,
    pay_type citext,
    opcode citext,
    status citext,
    PRIMARY KEY (store_code,ro,line));
create index on dds.tmp_ro_line(status);    
create index on dds.tmp_ro_line(ro);
create index on dds.tmp_ro_line(line);
create index on dds.tmp_ro_line(store_code);
create index on dds.tmp_ro_line(opcode);
create index on dds.tmp_ro_line(line_date);
-- SELECT string_agg(column_name, ',') FROM information_schema.columns WHERE table_schema = 'dds'  AND table_name = 'tmp_ro_line';   
insert into dds.tmp_ro_line (store_code,ro,line,line_date,sublet,paint_materials,serv_type,pay_type,opcode,status)
SELECT a.company_number AS store_code, a.ro_number AS ro, a.line_number AS line,
  -- there a goofy few with no A line
  coalesce(min(CASE WHEN a.line_type = 'A' THEN dds.db2_integer_to_date(a.transaction_date) ELSE '12/31/9999'::date END), '12/31/9999'::date) AS line_date,
  coalesce(SUM(CASE WHEN a.transaction_code = 'SL' AND a.line_type = 'N' THEN labor_amount END), 0) AS sublet, 
  coalesce(SUM(CASE WHEN a.transaction_code = 'PM' AND a.line_type = 'M' THEN labor_amount END), 0) AS paint_materials, 
  MAX(CASE WHEN a.line_type = 'A' THEN a.service_type END) AS serv_type,
  MAX(CASE WHEN a.line_type = 'A' THEN a.line_payment_method END) AS pay_type,
  MAX(CASE WHEN a.line_type = 'A' THEN a.labor_operation_code END) AS opcode,
  'Closed' AS status
FROM arkona.ext_sdprdet a  
INNER JOIN dds.tmp_ro b ON a.ro_number = b.ro 
GROUP BY a.company_number, a.ro_number, a.line_number;

insert into dds.tmp_ro_line (store_code,ro,line,line_date,sublet,paint_materials,serv_type,pay_type,opcode,status)
SELECT b.company_number AS store_code, a.document_number AS ro, b.line_number AS line,
  -- there a goofy few with no A line
  coalesce(min(CASE WHEN b.line_type = 'A' THEN dds.db2_integer_to_date(b.transaction_date) ELSE '12/31/9999'::date END), '12/31/9999'::date) AS line_date,
  coalesce(SUM(CASE WHEN b.transaction_code = 'SL' AND b.line_type = 'N' THEN net_price END), 0) AS sublet, 
  coalesce(SUM(CASE WHEN b.transaction_code = 'PM' AND b.line_type = 'M' THEN net_price END), 0) AS paint_materials, 
  MAX(CASE WHEN b.line_type = 'A' THEN b.service_type END) AS serv_type,
  MAX(CASE WHEN b.line_type = 'A' THEN b.line_payment_method END) AS pay_type,
  MAX(CASE WHEN b.line_type = 'A' THEN b.labor_operation_code END) AS opcode,
  MAX(CASE WHEN b.line_type = 'A' AND b.line_status = 'I' THEN 'Open' ELSE 'Closed' END) AS status 
FROM arkona.ext_pdpphdr a  
join arkona.ext_pdppdet b on a.pending_key = b.pending_key
where a.document_type = 'RO'
  and a.document_number is not null
  and a.cust_name <> '*VOIDED REPAIR ORDER*'
  and b.line_number < 900
  and b.company_number in ('RY1','RY2')
  and not exists (
    select 1
    from dds.tmp_ro_line
    where ro = a.document_number
    and line = b.line_number)
GROUP BY b.company_number, a.document_number, b.line_number
order by a.document_number, b.line_number;

-- update values on lines that exist in both sdprdet and pdppdet
update dds.tmp_ro_line aa
  set line_date = bb.line_date,
      sublet = bb.sublet,
      paint_materials = bb.paint_materials,
      serv_type = bb.serv_type,
      pay_type = bb.pay_type,
      opcode = bb.opcode,
      status = bb.status
from (      
  select a.store_code, a.ro, a.line,
    d.line_date,
    case when a.sublet > d.sublet then a.sublet else d.sublet end,
    case when a.paint_materials > d.paint_materials then a.paint_materials else d.paint_materials end,
    d.serv_type, d.pay_type, d.opcode, d.status
  from dds.tmp_ro_line a
  join (
    SELECT b.company_number AS store_code, a.document_number AS ro, b.line_number AS line,
      -- there a goofy few with no A line
      coalesce(min(CASE WHEN b.line_type = 'A' THEN dds.db2_integer_to_date(b.transaction_date) ELSE '12/31/9999'::date END), '12/31/9999'::date) AS line_date,
      coalesce(SUM(CASE WHEN b.transaction_code = 'SL' AND b.line_type = 'N' THEN net_price END), 0) AS sublet, 
      coalesce(SUM(CASE WHEN b.transaction_code = 'PM' AND b.line_type = 'M' THEN net_price END), 0) AS paint_materials, 
      MAX(CASE WHEN b.line_type = 'A' THEN b.service_type END) AS serv_type,
      MAX(CASE WHEN b.line_type = 'A' THEN b.line_payment_method END) AS pay_type,
      MAX(CASE WHEN b.line_type = 'A' THEN b.labor_operation_code END) AS opcode,
      MAX(CASE WHEN b.line_type = 'A' AND b.line_status = 'I' THEN 'Open' ELSE 'Closed' END) AS status 
    FROM arkona.ext_pdpphdr a  
    join arkona.ext_pdppdet b on a.pending_key = b.pending_key
    where a.document_type = 'RO'
      and a.document_number is not null
      and a.cust_name <> '*VOIDED REPAIR ORDER*'
      and b.line_number < 900
      and b.company_number in ('RY1','RY2')
    GROUP BY b.company_number, a.document_number, b.line_number) d on a.store_code = d.store_code 
      and a.ro = d.ro
      and a.line = d.line
  where a.line_date <> d.line_Date
    or a.sublet <> d.sublet
    or a.paint_materials <> d.paint_materials  
    or a.serv_type <> d.serv_type
    or a.pay_type <> d.pay_type
    or a.opcode <> d.opcode
    or a.status <> d.status) bb
where aa.store_code = bb.store_code
  and aa.ro = bb.ro
  and aa.line = bb.line; 
----------------------------------------------------------------------------------------------------------
--/> V. tmpROLine
----------------------------------------------------------------------------------------------------------  

----------------------------------------------------------------------------------------------------------
--< VI. tmpROLineKeys
----------------------------------------------------------------------------------------------------------  
drop table if exists dds.tmp_ro_line_keys cascade;
create TABLE dds.tmp_ro_line_keys(
    store_code citext NOT NULL,
    ro citext NOT NULL,
    line integer NOT NULL,
    line_date date NOT NULL,
    service_type_key integer NOT NULL,
    payment_type_key integer NOT NULL,
    ccc_key integer NOT NULL,
    opcode_key integer NOT NULL,
    status_key integer NOT NULL,
    sublet numeric(12,4) NOT NULL DEFAULT 0,
    ro_paint_materials numeric(12,4) DEFAULT 0,
    PRIMARY KEY (store_code,ro,line));
-- SELECT string_agg(column_name, ',') FROM information_schema.columns WHERE table_schema = 'dds'  AND table_name = 'tmp_ro_line_keys'; 
insert into dds.tmp_ro_line_keys (store_code,ro,line,line_date,service_type_key,payment_type_key,ccc_key,
  opcode_key,status_key,sublet,ro_paint_materials)
select a.store_Code, a.ro, a.line, 
  a.line_date, -- coalesce(d.date_key, dd.date_key) as line_date_key,
  coalesce(e.service_type_key, ee.service_type_key) as service_type_key,
  coalesce(f.payment_type_key, ff.payment_type_key) as payment_type_key,
  coalesce(g.ccc_key, gg.ccc_key) as ccc_key,
  coalesce(h.opcode_key, hh.opcode_key) as opcode_key,
  m.line_status_key, a.sublet, c.ro_paint_materials
from dds.tmp_ro_line a  
left join (
  select store_code, ro, sum(paint_materials) as ro_paint_materials
  from dds.tmp_ro_line
  group by store_code, ro) c on a.store_code = c.store_code and a.ro = c.ro
-- left join dds.dim_date d on a.line_date = d.the_date
-- left join dds.dim_date dd  on dd.date_type <> 'date'  
LEFT JOIN dds.dim_service_type e ON a.serv_type = e.service_type_code
LEFT JOIN dds.dim_service_type ee ON ee.service_type_code = 'UN'
LEFT JOIN dds.dim_payment_type f ON a.pay_type = f.payment_type_code
LEFT JOIN dds.dim_payment_type ff on ff.payment_type_code = 'U'
LEFT JOIN dds.dim_ccc g ON a.store_code = g.store_code AND a.ro = g.ro AND a.line = g.line
LEFT JOIN dds.dim_ccc gg ON a.store_code = gg.store_code AND gg.ro = 'N/A'
LEFT JOIN dds.dim_opcode h ON a.store_code = h.store_code AND a.opcode = h.opcode
LEFT JOIN dds.dim_opcode hh ON a.store_code = hh.store_code AND hh.opcode = 'N/A'            
LEFT JOIN dds.dim_line_status m ON a.status = m.line_status;
----------------------------------------------------------------------------------------------------------
--/> VI. tmpROLineKeys
----------------------------------------------------------------------------------------------------------  

----------------------------------------------------------------------------------------------------------
--< VII. tmpRoTechFlagCorSeqGroup
----------------------------------------------------------------------------------------------------------  
-- this IS WHERE THEN generation of tech/flagdate/corcode happens

-- assumption: cr seq always less than the tt seq#
-- each instance of a cor line generates a new seqgroup   
as illustrated by this query in ads SELECT * FROM tmpRoTechFlagCorSeqGroup ORDER BY ptro#, ptline, seqgroup, ptseq#
but appears to be violated by (in pg) ro 16360217 line 1, that is for a line, a flag seq# <  cor seq#

this may be a red herrring, i ran into the assumption in ads fuck me remodel to finer grain/take3.sql
but it appears to be essential to the generation of the seqgroup
start there next
-- fact_repair_order PK: StoreCode;RO;Line;FlagDateKey;TechKey;CorCodeKey

select * from cur where ro_number = '16360217' order by line_number, sequence_number

select * from dds.fact_repair_order where ro = '16360217'

select * from ads.ext_Fact_repair_order limit 100

select min(b.the_date) from ads.ext_Fact_repair_order a join dds.dim_date b on a.opendatekey = b.date_key


  SELECT 'flag' AS flagcor,company_number, ro_number, line_number, sequence_number, 
    dds.db2_integer_to_date(transaction_date) as transaction_date, technician_id, sum(labor_hours) AS ptlhrs
  FROM arkona.ext_sdprdet
  WHERE transaction_code = 'tt'
  AND ro_number = '16360217'
  GROUP BY company_number, ro_number, line_number, sequence_number, transaction_date, technician_id
   union
  SELECT 'cor' AS flagcor,company_number, ro_number, line_number, sequence_number, 
    dds.db2_integer_to_date(transaction_date) as transaction_date, max(correction_code) as ptcrlo, 0
  FROM arkona.ext_sdprdet
  WHERE transaction_code = 'cr'
  AND ro_number = '16360217'
  GROUP BY company_number, ro_number, line_number, sequence_number, transaction_date 
order by line_number, sequence_number  

it doesnt matter, the seqgroup will be generated whether the cor-seq is > than the flag-seq or not

for each

select min(ro_number) as ro from cur where left(ro_number,2) = '16'
union
select max(ro_number) from cur where left(ro_number,2) = '16'
union
select min(ro_number) from cur where left(ro_number,2) = '18'
union
select max(ro_number) from cur where left(ro_number,2) = '18'
union
select min(ro_number) from cur where left(ro_number,2) = '19'
union
select max(ro_number) from cur where left(ro_number,2) = '19'
union
select min(ro_number) from cur where left(ro_number,1) = '2'
union
select max(ro_number) from cur where left(ro_number,1) = '2'
order by ro


select ro_number, count(*) from cur where left(ro_number, 4)  = '2823' group by ro_number order by ro_number

select * from cur where ro_number = '16375005'

select string_agg(distinct '''' || ro_number ||'''',',') from (
select a.* from (select * from cur where flagcor = 'cor') a
join (select * from cur where flagcor = 'flag') b
on a.ro_number = b.ro_number
and a.line_number = b.line_number
and a.sequence_number > b.sequence_number) X

i think
every cor seq record has a unique seqgroup

select *
from cur
limit 100

select ro_number, line_number, technician_id
from cur
where flagcor = 'cor'
group by ro_number, line_number, technician_id
order by ro_number

select * from cur where ro_number = '16365618'

select * from arkona.ext_Sdprdet where ro_number = '16365618'

----------------------------------------------------------------------------------------
i dont even know what the purpose of the seq group is
so, lets just go ahead with it and see how it goes

drop table if exists dds.tmp_cor_group cascade;
create table dds.tmp_cor_group (
  flag_cor citext,
  ro citext, 
  line integer,
  seq integer,
  seq_group integer);


do $$
declare 
counter integer = 1;
_table dds.tmp_cor_group;
_cursor cursor for (  
select * 
from(
  SELECT 'flag' AS flagcor, ro_number, line_number, sequence_number
  FROM arkona.ext_sdprdet
  WHERE transaction_code = 'tt'
  AND labor_hours <> 0
  GROUP BY company_number, ro_number, line_number, sequence_number
  UNION 
  SELECT 'flag' AS flagcor, a.document_number AS ro_number, b.line_number, b.sequence_number
  FROM arkona.ext_pdpphdr a
  INNER JOIN arkona.ext_pdppdet b on a.pending_key = b.pending_key
  WHERE transaction_code = 'tt'
  AND labor_hours <> 0  
  GROUP BY a.document_number, b.line_number, b.sequence_number
  union
  SELECT 'cor' AS flagcor, ro_number, line_number, sequence_number
  FROM arkona.ext_sdprdet
  WHERE transaction_code = 'cr'
  GROUP BY  ro_number, line_number, sequence_number
  union
  SELECT 'cor' AS flagcor, a.document_number, b.line_number, b.sequence_number
  FROM arkona.ext_pdpphdr a
  INNER JOIN arkona.ext_pdppdet b on a.pending_key = b.pending_key
  WHERE transaction_code = 'cr'
  GROUP BY  a.document_number, b.line_number, b.sequence_number
  ORDER BY ro_number, line_number, sequence_number) x);

begin
delete from dds.tmp_cor_group;
open _cursor; 
  loop
    fetch _cursor into _table;
      if _table.flag_cor = 'cor' then
        counter = counter + 1;
      end if;
      insert into dds.tmp_cor_group values(_table.flag_cor,_table.ro,_table.line,_table.seq,counter);
  exit when _table.ro is null;
end loop;
close _cursor;  
end $$

----------------------------------------------------------------------------------------------------------
--/> VII. tmpRoTechFlagCorSeqGroup
----------------------------------------------------------------------------------------------------------  

----------------------------------------------------------------------------------------------------------
--< VIII. tmpRoTechFlagCor1
----------------------------------------------------------------------------------------------------------  
drop table if exists dds.tmp_ro_tech_flag_cor_1 cascade;
create TABLE dds.tmp_ro_tech_flag_cor_1(
    store_code citext,
    ro citext,
    line integer,
    flag_date date,
    technician_id citext,
    labor_hours numeric (12,4),
    flag_seq_group integer,
    correction_code citext,
    cor_seq_group integer,
    sequence_number integer,
    labor_amount numeric (8,2));

-- looks ok, matches ok with ads
-- SELECT string_agg(column_name, ',') FROM information_schema.columns WHERE table_schema = 'dds'  AND table_name = 'tmp_ro_tech_flag_cor_1'; 
insert into dds.tmp_ro_tech_flag_cor_1(store_code,ro,line,flag_date,technician_id,labor_hours,flag_seq_group,
  correction_code,cor_seq_group,sequence_number,labor_amount)
select distinct aa.store_code, aa.ro, aa.line, bb.transaction_date, bb.technician_id, bb.labor_hours,
  bb.seq_group, cc.correction_code, cc.seq_group, bb.sequence_number, bb.labor_amount
from dds.tmp_ro_line aa
left join (
  select a.*, b.seq_group
  from (
    SELECT 'flag' AS flagcor, company_number, ro_number, line_number, sequence_number,
      dds.db2_integer_to_date(transaction_date) as transaction_date, technician_id, sum(labor_hours) as labor_hours,
      sum(labor_amount) as labor_amount 
    FROM arkona.ext_sdprdet
    WHERE transaction_code = 'tt'
    AND labor_hours <> 0
    GROUP BY company_number, ro_number, line_number, sequence_number,
      dds.db2_integer_to_date(transaction_date), technician_id
    UNION 
    SELECT 'flag' AS flagcor, b.company_number, a.document_number, b.line_number, b.sequence_number,
      dds.db2_integer_to_date(transaction_date), technician_id, sum(labor_hours), sum(net_price)
    FROM arkona.ext_pdpphdr a
    INNER JOIN arkona.ext_pdppdet b on a.pending_key = b.pending_key
    WHERE transaction_code = 'tt'
    AND labor_hours <> 0  
    GROUP BY b.company_number, a.document_number, b.line_number, b.sequence_number,
      dds.db2_integer_to_date(b.transaction_date), b.technician_id) a
  left join dds.tmp_cor_group b on a.ro_number = b.ro
    and a.line_number = b.line
    and a.sequence_number = b.seq
    and b.flag_cor = 'flag') bb on aa.store_code = bb.company_number
      and aa.ro = bb.ro_number 
      and aa.line = bb.line_number
left join (
  select a.*, b.seq_group
  from (
    SELECT 'cor' AS flagcor, company_number, ro_number, line_number, sequence_number,
      dds.db2_integer_to_date(transaction_date) as transaction_date, max(correction_code) as correction_code
    FROM arkona.ext_sdprdet
    WHERE transaction_code = 'cr'
    GROUP BY company_number, ro_number, line_number, sequence_number,
      dds.db2_integer_to_date(transaction_date)
    UNION 
    SELECT 'cor' AS flagcor, b.company_number, a.document_number, b.line_number, b.sequence_number,
      dds.db2_integer_to_date(b.transaction_date), max(b.correction_code)
    FROM arkona.ext_pdpphdr a
    INNER JOIN arkona.ext_pdppdet b on a.pending_key = b.pending_key
    WHERE transaction_code = 'cr'
    GROUP BY b.company_number, a.document_number, b.line_number, b.sequence_number,
      dds.db2_integer_to_date(b.transaction_date)) a
  left join dds.tmp_cor_group b on a.ro_number = b.ro
    and a.line_number = b.line
    and a.sequence_number = b.seq
    and b.flag_cor = 'cor') cc on aa.store_code = cc.company_number
      and aa.ro = cc.ro_number 
      and aa.line = cc.line_number
      and
        case
          when bb.seq_group is null then 1 = 1
          else coalesce(bb.seq_group, - 1) = coalesce(cc.seq_group, -1)
        end;
----------------------------------------------------------------------------------------------------------
--/> VIII. tmpRoTechFlagCor1
----------------------------------------------------------------------------------------------------------  

----------------------------------------------------------------------------------------------------------
--< IX. tmpRoTechFlagCor2
----------------------------------------------------------------------------------------------------------  
drop table if exists dds.tmp_ro_tech_flag_cor_2 cascade;
create TABLE dds.tmp_ro_tech_flag_cor_2(
    store_code citext,
    ro citext,
    line integer,
    flag_date date,
    technician_id citext,
    correction_code citext,
    labor_hours numeric (12,4),
    labor_amount numeric (8,2),
    PRIMARY KEY (store_code,ro,line,flag_date,technician_id,correction_code));   

-- SELECT string_agg(column_name, ',') FROM information_schema.columns WHERE table_schema = 'dds'  AND table_name = 'tmp_ro_tech_flag_cor_2'; 

insert into dds.tmp_ro_tech_flag_cor_2(store_code,ro,line,flag_date,technician_id,correction_code,labor_hours,labor_amount)
select store_code, ro, line, flag_date, technician_id, correction_code, sum(labor_hours), sum(labor_amount)
from (
  select store_code, ro, line, coalesce(flag_date, '12/31/9999'::date) as flag_date,
    coalesce(technician_id, 'N/A') as technician_id, sum(coalesce(labor_hours, 0)) as labor_hours,
    sum(coalesce(labor_amount, 0)) as labor_amount, coalesce(correction_code, 'N/A') as correction_code,
    coalesce(flag_seq_group, cor_seq_group)
  from dds.tmp_ro_tech_flag_cor_1
  group by store_code, ro, line, coalesce(flag_date, '12/31/9999'::date), 
    coalesce(technician_id, 'N/A'), coalesce(correction_code, 'N/A'), coalesce(flag_seq_group, cor_seq_group)) a
group by store_code, ro, line, flag_date, technician_id, correction_code;
    
----------------------------------------------------------------------------------------------------------
--/> IX. tmpRoTechFlagCor2
----------------------------------------------------------------------------------------------------------  

----------------------------------------------------------------------------------------------------------
--< X. tmpRoTechFlagCorKeys
----------------------------------------------------------------------------------------------------------  
drop table if exists dds.tmp_ro_tech_flag_cor_keys cascade;
create TABLE dds.tmp_ro_tech_flag_cor_keys(
    store_code citext,
    ro citext,
    line integer,
    flag_date date,
    tech_key integer,
    cor_code_key integer,
    labor_hours numeric (8,2),
    labor_amount numeric (8,2),
    PRIMARY KEY (store_code,ro,line,flag_date,tech_key,cor_code_key));

-- SELECT string_agg(column_name, ',') FROM information_schema.columns WHERE table_schema = 'dds'  AND table_name = 'tmp_ro_tech_flag_cor_keys'; 
insert into dds.tmp_ro_tech_flag_cor_keys(store_code,ro,line,flag_date,
  tech_key,cor_code_key,labor_hours,labor_amount)
select a.store_code, a.ro, a.line, a.flag_date,
  case
    when a.store_code = 'RY1' then
      case when c.tech_key is not null then c.tech_key else d.tech_key end
    when a.store_code = 'RY2' then
      case when c.tech_key is not null then c.tech_key else d.tech_key end
    end,
  coalesce(e.opcode_key, f.opcode_key), a.labor_hours, a.labor_amount
from dds.tmp_ro_tech_flag_cor_2 a
-- join dds.dim_date b on a.flag_date = b.the_date
left join dds.dim_tech c on a.store_code = c.store_code
  and a.technician_id = c.tech_number
  and a.flag_date between c.row_from_date and c.row_thru_date
left join (
  select store_code, tech_key
  from dds.dim_tech
  where tech_number = 'UNK') d on a.store_code = d.store_code
left join dds.dim_opcode e on a.store_code = e.store_code
  and a.correction_code = e.opcode
left join dds.dim_opcode f on a.store_code = f.store_code
  and f.opcode = 'N/A';

----------------------------------------------------------------------------------------------------------
--/> X. tmpRoTechFlagCorKeys
----------------------------------------------------------------------------------------------------------  

----------------------------------------------------------------------------------------------------------
--< XI. tmpRoTechFlagCorKeys
----------------------------------------------------------------------------------------------------------  

drop table if exists dds.tmp_fact_repair_order cascade;
CREATE TABLE dds.tmp_fact_repair_order ( 
      store_code citext not null,
      ro citext not null,
      line Integer not null,
      tag citext not null,
      open_date date not null,
      close_date date not null,
      final_close_date date not null,
      line_date date not null,
      flag_date date not null,
      service_writer_key Integer not null,
      customer_key Integer not null,
      vehicle_key Integer not null,
      service_type_key Integer not null,
      payment_type_key Integer not null,
      ccc_key Integer not null,
      ro_comment_key Integer not null,
      opcode_key Integer not null,
      ro_status_key Integer not null,
      line_status_key Integer not null,
      ro_labor_sales numeric(8,2) not null,
      ro_parts_sales numeric(8,2) not null,
      flag_hours numeric(8,2) not null,
      ro_flag_hours numeric(8,2) not null,
      miles Integer not null,
      ro_created_ts TimeStamptz not null,
      sublet numeric(8,2) not null,
      ro_shop_supplies numeric(8,2) not null,
      ro_discount numeric(8,2) not null,
      ro_hazardous_materials numeric(8,2) not null,
      ro_paint_materials numeric(8,2) not null,
      cor_code_key Integer not null,
      tech_key Integer not null,
      labor_sales numeric(8,2) not null,
      primary key(store_code,ro,line,flag_date,tech_key,cor_code_key));

-- indexes increased the insert from 8 minutes to 1 sec
create index on dds.tmp_ro_keys(store_code);
create index on dds.tmp_ro_keys(ro);
create index on dds.tmp_ro_line_keys(store_code);
create index on dds.tmp_ro_line_keys(ro);
create index on dds.tmp_ro_line_keys(line);
create index on dds.tmp_ro_tech_flag_cor_keys(store_code);
create index on dds.tmp_ro_tech_flag_cor_keys(ro);
create index on dds.tmp_ro_tech_flag_cor_keys(line);

-- SELECT string_agg(column_name, ',') FROM information_schema.columns WHERE table_schema = 'dds'  AND table_name = 'tmp_fact_repair_order';  
insert into dds.tmp_fact_repair_order(store_code,ro,line,tag,open_date,close_date,
  final_close_date,line_date,flag_date,service_writer_key,customer_key,
  vehicle_key,service_type_key,payment_type_key,ccc_key,ro_comment_key,opcode_key,
  ro_status_key,line_status_key,ro_labor_sales,ro_parts_sales,flag_hours,ro_flag_hours,
  miles,ro_created_ts,sublet,ro_shop_supplies,ro_discount,ro_hazardous_materials,
  ro_paint_materials,cor_code_key,tech_key,labor_sales)
select a.store_code, a.ro, b.line, a.tag, a.open_date, a.close_date, a.final_close_date,
  b.line_date, c.flag_date, a.service_writer_key, a.customer_key, a.vehicle_key, 
  b.service_type_key, b.payment_type_key, b.ccc_key, a.ro_comment_key, b.opcode_key, a.ro_status_key,
  b.status_key, d.ro_labor_sales, a.ro_parts_sales, c.labor_hours, d.ro_flag_hours, a.miles,
  a.create_ts, b.sublet, a.ro_shop_supplies, a.ro_discount, a.ro_hazardous, b.ro_paint_materials, 
  c.cor_code_key, c.tech_key, c.labor_amount
from dds.tmp_ro_keys a
join dds.tmp_ro_line_keys b on a.store_code = b.store_code
  and a.ro = b.ro
  and b.line < 900
join dds.tmp_ro_tech_flag_cor_keys c on b.store_code = c.store_code
  and b.ro = c.ro
  and b.line = c.line
left join ( -- generate ro_flag_hours, ro_labor_sales
  select store_code, ro, sum(labor_hours) as ro_flag_hours,
    sum(labor_amount) as ro_labor_sales
  from dds.tmp_ro_tech_flag_cor_keys
  group by store_code, ro) d on a.store_code = d.store_code
    and a.ro = d.ro;

----------------------------------------------------------------------------------------------------------
--/> XI. tmpRoTechFlagCorKeys
----------------------------------------------------------------------------------------------------------  

----------------------------------------------------------------------------------------------------------
--< XII. factRepairOrder
----------------------------------------------------------------------------------------------------------  
-- SELECT string_agg(column_name, ',') FROM information_schema.columns WHERE table_schema = 'dds'  AND table_name = 'tmp_fact_repair_order'; 
-- SELECT string_agg(column_name, ',') FROM information_schema.columns WHERE table_schema = 'dds'  AND table_name = 'fact_repair_order'; 

DELETE 
FROM dds.fact_repair_order
WHERE ro IN (
  SELECT ro
  FROM dds.tmp_fact_repair_order);
  
INSERT INTO dds.fact_repair_order (store_code,ro,line,tag,open_date,close_date,final_close_date,line_date,flag_date,service_writer_key,
  customer_key,vehicle_key,service_type_key,payment_type_key,ccc_key,ro_comment_key,opcode_key,ro_status_key,
  line_status_key,ro_labor_sales,ro_parts_sales,flag_hours,ro_flag_hours,miles,ro_created_ts,sublet,
  ro_shop_supplies,ro_discount,ro_hazardous_materials,ro_paint_materials,cor_code_key,tech_key,labor_sales)
SELECT store_code,ro,line,tag,open_date,close_date,final_close_date,line_date,flag_date,service_writer_key,
  customer_key,vehicle_key,service_type_key,payment_type_key,ccc_key,ro_comment_key,opcode_key,ro_status_key,
  line_status_key,ro_labor_sales,ro_parts_sales,flag_hours,ro_flag_hours,miles,ro_created_ts,sublet,
  ro_shop_supplies,ro_discount,ro_hazardous_materials,ro_paint_materials,cor_code_key,tech_key,labor_sales 
FROM dds.tmp_fact_repair_order;  


comment on table dds.tmp_ro is 'staging table used in the construction of dds.fact_repair order';
comment on table dds.tmp_cor_group is 'staging table used in the construction of dds.fact_repair order';
comment on table dds.tmp_fact_repair_order is 'staging table used in the construction of dds.fact_repair order';
comment on table dds.tmp_ro_keys is 'staging table used in the construction of dds.fact_repair order';
comment on table dds.tmp_ro_line is 'staging table used in the construction of dds.fact_repair order';
comment on table dds.tmp_ro_line_keys is 'staging table used in the construction of dds.fact_repair order';
comment on table dds.tmp_ro_tech_flag_cor_1 is 'staging table used in the construction of dds.fact_repair order';
comment on table dds.tmp_ro_tech_flag_cor_2 is 'staging table used in the construction of dds.fact_repair order';
comment on table dds.tmp_ro_tech_flag_cor_keys is 'staging table used in the construction of dds.fact_repair order';
----------------------------------------------------------------------------------------------------------
--/> XII. factRepairOrder
----------------------------------------------------------------------------------------------------------  

/**/
SELECT string_agg('a.'||column_name, ',') FROM information_schema.columns WHERE table_schema = 'ads'  AND table_name = 'ext_fact_repair_order'; 

-- exclude ro_created_ts, ccc_key, ro_comment_key
drop table if exists ads cascade;
create temp table ads as
select storecode, ro, line, flagdatekey, techkey, corcodekey, flaghours, md5(x::text) as hash
from (
  select a.storecode,a.ro,a.line,a.tag,
    b.the_date as opendate,c.the_date as closedate,d.the_date as finalclosedatekey,
    e.the_date as linedatekey,f.the_date as flagdatekey,
    a.servicewriterkey,a.customerkey,a.vehiclekey,a.servicetypekey,a.paymenttypekey,
--     a.ccckey,
--     a.rocommentkey,
    a.opcodekey,a.rostatuskey,a.linestatuskey,a.rolaborsales,round(a.ropartssales,2),a.flaghours,a.roflaghours,a.miles,
--     a.rocreatedts,
    round(a.sublet,2),round(a.roshopsupplies,2),round(a.rodiscount,2),round(a.rohazardousmaterials,2),round(a.ropaintmaterials,2),a.corcodekey,a.techkey,a.laborsales
  from ads.ext_fact_repair_order a
  join dds.dim_date b on a.opendatekey = b.date_key
    and b.the_year = 2020
  join dds.dim_date c on a.closedatekey = c.date_key
    and c.the_date < current_date
  join dds.dim_date d on a.finalclosedatekey = d.date_key
  join dds.dim_date e on a.linedatekey = e.date_key
  join dds.dim_date f on a.flagdatekey = f.date_key) x;

drop table if exists fact cascade;
create temp table fact as
select store_code, ro, line, flag_date, tech_key, cor_code_key, flag_hours, md5(x::text) as hash
from (
  select a.store_code,a.ro,a.line,a.tag,a.open_date,a.close_date,a.final_close_date,a.line_date,a.flag_date,
    a.service_writer_key,a.customer_key,a.vehicle_key,a.service_type_key,a.payment_type_key,
--     a.ccc_key,
--     a.ro_comment_key,
    a.opcode_key,a.ro_status_key,a.line_status_key,a.ro_labor_sales,a.ro_parts_sales,
    a.flag_hours,a.ro_flag_hours,a.miles,a.sublet,a.ro_shop_supplies,a.ro_discount,
    a.ro_hazardous_materials,a.ro_paint_materials,a.cor_code_key,a.tech_key,a.labor_sales
  from dds.fact_repair_order a
  join dds.dim_date b on a.open_date = b.the_date
    and b.the_year = 2020
  join dds.dim_date c on a.final_close_date = c.the_date
    and c.the_date < current_date) x;    

-- leaving out created_ts, ccc_key, ro_comment_key, there are no discrpancies
-- between all ros for 2020, to comment and ccc keys are a matter of timing only
select *
from ads a
join fact b on a.ro = b.ro 
  and a.line = b.line 
  and a.corcodekey = b.cor_code_key 
  and a.techkey = b.tech_key 
  and a.flagdatekey = b.flag_date
  and a.hash <> b.hash  


-- this was used to look at differences on individiual ros
  select 'ads' as source,a.storecode,a.ro,a.line,a.tag,
    b.the_date as opendate,c.the_date as closedate,d.the_date as finalclosedatekey,
    e.the_date as linedatekey,f.the_date as flagdatekey,
    a.servicewriterkey,a.customerkey,a.vehiclekey,a.servicetypekey,a.paymenttypekey,a.ccckey,a.rocommentkey,
    a.opcodekey,a.rostatuskey,a.linestatuskey,a.rolaborsales,round(a.ropartssales,2),a.flaghours,a.roflaghours,a.miles,
--     a.rocreatedts,
    round(a.sublet,2),round(a.roshopsupplies,2),round(a.rodiscount,2),round(a.rohazardousmaterials,2),round(a.ropaintmaterials,2),a.corcodekey,a.techkey,a.laborsales
  from ads.ext_fact_repair_order a
  join dds.dim_date b on a.opendatekey = b.date_key
--     and b.the_date = '05/20/2020'
  join dds.dim_date c on a.closedatekey = c.date_key
    and c.the_date < current_date
  join dds.dim_date d on a.finalclosedatekey = d.date_key
  join dds.dim_date e on a.linedatekey = e.date_key
  join dds.dim_date f on a.flagdatekey = f.date_key
  where ro = '16407507' and line = 1

union
  select 'fact',a.store_code,a.ro,a.line,a.tag,a.open_date,a.close_date,a.final_close_date,a.line_date,a.flag_date,
    a.service_writer_key,a.customer_key,a.vehicle_key,a.service_type_key,a.payment_type_key,a.ccc_key,
    a.ro_comment_key,a.opcode_key,a.ro_status_key,a.line_status_key,a.ro_labor_sales,a.ro_parts_sales,
    a.flag_hours,a.ro_flag_hours,a.miles,a.sublet,a.ro_shop_supplies,a.ro_discount,
    a.ro_hazardous_materials,a.ro_paint_materials,a.cor_code_key,a.tech_key,a.labor_sales
  from dds.fact_repair_order a
  join dds.dim_date b on a.open_date = b.the_date
--     and b.the_date = '05/20/2020'
  join dds.dim_date c on a.final_close_date = c.the_date
    and c.the_date < current_date
  where ro = '16407507' and line = 1
order by source, line  


select min(b.the_date) from ads.ext_fact_Repair_order a join dds.dim_Date b on a.opendatekey = b.date_key  --07/11/2011
select min(open_date) from dds.fact_repair_order  -- 01/01/2015
select distinct a.ro, b.ro 
from ads.ext_fact_repair_order a
full outer join dds.fact_repair_order b on a.ro = b.ro
where a.ro is null or b.ro is null

drop table if exists ads_ext;
create temp table ads_ext as
select ro
from ads.ext_fact_repair_order a
join dds.dim_date b on a.opendatekey = b.date_key
  and b.year_month > 201412
group by ro;  

drop table if exists dds_fact;
create temp table dds_fact as
select ro
from dds.fact_repair_order
group by ro;

-- only a few (33) from ads not in dds, ALL VOIDS !!!
select * 
from ads_Ext a
full outer join dds_fact b on a.ro = b.ro
where a.ro is null or b.ro is null


 */ 