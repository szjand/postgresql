﻿-----------------------------------------------------------------------------
--< dim_vehicle
-- advantage doesn't even do type 1 updates, just new rows
-----------------------------------------------------------------------------
drop table if exists dds.dim_vehicle cascade;
CREATE TABLE dds.dim_vehicle ( 
      vehicle_key integer primary key,
      vin citext,
      model_year citext,
      make citext,
      model citext,
      model_code citext,
      body citext,
      color citext);
      
create unique index on dds.dim_vehicle(vin);


insert into dds.dim_vehicle(vehicle_key,vin,model_year,make,model,model_code,body,color)  
select vehiclekey,vin,modelyear,make,model,modelcode,body,color
from ads.ext_dim_vehicle;

select max(vehicle_key) + 1 from dds.dim_vehicle;  

create sequence dds.dim_vehicle_vehicle_key_seq
  start 213019
  owned by dds.dim_vehicle.vehicle_key; 
alter table dds.dim_vehicle
alter column vehicle_key set default nextval('dds.dim_vehicle_vehicle_key_seq');

select inpmast_company_number, count(*) from arkona.xfm_inpmast group by inpmast_company_number

select count(*) from ads.ext_dim_vehicle  --211629

select count(*) from dds.dim_vehicle  --211367

select count(*) from arkona.xfm_inpmast where current_row  -- 211277

select a.inpmast_stock_number, a.inpmast_vin, a.status
from arkona.xfm_inpmast a
left join dds.dim_vehicle b on a.inpmast_vin = b.vin
where a.current_row
  and b.vin is null

select a.row_From_date, a.inpmast_stock_number, a.inpmast_vin, a.status
from arkona.xfm_inpmast a
where row_From_Date > current_date - 4
order by a.row_from_Date

create or replace function dds.dim_vehicle_update()
returns void as
$BODY$
/*

*/
insert into dds.dim_vehicle(vin,model_year,make,model,model_code,body,color)
select inpmast_vin, year, make, model, model_code, body_style, color
from arkona.xfm_inpmast a
where a.current_row
  and a.row_from_date > current_date - 7
  and a.inpmast_company_number = 'RY1' -- from advantage
on conflict (vin) 
  do update
    set (vin,model_year,make,model,model_code,body,color)
      =
      (excluded.vin,excluded.model_year,excluded.make,excluded.model,
        excluded.model_code,excluded.body,excluded.color);
$BODY$
language sql;    