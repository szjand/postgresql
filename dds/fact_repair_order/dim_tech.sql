﻿-----------------------------------------------------------------------------
--< maintenance
-----------------------------------------------------------------------------
-- 6/10 first cut of query to add new tech
-- 09/03/20 Freddie Cisneros  265474
insert into dds.dim_tech (store_code,employee_number,tech_name,tech_number,
  active,labor_cost,flag_department_code,flag_department,current_row,
  row_from_date)
select a.store_code, '265474','CISNEROS, FREDDIE', /*a.employee_number, c.employee_name,*/ 
  a.tech_number, a.active, a.labor_cost, a.flag_department_code,
  case a.flag_department_code
    when 'MR' then 'Service'
    when 'BS' then 'Body Shop'
    when 'RE' then 'Detail'
    when 'HS' then 'Hobby Shop'
    when 'QL' then 'Quick Lane'
    when 'QS' then 'Quick Service'
    when 'AM' then 'Service'
  end, true, current_date - 1
from dds.xfm_dim_tech a
left join dds.dim_tech b on a.store_code = b.store_code
  and a.tech_number = b.tech_number
left join arkona.xfm_pymast c on a.employee_number = c.pymast_employee_number
  and c.current_row  
where b.store_code is null
  and a.active = true ;

-----------------------------------------------------------------------------
--/> dim_tech
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
--< dim_tech
-----------------------------------------------------------------------------
/*
back load with data from ads, tech_key as integer
once loaded, alter tech_key to serial
nice suggestion from horse_with_no_name
https://dba.stackexchange.com/questions/78732/change-existing-column-in-pg-to-auto-incremental-primary-key
got my doubts as to how it will work
tested on type 2 update
worked
*/
drop table if exists dds.dim_tech cascade;
create table dds.dim_tech (
      tech_key integer primary key,
      store_code citext,
      employee_number citext,
      tech_name citext,
--       Description CIChar( 20 ),
      tech_number citext,
      active boolean,
      labor_cost numeric(8,2),
      flag_department_code citext,
      flag_department citext,
      current_row boolean,
      row_change_date Date,
--       RowChangeDateKey Integer,
--       RowFromTS TimeStamp,
--       RowThruTS TimeStamp,
      row_change_reason citext,
      row_from_date Date,
--       TechKeyFromDateKey Integer,
      row_thru_date Date default '12/31/9999' check (row_thru_date >= row_from_date));
--       TechKeyThruDateKey Integer)
create index on dds.dim_tech(tech_number);
create index on dds.dim_tech(active);
create index on dds.dim_tech(tech_name);
create index on dds.dim_tech(employee_number);

-- SELECT string_agg(column_name, ',') FROM information_schema.columns WHERE table_schema = 'dds'  AND table_name = 'dim_tech';  
-- SELECT string_agg(column_name, ',') FROM information_schema.columns WHERE table_schema = 'ads'  AND table_name = 'ext_dim_tech';  

insert into dds.dim_tech (tech_key,store_code,employee_number,tech_name,
  tech_number,active,labor_cost,flag_department_code,flag_department,
  current_row,row_change_date,row_change_reason,row_from_date,row_thru_date)
select techkey,storecode,employeenumber,name,technumber,active,laborcost,flagdeptcode,flagdept,
  currentrow,rowchangedate,rowchangereason,techkeyfromdate,techkeythrudate
from ads.ext_dim_tech;  

select max(tech_key) + 1 from dds.dim_tech;

create sequence dds.dim_tech_tech_key_seq
  start 998
  owned by dds.dim_tech.tech_key; 
alter table dds.dim_tech
alter column tech_key set default nextval('dds.dim_tech_tech_key_seq');


drop table if exists dds.dim_tech_type_2_changes cascade;
create table dds.dim_tech_type_2_changes (
  tech_key integer not null,
  row_change_reason citext not null,
  store_code citext not null,
  tech_number citext not null,
  tech_name citext not null,
  labor_rate numeric(5,2) not null,
  employee_number citext,
  active boolean not null,
  flag_department_code citext not null);
  
-- this is the start of the xfm function
-- skip the test again stgArkonaSDPTECH, may want at daily review of orders with unknown for any dimension
-- since this is in now way going to be a real fucking warehouse, dispense with the keymap tables
-- type 1 attributes: name
-- type 2 attributes handled by the function: active, labor_cost
-- type 2 attributes that have to be processed manually (generate an email): flag_department.flag_department_code
-- a new row requires manual processing (generate an email)
-- thinking, a separate function for generating an email
-- *NOTE* previously, the email generation was outside of requirements for the fact
--    having second thoughts about that


-- -- some anomalies to fix, possibly after trans(itioning) to new vision (after surgery)
-- select *
-- from dds.dim_Tech a
-- join (
--   select employee_number
--   from dds.dim_tech
--   where active
--     and current_row
--     and employee_number <> 'NA'
--   group by employee_number
--   having count(*) > 1) b on coalesce(a.employee_number, '666') = coalesce(b.employee_number) 

-- 6/12/20 want a new naming protocol, group by the dimension/fact
alter function dds.xfm_dim_tech()
rename to dim_tech_xfm;

create or replace function dds.xfm_dim_tech()
returns void as
$BODY$
/*
*/
truncate dds.xfm_dim_tech;
insert into dds.xfm_dim_tech
select company_number, technician_id, name, labor_rate_a, payroll_emp_,
  case 
    when technician_active = 'Y' then true
    when technician_active = 'N' then false
  end, default_service_type
from arkona.ext_sdptech  
where (
  (company_number = 'RY1' and technician_id not in ('m1','m15','m16','m17','m3','m6','tc','523','530','632','635',
    '521','628','d03','d05','d10','d14','d15','507','67'))
  or
  (company_number = 'RY2' and technician_id not in ('042','176','200','21','28','495','698','980','67')))
  and company_number in ('RY1', 'RY2')
  and technician_id not like 'Q%';-- exclude dealerfx technumbers

truncate dds.dim_tech_type_2_changes;
insert into dds.dim_tech_type_2_changes
SELECT b.tech_key,
  CASE WHEN a.active = false THEN 'active to inactive ' ELSE '' END 
  ||
  CASE WHEN a.labor_cost <> b.labor_cost THEN 'labor cost ' ELSE '' END ,
  a.*
FROM dds.xfm_dim_tech a
INNER JOIN dds.dim_tech b ON a.store_code = b.store_code
  AND a.tech_number = b.tech_number
WHERE b.current_row = true  
  AND b.active = true
  AND (
    a.active = false 
    OR a.labor_cost <> b.labor_cost);

-- type 2 update old row
update dds.dim_tech x
set row_change_date = current_date,
    current_row = false,
    row_change_reason = y.row_change_reason,
    row_thru_date = current_date - 1
from dds.dim_tech_type_2_changes y
where x.tech_key = y.tech_key;
    
-- type 2 insert the new row
insert into dds.dim_tech (store_code,employee_number,tech_name,
  tech_number,active,labor_cost,flag_department_code,flag_department,
  current_row,row_from_date,row_thru_date)
select a.store_code, b.employee_number, b.tech_name, a.tech_number, a.active,
  a.labor_rate, a.flag_department_code,
  CASE 
    WHEN a.flag_department_code = 'BS' THEN 'Body Shop'
    WHEN a.flag_department_code = 'AM' or a.flag_department_code = 'MR' THEN 'Service'
    WHEN a.flag_department_code = 'QL' THEN 'Quick Lane'
    WHEN a.flag_department_code = 'RE' THEN 'Detail'
    WHEN a.flag_department_code = 'QS' THEN 'Quick Service'
  END,
  true, current_date, '12/31/9999'  
from dds.dim_tech_type_2_changes a
join dds.dim_tech b on a.tech_key = b.tech_key;     

-- type 1 changes, use name from pymast because we don't have a dim_employee yet
update dds.dim_tech a
set tech_name = b.employee_name
-- select *
from arkona.xfm_pymast b 
where b.current_row
  and a.employee_number = b.pymast_employee_number
  and a.tech_name <> b.employee_name;  
$BODY$
LANGUAGE sql;

-- generate an email for a new tech or a changed flag department for an existing tech

create or replace function dds.dim_tech_email_update()
returns integer as
$BODY$
/*
select * from dds.dim_tech_email_update();
*/
  select count(*)::integer 
  from (
    select a.*
    from dds.xfm_dim_tech a
    left join dds.dim_tech b on a.store_code = b.store_code
      and a.tech_number = b.tech_number
    where b.store_code is null
      and a.active = true
  union
    select a.*
    from dds.xfm_dim_tech a
    join dds.dim_tech b on a.store_code = b.store_code
      and a.tech_number = b.tech_number
      and b.active
      and b.current_row
      and a.flag_department_code <> b.flag_department_code
where a.active = true) x;
$BODY$
language sql;

-----------------------------------------------------------------------------
--/> dim_tech
-----------------------------------------------------------------------------

