﻿-----------------------------------------------------------------------------
--< dim_payment_type
-- this is just a hand crafted lookup table
-- there is a table in db2, sdplpym, but it does not include S
-- no processing at all, just a look up
-----------------------------------------------------------------------------
-- select * from ads.ext_dim_payment_type

drop table if exists dds.dim_payment_type;
create table dds.dim_payment_type (
  payment_type_key integer primary key,
  payment_type_code citext,
  payment_type citext);

insert into dds.dim_payment_type (payment_type_key,payment_type_code,payment_type)
select paymenttypekey,paymenttypecode,paymenttype
from ads.ext_dim_payment_type;  

select * from dds.dim_payment_type

-- 11 of 2,288,721 lines with an unknown paymenttypekey

select b.the_date, a.* 
-- select count(*)
from ads.ext_Fact_Repair_order a
join dds.dim_date b on a.opendatekey = b.date_key 
where paymenttypekey = 1

select storecode,paymenttypekey, count(*) from ads.ext_Fact_repair_order group by storecode,paymenttypekey order by storecode,paymenttypekey

-----------------------------------------------------------------------------
--/> dim_payment_type 
-----------------------------------------------------------------------------


-----------------------------------------------------------------------------
--< dim_ro_status
-----------------------------------------------------------------------------
-- select * from ads.ext_dim_ro_status
drop table if exists dds.dim_ro_status; 
create table dds.dim_ro_status(
  ro_status_key integer primary key,
  ro_status_code citext,
  ro_status citext);

insert into dds.dim_ro_status
select * from ads.ext_dim_ro_status ; 
-----------------------------------------------------------------------------
--/> dim_ro_status
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
--< dim_line_status
-----------------------------------------------------------------------------
drop table if exists dds.dim_line_status;
create table dds.dim_line_status (
  line_status_key integer primary key,
  line_status_code citext,
  line_status citext);
insert into dds.dim_line_status values(1,'I','Open');  
insert into dds.dim_line_status values(2,'C','Closed');  
-----------------------------------------------------------------------------
--/> dim_line_status
-----------------------------------------------------------------------------