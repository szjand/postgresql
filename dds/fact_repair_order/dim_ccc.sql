﻿-----------------------------------------------------------------------------
--< dim_ccc
-- complaint, cause & correction
-- derived from sdprtxt, pdpphdr, pdppdet
-----------------------------------------------------------------------------
-- select * from ads.ext_dim_ccc limit 10


drop table if exists dds.dim_ccc cascade;
create table dds.dim_ccc (
  ccc_key integer primary key,
  store_code citext,
  ro citext,
  line integer,
  complaint citext,
  cause citext,
  correction citext);

create unique index on dds.dim_ccc(ro,line) ;

-- SELECT string_agg(column_name, ',') FROM information_schema.columns WHERE table_schema = 'dds'  AND table_name = 'dim_ccc';  
-- SELECT string_agg(column_name, ',') FROM information_schema.columns WHERE table_schema = 'ads'  AND table_name = 'ext_dim_ccc';

insert into dds.dim_ccc(ccc_key,store_code,ro,line,complaint,cause,correction)
select ccckey,storecode,ro,line,complaint,cause,correction
-- select count(*)
from ads.ext_dim_ccc;


create sequence dds.dim_ccc_ccc_key_seq
  owned by dds.dim_ccc.ccc_key; 
alter table dds.dim_ccc
alter column ccc_key set default nextval('dds.dim_ccc_ccc_key_seq');

select setval('dds.dim_ccc_ccc_key_seq', (select max(ccc_key) from dds.dim_ccc));


6/28, 1365753 rows
select max(ccc_key) from dds.dim_ccc  -- 1365026
advantage: SELECT MAX(ccckey) FROM dimCCC -- 1365890
ok, to synchronize dim_ccc, need to add ext_dim_ccc to ads_for_luigi
  get max ccc_key from ads.ext_dim_ccc
-- where i am getting hung up, if i just get the new rows from advantage, i miss the rows that get updated
  copy all the new rows from dimccc to ads.ext_dim_ccc
  
on the switchover day, may need to do a full scrape
actually no
if i decide it is needed, can do maybe a one year scrape of sdprtxt and do any 
updates neccessary from that
this is also where the cynicism kicks in, once i am gone nobody is going to use 
this shit anyway


06/29/20
delete from ads.ext_dim_ccc where ccckey > 1364000
synchronize
run the ext_dim_ccc program
synchronize
select max(ccckey) from  ads.ext_dim_ccc -- 1368944
select max(ccc_key) from dds.dim_ccc -- 1367454  

add ext_dim_ccc.py to service_tables.py on 10.130.196.22

ran dds.dim_ccc_update()
select max(ccc_key) from dds.dim_ccc -- 1367454  

7/3/20
ok, looks like this is a new "type" of situation
ext_dim_ccc.py fails because, it updates ads.ext_dim_ccc based on
the max(ccc_key) which is going to be less than the max(ccckey) in dimccc
essentially, the script will always fail unless dds.dim_ccc is
first synchronized with ads.ext_dim_ccc
so, each night before running ext_dim_ccc.py, need to synchronize 
which i just did manually, so in the morning, i should not see that it failed

7/3/20
dim service writer
new writer today, rand the pg scripts and it picked up the new writer, 
have not configured a pg script for adding a new writer yet
so, added the writer in advantage, tomorrow, the keys should be different
and i can synchronize to add the writer to pg

7/4/20
worked as hoped/expected
ran sychronize_dimensions.sql, only took 32 seconds

select count(*) from arkona.ext_sdprtxt  -- 6820991
so, obviously, i dont want to scrape 6,820,991+ rows every day
what i did in adds was to base all the large service tables off of sdprhdr/sdprdet with
any changed date within 45 days


oops, looks like i have not migrated all of dimCCC to postgresql


select sxcode, count(*)
from arkona.ext_sdprtxt
group by sxcode

select * 
from arkona.ext_sdprtxt
where sxcode = 'FA'

select sxcode, sxltyp, count(*)
from arkona.ext_sdprtxt
group by sxcode, sxltyp

looks like sxcode is unnecessary, corresponds to transaction type except i dont know what FA is

what is line type R:
looks like a place holder in the line 950
select * from arkona.ext_sdprtxt where sxltyp = 'R'


-- 06/24
as i step thru copying everthing from ads table dimcc to ads.ext_dim_ccc
lets work on the processing
it is a type 1 dimension

Z: correction
A: complaint
L: cause


-- the same line in sdprtxt and pdppdet can have different comments, have to choose one
-- have to choose which has precedence
-- so, i guess the issue is which one do i keep, sdprtxt or pdppdet  
-- in each case where this exists, the pdppdet data seems to either be more recent or more detailed
-- so, the "solution", not exists pdppdet on sdprtxt, thereby giving precedence to the comment in pdppdet
-- and that fixes the problem
-- drop table if exists wtf;
-- create temp table wtf as

06/28/20
  actually, dont think we need the stg table, this is a type 1 dimension
  for production, this query can be an insert on conflict do update
  
-- drop table if exists dds.dim_ccc_stg cascade;
-- create table dds.dim_ccc_stg (
--   store_code citext,
--   ro citext,
--   line integer,
--   complaint citext,
--   cause citext,
--   correction citext,
--   primary key(store_code,ro,line));


  
CREATE OR REPLACE FUNCTION dds.dim_ccc_update()
  RETURNS void AS
$BODY$
/*
*/
insert into dds.dim_ccc(store_code,ro,line,complaint,cause,correction)
select aa.sxco_, aa.sxro_, aa.sxline, 
  coalesce(bb.complaint, 'N/A') as complaint, coalesce(cc.cause, 'N/A') as cause, coalesce(dd.correction, 'N/A') as correction
from ( -- 1 row per co/ro/line
  select sxco_, sxro_, sxline
  from arkona.ext_sdprtxt
  where sxline < 950
  group by sxco_, sxro_, sxline
  union
  SELECT a.company_number, a.document_number, b.line_number
  FROM arkona.ext_pdpphdr a
  INNER JOIN arkona.ext_PDPPDET b ON a.pending_key = b.pending_key
  WHERE a.document_type = 'RO'
    AND a.document_number <> ''
    AND b.comments <> ''
    and b.line_number < 950
  group by a.company_number, a.document_number, b.line_number) aa
left join ( -- complaint
  select sxco_, sxro_, sxline, replace(string_agg(trim(sxtext), ' ' order by sxseq_), '  ', ' ') as complaint
  from arkona.ext_sdprtxt aa
  where sxltyp = 'A'
    and not exists (
      select 1
      from arkona.ext_pdpphdr a
      join arkona.ext_pdppdet b on a.pending_key = b.pending_key
        and a.company_number = aa.sxco_
        and a.document_number = aa.sxro_
        and b.line_number = aa.sxline
        and b.line_type = 'A')  
  group by sxco_, sxro_, sxline
  union
  SELECT a.company_number, a.document_number, b.line_number, replace(string_agg(trim(b.comments), ' ' order by sequence_number), '  ', ' ')
  FROM arkona.ext_pdpphdr a
  INNER JOIN arkona.ext_PDPPDET b ON a.pending_key = b.pending_key
  WHERE a.document_type = 'RO'
    AND a.document_number <> ''
    AND b.line_type = 'A'
    AND b.comments <> ''
  group by a.company_number, a.document_number, b.line_number, b.line_type) bb on aa.sxco_ = bb.sxco_
    and aa.sxro_ = bb.sxro_
    and aa.sxline = bb.sxline
left join ( -- cause
  select sxco_, sxro_, sxline, replace(string_agg(trim(sxtext), ' ' order by sxseq_), '  ', ' ') as cause
  from arkona.ext_sdprtxt aa
  where sxltyp = 'L'
    and not exists (
      select 1
      from arkona.ext_pdpphdr a
      join arkona.ext_pdppdet b on a.pending_key = b.pending_key
        and a.company_number = aa.sxco_
        and a.document_number = aa.sxro_
        and b.line_number = aa.sxline
        and b.line_type = 'L')    
  group by sxco_, sxro_, sxline
  union
  SELECT a.company_number, a.document_number, b.line_number, replace(string_agg(trim(b.comments), ' ' order by sequence_number), '  ', ' ')
  FROM arkona.ext_pdpphdr a
  INNER JOIN arkona.ext_PDPPDET b ON a.pending_key = b.pending_key
  WHERE a.document_type = 'RO'
    AND a.document_number <> ''
    AND b.line_type = 'L'
    AND b.comments <> ''
  group by a.company_number, a.document_number, b.line_number, b.line_type) cc on aa.sxco_ = cc.sxco_
      and aa.sxro_ = cc.sxro_
      and aa.sxline = cc.sxline 
left join ( -- correction
  select sxco_, sxro_, sxline, replace(string_agg(trim(sxtext), ' ' order by sxseq_), '  ', ' ') as correction
  from arkona.ext_sdprtxt aa
  where sxltyp = 'Z'
    and not exists (
      select 1
      from arkona.ext_pdpphdr a
      join arkona.ext_pdppdet b on a.pending_key = b.pending_key
        and a.company_number = aa.sxco_
        and a.document_number = aa.sxro_
        and b.line_number = aa.sxline
        and b.line_type = 'Z')      
  group by sxco_, sxro_, sxline
  union
  SELECT a.company_number, a.document_number, b.line_number, replace(string_agg(trim(b.comments), ' ' order by sequence_number), '  ', ' ')
  FROM arkona.ext_pdpphdr a
  INNER JOIN arkona.ext_PDPPDET b ON a.pending_key = b.pending_key
  WHERE a.document_type = 'RO'
    AND a.document_number <> ''
    AND b.line_type = 'Z'
    AND b.comments <> ''
  group by a.company_number, a.document_number, b.line_number, b.line_type) dd on aa.sxco_ = dd.sxco_
      and aa.sxro_ = dd.sxro_
      and aa.sxline = dd.sxline
on conflict (ro,line) 
  do update
    set (store_code,ro,line,complaint,cause,correction)
      =
      (excluded.store_code,excluded.ro,excluded.line,excluded.complaint,excluded.cause,excluded.correction);
$BODY$
LANGUAGE sql;

-- -- this is all the unraveling for the multiple rows per line
-- -- wtf, the table built on union of sdprtxt & pdpphdr/det, has more rows than the table
-- -- of just co/ro/line
-- select *
-- from (
--   select sxco_, sxro_, sxline, count(*) as the_count
--   from wtf
--   group by sxco_, sxro_, sxline) a
-- join (
--   select sxco_, sxro_, sxline, count(*) as the_count
--   from dim_ccc_ros
--   group by sxco_, sxro_, sxline) b on a.sxco_ = b.sxco_ and a.sxro_ = b.sxro_ and a.sxline = b.sxline
--     and a.the_count <> b.the_count
-- 
-- -- the issue is different comments in sdprtxt and pdppdet for the same line
-- select sxco_, sxro_, sxline, replace(string_agg(trim(sxtext), ' ' order by sxseq_), '  ', ' ') as complaint
-- from arkona.ext_sdprtxt
-- where sxltyp = 'A'
--   and sxro_ = '16397787'
-- group by sxco_, sxro_, sxline
-- union
-- SELECT a.company_number, a.document_number, b.line_number, replace(string_agg(trim(b.comments), ' ' order by sequence_number), '  ', ' ')
-- FROM arkona.ext_pdpphdr a
-- INNER JOIN arkona.ext_PDPPDET b ON a.pending_key = b.pending_key
-- WHERE a.document_type = 'RO'
--   and a.document_number = '16397787'
--   AND a.document_number <> ''
--   AND b.line_type = 'A'
--   AND b.comments <> ''
-- group by a.company_number, a.document_number, b.line_number, b.line_type
-- 
-- -- from db2
-- -- fucking 8 rows match between sdprtxt and pdppdet with difference in comment
-- select a.*, c.ptcmnt, c.transaction_date, c.ptpkey
-- from rydedata.sdprtxt a
-- join rydedata.pdpphdr b on a.sxco# = b.ptco#
--   and a.sxro# = b.ptdoc#
-- join rydedata.pdppdet c on b.ptpkey = c.ptpkey
--   and a.sxline = c.ptline
--   and a.sxseq# = c.ptseq#
--   and a.sxtext <> c.ptcmnt  
--   and a.sxltyp = c.ptltyp
-- WHERE b.ptdtyp = 'RO'
--   AND b.ptdoc# <> ''
-- 
-- SXCO#	SXRO#	SXLINE	SXLTYP	SXSEQ#	SXCODE	SXTEXT	PTCMNT	                            TRANSACTION_DATE	PTPKEY
-- RY2	  2820989	1	Z	8	  CM	checked operation ,.	:: checked operation ,.Camera is working normal	  20200625	467326
-- RY1	 16399189	5	A	13	IS	RETURNED 5.28 AT 1700	:: RETURNED 06.05 AT 1700	                        20200528	1886223
-- RY1	 16403886	6	A	13	WS	RETURNED 06/24/2020 AT 17:00	:: RETURNED 06/25/2020 AT 17:00	          20200624	1900218
-- RY1	 16402607	3	A	13	SC	RETURNED 06.18 AT 1700	:: RETURNED 06.22 AT 1700	                      20200617	1896464
-- RY1	 16401930	4	A	13	WS	RETURNED 6.24 AT 1700	:: RETURNED 6.25 AT 1700	                          20200612	1894497
-- RY1	 16403656	4	A	13	WS	RETURNED 06/25/2020 AT 17:00	:: RETURNED 06/26/2020 AT 17:00	          20200623	1899454
-- RY2	  2822404	1	L	14	CS	battery since new.	:: battery since new. Intermittent internal failure,	20200626	471332
-- RY1	 16397787	5	A	13	WS	returned 06.18 at 1700	:: returned 06.19 at 1700	                      20200519	1881937  
-- 
-- have to choose which has precedence
-- so, i guess the issue is which one do i keep, sdprtxt or pdppdet  


-----------------------------------------------------------------------------
--/> dim_ccc
-----------------------------------------------------------------------------


