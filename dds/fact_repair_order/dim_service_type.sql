﻿-----------------------------------------------------------------------------
--< dim_service_type
-- ads doesn't do a nightly extract on SDPSVCT
-- add the extract to luigi ext_arkona
-- then just check for any new service type
-----------------------------------------------------------------------------
-- select * from ads.ext_dim_service_type

drop table if exists dds.dim_service_type;
create table dds.dim_service_type(
  service_type_key integer primary key,
  service_type_code citext,
  service_type citext);

insert into dds.dim_service_type (service_type_key,service_type_code,service_type)
select servicetypekey,servicetypecode,servicetype
from ads.ext_dim_service_type;

CREATE OR REPLACE FUNCTION dds.dim_service_type_email_update()
  RETURNS integer AS
$BODY$
/*
  don't know why i didn't go with the same descriptions used in sdpsvct
  essentially just a look up table
  all we are doing with dds.dim_service_type is checking for a new service_type_code
  select dds.dim_service_type_email_update();
*/
select count(*)::integer
-- select *
from arkona.ext_sdpsvct a
where active_flag = 'Y'
  and company_number in ('RY1','RY2')
  and not exists (
    select 1
    from dds.dim_service_type
    where service_type_code = a.service_type);
$BODY$
language sql;    

-----------------------------------------------------------------------------
--/> dim_service_type
-----------------------------------------------------------------------------