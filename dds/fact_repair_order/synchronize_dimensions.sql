﻿-----------------------------------------------------------------------------
--< dim_tech
-----------------------------------------------------------------------------

drop table if exists dds.dim_tech cascade;
create table dds.dim_tech (
      tech_key integer primary key,
      store_code citext,
      employee_number citext,
      tech_name citext,
--       Description CIChar( 20 ),
      tech_number citext,
      active boolean,
      labor_cost numeric(8,2),
      flag_department_code citext,
      flag_department citext,
      current_row boolean,
      row_change_date Date,
--       RowChangeDateKey Integer,
--       RowFromTS TimeStamp,
--       RowThruTS TimeStamp,
      row_change_reason citext,
      row_from_date Date,
--       TechKeyFromDateKey Integer,
      row_thru_date Date default '12/31/9999' check (row_thru_date >= row_from_date));
--       TechKeyThruDateKey Integer)
create index on dds.dim_tech(tech_number);
create index on dds.dim_tech(active);
create index on dds.dim_tech(tech_name);
create index on dds.dim_tech(employee_number);

-- SELECT string_agg(column_name, ',') FROM information_schema.columns WHERE table_schema = 'dds'  AND table_name = 'dim_tech';  
-- SELECT string_agg(column_name, ',') FROM information_schema.columns WHERE table_schema = 'ads'  AND table_name = 'ext_dim_tech';  

insert into dds.dim_tech (tech_key,store_code,employee_number,tech_name,
  tech_number,active,labor_cost,flag_department_code,flag_department,
  current_row,row_change_date,row_change_reason,row_from_date,row_thru_date)
select techkey,storecode,employeenumber,name,technumber,active,laborcost,flagdeptcode,flagdept,
  currentrow,rowchangedate,rowchangereason,techkeyfromdate,techkeythrudate
from ads.ext_dim_tech;  

create sequence dds.dim_tech_tech_key_seq
  owned by dds.dim_tech.tech_key; 
alter table dds.dim_tech
alter column tech_key set default nextval('dds.dim_tech_tech_key_seq');

select setval('dds.dim_tech_tech_key_seq', (select max(tech_key)from dds.dim_tech));
-----------------------------------------------------------------------------
--/> dim_tech
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
--< dim_service_writer
-----------------------------------------------------------------------------

drop table if exists dds.dim_service_writer cascade;
CREATE TABLE dds.dim_service_writer ( 
  service_writer_key integer primary key ,
  store_code citext not null,
  employee_number citext,
  writer_name citext,
  description citext,
  writer_number citext not null,
  dealertrack_user_name citext,
  active boolean,
  default_service_type citext,
  census_department citext not null,
  current_row boolean,
  row_change_date Date,
  row_change_reason citext,
  row_from_date Date,
  row_thru_date Date default '12/31/9999' check (row_thru_date >= row_from_date));

-- back fill with data from ads 
insert into dds.dim_service_writer(service_writer_key,store_code,employee_number,writer_name,
  description,writer_number,dealertrack_user_name,active,default_service_type,census_department,
  current_row,row_change_date, row_change_reason,row_from_date,row_thru_date)
select servicewriterkey,storecode,employeenumber,name,description,writernumber,dtusername,
  active,defaultservicetype,censusdept,currentrow,rowchangedate,rowchangereason,
  servicewriterkeyfromdate,servicewriterkeythrudate
from ads.ext_dim_service_writer;

create sequence dds.dim_service_writer_service_writer_key_seq
  owned by dds.dim_service_writer.service_writer_key; 
alter table dds.dim_service_writer
alter column service_writer_key set default nextval('dds.dim_service_writer_service_writer_key_seq');

select setval('dds.dim_service_writer_service_writer_key_seq', (select max(service_writer_key) from dds.dim_service_writer));
-----------------------------------------------------------------------------
--/> dim_service_writer
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
--< dim_vehicle
-----------------------------------------------------------------------------
drop table if exists dds.dim_vehicle cascade;
CREATE TABLE dds.dim_vehicle ( 
      vehicle_key integer primary key,
      vin citext,
      model_year citext,
      make citext,
      model citext,
      model_code citext,
      body citext,
      color citext);
      
create unique index on dds.dim_vehicle(vin);

insert into dds.dim_vehicle(vehicle_key,vin,model_year,make,model,model_code,body,color)  
select vehiclekey,vin,modelyear,make,model,modelcode,body,color
from ads.ext_dim_vehicle;

create sequence dds.dim_vehicle_vehicle_key_seq
  owned by dds.dim_vehicle.vehicle_key; 
alter table dds.dim_vehicle
alter column vehicle_key set default nextval('dds.dim_vehicle_vehicle_key_seq');

select setval('dds.dim_vehicle_vehicle_key_seq', (select max(vehicle_key)from dds.dim_vehicle));
-----------------------------------------------------------------------------
--/> dim_vehicle
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
--< dim_customer
-----------------------------------------------------------------------------
drop table if exists dds.dim_customer cascade;   
CREATE TABLE dds.dim_customer ( 
      customer_key integer primary key,
      store_code citext,
      bnkey Integer,
      customer_type_code citext,
      customer_type citext,
      full_name citext,
      last_name citext,
      first_name citext,
      middle_name citext,
      home_phone citext,
      business_phone citext,
      cell_phone citext,
      email citext,
      email_valid boolean,
      email_2  citext,
      email_2_valid boolean,
      city  citext,
      county citext,
      state citext,
      zip citext,
      has_valid_email boolean,
      do_not_call boolean);

create unique index on dds.dim_customer(bnkey);

-- SELECT string_agg(column_name, ',') FROM information_schema.columns WHERE table_schema = 'ads'  AND table_name = 'ext_dim_customer';
-- SELECT string_agg(column_name, ',') FROM information_schema.columns WHERE table_schema = 'dds'  AND table_name = 'dim_customer';

insert into dds.dim_customer(customer_key,store_code,bnkey,customer_type_code,
  customer_type,full_name,last_name,first_name,middle_name,home_phone,
  business_phone,cell_phone,email,email_valid,email_2,email_2_valid,city,
  county,state,zip,has_valid_email,do_not_call)
select customerkey,storecode,bnkey,customertypecode,customertype,fullname,lastname,
  firstname,middlename,homephone,businessphone,cellphone,email,emailvalid,email2,
  email2valid,city,county,state,zip,hasvalidemail,do_not_call
from ads.ext_dim_customer;  

create sequence dds.dim_customer_customer_key_seq
  owned by dds.dim_customer.customer_key; 
alter table dds.dim_customer
alter column customer_key set default nextval('dds.dim_customer_customer_key_seq');

select setval('dds.dim_customer_customer_key_seq', (select max(customer_key)from dds.dim_customer));
-----------------------------------------------------------------------------
--/> dim_customer
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
--< dim_ccc
-----------------------------------------------------------------------------
drop table if exists dds.dim_ccc cascade;
create table dds.dim_ccc (
  ccc_key integer primary key,
  store_code citext,
  ro citext,
  line integer,
  complaint citext,
  cause citext,
  correction citext);
create unique index on dds.dim_ccc(ro,line) ;

insert into dds.dim_ccc(ccc_key,store_code,ro,line,complaint,cause,correction)
select ccckey,storecode,ro,line,complaint,cause,correction
from ads.ext_dim_ccc;

create sequence dds.dim_ccc_ccc_key_seq
  owned by dds.dim_ccc.ccc_key; 
alter table dds.dim_ccc
alter column ccc_key set default nextval('dds.dim_ccc_ccc_key_seq');

select setval('dds.dim_ccc_ccc_key_seq', (select max(ccc_key)from dds.dim_ccc));

-----------------------------------------------------------------------------
--/> dim_ccc
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
--< dim_ro_comment
-----------------------------------------------------------------------------
drop table if exists dds.dim_ro_comment cascade;
create table dds.dim_ro_comment(
    ro_comment_key integer primary key,
    store_code citext,
    ro citext,
    comment_type citext,
    ro_comment citext);
create unique index on dds.dim_ro_comment(store_code, ro);

insert into dds.dim_ro_comment(ro_comment_key,store_code,ro,comment_type,ro_comment)
select rocommentkey,storecode,ro,commenttype,comment
from ads.ext_dim_ro_comment;

create sequence dds.ro_comment_ro_comment_key_seq
  owned by dds.dim_ro_comment.ro_comment_key; 
alter table dds.dim_ro_comment
alter column ro_comment_key set default nextval('dds.ro_comment_ro_comment_key_seq');

select setval('dds.ro_comment_ro_comment_key_seq', (select max(ro_comment_key) from dds.dim_ro_comment));

-----------------------------------------------------------------------------
--/> dim_ro_comment
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
--< dim_opcode
-----------------------------------------------------------------------------

drop table if exists dds.dim_opcode cascade;
create table dds.dim_opcode (
  opcode_key integer primary key,
  store_code citext,
  opcode citext,
  description citext,
  pdqcat1 citext,
  pdqcat2 citext,
  pdqcat3 citext);
create unique index on dds.dim_opcode(store_code,opcode);

insert into dds.dim_opcode(opcode_key,store_code,opcode,description,pdqcat1,pdqcat2,pdqcat3)
select opcodekey,storecode,opcode,description,pdqcat1,pdqcat2,pdqcat3
from ads.ext_dim_opcode;

create sequence dds.dim_opcode_opcode_key_seq
  owned by dds.dim_opcode.opcode_key; 
alter table dds.dim_opcode
alter column opcode_key set default nextval('dds.dim_opcode_opcode_key_seq');

select setval('dds.dim_opcode_opcode_key_seq', (select max(opcode_key) from dds.dim_opcode));

-----------------------------------------------------------------------------
--/> dim_opcode
-----------------------------------------------------------------------------