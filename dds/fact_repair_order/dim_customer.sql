﻿-----------------------------------------------------------------------------
--< dim_customer
-----------------------------------------------------------------------------
-- not really a dimension, type 1 from bopname, only bringing a subset of fields into ads.ext_dim_customer
-- none of the type 2 tracking attributes
-- good enough for the repair order fact
/*
major wtf
dimcustomer is all RY1 with a NK of bnkey
bopname PK is bopname_Company_number/bopname_record_key
what i am doing in advantage is excluding records where the customer_type = ''

--15291 of these (null company_individ), mostly company, mostly finance companies
select timestamp_updated, a.bopname_company_number, a.bopname_record_key, a.company_individ, a.bopname_search_name,
  a.last_company_name, a.first_name, a.middle_init, a.phone_number, a.business_phone, a.cell_phone, 
  a.email_address, a.second_email_address2, a.city, a.county, a.state_code, a.zip_code,
  a.allow_contact_by_phone
from arkona.ext_bopname a where company_individ is null
order by bopname_record_key

select left(coalesce(optional_field, 'XXX'), 7), count(*) from arkona.ext_bopname where company_individ is null group by left(coalesce(optional_field, 'XXX'), 7)
ok, for those records where copmany_individ is null:
BOPSSRC;158
BOPTRAD;10979  trade vin, looks like these are the banks to whom trade pay offs are due
BOPGSRC;4  gap sources
GLPBNKA;11  bank account def
BOPMAST;3870  car deal
XXX;208  jessica wahl: canceled appointment
BOPPDIC;61  physical damage insurance co definition

-- -- this convinces me i am wasting my time
-- select a.bopname_record_key, a.bopname_search_name, company_individ, b.bopmast_Stock_number, c.bopmast_stock_number, a.timestamp_updated
-- from arkona.ext_bopname a
-- left join arkona.ext_bopmast b on a.bopname_record_key = b.buyer_number
-- left join arkona.ext_bopmast c on a.bopname_record_key = c.co_buyer_number
-- where a.optional_Field = 'BOPMAST'
*/

-- fuck it the purpose is to move to pg from advantage, not redesign the fucking warehouse
-- so, just do the same thing in pg that is being done in ads and call it good enuf
-- MUST RESIST TEMPTATION TO DO A MORE REAL DIMENSION TABLE

drop table if exists dds.dim_customer;   
CREATE TABLE dds.dim_customer ( 
      customer_key integer primary key,
      store_code citext,
      bnkey Integer,
      customer_type_code citext,
      customer_type citext,
      full_name citext,
      last_name citext,
      first_name citext,
      middle_name citext,
      home_phone citext,
      business_phone citext,
      cell_phone citext,
      email citext,
      email_valid boolean,
      email_2  citext,
      email_2_valid boolean,
      city  citext,
      county citext,
      state citext,
      zip citext,
      has_valid_email boolean,
      do_not_call boolean);

create unique index on dds.dim_customer(bnkey);

-- SELECT string_agg(column_name, ',') FROM information_schema.columns WHERE table_schema = 'ads'  AND table_name = 'ext_dim_customer';
-- SELECT string_agg(column_name, ',') FROM information_schema.columns WHERE table_schema = 'dds'  AND table_name = 'dim_customer';

insert into dds.dim_customer(customer_key,store_code,bnkey,customer_type_code,
  customer_type,full_name,last_name,first_name,middle_name,home_phone,
  business_phone,cell_phone,email,email_valid,email_2,email_2_valid,city,
  county,state,zip,has_valid_email,do_not_call)
select customerkey,storecode,bnkey,customertypecode,customertype,fullname,lastname,
  firstname,middlename,homephone,businessphone,cellphone,email,emailvalid,email2,
  email2valid,city,county,state,zip,hasvalidemail,do_not_call
from ads.ext_dim_customer;  

select max(customer_key) + 1 from dds.dim_customer;  -- 241289

create sequence dds.dim_customer_customer_key_seq
  start 241458
  owned by dds.dim_customer.customer_key; 
alter table dds.dim_customer
alter column customer_key set default nextval('dds.dim_customer_customer_key_seq');

so, what is done in ads
scrape bopname where timestamp_updated > current_date - 7 into tmpBopname
sp stgArkBopname()
  DELETE 
  FROM stgArkonaBOPNAME
  WHERE bnkey IN (
    SELECT bnkey
    FROM tmpbopname); 
  INSERT INTO stgArkonaBOPNAME
  SELECT * FROM tmpbopname;  
sp xfmCustDim() 
  categorizes email, valid/not valid 
  inserts new rows
  updates existing rows (type 1)
sp xfmDimCustomerEmailUpdate
  rescrapes the entire bopname to update email addresses
  also used the old service scheduler as a possible source of email addresses

using xfm_bopname, should be able to upsert based on row_from_date
this should include any rows for which the email has changed

-- wait a minute, i don't want every instance, i just want the most current instance
select a.bopname_company_number, a.bopname_record_key, a.company_individ, a.bopname_search_name, 
  a.last_company_name, a.first_name, a.middle_init, a.phone_number, a.business_phone, a.cell_phone, 
  a.email_address, a.second_email_address2, a.city, a.county, a.state_code, a.zip_code,
  a.allow_contact_by_phone
from arkona.xfm_bopname a
join ( -- rows added or changed on current date
  select bopname_company_number, bopname_record_key
  from arkona.xfm_bopname
  where row_from_date = current_date
    -- restriction brought over from ads
    and bopname_company_number in ('RY1','RY2')
    and bopname_search_name is not null
    and company_individ is not null) b on a.bopname_company_number = b.bopname_company_number
    and a.bopname_Record_key = b.bopname_record_key
order by a.bopname_record_key, a.row_From_Date    

   
-- this is all i need
select a.bopname_company_number, a.bopname_record_key, a.company_individ, a.bopname_search_name, 
  a.last_company_name, a.first_name, a.middle_init, a.phone_number, a.business_phone, a.cell_phone, 
  a.email_address, a.second_email_address2, a.city, a.county, a.state_code, a.zip_code,
  a.allow_contact_by_phone
from arkona.xfm_bopname a 
where row_from_date = current_date
  -- restriction brought over from ads
  and bopname_company_number in ('RY1','RY2')
  and bopname_search_name is not null
  and company_individ is not null
  
-- 61/85 new rows
-- 6/13, don't need to do the dates, any current_row in xfm_Bopname that is not in (bopname_Record_key/bnkey) dim_customer
select a.row_From_Date, a.bopname_company_number, a.bopname_record_key, a.company_individ, a.bopname_search_name, 
  a.last_company_name, a.first_name, a.middle_init, a.phone_number, a.business_phone, a.cell_phone, 
  a.email_address, a.second_email_address2, a.city, a.county, a.state_code, a.zip_code,
  a.allow_contact_by_phone
from arkona.xfm_bopname a 
left join dds.dim_customer b on a.bopname_record_key = b.bnkey
where a.current_row -- row_from_date between  current_date - 3 and current_date -- = current_date
  -- restriction brought over from ads
  and bopname_company_number in ('RY1','RY2')
  and bopname_search_name is not null
  and company_individ is not null
  and b.bnkey is null



-- 245 changed/705 rows
-- changed rows seems a bit trickier
-- assuming a unique bopname_record_key/bnkey
-- any current row in bopmast that is different than the row in dim_customer
-- so hash seems the way to go
select a.bopname_company_number, a.bopname_record_key, a.company_individ, a.bopname_search_name, 
  a.last_company_name, a.first_name, a.middle_init, a.phone_number, a.business_phone, a.cell_phone, 
  a.email_address, a.second_email_address2, a.city, a.county, a.state_code, a.zip_code,
  a.allow_contact_by_phone
from arkona.xfm_bopname a 
join dds.dim_customer b on a.bopname_record_key = b.bnkey
where row_from_date between  current_date - 7 and current_date -- = current_date
  and current_row
  -- restriction brought over from ads
  and bopname_company_number in ('RY1','RY2')
  and bopname_search_name is not null
  and company_individ is not null
order by a.bopname_record_key

select bopname_record_key from (
select a.bopname_company_number, a.bopname_record_key, a.company_individ, a.bopname_search_name, 
  a.last_company_name, a.first_name, a.middle_init, a.phone_number, a.business_phone, a.cell_phone, 
  a.email_address, a.second_email_address2, a.city, a.county, a.state_code, a.zip_code,
  a.allow_contact_by_phone,
  (
    select  md5(z::text) as hash
    from (
      select aa.bopname_company_number, aa.bopname_record_key, aa.company_individ, aa.bopname_search_name, 
        aa.last_company_name, aa.first_name, aa.middle_init, aa.phone_number, aa.business_phone, aa.cell_phone, 
        aa.email_address, aa.second_email_address2, aa.city, aa.county, aa.state_code, aa.zip_code,
        aa.allow_contact_by_phone
      from arkona.xfm_bopname aa
      join dds.dim_customer bb on a.bopname_record_key = bb.bnkey
      where aa.current_row
        and aa.row_from_date between  current_date - 3 and current_date -- = current_date
        and aa.bopname_record_key = a.bopname_record_key) z)
from arkona.xfm_bopname a 
join dds.dim_customer b on a.bopname_record_key = b.bnkey
where a.row_from_date between  current_date - 3 and current_date -- = current_date
  and a.current_row
  -- restriction brought over from ads
  and a.bopname_company_number in ('RY1','RY2')
  and a.bopname_search_name is not null
  and a.company_individ is not null
) x group by bopname_record_key having count(*) > 1

or simply update all rows in dim_cust with values from xfm_bopmast where row_from_Date

ok, going with insert on conflict, but first need to generated the derived rows

-- SELECT string_agg(column_name, ',') FROM information_schema.columns WHERE table_schema = 'dds'  AND table_name = 'dim_customer'

create or replace function dds.dim_customer_update()
returns void as
$BODY$
/*
select dds.dim_customer_update()
*/
  insert into dds.dim_customer (store_code,bnkey,customer_type_code,customer_type,
    full_name,last_name,first_name,middle_name,home_phone,business_phone,
    cell_phone,email,email_valid,email_2,email_2_valid,city,county,state,zip,
    has_valid_email,do_not_call)
  select a.bopname_company_number, a.bopname_record_key, a.company_individ, 
    case a.company_individ
      when 'I' then 'Person'
      when 'C' then 'Company'
    end as customer_type,
    a.bopname_search_name, a.last_company_name, a.first_name, a.middle_init, 
    a.phone_number, a.business_phone, a.cell_phone, 
    a.email_address, 
    CASE 
      when a.email_address is null then false
      WHEN length(a.email_address) = 0 THEN False
      WHEN position('@' IN a.email_address) = 0 THEN False
      WHEN position ('.' IN a.email_address) = 0 THEN False
      WHEN position('wng' IN a.email_address) > 0 THEN False
      WHEN position('dnh' IN a.email_address) > 0 THEN False
      WHEN position('noemail' IN a.email_address) > 0 THEN False
      WHEN position('none@' IN a.email_address) > 0 THEN False
      WHEN position('dng' IN a.email_address) > 0 THEN False
      ELSE True
    END AS email_valid,   
    a.second_email_address2, 
    CASE 
      when a.second_email_address2 is null then false
      WHEN length(a.second_email_address2) = 0 THEN False
      WHEN position('@' IN a.second_email_address2) = 0 THEN False
      WHEN position ('.' IN a.second_email_address2) = 0 THEN False
      WHEN position('wng' IN a.second_email_address2) > 0 THEN False
      WHEN position('dnh' IN a.second_email_address2) > 0 THEN False
      WHEN position('noemail' IN a.second_email_address2) > 0 THEN False
      WHEN position('none@' IN a.second_email_address2) > 0 THEN False
      WHEN position('dng' IN a.second_email_address2) > 0 THEN False
      ELSE True
    END AS email_2_valid,  
    a.city, a.county, a.state_code, a.zip_code,
    case
      when (
        case
          when a.email_address is null then false
          WHEN length(a.email_address) = 0 THEN False
          WHEN position('@' IN a.email_address) = 0 THEN False
          WHEN position ('.' IN a.email_address) = 0 THEN False
          WHEN position('wng' IN a.email_address) > 0 THEN False
          WHEN position('dnh' IN a.email_address) > 0 THEN False
          WHEN position('noemail' IN a.email_address) > 0 THEN False
          WHEN position('none@' IN a.email_address) > 0 THEN False
          WHEN position('dng' IN a.email_address) > 0 THEN False
          ELSE True
        end)
      or (
        case
          when a.second_email_address2 is null then false
          WHEN length(a.second_email_address2) = 0 THEN False
          WHEN position('@' IN a.second_email_address2) = 0 THEN False
          WHEN position ('.' IN a.second_email_address2) = 0 THEN False
          WHEN position('wng' IN a.second_email_address2) > 0 THEN False
          WHEN position('dnh' IN a.second_email_address2) > 0 THEN False
          WHEN position('noemail' IN a.second_email_address2) > 0 THEN False
          WHEN position('none@' IN a.second_email_address2) > 0 THEN False
          WHEN position('dng' IN a.second_email_address2) > 0 THEN False
          ELSE True      
        end) = true then true
      else false
    end as has_valid_email,
    case 
      when a.allow_contact_by_phone = 'N' then true
      else false
    end as do_not_call
  from arkona.xfm_bopname a 
  where a.current_row 
    and a.row_from_date > current_date - 7
    -- restriction brought over from ads
    and a.bopname_company_number in ('RY1','RY2')
    and a.bopname_search_name is not null
    and a.company_individ is not null
  on conflict (bnkey)  
    do update
      set (store_code,bnkey,customer_type_code,customer_type,full_name,last_name,
            first_name,middle_name,home_phone,business_phone,cell_phone,email,
            email_valid,email_2,email_2_valid,city,county,state,zip,
            has_valid_email,do_not_call)
        =
          (excluded.store_code,excluded.bnkey,excluded.customer_type_code,
            excluded.customer_type,excluded.full_name,excluded.last_name,
            excluded.first_name,excluded.middle_name,excluded.home_phone,
            excluded.business_phone,excluded.cell_phone,excluded.email,
            excluded.email_valid,excluded.email_2,excluded.email_2_valid,
            excluded.city,excluded.county,excluded.state,excluded.zip,
            excluded.has_valid_email,excluded.do_not_call);  
$BODY$
language sql;

my concern is that there is no way to synchronize 2 separate processes generating customer keys
give it another day, resynchronized dds and ads (dropped and created dds.dim_customer and populated from ads.ext_dim_customer)
of course, now they are in synch
lets see what happens tomorrow

-- this is what i need to test on 6/14 and subsequent days
-- this might apply to all dimension: insure the PK & NK match between ads and dds
select a.customerkey, a.bnkey
from ads.ext_dim_customer a
left join dds.dim_customer b on a.customerkey = b.customer_key and a.bnkey = b.bnkey
where b.bnkey is null

select a.* -- 79 rows
from dds.dim_customer a
left join ads.ext_Dim_customer b on a.customer_key = b.customerkey
where b.customerkey is null 

drop table if exists the_bnkey;
create temp table the_bnkey as
select 'dds'::citext as source, a.customer_key, a.bnkey, a.full_name
from dds.dim_customer a
left join ads.ext_Dim_customer b on a.customer_key = b.customerkey
where b.customerkey is null; 

select * from the_bnkey
union
select 'ads'::citext as source, customerkey,bnkey,fullname
from ads.ext_dim_customer c
where bnkey in (select bnkey from the_bnkey)
order by bnkey, source



-- SELECT string_agg('excluded.' || column_name, ',') FROM information_schema.columns WHERE table_schema = 'dds'  AND table_name = 'dim_customer'
/* 
2. 6/12 i reran xfm_bopname today and that generated rows where the row_from_date > row_thru_date
    if a row, (eg bopname_recored_key 217418) was changed sometime today and had been updated
    in the luigi run last night, the row created last night was closed and thru_date changed to 6/11 (current_date - 1)
    to facilitate the second row with a from date of 6/12 being added

fuck me, this will be a problem for all xfm tables,
i dont know a workaround yet, but i can put a stop to it occurring by adding a check constraint, row_thru_date > row_from_date
i cant just delete the new extra row, the old row now longer has a thru date of 12/31/9999

cleaned up my fuck up caused by rerunning xfmBopname
-- -- 145 rows, includes new rows added today
-- select bopname_Record_key from arkona.xfm_bopname where timestamp_updated::date = current_date
-- 
-- fuck me, i have to clean up xfm_bopname
-- -- these are the ones that have to be fixed
-- -- new rows added this afternoon should not be a problem, they would have been added tonight 
-- select bopname_Record_key from arkona.xfm_bopname where row_thru_date < row_from_date
-- 217418
-- 217718
-- 222078
-- 248732
-- 266450
-- 1044297
-- 1088334
-- 1148318
-- 1148319
-- 
-- select * from arkona.xfm_Bopname where bopname_record_key = 1148319 order by bopname_key
-- -- get rid of the superfluous new row
-- delete from arkona.xfm_Bopname where bopname_key = 426438;
-- -- fix the old row with the from date > thru date
-- update arkona.xfm_bopname
--   set current_row = true,
--       row_thru_date = '12/31/9999'
-- where bopname_key = 426146;
*/

/* 
and add the check constraints to all the relevant xfm tables
ok, no other xfm tables with from > thru
select pymast_key from arkona.xfm_pymast where row_thru_date < row_from_date

ok, at least in bopname, 2075 rows where row_from_Date = row_thru_date, sl the check constraint has to be  >=
select * from arkona.xfm_Bopname where bopname_record_key = 1128073 order by bopname_key

alter table arkona.xfm_bopname
add constraint check_xfm_bopname
check (row_thru_date >= row_from_date);

alter table arkona.xfm_bopmast
add constraint check_xfm_bopmast
check (row_thru_date >= row_from_date);

alter table arkona.xfm_pyactgr
add constraint check_xfm_pyactgr
check (row_thru_date >= row_from_date);

alter table arkona.xfm_inpmast
add constraint check_xfm_inpmast
check (row_thru_date >= row_from_date);

alter table arkona.xfm_pymast
add constraint check_xfm_pymast
check (row_thru_date >= row_from_date);
*/
-----------------------------------------------------------------------------
--/> dim_customer
-----------------------------------------------------------------------------
