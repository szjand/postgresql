﻿
-----------------------------------------------------------------------------
--< dim_ro_comment
-- derived from sdprtxt, pdpphdr, pdppdet
-----------------------------------------------------------------------------
drop table if exists dds.dim_ro_comment;
create table dds.dim_ro_comment(
    ro_comment_key integer primary key,
    store_code citext,
    ro citext,
    comment_type citext,
    ro_comment citext);
create unique index on dds.dim_ro_comment(store_code, ro);
insert into dds.dim_ro_comment(ro_comment_key,store_code,ro,comment_type,ro_comment)
select rocommentkey,storecode,ro,commenttype,comment
from ads.ext_dim_ro_comment;

create sequence dds.ro_comment_ro_comment_key_seq
  owned by dds.dim_ro_comment.ro_comment_key; 
alter table dds.dim_ro_comment
alter column ro_comment_key set default nextval('dds.ro_comment_ro_comment_key_seq');

select setval('dds.ro_comment_ro_comment_key_seq', (select max(ro_comment_key) from dds.dim_ro_comment));

select count(*) from dds.dim_ro_comment; -- 346628
select max(ro_comment_key) from dds.dim_ro_comment;  -- 346669
select count(*) from ads.ext_dim_ro_comment;  -- 347695
select max(rocommentkey) from ads.ext_dim_ro_comment; -- 347736

from advantage: SELECT COUNT(*) FROM dimrocomment:  348039
from advantage: SELECT max(rocommentkey) FROM dimrocomment:348041

the pattern for this dimension, just like dim_ccc
not currently updating ads.ext_dim_ro_comment

delete 
-- select *
from ops.ads_extract where task = 'ext_dim_ro_comment' and the_date = current_date

select *
from dds.dim_ro_comment
limit 100

select ro_comment, count(*)
from dds.dim_ro_comment
group by ro_comment

select comment_type, count(*)
from dds.dim_ro_comment
group by comment_type

select count(*) from dds.dim_ro_comment; -- 347695

CREATE OR REPLACE FUNCTION dds.dim_ro_comment_update()
  RETURNS void AS
$BODY$
/*
*/
insert into dds.dim_ro_comment(store_code,ro,comment_type,ro_comment)
select aa.sxco_, aa.sxro_, 'Please Note', replace(string_agg(trim(sxtext), ' ' order by sxseq_), '  ', ' ') as ro_comment
from arkona.ext_sdprtxt aa
where sxltyp = 'X'
  and not exists (
    select 1
    from arkona.ext_pdpphdr a
    join arkona.ext_pdppdet b on a.pending_key = b.pending_key
      and a.company_number = aa.sxco_
      and a.document_number = aa.sxro_
      and b.line_number = aa.sxline
      and b.line_type = 'X')  
group by sxco_, sxro_
union
SELECT a.company_number, a.document_number, 'Please Note', replace(string_agg(trim(b.comments), ' ' order by sequence_number), '  ', ' ')
FROM arkona.ext_pdpphdr a
INNER JOIN arkona.ext_PDPPDET b ON a.pending_key = b.pending_key
WHERE a.document_type = 'RO'
  AND a.document_number <> ''
  AND b.line_type = 'X'
  AND b.comments <> ''
group by a.company_number, a.document_number
on conflict (store_code,ro) 
  do update
    set (store_code,ro,comment_type,ro_comment)
      =
      (excluded.store_code,excluded.ro,excluded.comment_type,excluded.ro_comment);
$BODY$
LANGUAGE sql;


-----------------------------------------------------------------------------
--/> dim_ro_comment
-----------------------------------------------------------------------------
