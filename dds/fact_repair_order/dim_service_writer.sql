﻿-----------------------------------------------------------------------------
--< dim_service_writer
-----------------------------------------------------------------------------
-- of course start with this one, many of the same issues that steered me away from dimEmployee
-- oh well
-- want to do some refactoring
-- eg dtUserNAme -> dealertrack_user_name but that should break some code somewhere
-- do we really need 2 fields for names: name and description
-- change name to writer_name and that is guaranteed to break something
-- DUH changing field names from camelCase to underscore will break every fucking thing
-- 
-- select * from ads.ext_dim_service_writer

-- 06/10/20 need to include description attribute

drop table if exists dds.dim_service_writer cascade;
CREATE TABLE dds.dim_service_writer ( 
  service_writer_key integer primary key ,
  store_code citext not null,
  employee_number citext,
  writer_name citext,
  description citext,
  writer_number citext not null,
  dealertrack_user_name citext,
  active boolean,
  default_service_type citext,
  census_department citext not null,
  current_row boolean,
  row_change_date Date,
  row_change_reason citext,
  row_from_date Date,
  row_thru_date Date default '12/31/9999' check (row_thru_date >= row_from_date));

-- back fill with data from ads 
insert into dds.dim_service_writer(service_writer_key,store_code,employee_number,writer_name,
  description,writer_number,dealertrack_user_name,active,default_service_type,census_department,
  current_row,row_change_date, row_change_reason,row_from_date,row_thru_date)
select servicewriterkey,storecode,employeenumber,name,description,writernumber,dtusername,
  active,defaultservicetype,censusdept,currentrow,rowchangedate,rowchangereason,
  servicewriterkeyfromdate,servicewriterkeythrudate
from ads.ext_dim_service_writer;

select max(service_writer_key) + 1 from dds.dim_service_writer;  -- 990

create sequence dds.dim_service_writer_service_wrtier_key_seq
  start 998
  owned by dds.dim_service_writer.service_writer_key; 
alter table dds.dim_service_writer
alter column service_writer_key set default nextval('dds.dim_service_writer_service_wrtier_key_seq');

select * from dds.dim_service_writer order by writer_number, service_writer_key

drop table if exists dds.xfm_dim_service_writer cascade;
create table dds.xfm_dim_service_writer (
  store_code citext not null,
  employee_number citext,
  description citext,
  writer_number citext not null,
  active boolean,
  default_service_type citext); 

alter function dds.xfm_dim_service_writer()
rename to dim_service_writer_xfm;


create or replace function dds.xfm_dim_service_writer()
returns void as
$BODY$
/*
06/10/20
  initially all this function is doing is populating dds.xfm_dim_service_writer
  would rather have code call a function than execute a query
*/
  truncate dds.xfm_dim_service_writer;
  insert into dds.xfm_dim_service_writer
  SELECT company_number, employee_, name, id, 
    case 
      when active = 'Y' then true
      when active = 'N' then false
      else false
    end,
    case
      when trim(misc_fields) like '%QL%' then 'QL'
      when trim(misc_fields) like '%AM%' then 'AM'
      when trim(misc_fields) like '%BS%' then 'BS'
      when trim(misc_fields) like '%MR%' then 'MR'
      when trim(misc_fields) like '%RE%' then 'RE'
      else 'XX'
    end
  FROM arkona.ext_sdpswtr
  WHERE ((
    company_number = 'ry1' AND id NOT IN ('110','124','34','36','38','404','422',
      '45','46','800','801','803','809','813','933','N07','41','722', '671',
      '319', '458','NS','576','316','742','007'))
    OR (
       company_number = 'ry2' AND id NOT IN ('12','27','685','688','720','75','803',
      'FO','KLB','LA','LIN','NEW','SEY', '222', '672', '671','435','436','458','678')))
    AND company_number in ('RY1','RY2')
    AND name not like '%DFX%' -- exclude dealerfx
    AND name NOT LIKE 'Saturday%'; -- exclude dealerfx
$BODY$
LANGUAGE sql;


-- no type 1 changes i am not going to fuck around with a field for the arkona name 
-- in ads that was an attribute named description
-- so all that is left is type 2 changes and they all need to be done manually,
-- so it is just another email

create or replace function dds.dim_service_writer_email_update()
returns integer as
$BODY$
/*
07/03/20
  added attribute to each subquery to quickly and easily see the source of change
*/
select count(*)::integer 
from (
  select a.*, 'new writer'  -- new writer
  from dds.xfm_dim_service_writer a
  left join dds.dim_service_writer b  on a.store_code = b.store_code
    and a.writer_number = b.writer_number
  where a.active  
    and b.store_code is null
  union
  select a.*, 'active_change'  -- active change
  from dds.xfm_dim_service_writer a
  join dds.dim_service_writer b  on a.store_code = b.store_code
    and a.writer_number = b.writer_number
    and b.active
    and b.current_row
    and b.census_department <> 'XX'
  where not a.active
  union
  select a.*, 'default service type' -- default service type change
  from dds.xfm_dim_service_writer a
  join dds.dim_service_writer b  on a.store_code = b.store_code
    and a.writer_number = b.writer_number
    and b.active
    and b.current_row
    and b.census_department <> 'XX'
    and b.writer_number NOT IN ('507','hs1','hs2','hs3','134','743','503')
  where a.default_service_type <> b.default_service_type) x;
$BODY$
LANGUAGE sql;

-----------------------------------------------------------------------------
--/> dim_service_writer
-----------------------------------------------------------------------------