﻿-----------------------------------------------------------------------------
--< dim_opcode : this table is the source for both opcode_key and cor_code_key
-----------------------------------------------------------------------------
-- select * from ads.ext_dim_opcode limit 10
drop table if exists dds.dim_opcode cascade;
create table dds.dim_opcode (
  opcode_key integer primary key,
  store_code citext,
  opcode citext not null,
  description citext not null,
  pdqcat1 citext,
  pdqcat2 citext,
  pdqcat3 citext);
create unique index on dds.dim_opcode(store_code,opcode);
-- SELECT string_agg(column_name, ',') FROM information_schema.columns WHERE table_schema = 'dds'  AND table_name = 'dim_opcode';  
-- SELECT string_agg(column_name, ',') FROM information_schema.columns WHERE table_schema = 'ads'  AND table_name = 'ext_dim_opcode';  

insert into dds.dim_opcode(opcode_key,store_code,opcode,description,pdqcat1,pdqcat2,pdqcat3)
select opcodekey,storecode,opcode,description,pdqcat1,pdqcat2,pdqcat3
from ads.ext_dim_opcode;

create sequence dds.dim_opcode_opcode_key_seq
  owned by dds.dim_opcode.opcode_key; 
alter table dds.dim_opcode
alter column opcode_key set default nextval('dds.dim_opcode_opcode_key_seq');

select setval('dds.dim_opcode_opcode_key_seq', (select max(opcode_key) from dds.dim_opcode));

-----------------------------------
nightly function
  1. upsert from sdplopc
  2. opcodes that exists in sdprdet/pdppdet that dont exists in sdplopc, invalid opcodes

select count(*) from dds.dim_opcode  30076
select count(*) from ads.ext_dim_opcode;

CREATE OR REPLACE FUNCTION dds.dim_opcode_update()
  RETURNS void AS
$BODY$
/*
select dds.dim_opcode_update();
*/
insert into dds.dim_opcode (store_code,opcode,description)
select company_number, operaction_code, concat_ws(' ',description_1,description_2,description_3,description_4)
from arkona.ext_sdplopc
where operaction_code not in ('d1999','e9425','j7507','j7921','v9665','3105h4','r06161') -- case sensitive dups
on conflict(store_code,opcode)
do update
  set (store_code, opcode, description)
  =
  (excluded.store_code, excluded.opcode, excluded.description);
  
-- bogus opcode
INSERT INTO dds.dim_opcode(store_code, opcode, description)
select company_number, correction_code, 'INVALID OPCODE' 
from (
  SELECT b.company_number, b.correction_code 
  FROM arkona.ext_sdprdet b
  WHERE b.line_type = 'L'
    AND b.transaction_code = 'CR' 
    and b.correction_code is not null
  union
  SELECT b.company_number, b.labor_operation_code 
  FROM arkona.ext_sdprdet b
  WHERE b.line_type = 'L'
    AND b.transaction_code = 'CR'
    and b.labor_operation_code is not null
  union
  SELECT b.company_number, b.correction_code 
  FROM arkona.ext_pdpphdr a
  INNER JOIN arkona.ext_PDPPDET b ON a.pending_key = b.pending_key
  WHERE a.document_type = 'RO'
    AND a.document_number <> ''
    AND b.line_type = 'L'
    AND b.transaction_code = 'CR' 
    and b.correction_code is not null
  union
  SELECT b.company_number, b.labor_operation_code 
  FROM arkona.ext_pdpphdr a
  INNER JOIN arkona.ext_PDPPDET b ON a.pending_key = b.pending_key
  WHERE a.document_type = 'RO'
    AND a.document_number <> ''
    AND b.line_type = 'L'
    AND b.transaction_code = 'CR'
    and b.labor_operation_code is not null) aa    
where not exists (
  select 1
  from dds.dim_opcode
  where store_code = aa.company_number
    and opcode = aa.correction_code);
$BODY$
  LANGUAGE sql;    


-----------------------------------------------------------------------------
--/> dim_opcode
-----------------------------------------------------------------------------