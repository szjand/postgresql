﻿1. new tech
2. default service typ change
3. rehire
4. term

-----------------------------------------------------------------------------
--< 1. new tech
-----------------------------------------------------------------------------
select a.*
from dds.xfm_dim_tech a
left join dds.dim_tech b on a.store_code = b.store_code
  and a.tech_number = b.tech_number
where b.store_code is null
  and a.active = true

-- 06/08/22 for some reason added hobby shop and nokelby honda nissan tech numbers
select * from dds.dim_tech order by tech_number desc

insert into dds.dim_tech (store_code,employee_number,tech_name,tech_number,
  active,labor_cost,flag_department_code,flag_department,current_row,
  row_from_date)
select a.store_code, 
-- 	'N/A',
  c.employee_number,
-- 	'PDQ Tech 2',
  c.last_name || ', ' || c.first_name as tech_name, 
  a.tech_number,
  a.active, a.labor_cost, 
--   'RE', 'DETAIL',  -- for when IT fucks up the default service type
  a.flag_department_code,
  case a.flag_department_code
    when 'MR' then 'Service'
    when 'BS' then 'Body Shop'
    when 'RE' then 'Detail'
    when 'HS' then 'Hobby Shop'
    when 'QL' then 'Quick Lane'
    when 'QS' then 'Quick Service'
    when 'AM' then 'Service'
  end as department_code, 
  true as active, '01/22/2024' as start_date  ----------------------  start date
from dds.xfm_dim_tech a
left join dds.dim_tech b on a.store_code = b.store_code
  and a.tech_number = b.tech_number
left join ukg.employees c on c.employee_number = '145786'  ------------- emp#
where b.store_code is null
  and a.active = true 
  and a.tech_number = '682'  ------------------  when there are more than one

select * from dds.dim_tech where tech_number in ('R43')

louis rodriguez, tech 676, set up in RY1 & RY2, per Tanner (IT):
The default location for GM I messed up. I have changed that to be MR. Louis is training at Midas 
and will need a tech number for GM until he is done training and then he will move to the Honda Nissan Store. 
I made both right away that way it would be taken care of before he went to HN.

to which i replied:
I understand
But on the other hand, do you understand that the data now does not accurately reflect the Honda Nissan tech population?





select * from dds.dim_tech where tech_number = '201'  
update dds.dim_tech set tech_number = '201' where tech_number = '444'

select * from ukg.employees where employee_number = '194675'
select * from arkona.xfm_pymast where employee_last_name = 'hack'
select * from dds.xfm_dim_tech where tech_number = 'r10'

the problem is fucking IT does not have/enter the employeenumber when they create the tech
select * from dds.dim_tech where tech_number = '519'

select * from dds.xfm_dim_tech where tech_name like '%batzer%'
select * from dds.dim_tech where tech_number = '659'

select * 
from dds.dim_tech where store_code = 'RY8'

-----------------------------------------------------------------------------
--/> 1. new tech
-----------------------------------------------------------------------------


-----------------------------------------------------------------------------
--< 2. default service type change
-----------------------------------------------------------------------------
select a.*
from dds.xfm_dim_tech a
join dds.dim_tech b on a.store_code = b.store_code
  and a.tech_number = b.tech_number
  and b.active
  and b.current_row
  and a.flag_department_code <> b.flag_department_code
where a.active = true

select * from dds.xfm_dim_tech where tech_number = '651' 
select * from dds.dim_tech where tech_number = '651' order by tech_key

select * from arkona.xfm_pypclockin where employee_number = '1117960' order by the_date desc limit 100

-- close the current_row
update dds.dim_tech
set row_thru_date = current_date - 1,
    current_row = false,
    row_change_date = current_date,
    row_change_reason = 'flagdept'
where tech_key = 1317;
-- insert the new row
insert into dds.dim_tech(store_code,employee_number, tech_name, tech_number,active, labor_cost,
  flag_department_code, flag_department, current_row, row_from_date)
select a.store_code, a.employee_number, b.employee_name, a.tech_number, true, a.labor_cost, a.flag_department_code,
  case a.flag_department_code
    when 'MR' then 'Service'
    when 'BS' then 'Body Shop'
    when 'RE' then 'Detail'
    when 'HS' then 'Hobby Shop'
    when 'QL' then 'Quick Lane'
    when 'QS' then 'Quick Service'
    when 'AM' then 'Service'
  end, true, current_date 
from dds.xfm_dim_tech a ------- !!! heads up dude no more pymast !!!!!!! -----------------
left join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
where a.tech_number = '651'
  and store_code = 'RY2';


-----------------------------------------------------------------------------
--/> 2. default service type change
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
--< 3. rehire
-----------------------------------------------------------------------------
carter ferry, termed on 6/10/20, rehired on 1/9/23
select * from dds.dim_tech where tech_name like 'ferry%'

update dds.dim_tech
set current_row = false,
    row_change_date = '01/10/2023',
    row_change_reason = 'rehire',
    row_thru_date = '01/08/2023'
where tech_key = 999;

insert into dds.dim_tech (store_code,employee_number,tech_name,tech_number,active,labor_cost,
  flag_department_code,flag_department,current_row,row_from_date)
values('RY1','159863','Ferry, Carter','672',true,27.60,'MR','Service',true,'01/09/2023');  
-----------------------------------------------------------------------------
--/> 3. rehire
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
--< 4. term
-----------------------------------------------------------------------------
Doug Bohm retired 6/2/23
select * from dds.dim_tech where tech_name like '%bohm%' order by row_from_Date

update dds.dim_tech
set current_row = false,
    row_change_date = '07/05/2023',
    row_change_reason = 'term',
    row_thru_date = '06/02/2023'
where tech_key = 1198;

insert into dds.dim_tech(store_code,employee_number,tech_name,tech_number,active,
  flag_department_code,flag_department,current_row,row_from_date)
values('RY1','116185','Bohm, Douglas','557',false,'MR','Service',true,'06/03/2023');

-----------------------------------------------------------------------------
--/> 4. term
-----------------------------------------------------------------------------