﻿1. new writer - employee
2. active to inactive
3. default service type change
4. ok, i surrender, put the dealerfx writers in dds.dim_service_writer

select dds.dim_service_writer_email()  

select * from dds.dim_service_writer where store_code = 'RY8'

select * from dds.xfm_dim_service_writer where writer_number = '843'

select * from ukg.employees where cost_center = 'customer care service specialist' and status = 'active' order by last_name
--05/16/23
taner smith, acquisition specialist, writer# 860, exclude
-- the email query
select count(*)::integer 
from (
  select a.*, 'new writer'  -- new writer
  from dds.xfm_dim_service_writer a
  left join dds.dim_service_writer b  on a.store_code = b.store_code
    and a.writer_number = b.writer_number
  where a.active  
    and b.store_code is null
    and a.writer_number not in ('P00','B00') -- 2 new series of writer #s started by IT, default serv type is EM, positions: parts counter, bdc
    and a.writer_number not in ('746','516')   -- excluce service concierge
    and a.writer_number not in ('267','268','269','860') -- exclude office, acquisition specialist
    and
      case
        when a.store_code = 'RY8' then a.writer_number not in ('301','302','404','405','709','901','902','903','904','112','861')
        else true
      end
  union
  select a.*, 'active_change'  -- active change
  from dds.xfm_dim_service_writer a
  join dds.dim_service_writer b  on a.store_code = b.store_code
    and a.writer_number = b.writer_number
    and b.active
    and b.current_row
    and b.census_department <> 'XX'
  where not a.active
  union
  select a.*, 'default service type' -- default service type change
  from dds.xfm_dim_service_writer a
  join dds.dim_service_writer b  on a.store_code = b.store_code
    and a.writer_number = b.writer_number
    and b.active
    and b.current_row
    and b.census_department <> 'XX'
    and b.writer_number NOT IN ('507','hs1','hs2','hs3','hs4','hs5','134','743','503','843','850','852','861')  
  where a.default_service_type <> b.default_service_type) x;

update dds.dim_service_writer
set default_service_type = 'MR'
where writer_number in ('819')
  and store_code = 'ry8'

select * from dds.dim_service_writer where writer_number = '868'
select * from dds.dim_service_writer where description like '%wippler'
select * from dds.xfm_dim_service_writer where writer_number in  ('hs4','hs5')
------------------------------------------------------------------------------
--< 1. new writer
------------------------------------------------------------------------------

select * from dds.dim_service_writer where writer_number in ('819')
select * from dds.fact_repair_order where service_writer_key = 1120

delete from dds.dim_service_writer where writer_number = '815'
-- 09/24/20  Austin Knutson
-- 11/04/20 Sierra Ferdon

-- looks like i have been going with MR for customer care service specialist, which is how IT sets them up
select b.* from ukg.employees a
left join dds.dim_service_writer b on a.employee_number = b.employee_number and b.current_row
where term_date > current_date
  and cost_center = 'customer care service specialist'

-- from dds.dim_service_writer_email()
  select a.*, 'new writer'  -- new writer
  from dds.xfm_dim_service_writer a
  left join dds.dim_service_writer b  on a.store_code = b.store_code
    and a.writer_number = b.writer_number
  where a.active  
    and b.store_code is null
    and a.writer_number not in ('P00','B00') -- 2 new series of writer #s started by IT, default serv type is EM, positions: parts counter, bdc
    and a.writer_number not in ('746','516')  -- excluce service concierge
    and a.writer_number not in ('267','268','269','853','860') -- exclude office, april richie
    and
      case
        when a.store_code = 'RY8' then a.writer_number not in ('301','302','405','709','901','902','903','904','112','853')
        else true
      end    

select * from dds.dim_service_writer where writer_name like '%amber%'

-- -- had to add store to not exists clause
-- -- select employee_name, pymast_employee_number from arkona.ext_pymast where employee_last_name = 'sattler'
-- connor: 479  152499
-- mayhayla: 540 (ry1 & 2)  132987
-- lucas: 614   178050

-- 02/02/23 3 hobby shop techs get writer numbers
select * from dds.dim_service_writer where writer_number = 'D00'
-- 05/24/23
-- max jones, car wash mgr, add car wash

insert into dds.dim_service_writer(store_code,employee_number,writer_name,
  description,writer_number,active,default_service_type,census_department,
  current_row,row_from_date)
select a.store_code, b.employee_number, b.employee_name, 
  a.description, a.writer_number, true, 
  default_service_type,default_service_type,
--   'RE','RE',
--   'CW','CW',
--   'MR','MR',
-- 		'QS','QS',
--    'BS','BS',  -- for when IT fucks up default service type
  true, '02/05/2024' -------------------------------------------------------------------------- start date
-- select *  
from dds.xfm_dim_service_writer a
join (
  select employee_number, last_name || ', ' || first_name as employee_name
  from ukg.employees
  where employee_number = '145659') b on true -------------------------------------------------- emp name
where not exists (
  select 1 
  from  dds.dim_service_writer 
  where writer_number = a.writer_number
    and store_code = a.store_code)
and a.active  
and a.writer_number = 'D01'

select *  
from dds.xfm_dim_service_writer where description like '%amber%'

select * from ukg.employees where employee_number = '145771'

-- dept tech
insert into dds.dim_service_writer(store_code,employee_number,writer_name,
  description,writer_number,active,default_service_type,census_department,
  current_row,row_from_date)
select 'RY8', 'n/a', 'Express 2', 
  'Express 2', a.writer_number, true, 
--   default_service_type,default_service_type,
--   'MR','MR',
-- 		'QS','QS',
		'QL','QL',
--    'BS','BS',  -- for when IT fucks up default service type
  true, '04/17/2023' ----------------------------------- start date
-- select *  
from dds.xfm_dim_service_writer a
-- join (
--   select employee_number, last_name || ', ' || first_name as employee_name
--   from ukg.employees
--   where employee_number = 'n/a') b on true ------- emp name
where not exists (
  select 1 
  from  dds.dim_service_writer 
  where writer_number = a.writer_number
    and store_code = a.store_code)
and a.active 
and a.writer_number = '407'
------------------------------------------------------------------------------
--/> 1. new writer
------------------------------------------------------------------------------
select * from ukg.employees where employee_number = '146398'

------------------------------------------------------------------------------
--< 2. active to inactive
------------------------------------------------------------------------------

  select a.*, 'active_change',  -- active change
    dds.db2_integer_to_date(c.termination_date) as term_date, c.pymast_employee_number, d.term_date
  from dds.xfm_dim_service_writer a
  join dds.dim_service_writer b  on a.store_code = b.store_code
    and a.writer_number = b.writer_number
    and b.active
    and b.current_row
    and b.census_department <> 'XX'
  left join arkona.ext_pymast c on b.employee_number = c.pymast_employee_number
  left join ukg.employees d on b.employee_number = d.employee_number
  where not a.active


select * from dds.dim_Service_writer where writer_number in ('403')    

select * from dds.dim_Service_writer where writer_name like 'dv%'

select * from arkona.ext_sdpswtr where id in ('489','503')   

select * from arkona.ext_pymast where employee_name like '%shadw%'
update dds.dim_service_writer set employee_number = '168243' where employee_number = '168423'

-- 9/2/21 fucked up jankowski, termed him (he was already termed) instead of piche 499
-- update the old record
do $$
declare
  _reason citext := 'active to inactive';
  _thru_date date := '01/08/2024';
  _store citext := 'RY2';
  _writer_number citext := '489';
  _from_date date := _thru_date + 1;
begin
  update dds.dim_service_writer
  set current_row = false,
      row_change_reason = _reason,
      row_thru_date = _thru_date
  where writer_number = _writer_number
    and store_code = _store;
-- insert new record
  insert into dds.dim_service_writer (store_code,employee_number,writer_name,description,
    writer_number,dealertrack_user_name,active,default_service_type,census_department,current_row,
    row_from_date)
  select store_code, employee_number, writer_name, description, writer_number,null,
    false, default_service_type, census_department, true, _from_date
  from dds.dim_service_writer
  where writer_number = _writer_number
    and store_Code = _store;
end $$;


select * from dds.dim_service_writer where writer_number = '489'

select * from dds.dim_service_writer where writer_number = '114'  975

select count(distinct ro) from dds.fact_repair_order where service_writer_key = 1123

update dds.dim_service_writer
set employee_number = '139856'
where service_writer_key = 975
------------------------------------------------------------------------------
--/> 2. active to inactive
------------------------------------------------------------------------------

------------------------------------------------------------------------------
--< 3. default service type change
------------------------------------------------------------------------------

 
select *
from dds.dim_service_writer
where writer_number = '647'

select *
from dds.dim_service_writer
where service_writer_key = 1036

-- careful with this one
update dds.dim_service_writer
set current_row = false,
    row_change_date = '06/11/2023',
    row_change_reason = 'default_service_type',
    row_thru_date = '12/31/9999'
where writer_number = '828'
  and store_code = 'RY1'
  and row_from_date = '2022-08-22';

insert into dds.dim_service_writer (store_code, employee_number, writer_name, description, 
  writer_number, dealertrack_user_name, active,default_service_type, census_department,
  current_row,row_from_date)
select store_code, employee_number, writer_name, description, writer_number, dealertrack_user_name, active,
  'MR', 'MR',true,current_date
from dds.dim_service_writer
where service_writer_key = 1164;

-- this is just a set up correction
-- drew brooks
update dds.dim_service_writer 
set default_service_type = 'MR', census_department = 'MR'
where writer_number = '484';

select * from dds.dim_service_writer where default_service_type = 'xx'

update dds.dim_service_writer
set default_service_type = 'XX', census_department = 'XX'
-- select * from dds.dim_service_writer
where writer_number = '906'
  and store_code <> 'RY8'

update dds.dim_service_writer 
set default_service_type = 'QL', census_department = 'QL'
where writer_number = '858'; 

select * from dds.dim_service_writer where writer_number = '828'
------------------------------------------------------------------------------
--/> 3. default service type change
------------------------------------------------------------------------------  

------------------------------------------------------------------------------
--< 4. ok, i surrender, put the dealerfx writers in dds.dim_service_writer
------------------------------------------------------------------------------

-- when i asked andrew if dealerfx writer numers should still be in use (7/9/20)
-- he replied:
-- Yes actually. The writers are supposed to switch them into their numbers as they check them in but 
-- there are a couple they are obviously missing. If they don’t put it into their number, they don’t 
-- get commission for it so I will remind them. TY

insert into dds.dim_service_writer(store_code,employee_number,writer_name,
  description,writer_number,active,default_service_type,census_department,
  current_row,row_from_date)
select company_number, 'n/a', name, 'dealerfx', id, 
  case
    when active = 'Y' then true
    when active = 'N' then false
  end as active, left(misc_fields, 2),
  left(misc_fields, 2), true, '01/01/2020'
-- select *
FROM arkona.ext_sdpswtr 
WHERE ((
  company_number = 'ry1' AND id NOT IN ('110','124','34','36','38','404','422',
    '45','46','800','801','803','809','813','933','N07','41','722', '671',
    '319', '458','NS','576','316','742','007'))
  OR (
     company_number = 'ry2' AND id NOT IN ('12','27','685','688','720','75','803',
    'FO','KLB','LA','LIN','NEW','SEY', '222', '672', '671','435','436','458','678')))
  AND company_number in ('RY1','RY2')
    AND (name like '%DFX%' or name LIKE 'Saturday%');


-- now have to fix all the ros from jeri.update_open_ros()
-- ok, clearly, some, as andrew suggested, have been updated


drop table if exists ros_1;
create temp table ros_1 as
  select a.ro, b.writer_name
  from dds.fact_repair_order a
  left join dds.dim_service_Writer b on a.service_writer_key = b.service_writer_key
     AND (writer_name = 'UNKNOWN' or writer_name is null)
  where a.ro in ('18066587','16363665','16367127','16367338','16367314','16379174',
    '16378333','2815896','2815906','2815882','16384004','16381203','2816690''16381026',
    '16381203','16379278','16381879','16382014',
    '16382032','16383750','16384195','16385295','16385493','16385423','16385594',
    '16385730','16385944','16386125','16386129','16386263','16386268','16386270',
    '16386780','16387090','16387337','16387422','16387722','19363546','16389474',
    '16392634','2818934','16393044','16393261','16393516','16393852','16394189',
    '16394577','16394968','16395734','16396002','16397520','16397553','16398385',
    '16398567','16399554','16399596','16401512','16401997','16402227','16402892',
    '16405275','16406133','16406550','16406580','16405964','16407011','16407282',
    '16403226','16407992','16408082','16408139','16409164','16409497','16411500',
    '16412008','16413253','16413266','16413489','16415161','16417639','16419373',
    '16419970','2827518','16422167','16422621','16424021','16424632','16423612',
    '16425412','16425443','16424879','16425981','16425993','16426440','16426592',
    '16426728','16426033','16427357')
  group by ro, b.writer_name

select a.ro, b.company_number, b.service_writer_id, c.service_writer_key, c.writer_name
from ros_1 a
left join arkona.ext_sdprhdr_history b on a.ro = b.ro_number
left join dds.dim_service_writer c on b.company_number = c.store_code
  and b.service_writer_id = c.writer_number
order by service_writer_key desc nulls last


update dds.fact_repair_order set service_Writer_key = 1036 where ro = '16394577';
update dds.fact_repair_order set service_Writer_key = 1035 where ro = '16393044';
update dds.fact_repair_order set service_Writer_key = 1016 where ro = '16385423';
update dds.fact_repair_order set service_Writer_key = 1012 where ro = '16382032';
update dds.fact_repair_order set service_Writer_key = 1006 where ro in ('2815906','2815896','2815882');
  
-- hell lets go for any unknown writer
select * FROM arkona.ext_sdpswtr where id = '41'

-- run this in db2 to get the writer numbers
select '(' ||string_agg(''''||ro||'''',',')||')'
-- select count(distinct ro)
from dds.fact_Repair_order
where open_date > '09/01/2019'
  and service_writer_key in (726,727)  

drop table if exists tem.ros_2;
create table tem.ros_2 (
  ro citext not null,
  writer_number citext not null);
create index on tem.ros_2(ro);
create index on tem.ros_2(writer_number);  

select count(*) from tem.ros_2;

update tem.ros_2
set writer_number = '08'
where writer_number = '8';

update tem.ros_2
set writer_number = '09'
where writer_number = '9';

update tem.ros_2
set writer_number = '01'
where writer_number = '1';

update tem.ros_2
set writer_number = '04'
where writer_number = '4';

update tem.ros_2
set writer_number = '05'
where writer_number = '5';


update dds.fact_repair_order x
set service_writer_key = y.service_writer_key
from (
  select a.ro, b.service_writer_key
  from tem.ros_2 a
  join dds.dim_service_writer b on a.writer_number = b.writer_number
    and b.store_code = 'ry1'
    and b.active) y
where x.ro = y.ro  
------------------------------------------------------------------------------
--/> 4. ok, i surrender, put the dealerfx writers in dds.dim_service_writer
------------------------------------------------------------------------------      
sundbakken  customer care service specialist

select * 
from dds.dim_service_writer
where active
  and current_row  
  and store_code = 'ry1'
order by default_service_type, writer_name



select store, last_name, first_name, cost_center
from ukg.employees
where status = 'active'
  and store = 'GM'
order by cost_center




