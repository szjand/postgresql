﻿ros only get updated for the last 45 days
often times they dont get closed until after that
so i need a way of just updateing ro dates going back more than 45 days

-- maybe start with a list of open ros
select count(distinct ro)  -- 752 open ros
from dds.fact_repair_order
where close_Date = '12/31/9999'
  or final_close_date = '12/31/9999'

do a db2 temp table ...?  



select count(distinct ro)  -- 643 open ros
from dds.fact_repair_order
where (close_Date = '12/31/9999'
  or final_close_date = '12/31/9999')
  and open_date > current_date - 40

  select * 
  from dds.fact_Repair_order 
  where ro = '16411154'


select base_salary, row_from_date 
from arkona.xfm_pymast
where employee_first_name = 'afton'
order by pymast_key  

drop table if exists arkona.ext_sdprhdr_close_dates_ext;
create table arkona.ext_sdprhdr_close_dates_ext (
  ro citext primary key,
  open_date integer not null,
  close_date integer not null,
  final_close_date integer not null);

drop table if exists arkona.ext_sdprhdr_close_dates;
create table arkona.ext_sdprhdr_close_dates (
  ro citext primary key,
  open_date date not null,
  close_date date not null,
  final_close_date date not null);

insert into arkona.ext_sdprhdr_close_dates
select ro, 
  dds.db2_integer_to_date(open_date),
  dds.db2_integer_to_date(close_date),
  dds.db2_integer_to_date(final_close_date)
from arkona.ext_sdprhdr_close_dates_ext;


create or replace function arkona.xfm_sdprhdr_close_dates()
returns void as
$BODY$
/*
  called from luigi.fact_repair_order.py
  updates ro close_dates and final_close dates 
*/
truncate arkona.ext_sdprhdr_close_dates;

insert into arkona.ext_sdprhdr_close_dates
select ro, 
  dds.db2_integer_to_date(open_date),
  dds.db2_integer_to_date(close_date),
  dds.db2_integer_to_date(final_close_date)
from arkona.ext_sdprhdr_close_dates_ext;

update dds.fact_repair_order x
set close_date = y.close_date,
    final_close_date = y.final_close_date
from (    
  select bb.ro, bb.close_date, bb.final_close_date
  from (
    select a.ro, a.open_date, a.close_date, a.final_close_date
    from dds.fact_repair_order a
    where (a.close_date = '12/31/9999' or a.final_close_date = '12/31/9999')
      and a.open_date > current_Date - 365
    group by a.ro, a.open_date, a.close_date, a.final_close_date) aa  
  join arkona.ext_sdprhdr_close_dates bb on aa.ro = bb.ro
    and (bb.close_date <> '12/31/9999' or bb.final_close_date <> '12/31/9999')) y
where x.ro = y.ro;  
$BODY$  
language sql;
comment on function arkona.xfm_sdprhdr_close_dates() is 'updates ro close_dates and final_close dates that get missed
  in regular processing due to the duration since open date';
  




  