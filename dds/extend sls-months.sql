﻿select * from sls.months where year_month = 202311
update sls.months
set first_day_of_next_month = '12/01/2023'
where year_month = 202311

select * from sls.months where year_month = 202312

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'sls'
   and table_name = 'months'

delete from sls.months where year_month > 202311

-- reset the sequence for sls.months
select setval('sls.months_seq_seq', 827, true)

update sls.months
set open_closed = 'open'
where year_month = 202312

select pto.sales_pto_rate_recalc()

insert into sls.months (year_month,open_closed,first_of_month,last_of_month,month_yyyy,mmm_yyyy,yyyy,previous_year_month,
  sc_working_days,next_year_month,sc_first_working_day_of_month,last_day_of_previous_month,first_day_of_next_month)
select distinct a.year_month, 'closed' as open_closed, b.the_date as first_of_month, 
  c.the_date as last_of_month, 
  a.month_name || ' ' || a.the_year::text as month_yyyy,
  left(a.month_name, 3) || ' ' || a.the_year::text as mmm_yyyy,
  b.the_year, a.last_year_month as previous_year_month, 
  d.sc_working_days,
  e.next_year_month,
  f.sc_first_working_day_of_month,
  (select distinct last_of_month from dds.dim_date where year_month = a.last_year_month) as last_day_of_previous_month,
  (select distinct first_of_month from dds.dim_date where year_month = e.next_year_month) as first_day_of_next_month
from dds.dim_date a
left join dds.dim_date b on a.year_month = b.year_month
  and b.first_day_of_month = true
left join dds.dim_date c on a.year_month = c.year_month
  and c.last_day_of_month = true  
left join (-- sc_working_days
	select year_month, count(*) as sc_working_days
	from dds.dim_date
	where day_of_week between 2 and 7
		and not holiday
		and year_month between 202312 and 205011
	group by year_month) d on a.year_month = d.year_month
left join (--next_year_month
	select a.year_month, b.year_month as next_year_month
	from (
		select distinct year_month, last_of_month, last_of_month + 1 as next_month_date
		from dds.dim_date 
		where year_month between 202312 and 205011) a    
	join (      
		select distinct year_month, first_of_month
		from dds.dim_date) b on a.next_month_date = b.first_of_month) e	on a.year_month = e.year_month
left join (-- first working day of month
	select year_month, min(a.the_date) as sc_first_working_day_of_month
	from dds.dim_date a
	where a.day_of_week between 2 and 7
		and holiday = false  
		and year_month between 202312 and 205011 
	group by year_month) f on a.year_month = f.year_month		
where a.year_month between 202312 and 205011
  and a.year_month is not null;

update sls.
-- need to fill out working days -- done
select * from dds.dim_date where the_date = current_date

select * from dds.working_days where department = 'parts' order by year_month desc, the_date desc, department limit 28

select * from dds.dim_date where the_date between current_date - 7 and current_Date
-- sc_working_days
select year_month, count(*)
from dds.dim_date
where day_of_week between 2 and 7
  and not holiday
  and year_month between 202312 and 205011
group by year_month  
order by year_month

--next_year_month
select a.year_month, b.year_month as next_year_month
from (
	select distinct year_month, last_of_month, last_of_month + 1 as next_month_date
	from dds.dim_date 
	where year_month between 202312 and 205011) a    
join (      
	select distinct year_month, first_of_month
	from dds.dim_date) b on a.next_month_date = b.first_of_month

-- first working day of month
select year_month, min(a.the_date) as sc_first_working_day_of_month
from dds.dim_date a
where a.day_of_week between 2 and 7
  and holiday = false  
  and year_month between 202312 and 205011 
group by year_month

