﻿/*
now that i am doing "real time" processing, i don't want to rely on an email to verify that 
each run has taken place.
i discovered that i can insert a line like MAILTO=bob prior to the real time cron job to disable the email,
so that takes care of the email blast

i am confident that if a task fails, luigi notification does the job
the concern is, what if the task chain simply has not run for the past 2 hours

so, i could run another cron job, say every 1/2 hour, if the last successful run was more than, say 10 minutes ago, 
send an email

that could work for all periodic jobs
what are the periodic jobs?
homenet new feed mon - sat @ 7 past the hour from 9 to 5
compli scraper every half hour 7:07 am thru 7:07 pm monday thru saturday
and now
  fact_Repair_order_today
  xfm_pypclockin_today


*/

!!!!!!! task is the class name !!!!!!!!!!!!!!!
select * 
from luigi.luigi_log
where the_Date between current_date - 1 and  current_date
order by the_date, pipeline, log_id, from_ts  

 
select a.*, thru_ts - from_ts from luigi.luigi_log a where pipeline = 'fact_repair_order_today' and the_date = current_date order by from_Ts

the timestamp portion of the log_id is the run id


select log_id, split_part(log_id, '__', 3) from luigi.luigi_log a where pipeline = 'fact_repair_order_today' and the_date = current_date order by from_Ts

select *
from (
select split_part(log_id, '__', 3), min(from_ts), max(thru_ts), max(from_ts) - min(thru_ts) as duration
from luigi.luigi_log a 
where pipeline = 'fact_repair_order_today' 
group by split_part(log_id, '__', 3)) x
where duration > interval '1' minute * 4


-- 10/28 turned off MAILTO in cron
create index on luigi.luigi_log(from_ts);
create index on luigi.luigi_log(the_date);
create index on luigi.luigi_log(thru_ts);
create index on luigi.luigi_log(pipeline);
create index on luigi.luigi_log(task);


select pipeline, split_part(log_id, '__', 1) as class, max(thru_ts), now() - max(thru_ts)
from luigi.luigi_log 
where the_date = current_date
  and status = 'pass'
  and pipeline = 'fact_repair_order_today'
group by pipeline, split_part(log_id, '__', 1)
order by pipeline, split_part(log_id, '__', 1)




select * 
from (-- a master list of pipelines (base table for left join)
  select pipeline
  from luigi.luigi_log
  group by pipeline) a
left join (
  select pipeline, max(thru_ts), now() - max(thru_ts)
  from luigi.luigi_log 
  where the_date = current_date
    and status = 'pass'
  group by pipeline) b on a.pipeline = b.pipeline
order by a.pipeline

-- looks like the 2_ hour runtime started with the introduction of 21 model year
select * 
from luigi.luigi_log
where task = 'DescribeVehicleByStyleID'
  and status = 'pass'
order by the_Date, from_Ts


select * 
from luigi.luigi_log
where the_Date between current_date - 1 and  current_date
order by the_date, from_ts  


ok, something weird is going on
starting on 09/06/20, all task called in run_all are logging twice 
possible coincidentally, that was when compli was first deployed, but that runs every 20 minutes and is not called by run_all
( but it calls ext_pymast, which in itself, has no logging)  this is just shit i notice as i try to document what is happening
so logging twice starting 9/6, stops on 10/21, resumes on 10/29
every fucking task starting and stopping within milliseconds of each other

select the_date, count(*) 
from luigi.luigi_log
where the_date > '09/01/2020'
  and from_Ts::time < '03:00:00'
group by the_date  
order by the_date

-- yep, it is here too
select * 
from luigi.luigi_log
where task = 'runall'
  and the_Date > '09/01/2020'
order by the_date, from_ts


try the local run of XfmPymast again tomorrow, try to step through it

no luck stepping 

select * 
from luigi.luigi_log
where task = 'XfmPyactgr'
  and the_Date = current_date
  and from_ts::time > '12:00:00'
order by the_date, from_ts




select the_date, count(*) 
from luigi.luigi_log
where the_date > '11/01/2020'
  and from_Ts::time < '03:00:00'
group by the_date  
order by the_date

select *
from (
select *
from luigi.luigi_log
where the_date = current_date
  and from_Ts::time < '03:00:00') a
full outer join (
select *
from luigi.luigi_log
where the_date = current_date - 1
  and from_Ts::time < '03:00:00') b on a.pipeline = b.pipeline and a.task = b.task
