﻿-- "E:\sql\advantage\DDS\dimDay\working_days-postgres.sql"
-- 
-- departments & working days: 
--   mon - fri: service, parts, body shop, detail
--   mon - sat: pdq, sales
--   all exc thanksgiving, ny, xmas: carwash
-- 
-- run this entire script, once for each department
-- after the initial run, do not recreate dds.working_days_2, comment out that block

-- ================================================================================================================================
-- ================================================================================================================================
do $$

declare _department citext := 'carwash';

begin

DROP TABLE if exists dds.working_days_new cascade;

CREATE TABLE dds.working_days_new
(
  department citext NOT NULL,
  the_year integer not null,
  year_month integer not null,
  biweekly_pay_period_sequence integer not null,
  the_date date NOT NULL,
  day_of_week integer not null,
  day_of_month integer not null,
  day_of_year integer not null,
  last_of_month date,
  wd_in_year integer,
  wd_of_year_elapsed integer,
  wd_of_year_remaining integer,
  wd_of_year integer,
  wd_in_month integer,
  wd_of_month_elapsed integer,
  wd_of_month_remaining integer,
  wd_of_month integer,
  wd_in_biweekly_pay_period integer,
  wd_of_biweekly_pay_period_elapsed integer,
  wd_of_bi_weekly_pay_period_remaining integer,
  wd_of_biweekly_pay_period integer,
  CONSTRAINT working_days_new_pkey PRIMARY KEY (department, the_date));
CREATE INDEX working_days_new_the_date_idx
  ON dds.working_days_new
  USING btree
  (the_date);

-- DROP TABLE if exists dds.working_days_2 cascade;
-- CREATE TABLE dds.working_days_2
-- (
--   department citext NOT NULL,
--   the_year integer not null,
--   year_month integer not null,
--   biweekly_pay_period_sequence integer not null,
--   the_date date NOT NULL,
--   day_of_week integer not null,
--   day_of_month integer not null,
--   day_of_year integer not null,
--   last_of_month date,
--   wd_in_year integer,
--   wd_of_year_elapsed integer,
--   wd_of_year_remaining integer,
--   wd_of_year integer,
--   wd_in_month integer,
--   wd_of_month_elapsed integer,
--   wd_of_month_remaining integer,
--   wd_of_month integer,
--   wd_in_biweekly_pay_period integer,
--   wd_of_biweekly_pay_period_elapsed integer,
--   wd_of_bi_weekly_pay_period_remaining integer,
--   wd_of_biweekly_pay_period integer,
--   CONSTRAINT working_days_2_pkey PRIMARY KEY (department, the_date));
-- CREATE INDEX working_days_2_the_date_idx
--   ON dds.working_days_2
--   USING btree
--   (the_date);

-- step 1
truncate dds.working_days_new;
-- base rows, 1 row/day for department
insert into dds.working_days_new(department, the_year, year_month, biweekly_pay_period_sequence, the_date,
  day_of_week, day_of_month, day_of_year,last_of_month)
select _department,  ----------------------------------------------------------------------------------------
  the_year, year_month, biweekly_pay_period_sequence, the_date, 
  day_of_week, day_of_month, day_of_year, last_of_month
from dds.dim_date
where the_year > 2022;
-- 
-- departments & working days: 
--   mon - fri: service, parts, body shop, detail
--   mon - sat: pdq, sales
--   all exc thanksgiving, ny, xmas: carwash

-- step 2, create the temp table once for each department
-- create the base wd table for the department
-- this table includes working days only
-- adjust the where clause to include only the work days relevant to the dpeartment
drop table if exists wd;
create temp table wd as
select _department, -----------------------------------------------------------------------------------
  x.the_date, x.the_year, x.year_month, x.biweekly_pay_period_sequence, 
  x.wd_in_year, 
  row_number() over (partition by the_year order by the_date) - 1 as wd_of_year_elapsed, 
  wd_in_year - row_number() over (partition by the_year order by the_date) + 1 as wd_of_year_remaining,
  row_number() over (partition by the_year order by the_date) as wd_of_year, 
  x.wd_in_month, 
  row_number() over (partition by year_month order by the_date) - 1 as wd_of_month_elapsed, 
  wd_in_month - row_number() over (partition by year_month order by the_date) + 1 as wd_of_month_remaining, 
  row_number() over (partition by year_month order by the_date) as wd_of_month,  
  x.wd_in_biweekly_pay_period,
  row_number() over (partition by biweekly_pay_period_sequence order by the_date) - 1 as wd_of_biweekly_pay_period_elapsed, 
  wd_in_biweekly_pay_period - row_number() over (partition by biweekly_pay_period_sequence order by the_date) + 1 as wd_of_bi_weekly_pay_period_remaining,   
  row_number() over (partition by biweekly_pay_period_sequence order by the_date) as wd_of_biweekly_pay_period
from (
  select the_year, year_month, the_date, biweekly_pay_period_sequence, day_name, first_of_month, last_of_month,
    count(*) over (partition by the_year) as wd_in_year,
    count(*) over (partition by year_month) as wd_in_month,
    count(*) over (partition by biweekly_pay_period_sequence) as wd_in_biweekly_pay_period
  from dds.dim_date 
  where the_year > 2022
--     and day_of_week <> 1 -- mon-sat
--     and day_of_week between 2 and 6 -- mon-fri
    and the_date not in (select the_date from dds.dim_date where month_of_year = 1 and holiday)
    and the_date not in (select the_date from dds.dim_date where month_of_year = 11 and holiday)
    and the_date not in (select the_date from dds.dim_date where month_of_year = 12 and holiday)  
  order by the_date) x;  


-- update working days in year, month, pay_period
update dds.working_days_new z
set wd_in_year = x.wd_in_year
from (
  select the_year, wd_in_year
  from wd
  group by the_year, wd_in_year) x
where z.the_year = x.the_year;    


update dds.working_days_new z
set wd_in_month = x.wd_in_month
from (
  select year_month, wd_in_month
  from wd
  group by year_month, wd_in_month) x
where z.year_month = x.year_month;  

update dds.working_days_new z
set wd_in_biweekly_pay_period = x.wd_in_biweekly_pay_period
from (
  select biweekly_pay_period_sequence, wd_in_biweekly_pay_period
  from wd
  group by biweekly_pay_period_sequence, wd_in_biweekly_pay_period) x
where z.biweekly_pay_period_sequence = x.biweekly_pay_period_sequence;

-- update working day of year, month, pay period
update dds.working_days_new z
set wd_of_year = x.wd_of_year,
    wd_of_month = x.wd_of_month,
    wd_of_biweekly_pay_period = x.wd_of_biweekly_pay_period
from (    
  select a.the_date, 
    coalesce(b.wd_of_year, 0) as wd_of_year,
    coalesce(b.wd_of_month, 0) as wd_of_month,
    coalesce(b.wd_of_biweekly_pay_period, 0) as wd_of_biweekly_pay_period
  from dds.working_days_new a
  left join wd b on a.the_date = b.the_date) x
where z.the_date = x.the_date;

-- update wd of year elapsed
update dds.working_days_new z
set wd_of_year_elapsed = x.wd_of_year_elapsed_1
from (
  select a.wd_in_year, a.the_date, a.day_of_week, a.day_of_year, b.the_year, b.wd_of_year_elapsed,
    case 
      when a.day_of_year = 1 then 0 -- new years day
      when b.the_date is null  -- non working day
        and lag(b.the_date, 1) over (order by a.the_date) is not null -- previous day is working day
            then lag(b.wd_of_year_elapsed, 1) over (order by a.the_date) + 1
      when b.the_date is null  -- non working day
        and lag(b.the_date, 1) over (order by a.the_date) is null -- previous day is a non working day
            then coalesce(lead(b.wd_of_year_elapsed, 1) over (order by a.the_date), lead(b.wd_of_year_elapsed, 2) over (order by a.the_date))
      else b.wd_of_year_elapsed
    end as wd_of_year_elapsed_1
  from dds.working_days_new a
  left join wd b on a.the_date = b.the_date) x
where z.the_date = x.the_date; 

-- udpate wd_of_year_remaining
update dds.working_days_new
set wd_of_year_remaining = wd_in_year - wd_of_year_elapsed;


-- wd of month elapsed 
update dds.working_days_new z
set wd_of_month_elapsed = x.wd_of_month_elapsed_1
from (
  select a.wd_in_month, a.the_date, a.day_of_week, a.day_of_month, a.last_of_month, b.the_year, b.wd_of_month_elapsed,
    case 
      when a.day_of_month = 1 then 0 -- new years day
      when b.the_date is null and day_of_month = 2 then 0     
      when b.the_date is null and day_of_month = 3
        and lag(b.the_date, 1) over (order by a.the_date) is null then 0 -- 1st, 2nd & 3rd of month all non working   
      when b.the_date is null  -- non working day
        and lag(b.the_date, 1) over (order by a.the_date) is not null -- previous day is working day
            then lag(b.wd_of_month_elapsed, 1) over (order by a.the_date) + 1
      when b.the_date is null  -- non working day
        and lag(b.the_date, 1) over (order by a.the_date) is null -- previous day is a non working day
          and lag(b.the_date, 2) over (order by a.the_date) is not null -- 2 days ago a working day
            then lag(b.wd_of_month_elapsed, 2) over (order by a.the_date) + 1
      when b.the_date is null  -- non working day
        and lag(b.the_date, 1) over (order by a.the_date) is null -- previous day is a non working day
          and lag(b.the_date, 2) over (order by a.the_date) is null -- 2 days ago a non working day
            then lag(b.wd_of_month_elapsed, 3) over (order by a.the_date) + 1                      
      else b.wd_of_month_elapsed
    end as wd_of_month_elapsed_1
  from dds.working_days_new a
  left join wd b on a.the_date = b.the_date) x
where z.the_date = x.the_date;  

-- udpate wd_of_year_remaining
update dds.working_days_new
set wd_of_month_remaining = wd_in_month - wd_of_month_elapsed;

-- and transfer the departmentally complete data from dds.working_days_new into dds.working_days_2

insert into dds.working_days_2
select * from dds.working_days_new;

end $$;
-- ================================================================================================================================
-- ================================================================================================================================

-- shit, sure hope this stuff is correct
delete 
from dds.working_days
where the_year = 2023;

-- dds.dim_date does not have all days of 205012
insert into dds.working_days
select *
from dds.working_days_2
where the_year < 2050;
