﻿-- CREATE TABLE dds.z_unused_ext_date (
--   the_date date primary key, -- the calendar date
--   day_of_week integer,
--   day_name citext,
--   day_of_month integer,
--   day_of_year integer,
--   month_of_year integer,
--   month_name citext,
--   first_day_of_month boolean,
--   last_day_of_month boolean,
--   weekday boolean,
--   weekend boolean,
--   holiday boolean NOT NULL,
--   the_year integer,
--   iso_week integer,
--   date_type citext,
--   year_month integer,
--   year_month_short citext,
--   sunday_to_saturday_week integer,
--   biweekly_pay_period_start_date date,
--   biweekly_pay_period_end_date date,
--   year_week integer,
--   mmdd citext,
--   mmddyy citext,
--   last_year_month integer,
--   first_of_month date,
--   first_of_month_ts timestamp with time zone,
--   first_of_last_month date,
--   first_of_last_month_ts timestamp with time zone,
--   mmmdd citext,
--   day_in_biweekly_pay_period integer,
--   biweekly_pay_period_sequence integer,
--   sunday_to_saturday_week_select_format citext,
--   quarter integer,
--   last_of_month date,
-- ****** am not populating any of the following fields, one not used, the other was a mistake, working days are department dependent **************************************                                                                                                                   
--   biweekly_pay_period_year_month_pct numeric(5,4), -- the percentage of the pay period in the year month, round((count(the_date) over (partition by year_month, biweekly_pay_period_start_date))/14.0, 4)
--   wd_in_year integer,
--   wd_of_year_elapsed integer,
--   wd_of_year_remaining integer,
--   wd_of_year integer,
--   wd_in_month integer,
--   wd_of_month_elapsed integer,
--   wd_of_month_remaining integer,
--   wd_of_month integer,
--   wd_in_biweekly_pay_period integer,
--   wd_of_biweekly_pay_period_elapsed integer,
--   wd_of_bi_weekly_pay_period_remaining integer,
--   wd_of_biweekly_pay_period integer);
-- 
-- SELECT '2023-12-31'::DATE + SEQUENCE.DAY AS datum
--       FROM GENERATE_SERIES(0, 9862) AS SEQUENCE (DAY)
--       GROUP BY SEQUENCE.DAY
-- 
--      
-- 
-- select current_date
-- 
-- -- select extract(epoch from current_date)

-- https://wiki.postgresql.org/wiki/Date_and_Time_dimensions
-- https://duffn.medium.com/creating-a-date-dimension-table-in-postgresql-af3f8e2941ac

-- WTF is TMDay: (date type formatting functions) https://www.postgresql.org/docs/9.6/functions-formatting.html
-- for biweekly, start at the beginning of dec 2023, manually update overlapping dates with dim_date for dec, then figure a formula
-- something similar for sunday_to_saturday_week
-- thanksgiving
-- last_year_month
-- insert into dds.z_unused_ext_date

select datum as the_date,
  extract(dow from datum) as day_of_week,
  to_char(datum, 'TMDay') AS day_name,
  extract(day from datum) as day_of_month,
  extract(DOY from datum) as day_of_year,
  extract(MONTH from datum) as month_of_year,
	to_char(datum, 'TMMonth') AS month_name,
	case when extract(day from datum) = 1 then true else false end as first_day_of_month,
	case
	  when (SELECT (date_trunc('month', datum) + interval '1 month' - interval '1 day')::date) = datum then true
	  else false
	end as last_day_of_month,
	case when extract(dow from datum) between 1 and 5 then true else false end as weekday,
	case when extract(dow from datum) in(0, 6) then true else false end as weekend,
  case when to_char(datum, 'MMDD') in ('0101', '0704','1225') then true else false end as holiday,
  extract(year from datum) as the_year,
  extract(week from datum) as iso_week,
  'DATE'::citext as date_type,
  to_char(datum, 'YYYYMM')::integer as year_month,
  to_char(datum, 'Mon-YY') as year_month_short,
  0 as sunday_to_saturday_week,
  '01/01/1901' as biweekly_pay_period_start_date,
  '01/01/1901' as biweekly_pay_period_end_date, 
  to_char(datum, 'YYYYWW')::integer as year_week,
--   (extract(year from datum)::text || extract(week from datum)::text)::integer as year_week,
  to_char(datum, 'MM-DD') as mmdd,
  to_char(datum, 'MM-DD-YY') as mmddyy,

  0 as last_year_month, 

  datum + (1 - EXTRACT(DAY FROM datum))::INT AS first_of_month,
  (datum + (1 - EXTRACT(DAY FROM datum))::INT)::timestamptz + '00:00:01' AS first_of_month_ts,
  (date_trunc('month', datum) - interval '1 month')::date as first_of_last_month,
  (date_trunc('month', datum) - interval '1 month')::timestamptz + '00:00:01' as first_of_last_month_ts,
  to_char(datum, 'Mon-DD') as mmmdd,
  0 as day_in_biweekly_pay_period,
  0 as biweekly_pay_period_sequence,
  'not yet' as sunday_to_saturday_week_select_format,
  case 
    when extract(MONTH from datum) between 1 and 3 then 1
    when extract(MONTH from datum) between 4 and 6 then 2 
    when extract(MONTH from datum) between 7 and 9 then 3
    when extract(MONTH from datum) between 10 and 12 then 4
  end as quarter,
  (DATE_TRUNC('MONTH', datum) + INTERVAL '1 MONTH - 1 day')::DATE AS last_of_month,
  0::numeric(5,4) as biweekly_pay_period_year_month_pct,
  0 as wd_in_year ,
  0 as wd_of_year_elapsed ,
  0 as wd_of_year_remaining ,
  0 as wd_of_year ,
  0 as wd_in_month ,
  0 as wd_of_month_elapsed ,
  0 as wd_of_month_remaining ,
  0 as wd_of_month ,
  0 as wd_in_biweekly_pay_period ,
  0 as wd_of_biweekly_pay_period_elapsed ,
  0 as wd_of_bi_weekly_pay_period_remaining ,
  0 as wd_of_biweekly_pay_period 
-- select *   
from (
-- 	SELECT '2023-12-31'::DATE + SEQUENCE.DAY AS datum
	SELECT '2023-12-01'::DATE + SEQUENCE.DAY AS datum
	FROM GENERATE_SERIES(0, 9892) AS SEQUENCE (DAY)
	GROUP BY SEQUENCE.DAY) a
order by datum 

-- day_of_week: extract(dow) gave me 0 - 6, existing table has 1 - 7
update dds.z_unused_ext_date
set day_of_week = day_of_week + 1
where the_date > '12/30/2023';

-- list of a table's fields, comma separated
SELECT string_agg(column_name, ',')
FROM information_schema.columns
WHERE table_schema = 'dds'
  AND table_name   = 'z_unused_ext_date'
group by table_name  


-- update dds.z_unused_ext_date with dds.dim_date for 12/01 -> 12/30 2023
delete from dds.z_unused_ext_date where the_date between '12/01/2023' and '12/30/2023';
insert into dds.z_unused_Ext_date
select the_date,day_of_week,day_name,day_of_month,day_of_year,month_of_year,month_name,
	first_day_of_month,last_day_of_month,weekday,weekend,holiday,the_year,iso_week,date_type,
	year_month,year_month_short,sunday_to_saturday_week,biweekly_pay_period_start_date,
	biweekly_pay_period_end_date,year_week,mmdd,mmddyy,last_year_month,first_of_month,
	first_of_month_ts,first_of_last_month,first_of_last_month_ts,mmmdd,day_in_biweekly_pay_period,
	biweekly_pay_period_sequence,sunday_to_saturday_week_select_format,quarter,last_of_month,
	biweekly_pay_period_year_month_pct,wd_in_year,wd_of_year_elapsed,wd_of_year_remaining,
	wd_of_year,wd_in_month,wd_of_month_elapsed,wd_of_month_remaining,wd_of_month,
	wd_in_biweekly_pay_period,wd_of_biweekly_pay_period_elapsed,
	wd_of_bi_weekly_pay_period_remaining,wd_of_biweekly_pay_period 
from dds.dim_date where the_date between '12/01/2023' and '12/30/2023';

select * from dds.z_unused_ext_date order by the_date

-------------------------------------------------------------------------------------------------------------------
-- biweekly_pay_period_sequence, biweekly_pay_period_start_date, biweekly_pay_period_end_date
-------------------------------------------------------------------------------------------------------------------
update dds.z_unused_ext_date x
set biweekly_pay_period_start_date = y.start_date,
    biweekly_pay_period_end_date = y.end_date,
    biweekly_pay_period_sequence = y.seq
from (    
SELECT a.the_date, c.start_date, b.end_date, seq
-- a.day_name, a.biweekly_pay_period_sequence, a.biweekly_pay_period_start_date, a.biweekly_pay_period_end_date
from dds.z_unused_ext_date a
left join ( -- use "seed" dates for start and end, then mod 14 = 0
  select the_date as end_date, row_number() over (order by the_date) + 389 as seq
  from dds.z_unused_ext_date
  where mod(the_date - '09/23/2023', 14) = 0) b on a.the_date <= b.end_date  and a.the_date > b.end_date - 14
left join (
  select the_date as start_date
  from dds.z_unused_ext_date
  where mod(the_date - '09/10/2023', 14) = 0) c on a.the_date >= c.start_date  and a.the_date < c.start_date + 14  
where a.the_date > '12/02/2023') y
where x.the_date = y.the_date

-- delete 12/25 -> 12/31/2050, no biweekly_pay_period_sequence
delete
-- select *
from dds.z_unused_ext_date 
where biweekly_pay_period_sequence is null;

-------------------------------------------------------------------------------------------------------------------
-- sunday to saturday week
-------------------------------------------------------------------------------------------------------------------

update dds.z_unused_ext_date x
set sunday_to_saturday_week = y.seq
from (  
	SELECT a.the_date, a.day_name, seq 
	from dds.z_unused_ext_date a
	left join ( -- 
		select the_date as the_date, row_number() over (order by the_date) + 1038 as seq
		from dds.z_unused_ext_date
		where mod(the_date - '12/30/2023', 7) = 0) b on a.the_date <= b.the_date and a.the_date > b.the_date - 7 order by a.the_date) y
where x.the_date = y.the_date

-------------------------------------------------------------------------------------------------------------------
-- last_year_month
-------------------------------------------------------------------------------------------------------------------

update dds.z_unused_ext_date x
set last_year_month = y.last_year_month
from (
select the_date, 
--   (extract(year from first_of_last_month)::text || extract(month from first_of_last_month)::text)::integer -- 202401 is coming out as 20241
  (extract(year from first_of_last_month)::text || to_char(first_of_last_month, 'MM'))::integer as last_year_month
from dds.z_unused_ext_date) y
where x.the_date = y.the_date


-------------------------------------------------------------------------------------------------------------------
-- day_in_biweekly_pay_period
-------------------------------------------------------------------------------------------------------------------

update dds.z_unused_ext_date x
set day_in_biweekly_pay_period = y.day_in_biweekly_pay_period
from (
	SELECT the_date, biweekly_pay_period_start_date, biweekly_pay_period_end_date,
		(the_date - biweekly_pay_period_start_date) + 1 as day_in_biweekly_pay_period
	FROM dds.z_unused_ext_date) y
where x.the_date = y.the_date


-------------------------------------------------------------------------------------------------------------------
-- sunday_to_saturday_week_select_format
-------------------------------------------------------------------------------------------------------------------

update dds.z_unused_ext_date x
set sunday_to_saturday_week_select_format = y.select_format
from (
	select .the_date, to_char(week_start, 'TMMonth') || ' ' || extract(day from week_start)::text || ' - ' || 
		to_char(week_end, 'TMMonth') || ' ' || extract(day from week_end)::text || ', ' || extract(year from week_end)::text as select_format
	from (
		select the_date, sunday_to_saturday_week,
			(select min(the_date) from dds.z_unused_ext_date where sunday_to_saturday_week = a.sunday_to_saturday_week) as week_start,
			(select max(the_date) from dds.z_unused_ext_date where sunday_to_saturday_week = a.sunday_to_saturday_week) as week_end,
			sunday_to_saturday_week_select_format
		from dds.z_unused_ext_date a
		where the_date between '12/31/2023' and '12/31/2050'
		order by a.the_date) b) y
where x.the_date = y.the_date;

-------------------------------------------------------------------------------------------------------------------
-- noticed 30 rows with null last_of_month
-------------------------------------------------------------------------------------------------------------------

update dds.dim_date
set last_of_month = '12/31/2023'
where last_of_month is null 
  and the_date between '12/01/2023' and '12/30/2023'

-------------------------------------------------------------------------------------------------------------------
-- delete from dds.z_unused_ext_date rows that already exist in dds.dim_date
-------------------------------------------------------------------------------------------------------------------

delete 
-- select the_date
from dds.z_unused_ext_date a
where exists (
  select 1
  from dds.dim_date
  where the_date = a.the_date);

-------------------------------------------------------------------------------------------------------------------
-- thanksgiving
-------------------------------------------------------------------------------------------------------------------
-- thanksgiving: thursday of the last full week of november (Last Saturday of November - 2)

update dds.z_unused_ext_date
set holiday = true
where the_date in (
	select max(the_date) - 2
	from dds.z_unused_ext_date a
	where month_of_year = 11
		and day_of_week = 7
	group by the_year); 

-- minimal holidays set, never know about labor day or memorial day
select the_date, day_name 
from dds.z_unused_ext_date
where holiday
order by the_date

-------------------------------------------------------------------------------------------------------------------
-- insert the new data into dds.dim_date
-------------------------------------------------------------------------------------------------------------------
insert into dds.dim_date (
the_date,day_of_week,day_name,day_of_month,day_of_year,month_of_year,month_name,
	first_day_of_month,last_day_of_month,weekday,weekend,holiday,the_year,iso_week,date_type,
	year_month,year_month_short,sunday_to_saturday_week,biweekly_pay_period_start_date,
	biweekly_pay_period_end_date,year_week,mmdd,mmddyy,last_year_month,first_of_month,
	first_of_month_ts,first_of_last_month,first_of_last_month_ts,mmmdd,day_in_biweekly_pay_period,
	biweekly_pay_period_sequence,sunday_to_saturday_week_select_format,quarter,last_of_month,
	biweekly_pay_period_year_month_pct,wd_in_year,wd_of_year_elapsed,wd_of_year_remaining,
	wd_of_year,wd_in_month,wd_of_month_elapsed,wd_of_month_remaining,wd_of_month,
	wd_in_biweekly_pay_period,wd_of_biweekly_pay_period_elapsed,
	wd_of_bi_weekly_pay_period_remaining,wd_of_biweekly_pay_period)
select the_date,day_of_week,day_name,day_of_month,day_of_year,month_of_year,month_name,
	first_day_of_month,last_day_of_month,weekday,weekend,holiday,the_year,iso_week,date_type,
	year_month,year_month_short,sunday_to_saturday_week,biweekly_pay_period_start_date,
	biweekly_pay_period_end_date,year_week,mmdd,mmddyy,last_year_month,first_of_month,
	first_of_month_ts,first_of_last_month,first_of_last_month_ts,mmmdd,day_in_biweekly_pay_period,
	biweekly_pay_period_sequence,sunday_to_saturday_week_select_format,quarter,last_of_month,
	biweekly_pay_period_year_month_pct,wd_in_year,wd_of_year_elapsed,wd_of_year_remaining,
	wd_of_year,wd_in_month,wd_of_month_elapsed,wd_of_month_remaining,wd_of_month,
	wd_in_biweekly_pay_period,wd_of_biweekly_pay_period_elapsed,
	wd_of_bi_weekly_pay_period_remaining,wd_of_biweekly_pay_period 
from dds.z_unused_ext_date
order by the_date;


ERROR:  duplicate key value violates unique constraint "dim_date_pk"
DETAIL:  Key (date_key)=(2) already exists.

thought it would just pick up the next value in the serial column for date_key



-- a copy of dds.dim_date with date_key as an integer
CREATE TABLE dds.test_dim_date
(
  date_key integer NOT NULL,
  the_date date NOT NULL, -- the calendar date
  day_of_week integer,
  day_name citext,
  day_of_month integer,
  day_of_year integer,
  month_of_year integer,
  month_name citext,
  first_day_of_month boolean,
  last_day_of_month boolean,
  weekday boolean,
  weekend boolean,
  holiday boolean NOT NULL,
  the_year integer,
  iso_week integer,
  date_type citext,
  year_month integer,
  year_month_short citext,
  sunday_to_saturday_week integer,
  biweekly_pay_period_start_date date,
  biweekly_pay_period_end_date date,
  year_week integer,
  mmdd citext,
  mmddyy citext,
  last_year_month integer,
  first_of_month date,
  first_of_month_ts timestamp with time zone,
  first_of_last_month date,
  first_of_last_month_ts timestamp with time zone,
  mmmdd citext,
  day_in_biweekly_pay_period integer,
  biweekly_pay_period_sequence integer,
  sunday_to_saturday_week_select_format citext,
  quarter integer,
  last_of_month date,
  biweekly_pay_period_year_month_pct numeric(5,4), -- the percentage of the pay period in the year month, round((count(the_date) over (partition by year_month, biweekly_pay_period_start_date))/14.0, 4)
  wd_in_year integer,
  wd_of_year_elapsed integer,
  wd_of_year_remaining integer,
  wd_of_year integer,
  wd_in_month integer,
  wd_of_month_elapsed integer,
  wd_of_month_remaining integer,
  wd_of_month integer,
  wd_in_biweekly_pay_period integer,
  wd_of_biweekly_pay_period_elapsed integer,
  wd_of_bi_weekly_pay_period_remaining integer,
  wd_of_biweekly_pay_period integer,
  CONSTRAINT test_dim_date_pk PRIMARY KEY (date_key)
)


insert into dds.test_dim_date
select * from dds.dim_date

select * from dds.test_dim_date order by the_date desc limit 20

select date_key, the_date from dds.test_dim_date order by the_date



-- create a table with a serial key
drop table if exists dds.test_1;
create table dds.test_1 (
  id serial,
  f1 citext);
insert into dds.test_1 (f1) values
('a'),  
('b'), 
('c'), 
('d'), 
('e');
select * from dds.test_1;

CREATE SEQUENCE dds.test_1_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 5
  CACHE 1;
-- compared to:  
CREATE SEQUENCE dds.dim_date_date_key_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 2
  CACHE 1;


-- another copy of dds.dim_date with date_key as a serial
drop table if exists dds.test_dim_date_2;
CREATE TABLE dds.test_dim_date_2
(
  date_key serial,
  the_date date NOT NULL, -- the calendar date
  day_of_week integer,
  day_name citext,
  CONSTRAINT test_dim_date_2_pk PRIMARY KEY (date_key)
);

-- created by the create table
CREATE SEQUENCE dds.test_dim_date_2_date_key_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 35794
  CACHE 1;

  
insert into dds.test_dim_Date_2 (the_date,day_of_week,day_name)
select the_date,day_of_week,day_name
from dds.dim_date
order by the_date;

select * from dds.test_dim_date_2 order by the_date desc limit 20

-- this doe not work, remain serial, bigint makes it bigserial
alter table dds.test_dim_Date_2
alter  column date_key type integer;

-- try this
DROP SEQUENCE dds.test_dim_date_2_date_key_seq;
ERROR:  cannot drop sequence dds.test_dim_date_2_date_key_seq because other objects depend on it
DETAIL:  default for table dds.test_dim_date_2 column date_key depends on sequence dds.test_dim_date_2_date_key_seq
HINT:  Use DROP ... CASCADE to drop the dependent objects too.

-- so, try this
ALTER TABLE dds.test_dim_Date_2 ALTER COLUMN date_key DROP DEFAULT;
that changed it to: date_key integer NOT NULL
-- now, this works
DROP SEQUENCE dds.test_dim_date_2_date_key_seq;

select * from dds.test_dim_Date_2 order by date_key desc
--------------------------------------------------------------------------------------------------------------------------
-- remember, the date key, though it is a serial is not in sequential order with the dates
--
-- so, what i think i need to do is to change the PK datatype in dds.dim_date
-- but, first i want to do that on a test table and make a back up of the dim_date table
-- we are stuck with using the goofy date_key values
---------------------------------------------------------------------------------------------------------------------------
-- backup
insert into dds.dim_date_backup  -- date_key is a bigint
select * from dds.dim_date;
select date_key, the_Date from dds.dim_date order by date_key desc

-- remove default on date_key
ALTER TABLE dds.dim_date ALTER COLUMN date_key DROP DEFAULT;
-- now drop the sequence
DROP SEQUENCE dds.dim_date_date_key_seq;

-- 35795 is the largest, 12/31/2003
select * from dds.dim_date order by date_key desc limit 10
select * from dds.dim_date where the_date = '12/30/2023'

insert into dds.dim_date (
	date_key, the_date,day_of_week,day_name,day_of_month,day_of_year,month_of_year,month_name,
	first_day_of_month,last_day_of_month,weekday,weekend,holiday,the_year,iso_week,date_type,
	year_month,year_month_short,sunday_to_saturday_week,biweekly_pay_period_start_date,
	biweekly_pay_period_end_date,year_week,mmdd,mmddyy,last_year_month,first_of_month,
	first_of_month_ts,first_of_last_month,first_of_last_month_ts,mmmdd,day_in_biweekly_pay_period,
	biweekly_pay_period_sequence,sunday_to_saturday_week_select_format,quarter,last_of_month,
	biweekly_pay_period_year_month_pct,wd_in_year,wd_of_year_elapsed,wd_of_year_remaining,
	wd_of_year,wd_in_month,wd_of_month_elapsed,wd_of_month_remaining,wd_of_month,
	wd_in_biweekly_pay_period,wd_of_biweekly_pay_period_elapsed,
	wd_of_bi_weekly_pay_period_remaining,wd_of_biweekly_pay_period)
select row_number() over(order by the_date) + 35795, the_date,day_of_week,day_name,day_of_month,day_of_year,month_of_year,month_name,
	first_day_of_month,last_day_of_month,weekday,weekend,holiday,the_year,iso_week,date_type,
	year_month,year_month_short,sunday_to_saturday_week,biweekly_pay_period_start_date,
	biweekly_pay_period_end_date,year_week,mmdd,mmddyy,last_year_month,first_of_month,
	first_of_month_ts,first_of_last_month,first_of_last_month_ts,mmmdd,day_in_biweekly_pay_period,
	biweekly_pay_period_sequence,sunday_to_saturday_week_select_format,quarter,last_of_month,
	biweekly_pay_period_year_month_pct,wd_in_year,wd_of_year_elapsed,wd_of_year_remaining,
	wd_of_year,wd_in_month,wd_of_month_elapsed,wd_of_month_remaining,wd_of_month,
	wd_in_biweekly_pay_period,wd_of_biweekly_pay_period_elapsed,
	wd_of_bi_weekly_pay_period_remaining,wd_of_biweekly_pay_period 
from dds.z_unused_ext_date
order by the_date;

select * from dds.dim_date where year_month in (202312,202401) order by the_date

select * from dds.dim_date order by the_date desc limit 10

-- looks good, do some clean up
DROP TABLE dds.z_unused_ext_date cascade;
DROP TABLE dds.test_dim_date cascade;
DROP TABLE dds.test_dim_date_2 cascade;
DROP TABLE dds.dim_date_backup cascade;
DROP TABLE dds.test_1 cascade;

