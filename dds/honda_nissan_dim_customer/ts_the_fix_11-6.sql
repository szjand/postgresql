﻿
--------------------------------------------------------------------------------------
-- 4. populate dds.tmp_ro_keys
--------------------------------------------------------------------------------------

/*
-- holy shit
select * from arkona.ext_bopname where bopname_company_number = 'RY2' and company_individ is not null

select * from dds.dim_customer where store_code = 'RY2'

the only RY2 dim_customer record is inventory
*/
-- and test for double ro, looks good
select ro from (
select a.store_code, a.ro, a.customer,f.bnkey, f.full_name, f.store_code, f.customer_key    
-- select a.*
  FROM dds.tmp_ro  a

--   LEFT JOIN dds.dim_customer f ON a.customer = f.bnkey 
--     and a.store_code = f.store_code ------------------------------------------------------------ *a*
  LEFT JOIN dds.dim_customer f ON a.customer = f.bnkey 
    and
      case   -- thought this was the fix, except returning 2 rows for every ro  , but what's weird is that it is the join to unknown that is causing the doubling   
             -- ok, this fixed it, instead of joining on full_name = 'unknown' (that is 2 rows in dim_customer) join on bnkey = 1                                         
        when a.store_code = 'RY8' then a.store_code = f.store_code
        when a.customer = 0 then a.store_code = f.store_code
        else f.store_code = 'RY1'
      end    
--     and
--       case                                                               
--         when a.store_code = 'RY8' then a.store_code = f.store_code
--         else f.store_code = 'RY1'
--       end    
--     and
--       case                                                               
--         when a.store_code in ('RY1','RY2') then a.store_code = f.store_code
--         else true
--       end
--     and
--       case
--         when a.customer = 0 then a.store_code = f.store_code
--         else true
--       end   
  LEFT JOIN dds.dim_customer k ON 1 = 1
    and k.bnkey = 1
) x group by ro having count(*) > 1
-- *i*    
--     and k.bnkey <> 1171298
-- where a.customer = 0
-- where a.ro = '16544035'  

order by a.store_code, ro    
