﻿/*
I am looking for how many customers who purchased a new Honda and a new Nissan vehicle from 2017, 2018 and 2019 
from our Honda Nissan store, of those customer’s that bought new, how many has had a customer pay repair order 
at the Honda Nissan store…………….and of those same customers, how many had a customer pay repair order at the GM store.

1/13/21
Randy Sattler

*/

select * from sls.deals_by_month a limit 10
select * from sls.deals a limit 10

select *
from sls.deals_by_month a 
where a.sale_type <> 'Wholesale'
  and a.unit_count = 1
  and a.store_code = 'RY2'
  and a.year_month between 201701 and 201912
  and a.vehicle_type = 'new'

-- these queries are based on sql/postgresql/used_car_meeting/glump_proportions/glump_proportion_of_sales.sql
drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select store, page, line, line_label, control, sum(unit_count) as unit_count
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201701 AND 201912 --------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month between 201701 AND 201912 -------------------------------------------------------------------
      and b.page between 5 and 15 and b.line between 1 and 45
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
      and e.current_row -- **
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
      and f.store = 'RY2'
    group by f.store, d.gl_account, b.page, b.line, b.line_label, e.description   ) d on c.account = d.gl_account -- when doing multiple months, group account info
  where a.post_status = 'Y') h
group by store, page, line, line_label, control
order by store, page, line;

select * from step_1

drop table if exists step_2 cascade;  
create temp table step_2 as
select b.date_capped as sale_date, a.control as stock_number, b.bopmast_vin as vin, b.buyer_number, b.retail_price 
from step_1 a
left join arkona.ext_bopmast b on a.control = b.bopmast_stock_number
where a.unit_count = 1  -- G37123G is the only one 
  and b.bopmast_vin is not null;
create index on step_2(vin);
create index on step_2(stock_number);
create index on step_2(retail_price);
create index on step_2(sale_date);

select count(*) from step_2;

drop table if exists ros cascade;
create temp table ros as
select e.bnkey as ro_cust, a.ro, a.open_date, b.make, c.stock_number, c.vin, c.buyer_number, c.sale_date
from dds.fact_repair_order a
join dds.dim_vehicle b on a.vehicle_key = b.vehicle_key
join step_2 c on b.vin = c.vin
join dds.dim_payment_type d on a.payment_type_key = d.payment_type_key
  and d.payment_type_code = 'C'
join dds.dim_customer e on a.customer_key = e.customer_key  
  and e.customer_key <> 2
where a.open_date > c.sale_date
group by e.bnkey, a.ro, a.open_date, b.make, c.stock_number, c.vin, c.buyer_number, c.sale_date;

select * from ros
select * from dds.dim_customer where customer_key in (2,105211,156611)
select * from dds.dim_customer where bnkey in (111,530)
select * from dds.dim_customer limit 10

select * from step_2 where buyer_number = 226274

select ro_cust, count(*)
from ros
group by ro_cust
order by count(*) desc

selecT * from  ros where ro_cust = buyer_number

-- these are the ro numbers
select 
  count(vin) filter (where ro_store = '1') as "GM",
  count(vin) filter (where ro_store = '2') as "Honda Nissan",
  count(vin) filter (where ro_store = '1,2') as "GM & Honda Nissan"
from (  
  select vin, string_agg(distinct left(ro, 1), ',') as ro_store
  from ros
  group by vin) a;

sent to randy:
Sales: 2184
Customer pay RO at Honda Nissan: 1081
Customer pay RO at GM: 66
Customer pay RO at both Honda Nissan & GM: 765

