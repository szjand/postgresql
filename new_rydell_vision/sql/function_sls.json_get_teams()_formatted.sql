﻿-- select row_to_json(YY)
-- from ( 
	select 
		( -- Y a single column called sales_teams
			select json_agg(row_to_json(Y)) as sales_teams
			from (
				select a.team_id as id, b.team as team_name, true as is_active, 
					array(
						select employee_number 
						from sls.team_personnel_new z 
						where z.team_id = a.team_id) as sales_employees,
					(
						select distinct store_key 
						from onedc.stores d 
						where c.store_code = d.arkona_store_key) as sales_store,
					(
						select employee_number 
						from sls.team_personnel_new z 
						where team_role = 'team_leader'
						and z.team_id = a.team_id) as team_leader
			from sls.team_personnel_new a
			inner join sls.teams_new b on a.team_id = b.team_id
			inner join sls.personnel_new c on a.employee_number = c.employee_number
			group by a.team_id, b.team,store_code)Y), 


			
			( -- sales-employees a single column
				select json_agg(row_to_json(Z))
				from (
					select a.employee_number as id, a.first_name,a.last_name, 
						c.the_role as sales_role,
						d.team_id as team_membership, 
						current_date - (select max(created_ts)::date from oo.one_on_ones where reviewee = a.employee_number) as days_since_last_one_on_one,  
						(select reviewer_notes from oo.prep_notes where reviewee =  a.employee_number) as reviewer_prep_notes,
						(select reviewer_notes from oo.prep_notes where reviewee =  a.employee_number) as reviewee_prep_notes, 
						(select last_reviewee_update_ts from oo.prep_notes where reviewee = a.employee_number) as reviewee_last_updated, 
						(select last_reviewer_update_ts  from oo.prep_notes where reviewee = a.employee_number) as reviewer_last_updated, 
						(
							select json_agg(row_to_json(A))
							from (
								select value_metric as measure,
									json_agg(json_build_object('sales_role',qq.team_role,'green',green_value,'yellow',yellow_value,'red',red_value)) as metrics
								from oo.one_on_one_goals qq
								group by value_metric ) A) as stats_color,

-- this is the column that: ERROR:  more than one row returned by a subquery used as an expression		
select						
						case 
							when c.the_role = 'team_leader' 
								then ( -- END 
									select row_to_json(T1)
										from ( -- T1
											select * ,
												( -- CONSULTANTS DATA as a column
													select json_agg(row_to_json(C1)) as consultants_data
													from ( -- C1
														select concat(first_name,' ',last_name) as fullname,r.team_role as sales_role, c.*
														from oo.mtd_stats C
														inner join sls.personnel_new b on c.employee_number = b.employee_number
														inner join sls.team_personnel_new r on b.employee_number = r.employee_number
														where team_leader in ( -- IN
															select employee_number
															from sls.team_personnel_new
															where team_role = 'team_leader')
																and c.employee_number not in ( -- NOT IN
																	select employee_number
																	from sls.team_personnel_new
																	where team_role = 'team_leader')
																		and team_leader = x.employee_number) C1)
												from oo.mtd_stats x
												where x.employee_number =  a.employee_number
													and x.employee_number in ( -- NOT IN
														select employee_number
														from sls.team_personnel_new
															where team_role = 'team_leader')) T1) 
							end as leader_mtd_stats -- ,


															
-- 									case 
-- 								when c.the_role in ('consultant','digital') 
-- 							then ( -- END 
-- 								select row_to_json(T)
-- 								from ( -- T
-- 									select *
-- 									from oo.mtd_stats x
-- 									where a.employee_number = x.employee_number) T 
-- 						) end as consultant_mtd_stats,
-- 							case 
-- 								when c.the_role = 'team_leader' 
-- 							then ( -- END
-- 								select row_to_json(T)
-- 								from ( -- T
-- 									select * ,( -- CONSULTANT DATA
-- 										select json_agg(row_to_json(A)) as consultants_data
-- 											from(  -- A
-- 												select concat(first_name,' ',last_name) as fullname,c.team_role as sales_role,a.*
-- 												from oo.time_since_last_stats_leader a
-- 												inner join sls.personnel_new b on a.employee_number = b.employee_number
-- 												inner join sls.team_personnel_new c on b.employee_number = c.employee_number
-- 												where team_leader  in ( -- IN
-- 													select employee_number
-- 													from sls.team_personnel_new
-- 													where  team_role = 'team_leader')
-- 														and a.employee_number not in (  -- NOT IN
-- 															select employee_number
-- 															from sls.team_personnel_new
-- 															where  team_role = 'team_leader')
-- 															and team_leader = x.employee_number
-- 											) A
-- 										)
-- 								from oo.time_since_last_stats_leader x
-- 								where x.employee_number =  a.employee_number ) T
-- 							) end as leader_since_last_stats, 
-- 														case 
-- 								when c.the_role in ('consultant','digital') 
-- 							then ( -- END 
-- 								select row_to_json(T)
-- 								from ( -- T
-- 									select *
-- 									from oo.time_since_last_stats x
-- 									where a.employee_number = x.employee_number) T 
-- 						) end as consultant_since_last_stats
     
						from sls.personnel_new a
						left join nrv.employee_roles b on a.employee_number = b.employee_number
						left join nrv.employee_role_types c on b.role_key = c.role_key
						inner join sls.team_personnel_new d on a.employee_number = d.employee_number order by last_name
				 ) Z  
						) as sales_employees
		--)YY
