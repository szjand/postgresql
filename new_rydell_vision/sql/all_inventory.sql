﻿
--1. base inpmast_inventory
/*
drop table if exists nrv.all_inventory cascade;
create table nrv.all_inventory (
  the_date date not null,
  stock_number citext not null,
  vin citext not null, 
  model_year integer not null,
  make citext not null,
  model citext not null, 
  trim_level citext,
  cost_gl integer,
  msrp integer,
  best_price integer,
  ad_price integer,
  age integer,
  new_used citext not null,
  certified boolean not null,
  fleet boolean not null,
  color citext,
  tool_status citext,
  board_status citext,
  primary key (the_date,stock_number));
create index on nrv.all_inventory(vin); 
create index on nrv.all_inventory(stock_number);

comment on table nrv.all_inventory is 'inpmast based inventory for testing accuracy of web sites';
comment on column nrv.all_inventory.tool_status is 'one of :Sold and Not Delivered,At Auction,
  Sales Buffer,WS Buffer,In Transit,Trade Buffer,Inspection Pending,Walk Pending,
  Raw Materials,Available,Pricing Buffer,Pulled';
*/  

insert into nrv.all_inventory 
select current_date, a.inpmast_stock_number as stock_number, 
  a.inpmast_vin as vin, a.year as model_year, a.make, a.model, 
  coalesce(a.body_style, b.trim) as trim, a.inpmast_vehicle_cost as cost, 
  a.list_price as msrp,  null as best_price, null as ad_price, null as age, 
  a.type_n_u as new_used, false as certified, false as fleet, 
  coalesce(a.color, b.exteriorcolor) as color, null as uc_status, 
  null as board_status
from arkona.xfm_inpmast a
left join ads.ext_vehicle_items b on a.inpmast_vin= b.vin
where current_row
--   and a.inpmast_vin <> '5XXGN4A7XCG079120'
  and status = 'I';

-- not sure where the 0's in msrp are coming from
-- TODO
update nrv.all_inventory
set msrp = null
where msrp = 0
  and the_date = current_date;  

-- -- used car statuses
-- -- for some unknown goofy reason this takes WAY to long unless i first manually run the inner query
-- update nrv.all_inventory z
-- set tool_status = x.uc_status
-- from (
--   select a.stock_number,
--     (select ads.get_current_vehicle_status (b.vehicleinventoryitemid)) as uc_status
--   from nrv.all_inventory  a
--   join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
--   where a.the_date = current_date) x
-- where z.stock_number = x.stock_number
--   and z.the_date = current_date;

-- used car statuses
update nrv.all_inventory z
set tool_status = x.status
from (
with 
  viiid as (
    select a.stock_number, b.vehicleinventoryitemid
    from nrv.all_inventory a
    join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
    where a.the_date = current_date)
select *, count(status) over (partition by status) 
from viiid c
  left join lateral (
    SELECT 
      CASE
    --     WHEN veh.Wholesaled = 1 THEN 'Wholesaled'
    --     WHEN veh.Delivered = 1 THEN 'Delivered'
        WHEN veh.Sold = 1 THEN 'Sold and Not Delivered'
        WHEN veh.AtAuction = 1 THEN 'At Auction'
        WHEN veh.SalesBuffer = 1 THEN 'Sales Buffer'
        WHEN veh.WholesaleBuffer = 1 THEN 'WS Buffer'
        WHEN veh.InTransit = 1 THEN 'In Transit'
        WHEN veh.TNA = 1 THEN 'Trade Buffer'  
        WHEN veh.InspPending = 1 THEN 'Inspection Pending'
        WHEN veh.WalkPending = 1 THEN 'Walk Pending'
        WHEN veh.RawMaterials = 1 THEN 'Raw Materials'          
        WHEN veh.Available = 1 THEN 'Available'
        WHEN veh.PricingBuffer = 1 THEN 'Pricing Buffer'
        WHEN veh.Pulled = 1 THEN 'Pulled'
    --     WHEN veh.BookingPending = 1 THEN 'Booking Pending'
        ELSE 'unknown'
      END AS Status
    FROM (
      select

          ( -- Sold
          SELECT 1
            FROM  ads.ext_vehicle_sales vs 
          WHERE vs.VehicleInventoryItemID = c.vehicleinventoryitemid
            AND vs.Status = 'VehicleSale_Sold'
          AND vs.Typ = 'VehicleSale_Retail') AS Sold,
        ( -- AtAuction  
          SELECT 1
          FROM ads.ext_vehicle_inventory_item_statuses
          WHERE VehicleInventoryItemID = c.vehicleinventoryitemid
          AND status = 'RMFlagAtAuction_AtAuction'
          AND ThruTS > now()) AS AtAuction,  
        ( -- SalesBuffer
          SELECT 1
          FROM ads.ext_vehicle_inventory_item_statuses
          WHERE VehicleInventoryItemID = c.vehicleinventoryitemid
          AND status = 'RMFlagSB_SalesBuffer'
          AND ThruTS > now()) AS SalesBuffer,  
        ( -- WholesaleBuffer
          SELECT 1
          FROM ads.ext_vehicle_inventory_item_statuses
          WHERE VehicleInventoryItemID = c.vehicleinventoryitemid
          AND status = 'RMFlagWSB_WholesaleBuffer'
          AND ThruTS > now()) AS WholesaleBuffer,      
        ( -- IN Transit
          SELECT 1
          FROM ads.ext_vehicle_inventory_item_statuses
          WHERE VehicleInventoryItemID = c.vehicleinventoryitemid
          AND status = 'RMFlagPIT_PurchaseInTransit'
          AND ThruTS > now()) AS InTransit,   
        ( -- TNA
          SELECT 1
          FROM ads.ext_vehicle_inventory_item_statuses
          WHERE VehicleInventoryItemID = c.vehicleinventoryitemid
          AND status = 'RMFlagTNA_TradeNotAvailable'
          AND ThruTS > now()) AS TNA, 
        ( -- Insp Pending
          SELECT 1
          FROM ads.ext_vehicle_inventory_item_statuses
          WHERE VehicleInventoryItemID = c.vehicleinventoryitemid
          AND status = 'RMFlagIP_InspectionPending'
          AND ThruTS > now()) AS InspPending, 
        ( -- Walk Pending
          SELECT 1
          FROM ads.ext_vehicle_inventory_item_statuses
          WHERE VehicleInventoryItemID = c.vehicleinventoryitemid
          AND status = 'RMFlagWP_WalkPending'
          AND ThruTS > now()) AS WalkPending,
        ( -- Raw Materials
          SELECT 1
          FROM ads.ext_vehicle_inventory_item_statuses
          WHERE VehicleInventoryItemID = c.vehicleinventoryitemid
          AND status = 'RMFlagRMP_RawMaterialsPool'
          AND ThruTS > now()) AS RawMaterials, 
        ( -- Available
          SELECT 1
          FROM ads.ext_vehicle_inventory_item_statuses
          WHERE VehicleInventoryItemID = c.vehicleinventoryitemid
          AND status = 'RMFlagAV_Available'
          AND ThruTS > now()) AS Available,
        ( -- Pricing Buffer
          SELECT 1
          FROM ads.ext_vehicle_inventory_item_statuses
          WHERE VehicleInventoryItemID = c.vehicleinventoryitemid
          AND status = 'RMFlagPB_PricingBuffer'
          AND ThruTS > now()) AS PricingBuffer,	  
        ( -- Pulled
          SELECT 1
          FROM ads.ext_vehicle_inventory_item_statuses
          WHERE VehicleInventoryItemID = c.vehicleinventoryitemid
          AND status = 'RMFlagPulled_Pulled'
          AND ThruTS > now()) AS Pulled) veh) d on true) x
where z.stock_number = x.stock_number
  and z.the_date = current_date;

-- certified
update nrv.all_inventory z
set certified  = true
where the_date = current_date
  and stock_number in (
    select stock_number
    from (  
      select a.stock_number, c.typ, row_number() over (partition by a.stock_number order by selectedreconpackagets desc)
      from nrv.all_inventory a
      join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
      join ads.ext_selected_recon_packages c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
      where a.the_date = current_date) x
    where x.row_number = 1
      and typ like '%Factory');
      

-- do these pricings sequentially, 1st tool, then inpmast, the pricing page
-- best price (tool)
-- compare to internet price: hardly any honda used in internet price
update nrv.all_inventory z
set best_price = x.amount
from (
  select stock_number, amount
  from (
    select a.stock_number, d.amount::integer, row_number() over (partition by a.stock_number order by vehiclepricingts desc)
    from nrv.all_inventory a
    join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
    join ads.ext_vehicle_pricings c on b.vehicleinventoryitemid = c.vehicleinventoryitemid
    join ads.ext_vehicle_pricing_details d on c.vehiclepricingid = d.vehiclepricingid
      and d.typ = 'VehiclePricingDetail_BestPrice'
    where a.new_used = 'U'
      and a.the_date = current_date) e
  where row_number = 1) x
where z.stock_number = x.stock_number
  and z.the_date = current_date;

-- best price (dealertrack: internet price new vehicles only)
update nrv.all_inventory z
set best_price = x.internet_price
from (
  SELECT a.stock_number, b.num_field_value::integer as internet_price
  FROM nrv.all_inventory a
  join arkona.ext_inpoptf b on a.vin = b.vin_number
  join arkona.ext_inpoptd c on b.company_number = c.company_number
    and b.seq_number = c.seq_number
    and c.field_descrip = 'Internet Price'
  where b.num_field_value <> 0
    and a.the_date = current_date
    and a.new_used = 'N') x
where z.stock_number = x.stock_number
  and z.the_date = current_date;  


-- best price (gm: pricing)
-- ad price (pricing)
update nrv.all_inventory z
set best_price = x. best_price, 
    ad_price = x.advertised_price
from ( -- x  
  select stock_number, vin, round(vehicle_cost + margin - (total_customer_cash)) as best_price,
    case 
      when ad_price_value = 'MSRP' then round(msrp - (total_stackable + total_major)) 
      when ad_price_value = 'Invoice' then round(total_invoice - (total_stackable + total_major))
      else round(vehicle_cost + margin - (total_customer_cash)) 
    end as advertised_price
  from ( -- y
    select b.vin, b.stock_number,
      coalesce(a.total_cash,0)+ case when coalesce(f.is_checked,false) is true then f.dealer_cash else 0 end total_customer_cash,
      coalesce(a.total_stackable,0) as total_stackable, 
      coalesce(a.total_major,0) as total_major, 
      coalesce(e.margin,0) as margin,
      coalesce(b.cost_gl,0) as vehicle_cost,
      b.msrp,  l.ad_price_value, d.total_invoice
    from nrv.all_inventory b
    left join gmgl.vehicle_invoices d on b.vin  = d.vin and d.thru_date > current_date
    left join gmgl.vin_incentive_cash  a on a.vin = b.vin 
      and a.thru_ts > now()
      and a.is_best_incentive = true
    left join gmgl.vin_margins e on b.vin = e.vin 
      and e.thru_ts > now()
    left join gmgl.vin_dealer_cash f on b.vin = f.vin 
      and f.thru_ts > now()
    left join gmgl.ad_prices l on b.model_year = l.model_year and b.make = l.make and l.thru_ts > now()
    where b.new_used = 'N'
      and b.make in ('chevrolet','gmc','buick','cadillac')
      and b.the_date = current_date) y) x
where z.stock_number = x.stock_number
  and z.the_date = current_date;  



-- age (accounting)
update nrv.all_inventory z
set age = x.days_in_inventory
from ( 
  select c.control, current_date - min(d.the_date) as days_in_inventory
  from arkona.xfm_inpmast a
  join nrv.all_inventory aa on a.inpmast_stock_number = aa.stock_number
  join fin.dim_Account b on a.inventory_account = b.account
  join fin.fact_gl c on b.account_key = c.account_key
      and c.post_status = 'Y'
      and c.control = aa.stock_number    
  join dds.dim_date d on c.date_key = d.date_key
  where a.current_row  
  group by c.control) x
where z.stock_number = x.control
  and z.the_date = current_date; 
  
-- fleet (orders)
update nrv.all_inventory
set fleet = true
where the_date = current_date
  and stock_number in (
  select a.stock_number
  from nrv.all_inventory a
  join gmgl.vehicle_orders e on a.vin = e.vin
  join gmgl.vehicle_order_types f on e.order_type = f.vehicle_order_type_key
    and f.vehicle_order_type like '%fleet%'
    and a.the_date = current_date);



/* 
-- new car board statuses, need to be aware of pending/delivery coming up, i know these fuckers are going 
-- to think "PENDING SALE" on website is important
-- 
drop table if exists board_data cascade;
create temp table board_data as
select a.board_id, a.boarded_ts::date as board_date, d.arkona_store_key, c.last_name as boarded_by, b.board_type, b.board_sub_type,
  e.vehicle_type, a.deal_number, a.stock_number, a.vin, 
  case when a.is_deleted then 'DELETED' else null end as deleted, 
  case when a.is_backed_on then 'BACK_ON' else null end as back_on, 
  e.sale_code, vehicle_make
-- select *
from board.sales_board a
left join board.board_types b on a.board_type_key = b.board_type_key
left join nrv.users c on a.boarded_by = c.user_key
left join onedc.stores d on a.store_key = d.store_key
left join (
  select vehicle_type, board_id, vehicle_make, customer_name, sale_code
  from board.daily_board 
  group by vehicle_type, board_id, vehicle_make, customer_name, sale_code) e on a.board_id = e.board_id; 
create index on board_data(stock_number);
create index on board_data(vin);
create index on board_data(board_type);

-- select * from board_data where stock_number = '32628a'

select *
from board_data a
left join board.physical_delivery_board b on a.board_id = b.board_id
where a.stock_number = '32138A'

select a.*, b.is_deleted, b.stock_number
from board.physical_delivery_board a
join board.sales_board b on a.board_id = b.board_id
  and not b.is_deleted
where not a.is_physically_delivered 

select * from board.pending_boards


deliveries coming up will not work, they are not in base inventory, have been converted to C in inpmast
-- deliveries coming up
select a.*, b.est_physically_delivered_ts::date, est_delivery_notes
from board_data a
join board.physical_delivery_board b on a.board_id = b.board_id
  and not b.is_physically_delivered
where a.back_on is null  
  and a.deleted is null
order by stock_number  

select a.stock_number
from board.sales_board a
join board.physical_delivery_board aa on a.board_id = aa.board_id
  and not aa.is_physically_delivered
left join board.board_types b on a.board_type_key = b.board_type_key
left join nrv.users c on a.boarded_by = c.user_key
left join onedc.stores d on a.store_key = d.store_key
left join (
  select vehicle_type, board_id, vehicle_make, customer_name, sale_code
  from board.daily_board 
  group by vehicle_type, board_id, vehicle_make, customer_name, sale_code) e on a.board_id = e.board_id
where not a.is_deleted
  and not a.is_backed_on    

-- deliveries coming up
update nrv.all_inventory
set board_status = 'delivery_coming_up'
where stock_number in (
  select a.stock_number
  from board.sales_board a
  join board.physical_delivery_board aa on a.board_id = aa.board_id
    and not aa.is_physically_delivered
  left join board.board_types b on a.board_type_key = b.board_type_key
  left join nrv.users c on a.boarded_by = c.user_key
  left join onedc.stores d on a.store_key = d.store_key
  left join (
    select vehicle_type, board_id, vehicle_make, customer_name, sale_code
    from board.daily_board 
    group by vehicle_type, board_id, vehicle_make, customer_name, sale_code) e on a.board_id = e.board_id
  where not a.is_deleted
    and not a.is_backed_on)  
and the_date = current_date;       

select * from nrv.all_inventory where the_date = current_date and stock_number in ('G35068','H11845','G34479','G34515RB','G34702','G35350X','G33909R','H11746A')

select * from arkona.xfm_inpmast where inpmast_stock_number in ('G35068','H11845','G34479','G34515RB','G34702','G35350X','G33909R','H11746A') and current_row
  
------------------------------------------------------------------------------
-- misc queries
------------------------------------------------------------------------------

select the_Date, count(*) from nrv.all_inventory group by the_date

-- newly acquired inventory today
select *
from nrv.all_inventory a
where the_Date = current_date
  and not exists (
    select 1
    from nrv.all_inventory
    where stock_number = a.stock_number
      and the_date < current_date)

*/     

