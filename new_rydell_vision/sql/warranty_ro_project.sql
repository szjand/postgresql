﻿/*
Business Problem / Opportunity:
As we have discussed in the TOC training we have several opportunities throughout the dealership to 
reduce batch sizing and find more effective ways to produce results. 
Currently, our warranty administrator is processing claims for all three rooftops in the 
general office  i don't think that the work load is too much to manage - I think that the 
current process of collecting paperwork from the service department and processing claims 
from a random stack is contributing to aging claims and a stressful "bottleneck" in that position. 

Business Objective:
What I would like to do is re-create our current Vision page warranty list to pull 
warranty data in a blue/red/yellow/green/black work cue based on work complete date. 
The warranty admin would then use this work cue to know that she should be working on 
items from each location that are highlighted in black and red first. This would keep 
her from getting off task with all the "noise" that having piles and piles of paperwork 
in front of her and really focus on what should be reviewed daily. 

Project Description:
Vision page reformatted to pull diff data than it is currently doing... or maybe 
just another page that would pull data we would need to make this possible. 

-- junes warranty ro request

12/28/22 met with june
columns: account, ro, description, balance, ro close date, comments
tabs for stores
as real time as it can be

*/

-- i dont understand the warranty schedule, is that acct 126300 ?
drop table if exists t1;
create temp table t1 as
select c.the_date, a.control, a.doc, a.ref, d.description, e.journal, e.journal_code, a.amount
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
  and b.account in ('126300', '226300', '226301', '2200','2255')
join dds.dim_date c on a.date_key = c.date_key  
  and c.year_month = 202212
join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key  
join fin.dim_journal e on a.journal_key = e.journal_key
where a.post_status = 'Y';

select * from t1


-- look at it from the standpoint of a balance in 126300
-- this looks reasonable
select a.control, b.account, sum(a.amount), min(the_date), max(the_date)
-- select a.*
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
  and b.account = '126300'
join dds.dim_date c on a.date_key = c.date_key  
where a.post_status = 'Y'
--   and control = '15640032'
  and a.control not in ('15066289','15066298')
  and a.control not like '19%'
  and a.control not like '18007%'
group by a.control, b.account
having sum(a.amount) <> 0


select * 
from (
	select a.control, sum(a.amount)
	-- select a.*
	from fin.fact_gl a
	join fin.dim_account b on a.account_key = b.account_key
		and b.account = '126300'
	join dds.dim_date c on a.date_key = c.date_key  
	--   and c.year_month > 202210
	where a.post_status = 'Y'
	--   and control = '15640032'
-- 	  and c.the_date > '01/01/2022'
		and a.control not in ('15066289','15066298','16045777')
		and a.control not like '18007%'
		and a.control not like '19%'
	group by control
	having sum(a.amount) <> 0) aa
	left join (
		select a.ro, a.close_date
		from dds.fact_repair_order a  
		where a.open_date > '01/01/2022'
		group by a.ro, a.close_date) bb on aa.control = bb.ro
order by aa.control	

-- start with balance
-- multiple transactions -> muiltiple descriptions
select * from (
	select a.control, b.account, sum(a.amount), min(the_date) as min_date, max(the_date) as max_date, string_agg(distinct d.description, ' | ')
	from fin.fact_gl a
	join fin.dim_account b on a.account_key = b.account_key
		and b.account in ('126300', '226300', '226301', '2200','2255')
	join dds.dim_date c on a.date_key = c.date_key  
	left join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key 
	where a.post_status = 'Y'
	group by a.control, b.account
	having sum(a.amount) <> 0
	  and min(the_date) > '01/01/2022' -- this takes car of the old old stuff for which fact_gl is incomplete
) x order by min_date	  
	order by a.control


	select a.control, b.account, string_agg(distinct d.description, ' | ')
	from fin.fact_gl a
	join fin.dim_account b on a.account_key = b.account_key
		and b.account in ('126300', '226300', '226301', '2200','2255')
	join dds.dim_date c on a.date_key = c.date_key  
	join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key 
	where a.post_status = 'Y'
 and control = '16531493'
 group by a.control, b.account

select min(gtdate), max(gtdate) from arkona.ext_glptrns_tmp limit 10 

-- me and my big mouth, june wants as close to real time as possible
-- that means fact_gl
-- fuck me
-- so, let's just go with a fresh cut from arkona for just glptrns and whatever else we need

--12/29/22 changed schema from jun to nrv

create schema jun;
comment on schema jun is 'development schema for real time accounting, for the warranty ro schedule project for june';
drop schema jun cascade

drop table if exists nrv.ext_warranty_schedule cascade;
create table nrv.ext_warranty_schedule (
  store citext,
  ro citext,
  account citext,
  balance numeric,
  description citext,
  close_date date);
alter table nrv.ext_warranty_schedule
add primary key(ro);

comment on table nrv.ext_warranty_schedule is 'first cut at a table populated by an extract from dealertrack of warranty schedule data';

select * from jun.ext_warranty_schedule  

select * from fin.dim_Account where account = '126300'

select * from nrv.ext_warranty_schedule

insert into nrv.ext_Warranty_schedule
select * from jun.ext_warranty_schedule  

 select ro
 from nrv.ext_Warranty_schedule
 group by ro 
 having count(*) > 1 


-- old toyota stuff
	select a.control, b.account, a.amount, the_date, d.description -- sum(a.amount), min(the_date) as min_date, max(the_date) as max_date, string_agg(distinct d.description, ' | ')
	from fin.fact_gl a
	join fin.dim_account b on a.account_key = b.account_key
		and b.account in ('2200','2255')
	join dds.dim_date c on a.date_key = c.date_key  
	left join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key 
	where a.post_status = 'Y'
  and control not like '86%'
  order by control