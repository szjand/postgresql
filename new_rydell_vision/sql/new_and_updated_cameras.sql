﻿/*
8/6/19
Jon – 

I’ve attached to spreadsheets.  One is for new cameras added to the system.  The other is for existing cameras that have been renamed.

LMK if you have questions.

Thanks for your help!
SDS
*/

from the spreadsheet:
insert into nrv.camera_ips values('Cartiva_EntryHallway','10.133.196.247','AXIS M3047-P','NVR1','GM');
insert into nrv.camera_ips values('Cartiva_Lobby','10.133.196.246','AXIS M3047-P','NVR1','GM');
insert into nrv.camera_ips values('SVC_Rental_Desk','10.133.196.249','AXIS P3384-V','NVR1','GM');
insert into nrv.camera_ips values('Parts_Counter','10.133.196.248','AXIS M3047-P','NVR1','GM');
insert into nrv.camera_ips values('NPU_1','10.133.196.99','AXIS P1367','NVR2','GM');
insert into nrv.camera_ips values('NPU_6','10.133.196.94','AXIS P1367','NVR2','GM');
insert into nrv.camera_ips values('NPU_7','10.133.196.97','AXIS P1367','NVR2','GM');
insert into nrv.camera_ips values('NPU_8','10.133.196.96','AXIS P1367','NVR2','GM');
insert into nrv.camera_ips values('NPU_9','10.133.196.95','AXIS P1367','NVR2','GM');
insert into nrv.camera_ips values('BDC_Marketing','10.133.196.240','AXIS M3047-P','NVR1','GM');

update nrv.camera_ips set camera_name = 'NPU_2'  where ip_address = '10.133.196.151';
update nrv.camera_ips set camera_name = 'NPU_3'  where ip_address = '10.133.196.150';
update nrv.camera_ips set camera_name = 'NPU_4'  where ip_address = '10.133.196.181';
update nrv.camera_ips set camera_name = 'NPU_5'  where ip_address = '10.133.196.180';
update nrv.camera_ips set camera_name = 'CW_TUNNEL_WEST_1'  where ip_address = '10.133.196.152';
update nrv.camera_ips set camera_name = 'CW_TUNNEL_WEST_2'  where ip_address = '10.133.196.182';
