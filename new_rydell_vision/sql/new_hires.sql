﻿laura is doing a new spreadsheet for 2021, so, despite all its flaws (misspellings, etc), generate new tables for 2021 and keep doing this (putting her spreadsheet into a table)


drop table if exists nrv.new_hires_2021;
create table nrv.new_hires_2021 (
  full_name citext not null,
  first_name citext not null,
  last_name citext not null,
  birth_date date not null,
  social_security_number citext not null,
  gender citext not null,
  phone citext not null,
  address citext not null,
  drivers_license citext not null,
  dl_state citext not null,
  start_date date not null,
  pay_rate citext not null,
  status citext not null,
  job_title citext not null,
  department citext not null,
  manager_name citext not null,
  store citext not null,
  referral citext not null,
  signing_bonus citext not null,
  primary key (full_name,start_date));
comment on table nrv.new_hires_2021 is 'spreadsheet maintained by hr (laura roth) that is sent to payroll (kim miller) and dev (jon and afton),
  according to laura:Any time I hire someone.  I try to get it to Kim within one or 2 days of setting a new person up in the system';


drop table if exists nrv.ext_new_hires_2022;
create table nrv.ext_new_hires_2022 (
  full_name citext not null,
  birth_date date not null,
  social_security_number citext not null,
  gender citext not null,
  phone citext not null,
  address citext not null,
  drivers_license citext not null,
  dl_state citext not null,
  start_date date not null,
  pay_rate citext not null,
  status citext not null,
  job_title citext not null,
  department citext not null,
  manager_name citext not null,
  store citext not null,
  referral citext not null,
  signing_bonus citext not null);
comment on table nrv.ext_new_hires_2022 is  
$$
="insert into nrv.ext_new_hires_2022 values('"&A2&"','"&TEXT(B2,"mm/dd/yyyy")&"','"&C2&"','"&D2&"','"&E2&"','"&F2&"','"&G2&"','"&H2&"','"&TEXT(I2,"mm/dd/yyyy")&"','"&J2&"','"&K2&"','"&L2&"','"&M2&"','"&N2&"','"&O2&"','"&P2&"','"&Q2&"');"
$$;

select * 
-- delete
from nrv.ext_new_hires_2022;


insert into nrv.ext_new_hires_2022 values('Joshua Martinez','04/07/2000','532-45-7273','M','701-403-4680','633 Parkway Drive, Wahpeton ND 58075','','','01/03/2022','$14.00/hour','PT','Body Shop Intern','Body Shop','John Gardner','RY1','joshua.a.martinez@ndus.edu','');
insert into nrv.ext_new_hires_2022 values('Cameron Bridger','09/19/1993','753-73-8801','M','701-330-1358','2852B 22nd Ave S, Grand Forks ND 58201','BRI-93-3989','ND','01/10/2022','$7.50/hour + .2% commission','FT','Service Advisor','Service','Nick Neumann','RY1','cuttingtothecorepod@gmail.com','');
insert into nrv.ext_new_hires_2022 values('Dan Feickert','05/02/1955','501-64-8572','M','313-525-2138','4812 6th Ave N, Apt. 4, Grand Forks ND 58203','FEI-55-1924','ND','01/10/2022','$14.00/hour','PT','PDQ Cashier','PDQ','Tyler Hagen','RY1','danfeickert53@gmail.com','');
insert into nrv.ext_new_hires_2022 values('Michael Fonteyne','11/20/1998','603-11-2332','M','509-251-0466','750 43rd St N, Apt. 51, Grand Forks ND 58203','','','01/10/2022','$12.00/flat rate hour','PT','Detail Technician','Detail','Ethan Farley','RY1','mike.fonteyne@und.edu','');
insert into nrv.ext_new_hires_2022 values('Landon Paurus','07/18/2002','474-41-0800','M','612-424-2747','10592 Alison Way, Inver Grove Heights, MN 55077','Z016280139007','MN','01/12/2021','$14.00/hour','PT','Detail Technician','Detail','Ethan Farley','RY1','Landon.paurus@gmail.com','');
insert into nrv.ext_new_hires_2022 values('Jeff Johnson','03/05/1985','501-08-8698','M','701-213-8522','540 7th Ave SE, East Grand Forks MN 56721','JOH-85-4925','ND','01/24/2022','$16.00/hour','PT','Parts Shipping & Receiving','Parts','Dayton Marek','RY1','jeffersonj85@live.com','');
insert into nrv.ext_new_hires_2022 values('Danielle Kellett','02/27/1992','501-19-0846','F','701-739-0349','5305 Center Ave, Grand Forks ND 58203','KEL-92-1889','ND','01/24/2022','$16.00/hour','FT','Customer Care Service Specialist','Customer Care Center','Justin Kilmer','RY1','daniellekellett@hotmail.com','');
insert into nrv.ext_new_hires_2022 values('Katie Heath','07/04/2002','501-29-2167','F','701-721-6099','901 University Ave, Grand Forks ND 58201','HEA-02-7335','ND','01/24/2022','Sales Pay Plan','FT','Sales Consultant','Sales','Nick Shirek','RY1','ktheath1205@gmail.com','');
insert into nrv.ext_new_hires_2022 values('Steven Zmarzlik','12/09/1972','365-02-8186','M','701-202-7474','615 9th Ave S, Grand Forks ND 58201','ZMA-72-9019','ND','01/24/2022','$16.00/hour','FT','Parts Counterperson','Parts','Dayton Marek','RY1','fuddzmarzlik@gmail.com','');
insert into nrv.ext_new_hires_2022 values('Cayden Knutson','11/16/2004','474-45-7682','M','701-335-9593','19368 445th Ave SW, East Grand Forks MN 56721','R000032623000','MN','01/24/2022','$14.00/hour','PT','Car Wash Team Member','Car Wash','Brittany Haarstad','RY1','caydenknutson@gmail.com','Dane Swehla, Ben Dockendorf, Jacob McNeff');
insert into nrv.ext_new_hires_2022 values('Ashley Decker','10/11/1984','502-04-0513','F','701-330-1546','294 20th St NE, Reynolds ND 58275','DEC-84-8203','ND','01/26/2022','$6,750/month.  Starting in August she will go to $66K Salary per year plus commission','FT','Body Shop Manager','Body Shop','Randy Sattler','RY1','ashleydecker@fastmail.com','');
insert into nrv.ext_new_hires_2022 values('Carter Kaufman','09/05/2002','501-29-2602','M','701-934-0457','1608 Oakland Drive, Bismarck ND 58504','KAU-02-5088','ND','02/01/2022','$14.00/hour','PT','Car Wash Team Member','Car Wash','Brittany Haarstad','RY1','carterjkaufman@gmail.com','Cole Bradley');
insert into nrv.ext_new_hires_2022 values('Zachary Ayotte','07/21/2004','471-45-4473','M','218-779-4904','14030 410th Ave SW, East Grand Forks MN 56721','','','02/07/2022','$14.00/hour','PT','Car Wash Team Member','Car Wash','Brittany Haarstad','RY1','zachary.ayotte@mygreenwave.net','Michael Wisk');
insert into nrv.ext_new_hires_2022 values('Isaiah Fronning','02/01/1998','476-33-2990','M','218-282-0756','34196 170th St, Battle Lake MN 56515','G907065107816','MN','02/07/2022','$16.00/hour','PT','Parts Shipping & Receiving','Parts','Dayton Marek','RY1','isaiahfronning@gmail.com','');
insert into nrv.ext_new_hires_2022 values('Silvester Brown','08/16/1999','426-87-0370','M','662-336-5003','3500 30th Ave S, Apt. 202, Grand Forks ND 58201','802810667','MS','02/08/2022','$16.00/hour','PT','Parts Shipping & Receiving','Parts','Dayton Marek','RY1','silvesterbrown42@gmail.com','');
insert into nrv.ext_new_hires_2022 values('Derek Olsen','04/13/2003','470-43-4667','M','612-308-0494','328 Squires Hall, Grand Forks 58202','Z166233265813','MN','02/08/2022','$14.00/hour','PT','Car Wash Team Member','Car Wash','Brittant Haarstad','RY1','Deisel1352@gmail.com','Cole Bradley');
insert into nrv.ext_new_hires_2022 values('Colton Nelson','12/17/1989','520-25-2619','M','720-771-9581','1260 55th Ave S, Apt. 203, Grand Forks ND 58201','NEL-89-2142','ND','02/08/2022','$16.00/hour','FT','Customer Care Service Specialist','Customer Care Center','Justin Kilmer','RY1','nelso.cn24@gmail.com','');
insert into nrv.ext_new_hires_2022 values('Johann Meyer','04/05/2000','217-71-3193','M','541-870-7977','306 N 50th St, Grand Forks, ND 58203','MEY-00-1977','ND','02/08/2022','$16.00/hour','FT','Wholesales Parts Driver','Parts','Dayton Marek','RY1','jo.quad.meyer@gmail.com','Carter Stinson');
insert into nrv.ext_new_hires_2022 values('Madison Rodriguez','03/06/2001','501-27-9946','F','701-317-4123','2002 8th Ave NW, Apt. 5, East Grand Forks MN 56721','ROD-01-6697','ND','02/08/2022','$14.00/hour','FT','PDQ Technicain','Honda PDQ','Ryan Shroyer','RY2','madi.riguez@icloud.com','Kane Moon');



---------------------------------------------------------------------------------------------------
--/> 2020
---------------------------------------------------------------------------------------------------
*/