﻿/*
since we can no longer scrape nissan, we need to build a manual incentives page so that incentives can be incorporated
in pricing.
start off with current nissan inventory (from dealertrack, trim from chrome) 
table truncated and repopulated nightly in luigi
*/
select inpmast_stock_number, inpmast_vin, year, make, model, r->'attributes'->>'trim' as trim_level
from arkona.ext_inpmast a
left join chr.describe_vehicle b on a.inpmast_vin = b.vin
left join jsonb_array_elements(b.response->'style') as r(style) on true
where status = 'I'
  and type_n_u = 'N'
  and make = 'Nissan'


drop table if exists hn.nissan_current_new_inventory cascade;
create table hn.nissan_current_new_inventory (
  stock_number citext not null,
  vin citext not null,
  model_year integer not null,
  make citext not null,
  model citext not null,
  trim_level citext not null);
comment on table hn.nissan_current_new_inventory is 'updated nightly in luigi, current new nissan inventory, inventory defined as
existing in dealertrack';
create unique index on hn.nissan_current_new_inventory(stock_number);
create unique index on hn.nissan_current_new_inventory(vin);


insert into hn.nissan_current_new_inventory
select inpmast_stock_number, inpmast_vin, year, make, model, r->'attributes'->>'trim' as trim_level
from arkona.ext_inpmast a
left join chr.describe_vehicle b on a.inpmast_vin = b.vin
left join jsonb_array_elements(b.response->'style') as r(style) on true
where status = 'I'
  and type_n_u = 'N'
  and make = 'Nissan';

delete
-- select * 
from hn.nissan_current_new_inventory where vin = '5N1DR3CC9NC220656'    


select inpmast_vin
from arkona.ext_inpmast a
where status = 'I'
  and type_n_u = 'N'
  and make = 'Nissan'
  and not exists (
    select 1
    from chr.describe_vehicle
    where vin = a.inpmast_vin)


delete 
-- select * 
from chr.describe_vehicle where vin = '5N1DR3CC9NC220656'    