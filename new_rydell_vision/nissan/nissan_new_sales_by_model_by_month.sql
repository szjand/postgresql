﻿select a.stock_number, a.vin, a.delivery_date, a.capped_date, b.make, b.model, b.date_delivered
-- select a.*
from sls.ext_bopmast_partial a
left join arkona.ext_inpmast b on a.stock_number = b.inpmast_stock_number
  and b.make = 'NISSAN'
where a.store = 'ry2'
  and a.deal_Status = 'U'
  and a.delivery_date > '12/31/2015'
  and a.vehicle_type = 'N'


select a.bopmast_stock_number, /*a.vin, a.delivery_date, a.capped_date,*/ b.make, b.model, b.date_delivered
-- select a.*
from arkona.ext_bopmast a
left join arkona.ext_inpmast b on a.bopmast_stock_number = b.inpmast_stock_number
  and b.make = 'NISSAN'
where a.store = 'ry2'
  and a.deal_Status = 'U'
  and a.delivery_date > '12/31/2015'
  and a.vehicle_type = 'N'


select a.bopmast_stock_number, a.bopmast_vin, a.record_Status, a.sale_type, a.record_type, a.vehicle_type, a.sale_account, a.date_capped, a.delivery_Date, b.*
-- select a.*
from arkona.ext_bopmast a
left join fin.dim_Account b on a.sale_account = b.account
where a.bopmast_company_number = 'RY2'
  and a.vehicle_type = 'n'
  and a.delivery_date > '06/01/2017'
  and a.


select * from arkona.ext_eisglobal_sypffxmst  limit 100


select 
  case fxmlne::integer
    when 15 then 'versa'
    when 16 then 'sentra'
    when 16 then 'altima 4dr'
    when 17 then 'maxiam'
    when 19 then '370z coupe'
    when 27 then 'juke'
    when 31 then 'nv200'
    when 32 then 'armadda'
    when 33 then 'rogue'
    when 34 then 'murano'
    when 35 then 'frontier'
    when 36 then 'pathfinder'
    when 37 then 'quest'
    when 38 then 'xterra'
    when 39 then 'titan king'
    when 40 then 'titan crew'
  end as model
  end
from ( 
select year_month, store_code, control, the_date, sum(unit_count) as unit_count, g_l_acct_number, fxmpge, fxmlne
from ( -- h


select fxmlne::integer as fin_stmt_line, model,
  sum(case when year_month = 201601 then unit_count else 0 end) as "201601",
  sum(case when year_month = 201602 then unit_count else 0 end) as "201602",
  sum(case when year_month = 201603 then unit_count else 0 end) as "201603",
  sum(case when year_month = 201604 then unit_count else 0 end) as "201604",
  sum(case when year_month = 201605 then unit_count else 0 end) as "201605",
  sum(case when year_month = 201606 then unit_count else 0 end) as "201606",
  sum(case when year_month = 201607 then unit_count else 0 end) as "201607",
  sum(case when year_month = 201608 then unit_count else 0 end) as "201608",
  sum(case when year_month = 201609 then unit_count else 0 end) as "201609",
  sum(case when year_month = 201610 then unit_count else 0 end) as "201610",
  sum(case when year_month = 201611 then unit_count else 0 end) as "201611",
  sum(case when year_month = 201612 then unit_count else 0 end) as "201612",
  sum(case when year_month = 201701 then unit_count else 0 end) as "201701",
  sum(case when year_month = 201702 then unit_count else 0 end) as "201702",
  sum(case when year_month = 201703 then unit_count else 0 end) as "201703",
  sum(case when year_month = 201704 then unit_count else 0 end) as "201704",
  sum(case when year_month = 201705 then unit_count else 0 end) as "201705",
  sum(case when year_month = 201706 then unit_count else 0 end) as "201706",
  sum(case when year_month = 201707 then unit_count else 0 end) as "201707",
  sum(case when year_month = 201708 then unit_count else 0 end) as "201708",
  sum(case when year_month = 201709 then unit_count else 0 end) as "201709"
from (  
  select b.year_month, c.store_code, d.g_l_acct_number, 
    a.control, a.amount, fxmpge, fxmlne,
  case fxmlne::integer
    when 15 then 'versa'
    when 16 then 'sentra'
    when 17 then 'altima 4dr'
    when 18 then 'maxima'
    when 19 then '370z coupe'
    when 27 then 'juke'
    when 31 then 'nv200'
    when 32 then 'armada'
    when 33 then 'rogue'
    when 34 then 'murano'
    when 35 then 'frontier'
    when 36 then 'pathfinder'
    when 37 then 'quest'
    when 38 then 'xterra'
    when 39 then 'titan king'
    when 40 then 'titan crew'
  end as model,
    case when a.amount < 0 then 1 else -1 end as unit_count, b.the_date
  from fin.fact_gl a   
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201601 and 201709
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account_type = 'Sale'
  inner join (
    select distinct g_l_acct_number, fxmpge, fxmlne
    from arkona.ext_eisglobal_sypffxmst a
    inner join (
      select factory_financial_year, g_l_acct_number, factory_account, fact_account_
      from arkona.ext_ffpxrefdta) b on a.fxmcyy = b.factory_financial_year and a.fxmact = b.factory_account
    where a.fxmcyy = 2017
      and fxmpge = 14
      and fxmlne between 1 and 40) d on c.account = d.g_l_acct_number
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
    and aa.journal_code in ('VSN')   
  where a.post_status = 'Y') x
group by fxmlne, model  
order by fxmlne




