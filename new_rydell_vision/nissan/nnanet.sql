﻿-- Function: jeri.update_open_ros()

-- DROP FUNCTION jeri.update_open_ros();

CREATE OR REPLACE FUNCTION jeri.update_open_ros()
  RETURNS void AS
$BODY$
Begin

/*

select jeri.update_open_ros()
3/27/19: exclude ro 18066587, something weird, terry harmon as the writer
1/15/20
oh fuck, here we go, dealerfx writer
for the meantime, exclude this dealerfx ros

*/ 

-- current day's open ros
insert into jeri.open_ros
select a.run_date, a.store_code, a.ro, a.customer, c.writer_name, a.ro_status,
  b.service_type, b.payment_type, b.flag_hours, b.labor_gross, b.parts_gross, 
  a.days_open, a.days_in_cashier_status
from jeri.open_ro_headers a
inner join jeri.open_ro_details b on a.ro = b.ro
left join jeri.service_writers c on a.service_writer_id = c.writer_id
where a.ro not in ( '18066587','16363665','16367127','16367338','16367314','16379174',
  '16378333','2815896','2815906','2815882','16384004')
  -- for now, this handles dealerfx
  and a.ro not in ('16381026','16381203','16379278','16381879','16382014','16382032','16383750')
  and a.customer not like 'TEST%';


