﻿7/12/21
7/13/21 starting to feel like purging everything and starting fresh
well, nissan is fucked up again
greg decides to look at the new car crayon report (nc.get_crayon_report()), nissan shows 136 ish incoming vehicles
ry2 says nope, 36 is more like it

-- this is the base query in nc.get_crayon_report()
    select b.make, case when cab = 'n/a' then concat(b.model, ' ', drive) else concat(b.model, ' ',initcap(cab), ' ', 'Cab', ' ', drive) end as model, b.model_year, b.trim_level, null, a.vin, order_number, null, drive
    from hn.nna_vehicle_details a
    left join jon.configurations b on a.model_code = b.model_code
    where not exists (
      select 1
      from nc.vehicles
      where vin = a.vin) order by order_number

jon.configurations gets updated only when i run the chrome.py in pycharm, of course havent done that in over a month
thats part of the problem
	there is some obviously fucked up data in hn.nna_vehicle_details
	4 rows where model = cab, a phone vin and an order number and that is it ('SD65974','SD66035','SD66518','SD67360')

rerun VehicleInventoryDetailParse in nissan.py to populate tem.nissan_vins

select * from hn.nna_order_statuses where order_number = 'SD66035'


select * from hn.nna_Vehicle_details where order_number = 'sd66035'

select * from hn.ext_nna_vehicle_details 

select * from hn.stg_nna_vehicle_details order by order_number

select * from jon.configurations where model_code = '26212'

select * from nc.vehicle_configurations where model_code = '26212'




select b.*, c.stock_number, c.vin, d.*
from (
	select a.*,  row_number() over (partition by order_number order by from_date desc) as seq
	from hn.nna_order_statuses a
	where a.from_Date> '01/01/2021') b
left join hn.nna_Vehicle_details c on b.order_number = c.order_number	
left join nc.vehicles d on c.vin = d.vin
where b.seq = 1
  and b.location_status <> 'DLR-INV'




select * from jon.configurations where model = 'armada'
select * from hn.nna_vehicle_details where model_code = '26212'


select * from hn.nna_vehicle_details where model = 'altima' and location_status <> 'DLR-INV' order by location_status


select * from hn.stg_nna_vehicle_details where model = 'altima' and location_status <> 'DLR-INV' order by location_status



















12/4/20
ok, now that i believe i have the data processing fixed
trying to understand a bunch of "old" records in nna_vehiccle_details, dont understand why they are there
or what they signify




select * from hn.stg_nna_vehicle_details

select * from tem.nissan_vins

select * from hn.ext_nna_vehicle_details

select *
from tem.nissan_vins a
left join hn.stg_nna_vehicle_details b on a.vin = b.vin || '.csv'  or a.vin = b.order_number || '.csv'
where b.vin is null
order by a.vin

parses: XL57143.csv

dos not parse WI04662.csv

select *
from tem.nissan_vins a
left join hn.stg_nna_vehicle_details b on a.vin = b.vin || '.csv'  or a.vin = b.order_number || '.csv'
order by model_code

ONS=Ordered Not Serialized, SIT=Sea In Transit, NNA=NNA Inventory, DLR=Dealer Inventory, RTL= Retailed

select *
from tem.nissan_vins a
left join hn.nna_vehicle_details b on a.vin = b.vin || '.csv'  or a.vin = b.order_number || '.csv'
where b.vin is null
order by a.vin

select * 
from hn.nna_vehicle_details
where vehicle_status <> 'delivered'
  and model like 'rogue%'
order by model_code


select model, location_status, count(*)
from hn.nna_vehicle_details
where model like 'rogue%'
group by model, location_status
order by model

ROGUE;DLR-INV;40 - 27 = 13, dbs shows 5
ROGUE;NNA-INV;12 -- ok
ROGUE;ONS;46 - 28 = 28, dbs shows 24
ROGUE;SIT;5 == ok
ROGUE SPORT;DLR-INV;12
ROGUE SPORT;ONS;31
ROGUE SPORT;SIT;3 -- ok



select *
from hn.nna_Vehicle_details
where vehicle_status <> 'delivered'
  and model = 'rogue'
order by stock_number


select *
from hn.nna_vehicle_details
where vin = 'JN8AT3BB2MW205824'


select *
from hn.nna_vehicle_details
where model = 'rogue'
  and location_status = 'ons'
order by order_number  











ONS=Ordered Not Serialized, SIT=Sea In Transit, NNA=NNA Inventory, DLR=Dealer Inventory, RTL= Retailed


it appears that on those that do not parse, there is no MEMO Subtotal for BAP and instead the Dealer Invoice is in column 3 rather than column 1
which breaks the parsing, this is true for order number WI04662, need to determine if this is consistent in all non parsed vehicles
on a good one Dealer Invoice is line 20 col 1, on a bad one Dealer Invoice is line 19 col 3
and the bad ones have no options
bad ones have an invoice number, good ones dont, although it appears that the invoice number goes away as the 
anyway, what i need to determine, are there only 2 formats?
on the bad formats, it appears there is no relevant data after lin 15

-- insert into hn.stg_nna_vehicle_details
select *
from 
  (select 
    case
      when length(trim(split_part(field_1, ':', 2))) = 0 then (select (random()*100)::text)
      else trim(split_part(field_1, ':', 2))
    end as vin
  from hn.ext_nna_vehicle_details where position('VIN' in field_1) <> 0) a,   
  (select  trim(split_part(field_3, ':', 2)) as order_number from hn.ext_nna_vehicle_details where position('Order' in field_3) <> 0) b,
  (select  trim(split_part(field_1, ':', 2)) as stock_number from hn.ext_nna_vehicle_details where position('Stock' in field_1) <> 0) c,
  (select  trim(split_part(field_1, ':', 2)) as model from hn.ext_nna_vehicle_details where position('Model Line' in field_1) <> 0) d,
  (select  trim(split_part(field_1, ':', 2))::integer as model_year from hn.ext_nna_vehicle_details where position('Model Year' in field_1) <> 0) e,
  (select  trim(split_part(field_1, ':', 2)) as vehicle_status from hn.ext_nna_vehicle_details where position('Vehicle Status:' in field_1) <> 0) ee,
  (select  trim(split_part(field_1, ':', 2)) as exterior_color from hn.ext_nna_vehicle_details where position('Exterior' in field_1) <> 0) f,
  (select  trim(split_part(field_1, ':', 2)) as trim_level from hn.ext_nna_vehicle_details where position('Trim' in field_1) <> 0) g,
  (select  nullif(trim(split_part(field_1, ':', 2)), '')::date as wholesale_date from hn.ext_nna_vehicle_details where position('Wholesale Date' in field_1) <> 0) h,
  (select  trim(split_part(field_1, ':', 2)) as ctp_flag from hn.ext_nna_vehicle_details where position('CTP Flag' in field_1) <> 0) i,
  (select  trim(split_part(field_1, ':', 2)) as original_eta from hn.ext_nna_vehicle_details where position('Original ETA' in field_1) <> 0) j,
  (select  nullif(trim(split_part(field_1, ':', 2)), '')::date as ship_date from hn.ext_nna_vehicle_details where position('Ship Date' in field_1) <> 0) k,    
  (select  trim(split_part(field_1, ':', 2)) as key_info from hn.ext_nna_vehicle_details where position('Key Info' in field_1) <> 0) l,
  (select  trim(split_part(field_1, ':', 2)) as dealer_status from hn.ext_nna_vehicle_details where position('Dealer Status' in field_1) <> 0) m,
  coalesce((select  replace(trim(split_part(field_1, ':', 2)), '$', '')::integer as invoice from hn.ext_nna_vehicle_details where position('Dealer Invoice' in field_1) <> 0), null::integer) n,  
  coalesce((select trim(substring(field_1, position('$'in field_1) + 1))::integer as hold_back from hn.ext_nna_vehicle_details where position('HB' in field_1) <> 0 or position('HOLDBACK' in field_1) <> 0), null::integer) o, 
  (select  trim(split_part(field_3, ':', 2))::integer as dis from hn.ext_nna_vehicle_details where position('DIS' in field_3) <> 0) p,
  (select  trim(split_part(field_3, ':', 2)) as model_code from hn.ext_nna_vehicle_details where position('Model Code' in field_3) <> 0) q,
  (select  trim(split_part(field_3, ':', 2))::integer as production_month from hn.ext_nna_vehicle_details where position('Production Month' in field_3) <> 0) r,
  (select  trim(split_part(field_3, ':', 2)) as interior_color from hn.ext_nna_vehicle_details where position('Interior Color' in field_3) <> 0) s,
  (select 
    case -- msrp in field_3
      when exists (select 1 from hn.ext_nna_vehicle_details where position('$' in field_3) <> 0) then (
        select  replace(trim(split_part(field_3, ':', 2)), '$', '')::integer as msrp from hn.ext_nna_vehicle_details where position('MSRP' in field_3) <> 0)
    else
      (select null::integer)
    end) t,
  (select  trim(split_part(field_3, ':', 2)) as location_status from hn.ext_nna_vehicle_details where position('Location:' in field_3) <> 0) u,        
  (select  trim(split_part(field_3, ':', 2)) as current_eta from hn.ext_nna_vehicle_details where position('Current ETA:' in field_3) <> 0) v,
  coalesce((select  replace(trim(split_part(field_3, ':', 3)), '$', '')::integer as subtotal_for_bap from hn.ext_nna_vehicle_details where position('MEMO: Subtotal (for BAP):' in field_3) <> 0), null::integer) w,
  coalesce((select  nullif(replace(trim(split_part(field_3, ':', 2)), '$', ''), '')::integer as dealer_price from hn.ext_nna_vehicle_details where position('Dealer Price:' in field_3) <> 0), null::integer) x,
  coalesce((select  trim(split_part(field_3, ':', 2)) as invoice_number from hn.ext_nna_vehicle_details where position('Invoice Number:' in field_3) <> 0), null) y;




select *
from (
  select *, row_number() over ()
  from hn.ext_nna_Vehicle_details_1) a

  
select 
  case
    when row_number = 2 then 
      case
        when length(trim(split_part(field_1, ':', 2))) = 0 then (select (random()*100)::text)
        else trim(split_part(field_1, ':', 2))
      end
  end as vin    
from (
  select *, row_number() over ()
  from hn.ext_nna_Vehicle_details_1) a


 with the_rows as (
  select a.*, row_number() over ()
  from hn.ext_nna_vehicle_details_1 a)
select 
  (select  trim(split_part(field_3, ':', 2)) as order_number from hn.ext_nna_vehicle_details_1 where position('Order' in field_3) <> 0) ,
  field_1, left(field_2, 3), trim(substring(field_2, position(' ' in field_2) + 1))
from the_rows  
where row_number > (
  select row_number
  from the_rows
  where field_1 = 'Code')
  and field_1 is not null; 