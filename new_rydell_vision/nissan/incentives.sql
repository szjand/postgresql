﻿
---------------------------------------------------------------
-- 03/27/20 
Incentives

drop table if exists hn.nna_extract_incentives;
create table hn.nna_extract_incentives (
  vin citext not null,
  raw_incentives jsonb not null,
  the_date date not null default current_date);

select * from hn.nna_extract_incentives 

bad vin 3N1AB8CV1LY239140
psycopg2.errors.InvalidTextRepresentation: invalid input syntax for type json
LINE 3:                     values ('3N1AB8CV1LY239140','<ul class="...
                                                        ^
DETAIL:  Token "<" is invalid.
CONTEXT:  JSON data, line 1: <...'

<ul class="errorMessage"><li>This vehicle is not eligible for retail sales and retail incentives due to Inventory Status Code 5 - Assigned for Shipment</li></ul>

select *
from hn.nna_vehicle_details
where vin = '3N1AB8CV1LY239140'

all vins where location_status <> DLR-INV throw this error

-- use this query in the scrape
select vin  -- 66
from hn.nna_vehicle_details  
where vin not like 'DBS%'
  and location_status = 'DLR-INV'

think i need to just get the programs first
separate tables
-- vehicles -- vehicle_programs -- programs  
  
drop table if exists hn.nna_eligible_programs;
create table hn.nna_eligible_programs (
      vin citext not null,
      rule_ind citext,
      avg_amount integer,
      max_amount integer,
      min_amount integer,
      pgm_inc_type citext,
      source_type citext,
      highlighted citext,
      sales_end_date citext,
      cash_Incentive citext,
      lease_Incentive citext,
      sales_Begin_Date citext,
      sales_Type_Codes citext,
      customer_County_Key citext,
      incentive_Type_Code citext,
      incentive_Program_ID citext,
      incentive_Program_Key citext,
      inventory_Payout_Date citext,
      special_APR_Incentive citext,
      incentive_Program_Desc citext,
      incentive_Program_Name citext,
      standard_APR_Incentive citext,
      incentive_Program_End_Date citext,
      incentive_Program_Applicability citext);

select a.vin, b.*
from hn.nna_extract_incentives a
join jsonb_array_elements(a.raw_incentives->'eligiblePrograms') as b(programs) on true

-- get some vins
      select * -- 60
      from nc.vehicle_Acquisitions a
      join nc.vehicles b on a.vin = b.vin
        and b.make = 'nissan'
      where thru_Date > current_date  

      select inpmast_stock_number, inpmast_vin -- 66
      from arkona.xfm_inpmast
      where make = 'nissan'
        and type_n_u = 'N'
        and status = 'I'
        and current_row

      select vin, stock_number, location_status, vehicle_status -- 98
      from hn.nna_vehicle_details 
      order by location_status, vehicle_status

      select *  -- 88
      from hn.nna_vehicle_details  
      where vin not like 'DBS%'
      order by location_status, vehicle_status

      select location_status, vehicle_status, count(*)
      from hn.nna_vehicle_details  
      group by location_status, vehicle_status
      order by location_status, vehicle_status

insert into hn.nna_eligible_programs
select a.vin, 
      b.programs ->> 'ruleInd',                                                               
      b.programs ->> 'avgAmount',                                                                   
      b.programs ->> 'maxAmount',                                                                   
      b.programs ->> 'minAmount',                                                                   
      b.programs ->> 'pgmIncType',                                                                  
      b.programs ->> 'sourceType',                                                                  
      b.programs ->> 'highlighted',                                                                 
      b.programs ->> 'salesEndDate',                                                                
      b.programs ->> 'cashIncentive',                                                               
      b.programs ->> 'leaseIncentive',                                                                
      b.programs ->> 'salesBeginDate',                                                                   
      b.programs ->> 'salesTypeCodes',                                                                   
      b.programs ->> 'customerCountyKey',                                                                  
      b.programs ->> 'incentiveTypeCode',                                                                   
      b.programs ->> 'incentiveProgramID',                                                                  
      b.programs ->> 'incentiveProgramKey',                                                                
      b.programs ->> 'inventoryPayoutDate',                                                                 
      b.programs ->> 'specialAPRIncentive',                                                                  
      b.programs ->> 'incentiveProgramDesc',                                                                   
      b.programs ->> 'incentiveProgramName',                                                                   
      b.programs ->> 'standardAPRIncentive',                                                                  
      b.programs ->> 'incentiveProgramEndDate',                                                                  
      b.programs ->> 'incentiveProgramApplicability'  
from hn.nna_extract_incentives a
join jsonb_array_elements(a.raw_incentives->'eligiblePrograms') as b(programs) on true;

select * from hn.nna_eligible_programs
order by incentive_program_id


select string_agg(column_name,',')
from information_schema.columns
where table_name = 'nna_eligible_programs' -- enter table name here
  and table_schema= 'hn'

select incentive_program_id, md5(a::text), count(*)
from (
  select rule_ind,avg_amount,max_amount,min_amount,pgm_inc_type,source_type,highlighted,sales_end_date,
    cash_incentive,lease_incentive,sales_begin_date,sales_type_codes,customer_county_key,
    incentive_type_code,incentive_program_id,incentive_program_key,inventory_payout_date,
    special_apr_incentive,incentive_program_desc,incentive_program_name,standard_apr_incentive,
    incentive_program_end_date,incentive_program_applicability  
  from hn.nna_eligible_programs) a 
group by incentive_program_id, md5(a::text)  
order by incentive_program_id

select b.make, b.model, a.vin, a.sales_begin_date, a.lease_incentive
from hn.nna_eligible_programs a
left join nc.vehicles b on a.vin = b.vin
where a.incentive_program_id = '19N2299RTP'
order by a.vin



-- it appears that the incentive_program_id only varies in amounts based on model code
select b.make, b.model, b.model_code, a.* 
from hn.nna_eligible_programs a
left join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
where a.incentive_program_id = '19N2299RTP'
order by a.vin

-- only one vin not in chr.describe_vehicle
select distinct a.vin
from hn.nna_eligible_programs a
left join chr.describe_vehicle b on a.vin = b.vin
where b.vin is null 

-- and all are in inpmast
select distinct a.vin
from hn.nna_eligible_programs a
left join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
where b.inpmast_vin is null 

-- of course, they are all in nna_vehicle_details
select distinct a.vin
from hn.nna_eligible_programs a
left join hn.nna_vehicle_details b on a.vin = b.vin
where b.vin is null 



select count(*),
  (r.style ->'attributes'->>'mfrModelCode')::citext as model_code,
  (r.style ->'model'->>'$value'::citext)::citext as model, 
  a.incentive_program_id, a.incentive_program_name, a.incentive_program_key, a.sales_type_codes,
  a.cash_incentive, a.lease_incentive, a.special_apr_incentive, a.standard_apr_incentive,
  a.sales_begin_date, a.sales_end_date, a.incentive_program_end_date,
  a.incentive_program_applicability
from hn.nna_eligible_programs a
join chr.describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'style') as r(style) on true
group by   (r.style ->'attributes'->>'mfrModelCode')::citext,
  (r.style ->'model'->>'$value'::citext)::citext, 
  a.incentive_program_id, a.incentive_program_name, a.incentive_program_key, a.sales_type_codes,
  a.cash_incentive, a.lease_incentive, a.special_apr_incentive, a.standard_apr_incentive,
  a.sales_begin_date, a.sales_end_date, a.incentive_program_end_date,
  a.incentive_program_applicability
order by incentive_program_id, model

-- verify that the only variablity in elible programs is dollar amount

select incentive_program_id, md5(a::text), count(*)
from (
  select rule_ind,avg_amount,max_amount,min_amount,pgm_inc_type,source_type,highlighted,sales_end_date,
    cash_incentive,lease_incentive,sales_begin_date,sales_type_codes,customer_county_key,
    incentive_type_code,incentive_program_id,incentive_program_key,inventory_payout_date,
    special_apr_incentive,incentive_program_desc,incentive_program_name,standard_apr_incentive,
    incentive_program_end_date,incentive_program_applicability  
  from hn.nna_eligible_programs) a 
group by incentive_program_id, md5(a::text)  
order by incentive_program_id


select string_agg(column_name,',')
from information_schema.columns
where table_name = 'nna_eligible_programs' -- enter table name here
  and table_schema= 'hn'

-- the only dup is 19N2299RTH, goofy sales_end_date
-- and today, 03/30, that end_Date has been fixed
select incentive_program_id
from (
  select 
  --   vin,
    rule_ind,avg_amount,max_amount,min_amount,pgm_inc_type,source_type,highlighted,
    sales_end_date,
  --   cash_incentive,lease_incentive,
    sales_begin_date,sales_type_codes,
    customer_county_key,incentive_type_code,incentive_program_id,incentive_program_key,
    inventory_payout_date,
  --   special_apr_incentive,
    incentive_program_desc,incentive_program_name,
  --   standard_apr_incentive,
    incentive_program_end_date,incentive_program_applicability  
  from hn.nna_eligible_programs 
  group by   rule_ind,avg_amount,max_amount,min_amount,pgm_inc_type,source_type,highlighted,
    sales_end_date,
  --   cash_incentive,lease_incentive,
    sales_begin_date,sales_type_codes,
    customer_county_key,incentive_type_code,incentive_program_id,incentive_program_key,
    inventory_payout_date,
  --   special_apr_incentive,
    incentive_program_desc,incentive_program_name,
  --   standard_apr_incentive,
    incentive_program_end_date,incentive_program_applicability) a  
group by incentive_program_id
having count(*) > 1


select b.*, md5(b::text)
from (
  select   rule_ind,avg_amount,max_amount,min_amount,pgm_inc_type,source_type,highlighted,
    sales_end_date,
  --   cash_incentive,lease_incentive,
    sales_begin_date,sales_type_codes,
    customer_county_key,incentive_type_code,incentive_program_id,incentive_program_key,
    inventory_payout_date,
  --   special_apr_incentive,
    incentive_program_desc,incentive_program_name,
  --   standard_apr_incentive,
    incentive_program_end_date,incentive_program_applicability
  from hn.nna_eligible_programs a
  where incentive_program_id = '19N2299RTH') b


select *
from hn.nna_eligible_programs a
where incentive_program_id = '19N2299RTH'
order by sales_end_date


select *
from hn.nna_eligible_programs a
where vin = '1N6AA1EDXLN506880'


select a.vin, 
      b.programs ->> 'ruleInd',                                                               
      b.programs ->> 'avgAmount',                                                                   
      b.programs ->> 'maxAmount',                                                                   
      b.programs ->> 'minAmount',                                                                   
      b.programs ->> 'pgmIncType',                                                                  
      b.programs ->> 'sourceType',                                                                  
      b.programs ->> 'highlighted',                                                                 
      b.programs ->> 'salesEndDate',                                                                
      b.programs ->> 'cashIncentive',                                                               
      b.programs ->> 'leaseIncentive',                                                                
      b.programs ->> 'salesBeginDate',                                                                   
      b.programs ->> 'salesTypeCodes',                                                                   
      b.programs ->> 'customerCountyKey',                                                                  
      b.programs ->> 'incentiveTypeCode',                                                                   
      b.programs ->> 'incentiveProgramID',                                                                  
      b.programs ->> 'incentiveProgramKey',                                                                
      b.programs ->> 'inventoryPayoutDate',                                                                 
      b.programs ->> 'specialAPRIncentive',                                                                  
      b.programs ->> 'incentiveProgramDesc',                                                                   
      b.programs ->> 'incentiveProgramName',                                                                   
      b.programs ->> 'standardAPRIncentive',                                                                  
      b.programs ->> 'incentiveProgramEndDate',                                                                  
      b.programs ->> 'incentiveProgramApplicability'  
from hn.nna_extract_incentives a
join jsonb_array_elements(a.raw_incentives->'eligiblePrograms') as b(programs) on true
where vin = '1N6AA1EDXLN506880'


so, need a programs table with begin and end dates, looks like program_id as PK
a vehicle program tables with amounts, from/thru dates ??

-- 03/30, populated hn.nna_extract_incentives, but have not updated hn.nna_eligible_programs
-- what is the diff
drop table if exists ext;
create temp table ext as
select a.vin, 
      b.programs ->> 'ruleInd' as ruleInd,                                                               
      b.programs ->> 'avgAmount' as avgAmount,                                                                   
      b.programs ->> 'maxAmount' as maxAmount,                                                                   
      b.programs ->> 'minAmount' as minAmount,                                                                   
      b.programs ->> 'pgmIncType' as pgmIncType,                                                                  
      b.programs ->> 'sourceType' as sourceType,                                                                  
      b.programs ->> 'highlighted' as highlighted,                                                                 
      b.programs ->> 'salesEndDate' as salesEndDate,                                                                
      b.programs ->> 'cashIncentive' as cashIncentive,                                                               
      b.programs ->> 'leaseIncentive' as leaseIncentive,                                                                
      b.programs ->> 'salesBeginDate' as salesBeginDate,                                                                   
      b.programs ->> 'salesTypeCodes' as salesTypeCodes,                                                                   
      b.programs ->> 'customerCountyKey' as customerCountyKey,                                                                  
      b.programs ->> 'incentiveTypeCode' as incentiveTypeCode,                                                                   
      b.programs ->> 'incentiveProgramID' as incentiveProgramID,                                                                  
      b.programs ->> 'incentiveProgramKey' as incentiveProgramKey,                                                                
      b.programs ->> 'inventoryPayoutDate' as inventoryPayoutDate,                                                                 
      b.programs ->> 'specialAPRIncentive' as specialAPRIncentive,                                                                  
      b.programs ->> 'incentiveProgramDesc' as incentiveProgramDesc,                                                                   
      b.programs ->> 'incentiveProgramName' as incentiveProgramName,                                                                   
      b.programs ->> 'standardAPRIncentive' as standardAPRIncentive,                                                                  
      b.programs ->> 'incentiveProgramEndDate' as incentiveProgramEndDate,                                                                  
      b.programs ->> 'incentiveProgramApplicability'as incentiveProgramApplicability
from hn.nna_extract_incentives a
join jsonb_array_elements(a.raw_incentives->'eligiblePrograms') as b(programs) on true

select * from hn.nna_eligible_programs where vin = '5N1AT2MV2LC780711'

select * from hn.nna_eligible_programs where incentive_program_id = '20N2299RAI'

select *
from (
  select distinct vin, incentive_program_id
  from hn.nna_eligible_programs) a
full outer join (
  select distinct vin, incentiveProgramID
  from ext) b on a.vin = b.vin and a.incentive_program_id = b.incentiveProgramID

----------------------------------------
-- 04/01
lets build some tables and get done with this

does the applicablility of a particular program vary with the vehicle to which it is applied?

select * from hn.nna_extract_incentives


select * from hn.nna_eligible_programs 

      vin citext not null,
      rule_ind citext,
      avg_amount integer,
      max_amount integer,
      min_amount integer,
      pgm_inc_type citext,
      source_type citext,
      highlighted citext,
      sales_end_date citext,
      cash_Incentive citext,
      lease_Incentive citext,
      sales_Begin_Date citext,
      sales_Type_Codes citext,
      customer_County_Key citext,
      incentive_Type_Code citext,
      incentive_Program_ID citext,
      incentive_Program_Key citext,
      inventory_Payout_Date citext,
      special_APR_Incentive citext,
      incentive_Program_Desc citext,
      incentive_Program_Name citext,
      standard_APR_Incentive citext,
      incentive_Program_End_Date citext,
      incentive_Program_Applicability citext);

-- this looks like the relevant fields for programs
drop table if exists hn.nna_incentives (
  rule_ind citext,
  pgm_inc_type citext,
  source_type citext,
  highlighted citext,
  sales_end_date citext,
  sales_begin_Date citext,
  sales_type_codes citext,
  incentive_Type_Code citext,
  incentive_Program_ID citext,
  incentive_Program_Key citext,
  incentive_Program_Name citext,
  incentive_Program_End_Date citext,
  incentive_Program_Applicability citext)  

  select rule_ind,pgm_inc_type,highlighted,sales_end_date,
    sales_begin_date,sales_type_codes,
    incentive_type_code,incentive_program_id,incentive_program_key,
    incentive_program_name,
    incentive_program_end_date,incentive_program_applicability  
from hn.nna_eligible_programs 
where incentive_program_id = '19N2299RTH'
group by rule_ind,pgm_inc_type,highlighted,sales_end_date,
    sales_begin_date,sales_type_codes,
    incentive_type_code,incentive_program_id,incentive_program_key,
    incentive_program_name,
    incentive_program_end_date,incentive_program_applicability

    
and the fields for vehicle_programs

      vin citext not null,

      avg_amount integer,
      max_amount integer,
      min_amount integer,
      cash_Incentive citext,
      lease_Incentive citext,
      special_APR_Incentive citext,
      standard_APR_Incentive citext,





select * from hn.nna_eligible_programs 

 what is pgm_inc_type does it change within program

drop table if exists program_id;
create temp table program_id as
select incentive_program_id  -- 17
from hn.nna_eligible_programs  
group by incentive_program_id

select incentive_program_id, incentive_Program_Key  -- 17
from hn.nna_eligible_programs  
group by incentive_program_id,incentive_Program_Key


select *
from hn.nna_extract_incentives

                select vin  
                from hn.nna_vehicle_details  
                where vin not like 'DBS%'
                  and location_status = 'DLR-INV';

---------------------------------------------------------------------------
-- 04/02
remove truncate from nissan_incentive_rough.py, process based on date field

select the_date, count(*)
from hn.nna_extract_incentives
group by the_date

select * from hn.nna_eligible_programs 

originally, populated hn.nna_eligible_programs  with data from hn.nna_extract_incentives
now, instead, maintain the historical scrapes in hn.nna_extract_incentives (raw json w/vin & date)
and populate the nna_incentives table and the nna_vehicle_incentives table

-- this looks like the relevant fields for programs
drop table if exists hn.nna_incentives;
create table hn.nna_incentives (
  rule_ind citext,
  pgm_inc_type citext,
  source_type citext,
  highlighted citext,
  sales_end_date date,
  sales_begin_date date,
  sales_type_codes citext,
  incentive_type_code citext,
  incentive_program_id citext primary key,
  incentive_program_key citext,
  incentive_program_name citext not null,
  incentive_program_end_date date not null,
  incentive_program_applicability citext not null);  
comment on column hn.nna_incentives.sales_type_codes is 'description of codes is in table hn.nna_sale_types'; 

  select rule_ind,pgm_inc_type,highlighted,sales_end_date,
    sales_begin_date,sales_type_codes,
    incentive_type_code,incentive_program_id,incentive_program_key,
    incentive_program_name,
    incentive_program_end_date,incentive_program_applicability  
from hn.nna_eligible_programs 
where incentive_program_id = '19N2299RTH'
group by rule_ind,pgm_inc_type,highlighted,sales_end_date,
    sales_begin_date,sales_type_codes,
    incentive_type_code,incentive_program_id,incentive_program_key,
    incentive_program_name,
    incentive_program_end_date,incentive_program_applicability

    
-- and the fields for vehicle_programs
-- amount fields had to be text, may include a range, eg, 0 - 1000
drop table if exists hn.nna_vehicle_incentives;
create table hn.nna_vehicle_incentives(
  vin citext not null references hn.nna_vehicle_details(vin),
  incentive_program_id citext not null references hn.nna_incentives(incentive_program_id),
  avg_amount citext,
  max_amount citext,
  min_amount citext,
  cash_incentive citext,
  lease_incentive citext,
  special_apr_incentive citext,
  standard_apr_incentive citext,
  primary key(vin,incentive_program_id));
comment on column hn.nna_vehicle_incentives.avg_amount is 'numeric fields had to be text as they may contain a range, eg 1 - 1000';  

-- first load only from hn.nna_eligible_programs 
insert into hn.nna_incentives(rule_ind,pgm_inc_type,source_type,highlighted,
  sales_end_date,sales_begin_date,sales_type_codes,incentive_type_code,
  incentive_program_id,incentive_program_key,incentive_program_name,
  incentive_program_end_date,incentive_program_applicability)
select distinct rule_ind,pgm_inc_type,source_type,highlighted,
  sales_end_date::date,sales_begin_date::Date,sales_type_codes,incentive_type_code,
  incentive_program_id,incentive_program_key,incentive_program_name,
  incentive_program_end_date::date,incentive_program_applicability
from hn.nna_eligible_programs     
where sales_end_date <> '03/03/2020';

insert into hn.nna_vehicle_incentives (vin,incentive_program_id,avg_amount,
  max_amount,min_amount,cash_incentive,lease_incentive,special_apr_incentive,
  standard_apr_incentive, from_date)
select distinct vin,incentive_program_id,
  nullif(trim(replace(replace(avg_amount, '$', ''), ',','')), ''),
  nullif(trim(replace(replace(max_amount, '$', ''), ',','')), ''),
  nullif(trim(replace(replace(min_amount, '$', ''), ',','')), ''),
  nullif(trim(replace(replace(cash_incentive, '$', ''), ',','')), ''),
  nullif(trim(replace(replace(lease_incentive, '$', ''), ',','')), ''),
  nullif(trim(replace(replace(special_apr_incentive, '$', ''), ',','')), ''),
  nullif(trim(replace(replace(standard_apr_incentive, '$', ''), ',','')), ''),
  '03/28/2020'::date
from hn.nna_eligible_programs     
where sales_end_date <> '03/03/2020';

ok, now on to real time processing

select distinct b.programs ->> 'ruleInd',
  b.programs ->> 'pgmIncType',
  b.programs ->> 'sourceType',
  b.programs ->> 'highlighted',
  b.programs ->> 'salesEndDate',
  b.programs ->> 'salesBeginDate',
  b.programs ->> 'salestypecodes',
  b.programs ->> 'incentiveTypeCode',
  b.programs ->> 'incentiveProgramID',
  b.programs ->> 'incentiveProgramKey',
  b.programs ->> 'incentiveProgramName',
  b.programs ->> 'incentiveProgramEndDate',
  b.programs ->> 'incentiveProgramApplicability' 
from hn.nna_extract_incentives a
join jsonb_array_elements(a.raw_incentives->'eligiblePrograms') as b(programs) on true
where the_date = '03/30/2020'

select a.vin, 
  b.programs ->> 'incentiveProgramID',
  b.programs ->> 'avgAmount',                                                                   
  b.programs ->> 'maxAmount',                                                               
  b.programs ->> 'minAmount',    
  b.programs ->> 'cashIncentive',                                                               
  b.programs ->> 'leaseIncentive', 
  b.programs ->> 'specialAPRIncentive',  
  b.programs ->> 'standardAPRIncentive'        
from hn.nna_extract_incentives a
join jsonb_array_elements(a.raw_incentives->'eligiblePrograms') as b(programs) on true
where the_date = '03/30/2020'

select *
from (
  select a.*, md5(a::text) as hash
  from hn.nna_incentives a) aa
join (
select b.*, md5(b::text) as hash
from (
  select distinct b.programs ->> 'ruleInd',
    b.programs ->> 'pgmIncType',
    b.programs ->> 'sourceType',
    b.programs ->> 'highlighted',
    b.programs ->> 'salesEndDate',
    b.programs ->> 'salesBeginDate',
    b.programs ->> 'salestypecodes',
    b.programs ->> 'incentiveTypeCode',
    b.programs ->> 'incentiveProgramID' as incentive_program_id,
    b.programs ->> 'incentiveProgramKey',
    b.programs ->> 'incentiveProgramName',
    b.programs ->> 'incentiveProgramEndDate',
    b.programs ->> 'incentiveProgramApplicability' 
  from hn.nna_extract_incentives a
  join jsonb_array_elements(a.raw_incentives->'eligiblePrograms') as b(programs) on true
  where the_date = '03/30/2020') b) bb on aa.incentive_program_id = bb.incentive_program_id and aa.hash <> bb.hash




select *
from hn.nna_vehicle_incentives

-- hn.nna_incentives
-- upsert: insert andd update in place

insert into hn.nna_incentives
select distinct b.programs ->> 'ruleInd',
  b.programs ->> 'pgmIncType',
  b.programs ->> 'sourceType',
  b.programs ->> 'highlighted',
  (b.programs ->> 'salesEndDate')::date,
  (b.programs ->> 'salesBeginDate')::date,
  b.programs ->> 'salestypecodes',
  b.programs ->> 'incentiveTypeCode',
  b.programs ->> 'incentiveProgramID', 
  b.programs ->> 'incentiveProgramKey',
  b.programs ->> 'incentiveProgramName',
  (b.programs ->> 'incentiveProgramEndDate')::date,
  b.programs ->> 'incentiveProgramApplicability' 
from hn.nna_extract_incentives a
join jsonb_array_elements(a.raw_incentives->'eligiblePrograms') as b(programs) on true
where the_date = current_date
on conflict(incentive_program_id)
do update 
  set (
    rule_ind,pgm_inc_type,source_type,highlighted,sales_end_date,sales_begin_date,
    sales_type_codes,incentive_type_code,incentive_program_key,
    incentive_program_name,incentive_program_end_date,incentive_program_applicability)
  = (
    excluded.rule_ind,excluded.pgm_inc_type,excluded.source_type,excluded.highlighted,
    excluded.sales_end_date,excluded.sales_begin_date,excluded.sales_type_codes,
    excluded.incentive_type_code,excluded.incentive_program_key,
    excluded.incentive_program_name,excluded.incentive_program_end_date,
    excluded.incentive_program_applicability);

-- insert into hn.nna_vehicle_incentives (vin,incentive_program_id,avg_amount,
--   max_amount,min_amount,cash_incentive,lease_incentive,special_apr_incentive,
--   standard_apr_incentive)

select distinct vin,
  b.programs ->> 'incentiveProgramID' as incentive_program_id,
  nullif(trim(replace(replace(b.programs ->> 'avgAmount', '$', ''), ',','')), ''),
  nullif(trim(replace(replace(b.programs ->> 'maxAmount', '$', ''), ',','')), ''),
  nullif(trim(replace(replace(b.programs ->> 'minAmount', '$', ''), ',','')), ''),
  nullif(trim(replace(replace(b.programs ->> 'cashIncentive', '$', ''), ',','')), ''),
  nullif(trim(replace(replace(b.programs ->> 'leaseIncentive', '$', ''), ',','')), ''),
  nullif(trim(replace(replace(b.programs ->> 'specialAPRIncentive', '$', ''), ',','')), ''),
  nullif(trim(replace(replace(b.programs ->> 'standardAPRIncentive', '$', ''), ',','')), '')
from hn.nna_extract_incentives a
join jsonb_array_elements(a.raw_incentives->'eligiblePrograms') as b(programs) on true
where the_date = current_date


  
select string_agg(column_name,',')
from information_schema.columns
where table_name = 'nna_incentives' -- enter table name here
  and table_schema= 'hn'

select string_agg('excluded.' || column_name,',')
from information_schema.columns
where table_name = 'nna_incentives' -- enter table name here
  and table_schema= 'hn'  

 allright, need to think this out 
 cant just update in place, need to know, that on this date, these incentives apply to this vehicle
 each month, it appears, the incentive id changes, so a particular vin with a Bonus Cash 
 incentive for march, in april will have a different inceentive id for Bonus Cash
 no
 fuckit
 keep it simple
 vehicle_incentives needs no history, just current, it is a fucking truncate and fill


select * from hn.nna_extract_incentives where the_date = current_date 

select a.*
from hn.nna_order_Statuses a
-- where order_number in (
-- select ordeR_number
-- from hn.nna_order_statuses
-- where from_date > '03/31/2020') 
order by a.order_number, a.from_date


select *
from hn.nna_Vehicle_details
where order_number = 'NH68408'


select *
from hn.nna_Vehicle_details
where location_status = 'ons'

select location_status, count(*)
from hn.nna_vehicle_details
group by location_status


----------------------------------------------------------------
-- 043/30
----------------------------------------------------------------

drop table if exists hn.ext_nna_incentives;
create table hn.ext_nna_incentives (
  vin citext not null,
  raw_incentives jsonb not null,
  the_date date not null default current_date);
comment on table hn.ext_nna_incentives is 'repository for raw incentive data scraped from nnanet for vins in hn.nna_vehicle_details, 
  where vin not like "DBS%" and location_status = DLR-INV';
alter table hn.ext_nna_incentives add primary key(vin,the_date);

drop table if exists hn.nna_incentives;
create table hn.nna_incentives (
  rule_ind citext,
  pgm_inc_type citext,
  source_type citext,
  highlighted citext,
  sales_end_date date,
  sales_begin_date date,
  sales_type_codes citext,
  incentive_type_code citext,
  incentive_program_id citext primary key,
  incentive_program_key citext,
  incentive_program_name citext not null,
  incentive_program_end_date date not null,
  incentive_program_applicability citext not null);  
comment on column hn.nna_incentives.sales_type_codes is 'description of codes is in table hn.nna_sale_types'; 
comment on table hn.nna_incentives is 'key elements of each incentive, parsed from hn.ext_nna_incentives, no history maintained,
  updated in place daily';

drop table if exists hn.nna_vehicle_incentives;
create table hn.nna_vehicle_incentives(
  vin citext not null references hn.nna_vehicle_details(vin),
  incentive_program_id citext not null references hn.nna_incentives(incentive_program_id),
  avg_amount citext,
  max_amount citext,
  min_amount citext,
  cash_incentive citext,
  lease_incentive citext,
  special_apr_incentive citext,
  standard_apr_incentive citext,
  primary key(vin,incentive_program_id));
comment on column hn.nna_vehicle_incentives.avg_amount is 'numeric fields had to be text as they may contain a range, eg 1 - 1000';  
comment on table hn.nna_vehicle_incentives is 'all incentives applicable to each vin currently in hn.nna_vehicle_details,
  no history maintained, truncated and repopulated daily, includes the dollar amounts for each incentive';

CREATE OR REPLACE FUNCTION hn.update_nna_incentives()
  RETURNS void AS
$BODY$
/*
select hn.update_nna_incentives()
*/
insert into hn.nna_incentives
select distinct b.programs ->> 'ruleInd',
  b.programs ->> 'pgmIncType',
  b.programs ->> 'sourceType',
  b.programs ->> 'highlighted',
  (b.programs ->> 'salesEndDate')::date,
  (b.programs ->> 'salesBeginDate')::date,
  b.programs ->> 'salestypecodes',
  b.programs ->> 'incentiveTypeCode',
  b.programs ->> 'incentiveProgramID', 
  b.programs ->> 'incentiveProgramKey',
  b.programs ->> 'incentiveProgramName',
  (b.programs ->> 'incentiveProgramEndDate')::date,
  b.programs ->> 'incentiveProgramApplicability' 
from hn.ext_nna_incentives a
join jsonb_array_elements(a.raw_incentives->'eligiblePrograms') as b(programs) on true
where the_date = current_date
on conflict(incentive_program_id)
do update 
  set (
    rule_ind,pgm_inc_type,source_type,highlighted,sales_end_date,sales_begin_date,
    sales_type_codes,incentive_type_code,incentive_program_key,
    incentive_program_name,incentive_program_end_date,incentive_program_applicability)
  = (
    excluded.rule_ind,excluded.pgm_inc_type,excluded.source_type,excluded.highlighted,
    excluded.sales_end_date,excluded.sales_begin_date,excluded.sales_type_codes,
    excluded.incentive_type_code,excluded.incentive_program_key,
    excluded.incentive_program_name,excluded.incentive_program_end_date,
    excluded.incentive_program_applicability);
$BODY$
LANGUAGE sql; 
comment on FUNCTION hn.update_nna_incentives() is 'extracts program data from hn.nna_incentives.raw_incentives, inserts
  new rows and updates existing rows in hn.nna_incentives';
  
create temp table wtf as select * from hn.nna_incentives

all 3 new rows today have goofy begin/end dates
20N2299RRM/20N2299RRK goofy begin/end commercial stairstep
20N2299RRH memo

select *
from (
select a.*, md5(a::text)
from hn.nna_incentives a) aa
full outer join (
select a.*, md5(a::text)
from wtf a) bb on aa.incentive_program_id = bb.incentive_program_id


CREATE OR REPLACE FUNCTION hn.update_nna_vehicle_incentives()
  RETURNS void AS
$BODY$
/*
select hn.update_nna_vehicle_incentives()
*/
truncate hn.nna_vehicle_incentives;
insert into hn.nna_vehicle_incentives (vin,incentive_program_id,avg_amount,
  max_amount,min_amount,cash_incentive,lease_incentive,special_apr_incentive,
  standard_apr_incentive)
select distinct vin,
  b.programs ->> 'incentiveProgramID' as incentive_program_id,
  nullif(trim(replace(replace(b.programs ->> 'avgAmount', '$', ''), ',','')), ''),
  nullif(trim(replace(replace(b.programs ->> 'maxAmount', '$', ''), ',','')), ''),
  nullif(trim(replace(replace(b.programs ->> 'minAmount', '$', ''), ',','')), ''),
  nullif(trim(replace(replace(b.programs ->> 'cashIncentive', '$', ''), ',','')), ''),
  nullif(trim(replace(replace(b.programs ->> 'leaseIncentive', '$', ''), ',','')), ''),
  nullif(trim(replace(replace(b.programs ->> 'specialAPRIncentive', '$', ''), ',','')), ''),
  nullif(trim(replace(replace(b.programs ->> 'standardAPRIncentive', '$', ''), ',','')), '')
from hn.ext_nna_incentives a
join jsonb_array_elements(a.raw_incentives->'eligiblePrograms') as b(programs) on true
where the_date = current_date;
$BODY$
LANGUAGE sql; 
comment on FUNCTION hn.update_nna_vehicle_incentives() is 'extracts incentive values from hn.nna_incentives.raw_incentives
  for each vehicle, daily truncates and repopulates hn.nna_vehicle_incentives';



  
