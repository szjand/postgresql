﻿-- Function: nc.xfm_model_year_build_out_start_up()

-- DROP FUNCTION nc.xfm_model_year_build_out_start_up();

CREATE OR REPLACE FUNCTION nc.xfm_model_year_build_out_start_up()
  RETURNS void AS
$BODY$
/*
6/12/19 major refactor to accomodate report changing from pdf to xlsx
build out issues:
  null division: division name is in first row only for each division
  dates: date fields are all populated with excel integers
11/22/2019
    report is now a pdf again, and all this integer stuff is failing
    don't have the old pdf code so here we go ...
    fields in target table are already citext, should be ok
*/
 -- build out -----------------------------------------------------------------------------------------------------------------
  truncate nc.build_out_2019;
  alter table nc.build_out_2019
  drop constraint build_out_2019_pkey;

  insert into nc.build_out_2019 (division,model,alloc_group,
    my_build_out_date, final_consensus_month, final_dosp, unallocated_sold_order_cutoff, 
    insystem_return_cut_off, special_paint_order_cutoff,
    seo_order_cutoff, non_seo_order_cutoff)  
  select coalesce(division, first_value(division) over (partition by div_grp)) as division,
    model,alloc_group,
--     case -- replace excel integer dates with date format string
--       when my_build_out_date like '4%' then ('12/31/1899'::date + (left(my_build_out_date, 5)::integer) - 1)::text
--       else my_build_out_date
--     end as my_build_out_date,
--     final_consensus_month, 
--     case 
--       when final_dosp like '4%' then ('12/31/1899'::date + (left(final_dosp, 5)::integer) - 1)::text
--       else final_dosp
--     end as final_dosp,  
--     case 
--       when unallocated_sold_order_cutoff like '4%' then ('12/31/1899'::date + (left(unallocated_sold_order_cutoff, 5)::integer) - 1)::text
--       else unallocated_sold_order_cutoff
--     end as unallocated_sold_order_cutoff,    
--     case 
--       when insystem_return_cut_off like '4%' then ('12/31/1899'::date + (left(insystem_return_cut_off, 5)::integer) - 1)::text
--       else insystem_return_cut_off
--     end as insystem_return_cut_off,    
--     case 
--       when special_paint_order_cutoff like '4%' then ('12/31/1899'::date + (left(special_paint_order_cutoff, 5)::integer) - 1)::text
--       else special_paint_order_cutoff
--     end as special_paint_order_cutoff,     
--     case 
--       when seo_order_cutoff like '4%' then ('12/31/1899'::date + (left(seo_order_cutoff, 5)::integer) - 1)::text
--       else seo_order_cutoff
--     end as seo_order_cutoff,   
--     case 
--       when non_seo_order_cutoff like '4%' then ('12/31/1899'::date + (left(non_seo_order_cutoff, 5)::integer) - 1)::text
--       else non_seo_order_cutoff
--     end as non_seo_order_cutoff
    my_build_out_date, final_consensus_month, final_dosp, unallocated_sold_order_cutoff, insystem_return_cut_off,
    special_paint_order_cutoff, seo_order_cutoff, non_seo_order_cutoff
  from ( -- add div_grp 
    select division,model,alloc_group,
      my_build_out_date, final_consensus_month, final_dosp, unallocated_sold_order_cutoff, 
      insystem_return_cut_off, special_paint_order_cutoff,
      seo_order_cutoff, non_seo_order_cutoff, row_number,
      sum(case when division is not null then 1 end) over (order by row_number) as div_grp
    from (-- add row_number
      select division,model,alloc_group,
        my_build_out_date, final_consensus_month, final_dosp, unallocated_sold_order_cutoff, 
        insystem_return_cut_off, special_paint_order_cutoff,
        seo_order_cutoff, non_seo_order_cutoff,
        row_number() over ()
      from nc.ext_build_out_2019
      where model is not null
        and model <> 'MODEL'
        and trim(model) <> '') a) b;

  insert into nc.build_out_2019 (division,model,alloc_group,
    my_build_out_date, final_consensus_month, final_dosp, unallocated_sold_order_cutoff, 
    insystem_return_cut_off, special_paint_order_cutoff,
    seo_order_cutoff, non_seo_order_cutoff)
  select division, trim(left(model, position ('/' in model)-1)), 
    trim(left(alloc_group, position ('/' in alloc_group)-1)),
    my_build_out_date, final_consensus_month, final_dosp, unallocated_sold_order_cutoff, 
    insystem_return_cut_off, special_paint_order_cutoff, 
    seo_order_cutoff, non_seo_order_cutoff
  from nc.build_out_2019
  where position('/' in alloc_group) <> 0;

  insert into nc.build_out_2019 (division,model,alloc_group,
    my_build_out_date, final_consensus_month, final_dosp, unallocated_sold_order_cutoff, 
    insystem_return_cut_off, special_paint_order_cutoff,
    seo_order_cutoff, non_seo_order_cutoff)
  select division, trim(substring(model, position('/' in model) + 1, length(model))), 
    trim(substring(alloc_group, position('/' in alloc_group) + 1, length(alloc_group))),
    my_build_out_date, final_consensus_month, final_dosp, unallocated_sold_order_cutoff, 
    insystem_return_cut_off, special_paint_order_cutoff,
    seo_order_cutoff, non_seo_order_cutoff
  from nc.build_out_2019
  where position('/' in alloc_group) <> 0;  

  delete 
  from nc.build_out_2019
  where position('/' in alloc_group) <> 0;          

  alter table nc.build_out_2019
  add primary key (model,alloc_group);

  
  -- start up -----------------------------------------------------------------------------------
  truncate nc.start_up_2020;
  alter table nc.start_up_2020
  drop constraint start_up_2020_pkey;  

  insert into nc.start_up_2020 (division,model,alloc_group,
    my_start_up_date,initial_consensus,initial_dosp, order_entry_availability)  
  select coalesce(division, first_value(division) over (partition by div_grp)) as division,
    model,alloc_group, 
--     case -- replace excel integer dates with date format string
--       when my_start_up_date like '4%' then ('12/31/1899'::date + (left(my_start_up_date, 5)::integer) - 1)::text
--       else my_start_up_date
--     end as my_start_up_date,  
--     initial_consensus,
--     case
--       when initial_dosp like '4%' then ('12/31/1899'::date + (left(initial_dosp, 5)::integer) - 1)::text
--       else initial_dosp
--     end as initial_dosp,    
--     case 
--       when order_entry_availability like '4%' then ('12/31/1899'::date + (left(order_entry_availability, 5)::integer) - 1)::text
--       else order_entry_availability
--     end as order_entry_availability
    my_start_up_date, initial_consensus, initial_dosp, order_entry_availability
  -- select *  
  from ( -- add div_grp 
    select division,model,alloc_group, my_start_up_date,
      initial_consensus,initial_dosp, order_entry_availability,
      row_number,
      sum(case when division is not null then 1 end) over (order by row_number) as div_grp
    from ( -- add row_number
      select division,model,alloc_group, my_start_up_date,
        initial_consensus,initial_dosp, order_entry_availability,
        row_number() over ()
      from nc.ext_start_up_2020
      where model is not null
        and model <> 'MODEL'
        and trim(model) <> '') a) b;

  insert into nc.start_up_2020 (division,model,alloc_group,
    my_start_up_date,initial_consensus,initial_dosp, order_entry_availability)
  select division, trim(left(model, position ('/' in model)-1)), 
    trim(left(alloc_group, position ('/' in alloc_group)-1)),
    my_start_up_date,initial_consensus,initial_dosp, order_entry_availability
  from nc.start_up_2020
  where position('/' in alloc_group) <> 0;

  insert into nc.start_up_2020 (division,model,alloc_group,
    my_start_up_date,initial_consensus,initial_dosp, order_entry_availability)
  select division, trim(substring(model, position('/' in model) + 1, length(model))), 
    trim(substring(alloc_group, position('/' in alloc_group) + 1, length(alloc_group))),
    my_start_up_date,initial_consensus,initial_dosp, order_entry_availability
  from nc.start_up_2020
  where position('/' in alloc_group) <> 0;  

  delete 
  from nc.start_up_2020
  where position('/' in alloc_group) <> 0;  


  alter table nc.start_up_2020
  add primary key (model,alloc_group);
  
$BODY$
  LANGUAGE sql VOLATILE
  COST 100;
ALTER FUNCTION nc.xfm_model_year_build_out_start_up()
  OWNER TO rydell;
COMMENT ON FUNCTION nc.xfm_model_year_build_out_start_up() IS 'populate tables nc.build_out_2019
  and nc.start_up_2020 with cleaned versions of the extract tables';
