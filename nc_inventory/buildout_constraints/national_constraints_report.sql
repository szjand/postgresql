﻿CREATE TABLE nc.ext_build_out_constraints (
  division citext,
  model citext,
  description citext,
  last_dosp citext,
  last_production_week citext);
comment on table nc.ext_build_out_constraints is 'raw extract of the top portion of the 
  national_constraints_report.pdf';

CREATE TABLE nc.xfm_build_out_constraints (
  model citext,
  description citext,
  last_dosp citext,
  last_production_week citext);
comment on table nc.ext_build_out_constraints is 'cleaned up version of nc.ext_build_out_constraints,
  left out division, only populate rows with data in description';
  
create table nc.ext_national_constraints (
  status citext,
  alloc_group citext,
  tpp citext,
  constraint_number citext,
  option_model_code citext,
  description citext,
  percent_natl_alloc_qty citext,
  constraint_duration citext,
  comments citext);
comment on table nc.ext_national_constraints is 'raw extract of the bottom portion of the 
  national_constraints_report.pdf';

create table nc.xfm_national_constraints (
  alloc_group citext,
  tpp citext,
  constraint_number citext,
  option_model_code citext,
  description citext,
  percent_natl_alloc_qty citext,
  constraint_duration citext,
  comments citext);
comment on table nc.xfm_national_constraints is 'cleaned up version of nc.ext_national_constraints,
  left out status'; 

select * from nc.ext_build_out_constraints    
select * from nc.ext_national_constraints  

do
$$
begin
  -- build out constraints
  truncate nc.xfm_build_out_constraints;

  insert into nc.xfm_build_out_constraints
  select model,description,last_dosp,last_production_week
  from nc.ext_build_out_constraints
  where model is not null
    and description is not null
    and model <> 'Brand / Model';

  -- national constraints
  truncate nc.xfm_national_constraints;
  insert into nc.xfm_national_constraints (alloc_group,tpp,constraint_number,option_model_code,
    description,percent_natl_alloc_qty,constraint_duration,comments)
  select 
    case
      when length(status) > 1 then trim(replace(status, '#',''))
      else alloc_group
    end,
    tpp, constraint_number, option_model_code, description, percent_natl_alloc_qty,
    constraint_duration, comments 
  from nc.ext_national_constraints
  -- coalesce necessary because alloc_group could be null
  where coalesce(alloc_group, '1') <> 'Allocation Group'
    and constraint_number is not null;
end
$$;


truncate nc.ext_build_out_constraints;
truncate nc.xfm_build_out_constraints;
truncate nc.ext_national_constraints;  
truncate nc.xfm_national_constraints;  

select * from nc.ext_build_out_constraints;
select * from nc.xfm_build_out_constraints;
select * from nc.ext_national_constraints;  
select * from nc.xfm_national_constraints;  