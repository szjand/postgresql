﻿drop table if exists market_summary;
create temp table market_summary as
select a.division, a.model, a.description,
  '12/31/1899'::date + (left(last_dosp, 5)::integer) - 1 as last_dosp,
  '12/31/1899'::date + (left(last_production_week, 5)::integer) - 1 as last_production_week,
  row_number() over ()  
from nc.ext_build_out_constraints a
where last_dosp like '4%';

select * from market_summary

tabs: islands, gaps, fill
                                                                   || ',''' || distcode || ''',' || ptopay || ');'

                                                                   
select '(''' || coalesce(division, 'NULL') || ''',''' || coalesce(model, 'NULL') || ''',''' || ''',''' || description ||''',' || row_number || '),'
-- select division, model, description, row_number
from (
  select *, row_number() over ()
  from nc.ext_build_out_constraints
  where last_dosp like '4%') A
where row_number in (1,2,3,4,5,6,17,18,19,25,26,27)


-- for the confluence article
with 
  build_out_constraints (division,model,description,row_number) as
  (values
    ('Cadillac','CT6','GB8 Steller Black Paint',1),
    (NULL,NULL,'GAN Radiant Silver Metallic',2),
    (NULL,NULL,'LGX 3.6L V6 Engine',3),
    (NULL,'XT4','G6O ',4),
    (NULL,'XT5','GXG- Iridium Metallic Exterior color',5),
    (NULL,NULL,'G35- Harbor Blue Metallic Exterior Color',6),
    ('Chevrolet','BLAZER','GD1 - Kinetic Blue Metallic Exterior Color',7),
    (NULL,'BOLTEV','G9A Opal Mist',8),
    (NULL,NULL,'4F3 Light Interior',9),
    (NULL,'EQUINOX','G8K Sandy Ridge Metallic Exterior Paint with 3LT/3LZ',10),
    (NULL,NULL,'GMU Pepperdust Metallic Exterior Paint',11),
    (NULL,NULL,'G35 Storm Blue Metallic Exterior Paint',12))
--  select * from build_out_constraints
select division, model, description, row_number,
  first_value(division) over (partition by div_grp) as corrected_div,
  first_value(model) over (partition by model_grp) as corrected_model
from (
  select division, model, description, row_number,
    sum(case when division is not null then 1 end) over (order by row_number) as div_grp,
    sum(case when model is not null then 1 end) over (order by row_number) as model_grp
  from build_out_constraints) t


select division, model, description, row_number,
  first_value(division) over (partition by div_grp) as corrected_div,
  first_value(model) over (partition by model_grp) as corrected_model
from (
  select division, model, description, row_number,
    sum(case when division is not null then 1 end) over (order by row_number) as div_grp,
    sum(case when model is not null then 1 end) over (order by row_number) as model_grp
  from market_summary) t


-- the function
with 
  xfm_build_out_constraints as (
    select a.division, a.model, a.description,
      '12/31/1899'::date + (left(last_dosp, 5)::integer) - 1 as last_dosp,
      '12/31/1899'::date + (left(last_production_week, 5)::integer) - 1 as last_production_week,
      row_number() over ()  
    from nc.ext_build_out_constraints a
    where last_dosp like '4%')  
select first_value(division) over (partition by div_grp) as corrected_div,
  first_value(model) over (partition by model_grp) as corrected_model,
  description, last_dosp, last_production_week
from (
  select division, model, description, row_number, last_dosp, last_production_week,
    sum(case when division is not null then 1 end) over (order by row_number) as div_grp,
    sum(case when model is not null then 1 end) over (order by row_number) as model_grp
  from xfm_build_out_constraints) t  