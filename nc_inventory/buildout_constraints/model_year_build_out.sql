﻿drop table if exists nc.ext_build_out_2019;
create table nc.ext_build_out_2019 (
  division citext,
  model citext,
  alloc_group citext,
  my_build_out_date citext,
  final_consensus_month citext,
  final_dosp citext,
  unallocated_sold_order_cutoff citext,
  insystem_return_cut_off citext,
  special_paint_order_cutoff citext,
  seo_order_cutoff citext,
  non_seo_order_cutoff citext);
comment on table nc.ext_build_out_2019 is 'raw parsing of pdf report from global connect,
don''t at this time know where the report comes from. Report contains 2019 model year build-out
and 2020 start-up';  

drop table if exists nc.xfm_build_out_2019;
create table nc.xfm_build_out_2019 (
  division citext,
  model citext,
  alloc_group citext,
  my_build_out_date citext,
  final_consensus_month citext,
  final_dosp citext,
  unallocated_sold_order_cutoff citext,
  insystem_return_cut_off citext,
  special_paint_order_cutoff citext,
  seo_order_cutoff citext,
  non_seo_order_cutoff citext);
comment on table nc.xfm_build_out_2019 is 'cleaned up version of nc.ext_build_out_2019,
  truncate and fill nightly';  
  
drop table if exists nc.ext_start_up_2020;
create table nc.ext_start_up_2020 (
  division citext,
  model citext,
  alloc_group citext,
  my_start_up_date citext,
  initial_consensus citext,
  initial_dosp citext,
  order_entry_availability citext); 
comment on table nc.ext_start_up_2020 is 'raw parsing of pdf report from global connect,
don''t at this time know where the report comes from. Report contains 2019 model year build-out
and 2020 start-up';  


drop table if exists nc.xfm_start_up_2020;
create table nc.xfm_start_up_2020 (
  division citext,
  model citext,
  alloc_group citext,
  my_start_up_date citext,
  initial_consensus citext,
  initial_dosp citext,
  order_entry_availability citext); 
comment on table nc.xfm_start_up_2020 is 'cleaned up version of nc.ext_start_up_2019,
  truncate and fill nightly';  

truncate nc.ext_build_out_2019;
truncate nc.xfm_build_out_2019;
truncate nc.ext_start_up_2020;
truncate nc.xfm_start_up_2020;  


select * from nc.ext_build_out_2019;
select * from nc.xfm_build_out_2019;
select * from nc.ext_start_up_2020;
select * from nc.xfm_start_up_2020;  



-- need a proc to do minimal but necessary transform on ext
-- remove non vehicle rows
-- breakout rows that contain multiple models: "Regal Sportback / TourX", "Camaro Coupe / Convertible", "Corvette Stingray / Z06"
-- into multiple rows
do
$$
begin
  -- build out
  truncate nc.xfm_build_out_2019;

  insert into nc.xfm_build_out_2019 (division,model,alloc_group,
    my_build_out_date, final_consensus_month, final_dosp, unallocated_sold_order_cutoff, 
    insystem_return_cut_off, special_paint_order_cutoff,
    seo_order_cutoff, non_seo_order_cutoff)
  select division,model,alloc_group,
    my_build_out_date, final_consensus_month, final_dosp, unallocated_sold_order_cutoff, 
    insystem_return_cut_off, special_paint_order_cutoff,
    seo_order_cutoff, non_seo_order_cutoff
  from nc.ext_build_out_2019;

  delete from nc.xfm_build_out_2019
  where division is null 
    or division = 'DIVISION';

  insert into nc.xfm_build_out_2019 (division,model,alloc_group,
    my_build_out_date, final_consensus_month, final_dosp, unallocated_sold_order_cutoff, 
    insystem_return_cut_off, special_paint_order_cutoff,
    seo_order_cutoff, non_seo_order_cutoff)
  select division, trim(left(model, position ('/' in model)-1)), 
    trim(left(alloc_group, position ('/' in alloc_group)-1)),
    my_build_out_date, final_consensus_month, final_dosp, unallocated_sold_order_cutoff, 
    insystem_return_cut_off, special_paint_order_cutoff, 
    seo_order_cutoff, non_seo_order_cutoff
  from nc.ext_build_out_2019
  where position('/' in alloc_group) <> 0;

  insert into nc.xfm_build_out_2019 (division,model,alloc_group,
    my_build_out_date, final_consensus_month, final_dosp, unallocated_sold_order_cutoff, 
    insystem_return_cut_off, special_paint_order_cutoff,
    seo_order_cutoff, non_seo_order_cutoff)
  select division, trim(substring(model, position('/' in model) + 1, length(model))), 
    trim(substring(alloc_group, position('/' in alloc_group) + 1, length(alloc_group))),
    my_build_out_date, final_consensus_month, final_dosp, unallocated_sold_order_cutoff, 
    insystem_return_cut_off, special_paint_order_cutoff,
    seo_order_cutoff, non_seo_order_cutoff
  from nc.ext_build_out_2019
  where position('/' in alloc_group) <> 0;  

  delete 
  from nc.xfm_build_out_2019
  where position('/' in alloc_group) <> 0;  

  -- start up
  truncate nc.xfm_start_up_2019;

  insert into nc.xfm_start_up_2019 (division,model,alloc_group,
    my_start_up_date,initial_consensus,initial_dosp, order_entry_availability)
  select division,model,alloc_group,
    my_start_up_date,initial_consensus,initial_dosp, order_entry_availability
  from nc.ext_start_up_2019;

  delete from nc.xfm_start_up_2019
  where division is null 
    or division = 'DIVISION';

  insert into nc.xfm_start_up_2019 (division,model,alloc_group,
    my_start_up_date,initial_consensus,initial_dosp, order_entry_availability)
  select division, trim(left(model, position ('/' in model)-1)), 
    trim(left(alloc_group, position ('/' in alloc_group)-1)),
    my_start_up_date,initial_consensus,initial_dosp, order_entry_availability
  from nc.ext_start_up_2019
  where position('/' in alloc_group) <> 0;

  insert into nc.xfm_start_up_2019 (division,model,alloc_group,
    my_start_up_date,initial_consensus,initial_dosp, order_entry_availability)
  select division, trim(substring(model, position('/' in model) + 1, length(model))), 
    trim(substring(alloc_group, position('/' in alloc_group) + 1, length(alloc_group))),
    my_start_up_date,initial_consensus,initial_dosp, order_entry_availability
  from nc.ext_start_up_2019
  where position('/' in alloc_group) <> 0;  

  delete 
  from nc.xfm_start_up_2019
  where position('/' in alloc_group) <> 0;    
end
$$;






truncate nc.ext_build_out_2019;
truncate nc.xfm_build_out_2019;
truncate nc.ext_start_up_2020;
truncate nc.xfm_start_up_2020;  

select * from nc.ext_build_out_2019;
select * from nc.xfm_build_out_2019;
select * from nc.ext_start_up_2020;
select * from nc.xfm_start_up_2020;  




 