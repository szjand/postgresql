﻿-- 7/11 ben says gm has a glitch showing orders as delivered to dealer when they are not

drop table if exists suspect_vehicles;
select cc.inpmast_stock_number, aa.* --, bb.*, , cc.status, dd.*
from (
  SELECT a.order_number, a.order_type, a.vin, a.model_year,a.alloc_group, a.vehicle_trim,
    a.model_code, a.ship_to_bac, b.event_code, c.description, c.category,
    b.from_date
-- select *    
  FROM gmgl.vehicle_orders a
  JOIN gmgl.vehicle_order_events b ON a.order_number = b.order_number
  JOIN gmgl.vehicle_event_codes c ON b.event_code = c.code
  where b.event_code = '5000'
    and a.model_year = 2018) aa
left join (
    select d.vin, max(c.opcode) as opcode
--       max(case when c.opcode = 'tsp' then b.the_date end) as tsp,
--       max(case when c.opcode = 'pdi' then b.the_date end) as pdi,
--       max(case when c.opcode = 'dt' then b.the_date end) as dt
    from ads.ext_fact_repair_order a
    inner join dds.dim_date b on a.opendatekey = b.date_key
    inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
--       and c.opcode in ('tsp', 'dt', 'pdi')
    inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
    inner join gmgl.vehicle_orders e on d.vin = e.vin
    group by d.vin) bb on aa.vin = bb.vin
left join arkona.xfm_inpmast cc on aa.vin = cc.inpmast_vin
  and cc.current_row = true 
left join ads.keyper_creation_dates dd on cc.inpmast_stock_number = dd.stock_number      
where bb.vin is null    
  and dd.stock_number is null
  and cc.status = 'I'
  and ship_to_bac = '111708'





