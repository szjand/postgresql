﻿/*
ben questioins

ctp?


*/
/*
per michelle
honda nissan holdback account, for dealer trades in PVI
Yeah, the account to look for with Nissan is 226100 and Honda is 226105/226106

Side note, all Honda/Nissan dealer trades are check trades so there should never be a dealer trade that is entered under a PVI journal 
*/

--inventory accounts
select *
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201806
  and b.page = 17
  and b.line in (26,27)
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
--   and c.store = 'ry1'
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key  
-- where d.gl_account = '123700'

select *
from fin.dim_account
where account in ('223105','223100','223700','223000')

-- accounting acquisitions
-- drop table if exists stg_avail;
-- create temp table stg_avail as
create table nc.stg_avail_hn (
  journal_date date,
  stock_number citext,
  vin citext,
  model_year citext,
  make citext, 
  model citext,
  model_code citext,
  color_code citext,
  color citext, 
  source citext,
  chrome_style_id_inp citext,
  ro_date date,
  keyper_date date,
  ground_date date);
-- at least for hondas, dealertrack body style seems to be accurate
alter table nc.stg_avail_hn
add column body_style citext; 
update nc.stg_avail_hn z
set body_style = (
  select body_style
  from arkona.xfm_inpmast
  where inpmast_vin = z.vin
    and current_row = true);
-- 7/23 made a physical record, since stg_avail includes all the cleanup work i've done today
-- the data in nc.stg_avail_hn has been updated through colors only
insert into nc.stg_avail_hn
select * from stg_avail;


select f.the_date as journal_date, f.control as stock_number, 
  g.inpmast_vin as vin, g.year as model_year, g.make, g.model, g.model_code, 
  g.color_code, g.color,
-- select distinct  
  case f.journal_code
    when 'PVI' then 'factory'
    when 'CDH' then 'dealer trade'
    when 'GJE' then 'ctp'
  end as source, g.chrome_style_id as chrome_style_id_inp, 
  null::date as ro_date, null::date as keyper_date, null::date as ground_date
-- select model, count(*)  -- cr-v:141, rogue:66, pilot:65, accord sedan:89, odyssey:69
from ( -- accounting journal
  select a.control, d.journal_code, max(b.the_date) as the_date, sum(a.amount) as amount
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
--     and b.the_date between current_Date - 365 and current_date--'06/22/2018'
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account in ('223105','223100','223700','223000')
  inner join fin.dim_journal d on a.journal_key = d.journal_key
    and d.journal_code in ('PVI','CDH','GJE')
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where a.post_status = 'Y'
  group by a.control, d.journal_code
  having sum(a.amount) > 10000) f
inner join arkona.xfm_inpmast g on f.control = g.inpmast_stock_number
  and g.year = 2018
  and g.model in ('rogue','pilot','cr-v')
  and g.current_row = true -- group by model order by count(*) desc

-- unwind acquisitions
insert into stg_avail
select a.run_date, a.stock_number, 
  a.vin, b.year, b.make, b.model, b.model_code, 
  b.color_code, b.color, 
  'unwind' as source, b.chrome_style_id
from sls.deals a
inner join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row = true
  and b.model in ('rogue','pilot','cr-v')
  and a.vehicle_type_code = 'N'
  and b.year = 2018
where deal_status_code = 'deleted';

-- -- most common opcodes
-- select x.*, coalesce(creation_date, journal_date) - journal_date, coalesce(pdi, journal_date) - journal_date
-- from (
-- select aa.control, aa.inpmast_vin, aa.journal_date, aa.source, bb.pdi, bb.dt, bb.caltex, cc.creation_Date
-- from stg_avail aa
-- left join (
--   select d.vin, 
--     max(case when c.opcode = 'pdi' then b.the_date end) as pdi,
--     max(case when c.opcode = 'dt' then b.the_date end) as dt,
--     max(case when c.opcode = 'caltex' then b.the_date end) as caltex
--   from ads.ext_fact_repair_order a
--   inner join dds.dim_date b on a.opendatekey = b.date_key
--   inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
--     and c.opcode in ('dt','pdi', 'caltex')
--   inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
--   inner join stg_avail e on d.vin = e.inpmast_vin
--   group by d.vin) bb on aa.inpmast_vin = bb.vin
-- left join ads.keyper_creation_dates cc on aa.control = cc.stock_number
-- order by source
-- ) x 


-- why limit the opcodes, go for the first ro
select *
from (
  select d.vin, a.ro, c.opcode, b.the_date,
    row_number() over (partition by vin order by the_date asc)
  from ads.ext_fact_repair_order a
  inner join dds.dim_date b on a.opendatekey = b.date_key
  inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
  inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
  inner join stg_avail e on d.vin = e.inpmast_vin) x
where row_number = 1  
order by vin

update stg_avail x
set ro_date = y.the_date,
    keyper_date = y.keyper_date,
    ground_date = y.ground_date
from (    
  select aa.vin, bb.the_date, cc.creation_date as keyper_date, least(the_date, creation_date) as ground_date
  from stg_avail aa
  left join (  
    select *
    from ( -- first ro
      select d.vin, a.ro, c.opcode, b.the_date,
        row_number() over (partition by d.vin order by the_date asc)
      from ads.ext_fact_repair_order a
      inner join dds.dim_date b on a.opendatekey = b.date_key
      inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
      inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
      inner join stg_avail e on d.vin = e.vin) x
    where row_number = 1) bb on aa.vin = bb.vin
  left join ads.keyper_creation_dates cc on aa.stock_number = cc.stock_number) y  
where x.vin = y.vin


-----------------------------------------------------------------------------------------------
-- < dealer trade / PVI ???  H11420, H10703, H11422
-----------------------------------------------------------------------------------------------
-- these 3 seem to contradict michelle, 2 of them (in UI) show inv date = sale date
select *
from nc.stg_avail_hn where stock_number in (
    select stock_number
    from nc.stg_avail_hn a
    left join (
      select a.control, a.doc, d.journal_code, a.amount
      from fin.fact_gl a
      inner join fin.dim_account c on a.account_key = c.account_key
        and c.account in ('226105','226106','226100')
      inner join fin.dim_journal d on a.journal_key = d.journal_key
        and d.journal_code = 'PVI'
      where a.post_status = 'Y') b on a.stock_number = b.doc
    where b.doc is null
      and a.source = 'factory')


  select b.the_date, a.*, d.journal_code, c.account
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
--     and b.the_date between current_Date - 365 and current_date--'06/22/2018'
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account in ('223105','223100','223700','223000','226105','226106','226100')
  inner join fin.dim_journal d on a.journal_key = d.journal_key
--     and d.journal_code in ('PVI','CDH','GJE')
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where 1 = 1 --a.post_status = 'Y'
    and a.control in ('H10703')

from jeri:
H11420 - Michelle accrued this at EOM because we hadn’t written a check by the time they sold it.  Odd exception
H10703 - this isn’t a dealer trade
H11422 - Michelle accrued this at EOM because we hadn’t written a check by the time they sold it.  Odd exception

update nc.stg_avail_hn
set source = 'dealer trade'
where stock_number in ('h11420','h11422') ;

  
-----------------------------------------------------------------------------------------------
--/> dealer trade / PVI ???  
-----------------------------------------------------------------------------------------------


-- vins for chr.describe vehicle
drop table if exists nc.tmp_vins;
create table nc.tmp_vins (vin citext primary key);
insert into nc.tmp_vins
select a.vin
from stg_avail a
left join chr.describe_vehicle b on a.vin = b.vin
where b.vin is null


----------------------------------------------------------------------------------------
--< colors
----------------------------------------------------------------------------------------
-- colors I
select *
from stg_avail a
left join nc.exterior_colors b
on a.model_year = b.model_year::integer
  and a.make = b.make 
  and a.model = b.model
  and a.color_code = b.mfr_color_code
where a.color <> b.mfr_color_name  
order by b.mfr_color_name


update stg_avail
set color = 'Basque Red Pearl II'
where color_code = 'RB'
  and model_year = 2018
  and make = 'honda'
  and model = 'cr-v'
  and color <> 'Basque Red Pearl II';

update stg_avail
set color = 'Brilliant Silver'
where color_code = 'K23'
  and model_year = 2018
  and make = 'nissan'
  and model = 'rogue'
  and color <> 'Brilliant Silver';  

update stg_avail
set color = 'Crystal Black Pearl'
where color_code = 'BX'
  and model_year = 2018
  and make = 'honda'
  and model = 'pilot'
  and color <> 'Crystal Black Pearl';  

update stg_avail
set color = 'Crystal Black Pearl'
where color_code = 'BK'
  and model_year = 2018
  and make = 'honda'
  and model = 'pilot'
  and color <> 'Crystal Black Pearl';    

update stg_avail
set color = 'Crystal Black Pearl'
where color_code = 'BK'
  and model_year = 2018
  and make = 'honda'
  and model = 'CR-v'
  and color <> 'Crystal Black Pearl';    

update stg_avail
set color = 'Dark Olive Metallic'
where color_code = 'GN'
  and model_year = 2018
  and make = 'honda'
  and model = 'CR-v'
  and color <> 'Dark Olive Metallic';     

update stg_avail
set color = 'Deep Scarlet Pearl'
where color_code = 'RX'
  and model_year = 2018
  and make = 'honda'
  and model = 'pilot'
  and color <> 'Deep Scarlet Pearl';    

update stg_avail
set color = 'Gunmetal Metallic'
where color_code = 'GB'
  and model_year = 2018
  and make = 'honda'
  and model = 'cr-v'
  and color <> 'Gunmetal Metallic';    

update stg_avail
set color = 'Lunar Silver Metallic'
where color_code = 'SX'
  and model_year = 2018
  and make = 'honda'
  and model = 'cr-v'
  and color <> 'Lunar Silver Metallic';    

update stg_avail
set color = 'Lunar Silver Metallic'
where color_code = 'SX'
  and model_year = 2018
  and make = 'honda'
  and model = 'pilot'
  and color <> 'Lunar Silver Metallic';    

update stg_avail
set color = 'Modern Steel Metallic'
where color_code = 'GY'
  and model_year = 2018
  and make = 'honda'
  and model = 'pilot'
  and color <> 'Modern Steel Metallic';     

update stg_avail
set color = 'Modern Steel Metallic'
where color_code = 'GY'
  and model_year = 2018
  and make = 'honda'
  and model = 'cr-v'
  and color <> 'Modern Steel Metallic';   

update stg_avail
set color = 'Modern Steel Metallic'
where color_code = 'GX'
  and model_year = 2018
  and make = 'honda'
  and model = 'pilot'
  and color <> 'Modern Steel Metallic';   

update stg_avail
set color = 'Modern Steel Metallic'
where color_code = 'GX'
  and model_year = 2018
  and make = 'honda'
  and model = 'CR-V'
  and color <> 'Modern Steel Metallic';   
  
update stg_avail
set color = 'Modern Steel Metallic'
where color_code = 'GY'
  and model_year = 2018
  and make = 'honda'
  and model = 'cr-v'
  and color <> 'Modern Steel Metallic';   

update stg_avail
set color = 'Molten Lava Pearl'
where color_code = 'RD'
  and model_year = 2018
  and make = 'honda'
  and model = 'cr-v'
  and color <> 'Molten Lava Pearl';   

update stg_avail
set color = 'Obsidian Blue Pearl'
where color_code = 'BU'
  and model_year = 2018
  and make = 'honda'
  and model = 'cr-v'
  and color <> 'Obsidian Blue Pearl';     

update stg_avail
set color = 'Obsidian Blue Pearl'
where color_code = 'BS'
  and model_year = 2018
  and make = 'honda'
  and model = 'pilot'
  and color <> 'Obsidian Blue Pearl';      

update stg_avail
set color = 'Steel Sapphire Metallic'
where color_code = 'BH'
  and model_year = 2018
  and make = 'honda'
  and model = 'pilot'
  and color <> 'Steel Sapphire Metallic';   

update stg_avail
set color = 'White Diamond Pearl'
where color_code = 'WA'
  and model_year = 2018
  and make = 'honda'
  and model = 'pilot'
  and color <> 'White Diamond Pearl';   

update stg_avail
set color = 'White Diamond Pearl'
where color_code = 'WA'
  and model_year = 2018
  and make = 'honda'
  and model = 'cr-v'
  and color <> 'White Diamond Pearl';        

update stg_avail
set color = 'White Diamond Pearl'
where color_code = 'WB'
  and model_year = 2018
  and make = 'honda'
  and model = 'pilot'
  and color <> 'White Diamond Pearl';   

update stg_avail
set color = 'White Diamond Pearl'
where color_code = 'WB'
  and model_year = 2018
  and make = 'honda'
  and model = 'cr-v'
  and color <> 'White Diamond Pearl';   

-- colors II
select *
from stg_avail a
left join nc.exterior_colors b
on a.model_year = b.model_year::integer
  and a.make = b.make 
  and a.model = b.model
  and a.color_code = b.mfr_color_code
where b.mfr_color_name is null 
order by color

select * from stg_avail where color like '%silver%' and model = 'cr-v'

update stg_avail
set color = 'LUNAR SILVER METALLIC', color_code = 'SI'
where vin = '2HKRW2H98JH640935'

update stg_Avail
set color_code = 'GY'
where color = 'modern steel'
  and color_code is null;
  
update stg_avail
set color_code = replace(color_code, '`', '')
-- select * from stg_avail
where position('`' in color_code) <> 0;

update stg_avail
set color_code = 'GN'
where vin = '7FARW2H50JE016126'


select *
from nc.exterior_colors
where model_year = '2018'
  and make = 'nissan'
  and model = 'rogue'
  and mfr_color_name like '%white%'

update stg_Avail
set color_code = 'QAB'
where color = 'pearl white'
  and color_code is null;
  
----------------------------------------------------------------------------------------
-- /> colors
----------------------------------------------------------------------------------------

!!!!!!!!!!!!!!!!!! 7/24 from this point on use nc.stg_avail_hn
----------------------------------------------------------------------------------------
-- < multiple trims
----------------------------------------------------------------------------------------
do
$$
begin
assert (
      select count(*) = sum(b.style_count)
      from nc.stg_avail_hn a
      left join chr.describe_vehicle b on a.vin = b.vin), 'DANGER WILL ROBINSON, there are vehicles with multiple chrome records';
end
$$;

do -- honda model_code vs vin
$$
begin
assert (
  select count(*)
  from nc.stg_avail_hn 
  where make = 'honda'
    and substring(vin, 4, 5) <> left(model_code, 5)) = 0, 'DANGER WILL ROBINSON, there are hondas with nonconforming model code';
end
$$;

-- -- fix the one bad inpmast model code
-- update nc.stg_avail_hn
-- set model_code = 'RW2H8JJNW',
--     body_style = 'EX-L AWD',
--     chrome_style_id_inp = '397488'
-- where vin = '2HKRW2H81JH648088'

-- the only vehicles with mutliple chrome records are honda cr-v
-- 63 rows with style_count = 2
select a.*, b.style_count
from nc.stg_avail_hn a
inner join chr.describe_vehicle b on a.vin = b.vin
where b.style_count > 1

-- so far, matching on model code gives me the correct data
select *
from (
  select a.*, 
    jsonb_array_elements(b.response->'style')->'attributes'->>'id' as chrome_style_id,
    jsonb_array_elements(b.response->'style')->'attributes'->>'name' as name,
    jsonb_array_elements(b.response->'style')->'attributes'->>'mfrModelCode' as mfr_model_code
  from nc.stg_avail_hn a
  left join chr.describe_vehicle b on a.vin = b.vin
  where b.style_count > 1) x
where model_code = mfr_model_code

-- so far, matching on model code gives me the correct data
-- all matches: chrome_Style, name/body_style, model code
select *
from (
  select a.*, 
    jsonb_array_elements(b.response->'style')->'attributes'->>'id' as chrome_style_id,
    jsonb_array_elements(b.response->'style')->'attributes'->>'name' as name,
    jsonb_array_elements(b.response->'style')->'attributes'->>'mfrModelCode' as mfr_model_code
  from nc.stg_avail_hn a
  left join chr.describe_vehicle b on a.vin = b.vin
  where b.style_count > 1) x
where model_code = mfr_model_code
  and chrome_style_id_inp = chrome_style_id
  and body_style = name::citext



select *
from chr.describe_vehicle
where vin = '7FARW2H89JE000095'

select *
from dao.vin_reference
where year = 2018
  and make = 'honda'
  and model like 'a%'
  and left(mfr_model_num, 5) = 'RW2H8'





select *  
from (
select vin, style_count, 
  jsonb_array_elements(response->'style')->'model'->>'$value' as name,
  jsonb_array_elements(response->'engine')->'displacement'->>'value' as engine_array
from chr.describe_vehicle) x where name like '%Accord%'
limit 1  
  
-- the trick was in the base query changing 'displacement'->>'value' as engine_array to 'displacement'->'value' as engine_array
-- thereby exposing the object instead of text enabling jsonb_array_elements to work in the outer query
select jsonb_array_elements(engine_array)->>'$value', jsonb_array_elements(engine_array)->'attributes'->>'unit'
from (
select vin, style_count, 
  jsonb_array_elements(response->'style')->'model'->>'$value' as name,
  jsonb_array_elements(response->'engine')->'displacement'->'value' as engine_array
from chr.describe_vehicle) x where name like '%Accord%'
limit 1  




select * from (

select vin, style_count, 
  jsonb_array_elements(response->'style')->'model'->>'$value' as name,
  jsonb_array_elements(response->'engine')->'displacement'->'value' as engine_array,
  jsonb_array_elements(jsonb_array_elements(response->'engine')->'displacement'->'value')->>'$value' as engine_displacement,
  jsonb_array_elements(jsonb_array_elements(response->'engine')->'displacement'->'value')->'attributes'->>'unit' as engine_displacement_units
from chr.describe_vehicle
-- order by jsonb_array_elements(response->'style')->'model'->>'$value'
limit 100
) x where engine_displacement_units = 'liters'
order by vin












              
.
