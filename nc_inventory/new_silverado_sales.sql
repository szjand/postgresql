﻿drop table if exists inpmast;
create temp table inpmast as

drop table if exists nc.silverado_sales cascade;
create table nc.silverado_sales (
  vin citext primary key,
  delivery_date date not null,
  model_year citext not null,
  make citext not null, 
  model citext not null,
  color citext,
  body_style citext);
comment on table nc.silverado_sales is '18 months of new silverado 1500''s sold retail from bopmast/inpmast';
-- 6/4/18 updated, include all sales
insert into nc.silverado_sales  
select a.vin, max(a.delivery_date) as delivery_date, b.year, 
  b.make, b.model, b.color, b.body_style-- select *
from sls.ext_bopmast_partial a
inner join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row = true
  and b.model = 'SILVERADO 1500'
where a.delivery_date between current_date - interval '18 months' and current_date
  and a.vehicle_type = 'N'
  and a.deal_status = 'U'
--   and a.sale_type in ('R','L')
group by a.vin, b.year, b.make, b.model, b.color, b.body_style;


select * from sls.ext_bopmast_partial limit 100


select b.year_month, count(*)
from nc.silverado_sales a
inner join dds.dim_date b on a.delivery_date = b.the_date
group by b.year_month
order by b.year_month desc 

select b.the_date, a.vin
from nc.silverado_sales a
inner join dds.dim_date b on a.delivery_date = b.the_date
  and b.year_month = 201806
order by vin

select * from inpmast
-- there are bad body styles,
-- some missing drive, cab, trim
select body_style, count(*) from inpmast group by body_style

drop table if exists nc.silverado_chrome_attributes cascade;
create table nc.silverado_chrome_attributes (
  vin citext primary key,
  best_style_name citext,
  engine_displacement citext,
  driving_wheels citext,
  style_name citext,
  alt_style_name citext,
  drive_train citext);
--   color_code citext,
--   color_name citext);

comment on column nc.silverado_chrome_attributes.best_style_name is 'attributes::bestStyleName';  
comment on column nc.silverado_chrome_attributes.engine_displacement is 'engine::displacement::value::$value';
comment on column nc.silverado_chrome_attributes.driving_wheels is 'vinDescription::attributes::drivingWheels';
comment on column nc.silverado_chrome_attributes.style_name is 'style::attributes::name';
comment on column nc.silverado_chrome_attributes.alt_style_name is 'style::attributes::altStyleName';
comment on column nc.silverado_chrome_attributes.drive_train is 'style::attributes::drivetrain';
comment on table nc.silverado_chrome_attributes is 'populated from chrome web service, describe-vehicle request, end point: http://beta.rydellvision.com:8888/chrome/describe-vehicle/"vin"';
-- comment on column nc.silverado_chrome_attributes.color_code is 'exteriorColor::attributes::colorCode';
-- comment on column nc.silverado_chrome_attributes.best_style_name is 'exteriorColor::attributes::colorName';


delete
from nc.silverado_chrome_attributes


select * 
from nc.silverado_sales a
left join nc.silverado_chrome_attributes b on a.vin = b.vin
order by alt_style_name


select style_name, alt_style_name, min(vin), max(vin), count(*)
from nc.silverado_chrome_attributes
group by style_name, alt_style_name
order by  style_name, alt_style_name



select * 
from nc.silverado_sales a
left join nc.silverado_chrome_attributes b on a.vin = b.vin
where style_name like '%1LT%'
  and color like '%silver%'
order by delivery_Date  




