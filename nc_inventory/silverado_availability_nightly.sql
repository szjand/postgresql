﻿/*
---------------------------------------------------------------------------------------------
--< start out creating and backfilling base table from jon.test_4: data thru 5/31/18
---------------------------------------------------------------------------------------------

create table nc.vehicles (
  vin citext primary key,
  chrome_style_id integer default -1,
  model_year citext not null,
  make citext not null,
  model citext not null,
  model_code citext not null,
  drive citext not null, 
  cab citext not null,
  trim_level citext,
  engine citext,
  color citext not null);
create index on nc.vehicles(model_year);  
create index on nc.vehicles(make); 
create index on nc.vehicles(model); 
create index on nc.vehicles(model_code); 
create index on nc.vehicles(cab); 
create index on nc.vehicles(trim_level); 
create index on nc.vehicles(engine); 
create index on nc.vehicles(color); 

insert into nc.vehicles 
select distinct vin,chrome_style_id,model_year,make,model,model_code,drive,cab,trim_level,engine,color
from jon.test_4;

create table nc.vehicle_sources (
  source citext primary key);
insert into nc.vehicle_sources values
('factory'),  
('dealer trade'),
('ctp'),
('unwind');

-- date range is [), includes ground, does not include thru_date
-- this accomodates the exclusion constraint
-- and prevents thru_date being < ground_date
create table nc.vehicle_acquisitions (
  vin citext not null references nc.vehicles(vin),
  stock_number citext not null,
  ground_date date not null,
  thru_date date not null default '12/31/9999'::date,
  source citext not null references nc.vehicle_sources,
  exclude using gist (lower(stock_number) with =, daterange(ground_date, coalesce(thru_date, '12/31/9999'::date), '[)') with &&));
insert into nc.vehicle_acquisitions (vin,stock_number,ground_date,thru_date,source)
select vin, stock_number, ground,
  case  
    when delivery_date is null then '12/31/9999'
    else delivery_date
  end,
  case
    when acq_type is null then 'factory'
    when acq_type = 'dealer trade' then 'dealer trade'
    when acq_type = 'from drac/ctp' then 'ctp'
  end
from jon.test_4;  

alter table nc.vehicle_acquisitions
add primary key (stock_number, ground_date)

create index on nc.vehicle_acquisitions(thru_date);
create index on nc.vehicle_acquisitions(stock_number)

alter table nc.vehicle_acquisitions
add constraint ground_thru_check check(ground_date <= thru_date);

-- unwinds, for this blanket update, this is a manual process
select *
from nc.vehicle_acquisitions a
inner join sls.deals b on a.stock_number = b.stock_number
where exists (
  select 1
  from sls.deals
  where vin = b.vin
    and deal_status_code = 'deleted')
order by a.vin, bopmast_id, seq

-- unwinds result in multiple acquisition rows for vehicles
update nc.vehicle_acquisitions
set thru_date = '01/05/2018'
where vin = '1GCVKREC2JZ174396';
insert into nc.vehicle_acquisitions (vin,stock_number,ground_date,thru_date,source) values
('1GCVKREC2JZ174396','32370','01/05/2018','02/26/2018', 'unwind');

update nc.vehicle_acquisitions
set thru_date = '08/20/2018'
where vin = '1GCVKREC5HZ402921';
insert into nc.vehicle_acquisitions (vin,stock_number,ground_date,thru_date,source) values
('1GCVKREC5HZ402921','31475','08/20/2018','09/01/2018', 'unwind');

update nc.vehicle_acquisitions
set thru_date = '02/24/2018'
where vin = '1GCVKREC9JZ137555';
insert into nc.vehicle_acquisitions (vin,stock_number,ground_date,thru_date,source) values
('1GCVKREC9JZ137555','32185','02/24/2018','05/28/2018', 'unwind');

insert into nc.vehicle_acquisitions (vin,stock_number,ground_date,thru_date,source) values
('1GCVKREC9JZ137555','32185','06/11/2018','12/31/9999', 'unwind');

update nc.vehicle_acquisitions
set ground_date = '03/03/2018'
where vin = '1GCVKREC9JZ137555'
  and ground_date = '02/24/2018';

-- 3GCUKREC1HG392187 sold 6/30, unw 7/21 sold 9/2
update nc.vehicle_acquisitions
set thru_date = '06/30/2018'
where vin = '3GCUKREC1HG392187';
insert into nc.vehicle_acquisitions (vin,stock_number,ground_date,thru_date,source) values
('3GCUKREC1HG392187','31011','07/21/2018','09/02/2018', 'unwind');

-- there are a couple more, but current representation is good enuf

create table nc.vehicle_sale_types (
  sale_type citext primary key);
insert into nc.vehicle_sale_types values
('retail'),
('ctp'),
('fleet'),
('dealer trade');


create or replace function nc.stock_number_exists(_stock_number citext)
    returns boolean as
$BODY$
select exists (
  select 1
  from nc.vehicle_acquisitions
  where stock_number = _stock_number);
$BODY$
language sql;

select * from jon.test_4


7-4-18 update vehicle sales where PK is stock_number, ground_date from acquisitions

drop table if exists nc.vehicle_sales;      
-- create table nc.vehicle_sales (
--   stock_number citext not null check(nc.stock_number_exists(stock_number)),
--   vin citext not null references nc.vehicles(vin), 
--   bopmast_id integer not null default -1,
--   delivery_date date not null,
--   sale_type citext not null references nc.vehicle_sale_types(sale_type),
--   constraint vehicle_sales_pkey primary key (stock_number,bopmast_id));
-- create index on nc.vehicle_sales(vin);
-- create index on nc.vehicle_sales(delivery_date);
-- create index on nc.vehicle_sales(sale_type);
-- insert into nc.vehicle_sales
-- 
-- select stock_number,vin,coalesce(bopmast_id,-1),delivery_date,sale_type
-- from (
--   select a.stock_number, a.vin, b.bopmast_id, a.delivery_date,
--     case sale_type
--       when 'to drac/ctp' then 'ctp'
--       when 'dealer trade' then 'dealer trade'
--       else 'retail'
--     end as sale_type
--   from jon.test_4 a
--   left join sls.deals b on a.stock_number = b.stock_number
--     and a.delivery_date = b.delivery_date
--   where a.delivery_date is not null) x
-- group by stock_number,vin,bopmast_id,delivery_date,sale_type  
-- order by delivery_date;

drop table if exists nc.vehicle_sales;
create table nc.vehicle_sales (
  stock_number citext not null,
  ground_date date not null,
  vin citext not null references nc.vehicles(vin), 
  bopmast_id integer not null default -1,
  delivery_date date not null,
  sale_type citext not null references nc.vehicle_sale_types(sale_type),
  foreign key (stock_number,ground_date) references nc.vehicle_acquisitions(stock_number,ground_date),
  primary key (stock_number,ground_date));
create index on nc.vehicle_sales(vin);
create index on nc.vehicle_sales(delivery_date);
create index on nc.vehicle_sales(sale_type);

insert into nc.vehicle_sales
select stock_number,ground,vin,coalesce(bopmast_id,-1),delivery_date,sale_type
from (
  select a.stock_number, a.vin, b.bopmast_id, a.delivery_date, ground,
    case sale_type
      when 'to drac/ctp' then 'ctp'
      when 'dealer trade' then 'dealer trade'
      else 'retail'
    end as sale_type
  from jon.test_4 a
  left join sls.deals b on a.stock_number = b.stock_number
    and a.delivery_date = b.delivery_date
  where a.delivery_date is not null) x
group by stock_number,vin,bopmast_id,delivery_date,sale_type, ground; 

update nc.vehicle_sales
set sale_type = 'fleet'
where stock_number in (-- fleet sales based on fs accounting
  select control
  from (
    select b.the_date, c.account, a.control
    from fin.fact_gl a
    inner join dds.dim_date b on a.date_key = b.date_key
  --     and b.year_month > 201701
    inner join fin.dim_account c on a.account_key = c.account_key
      and c.account in (  
      select d.gl_account
      from fin.fact_fs a 
      inner join fin.dim_fs b on a.fs_key = b.fs_key
  --       and b.year_month > 201701
        and b.page in (5,8,9,10)
        and b.line in (22,43)
        and b.col = 1 -- sales
      inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
        and c.store = 'ry1'
      inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key)) g
  inner join jon.test_4 h on g.control = h.stock_number
    and h.model_year::integer > 2016);

---------------------------------------------------------------------------------------------
--/> start out creating and backfilling base table from jon.test_4: data thru 5/31/18
---------------------------------------------------------------------------------------------
*/

---------------------------------------------------------------------------------------------
--< now, the actual nightly, starting with jon.test_base, nope, that's just a reference
-- actually nightly will be scraping into a staging table that will be like jon.test_4
-- and from that testing and inserting into base tables: nc.vehicles, etc
---------------------------------------------------------------------------------------------
drop table if exists nc.stg_availability cascade;
create table nc.stg_availability (
  the_date date not null,
  row_number integer,
  journal_date date,
  stock_number citext primary key,
  vin citext,
  chrome_style_id integer,
  model_year citext,
  make citext,
  model citext,
  model_code citext,
  drive citext,
  cab citext,
  trim_level citext,
  engine citext,
  color_code citext,
  color citext,
  ground_date date,
  delivery_date date,
  source citext,
  sale_type citext);
create index on nc.stg_availability(vin);
create index on nc.stg_availability(stock_number);

drop table if exists nc.exterior_colors cascade;
create table nc.exterior_colors (
  model_year citext not null,
  make citext not null,
  model citext not null,
  mfr_color_code citext,
  mfr_color_name citext not null,
  constraint exterior_colors_pkey primary key (model_year, make, model, mfr_color_code, mfr_color_name)); 
comment on table nc.exterior_colors is 'subset of color_code and colors from dataone for rydell franchises only, 
  model year greater than 2016'; -- Table has no primary key, too many null mfr_color_code';  
create index on nc.exterior_colors(model_year);  
create index on nc.exterior_colors(make);
create index on nc.exterior_colors(model);
create index on nc.exterior_colors(mfr_color_code);
create index on nc.exterior_colors(mfr_color_name);

truncate nc.exterior_colors;
insert into nc.exterior_colors
select a.year, a.make, a.model, c.mfr_color_code, c.mfr_color_name
from dao.veh_trim_styles a
inner join dao.lkp_veh_ext_color b on a.vehicle_id = b.vehicle_id
inner join dao.def_ext_color c on b.ext_color_id = c.ext_color_id
where year > 2016
  and a.make in ('chevrolet','gmc','buick','cadillac','honda','nissan')
group by a.year, a.make, a.model, c.mfr_color_code, c.mfr_color_name;




/* 
1. acquisitions: 
      factory, dealer trade & ctp from accounting based on inventory account
      unwind from sls.deals
*/  
-- acquisitions: factory, dealer trade, ctp*

-- do i scrape based on date or based on existing records in nc.vehice_acquisitions
-- going with existing records
-- keep in mind, this is V1, can monitor daily and judge performance and adjust as needed
-- just not exists is not good enuf returns all history, so limit the dates to 40 days
/*
drop table if exists avail_1;
create temp table avail_1 as
select f.control as stock_number, f.journal_code, f.the_date, f.amount, 
  case f.journal_code
    when 'PVI' then 'factory'
    when 'CDH' then 'dealer trade'
    when 'GJE' then 'ctp'
  end as acquisition_type,
  g.inpmast_vin as vin, g.year as model_year, g.model_code, g.color as color, g.color_code
from ( -- accounting journal
  select a.control, d.journal_code, max(b.the_date) as the_date, sum(a.amount) as amount
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_date between current_Date - 40 and current_date--'06/22/2018'
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account = '123700'
  inner join fin.dim_journal d on a.journal_key = d.journal_key
    and d.journal_code in ('PVI','CDH','GJE')
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where a.post_status = 'Y'
  group by a.control, d.journal_code
  having sum(a.amount) > 10000) f
inner join arkona.xfm_inpmast g on f.control = g.inpmast_stock_number
  and g.model = 'silverado 1500'
  and g.current_row = true
where not exists (
  select 1
  from nc.vehicle_acquisitions
  where stock_number = f.control) ;

-- journal PVI with no transactions in 126102 (hold back) = dealer trade
update avail_1
set acquisition_type = 'dealer trade'
where stock_number in (
  select stock_number
  from avail_1 a
  left join (
    select a.control, a.doc, d.journal_code, a.amount
    from fin.fact_gl a
    inner join fin.dim_account c on a.account_key = c.account_key
      and c.account = '126102'
    inner join fin.dim_journal d on a.journal_key = d.journal_key
      and d.journal_code = 'PVI'
    where a.post_status = 'Y') b on a.stock_number = b.doc
  where b.doc is null
    and a.acquisition_type = 'factory');


-- this should expose unwinds: basis for unwind acquisitions, 
-- run_date -1: ground date for unwinds
with
  base as (
    select a.run_date, aa.*
    from sls.deals a
    inner join nc.vehicle_acquisitions aa on a.stock_number = aa.stock_number
    inner join nc.vehicles b on a.vin = b.vin
    where a.run_date > current_Date - 60
      and deal_status_code = 'deleted'
      and not exists (
        select 1
        from nc.vehicle_acquisitions
        where stock_number = a.stock_number
          and ground_date = a.run_date - 1))

update nc.vehicle_acquisitions
set thru_date = (select run_date - 1 from base
-- 32584 sold 6/23, unw 6/26
select *
from nc.vehicle_acquisitions
where vin = '1GCVKREC1JZ189083'


select * from nc.vehicle_sales




select *
from nc.vehicle_acquisitions a
inner join (
  select stock_number
  from nc.vehicle_acquisitions
  group by stock_number
  having count(*) > 1) b on a.stock_number = b.stock_number

*/






ok, thats the acquistions, in table avail_1
do i clean up colors now and then start populating nc.stg_availability
what else do i need
ground date
sale info
sale & ground date rational
chrome vehicle info
  mult styles/vin (row number)


--------------------------------------------------------------
/*
 6/27 start over, use the stg_availabity table as the basis
1. insert acquisitions
2. insert sales
3. fill out data as necessary
*/
-- 1. new acquisitions
-- truncate nc.stg_availability

-- take as much from inpmast as possible, verify later against colors, chrome
-- 7/2 chrome style id will come from chrome not inpmast, not sure about model_code yet
truncate nc.stg_availability;
insert into nc.stg_availability (the_date,journal_date,stock_number,
  vin,chrome_style_id,model_year,make, model,
  model_code,color_code,color, source)
select current_date, f.the_date, f.control, 
  g.inpmast_vin, /*g.chrome_style_id,*/ g.year, g.make, g.model, g.model_code, 
  g.color_code, g.color,
  case f.journal_code
    when 'PVI' then 'factory'
    when 'CDH' then 'dealer trade'
    when 'GJE' then 'ctp'
  end as source
from ( -- accounting journal
  select a.control, d.journal_code, max(b.the_date) as the_date, sum(a.amount) as amount
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_date between current_Date - 40 and current_date--'06/22/2018'
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account = '123700'
  inner join fin.dim_journal d on a.journal_key = d.journal_key
    and d.journal_code in ('PVI','CDH','GJE')
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where a.post_status = 'Y'
  group by a.control, d.journal_code
  having sum(a.amount) > 10000) f
inner join arkona.xfm_inpmast g on f.control = g.inpmast_stock_number
  and g.model = 'silverado 1500'
  and g.current_row = true
where not exists (
  select 1
  from nc.vehicle_acquisitions
  where stock_number = f.control) ;
 
-- update source to dealer trade where journal = PVI and no transactions in acct 126102
update nc.stg_availability
set source = 'dealer trade'
where the_date = current_date
  AND stock_number in (
    select stock_number
    from nc.stg_availability a
    left join (
      select a.control, a.doc, d.journal_code, a.amount
      from fin.fact_gl a
      inner join fin.dim_account c on a.account_key = c.account_key
        and c.account = '126102'
      inner join fin.dim_journal d on a.journal_key = d.journal_key
        and d.journal_code = 'PVI'
      where a.post_status = 'Y') b on a.stock_number = b.doc
    where a.the_date = current_date
      and b.doc is null
      and a.source = 'factory');

-- unwind acquisitions
-- test for unwind on same day as sale ?!?
insert into nc.stg_availability (the_date,stock_number,vin,model_year,
  model_code,color_code,color,ground_date,source)
select current_date, a.stock_number, a.vin, b.model_year,
  b.model_Code, c.color_code, b.color, a.run_date - 1, 'unwind'
from sls.deals a
inner join nc.vehicle_acquisitions aa on a.stock_number = aa.stock_number -- to be an unwind, previous acq record must exist
inner join nc.vehicles b on a.vin = b.vin
inner join arkona.xfm_inpmast c on b.vin = c.inpmast_vin
  and c.current_row = true
where a.run_date > current_Date - 60
  and deal_status_code = 'deleted'
  and not exists (
    select 1
    from nc.vehicle_acquisitions
    where stock_number = a.stock_number
      and ground_date = a.run_date - 1);




/*
-- afton why no current event ?
select *
from gmgl.order_info a 
where not exists (
  select 1
  from gmgl.order_info
  where order_number = a.order_number
    and thru_date > current_date)
order by order_number, from_date

DROP VIEW gmgl.order_info cascade;

create materialized view gmgl.order_info as
 SELECT a.order_number,
    a.order_type,
    a.vin,
    a.alloc_group,
    a.vehicle_trim,
    a.model_code,
    a.ship_to_bac,
    b.event_code,
    c.description,
    c.category,
    b.from_date,
    b.thru_date
   FROM gmgl.vehicle_orders a
     JOIN gmgl.vehicle_order_events b ON a.order_number = b.order_number
     JOIN gmgl.vehicle_event_codes c ON b.event_code = c.code
with no data;

refresh materialized view gmgl.order_info;
*/

select * from nc.stg_availability
-- ground date     
-- yikes
-- shit load of null keyper and RO dates 
-- thats because almost all the factory acquisitions are still in transit

-- 6/28 ok, i need to dispense with the notion of one huge coalesecse, deal with each acquisition type separately
-- first, lets get ro/keyper/gmgl.order dates
-- !!! sometimes will not have an accurate ground_date as evidenced (later) by delivery_Date being before ground_date (G34248)
-- ctp: ground = journal_date, ie, the date the vehicle was moved from ctp into inventory and stocknumber was changed 
update nc.stg_availability
set ground_date = journal_date
where source = 'ctp';

-- dealer trades: ground = coalesce(keyper, coaelsece dt ro, journal date)
update nc.stg_availability x
set ground_date = y.ground
from (
  select aa.stock_number, aa.vin, aa.journal_date, aa.source, 
    bb.tsp, bb.pdi, bb.dt, cc.creation_date as keyper,
    coalesce(cc.creation_date, coalesce(bb.dt, aa.journal_date)) as ground
  from nc.stg_availability aa
  left join (
    select d.vin, 
      max(case when c.opcode = 'tsp' then b.the_date end) as tsp,
      max(case when c.opcode = 'pdi' then b.the_date end) as pdi,
      max(case when c.opcode = 'dt' then b.the_date end) as dt
    from ads.ext_fact_repair_order a
    inner join dds.dim_date b on a.opendatekey = b.date_key
    inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
      and c.opcode in ('dt')
    inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
    inner join nc.stg_availability e on d.vin = e.vin
    group by d.vin) bb on aa.vin = bb.vin
  left join ads.keyper_creation_dates cc on aa.stock_number = cc.stock_number  
  where aa.source = 'dealer trade') y
where x.stock_number = y.stock_number; 


-- factory
-- if there is neither keyper nor ro, ground = 12/31/9999
-- what i have to decide is, what do i do with vehicles not here yet
update nc.stg_availability x
set ground_date = y.ground
from (
  select aa.stock_number, aa.vin, aa.journal_date, aa.source, 
    bb.tsp, bb.pdi, bb.dt, cc.creation_date as keyper, dd.event_code, dd.description, dd.from_date,
    coalesce(cc.creation_date, coalesce(bb.tsp, coalesce(bb.pdi, '12/31/9999'::date))) as ground
  from nc.stg_availability aa
  left join (
    select d.vin, 
      max(case when c.opcode = 'tsp' then b.the_date end) as tsp,
      max(case when c.opcode = 'pdi' then b.the_date end) as pdi,
      max(case when c.opcode = 'dt' then b.the_date end) as dt
    from ads.ext_fact_repair_order a
    inner join dds.dim_date b on a.opendatekey = b.date_key
    inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
      and c.opcode in ('tsp', 'dt', 'pdi')
    inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
    inner join nc.stg_availability e on d.vin = e.vin
    group by d.vin) bb on aa.vin = bb.vin
  left join ads.keyper_creation_dates cc on aa.stock_number = cc.stock_number  
  left join (
    SELECT a.order_number, a.order_type, a.vin, a.alloc_group, a.vehicle_trim,
      a.model_code, a.ship_to_bac, b.event_code, c.description, c.category,
      b.from_date
    FROM gmgl.vehicle_orders a
    JOIN gmgl.vehicle_order_events b ON a.order_number = b.order_number
    JOIN gmgl.vehicle_event_codes c ON b.event_code = c.code
    join nc.stg_availability d on a.vin = d.vin
      and d.source = 'factory'
    where b.thru_date = (
        select max(thru_date)
        from gmgl.vehicle_order_events
        where order_number = b.order_number)
    group by a.order_number, a.order_type, a.vin, a.alloc_group, a.vehicle_trim,
      a.model_code, a.ship_to_bac, b.event_code, c.description, c.category, b.from_date) dd on aa.vin = dd.vin  
  where source = 'factory') y  
where x.stock_number = y.stock_number;

-- ok, the i choose, delete if ground = 12/31/9999
delete 
from nc.stg_availability
where ground_date = '12/31/9999';

-- fix colors
-- 1. color does not match or ext_color is null 
-- i don't see how this can be automated yet, so, find them and break
-- keep a record of the anomalies and the fixes
select *
from nc.stg_availability a
left join nc.exterior_colors b on a.color_code = b.mfr_color_code
  and a.model_year = b.model_year
  and a.make = b.make
  and a.model = b.model
where (a.color <> b.mfr_color_name or b.mfr_color_name is null)
  and a.source <> 'unwind' -- presume color has already been checked on previous acquisition/sale
order by stock_number  

-- 1. color_code matches color does not
update nc.stg_availability
set color = 'Deep Ocean Blue Metallic'
where stock_number = 'G33179R';
-- rental, so, the original color must be wrong also
update nc.vehicles 
set color = 'Deep Ocean Blue Metallic'
where vin = '3GCUKSEC6JG225921';
-- 2. no color code and no such color in ext_colors (dealer trade)
select *
from nc.exterior_colors
where mfr_color_name like '%tan%'
  and model = 'silverado 1500'
  
update nc.stg_availability
set color = 'Doeskin Tan'
where vin = '3GCUKREC8JG376168';  

--------
-- run chr.describe vehicle: adds vins from nc.stg_availability
--------

-- 7/1
-- for the new rows
1. verify trim agrees with chrome/vauto
--------------------------------------------------------------------------------------
2. do i need to check on model_code?

/*
7/2/18 
i really do not care about inpmast, whether it agrees with chrome or not
and the rest of this section is me painfully sorting out canonical vehicle build data
-- 5 sources: inpmast, chrome, vauto, global connect(orders), invoices
-- inpmast 488 2018 silverados

-- these are all descrepancies between inpmast and chrome
-- ok, this is seemed goofy, for example, 3GCUKREC3JG415734 has 4 styles but only 2 show up in the query
-- duh, that is because 2 of the 4 have matching model_codes
-- SO, for those vins with multiple chrome styles, chrome is not a core identifying factor
-- THIS IS IMPORTANT NOT YET CLEAR ON HOW IT PANS OUT, BUT GETTING CLOSER 
-- for vins with multiple chrome styles, this query is not relevant
-- *a* need to limit these to style_count = 1
-- 221 where chrome_style_id don't match
-- 5 where model code don't match
select *
from (
  select a.inpmast_vin, a.inpmast_stock_number, a.model_code as inp_model, a.chrome_style_id as inp_style_id,
    jsonb_array_elements(b.response->'style')->'attributes'->>'mfrModelCode' as chr_model, 
    jsonb_array_elements(b.response->'style')->'attributes'->>'name' as chr_style_name, -- shows trim level only place with 1LT, 2LT, etc
    jsonb_array_elements(b.response->'style')->'attributes'->>'id' as chr_style_id, b.style_count,
    c.model_code as gmgl_model, 
    d.trim_level as vauto_trim,
    c.model_code as gmgl_model_code, peg as gmgl_trim,
    b.response
  from arkona.xfm_inpmast a
  left join chr.describe_vehicle b on a.inpmast_vin = b.vin
    -- *a*
    and b.style_count = 1
  left join gmgl.vehicle_orders c on a.inpmast_vin = c.vin
  left join nc.vauto_trim d on a.inpmast_vin = d.vin 
  where a.current_row = true
    and a.year = '2018'
    and a.model = 'silverado 1500') x where inp_style_id <> chr_Style_id::integer -- inp_model <> chr_model --
order by inpmast_vin

-- these with multiple styles in chr.describe_vehicle will probably require a manual intervention
with 
  engine as ( -- necessary to do this in CTE to be able to filter on one of the array values (liters)
    select vin, jsonb_array_elements(jsonb_array_elements(response->'engine')->'displacement'->'value') as displacement
    from chr.describe_vehicle
    where vin in (select vin from nc.stg_availability))
select a.stock_number, a.vin, a.chrome_style_id, a.model_code, 
  b.model_code, b.peg,  
  c.trim_level,
  d.style_count, 
  jsonb_array_elements(d.response->'style')->'attributes'->>'mfrModelCode' as chr_model,
  jsonb_array_elements(d.response->'style')->'attributes'->>'id' as chr_style_id,
  jsonb_array_elements(d.response->'style')->'attributes'->>'name' as chr_style_id,
  e.displacement->>'$value' as engine,
  d.response
from nc.stg_availability a
left join gmgl.vehicle_orders b on a.vin = b.vin
left join nc.vauto_trim c on a.vin = c.vin
left join chr.describe_vehicle d on a.vin = d.vin
left join engine e on a.vin = e.vin and e.displacement ->'attributes'->>'unit' = 'liters'
where d.style_count <> 1
  and source <> 'unwind'; -- unwinds are processed separately

-- yikes some of these already exist in vehicles ?!?!?!
-- calm down, makes sense G34206: dlr trade back on stk# changed and rentals, so, yes of course they exist in vehicles
-- stg_avail based on stk# not existing in acquisitions
with 
  engine as ( -- necessary to do this in CTE to be able to filter on one of the array values (liters)
    select vin, jsonb_array_elements(jsonb_array_elements(response->'engine')->'displacement'->'value') as displacement
    from chr.describe_vehicle
    where vin in (select vin from nc.stg_availability))
select a.stock_number, a.vin, a.chrome_style_id, a.model_code, 
  b.model_code, b.peg,  
  c.trim_level,
  d.style_count, 
  jsonb_array_elements(d.response->'style')->'attributes'->>'mfrModelCode' as chr_model,
  jsonb_array_elements(d.response->'style')->'attributes'->>'id' as chr_style_id,
  jsonb_array_elements(d.response->'style')->'attributes'->>'name' as chr_style_id,
  e.displacement->>'$value' as engine,
  d.response, 
  f.*
from nc.stg_availability a
left join gmgl.vehicle_orders b on a.vin = b.vin
left join nc.vauto_trim c on a.vin = c.vin
left join chr.describe_vehicle d on a.vin = d.vin
left join engine e on a.vin = e.vin and e.displacement ->'attributes'->>'unit' = 'liters'
left join nc.vehicles f on a.vin = f.vin
where style_count = 1
  and source <> 'unwind'; -- unwinds are processed separately

-- 2 steps i believe
those which already exist in nc.vehicles and those that do not
for those that exists, verify that info is correct

-- 4 that exist 
-- i believe all i am looking for here is that the color and chrome_style_id matches between nc.vehicles and nc.stg_availability
-- with 
--   engine as ( -- necessary to do this in CTE to be able to filter on one of the array values (liters)
--     select vin, jsonb_array_elements(jsonb_array_elements(response->'engine')->'displacement'->'value') as displacement
--     from chr.describe_vehicle
--     where vin in (select vin from nc.stg_availability))
ok, G33180R/3GCUKSEC0JG254525 chrome style id  stg_avail: 391727  vehicles: 391728
    G33179R/3GCUKSEC6JG225921  chrome style id  stg_avail: 391727  vehicles: 391728
so, what is the difference between 391727 & 391728
391727: LTZ w/1LZ
391728: LTZ w/2LZ

currently, the only other place to verify trim is vauto
select * from nc.vauto_trim where vin in ('3GCUKSEC0JG254525','3GCUKSEC6JG225921')
only one of them in vauto, -525 and it shows 2LZ, so, stg_Avail is wrong, of course, that value comes from inpmast

select * from gmgl.ext_master_vin_list where vin in ('3GCUKSEC0JG254525','3GCUKSEC6JG225921')

select * from nc.stg_availability where vin in ('3GCUKSEC0JG254525','3GCUKSEC6JG225921')

select * from nc.vehicle_acquisitions where vin in ('3GCUKSEC0JG254525','3GCUKSEC6JG225921')

select * from nc.vehicles where vin in ('3GCUKSEC0JG254525','3GCUKSEC6JG225921')

select * from nc.vauto_Trim where vin in ('3GCUKSEC0JG254525','3GCUKSEC6JG225921')

select vin, jsonb_array_elements(response->'style')->'attributes'->>'id' as chr_style_id from chr.describe_vehicle where vin in ('3GCUKSEC0JG254525','3GCUKSEC6JG225921')

select inpmast_vin, inpmast_stock_number, chrome_style_id from arkona.xfm_inpmast where inpmast_vin in ('3GCUKSEC0JG254525','3GCUKSEC6JG225921') and current_row = true

ok, reprinted invoices, they are both Z71, eg 391728, only inpmast is wrong

so what i want to see now is if all nc.vehicles match chrome
but all these diffs are against chrome records with multiple styles
i fucking need to fix chr.describe_vehicle to all be single styles
select *
from (
  select a.*, jsonb_array_elements(b.response->'style')->'attributes'->>'id' as chr_style_id,
  jsonb_array_elements(b.response->'style')->'attributes'->>'name' as chr_name,
  jsonb_array_elements(b.response->'style')->'attributes'->>'mfrModelCode' as chr_model_code,
  b.style_count
  from nc.vehicles a
  left join chr.describe_vehicle b on a.vin = b.vin) c
where chrome_Style_id <> chr_style_id::integer 

-- shit there are only 8 of them
-- and this takes care of 7 of them
select *
from (
  select a.vin, jsonb_array_elements(a.response->'style')->'attributes'->>'id' as chr_style_id,
    jsonb_array_elements(a.response->'style')->'attributes'->>'name' as chr_name,
    jsonb_array_elements(a.response->'style')->'attributes'->>'mfrModelCode' as chr_model_code,
    b.*
  from chr.describe_vehicle a
  left join gmgl.vehicle_orders b on a.vin = b.vin
  where a.style_count > 1) c
where chr_model_code = model_code
  and right(trim(chr_name), 3) = vehicle_trim

select * from chr.describe_vehicle where vin = '3GCUKSEJ2JG453560'

ok, have been thinking of it wrong, chr.describe vehicles will always have vins with multiple style_counts
my interest is in nc.vehicles and ensuring that it has the correct chrome_style_id
-- so these are the vins of interest, chrome simply tells me that there are multiple styles
-- need to go somewhere else to determine which applis to our vehicle
-- per cahalan, fuck vauto, global connect is the canonical source, so, 1st check the orders data, if not there print the invoice
select *
from (
  select a.*, jsonb_array_elements(b.response->'style')->'attributes'->>'id' as chr_style_id,
  jsonb_array_elements(b.response->'style')->'attributes'->>'name' as chr_name,
  jsonb_array_elements(b.response->'style')->'attributes'->>'mfrModelCode' as chr_model_code,
  b.style_count
  from nc.vehicles a
  left join chr.describe_vehicle b on a.vin = b.vin) c
where style_count > 1

select *
from gmgl.vehicle_orders
where vin in ('3GCUKSEC3JG414879','1GCVKREC8JZ347077','3GCUKREC3JG415734')
-- per orders:
3GCUKREC3JG415734: CK15543 LT w/2LT :: 391726
3GCUKSEC3JG414879: CK15543 LZ w/1LZ :: 391727
-- print invoice
1GCVKREC8JZ347077: CK15753 LT ::391719 

so, only -734 is wrong

update nc.vehicles
set chrome_style_id = 391726,
    trim_level = '2LT'
where vin = '3GCUKREC3JG415734'

-- that takes care of all vins in nc.vehicles where chrome returns multiple styles
-- now verify model/chrome_style_id on all other vins
-------------------- this is the line in the sand ------------------------------------
-------------------- as of this moment, all vins  ------------------------------------
-------------------- in nc.vehicles are correctly ------------------------------------
---------------------        categorized          ------------------------------------
-- fucking finally    
-- there are no rows in nc.vehicles where chrome_style_id or model_code disagrees with chr.describe_vehicle     
select *
from (
  select a.vin, a.model_code, a.trim_level, a.chrome_style_id,
    jsonb_array_elements(b.response->'style')->'attributes'->>'mfrModelCode' as chr_model_code,
    jsonb_array_elements(b.response->'style')->'attributes'->>'id' as chr_style_id,
    jsonb_array_elements(b.response->'style')->'attributes'->>'name' as chr_name
  from nc.vehicles a
  inner join chr.describe_vehicle b on a.vin = b.vin
    and b.style_count = 1) c
where model_code <> chr_model_code 
  or chrome_style_id <> chr_style_id::integer    

*/

-- 7-2
-- new acquisitions where vin already exists in nc.vehicles
insert into nc.vehicle_acquisitions (vin,stock_number,ground_date,source)
select a.vin, a.stock_number, a.ground_date, a.source
from nc.stg_availability a
inner join nc.vehicles aa on a.vin = aa.vin
  and a.source <> 'unwind'; -- unwinds are processed separately


-- new acquisitions where vin does not yet exist in nc.vehicles
-- first, insert into nc.vehicles
insert into nc.vehicles(vin,chrome_style_id,model_year,make,model,
  model_code,drive,cab,trim_level,engine,color)
select f.vin, f.chrome_style_id::integer, model_year, f.make, f.model, f.model_code,
  case
    when f.style like '2WD%' then '2WD'
    when f.style like '4WD%' then '4WD'
  end as drive,
  case 
    when f.style like '%Reg%' then 'reg'
    when f.style like '%Double%' then 'double'
    when f.style like '%Crew%' then 'crew'
    when f.style like '%Ext%' then 'extended'
  end as cab,
  case 
    when f.style like '%1LZ%' then '1LZ'
    when f.style like '%2LZ%' then '2LZ'
    when f.style like '%Work%' then 'WT'
    when f.style like '%High%' then 'high country'
    when f.style like '%1LT%' then '1LT'
    when f.style like '%2LT%' then '2LT'
    when f.style like '%LS%' then 'LS'
    when f.style like '%LT%' then 'LT'
    when f.style like '%Custom%' then 'custom'
  end as trim,  
  f.engine, f.color 
from (    
  with 
    engine as ( -- necessary to do this in CTE to be able to filter on one of the array values (liters)
      select vin, jsonb_array_elements(jsonb_array_elements(response->'engine')->'displacement'->'value') as displacement
      from chr.describe_vehicle
      where vin in (select vin from nc.stg_availability))
  select a.vin, 
    jsonb_array_elements(d.response->'style')->'attributes'->>'id' as chrome_style_id,
    a.model_year, a.make, a.model,
    jsonb_array_elements(d.response->'style')->'attributes'->>'mfrModelCode' as model_code,
    jsonb_array_elements(d.response->'style')->'attributes'->>'name' as style,
    e.displacement->>'$value' as engine,
    a.color
  from nc.stg_availability a
  left join chr.describe_vehicle d on a.vin = d.vin
  left join engine e on a.vin = e.vin and e.displacement ->'attributes'->>'unit' = 'liters'
  where style_count = 1
    and source <> 'unwind' -- unwinds are processed separately
    and not exists (
      select 1
      from nc.vehicles
      where vin = a.vin)) f;

-- now insert the new acquisitions
insert into nc.vehicle_acquisitions  (vin,stock_number,ground_date,source)
select a.vin, a.stock_number, a.ground_date, a.source
from nc.stg_availability a
left join nc.vehicle_acquisitions b on a.stock_number = b.stock_number
  and a.ground_date = b.ground_date
where b.vin is null  
  and a.source <> 'unwind';

-- and finally, the unwind acquisition
-- not sure how this will work for multiple unwinds, only want to update the 
-- must recent acquisition record 
update nc.vehicle_acquisitions x
set thru_date = y.the_date
from (
  select a.vin, a.stock_number, a.ground_date - 1 as the_date
  from nc.stg_availability a
  where source = 'unwind') y
where x.stock_number = y.stock_number;

insert into nc.vehicle_acquisitions  (vin,stock_number,ground_date,source)
select a.vin, a.stock_number, a.ground_date, a.source
from nc.stg_availability a
where source = 'unwind';

select *
from nc.vehicle_acquisitions
where thru_date > current_date

-----------------------------------------------------------------------
--/> acquisitions
-----------------------------------------------------------------------

-- style_id & model_codes for 2018 silverados
select style_id, model_year, model_code, alt_Style_name, count(*)
from (
  select jsonb_array_elements(c.response->'style')->'attributes'->>'id' as style_id,
    c.response->'attributes'->>'modelYear' as model_year,
    jsonb_array_elements(c.response->'style')->'attributes'->>'mfrModelCode' as model_code,
    jsonb_array_elements(c.response->'style')->'attributes'->>'altStyleName' as alt_style_name
  from chr.describe_vehicle c) x
where model_year = '2018'  
group by style_id, model_year, model_code, alt_Style_name
order by model_code, alt_Style_name
--------------------------------------------------------------------------------------

drop table if exists nc.vauto_trim;
create table nc.vauto_trim (
  vin citext primary key,
  stock_number citext not null,
  description citext not null, 
  trim_level citext not null);
insert into nc.vauto_trim  
  select vin, stock_number, vehicle, 
    case length(substring(vehicle, position('1500' in vehicle) + 5, 12))
      when 2 then trim(substring(vehicle, position('1500' in vehicle) + 5))::citext
      when 6 then right(trim(substring(vehicle, position('1500' in vehicle) + 5)),3)::citext
      when 7 then right(trim(substring(vehicle, position('1500' in vehicle) + 5)),3)::citext
      else right(trim(substring(vehicle, position('1500' in vehicle) + 5)),12)::citext
    end as vauto_trim
  from gmgl.ext_vauto
  where vehicle like '%Silverado 1500%';
  
-----------------------------------------------------------------------
--< sales
-----------------------------------------------------------------------
all sales should come from sls.deals except dealer trades
dealer trades will have to come from accounting

select * from nc.stg_availability

may want a second staging table for sales

select max(ground_date) from nc.vehicle_acquisitions

select * from nc.vehicle_sale_types
select * from nc.vehicle_sales where sale_type = 'ctp'
sale types:
  ctp
  fleet
  dealer trade
  retail (inc wholesale ?)
  are there any wholesale except ctp ?


drop table if exists nc.stg_sales;
create table nc.stg_sales (
  stock_number citext,
  vin citext,
  bopmast_id integer,
  delivery_date date,
  sale_type citext,
  trans_Description citext,
  amount numeric(8,2),
  acct_date date,
  ground_date date);

-- accounting should include all sales
-- first step, accounting info and vin from inpmast
insert into nc.stg_sales (stock_number, vin, trans_description, amount, acct_date,ground_date)
select a.control, f.inpmast_vin, e.description, a.amount, b.the_date, g.ground_date
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_date between current_Date - 40 and '06/28/2018'
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account = '123700'
inner join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code = 'VSN'
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
left join arkona.xfm_inpmast f on a.control = f.inpmast_stock_number
  and f.current_row = true  
left join nc.vehicle_acquisitions g on a.control = g.stock_number
  and g.thru_date = ( -- need most recent acquisition
    select max(thru_date)
    from nc.vehicle_acquisitions
    where stock_number = g.stock_number)  
where a.post_status = 'Y'
  and abs(a.amount) > 10000
  and f.model = 'silverado 1500'
  and f.year = '2018'
  and not exists (
    select 1
    from nc.vehicle_sales
    where stock_number = a.control) ;

-- ok the mind fuck is , no deal for dealer trades, but also unwinds (deal deleted)
-- which is why i should  use sls.deals instead of bopmast


update nc.stg_sales x
set bopmast_id = y.bopmast_id,
    delivery_date = y.delivery_date
from (    
  select b.*
  from nc.stg_sales a
  left join (
    select stock_number, bopmast_id, max(seq) as seq, max(delivery_date) as delivery_date
    from sls.deals
    group by stock_number, bopmast_id) b on a.stock_number = b.stock_number) y
where x.stock_number = y.stock_number;

update nc.stg_sales x
set sale_type =
  case
    when bopmast_id is null then 'dealer trade'
    when trans_description like '%RYDELL%' then 'ctp'
  end;

update nc.stg_sales x
set sale_type = 'fleet'
where stock_number in (
  select a.stock_number
  from nc.stg_sales a
  inner join (
    select a.control, b.the_date
    from fin.fact_gl a
    inner join dds.dim_date b on a.date_key = b.date_key
    inner join fin.dim_account c on a.account_key = c.account_key
    inner join (
      select distinct d.gl_account
      from fin.fact_fs a 
      inner join fin.dim_fs b on a.fs_key = b.fs_key
        and b.year_month > 201601
        and b.page in (5,8,9,10)
        and b.line in (22,43)
        and b.col = 1 -- sales
      inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
        and c.store = 'ry1'
      inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key) d on c.account = d.gl_account
    where a.post_status = 'Y') b on a.stock_number = b.control);

update nc.stg_sales
set delivery_date = acct_date
where sale_type = 'dealer trade';

-- and finally
update nc.stg_sales
set sale_type = 'retail'
where sale_type is null;

-- due to the incremental populatuing of stg_sales, can't do proper constraints
-- need to do them now
  
-- ground vs delivery
select *
from nc.stg_sales
where ground_date > delivery_date

-- -- g34248 is an anomaly, used journal date for ground, didn't work out, make it the same as delivery date
-- update nc.stg_sales set ground_date = '06/08/2018' where stock_number = 'g34248';
-- update nc.vehicle_acquisitions
-- set ground_date = '06/08/2018'
-- where stock_number = 'g34248';

-- 7/3 don't know what else to check at this time, go ahead and proceed with the processing
-- looking at the resulting data will reveal anomalies

-- so, 1st, update acquisitions.thru_date with info from stg_sales
-- issue G34248, ground and delivered on the same day
-- in that case thru_date = ground_date which fits the constraint
update nc.vehicle_acquisitions x
set thru_date  = y.delivery_date
from (
  select a.*
  from nc.stg_sales a
  left join nc.vehicle_acquisitions b on a.stock_number = b.stock_number) y
where x.stock_number = y.stock_number
  and x.thru_date = '12/31/9999';

insert into nc.vehicle_sales
select stock_number, ground_date, vin, coalesce(bopmast_id, -1), delivery_date, sale_type
from nc.stg_sales;

--7/4/ ok that's it first take done



--------------------------------------------------------------------------------------------------------
some notion of inventory id, a combination of vin, stock_number, acq date
an unwind would be a different inventory id
sales would be of an inventory_id
the history of a vehicle could include many inventory ids
natural key would be stock_number and acquisition date

a true unwind implies a new bompast id (as opposed to accounting corrections, eg, uncapping a deal to make changes)

when a vehicle is sold, how dows one know which inventory_id it is, it will always be the 
instance where acq.thru_date > current_Date
7/4 so what i did was put ground_date in vehicle_sales, made stock_number/ground_date PK FK to acquisitions




-- ok, all 2018 silverados sold (non dealer trade) are in nc.vehicles
select a.run_date, a.bopmast_id, a.stock_number, a.vin, a.unit_count, a.delivery_date,
  c.*
-- select count(distinct vin) -- 313 2018 silverados have been sold
from sls.deals a
inner join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row = true
  and b.model = 'silverado 1500'
  and b.year = '2018'
left join nc.vehicles c on a.vin = c.vin

select a.run_date, a.bopmast_id, a.stock_number, a.vin, a.unit_count, a.delivery_date,
  c.*
-- select count(distinct vin) -- 313 2018 silverados have been sold
from sls.deals a
inner join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row = true
  and b.model = 'silverado 1500'
  and b.year = '2018'
left join nc.vehicle_sales c on a.stock_number = c.stock_number
  
-----------------------------------------------------------------------
--/> sales
-----------------------------------------------------------------------


-- shit need to do sales first, sold and unwound in the same month, 
-- will not allow insertion of uwnind acquition where sale does not exist
-- this will be one of the last transactions
-- for unwinds
-- if style count is 1 go ahead, if not, throw an error
select a.stock_number, a.vin, a.model_code, b.chrome_style_id, b.model_code as veh_model_code, 
  c.*
-- select *  
from nc.stg_availability a 
left join nc.vehicles b on a.vin = b.vin
left join chr.describe_vehicle c on a.vin = c.vin
where source = 'unwind'

------------------------------------------------------------------------
--< anomalies
------------------------------------------------------------------------

--< g33744/g34206 dealer trade, unwound, retail sale
select a.inpmast_Stock_number, a.status, a.row_from_date, a.row_thru_date, a.current_row
from arkona.xfm_inpmast a
where inpmast_c
order by inpmast_key

timelime:
G33744 ground 4/27 factory
G33744 delivery 5/23 dealer trade
G34206 unwind 6/6
G34206 delivery 6/20 fleet

select *
from nc.vehicle_acquisitions
where vin = '3GCUKREC2JG364680'

update nc.vehicle_acquisitions
set source = 'unwind',
    thru_date = '06/20/2018'
where stock_number = 'G34206';
-- and the sale record already exists, anomaly resolved
select a.*, b.stock_number, b.ground_date, b.thru_Date, b.source, c.delivery_Date, c.bopmast_id, c.sale_type
from nc.vehicles a
left join nc.vehicle_acquisitions b on a.vin = b.vin
left join nc.vehicle_sales c on b.stock_number = c.stock_number
  and c.ground_date = b.ground_date
where a.vin = '3GCUKREC2JG364680'
order by a.vin, b.ground_date  

--/> g33744/g34206 dealer trade, unwound, retail sale

-- comments:
--    at dealer trade: converted to customer
--    at dealer trade unwind: 1. converted to inventory (not always) 2.stock# change
-- 3GTU2NEC8JG187397: dealer trade, unwound, dealer trade !
-- instances of vins with multiple new car stock numbers
select inpmast_vin, inpmast_stock_number, status, type_n_u, inpmast_key, row_From_Date, row_thru_date
from arkona.xfm_inpmast
where inpmast_vin in (
  select inpmast_vin
  from (
    select inpmast_vin, inpmast_stock_number
    from arkona.xfm_inpmast
    where type_n_u = 'N'
      and inpmast_stock_number not like '%R'
    group by inpmast_vin, inpmast_stock_number) a
  group by inpmast_vin
  having count(*) > 1)
order by inpmast_vin, inpmast_key  

------------------------------------------------------------------
-- no nc.vehicle_sales records for 1GCVKREC9JZ137555
select a.*, b.stock_number, b.ground_date, b.thru_Date, b.source, c.delivery_Date, c.bopmast_id, c.sale_type
from nc.vehicles a
left join nc.vehicle_acquisitions b on a.vin = b.vin
left join nc.vehicle_sales c on b.stock_number = c.stock_number
  and c.ground_date = b.ground_date
where a.vin = '1GCVKREC9JZ137555'
order by a.vin, b.ground_date  

need a timeline for this dude
select inpmast_stock_number, status, date_delivered, row_From_date, row_thru_date, current_row
from arkona.xfm_inpmast
where inpmast_vin = '1GCVKREC9JZ137555'
order by inpmast_key

select b.bopname_search_name, a.*
from sls.deals a
left join arkona.xfm_bopname b on a.buyer_bopname_id = b.bopname_Record_key
where vin = '1GCVKREC9JZ137555'
order by run_date

2/24 sale retail
3/3 unwind 
5/28 sale retail
6/11 unwind

update nc.vehicle_sales
set delivery_date = '02/24/2018',
    sale_type = 'retail',
    bopmast_id = 46549
where stock_number = '32185'
  and ground_date = '10/09/2017';

insert into nc.vehicle_sales values
('32185','03/03/2018','1GCVKREC9JZ137555',48396,'05/28/2018','retail');
------------------------------------------------------------------
unwind acquisitions
1GCVKREC5HZ402921 / 31475 ground_date = 08/20
3GCUKREC1HG392187 / 31011 ground date = 07/21

select a.*, b.stock_number, b.ground_date, b.thru_Date, b.source, c.delivery_Date, c.bopmast_id, c.sale_type
from nc.vehicles a
left join nc.vehicle_acquisitions b on a.vin = b.vin
left join nc.vehicle_sales c on b.stock_number = c.stock_number
  and c.ground_date = b.ground_date
where a.vin = '1GCVKREC5HZ402921'
order by a.vin, b.ground_date  

select b.bopname_search_name, a.*
from sls.deals a
left join arkona.xfm_bopname b on a.buyer_bopname_id = b.bopname_Record_key
where vin = '1GCVKREC5HZ402921'
order by run_date

sale 8/17/17 retail
unwind 8/20
sale 8/31/17 retail

update nc.vehicle_acquisitions
set thru_date = '08/17/2017'
where stock_number = '31475'
  and ground_date = '07/13/2017';

update nc.vehicle_sales
set delivery_date = '08/17/2017',
    bopmast_id = 43608
where stock_number = '31475'; 

update nc.vehicle_acquisitions
set ground_date = '08/20/2017',
    thru_date = '08/31/2017'
where stock_number = '31475' 
  and source = 'unwind';

insert into nc.vehicle_sales values
('31475','08/20/2017','1GCVKREC5HZ402921',43826,'08/31/2017','retail');


select a.*, b.stock_number, b.ground_date, b.thru_Date, b.source, c.delivery_Date, c.bopmast_id, c.sale_type
from nc.vehicles a
left join nc.vehicle_acquisitions b on a.vin = b.vin
left join nc.vehicle_sales c on b.stock_number = c.stock_number
  and c.ground_date = b.ground_date
where a.vin = '3GCUKREC1HG392187'
order by a.vin, b.ground_date  

select b.bopname_search_name, a.*
from sls.deals a
left join arkona.xfm_bopname b on a.buyer_bopname_id = b.bopname_Record_key
where vin = '3GCUKREC1HG392187'
order by run_date
sale 6/30/17 retail 42937
unwind 7/21
sale 9/2/17 retail  43918

update nc.vehicle_acquisitions
set thru_date = '06/30/2017'
where stock_number = '31011'
  and source = 'factory';

update nc.vehicle_acquisitions
set ground_date = '07/21/2017',
    thru_date = '09/02/2017'
where stock_number = '31011'
  and source = 'unwind';

update nc.vehicle_sales
set delivery_date = '06/30/2017',
    bopmast_id = 42937
where stock_number = '31011';

insert into nc.vehicle_sales values
('31011','07/21/2017','3GCUKREC1HG392187',43918,'09/02/2017','retail');    
------------------------------------------------------------------------
--/> anomalies
------------------------------------------------------------------------

-- add sku to vehicles
-- only 2 without sku, fuck'em
alter table nc.vehicles
add column sku_id integer;
create index on nc.vehicles(sku_id);

update nc.vehicles x
set sku_id = y.sku_id
from (
  select aa.vin, bb.*
  from nc.vehicles aa
  left join gmgl.sku bb on aa.engine = bb.engine
    and
      case
        when bb.cab = 'regular' then aa.cab = 'reg'
        else aa.cab = bb.cab
      end
    and 
      case
        when bb.vehicle_trim = '1WT' then  aa.trim_level = 'WT'
        when bb.vehicle_trim = 'GAJ' then aa.trim_level = 'high country'
        when bb.vehicle_Trim = '1LS' then aa.trim_level = 'LS'
        when bb.vehicle_trim = 'ST9' then aa.trim_level = 'custom'
        else bb.vehicle_trim = aa.trim_level
      end
    and
      case
        when bb.color = 'unripened green' then aa.color = 'Unripened Green Metallic'
        else bb.color = aa.color
      end) y
where x.vin = y.vin


---------------------------------------------------------------------
-- queries
---------------------------------------------------------------------
-- vehicle - acquisitions - sales
select a.*, b.stock_number, b.ground_date, b.thru_Date, b.source, c.delivery_Date, c.bopmast_id, c.sale_type
from nc.vehicles a
left join nc.vehicle_acquisitions b on a.vin = b.vin
left join nc.vehicle_sales c on b.stock_number = c.stock_number
  and c.delivery_date = b.thru_date
-- where a.vin = '3GCUKREC2JG364680'
order by a.vin, b.ground_date  



-- 365 days sales by sku
select b.sku_id, count(a.stock_number) as sales_365
from nc.vehicle_sales a
inner join nc.vehicles b on a.vin = b.vin
  and b.model_year = '2018'
where a.delivery_date between current_date - 365 and current_date
  and a.sale_type <> 'fleet'
group by b.sku_id
order by count(*) desc

-- 90 days sales by sku
select b.sku_id, count(a.stock_number) as sales_365
from nc.vehicle_sales a
inner join nc.vehicles b on a.vin = b.vin
  and b.model_year = '2018'
where a.delivery_date between current_date - 90 and current_date
  and a.sale_type <> 'fleet'
group by b.sku_id
order by count(*) desc
-----------------------------------------------------------------------------------