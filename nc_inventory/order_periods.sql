﻿
-- without dao: 8409
-- with dao.model_code, mfr_package_code: 8442
select a.order_number, a.order_type, a.vin, a.peg, c.event_code, c.from_date, 
  c.thru_date, d.description, d.category, e.ground_date, a.model_year, b.make, b.model ,f.country_of_mfr, plant
-- select count(distinct a.vin)  -- 1264
from gmgl.vehicle_orders a
join nc.vehicles b on a.vin = b.vin
left join gmgl.vehicle_order_events c on a.order_number = c.order_number
left join gmgl.vehicle_event_codes d on c.event_code = d.code
left join nc.vehicle_acquisitions e on a.vin = e.vin
left join dao.vin_reference f on left(a.vin, 8) || substring(a.vin, 10, 1) || substring(a.vin, 11, 1) = f.vin_pattern
  and a.model_code = f.mfr_model_num
  and a.peg = f.mfr_package_code
where left(c.event_code, 1)::integer < 5
order by a.order_number, c.from_date


select *
from dao.vin_reference
where year = 2019
  and model like 'silverado%'
limit 10 

select *
from gmgl.vehicle_event_codes

do
$$
declare
  vin citext := '3GNAXUEV1KL270055';
  vin_patt citext :=  left(vin, 8) || substring(vin, 10, 1) || substring(vin, 11, 1);
begin
drop table if exists wtf;
create temp table wtf as
select *
from dao.vin_reference
where vin_pattern = vin_patt;
end
$$;
select * from wtf;


select *
from dao.vin_reference
where vin_id = 300073094

select *
from dao.vin_reference
where vin_pattern = '3GNAXUEVKL'

select *
from dao.vin_reference
where vehicle_id = 400902657