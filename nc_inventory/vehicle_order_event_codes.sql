﻿
select *, a.description = b.description
from (
  select 
    case
      when length(code) = 3 then '0' || code
      when code = '0' then '0000'
      when code = '5' then '0005'
      else code
    end as code,
    description, category
  from gmgl.vehicle_event_codes) a
full outer join gmgl.ext_vehicle_order_event_codes b on a.code = b.code
where b.code <> 'Code Description'
order by b.code

select * from gmgl.ext_vehicle_order_event_codes

Vehicle Transfer â Dealer Initiated


Available for Export Shipping NOTE: This event code is applied to an order if the Vehicle Transportation 
and Information Management System 4B event record has an NAIPC Selling Source and Port-of-Exit in the 
Originating and/or Destination CDL.




script to change the event codes in gmgl to restore the leading 0s

select * 
from gmgl.vehicle_event_codes
where length(code) <> 4

update gmgl.vehicle_event_codes
set code = 
  case
    when code = '0' then '0000'
    when code = '5' then '0005'
    when length(code) = 3 then '0' || code
    else code
  end;



shit, need to update the descriptions in gmgl.vehicle_event_codes first 

update gmgl.vehicle_event_codes a
set description = b.description
from (
  select aa.*
  from gmgl.ext_vehicle_order_event_codes aa
  join gmgl.vehicle_event_codes bb on aa.code = bb.code
    and aa.description <> bb.description) b
where a.code = b.code    


now need a script to compare gmgl.ext_vehicle_order_event_codes to gmgl.vehicle_event_codes  

select 'ext', a.*
from (
select code, description from gmgl.ext_vehicle_order_event_codes where code <> 'Code Description'
except
select code, description from gmgl.vehicle_event_codes) a
union all
select 'non_ext', b.* 
from (
select code, description from gmgl.vehicle_event_codes 
except
select code, description from gmgl.ext_vehicle_order_event_codes where code <> 'Code Description') b


select * from gmgl.vehicle_event_codes order by left(code,1) 