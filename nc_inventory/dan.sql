﻿select *
from nc.vehicle_configurations


select * 
from nc.open_orders
order by configuration_id


select a.order_number, a.model_year, a.vin, a.make, a.alloc_group, 
  a.peg, a.model_code, a.color, a.dan,
  coalesce(b.configuration_id, c.configuration_id),
  d.model_year, d.make, d.model, d.level_header, d.config_type
-- select *
from gmgl.vehicle_orders a
left join nc.open_orders b on a.order_number = b.order_number
left join nc.vehicles c on a.vin = c.vin
left join nc.vehicle_configurations d on coalesce(b.configuration_id, c.configuration_id) = d.configuration_id
where a.order_type = 'TRE'
  and a.dan is not null
  and a.model_year > 2018
order by a.make, a.alloc_group, a.dan  
-- order by a.alloc_Group, a.peg, a.color



select a.model_year, a.make, a.alloc_group, 
  a.peg, a.model_code, a.dan,
  d.model, d.level_header, d.config_type
-- select *
from gmgl.vehicle_orders a
left join nc.open_orders b on a.order_number = b.order_number
left join nc.vehicles c on a.vin = c.vin
left join nc.vehicle_configurations d on coalesce(b.configuration_id, c.configuration_id) = d.configuration_id
where a.order_type = 'TRE'
  and a.dan is not null
  and a.model_year > 2018
group by a.model_year, a.make, a.alloc_group, 
  a.peg, a.model_code, a.dan,
  d.model, d.level_header, d.config_type  
order by a.make, a.alloc_group, a.dan  
