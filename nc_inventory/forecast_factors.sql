﻿-- need a list of model_levels with [model_year]
-- with colors
select aa.*, bb.sold_365
from (
  select b.make, b.model, b.level_header, b.cab, initcap(a.color) as color, array_agg(distinct a.model_year)
  from nc.daily_inventory a
  join nc.vehicle_configurations b on a.configuration_id = b.configuration_id
  where a.make in ('chevrolet','gmc','buick','cadillac')
    and b.model_year > 2017
  group by a.color, b.make, b.model, b.level_header, b.cab
  order by b.make, b.model, b.level_header, a.color) aa
left join (
  select make, model, level_header, initcap(color) as color, sum(sold_365) as sold_365
  from nc.model_level_color_stats
  where make in ('chevrolet','gmc','buick','cadillac')
  group by make, model, level_header, color) bb on aa.model = bb.model and aa.level_header = bb.level_header and aa.color = bb.color

-- with colors as array
select aa.*, bb.sold_365
from (
  select b.make, b.model, b.level_header, b.cab, array_agg(distinct a.model_year), array_agg(distinct initcap(a.color) order by initcap(a.color))
  from nc.daily_inventory a
  join nc.vehicle_configurations b on a.configuration_id = b.configuration_id
  where a.make in ('chevrolet','gmc','buick','cadillac')
    and b.model_year > 2017
  group by b.make, b.model, b.level_header, b.cab
  order by b.make, b.model, b.level_header) aa
left join (
  select make, model, level_header, sum(sold_365) as sold_365
  from nc.model_level_color_stats
  where make in ('chevrolet','gmc','buick','cadillac')
  group by make, model, level_header) bb on aa.model = bb.model and aa.level_header = bb.level_header 
 
-- without colors
select aa.*, bb.sold_365
--   years @> '{2018}'
from (
  select b.make, b.model, b.alloc_group, b.level_header, b.cab, array_agg(distinct a.model_year) as years
  from nc.daily_inventory a
  join nc.vehicle_configurations b on a.configuration_id = b.configuration_id
  where a.make in ('chevrolet','gmc','buick','cadillac')
    and b.model_year > 2017
  group by b.make, b.model, b.alloc_group, b.level_header, b.cab
  order by b.make, b.model, b.level_header) aa
left join (
  select make, model, level_header, sum(sold_365) as sold_365
  from nc.model_level_color_stats
  where make in ('chevrolet','gmc','buick','cadillac')
  group by make, model, level_header) bb on aa.model = bb.model and aa.level_header = bb.level_header


select *
from nc.vehicle_configurations
where model like '%3500%'
  and model_year = 2019

select *
from nc.vehicle_configurations
where configuration_id = 6

select*
from nc.vehicles
where configuration_id = 6

update nc.vehicle_configurations
set cab = 'crew',
  level_header = 'Work Truck crew n/a'
where configuration_id = 6;

select *
from nc.model_levels
where model_year = 2019
  and model = 'Silverado 3500HD'
  and seq = 3;

update nc.model_levels
set level_header = 'Work Truck crew n/a'
where model_year = 2019
  and model = 'Silverado 3500HD'
  and seq = 3;

  
select *
from nc.vehicles
where configuration_id = 6

select * from chr.describe_vehicle where vin = '1GB4KVCY2KF125371'

update nc.vehicle_configurations
-- equinox & traverse work for examples

-- model totals
-- combines 2018 & 2019
-- BN47:BR47
-- drop table if exists model_stat_totals;
-- create temp table model_stat_totals as

drop table if exists nc.model_stat_totals cascade;
create table nc.model_stat_totals (
  make citext not null,
  model citext not null, 
  sold_90 integer not null,
  sold_365 integer not null,
  inv_og integer not null,
  inv_transit integer not null,
  inv_system integer not null,
  primary key (make,model));
comment on table nc.model_stat_totals is 'basic sale and inventory counts at the  
  make/model level, truncated nightly and populated from nc.model_color_stats.
  Data required for';  
insert into nc.model_stat_totals  
select make, model, 
  sum(coalesce(sold_90, 0)) as sold_90, sum(coalesce(sold_365, 0)) as sold_365, 
  sum(coalesce(inv_og, 0)) as inv_og, sum(coalesce(inv_transit, 0)) as inv_transit, 
  sum(coalesce(inv_system, 0)) as inv_system
from nc.model_color_stats  
group by make, model;


-- model level totals
-- combines 2018 & 2019
-- B47:G47, J47:P47, R47:X47 ...
-- drop table if exists model_level_stat_totals;
-- create table model_level_stat_totals as

drop table if exists nc.model_level_stat_totals cascade;
create table nc.model_level_stat_totals (
  make citext not null,
  model citext not null, 
  level_header citext not null,
  sold_90 integer not null,
  sold_365 integer not null,
  inv_og integer not null,
  inv_transit integer not null,
  inv_system integer not null,
  primary key (make,model,level_header));
comment on table nc.model_level_stat_totals is 'basic sale and inventory counts at the  
  make/model/level_header level, truncated nightly and populated from nc.model_level_stats';  
insert into nc.model_level_stat_totals 
select make, model, level_header,
  sum(coalesce(sold_90, 0)) as sold_90, sum(coalesce(sold_365, 0)) as sold_365,
  sum(coalesce(inv_og, 0)) as inv_og, sum(coalesce(inv_transit, 0)) as inv_transit, 
  sum(coalesce(inv_system, 0)) as inv_system
from nc.model_level_stats
group by make, model, level_header;

-- model level color totals
-- combines 2018 & 2019
-- B47:G47, J47:P47, R47:X47 ...
-- drop table if exists model_level_color_stat_totals;
-- create table model_level_color_stat_totals as

drop table if exists nc.model_level_color_stat_totals cascade;
create table nc.model_level_color_stat_totals (
  make citext not null,
  model citext not null, 
  level_header citext not null,
  color citext not null,
  sold_90 integer not null,
  sold_365 integer not null,
  inv_og integer not null,
  inv_transit integer not null,
  inv_system integer not null,
  primary key (make,model,level_header,color));
comment on table nc.model_level_color_stat_totals is 'basic sale and inventory counts at the  
  make/model/level_header level, truncated nightly and populated from nc.model_level_color_stats';  
insert into nc.model_level_color_stat_totals 
select a.make, a.model, a.level_header, initcap(a.color) as color,
  sum(coalesce(a.sold_90, 0)) as sold_90, sum(coalesce(a.sold_365, 0)) as sold_365,
  sum(coalesce(a.inv_og, 0)) as inv_og, sum(coalesce(a.inv_transit, 0)) as inv_transit, 
  sum(coalesce(a.inv_system, 0)) as inv_system
from nc.model_level_color_stats a
join nc.vehicle_configurations b on a.configuration_id = b.configuration_id
group by a.make, a.model, a.level_header, a.color;


  
total forecast for feb & mar 
equinox: 46
traverse: 30
-- percentage of model sales by level header
-- stocking level by level header
select x.*, sum(round_0) over (partition by model) as sum_round_0, 
  sum(round_2) over (partition by model) as sum_round_2
from (
select b.make, b.model, b.level_header, b.sold_365 as level_total,
  a.sold_365 as model_total, round(100.0 * b.sold_365/a.sold_365, 2) as perc_of_sales,
  case
    when a.model = 'equinox' then round(46 * round(100.0 * b.sold_365/a.sold_365, 2)/100, 0)
    when a.model = 'traverse' then round(30 * round(100.0 * b.sold_365/a.sold_365, 2)/100, 0)  
  end as round_0,
  case
    when a.model = 'equinox' then round(46 * round(100.0 * b.sold_365/a.sold_365, 2)/100, 2)
    when a.model = 'traverse' then round(30 * round(100.0 * b.sold_365/a.sold_365, 2)/100, 2)  
  end as round_2 
from model_stat_totals a
join model_level_stat_totals b on a.make = b.make and a.model = b.model
) x 
order by model, level_header


-- percentage of level_header sales by color
total forecast for feb & mar 
equinox: 46
traverse: 30

select make, model, level_header, color, desired_stocking_level, currently_on_grd, 
  currently_on_grd - desired_stocking_level as stocking_difference,
  desired_inbound_level, currently_inbound, 
  currently_inbound - desired_inbound_level as inbound_difference
from (  
  select b.make, b.model, b.level_header, c.color, a.sold_365 as model_total,
    b.sold_365 as level_total, c.sold_365 as color_total,
    round(100.0 * b.sold_365/a.sold_365, 2) as level_perc_of_sales,
    case
      when b.sold_365 = 0 then 0
      else round(100.0 * c.sold_365/b.sold_365, 2)
    end as color_percent_of_sales,
    case -- forecast * level_perc_sales * color_perc_sales
      when a.sold_365 = 0 then 0
      else 
        round(
          46 * round(100.0 * b.sold_365/a.sold_365, 2)/100 *
            case
              when b.sold_365 = 0 then 0
              else round(100.0 * c.sold_365/b.sold_365, 2)
            end/100, 2) 
    end as desired_stocking_level,
    c.inv_og as currently_on_grd, c.inv_transit + c.inv_system as currently_inbound,
    case
      when a.sold_365 = 0 then 0
      else 
        round(
          60 * round(100.0 * b.sold_365/a.sold_365, 2)/100 *
            case
              when b.sold_365 = 0 then 0
              else round(100.0 * c.sold_365/b.sold_365, 2)
            end/100, 2) 
    end as desired_inbound_level  
  from nc.model_stat_totals a
  left join nc.model_level_stat_totals b on a.make = b.make and a.model = b.model
  left join nc.model_level_color_stat_totals c on a.make = c.make and a.model = c.model and b.level_header = c.level_header
  where a.model = 'equinox'
--     and b.level_header = 'LT AWD 1.5'
  order by a.make, a.model, b.level_header, c.color) x

union

select 'TOTAL', model, level_header, null::citext as color,
  sum(desired_stocking_level) as desired_stocking_level, 
  sum(currently_on_grd) as currently_on_grd, 
  sum(currently_on_grd - desired_stocking_level) as stocking_difference,
  sum(desired_inbound_level) as desired_inbound_level, 
  sum(currently_inbound) as currently_inbound, 
  sum(currently_inbound - desired_inbound_level) as inbound_difference
from (  
  select b.make, b.model, b.level_header, c.color, a.sold_365 as model_total,
    b.sold_365 as level_total, c.sold_365 as color_total,
    round(100.0 * b.sold_365/a.sold_365, 2) as level_perc_of_sales,
    case
      when b.sold_365 = 0 then 0
      else round(100.0 * c.sold_365/b.sold_365, 2)
    end as color_percent_of_sales,
    case -- forecast * level_perc_sales * color_perc_sales
      when a.sold_365 = 0 then 0
      else 
        round(
          46 * round(100.0 * b.sold_365/a.sold_365, 2)/100 *
            case
              when b.sold_365 = 0 then 0
              else round(100.0 * c.sold_365/b.sold_365, 2)
            end/100, 2) 
    end as desired_stocking_level,
    c.inv_og as currently_on_grd, c.inv_transit + c.inv_system as currently_inbound,
    case
      when a.sold_365 = 0 then 0
      else 
        round(
          60 * round(100.0 * b.sold_365/a.sold_365, 2)/100 *
            case
              when b.sold_365 = 0 then 0
              else round(100.0 * c.sold_365/b.sold_365, 2)
            end/100, 2) 
    end as desired_inbound_level  
  from nc.model_stat_totals a
  left join nc.model_level_stat_totals b on a.make = b.make and a.model = b.model
  left join nc.model_level_color_stat_totals c on a.make = c.make and a.model = c.model and b.level_header = c.level_header
  where a.model = 'equinox') x
group by model, level_header  
order by model, level_header, color, make
  
select distinct level_header from nc.model_levels where model = 'equinox' order by level_header

select * from model_level_color_stat_totals


document the formulas for desired_stocking_level and desired_inbound_level

desired stocking level = two_month_forecast * level_perc_sales * color_perc_sales  (sold_365)
desired inbound level = total_3_4_months_out * level_perc_sales * color_perc_sales
do 
$$
declare
  _two_month_forecast integer := 46;
  _months_supply_on_lot integer := 1;
  _total_3_4_months_out integer = 60;
  _make citext := 'chevrolet';
  _model citext := 'traverse';
begin
  drop table if exists wtf;
  create temp table wtf as 
  select make, model, level_header, color, desired_stocking_level, currently_on_grd, 
    currently_on_grd - desired_stocking_level as stocking_difference,
    desired_inbound_level, currently_inbound, 
    currently_inbound - desired_inbound_level as inbound_difference
  from (  
    select b.make, b.model, b.level_header, c.color, a.sold_365 as model_total,
      b.sold_365 as level_total, c.sold_365 as color_total,
      round(100.0 * b.sold_365/a.sold_365, 2) as level_perc_of_sales,
      case
        when b.sold_365 = 0 then 0
        else round(100.0 * c.sold_365/b.sold_365, 2)
      end as color_percent_of_sales,
      case -- two_month_forecast * level_perc_sales * color_perc_sales  (sold_365)
        when a.sold_365 = 0 then 0
        else 
          round(
            _two_month_forecast * round(100.0 * b.sold_365/a.sold_365, 2)/100 *
              case
                when b.sold_365 = 0 then 0
                else round(100.0 * c.sold_365/b.sold_365, 2)
              end/100, 2) 
      end as desired_stocking_level,
      c.inv_og as currently_on_grd, c.inv_transit + c.inv_system as currently_inbound,
      case -- _total_3_4_months_out * level_perc_sales * color_perc_sales
        when a.sold_365 = 0 then 0
        else 
          round(
            _total_3_4_months_out * round(100.0 * b.sold_365/a.sold_365, 2)/100 *
              case
                when b.sold_365 = 0 then 0
                else round(100.0 * c.sold_365/b.sold_365, 2)
              end/100, 2) 
      end as desired_inbound_level  
    from nc.model_stat_totals a
    left join nc.model_level_stat_totals b on a.make = b.make and a.model = b.model
    left join nc.model_level_color_stat_totals c on a.make = c.make and a.model = c.model and b.level_header = c.level_header
    where a.model = _model
    order by a.make, a.model, b.level_header, c.color) x

  union

  select 'TOTAL', model, level_header, null::citext as color,
    sum(desired_stocking_level) as desired_stocking_level, 
    sum(currently_on_grd) as currently_on_grd, 
    sum(currently_on_grd - desired_stocking_level) as stocking_difference,
    sum(desired_inbound_level) as desired_inbound_level, 
    sum(currently_inbound) as currently_inbound, 
    sum(currently_inbound - desired_inbound_level) as inbound_difference
  from (  
    select b.make, b.model, b.level_header, c.color, a.sold_365 as model_total,
      b.sold_365 as level_total, c.sold_365 as color_total,
      round(100.0 * b.sold_365/a.sold_365, 2) as level_perc_of_sales,
      case
        when b.sold_365 = 0 then 0
        else round(100.0 * c.sold_365/b.sold_365, 2)
      end as color_percent_of_sales,
      case -- forecast * level_perc_sales * color_perc_sales (sold_365)
        when a.sold_365 = 0 then 0
        else 
          round(
            _two_month_forecast * round(100.0 * b.sold_365/a.sold_365, 2)/100 *
              case
                when b.sold_365 = 0 then 0
                else round(100.0 * c.sold_365/b.sold_365, 2)
              end/100, 2) 
      end as desired_stocking_level,
      c.inv_og as currently_on_grd, c.inv_transit + c.inv_system as currently_inbound,
      case
        when a.sold_365 = 0 then 0
        else 
          round(
            _total_3_4_months_out * round(100.0 * b.sold_365/a.sold_365, 2)/100 *
              case
                when b.sold_365 = 0 then 0
                else round(100.0 * c.sold_365/b.sold_365, 2)
              end/100, 2) 
      end as desired_inbound_level  
    from nc.model_stat_totals a
    left join nc.model_level_stat_totals b on a.make = b.make and a.model = b.model
    left join nc.model_level_color_stat_totals c on a.make = c.make and a.model = c.model and b.level_header = c.level_header
    where a.model = _model) x
  group by model, level_header  
  order by model, level_header, make, color;

end
$$;
select * from wtf;  


step thru UI steps
1. select model

create or replace function nc.get_forecast_model()
returns setof json as
$BODY$
/*
select nc.get_forecast_model();
*/
select row_to_json(a) as model
from (
  select array (
    select make || ' ' || model
    from nc.forecast_models
    order by make, model) as models) a;
$BODY$
language SQL;

2. select forecast month(s)

CREATE OR REPLACE FUNCTION nc.get_forecast_months()
  RETURNS SETOF json AS
$BODY$
/*
select nc.get_forecast_months();
*/
select row_to_json(c)
from (
select array(
  select b.month_name
  from nc.forecast a
  join dds.dim_date b on a.year_month = b.year_month
  group by a.year_month, b.month_name
  order by a.year_month) as months ) c
$BODY$
LANGUAGE sql;


CREATE OR REPLACE FUNCTION nc.get_forecast_3_4_months_out()
  RETURNS SETOF json AS
$BODY$
/*
select nc.get_forecast_3_4_months_out();
*/
select row_to_json(c)
from (
select array(
  select b.month_name
  from nc.forecast a
  join dds.dim_date b on a.year_month = b.year_month
  group by a.year_month, b.month_name
  order by a.year_month) as months ) c
$BODY$
LANGUAGE sql;



3. and the forecast status

desired stocking level = two_month_forecast * level_perc_sales * color_perc_sales  (sold_365)
desired inbound level = total_3_4_months_out * level_perc_sales * color_perc_sales

select * from nc.get_model_forecast_status('["March","April"]','["May","June"]','["Chevrolet Traverse"]')
where make = 'TOTAL'


do
$$
declare 
  _forecast_months json := '["March","April"]';
  _3_4_months_out json := '["May","June"]';
  _models json := '["Chevrolet 1500 Crew"]'; 

  _model citext := (
    select models 
    from json_array_elements_text(_models) as models);
            
  _total_forecast integer := (
    select sum(forecast) 
    from nc.forecast
    where make || ' ' || model = _model
      and year_month in (
        select distinct year_month
        from dds.dim_date
        where the_year = (select extract(year from current_Date)::integer)
          and month_name in (select months from json_array_elements_text(_forecast_months) as months)));

  _total_3_4_months_out integer := (
    select sum(forecast) 
    from nc.forecast
    where make || ' ' || model = _model
      and year_month in (
        select distinct year_month
        from dds.dim_date
        where the_year = (select extract(year from current_Date)::integer)
          and month_name in (select months from json_Array_elements_text(_3_4_months_out) as months)));
          

    
begin

drop table if exists wtf;
create temp table wtf as   
  select make, model, level_header, color, desired_stocking_level, currently_on_grd, 
    currently_on_grd - desired_stocking_level as stocking_difference,
    desired_inbound_level, currently_inbound, 
    currently_inbound - desired_inbound_level as inbound_difference
  from (  
    select b.make, b.model, b.level_header, c.color, a.sold_365 as model_total,
      b.sold_365 as level_total, c.sold_365 as color_total,
      round(100.0 * b.sold_365/a.sold_365, 2) as level_perc_of_sales,
      case
        when b.sold_365 = 0 then 0
        else round(100.0 * c.sold_365/b.sold_365, 2)
      end as color_percent_of_sales,
      case -- _total_forecast * level_perc_sales * color_perc_sales  (sold_365)
        when a.sold_365 = 0 then 0
        else 
          round(
            _total_forecast * round(100.0 * b.sold_365/a.sold_365, 2)/100 *
              case
                when b.sold_365 = 0 then 0
                else round(100.0 * c.sold_365/b.sold_365, 2)
              end/100, 2) 
      end as desired_stocking_level,
      c.inv_og as currently_on_grd, c.inv_transit + c.inv_system as currently_inbound,
      case -- _total_3_4_months_out * level_perc_sales * color_perc_sales
        when a.sold_365 = 0 then 0
        else 
          round(
            _total_3_4_months_out * round(100.0 * b.sold_365/a.sold_365, 2)/100 *
              case
                when b.sold_365 = 0 then 0
                else round(100.0 * c.sold_365/b.sold_365, 2)
              end/100, 2) 
      end as desired_inbound_level   
    from nc.model_stat_totals a
    join ( -- convert forecast makes & models to config makes & models
      select distinct c.make, c.model
      from nc.forecast_models a
      join nc.forecast_model_configurations b on a.category = b.category
        and a.make = b.make
        and a.model = b.model
      join nc.vehicle_configurations c on b.configuration_id = c.configuration_id  
      where a.make || ' ' || a.model = _model) aa on a.make = aa.make and a.model = aa.model        
    left join nc.model_level_stat_totals b on a.make = b.make and a.model = b.model
    left join nc.model_level_color_stat_totals c on a.make = c.make and a.model = c.model and b.level_header = c.level_header
    order by a.make, a.model, b.level_header, c.color) x


  union

  select 'TOTAL', model, level_header, null::citext as color,
    sum(desired_stocking_level) as desired_stocking_level, 
    sum(currently_on_grd) as currently_on_grd, 
    sum(currently_on_grd - desired_stocking_level) as stocking_difference,
    sum(desired_inbound_level) as desired_inbound_level, 
    sum(currently_inbound) as currently_inbound, 
    sum(currently_inbound - desired_inbound_level) as inbound_difference
  from (  
    select b.make, b.model, b.level_header, c.color, a.sold_365 as model_total,
      b.sold_365 as level_total, c.sold_365 as color_total,
      round(100.0 * b.sold_365/a.sold_365, 2) as level_perc_of_sales,
      case
        when b.sold_365 = 0 then 0
        else round(100.0 * c.sold_365/b.sold_365, 2)
      end as color_percent_of_sales,
      case -- forecast * level_perc_sales * color_perc_sales (sold_365)
        when a.sold_365 = 0 then 0
        else 
          round(
            _total_forecast * round(100.0 * b.sold_365/a.sold_365, 2)/100 *
              case
                when b.sold_365 = 0 then 0
                else round(100.0 * c.sold_365/b.sold_365, 2)
              end/100, 2) 
      end as desired_stocking_level,
      c.inv_og as currently_on_grd, c.inv_transit + c.inv_system as currently_inbound,
      case
        when a.sold_365 = 0 then 0
        else 
          round(
            _total_3_4_months_out * round(100.0 * b.sold_365/a.sold_365, 2)/100 *
              case
                when b.sold_365 = 0 then 0
                else round(100.0 * c.sold_365/b.sold_365, 2)
              end/100, 2) 
      end as desired_inbound_level  
    from nc.model_stat_totals a
    join (-- convert forecast makes & models to config makes & models
      select distinct c.make, c.model
      from nc.forecast_models a
      join nc.forecast_model_configurations b on a.category = b.category
        and a.make = b.make
        and a.model = b.model
      join nc.vehicle_configurations c on b.configuration_id = c.configuration_id  
      where a.make || ' ' || a.model = _model) aa on a.make = aa.make and a.model = aa.model      
    left join nc.model_level_stat_totals b on a.make = b.make and a.model = b.model
    left join nc.model_level_color_stat_totals c on a.make = c.make and a.model = c.model and b.level_header = c.level_header) x
  group by model, level_header  
  order by model, level_header, make, color;

      

    
end
$$;

select * from wtf where make = 'TOTAL';

/*
2/15 see the partial fix in nightly

-- 2/14 sidetrack, 2018 traverse, autobook does not show drive in the trim, why do i
-- because i am using the header_level, 2018 traverse config_type = td, 2019 config_type = t;
-- so, if am worried about matching header_levels across model years
select * from nc.vehicle_configurations where model_year = 2018 and model = 'equinox'

select * from nc.vehicles where configuration_id = 33


select model_year, make, model, array_agg(distinct config_type)
from nc.vehicle_configurations
where model_year > 2017
group by make, model, model_year
order by make, model, model_year


select model_year, make, model, config_type
from nc.vehicle_configurations
where model_year = 2018
group by make, model, model_year, config_type
order by make, model, model_year, config_type

select model_year, make, model, config_type
from nc.vehicle_configurations
where model_year = 2019
group by make, model, model_year, config_type
order by make, model, model_year, config_type


select * 
from (
  select model_year, make, model, config_type, array_agg(configuration_id)
  from nc.vehicle_configurations
  where model_year = 2018
  group by make, model, model_year, config_type
  order by make, model, model_year, config_type) a
join (
  select model_year, make, model, config_type, array_agg(configuration_id)
  from nc.vehicle_configurations
  where model_year = 2019
  group by make, model, model_year, config_type
  order by make, model, model_year, config_type) b on a.make = b.make and a.model = b.model and a.config_type <> b.config_type
where a.make in ('chevrolet','gmc','buick','cadillac')  

i am going down the rabbit hole, 
forget all this fucking fine tuning stuff for now, just get a page up for ben and see what we get

2/15/19
possible temporary fix
update the significant differences (traverse, ...) maybe just in vehicle_configurations.level_header
update the shorter to the longer (config_type)
so, for traverse, 2018 is td, 2019 is t, so make 2019 td
this will give better combined year data for generating forecast

after the nightly generation of model_levels

or maybe generate model_level without the model_year in the grouping

select * 
from (
  select model_year, make, model, config_type, level_header
  from nc.vehicle_configurations
  where model_year = 2018
  group by make, model, model_year, config_type, level_header
  order by make, model, model_year, config_type) a
join (
  select model_year, make, model, config_type, level_header
  from nc.vehicle_configurations
  where model_year = 2019
  group by make, model, model_year, config_type, level_header
  order by make, model, model_year, config_type) b on a.make = b.make and a.model = b.model and a.config_type <> b.config_type
where a.make in ('chevrolet','gmc','buick','cadillac')  
*/



/*

2/22/19
-- ***
fix the fucked up
was returning all silverado 1500's, not just the appropriate cab
  added the join to level header on : nc.model_level_stat_totals b on aa.make = b.make and aa.model = b.model and aa.level_header = b.level_header 
changed left joins to inner
changed input parameter of model to text from json
corrected a couple of typos  
*/



do
$$
declare 
  _forecast_months json := '["March","April"]';
  _3_4_months_out json := '["May","June"]';
  _models citext := 'Chevrolet 1500 Reg'; -- ***

  _model citext := _models; -- ***
            
  _total_forecast integer := (
    select sum(forecast) 
    from nc.forecast
    where make || ' ' || model = _model
      and year_month in (
        select distinct year_month
        from dds.dim_date
        where the_year = (select extract(year from current_Date)::integer)
          and month_name in (select months from json_array_elements_text(_forecast_months) as months)));

  _total_3_4_months_out integer := (
    select sum(forecast) 
    from nc.forecast
    where make || ' ' || model = _model
      and year_month in (
        select distinct year_month
        from dds.dim_date
        where the_year = (select extract(year from current_Date)::integer)
          and month_name in (select months from json_Array_elements_text(_3_4_months_out) as months)));
          

    
begin

drop table if exists wtf;
create temp table wtf as   
  select make, model, level_header, color, desired_stocking_level, currently_on_grd, 
    currently_on_grd - desired_stocking_level as stocking_difference,
    desired_inbound_level, currently_inbound, 
    currently_inbound - desired_inbound_level as inbound_difference
  from (  
    select b.make, b.model, 
      (b.level_header || '  ' ||
      case 
        when 
          exists (
            select 1 
            from nc.vehicle_configurations 
            where level_header = b.level_header 
              and model_year = 2018) 
          and exists (
            select 1 
            from nc.vehicle_configurations 
            where level_header = b.level_header 
              and model_year = 2019)  then '(BOTH)'   
        when 
          exists (
            select 1 
            from nc.vehicle_configurations 
            where level_header = b.level_header 
              and model_year = 2018) then '(2018)'                          
        when 
          exists (
            select 1 
            from nc.vehicle_configurations 
            where level_header = b.level_header 
              and model_year = 2019) then '(2019)'::citext    
      end)::citext   as level_header,      
      (c.color || '  ' ||
      case 
        when 
          exists (
            select 1 
            from nc.vehicles 
            where color = c.color
              and model_year = 2018) 
          and exists (
            select 1 
            from nc.vehicles 
            where color = c.color
              and model_year = 2019) then '(BOTH)'   
        when 
          exists (
            select 1 
            from nc.vehicles 
            where color = c.color
              and model_year = 2018) then '(2018)'    
        when 
          exists (
            select 1 
            from nc.vehicles 
            where color = c.color
              and model_year = 2019) then '(2019)'              
      end)::citext   as color, 
      a.sold_365 as model_total,
      b.sold_365 as level_total, c.sold_365 as color_total,
      round(100.0 * b.sold_365/a.sold_365, 2) as level_perc_of_sales,
      case
        when b.sold_365 = 0 then 0
        else round(100.0 * c.sold_365/b.sold_365, 2)
      end as color_percent_of_sales,
      case -- _total_forecast * level_perc_sales * color_perc_sales  (sold_365)
        when a.sold_365 = 0 then 0
        else 
          round(
            _total_forecast * round(100.0 * b.sold_365/a.sold_365, 2)/100 *
              case
                when b.sold_365 = 0 then 0
                else round(100.0 * c.sold_365/b.sold_365, 2)
              end/100, 2) 
      end as desired_stocking_level,
      c.inv_og as currently_on_grd, c.inv_transit + c.inv_system as currently_inbound,
      case -- _total_3_4_months_out * level_perc_sales * color_perc_sales
        when a.sold_365 = 0 then 0
        else 
          round(
            _total_3_4_months_out * round(100.0 * b.sold_365/a.sold_365, 2)/100 *
              case
                when b.sold_365 = 0 then 0
                else round(100.0 * c.sold_365/b.sold_365, 2)
              end/100, 2) 
      end as desired_inbound_level   
    from nc.model_stat_totals a
    join ( -- convert forecast makes & models to config makes & models
      select distinct c.make, c.model, c.level_header -- ***
      from nc.forecast_models a
      join nc.forecast_model_configurations b on a.category = b.category
        and a.make = b.make
        and a.model = b.model
      join nc.vehicle_configurations c on b.configuration_id = c.configuration_id  
      where a.make || ' ' || a.model = _model) aa on a.make = aa.make and a.model = aa.model        
    join nc.model_level_stat_totals b on aa.make = b.make and aa.model = b.model and aa.level_header = b.level_header -- ***
    join nc.model_level_color_stat_totals c on aa.make = c.make and aa.model = c.model and b.level_header = c.level_header
    order by a.make, a.model, b.level_header, c.color) x


  union

  select 'TOTAL', model, level_header, null::citext as color,
    sum(desired_stocking_level) as desired_stocking_level, 
    sum(currently_on_grd) as currently_on_grd, 
    sum(currently_on_grd - desired_stocking_level) as stocking_difference,
    sum(desired_inbound_level) as desired_inbound_level, 
    sum(currently_inbound) as currently_inbound, 
    sum(currently_inbound - desired_inbound_level) as inbound_difference
  from (  
     select b.make, b.model, 
      (b.level_header || '  ' ||
      case 
        when 
          exists (
            select 1 
            from nc.vehicle_configurations 
            where level_header = b.level_header 
              and model_year = 2018) 
          and exists (
            select 1 
            from nc.vehicle_configurations 
            where level_header = b.level_header 
              and model_year = 2019)  then '(2018 2019)'
        when 
          exists (
            select 1 
            from nc.vehicle_configurations 
            where level_header = b.level_header 
              and model_year = 2018) then '(2018)'                       
        when 
          exists (
            select 1 
            from nc.vehicle_configurations 
            where level_header = b.level_header 
              and model_year = 2019) then '(2019)' 
      end)::citext   as level_header, c.color,   
      a.sold_365 as model_total,
      b.sold_365 as level_total, c.sold_365 as color_total,
      round(100.0 * b.sold_365/a.sold_365, 2) as level_perc_of_sales,
      case
        when b.sold_365 = 0 then 0
        else round(100.0 * c.sold_365/b.sold_365, 2)
      end as color_percent_of_sales,
      case -- forecast * level_perc_sales * color_perc_sales (sold_365)
        when a.sold_365 = 0 then 0
        else 
          round(
            _total_forecast * round(100.0 * b.sold_365/a.sold_365, 2)/100 *
              case
                when b.sold_365 = 0 then 0
                else round(100.0 * c.sold_365/b.sold_365, 2)
              end/100, 2) 
      end as desired_stocking_level,
      c.inv_og as currently_on_grd, c.inv_transit + c.inv_system as currently_inbound,
      case
        when a.sold_365 = 0 then 0
        else 
          round(
            _total_3_4_months_out * round(100.0 * b.sold_365/a.sold_365, 2)/100 *
              case
                when b.sold_365 = 0 then 0
                else round(100.0 * c.sold_365/b.sold_365, 2)
              end/100, 2) 
      end as desired_inbound_level  
    from nc.model_stat_totals a
    join (-- convert forecast makes & models to config makes & models
      select distinct c.make, c.model, c.level_header -- ***
      from nc.forecast_models a
      join nc.forecast_model_configurations b on a.category = b.category
        and a.make = b.make
        and a.model = b.model
      join nc.vehicle_configurations c on b.configuration_id = c.configuration_id  
      where a.make || ' ' || a.model = _model) aa on a.make = aa.make and a.model = aa.model      
    join nc.model_level_stat_totals b on aa.make = b.make and aa.model = b.model and aa.level_header = b.level_header -- ***
    join nc.model_level_color_stat_totals c on a.make = c.make and a.model = c.model and b.level_header = c.level_header) x
  group by model, level_header  
  order by model, level_header, make, color;

      

    
end
$$;

select * from wtf --where make = 'TOTAL';


