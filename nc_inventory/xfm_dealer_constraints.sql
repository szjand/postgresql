﻿select *
from nc.ext_dealer_constraints
where f1 is not null

-- drop table if exists step_1 cascade;
-- -- eliminates report header, blank & superfluous rows
-- create temp table step_1 as
-- select *
-- from (
--   select row_number() over (), a.*
--   from nc.ext_dealer_constraints a
--   where f1 is not null) b
-- where row_number > 3;

drop table if exists step_1 cascade;
-- eliminates report header, blank & superfluous rows
create temp table step_1 as
select *
from (
  select row_number() over (), a.*
  from nc.ext_dealer_constraints a
  where f1 is not null) b
where row_number > 3
  and trim(f1) not in ('Constraint','Number');

select * from step_1;


-- for a subset of division/model_year/alloc_goup this gives me a group number
drop table if exists alloc_group_groups;
create temp table alloc_group_groups as
select trim(replace(f1, ':', '')), f2, row_number() over (),  
  (row_number()OVER(ORDER BY row_number) - 1 ) / 3 + 1 as group_no,
  row_number as division_row_number
from step_1
where f1 in('Division: ','Model Year: ','Allocation Group: ')
order by row_number;

select * from alloc_group_groups

drop table if exists nc.tmp_alloc_groups cascade;
create table nc.tmp_alloc_groups (
  division citext,
  model_year integer,
  alloc_group citext,
  division_row_number bigint,
  primary key(division,model_year,alloc_group));

insert into nc.tmp_alloc_groups 
select a.the_data[1],a.the_data[2]::integer,a.the_data[3], division_row_number
from (
  select group_no, array_agg(f2 order by row_number) the_data, min(division_row_number) as division_row_number
  from alloc_group_groups
  group by group_no) a;

select  * from nc.tmp_alloc_groups 

-- this is just a minimal set of data from ext, row_number and f1, f2, f3
-- to work with shaping the transformation, once i get it, i can add the additional columns
drop table if exists minim cascade;
create temp table minim as
select row_number, f1, f2, f3
from step_1;

select * from minim;

select row_number, f2, coalesce(lead(row_number, 1) over () - 1, (select max(row_number) from minim))
from minim
where trim(f1) = 'Division:'

-- aha, this look like it
select a.division, a.model_year, a.alloc_group, c.f1, c.f2
from nc.tmp_alloc_groups a
left join (
  select row_number, f2, coalesce(lead(row_number, 1) over () - 1, (select max(row_number) from minim)) as max_row_number
  from minim
  where trim(f1) = 'Division:') b on a.division_row_number = b.row_number
left join minim c on c.row_number between b.row_number and b.max_row_number  
  and c.f1 not in('Division: ','Model Year: ','Allocation Group: ')
  

need TPP Start & End

--------------------------------------------------------------------------------------  
4/4/19
finally, believe i have the file in a parseable state
run thru the xfm process, make sure the new data is good
then
probably the first priority then is a data model and process flow

--------------------------------------------------------------------------------------
drop table if exists nc.ext_dealer_constraints_1 cascade;
create table nc.ext_dealer_constraints_1 (
  f1 citext);
comment on table nc.ext_dealer_constraints_1 is 'raw data parsed from global connect report, dealer_constraints_distribution_report.pdf,
  each row into a single attribute';
  
drop table if exists nc.ext_dealer_constraints_2 cascade;
create table nc.ext_dealer_constraints_2 (
  row_number integer  primary key,
  f1 citext);
comment on table nc.ext_dealer_constraints_2 is 'add row number to data from nc.ext_dealer_constraints_1';

insert into nc.ext_dealer_constraints_2
select row_number() over (), a.*
from nc.ext_dealer_constraints_1 a;

-- replace , with ; in fields surrounded by "" that contain , within the ""
-- fucked up the split_part on , for parsing out the fields
update nc.ext_dealer_constraints_2 z
set f1 = x.new_f1
from (
  select row_number, -- f1, substring(f1, the_first, the_last-the_first+1), 
    replace(f1, substring(f1, the_first, the_last-the_first+1), 
    replace( substring(f1, the_first, the_last-the_first+1), ',', ';'))  as new_f1
  from (
    select row_number, f1 , position('"' in f1) as the_first, 
      position('"' in f1) + position('"' in substring(f1, position('"' in f1) + 1, length(f1))) as the_last
    from nc.ext_dealer_constraints_2
    where f1 like '%"%') a
  where position(',' in substring(f1, the_first, the_last-the_first+1)) > 0) x -- only "" that contain a comma) 
where z.row_number = x.row_number;

drop table if exists nc.ext_dealer_constraints_3 cascade;
CREATE TABLE nc.ext_dealer_constraints_3(
  row_number integer primary key,
  f1 citext,
  f2 citext,
  f3 citext,
  f4 citext,
  f5 citext,
  f6 citext,
  f7 citext,
  f8 citext,
  f9 citext,
  f10 citext,
  f11 citext,
  f12 citext,
  f13 citext,
  f14 citext);
comment on table nc.ext_dealer_constraints_3 is 'data from nc.ext_dealer_constraints_2 parsed into row_number & 14 attributes';

truncate nc.ext_dealer_constraints_3;
insert into nc.ext_dealer_constraints_3(row_number, f1)
select row_number, split_part(f1, ',', 1)
from nc.ext_dealer_constraints_2;
update nc.ext_dealer_constraints_3 a
set f2 = (
  select split_part(f1, ',', 2)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);
update nc.ext_dealer_constraints_3 a
set f3 = (
  select split_part(f1, ',', 3)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);  

update nc.ext_dealer_constraints_3 a
set f4 = (
  select split_part(f1, ',', 4)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);  
update nc.ext_dealer_constraints_3 a
set f5 = (
  select split_part(f1, ',', 5)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);  
    update nc.ext_dealer_constraints_3 a
set f6 = (
  select split_part(f1, ',', 6)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);  
update nc.ext_dealer_constraints_3 a
set f7 = (
  select split_part(f1, ',', 7)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);  
update nc.ext_dealer_constraints_3 a
set f8 = (
  select split_part(f1, ',', 8)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);  
update nc.ext_dealer_constraints_3 a
set f9 = (
  select split_part(f1, ',', 9)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);  
update nc.ext_dealer_constraints_3 a
set f10 = (
  select split_part(f1, ',', 10)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);  
update nc.ext_dealer_constraints_3 a
set f11 = (
  select split_part(f1, ',', 11)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);  
update nc.ext_dealer_constraints_3 a
set f12 = (
  select split_part(f1, ',', 12)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);  
update nc.ext_dealer_constraints_3 a
set f13 = (
  select split_part(f1, ',', 13)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);  
update nc.ext_dealer_constraints_3 a
set f14 = (
  select split_part(f1, ',', 14)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);                  



-- return commas and delete "
update nc.ext_dealer_constraints_3
set f2 = replace(f2, '"', ''),
    f4 = replace(f4, '"', '')
where f2 like '%"%'
  or f2 like '%;%'
  or f4 like '%;%';
update nc.ext_dealer_constraints_3
set f2 = replace(f2, ';', ','),
    f4 = replace(f4, ';', ',')
where f2 like '%"%'
  or f2 like '%;%'
  or f4 like '%;%';

-----------------------------------------------------------------
select * from nc.ext_dealer_constraints_3 order by row_number


comparing the new data with the previous xfm process

1.
step_1 = select * from nc.ext_dealer_constraints_3 where row_number > 3 and f1 not in ('Constraint', 'Number') 

drop table if exists new_step_1;
create temp table new_step_1 as
select * 
from nc.ext_dealer_constraints_3 
where row_number > 3 order by row_number
  and f1 not in ('Constraint', 'Number');

select * from new_step_1 order by row_number  

2.
-- for a subset of division/model_year/alloc_goup this gives me a group number
-- drop table if exists alloc_group_groups;
-- create temp table alloc_group_groups as
select trim(replace(f1, ':', '')), f2, row_number() over (),  
  (row_number()OVER(ORDER BY row_number) - 1 ) / 3 + 1 as group_no,
  row_number as division_row_number
from step_1
where f1 in('Division: ','Model Year: ','Allocation Group: ')
order by row_number;

=

drop table if exists new_alloc_group_groups;
create temp table new_alloc_group_groups as
select trim(replace(f1, ':', '')), f2, row_number,  
  (row_number()OVER(ORDER BY row_number) - 1 ) / 3 + 1 as group_no,
  row_number as division_row_number
from new_step_1 a
where trim(f1) in('Division:','Model Year:','Allocation Group:');

select * from new_alloc_group_groups;


3.

select * from nc.tmp_alloc_Groups

insert into nc.tmp_alloc_groups 
select a.the_data[1],a.the_data[2]::integer,a.the_data[3], division_row_number
from (
  select group_no, array_agg(f2 order by row_number) the_data, min(division_row_number) as division_row_number
  from alloc_group_groups
  group by group_no) a;  
  
=

select * from  nc.ext_dealer_constraints_3 order by row_number

drop table if exists new_tmp_alloc_groups;
create temp table new_tmp_alloc_groups as
select a.the_data[1] as division,a.the_data[2]::integer as model_year ,a.the_data[3] as alloc_group, division_row_number
from (
  select group_no, array_agg(f2 order by row_number) the_data, min(division_row_number) as division_row_number
  from new_alloc_group_groups
  group by group_no) a;  

select * from new_tmp_alloc_groups  



-- this is just a minimal set of data from ext, row_number and f1, f2, f3
-- to work with shaping the transformation, once i get it, i can add the additional columns
drop table if exists minim cascade;
create temp table minim as
select row_number, f1, f2, f3
from step_1;

select * from minim;

drop table if exists new_minim cascade;
create temp table new_minim as
select row_number, f1, f2, f3
from new_step_1
order by row_number

select * from new_minim
order by row_number

select *
from minim a
full outer join new_minim b on a.row_number = b.row_number
where trim(a.f3) <> trim(b.f3)

select row_number, f2, coalesce(lead(row_number, 1) over () - 1, (select max(row_number) from minim))
from minim


select *
from nc.tmp_alloc_groups a
full outer join new_tmp_alloc_groups b on a.division_row_number = b.division_row_number


-- aha, this look like it
create temp table test_old as
select a.division, a.model_year, a.alloc_group, c.f1, c.f2
from nc.tmp_alloc_groups a
left join (
  select row_number, f2, coalesce(lead(row_number, 1) over () - 1, (select max(row_number) from minim)) as max_row_number
  from minim
  where trim(f1) = 'Division:') b on a.division_row_number = b.row_number
left join minim c on c.row_number between b.row_number and b.max_row_number  
  and c.f1 not in('Division: ','Model Year: ','Allocation Group: ')
order by division, alloc_group


-- something is fucked up here
-- all better now
drop table if exists test_new;
create temp table test_new as
select a.division, a.model_year, a.alloc_group, c.f1, c.f2
from new_tmp_alloc_groups a
left join (  
  select row_number, f2, coalesce(lead(row_number, 1) over () - 1, (select max(row_number) from minim)) as max_row_number
  from (-- updating nc.ext_dealer_contraints_2 changes the default order, but this disambiguates the subset and order
    select *
    from new_minim
    where trim(f1)  = 'Division:' 
    order by row_number) x) b on a.division_row_number = b.row_number
--   select row_number, f2, coalesce(lead(row_number, 1) over () - 1, (select max(row_number) from minim)) as max_row_number
--   from new_minim
--   where trim(f1) = 'Division:'
--   order by row_number) b on a.division_row_number = b.row_number
left join new_minim c on c.row_number between b.row_number and b.max_row_number  
  and trim(c.f1) not in('Division:','Model Year:','Allocation Group:')  
order by division, alloc_group

-- and now they match
select *
from test_old a
full outer join test_new b on trim(replace(a.f2, '"', '')) = trim(b.f2) and a.model_year = b.model_year and trim(a.alloc_group) = trim(b.alloc_group)
order by a.division, a.model_year, a.alloc_group




select * 
from nc.ext_dealer_constraints_3
order by row_number



need TPP Start & End
