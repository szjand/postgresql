﻿drop table if exists orders cascade;
create temp table orders as
select a.stock_number, a.vin, a.source, a.ground_date,
  b.order_number, b.order_type, b.alloc_group, b.model_code, b.vehicle_trim, b.dan, b.peg, b.color, 
  c.event_code,
  c.from_Date, c.thru_date, d.*
from nc.stg_Availability a
left join gmgl.vehicle_orders b on a.vin = b.vin
left join gmgl.vehicle_order_events c on b.order_number = c.order_number
  and c.thru_date = (
    select max(thru_date)
    from gmgl.vehicle_order_events
    where order_number = c.order_number)
left join gmgl.vehicle_event_codes d on c.event_code = d.code
where a.source = 'factory' 
order by a.vin, c.from_date;
create unique index on orders(vin);

select * 
from orders

-- equinox orders that have a 5000
-- could be used for "averaging" time in statuses
select b.order_number, b.order_type, b.vin, b.vehicle_trim, b.color, b.model_code, b.dan, b.peg,
  c.event_code, c.from_Date, c.thru_date, 
  d.description, d.category
from gmgl.vehicle_orders b 
left join gmgl.vehicle_order_events c on b.order_number = c.order_number
left join gmgl.vehicle_event_codes d on c.event_code = d.code
where b.alloc_group = 'EQUINX'
  and exists (
    select 1
    from gmgl.vehicle_order_events
    where order_number = b.order_number
      and event_code = '5000')
order by b.order_number, c.from_Date

select * from gmgl.vehicle_orders limit 10





-- no consistent orders until 201805
select a.stock_number, a.vin, a.source, a.ground_date,
  b.order_type, b.alloc_group, b.model_code, b.vehicle_trim, b.dan, b.peg, b.color
from nc.vehicle_acquisitions a
left join gmgl.vehicle_orders b on a.vin = b.vin
where a.source = 'factory'
order by ground_date

-- orders with no current row
select x.*
from gmgl.vehicle_order_events x
join (
  select a.order_number
  from gmgl.vehicle_order_events a
  where not exists (
    select 1
    from gmgl.vehicle_order_events
    where order_number = a.order_number
      and thru_Date > current_date)) y on x.order_number = y.order_number
order by x.order_number, from_date    



select b.order_number, b.order_type, b.vin, b.vehicle_trim, b.color, b.model_code, b.dan, b.peg,
  c.event_code, c.from_Date, c.thru_date, 
  d.description, d.category
from gmgl.vehicle_orders b 
left join gmgl.vehicle_order_events c on b.order_number = c.order_number
left join gmgl.vehicle_event_codes d on c.event_code = d.code
where b.order_number = 'VVVQVW' 
order by c.from_date

select *
from gmgl.vehicle_invoices
where vin = '2GCVKPEC1K1119162'


select * from gmgl.vehicle_orders

select a.*, '*', b.*
from nc.stg_Availability a
left join gmgl.vehicle_orders b on a.vin = b.vin


select a.*, b.source,'*', c.*
from nc.vehicles a
join nc.vehicle_acquisitions b on a.vin = b.vin
left join gmgl.vehicle_orders c on a.vin = c.vin
where a.model = 'equinox'
  and b.ground_date > '09/30/2018'

-- how many possible events in order histor

select vin, count(*)
from (
select b.vin,
  b.order_number, b.order_type, b.alloc_group, b.model_code, b.vehicle_trim, b.dan, b.peg, b.color, 
  c.event_code,
  c.from_Date, c.thru_date, d.*
from gmgl.vehicle_orders b 
left join gmgl.vehicle_order_events c on b.order_number = c.order_number
left join gmgl.vehicle_event_codes d on c.event_code = d.code) x group by vin order by count(*) desc
order by b.vin  

/*
11/26/18
was thinking of storing order info (in system/in transit) for vehicles in nc.stg_availability
where ground_date = 12/31/9999
but that would only include "invoiced" orders, i think
so, what i want to see is all open equinox orders, then compare that to nc.stg_Availability
*/


-- this appears to be correct for open equinox orders
select *
from (
select b.vin,
  b.order_number, b.order_type, b.alloc_group, b.model_year, b.model_code, b.vehicle_trim, b.dan, b.peg, b.color, 
  c.event_code,
  c.from_Date, c.thru_date, d.*
from gmgl.vehicle_orders b
left join gmgl.vehicle_order_events c on b.order_number = c.order_number
  and c.thru_date = (
    select max(thru_date)
    from gmgl.vehicle_order_events
    where order_number = c.order_number)
left join gmgl.vehicle_event_codes d on c.event_code = d.code
where b.alloc_group = 'EQUINX'
  and left(d.code, 1)::integer < 5) aa
left join nc.stg_availability bb on aa.vin = bb.vin  
where not exists (
  select 1
  from nc.vehicles
  where vin = aa.vin)
order by event_code


-- open order on received vehicle
-- this is cleaned out by filtering out existing vins in nc.vehicles
select * 
from gmgl.vehicle_orders a
left join gmgl.vehicle_order_events b on a.order_number = b.order_number
where a.vin = '3GNAXSEV6JL404447'

-- shipped order not yet invoiced
select * 
from gmgl.vehicle_orders a
left join gmgl.vehicle_order_events b on a.order_number = b.order_number
where a.vin = '3GNAXKEV5KS562219'


-- the issue here is extracting the "sku" from order information on those
-- orders that don't yet have a VIN
select a.*, b.*--b.model_year, b.model_code, b.vehicle_trim, b.dan, b.peg, b.color
from nc.vehicles a
join gmgl.vehicle_orders b on a.vin = b.vin
where a.model = 'equinox'
  and a.model_year = '2019'
order by a.model_code, a.engine  

select a.vin, a.vehicle_trim, a.model_code, a.dan, a.peg, b.body_style, c.engine
-- select *
from gmgl.vehicle_orders a
left join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
left join nc.vehicles c on a.vin = c.vin 
where a.model_year = '2019'
  and a.alloc_group = 'EQUINX'
order by b.body_style

select body_Style, engine_code, min(inpmast_vin), max(inpmast_vin)
-- select *
from arkona.xfm_inpmast 
where year > 2017
  and model = 'equinox'
group by body_Style, engine_code  

select * from chr.describe_vehicle where vin = '3GNAXSEV9KL108632'


-- distinct styles from invoices
select  left(raw_invoice, 35), count(*)
from gmgl.vehicle_invoices
where raw_invoice like '%EQUINOX%'
group by left(raw_invoice, 35)


-- distinct styles from chrome
select distinct style
from (
select  
  jsonb_array_elements(response->'style')->'model'->>'$value' as model,
  jsonb_array_elements(response->'style')->'attributes'->>'name' as style
from chr.describe_vehicle
where vin = '2GNAXJEV5J6164127') a
where model = 'Equinox'


select distinct r.style->'attributes'->>'name'
from chr.describe_vehicle b
left join jsonb_array_elements(b.response->'style') as r(style) on true 
where r.style->'model'->>'$value' = 'Equinox'
limit 10



select * from nc.vehicle_acquisitions where vin in ('3GNAXXEV6KS510322','3GNAXUEV9KS537190','2GNAXUEV7K6106443')


select year, model, trim, style, drive_type, engine_size, mfr_model_num, mfr_package_code
from dao.vin_Reference
where model = 'equinox'
  and year in (2018,2019)
group by year, model, trim, style, drive_type, engine_size, mfr_model_num, mfr_package_code
order by drive_type, mfr_model_num -- year, trim, drive_type, engine_size

ok w/ is always engine regardless of LT, LS or LZ, 1 = 1.5, 2 = 2.0, 3 = 1.6
i think drive is in the model code

FWD: 1XP26, 1XR26, 1XS26
4x4: 1XX26, 1XY26, 1XZ26

-- yep
select year, left(body_style, 3), model_code
from arkona.xfm_inpmast 
where year > 2017
  and model = 'equinox'
group by year, left(body_style, 3), model_code  
order by model_code

now, what tells me whether it is LS, LT or PREM

select model_code, trim_level
-- select *
from nc.vehicles
where model = 'equinox'
group by model_code, trim_level


select substring(body_style, 9, 2), body_style, model_code, count(*)
from arkona.xfm_inpmast 
where year > 2017
  and model = 'equinox'
group by body_style, model_code  
order by model_code


1XP26: FWD LS
1XR26: FWD LT
1XS26: FWD PREM
1XX26: AWD LS
1XY26: AWD LT
1XZ26: AWD PREM

and engine comes from left(vehicle_orders.vehicle_trim, 1):  1=1.5, 2=2.0,3=1.6

-- this appears to be correct for open equinox orders
select vin, order_number, order_type, alloc_Group, model_year, model_code, vehicle_trim,
  dan, peg, color, event_code, category
  
from ( -- equinox orders where current_event < 5000
  select b.vin,
    b.order_number, b.order_type, b.alloc_group, b.model_year, b.model_code, b.vehicle_trim, b.dan, b.peg, b.color, 
    c.event_code,
    c.from_Date, c.thru_date, d.*
  from gmgl.vehicle_orders b
  left join gmgl.vehicle_order_events c on b.order_number = c.order_number
    and c.thru_date = (
      select max(thru_date)
      from gmgl.vehicle_order_events
      where order_number = c.order_number)
  left join gmgl.vehicle_event_codes d on c.event_code = d.code
  where b.alloc_group = 'EQUINX'
    and left(d.code, 1)::integer < 5) aa
-- left join nc.stg_availability bb on aa.vin = bb.vin  
where not exists (
  select 1
  from nc.vehicles
  where coalesce(vin, 'no vin') = aa.vin)



select vehicle_trim, dan, peg, min(vin), max(vin)
from gmgl.vehicle_orders
where alloc_group = 'EQUINX'
group by vehicle_trim, dan, peg
order by vehicle_trim

select inpmast_vin, body_style, b.*
from arkona.xfm_inpmast a
left join nc.vehicles b on a.inpmast_vin = b.vin
where current_row
  and inpmast_vin in ('3GNAXUEU5JS552680','2GNAXWEX1J6267818','2GNAXTEX1J6304347','3GNAXXEV6KL128186')



-- 11/28/18 what is the flow

orders not in nc.vehicles
  but now, today, i am wondering, does an order create a row in nc.vehicles
  given how much s truggled with identifying skus for just equinox, yuck ...

leaning toward the notion of open orders only, if more order detail is ever needed,
the raw order data is available

so, open orders whats the flow
nc.stg_Availability does not matter
open orders comes from orders that do not exist in nc.vehicles
from the order data, need sku level identification
  
1. delete from open orders where exists in nc.vehicles
2. insert new rows, PK: order_number, event_code
3. udpate changed rows
4. add it to equinox script

drop table if exists nc.open_orders cascade;
create table nc.open_orders (
  order_number citext primary key,
  event_code citext not null,
  vin citext,
  order_type citext,
  alloc_group citext not null, 
  model_year integer not null,
  model_code citext not null,
  vehicle_trim citext,
  dan citext,
  peg citext,
  color citext not null,
  category citext not null);
  comment on table nc.open_orders is 'global connect order information from tables gmgl.vehicle_orders, gmgl.vehicle_order_events
    gmgl.vehicle_event_codes for orders with event_code < 5000 and vehicles that do not yet exist in nc.vehicles. 
    Only the most current order information, when the vehicle is received at the dealership, the order information in 
    this table is deleted.  all history and detail is stored in the gmgl tables.'
create unique index on nc.open_orders(vin) where vin is not null;

-- may end up doing a conversion table: order info -> sku info
-- definitely need a xref for alloc_group

insert into nc.open_orders
-- this appears to be correct for open equinox orders
select order_number, event_code, vin, order_type, alloc_Group, model_year::integer, model_code, vehicle_trim,
  dan, peg, color, category 
from ( -- equinox orders where current_event < 5000
  select b.vin,
    b.order_number, b.order_type, b.alloc_group, b.model_year, b.model_code, b.vehicle_trim, b.dan, b.peg, b.color, 
    c.event_code,
    c.from_Date, c.thru_date, d.*
  from gmgl.vehicle_orders b
  left join gmgl.vehicle_order_events c on b.order_number = c.order_number
    and c.thru_date = (
      select max(thru_date)
      from gmgl.vehicle_order_events
      where order_number = c.order_number)
  left join gmgl.vehicle_event_codes d on c.event_code = d.code
  where left(d.code, 1)::integer < 5) aa
where not exists (
  select 1
  from nc.vehicles
  where coalesce(vin, 'no vin') = aa.vin) order by model_year;




so, for those orders with a vin, can get the "sku" (trim_level, engine, drive, color) from chrome, the rest 
need the sku for the end product which is the sales/inv spreadsheet
-- orders: decode config from order information
select a.*,
  case 
    when a.model_code in ('1XP26','1XR26','1XS26') then 'FWD'
    when a.model_code in ('1XX26','1XY26','1XZ26') then 'AWD'
    else 'UNKNOWN'
  end as drive,
  case 
    when a.model_code in ('1XP26','1XX26') then 'LS'
    when a.model_code in ('1XR26','1XY26') then 'LT'
    when a.model_code in ('1XS26','1XZ26') then 'PREM'
    else 'UNKNOWN'
  end as trim_level,
  case
    when left(vehicle_trim, 1) = '1' then '1.5'
    when left(vehicle_trim, 1) = '2' then '2.0'
    when left(vehicle_trim, 1) = '3' then '1.6'
    else 'UNKNOWN'
  end as engine, 
  b.mfr_color_name
from nc.open_orders a
left join nc.exterior_colors b on a.model_year = b.model_year::integer
  and a.color = b.mfr_color_code
  and b.model = 'equinox'
where a.alloc_group = 'EQUINX'

-- compare that to orders (with vins) decoded from chrome
-- looks good, at least for equinoxen
select vin, chr_style_id::integer, model_year, chr_model_code,
  case drive_train
    when 'All Wheel Drive' then 'AWD'
    when 'Front Wheel Drive' then 'FWD'
    else 'XXX'
  end as drive_train,
  'N/A'::citext as cab,
  case 
    when c.style like '%4dr LT%' then 'LT'
    when c.style like '%4dr LS%' then 'LS'
    when c.style like '%4dr L%' then 'L'
    when c.style like '%Premier%' then 'PREM'
    else 'XXXXXX'
  end as trim_level,
  c.engine_displacement, c.color
from(
  select a.*,
    r.style ->'attributes'->>'id' as chr_style_id,
    r.style ->'attributes'->>'mfrModelCode' as chr_model_code,
    r.style ->'attributes'->>'modelYear' as chr_model_year,
    r.style ->'attributes'->>'name' as style,
    r.style ->'attributes'->>'drivetrain' as drive_train,
    t.displacement->>'$value' as engine_displacement
  from nc.open_orders a      
  join chr.describe_vehicle b on a.vin = b.vin
  left join jsonb_array_elements(b.response->'style') as r(style) on true
  left join jsonb_array_elements(b.response->'engine') as s(engine) on true
  left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
    and t.displacement ->'attributes'->>'unit' = 'liters'
  where a.alloc_group = 'EQUINX'    ) c
order by vin  


-- decode allocation groups
select model_year, model_code, chr_style_id, alloc_group, chr_name, make, best_model_name
from (
  select a.model_year, a.make, a.model_code, a.vehicle_trim, a.alloc_group,
        r.style ->'attributes'->>'id' as chr_style_id,
        r.style ->'attributes'->>'mfrModelCode' as chr_model_code,
        r.style ->'attributes'->>'modelYear' as chr_model_year,
        r.style ->'attributes'->>'name' as chr_name,
        r.style ->'attributes'->>'drivetrain' as drive_train,
        b.response->'attributes'->>'bestTrimName' as best_trim_name, -- may be useful
        b.response->'attributes'->>'bestModelName' as best_model_name
  from gmgl.vehicle_orders a
  join chr.describe_vehicle b on a.vin = b.vin
  left join jsonb_array_elements(b.response->'style') as r(style) on true) x
group by model_year, model_code, chr_style_id, alloc_group, chr_name, make, best_model_name
order by alloc_group

select distinct alloc_group from gmgl.vehicle_orders

