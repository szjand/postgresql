﻿-- -- -- select *
-- -- -- from nc.vehicle_configurations
-- -- -- where model = 'camaro'
-- -- --   and model_year = 2018
-- -- -- 
-- -- -- 
-- -- -- select *
-- -- -- from nc.vehicles a
-- -- -- where configuration_id in (49,73,218)  
-- -- -- 
-- -- -- -- same trim, different model_code
-- -- -- select * from chr.describe_vehicle where vin in ('1G1FK1R61J0122121','1G1FK3D63J0158136')
-- -- -- 
-- -- -- select distinct --a.model_year, a.make, a.model, a.trim_level, a.model_code,
-- -- --   r.style ->'attributes'->>'modelYear' as model_year,
-- -- --   r.style ->'division'->>'$value' as make,
-- -- --   r.style ->'model'->>'$value' as model,
-- -- --   r.style ->'attributes'->>'trim' as trim,
-- -- --   r.style ->'attributes'->>'mfrModelCode' as model_code,
-- -- --   t.displacement->>'$value' as engine,
-- -- --   r.style->'attributes'->>'altBodyType', -- does not distinguish between pilot ex
-- -- --   r.style->'attributes'->>'nameWoTrim'
-- -- -- from (
-- -- --   select vin
-- -- --   from nc.vehicles 
-- -- --   union
-- -- --   select vin
-- -- --   from nc.open_orders) a
-- -- -- join chr.describe_vehicle b on a.vin = b.vin
-- -- --   join jsonb_array_elements(b.response->'style') as r(style) on true
-- -- --   left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
-- -- --     and u.ordinality =  2
-- -- --   left join jsonb_array_elements(r.style->'bodyType') with ordinality as v(bed) on true
-- -- --     and v.ordinality =  1   
-- -- --   left join jsonb_array_elements(b.response->'engine') as s(engine) on true  
-- -- --   left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
-- -- --     and t.displacement ->'attributes'->>'unit' = 'liters'
-- -- -- order by r.style ->'division'->>'$value', r.style ->'model'->>'$value', r.style ->'attributes'->>'modelYear'
-- -- -- 
-- -- -- 
-- -- -- select * 
-- -- -- from nc.vehicles a
-- -- -- join chr.describe_vehicle b on a.vin = b.vin
-- -- -- where a.model_year = 2018
-- -- --   and a.model = 'pilot'
-- -- --   and a.trim_level = 'ex'
-- -- -- 
-- -- -- select *
-- -- -- from nc.vehicles
-- -- -- where trim_level = 'LX 2.0'
-- -- -- 
-- -- -- 
-- -- -- select distinct --a.model_year, a.make, a.model, a.trim_level, a.model_code,
-- -- --   r.style ->'attributes'->>'modelYear' as model_year,
-- -- --   r.style ->'division'->>'$value' as make,
-- -- --   r.style ->'model'->>'$value' as model,
-- -- --   r.style ->'attributes'->>'trim' as trim,
-- -- --   r.style ->'attributes'->>'mfrModelCode' as model_code,
-- -- --   t.displacement->>'$value' as engine,
-- -- --   r.style->'attributes'->>'altBodyType', -- does not distinguish between pilot ex
-- -- --   r.style->'attributes'->>'nameWoTrim'
-- -- -- from nc.vehicles a
-- -- -- join chr.describe_vehicle b on a.vin = b.vin
-- -- --   join jsonb_array_elements(b.response->'style') as r(style) on true
-- -- --   left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
-- -- --     and u.ordinality =  2
-- -- --   left join jsonb_array_elements(r.style->'bodyType') with ordinality as v(bed) on true
-- -- --     and v.ordinality =  1   
-- -- --   left join jsonb_array_elements(b.response->'engine') as s(engine) on true  
-- -- --   left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
-- -- --     and t.displacement ->'attributes'->>'unit' = 'liters'
-- -- -- order by r.style ->'division'->>'$value', r.style ->'model'->>'$value', r.style ->'attributes'->>'modelYear'
-- -- -- 
-- -- -- select * from nc.vehicle_configurations
-- -- -- 
-- -- -- select configuration_id, name_wo_trim
-- -- -- from (
-- -- --   select a.configuration_id, r.style->'attributes'->>'nameWoTrim' as name_wo_trim
-- -- --   from (
-- -- --     select vin, configuration_id, model_code
-- -- --     from nc.vehicles 
-- -- --     union
-- -- --     select vin, configuration_id, model_code
-- -- --     from nc.open_orders) a  
-- -- --   join chr.describe_vehicle b on a.vin = b.vin
-- -- --   join jsonb_array_elements(response->'style') as r(style) on true
-- -- --   where a.model_code = (r.style ->'attributes'->>'mfrModelCode')::citext) c
-- -- -- group by configuration_id, name_wo_trim   
-- -- -- 
-- -- -- 
-- -- -- 
-- -- -- select distinct config_type from ( -- 12 distinct configs_types
-- -- -- select a.*, 
-- -- --   case when trim_level = 1 and cab = 1 and box = 1 and drive = 1 and engine = 1 then 'm' else '' end
-- -- --   ||
-- -- --   case when trim_level > 1 then 't' else '' end
-- -- --   ||
-- -- --   case when cab > 1 then 'c' else '' end
-- -- --   ||
-- -- --   case when box > 1 then 'b' else '' end
-- -- --   ||
-- -- --   case when drive > 1 then 'd' else '' end
-- -- --   ||
-- -- --   case when engine > 1 then 'e' else '' end
-- -- --   ||
-- -- --   case when name_wo_trim > 1 then 'n' else '' end as config_type  
-- -- -- from (
-- -- --   select model_year, make, model, count(distinct trim_level) as trim_level, count(distinct cab) as cab, 
-- -- --     count(distinct box_size) as box,
-- -- --     count(distinct drive) as drive, count(distinct engine) as engine,
-- -- --     count(distinct name_wo_trim) as name_wo_trim
-- -- --   from nc.vehicle_configurations
-- -- --   where model_year > 2017
-- -- --   group by model_year, make, model
-- -- --   order by make, model) a
-- -- -- ) x order by config_type;
-- -- -- 
-- -- -- 
-- -- -- select model_year,make,model,trim_level,cab,box,drive,engine
-- -- -- from  (
-- -- --   select model_year, make, model, count(distinct trim_level) as trim_level, count(distinct cab) as cab, 
-- -- --     count(distinct box_size) as box,
-- -- --     count(distinct drive) as drive, count(distinct engine) as engine
-- -- --   from nc.vehicle_configurations
-- -- --   where model_year > 2017
-- -- --   group by model_year, make, model) x
-- -- -- group by model_year,make,model,trim_level,cab,box,drive,engine
-- -- -- having count(*) > 1  
-- -- -- 
-- -- -- 
-- -- -- select model_year,make,model,trim_level,cab,box_size,drive,engine
-- -- -- from nc.vehicle_configurations
-- -- -- group by model_year,make,model,trim_level,cab,box_size,drive,engine
-- -- -- having count(*) > 1
-- -- -- 
-- -- -- -- name_wo_trim still 3 2018 hondas that are not unique
-- -- -- select model_year, make, model, config_type, config_header
-- -- -- from test_1
-- -- -- group by model_year, make, model, config_type, config_header
-- -- -- having count(*) > 1
-- -- -- order by make, model, model_year
-- -- -- 
-- -- -- select * 
-- -- -- from nc.vehicle_configurations
-- -- -- where model_year = 2019
-- -- --   and model = 'hr-v'
-- -- --   and trim_level = 'ex-l'
-- -- -- 
-- -- -- select *
-- -- -- from nc.vehicles
-- -- -- where configuration_id in (147,385)
-- -- --   
-- -- -- select * from chr.describe_vehicle where vin in ('2HKRW2H83JH651462','7FARW2H89JE000095')  


-- ok, lets start working on the queries
-- 
-- everything is at the year/make/model level

do
$$
declare
  _model_year integer := 2019 ;
  _make citext := 'chevrolet';
  _model citext := 'equinox';  
  _config_type citext := (
    select distinct config_type
    from nc.model_levels
    where model_year = _model_year
      and make = _make
      and model = _model);

begin
drop table if exists wtf;
create temp table wtf as
with 
  colors as ( -- totals across all columns for each color
    select a.model_year, a.make, a.model, a.seq, a.color, 
      count(*) filter (where b.status = 'sold_365') as sold_365,
      count(*) filter (where b.status = 'sold_90') as sold_90,
      count(*) filter (where b.status = 'inv_og') as inv_og,
      count(*) filter (where b.status = 'inv_transit') as inv_transit,
      count(*) filter (where b.status = 'inv_system') as inv_system
    from nc.model_colors a
    left join nc.daily_inventory b on a.model_year = b.model_year
      and a.make = b.make
      and a.model = b.model
      and a.color = b.color
      and the_date = current_date - 1
    where a.model_year = _model_year
      and a.make = _make
      and a.model = _model
    group by a.model_year, a.make, a.model, a.seq, a.color  
    order by a.seq),
  levels as ( -- vertical totals for each model level
--     if config_type = 'tde' then (
      select a.seq, a.level_header, status, count(*) 
      from nc.model_levels a
      join nc.daily_inventory b on a.model_year = b.model_year
        and a.make = b.make
        and a.model = b.model
        and a.level_header = b.trim_level || ' ' || b.drive || ' ' || b.engine -- this is determined by the config_type
      where a.model_year = 2019
        and a.make = 'chevrolet'
        and a.model = 'equinox'
      group by a.seq, a.level_header, status)
--     end if)
        

select * from colors;
-- select * from  levels;

end
$$;  
  
select * from wtf;


-- vertical totals for each level_header/status
select d.seq, d.level_header, jsonb_agg(d.status_count)
from (
  select c.seq, c.level_header, jsonb_build_object(c.status, c.the_count) as status_count
  from (
    select a.seq, a.level_header, status, count(*) as the_count
    from nc.model_levels a
    join nc.daily_inventory b on a.model_year = b.model_year
      and a.make = b.make
      and a.model = b.model
      and b.the_date = current_date - 1
      and a.level_header = b.trim_level || ' ' || b.drive || ' ' || b.engine
    where a.model_year = 2019
      and a.make = 'chevrolet'
      and a.model = 'equinox'
    group by a.seq, a.level_header, status) c) d
group by d.seq, d.level_header
order by d.seq    





----------------------------------------------------------------------------------------
-- 1/13/19 going with these 3 as tables tables, at least for v1

config with totals for each status accross all rows (all colors)
  [] colors
      one object for each color with the valud for that config for each status


update daily_inventory with config_type and level header
i cant fucking figure out if that would help or not
need to actually start doing some fucking queries

need to be able to group by the header level
i know the header levels from the config_type in nc.model_levels
select * from nc.model_levels

what are all the stats i need
at model level
  total for each color/status combination
  total for each header/status combination
  total for each header/status/color combination: these are the "internal" cells

hmmm, tables?

year  make  model color status count

and, thats it for the V1 spreadsheet
maybe generate all the detail data daily



select * 
from nc.model_levels

select * 
from nc.model_colors

select model_year, make, model, color 
from nc.model_color_stats
group by model_year, make, model, color 
having count(*) > 1
  
-- total for each color/status combination
drop table if exists nc.model_color_stats cascade;
create table nc.model_color_stats (
  model_year integer not null,
  make citext not null,
  model citext not null,
  seq integer not null,
  color citext not null,
  sold_90 integer,
  sold_365 integer,
  inv_og integer,
  inv_transit integer,
  inv_system integer,
  foreign key (model_year,make,model,color) references nc.model_colors(model_year,make,model,color),
  primary key (model_year,make,model,color));

insert into nc.model_color_stats
select a.model_year, a.make, a.model, a.seq, a.color,
  case 
    when count(vin) filter (where status = 'sold_90') > 0 then count(vin) filter (where status = 'sold_90')
    else null
  end as sold_90,
  case 
    when count(vin) filter (where status = 'sold_365') > 0 then count(vin) filter (where status = 'sold_365')
    else null
  end as sold_365,  
  case 
    when count(vin) filter (where status = 'inv_og') > 0 then count(vin) filter (where status = 'inv_og')
    else null
  end as inv_og,    
  case 
    when count(vin) filter (where status = 'inv_transit') > 0 then count(vin) filter (where status = 'inv_transit')
    else null
  end as inv_transit,    
  case 
    when count(vin) filter (where status = 'inv_system') > 0 then count(vin) filter (where status = 'inv_system')
    else null
  end as inv_system       
from nc.model_colors a
left join nc.daily_inventory b on a.model_year = b.model_year
  and a.make = b.make
  and a.model = b.model
  and a.color = b.color
  and b.the_date = current_date - 1
group by a.model_year, a.make, a.model, a.seq, a.color;

select * 
from nc.model_color_stats 
where model_year = 2019 
  and make = 'chevrolet'
  and model = 'equinox' 
order by seq;

select model_year, make, model, 
  sum(sold_90), sum(sold_365), sum(inv_og), 
  sum(inv_transit), sum(inv_system)
from nc.model_color_stats  
where model_year = 2019 
  and make = 'chevrolet'
  and model = 'equinox' 
group by model_year, make, model

-- total for each header/status combination 
drop table if exists nc.model_level_stats cascade;
create table nc.model_level_stats (
  model_year integer not null,
  make citext not null,
  model citext not null,
  seq integer not null,
  level_header citext not null,
  sold_90 integer,
  sold_365 integer,
  inv_og integer,
  inv_transit integer,
  inv_system integer,
  foreign key (model_year,make,model,level_header) references nc.model_levels(model_year,make,model,level_header),
  primary key (model_year,make,model,level_header));

insert into nc.model_level_stats
select a.model_year, a.make, a.model, a.seq, a.level_header, 
  case 
    when count(vin) filter (where status = 'sold_90') > 0 then count(vin) filter (where status = 'sold_90')
    else null
  end as sold_90,
  case 
    when count(vin) filter (where status = 'sold_365') > 0 then count(vin) filter (where status = 'sold_365')
    else null
  end as sold_365,  
  case 
    when count(vin) filter (where status = 'inv_og') > 0 then count(vin) filter (where status = 'inv_og')
    else null
  end as inv_og,    
  case 
    when count(vin) filter (where status = 'inv_transit') > 0 then count(vin) filter (where status = 'inv_transit')
    else null
  end as inv_transit,    
  case 
    when count(vin) filter (where status = 'inv_system') > 0 then count(vin) filter (where status = 'inv_system')
    else null
  end as inv_system   
from nc.model_levels a
join nc.vehicle_configurations b on a.model_year = b.model_year
  and a.make = b.make
  and a.model = b.model
  and a.level_header = b.level_header 
join nc.daily_inventory c on b.configuration_id = c.configuration_id
  and c.the_date = current_date - 2                                                                                                                                                       
-- where a.model_year = 2019
--   and a.make = 'chevrolet'
--   and a.model = 'equinox'
group by a.seq, a.level_header, a.model_year, a.make, a.model
order by a.seq;

select *
from nc.model_level_stats
where model_year = 2019 
  and make = 'buick'
  and model = 'enclave' 
order by seq;
  

!!!!!!!!!!!! put level header in vehicle_configurations !!!!!!!!!!!!!!!!!!!!
????????????????????????????????????????????????????????????????????????????
-- WHY
one time generation of level_headers to update vehic_configs, then each query is a simple
join between model_levels and vehic config
wait
wont even need model_levels in the stats queries, just daily_inventory * vehic_configs
except maybe for level sequence

be specific about what i am trying to accomplish
this is coming out of trying to generate the table of "inner cell" stats for inventory


model_year  make        model     level         level_seq  color                        color_seq  sold_90  sold_365  inv_og  inv_transit   inv_system
2019        chevrolet   equinox   LS AWD 1.5        1       CAJUN RED TINTCOAT            1
                                  LS AWD 1.5        1       IRIDESCENT PEARL TRICOAT      2 
                                  LS AWD 1.5        1       Kinetic Blue Metallic         3
                                  LS FWD 1.5        2       CAJUN RED TINTCOAT            1
                                  LS FWD 1.5        2       IRIDESCENT PEARL TRICOAT      2 
                                  LS FWD 1.5        2       Kinetic Blue Metallic         3

i want all levels and all colors regardless of activity for the day       


-- total for each header/color/status combination 
drop table if exists nc.model_level_color_stats cascade;
create table nc.model_level_color_stats (
  model_year integer not null,
  make citext not null,
  model citext not null,
  level_seq integer not null,
  level_header citext not null,
  color_seq integer not null,
  color citext not null,
  sold_90 integer,
  sold_365 integer,
  inv_og integer,
  inv_transit integer,
  inv_system integer,
  foreign key (model_year,make,model,level_header) references nc.model_levels(model_year,make,model,level_header),
  foreign key (model_year,make,model,color) references nc.model_colors(model_year,make,model,color),  
  primary key (model_year,make,model,level_header,color));

insert into nc.model_level_color_stats  
select a.model_year, a.make, a.model, a.seq, a.level_header, b.seq, b.color, 
  case 
    when count(vin) filter (where status = 'sold_90') > 0 then count(vin) filter (where status = 'sold_90')
    else null
  end as sold_90,
  case 
    when count(vin) filter (where status = 'sold_365') > 0 then count(vin) filter (where status = 'sold_365')
    else null
  end as sold_365,
  case 
    when count(vin) filter (where status = 'inv_og') > 0 then count(vin) filter (where status = 'inv_og')
    else null
  end as inv_og,
  case 
    when count(vin) filter (where status = 'inv_transit') > 0 then count(vin) filter (where status = 'inv_transit')
    else null
  end as inv_transit,
  case 
    when count(vin) filter (where status = 'inv_system') > 0 then count(vin) filter (where status = 'inv_system')
    else null
  end as inv_system        
from nc.model_levels a
join nc.model_colors b on a.model_year = b.model_year
  and a.make = b.make
  and a.model = b.model
join nc.vehicle_configurations c on a.model_year = c.model_year
  and a.make = c.make
  and a.model = c.model
  and a.level_header = c.level_header 
left join nc.daily_inventory d on c.configuration_id = d.configuration_id  
  and b.color = d.color
  and d.the_date = current_date - 1
-- where a.model_year = 2019
--   and a.make = 'chevrolet'
--   and a.model = 'equinox'
group by a.model_year, a.make, a.model, a.seq, a.level_header, b.seq, b.color  
order by a.seq, b.seq;



select *
from nc.model_level_color_stats
where model_year = 2019
  and make = 'chevrolet'
  and model = 'equinox';

select *
from nc.model_levels
where model_year = 2019
  and make = 'chevrolet'
  and model = 'equinox'  
order by seq;  


select *
from nc.model_colors
where model_year = 2019
  and make = 'chevrolet'
  and model = 'equinox'  
order by seq;                         

select *
from nc.daily_inventory a
where the_Date = current_date -1 
  and model_year = 2019
  and model = 'enclave'
  and status like 'inv%'

select * from nc.daily_inventory 
where stock_number = 'G35927'

select *
from nc.open_orders
where vin = '5GAEVAKW3KJ209391'

select * from nc.daily_inventory 
where vin = '5GAEVAKW3KJ209391'

-- nc.open_orders stock_numbers
select a.*, b.inpmast_stock_number, b.status
from nc.daily_inventory a
left join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
where a.the_date = current_date -1
  and a.stock_number = 'none'
order by a.make, a.model  

-- what is tpw (date) ???  
select *
from gmgl.vehicle_orders
where vin = '5GAEVCKWXKJ224725'

select * 
from gmgl.vehicle_order_events a
left join gmgl.vehicle_event_codes b on a.event_code = b.code
where order_number = 'wqgtrg'




  