﻿-- 

/* 
 -- nc.open_orders --
 Current orders with an event code of 5000 or less. 
 Vehicles will NOT exist in nc.vehicles
 Coming from gmgl tables

 -- nc.vehicle --
 When is this populated?
 No date when it is in nc.vehicles or is that the date stored in nc.vehicle_aquisitions?

 -- nc.vehicle_aquisitions -- 
 Vehicles that are on the ground?
 Thru date? ahhh ctp
 Thru date should match the delivery date in vehicle sales?
 Also includes honda? How are you determining if it is factory?

 -- nc.vehicle_sales --
 Vehicles that are capped?
 includes honda??

*/

with 
  in_sys_in_trans_vehicles as (
    -- vehicles that have event codes in the category of in transit and in system
    select *
    from nc.open_orders a
    inner join nc.vehicle_configurations b on a.configuration_id = b.configuration_id ),

  on_grd_factory_vehicles as (
    -- vehicles currently on the ground ordered from the factory 'GM'
    select *
    from nc.vehicles a
    inner join nc.vehicle_acquisitions b on a.vin = b.vin
    where thru_date > current_date
      and source = 'factory'
      and make in ('GMC','Cadillac','Chevrolet','Buick')
      )
-----
-- way off compared to what
-----
-- Global Connect Comparison Query
/* Chev and Gmc seem way off. could be that things happened throughout the day. 
 Cadillac off by one and buick is on*/
  select make, category, count(*)
  from in_sys_in_trans_vehicles
  group by make, category
union 
  select make, 'On Ground' as category, count(*)
  from on_grd_factory_vehicles
  group by make
  order by make,category

select a.make, c.category, count(*)
-- select *vehicle_event_codes
from gmgl.vehicle_orders a
left join gmgl.vehicle_order_events b on a.order_number = b.order_number
  and b.thru_date = (
    select max(thru_date)
    from gmgl.vehicle_order_events
    where order_number = b.order_number)
left join gmgl.vehicle_event_codes c on b.event_code = c.code
where left(c.code, 1)::integer < 5
  and order_type = 'TRE'
  and category <> 'Placed'
  and a.order_number not in ('VXXMJ1','VXXMJ2','VXXMJZ','VZHB4R','VZHBJ8','VZSRMK','VZSRMM','WBVX79','WCGN2T')  
-- where a.order_number not in ('VXXMJ1','VXXMJ2','VXXMJZ','VZHB4R','VZHBJ8','VZSRMK','VZSRMM','WBVX79','WCGN2T')
  and not exists (
    select 1
    from nc.vehicles
    where vin = coalesce(a.vin, '666'))
group by a.make, c.category  


select * from gmgl.vehicle_orders where vin = '1GKS2CKJ1KR309764'
  
select * from gmgl.vehicle_event_codes

select * 
from nc.vehicle_acquisitions
where in_transit_sale
order by ground_date desc 
  