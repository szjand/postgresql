﻿drop table if exists nc.daily_inventory cascade ;
create table nc.daily_inventory (
  the_date date not null,
  stock_number citext not null,
  vin citext not null,
  color citext not null,
  model_year citext not null,
  make citext not null,
  model citext not null,
  model_code citext not null,
  trim_level citext not null,
  cab citext not null, `
  cab citext not null, 
  drive citext not null,
  engine citext not null,
  chrome_style_id citext not null,
  configuration_id integer not null,
  status citext not null);
create index on nc.daily_inventory (the_date);  
create index on nc.daily_inventory (color);  
create index on nc.daily_inventory (model_year);  
create index on nc.daily_inventory (make);  
create index on nc.daily_inventory (model);  
create index on nc.daily_inventory (configuration_id);  
create index on nc.daily_inventory (status);  
create index on nc.daily_inventory (stock_number);  
create index on nc.daily_inventory (vin);  
create index on nc.daily_inventory (model_code);  
create index on nc.daily_inventory (trim_level);  
create index on nc.daily_inventory (cab);  
create index on nc.daily_inventory (drive);  
create index on nc.daily_inventory (engine);  

alter table nc.daily_inventory
alter column model_year type integer using model_year::integer


drop table if exists nc.model_years;
create table nc.model_years (
  model_year integer primary key,
  seq integer not null);

insert into nc.model_years
select model_year, row_number() over (order by model_year)
from nc.daily_inventory
where model_year > 2017
group by model_year;


drop table if exists nc.model_colors cascade;
create table nc.model_colors (
  model_year integer not null,
  make citext not null, 
  model citext not null,
  color citext not null,
  seq integer not null,
  foreign key (model_year,make,model) references nc.year_make_models(model_year,make,model),
  primary key (model_year,make,model,color));
comment on table nc.model_colors is 'truncated and filled nightly from nc.year_make_models and
nc.daily_inventory (color)';  

insert into nc.model_colors(model_year,make,model,color,seq)
select a.model_year, a.make, a.model, b.color, 
  row_number() over (partition by a.model_year, a.make, a.model order by b.color) as seq
from nc.year_make_models a  
join nc.daily_inventory b on a.model_year = b.model_year
  and a.make = b.make
  and a.model = b.model
group by a.model_year, a.make, a.model, b.color;

-- since box size is determined by the vin, should it be in config?
-- does that even fucking matter
-- most of the other attributes are also chrome(vin) derived
-- so does it need to be in config and vehicles
-- at least in config due to the way i am going to generate confg_type & config_header

--------------------------------------------------
-- box_size
--------------------------------------------------
alter table nc.vehicle_configurations
add column box_size citext;

update nc.vehicle_configurations z
set box_size = x.box_size
from (
  select configuration_id, box_size
  from (
    select a.configuration_id, 
      t. bed->>'$value' as bed, 
      case
        when t. bed->>'$value' like '%Bed' then substring(bed->>'$value', 1, position(' ' in bed->>'$value') - 1)
        else 'n/a'
      end as box_size
    from (
      select vin, configuration_id, model_code
      from nc.vehicles 
      union
      select vin, configuration_id, model_code
      from nc.open_orders) a  
    join chr.describe_vehicle b on a.vin = b.vin
    join jsonb_array_elements(response->'style') as r(style) on true
    join jsonb_array_elements(r.style->'bodyType') with ordinality as t(bed) on true
      and t.ordinality =  1
    where a.model_code = (r.style ->'attributes'->>'mfrModelCode')::citext) c
  group by configuration_id, box_size) x
where z.configuration_id = x.configuration_id;  

alter table nc.vehicle_configurations
alter column box_size set not null;

--------------------------------------------------
-- name_wo_trim
--------------------------------------------------
-- by default this field will be n/a
-- only populated for those configurations that are not unique 
-- accross model_year,make,model,trim_level,cab,box_size,drive,engine

alter table nc.vehicle_configurations
add column name_wo_trim citext;

update nc.vehicle_configurations
set name_wo_trim = 'n/a';


-- -- nope, generates way to much noise in the headers where it is not necessary
-- -- to distinguish between the different configs    
-- update nc.vehicle_configurations z
-- set name_wo_trim = x.name_wo_trim
-- from (
--   select configuration_id, name_wo_trim
--   from (
--     select a.configuration_id, r.style->'attributes'->>'nameWoTrim' as name_wo_trim
--     from (
--       select vin, configuration_id, model_code
--       from nc.vehicles 
--       union
--       select vin, configuration_id, model_code
--       from nc.open_orders) a  
--     join chr.describe_vehicle b on a.vin = b.vin
--     join jsonb_array_elements(response->'style') as r(style) on true
--     where a.model_code = (r.style ->'attributes'->>'mfrModelCode')::citext) c
-- group by configuration_id, name_wo_trim) x
-- where z.configuration_id = x.configuration_id;

alter table nc.vehicle_configurations
alter column name_wo_trim set not null;


-- 1/8/ think i want some separate tables here

this makes checking for new combinations simple and straightforward
assumes all elements comprising a config type are: model/trim_leve/cab/box/drive/engine/name_wo_trim (only as reqd)

drop table if exists nc.configuration_types cascade;
create table nc.configuration_types (config_type citext primary key);
insert into nc.configuration_types
select distinct config_type from ( -- 15 distinct configs_types
select a.*, 
  case when trim_level = 1 and cab = 1 and box = 1 and drive = 1 and engine = 1 then 'm' else '' end
  ||
  case when trim_level > 1 then 't' else '' end
  ||
  case when cab > 1 then 'c' else '' end
  ||
  case when box > 1 then 'b' else '' end
  ||
  case when drive > 1 then 'd' else '' end
  ||
  case when engine > 1 then 'e' else '' end
  ||
  case when name_wo_trim > 1 then 'n' else '' end as config_type  
from (
  select model_year, make, model, count(distinct trim_level) as trim_level, count(distinct cab) as cab, 
    count(distinct box_size) as box,
    count(distinct drive) as drive, count(distinct engine) as engine,
    count(distinct name_wo_trim) as name_wo_trim
  from nc.vehicle_configurations
  where model_year > 2017
  group by model_year, make, model
  order by make, model) a
) x order by config_type;
comment on table nc.configuration_types is 'derived from nc.vehicle_configurations, can be any
combination of m(model),t(trim),c(cab),b(box),d(drive),e(engine),n(name_wo_trim) that uniquely describe
the configuration of a vehicle. tested nightly for changes, additions';

drop table if exists nc.year_make_models cascade;
create table nc.year_make_models (
  model_year integer not null,
  make citext not null,
  model citext not null,
  primary key (model_year,make,model));
insert into nc.year_make_models
select model_year, make, model  
from nc.vehicle_configurations
group by model_year, make, model;

  
drop table if exists nc.model_levels cascade;
create table nc.model_levels (
  model_year integer not null,
  make citext not null,
  model citext not null,
  config_type citext not null references nc.configuration_types(config_type),
  seq integer not null,
  level_header citext not null,
  foreign key (model_year,make,model) references nc.year_make_models(model_year,make,model),
  primary key(model_year,make,model,level_header));
create unique index on nc.model_levels(model_year,make,model,seq);  
comment on table nc.model_levels is ''  
create  
model_year    make    model     config_type   level_seq     level_header
2018          buick   enclave     t              1            Avenir
2018          buick   enclave     t              2            Essence
2018          buick   enclave     t              3            Premier

truncate nc.model_levels;
insert into nc.model_levels
-- drop table if exists test_1; create temp table test_1 as
select model_year, make, model, config_type, seq, config_header
from (
  select b.*,
    case -- one case option for each config_type
      when config_type = 'd' then (
        select array_agg(drive order by drive) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)  
      when config_type = 'm' then (
        select array_agg(model order by model) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)  
      when config_type = 'mn' then (
        select array_agg(model || ' ' || name_wo_trim order by model, name_wo_trim) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)          
      when config_type = 't' then (
        select array_agg(trim_level order by trim_level) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)
      when config_type = 'tb' then (
        select array_agg(trim_level || ' ' || box_size order by trim_level, box_size) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)   
      when config_type = 'tbe' then (
        select array_agg(trim_level || ' ' || box_size || ' ' || engine order by trim_level, box_size, engine) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)             
      when config_type = 'tcb' then (
        select array_agg(trim_level || ' ' || cab || ' ' || box_size order by trim_level, cab, box_size) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)    
      when config_type = 'tcbde' then (
        select array_agg(trim_level || ' ' || cab || ' ' || box_size || ' ' || drive || ' ' || engine order by trim_level, cab, box_size, drive, engine) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)   
      when config_type = 'tcbe' then (
        select array_agg(trim_level || ' ' || cab || ' ' || box_size || ' ' || engine order by trim_level, cab, box_size, engine) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)   
      when config_type = 'tce' then (
        select array_agg(trim_level || ' ' || cab || ' ' || engine order by trim_level, cab, engine) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)    
      when config_type = 'td' then (
        select array_agg(trim_level || ' ' || drive order by trim_level, drive) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)   
      when config_type = 'tde' then (
        select array_agg(trim_level || ' ' || drive || ' ' || engine order by trim_level, drive, engine) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)  
      when config_type = 'te' then (
        select array_agg(trim_level || ' ' || engine order by trim_level, engine) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)    
      when config_type = 'ten' then (
        select array_agg(trim_level || ' ' || engine || ' ' || name_wo_trim order by trim_level, engine, name_wo_trim) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)    
      when config_type = 'tn' then (
        select array_agg(trim_level || ' ' || name_wo_trim order by trim_level, name_wo_trim) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)                                                  
      end as config_headers
  from (
    select a.*, 
      case when trim_level = 1 and cab = 1 and box = 1 and drive = 1 and engine = 1 then 'm' else '' end
      ||
      case when trim_level > 1 then 't' else '' end
      ||
      case when cab > 1 then 'c' else '' end
      ||
      case when box > 1 then 'b' else '' end
      ||
      case when drive > 1 then 'd' else '' end
      ||
      case when engine > 1 then 'e' else '' end
      ||
      case when name_wo_trim > 1 then 'n' else '' end as config_type  
from (
  select model_year, make, model, count(distinct trim_level) as trim_level, count(distinct cab) as cab, 
    count(distinct box_size) as box,
    count(distinct drive) as drive, count(distinct engine) as engine,
    count(distinct name_wo_trim) as name_wo_trim
  from nc.vehicle_configurations
  where model_year > 2017
  group by model_year, make, model
  order by make, model) a) b) c
left join lateral unnest(config_headers) 
  with ordinality as f(config_header, seq) on true;     
comment on table nc.model_levels is 'establishes a sequence and concatenates the elements from the 
configuration_type to genereate a config_header, used as a major column header for displaying
the different configurations of a model. truncated and filled nightly';

-- all unique now
select model_year, make, model, config_type, config_header
from test_1
group by model_year, make, model, config_type, config_header
having count(*) > 1
order by make, model, model_year

-- new rows not yet in model_levels
select * 
from test_1 a
where not exists (
  select 1
  from nc.model_levels
  where model_year = a.model_year
    and make = a.make
    and model = a.model
    and level_header = a.config_header)

-- count of configs test_1 vs model_levels
select a.*, b.model_level
from (
  select model_year, make, model, count(*) as test
  from test_1  
  group by model_year, make, model) a
full outer join (
  select model_year, make, model, count(*) as model_level
  from nc.model_levels  
  group by model_year, make, model) b on a.model_year = b.model_year and a.make = b.make and a.model = b.model  
where test <> model_level  
order by a.model_year, a.make, a.model

-- config_types that have changed
select *
from test_1 a
join nc.model_levels b on a.model_year = b.model_year
  and a.make = b.make
  and a.model = b.model
  and coalesce(a.config_type, 'none_a') <> coalesce(b.config_type, 'none_b')


-- 11/12 add config_type & level_headers to vehicle_configurations
alter table nc.vehicle_configurations
-- add column config_type citext;
add column level_header citext;

update nc.vehicle_configurations z
set config_type = x.config_type,
    level_header = x.level_header
from (    
  select a.*, b.configuration_id
  from nc.model_levels a
  left join nc.vehicle_configurations b on a.model_year = b.model_year
    and a.make = b.make
    and a.model = b.model
    and 
      case
        when  a.config_type = 'd' 
          then a.level_header = b.drive 
        when  a.config_type = 'm' 
          then a.level_header = b.model --then 1 = 1 -- model is already in the join     
        when  a.config_type = 'mn' 
          then a.level_header = b.model || ' ' || b.name_wo_trim
        when  a.config_type = 't' 
          then a.level_header = b.trim_level 
        when  a.config_type = 'tb' 
          then a.level_header = b.trim_level || ' ' || b.box_size
        when  a.config_type = 'tbe' 
          then a.level_header = b.trim_level || ' ' || b.box_size || ' ' || b.engine
        when  a.config_type = 'tcb' 
          then a.level_header = b.trim_level || ' ' || b.cab || ' ' || b.box_size
        when  a.config_type = 'tcbde' 
          then a.level_header = b.trim_level || ' ' || b.cab || ' ' || b.box_size || ' ' || b.drive || ' ' || b.engine        
        when  a.config_type = 'tcbe' 
          then a.level_header = b.trim_level || ' ' || b.cab || ' ' || b.box_size || ' ' || b.engine
        when  a.config_type = 'tce' 
          then a.level_header = b.trim_level || ' ' || b.cab || ' ' || b.engine         
        when  a.config_type = 'td' 
          then a.level_header = b.trim_level || ' ' || b.drive 
        when  a.config_type = 'tde' 
          then a.level_header = b.trim_level || ' ' || b.drive || ' ' || b.engine
        when  a.config_type = 'te' 
          then a.level_header = b.trim_level || ' ' || b.engine
        when  a.config_type = 'ten' 
          then a.level_header = b.trim_level|| ' ' || b.engine || ' ' || b.name_wo_trim 
        when  a.config_type = 'tn' 
          then a.level_header = b.trim_level || ' ' || b.name_wo_trim 
      end) x 
where z.configuration_id = x.configuration_id;          

-----------------------------------------------------------------
-- total for each color/status combination
drop table if exists nc.model_color_stats cascade;
create table nc.model_color_stats (
  model_year integer not null,
  make citext not null,
  model citext not null,
  seq integer not null,
  color citext not null,
  sold_90 integer,
  sold_365 integer,
  inv_og integer,
  inv_transit integer,
  inv_system integer,
  foreign key (model_year,make,model,color) references nc.model_colors(model_year,make,model,color),
  primary key (model_year,make,model,color));
comment on table nc.model_color_stats is 'aggregrated status numbers by year/make/model/color for the 
  most recent day in nc.daily_inventory. truncated and filled nightly';
  
insert into nc.model_color_stats
select a.model_year, a.make, a.model, a.seq, a.color,
  case 
    when count(vin) filter (where status = 'sold_90') > 0 then count(vin) filter (where status = 'sold_90')
    else null
  end as sold_90,
  case 
    when count(vin) filter (where status = 'sold_365') > 0 then count(vin) filter (where status = 'sold_365')
    else null
  end as sold_365,  
  case 
    when count(vin) filter (where status = 'inv_og') > 0 then count(vin) filter (where status = 'inv_og')
    else null
  end as inv_og,    
  case 
    when count(vin) filter (where status = 'inv_transit') > 0 then count(vin) filter (where status = 'inv_transit')
    else null
  end as inv_transit,    
  case 
    when count(vin) filter (where status = 'inv_system') > 0 then count(vin) filter (where status = 'inv_system')
    else null
  end as inv_system       
from nc.model_colors a
left join nc.daily_inventory b on a.model_year = b.model_year
  and a.make = b.make
  and a.model = b.model
  and a.color = b.color
  and b.the_date = current_date - 1
group by a.model_year, a.make, a.model, a.seq, a.color;


-- total for each header/status combination 
drop table if exists nc.model_level_stats cascade;
create table nc.model_level_stats (
  model_year integer not null,
  make citext not null,
  model citext not null,
  seq integer not null,
  level_header citext not null,
  sold_90 integer,
  sold_365 integer,
  inv_og integer,
  inv_transit integer,
  inv_system integer,
  foreign key (model_year,make,model,level_header) references nc.model_levels(model_year,make,model,level_header),
  primary key (model_year,make,model,level_header));
comment on table nc.model_level_stats is 'aggregrated status numbers by year/make/model/level_header for the 
  most recent day in nc.daily_inventory. truncated and filled nightly';
  
insert into nc.model_level_stats
select a.model_year, a.make, a.model, a.seq, a.level_header, 
  case 
    when count(vin) filter (where status = 'sold_90') > 0 then count(vin) filter (where status = 'sold_90')
    else null
  end as sold_90,
  case 
    when count(vin) filter (where status = 'sold_365') > 0 then count(vin) filter (where status = 'sold_365')
    else null
  end as sold_365,  
  case 
    when count(vin) filter (where status = 'inv_og') > 0 then count(vin) filter (where status = 'inv_og')
    else null
  end as inv_og,    
  case 
    when count(vin) filter (where status = 'inv_transit') > 0 then count(vin) filter (where status = 'inv_transit')
    else null
  end as inv_transit,    
  case 
    when count(vin) filter (where status = 'inv_system') > 0 then count(vin) filter (where status = 'inv_system')
    else null
  end as inv_system   
from nc.model_levels a
join nc.vehicle_configurations b on a.model_year = b.model_year
  and a.make = b.make
  and a.model = b.model
  and a.level_header = b.level_header 
join nc.daily_inventory c on b.configuration_id = c.configuration_id
  and c.the_date = current_date - 2                                                                                                                                                       
-- where a.model_year = 2019
--   and a.make = 'chevrolet'
--   and a.model = 'equinox'
group by a.seq, a.level_header, a.model_year, a.make, a.model
order by a.seq;

-- total for each header/color/status combination 
drop table if exists nc.model_level_color_stats cascade;
create table nc.model_level_color_stats (
  model_year integer not null,
  make citext not null,
  model citext not null,
  level_seq integer not null,
  level_header citext not null,
  color_seq integer not null,
  color citext not null,
  sold_90 integer,
  sold_365 integer,
  inv_og integer,
  inv_transit integer,
  inv_system integer,
  foreign key (model_year,make,model,level_header) references nc.model_levels(model_year,make,model,level_header),
  foreign key (model_year,make,model,color) references nc.model_colors(model_year,make,model,color),  
  primary key (model_year,make,model,level_header,color));
comment on table nc.model_level_stats is 'aggregrated status numbers by year/make/model/level_header/color
 for the most recent day in nc.daily_inventory. truncated and filled nightly';
  
insert into nc.model_level_color_stats  
select a.model_year, a.make, a.model, a.seq, a.level_header, b.seq, b.color, 
  case 
    when count(vin) filter (where status = 'sold_90') > 0 then count(vin) filter (where status = 'sold_90')
    else null
  end as sold_90,
  case 
    when count(vin) filter (where status = 'sold_365') > 0 then count(vin) filter (where status = 'sold_365')
    else null
  end as sold_365,
  case 
    when count(vin) filter (where status = 'inv_og') > 0 then count(vin) filter (where status = 'inv_og')
    else null
  end as inv_og,
  case 
    when count(vin) filter (where status = 'inv_transit') > 0 then count(vin) filter (where status = 'inv_transit')
    else null
  end as inv_transit,
  case 
    when count(vin) filter (where status = 'inv_system') > 0 then count(vin) filter (where status = 'inv_system')
    else null
  end as inv_system        
from nc.model_levels a
join nc.model_colors b on a.model_year = b.model_year
  and a.make = b.make
  and a.model = b.model
join nc.vehicle_configurations c on a.model_year = c.model_year
  and a.make = c.make
  and a.model = c.model
  and a.level_header = c.level_header 
left join nc.daily_inventory d on c.configuration_id = d.configuration_id  
  and b.color = d.color
  and d.the_date = current_date - 1
-- where a.model_year = 2019
--   and a.make = 'chevrolet'
--   and a.model = 'equinox'
group by a.model_year, a.make, a.model, a.seq, a.level_header, b.seq, b.color  
order by a.seq, b.seq;



-- select trim_level
-- from nc.daily_inventory a
-- where a.the_date = current_date - 1
--   and a.model_year = 2019
--   and a.make = 'chevrolet'
--   and a.model = 'equinox'
-- 
-- select *
-- from nc.model_levels a
-- where a.model_year = 2019
--   and a.make = 'chevrolet'
--   and a.model = 'equinox'
-- 
-- -- ok
-- select model_year, make, model
-- from (
--   select model_year,make,model,config_type
--   from nc.model_levels  
--   group by model_year,make,model,config_type) a
-- group by model_year, make, model
-- having count(*) > 1

-----------------------------------------------------
/*
  may need to refine some of this
  for example, cruze LS includes name_wo_trim, but it isn't needed
  it is needed for LT, but since this is at model level, all trims get it
  for now it's simply a few level_header with a superfluous n/a
*/  

select *
from nc.model_levels
where config_type like '%n'

select *
from nc.vehicle_configurations
where name_wo_trim <> 'n/a'
order by model_year, make, model, trim_level

select *
from nc.model_levels
where level_header like '%n/a'

------------------------------------------------------------------------
-- from .../ext_chrome/describe_Vehicle_2_end_points.sql

drop table if exists chr.exterior_colors cascade;
create table chr.exterior_colors (
  model_year integer not null,
  make citext not null,
  model citext not null,
  color_code citext not null,
  color citext not null,
  primary key (model_year,make,model,color_code,color));
comment on table chr.exterior_colors is 'based on chrome ADS service with the ShowAvailableEquipment switch which returns
all available equipment, including colors. table is truncated and loaded daily. PK consists of all attributes
because honda/nissan uses multiple color codes for the same color';
 
insert into chr.exterior_colors  
select model_year::integer, make, model, color_code, color
from (
  select a.vin, 
    r.style ->'attributes'->>'modelYear' as model_year,
    r.style ->'division'->>'$value' as make,
    r.style ->'model'->>'$value' as model,
    b.colors->'attributes'->>'colorCode' as color_code,
    b.colors->'attributes'->>'colorName' as color,
    b.ordinality
  -- select count(*)
  from jon.describe_vehicle a
  join jsonb_array_elements(a.response->'exteriorColor') with ordinality as b(colors) on true
  join jsonb_array_elements(a.response->'style') as r(style) on true) x
where model_year in ('2018','2019')
group by model_year, make, model, color_code, color;

-- modified 2/8/19
drop table if exists dao.peg_to_trim cascade;
create table dao.peg_to_trim (
  model_year integer not null,
  make citext not null,
  model citext not null,
  model_code citext not null,
  trim_level citext not null,
  peg citext not null,
  primary key(model_year,make,model,model_code,trim_level,peg));
comment on table dao.peg_to_trim is 'derived from dao.vin_reference as a cross reference
  between a trim_level and GM peg (preferred equipment group) to assist in 
  categorizing orders without vins. Truncated and refilled nightly. excludes fleet vehicles';  
insert into dao.peg_to_trim
select a.year, a.make, a.model, a.mfr_model_num, a.trim, a.mfr_package_code
from dao.vin_reference a
join dao.veh_trim_styles aa on a.vehicle_id = aa.vehicle_id
  and aa.fleet = 'N'
where a.year = 2019
  and a.make in ('chevrolet','gmc','buick','cadillac')
group by a.year, a.make, a.model, a.mfr_model_num, a.trim,a. mfr_package_code;  

drop table if exists dao.model_codes cascade;
create table dao.model_codes (
  model_year integer not null,
  make citext not null,
  model citext not null, 
  model_number citext not null,
  primary key (model_year,make,model,model_number));
comment on table dao.model_codes is 'subset of dao.veh_trim_styles comprised of rydell gf makes,
  used to identify year/make/model from model_code. Truncate and fill nightly';  
insert into dao.model_codes  
select year, make, model, mfr_model_num
from dao.veh_trim_styles a
where fleet = 'N'
  and year > 2017
  and make in ('chevrolet','gmc','buick','cadillac','honda','nissan')
group by year, make, model, mfr_model_num;

drop table if exists dao.exterior_colors cascade;
CREATE TABLE dao.exterior_colors(
  model_year integer NOT NULL,
  make citext NOT NULL,
  model citext NOT NULL,
  color_code citext NOT NULL,
  color citext NOT NULL,
  CONSTRAINT exterior_colors_pkey PRIMARY KEY (model_year, make, model, color_code, color));
COMMENT ON TABLE dao.exterior_colors
  IS 'subset of color_code and colors from dataone for rydell franchises only, 
  model year greater than 2016';
create index on dao.exterior_colors(make);
create index on dao.exterior_colors(color_code);
create index on dao.exterior_colors(color);
create index on dao.exterior_colors(model);
create index on dao.exterior_colors(model_year);

drop table if exists dao.engine_option_codes;
create table dao.engine_option_codes  (
  model_year integer not null,
  make citext not null,
  model citext not null,
  model_code citext not null,
  engine_code citext not null,
  engine citext not null,
  foreign key (model_year, make, model, model_code) references dao.model_codes(model_year, make, model, model_code),
  primary key (model_year,make,model,model_code,engine_code));
comment on table dao.engine_option_codes is 'for each GM model, the option codes for engines.
  truncate and fill nightly. used for determining configuration_id for orders without a vin';
insert into dao.engine_option_codes    
select a.year as model_year, a.make, a.model, 
  a.mfr_model_num as model_code,
  b.engine_code, c.ice_displacement as engine
-- select *
from dao.vin_reference a
join dao.veh_trim_styles aa on a.vehicle_id = aa.vehicle_id
  and aa.fleet = 'N'
join dao.lkp_veh_eng b on a.vehicle_id = b.vehicle_id
join dao.def_engine c on b.engine_id = c.engine_id
where a.make in ('chevrolet','gmc','buick','cadillac')
  and a.year > 2017
group by a.year, a.make, a.model, a.mfr_model_num, 
  b.engine_code, c.ice_displacement;

drop table if exists nc.xfm_open_orders cascade;
create table nc.xfm_open_orders (
  order_number citext primary key,
  alloc_group citext not null,
  peg citext not null,
  model_year integer not null,
  make citext not null,
  model citext not null,
  model_code citext not null,
  trim_level citext not null,
  cab citext not null,
  drive citext,
  box_size citext not null,
  engine citext not null,
  color citext not null);
comment on table nc.xfm_open_orders is 'flesh out rows from nc.open_orders without a vin';  

truncate nc.xfm_open_orders;
insert into nc.xfm_open_orders  
select a.order_number, a.alloc_group, a.peg, a.model_year, 
  b.make, b.model, a.model_code, c.trim_level,
  case
    when left(a.model_code, 2) in ('CC','CK', 'TC','TK') and b.model not in ('suburban','tahoe','yukon','yukon xl') then
      case
        when right(a.model_code, 2) = '03' then 'reg'
        when right(a.model_code, 2) = '53' then 'double'
        when right(a.model_code, 2) = '43' then 'crew'
      end
    else 'n/a'
  end as cab,
  case
    when left(a.model_code, 2) in ('CC','CK', 'TC','TK') THEN -- and b.model not in ('suburban','tahoe') then
      case
        when substring(a.model_code, 2, 1) = 'C' then 'RWD'
        when substring(a.model_code, 2, 1) = 'K' then '4WD'
      end
  end as drive,  
  case
    when left(a.model_code, 2) in ('CC','CK', 'TC','TK') and b.model not in ('suburban','tahoe','yukon','yukon xl') then
      case
        when substring(a.model_code, 5,1) = '5' then 'Short'
        when substring(a.model_code, 5,1) = '7' then 'Standard'
        when substring(a.model_code, 5,1) = '9' then 'Long'
        else 'n/a'
      end
    else 'n/a'
  end as box_size, e.engine, f.color
from nc.open_orders a
join dao.model_codes b on a.model_year = b.model_year
  and a.model_code = b.model_code
join dao.peg_to_trim c on a.model_year = c.model_year
  and b.make = c.make  
  and b.model = c.model
  and a.peg = c.peg
join gmgl.vehicle_order_options d on a.order_number = d.order_number
  and d.thru_ts > now()
join dao.engine_option_codes e on a.model_year = e.model_year
  and a.model_code = e.model_code
  and d.option_code = e.engine_code 
left join chr.exterior_colors f on a.model_year = f.model_year
  and e.make = f.make
  and e.model = f.model
  and a.color_code = f.color_code
where a.configuration_id is null
  and a.order_type = 'tre'
order by a.order_number;


adding order_number to daily_inventory may solve my PK dilemna
PK then becomes stock_number, order_number, the_Date, status, i think

alter table nc.daily_inventory
add column order_number citext;

update nc.daily_inventory z
set order_number = x.order_number
from (
  select distinct a.vin, coalesce(b.order_number, 'none') as order_number
  from nc.daily_inventory a
  left join gmgl.vehicle_orders b on a.vin = b.vin) x
where z.vin = x.vin  

alter table nc.daily_inventory
alter column order_number set not null;

alter table nc.daily_inventory
add primary key (stock_number, order_number, the_date, status);

-- 1/31 added configuration_id
alter table nc.model_level_stats
add column configuration_id integer;
alter table nc.model_level_stats
alter column configuration_id set not null;

alter table nc.model_level_color_stats
add column configuration_id integer;
alter table nc.model_level_color_stats
alter column configuration_id set not null;




drop table if exists nc.model_stat_totals cascade;
create table nc.model_stat_totals (
  make citext not null,
  model citext not null, 
  sold_90 integer not null,
  sold_365 integer not null,
  inv_og integer not null,
  inv_transit integer not null,
  inv_system integer not null,
  primary key (make,model));
COMMENT ON TABLE nc.model_stat_totals IS 'aggregated status counts at the  
  make/model level combining model years, truncated nightly and populated from nc.model_color_stats';
insert into nc.model_stat_totals  
select make, model, 
  sum(coalesce(sold_90, 0)) as sold_90, sum(coalesce(sold_365, 0)) as sold_365, 
  sum(coalesce(inv_og, 0)) as inv_og, sum(coalesce(inv_transit, 0)) as inv_transit, 
  sum(coalesce(inv_system, 0)) as inv_system
from nc.model_color_stats  
group by make, model;

drop table if exists nc.model_level_stat_totals cascade;
create table nc.model_level_stat_totals (
  make citext not null,
  model citext not null, 
  level_header citext not null,
  sold_90 integer not null,
  sold_365 integer not null,
  inv_og integer not null,
  inv_transit integer not null,
  inv_system integer not null,
  primary key (make,model,level_header));
COMMENT ON TABLE nc.model_level_stat_totals IS 'aggregated status counts at the make/model/level_header level combining model years, 
  truncated nightly and populated from nc.model_level_stats';
 
insert into nc.model_level_stat_totals 
select make, model, level_header,
  sum(coalesce(sold_90, 0)) as sold_90, sum(coalesce(sold_365, 0)) as sold_365,
  sum(coalesce(inv_og, 0)) as inv_og, sum(coalesce(inv_transit, 0)) as inv_transit, 
  sum(coalesce(inv_system, 0)) as inv_system
from nc.model_level_stats
group by make, model, level_header;

drop table if exists nc.model_level_color_stat_totals cascade;
create table nc.model_level_color_stat_totals (
  make citext not null,
  model citext not null, 
  level_header citext not null,
  color citext not null,
  sold_90 integer not null,
  sold_365 integer not null,
  inv_og integer not null,
  inv_transit integer not null,
  inv_system integer not null,
  primary key (make,model,level_header,color));
COMMENT ON TABLE nc.model_level_stat_totals IS 'aggregated status counts at the make/model/level_header/color level combining model years, 
  truncated nightly and populated from nc.model_level_stats';  
insert into nc.model_level_color_stat_totals 
select a.make, a.model, a.level_header, initcap(a.color) as color,
  sum(coalesce(a.sold_90, 0)) as sold_90, sum(coalesce(a.sold_365, 0)) as sold_365,
  sum(coalesce(a.inv_og, 0)) as inv_og, sum(coalesce(a.inv_transit, 0)) as inv_transit, 
  sum(coalesce(a.inv_system, 0)) as inv_system
from nc.model_level_color_stats a
join nc.vehicle_configurations b on a.configuration_id = b.configuration_id
group by a.make, a.model, a.level_header, a.color;

drop table if exists nc.vehicle_invoices;
create table nc.vehicle_invoices (
  vin citext primary key,
  description citext not null,
  source citext not null,
  model_code citext not null,
  color_code citext not null,
  color citext not null);
truncate nc.vehicle_invoices;
insert into nc.vehicle_invoices  
select vin, trim(left(raw_invoice, position('GENERAL MOTORS LLC' in raw_invoice)-1)):: citext as description,
  case position('RYDELL' in raw_invoice)
    when 0 then 'dealer trade'
    else 'factory'
  end as source,
  left(substring(raw_invoice from position('MODEL' in raw_invoice) + 78 for 8), 
    position(' ' in substring(raw_invoice from position('MODEL' in raw_invoice) + 78 for 8))) as model_code,
  substring(raw_invoice from position(E'\n' in raw_invoice) + 1 for 3) as color_code,
  trim(substring(raw_invoice from position(E'\n' in raw_invoice) + 6 for 31))::citext as color       
from gmgl.vehicle_invoices
where thru_date > current_date;

2/6/19
alter table nc.open_orders
alter column peg set not null;

drop table if exists nc.stg_vehicle_configurations cascade;
CREATE TABLE nc.stg_vehicle_configurations(
  model_year integer NOT NULL,
  make citext NOT NULL,
  model citext NOT NULL,
  model_code citext NOT NULL,
  trim_level citext NOT NULL,
  cab citext NOT NULL DEFAULT 'N/A'::citext,
  drive citext NOT NULL,
  engine citext NOT NULL,
  chrome_style_id citext,
  alloc_group citext,
  configuration_id serial NOT NULL,
  box_size citext NOT NULL,
  name_wo_trim citext NOT NULL,
  config_type citext,
  level_header citext,
  CONSTRAINT stg_vehicle_configurations_pkey PRIMARY KEY (configuration_id));

CREATE UNIQUE INDEX stg_chrome_idx
  ON nc.stg_vehicle_configurations
  USING btree
  (chrome_style_id COLLATE pg_catalog."default", engine COLLATE pg_catalog."default", trim_level COLLATE pg_catalog."default");

CREATE UNIQUE INDEX stg_config_idx
  ON nc.stg_vehicle_configurations
  USING btree
  (model_year, make COLLATE pg_catalog."default", model COLLATE pg_catalog."default", model_code COLLATE pg_catalog."default", trim_level COLLATE pg_catalog."default", cab COLLATE pg_catalog."default", engine COLLATE pg_catalog."default");

2/7/19
alter table nc.stg_vehicles
alter column trim_level set not null;

alter table nc.stg_vehicles
add constraint check_trim_level check (trim(trim_level) <> '');  

2/11/19

create table nc.forecast_models (
  category citext not null,
  make citext not null,
  model citext not null,
  primary key (category,make,model));
insert into nc.forecast_models
select category, make, model
from forecast
group by category,make,model;
comment on table nc.forecast_models is 'unique combination of categories,makes,models derived
from spreadsheet (copy of pulse-2019-02-07.xlxs), this is the level at which sales is
forecast for the GM store';

drop TABLE if exists nc.forecast cascade;
CREATE TABLE nc.forecast(
  category citext NOT NULL,
  make citext NOT NULL,
  model citext NOT NULL,
  year_month integer NOT NULL,
  forecast integer NOT NULL,
  foreign key (category,make,model) references nc.forecast_models(category,make,model),
  PRIMARY KEY (category, make, model, year_month));
comment on table nc.forecast is 'the actual monthly sales forecast for 2019, from the "pulse" spreadsheet';  

-- 3/13/19
comment on table nc.forecast is 'the actual monthly sales forecast for 2019, originally from the "pulse" spreadsheet.
3/13/19, structure modified to accommodate editing and storing history';  

alter table nc.forecast
add column from_date date,
add column thru_date date,
add column current_row boolean,
add column created_by uuid;

update nc.forecast
set created_by = (select user_key from nrv.users where first_name = 'jon' and last_name = 'andrews'),
    from_date = '03/01/2019',
    thru_date = '12/31/9999',
    current_row = true;

alter table nc.forecast
drop constraint forecast_pkey,
add primary key(category,make,model,year_month,thru_Date);

alter table nc.forecast
alter column from_date set not null,
alter column thru_date set not null,
alter column current_row set not null,
alter column created_by set not null;


    

drop table if exists nc.forecast_model_configurations cascade;
create table nc.forecast_model_configurations (
  category citext not null,
  make citext not null,
  model citext not null,
  configuration_id integer not null,
  foreign key (category,make,model) references nc.forecast_models(category,make,model),
  primary key (category,make,model,configuration_id));
comment on table nc.forecast_model_configurations is 'junction table relating forecast models to vehicle_configurations';  
  
-- select *
-- select count(distinct a.category || a.make || a.model) -- 48, good, got all of them
insert into nc.forecast_model_configurations
select a.category, a.make, a.model, coalesce(b.configuration_id, 0)
from nc.forecast_models a
left join (
  select * 
  from nc.vehicle_configurations
  where model_year > 2017) b on (a.make = b.make and a.model = b.model and b.model <> 'camaro')
  or
    case
      when (a.make = 'buick' and a.model = 'regal') then (b.make = 'buick' and b.model like '%regal%')
      when (a.make = 'cadillac' and a.model = 'ct6') then (b.make = 'cadillac' and b.model like '%ct6%')
      when (a.make = 'chevrolet' and a.model = 'camaro convert') then (b.make = 'chevrolet' and b.alloc_group = 'camcon')
      when (a.make = 'chevrolet' and a.model = 'camaro') then (b.make = 'chevrolet' and b.alloc_group = 'cam')
      when (a.make = 'chevrolet' and a.model = '1500 reg') then (b.make = 'chevrolet' and b.model like '%1500%' and cab = 'reg')
      when (a.make = 'chevrolet' and a.model = '1500 double') then (b.make = 'chevrolet' and b.model like '%1500%' and cab = 'double')
      when (a.make = 'chevrolet' and a.model = '1500 crew') then (b.make = 'chevrolet' and b.model like '%1500%' and cab = 'crew')
      when (a.make = 'chevrolet' and a.model = '2500 reg') then (b.make = 'chevrolet' and b.model like '%2500%' and cab = 'reg')
      when (a.make = 'chevrolet' and a.model = '2500 double') then (b.make = 'chevrolet' and b.model like '%2500%' and cab = 'double')
      when (a.make = 'chevrolet' and a.model = '2500 crew') then (b.make = 'chevrolet' and b.model like '%2500%' and cab = 'crew')
      when (a.make = 'chevrolet' and a.model = '3500 reg') then (b.make = 'chevrolet' and b.model like '%3500%' and cab = 'reg')
      when (a.make = 'chevrolet' and a.model = '3500 double') then (b.make = 'chevrolet' and b.model like '%3500%' and cab = 'double')
      when (a.make = 'chevrolet' and a.model = '3500 crew') then (b.make = 'chevrolet' and b.model like '%3500%' and cab = 'crew')      
      when (a.make = 'chevrolet' and a.model = 'express') then (b.make = 'chevrolet' and b.model like '%express%')   
      when (a.make = 'gmc' and a.model = '1500 reg') then (b.make = 'gmc' and b.model like '%1500%' and cab = 'reg')
      when (a.make = 'gmc' and a.model = '1500 double') then (b.make = 'gmc' and b.model like '%1500%' and cab = 'double')
      when (a.make = 'gmc' and a.model = '1500 crew') then (b.make = 'gmc' and b.model like '%1500%' and cab = 'crew')
      when (a.make = 'gmc' and a.model = '2500 reg') then (b.make = 'gmc' and b.model like '%2500%' and cab = 'reg')
      when (a.make = 'gmc' and a.model = '2500 double') then (b.make = 'gmc' and b.model like '%2500%' and cab = 'double')
      when (a.make = 'gmc' and a.model = '2500 crew') then (b.make = 'gmc' and b.model like '%2500%' and cab = 'crew')
      when (a.make = 'gmc' and a.model = '3500 reg') then (b.make = 'gmc' and b.model like '%3500%' and cab = 'reg')
      when (a.make = 'gmc' and a.model = '3500 double') then (b.make = 'gmc' and b.model like '%3500%' and cab = 'double')
      when (a.make = 'gmc' and a.model = '3500 crew') then (b.make = 'gmc' and b.model like '%3500%' and cab = 'crew')  
      when (a.make = 'gmc' and a.model = 'savana') then (b.make = 'gmc' and b.model like '%savana%')      
    end; 

drop table if exists nc.non_forecast_configurations cascade;
create table nc.non_forecast_configurations (
  configuration_id integer not null primary key references nc.vehicle_configurations(configuration_id));
comment on table nc.non_forecast_configurations is 'GM vehicle configurations for which there is no 2019 forecast';
insert into nc.non_forecast_configurations
select a.configuration_id 
from nc.vehicle_configurations a
left join nc.forecast_model_configurations b on a.configuration_id = b.configuration_id
where a.model_year > 2017
  and a.make in ('chevrolet','gmc','buick','cadillac')
  and b.configuration_id is null;    

-- 3/29/19
-- at ben/greg request/insistence, added notes to forecast status  

drop table if exists nc.forecast_status_notes cascade;
create table nc.forecast_status_notes (
  make_model citext not null,
  level_header citext not null,
  color citext not null,
  note citext not null,
  primary key (make_model,level_header,color));     

insert into nc.forecast_status_notes (make_model,level_header,color, note) values
('Chevrolet Equinox','LT AWD 1.5  (BOTH)','Pepperdust Metallic  (BOTH)','Pepperdust Metallic: Last DOSP: 11/29/2018'),
('Chevrolet Equinox','LT AWD 2.0  (BOTH)','Pepperdust Metallic  (BOTH)','Pepperdust Metallic: Last DOSP: 11/29/2018'),
('Chevrolet Equinox','Premier AWD 2.0 (BOTH)','Pepperdust Metallic  (BOTH)','Pepperdust Metallic: Last DOSP: 11/29/2018'),
('Chevrolet Equinox','LS FWD 1.5 (BOTH)','Pepperdust Metallic  (BOTH)','Pepperdust Metallic: Last DOSP: 11/29/2018'),
('Chevrolet Equinox','Premier AWD 1.5  (BOTH)','Pepperdust Metallic  (BOTH)','Pepperdust Metallic: Last DOSP: 11/29/2018'),
('Chevrolet Equinox','LT AWD 1.6 (2018)','Pepperdust Metallic  (BOTH)','Pepperdust Metallic: Last DOSP: 11/29/2018'),
('Chevrolet Equinox','LS AWD 1.5 (2019)','Pepperdust Metallic  (BOTH)','Pepperdust Metallic: Last DOSP: 11/29/2018'),
('Chevrolet Equinox','LT FWD 1.5 (BOTH)','Pepperdust Metallic  (BOTH)','Pepperdust Metallic: Last DOSP: 11/29/2018');