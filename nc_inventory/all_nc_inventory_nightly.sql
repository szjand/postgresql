﻿--< nightly
/*

populate and maintain:
  nc.vehicles
  nc.vehicles
  nc.vehicle_acquisitions
  nc.vehicle_sales
  nc.open_orders

12/16/18
    include equinox
    now, the only vehicle not included is silverado 1500%
    12/30/18 include silverado
*/
--------------------------------------------------------------------------------------------
-- -- invoices: this picks up invoices not yet entered in dealertrack (inpmast/fact_gl)
-- -- -- returns one row, no invoice, but vehicle is already sold in february
-- -- select * from jo.vehicle_order_events where order_number = 'xjgctm' order by event_code
-- -- select * from gmgl.vehicle_invoices where vin = '1GT49REY3LF215108'
-- -- 7/16/20 1GT59LE79LF277119 failing, no invoice
-- 
-- !!!!!!!!!!!!
-- no it does not
-- !!!!!!!!!!!!!
-- these 2
-- XJGCTM;1GT49REY3LF215108;2020-01-28
-- XJGVJ1;1GT59LE79LF277119;2020-06-29
-- show up even after runing missing invoice script ???
-- 
-- select a.order_number, a.vin, b.effective_date 
-- from gmgl.vehicle_orders a
-- join jo.vehicle_order_events b on a.order_number = b.order_number
--   and b.event_code = '4150'
--   and b.effective_date > '01/01/2020'
-- left join gmgl.vehicle_invoices c on a.vin = c.vin  
--   and c.thru_date > current_date
-- where a.vin is not null
--   and c.vin is null
-- --   and a.vin not in ('1GT49REY3LF215108','1GT59LE79LF277119')
-- order by b.effective_date  
-- 
-- truncate nc.vehicle_invoices cascade;  
-- insert into nc.vehicle_invoices
-- select vin,
--   trim((left(split_part(raw_invoice, E'\n', 1), position('GENERAL' in raw_invoice) -1))::citext) as description,
--   case 
--     when position('RYDELL' in raw_invoice) = 0 and position('CADILLAC OF GRAND FORKS' in raw_invoice) = 0 then 'dealer trade'
--     else 'factory'
--   end as source,  
--   trim((left(split_part(raw_invoice, E'\n', 8), 7))::citext) as model_code,
--   trim((left(split_part(raw_invoice, E'\n', 2), 3))::citext) as color_code,
--   trim((trim(substring(split_part(raw_invoice, E'\n', 2), 6, 31)))::citext) as color,
--   left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date as invoice_date,
--   left(trim(split_part(raw_invoice, 'SHIPPED', 2)), 8)::date as shipped_date,
--   left(trim(split_part(raw_invoice, 'EXP I/T', 2)), 8)::date as exp_in_transit_date
-- from (
--   select vin, replace(raw_invoice, '&amp;', '&') as raw_invoice
--   from gmgl.vehicle_invoices
--   where thru_date > current_date
--     and position('CREDIT FOR INVOICE' in raw_invoice) = 0) a
-- 
-- -- 1/23/19: boosted date range to 120 days to pick up G35156, got a few others as well
-- -- occasionally need to adj the date range, a vehicle invoiced 150 days ago that is
-- -- just now received will be missed
-- -- 10/23/19 exclude bogus broker vins
-- -- 01/29/20: added 24 stocknumbers to the journal decoding: EFT, purchased 24 FWD Equinoxen from another dealer
-- --           actually added EFT to the journal join clause to handle these, don't know if it should stay ...
-- --           probably remove it once these 24 are here


truncate nc.stg_availability; 
insert into nc.stg_availability (the_date,journal_date,stock_number,vin,model_year,
  make, model,model_code,color_code,color, source) 
select current_date, f.the_date, f.control,g.inpmast_vin, g.year, g.make, 
  g.model, g.model_code,g.color_code, g.color,
  case f.journal_code
    when 'PVI' then 'factory'
    when 'CDH' then 'dealer trade'
    when 'GJE' then 'ctp'
    when 'EFT' then 'dealer trade' end as source 
from ( -- accounting journal
  select a.control, d.journal_code, max(b.the_date) as the_date, sum(a.amount) as amount
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_date between current_Date - 365 and current_date
    --'06/22/2018'
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account in ( -- all new vehicle inventory account
      select b.account
      from fin.dim_account b
      where b.account_type = 'asset'
        and b.department_code = 'nc'
        and b.typical_balance = 'debit'
        and b.current_row
        and b.account <> '126104')
  inner join fin.dim_journal d on a.journal_key = d.journal_key
    and
      case
        when a.control = 'G37876' then d.journal_code = 'CDH' 
        -- when a.control in ('G38401','G38402','G38403','G38404','G38405','G38406','G38407','G38408','G38409') then d.journal_code = 'EFT'
        else d.journal_code in ('PVI','CDH','GJE','EFT')end
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where a.post_status = 'Y'
  group by a.control, d.journal_code
  having sum(a.amount) > 10000) f 
inner join arkona.xfm_inpmast g on f.control = g.inpmast_stock_number
  and g.current_row = true
  and g.year::integer > 2017 -- in ('2018', '2019','2020') 
  -- bogus brokerand g.inpmast_vin not in ('2GNAXUEVXL6151670','2GNAXUEV0L6147806','2GNAXUEV9L6147996','2GNAXUEV4L6146626','2GNAXUEV4L6154256','2GNAXUEV8L6146550','2GNAXUEV8L6150291','2GNAXUEV9L6150932','2GNAXUEV0L6104339')
  and not (f.control = 'G40872' and journal_code = 'CDH') 
  -- *a* 
where not exists (
  select 1
  from nc.vehicle_acquisitions
  where stock_number = f.control)

-- -- accounting done against GJE
-- update nc.stg_availability
-- set source = 'factory'
-- -- select * from nc.stg_availability
-- where stock_number = 'T11014';

-- detect ctps returned to inventory in journal SVI instead of GJE  
-- generalize it, any journal other than PVI, CDH, GJE
-- any vehicle in acct inventory that is in neither nc.vehicle_acquisitions or nc.stg_availability
-- in the future if this assert fails, need to analyze and proceed accordingly
-- 5/9/19 removed the limit of journal to svi, today we have 10 put into inventory in SCA

-- 9/7/19 this misses G36996R because it is not in inpmast yet

-- 11/01, bunch of locator acquisitions against EFT that showed up here,
--      for now, coded them into nc.stg_availability

do $$
begin
assert (
  select count(*)
  from (
    select current_date, f.the_date, f.control, 
      g.inpmast_vin, g.year, g.make, g.model, g.model_code, 
      g.color_code, g.color, f.journal_code
    from ( -- accounting journal
      select a.control, d.journal_code, min(b.the_date) as the_date, sum(a.amount) as amount
      from fin.fact_gl a
      inner join dds.dim_date b on a.date_key = b.date_key
        and b.the_date between current_Date - 365 and current_date
      inner join fin.dim_account c on a.account_key = c.account_key
        and c.account in ( -- all new vehicle inventory account
          select b.account
          from fin.dim_account b 
          where b.account_type = 'asset'
            and b.department_code = 'nc'
            and b.typical_balance = 'debit'
            and b.current_row
            and b.account <> '126104')   
      inner join fin.dim_journal d on a.journal_key = d.journal_key
--         and d.journal_code in ('SVI')
      inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
      where a.post_status = 'Y'
      group by a.control, d.journal_code
      having sum(a.amount) > 10000) f
    inner join arkona.xfm_inpmast g on f.control = g.inpmast_stock_number
      and g.current_row = true
      and g.year::integer > 2017 -- in ('2018', '2019','2020')
  -- bogus broker
    and g.inpmast_vin not in ('2GNAXUEVXL6151670','2GNAXUEV0L6147806','2GNAXUEV9L6147996','2GNAXUEV4L6146626',
          '2GNAXUEV4L6154256','2GNAXUEV8L6146550','2GNAXUEV8L6150291','2GNAXUEV9L6150932','2GNAXUEV0L6104339',
          '2T3A1RFV9RW425699')      
    where not exists (
      select 1
      from nc.vehicle_acquisitions
      where stock_number = f.control) 
    and not exists (
      select 1
      from nc.stg_availability   -- this is what limits the return set to journal 'PVI','CDH','GJE','EFT'
      where stock_number = f.control)) x) = 0, 'CTP returned to inventory in wrong journal';
end $$;    

-- select * from fin.dim_journal where journal_code = 'SJE'
/*
5/1/19
got a shit load of ctps returned to inventory in march and april for which the 
accounting was done in SVI rather than GJE
add them to nc.stg_availability
force source to ctp
in the future if this assert fails, need to analyze and proceed accordingly
5/9/19: 10 moved to inventory in SCA
5/10 2 more sca

insert into nc.stg_availability (the_date,journal_date,stock_number,
  vin,model_year,make, model,
  model_code,color_code,color, source)
select current_date, f.the_date, f.control, 
  g.inpmast_vin, g.year, g.make, g.model, g.model_code, 
  g.color_code, g.color,
  'ctp' as source
--   journal_code
--   case f.journal_code
--     when 'PVI' then 'factory'
--     when 'CDH' then 'dealer trade'
--     when 'GJE' then 'ctp'
--   end as source
from ( -- accounting journal
  select a.control, d.journal_code, max(b.the_date) as the_date, sum(a.amount) as amount
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_date between current_Date - 365 and current_date--'06/22/2018'
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account in ( -- all new vehicle inventory account
      select b.account
      from fin.dim_account b 
      where b.account_type = 'asset'
        and b.department_code = 'nc'
        and b.typical_balance = 'debit'
        and b.current_row
        and b.account <> '126104')   
  inner join fin.dim_journal d on a.journal_key = d.journal_key
--     and d.journal_code in ('PVI','CDH','GJE')
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where a.post_status = 'Y'
  group by a.control, d.journal_code
  having sum(a.amount) > 10000) f
inner join arkona.xfm_inpmast g on f.control = g.inpmast_stock_number
  and g.current_row = true
  and g.year::integer > 2017
where not exists (
  select 1
  from nc.vehicle_acquisitions
  where stock_number = f.control)
and not exists (
  select 1
  from nc.stg_availability
  where stock_number = f.control);

*/
-- when i finally get to putting this whole script into luigi, the way 
-- to handle an assertion like this, ie, of interest but not a show stopper
-- would be to generate an email



-- GM only compare vis delivery store to accounting source
-- 1/19 unexpected, G36238, this picked up what is probably a factory acquisition misidentified as ctp
-- why posting to 123700 was in GJE i have no idea, but this is definitely not a ctp acquisition
-- update nc.stg_Availability set source = 'factory' where stock_number = 'G36238'

-- 11/20/20
-- 1GCPYFED6LZ370794 in inventory as G40750, dealertraded on 09/28/20
-- reacquired on 11/20/20 via dealer trade and given stock number G41307


do $$
begin
assert ( -- Delivery store = Rydell, source <> factory
  select count(*)
  -- select a.*, regexp_replace(b.delivery_store, '\n', '', 'g')
  from nc.stg_availability a
  join gmgl.vehicle_vis b on a.vin = b.vin
  where regexp_replace(b.delivery_store, '\n', '', 'g') like 'RYDELL%'
    and a.stock_number not like '%R'
    and a.source <> 'factory') = 0, 'Delivery store = Rydell, source <> factory';
end $$;

-- -- 11/19/20 bunch of incomplete VIS meaning the delivery store = N/A
-- --   but invoiced to Rydell
-- -- SO, need to check delivery store before wholesale updating everything
-- -- incomplete VIS results in the regex result of N/A 
-- 
-- do $$
-- begin
-- assert ( -- Delivery store not Rydell, source = factory
--   select count(*)
--   -- select a.the_date, a.stock_number, a.vin, a.model_year, a.make, a.model, regexp_replace(b.delivery_store, '\n', '', 'g')
--   from nc.stg_availability a
--   join gmgl.vehicle_vis b on a.vin = b.vin
--   where regexp_replace(b.delivery_store, '\n', '', 'g') not like 'RYDELL%'
-- --     and delivery_store <> 'N/A'
--     and a.stock_number not like '%R'
--     and a.source = 'factory'
--     and a.stock_number not in ('g39177','g39176','g39175','g39173')) = 0, 'Delivery store not Rydell, source = factory';
-- end $$;
-- 
-- 
-- -- -- stg_Availability shows factory, good enough, no need to do anything
-- select * from nc.stg_availability where vin in ('1GCPDDEK0NZ563220','1GCPDDEK8NZ563188','1GCUDEEL4NZ563125')
-- -- which are the bad ones
-- select a.the_date, a.stock_number, a.vin, a.model_year, a.make, a.model, regexp_replace(b.delivery_store, '\n', '', 'g')
-- from nc.stg_availability a
-- join gmgl.vehicle_vis b on a.vin = b.vin
-- where regexp_replace(b.delivery_store, '\n', '', 'g') not like 'RYDELL%'
-- --     and delivery_store <> 'N/A'
--   and a.stock_number not like '%R'
--   and a.source = 'factory'
--   and a.stock_number not in ('g39177','g39176','g39175','g39173')

-- -- acct source = factory, delivery store = dealer trade
-- -- override acct source with vis delivery store
-- update nc.stg_availability
-- set source = 'dealer trade'
-- where stock_number in (
--   select a.stock_number
--   -- select a.*, regexp_replace(b.delivery_store, '\n', '', 'g')
--   from nc.stg_availability a
--   join gmgl.vehicle_vis b on a.vin = b.vin
--   where regexp_replace(b.delivery_store, '\n', '', 'g') not like 'RYDELL%'
--     and a.source = 'factory'
--     and a.stock_number not in ('g39177','g39176','g39175','g39173'))
-- returning *;  
    
do
$$
begin
assert (
  select count(*)
  -- select *
  from nc.stg_availability
  where ((stock_number like '%R'and source <> 'ctp')
    or (source = 'ctp' and stock_number not like '%R'))
    and source <> 'unwind') = 0, 'miscategorized ctp';
end
$$;  



/*
9 locator purchases against GJE instead of CDH, therefore source is categorized as ctp
wrong, per jeri, this is correct accounting:
  "Yes, the locator required us to do a wire transfer instead of a check. "

select * from nc.stg_availability where stock_number = 'g39135'

the fix:
update nc.stg_availability
set source = 'dealer trade'
where stock_number in (
  select stock_number
    from nc.stg_availability
    where ((stock_number like '%R'and source <> 'ctp')
      or (source = 'ctp' and stock_number not like '%R'))
      and source <> 'unwind');
*/

-- select * from arkona.xfm_bopmast where bopmast_stock_number = 'T10714'
-- 1/29/21 G41563 shows up later (2152), unwound 1/28, but not detected here ???
-- because it has not been deleted yet
-- select * from arkona.ext_bopmast where record_key = 64774
-- but the record_Status is null
-- email michelle, what is up with when the deal gets deleted
do $$
begin
assert (
  select count(*)
  -- select *
  from sls.deals a 
  join nc.vehicle_acquisitions aa on a.stock_number = aa.stock_number -- to be an unwind, previous acq record must exist
  join nc.vehicles b on a.vin = b.vin
  join arkona.xfm_inpmast c on b.vin = c.inpmast_vin
    and c.current_row = true
  where a.run_date > current_Date - 60
    and deal_status_code = 'deleted'
    and a.stock_number not in ( 'H11601', 'G35171','g36203','G37115','g37371','G37659',
      'G36862R','H12333','H13213','H13679','G39815','G39751','G40363','G41563','T10824','T10847')
    and a.bopmast_id not in (53059,57814,62544)
    and not exists (
      select 1
      from nc.vehicle_acquisitions
      where stock_number = a.stock_number
        and ground_date = a.run_date - 1)) = 0, 'unwinds to investigate';
end $$;  

/*
T10827 unwound twice
select * from sls.deals where stock_number = 't10827'
created the 3rd acquisition here:
insert into nc.vehicle_acquisitions
values('JTEFU5JR2P5295071','T10827','10/05/2023','12/31/9999','unwind', false)
*/

/*
G40363 looks like it was sold in transit and then unwound, vehicle finally arrived on 10/2/20
select * from nc.vehicle_Acquisitions where vin = '1GKS2DKL9MR111802'
select * from sls.deals where vin = '1GKS2DKL9MR111802'
select * from nc.vehicle_sales where vin = '1GKS2DKL9MR111802'
select * from arkona.ext_bopname where bopname_record_key = 1155051
*/

/*
G37371:
  55597: sold in june, unwound on on 6/30, deleted on 7/30
  55925: sold on 7/15
  unwind of 55597 doesn't show in acquisitions
  sale of 55925 doesn't show in sales
  wtf
*/

-- unwind acquisitions
-- test for unwind on same day as sale ?!?
-- 10/28/22: added and a.store_code = c.inpmast_company_number
insert into nc.stg_availability (the_date,stock_number,vin,model_year,
  model_code,color_code,color,ground_date,source)
select current_date, a.stock_number, a.vin, b.model_year,
  b.model_Code, c.color_code, b.color, 
  -- '01/28/2021',
  a.run_date - 1, 
  'unwind'
-- select *   
from sls.deals a
join nc.vehicle_acquisitions aa on a.stock_number = aa.stock_number -- to be an unwind, previous acq record must exist
join nc.vehicles b on a.vin = b.vin
join arkona.ext_inpmast c on b.vin = c.inpmast_vin
  and 
    case -- RY1 & RY8 separate inventory, so inpmast shows both
      when left(a.stock_number, 1) = 'T' then c.inpmast_company_number = 'RY8'
      else true
    end
--   and c.current_row = true
--   and a.store_code = c.inpmast_company_number --3/24/23 H16361 co# = RY1
where a.run_date > current_Date - 60
  and deal_status_code = 'deleted'
  and a.stock_number not in ( 'H11601', 'G35171','G36203','G37115','g37371','G37659','G37547',
    'G36862R','H12333','H13213','H13679','G39815','G39751','G40363','G40358','G41563','T10824','T10847','T10827')
  and a.bopmast_id not in (53059,57814)
  and not exists (
    select 1
    from nc.vehicle_acquisitions
    where stock_number = a.stock_number
      and ground_date = a.run_date - 1);


-------------------------------------------------------------------------------------
--< ground dates
-- requires:
--   ads.ext_dim_vehicle
--   ads.ext_fact_repair_order
-------------------------------------------------------------------------------------
-- ctp
update nc.stg_availability
set ground_date = journal_date
where source = 'ctp';


-- 11/6/23 replace ads.ext_fact_repair_order with dds.fact_repair_order and ads.ext_dim_vehicle with dds.dim_vehicle
-- if there is neither keyper nor ro, ground = 12/31/9999
update nc.stg_availability x
set ground_date = y.ground_date
from (
  select aa.stock_number, 
    coalesce(cc.keyper_date, coalesce(bb.ro_date, '12/31/9999'::date)) as ground_date
  from nc.stg_availability aa
  left join (
    select d.vin, min(b.the_date) as ro_date
    from dds.fact_repair_order a
    inner join dds.dim_date b on a.open_date = b.the_date
    inner join dds.dim_vehicle d on a.vehicle_key = d.vehicle_key-- needed for vin from ro
    inner join nc.stg_availability e on d.vin = e.vin
    -- as a "reminder" of a stop sale, joel wrote an ro on H13312 & H13313 (2815811,2815812) on 02/07,
    -- thereby signaling that they are HERE, but they won't be for a couple of months
    -- so, to keep them out of the inventory & feed, this seemed like the best place
    -- to catch as nc.vehicle_acquisitions drives all kinds of tables
    where a.ro not in ('2815811','2815812','2854653','16514005','16514061')
    group by d.vin) bb on aa.vin = bb.vin
    left join (
      select a.stock_number, min(transaction_date::date) as keyper_date
      from keys.ext_keyper a
      join nc.stg_availability b on a.stock_number = b.stock_number
-- *** double check the affect of this ***
      where 
        case
          when a.stock_number = 'G38187' then a.transaction_date::date > '10/01/2019'
          else 1 =1
        end       
      group by a.stock_number) cc on aa.stock_number = cc.stock_number 
  where aa.source <> 'ctp') y
where x.stock_number = y.stock_number
  and x.source <> 'unwind';  

-- ok, delete if ground = 12/31/9999 
-- those will be picked up as in transit/in system orders
delete 
-- select * 
from nc.stg_availability 
where ground_date = '12/31/9999';


-----------------------------------------------------------------------------------------------------------
--< this is where chrome gets run on nc.stg_availability
-----------------------------------------------------------------------------------------------------------
-- this is the query to run in cvd_new.py
select vin
from nc.stg_availability a
where not exists (
				select 1
				from cvd.nc_vin_descriptions
				where vin = a.vin);
                    
-- cvd_new.py
select * from cvd.nc_vin_descriptions where the_date = current_date

-- errors -------------------------------------------------------------------------------------------------
select vin, a.response->>'message'
from cvd.nc_vin_descriptions a
where the_date = current_date
  and (
			a.response->>'message' = 'Invalid vin'
      or a.response->>'message' = 'The VIN passed was modified to convert invalid characters'
      or a.response->>'message' = 'VIN not carried in our data')
union
select vin, 'motorcycle'
from cvd.nc_vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
where b->>'bodyType' = 'Motorcycle'
	and the_Date = current_date
union
select vin, 'Sparse with no style_id' 
from cvd.nc_vin_descriptions
where source = 'S'
	and the_date = current_date	  
	and vin in (
		select vin 
		from cvd.nc_vin_descriptions a
		join jsonb_array_elements(a.response->'result'->'vehicles') b on true
		where b->>'styleId' is null
			and the_date = current_date)	  
union			
select vin, 'No source'
from cvd.nc_vin_descriptions
where source is null
  and the_date = current_date
union
select vin, 'vinSubmitted <> vinProcessed'
from cvd.nc_vin_descriptions a
where a.response->'result'->>'vinProcessed' <> a.response->'result'->>'vinSubmitted';

-- colors ---------------------------------------------------------------------------------------------

select * from nc.stg_availability a
where not exists (select 1 from cvd.nc_colors where vin = a.vin)

select * from cvd.nc_vin_descriptions where vin = '5N1BT3BB9PC926269'

select a.vin, b.exterior, b.interior 
from nc.stg_availability a
left join (
	select vin, b->>'description' as exterior, c->>'description' as interior
	from cvd.nc_vin_descriptions a
	join jsonb_array_elements(a.response->'result'->'exteriorColors') b on true
	join jsonb_array_elements(a.response->'result'->'interiorColors') c on true
	where jsonb_array_length(a.response->'result'->'exteriorColors') = 1
		and jsonb_array_length(a.response->'result'->'interiorColors') = 1) b on a.vin = b.vin
where a.make <> 'Honda'	
  and not exists (
    select 1
    from cvd.nc_colors
    where vin = a.vin);

-- exclude Honda  
insert into cvd.nc_colors(vin,exterior,interior)
select a.vin, b->>'description', c->>'description'
from nc.stg_availability a
left join cvd.nc_vin_descriptions aa on a.vin = aa.vin
left join jsonb_array_elements(aa.response->'result'->'exteriorColors') b on true
left join jsonb_array_elements(aa.response->'result'->'interiorColors') c on true
where jsonb_array_length(aa.response->'result'->'exteriorColors') = 1
  and jsonb_array_length(aa.response->'result'->'interiorColors') = 1
  and a.make <> 'Honda'
  and not exists (
    select 1
    from cvd.nc_colors
    where vin = a.vin);

-- 1/28/24
-- 1 gm missing from colors
select * from cvd.nc_vin_descriptions where vin = '1GT39TEY7RF293909'
insert into cvd.nc_colors
values('1GT39TEY7RF293909','Summit White','Dark Walnut/Slate');


-- 1/3/24 KL79MSSL0RB106056 has type 1 & type 2 exterior
select a.vin, b->>'description', c->>'description'
from nc.stg_availability a
left join cvd.nc_vin_descriptions aa on a.vin = aa.vin
left join jsonb_array_elements(aa.response->'result'->'exteriorColors') b on true 
	and b->>'type' = '1'
left join jsonb_array_elements(aa.response->'result'->'interiorColors') c on true
where not exists ( -- 1 of the vins already has a color
  select 1
  from cvd.nc_colors
  where vin = a.vin);
  
-- 02/05/24 works for 1 honda
insert into cvd.nc_colors(vin,exterior,interior)
select a.vin, b->>'description', c->>'description'
from nc.stg_availability a
left join cvd.nc_vin_descriptions aa on a.vin = aa.vin
left join jsonb_array_elements(aa.response->'result'->'exteriorColors') b on true 
  and a.color_code = b->>'colorCode'
left join jsonb_array_elements(aa.response->'result'->'interiorColors') c on true
where not exists ( -- 1 of the vins already has a color
  select 1
  from cvd.nc_colors
  where vin = a.vin);
  
-- 1/11/24: this worked for 1 honda
insert into cvd.nc_colors(vin,exterior,interior)
select a.vin, b->>'description', c->>'description'
from nc.stg_availability a
left join cvd.nc_vin_descriptions aa on a.vin = aa.vin
left join jsonb_array_elements(aa.response->'result'->'exteriorColors') b on true 
	and b->>'type' = '1'
left join jsonb_array_elements(aa.response->'result'->'interiorColors') c on true
where not exists ( -- 1 of the vins already has a color
  select 1
  from cvd.nc_colors
  where vin = a.vin);

select * from cvd.nc_vin_descriptions where vin = '5FPYK3F65RB000849'

-- 1/7/24 11 hondas, no color
-- availability has color for all but 1, 5FNYF8H85RB002285

-- 01/28/24 6 hondas for color, exterior is ok, need to check inpmast for interior
-- all interior are black
-- select inpmast_vin, color, trim
-- from arkona.ext_inpmast
-- where inpmast_vin in ('3CZRZ2H57RM753344','5FNRL6H99RB029953','5FPYK3F86RB000246','1HGCY1F31RA032963','19XFL1H73RE015234','5FNYF8H8XRB005344')
insert into cvd.nc_colors(vin,exterior,interior)
select a.vin, 
  case
    when a.vin = '5FNYF8H85RB002285' then 'Platinum White Pearl'
    else b->>'description'
  end as exterior, 
  case
    when a.vin = '5FNYF8H85RB002285' then 'Black'
    else c->>'description'
  end as interior
--    b->>'colorCode', d.trim, d.color, d.color_code, similarity(b->>'description', d.color)
from nc.stg_availability a
left join cvd.nc_vin_descriptions aa on a.vin = aa.vin
left join jsonb_array_elements(aa.response->'result'->'exteriorColors') b on true 
  and a.color_code = b->>'colorCode'
left join jsonb_array_elements(aa.response->'result'->'interiorColors') c on true
left join arkona.ext_inpmast d on a.vin = d.inpmast_vin
where a.make = 'honda'
	and not exists ( 
		select 1
		from cvd.nc_colors
		where vin = a.vin)
--   and similarity(coalesce(b->>'description', 'WHITE'), d.color) > 0.1
  and 
    case
      when d.trim = 'BK' then c->>'description' = 'Black'
      when d.trim = 'GR' then c->>'description' = 'Gray'
      else true
    end;


-- do not leave here without insuring that all vins in cvd.nc_vin_descriptions have a row in cvd.nc_colors
select vin
from cvd.nc_vin_descriptions a
where not exists (
  select 1
  from cvd.nc_colors
  where vin = a.vin);

select * 
from nc.stg_availability a
where not exists (
  select 1
  from cvd.nc_colors
  where vin = a.vin)

do
$$
begin
assert ( -- test for null ground date
  select count(*)
  -- select *
  from nc.stg_availability a
  where ground_date is null) = 0, 'acquisitions with null ground date';
end
$$;


do $$-- all vins from nc.stg_availability exist in cvd.nc_vin_descriptions
begin
	assert (
		select count(*)
		from nc.stg_availability a
		where not exists (
				select 1
				from cvd.nc_vin_descriptions
				where vin = a.vin)) = 0, 'DANGER WILL ROBINSON, there are new vins missing from chr.describe_vehicle';
end
$$;  

-- 1/4/24 i don't think this is relevant any more
-- -- test for multiple chrome styles not resolved by model code
-- -- this was hard coded for 2019, don't think it is really an issue
-- do
-- $$
-- begin
-- assert (
--   select count(*) 
--   from (
--     select a.stock_number, a.vin
--     from nc.stg_availability a      
--     join cvd.nc_vin_descriptions b on a.vin = b.vin
--     join jsonb_array_elements(b.response->'result'->'vehicles') c on true
--     where b.style_count > 1
--       and a.model_code = c->>'mfrModelCode'::citext
--     group by a.stock_number, a.vin
--     having count(*) > 1) c) = 0, 'multiple chrome styles not resolved by model code, 1500 LD trim, exc fleet';
-- end
-- $$;    
-- 
-- select * from cvd.nc_vin_descriptions

-----------------------------------------------------------------------------------------------------------
--/> this is where chrome gets run on nc.stg_availability
-----------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--< new acquisitions where vin already exists in nc.vehicles (ctp)
12/29/23 dont know how relevant this is, but since CTPs are again picking up, leave it here for now
-------------------------------------------------------------------------------------
3/16/19
first time i have seen this
1GCUYDED6KZ190687
dealer trade to ??? on 1/16/19 as G35707
back in inventory on 3/15/19 as G36697
as a result
in nc.stg_availability source = factory
but 1GCUYDED6KZ190687 already exists in nc.vehicles
it will be bypassed in the insertion to nc.vehicles since it already exists there
so, i think, i need to manually change the source to dealer trade 
insert the acquisition here and we should be good

update nc.stg_availability
set source = 'dealer trade'
where stock_number = 'G36697';

7/13/19 another one
  3GTP9EEL9KG240120:
    dealer traded on 6/26/19 as G37359
    returned to inventory on 7/12/19 as G37600
    
  update nc.stg_availability
  set source = 'dealer trade', ground_date = '07/12/2019'
  where stock_number = 'G37600';
 
select * from nc.stg_availability where vin = 'KL7CJLSB3LB340047' 
select * from nc.vehicle_acquisitions where vin = '1GCGTDEN3N1167536' 

first, need to test for this in the future
-------------------------------------------------------------------------------------
do
$$
begin
  assert (
    select count(*)
    -- select *
    from nc.stg_availability a
    where source not in ('ctp','unwind')
      and stock_number not in ('G38676', 'G38079','G39135','G40861','G41307','H14776')
      and exists (
        select 1
        from nc.vehicles
        where vin = a.vin)) = 0, 'vehicles with factory source already exists in nc.vehicles';
end
$$;  

-- 08/10/23 i don't think i need to delete from nc.vehicles those that already exist
--    no delete today, see what happens
-- none had record in nc.vehicle_acquisitions
-- select * from nc.vehicle_acquisitions where vin in (select vin from nc.stg_availability)   
-- when i got to nc.stg_vehicles, include the not exists (select 1 from nc.vehicles where vin = a.vin)
-- success, this worked out ok on 8/10, no need to delete

-- -- they fucking changed the stock number
-- 8/29/22 8:59:17Stock# change: G45832 to G45232 by Gabrielle Beite
-- update nc.vehicle_acquisitions
-- set stock_number = 'G45232'
-- where stock_number = 'g45832'

-- select * from nc.vehicles where vin in (select vin from nc.stg_availability)   
-- delete from nc.vehicles where vin in (select vin from nc.stg_availability) 

-- insert ctp acquisitions into nc.vehicle_acquisitions
insert into nc.vehicle_acquisitions (vin,stock_number,ground_date,source,in_transit_sale)
select a.vin, a.stock_number, a.ground_date, a.source, false
from nc.stg_availability a
inner join nc.vehicles aa on a.vin = aa.vin
  and stock_number not in ('G38676','G47052','G47322','H16762','G47126','G46825')
  and a.source <> 'unwind'; -- unwinds are processed separately


-- make sure inpmcmnt returns a single row
do
$$
begin
  assert (
    select count(*)
    from (
      select stock_number
      from nc.get_r_units_missing_cost() a
      left join arkona.ext_inpcmnt b on a.vin = b.vin
        and position(a.stock_number in b.comment) <> 0
      group by stock_number
      having count(*) > 1) c) = 0, 'multiple rows';
end
$$;

-- 6/27/19 got a little hasty (maybe) in using inpcmnt for ground_date, 
--    double check it against inpmast.date_in_invent
do
$$
begin
  assert (
    select count(*)
      from (
        select stock_number, b.transaction_date, c.date_in_invent
        from nc.get_r_units_missing_cost() a
        left join arkona.ext_inpcmnt b on a.vin = b.vin
          and position(a.stock_number in b.comment) <> 0
        left join arkona.xfm_inpmast c on a.stock_number = c.inpmast_stock_number
          and c.current_row  
        where coalesce(b.transaction_date, 0) <> coalesce(c.date_in_invent, 0)) d) = 0, 'ground_date discrepancy';
end
$$;             
      
insert into nc.vehicle_acquisitions(vin,stock_number,ground_date,thru_date,source,in_transit_sale)
select distinct a.vin, a.stock_number, arkona.db2_integer_to_date_long(b.transaction_date),
  '12/31/9999'::date, 'ctp', false
from nc.get_r_units_missing_cost() a
left join arkona.ext_inpcmnt b on a.vin = b.vin
  and position(a.stock_number in b.comment) <> 0
where not exists (
  select 1
  from nc.vehicle_acquisitions
  where stock_number = a.stock_number);  
-------------------------------------------------------------------------------------
--/> new acquisitions where vin already exists in nc.vehicles (ctp)
-------------------------------------------------------------------------------------  


--------------------------------------------------------------------------------------------------
--< nc.vehicles parse cvd.nc_vin_descriptions into nc.stg_vehicles
--------------------------------------------------------------------------------------------------

/*  keep these
Pickup box sizes
24 Sierra 2500 HD: Standard  82.2", Long 96"
24 Sierra 1500: Short: 5.8', Standard: 6.6', Long 8.2'
24 Toyota Tacoma: Short: 5', Long: 6'
*/

truncate nc.stg_vehicles;
insert into nc.stg_vehicles (vin, chrome_Style_id, model_year, make, model,
  model_code, drive, cab, trim_level, engine, color, box_size)
select a.vin, c->>'styleId' as style_id,
  (b.response->'result'->>'year')::integer as model_year, b.response->'result'->>'make' as make, 
  b.response->'result'->>'model' as model, c->>'mfrModelCode' as model_code,
  c->>'driveType' as drive,
  case when c->>'bodyType' like '%Cab%' then c->>'bodyType' else 'n/a' end as cab,
  c->>'trim' as trim_level, 
  case
    when (b.response->'result'->>'model')::citext in ('bolt', 'bolt ev','lyriq','bZ4X') then 'EV'
    else e->>'name'
  end as eng_disp, 
  d.exterior as color,
  coalesce(f->>'name', 'n/a') as box_length
-- select a.*  
from nc.stg_Availability a
join cvd.nc_vin_descriptions b on a.vin = b.vin
join jsonb_array_elements(b.response->'result'->'vehicles') as c on true
join cvd.nc_colors d on a.vin = d.vin
left join jsonb_array_elements(b.response->'result'->'techSpecs') e on e->>'id' = '10120' -- engine displacement
left join jsonb_array_elements(b.response->'result'->'techSpecs') f on f->>'id' = '18090' -- box length
where not exists (
  select 1
  from nc.vehicles
  where vin = a.vin)
  and 
    case  -- disambiguates hondas and toyotas
      when a.make = 'toyota' then left(a.model_code, 4) = c->>'mfrModelCode'
      else a.model_code = c->>'mfrModelCode'
    end 
  and
    case
      when a.vin = '5TFMA5DB6RX164624' then c->>'styleId' = '440367'  -- fucking toyota GS / SE, should be a Natl option but have not seen one
      else true
    end

-- NO MORE CONFIG
-- -- add the configuraton_id  
-- update nc.stg_vehicles z
-- set configuration_id = (
--   select configuration_id
--   from nc.vehicle_configurations a
--   where a.chrome_style_id = z.chrome_style_id
--     and a.trim_level = z.trim_level
--     and a.engine = coalesce(z.engine, 'EV'));
-- select * from nc.vehicle_configurations
-- 
-- do
-- $$
-- begin
-- assert (
--   select count(*)
--   -- select *
--   from nc.stg_vehicles
--   where configuration_id is null) = 0, 'null configuration_id';
-- end
-- $$;     
-- 
--  select *
--   from nc.stg_vehicles
--   where configuration_id is null
--   order by make, model
  
-- select * from chr.describe_vehicle where vin = '5TFMA5DB2PX084377'
-- 6/22/21
-- fuck me 1GCRYDEK3MZ359698 2021, Chevrolet, Silverado 1500, CK10753, LT, double, 2.7
-- but with a styleid of 413036, nc.vehicle_configurations wont accept it as a new config
-- broke precedent by making the trim LT w/1LT
-- 
-- 12/14/22
--   ** i may have messed up some data dealing witth this stuff incorrectly recently, essentially randomly changing shit to make queries pass
--   well, build data has broken the config_idx
--   23 equinox vin 3GNAXUEG1PL102439
-- 	ERROR: duplicate key value violates unique constraint "config_idx"
-- 	SQL state: 23505
-- 	Detail: Key (model_year, make, model, model_code, trim_level, cab, engine)=(2023, Chevrolet, Equinox, 1XY26, LT, n/a, 1.5) already exists.
-- 	seems the choice is to abandon the index or add chrome_style_id to it, go with option 2
-- 	ERROR:  cannot drop index nc.config_idx because other objects depend on it
-- 	DETAIL:  constraint vehicles_model_year_fkey on table nc.vehicles depends on index nc.config_idx
-- 	
-- 	ALTER TABLE nc.vehicles DROP CONSTRAINT vehicles_model_year_fkey;
-- 	DROP INDEX nc.config_idx;
-- 	create unique index config_idx on nc.vehicle_configurations(model_year, make, model, model_code, trim_level, cab, engine, chrome_style_id) ;


	
-- -- insert new configuration
-- -- after inserting new config, rerun update nc.stg_vehicles z set configuration_id = (...
-- insert into nc.vehicle_configurations(model_year,make,model,model_code,trim_level,cab,
--   drive,engine,chrome_style_id,alloc_group, box_size, name_wo_trim,config_type,level_header)
-- select model_year, make, model, model_code, trim_level, cab, drive, 
--   engine, chrome_Style_id, 
--   'n/a' as alloc_group, -- ALLOCATION GROUP WHEN POSSIBLE, ELSE N/A
--   box_size, 
--   'n/a', -- by default name_wo_trim = n/a, populate only when necessary for unique configs
--   'tde' as config_type, 'Platinum 4WD 3.5' as level_header
-- -- select *  
-- from nc.stg_vehicles
-- where vin in ('3TMCZ5ANXPM654473'); -- JN1BJ1CW0LW375919
-- 
-- -- if a new config is added, need to rerun update of config_id in stg_vehicles 
-- update nc.stg_vehicles z
-- set configuration_id = (
--   select configuration_id
--   from nc.vehicle_configurations a
--   where a.chrome_style_id = z.chrome_style_id
--     and a.trim_level = z.trim_level
--     and a.engine = z.engine);

-- insert the new vehicles
insert into nc.vehicles(vin,chrome_style_id,model_year,make,model,
  model_code,drive,cab,trim_level,engine,color,configuration_id)
select vin,chrome_style_id::citext,model_year::integer,make,model,
  model_code,drive,cab,trim_level,engine,color,configuration_id
from nc.stg_vehicles a
where not exists (
  select 1
  from nc.vehicles 
  where vin = a.vin);

--------------------------------------------------------------------------------------------------
--/> nc.vehicles
--------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------
--< nc.vehicle_acquisitions
--------------------------------------------------------------------------------------------------



insert into nc.vehicle_acquisitions  (vin,stock_number,ground_date,source,in_transit_sale)
select a.vin, a.stock_number, a.ground_date, a.source, false
from nc.stg_availability a
left join nc.vehicle_acquisitions b on a.stock_number = b.stock_number
  and a.ground_date = b.ground_date
where b.vin is null  
  and a.source <> 'unwind';


-- and finally, the unwind acquisition
-- not sure how this will work for multiple unwinds, only want to update the 
-- must recent acquisition record 

-- select * from nc.vehicle_acquisitions where stock_number = 'g39709'
-- -- 
-- update nc.stg_availability 
-- set ground_Date = '05/19/2020'
-- -- select * from nc.stg_availability 
-- where stock_number = 'G38832R'
-- 
-- select * from nc.vehicle_acquisitions where stock_number = 'T10824'
-- select * from nc.stg_Availability where stock_number = 'T10824'
-- update nc.stg_availability set ground_date = '08/29/2023' where stock_number = 'T10824'

update nc.vehicle_acquisitions x
set thru_date = y.the_date
from (
  select a.vin, a.stock_number, a.ground_date - 1 as the_date
  from nc.stg_availability a
  where source = 'unwind') y
where x.stock_number = y.stock_number;

-- select * from nc.vehicle_acquisitions where stock_number = 'h15077'

insert into nc.vehicle_acquisitions  (vin,stock_number,ground_date,source,in_transit_sale)
select a.vin, a.stock_number, ground_date, a.source, false
from nc.stg_availability a
where source = 'unwind';   

-- test for open acquisitions with no non-void gl transactions
-- this will also expose the r_units_missing_cost from above, these require no action
do
$$
begin
assert (
  select count(*)
  from (
    select * 
    from nc.vehicle_acquisitions a
    where thru_date > current_date
      and stock_number <> 'G47874R'
      and not exists (
        select 1
        from fin.fact_gl
        where control = a.stock_number
          and post_status = 'Y'))x ) = 0, 'possible void dealer trade';
end
$$;

-- 8/16/19 i believe this excludes the r_units_missing_cost which generate a false positive
do
$$
begin
assert (
  select count(*)
  from (
    select * 
    from nc.vehicle_acquisitions a
    where thru_date > current_date
      and not exists (
        select 1
        from fin.fact_gl
        where control = a.stock_number
          and post_status = 'Y')
      and not exists (
        select 1
        from (
          select *
          from nc.get_r_units_missing_cost())aa
        where a.stock_number = aa.stock_number))x ) = 0, 'possible void dealer trade';      
end
$$;


--------------------------------------------------------------------------------------------------
--/> nc.vehicle_acquisitions
--------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------
--< SALES
--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
--< nc.stg_sales
--------------------------------------------------------------------------------------------------
-- accounting should include all sales
-- first step, accounting info and vin from inpmast
truncate nc.stg_sales;
insert into nc.stg_sales (stock_number, vin, trans_description, amount, acct_date,ground_date)
select a.control, f.inpmast_vin, e.description, a.amount, b.the_date, g.ground_date
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
--   and b.the_date between current_Date - 40 and current_date
join fin.dim_account c on a.account_key = c.account_key
  and c.account in ( -- all new vehicle inventory account
    select b.account
    from fin.dim_account b 
    where b.account_type = 'asset'
      and b.department_code = 'nc'
      and b.typical_balance = 'debit'
      and b.current_row
      and b.account <> '126104')    
join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code = 'VSN'
join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
left join arkona.xfm_inpmast f on a.control = f.inpmast_stock_number
  and f.current_row = true  
left join nc.vehicle_acquisitions g on a.control = g.stock_number
  and g.thru_date = ( -- need most recent acquisition
    select max(thru_date)
    from nc.vehicle_acquisitions
    where stock_number = g.stock_number)  
where a.post_status = 'Y'
  and abs(a.amount) > 10000
  and f.year::integer > 2022
  and a.control not in( 'G38994','g39972','G48834','G48985','G48948') --and a.control = 'g44953'
  and not exists (
    select 1
    from nc.vehicle_sales
    where stock_number = a.control
      and ground_date = g.ground_date) ;


delete
-- select *
from nc.stg_sales
where (
  (stock_number = 'G45423' and trans_description = 'BRITTEN, KEITH PAUL') or    
  (stock_number = 'G45370' and trans_description = 'KNUTSON, JENNIFER LYNN') or
  (stock_number = 'G45734' and trans_description = 'HUSCHLE, BRIAN JAMES') or
  (stock_number = 'G45625' and trans_description = 'NOSBUSCH, CASEY ANN') or
  (stock_number = 'G45625' and trans_description = 'NOSBUSCH, CASEY ANN') or
  (vin  = '2T3F1RFV2NC39E379' and trans_description = 'SUN, RUNZE') or
  (stock_number = 'T10250' and trans_description = 'MERIWETHER, LUCAS') or
  (stock_number = 'G45625' and trans_description = 'NOSBUSCH, CASEY ANN') or
  (stock_number = 'G45892' and trans_description = 'MYRON, WILLIAM JOHN') or
  (stock_number = 'G45407' and trans_description = 'TENG, YUXIAO') or
  (stock_number = 'T10363' and trans_description = 'A1 ACCREDITED BATTERIES') or
  (stock_number = 'T10456' and trans_description = '5TFNA5DB4PX072378') or
  (stock_number = 'G45892' and trans_description = 'MYRON, WILLIAM JOHN') or
  (stock_number = 'G45370' and trans_description = 'KNUTSON, JENNIFER LYNN') or
  (stock_number = 'G45734' and trans_description = 'HUSCHLE, BRIAN JAMES') or
  (stock_number = 'G45127' and trans_description = 'SCHULZ, KALLIE JEAN') or
  (stock_number = 'G46973' and trans_description = 'GONZALEZ-MARTINEZ, RODNEY') or
  (stock_number = 'G47000' and trans_description = 'MATTHEW STRAND HARDWARE INC') or
  (stock_number = 'G46834' and trans_description = 'MLODZIK, LUANN RENEE') or
  (stock_number = 'G46821' and trans_description = 'EAGLEMAN, RONALD MARTIN') or
  (stock_number = 'T10827' and trans_description = 'STONE, MARILYN LORRAINE') or
  (stock_number = 'T10847' and trans_description = 'KALBRENER, KELLY ANN') or  
  (stock_number = 'G48407' and trans_description = 'KNOPE JR, DAVID MATTHEW') or 
  (stock_number = 'T10827' and trans_description = 'RAYNER, RENAE BRANSON') or
  (stock_number = 'H17145' and trans_description = 'SATHER, DAVID ROY') or
  (vin in('JTMEB3FV6PD160942','1HGCY1F36PA056222','1HGCY1F37PA046511') and acct_date > '10/31/2023') or
  (stock_number = 'T10965' and trans_description = 'PRICE MANAGEMENT, LLC') or
  (stock_number = 'T10961' and trans_description = 'THIELE, LORENZO THURMAN') or
  (stock_number = 'H17145' and trans_description = 'BUTTERFIELD, SABRINA JEANNE') or
  (stock_number = 'G49029' and trans_description = 'DIETRICH, WAYNE GERALD') or
  (stock_number = 'T11137' and trans_description = 'TCB TAX SERVICE LLC, BRADLEY W') or
  (stock_number = 'G47608R' and trans_description = 'SHILTZ, GREGORY GORDON') or
  (stock_number = 'G49205' and trans_description = 'JEFFERY, BILLY JOE') or  
  (stock_number = 'T11136' and trans_description = 'FETZER, JEFFREY REN') or  
  (stock_number = 'H15883' and trans_description = 'STORANDT, SARAH CATHERINE' and acct_date < '10/05/2022'));     

do 
$$
begin
assert ( -- ground date null
  select count(*)
	--   select a.*, b.make
		from nc.stg_sales a
		left join arkona.ext_inpmast b on a.stock_number = b.inpmast_stock_number
  where a.ground_date is null) = 0, 'NULL ground date: ';  
end
$$;

/*
select * from cvd.nc_vin_descriptions where vin in('5FNRL6H96RB028159')  

*/

select * from nc.vehicle_Acquisitions where vin = '5GAEVBKW3RJ128018'
-------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------
-- THESE 2 QUERIES WILL HAVE TO BE UPDATED TO USE CVD

-- 1/11/24: 10 in nc.stg_sales, already exist in nc.vehicles but not in cvd.vin_descriptions, this will happen for awhile

-- 1/28/24  49 vehicles in stg_sales, all already exist in nc.vehicles, so i am doing nothing with cvd on them

select * from nc.stg_sales a
where not exists (select 1 from cvd.nc_vin_descriptions where vin = a.vin)

-- as long as they are already inb nc.vehicles, ok
select * from nc.stg_sales a
where not exists (select 1 from nc.vehicles where vin = a.vin)

!!! if add vins to cvd.nc_vin_descriptions: Check ERRORS and do COLORS

select * from cvd.nc_vin_descriptions where vin in('5FNRL6H96RB028159')  

select * from nc.stg_sales where vin = '5FNRL6H96RB028159'

select * from cvd.nc_colors where vin = '5FNRL6H96RB028159'

insert into cvd.nc_colors(vin,exterior,interior)
select a.vin, 
  case
    when a.vin = '5FNYF8H85RB002285' then 'Platinum White Pearl'
    else b->>'description'
  end as exterior, 
  case
    when a.vin = '5FNYF8H85RB002285' then 'Black'
    else c->>'description'
  end as interior,
   b->>'colorCode', d.trim, d.color, d.color_code, similarity(b->>'description', d.color)
from nc.stg_sales a
left join cvd.nc_vin_descriptions aa on a.vin = aa.vin
left join jsonb_array_elements(aa.response->'result'->'exteriorColors') b on true 
left join jsonb_array_elements(aa.response->'result'->'interiorColors') c on true
left join arkona.ext_inpmast d on a.vin = d.inpmast_vin
where not exists ( 
		select 1
		from cvd.nc_colors
		where vin = a.vin)
  and similarity(coalesce(b->>'description', 'WHITE'), d.color) > 0.1
  and 
    case
      when d.trim = 'BK' then c->>'description' = 'Black'
      when d.trim = 'GR' then c->>'description' = 'Gray'
      else true
    end;

-- ended up googling the vin to get the interior color
insert into cvd.nc_colors
values('5FNRL6H96RB028159','Platinum White Pearl','Beige');


    
do
$$
declare
  _stock_number citext := 'H17384';
begin  
-- nc.vehicles 
  insert into nc.vehicles(vin,chrome_style_id,model_year,make,model,
    model_code,drive,cab,trim_level,engine,color) -- ,configuration_id)

	select a.vin, c->>'styleId' as style_id,
		(b.response->'result'->>'year')::integer as model_year, b.response->'result'->>'make' as make, 
		b.response->'result'->>'model' as model, c->>'mfrModelCode' as model_code,
		c->>'driveType' as drive,
		case when c->>'bodyType' like '%Cab%' then c->>'bodyType' else 'n/a' end as cab,
		c->>'trim' as trim_level, 
		case
			when (b.response->'result'->>'model')::citext in ('bolt', 'bolt ev','lyriq','bZ4X') then 'EV'
			else e->>'name'
		end as eng_disp, 
		d.exterior as color
	from nc.stg_sales a  -- nc.vehicle_acquisitions a -- for testing-
	join cvd.nc_vin_descriptions b on a.vin = b.vin
	join jsonb_array_elements(b.response->'result'->'vehicles') as c on true
	join cvd.nc_colors d on a.vin = d.vin
	left join jsonb_array_elements(b.response->'result'->'techSpecs') e on e->>'id' = '10120' -- engine displacement
	left join jsonb_array_elements(b.response->'result'->'techSpecs') f on f->>'id' = '18090' -- box length
	where a.stock_number = _stock_number;	   
	 
-- !!!!!!!!!!!!!!!!!!!!!!!! manually set ground_date and source !!!!!!!!!!!!!!!!!!
-- thru_date will be set by normal processing later in the script
  insert into nc.vehicle_acquisitions(vin,stock_number,ground_date,thru_date,source,in_transit_sale)
  select vin, stock_number, '01/05/2024', '12/31/9999', 'factory', true   --------------------- ground date - same as sale date
  from nc.stg_sales
  where stock_number = _stock_number;

-- update the ground date in nc.stg_sales
  update nc.stg_sales
  set ground_date = (
    select ground_date
    from nc.vehicle_acquisitions
    where stock_number = _stock_number
      and thru_date > current_Date)
  where stock_number = _stock_number;
  
end $$;


-------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------
G47608R
-- select * from nc.stg_sales where stock_number = 'H17262'
-- select * from sls.deals where stock_number in ('H17262')
-- select * from nc.vehicles where vin = 'JN8BT3BBXP2480155'
-- 
-- select * from chr.build_data_describe_vehicle where vin = 'JN8BT3BBXP2480155'
-- 
-- select * from arkona.xfm_bopmast where bopmast_stock_number = 'G49205' order by row_from_date

-- select * from arkona.ext_bopname where bopname_record_key in (1021898,1188813)



do
$$
begin
assert (
  select count(*)
  from (
    select b.stock_number, count(*)
    from nc.stg_sales a
    left join (
      select stock_number, bopmast_id, max(seq) as seq, max(delivery_date) as delivery_date
      from sls.deals
      where bopmast_id not in(16569,53323,16366,52312,53844, 54171,54563,
        80923,1851,80992,81236,80483,1989,26262) -- exclude the fucking prev unwinds that i haven't figured out yet H11981,G36153,G35854,g35074,g36884
      group by stock_number, bopmast_id) b on a.stock_number = b.stock_number
    where trans_description not like 'DT%' -- half assed removal of dealer trades
    group by b.stock_number
    having count(*) > 1) x) = 0, 'dup stock numbers';
end
$$;  

-- bopmast_id & delivery_date
update nc.stg_sales x
set bopmast_id = y.bopmast_id,
    delivery_date = y.delivery_date
from (
  select b.*
  from nc.stg_sales a
  left join (
    select stock_number, bopmast_id, max(seq) as seq, max(delivery_date) as delivery_date
    from sls.deals
    where bopmast_id not in(16569,53323,16366,52312,17469,55597,17689,17647,57814,80992,81236) -- exclude the fucking prev unwinds that i haven't figured out yet H11981,G36153
    group by stock_number, bopmast_id) b on a.stock_number = b.stock_number) y
where x.stock_number = y.stock_number;

-- -- and the manual fix
-- update nc.stg_sales
-- set delivery_date = '02/09/2019'
-- where stock_number = 'h11801';

-- sale_types dealer trade & ctp
update nc.stg_sales x
set sale_type =
  case
    when bopmast_id is null then 'dealer trade'
    when trans_description like '%RYDELL%' then 'ctp'
  end;

   -- sale_type fleet
update nc.stg_sales x
set sale_type = 'fleet'
where stock_number in (
  select a.stock_number
  from nc.stg_sales a
  inner join (
    select a.control, b.the_date
    from fin.fact_gl a
    inner join dds.dim_date b on a.date_key = b.date_key
    inner join fin.dim_account c on a.account_key = c.account_key
    inner join (
      select distinct d.gl_account
      from fin.fact_fs a
      inner join fin.dim_fs b on a.fs_key = b.fs_key
        and b.year_month > 201601
        and b.page in (5,8,9,10)
        and b.line in (22,43)
        and b.col = 1 -- sales
      inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
--         and c.store = 'ry1'
      inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key) d on c.account = d.gl_account
    where a.post_status = 'Y') b on a.stock_number = b.control);

-- dealer trade delivery dates
update nc.stg_sales
set delivery_date = acct_date
where sale_type = 'dealer trade';

-- and finally, everything else is retail
update nc.stg_sales
set sale_type = 'retail'
where sale_type is null;


select *
from nc.stg_sales a -- left join nc.vehicles b on a.vin = b.vin
order by stock_number



select * from arkona.ext_bopmast where bopmast_stock_number = 'T11136'


do
$$
begin
assert (
  select count(*)
  -- select *
    from nc.stg_sales
    where ground_date > delivery_date) = 0, 'ground date > delivery date';
end
$$;     
-- 
-- update nc.stg_sales
-- set delivery_date = '01/29/2024'
-- --     ground_date = '01/31/2024'
-- where stock_number in( 'T11031')

-- update nc.stg_sales
-- set ground_date = '11/29/2023'
-- where stock_number in( 'H17214')


-- update nc.vehicle_acquisitions.thru_date for sales

update nc.vehicle_acquisitions x
set thru_date  = y.delivery_date
from (
  select a.*
  from nc.stg_sales a
  left join nc.vehicle_acquisitions b on a.stock_number = b.stock_number) y
where x.stock_number = y.stock_number
  and x.thru_date = '12/31/9999';  


-- and install the sales
insert into nc.vehicle_sales
select stock_number, ground_date, vin, coalesce(bopmast_id, -1), delivery_date, sale_type
from nc.stg_sales a
where not exists (
  select 1 
  from nc.vehicle_sales
  where stock_number = a.stock_number
    and delivery_date = a.delivery_date)



do
$$
begin
assert (
  select count(*)
  -- select *
  from nc.vehicle_sales a
  join nc.vehicle_Acquisitions b on a.stock_number = b.stock_number
    and a.ground_date = b.ground_date
    and b.thru_date > current_date) = 0, 'sales with open acquisition';
end
$$;  

-- test for accounting in/out generating multiple acquisitions/sales
-- sometimes a resold unwind gets the wrong bopmast_id
-- if an unwind is subsequently dealer traded, bopmast_id needs to be set to -1
do
$$
begin
  assert (
    select count(*)
    from (
      select stock_number, vin, bopmast_id
      from nc.vehicle_sales
      where stock_number not in ('H15077', 'G40915R','G44065','H15636','G45172','T10241','T10250',
				'G45423','G45625','G45407','G46821','G47251','T10714', 'T10874', 'T10827','H16693','T10824',
				'T10847','H16694','G48623','T11013','T10965','G49038','T10975','G48424','G48825','H17262','T11031')
      group by stock_number, vin, bopmast_id
      having count(*) > 1) a) = 0, 'possible phony deals';
end
$$;    


-- -- select * from nc.vehicle_sales where stock_number in ('G46821','G47251')
-- update nc.vehicle_sales
-- set bopmast_id = 24733
-- where stock_number = 'H16361'
--   and delivery_date = '03/22/2023'


--------------------------------------------------------------------------------------------------
--/> SALES
--------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------
--< misc reconciliations
--------------------------------------------------------------------------------------------------


select a.*
from nc.vehicle_sales a
-- -- expose the unwinds with this join
-- left join nc.vehicle_acquisitions c on a.stock_number = c.stock_number
--   and c.ground_date >= a.delivery_date
join arkona.xfm_inpmast b on a.stock_number = b.inpmast_stock_number
  and b.status = 'I'
  and b.current_row
where a.stock_number not like 'H%'
  and not exists ( -- no post sale acquisition
    select 1
    from nc.vehicle_acquisitions
    where stock_number = a.stock_number
      and ground_date >= a.delivery_date)
order by delivery_date;         

-- -- dealer trades that didn't get delivered
-- select * from nc.vehicle_acquisitions where stock_number in ('G48948','G48985','G48834')
-- update nc.vehicle_acquisitions
-- set thru_date = '12/31/9999'
-- where stock_number in ('G48948','G48985','G48834');
-- 
-- select * from nc.vehicle_sales where stock_number in ('G48948','G48985','G48834')
-- delete from nc.vehicle_sales where stock_number in ('G48948','G48985','G48834');

-- deals that go thru the 2 stage unwind in bopmast, resulting in -2 unit count
select run_date, bopmast_id, deal_Status, stock_number, primary_sc, secondary_sc, fi_manager, capped_date, year_month, unit_count 
from sls.deals
where bopmast_id in (
  select bopmast_id 
  from sls.deals a
  where deal_status_code = 'none'
    and exists (
      select 1
      from sls.deals
      where bopmast_id = a.bopmast_id
        and deal_status_code = 'deleted'))
  and run_date > '07/31/2019'    
  and bopmast_id not in (17678,17689,55331,55783,56292,56361,57522,57509,57283,57521,65456,
    57814,57839,58176,58197,58099,49879,60016,60799,60945,60857,19628,61341,61780,20234,63727,
    65600,21278,66596,67279,67695,22051,68733,69624,71964,23620,136,73139,23963,316,24046,75114,
    75958,696,76129,76326,1029,78133,1474,1573,1556, 1578,25706,79968)    
order by bopmast_id, run_date 

-- select * from sls.deals where stock_number = 'G43556L'
-- delete from sls.deals where bopmast_id = 64774 and deal_status_code = 'none'
-- 
-- one type of fix
update sls.deals
set unit_count = 0
-- select * from sls.deals
where bopmast_id = 79968
  and deal_status_code = 'deleted'
  and store_code = 'RY1'

--------------------------------------------------------------------------------------------------
--/> misc reconciliations
--------------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------------
--< ORDERS
--------------------------------------------------------------------------------------------------

-- 1. delete rows from nc.open_orders where the vin now exists in nc.vehicles (vehicle on ground or sold in transit)
delete 
-- select *
from nc.open_orders
where vin in (
  select a.vin
  from nc.open_orders a
  join nc.vehicles b on a.vin = b.vin);


-- 1/30/19 limit to order_type TRE
-- 3/13/19 per Taylor, exclude category Placed
insert into nc.open_orders
select order_number, event_code, vin, order_type, alloc_Group, 
  model_year::integer, model_code, 
  dan, 
  case
    when order_number in ('XBNRW6','XBNRW5','WZBNMV','WZTRMF') then '1SL'
    when order_number in ('XDSXRN','XDSXRP') then '4SB'
    when order_number in ('XDSXRR','XDSXRQ') then '5SA'
    else peg
  end as peg, 
  case
    when order_number in ('XDSXRN') then 'GAZ'
    when order_number in ('XDSXRP') then 'GAN'
    when order_number in ('XDSXRR') then 'G1W'
    when order_number in ('XDSXRQ') then 'GB8'
    else color
  end as color_code, 
  category 
from ( -- orders where current_event < 5000, type = TRE, category <> Placed
  select b.vin,
    b.order_number, b.order_type, b.alloc_group, b.model_year, 
    b.model_code, b.dan, b.peg, b.color, 
    c.event_code, c.from_Date, c.thru_date, d.*
  from gmgl.vehicle_orders b
  left join gmgl.vehicle_order_events c on b.order_number = c.order_number
    and c.thru_date = (
      select max(thru_date)
      from gmgl.vehicle_order_events
      where order_number = c.order_number)
  left join gmgl.vehicle_event_codes d on c.event_code = d.code
  where left(d.code, 1)::integer < 5
    and order_type = 'TRE'
    and category <> 'Placed'
    and b.order_number not in ('VXXMJ1','VXXMJ2','VXXMJZ','VZHB4R','VZHBJ8','VZSRMK','VZSRMM','WBVX79','WCGN2T')) aa
where not exists (
  select 1
  from nc.vehicles
  where coalesce(vin, 'no vin') = aa.vin) order by aa.vin
on conflict (order_number)  
do update set (event_code,vin,order_type,alloc_group,model_year,model_code,
  dan,peg,color_code,category)
= (excluded.event_code, excluded.vin, excluded.order_type, excluded.alloc_group, 
     excluded.model_year, excluded.model_code, 
      excluded.dan, excluded.peg,excluded.color_code, excluded.category);


-- do we need to run cvd?
do $$
begin
assert (
  select count(*)
  -- select *
  from nc.open_orders a
  where a.vin is not null
    and not exists (
      select 1
      from cvd.nc_vin_descriptions
      where vin = a.vin)) = 0, 'vins not in cvd.nc_vin_descriptions';
end $$;          


---------------------------------------------------------------------------------------------------------
--< run CVD for open orders
---------------------------------------------------------------------------------------------------------
-- 12/29/23 87 to run thru CVD      
do $$
begin
assert (
  select count(*)
  -- select *
  from nc.open_orders a
  where a.vin is not null
    and not exists (
      select 1
      from cvd.nc_vin_descriptions
      where vin = a.vin)) = 0, 'vins not in chr.nc_describe_vehicle';
end $$;    

-- CVD errors
select vin, a.response->>'message'
from cvd.nc_vin_descriptions a
where the_date = current_date
  and (
			a.response->>'message' = 'Invalid vin'
      or a.response->>'message' = 'The VIN passed was modified to convert invalid characters'
      or a.response->>'message' = 'VIN not carried in our data')
union
select vin, 'motorcycle'
from cvd.nc_vin_descriptions a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
where b->>'bodyType' = 'Motorcycle'
	and the_Date = current_date
union
select vin, 'Sparse with no style_id' 
from cvd.nc_vin_descriptions
where source = 'S'
	and the_date = current_date	  
	and vin in (
		select vin 
		from cvd.nc_vin_descriptions a
		join jsonb_array_elements(a.response->'result'->'vehicles') b on true
		where b->>'styleId' is null
			and the_date = current_date)	  
union			
select vin, 'No sourCVDce'
from cvd.nc_vin_descriptions
where source is null
  and the_date = current_date
union
select vin, 'vinSubmitted <> vinProcessed'
from cvd.nc_vin_descriptions a
where a.response->'result'->>'vinProcessed' <> a.response->'result'->>'vinSubmitted';

-- CVD colors

-- this picks up 50 of the 87
select vin, b->>'description', c->>'description'
from cvd.nc_vin_descriptions a
join jsonb_array_elements(a.response->'result'->'exteriorColors') b on true
join jsonb_array_elements(a.response->'result'->'interiorColors') c on true
where jsonb_array_length(a.response->'result'->'exteriorColors') = 1
  and jsonb_array_length(a.response->'result'->'interiorColors') = 1
  and the_date = current_date
	and not exists (
		select 1
		from cvd.nc_colors
		where vin = a.vin)  

-- i am not crazy about unknow on the interior, but dont see another alternative currently
insert into cvd.nc_colors(vin,exterior,interior)
select a.vin, coalesce(b.exterior, c.color) as exterior, coalesce(b.interior, 'unknown') as interior
from cvd.nc_vin_descriptions a
left join (
	select vin, b->>'description' as exterior, c->>'description' as interior
	from cvd.nc_vin_descriptions a
	join jsonb_array_elements(a.response->'result'->'exteriorColors') b on true 
	join jsonb_array_elements(a.response->'result'->'interiorColors') c on true
	where jsonb_array_length(a.response->'result'->'exteriorColors') = 1
		and jsonb_array_length(a.response->'result'->'interiorColors') = 1
		and the_date = current_date
		and not exists (
			select 1
			from cvd.nc_colors
			where vin = a.vin)) b on a.vin = b.vin
left join nc.open_orders c on a.vin = c.vin			
where not exists (
  select 1
  from cvd.nc_colors
  where vin = a.vin);	


---------------------------------------------------------------------------------------------------------
--/> run CVD for open orders
---------------------------------------------------------------------------------------------------------	


-- !!!! this may be the only place where i continue using nc.vehicle_configurations 
---------------------------------------------------------------------------------------------------------
--< generate configuration data for open orders where it does not exist yet
---------------------------------------------------------------------------------------------------------	
truncate nc.stg_vehicle_configurations;
insert into nc.stg_vehicle_configurations (model_year, make, model, model_code,
  trim_level,cab,drive,engine,chrome_Style_id,alloc_group, box_size, name_wo_trim)
-- limited to > 2022
select distinct aa.*
from (
	select (b.response->'result'->>'year')::integer as chr_model_year,
	  b.response->'result'->>'make' as chr_make, 
	  b.response->'result'->>'model' as chr_model,
	  c->>'mfrModelCode' as model_code,
	  coalesce(c->>'trim', 'none') as chr_trim,
	  case when c->>'bodyType' like '%Cab%' then c->>'bodyType' else 'n/a' end as cab, 
	  c->>'driveType' as drive,
	  d->>'name' as engine,
	  c->>'styleId' as chr_style_id,
	  a.alloc_group,
-- 	  coalesce(e->>'name', 'n/a') as box_length,
		case 
			when c->>'bodyType' like '%Cab%' or c->>'bodyType' like '%Crew%' then
			  case
			    when position('Short' in c->>'trim') > 0 then 'Short'
			    when position('Standard' in c->>'trim') > 0 then 'Standard'
			    when position('Long' in c->>'trim') > 0 then 'Long'
			  end
			else 'n/a'
	  end as box_size,
	  'n/a'::text
	from nc.open_orders a
	join cvd.nc_vin_descriptions b on a.vin = b.vin
	join jsonb_array_elements(b.response->'result'->'vehicles') c on true
	left join jsonb_array_elements(b.response->'result'->'techSpecs') d on d->>'id' = '10120' -- engine displacement
	left join jsonb_array_elements(b.response->'result'->'techSpecs') e on e->>'id' = '18090'  -- box length
	where a.model_code = c->>'mfrModelCode'
	  and (b.response->'result'->>'year')::integer > 2022) aa 
left join nc.vehicle_configurations bb on aa.chr_style_id = bb.chrome_style_id
  and aa.chr_trim = bb.trim_level
  and aa.engine = bb.engine
where not exists (
  select 1
  from nc.vehicle_configurations 
  where chrome_style_id = aa.chr_style_id);

-- 12/14 had to add chrome_style_id to the on conflict statement
insert into nc.vehicle_configurations (model_year, make, model, model_code,
  trim_level,cab,drive,engine,chrome_style_id,alloc_group, box_size, name_wo_trim)
select model_year, make, model, model_code,
  trim_level,cab,drive,engine,chrome_style_id,alloc_group, box_size, name_wo_trim
from nc.stg_vehicle_configurations 
on conflict (model_year,make,model,model_code,trim_level,cab,engine,chrome_style_id)  
do update set (drive, chrome_style_id,alloc_group,box_size,name_wo_trim)
= (excluded.drive, excluded.chrome_style_id,excluded.alloc_group,excluded.box_size,excluded.name_wo_trim);

select * from nc.vehicle_configurations limit 10
-- ---------------------------------------------------------------------------------------------------------
-- --/> generate configuration data for open orders where it does not exist yet
-- ---------------------------------------------------------------------------------------------------------	

---------------------------------------------------------------------------------------------------------
--< orders with multiple styles not resolved by model_code
---------------------------------------------------------------------------------------------------------	
do
$$
begin
assert (
  select count(*)
  from (
    select a.order_number, a.vin
    from (
      select aa.*
      from nc.open_orders aa
      join cvd.nc_vin_descriptions bb on aa.vin = bb.vin
      where aa.vin is not null
        and bb.style_count > 1) a
    left join cvd.nc_vin_descriptions b on a.vin = b.vin
    join jsonb_array_elements(b.response->'result'->'vehicles') c on true   
    where a.model_code = (c->>'mfrModelCode')::citext
    group by order_number, a.vin
    having count(*) > 1) c) = 0, 'uhoh, multiple styles not resolved by model_code';
end
$$; 

---------------------------------------------------------------------------------------------------------
--/> orders with multiple styles not resolved by model_code
---------------------------------------------------------------------------------------------------------	
elimitated all the nc.xfm_open_orders, CVD handles all of it, and if it doesnt have a vin, delete it

select *
-- delete
from nc.open_orders
where order_type = 'TRE'
  and vin is null


--------------------------------------------------------------------------------------------------
--/> ORDERS
--------------------------------------------------------------------------------------------------


/******************************************************************************************/
1/25/19
at this point run daily_inventory (new_inventory_v1.sql)
will definitely need to revisit after figuring out how to configure
no-vin open orders

/******************************************************************************************/
--------------------------------------------------------------------------------------------
--< year_make_models & config types/headers & stuff
--------------------------------------------------------------------------------------------

insert into nc.year_make_models
select model_year, make, model
from nc.vehicles a
where model_year > 2022
  and not exists (
    select 1
    from nc.year_make_models
    where model_year = a.model_year
      and make = a.make
      and model = a.model) 
group by model_year, make, model;


do $$
begin
  drop table if exists all_types;
  create temp table all_types as
    select distinct config_type from ( -- 15 distinct configs_types
      select a.*, 
        case when trim_level = 1 and cab = 1 and box = 1 and drive = 1 and engine = 1 then 'm' else '' end
        ||        
        case when trim_level > 1 then 't' else '' end
        ||
        case when cab > 1 then 'c' else '' end
        ||
        case when box > 1 then 'b' else '' end
        ||
        case when drive > 1 then 'd' else '' end
        ||
        case when engine > 1 then 'e' else '' end 
        ||
        case when name_wo_trim > 1 then 'n' else '' end as config_type
      from (
        select model_year, make, model, count(distinct trim_level) as trim_level, count(distinct cab) as cab, 
          count(distinct box_size) as box,
          count(distinct drive) as drive, count(distinct engine) as engine, count(distinct name_wo_trim) as name_wo_trim
        from nc.vehicle_configurations
        where model_year > 2021
        group by model_year, make, model) a) b order by config_type;
assert (
  select count(*) 
  -- select *
  from all_types a
  where not exists (
    select 1
    from nc.configuration_types
    where config_type = a.config_type)) = 0, 'new config_type';
end $$;    

truncate nc.model_levels cascade;
insert into nc.model_levels
select model_year, make, model, config_type, seq, config_header
from (
  select b.*,
    case -- one case option for each config_type
      when config_type = 'd' then (
        select array_agg(drive order by drive) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)  
      when config_type = 'm' then (
        select array_agg(model order by model) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)  
      when config_type = 'mn' then (
        select array_agg(model || ' ' || name_wo_trim order by model, name_wo_trim) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)          
      when config_type = 't' then (
        select array_agg(trim_level order by trim_level) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)
      when config_type = 'tb' then (
        select array_agg(trim_level || ' ' || box_size order by trim_level, box_size) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)   
      when config_type = 'tbe' then (
        select array_agg(trim_level || ' ' || box_size || ' ' || engine order by trim_level, box_size, engine) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)    
      when config_type = 'tc' then (
        select array_agg(trim_level || ' ' || cab order by trim_level, cab) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)                  
      when config_type = 'tcb' then (
        select array_agg(trim_level || ' ' || cab || ' ' || box_size order by trim_level, cab, box_size) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)   
      when config_type = 'tcbd' then (
        select array_agg(trim_level || ' ' || cab || ' ' || box_size || ' ' || drive order by trim_level, cab, box_size, drive, engine) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)           
      when config_type = 'tcbde' then (
        select array_agg(trim_level || ' ' || cab || ' ' || box_size || ' ' || drive || ' ' || engine order by trim_level, cab, box_size, drive, engine) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)   
      when config_type = 'tcbe' then (
        select array_agg(trim_level || ' ' || cab || ' ' || box_size || ' ' || engine order by trim_level, cab, box_size, engine) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)   
      when config_type = 'tce' then (
        select array_agg(trim_level || ' ' || cab || ' ' || engine order by trim_level, cab, engine) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)    
      when config_type = 'td' then (
        select array_agg(trim_level || ' ' || drive order by trim_level, drive) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)   
      when config_type = 'tde' then (
        select array_agg(trim_level || ' ' || drive || ' ' || engine order by trim_level, drive, engine) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)  
      when config_type = 'te' then (
        select array_agg(trim_level || ' ' || engine order by trim_level, engine) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)    
      when config_type = 'ten' then (
        select array_agg(trim_level || ' ' || engine || ' ' || name_wo_trim order by trim_level, engine, name_wo_trim) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)    
      when config_type = 'tn' then (
        select array_agg(trim_level || ' ' || name_wo_trim order by trim_level, name_wo_trim) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)
      when config_type = 'e' then (
        select array_agg(engine order by engine) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model) 

      when config_type = 'b' then (
        select array_agg(box_size order by box_size) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)   

      when config_type = 'tcd' then (
        select array_agg(trim_level || ' ' || cab || ' ' || box_size order by cab, box_size) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model)          

      when config_type = 'cb' then (
        select array_agg(cab || ' ' || drive order by trim_level, cab, drive) 
        from nc.vehicle_configurations 
        where model_year = b.model_year and make = b.make and model = b.model) 
        
      end as config_headers
  from (
    select a.*, 
      case when trim_level = 1 and cab = 1 and box = 1 and drive = 1 and engine = 1 then 'm' else '' end
      ||
      case when trim_level > 1 then 't' else '' end
      ||
      case when cab > 1 then 'c' else '' end
      ||
      case when box > 1 then 'b' else '' end
      ||
      case when drive > 1 then 'd' else '' end
      ||
      case when engine > 1 then 'e' else '' end
      ||
      case when name_wo_trim > 1 then 'n' else '' end as config_type  
from (
  select model_year, make, model, count(distinct trim_level) as trim_level, count(distinct cab) as cab, 
    count(distinct box_size) as box,
    count(distinct drive) as drive, count(distinct engine) as engine,
    count(distinct name_wo_trim) as name_wo_trim
  from nc.vehicle_configurations
  where model_year > 2017
  group by model_year, make, model
  order by make, model) a) b) c
left join lateral unnest(config_headers) 
  with ordinality as f(config_header, seq) on true;     


--  8/28 add config_type b
-- 7/8/21 need to add the new config_type of tcbd
update nc.vehicle_configurations z
set config_type = x.config_type,
    level_header = x.level_header
from (    
  select a.*, b.configuration_id
  from nc.model_levels a
  left join nc.vehicle_configurations b on a.model_year = b.model_year
    and a.make = b.make
    and a.model = b.model
    and 
      case
        when  a.config_type = 'd' 
          then a.level_header = b.drive 
        when  a.config_type = 'm' 
          then a.level_header = b.model --then 1 = 1 -- model is already in the join     
        when  a.config_type = 'mn' 
          then a.level_header = b.model || ' ' || b.name_wo_trim
        when  a.config_type = 't' 
          then a.level_header = b.trim_level 
        when  a.config_type = 'tb' 
          then a.level_header = b.trim_level || ' ' || b.box_size
        when  a.config_type = 'tbe' 
          then a.level_header = b.trim_level || ' ' || b.box_size || ' ' || b.engine
        when  a.config_type = 'tcb' 
          then a.level_header = b.trim_level || ' ' || b.cab || ' ' || b.box_size
        when  a.config_type = 'tcbd' 
          then a.level_header = b.trim_level || ' ' || b.cab || ' ' || b.box_size || ' ' || b.drive          
        when  a.config_type = 'tcbde' 
          then a.level_header = b.trim_level || ' ' || b.cab || ' ' || b.box_size || ' ' || b.drive || ' ' || b.engine        
        when  a.config_type = 'tcbe' 
          then a.level_header = b.trim_level || ' ' || b.cab || ' ' || b.box_size || ' ' || b.engine
        when  a.config_type = 'tce' 
          then a.level_header = b.trim_level || ' ' || b.cab || ' ' || b.engine         
        when  a.config_type = 'td' 
          then a.level_header = b.trim_level || ' ' || b.drive 
        when  a.config_type = 'tde' 
          then a.level_header = b.trim_level || ' ' || b.drive || ' ' || b.engine
        when  a.config_type = 'te' 
          then a.level_header = b.trim_level || ' ' || b.engine
        when  a.config_type = 'ten' 
          then a.level_header = b.trim_level|| ' ' || b.engine || ' ' || b.name_wo_trim 
        when  a.config_type = 'tn' 
          then a.level_header = b.trim_level || ' ' || b.name_wo_trim 
        when  a.config_type = 'b' 
          then a.level_header = b.box_size                    
      end) x 
where z.configuration_id = x.configuration_id;    


   
do
$$
begin
assert (
  select count(*)
  -- select *
  from nc.vehicle_configurations
  where model_year > 2017
    and (config_type is null or level_header is null)) = 0, 'configuration missing type or header';
end
$$;  



truncate nc.model_color_stats;
insert into nc.model_color_stats
select a.model_year, a.make, a.model, a.seq, a.color,
  case 
    when count(vin) filter (where status = 'sold_90') > 0 then count(vin) filter (where status = 'sold_90')
    else null
  end as sold_90,
  case 
    when count(vin) filter (where status = 'sold_365') > 0 then count(vin) filter (where status = 'sold_365')
    else null
  end as sold_365,  
  case 
    when count(vin) filter (where status = 'inv_og') > 0 then count(vin) filter (where status = 'inv_og')
    else null
  end as inv_og,    
  case 
    when count(vin) filter (where status = 'inv_transit') > 0 then count(vin) filter (where status = 'inv_transit')
    else null
  end as inv_transit,    
  case 
    when count(vin) filter (where status = 'inv_system') > 0 then count(vin) filter (where status = 'inv_system')
    else null
  end as inv_system       
from nc.model_colors a
left join nc.daily_inventory b on a.model_year = b.model_year
  and a.make = b.make
  and a.model = b.model
  and a.color = b.color
  and b.the_date = current_date - 1
group by a.model_year, a.make, a.model, a.seq, a.color;   

-- 1/31 added configuration_id
truncate nc.model_level_stats;
insert into nc.model_level_stats
select a.model_year, a.make, a.model, a.seq, a.level_header, 
  case 
    when count(vin) filter (where status = 'sold_90') > 0 then count(vin) filter (where status = 'sold_90')
    else null
  end as sold_90,
  case 
    when count(vin) filter (where status = 'sold_365') > 0 then count(vin) filter (where status = 'sold_365')
    else null
  end as sold_365,  
  case 
    when count(vin) filter (where status = 'inv_og') > 0 then count(vin) filter (where status = 'inv_og')
    else null
  end as inv_og,    
  case 
    when count(vin) filter (where status = 'inv_transit') > 0 then count(vin) filter (where status = 'inv_transit')
    else null
  end as inv_transit,    
  case 
    when count(vin) filter (where status = 'inv_system') > 0 then count(vin) filter (where status = 'inv_system')
    else null
  end as inv_system, b.configuration_id  
from nc.model_levels a
join nc.vehicle_configurations b on a.model_year = b.model_year
  and a.make = b.make
  and a.model = b.model
  and a.level_header = b.level_header 
join nc.daily_inventory c on b.configuration_id = c.configuration_id
  and c.the_date = current_date - 1  -- where a.model = 'acadia'                                                                                                                                                    
group by a.seq, a.level_header, a.model_year, a.make, a.model, b.configuration_id --order by configuration_id
order by a.seq;   


-- 1/31 added configuration_id
truncate nc.model_level_color_stats; 
insert into nc.model_level_color_stats
select a.model_year, a.make, a.model, coalesce(a.seq, 1), a.level_header, coalesce(b.seq,1), b.color, 
  case 
    when count(vin) filter (where status = 'sold_90') > 0 then count(vin) filter (where status = 'sold_90')
    else null
  end as sold_90,
  case 
    when count(vin) filter (where status = 'sold_365') > 0 then count(vin) filter (where status = 'sold_365')
    else null
  end as sold_365,
  case 
    when count(vin) filter (where status = 'inv_og') > 0 then count(vin) filter (where status = 'inv_og')
    else null
  end as inv_og,
  case 
    when count(vin) filter (where status = 'inv_transit') > 0 then count(vin) filter (where status = 'inv_transit')
    else null
  end as inv_transit,
  case 
    when count(vin) filter (where status = 'inv_system') > 0 then count(vin) filter (where status = 'inv_system')
    else null
  end as inv_system, c.configuration_id       
from nc.model_levels a
join nc.model_colors b on a.model_year = b.model_year
  and a.make = b.make
  and a.model = b.model
join nc.vehicle_configurations c on a.model_year = c.model_year
  and a.make = c.make
  and a.model = c.model
  and a.level_header = c.level_header 
left join nc.daily_inventory d on c.configuration_id = d.configuration_id  
  and b.color = d.color
  and d.the_date = current_date - 1
group by a.model_year, a.make, a.model, a.seq, a.level_header, b.seq, b.color, c.configuration_id  
order by a.seq, b.seq;

truncate nc.model_stat_totals;
insert into nc.model_stat_totals  
select make, model, 
  sum(coalesce(sold_90, 0)) as sold_90, sum(coalesce(sold_365, 0)) as sold_365, 
  sum(coalesce(inv_og, 0)) as inv_og, sum(coalesce(inv_transit, 0)) as inv_transit, 
  sum(coalesce(inv_system, 0)) as inv_system
from nc.model_color_stats  
group by make, model;
COMMENT ON TABLE nc.model_stat_totals IS 'aggregated status counts at the  
  make/model level combining model years, truncated nightly and populated from nc.model_color_stats';

truncate nc.model_level_stat_totals;
insert into nc.model_level_stat_totals 
select make, model, level_header,
  sum(coalesce(sold_90, 0)) as sold_90, sum(coalesce(sold_365, 0)) as sold_365,
  sum(coalesce(inv_og, 0)) as inv_og, sum(coalesce(inv_transit, 0)) as inv_transit, 
  sum(coalesce(inv_system, 0)) as inv_system
from nc.model_level_stats
group by make, model, level_header;
COMMENT ON TABLE nc.model_level_stat_totals IS 'aggregated status counts at the make/model/level_header level combining model years, 
  truncated nightly and populated from nc.model_level_stats';

truncate nc.model_level_color_stat_totals;
insert into nc.model_level_color_stat_totals 
select a.make, a.model, a.level_header, initcap(a.color) as color,
  sum(coalesce(a.sold_90, 0)) as sold_90, sum(coalesce(a.sold_365, 0)) as sold_365,
  sum(coalesce(a.inv_og, 0)) as inv_og, sum(coalesce(a.inv_transit, 0)) as inv_transit, 
  sum(coalesce(a.inv_system, 0)) as inv_system
from nc.model_level_color_stats a
join nc.vehicle_configurations b on a.configuration_id = b.configuration_id
group by a.make, a.model, a.level_header, a.color;

-- delete from nc.model_level_color_stat_totals_archive where the_date = current_date;
insert into nc.model_level_color_stat_totals_archive
select current_date, a.*
from nc.model_level_color_stat_totals a
where sold_90 + sold_365 + inv_og + inv_transit + inv_system <> 0;


--------------------------------------------------------------------------------------------
--/> misc

--------------------------------------------------------------------------------------------
