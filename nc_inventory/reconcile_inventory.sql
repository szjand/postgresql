﻿drop table if exists acq;
create temp table acq as
select *  -- 486
from nc.vehicle_acquisitions
where thru_Date > current_date ;

select category, count(*)  -- in system: 227, in transit: 144
from nc.open_orders
group by category

select count(distinct inpmast_vin) -- 591
from arkona.xfm_inpmast
where current_row
  and status = 'I'
  and type_n_u = 'N'

drop table if exists acct;
create temp table acct as
select d.control, e.inpmast_vin as vin, acct_date -- 601
from ( -- accounting
  select a.control, max(b.the_date) as acct_date 
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_date between current_Date - 900 and current_date--'06/22/2018'
  inner join fin.dim_account c on a.account_key = c.account_key 
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where a.post_status = 'Y'
    and c.account in ( -- all new vehicle inventory account
      select b.account
      from fin.dim_account b 
      where b.account_type = 'asset'
        and b.department_code = 'nc'
        and b.typical_balance = 'debit'
        and b.current_row
        and b.account <> '126104')   
  --   and b.the_date between current_Date - 600 and current_date -- this has no effect
  group by a.control
  having sum(a.amount) > 10000) d
left join arkona.xfm_inpmast e on d.control = e.inpmast_stock_number
  and e.current_row  

select a.*, b.ro_date, c.creation_date as key_date
from acct a
left join (
  select d.vin, min(b.the_date) as ro_date
  from ads.ext_fact_repair_order a
  inner join dds.dim_date b on a.opendatekey = b.date_key
  inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
  inner join acct e on d.vin = e.vin -- only acct vins
  group by d.vin) b on a.vin = b.vin
left join ads.keyper_creation_dates c on a.control = c.stock_number    

drop table if exists all_three; 
-- base is accounting   
create temp table all_three as
select aa.*, bb.stock_number, bb.ground_Date, bb.source, cc.order_number, cc.order_type, cc.alloc_group, cc.peg, cc.category
from (
  select a.*, b.ro_date, c.creation_date as key_date
  from acct a
  left join (
    select d.vin, min(b.the_date) as ro_date
    from ads.ext_fact_repair_order a
    inner join dds.dim_date b on a.opendatekey = b.date_key
    inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
    inner join acct e on d.vin = e.vin -- only acct vins
    group by d.vin) b on a.vin = b.vin
  left join ads.keyper_creation_dates c on a.control = c.stock_number) aa
left join acq bb on aa.control = bb.stock_number
left join nc.open_orders cc on aa.vin = cc.vin;


-- holy shit, an awful lot of inventory not in acquisitions
-- has an ro, no row in acq
select * 
from all_three
where ro_date is not null 
  and stock_number is null
union
select * 
from all_three
where ro_date is null
  and key_date is not null
  and stock_number is null 
order by stock_number   

-- look at it with stg_availability
-- nope, not a single one
select * 
from all_three a
join nc.stg_availability b on a.vin = b.vin
where a.ro_date is not null 
  and a.stock_number is null


-- chr/jon describe_vehicle updated

-- ok, need to get these into vehicles and acquisitions
select *
from all_three
where ro_date is not null 
  and stock_number is null
union
select * 
from all_three
where ro_date is null
  and key_date is not null
  and stock_number is null

-- 2 are rentals, take care of those first
select *
from (
  select *
  from all_three
  where ro_date is not null 
    and stock_number is null
  union
  select * 
  from all_three
  where ro_date is null
    and key_date is not null
    and stock_number is null) a
where control like '%R'  

insert into nc.vehicle_acquisitions values
('1GTG6DENXJ1203613','32969R','08/22/2018','12/31/9999','ctp',false),
('W04GP6SXXJ1074880','G33117R','06/22/2018','12/31/9999','ctp',false);


-- stg_availability remaining 118 vehicles
truncate nc.stg_availability;
insert into nc.stg_availability (the_date,journal_date,stock_number,
  vin,model_year,make, model,
  model_code,color_code,color, source)
select current_date, f.journal_date, f.control, f.vin, g.year, g.make, g.model, g.model_code, 
  g.color_code, g.color, f.source
from (
  select a.control, aa.vin, 
    case d.journal_code
      when 'PVI' then 'factory'
      when 'CDH' then 'dealer trade'
      when 'GJE' then 'ctp'
      else 'XXX'
    end as source, max(b.the_date) as journal_date
  from fin.fact_gl a
  join (
    select *
    from all_three
    where ro_date is not null 
      and stock_number is null
    union
    select * 
    from all_three
    where ro_date is null
      and key_date is not null
      and stock_number is null) aa on a.control = aa.control
  inner join dds.dim_date b on a.date_key = b.date_key
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account in ( -- all new vehicle inventory account
      select b.account
      from fin.dim_account b 
      where b.account_type = 'asset'
        and b.department_code = 'nc'
        and b.typical_balance = 'debit'
        and b.current_row
        and b.account <> '126104')   
  inner join fin.dim_journal d on a.journal_key = d.journal_key
    and d.journal_code in ('PVI','CDH','GJE')
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where a.post_status = 'Y'
  group by a.control, d.journal_code, aa.vin
  having sum(a.amount) > 10000) f 
join arkona.xfm_inpmast g on f.vin = g.inpmast_vin
  and g.current_row;

-- and finish processing using .../all_nc_inventory_nightly.sql

-- this returns 8 rows all honda/nissan
select *
from all_three
where stock_number is null
  and order_number is null

what this does not include are the orders without vins
for now, i am going to move on and come back to reconciling orders(without vins) and configurations later

-- 182 orders without vins
select *
from nc.open_orders
where vin is null

and of course, do not yet know anything about honda/nissan orders












  