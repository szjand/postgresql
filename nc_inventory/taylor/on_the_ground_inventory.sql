﻿/*
8/30
taylor needs
stk, vin, year, make, model, trim color, msrp, best price, margin for current on the ground inventory
*/
drop table taylor;
create temp table taylor as
select a.stock_number, a.vin, b.model_year, b.make, b.model, b.trim_level, b.color,
  c.msrp::integer as msrp, coalesce(c.sellingprice, bb.num_field_value)::integer as best_price, f.margin::integer as margin 
from nc.vehicle_acquisitions a
join nc.vehicles b on a.vin = b.vin
left join ifd.homenet_new c on a.vin = c.vin
left join board.pending_boards d on a.stock_number = d.stock_number
left join board.sales_board e on a.stock_number = e.stock_number
  and e.board_type_key in ('5','6','12','13','17')
left join arkona.xfm_INPMAST aa on a.vin = aa.inpmast_vin and aa.current_row
left join arkona.ext_inpoptf bb on aa.inpmast_vin = bb.vin_number
join arkona.ext_inpoptd cc on bb.company_number = cc.company_number
  and bb.seq_number = cc.seq_number
  and cc.field_descrip = 'Internet Price'
left join gmgl.vin_margins f on a.stock_number = f.stock_number
  and f.thru_Ts > now()
  and f.stock_number <> 'N/A'
where a.thru_date > current_date
  and d.stock_number is null
  and e.stock_number is null
order by a.stock_number

select * from taylor order by stock_number


where stock_number in (
select stock_number from taylor group by stock_number having count(*) > 1)

select * from gmgl.daily_inventory limit 10

select * from board.pending_boards limit 10

select * from taylor where coalesce(msrp, 0) < 10000

update taylor
set msrp = 27595
where stock_number = 'G37001R'

select * from board.sales_board limit 10

select * from gmgl.vin_margins where vin = '1GNSKJKC8KR355455'

select * from nc.vehicle_acquisitions where stock_number = 'g37917'