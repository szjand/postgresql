﻿----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- keyper data: creation dates
----------------------------------------------------------------------------------------------------
-- keyper looks promising, need to scrape it
-- 
-- oh shit in ads, dds.ext_keyper_assets already has stocknumber & sale_reated date and is updated nightly
-- 
-- drop table if exists ads.keyper_creation_dates;
-- create table ads.keyper_creation_dates (
--   stock_number citext primary key,
--   creation_date date not null);
-- 
-- select *
-- from ads.keyper_creation_dates  
-- order by creation_date
----------------------------------------------------------------------------------------------------
-- keyper data: creation dates
----------------------------------------------------------------------------------------------------
-- inventory account for chev light trucks, including silverado 1500 is 123700
-- nothing in accounting that tells me this is a silverado (as opposed to a suburban)
-- but, the vin is in the gl_description for the purchase transacdtion
-- all inventory transactions on acct 123700 (light duty trucks) since 2013
-- 6/19/18 need to convert to nightly, the "inventory" app is getting a lot of attention
-- so, need to do an initial load, then nightly updating
-- start off, change the year to > 2015, eventually, ground date greater than 2016, i think
-- don't even know yet what tables i will generate or need
-- the target is silverados, stk#, vin, sku level detail with ground date, delivery date, age, acq_type & sale_type (ctp/dealer trade)
-- *a* make the base load thru may 2018, leave some headroom for testing nightly updating
-- thinking for the nightly, limit the scrape by date, eg, if previous scrape was thru 5/13/18, then next scrape starts on 06/01/18
-- sls.deals should actually have everything except dealer trades and acquisitions, does sales have drac/ctp?
drop table if exists inv; 
create temp table inv as
select a.control, --d.journal_code, e.description, 
  sum(a.amount) filter (where d.journal_code = 'PVI') as inv_amount,
  max(case when d.journal_code = 'PVI' then b.the_date end) as inv_date,
  max(case when d.journal_code = 'PVI' then e.description end) as inv_desc,
  sum(a.amount) filter (where d.journal_code = 'VSN' or (control = 'g33744' and d.journal_code = 'VSU')) as sale_amount,
  max(case when d.journal_code = 'VSN' or (control = 'g33744' and d.journal_code = 'VSU') then b.the_date end) as sale_date,
  max(case when d.journal_code = 'VSN' or (control = 'g33744' and d.journal_code = 'VSU') then e.description end) as sale_desc,
  sum(a.amount) filter (where d.journal_code = 'CDH') as cdh_amount,
  max(case when d.journal_code = 'CDH' then b.the_date end) as cdh_date,
  max(case when d.journal_code = 'CDH' then e.description end) as cdh_desc,
  sum(a.amount) filter (where d.journal_code = 'GJE') as gje_amount,
  max(case when d.journal_code = 'GJE' then b.the_date end) as gje_date,
  max(case when d.journal_code = 'GJE' then e.description end) as gje_desc
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  -- *a*
  and b.the_year > 2015
--   and b.the_date between '01/01/2016' and '05/31/2018'  
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account = '123700'
inner join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code not in ('SVI','PCA','POT','SCA','AFM','SWA','WCM')
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and control not like 'R%'
group by a.control--, d.journal_code, e.description
order by control;
create unique index on inv(control);

-- select * from inv where control in ('g33179','g33744')

-- limit inv to silverados, get color, vin, body style from inpmast, keyper creation date
-- 6/19/18 added current_row to xfm_inpmast, was getting dups in test_1
drop table if exists test_1;
create temp table test_1 as
  -- join inv to inpmast on stock number
  select a.*, b.creation_date as keyper_date, c.body_style, c.color, c.inpmast_vin as vin
  from inv a
  inner join (
    select inpmast_stock_number, inpmast_vin, make, model, body_style, color
    from arkona.xfm_inpmast
    where model = 'SILVERADO 1500'
      and current_row = true
    group by inpmast_stock_number, inpmast_vin, make, model, body_style, color) c on a.control = c.inpmast_stock_number 
  left join ads.keyper_creation_dates b on a.control = b.stock_number
  union
  -- join inv to inpmast on vin
  select a.*, b.creation_date as keyper_date, c.body_style, c.color, c.inpmast_vin as vin
  from inv a
  inner join (
    select inpmast_stock_number, inpmast_vin, make, model, body_style, color
    from arkona.xfm_inpmast
    where model = 'SILVERADO 1500'
      and current_row = true
    group by inpmast_stock_number, inpmast_vin, make, model, body_style, color) c on a.inv_desc = c.inpmast_vin
  left join ads.keyper_creation_dates b on a.control = b.stock_number;
create unique index on test_1(control);
-- 
-- -- 6/20 does not appear to be any dup anmoalies
-- select control
-- from test_1
-- group by control
-- having count(*) > 1

-- -- get rid of dup control rows
-- delete from test_1
-- where (
--   (control = '22445' and color = 'WHITE DIAMOND TRICOAT') or
--   (control = '22475' and color = 'TUNGSTEN') or
--   (control = '23496' and color = 'SILVER') or
--   (control in('19466', '24307') and color = 'GRAY') or
--   (control = '24746' and color = 'WHITE') or
--   (control = '24746R' and color = 'WHITE') or
--   (control = '24882' and color = 'BLACK') or
--   (control = '25264' and color = 'RUBY') or
--   (control = '30156' and color = 'BLUE') or
--   (control = '30606' and color = 'RED') or
--   (control = '32185' and color = 'HAVANA MIST') or
--   (control = 'G33188' and color = 'VLACK'));
-- create unique index on test_1(control);

-- looks good, sold vehicles w/blank delivery_date = dealer trade
-- get delivery date from bopmast
drop table if exists test_2;
create table test_2 as
select a.*, b.delivery_Date
from test_1 a
left join sls.ext_bopmast_partial b on a.control = b.stock_number
  and b.capped_date > '01/01/2010';
create unique index on test_2(control);



-- get ro dates for tsp, pdi, dt
drop table if exists test_3;
create table test_3 as
select a.*, tsp, pdi, dt,
  coalesce(keyper_date, coalesce(tsp, coalesce(pdi, coalesce(dt, coalesce(inv_date, coalesce(cdh_date,'12/31/9999'::date)))))) as ground,
  null::citext as acq_type, null::citext as sale_type
from test_2 a
left join (
  select d.vin, 
    max(case when c.opcode = 'tsp' then b.the_date end) as tsp,
    max(case when c.opcode = 'pdi' then b.the_date end) as pdi,
    max(case when c.opcode = 'dt' then b.the_date end) as dt
  from ads.ext_fact_repair_order a
  inner join dds.dim_date b on a.opendatekey = b.date_key
--     and b.the_year > 2014
  inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
    and c.opcode in ('tsp', 'dt', 'pdi')
  inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
  group by d.vin) b on a.vin = b.vin
-- misc anomalies stock numbers changed, these are the old invalid ones
where a.control not in ('32458','28921','27683')  ;
create unique index on test_3(control);  

-- -- this returns the above 3, but also G34206/G33744 (see inv query) which looks like a cancelled dealer trade and is currently in inventory
-- -- talk to jeri about this one
-- select *
-- from test_3 a
-- inner join (
--   select vin
--   from test_3
--   group by vin 
--   having count(*) > 1) b on a.vin = b.vin
-- where not exists (-- exclude rentals
--   select 1
--   from test_3
--   where vin = a.vin
--     and control like '%R')  
-- order by a.vin 
-------------------------------------------------------------------------------------------------------------------------------------------------
-- 6/20 these are all still in test_3
-- select * from test_3 where control in ('26208R','26841R','30526R','30607R','30917R','31048R','31146R','28372R','28494R','31745r')
-- fixes anomalies from posting to incorrect journal
-- these are all acq from drac/ctp
update test_3 y
set gje_date = x.the_date,
    gje_amount = x.amount
from (    
  select b.the_date, a.control, a.amount
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account = '123700'
  inner join fin.dim_journal d on a.journal_key = d.journal_key
  where (
    (d.journal_code = 'sca' and a.control in ('26208R','26841R','30526R','30607R','30917R','31048R','31146R'))
    or
    (d.journal_code = 'pot' and a.control in ('28372R','28494R'))
    or
    (d.journal_code = 'wcm' and a.control = '31745r'))) x
where y.control = x.control  ;

-- -- 6/20 these are no longer in test_3
-- -- oldies with no sale_date and no Delivery_date, whack em
-- delete 
-- -- select *
-- from test_3 
-- where delivery_date is null 
--   and sale_date is null 
--   and extract(year from inv_date) < 2016;
  
-- for drac/ctp acquisitions, set ground date to the date the unit was charged to nc
-- select * from test_3 where inv_amount is null and gje_amount is not null
update test_3 y
set ground = 
  case 
    when x.gje_date < coalesce(x.delivery_date, current_date) then x.gje_date
    else x.delivery_date
  end
from (
  select *
  from test_3
  where inv_amount is null
    and gje_amount is not null) x
where y.control = x.control;    


-- for drac/ctp acquisitions, set acq_type to from drac/ctp
update test_3 
set acq_type = 'from drac/ctp'
where inv_amount is null
  and gje_amount is not null
  and control <> '29821'; -- one of the stk# changed vehicles, not a drac/ctp acq

-- sales to drac/ctp
update test_3 
set sale_type = 'to drac/ctp'
where sale_desc like '%RYDELL%';

-- sale_type dealer trade
update test_3 
set sale_type = 'dealer trade'
where control in (
  select a.control
  from test_3 a
  where delivery_date is null 
    and sale_amount is not null 
    and control not in ('28445','28426','31048R')); -- not dealer trade sales, fix delivery_manually
    
update test_3
set delivery_date = sale_date
where control in ('28445','28426');   
    
-- ground date for a few of the sales of dealer trades where current ground > sale_date
update test_3
set ground = inv_date
where delivery_Date is null 
  and sale_amount is not null 
  and ground > sale_date;

-- acq_type: dealer trade
-- update ground to cdh_date
update test_3
set acq_type = 'dealer trade'
where control in (
  select a.control
  from test_3 a
  where inv_date is null 
    and cdh_date is not null);

update test_3
set ground = cdh_date
where control in (
  select a.control
  from test_3 a
  where inv_date is null 
    and cdh_date is not null);

-- ground date > delivery date, these need to be cleaned up (55)
select *
from test_3
where ground > delivery_Date;
-- no ground date, all (1) oldies, whack em
delete
from test_3
where ground > current_date;
-- replace delivery date with sale date when sale date >= ground (10)
-- select * from test_3 where ground > delivery_date and sale_date >= ground
update test_3
set delivery_date = sale_date
where ground > delivery_date
  and sale_date >= ground;
-- replace ground with inv_date where it works
-- select * from test_3 where ground > delivery_date and inv_date <= delivery_Date
update test_3
set ground = inv_date
where ground > delivery_date
  and inv_date <= delivery_date;
-- after the above, only 3 left, 2 oldies, whack em, fix 1
update test_3
set ground = sale_date
where control = 'g34248';
delete
from test_3
where ground > delivery_Date;

-- dealer trades have a null delivery date, but sale date will work
-- select * from test_3 where sale_type = 'dealer trade'

update test_3
set delivery_date = sale_date
where sale_type = 'dealer trade';


------------------------------------------------------
-- color clean up
------------------------------------------------------
-- select color, count(*)
-- from test_3
-- group by color
-- order by color

update test_3  -- 3
set color = 'UNKNOWN'
where color is null;

update test_3
set color = 'DEEP OCEAN BLUE METALLIC'
where color like '%OCEAN%';

update test_3
set color = 'CAJUN RED TINTCOAT'
where color = 'CAJUN RED';

update test_3
set color = 'SIREN RED TINTCOAT'
where color = 'SIREN RED';

update test_3
set color = 'VICTORY RED'
where color = 'VICOTRY RED';

update test_3
set color = 'RED HOT'
where color = 'RED';

update test_3
set color = 'IRIDESCENT PEARL'
where color = 'IRIDESCENT PEAL';

update test_3
set color = 'IRIDESCENT PEARL TRICOAT'
where color = 'IRIDESCENT PEARL COAT';

update test_3
set color = 'BLACK'
where color in ('BLCK', 'JET BLACK');

update test_3
set color = 'BROWNSTONE METALLIC'
where color = 'BROWNSTONE MET';

update test_3
set color = 'GRAPHITE METALLIC'
where color = 'GRAHITE METALLIC';

update test_3
set color = 'PEPPERDUST METALLIC'
where color = 'PEPERUST METALLIC';

update test_3
set color = 'RAINFOREST GREEN METALLIC'
where color in ('RAINFOREST GREEEN METALLI','RAINFOREST GREEN','RAINFORET GREEN METALLIC');

update test_3
set color = 'SILVER ICE'
where color in( 'SILVERICE','SIVER ICE');

update test_3
set color = 'SILVER ICE METALLIC'
where color in ( 'SILVER ICE METALIC','SIVLER ICE METALLIC');

update test_3
set color = 'SLATE GREY METALLIC'
where color = 'SLATE GRAY METALLIC';

update test_3
set color = 'SPECIAL PAINT GREEN'
where color = 'SPECIAL PAINT/GREEN';

update test_3
set color = 'SUMMIT WHITE'
where color in ('SUMMITE WHITE','SUMMIT WHTIE');

update test_3
set color = 'TUNGSTEN METALLIC'
where color in ('TUNSTEN METALLIC','TUNGESTEN METALLIC','TUGNSTEN METALLIC');

update test_3
set color = 'Unripened Green Metallic'
where color like 'unrip%';

update test_3
set color = 'WHITE DIAMOND'
where color = 'WHITE DIAMON';

update test_3
set color = 'WHITE'
where color = 'WHIE';

update test_3
set color = 'Blue Granite Metallic'
where color = 'BLUE GRANITE';

update test_3
set color = 'Blue Topaz Metallic'
where color = 'BLUE TOPAZ';

update test_3
set color = 'Blue Ray Metallic'
where color = 'BLUE RAY';

update test_3
set color = 'Brownstone Metallic'
where color = 'BROWNSTONE';

update test_3
set color = 'Deep Ruby Metallic'
where color = 'DEEP RUBY';

update test_3
set color = 'Graystone Metallic'
where color = 'GRAYSTONE';

update test_3
set color = 'Tungsten Metallic'
where color = 'GXG';

update test_3
set color = 'Iridescent Pearl Tricoat'
where color = 'IRIDESCENT PEARL';

update test_3
set color = 'Mocha Steel Metallic'
where color like 'MOCHA%';

update test_3
set color = 'Silver Ice Metallic'
where color = 'SILVER';

update test_3
set color = 'Silver Ice Metallic'
where color = 'SILVER ICE';

update test_3
set color = 'Slate Gray Metallic'
where color = 'SLATE GREY METALLIC';

update test_3
set color = 'Tungsten Metallic'
where color = 'TUNGSTEN';

update test_3
set color = 'White Diamond Tricoat'
where color = 'WHITE DIAMOND';

-- fix the missing colors
update test_3
set color = 'black' 
where vin = '1GCVKREC9HZ286624';

update test_3
set color = 'summit white' 
where vin in ('3GCUKREC7JG333375','3GCUKREC8JG287796');

-- -- compare to dataone colors
-- select a.control, a.vin, a.color, a.ground, b.year, c.*
-- from test_3 a
-- left join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
-- left join (
-- select a.year, c.mfr_color_code, c.mfr_color_name
-- from dao.veh_trim_styles a
--   left join dao.lkp_veh_ext_color b on a.vehicle_id = b.vehicle_id
--   left join dao.def_ext_color c on b.ext_color_id = c.ext_color_id
--   where model = 'silverado 1500'
--     and year > 2012
--     and c.mfr_color_code <> ''
--   group by a.year, c.mfr_color_code, c.mfr_color_name) c on a.color = c.mfr_color_name and b.year = c.year
-- where c.year is null

  
/*
-------------------------------------------------------------------------------
--< chrome web service json
-------------------------------------------------------------------------------

-- 5/20 physical table to store vins to send to chrome webservice
drop table if exists jon.test_3_vins;
create table jon.test_3_vins (
  vin citext primary key);

insert into jon.test_3_vins
select distinct vin from test_3;

select *
from jon.test_3_vins a
where not exists (
  select 1
  from chr.describe_vehicle where vin = a.vin)

select * 
-- select count(*)
from chr.describe_vehicle


select jsonb_pretty(response)
-- select *
from chr.describe_vehicle
where vin = '3GCUKREC3JG415734'


select vin, response->>'engine'
from chr.describe_vehicle
where vin = '1GCNCNEH1GZ423894'

select vin, response->'style' -- returns a list - i am mind fucking on list vs array
from chr.describe_vehicle
where vin = '1GCNCNEH1GZ423894'

--ok, here are the items in the style list
select vin, jsonb_array_elements(response->'style')
from chr.describe_vehicle
where vin = '1GCNCNEH1GZ423894'

write a function to extracts the values needed from respons, engine, stylename, year, make, model

select vin, jsonb_typeof(response->'style')  -- array
from chr.describe_vehicle
where vin = '1GCNCNEH1GZ423894'


select vin, jsonb_typeof(response->'responseStatus')  -- object
from chr.describe_vehicle
where vin = '1GCNCNEH1GZ423894'

http://www.craigkerstiens.com/2017/03/12/getting-started-with-jsonb-in-postgres/


{"country": "US", "language": "en", "modelYear": "2016", "bestMakeName": "Chevrolet", "bestTrimName": "Work Truck", "bestModelName": "Silverado 1500", "bestStyleName": "2WD Reg Cab 133.0\" Work Truck"}


select vin, jsonb_typeof(response->'attributes')  -- object
from chr.describe_vehicle
where vin = '3GCUKSEC3GG263177'

-- returns one row for each top level key, response is a json object consisting of 10 objects, hence 10 rows
-- The json_each() function allows us to expand the outermost JSON object into a set of key-value pairs. 
select vin, jsonb_each(response)
from chr.describe_vehicle
where vin = '3GCUKSEC3GG263177'

-- returns just the outermost keys
select vin, jsonb_object_keys(response)
from chr.describe_vehicle
where vin = '3GCUKSEC3GG263177'


want: 
  style::attributes::altStyleName
  engine::1st element::displacement::value::1st element::$value

-- this works
select response->'vinDescription'->'attributes'->>'vin'
from chr.describe_vehicle
where vin = '3GCUKSEC3GG263177'

select jsonb_typeof(response->'style') -- array
from chr.describe_vehicle
where vin = '3GCUKSEC3GG263177'

-- hip hip fucking hooray, this works
select jsonb_array_elements(response->'style')->'attributes'->>'name'
from chr.describe_vehicle
where vin = '3GCUKSEC3GG263177'

select jsonb_typeof(response->'engine') -- array
from chr.describe_vehicle
where vin = '3GCUKSEC3GG263177'

select jsonb_typeof(jsonb_array_elements(response->'engine')->'displacement') -- object
from chr.describe_vehicle
where vin = '3GCUKSEC3GG263177'

select jsonb_typeof(jsonb_array_elements(response->'engine')->'displacement'->'value') -- array
from chr.describe_vehicle
where vin = '3GCUKSEC3GG263177'

select jsonb_array_length(jsonb_array_elements(response->'engine')->'displacement'->'value') -- array
from chr.describe_vehicle
where vin = '3GCUKSEC3GG263177'

-- this gives me 2 rows, one for each element in the array 'value'
select jsonb_array_elements(jsonb_array_elements(response->'engine')->'displacement'->'value')
from chr.describe_vehicle
where vin = '3GCUKSEC3GG263177'

-- this gives me 2 rows, one for each element in the array 'value'
select jsonb_array_elements_text(jsonb_array_elements(response->'engine')->'displacement'->'value')
from chr.describe_vehicle a
where a.vin = '3GCUKSEC3GG263177'

-- this returns 2 rows, unit (liters/cubicIn) as text
select jsonb_array_elements(jsonb_array_elements(response->'engine')->'displacement'->'value')->'attributes'->>'unit'
from chr.describe_vehicle
where vin = '3GCUKSEC3GG263177'

-- this returns an error: ERROR:  argument of AND must not return a set
select jsonb_array_elements(jsonb_array_elements(response->'engine')->'displacement'->'value')->'attributes'->>'unit'
from chr.describe_vehicle
where vin = '3GCUKSEC3GG263177'
  and jsonb_array_elements_text(jsonb_array_elements(response->'engine')->'displacement'->'value') like '%liters%'




-- craig ringer https://stackoverflow.com/questions/21675174/querying-data-within-json-array-data-field/21677486#21677486

CREATE temp TABLE company AS SELECT 1 AS id, '[{"name": "foo", "account_id": "123"}, {"name": "bar", "account_id": "321"}]'::jsonb AS accounts;
insert into company SELECT 2 AS id, '[{"name": "foo", "account_id": "223"}, {"name": "bar", "account_id": "221"}]'::jsonb AS accounts;
insert into company SELECT 3 AS id, '[{"name": "foo", "account_id": "323"}, {"name": "bar", "account_id": "321"}]'::jsonb AS accounts;

select jsonb_array_elements(accounts)
from company

SELECT id 
-- select *
FROM company c,
LATERAL jsonb_array_elements(c.accounts) acc 
WHERE acc ->> 'account_id' = '321';

-- i have not been able to get the lateral approach to work with my stuff, 
-- 1. wtf is the usage? Lateral, i am assuming a join but on what, ok, the comma, old school, not a join, listing tables
--      but it still seems weird, does "LATERAL" define jsonb_array_elements(c.accounts) acc  as a values list?

select jsonb_array_elements(jsonb_array_elements(response->'engine')->'displacement'->'value')->'attributes',
  jsonb_array_elements(response->'engine')->'displacement'
from chr.describe_vehicle
where vin = '3GCUKSEC3GG263177'


-- ERROR:  set-valued function called in context that cannot accept a set
select *
from chr.describe_vehicle a,
lateral jsonb_array_elements(jsonb_array_elements(a.response->'engine')->'displacement'->'value') val
where vin = '3GCUKSEC3GG263177'
  and val->'attributes'->>'unit' = 'liters'       

-- ERROR:  syntax error at or near "->"
-- LINE 6: ...ts(a.response->'engine')->'displacement'->'value')->'attribu...
select *
from (
  select *
  from chr.describe_vehicle  
  where vin = '3GCUKSEC3GG263177') a,
lateral jsonb_array_elements(jsonb_array_elements(a.response->'engine')->'displacement'->'value')->'attributes' val
where val->>'unit' = 'liters' 


select jsonb_array_elements(jsonb_array_elements(response->'engine')->'displacement'->'value')->'attributes'
  jsonb_array_elements(response->'engine')->'displacement'
from chr.describe_vehicle
where vin = '3GCUKSEC3GG263177'

select jsonb_array_elements(jsonb_array_elements(response->'engine')->'displacement'->'value')->>'$value'
from chr.describe_vehicle
where vin = '3GCUKSEC3GG263177'   

-- fucking finally
with 
  engine as (
    select jsonb_array_elements(jsonb_array_elements(response->'engine')->'displacement'->'value') as displacement
    from chr.describe_vehicle
    where vin = '3GCUKSEC3GG263177')
select displacement->>'$value'
from engine
where displacement->'attributes'->>'unit' = 'liters'




select make, model, style_name, count(*)
from nc.silverado_sierra_chrome_attributes a
inner join nc.new_vehicle_sales b on a.vin = b.vin
group by make, model,style_name
order by make, model,style_name


-- style name, includes drive train, cab, wheel base, trim
select jsonb_array_elements(response->'style')->'attributes'->>'name'
from chr.describe_vehicle
where vin = '3GCUKSEC3GG263177'


select jsonb_array_elements(jsonb_array_elements(response->'style')->'bodyType')-->>'$value'
from chr.describe_vehicle
where vin = '3GCUKREC8FG233325'

-- bed type
with 
  bed as (
    select jsonb_array_elements(jsonb_array_elements(response->'style')->'bodyType') as body_types
    from chr.describe_vehicle
    where vin = '3GCUKREC8FG233325')
select body_types->>'$value'
from bed
where body_types->>'$value' like '%Bed%'


    
-- engine
with 
  engine as (
    select jsonb_array_elements(jsonb_array_elements(response->'engine')->'displacement'->'value') as displacement
    from chr.describe_vehicle
    where vin = '3GCUKSEC3GG263177')
select displacement->>'$value'
from engine
where displacement->'attributes'->>'unit' = 'liters'

select response->'attributes'->>'bestMakeName'
from chr.describe_vehicle
where vin = '3GCUKSEC3GG263177'

select *
from chr.describe_vehicle a
where (
  select response->'attributes'->>'bestMakeName'
  from chr.describe_vehicle
  where vin = a.vin) = 'Chevrolet'
limit 100  
select * from test_3

-------------------------------------------------------------------------------
--/> chrome web service json
-------------------------------------------------------------------------------

*/
-- 6/21 add style_id
-- leave out bed, it was generating a cartesian
-- change field names to match afton's
drop table if exists test_4 cascade;
create table test_4 as
with 
  engine as ( -- necessary to do this in CTE to be able to filter on one of the array values (liters)
    select vin, jsonb_array_elements(jsonb_array_elements(response->'engine')->'displacement'->'value') as displacement
    from chr.describe_vehicle
    where vin in (select vin from test_3))
--   bed as (
--     select distinct vin, jsonb_array_elements(jsonb_array_elements(response->'style')->'bodyType') as body_types
--     from chr.describe_vehicle
--     where vin in (select vin from test_3))   
  select row_number() over (order by null) as row_number,
    e.control as stock_number, e.vin, e.style_id::integer, e.model_year, e.make, e.model, e.model_code, /*e.style, e.bed, */
--     substring(e.wheel_base from position('1' in wheel_base) for 5) as wheel_base,
    case
      when e.style like '2WD%' then '2WD'
      when e.style like '4WD%' then '4WD'
    end as drive,
    case 
      when e.style like '%Reg%' then 'reg'
      when e.style like '%Double%' then 'double'
      when e.style like '%Crew%' then 'crew'
      when e.style like '%Ext%' then 'extended'
    end as cab,
    case 
      when e.style like '%1LZ%' then '1LZ'
      when e.style like '%2LZ%' then '2LZ'
      when e.style like '%Work%' then 'WT'
      when e.style like '%High%' then 'high country'
      when e.style like '%1LT%' then '1LT'
      when e.style like '%2LT%' then '2LT'
      when e.style like '%LS%' then 'LS'
      when e.style like '%LT%' then 'LT'
      when e.style like '%Custom%' then 'custom'
    end as trim,
    e.engine, e.color, e.ground, e.delivery_date, e.age, e.acq_type, e.sale_type  
  from (
    select a.control, a.vin, a.color, a.delivery_date, a.ground, 
      jsonb_array_elements(c.response->'style')->'attributes'->>'id' as style_id,
      c.response->'attributes'->>'modelYear' as model_year,
      c.response->'attributes'->>'bestMakeName' as make,
      c.response->'attributes'->>'bestModelName' as model,
      coalesce(a.delivery_date, current_date) - a.ground as age, a.acq_type, a.sale_type, 
      b.displacement->>'$value' as engine,
      jsonb_array_elements(c.response->'style')->'attributes'->>'name' as style,
      jsonb_array_elements(c.response->'style')->'attributes'->>'mfrModelCode' as model_code,
--       d.body_types->>'$value' as bed,
      jsonb_array_elements(c.response->'style')->'attributes'->>'nameWoTrim' as wheel_base
    from test_3 a
    left join engine b on a.vin = b.vin and b.displacement ->'attributes'->>'unit' = 'liters'
--     left join bed d on a.vin = d.vin and d.body_types->>'$value' like '%Bed%'
    left join chr.describe_vehicle c on a.vin = c.vin) e;

-- unique anomaly
update test_4
set ground = '04/27/2018', 
    age = current_date  - '04/27/2018'::date,
    acq_type = null::citext
where stock_number = 'g34206';
delete 
from test_4
where stock_number = 'g34206';    
create unique index on test_4(vin, style_id) where stock_number not like '%R'; -- exclude rentals  

-- before applying a unique index on stock_number, need to fix multiple styles/vin

-- multiple styles:
-- 6/22, there are 8 with  multiple styles
select a.*
from test_4 a
inner join (
  select vin
  from (
    select vin, jsonb_array_elements(response->'style')
    from chr.describe_vehicle) x
  group by vin
  having count(*) > 1) b on a.vin = b.vin
-- -- 
delete 
from test_4
where row_number in (
  select row_number
--   -- these are the records that don't match inpmast and should be deleted
  -- select a.*, c.inpmast_stock_number, c.model_code, c.body_style, c.chrome_style_id
  from test_4 a
  inner join (
    select vin
    from (
      select vin, jsonb_array_elements(response->'style')
      from chr.describe_vehicle) x
    group by vin
    having count(*) > 1) b on a.vin = b.vin
  inner join arkona.xfm_inpmast c on a.vin = c.inpmast_vin
    and a.style_id <> c.chrome_Style_id
    and c.current_row = true );

create unique index on test_4(stock_number);

-- fix misc anomalies
-- G33268 goofy, sold to drac/ctp on 3/26
-- but there is no deal, cust in UI shows ERROR
update test_4
set ground = '02/14/2018',
    delivery_date = '03/26/2018',
    age = '03/26/2018'::date - '02/14/2018'::date,
    sale_type = 'to drac/ctp'
where stock_number = 'G33268';

-- -- 32185: sold 2/24 unwound 3/4, sold 5/28 unwound 6/11/18
-- -- don't fix this one here, do it in nightly
-- update test_4
-- set delivery_date = null::date, 
--     sale_type = null::citext
-- where stock_number = '32185';

-- bad ground date (goofy keyper date)
update test_4
set ground = '11/03/2017',
    age = '03/08/2018'::date - '11/03/2017'::date
where stock_number = '32262'
   
g34145 sold 6/20 capped 6/21
g34101 sold 6/20 capped 6/21
-- only 4 anomalies
-- and on 6/22 no anomalies
-- inventory: test_4 to inpmast where status = I
-- 6/23, 3 anomalies, vehicles sold on 6/22, no delivery date yet (test_4), but status in i npmast has been changed
select *
from (
  select *
  from test_4
  where delivery_date is null) a
full outer join (
  select inpmast_stock_number, inpmast_vin, year, model_code, body_style, color, chrome_style_id
  from arkona.xfm_inpmast
  where status = 'I'
    and current_row = true
    and model = 'silverado 1500'
    and type_n_u = 'n') b on a.vin = b.inpmast_vin and a.stock_number = b.inpmast_stock_number
where a.vin is null or b.inpmast_vin is null

select * from sls.deals where vin = '1GCVKREC9JZ137555'

select * from test_4


drop table if exists jon.test_4;
create table jon.test_4 (
  row_number integer not null,
  stock_number citext primary key,
  vin citext not null,
  chrome_style_id integer,
  model_year citext not null,
  make citext not null,
  model citext not null,
  model_code citext not null,
  drive citext not null,
  cab citext not null,
  trim_level citext not null,
  engine citext not null,
  color citext not null,
  ground date,
  delivery_date date,
  age integer,
  acq_type citext,
  sale_type citext);
create index on jon.test_4(ground);
create index on jon.test_4(delivery_date);
create index on jon.test_4(trim_level);
create index on jon.test_4(cab);
create index on jon.test_4(color);
create index on jon.test_4(engine);


insert into jon.test_4
select row_number,stock_number,vin, style_id,model_year,make, model,model_code,
  drive,cab,trim,engine,color,ground,delivery_Date,age,acq_type,sale_type
from test_4;



drop table if exists jon.test_base;
create table jon.test_base (
  row_number integer not null,
  stock_number citext primary key,
  vin citext not null,
  chrome_style_id integer,
  model_year citext not null,
  make citext not null,
  model citext not null,
  model_code citext not null,
  drive citext not null,
  cab citext not null,
  trim_level citext not null,
  engine citext not null,
  color citext not null,
  ground date,
  delivery_date date,
  age integer,
  acq_type citext,
  sale_type citext);

insert into jon.test_base
select row_number,stock_number,vin, style_id,model_year,make, model,model_code,
  drive,cab,trim,engine,color,ground,delivery_Date,age,acq_type,sale_type
from test_4; 
---------------------------- data complete -------------------------------------------------------------------------


-------------------------------------------------
-- 5/24
-- need the graph, start with a sku, white crew 1lt
-- limit to model year 2018 (?)

-- 2018s top skus
select a.drive,a.cab,a.trim_level,a.engine, b.friendly_color, count(*)
from test_4 a
left join gmgl.friendly_colors b on a.color = b.color
where a.model_year = '2018'
group by a.drive,a.cab,a.trim_level,a.engine, b.friendly_color
order by count(*) desc 



select * -- first one on ground 7/25/17
from jon.test_4 a
left join gmgl.friendly_colors b on a.color = b.color
where cab = 'crew'
  and trim_level = '1lt'
  and b.friendly_color = 'white'
  and model_year = '2018'  
order by ground  


-- anomalies are all old or "unknown"
-- eg no worries
drop table if exists test_5;
create temp table test_5 as
select a.*, b.friendly_color
from jon.test_4 a
left join gmgl.friendly_colors b on a.color = b.color;
-- where b.friendly_color is null
create unique index on test_5(stock_number);


select e.the_date, e.inventory, f.sales
from (
  select a.the_date, count(b.stock_number) as inventory
  from dds.dim_date a
  left join ( -- inventory
    select * 
    from test_
    where trim_level = '1lt' and friendly_color = 'white' and cab = 'crew' and engine = '5.3') b on a.the_date between b.ground and coalesce(b.delivery_date, current_date) - 1
  where a.the_date between current_date - interval '180 days'  and current_Date -1  
group by a.the_date) e
left join ( -- sales
  select a.the_date, count(b.stock_number) as sales
  from dds.dim_date a
  left join (
    select * 
    from test_5
  where trim_level = '1lt' and friendly_color = 'white' and cab = 'crew' and engine = '5.3') b on a.the_date = b.delivery_date
where a.the_date between current_date - interval '180 days'  and current_Date -1  
group by a.the_date) f on e.the_date = f.the_date

-- this generates the sku graph data: inventory/sales by day
do
$$
declare
  _trim_level citext := '1lt';
  _color citext := 'silver ice metallic';
  _cab citext := 'crew';
  _engine citext := '5.3';
begin
  drop table if exists wtf;
  create temp table wtf as
  select e.the_date, e.inventory, f.sales
  from (
    select a.the_date, count(b.stock_number) as inventory
    from dds.dim_date a
    left join ( -- inventory
      select * 
      from test_5
      where trim_level = _trim_level 
        and color = _color 
        and cab = _cab 
        and engine = _engine) b on a.the_date between b.ground and coalesce(b.delivery_date, current_date) - 1
    where a.the_date between current_date - interval '180 days'  and current_Date -1  
  group by a.the_date) e
  left join ( -- sales
    select a.the_date, count(b.stock_number) as sales
    from dds.dim_date a
    left join (
      select * 
      from test_5
    where trim_level = _trim_level and color = _color and cab = _cab and engine = _engine) b on a.the_date = b.delivery_date
  where a.the_date between current_date - interval '180 days'  and current_Date -1  
  group by a.the_date) f on e.the_date = f.the_date;  
end
$$;
select * from wtf;


-- how to set one or more parameters to wildcard
do
$$
declare
  _trim_level citext := '2lT';
  _color citext := '%%';
  _cab citext := 'crew';
  _engine citext := '5.3';
begin
  drop table if exists wtf;
  create temp table wtf as
  select e.the_date, e.inventory, f.sales
  from (
    select a.the_date, count(b.stock_number) as inventory
    from dds.dim_date a
    left join ( -- inventory
      select * 
      from test_5
      where trim_level = _trim_level and friendly_color like _color and cab = _cab and engine = _engine) b on a.the_date between b.ground and coalesce(b.delivery_date, current_date) - 1
    where a.the_date between current_date - interval '180 days'  and current_Date -1  
  group by a.the_date) e
  left join ( -- sales
    select a.the_date, count(b.stock_number) as sales
    from dds.dim_date a
    left join (
      select * 
      from test_5
    where trim_level = _trim_level and friendly_color like _color and cab = _cab and engine = _engine) b on a.the_date = b.delivery_date
  where a.the_date between current_date - interval '180 days'  and current_Date -1  
  group by a.the_date) f on e.the_date = f.the_date;  
end
$$;
select * from wtf


do
$$
declare
  _trim_level citext := '2lT';
  _color citext := '%%';
  _cab citext := 'double';
  _engine citext := '5.3';
begin
  drop table if exists wtf;
  create temp table wtf as
  select *
  from test_4
  where trim_level like _trim_level
    and cab like _cab
    and engine like _engine
    and color like _color;
end
$$;
select * from wtf

select * from test_5 where model_year = '2018'

-- ok, that works, but in JS world, what i need is json based on gmgl.sku, fucking json includes every fucking thing

select *
from gmgl.sku a
inner join test_5 b on a.cab = b.cab 
  and a.vehicle_trim = b.trim_level
  and replace(a.engine, 'L', '') = b.engine
  and a.color = b.color
  and b.model_year = '2018'


select distinct model_year from test_5


-- test_5 vehicles not in gmgl_sku, down to 3 vehicles, 2 skus
select b.cab, b.trim_level, b.color, b.engine, count(*)
from test_5 b
left join gmgl.sku a on
  case
    when a.cab = 'regular' then b.cab = 'reg'
    else a.cab = b.cab
  end
  and 
    case
      when a.vehicle_trim = '1WT' then b.trim_level = 'WT'
      when a.vehicle_trim = 'GAJ' then b.trim_level = 'high country'
      when a.vehicle_trim = '1LS' then b.trim_level = 'LS'
      when a.vehicle_Trim = 'ST9' then b.trim_level = 'custom'
      else a.vehicle_trim = b.trim_level
    end
  and a.engine = b.engine
  and a.color = b.color
where b.model_year = '2018'
  and a.sku_id is null
group by b.cab, b.trim_level, b.color, b.engine
order by count(*) desc   


-- test 5 * sku groups
select *
from gmgl.sku a
left join test_5 b on
  case
    when a.cab = 'regular' then b.cab = 'reg'
    else a.cab = b.cab
  end
  and 
    case
      when a.vehicle_trim = '1WT' then b.trim_level = 'WT'
      when a.vehicle_trim = 'GAJ' then b.trim_level = 'high country'
      when a.vehicle_trim = '1LS' then b.trim_level = 'LS'
      else a.vehicle_trim = b.trim_level
    end
  and a.engine = b.engine
  and a.color = b.color
where b.model_year = '2018'
order by group_id

-- sku_groups with multiple skus
select group_id, count(*)
from gmgl.sku
group by group_id
order by count(*) desc


select stock_number, vin, 
-- select string_agg(vin, ',')
from test_5
where color = 'silver ice metallic'
  and trim_level = '1lt'
  and model_year = '2018'
  and cab = 'crew'
  and coalesce(delivery_date, current_date) > current_date - 180
order by ground  

1GCUKREC3JF134504,1GCUKREC8JF244397,3GCUKREC0JG296248,3GCUKREC1JG287476,3GCUKREC2JG400027,3GCUKREC3JG218787,3GCUKREC4JG202243,3GCUKRECXJG317705


-- most active skus
select cab, trim_level, color, engine, count(*)
from test_5
where model_year = '2018'
group by cab, trim_level, color, engine
order by count(*) desc

-- vehicles (in sku) for which we have vis data
select *
from test_5 a
where cab = 'crew'
  and trim_level = '2lz'
  and color = 'black'
  and engine = '5.3'
  and model_year = '2018'
  and exists (
    select 1
    from gmgl.sold_vehicle_options
    where vin = a.vin)
order by ground    

select *
from (
select *-- compare 2 of a sku, ground same(ish), one sold in 105 the other in 3
from (
  select a.stock_number as stk1, a.vin, a.ground, a.delivery_date, a.age, c.*
  from test_5 a
  left join gmgl.sold_vehicle_options b on a.vin = b.vin
  left join gmgl.vehicle_option_codes c on b.option_code = c.option_code
  where a.vin in ('3GCUKSECXJG255181')--,'3GCUKSEC8JG286784')
  order by c.option_code ) aa
full outer join (
  select a.stock_number as stk2, a.vin, a.ground, a.delivery_date, a.age, c.*
  from test_5 a
  left join gmgl.sold_vehicle_options b on a.vin = b.vin
  left join gmgl.vehicle_option_codes c on b.option_code = c.option_code
  where a.vin in ('3GCUKSEC8JG286784')
  order by c.option_code) bb on aa.option_code = bb.option_code) x
where stk1 is null or stk2 is null


-- group by total options
select count(distinct a.vin)
from gmgl.sold_Vehicle_options a
inner join test_5 b on a.vin = b.vin
  and b.model_year = '2018'
  
-- unfortunately this generates 240 unique vis_configs, yuk
select vis_config, count(*)
from (
  select a.vin, string_agg(a.option_code ||': ' ||c.description, ',') as vis_config
  from gmgl.sold_Vehicle_options a
  inner join test_5 b on a.vin = b.vin
    and b.model_year = '2018'
  inner join gmgl.vehicle_option_codes c on a.option_code = c.option_code
  group by a.vin) x
group by vis_config

-- narrow it down to a high running sku
drop table if exists black_crew_2lz;
create temp table black_crew_2lz as
select stock_number, vin, ground, delivery_Date, age
from test_5 b
where b.model_year = '2018'
  and b.cab = 'crew'
  and b.trim_level = '2LZ'
  and b.color = 'black'
  and b.engine= '5.3';

-- 4 of the 22 don't exist in sold_vehicle_options
select * 
from black_crew_2lz a
left join gmgl.sold_vehicle_options b on a.vin = b.vin
where b.vin is null

-- here are all the option codes for these 18: 181 option codes, 95 exist on all 18 vehicles
select b.option_code, count(*)
from black_crew_2lz a
inner join gmgl.sold_vehicle_options b on a.vin = b.vin
group by b.option_code
order by count(*) desc 

-- with descriptions
drop table if exists wtf_1;
create temp table wtf_1 as
select c.the_count, c.all_stock, d.option_code, d.description
from (
  select b.option_code, count(*) as the_count, string_agg(a.stock_number, ',' order by stock_number) all_stock
  from black_crew_2lz a
  inner join gmgl.sold_vehicle_options b on a.vin = b.vin
  group by b.option_code) c
left join gmgl.vehicle_option_codes d on c.option_code = d.option_code  
where the_count <> 18
order by the_count desc 


select *
from wtf_1 a
left join black_crew_2lz b on b.stock_number ~~ a.all_stock


-- need to eliminate fleet vehicles
-- my assumption is page 5's lines 22 & 43


-- fleet sales with vin
select g.*, h.cab, h.trim_level, h.engine, h.color, i.sale_type, i.primary_sc, j.fullname, k.inpmast_vin
from (
  select b.the_date, c.account, a.control
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month > 201701
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account in (  
    select d.gl_account
    from fin.fact_fs a 
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month > 201701
      and b.page in (5,8,9,10)
      and b.line in (22,43)
      and b.col = 1 -- sales
    inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
      and c.store = 'ry1'
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key)) g
inner join jon.test_4 h on g.control = h.stock_number
  and h.model_year = '2018'
left join sls.ext_bopmast_partial i on g.control = i.stock_number
left join ads.ext_dim_customer j on i.buyer_bopname_id = j.bnkey
left join arkona.xfm_inpmast k on g.control = k.inpmast_stock_number
  and k.current_row = true
order by control

-- concerned that updating the sale_type field may not be the best approach
-- what if the sale_type is already another value
-- in theis subset, there are none like that, but ...
select a.*
from jon.test_4 a
inner join (
  select g.*, h.cab, h.trim_level, h.engine, h.color, i.sale_type, i.primary_sc, j.fullname, k.inpmast_vin
  from (
    select b.the_date, c.account, a.control
    from fin.fact_gl a
    inner join dds.dim_date b on a.date_key = b.date_key
      and b.year_month > 201701
    inner join fin.dim_account c on a.account_key = c.account_key
      and c.account in (  
      select d.gl_account
      from fin.fact_fs a 
      inner join fin.dim_fs b on a.fs_key = b.fs_key
        and b.year_month > 201701
        and b.page in (5,8,9,10)
        and b.line in (22,43)
        and b.col = 1 -- sales
      inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
        and c.store = 'ry1'
      inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key)) g
  inner join jon.test_4 h on g.control = h.stock_number
    and h.model_year = '2018'
  left join sls.ext_bopmast_partial i on g.control = i.stock_number
  left join ads.ext_dim_customer j on i.buyer_bopname_id = j.bnkey
  left join arkona.xfm_inpmast k on g.control = k.inpmast_stock_number
    and k.current_row = true) b on a.stock_number = b.control
  
-- remove fleet sales from nc.silverado_sales
delete from nc.silverado_sales
where vin in (
  select inpmast_vin
  from arkona.xfm_inpmast
  where inpmast_stock_number in (
    select g.control
    from (
      select b.the_date, c.account, a.control
      from fin.fact_gl a
      inner join dds.dim_date b on a.date_key = b.date_key
        and b.year_month > 201701
      inner join fin.dim_account c on a.account_key = c.account_key
        and c.account in (  
        select d.gl_account
        from fin.fact_fs a 
        inner join fin.dim_fs b on a.fs_key = b.fs_key
          and b.year_month > 201701
          and b.page in (5,8,9,10)
          and b.line in (22,43)
          and b.col = 1 -- sales
        inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
          and c.store = 'ry1'
        inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key)) g
    inner join jon.test_4 h on g.control = h.stock_number
      and h.model_year = '2018'
    left join sls.ext_bopmast_partial i on g.control = i.stock_number
    left join ads.ext_dim_customer j on i.buyer_bopname_id = j.bnkey))


select *
from jon.test_4 a
left join nc.silverado_sales b on a.vin = b.vin
where coalesce(a.delivery_date, current_date) > current_date - interval '18 months' 
  and b.vin is null 
  and a.sale_Type is null


-----------------------------------------------------------
-- kelly's anomalies
-----------------------------------------------------------
select *
from jon.test_4
where stock_number in ('g34262','g34125','g34104','g34107','g34145','g34160')

select *
from jon.test_4 a
left join gmgl.sku b on a.cab = b.cab
  and a.trim_level = b.vehicle_trim
  and a.color = b.color
where a.stock_number in ('g34262','g34125','g34104','g34107','g34145','g34160')

select * 
from wtf
where sku_id = 124




drop table if exists sku_days;
create temp table sku_days as
        select a.the_date, b.sku_id
        from dds.dim_date a
        left join gmgl.sku b on 1 = 1
        where a.the_date between current_Date - 180 and current_date - 1
          and b.sku_id in (160,166,139,125,124,139);

drop table if exists wtf;
create temp table wtf as
--     with 
--       sku_days as ( -- one row for each sku_ic in gmgl.sku for each day for the previous 180 days
--         select a.the_date, b.sku_id
--         from dds.dim_date a
--         left join gmgl.sku b on 1 = 1
--         where a.the_date between current_Date - 180 and current_date - 1 --)
--           and b.sku_id in (160,166,139,125,124,139)) -----------------------------------------------------------------------------------
    select c.the_date, c.sku_id, c.inventory, g.sold
    from (    
      select a.the_date, a.sku_id, count(b.sku_id) as inventory
      from sku_days a
      left join ( -- inventory
          select bb.sku_id, ground, delivery_date
          from jon.test_4 aa
          inner join gmgl.sku bb on aa.engine = bb.engine
            and aa.color = bb.color
            and
              case
                when bb.cab = 'regular' then aa.cab = 'reg'
                else aa.cab = bb.cab
              end
            and 
              case
                when bb.vehicle_trim = '1WT' then  aa.trim_level = 'WT'
                when bb.vehicle_trim = 'GAJ' then aa.trim_level = 'high country'
                when bb.vehicle_Trim = '1LS' then aa.trim_level = 'LS'
                else bb.vehicle_trim = aa.trim_level
              end
          where model_year = '2018') b on a.sku_id = b.sku_id and a.the_date between b.ground and coalesce(b.delivery_date, current_date) - 1
      group by a.the_date, a.sku_id) c
    left join (-- g
      select d.the_date, d.sku_id, count(e.sku_id) as sold
      from sku_days d
      left join ( -- e: sales
        select bb.sku_id, ground, delivery_date
        from jon.test_4 aa
        inner join gmgl.sku bb on aa.engine = bb.engine
          and aa.color = bb.color
          and
            case
                when bb.cab = 'regular' then aa.cab = 'reg'
                else aa.cab = bb.cab
            end
          and 
            case
              when bb.vehicle_trim = '1WT' then  aa.trim_level = 'WT'
              when bb.vehicle_trim = 'GAJ' then aa.trim_level = 'high country'
              when bb.vehicle_Trim = '1LS' then aa.trim_level = 'LS'
              else bb.vehicle_trim = aa.trim_level
            end
        where model_year = '2018') e on d.sku_id = e.sku_id and d.the_date = e.delivery_date
      group by d.the_date, d.sku_id) g on c.the_date = g.the_date 
        and c.sku_id = g.sku_id  
order by sku_id, the_date       


----------------------------------------------------------------------------------------------------------------------
what needs to be scraped

select a.stock_number, a.vin, a.ground, a.delivery_date, a.acq_type, a.sale_type,
  b.bopmast_id, 
  c.*
from jon.test_4 a
left join sls.ext_bopmast_partial b on a.stock_number = b.stock_number
left join sls.deals c on b.bopmast_id = c.bopmast_id
where a.acq_type like 'from%'
  or a.sale_type like 'to%'

select * from sls.ext_Bopmast_partial where vin = '3GCUKSER2JG303860'


select *
from inv
where control in ('G34125','g33268')

select year_month, sale_type, count(*)
from jon.test_4 a
inner join dds.dim_date b on a.delivery_date = b.the_date
group by year_month, sale_type
order by year_month desc 

-- matches silverado sales, good enough,
-- but i am going to trash silverado sales
select *
from (
  select b.the_date, a.stock_number, a.sale_type, a.vin
  from jon.test_4 a
  inner join dds.dim_date b on a.delivery_date = b.the_date
    and b.year_month = 201806
  order by a.vin) a
full outer join (
  select b.the_date, a.vin
  from nc.silverado_sales a
  inner join dds.dim_date b on a.delivery_date = b.the_date
    and b.year_month = 201806
  order by vin) b on a.vin = b.vin


select *
from jon.test_4
where vin in ('1GCVKREC4JZ176036','1GCVKREC4JZ329076')


select b.*, c.*
from jon.test_4 a 
inner join sls.deals b on a.stock_number = b.stock_number
left join arkona.xfm_bopname c on b.buyer_bopname_id = c.bopname_record_key
  and c.current_row = true
where a.sale_type like 'to%'
order by b.vin

select b.*
from jon.test_4 a 
left join  sls.deals_by_month b on a.stock_number = b.stock_number
where a.sale_type like 'to%'
order by b.vin, bopmast_id
------------------------------------------------------------------------------------
-- silverados that have unwound
------------------------------------------------------------------------------------
select *
from sls.deals_by_month a
where vehicle_type = 'new'
  and model = 'silverado 1500'
  and exists (
    select 1
    from sls.deals_by_month
    where stock_number = a.stock_number
    and unit_count = -1)
order by vin, bopmast_id, seq


select *
from arkona.ext_inpcmnt
where vin = '1GCVKREC9JZ137555'
order by transaction_date, transaction_time


select *
from arkona.ext_inpcmnt a
where transaction_date between 20180100 and 20190000
  and exists (
    select 1
    from arkona.ext_inpcmnt
    where vin = a.vin
      and comment like '%unwound%')
order by vin, transaction_date, transaction_time      


select *
from sls.deals_by_month a
inner join arkona.ext_inpcmnt b on a.vin = b.vin
  and b.comment like '%unwound%'
where vehicle_type = 'new'
  and model = 'silverado 1500'
order by a.vin, a.bopmast_id, a.seq


-------------------------------------------------------------------
--
-------------------------------------------------------------------
/*
G33700, G33779, G33951 (and many others) show as sale_type = dealer trade
but sale_amount/date,desc in inv are null ???
*/
select a.stock_number, a.acq_type, a.sale_type, a.ground, a.delivery_date, b.*
from jon.test_base a
inner join inv b on a.stock_number = b.control
where a.sale_type = 'dealer trade'
order by sale_Date