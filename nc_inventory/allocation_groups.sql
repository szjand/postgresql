﻿/*
/*
5/20/19
Manually "scraped" global connect order page to get all the allocation group / model code combinations (2019)
into a spreadsheet (.../docs/alloc_group_model_codes.xlsx)
*/

drop table if exists nc.allocation_groups;
create table if not exists nc.allocation_groups (
  model_year integer not null,
  make citext not null,
  model citext not null,
  model_code citext not null,
  alloc_group citext not null,
  description citext not null);

populate it with query generated on spreadsheet
and some inevitable cleanup

update nc.allocation_Groups
set model = trim(model),
    make = trim(make),
    model_code = trim(model_code),
    alloc_group = trim(alloc_group),
    description = trim(description)

select model_year, model_code, alloc_group
from nc.allocation_groups
group by model_year, model_code, alloc_group
having count (*) > 1

delete from nc.allocation_groups where model_code = 'ck15753' and description = 'SWB, 4WD Standard Box Double Cab'

update nc.allocation_Groups set model = 'Silverado 1500 LD' where model_Code = 'CK15753' 

insert into nc.allocation_Groups values (2019,'BMC','Savana','TG33406','SAVVAN','3500 Passenger Van');
    
select * from nc.allocation_Groups where model_code = 'TG33406'

select * from nc.allocation_Groups where model = 'enclave'

update nc.allocation_Groups set model_code = '1GZ69' where model_code = '1GZ'  

select * from nc.allocation_groups where model_code = '6NW26'

delete from nc.allocation_groups where alloc_group = 'enclav'

update nc.allocation_Groups set alloc_Group = 'XT5' where model = 'XT5' 

insert into nc.allocation_groups values (2020,'Cadillac','XT6','6NW26','XT6','Premium Luxury FWD-AWD');


-- check for and insert 2020 alloc_groups
-- insert into nc.allocation_groups
-- buick, cadillac, chev cars done
select 2020, make, model, model_code, alloc_group, description
from nc.allocation_groups
where make = 'Chevrolet'
  and alloc_group in ('trax')



-- there is no single canonical source for allocation groups, either a list or definition
-- so, nc.vehicle_configurations, gmgl.vehicle_orders, nc.build_out_2019
-- all alloc groups from vehicle_configurations

select alloc_group -- 60
from nc.vehicle_configurations
where alloc_group <> 'unknown'
group by alloc_group

-- all alloc_group from gmgl.vehicle_orders
select alloc_group  -- 60
from gmgl.vehicle_orders
group by alloc_group;

select distinct model_year, alloc_group, model_code  -- 60
from gmgl.vehicle_orders
where model_code is not null
  and model_year > 2017
group by model_year, alloc_group, model_code
order by alloc_group

-- all alloc_group from nc.build_out_2019
select alloc_group -- 58
from nc.build_out_2019
group by alloc_group

-- 5/19, not sure this is useful
-- wtf, i can't see what i am unioning here
-- order, max vin vs min vin, generating result sets based on different vins
select distinct b.alloc_group, b.model_code, 
    (r.style ->'attributes'->>'modelYear')::citext as model_year,
    (r.style ->'division'->>'$value')::citext as make,
    (r.style ->'model'->>'$value'::citext)::citext as model,
--     (r.style ->'attributes'->>'mfrModelCode')::citext as model_code,
    case
      when u.cab->>'$value' like 'Crew%' then 'crew' 
      when u.cab->>'$value' like 'Extended%' then 'double'  
      when u.cab->>'$value' like 'Regular%' then 'reg'  
      else 'N/A'   
    end as cab     
from (
  select a.alloc_group, a.model_code, min(a.vin) as vin
  from gmgl.vehicle_orders a
  where model_year > 2017
  group by alloc_group, model_code) b
left join chr.describe_vehicle c on b.vin = c.vin  
left join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
where b.model_code = (r.style ->'attributes'->>'mfrModelCode')::citext
union
select distinct b.alloc_group, b.model_code, 
    (r.style ->'attributes'->>'modelYear')::citext as model_year,
    (r.style ->'division'->>'$value')::citext as make,
    (r.style ->'model'->>'$value'::citext)::citext as model,
--     (r.style ->'attributes'->>'mfrModelCode')::citext as model_code,
    case
      when u.cab->>'$value' like 'Crew%' then 'crew' 
      when u.cab->>'$value' like 'Extended%' then 'double'  
      when u.cab->>'$value' like 'Regular%' then 'reg'  
      else 'N/A'   
    end as cab     
from (
  select a.alloc_group, a.model_code, max(a.vin) as vin
  from gmgl.vehicle_orders a
  where model_year > 2017
  group by alloc_group, model_code) b
left join chr.describe_vehicle c on b.vin = c.vin  
left join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
where b.model_code = (r.style ->'attributes'->>'mfrModelCode')::citext
order by model, model_year

-- models that exist in either only 2018 or only 2019
select *
from (
  select model_year, model
  from nc.vehicle_configurations 
  where model_year = 2018
    and make in ('chevrolet','gmc','buick','cadillac')
  group by model_year, model) a
full outer join (
  select model_year, model
  from nc.vehicle_configurations 
  where model_year = 2019
    and make in ('chevrolet','gmc','buick','cadillac')
  group by model_year, model) b on a.model = b.model
full outer join (
  select model_year, model
  from nc.vehicle_configurations 
  where model_year = 2020
    and make in ('chevrolet','gmc','buick','cadillac')
  group by model_year, model) c on b.model = c.model  
where a.model is null 
  or b.model is null    
  or c.model is null


select distinct model_year, alloc_group, model
from nc.vehicle_configurations
where alloc_Group <> 'n/a'
  and model_year > 2017
order by alloc_group, model_year



select *
from  nc.vehicle_configurations 
where model = 'silverado 1500 ld'

-- got some cleanup to do on allocation groups
  select model_year, model_code from nc.allocation_groups group by model_year, model_code having count(*) > 1

  select * from nc.allocation_groups where model_year = 2018 and model_code = '1JU76'

  select model_year, model_code from (
  select distinct model_year, model_code, alloc_group from nc.allocation_groups) a
  group by model_year, model_code having count(*) > 1


select * 
from nc.allocation_groups
where model_code in ('1JW76','1JU76','1JV76','1JS76','1JT76','1JR76')
  and model_year = 2018

select * 
from nc.allocation_groups
where model_year = 2018
  and alloc_group = 'GHDREG'

select * 
from nc.allocation_groups
where alloc_group = 'GHDREG'

delete from nc.allocation_groups
where model_year = 2018
  and alloc_Group = 'GHDREG'


select *
from nc.allocation_groups
where make not in ('chevrolet','buick','gmc','cadillac')

update nc.allocation_groups set make = 'GMC' where make = 'BMC'

select distinct model_year, model_code, alloc_group
from nc.allocation_groups


select model_year, model_code, alloc_group
from nc.allocation_groups
group by model_year, model_code, alloc_group
having count(*) >  1

select * from nc.allocation_groups
where model_year = 2019
  and model_code = 'CK36043'
  and alloc_group = 'ccruhd'


-- 5/24 some more clean up, get rid of description
-- may reduce it even further to just model_year, model_code, alloc_group
create temp table alloc_groups as
select distinct model_year, make, model, model_code, alloc_group
from nc.allocation_groups    

select model_year, model_code, alloc_group
from alloc_groups
group by model_year, model_code, alloc_group
having count(*) > 1

drop table if exists nc.allocation_groups cascade;
create table if not exists nc.allocation_groups (
  model_year integer not null,
  make citext not null,
  model citext not null,
  model_code citext not null,
  alloc_group citext not null,
  primary key (model_year,model_code,alloc_group));

insert into nc.allocation_groups
select * from alloc_groups;

-- possibly the only way to maintain allocation cgroups is by 
-- monitoring orders
select * 
from nc.open_orders a
where not exists (
  select 1
  from nc.allocation_groups
  where model_year = a.model_year
    and model_code = a.model_code
    and alloc_group = a.alloc_group)

-- agreement between allocation_groups and vehicle_configurations
select *
from (
  select distinct model_code, alloc_group
  from nc.vehicle_configurations
  where alloc_group <> 'n/a'
    and model_year > 2017) a
full outer join (
  select distinct model_code, alloc_group
  from nc.allocation_groups) b on a.model_code = b.model_code and a.alloc_group = b.alloc_group

6/9/19 some more cleanup
select * from nc.allocation_groups where left(model, 1) = ' '
update nc.allocation_groups
set model = trim(model)

-- are there any alloc_groups that apply to multiple model_Codes -- yes, lots, duh,
-- alloc groups are roughly model based, each model can have many model_codes
-- are there any model_codes that apply to multiple alloc_Groups
-- nope, that needs to be a constraint
select model_code from (
select model_Code, alloc_Group
from nc.allocation_Groups
group by model_code, alloc_group 
) x group by model_code having count(*) > 1

do i need model_year?
how do i know?
it depends on how i model this stuff
and i am not yet clear on that

select alloc_Group, array_agg(distinct model_year order by model_year)
from nc.allocation_Groups
group by alloc_group 
order by alloc_group

-- this may be the best check available, (ESCUTL has been changed to ESC)
select * from gmgl.vehicle_orders a where alloc_Group <> 'ESCUTL' and not exists (select 1 from nc.allocation_groups where alloc_group = a.alloc_group)
-- the only exceptions here are chev med duty & LCF
select * from nc.build_out_2019 a where not exists (select * from nc.allocation_groups where alloc_Group = a.alloc_group)

select model_year
think its time to do a trigger

trying to reason about the modeling

GM: multiple same trim_level distinguished by peg (or style_name)
Honda/Nissan: multiple same trim_level distinguished by style_name
6/6/19, actually, distinguished by model_code, style_name only makes the differences more plain

as an example
vin: 2GC2KREG0K1194470
chrome_style_id: 398852
in chr.describe_vehicle (vin) there is an attribute vinDescription -> attributes -> styleName: "4WD Double Cab Work Truck"

in chr.get_describe_vehicle_by_style_id, there is no attribute called styleName, nor is there any attribute with a value of "4WD Double Cab Work Truck"
what i am using in config is: style -> attributes -> name: "4WD Double Cab 144.2\" Work Truck"

create temp table wtf as
select * 
from config a
left join (
  select r.style ->'attributes'->>'id' as chr_style_id, min(vin), max(vin),
    response -> 'vinDescription' -> 'attributes' ->> 'styleName'
  from chr.describe_vehicle a
  join jsonb_array_elements(response->'style') as r(style) on true
  group by r.style ->'attributes'->>'id',
    response -> 'vinDescription' -> 'attributes' ->> 'styleName') b on a.chrome_style_id = b.chr_Style_id

select * from wtf

select chrome_Style_id from wtf group by chrome_style_id having count(*) > 1    
-- shit, describe vehicle returns multiple values of styleName for the same chrome_style_id
select * from wtf where chrome_style_id in ('403396','391725','398854','391727')

select chrome_style_id from config group by chrome_style_id having count(*) > 1

857 unique csi
select r.style ->'attributes'->>'id' as chrome_style_id, min(vin), max(vin)
-- select count(*) -- 9347
from chr.describe_vehicle a
join jsonb_array_elements(response->'style') as r(style) on true
group by r.style ->'attributes'->>'id'


select * from config where chrome_style_id = '398852'

select distinct make from config

select *
from config
where make in ('Honda','Nissan')
order by make, model_year, model_code, trim_level
having count(*) > 1

drop table if exists config;
create temp table config as
select aa.model_year, aa.make, aa.model, coalesce(bb.alloc_group, 'n/a') as alloc_group, aa.cab, 
  aa.drive, aa.model_code, aa.box, aa.trim_level, aa.peg, aa.style_name, aa.chrome_style_id, aa.engine
from (
  select e.chrome_style_id,
    max(f.style -> 'attributes' ->> 'modelYear') as model_year,
    max(f.style -> 'division'  ->> '$value') as make,
    max(f.style -> 'model'  ->> '$value') as model,
    max(f.style -> 'attributes' ->> 'mfrModelCode') as model_code,
    max(f.style -> 'attributes' ->> 'trim') as trim_level,
--     max(f.style -> 'attributes' ->> 'drivetrain') as drive,
  max(case f.style -> 'attributes' ->> 'drivetrain'
    when 'Front Wheel Drive' then 'FWD'
    when 'All Wheel Drive' then 'AWD'
    when 'Rear Wheel Drive' then 'RWD'
    when 'Four Wheel Drive' then '4WD'
    else 'XXXXXX'
  end)::citext as drive,     
  max(case
    when f.style -> 'attributes' ->> 'altBodyType' like '%Bed%' 
      then trim(
        left(f.style -> 'attributes' ->> 'altBodyType', position('Cab' in f.style -> 'attributes' ->> 'altBodyType') -1))
    when f.style -> 'attributes' ->> 'altBodyType' like '%Chassis%' 
      then trim(
        left(f.style -> 'attributes' ->> 'altBodyType', position(' ' in f.style -> 'attributes' ->> 'altBodyType') - 1))
    else 'n/a'
  end)::citext as cab,
  max(case
    when f.style -> 'attributes' ->> 'altBodyType' like '%Bed%' 
      then trim(
        replace(
          substring(f.style -> 'attributes' ->> 'altBodyType', position('-' in f.style -> 'attributes' ->> 'altBodyType') + 2, 
            position('Bed' in f.style -> 'attributes' ->> 'altBodyType') -2), 'Bed', ''))
    else 'n/a'
  end)::citext as box,        
  array_agg(distinct g.engine -> 'displacement' -> 'value' -> 0 ->> '$value') as engine,
  max(h.pegs -> 'attributes' ->> 'oemCode') as peg,
  max(f.style -> 'attributes' ->> 'name') as style_name
  -- select count(*) -- 2397, 2274 incl unsuccessful
  from chr.get_describe_vehicle_by_style_id  e 
  left join jsonb_array_elements(e.response->'style') as f(style) on true 
  left join jsonb_array_elements(e.response->'engine') as g(engine) on true 
  left join jsonb_array_elements(e.response->'factoryOption') as h(pegs) on true
    and h.pegs -> 'header' ->> '$value' in ('PREFERRED EQUIPMENT GROUP')
  where e.response -> 'responseStatus' -> 'attributes' ->>'responseCode' = 'Successful'
    and f.style -> 'attributes' ->> 'fleetOnly' = 'false'
  group by e.chrome_style_id) aa
left join nc.allocation_groups bb on aa.model_year::integer = bb.model_year
  and aa.model_code = bb.model_code  
order by make, model, model_year, alloc_group, cab, drive, model_code, trim_level, peg;

gm: trim_level is more specifi than model_code
select * from config 
where model_year = '2018' 
  and model = 'Malibu'
not always, cruze model_code 1BT68 -> trim LT -> 4 pegs  

select * from config where model_year = '2019' and model = 'Acadia'
select chrome_style_id from config group by chrome_style_id having count(*) > 1

select * from nc.vehicle_configurations where chrome_style_id = '398852'
select * from nc.vehicles where configuration_id = 680
select jsonb_pretty(response) from chr.describe_vehicle where vin = '2GC2KREG0K1194470'

select jsonb_pretty(response) from chr.get_describe_vehicle_by_style_id where chrome_Style_id = '398824'
sometimes the model_Code is more specific:
select trim_level from (
select model_code, trim_level
from config
where make not in ('Nissan','Honda') and cab = 'n/a'
group by model_code, trim_level
) x group by trim_level having count(*) > 1

select * from config where trim_level = 'Grand Sport 1LT'

sometimes the trim is more specific
select model_code from (
select model_code, trim_level
from config
where make not in ('Nissan','Honda') and cab = 'n/a'
group by model_code, trim_level
) x group by model_code having count(*) > 1

select * from config where model_code = 'TXC26'

in addition model codes repeat across model years
select * from config where model_code = 'TXC26'

-------------------------------

hondas and nissans: model_code is more specific than trim_level
select * from config 
where model_year = '2018' 
  and model = 'CR-V'
  
select trim_level from (
select model_code, trim_level 
from config 
where model_year = '2018' 
  and model = 'CR-V'
  and drive = 'AWD'
group by  model_code, trim_level 
) x group by trim_level having count(*) > 1

select trim_level from (
select model_code, trim_level 
from config 
where model_year = '2018' 
  and make = 'Nissan'
group by  model_code, trim_level 
) x group by trim_level having count(*) > 1

select * from config where make not in ('Nissan','Honda') and cab = 'n/a'

select *
from config
where alloc_group <> 'n/a' and model_year = '2019' and model = 'Silverado 1500'
order by make, model, alloc_Group, cab, drive, box, model_code, trim_level, chrome_style_id

select * 
from config a
left join nc.vehicle_configurations b on a.chrome_style_id = b.chrome_style_id
where a.alloc_group <> 'n/a'
  and a.model_year = '2020'


selecT * 
from nc.vehicle_conf

selecT *
from nc.open_orders

select distinct a.*, b.cab
from nc.allocation_Groups a
left join nc.vehicle_configurations b on a.alloc_group = b.alloc_group
  and a.model_code = b.model_code
  and a.model_year = b.model_year
where a.model like 'silverado 1%'
order by model_year, model_code


select a.*, c.alloc_group
from nc.vehicles a
join nc.vehicle_acquisitions b on a.vin = b.vin
  and b.thru_date > current_date
left join nc.allocation_groups c on a.model_year = c.model_year
  and a.model_code = c.model_code
where a.model like 'silverado 1%'
  and a.cab = 'crew'

select * from nc.allocation_Groups where alloc_Group = 'CDBLLD'

-- check against sales
-- only a single alloc_group for crew cabs  
select a.model_year, c.alloc_group, count(*), array_agg(distinct a.model_code)
from nc.vehicles a
join nc.vehicle_sales b on a.vin = b.vin
--   and b.thru_date > current_date
left join nc.allocation_groups c on a.model_year = c.model_year
  and a.model_code = c.model_code
where a.model like 'silverado 1%'
  and a.cab = 'crew'
group by a.model_year, c.alloc_group


select * from config where model_year = '2019' and model = 'Equinox'

select jsonb_pretty(response) from chr.get_describe_vehicle_by_style_id where chrome_Style_id = '398677'

select * from nc.vehicle_configurations where model_year = 2019 and model = 'Silverado 1500' order by chrome_style_id

select * from nc.vehicles where 
select jsonb_pretty(response) from chr.describe_vehicle where vin = '3GNAXUEV4KS614273'

select * 
from gmgl.vehicle_option_codes
where vin = '3GNAXUEV4KS614273'
  and option_code in ('H1T','H72','HQJ')

factory_options
  header: seat trim
it appears that the same option codes apply to interiorcolor  
which is a high level category    
