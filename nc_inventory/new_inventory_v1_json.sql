﻿do
$$
declare 
  _model_year integer := 2018;
  _make citext := 'chevrolet';
  _model citext := 'equinox';
begin
drop table if exists wtf;
create temp table wtf as

with
  model_years as (
    select seq, model_year
    from nc.model_years
    where model_year = _model_year), --2018),
  color_stats as (
    select a.seq, a.color, 
      count(*) filter (where b.status = 'sold_365') as sold_365,
      count(*) filter (where b.status = 'sold_90') as sold_90,
      count(*) filter (where b.status = 'inv_og') as inv_og,
      count(*) filter (where b.status = 'inv_transit') as inv_transit,
      count(*) filter (where b.status = 'inv_system') as inv_system
    from nc.model_colors a
    left join nc.daily_inventory b on a.model_year = b.model_year
      and a.make = b.make
      and a.model = b.model
      and a.color = b.color
      and the_date = current_date - 1
    where a.model_year = _model_year -- 2019
      and a.make = _make -- 'chevrolet'
      and a.model = _model --'equinox'
    group by a.seq, a.color  
    order by a.seq),
  model_level_stats as (
    select model_code, trim_level, cab, drive, engine, status, count(*)  
    from nc.daily_inventory a
    where model_year = 2018 -- _model_year
      and make = 'chevrolet' --make
      and model = 'equinox' -- _model
    group by  model_code, trim_level, cab, drive, engine, status
    order by trim_level, model_code)
       


select row_to_json(b)
from (
  select array_to_json(array_agg(row_to_json(a))) as colors
  from (
    select * 
    from color_stats) a) b;

  


end
$$;  

select * from wtf;


-----------------------------------
-- some failed struggles from another script

(the basic pattern seems to be

select row_to_json(t)
from (
  select text, pronunciation,
    (
      select array_to_json(array_agg(row_to_json(d)))
      from (
        select part_of_speech, body
        from definitions
        where word_id=words.id
        order by position asc
      ) d
    ) as definitions
  from words
  where text = 'autumn'
) t

resulting in
{
  "text": "autumn", -- 1 text per word
  "pronunciation": "autumn", -- 1 pronounciation per word
  "definitions": [  -- multiple definitons per word
    {  -- each definition consists of a part_of_speech and a body
        "part_of_speech": "noun",  
        "body": "skilder wearifully uninfolded..."
    },
    {
        "part_of_speech": "verb",
        "body": "intrafissural fernbird kittly..."
    },
    {
        "part_of_speech": "adverb",
        "body": "infrugal lansquenet impolarizable..."
    }
  ]
}
)
-- equinox : object
--   model_year_colors: array
--     model_year:2018
--     seq:1
--     colors:array
--       0
--         color & stats
--       1
--         color & status

create table jon.equinox_json (response jsonb);
insert into jon.equinox_json values('{"equinox":{"model_year_colors":[{"model_year":2019,"sequence":"1","colors":[{"color":"red","sold_90":"8","sold_365":"10","in_sys":"8","in_trans":"110","on_grd":"8","sequence":"1"},{"color":"blue","sold_90":"8","sold_365":"10","in_sys":"8","in_trans":"110","on_grd":"8","sequence":"2"}]},{"model_year":2018,"sequence":"2","colors":[{"color":"red","sold_90":"8","sold_365":"10","in_sys":"8","in_trans":"110","on_grd":"8","sequence":"1"},{"color":"blue","sold_90":"8","sold_365":"10","in_sys":"8","in_trans":"110","on_grd":"8","sequence":"2"}]}],"equinox_types":[{"model_year":"2019","sequence":"1","levels":[{"trim":"LS","drive":"FWD","engine":"1.5","sold_90":"4","sold_365":"5","in_sys":"4","sequence":"1","in_trans":"55","on_grd":"4","colors":[{"color":"red","sold_90":"8","sold_365":"10","in_sys":"8","in_trans":"110","on_grd":"8","sequence":"1"},{"color":"blue","sold_90":"8","sold_365":"10","in_sys":"8","in_trans":"110","on_grd":"8","sequence":"2"}]},{"trim":"LT","drive":"FWD","engine":"1.5","sequence":"2","sold_90":"4","sold_365":"5","in_sys":"4","in_trans":"55","on_grd":"4","colors":[{"color":"red","sold_90":"8","sold_365":"10","in_sys":"8","in_trans":"110","on_grd":"8","sequence":"1"},{"color":"blue","sold_90":"8","sold_365":"10","in_sys":"8","in_trans":"110","on_grd":"8","sequence":"2"}]}]},{"model_year":"2018","sequence":"2","levels":[{"trim":"LS","drive":"FWD","engine":"1.5","sold_90":"4","sold_365":"5","sequence":"1","in_sys":"4","in_trans":"55","on_grd":"4","colors":[{"color":"red","sold_90":"8","sold_365":"10","in_sys":"8","in_trans":"110","on_grd":"8","sequence":"1"},{"color":"blue","sold_90":"8","sold_365":"10","in_sys":"8","in_trans":"110","on_grd":"8","sequence":"2"}]},{"trim":"LT","drive":"FWD","engine":"1.5","sold_90":"4","sold_365":"5","in_sys":"4","sequence":"2","in_trans":"55","on_grd":"4","colors":[{"color":"red","sold_90":"8","sold_365":"10","in_sys":"8","in_trans":"110","on_grd":"8","sequence":"1"},{"color":"blue","sold_90":"8","sold_365":"10","in_sys":"8","in_trans":"110","on_grd":"8","sequence":"2"}]}]}]}}')

select jsonb_each_text(response)
from jon.equinox_json

ok, need to create a json object called equinox.


with 
  stats as (
    select a.seq, a.color, 
      count(*) filter (where b.status = 'sold_365') as sold_365,
      count(*) filter (where b.status = 'sold_90') as sold_90,
      count(*) filter (where b.status = 'inv_og') as inv_og,
      count(*) filter (where b.status = 'inv_transit') as inv_transit,
      count(*) filter (where b.status = 'inv_system') as inv_system
    from nc.model_colors a
    left join nc.daily_inventory b on a.model_year = b.model_year
      and a.make = b.make
      and a.model = b.model
      and a.color = b.color
      and the_date = current_date - 1
    where a.model_year = 2019
      and a.make = 'chevrolet'
      and a.model = 'equinox'
    group by a.seq, a.color  
    order by a.seq),
  model_years as (
    select seq, model_year
    from nc.model_years
    where model_year = 2018),
  selected_model as (select 'equinox'::citext as model)

-- -- this is interesting
-- select json_build_object(model_year,
--   array_agg(
--     json_build_object('seq', seq, 'year', model_year)))
-- from model_years  
-- group by model_year
-- 
-- select row_to_json(b)
-- from (
-- select row_to_json(a) as equinox
-- from (
--   select *
--   from model_years
--   where model_year = 2018) a) b
  
select row_to_json (c)
from (
  select
    json_build_object(
      'model_year_colors', to_json(a)),
    (
    select array_to_json(array_agg(row_to_json(b)))
    from (
      select * 
      from stats) b) as colors
  from model_years a) c 

-- -- 11/6 1 
-- -- nope, 
-- -- 1.returns the colors array under the to level of json
-- -- 2'top level is called json_build_object
-- -- JSON
-- --   {}json_build_object
-- --     {}model_year_colors
-- --       seq : 1
-- --       model_year : 2018
-- --   []colors
-- select row_to_json (c)
-- from (
--   select
--     json_build_object(
--       'model_year_colors', to_json(a)),
--     (
--     select array_to_json(array_agg(row_to_json(b)))
--     from (
--       select * 
--       from stats) b) as colors
--   from model_years a) c 

--1.
select
  json_build_object(
    'model_year_colors', to_json(a))
from model_years a;      
-- this gives me: {"model_year_colors" : {"seq":1,"model_year":2018}}
-- which is not quite what i need
2.
-- -- need to wrap this an object with the model name
-- -- this doesn't do it
-- select
--   json_build_object(
--     'equinox', 
--       json_agg(json_build_object(
--         'model_year_colors', to_json(a))))
-- from model_years a;  