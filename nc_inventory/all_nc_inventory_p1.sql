﻿-- these dont work going back 700 days because the sale journal is not included
-- so these are essentially any vehicle that has been in inventory in the past 700 days
-- NOT current inventory going back to acquisitions of 700 days ago
--   select a.control, d.journal_code, max(b.the_date) as the_date, sum(a.amount) as amount
--   from fin.fact_gl a
--   join dds.dim_date b on a.date_key = b.date_key
--     and b.the_date between current_Date - 700 and current_date--'06/22/2018'
--   join fin.dim_account c on a.account_key = c.account_key
--   join nc.inventory_accounts cc on c.account = cc.account
--   join fin.dim_journal d on a.journal_key = d.journal_key
--     and d.journal_code in ('PVI','CDH','GJE')
--   join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
--   where a.post_status = 'Y'
--   group by a.control, d.journal_code
--   having sum(a.amount) > 10000
-- 

drop table if exists inv_1;  -- new vehicles currently in inventory
create temp table inv_1 as
select a.control-- , sum(a.amount) as amount
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_date between current_Date - 900 and current_date--'06/22/2018'
inner join fin.dim_account c on a.account_key = c.account_key 
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and c.account in ( -- all new vehicle inventory account
    select b.account
    from fin.dim_account b 
    where b.account_type = 'asset'
      and b.department_code = 'nc'
      and b.typical_balance = 'debit'
      and b.current_row
      and b.account <> '126104')   
--   and b.the_date between current_Date - 600 and current_date -- this has no effect
group by a.control
having sum(a.amount) > 10000;

drop table if exists inv_2; -- current inventory and acquisition journal
create temp table inv_2 as
select a.control, d.journal_code, max(b.the_date) as the_date, sum(a.amount) as amount
from fin.fact_gl a
join inv_1 aa on a.control = aa.control
join dds.dim_date b on a.date_key = b.date_key
join fin.dim_account c on a.account_key = c.account_key
join nc.inventory_accounts cc on c.account = cc.account
join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code in ('PVI','CDH','GJE')
join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y' 
group by a.control, d.journal_code
having sum(a.amount) > 10000  

drop table if exists dealer_trades;
create temp table dealer_trades as
select a.control, max(e.the_date) as the_date, sum(amount)
from fin.fact_gl a
join (
  select b.account, b.account_key
  from fin.dim_account b 
  where b.account_type = 'asset'
    and b.department_code = 'nc'
    and b.typical_balance = 'debit'
    and b.current_row
    and b.account <> '126104') c on a.account_key = c.account_key
join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code = 'VSN'
join dds.dim_date e on a.date_key = e.date_key
  and e.year_month > 201612 
where not exists ( -- no deal in bopmast
  select 1
  from sls.ext_bopmast_partial 
  where stock_number = a.control)
group by a.control
having sum(abs(a.amount)) > 10000    
order by control;

-- sales
-- use the bopmast vin
drop table if exists sales_1;
create temp table sales_1 as
select a.control, max(e.the_date) as the_date, sum(amount), 
  (case when f.stock_number is null then 'dealer_trade' else null end)::citext as dealer_trade,
  f.vin
from fin.fact_gl a
join ( 
  select b.account, b.account_key
  from fin.dim_account b 
  where b.account_type = 'asset'
    and b.department_code = 'nc'
    and b.typical_balance = 'debit'
    and b.current_row
    and b.account <> '126104') c on a.account_key = c.account_key
join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code = 'VSN'
join dds.dim_date e on a.date_key = e.date_key
  and e.year_month > 201612 
left join sls.ext_bopmast_partial f on a.control = f.stock_number
where a.post_status = 'Y'
group by a.control, f.stock_number, f.vin
having sum(abs(a.amount)) > 10000;

delete -- old goofy records, multiple vins per stocknumber
from sales_1
where control in (
  select control
  from (
    select control, vin
    from sales_1
    group by control, vin) a
  group by control 
  having count(*) > 1);

drop table if exists sales_2;
create temp table sales_2 as -- get missing vins
select control, vin
from (
  select a.control, coalesce(a.vin, b.inpmast_vin, c.vin) as vin
  from sales_1 a
  left join arkona.xfm_inpmast b on a.control = b.inpmast_stock_number
    and b.current_row
  left join sls.deals c on a.control = c.stock_number) c
group by control, vin;
create index on sales_2(control);
create index on sales_2(vin);

delete 
from sales_2 
where vin is null;

--  ok, with sales_2, have all the control/stocknumbers of sales
-- 1. add to vehicles
-- 2. add to acquisitions
-- 3. add to sales

select * from nc.vehicle_sales a
left join sales_2 b on a.stock_number = b.control
where sale_type = 'dealer trade'


select * from sales_1

select * from nc.vehicles limit 10
---------------------------------------------------------------------------------------------------------
--< nc.vehicles where chr.style_count = 1
---------------------------------------------------------------------------------------------------------
-- 5791 rows
-- 4252 rows not in nc.vehicles
-- 4015 rows where style_count = 1
-- 2155 rows where year > 2017
-- 2075 unique vins

vin <-sales_2
chrome_style_id <- chrome
model_year <- chrome
make <- inpmast
model <- chrome
model_code <- chrome
drive <- chrome
cab <- chrome
trim_level <-chrome
engine <- chrome
color <- inpmast <_ dataone

-------------------------- do sales_3, do fixes, then into nc.vehicles ---------------------------------------
drop table if exists sales_3 cascade;
create temp table sales_3 as

select a.vin, (r.style ->'attributes'->>'id')::integer as chr_style_id,
  (r.style ->'attributes'->>'modelYear')::citext as chr_model_year,
  case
    when b.make like 'HONDA%' then 'HONDA'::citext
    when b.make like 'CHEV%' then 'CHEVROLET'::citext
    else b.make
  end as make,
  (r.style ->'model'->>'$value'::citext)::citext as model,
  (r.style ->'attributes'->>'mfrModelCode')::citext as model_code,
  case
    when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drive,
  case
    when u.cab->>'$value' like 'Crew%' then 'crew' 
    when u.cab->>'$value' like 'Extended%' then 'double'  
    when u.cab->>'$value' like 'Regular%' then 'reg'  
    else 'N/A'   
  end as cab,
  r.style ->'attributes'->>'trim' as chr_trim,
  t.displacement->>'$value' as engine_displacement, b.color /**/, b.color_code /**/
from (
  select distinct vin
  from sales_2) a
join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
join chr.describe_vehicle c on a.vin = c.vin
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->'attributes'->>'unit' = 'liters'  
where c.style_count = 1
  and r.style ->'attributes'->>'modelYear' in ('2018','2019')
  and (r.style ->'model'->>'$value'::citext)::citext not like ('Silverado 1%')
  and (r.style ->'model'->>'$value'::citext)::citext not like ('Equin%')
  and exists (
    select 1
    from nc.vehicles 
    where vin <> a.vin);
create unique index on sales_3(vin);    

--------------------------------------------------------------------------------------------------------------
--< fix colors
--------------------------------------------------------------------------------------------------------------
-- like in equinox, test for color discrepancies
-- what about those with no matching rows (excluded by inner join)
select *
  from sales_3 a
  join nc.exterior_colors b on a.color_code = b.mfr_color_code
    and a.chr_model_year = b.model_year
    and a.make = b.make
    and a.model = b.model 
  where (a.color <> b.mfr_color_name or b.mfr_color_name is null)
order by b.model_year, b.make, b.model, a.color_code


-- and this fixes most of them
update sales_3 z
set color = x.mfr_color_name
from (
  select *
  from nc.exterior_colors) x
where z.chr_model_year = x.model_year
  and z.make = x.make
  and z.model::citext = x.model
  and z.color_code = x.mfr_color_code
  and z.color::citext <> x.mfr_color_name

  
update sales_3 set color = 'CAJUN RED TINTCOAT' where color = 'CAJUN RED';
update sales_3 set color = 'BLACK CURRANT METALLIC' where vin = '1GNEVKKW5KJ104105';
update sales_3 set color = 'SUMMIT WHITE' where vin = '3GTU2NECXJG424536';
update sales_3 set color = 'STONE BLUE METALLIC' where vin = '1GT42VEG4KF145590';
update sales_3 set color = 'CRYSTAL BLACK PEARL' where vin = '1HGCV1F14JA030152'; 
update sales_3 set color = 'OBSIDIAN BLUE PEARL' where model = 'Accord Sedan' and color_code = 'BV' and color <> 'OBSIDIAN BLUE PEARL';
update sales_3 set color = 'MODERN STEEL METALLIC' where model = 'Accord Sedan' and color_code = 'GY' and color <> 'MODERN STEEL METALLIC';
update sales_3 set color = 'RADIANT RED METALLIC' where model = 'Accord Sedan' and color_code = 'RX' and color = 'RADIENT RED';
update sales_3 set color = 'LUNAR SILVER METALLIC' where vin in ('1HGCV1F16JA123724','1HGCV1F46JA075815','2HKRW2H98JH640935',
  '5FPYK3F14JB016209','5FPYK3F5XKB010385');
update sales_3 set color = 'PLATINUM WHITE PEARL' where model = 'Accord Sedan' and color_code in('WA','WB');
update sales_3 set color = 'AEGEAN BLUE METALLIC' where vin in( '2HGFC4B09JH302388','SHHFK7H53JU200191','SHHFK7H57JU211727',
  'SHHFK7H24JU206121','19XFC2E57JE000541','2HGFC1F38JH642036','2HGFC1F30JH642015','2HGFC2F52JH507108');
update sales_3 set color = 'White Orchid Pearl' where vin = '2HGFC3B39JH354317';
UPDATE SALES_3 set color = 'Sonic Gray Pearl' where color = 'SONIC GREY PEARL';
update sales_3 set color = 'CRYSTAL BLACK PEARL' where vin in( '19XFC1F38JE001897','2HGFC2F73JH568369','SHHFK7H53JU214169');
update sales_3 set color = 'MODERN STEEL METALLIC' where vin in( '2HGFC2F52JH504435','5FNYF6H55JB011356',
  '5FNYF6H48KB000652','5FNYF6H55JB011356','5FNYF6H48KB000652');
update sales_3 set color = 'RALLYE RED' where vin = 'JHMFC1F40JX033525';
update sales_3 set color = 'LUNAR SILVER METALLIC' where model = 'Civic Sedan' and color_code = 'SI' and color <> 'LUNAR SILVER METALLIC';
update sales_3 set color = 'Taffeta White' where vin in ('2HGFC2F56JH507628');
update sales_3 set color = 'White Orchid Pearl' where vin in ('2HGFC2F71JH551585','19XFC2F77JE007618','JHMZC5F30JC008224');
update sales_3 set color = 'BASQUE RED PEARL II' where vin in ('2HKRW2H56JH611964');
update sales_3 set color = 'Pacific Pewter Metallic' where vin in ('5FNRL6H99JB081278');
update sales_3 set color = 'GLACIER WHITE' where vin in ('1N4AL3APXJC258539','1N4AL3AP0JC157400','1N4AL3AP0JC157445','1N4AL3AP3JC225849','1N6AA1F41JN521568');
update sales_3 set color = 'SUPER BLACK' where vin in ('1N4AL3AP2JC176210');
update sales_3 set color = 'Brilliant Silver' where vin in ('JN8AY2NE1J9732698');
update sales_3 set color = 'Arctic Blue Metallic' where vin in ('1N6DD0EV5JN758458','1N6AD0EV0JN720980','5N1AZ2MH6JN144268','5N1AZ2MH6JN132623');
update sales_3 set color = 'Cayenne Red' where vin in ('1N6DD0CW4JN703026');
update sales_3 set color = 'Magnetic Black' where vin in ('5N1AZ2MH9JN107652');
update sales_3 set color = 'Pearl White' where vin in ('5N1AZ2MH7JN197156','JN8AT2MV8JW350959');



--------------------------------------------------------------------------------------------------------------
--/> fix colors
--------------------------------------------------------------------------------------------------------------  

insert into nc.vehicles (vin,chrome_style_id,model_year,make,model,model_code,
  drive,cab,trim_level,engine,color)
select vin, chr_style_id, chr_model_year, make, model, model_code,
  drive, cab, chr_trim, engine_displacement ,color
from sales_3;  



---------------------------------------------------------------------------------------------------------
-- /> nc.vehicles where chr.style_count = 1
---------------------------------------------------------------------------------------------------------

-- invoice info
drop table if exists invoices cascade;
create temp table invoices as
select vin, trim(left(raw_invoice, position('GENERAL MOTORS LLC' in raw_invoice)-1)):: citext as description,
  case position('RYDELL' in raw_invoice)
    when 0 then 'dealer trade'
    else 'factory'
  end as source,
  left(substring(raw_invoice from position('MODEL' in raw_invoice) + 78 for 8), 
    position(' ' in substring(raw_invoice from position('MODEL' in raw_invoice) + 78 for 8))) as model_code,
  substring(raw_invoice from position(E'\n' in raw_invoice) + 1 for 3) as color_code,
  trim(substring(raw_invoice from position(E'\n' in raw_invoice) + 6 for 31))::citext as color       
from gmgl.vehicle_invoices
where thru_date > current_date;
create unique index on invoices(vin);

---------------------------------------------------------------------------------------------------------
--< acquisitions: nc.vehicles where chr.style_count = 1
---------------------------------------------------------------------------------------------------------
-- vins from nc.vehicles without a row in nc.vehicle_acquisitions
drop table if exists acq_1 cascade;
create temp table acq_1 as
select b.control, b.vin
from nc.vehicles a
join sales_2 b on a.vin = b.vin
where not exists (
  select 1
  from nc.vehicle_acquisitions
  where vin = a.vin); 
create index on acq_1(control);
create index on acq_1(vin);   


drop table if exists acq_2 cascade;
create temp table acq_2 as
select g.journal_date, g.source as acct_source, h.*
from (
  select f.journal_date, f.control, f.vin,
    case f.journal_code
      when 'PVI' then 'factory'
      when 'CDH' then 'dealer trade'
      when 'GJE' then 'ctp'
    end as source
  from ( -- accounting journal
    select a.control, aa.vin, d.journal_code, max(b.the_date) as journal_date, sum(a.amount) as amount
    from fin.fact_gl a
    join acq_1 aa on a.control = aa.control
    join dds.dim_date b on a.date_key = b.date_key
    join fin.dim_account c on a.account_key = c.account_key
    and c.account in ( -- all new vehicle inventory account
      select b.account
      from fin.dim_account b 
      where b.account_type = 'asset'
        and b.department_code = 'nc'
        and b.typical_balance = 'debit'
        and b.current_row
        and b.account <> '126104')     
    join fin.dim_journal d on a.journal_key = d.journal_key
      and d.journal_code in ('PVI','CDH','GJE')
    where a.post_status = 'Y'
    group by a.control, aa.vin, d.journal_code
    having sum(a.amount) > 10000) f) g
left join acq_1 h on g.control = h.control;



drop table if exists acq_3 cascade;
create temp table acq_3 as -- source modified with invoice info
select a.journal_date, a.control, a.vin,
  case
    when a.acct_source = 'ctp' then 'ctp'
    else coalesce(b.source, a.acct_source) 
  end as source
from acq_2 a
left join invoices b on a.vin = b.vin


-----------------------------------------------------------------------------------------------------------
--< vehicles from acq_1 that didn't make it to acq_3
-----------------------------------------------------------------------------------------------------------
-- drop table if exists problems;
-- create temp table problems as
-- select a.*
-- from acq_1 a
-- left join (
-- select control, vin, source, ground
-- from (
-- -- dealer trade ground dates
--   select aa.control, aa.vin, aa.journal_date, aa.source, 
--     bb.the_date, cc.creation_date as keyper,
--     coalesce(cc.creation_date, coalesce(bb.the_date, aa.journal_date)) as ground
--   from acq_3 aa
--   left join (
--     select d.vin, min(b.the_date) as the_Date
--     from ads.ext_fact_repair_order a
--     inner join dds.dim_date b on a.opendatekey = b.date_key
-- --     inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
--     inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
--     inner join acq_3 e on d.vin = e.vin
--     group by d.vin) bb on aa.vin = bb.vin
--   left join ads.keyper_creation_dates cc on aa.control = cc.stock_number  
--   where aa.source = 'dealer trade'
-- union
-- -- ctp trade ground dates
--   select aa.control, aa.vin, aa.journal_date, aa.source, 
--     null, null as keyper,
--     aa.journal_date as ground
--   from acq_3 aa
--   where aa.source = 'ctp'
-- union
-- -- factory trade ground dates
--   select aa.control, aa.vin, aa.journal_date, aa.source, 
--     bb.the_date, cc.creation_date as keyper,
--     coalesce(cc.creation_date, coalesce(bb.the_date, aa.journal_date)) as ground
--   from acq_3 aa
--   left join (
--     select d.vin, min(b.the_date) as the_Date
--     from ads.ext_fact_repair_order a
--     inner join dds.dim_date b on a.opendatekey = b.date_key
-- --     inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
--     inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
--     inner join acq_3 e on d.vin = e.vin
--     group by d.vin) bb on aa.vin = bb.vin
--   left join ads.keyper_creation_dates cc on aa.control = cc.stock_number  
--   where aa.source = 'factory') a) b on a.control  = b.control
-- where b.control is null    
-- 
-- select *
-- from problems a
-- join nc.vehicles b on a.vin = b.vin
-- 
-- -- this fixes 4 problems
-- insert into acq_2
--     select max(b.the_date) as journal_date, 'ctp'::citext as source, a.control, aa.vin
--     from fin.fact_gl a
--     join problems aa on a.control = aa.control
--     join dds.dim_date b on a.date_key = b.date_key
--     join fin.dim_account c on a.account_key = c.account_key
--     and c.account in ( -- all new vehicle inventory account
--       select b.account
--       from fin.dim_account b 
--       where b.account_type = 'asset'
--         and b.department_code = 'nc'
--         and b.typical_balance = 'debit'
--         and b.current_row
--         and b.account <> '126104')     
--     join fin.dim_journal d on a.journal_key = d.journal_key
--       and d.journal_code in ('WCM')
--     where a.post_status = 'Y'
--     group by a.control, aa.vin, d.journal_code
--     having sum(a.amount) > 10000
-- 
-- -- this fixes 8 problems
-- insert into acq_2    
--     select max(b.the_date) as journal_date, 'ctp'::citext as source, a.control, aa.vin
--     from fin.fact_gl a
--     join problems aa on a.control = aa.control
--     join dds.dim_date b on a.date_key = b.date_key
--     join fin.dim_account c on a.account_key = c.account_key
--     and c.account in ( -- all new vehicle inventory account
--       select b.account
--       from fin.dim_account b 
--       where b.account_type = 'asset'
--         and b.department_code = 'nc'
--         and b.typical_balance = 'debit'
--         and b.current_row
--         and b.account <> '126104')     
--     join fin.dim_journal d on a.journal_key = d.journal_key
--       and d.journal_code in ('PCA')
--     where a.post_status = 'Y'
--     group by a.control, aa.vin, d.journal_code
--     having sum(a.amount) > 10000    
-- 
-- -- and the last 2 problems
-- insert into acq_2 
-- select '03/06/2018','factory', control, vin
-- from problems
-- where control = 'H11113';
-- 
-- insert into acq_2 
-- select '05/02/2018','unwind', control, vin
-- from problems
-- where control = 'G33910';

-----------------------------------------------------------------------------------------------------------
--/> vehicles from acq_1 that didn't make it to acq_3
-----------------------------------------------------------------------------------------------------------
-- ground dates
insert into nc.vehicle_acquisitions
select aa.vin, aa.control, 
  case
    when source = 'ctp' then aa.journal_date
--       else coalesce(cc.creation_date, coalesce(bb.the_date, aa.journal_date)) 
    -- keyper dates are too potentially wacky
    else coalesce(bb.the_date, coalesce(cc.creation_date, aa.journal_date))
  end as ground_date, '12/31/9999'::date, 
  aa.source, false
from acq_3 aa
left join (
  select d.vin, min(b.the_date) as the_Date
  from ads.ext_fact_repair_order a
  inner join dds.dim_date b on a.opendatekey = b.date_key
--     inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
  inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
  inner join acq_3 e on d.vin = e.vin
  group by d.vin) bb on aa.vin = bb.vin
left join ads.keyper_creation_dates cc on aa.control = cc.stock_number; 


-- use nc.stg_sales
truncate nc.stg_sales; --2293
insert into nc.stg_sales(stock_number, vin, acct_date, ground_date)
select a.stock_number, a.vin, c.the_date, a.ground_date
from nc.vehicle_acquisitions a
join acq_3 b on a.vin = b.vin
left join sales_1 c on a.stock_number = c.control;
--null ground date
select * from nc.stg_sales where ground_date is null  
-- bopmast_id & delivery_date from sls.deals
update nc.stg_sales x
set bopmast_id = y.bopmast_id,
    delivery_date = y.delivery_date
from (    
  select b.*
  from nc.stg_sales a
  left join (
    select stock_number, bopmast_id, max(seq) as seq, max(delivery_date) as delivery_date
    from sls.deals
    group by stock_number, bopmast_id) b on a.stock_number = b.stock_number) y
where x.stock_number = y.stock_number;

select * from nc.stg_sales

-- modify the real script to use acq_3 and all inventory accounts

truncate nc.stg_sales; --2142
insert into nc.stg_sales (stock_number, vin, trans_description, amount, acct_date,ground_date)
select a.control, f.inpmast_vin, e.description, a.amount, b.the_date, g.ground_date
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
--   and b.the_date between current_Date - 40 and current_date
join fin.dim_account c on a.account_key = c.account_key
  and c.account in ( -- all new vehicle inventory account
    select b.account
    from fin.dim_account b 
    where b.account_type = 'asset'
      and b.department_code = 'nc'
      and b.typical_balance = 'debit'
      and b.current_row
      and b.account <> '126104')    
join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code = 'VSN'
join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
left join arkona.xfm_inpmast f on a.control = f.inpmast_stock_number
  and f.current_row = true  
left join nc.vehicle_acquisitions g on a.control = g.stock_number
  and g.thru_date = ( -- need most recent acquisition
    select max(thru_date)
    from nc.vehicle_acquisitions
    where stock_number = g.stock_number)  
join acq_3 h  on g.vin = h.vin 
where a.post_status = 'Y'
  and abs(a.amount) > 10000
  and f.year in ('2018', '2019')
  and f.model not like 'equ%'
  and f.model not like 'silverado 1%'
  and not exists (
    select 1
    from nc.vehicle_sales
    where stock_number = a.control
      and ground_date = g.ground_date) ;
do 
$$
begin
assert ( -- ground date null
  select count(*)
--   select stock_number
  from nc.stg_sales a
  where a.ground_date is null) = 0, 'NULL ground date';
end
$$;

-- bopmast_id & delivery_date from sls.deals
update nc.stg_sales x
set bopmast_id = y.bopmast_id,
    delivery_date = y.delivery_date
from (    
  select b.*
  from nc.stg_sales a
  left join (
    select stock_number, bopmast_id, max(seq) as seq, max(delivery_date) as delivery_date
    from sls.deals
    group by stock_number, bopmast_id) b on a.stock_number = b.stock_number) y
where x.stock_number = y.stock_number;

-- sale_types dealer trade & ctp
update nc.stg_sales x
set sale_type =
  case
    when bopmast_id is null then 'dealer trade'
    when trans_description like '%RYDELL%' then 'ctp'
  end;

 -- sale_type fleet
 update nc.stg_sales x
set sale_type = 'fleet'
where stock_number in (
  select a.stock_number
  from nc.stg_sales a
  inner join (
    select a.control, b.the_date
    from fin.fact_gl a
    inner join dds.dim_date b on a.date_key = b.date_key
    inner join fin.dim_account c on a.account_key = c.account_key
    inner join (
      select distinct d.gl_account
      from fin.fact_fs a 
      inner join fin.dim_fs b on a.fs_key = b.fs_key
        and b.year_month > 201601
        and b.page in (5,8,9,10)
        and b.line in (22,43)
        and b.col = 1 -- sales
      inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
        and c.store = 'ry1'
      inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key) d on c.account = d.gl_account
    where a.post_status = 'Y') b on a.stock_number = b.control);

-- dealer trade delivery dates
update nc.stg_sales
set delivery_date = acct_date
where sale_type = 'dealer trade';

-- and finally, everything else is retail
update nc.stg_sales
set sale_type = 'retail'
where sale_type is null;    

-- test for ground date > delivery date
-- 169 of these
do
$$
begin
assert (
  select count(*)
  -- select *
    from nc.stg_sales
    where ground_date > delivery_date) = 0, 'ground date > delivery date';
end
$$;

-- its broad, but good enuf, replace ground_date with acct_date for these
update nc.stg_sales z
set ground_date = x.acct_date
from (
  select *
  from nc.stg_sales
  where ground_date > delivery_date) x
where z.stock_number = x.stock_number

-- down to 12 rows and they are close enuf to just replace ground with delivery
update nc.stg_sales z
set ground_date = x.delivery_date
from (
  select *
  from nc.stg_sales
  where ground_date > delivery_date) x
where z.stock_number = x.stock_number

-- shit that won't work, vehicle.acquisitions also have the bad ground dates
-- yep
select * 
from nc.stg_sales a
join nc.vehicle_acquisitions b on a.stock_number = b.stock_number
where a.ground_Date <> b.ground_date
-- so
update nc.vehicle_acquisitions z
set ground_date = x.ground_date
from (
  select a.* 
  from nc.stg_sales a
  join nc.vehicle_acquisitions b on a.stock_number = b.stock_number
  where a.ground_Date <> b.ground_date) x
where z.stock_number = x.stock_number
  and z.ground_date <> x.ground_date;

-- update nc.vehicle_acquisitions.thru_date for sales
update nc.vehicle_acquisitions x
set thru_date  = y.delivery_date
from (
  select a.*
  from nc.stg_sales a
  left join nc.vehicle_acquisitions b on a.stock_number = b.stock_number) y
where x.stock_number = y.stock_number
  and x.thru_date = '12/31/9999';

-- and install the sales
-- ok got some problems
-- H11353 retailed 7/31/2018
--        unwound 10/17/18
--        sold ctp 11/08/18
insert into nc.vehicle_sales
select stock_number, ground_date, vin, coalesce(bopmast_id, -1), delivery_date, sale_type
from nc.stg_sales
group by stock_number, ground_date, vin, coalesce(bopmast_id, -1), delivery_date, sale_type

-- -- this was the only anomaly from the insert into sales  
-- delete 
-- from nc.stg_sales
-- where stock_number = 'H11353'
-- 
-- select *
-- from nc.stg_sales a
-- join (
--   select stock_number
--   from nc.stg_sales
--   group by stock_number
--   having count(*) > 1) b on a.stock_number = b.stock_number
-- 
-- select * from nc.vehicle_acquisitions where stock_number = 'H11353'  
-- update nc.vehicle_Acquisitions set thru_date = '07/31/2018' where stock_number = 'H11353';
-- insert into nc.vehicle_acquisitions values ('5FNYF6H54JB051556','H11353','10/17/2018','11/08/2018','unwind', false);
-- insert into nc.vehicle_sales values('H11353','2018-06-02','5FNYF6H54JB051556',15565,'10/17/2018','retail',false);
-- insert into nc.vehicle_sales values('H11353','10/17/2018','5FNYF6H54JB051556',16141,'11/08/2018','ctp',false);

-- -- 2 vehicles without an acquisition
-- select *
-- from nc.vehicles a
-- where not exists (
--   select 1
--   from nc.vehicle_acquisitions
--   where vin = a.vin)
-- 
-- -- 299178 G33910
-- retailed 4/30/18
-- unwind 5/2/18
-- sold dealer trade 5/3 (stk# chg G33910)
-- 
-- select * from nc.vehicles where vin = '3GKALXEX5JL299178'
-- insert into nc.vehicle_acquisitions values ('3GKALXEX5JL299178','G33910','04/30/2018','04/30/2018','dealer trade', false);
-- insert into nc.vehicle_acquisitions values ('3GKALXEX5JL299178','G33910','05/02/2018','05/03/2018','unwind', false);
-- insert into nc.vehicle_sales values('G33910','04/30/2018','3GKALXEX5JL299178',47857,'04/30/2018','retail',false);
-- insert into nc.vehicle_sales values('G33910','05/02/2018','3GKALXEX5JL299178',-1,'05/03/2018','dealer trade',false);
-- 
-- 5FNYF6H55JB026469  H11113
-- 3/13 acq dealer trade
-- 4/11 retail
-- select * from sls.deals where vin = '5FNYF6H55JB026469'
-- insert into nc.vehicle_acquisitions values ('5FNYF6H55JB026469','H11113','03/13/2018','04/11/2018','dealer trade', false);
-- insert into nc.vehicle_sales values('H11113','03/13/2018','5FNYF6H55JB026469',14966,'04/11/2018','retail',false);

-- no sales without an acquisition
select *
from nc.vehicle_sales a
where not exists (
  select 1
  from nc.vehicle_acquisitions
  where stock_number = a.stock_number)

-- uh oh, whole bunch, 113, of sale derived acquisitions with no row in vehicle_sales
-- most of them are ctp sales
drop table if exists uhoh cascade;
create temp table uhoh as
select a.*
from nc.vehicle_acquisitions a  
join nc.vehicles b on a.vin = b.vin
  and b.model not like 'silverado 1%'
  and b.model not like 'equin%'
where not exists (
  select 1
  from nc.vehicle_sales
  where stock_number = a.stock_number) ; 

-- 94 of them are ctp
-- first close out the acquisition
update nc.vehicle_acquisitions z
set thru_date = x.delivery_date
from (
  select a.*, b.bopmast_id, c.bopname_Search_name, b.delivery_date
  from uhoh a
  left join sls.ext_bopmast_partial b on a.stock_number = b.stock_number
  left join arkona.xfm_bopname c on b.buyer_bopname_id = c.bopname_record_key
    and c.current_row
    and b.store = 
  c.bopname_company_number
  left join sls.deals d on a.stock_number = d.stock_number
    and d.seq = (
      select max(seq)
      from sls.deals
      where stock_number = d.stock_number)
  where c.bopname_Search_name like '%rydell%') x
where z.stock_number = x.stock_number
  and z.ground_date = x.ground_date;  

-- create the sales record
insert into nc.vehicle_sales
select a.stock_number, a.ground_Date, a.vin, b.bopmast_id, b.delivery_date, 'ctp'
from uhoh a
left join sls.ext_bopmast_partial b on a.stock_number = b.stock_number
left join arkona.xfm_bopname c on b.buyer_bopname_id = c.bopname_record_key
  and c.current_row
  and b.store = 
c.bopname_company_number
left join sls.deals d on a.stock_number = d.stock_number
  and d.seq = (
    select max(seq)
    from sls.deals
    where stock_number = d.stock_number)
where (c.bopname_Search_name like '%und%' or c.bopname_Search_name like '%rydell%');



-- and the rest of them

-- create the sales record
-- insert into nc.vehicle_sales
-- one, G34877 has a bad ground date, less than sale date, change it to the same as the sale date
-- first fix the acquisition record
update nc.vehicle_Acquisitions
set ground_date = '08/24/2018'
where stock_number = 'g34877';
-- fix the uhoh record
update uhoh
set ground_date = '08/24/2018'
where stock_number = 'g34877';
-- first close out the acquisition
update nc.vehicle_acquisitions z
set thru_date = x.delivery_date
from (
  select a.stock_number, a.ground_Date, a.vin, b.bopmast_id, b.delivery_date, 
    case
      when a.stock_number = 'H10808' then 'ctp'
      else 'retail'
    end
  from uhoh a
  left join sls.ext_bopmast_partial b on a.stock_number = b.stock_number
  left join arkona.xfm_bopname c on b.buyer_bopname_id = c.bopname_record_key
    and c.current_row
    and b.store = c.bopname_company_number) x
where z.stock_number = x.stock_number
  and z.ground_date = x.ground_date; 
-- create the sales record
insert into nc.vehicle_sales
select a.stock_number, a.ground_Date, a.vin, b.bopmast_id, b.delivery_date, 
  case
    when a.stock_number = 'H10808' then 'ctp'
    else 'retail'
  end
from uhoh a
left join sls.ext_bopmast_partial b on a.stock_number = b.stock_number
left join arkona.xfm_bopname c on b.buyer_bopname_id = c.bopname_record_key
  and c.current_row
  and b.store = c.bopname_company_number;

-- here are the unwinds without an unwind acquisition
-- manually fixed it all
select a.run_date, a.bopmast_id, a.stock_number, a.vin, a.deal_Status_code, 
  a.unit_count, a.gl_date, a.delivery_Date, a.deal_status,
  b.year, b.make, b.model, c.ground_date, c.thru_date, c.source, 
  d.delivery_date, d.sale_type, d.bopmast_id
from sls.deals a
join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
  and b.year > 2017
left join nc.vehicle_acquisitions c on a.stock_number = c.stock_number  
left join nc.vehicle_sales d on a.stock_number = d.stock_number
where a.stock_number in (
  select a.stock_number 
  from sls.deals a
  where deal_status_Code not in ('U','A')
    and not exists (
      select 1
      from nc.vehicle_acquisitions
      where source = 'unwind'
        and stock_number = a.stock_number))
  and vehicle_type_code = 'N'        
order by a.stock_number, a.bopmast_id, run_date      

---------------------------------------------------------------------------------------------------------
-- /> acquisitions: nc.vehicles where chr.style_count = 1
---------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------
--< sales: nc.vehicles where chr.style_count = 1
---------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------
-- /> sales: nc.vehicles where chr.style_count = 1
---------------------------------------------------------------------------------------------------------






--------------------------------------------------------------------------------
--< sidetrack look for PEG in build data, to check trim vs chrome autobook
--------------------------------------------------------------------------------
select * from gmgl.vehicle_vis where vehicle_description like 'sierra%'

select* from gmgl.vehicle_order_options where order_number = 'WFKRQW'

Sierra 1500
TK15703 4WD Reg Cab
PEG: 1SA
     3SA: SLE

select * 
from gmgl.vehicle_order_options a
left join gmgl.vehicle_vis b on a.order_number = b.order_number
where a.option_code in ('1SA','3SA')

--------------------------------------------------------------------------------
--/> sidetrack look for PEG in build data, to check trim vs chrome autobook
--------------------------------------------------------------------------------
             


------------------------------------------------------------------------------------
--< colors issues
------------------------------------------------------------------------------------
-- -- -- there are some anomalies in nc.exterior_colors
-- -- -- multiple colors for same config, delete them in the creation of nc.exterior_colors
-- -- select *
-- -- from nc.exterior_colors a
-- -- join (
-- --   select model_year, make, model, mfr_color_code
-- --   from nc.exterior_colors
-- --   group by model_year, make, model, mfr_color_code
-- --   having count(*) > 1) b on a.model_year = b.model_year
-- --     and a.make = b.make
-- --     and a.model = b.model
-- --     and a.mfr_color_code = b.mfr_color_code
-- -- order by a.mfr_color_code
-- -- 
-- -- 
-- -- -- a few outliers compared to invoice, ignore them for now
-- -- select a.vin, a.chr_model_year, a.make, a.model, a.color_code, a.color, b.color_code, b.color
-- -- from sales_3 a
-- -- join ( -- invoice info, GM only
-- --   select vin, trim(left(raw_invoice, position('GENERAL MOTORS LLC' in raw_invoice)-1)):: citext as description,
-- --     case position('RYDELL' in raw_invoice)
-- --       when 0 then 'dealer trade'
-- --       else 'factory'
-- --     end as source,
-- --     left(substring(raw_invoice from position('MODEL' in raw_invoice) + 78 for 8), 
-- --       position(' ' in substring(raw_invoice from position('MODEL' in raw_invoice) + 78 for 8))) as model_code,
-- --     substring(raw_invoice from position(E'\n' in raw_invoice) + 1 for 3) as color_code,
-- --     trim(substring(raw_invoice from position(E'\n' in raw_invoice) + 6 for 31))::citext as color       
-- --   from gmgl.vehicle_invoices
-- --   where thru_date > current_date) b on a.vin = b.vin
-- -- where a.color <> b.color  
-- -- 
-- -- -- bogus colors, check invoices
-- -- select a.vin, a.chr_model_year, a.make, a.model, a.color_code, a.color, b.color_Code, b.color
-- -- from sales_3 a
-- -- left join ( -- invoice info, GM only
-- --   select vin, trim(left(raw_invoice, position('GENERAL MOTORS LLC' in raw_invoice)-1)):: citext as description,
-- --     case position('RYDELL' in raw_invoice)
-- --       when 0 then 'dealer trade'
-- --       else 'factory'
-- --     end as source,
-- --     left(substring(raw_invoice from position('MODEL' in raw_invoice) + 78 for 8), 
-- --       position(' ' in substring(raw_invoice from position('MODEL' in raw_invoice) + 78 for 8))) as model_code,
-- --     substring(raw_invoice from position(E'\n' in raw_invoice) + 1 for 3) as color_code,
-- --     trim(substring(raw_invoice from position(E'\n' in raw_invoice) + 6 for 31))::citext as color       
-- --   from gmgl.vehicle_invoices
-- --   where thru_date > current_date) b on a.vin = b.vin
-- -- where not exists (
-- --   select 1
-- --   from nc.exterior_colors
-- --   where mfr_color_name = a.color)
-- -- order by make, model, a.color_code
-- -- 
-- -- update sales_3 set color = 'CAJUN RED TINTCOAT' where color = 'CAJUN RED';
-- -- update sales_3 set color = 'BLACK CURRANT METALLIC' where vin = '1GNEVKKW5KJ104105';
-- -- update sales_3 set color = 'SUMMIT WHITE' where vin = '3GTU2NECXJG424536';
-- -- update sales_3 set color = 'STONE BLUE METALLIC' where vin = '1GT42VEG4KF145590';
-- -- update sales_3 set color = 'CRYSTAL BLACK PEARL' where vin = '1HGCV1F14JA030152'; 
-- -- update sales_3 set color = 'OBSIDIAN BLUE PEARL' where model = 'Accord Sedan' and color_code = 'BV' and color <> 'OBSIDIAN BLUE PEARL';
-- -- update sales_3 set color = 'MODERN STEEL METALLIC' where model = 'Accord Sedan' and color_code = 'GY' and color <> 'MODERN STEEL METALLIC';
-- -- update sales_3 set color = 'RADIANT RED METALLIC' where model = 'Accord Sedan' and color_code = 'RX' and color = 'RADIENT RED';
-- -- update sales_3 set color = 'LUNAR SILVER METALLIC' where vin in ('1HGCV1F16JA123724','1HGCV1F46JA075815','2HKRW2H98JH640935',
-- --   '5FPYK3F14JB016209','5FPYK3F5XKB010385');
-- -- update sales_3 set color = 'PLATINUM WHITE PEARL' where model = 'Accord Sedan' and color_code in('WA','WB');
-- -- update sales_3 set color = 'AEGEAN BLUE METALLIC' where vin in( '2HGFC4B09JH302388','SHHFK7H53JU200191','SHHFK7H57JU211727',
-- --   'SHHFK7H24JU206121','19XFC2E57JE000541','2HGFC1F38JH642036','2HGFC1F30JH642015','2HGFC2F52JH507108');
-- -- update sales_3 set color = 'White Orchid Pearl' where vin = '2HGFC3B39JH354317';
-- -- UPDATE SALES_3 set color = 'Sonic Gray Pearl' where color = 'SONIC GREY PEARL';
-- -- update sales_3 set color = 'CRYSTAL BLACK PEARL' where vin in( '19XFC1F38JE001897','2HGFC2F73JH568369','SHHFK7H53JU214169');
-- -- update sales_3 set color = 'MODERN STEEL METALLIC' where vin in( '2HGFC2F52JH504435','5FNYF6H55JB011356',
-- --   '5FNYF6H48KB000652','5FNYF6H55JB011356','5FNYF6H48KB000652');
-- -- update sales_3 set color = 'RALLYE RED' where vin = 'JHMFC1F40JX033525';
-- -- update sales_3 set color = 'LUNAR SILVER METALLIC' where model = 'Civic Sedan' and color_code = 'SI' and color <> 'LUNAR SILVER METALLIC';
-- -- update sales_3 set color = 'Taffeta White' where vin in ('2HGFC2F56JH507628');
-- -- update sales_3 set color = 'White Orchid Pearl' where vin in ('2HGFC2F71JH551585','19XFC2F77JE007618','JHMZC5F30JC008224');
-- -- update sales_3 set color = 'BASQUE RED PEARL II' where vin in ('2HKRW2H56JH611964');
-- -- update sales_3 set color = 'Pacific Pewter Metallic' where vin in ('5FNRL6H99JB081278');
-- -- update sales_3 set color = 'GLACIER WHITE' where vin in ('1N4AL3APXJC258539','1N4AL3AP0JC157400','1N4AL3AP0JC157445','1N4AL3AP3JC225849','1N6AA1F41JN521568');
-- -- update sales_3 set color = 'SUPER BLACK' where vin in ('1N4AL3AP2JC176210');
-- -- update sales_3 set color = 'Brilliant Silver' where vin in ('JN8AY2NE1J9732698');
-- -- update sales_3 set color = 'Arctic Blue Metallic' where vin in ('1N6DD0EV5JN758458','1N6AD0EV0JN720980','5N1AZ2MH6JN144268','5N1AZ2MH6JN132623');
-- -- update sales_3 set color = 'Cayenne Red' where vin in ('1N6DD0CW4JN703026');
-- -- update sales_3 set color = 'Magnetic Black' where vin in ('5N1AZ2MH9JN107652');
-- -- update sales_3 set color = 'Pearl White' where vin in ('5N1AZ2MH7JN197156','JN8AT2MV8JW350959');
-- -- 
-- -- 
-- -- -- yikes missing colors
-- -- -- most of these have to do with chr model name including sedan, etc, dataone does not,
-- -- -- you know what, i don't even care about these, they are valid colors, so, close enuf
-- -- select *
-- -- from sales_3 a
-- -- left join nc.exterior_colors b on a.color_code = b.mfr_color_code
-- --   and a.chr_model_year = b.model_year
-- --   and a.make = b.make
-- --   and a.model = b.model 
-- -- where b.model_year is null -- (a.color <> b.mfr_color_name or b.mfr_color_name is null)
-- --   and a.model not like '%sedan%'
-- --   and a.model not like '%hatchback%'
-- --   and a.model not like '%coupe%'
-- -- order by b.model_year, b.make, b.model, a.color_code


------------------------------------------------------------------------------------
--/> colors issues
------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------
-- < nc.vehicles where chr.style_count > 1
---------------------------------------------------------------------------------------------------------
-- sales
-- use the bopmast vin
drop table if exists sales_1;
create temp table sales_1 as
select a.control, max(e.the_date) as the_date, sum(amount), 
  (case when f.stock_number is null then 'dealer_trade' else null end)::citext as dealer_trade,
  f.vin
from fin.fact_gl a
join ( 
  select b.account, b.account_key
  from fin.dim_account b 
  where b.account_type = 'asset'
    and b.department_code = 'nc'
    and b.typical_balance = 'debit'
    and b.current_row
    and b.account <> '126104') c on a.account_key = c.account_key
join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code = 'VSN'
join dds.dim_date e on a.date_key = e.date_key
  and e.year_month > 201612 
left join sls.ext_bopmast_partial f on a.control = f.stock_number
where a.post_status = 'Y'
group by a.control, f.stock_number, f.vin
having sum(abs(a.amount)) > 10000;

delete -- old goofy records, multiple vins per stocknumber
from sales_1
where control in (
  select control
  from (
    select control, vin
    from sales_1
    group by control, vin) a
  group by control 
  having count(*) > 1);

drop table if exists sales_2;
create temp table sales_2 as -- get missing vins
select control, vin
from (
  select a.control, coalesce(a.vin, b.inpmast_vin, c.vin) as vin
  from sales_1 a
  left join arkona.xfm_inpmast b on a.control = b.inpmast_stock_number
    and b.current_row
  left join sls.deals c on a.control = c.stock_number) c
group by control, vin;
create index on sales_2(control);
create index on sales_2(vin);

delete 
from sales_2 
where vin is null;


drop table if exists sales_3 cascade;
create temp table sales_3 as
select a.vin, r.style ->'attributes'->>'id' as chr_style_id,
r.style ->'attributes'->>'modelYear' as chr_model_year,
  case
    when b.make like 'HONDA%' then 'HONDA'
    when b.make like 'CHEV%' then 'CHEVROLET'
    else b.make
  end as make,
  r.style ->'model'->>'$value'::citext as model,
  r.style ->'attributes'->>'mfrModelCode' as chr_model_code,
  case
    when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drive,
  case
    when u.cab->>'$value' like 'Crew%' then 'crew' 
    when u.cab->>'$value' like 'Extended%' then 'double'  
    when u.cab->>'$value' like 'Regular%' then 'reg'  
    else 'N/A'   
  end as cab,
  r.style ->'attributes'->>'trim' as chr_trim,
  t.displacement->>'$value' as engine_displacement, b.color, c.style_count, b.model_code, b.body_style, b.color_code
from (
  select distinct vin
  from sales_2) a
join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
join chr.describe_vehicle c on a.vin = c.vin
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->'attributes'->>'unit' = 'liters'  
where c.style_count <> 1
--   and b.model_code = r.style ->'attributes'->>'mfrModelCode'::citext
  and r.style ->'attributes'->>'modelYear' in ('2018','2019')
  and not exists (
    select 1
    from nc.vehicles 
    where vin = a.vin);

-- 135 distinct vins
select count(distinct vin) from sales_3

-- chr model code matches inpmast model code
select * --134
from sales_3  
where chr_model_code = model_code
order by vin

-- only one anomalous vin: 2HKRW2H81JH648088
select * 
from sales_3 a
where not exists (
  select 1
  from sales_3
  where vin = a.vin
    and chr_model_code = model_code)

drop table if exists sales_4 cascade;
create temp table sales_4 as
select * --134
from sales_3  
where chr_model_code = model_code
union
select *
from sales_3
where vin = '2HKRW2H81JH648088'
  and chr_model_code = 'RW2H8JJNW';
create unique index on sales_4(vin);

--------------------------------------------------------------------------------------------------------------
--< fix colors nc.vehicles where chr.style_count > 1
--------------------------------------------------------------------------------------------------------------
-- like in equinox, test for color discrepancies
-- what about those with no matching rows (excluded by inner join)
select *
  from sales_4 a
  join nc.exterior_colors b on a.color_code = b.mfr_color_code
    and a.chr_model_year = b.model_year
    and a.make = b.make
    and a.model = b.model 
  where (a.color <> b.mfr_color_name or b.mfr_color_name is null)
order by b.model_year, b.make, b.model, a.color_code    

-- and this fixes most of them
update sales_4 z
set color = x.mfr_color_name
from (
  select *
  from nc.exterior_colors) x
where z.chr_model_year = x.model_year
  and z.make = x.make
  and z.model::citext = x.model
  and z.color_code = x.mfr_color_code
  and z.color::citext <> x.mfr_color_name;

-- remaining anomalies
select *
from sales_4 a
left join nc.exterior_colors b on a.color_code = b.mfr_color_code
    and a.chr_model_year = b.model_year
    and a.make = b.make
    and a.model = b.model 
where b.make is null    

-- and the individual fixes  
update sales_4
set color = 'White Diamond Pearl'
where vin in ('2HGFC1E5XJH700666','2HGFC3A50JH754334');

update sales_4
set color = 'Deep Scarlet Pearl'
where vin in ('2HGFC1E5XJH706158');

update sales_4
set color = 'FOREST MIST METALLIC'
where vin in ('5FNRL6H74JB075032','5FNRL6H78JB056483');

update sales_4
set color = 'UNKNOWN'
where vin = '2HGFC3A53JH754165';

--------------------------------------------------------------------------------------------------------------
--/> fix colors nc.vehicles where chr.style_count > 1
--------------------------------------------------------------------------------------------------------------

insert into nc.vehicles (vin,chrome_style_id,model_year,make,model,model_code,
  drive,cab,trim_level,engine,color)
select vin, chr_style_id::integer, chr_model_year, make, model, model_code,
  drive, cab, chr_trim, engine_displacement ,color
from sales_4;

---------------------------------------------------------------------------------------------------------
--/> nc.vehicles where chr.style_count > 1
---------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------
--< acquisitions: nc.vehicles where chr.style_count > 1
---------------------------------------------------------------------------------------------------------

-- vins from nc.vehicles without a row in nc.vehicle_acquisitions
drop table if exists acq_1 cascade;
create temp table acq_1 as
select b.control, b.vin
from nc.vehicles a
join sales_2 b on a.vin = b.vin
where not exists (
  select 1
  from nc.vehicle_acquisitions
  where vin = a.vin); 
create index on acq_1(control);
create index on acq_1(vin); 

drop table if exists acq_2 cascade;
create temp table acq_2 as
select g.journal_date, g.source as acct_source, h.*
from (
  select f.journal_date, f.control, f.vin,
    case f.journal_code
      when 'PVI' then 'factory'
      when 'CDH' then 'dealer trade'
      when 'GJE' then 'ctp'
    end as source
  from ( -- accounting journal
    select a.control, aa.vin, d.journal_code, max(b.the_date) as journal_date, sum(a.amount) as amount
    from fin.fact_gl a
    join acq_1 aa on a.control = aa.control
    join dds.dim_date b on a.date_key = b.date_key
    join fin.dim_account c on a.account_key = c.account_key
    and c.account in ( -- all new vehicle inventory account
      select b.account
      from fin.dim_account b 
      where b.account_type = 'asset'
        and b.department_code = 'nc'
        and b.typical_balance = 'debit'
        and b.current_row
        and b.account <> '126104')     
    join fin.dim_journal d on a.journal_key = d.journal_key
      and d.journal_code in ('PVI','CDH','GJE')
    where a.post_status = 'Y'
    group by a.control, aa.vin, d.journal_code
    having sum(a.amount) > 10000) f) g
left join acq_1 h on g.control = h.control;

select * from acq_2 order by acct_source

-- don't have invoices yet to double check dealer trade acquisitions
insert into nc.vehicle_acquisitions
select aa.vin, aa.control, 
  case
    when acct_source = 'ctp' then aa.journal_date
--       else coalesce(cc.creation_date, coalesce(bb.the_date, aa.journal_date)) 
    -- keyper dates are too potentially wacky
    else coalesce(bb.the_date, coalesce(cc.creation_date, aa.journal_date))
  end as ground_date, '12/31/9999'::date, 
  aa.acct_source, false
from acq_2 aa
left join (
  select d.vin, min(b.the_date) as the_Date
  from ads.ext_fact_repair_order a
  inner join dds.dim_date b on a.opendatekey = b.date_key
--     inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
  inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
  inner join acq_2 e on d.vin = e.vin
  group by d.vin) bb on aa.vin = bb.vin
left join ads.keyper_creation_dates cc on aa.control = cc.stock_number; 

---------------------------------------------------------------------------------------------------------
--/> acquisitions: nc.vehicles where chr.style_count > 1
---------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------
--< sales: nc.vehicles where chr.style_count > 1
---------------------------------------------------------------------------------------------------------

-- use nc.stg_sales

-- modify the real script to use acq_2 and all inventory accounts

-- 139 rows
3: H10537
3: H11526
select * from sls.deals where stock_number in ('h10537','h11526')
select * from nc.stg_sales where stock_number in ('h10537','h11526')


truncate nc.stg_sales; --2142
insert into nc.stg_sales (stock_number, vin, trans_description, amount, acct_date,ground_date)
select a.control, f.inpmast_vin, e.description, a.amount, b.the_date, g.ground_date
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
--   and b.the_date between current_Date - 40 and current_date
join fin.dim_account c on a.account_key = c.account_key
  and c.account in ( -- all new vehicle inventory account
    select b.account
    from fin.dim_account b 
    where b.account_type = 'asset'
      and b.department_code = 'nc'
      and b.typical_balance = 'debit'
      and b.current_row
      and b.account <> '126104')    
join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code = 'VSN'
join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
left join arkona.xfm_inpmast f on a.control = f.inpmast_stock_number
  and f.current_row = true  
left join nc.vehicle_acquisitions g on a.control = g.stock_number
  and g.thru_date = ( -- need most recent acquisition
    select max(thru_date)
    from nc.vehicle_acquisitions
    where stock_number = g.stock_number)  
join acq_2 h  on g.vin = h.vin 
where a.post_status = 'Y'
  and abs(a.amount) > 10000
  and f.year in ('2018', '2019')
  and f.model not like 'equ%'
  and f.model not like 'silverado 1%'
  and not exists (
    select 1
    from nc.vehicle_sales
    where stock_number = a.control
      and ground_date = g.ground_date);

do 
$$
begin
assert ( -- ground date null
  select count(*)
--   select stock_number
  from nc.stg_sales a
  where a.ground_date is null) = 0, 'NULL ground date';
end
$$;


-- bopmast_id & delivery_date from sls.deals
update nc.stg_sales x
set bopmast_id = y.bopmast_id,
    delivery_date = y.delivery_date
from (    
  select b.*
  from nc.stg_sales a
  left join (
    select stock_number, bopmast_id, max(seq) as seq, max(delivery_date) as delivery_date
    from sls.deals
    group by stock_number, bopmast_id) b on a.stock_number = b.stock_number) y
where x.stock_number = y.stock_number;

-- sale_types dealer trade & ctp
update nc.stg_sales x
set sale_type =
  case
    when bopmast_id is null then 'dealer trade'
    when trans_description like '%RYDELL%' then 'ctp'
  end;

-- no fleet sales at honda

-- dealer trade delivery dates
update nc.stg_sales
set delivery_date = acct_date
where sale_type = 'dealer trade';

-- and finally, everything else is retail
update nc.stg_sales
set sale_type = 'retail'
where sale_type is null;    

-- test for ground date > delivery date
-- 16 of these
do
$$
begin
assert (
  select count(*)
  -- select *
    from nc.stg_sales
    where ground_date > delivery_date) = 0, 'ground date > delivery date';
end
$$;

-- its broad, but good enuf, replace ground_date with acct_date or delivery _date for these
update nc.stg_sales z
set ground_date = 
  case
    when x.acct_date <= x.delivery_date then x.acct_date
    when x.acct_date > x.delivery_date then x.delivery_date
  end
from (
  select *
  from nc.stg_sales
  where ground_date > delivery_date) x
where z.stock_number = x.stock_number

-- need to change the ground date in acquisitions as well
update nc.vehicle_acquisitions z
set ground_date = x.ground_date
from (
  select a.* 
  from nc.stg_sales a
  join nc.vehicle_acquisitions b on a.stock_number = b.stock_number
  where a.ground_Date <> b.ground_date) x
where z.stock_number = x.stock_number
  and z.ground_date <> x.ground_date;

-- update nc.vehicle_acquisitions.thru_date for sales
update nc.vehicle_acquisitions x
set thru_date  = y.delivery_date
from (
  select a.*
  from nc.stg_sales a
  left join nc.vehicle_acquisitions b on a.stock_number = b.stock_number) y
where x.stock_number = y.stock_number
  and x.thru_date = '12/31/9999';

-- and install the sales

insert into nc.vehicle_sales
select stock_number, ground_date, vin, coalesce(bopmast_id, -1), delivery_date, sale_type
from nc.stg_sales
group by stock_number, ground_date, vin, coalesce(bopmast_id, -1), delivery_date, sale_type;  

!!! everything looks good !!!
---------------------------------------------------------------------------------------------------------
--/> sales: nc.vehicles where chr.style_count > 1
---------------------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------
--< nightly 12/16/18 moved to and maintained in /nc_inventory/sql/all_nc_inventory_nightly.sql
--------------------------------------------------------------------------------------------

-- make invoice info available
drop table if exists invoices cascade;
create temp table invoices as
select vin, trim(left(raw_invoice, position('GENERAL MOTORS LLC' in raw_invoice)-1)):: citext as description,
  case position('RYDELL' in raw_invoice)
    when 0 then 'dealer trade'
    else 'factory'
  end as source,
  left(substring(raw_invoice from position('MODEL' in raw_invoice) + 78 for 8), 
    position(' ' in substring(raw_invoice from position('MODEL' in raw_invoice) + 78 for 8))) as model_code,
  substring(raw_invoice from position(E'\n' in raw_invoice) + 1 for 3) as color_code,
  trim(substring(raw_invoice from position(E'\n' in raw_invoice) + 6 for 31))::citext as color       
from gmgl.vehicle_invoices
where thru_date > current_date;
create unique index on invoices(vin);

truncate nc.stg_availability;
insert into nc.stg_availability (the_date,journal_date,stock_number,
  vin,model_year,make, model,
  model_code,color_code,color, source)
select current_date, f.the_date, f.control, 
  g.inpmast_vin, g.year, g.make, g.model, g.model_code, 
  g.color_code, g.color,
  case f.journal_code
    when 'PVI' then 'factory'
    when 'CDH' then 'dealer trade'
    when 'GJE' then 'ctp'
  end as source
from ( -- accounting journal
  select a.control, d.journal_code, max(b.the_date) as the_date, sum(a.amount) as amount
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_date between current_Date - 90 and current_date--'06/22/2018'
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account in ( -- all new vehicle inventory account
      select b.account
      from fin.dim_account b 
      where b.account_type = 'asset'
        and b.department_code = 'nc'
        and b.typical_balance = 'debit'
        and b.current_row
        and b.account <> '126104')   
  inner join fin.dim_journal d on a.journal_key = d.journal_key
    and d.journal_code in ('PVI','CDH','GJE')
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where a.post_status = 'Y'
  group by a.control, d.journal_code
  having sum(a.amount) > 10000) f
inner join arkona.xfm_inpmast g on f.control = g.inpmast_stock_number
  and g.model not like 'eq%' 
  and g.model not like 'silverado 1%'
  and g.current_row = true
  and g.year in ('2018', '2019')
where not exists (
  select 1
  from nc.vehicle_acquisitions
  where stock_number = f.control) ;

do
$$
begin
assert ( -- all gm vehicles have a vis
  select count(*)
  from nc.stg_availability a
  where make not in ('honda','nissan')
    and not exists (
      select 1
      from gmgl.vehicle_vis
      where vin = a.vin)) = 0, 'GM vehicles without a vis';
end
$$;      

-- GM only compare vis delivery store to accounting source
do 
$$
begin
assert ( -- Delivery store = Rydell, source <> factory
  select count(*)
  -- select a.*, regexp_replace(b.delivery_store, '\n', '', 'g')
  from nc.stg_availability a
  join gmgl.vehicle_vis b on a.vin = b.vin
  where regexp_replace(b.delivery_store, '\n', '', 'g') like 'RYDELL%'
    and a.stock_number not like '%R'
    and a.source <> 'factory') = 0, 'Delivery store = Rydell, source <> factory';
end
$$;

do 
$$
begin
assert ( -- Delivery store not Rydell, source = factory
  select count(*)
  -- select a.*, regexp_replace(b.delivery_store, '\n', '', 'g')
  from nc.stg_availability a
  join gmgl.vehicle_vis b on a.vin = b.vin
  where regexp_replace(b.delivery_store, '\n', '', 'g') not like 'RYDELL%'
    and a.stock_number not like '%R'
    and a.source = 'factory') = 0, 'Delivery store not Rydell, source = factory';
end
$$;

-- acct source = factory, delivery store = dealer trade
-- override acct source with vis delivery store
update nc.stg_availability
set source = 'dealer trade'
where stock_number in (
  select a.stock_number
  -- select a.*, regexp_replace(b.delivery_store, '\n', '', 'g')
  from nc.stg_availability a
  join gmgl.vehicle_vis b on a.vin = b.vin
  where regexp_replace(b.delivery_store, '\n', '', 'g') not like 'RYDELL%'
    and a.source = 'factory');


((( -- inpmast vs chrome  -- aha, use triple parens for a collapsible block

-- clean this up, basically this is inpmast vs chrome
drop table if exists wtf;
create temp table wtf as
select a.*, (r.style ->'attributes'->>'id')::integer as chr_style_id,
  (r.style ->'attributes'->>'modelYear')::citext as chr_model_year,
  (r.style ->'division'->>'$value')::citext as chr_make,
  (r.style ->'model'->>'$value'::citext)::citext as chr_model,
  (r.style ->'attributes'->>'mfrModelCode')::citext as chr_model_code,
  case
    when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as chr_drive,
  case
    when u.cab->>'$value' like 'Crew%' then 'crew' 
    when u.cab->>'$value' like 'Extended%' then 'double'  
    when u.cab->>'$value' like 'Regular%' then 'reg'  
    else 'N/A'   
  end as chr_cab,
  r.style ->'attributes'->>'trim' as chr_trim,
  t.displacement->>'$value' as engine_displacement, c.style_count/*, b.color , b.color_code */
from (
  select *
  from nc.stg_Availability) a
join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
join chr.describe_vehicle c on a.vin = c.vin
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->'attributes'->>'unit' = 'liters'  
where c.style_count = 1
  and r.style ->'attributes'->>'modelYear' in ('2018','2019')
  and (r.style ->'model'->>'$value'::citext)::citext not like ('Silverado 1%')
  and (r.style ->'model'->>'$value'::citext)::citext not like ('Equin%')
union
select a.*, (r.style ->'attributes'->>'id')::integer as chr_style_id,
  (r.style ->'attributes'->>'modelYear')::citext as chr_model_year,
  case
    when b.make like 'HONDA%' then 'HONDA'::citext
    when b.make like 'CHEV%' then 'CHEVROLET'::citext
    else b.make
  end as make,
  (r.style ->'model'->>'$value'::citext)::citext as model,
  (r.style ->'attributes'->>'mfrModelCode')::citext as model_code,
  case
    when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drive,
  case
    when u.cab->>'$value' like 'Crew%' then 'crew' 
    when u.cab->>'$value' like 'Extended%' then 'double'  
    when u.cab->>'$value' like 'Regular%' then 'reg'  
    else 'N/A'   
  end as cab,
  r.style ->'attributes'->>'trim' as chr_trim,
  t.displacement->>'$value' as engine_displacement, c.style_count/*, b.color , b.color_code */
from (
  select *
  from nc.stg_Availability) a
join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
join chr.describe_vehicle c on a.vin = c.vin
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->'attributes'->>'unit' = 'liters'  
where c.style_count > 1
  and r.style ->'attributes'->>'modelYear' in ('2018','2019')
  and (r.style ->'model'->>'$value'::citext)::citext not like ('Silverado 1%')
  and (r.style ->'model'->>'$value'::citext)::citext not like ('Equin%')
  and a.model_code = (r.style ->'attributes'->>'mfrModelCode')::citext;

select * from wtf a
where exists (select 1 from nc.vehicles where vin = a.vin)


-- differences betw inpmast and chr
select 'model' as field, model, chr_model
from wtf
where coalesce(model, 'inp') <> coalesce(chr_model, 'chr')
union
select 'make', make, chr_make
from wtf
where make <> chr_make
union
select 'model_code', model_code, chr_model_code
from wtf
where model_code <> chr_model_code
union
select 'cab', cab, chr_cab
from wtf
where coalesce(cab, 'inp') <> coalesce(chr_cab, 'chr')
union
select 'drive', drive, chr_drive
from wtf
where drive <> chr_drive

this is no good
at this point in time nc.stg_availability is all nulls for drive, cab, trim, engine
thinking, what do i want in stg_availability:
based on accounting, vehicles that do not yet exist in nc.vehicle_acquisitions
and this table is the foundation for new rows in vehicles and vehicle_Acquisitions

well and good, not up to a full refactoring yet, get the nightly deal going in the style of equinoxen
)))


((( -- compare source in invoice to source in vis

invoice for 1GYFZBR48KF130734 shows the dealer as CADILLAC OF GRAND FORKS
but the vis delivery_store = RYDELL CHEVROLET BUICK GMC CADILLAC
select * from gmgl.vehicle_vis where vin = '1GYFZBR48KF130734'

-- invoice and vis are in complete aggreement
drop table if exists sources cascade;
create temp table sources as
select a.make, a.stock_number, a.vin, a.source,
  case 
    when position('RYDELL' in b.raw_invoice) = 0 and position('CADILLAC OF GRAND FORKS' in b.raw_invoice) = 0 then 'dealer trade'
    when position('RYDELL' in b.raw_invoice) <> 0 or position('CADILLAC OF GRAND FORKS' in b.raw_invoice) <> 0 then 'factory'
    else 'xxx'
  end as inv_source, 
  case 
    when position('RYDELL' in replace(regexp_replace(c.delivery_store, '\n', '', 'g'), '&nbsp;','')) = 0 then 'dealer trade'
    when position('RYDELL' in replace(regexp_replace(c.delivery_store, '\n', '', 'g'), '&nbsp;','')) <> 0 then 'factory' 
    else 'xxx' 
  end as vis_source
from nc.stg_availability a  
left join gmgl.vehicle_invoices b on a.vin = b.vin
  and b.thru_date > current_date
left join gmgl.vehicle_vis c on a.vin = c.vin

select * 
from sources
where inv_source <> 'xxx'
  and source <> 'ctp'
  and source <> inv_source
))))



-- unwind acquisitions
-- test for unwind on same day as sale ?!?
insert into nc.stg_availability (the_date,stock_number,vin,model_year,
  model_code,color_code,color,ground_date,source)
select current_date, a.stock_number, a.vin, b.model_year,
  b.model_Code, c.color_code, b.color, a.run_date - 1, 'unwind'
from sls.deals a
join nc.vehicle_acquisitions aa on a.stock_number = aa.stock_number -- to be an unwind, previous acq record must exist
join nc.vehicles b on a.vin = b.vin
  and b.model <> 'equinox'
  and b.model not like 'silverado 1%'
join arkona.xfm_inpmast c on b.vin = c.inpmast_vin
  and c.current_row = true
where a.run_date > current_Date - 60
  and deal_status_code = 'deleted'
  and a.stock_number <> 'H11601'
  and not exists (
    select 1
    from nc.vehicle_acquisitions
    where stock_number = a.stock_number
      and ground_date = a.run_date - 1);

-------------------------------------------------------------------------------------
--< ground dates
-------------------------------------------------------------------------------------
-- ctp
update nc.stg_availability
set ground_date = journal_date
where source = 'ctp';

-- dealer trades: ground = coalesce(keyper, coaelsece dt ro, journal date)
-- 10/23 any ro
update nc.stg_availability x
set ground_date = y.ground
from (
  select aa.stock_number, aa.vin, aa.journal_date, aa.source, 
    bb.the_date, cc.creation_date as keyper,
    coalesce(cc.creation_date, coalesce(bb.the_date, aa.journal_date)) as ground
  from nc.stg_availability aa
  left join (
    select d.vin, min(b.the_date) as the_Date
    from ads.ext_fact_repair_order a
    inner join dds.dim_date b on a.opendatekey = b.date_key
--     inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
    inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
    inner join nc.stg_availability e on d.vin = e.vin
    group by d.vin) bb on aa.vin = bb.vin
  left join ads.keyper_creation_dates cc on aa.stock_number = cc.stock_number  
  where aa.source = 'dealer trade') y
where x.stock_number = y.stock_number; 

-- factory
-- 10/23 any ro, no need for order info
-- if there is neither keyper nor ro, ground = 12/31/9999
update nc.stg_availability x
set ground_date = y.ground
from (
  select aa.stock_number, aa.vin, aa.journal_date, aa.source, 
    bb.the_date, cc.creation_date as keyper, -- dd.event_code, dd.description, dd.from_date,
    coalesce(cc.creation_date, coalesce(bb.the_date, '12/31/9999'::date)) as ground
  from nc.stg_availability aa
  left join (
    select d.vin, min(b.the_date) as the_date
    from ads.ext_fact_repair_order a
    inner join dds.dim_date b on a.opendatekey = b.date_key
    inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
    inner join nc.stg_availability e on d.vin = e.vin
    group by d.vin) bb on aa.vin = bb.vin
  left join ads.keyper_creation_dates cc on aa.stock_number = cc.stock_number  
  where source = 'factory') y  
where x.stock_number = y.stock_number;

-- ok, delete if ground = 12/31/9999 
-- those will be picked up as in transit/in system orders
delete 
-- select * 
from nc.stg_availability
where ground_date = '12/31/9999';

do
$$
begin
assert ( -- test for null ground date
  select count(*)
  -- select *
  from nc.stg_availability a
  where ground_date is null) = 0, 'acquisitions with null ground date';
end
$$;
-------------------------------------------------------------------------------------
--/> ground dates
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--< colors
-------------------------------------------------------------------------------------

select * from gmgl.vehicle_invoices where vin = '1GTU9DED0KZ202958'

do
$$
begin
assert (
  select count(*)
  -- select *
  from nc.stg_availability a
  left join nc.exterior_colors b on a.color_code = b.mfr_color_code
    and a.model_year = b.model_year
    and a.make = b.make
    and trim(replace(a.model, 'SEDAN',''))::citext = b.model -- honda models
  where (a.color <> b.mfr_color_name or b.mfr_color_name is null)) = 0, 'DANGER WILL ROBINSON';
end
$$;

( -- color fixes

-- -- sometimes, the color in inpmast matches the color in the invoice which is misspelled
-- select * from gmgl.vehicle_invoices where vin = '1GTU9DED0KZ202958'
-- select * from nc.exterior_colors where model = 'sierra 1500' and mfr_color_code = 'GAN'

update nc.stg_availability
set color = 'Quicksilver Metallic'
where vin = '1GTU9DED0KZ202958';

update nc.stg_availability
set color = 'RED QUARTZ TINTCOAT'
where vin = '3GTU2NEJXJG569459';

select * 
from nc.stg_Availability a
left join invoices b on a.vin = b.vin
where a.color <> b.color

select a.vin, a.model_year, a.make, a.model, a.color_code, a.color, b.*
from nc.stg_availability a
left join nc.exterior_colors b on a.color_code = b.mfr_color_code
  and a.model_year = b.model_year
  and a.make = b.make
  and trim(replace(a.model, 'SEDAN',''))::citext = b.model -- honda models
where (a.color <> b.mfr_color_name or b.mfr_color_name is null)
order by color_code

update nc.stg_availability
set color = 'Magnetic Black Pearl'
where vin in ('1N6DD0EV7KN727147','5N1AT2MV0KC745650');

update nc.stg_availability
set color = 'Pearl White Tricoat'
where vin in ('1N4BL4DW0KN308030');


-- the remaining 24 are all valid colors
update nc.stg_availability
set color = 'Nightfall Gray Metallic'
where vin in ('KL8CD6SA8KC721218');
update nc.stg_availability
set color = 'SATIN STEEL GRAY METALLIC'
where vin in ('KL4CJGSM6KB734811','KL4CJGSM5KB750966','LRBFX2SA9KD030521','SATIN STEEL GRAY METALLIC');

update nc.stg_availability
set color = 'Caspian Blue'
where vin in ('5N1AT2MV5JC838808');
update nc.stg_availability
set color = 'Caspian Blue Metallic'
where vin in ('JN8AT2MV2KW377818');
update nc.stg_availability
set color = 'Magnetic Black'
where vin in ('KNMAT2MV0JP576709','KNMAT2MV0JP561921','KNMAT2MV9JP551971');
update nc.stg_availability
set color = 'Magnetic Black Pearl'
where vin in ('1N6AD0EV5KN711757','5N1AT2MV1KC739534','JN8AT2MV1KW379690');
update nc.stg_availability
set color = 'Modern Steel Metallic'
where vin in ('2HKRW2H54JH685450','2HKRW2H89JH668556');
update nc.stg_availability
set color = 'Pacific Blue Metallic'
where vin in ('1G1ZD5ST3KF115149');
update nc.stg_availability
set color = 'Pearl White'
where vin in ('5N1AT2MV4JC771389');
update nc.stg_availability
set color = 'Polished Metal Metallic'
where vin = 'SHHFK7H64KU200162'
update nc.stg_availability
set color = 'PEARL WHITE'
where vin = 'JN1BJ1CRXJW254547'
update nc.stg_availability
set color = 'Scarlet Ember'
where vin = '1N4BL4DV8KC113845'

update nc.stg_availability
set color = 'Basque Red Pearl II'
where model = 'CR-V'
  and color_code = 'RB'
  and color <> 'Basque Red Pearl II';

update nc.stg_availability
set color = 'Brilliant Silver Metallic'
where make = 'NISSAN'
  and color_code = 'K23'
  and color <> 'Brilliant Silver Metallic';

update nc.stg_availability
set color = 'Cajun Red Tintcoat'
where make = 'Chevrolet'
  and color_code = 'GPJ'
  and color <> 'Cajun Red Tintcoat';  

update nc.stg_availability
set color = 'Caspian Blue Metallic'
where model = 'rogue'
  and color_code = 'RBY'
  and color <> 'Caspian Blue Metallic';    

update nc.stg_availability
set color = 'Chili Red Metallic'
where model = 'envision'
  and color_code = 'G8B'
  and color <> 'Chili Red Metallic';  

update nc.stg_availability
set color = 'Crystal White Tricoat'
where model = 'xt5'
  and color_code = 'G1W'
  and color <> 'Crystal White Tricoat';  
update nc.stg_availability
set color = 'Deep Blue Pearl Metallic'
where model = 'sentra'
  and color_code = 'RAY'
  and color <> 'Deep Blue Pearl Metallic';  
update nc.stg_availability
set color = 'Iridium Metallic'
where model = 'acadia'
  and color_code = 'gxg'
  and color <> 'Iridium Metallic';  
update nc.stg_availability
set color = 'Monarch Orange/Super Black'
where model = 'kicks'
  and color_code = 'XAH'
  and color <> 'Monarch Orange/Super Black';  
update nc.stg_availability
set color = 'Mosaic Black Metallic'
where model = 'sonic'
  and color_code = 'GB8'
  and color <> 'Mosaic Black Metallic';  
update nc.stg_availability
set color = 'Nightfall Gray Metallic'
where model = 'spark'
  and color_code = 'GK2'
  and color <> 'Nightfall Gray Metallic';  
update nc.stg_availability
set color = 'Northsky Blue Metallic'
where model = 'malibu'
  and color_code = 'GA0'
  and color <> 'Northsky Blue Metallic';  
update nc.stg_availability
set color = 'Obsidian Blue Pearl'
where model = 'cr-v'
  and color_code = 'BU'
  and color <> 'Obsidian Blue Pearl';                
update nc.stg_availability
set color = 'Pacific Blue Metallic'
where model = 'malibu'
  and color_code = 'g60'
  and color <> 'Pacific Blue Metallic';
update nc.stg_availability
set color = 'Pearl White Tricoat'
where make = 'nissan'
  and color_code = 'qab'
  and color <> 'Pearl White Tricoat';
update nc.stg_availability
set color = 'Pepperdust Metallic'
where model = 'acadia'
  and color_code = 'gmu'
  and color <> 'Pepperdust Metallic';
update nc.stg_availability
set color = 'Red Quartz Tintcoat'
where model = 'acadia'
  and color_code = 'gpj'
  and color <> 'Red Quartz Tintcoat';
update nc.stg_availability
set color = 'Sandstorm Metallic'
where model = 'cr-v'
  and color_code = 'bg'
  and color <> 'Sandstorm Metallic';
update nc.stg_availability
set color = 'Satin Steel Metallic'
where make = 'buick'
  and color_code = 'gir'
  and color <> 'Satin Steel Metallic';
update nc.stg_availability
set color = 'Satin Steel Metallic'
where make = 'gmc'
  and color_code = 'g9k'
  and color <> 'Satin Steel Metallic';
update nc.stg_availability
set color = 'Satin Steel Metallic'
where make = 'buick'
  and color_code = 'GYM'
  and color <> 'Satin Steel Metallic';
update nc.stg_availability
set color = 'Storm Blue'
where model = 'altima'
  and color_code = 'rbd'
  and color <> 'Storm Blue';
update nc.stg_availability
set color = 'Summit White'
where model = 'acadia'
  and color_code = 'gaz'
  and color <> 'Summit White';     
update nc.stg_availability
set color = 'White Diamond Pearl'
where model = 'cr-v'
  and color_code = 'wb'
  and color <> 'White Diamond Pearl'; 
update nc.stg_availability
set color = 'White Frost Tricoat'
where make = 'gmc'
  and color_code = 'g1w'
  and color <> 'White Frost Tricoat'; 
update nc.stg_availability
set color = 'Winterberry Red Metallic'
where model = 'encore'
  and color_code = 'gcs'
  and color <> 'Winterberry Red Metallic'; 
update nc.stg_availability
set color = ''
where model = 'rogue'
  and color_code = 'RBY'
  and color <> ''; 
update nc.stg_availability
set color = ''
where model = 'rogue'
  and color_code = 'RBY'
  and color <> ''; 
update nc.stg_availability
set color = ''
where model = 'rogue'
  and color_code = 'RBY'
  and color <> ''; 
update nc.stg_availability
set color = ''
where model = 'rogue'
  and color_code = 'RBY'
  and color <> ''; 
update nc.stg_availability
set color = ''
where model = 'rogue'
  and color_code = 'RBY'
  and color <> '';
update nc.stg_availability
set color = ''
where model = 'rogue'
  and color_code = 'RBY'
  and color <> ''; 
update nc.stg_availability
set color = ''
where model = 'rogue'
  and color_code = 'RBY'
  and color <> ''; 
update nc.stg_availability
set color = ''
where model = 'rogue'
  and color_code = 'RBY'
  and color <> '';       
)                                     
-------------------------------------------------------------------------------------
--/> colors
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--< chrome
-------------------------------------------------------------------------------------
do -- all vins exist in chr.describe_vehicle
$$
begin
assert (
  select count(*)
--   select * --a.vin
  from nc.stg_availability a
  left join (
    select vin, count(*)
    from (
      select vin, jsonb_array_elements(response->'style')
      from chr.describe_vehicle) x
    group by vin) b on a.vin = b.vin
  where b.vin is null) = 0, 'DANGER WILL ROBINSON, there are new vins missing from chr.describe_vehicle';
end
$$;  


do
$$
begin
assert ( -- vins with multiple chrome styles
      select count(*) = sum(b.style_count)
      from nc.stg_availability a
      left join chr.describe_vehicle b on a.vin = b.vin), 'vehicles with multiple chrome styles';
end
$$;


i anticipate multiple scenarios for vin with multiple chrome styles
this one is very straightforward
all hondas
all resolved by matching inpmast model_code to chrome model_code


select a.*
from nc.stg_availability a      
left join chr.describe_vehicle b on a.vin = b.vin
left join jsonb_array_elements(b.response->'style') as r(style) on true
where a.model_code = (r.style ->'attributes'->>'mfrModelCode')::citext


select *
from nc.stg_Availability a
join chr.describe_vehicle b on a.vin = b.vin
where b.style_count <> 1


  select a.*,
    r.style ->'attributes'->>'id' as chr_style_id,
    r.style ->'attributes'->>'mfrModelCode' as chr_model_code,
    r.style ->'attributes'->>'modelYear' as chr_model_year,
    r.style ->'attributes'->>'name' as style,
    r.style ->'attributes'->>'drivetrain' as drive_train,
    t.displacement->>'$value' as engine_displacement
  from nc.stg_availability a      
  left join chr.describe_vehicle b on a.vin = b.vin
  left join jsonb_array_elements(b.response->'style') as r(style) on true
  left join jsonb_array_elements(b.response->'engine') as s(engine) on true
  left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
    and t.displacement ->'attributes'->>'unit' = 'liters'
  where b.style_count <> 1
    and a.model_code <> (r.style ->'attributes'->>'mfrModelCode')::citext
-------------------------------------------------------------------------------------
--/> chrome
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--< new acquisitioins where vin already exists in nc.vehicles (ctp)
-------------------------------------------------------------------------------------
insert into nc.vehicle_acquisitions (vin,stock_number,ground_date,source,in_transit_sale)
select a.vin, a.stock_number, a.ground_date, a.source, false
from nc.stg_availability a
inner join nc.vehicles aa on a.vin = aa.vin
  and a.source <> 'unwind'; -- unwinds are processed separately
-------------------------------------------------------------------------------------
--/> new acquisitioins where vin already exists in nc.vehicles (ctp)
-------------------------------------------------------------------------------------  

--------------------------------------------------------------------------------------------------
--< nc.vehicles
--------------------------------------------------------------------------------------------------

(( -- nc.stg_vehicles
/*
  for now at least, start with stg_Vehicles, clean up the anomalies before 
  inserting into nc.vehicles
*/
-- CREATE TABLE nc.stg_vehicles
-- (
--   vin citext NOT NULL,
--   chrome_style_id integer DEFAULT '-1'::integer,
--   model_year citext NOT NULL,
--   make citext NOT NULL,
--   model citext NOT NULL,
--   model_code citext NOT NULL,
--   drive citext NOT NULL,
--   cab citext NOT NULL,
--   trim_level citext,
--   engine citext,
--   color citext NOT NULL,
--   sku_id integer,
--   PRIMARY KEY (vin)
-- ); 

truncate nc.stg_vehicles;
insert into nc.stg_vehicles (vin, chrome_Style_id, model_year, make, model,
  model_code, drive, cab, trim_level, engine, color)
-- the single chrome_style vins
select a.vin, (r.style ->'attributes'->>'id')::integer as chr_style_id,
  (r.style ->'attributes'->>'modelYear')::citext as chr_model_year,
  (r.style ->'division'->>'$value')::citext as chr_make,
  (r.style ->'model'->>'$value'::citext)::citext as chr_model,
  (r.style ->'attributes'->>'mfrModelCode')::citext as chr_model_code,
  case
    when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as chr_drive,
  case
    when u.cab->>'$value' like 'Crew%' then 'crew' 
    when u.cab->>'$value' like 'Extended%' then 'double'  
    when u.cab->>'$value' like 'Regular%' then 'reg'  
    else 'N/A'   
  end as chr_cab,
  r.style ->'attributes'->>'trim' as chr_trim,
  t.displacement->>'$value' as engine_displacement, a.color
from (
  select *
  from nc.stg_Availability) a
join chr.describe_vehicle c on a.vin = c.vin
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->'attributes'->>'unit' = 'liters'  
where c.style_count = 1
  and r.style ->'attributes'->>'modelYear' in ('2018','2019')
  and (r.style ->'model'->>'$value'::citext)::citext not like ('Silverado 1%')
  and (r.style ->'model'->>'$value'::citext)::citext not like ('Equin%')
union
-- the multiple chrome_style_vins
select a.vin, (r.style ->'attributes'->>'id')::integer as chr_style_id,
  (r.style ->'attributes'->>'modelYear')::citext as chr_model_year,
  (r.style ->'division'->>'$value')::citext as chr_make,
  (r.style ->'model'->>'$value'::citext)::citext as chr_model,
  (r.style ->'attributes'->>'mfrModelCode')::citext as chr_model_code,
  case
    when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'::citext
  end as chr_drive,
  case
    when u.cab->>'$value' like 'Crew%' then 'crew' 
    when u.cab->>'$value' like 'Extended%' then 'double'  
    when u.cab->>'$value' like 'Regular%' then 'reg'  
    else 'N/A'   
  end as chr_cab,
  r.style ->'attributes'->>'trim' as chr_trim,
  t.displacement->>'$value' as engine_displacement, a.color
from (
  select *
  from nc.stg_Availability) a
join chr.describe_vehicle c on a.vin = c.vin
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->'attributes'->>'unit' = 'liters'  
where c.style_count > 1
  and r.style ->'attributes'->>'modelYear' in ('2018','2019')
  and (r.style ->'model'->>'$value'::citext)::citext not like ('Silverado 1%')
  and (r.style ->'model'->>'$value'::citext)::citext not like ('Equin%')
  and a.model_code = (r.style ->'attributes'->>'mfrModelCode')::citext;



looks awfully good
select *
from nc.stg_vehicles
where drive = 'XXX'

select *
from nc.stg_vehicles a
join nc.vehicles b on a.vin = b.vin

-- only 4 without trim and they look legitimate
select *
from nc.stg_vehicles a
left join nc.stg_Availability b on a.vin = b.vin
where a.trim_level is null
order by a.make, a.model

select * from chr.describe_vehicle where vin = '2GT22NEG9K1136821'

)) 

(( -- bring equinox and silverado into the single script
-- equinox only dif is PREM vs Premier
-- engine and drive are good
select *
from (
  select a.*, 
    r.style ->'attributes'->>'trim' as chr_trim,
    (r.style ->'attributes'->>'modelYear')::citext as chr_model_year,
    (r.style ->'division'->>'$value')::citext as chr_make,
    (r.style ->'model'->>'$value'::citext)::citext as chr_model,
    (r.style ->'attributes'->>'mfrModelCode')::citext as chr_model_code,
    case
      when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
      when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
      when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
      when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
      else 'XXX'::citext
    end as chr_drive,
    t.displacement->>'$value' as chr_eng
  from nc.vehicles a
  join chr.describe_vehicle c on a.vin = c.vin
  join jsonb_array_elements(c.response->'style') as r(style) on true
  left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
  left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
    and t.displacement ->'attributes'->>'unit' = 'liters'    
  where a.model = 'equinox') x
-- where trim_level <> chr_trim
-- where drive <> chr_drive
where engine <> chr_eng



-- silverados lots of issues
biggest deal diff between 1LT and 2LT, and between 1LTZ and 2LTZ
WT vs work truck
2WD vs RWD
-- engine and drive are good
select *
from (
  select a.*, 
    r.style ->'attributes'->>'trim' as chr_trim,
    (r.style ->'attributes'->>'modelYear')::citext as chr_model_year,
    (r.style ->'division'->>'$value')::citext as chr_make,
    (r.style ->'model'->>'$value'::citext)::citext as chr_model,
    (r.style ->'attributes'->>'mfrModelCode')::citext as chr_model_code,
    case
      when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
      when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
      when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
      when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
      else 'XXX'::citext
    end as chr_drive,
    t.displacement->>'$value' as chr_eng
  from nc.vehicles a
  join chr.describe_vehicle c on a.vin = c.vin
  join jsonb_array_elements(c.response->'style') as r(style) on true
  left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
  left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
    and t.displacement ->'attributes'->>'unit' = 'liters'    
  where a.model like 'silverado 1%') x
-- where trim_level <> chr_trim
-- where drive <> chr_drive
where engine <> chr_eng
order by trim_level

))

insert into nc.vehicles(vin,chrome_style_id,model_year,make,model,
  model_code,drive,cab,trim_level,engine,color)
select vin,chrome_style_id,model_year,make,model,
  model_code,drive,cab,trim_level,engine,color
from nc.stg_vehicles a
where not exists (
  select 1
  from nc.vehicles 
  where vin = a.vin);

--------------------------------------------------------------------------------------------------
--/> nc.vehicles
--------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------
--< nc.vehicle_acquisitions
--------------------------------------------------------------------------------------------------
insert into nc.vehicle_acquisitions  (vin,stock_number,ground_date,source,in_transit_sale)
select a.vin, a.stock_number, a.ground_date, a.source, false
from nc.stg_availability a
left join nc.vehicle_acquisitions b on a.stock_number = b.stock_number
  and a.ground_date = b.ground_date
where b.vin is null  
  and a.source <> 'unwind';

-- and finally, the unwind acquisition
-- not sure how this will work for multiple unwinds, only want to update the 
-- must recent acquisition record 
update nc.vehicle_acquisitions x
set thru_date = y.the_date
from (
  select a.vin, a.stock_number, a.ground_date - 1 as the_date
  from nc.stg_availability a
  where source = 'unwind') y
where x.stock_number = y.stock_number;

insert into nc.vehicle_acquisitions  (vin,stock_number,ground_date,source,in_transit_sale)
select a.vin, a.stock_number, a.ground_date, a.source, false
from nc.stg_availability a
where source = 'unwind';   

--------------------------------------------------------------------------------------------------
--/> nc.vehicle_acquisitions
--------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------
--< SALES
--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
--< nc.stg_sales
--------------------------------------------------------------------------------------------------
-- accounting should include all sales
-- first step, accounting info and vin from inpmast
truncate nc.stg_sales;
insert into nc.stg_sales (stock_number, vin, trans_description, amount, acct_date,ground_date)
select a.control, f.inpmast_vin, e.description, a.amount, b.the_date, g.ground_date
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
--   and b.the_date between current_Date - 40 and current_date
join fin.dim_account c on a.account_key = c.account_key
  and c.account in ( -- all new vehicle inventory account
    select b.account
    from fin.dim_account b 
    where b.account_type = 'asset'
      and b.department_code = 'nc'
      and b.typical_balance = 'debit'
      and b.current_row
      and b.account <> '126104')    
join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code = 'VSN'
join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
left join arkona.xfm_inpmast f on a.control = f.inpmast_stock_number
  and f.current_row = true  
left join nc.vehicle_acquisitions g on a.control = g.stock_number
  and g.thru_date = ( -- need most recent acquisition
    select max(thru_date)
    from nc.vehicle_acquisitions
    where stock_number = g.stock_number)  
where a.post_status = 'Y'
  and abs(a.amount) > 10000
  and f.year in ('2018', '2019')
  and f.model not like 'equ%'
  and f.model not like 'silverado 1%'
  and not exists (
    select 1
    from nc.vehicle_sales
    where stock_number = a.control
      and ground_date = g.ground_date) ;



do 
$$
begin
assert ( -- ground date null
  select count(*)
--   select *
  from nc.stg_sales a
  where a.ground_date is null) = 0, 'NULL ground date';
end
$$;

( -- initial null ground dates 
holy shit 25 with null ground dates

-- case 1 missing acquisition for retail of ctp unit
select * from nc.vehicle_acquisitions where vin = '1G1ZD5ST2JF156080'
select * from nc.vehicles where vin = '1G1ZD5ST2JF156080'
select * from nc.vehicle_sales where vin = '1G1ZD5ST2JF156080'
select * from sls.ext_bopmast_partial where vin = '1G1ZD5ST2JF156080';
insert into nc.vehicle_acquisitions values ('1G1ZD5ST2JF156080', '32438R', '05/21/2018', '07/17/2018', 'ctp', false);
insert into nc.vehicle_sales values ('32438R', '05/21/2018','1G1ZD5ST2JF156080', 49310, '07/17/2018', 'retail', false) ; 

select * from sls.deals where vin = '1N4AL3APXJC477291'

-- factory acquisition dealer traded, no record in nc.vehicles
H11618  1N6AA1RP6JN507638

select * from nc.vehicles where vin = '1N6AA1RP6JN507638'

do
$$
declare
  _stock_number citext := 'H11618';
begin  
-- nc.vehicles 
  insert into nc.vehicles(vin,chrome_style_id,model_year,make,model,
    model_code,drive,cab,trim_level,engine,color)
  select a.vin, (r.style ->'attributes'->>'id')::integer as chr_style_id,
    (r.style ->'attributes'->>'modelYear')::citext as chr_model_year,
    (r.style ->'division'->>'$value')::citext as chr_make,
    (r.style ->'model'->>'$value'::citext)::citext as chr_model,
    (r.style ->'attributes'->>'mfrModelCode')::citext as chr_model_code,
    case
      when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
      when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
      when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
      when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
      else 'XXX'
    end as chr_drive,
    case
      when u.cab->>'$value' like 'Crew%' then 'crew' 
      when u.cab->>'$value' like 'Extended%' then 'double'  
      when u.cab->>'$value' like 'Regular%' then 'reg'  
      else 'N/A'   
    end as chr_cab,
    r.style ->'attributes'->>'trim' as chr_trim,
    t.displacement->>'$value' as engine_displacement, d.color
  from nc.stg_sales a
  join chr.describe_vehicle c on a.vin = c.vin
  join jsonb_array_elements(c.response->'style') as r(style) on true
  left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
    and u.ordinality =  2
  left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
  left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
    and t.displacement ->'attributes'->>'unit' = 'liters'  
  left join arkona.xfm_inpmast d on a.stock_number = d.inpmast_stock_number
    and d.current_row   
    where a.stock_number = _stock_number;

  -- now need the acquisition
  -- ground date = sale date = thru_Date
  insert into nc.vehicle_acquisitions(vin,stock_number,ground_date,thru_date,source,in_transit_sale)
  select vin, stock_number, '09/30/2016', acct_date, 'factory', false
  from nc.stg_sales
  where stock_number = _stock_number;

-- update the ground date in nc.stg_sales
-- select * from nc.stg_sales
  update nc.stg_sales
  set ground_date = acct_date
  where stock_number = _stock_number;
  
end  
$$;

)


-- bopmast_id
update nc.stg_sales x
set bopmast_id = y.bopmast_id,
    delivery_date = y.delivery_date
from (    
  select b.*
  from nc.stg_sales a
  left join (
    select stock_number, bopmast_id, max(seq) as seq, max(delivery_date) as delivery_date
    from sls.deals
    group by stock_number, bopmast_id) b on a.stock_number = b.stock_number) y
where x.stock_number = y.stock_number;

-- sale_types dealer trade & ctp
update nc.stg_sales x
set sale_type =
  case
    when bopmast_id is null then 'dealer trade'
    when trans_description like '%RYDELL%' then 'ctp'
  end;

   -- sale_type fleet
 update nc.stg_sales x
set sale_type = 'fleet'
where stock_number in (
  select a.stock_number
  from nc.stg_sales a
  inner join (
    select a.control, b.the_date
    from fin.fact_gl a
    inner join dds.dim_date b on a.date_key = b.date_key
    inner join fin.dim_account c on a.account_key = c.account_key
    inner join (
      select distinct d.gl_account
      from fin.fact_fs a 
      inner join fin.dim_fs b on a.fs_key = b.fs_key
        and b.year_month > 201601
        and b.page in (5,8,9,10)
        and b.line in (22,43)
        and b.col = 1 -- sales
      inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
--         and c.store = 'ry1'
      inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key) d on c.account = d.gl_account
    where a.post_status = 'Y') b on a.stock_number = b.control);

-- dealer trade delivery dates
update nc.stg_sales
set delivery_date = acct_date
where sale_type = 'dealer trade';

-- and finally, everything else is retail
update nc.stg_sales
set sale_type = 'retail'
where sale_type is null;   

select * 
from nc.stg_sales

-- -- one outlier
-- update nc.stg_sales set sale_type = 'retail' where stock_number = 'H11618';

-- test for ground date > delivery date
do
$$
begin
assert (
  select count(*)
  -- select *
    from nc.stg_sales
    where ground_date > delivery_date) = 0, 'ground date > delivery date';
end
$$;    

-- -- 2 anomalies, just change the ground date
-- -- first in the acquisition
-- update nc.vehicle_acquisitions
-- set ground_date = '12/11/2018'
-- where stock_number in ('G35903','G35895');
-- 
-- update nc.stg_sales
-- set ground_date = '12/11/2018'
-- where stock_number in ('G35903','G35895');

-- update nc.vehicle_acquisitions.thru_date for sales
update nc.vehicle_acquisitions x
set thru_date  = y.delivery_date
from (
  select a.*
  from nc.stg_sales a
  left join nc.vehicle_acquisitions b on a.stock_number = b.stock_number) y
where x.stock_number = y.stock_number
  and x.thru_date = '12/31/9999';

-- and install the sales
insert into nc.vehicle_sales
select stock_number, ground_date, vin, coalesce(bopmast_id, -1), delivery_date, sale_type
from nc.stg_sales;  

--------------------------------------------------------------------------------------------------
--/> SALES
--------------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------
--/> nightly
--------------------------------------------------------------------------------------------



--------------------------------------------------------------------------------------------
-- < TODO shit that has to be handled
--------------------------------------------------------------------------------------------
buick regal 4dr sdn vs 5dr wgn - change cab to body?, or cab/body


--------------------------------------------------------------------------------------------
--/> TODO shit that has to be handled
--------------------------------------------------------------------------------------------



-- compares descriptions from inpmast, chrome, dataone
/*

1GC1KTEY3KF136380: 2019 silverado, dataone has 2 styles:
    1. 4x4 LTZ 4dr Crew Cab SB
    2. 4x4 Work Truck 4dr Crew Cab LB
  find no other reference to the SB vs LB

  
select * from dao.vin_reference where vin_pattern = '1GC1KTEYKF'  
  looks like the difference is wheelbase and that, i believe translates to bed size (SB: standard, LB: long ??)
  and that is reflected in the model code, so, does invoice or chrome have wheelbase, 
  select * from gmgl.vehicle_invoices where vin = '1GC1KTEY3KF136380'
  
*/
-- 11/22/18 refactored jsob_array_elements out of the select clause
-- added join chrome to dataone model code:and r.style ->'attributes'->>'mfrModelCode' = d.mfr_model_num
drop table if exists compare cascade;
create temp table compare as
select a.inpmast_vin, a.inpmast_stock_number, a.year, a.make, a.model, a.body_style, a.model_code,
  b.invoice, 
    r.style ->'attributes'->>'id' as chr_style_id,
    r.style ->'attributes'->>'mfrModelCode' as chr_model_code,
    r.style ->'attributes'->>'modelYear' as chr_model_year,
    r.style ->'attributes'->>'name' as chr_name,
    r.style ->'attributes'->>'drivetrain' as drive_train,
    t.displacement->>'$value' as engine_displacement,
--     c.response->'attributes'->>'bestStyleName' as best_style_name,
    c.response->'attributes'->>'bestTrimName' as best_trim_name,
    c.response->'attributes'->>'bestModelName' as best_model_name,
    d.model as dao_model, d.trim, d.style as dao_style, d.drive_type, d.engine_size,
    e.make as dao_make--, e.trim, e.style
-- select a.inpmast_vin      
from arkona.xfm_inpmast a
left join (
  select vin, left(raw_invoice, position('GENERAL' in raw_invoice) - 1) as invoice
  from gmgl.vehicle_invoices) b on a.inpmast_vin = b.vin
left join chr.describe_vehicle c on a.inpmast_vin = c.vin  
left join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(c.response->'engine') as s(engine) on true
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->'attributes'->>'unit' = 'liters'
left join dao.vin_reference d on left(inpmast_vin, 8) || substring(inpmast_vin, 10, 1) || substring(inpmast_vin, 11, 1) = d.vin_pattern
  and r.style ->'attributes'->>'mfrModelCode' = d.mfr_model_num
left join dao.veh_trim_styles e on d.vehicle_id = e.vehicle_id  
where a.current_row
  and a.status = 'I'
  and a.type_n_u = 'N'
--   and inpmast_vin = '1GCRYEED3KZ190500'
order by a.inpmast_vin;

-- multiple versions of vin
select *
from compare a
join (
  select inpmast_vin
  from compare
  group by inpmast_vin
  having count(*) > 1) b on a.inpmast_vin = b.inpmast_vin



----------------------------------------------------------------------------------------------------------------------
-- 11/23/18 trying to sort out how i am going to come up with the relevant trim levels for all makes and models
--             like what i have done with silverados and equinoxen
-----------------------------------------------------------------------------------------------------------------------


-- from inpmast, all 2018/2019 make/model/body_style/color
select b.year, b.make, b.model, b.body_style, b.model_code, min(b.inpmast_vin), max(b.inpmast_vin), count(*)--, b.color_code, b.color
from sls.ext_bopmast_partial a
join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
  and b.year > 2017
where a.vehicle_type = 'N'
group by b.year, b.make, b.model, b.body_style, b.model_code--, b.color_code, b.color
order by count(*) desc


select aa.*, bb.style_count, cc.style_count
from (
  select b.year, b.make, b.model, b.body_style, b.model_code, min(b.inpmast_vin) as min_vin, max(b.inpmast_vin) as max_vin, count(*)
  from sls.ext_bopmast_partial a
  join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
    and b.current_row
    and b.year > 2017
  where a.vehicle_type = 'N'
  group by b.year, b.make, b.model, b.body_style, b.model_code) aa
left join chr.describe_vehicle bb on aa.min_vin = bb.vin
left join chr.describe_vehicle cc on aa.max_vin = cc.vin

------------------------------------------------------------------------------------------
-- leave color out for now, what is the comprehensive list of make/model/trims
-- in silverados and equinoxen, used chrome r.style ->'attributes'->>'name'
-- to separate trims
------------------------------------------------------------------------------------------
select a.vin, c.style_count,
    r.style ->'attributes'->>'id' as chr_style_id,
    r.style ->'attributes'->>'mfrModelCode' as chr_model_code,
    r.style ->'attributes'->>'modelYear' as chr_model_year,
    r.style ->'attributes'->>'name' as chr_name,
    r.style ->'attributes'->>'drivetrain' as drive_train,
    t.displacement->>'$value' as engine_displacement,
--     c.response->'attributes'->>'bestStyleName' as best_style_name, --r.style ->'attributes'->>'name' is better
    c.response->'attributes'->>'bestTrimName' as best_trim_name, -- may be useful
    c.response->'attributes'->>'bestModelName' as best_model_name,
    c.response->'attributes'->>'bestMakeName' as make
--     c.response->'vinDescription'->'attributes'->>'styleName' as style_name, -- too general
--     c.response->'vinDescription'->'attributes'->>'drivingWheels' as driving_wheels -- does not include fwd cars   
from sls.ext_bopmast_partial a
join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
  and b.year > 2017
left join chr.describe_vehicle c on a.vin = c.vin  
left join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(c.response->'engine') as s(engine) on true
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->'attributes'->>'unit' = 'liters'
where a.vehicle_type = 'N'
  and c.style_count = 1
order by chr_style_id
limit 1000

select * from chr.describe_Vehicle where vin = '3GTU2MEC3JG182909'

-- c.response->'attributes'->>'bestModelName' always the same as r.style ->'model'->>'$value'
-- c.response->'attributes'->>'bestStyleName' vs r.style ->'attributes'->>'name':  r.style ->'attributes'->>'name' is more precise
-- c.response->'attributes'->>'bestMakeName' always the same as c.response->'attributes'->>'bestMakeName'
select *
from (
  select vin,
    c.response->'vinDescription'->'attributes'->>'modelName' as modelname,
    r.style ->'model'->>'$value' as model
  from chr.describe_vehicle c
  left join jsonb_array_elements(c.response->'style') as r(style) on true) aa
where modelname <> model


-- vins based on sales, 1 chrome style per vin
drop table if exists chrome_sales cascade;
create temp table chrome_sales as
select chr_style_id,chr_model_code,chr_model_year,chr_name,drive_train,engine_displacement,
  best_Trim_name, best_model_name, make
from (  
  select a.vin, c.style_count,
      r.style ->'attributes'->>'id' as chr_style_id,
      r.style ->'attributes'->>'mfrModelCode' as chr_model_code,
      r.style ->'attributes'->>'modelYear' as chr_model_year,
      r.style ->'attributes'->>'name' as chr_name,
      r.style ->'attributes'->>'drivetrain' as drive_train,
      t.displacement->>'$value' as engine_displacement,
      c.response->'attributes'->>'bestTrimName' as best_trim_name, -- may be useful
      c.response->'attributes'->>'bestModelName' as best_model_name,
      c.response->'attributes'->>'bestMakeName' as make
  from sls.ext_bopmast_partial a
  join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
    and b.current_row
    and b.year > 2017
  left join chr.describe_vehicle c on a.vin = c.vin  
  left join jsonb_array_elements(c.response->'style') as r(style) on true
  left join jsonb_array_elements(c.response->'engine') as s(engine) on true
  left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
    and t.displacement ->'attributes'->>'unit' = 'liters'
  where a.vehicle_type = 'N'
    and c.style_count = 1) x
group by chr_style_id,chr_model_code,chr_model_year,chr_name,drive_train,engine_displacement,
  best_Trim_name, best_model_name, make;
create unique index on chrome_sales(chr_style_id, engine_displacement, chr_name);


select *
from chrome_sales limit 10


drop table if exists chrome_inv cascade;
create temp table chrome_inv as
select chr_style_id,chr_model_code,chr_model_year,chr_name,drive_train,engine_displacement,
  best_Trim_name, best_model_name, make  -- , max(bed) as bed, min(inpmast_vin), max(inpmast_vin), count(*)
from (  
  select b.inpmast_vin, c.style_count,
      r.style ->'attributes'->>'id' as chr_style_id,
      r.style ->'attributes'->>'mfrModelCode' as chr_model_code,
      r.style ->'attributes'->>'modelYear' as chr_model_year,
      r.style ->'attributes'->>'name' as chr_name,
      r.style ->'attributes'->>'drivetrain' as drive_train,
      t.displacement->>'$value' as engine_displacement,
      c.response->'attributes'->>'bestTrimName' as best_trim_name, -- may be useful
      c.response->'attributes'->>'bestModelName' as best_model_name,
      c.response->'attributes'->>'bestMakeName' as make,
      u.body_type->>'$value' as bed
  from arkona.xfm_inpmast b 
  left join chr.describe_vehicle c on b.inpmast_vin = c.vin  
  left join jsonb_array_elements(c.response->'style') as r(style) on true
  left join jsonb_array_elements(c.response->'engine') as s(engine) on true
  left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
    and t.displacement ->'attributes'->>'unit' = 'liters'
  left join jsonb_array_elements(r.style->'bodyType') as u(body_type) on true
    and lower(u.body_type->>'$value') like '%bed%'    
  where b.current_row
    and b.status = 'I'
    and b.type_n_u = 'N'
    and c.style_count = 1) x
group by chr_style_id,chr_model_code,chr_model_year,chr_name,drive_train,engine_displacement,
  best_Trim_name, best_model_name, make;
create unique index on chrome_inv(chr_style_id, engine_displacement, chr_name);

-- lets do orders too
-- this gives me orders with vins, but not those without vins
drop table if exists chrome_orders cascade;
create temp table chrome_orders as
with
  order_vins as (
    select vin
    from (
      select b.vin, 
        b.order_number, b.order_type, b.alloc_group, b.model_code, b.vehicle_trim, b.dan, b.peg, b.color, 
        c.event_code,
        c.from_Date, c.thru_date, d.*
      from gmgl.vehicle_orders b 
      left join gmgl.vehicle_order_events c on b.order_number = c.order_number
        and c.thru_date = (
          select max(thru_date)
          from gmgl.vehicle_order_events
          where order_number = c.order_number)
      left join gmgl.vehicle_event_codes d on c.event_code = d.code) e
    where left(event_code, 1)::integer < 5
      and vin is not null)
select chr_style_id,chr_model_code,chr_model_year,chr_name,drive_train,engine_displacement,
  best_Trim_name, best_model_name, make  -- , max(bed) as bed, min(inpmast_vin), max(inpmast_vin), count(*)
from (  
  select b.vin, c.style_count,
      r.style ->'attributes'->>'id' as chr_style_id,
      r.style ->'attributes'->>'mfrModelCode' as chr_model_code,
      r.style ->'attributes'->>'modelYear' as chr_model_year,
      r.style ->'attributes'->>'name' as chr_name,
      r.style ->'attributes'->>'drivetrain' as drive_train,
      t.displacement->>'$value' as engine_displacement,
      c.response->'attributes'->>'bestTrimName' as best_trim_name, -- may be useful
      c.response->'attributes'->>'bestModelName' as best_model_name,
      c.response->'attributes'->>'bestMakeName' as make,
      u.body_type->>'$value' as bed
  from order_vins b 
  left join chr.describe_vehicle c on b.vin = c.vin  
  left join jsonb_array_elements(c.response->'style') as r(style) on true
  left join jsonb_array_elements(c.response->'engine') as s(engine) on true
  left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
    and t.displacement ->'attributes'->>'unit' = 'liters'
  left join jsonb_array_elements(r.style->'bodyType') as u(body_type) on true
    and lower(u.body_type->>'$value') like '%bed%'    
  where c.style_count = 1) x
group by chr_style_id,chr_model_code,chr_model_year,chr_name,drive_train,engine_displacement,
  best_Trim_name, best_model_name, make;
create unique index on chrome_inv(chr_style_id, engine_displacement, chr_name);

-- so this gives me 364 distinct configurations
drop table if exists configs;
create temp table configs as 
select 'inv' as source, a.* from chrome_inv a where chr_model_year::integer > 2017
union
select 'sales' as source, a.* from chrome_sales a where chr_model_year::integer > 2017
union
select 'orders' as source, a.* from chrome_orders a where chr_model_year::integer > 2017;


select source, chr_model_year, make, best_model_name, string_agg(distinct chr_name, ',')
from configs
where chr_model_year = '2018'
group by source, chr_model_year, make, best_model_name
union
select source, chr_model_year, make, best_model_name, string_agg(distinct chr_name, ',')
from configs
where chr_model_year = '2019'
group by source, chr_model_year, make, best_model_name
order by make, best_model_name, chr_model_year

select * from configs

-- break out the trims
-- source isn't very interesting
-- but model_code would be good
select chr_model_year, make, best_model_name, chr_name, chr_model_code
from configs
where chr_model_year = '2018'
group by chr_model_year, make, best_model_name, chr_name, chr_model_code
union
select chr_model_year, make, best_model_name, chr_name, chr_model_code
from configs
where chr_model_year = '2019'
group by chr_model_year, make, best_model_name, chr_name, chr_model_code
order by make, best_model_name, chr_name, chr_model_year

way short on 2019s, eg, no 2019 canyon
lets look at dataone

select year, make, model, trim, drive_type, style
from dao.veh_trim_styles -- doesn't have engine
where year > 2017
  and make in ('honda','nissan','gmc','chevrolet','buick','cadillac')
group by year, make, model, trim, drive_type, style  
order by make, model, trim, drive_type, style, year



select *
from chrome_sales a
where chr_model_year in ('2018','2019')


select *
from (
  select chr_model_year, make, best_model_name, chr_name, chr_model_code
  from configs
  where chr_model_year = '2018'
  group by chr_model_year, make, best_model_name, chr_name, chr_model_code
  union
  select chr_model_year, make, best_model_name, chr_name, chr_model_code
  from configs
  where chr_model_year = '2019'
  group by chr_model_year, make, best_model_name, chr_name, chr_model_code
  order by make, best_model_name, chr_name, chr_model_year) a
where best_model_name in ('Silverado 1500','Silverado 1500 LD')
  and chr_model_year = '2019'

--------------------------------------------------------------------------------------------------------
--< bed and cab json problem
--------------------------------------------------------------------------------------------------------
-- how to separate them without hard coding a like in the join
/*
json object looks like
object {10}
    style [1]
        bodyType [2]
            0 {2}
                $value: Standard Bed
            1 {2}
                $value: Extended Cab Pickup

-- here is the solution

select
   u.body_type->>'$value' as bed,
   v.body_type->>'$value' as cab
from chr.describe_vehicle  c
left join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(body_type) on true
 and u.ordinality = 1
left join jsonb_array_elements(r.style->'bodyType') with ordinality as v(body_type) on true
 and v.ordinality = 2
where vin = '1GCRYDED5KZ188693'
*/
--------------------------------------------------------------------------------------------------------
--/> bed and cab json problem
--------------------------------------------------------------------------------------------------------

-- i am currently at fuck data one, it is of no use at least as far as the data
do an exhaustive listing from chrome based on 2018/2019 honda nissan chev gmc buick cadillac
dont have fucking chrome data, maybe data one

select * 
from dao.veh_trim_styles 
where year > 2017
  and make in ('honda','nissan','gmc','chevrolet','buick','cadillac')