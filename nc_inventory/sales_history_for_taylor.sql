﻿/*
12/22/20
Hey Jon,

Would we be able to get this same data for GM lines like you did for Honda and Nissan? There would be no hurry on this. 

Could we see retail broken out in same or similar fashion as you did the HN sales but also have a total fleet number 
for each model next to the retail total? Trim would not be necessary I don’t think for fleet but 
I’d like to see the overall volume.

Taylor Monson
*/
-- copy what i did in .../honda_sales/sales_history_for_taylor.sql
-- saved as .../nc_inventory/sales_history_for_taylor.sql
-- add fleet, change makes, lets try adding dealer trade sales as well
drop table if exists tem.taylor_gm_sale_history cascade;
create table tem.taylor_gm_sale_history as
select c.year_month, a.stock_number, a.vin, a.delivery_date, a.ground_date, b.model_year, 
  b.make, b.model, b.model_code, b.trim_level, b.drive, b.engine, b.color,
  a.delivery_date - ground_date as turn, d.list_price as msrp, a.sale_type
from nc.vehicle_sales a 
join nc.vehicles b on a.vin = b.vin
join dds.dim_date c on a.delivery_date = c.the_date
  and c.the_year > 2017
left join arkona.ext_inpmast d on a.vin = d.inpmast_vin  
where a.sale_type in ( 'retail','fleet', 'dealer trade')
  and b.make not in ('honda','nissan')
order by delivery_date;


-- drop table if exists tem.taylor_gm_models cascade;
-- create table tem.taylor_gm_models as
-- select extract(year from delivery_date) as sale_year, make, model, count(*) as model_count
-- from tem.taylor_gm_sale_history
-- -- where model not in ('fit','titan xd')
-- group by extract(year from delivery_date), make, model
-- having count(*) > 5
-- order by make, model, extract(year from delivery_date);

-- separate counts for retail/fleet/dt
-- this appears to work ok
-- exclude the sales count limit, there are no 0's but several 1's might be interesting if we ever get to including inventory
drop table if exists tem.taylor_gm_models cascade;
create table tem.taylor_gm_models as
select extract(year from delivery_date) as sale_year, make, model, 
  count(*) filter (where sale_type = 'retail') as retail_model_count,
  count(*) filter (where sale_type = 'fleet') as fleet_model_count,
  count(*) filter (where sale_type = 'dealer trade') as dt_model_count,
  count(*) as model_count
from tem.taylor_gm_sale_history
group by extract(year from delivery_date), make, model
-- having count(*) > 1
order by sale_year, make, model, extract(year from delivery_date);



-- this is the query to generate the original output, 
-- big caviat: trim_count is retail sales only
-- this puts it all together for a given sale_year
-- 2018
select aa.*, coalesce(bb."FWD/AWD", '="' || '0 / 0' || '"')
from (
  select a.sale_year, a.make, a.model, (coalesce(a.retail_model_count, 0))::text ||'/'|| (coalesce(a.fleet_model_count, 0)):: text ||'/'|| (coalesce(a.dt_model_count, 0))::text as "ret/flt/dt", 
    b.trim_level, b.trim_count
  from tem.taylor_gm_models a
  left join (
    select extract(year from delivery_date) as sale_year, make, model, trim_level, count(*) as trim_count
    from tem.taylor_gm_sale_history
    where sale_type = 'retail'
    group by extract(year from delivery_date), make, model, trim_level) b on a.sale_year = b.sale_year
      and a.make = b.make
      and a.model = b.model
  where a.sale_year = 2018    
  order by a.sale_year, a.make, a.model, b.trim_count desc) aa
left join (
  select extract(year from delivery_date) as sale_year, make, model, trim_level,
  '="' || coalesce((count(*) filter (where drive = 'FWD'))::text, '0') || ' / ' ||  coalesce((count(*) filter (where drive in ('4WD','AWD')))::text, '0') || '"' as "FWD/AWD"
  from tem.taylor_gm_sale_history
  where sale_type = 'retail'
  group by extract(year from delivery_date), make, model, trim_level) bb on aa.sale_year = bb.sale_year
    and aa.make = bb.make and aa.model = bb.model and aa.trim_level = bb.trim_level



--< before putting it all together ------------------------------------------------------------ 
select *
from ( -- 2018
  select a.*, b.trim_level, b.trim_count
  from tem.taylor_gm_models a
  left join (
    select extract(year from delivery_date) as sale_year, make, model, trim_level, count(*) as trim_count
    from tem.taylor_gm_sale_history
    where sale_type = 'retail'
    group by extract(year from delivery_date), make, model, trim_level) b on a.sale_year = b.sale_year
      and a.make = b.make
      and a.model = b.model
  where a.sale_year = 2018    
  order by a.sale_year, a.make, a.model, b.trim_count desc) aa
full outer join ( -- 2019
  select a.*, b.trim_level, b.trim_count
  from tem.taylor_gm_models a
  left join (
    select extract(year from delivery_date) as sale_year, make, model, trim_level, count(*) as trim_count
    from tem.taylor_gm_sale_history
    group by extract(year from delivery_date), make, model, trim_level) b on a.sale_year = b.sale_year
      and a.make = b.make
      and a.model = b.model
  where a.sale_year = 2019   
  order by a.sale_year, a.make, a.model, b.trim_count desc) bb on aa.make = bb.make and aa.model = bb.model and aa.trim_level = bb.trim_level
full outer join ( -- 2020
  select a.*, b.trim_level, b.trim_count
  from tem.taylor_gm_models a
  left join (
    select extract(year from delivery_date) as sale_year, make, model, trim_level, count(*) as trim_count
    from tem.taylor_gm_sale_history
    group by extract(year from delivery_date), make, model, trim_level) b on a.sale_year = b.sale_year
      and a.make = b.make
      and a.model = b.model
  where a.sale_year = 2020   
  order by a.sale_year, a.make, a.model, b.trim_count desc) cc on coalesce(aa.make,bb.make) = cc.make
    and coalesce(aa.model, bb.model) = cc.model 
    and coalesce(aa.trim_level, bb.trim_level) = cc.trim_level 
order by coalesce(aa.make, bb.make, cc.make), coalesce(aa.model, bb.model, cc.model), coalesce(aa.trim_level, bb.trim_level, cc.trim_level)   

select extract(year from delivery_date) as sale_year, make, model, trim_level,
'="' || coalesce((count(*) filter (where drive = 'FWD'))::text, '0') || ' / ' ||  coalesce((count(*) filter (where drive in ('4WD','AWD')))::text, '0') || '"'
from tem.taylor_gm_sale_history
where sale_type = 'retail'
group by extract(year from delivery_date), make, model, trim_level
order by make, extract(year from delivery_date), model, trim_level

--/> before putting it all together ------------------------------------------------------------

-- the goofy concatenation in FWD/AWD and ret/flt/dt is so that excel doesn't convert it to a date
select *
from ( -- 2018
  select aa.*, "FWD/AWD" -- '="' || coalesce(bb."FWD/AWD", '="' || '0 / 0' || '"') as "FWD/AWD"
  from (
    select a.sale_year, a.make, a.model, '="' || (coalesce(a.retail_model_count, 0))::text ||'/'|| (coalesce(a.fleet_model_count, 0)):: text ||'/'|| (coalesce(a.dt_model_count, 0))::text || '"' as "ret/flt/dt", 
      b.trim_level, b.trim_count
    from tem.taylor_gm_models a
    left join (
      select extract(year from delivery_date) as sale_year, make, model, trim_level, count(*) as trim_count
      from tem.taylor_gm_sale_history
      where sale_type = 'retail'
      group by extract(year from delivery_date), make, model, trim_level) b on a.sale_year = b.sale_year
        and a.make = b.make
        and a.model = b.model
    where a.sale_year = 2018    
    order by a.sale_year, a.make, a.model, b.trim_count desc) aa
  left join (
    select extract(year from delivery_date) as sale_year, make, model, trim_level,
    '="' || coalesce((count(*) filter (where drive = 'FWD'))::text, '0') || ' / ' ||  coalesce((count(*) filter (where drive in ('4WD','AWD')))::text, '0') || '"' as "FWD/AWD"
    from tem.taylor_gm_sale_history
    where sale_type = 'retail'
    group by extract(year from delivery_date), make, model, trim_level) bb on aa.sale_year = bb.sale_year
      and aa.make = bb.make and aa.model = bb.model and aa.trim_level = bb.trim_level) aaa
full outer join ( -- 2019      
  select aa.*, coalesce(bb."FWD/AWD", '="' || '0 / 0' || '"') as "FWD/AWD"
  from (
    select a.sale_year, a.make, a.model, '="' || (coalesce(a.retail_model_count, 0))::text ||'/'|| (coalesce(a.fleet_model_count, 0)):: text ||'/'|| (coalesce(a.dt_model_count, 0))::text || '"' as "ret/flt/dt", 
      b.trim_level, b.trim_count
    from tem.taylor_gm_models a
    left join (
      select extract(year from delivery_date) as sale_year, make, model, trim_level, count(*) as trim_count
      from tem.taylor_gm_sale_history
      where sale_type = 'retail'
      group by extract(year from delivery_date), make, model, trim_level) b on a.sale_year = b.sale_year
        and a.make = b.make
        and a.model = b.model
    where a.sale_year = 2019    
    order by a.sale_year, a.make, a.model, b.trim_count desc) aa
  left join (
    select extract(year from delivery_date) as sale_year, make, model, trim_level,
    '="' || coalesce((count(*) filter (where drive = 'FWD'))::text, '0') || ' / ' ||  coalesce((count(*) filter (where drive in ('4WD','AWD')))::text, '0') || '"' as "FWD/AWD"
    from tem.taylor_gm_sale_history
    where sale_type = 'retail'
    group by extract(year from delivery_date), make, model, trim_level) bb on aa.sale_year = bb.sale_year
      and aa.make = bb.make and aa.model = bb.model and aa.trim_level = bb.trim_level) bbb on aaa.make = bbb.make and aaa.model = bbb.model and aaa.trim_level = bbb.trim_level   
full outer join ( -- 2020   
  select aa.*, coalesce(bb."FWD/AWD", '="' || '0 / 0' || '"') as "FWD/AWD"
  from (
    select a.sale_year, a.make, a.model, '="' || (coalesce(a.retail_model_count, 0))::text ||'/'|| (coalesce(a.fleet_model_count, 0)):: text ||'/'|| (coalesce(a.dt_model_count, 0))::text || '"' as "ret/flt/dt", 
      b.trim_level, b.trim_count
    from tem.taylor_gm_models a
    left join (
      select extract(year from delivery_date) as sale_year, make, model, trim_level, count(*) as trim_count
      from tem.taylor_gm_sale_history
      where sale_type = 'retail'
      group by extract(year from delivery_date), make, model, trim_level) b on a.sale_year = b.sale_year
        and a.make = b.make
        and a.model = b.model
    where a.sale_year = 2020    
    order by a.sale_year, a.make, a.model, b.trim_count desc) aa
  left join (
    select extract(year from delivery_date) as sale_year, make, model, trim_level,
    '="' || coalesce((count(*) filter (where drive = 'FWD'))::text, '0') || ' / ' ||  coalesce((count(*) filter (where drive in ('4WD','AWD')))::text, '0') || '"' as "FWD/AWD"
    from tem.taylor_gm_sale_history
    where sale_type = 'retail'
    group by extract(year from delivery_date), make, model, trim_level) bb on aa.sale_year = bb.sale_year
      and aa.make = bb.make and aa.model = bb.model and aa.trim_level = bb.trim_level) ccc on coalesce(aaa.make,bbb.make) = ccc.make
    and coalesce(aaa.model, bbb.model) = ccc.model 
    and coalesce(aaa.trim_level, bbb.trim_level) = ccc.trim_level 
order by coalesce(aaa.make, bbb.make, ccc.make), coalesce(aaa.model, bbb.model, ccc.model), coalesce(aaa.trim_level, bbb.trim_level, ccc.trim_level)  