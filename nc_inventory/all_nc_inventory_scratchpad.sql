﻿--------------------------------------------------------------------------------------------
--< nightly in style of equinoxen
--------------------------------------------------------------------------------------------

truncate nc.stg_availability;
insert into nc.stg_availability (the_date,journal_date,stock_number,
  vin,model_year,make, model,
  model_code,color_code,color, source)
select current_date, f.the_date, f.control, 
  g.inpmast_vin, g.year, g.make, g.model, g.model_code, 
  g.color_code, g.color,
  case f.journal_code
    when 'PVI' then 'factory'
    when 'CDH' then 'dealer trade'
    when 'GJE' then 'ctp'
  end as source
from ( -- accounting journal
  select a.control, d.journal_code, max(b.the_date) as the_date, sum(a.amount) as amount
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_date between current_Date - 90 and current_date--'06/22/2018'
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account in ( -- all new vehicle inventory account
      select b.account
      from fin.dim_account b 
      where b.account_type = 'asset'
        and b.department_code = 'nc'
        and b.typical_balance = 'debit'
        and b.current_row
        and b.account <> '126104')   
  inner join fin.dim_journal d on a.journal_key = d.journal_key
    and d.journal_code in ('PVI','CDH','GJE')
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where a.post_status = 'Y'
  group by a.control, d.journal_code
  having sum(a.amount) > 10000) f
inner join arkona.xfm_inpmast g on f.control = g.inpmast_stock_number
  and g.model not like 'eq%' 
  and g.model not like 'silverado 1%'
  and g.current_row = true
  and g.year in ('2018', '2019')
where not exists (
  select 1
  from nc.vehicle_acquisitions
  where stock_number = f.control) ;

select * from nc.stg_availability order by source

select a.*, regexp_replace(b.delivery_store, '\n', '', 'g')
  from nc.stg_availability a
  join gmgl.vehicle_vis b on a.vin = b.vin
where stock_number in ('G35329','G35729')


            select vin
            from nc.stg_availability a
            where not exists (
                select 1
                from chr.describe_vehicle
                where vin = a.vin);

((( -- inpmast vs chrome  -- aha, use triple parens for a collapsible block

-- clean this up, basically this is inpmast vs chrome
drop table if exists wtf;
create temp table wtf as
select a.*, (r.style ->'attributes'->>'id')::integer as chr_style_id,
  (r.style ->'attributes'->>'modelYear')::citext as chr_model_year,
  (r.style ->'division'->>'$value')::citext as chr_make,
  (r.style ->'model'->>'$value'::citext)::citext as chr_model,
  (r.style ->'attributes'->>'mfrModelCode')::citext as chr_model_code,
  case
    when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as chr_drive,
  case
    when u.cab->>'$value' like 'Crew%' then 'crew' 
    when u.cab->>'$value' like 'Extended%' then 'double'  
    when u.cab->>'$value' like 'Regular%' then 'reg'  
    else 'N/A'   
  end as chr_cab,
  r.style ->'attributes'->>'trim' as chr_trim,
  t.displacement->>'$value' as engine_displacement, c.style_count/*, b.color , b.color_code */
from (
  select *
  from nc.stg_Availability) a
join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
join chr.describe_vehicle c on a.vin = c.vin
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->'attributes'->>'unit' = 'liters'  
where c.style_count = 1
  and r.style ->'attributes'->>'modelYear' in ('2018','2019')
  and (r.style ->'model'->>'$value'::citext)::citext not like ('Silverado 1%')
  and (r.style ->'model'->>'$value'::citext)::citext not like ('Equin%')
union
select a.*, (r.style ->'attributes'->>'id')::integer as chr_style_id,
  (r.style ->'attributes'->>'modelYear')::citext as chr_model_year,
  case
    when b.make like 'HONDA%' then 'HONDA'::citext
    when b.make like 'CHEV%' then 'CHEVROLET'::citext
    else b.make
  end as make,
  (r.style ->'model'->>'$value'::citext)::citext as model,
  (r.style ->'attributes'->>'mfrModelCode')::citext as model_code,
  case
    when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as drive,
  case
    when u.cab->>'$value' like 'Crew%' then 'crew' 
    when u.cab->>'$value' like 'Extended%' then 'double'  
    when u.cab->>'$value' like 'Regular%' then 'reg'  
    else 'N/A'   
  end as cab,
  r.style ->'attributes'->>'trim' as chr_trim,
  t.displacement->>'$value' as engine_displacement, c.style_count/*, b.color , b.color_code */
from (
  select *
  from nc.stg_Availability) a
join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
join chr.describe_vehicle c on a.vin = c.vin
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->'attributes'->>'unit' = 'liters'  
where c.style_count > 1
  and r.style ->'attributes'->>'modelYear' in ('2018','2019')
  and (r.style ->'model'->>'$value'::citext)::citext not like ('Silverado 1%')
  and (r.style ->'model'->>'$value'::citext)::citext not like ('Equin%')
  and a.model_code = (r.style ->'attributes'->>'mfrModelCode')::citext;

select * from wtf a
where exists (select 1 from nc.vehicles where vin = a.vin)


-- differences betw inpmast and chr
select 'model' as field, model, chr_model
from wtf
where coalesce(model, 'inp') <> coalesce(chr_model, 'chr')
union
select 'make', make, chr_make
from wtf
where make <> chr_make
union
select 'model_code', model_code, chr_model_code
from wtf
where model_code <> chr_model_code
union
select 'cab', cab, chr_cab
from wtf
where coalesce(cab, 'inp') <> coalesce(chr_cab, 'chr')
union
select 'drive', drive, chr_drive
from wtf
where drive <> chr_drive

this is no good
at this point in time nc.stg_availability is all nulls for drive, cab, trim, engine
thinking, what do i want in stg_availability:
based on accounting, vehicles that do not yet exist in nc.vehicle_acquisitions
and this table is the foundation for new rows in vehicles and vehicle_Acquisitions

well and good, not up to a full refactoring yet, get the nightly deal going in the style of equinoxen
)))


((( -- compare source in invoice to source in vis

invoice for 1GYFZBR48KF130734 shows the dealer as CADILLAC OF GRAND FORKS
but the vis delivery_store = RYDELL CHEVROLET BUICK GMC CADILLAC
select * from gmgl.vehicle_vis where vin = '1GYFZBR48KF130734'

-- invoice and vis are in complete aggreement
drop table if exists sources cascade;
create temp table sources as
select a.make, a.stock_number, a.vin, a.source,
  case 
    when position('RYDELL' in b.raw_invoice) = 0 and position('CADILLAC OF GRAND FORKS' in b.raw_invoice) = 0 then 'dealer trade'
    when position('RYDELL' in b.raw_invoice) <> 0 or position('CADILLAC OF GRAND FORKS' in b.raw_invoice) <> 0 then 'factory'
    else 'xxx'
  end as inv_source, 
  case 
    when position('RYDELL' in replace(regexp_replace(c.delivery_store, '\n', '', 'g'), '&nbsp;','')) = 0 then 'dealer trade'
    when position('RYDELL' in replace(regexp_replace(c.delivery_store, '\n', '', 'g'), '&nbsp;','')) <> 0 then 'factory' 
    else 'xxx' 
  end as vis_source
from nc.stg_availability a  
left join gmgl.vehicle_invoices b on a.vin = b.vin
  and b.thru_date > current_date
left join gmgl.vehicle_vis c on a.vin = c.vin

select * 
from sources
where inv_source <> 'xxx'
  and source <> 'ctp'
  and source <> inv_source
))))


-- fix dealer trades not picked up by accounting
update nc.stg_availability
set source = 'dealer trade'
where stock_number in (
  select a.stock_number
  -- select a.*, regexp_replace(b.delivery_store, '\n', '', 'g')
  from nc.stg_availability a
  join gmgl.vehicle_vis b on a.vin = b.vin
  where regexp_replace(b.delivery_store, '\n', '', 'g') not like 'RYDELL%'
    and a.source = 'factory');

-- unwind acquisitions
-- test for unwind on same day as sale ?!?
insert into nc.stg_availability (the_date,stock_number,vin,model_year,
  model_code,color_code,color,ground_date,source)
select current_date, a.stock_number, a.vin, b.model_year,
  b.model_Code, c.color_code, b.color, a.run_date - 1, 'unwind'
from sls.deals a
join nc.vehicle_acquisitions aa on a.stock_number = aa.stock_number -- to be an unwind, previous acq record must exist
join nc.vehicles b on a.vin = b.vin
  and b.model <> 'equinox'
  and b.model not like 'silverado 1%'
join arkona.xfm_inpmast c on b.vin = c.inpmast_vin
  and c.current_row = true
where a.run_date > current_Date - 60
  and deal_status_code = 'deleted'
  and a.stock_number <> 'H11601'
  and not exists (
    select 1
    from nc.vehicle_acquisitions
    where stock_number = a.stock_number
      and ground_date = a.run_date - 1);

-------------------------------------------------------------------------------------
--< ground dates
-------------------------------------------------------------------------------------
-- ctp
update nc.stg_availability
set ground_date = journal_date
where source = 'ctp';

-- dealer trades: ground = coalesce(keyper, coaelsece dt ro, journal date)
-- 10/23 any ro
update nc.stg_availability x
set ground_date = y.ground
from (
  select aa.stock_number, aa.vin, aa.journal_date, aa.source, 
    bb.the_date, cc.creation_date as keyper,
    coalesce(cc.creation_date, coalesce(bb.the_date, aa.journal_date)) as ground
  from nc.stg_availability aa
  left join (
    select d.vin, min(b.the_date) as the_Date
    from ads.ext_fact_repair_order a
    inner join dds.dim_date b on a.opendatekey = b.date_key
--     inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
    inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
    inner join nc.stg_availability e on d.vin = e.vin
    group by d.vin) bb on aa.vin = bb.vin
  left join ads.keyper_creation_dates cc on aa.stock_number = cc.stock_number  
  where aa.source = 'dealer trade') y
where x.stock_number = y.stock_number; 

-- factory
-- 10/23 any ro, no need for order info
-- if there is neither keyper nor ro, ground = 12/31/9999
update nc.stg_availability x
set ground_date = y.ground
from (
  select aa.stock_number, aa.vin, aa.journal_date, aa.source, 
    bb.the_date, cc.creation_date as keyper, -- dd.event_code, dd.description, dd.from_date,
    coalesce(cc.creation_date, coalesce(bb.the_date, '12/31/9999'::date)) as ground
  from nc.stg_availability aa
  left join (
    select d.vin, min(b.the_date) as the_date
    from ads.ext_fact_repair_order a
    inner join dds.dim_date b on a.opendatekey = b.date_key
    inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
    inner join nc.stg_availability e on d.vin = e.vin
    group by d.vin) bb on aa.vin = bb.vin
  left join ads.keyper_creation_dates cc on aa.stock_number = cc.stock_number  
  where source = 'factory') y  
where x.stock_number = y.stock_number;

-- ok, delete if ground = 12/31/9999 
-- those will be picked up as in transit/in system orders
delete 
-- select * 
from nc.stg_availability
where ground_date = '12/31/9999';

do
$$
begin
assert ( -- test for null ground date
  select count(*)
  -- select *
  from nc.stg_availability a
  where ground_date is null) = 0, 'acquisitions with null ground date';
end
$$;
-------------------------------------------------------------------------------------
--/> ground dates
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--< colors
-------------------------------------------------------------------------------------
do
$$
begin
assert (
  select count(*)
  -- select *
  from nc.stg_availability a
  left join nc.exterior_colors b on a.color_code = b.mfr_color_code
    and a.model_year = b.model_year
    and a.make = b.make
    and trim(replace(a.model, 'SEDAN',''))::citext = b.model -- honda models
  where (a.color <> b.mfr_color_name or b.mfr_color_name is null)) = 0, 'DANGER WILL ROBINSON';
end
$$;

( -- initial color fixes
-- damnit, should have don the gm color fiixng with invoices
drop table if exists invoices cascade;
create temp table invoices as
select vin, trim(left(raw_invoice, position('GENERAL MOTORS LLC' in raw_invoice)-1)):: citext as description,
  case position('RYDELL' in raw_invoice)
    when 0 then 'dealer trade'
    else 'factory'
  end as source,
  left(substring(raw_invoice from position('MODEL' in raw_invoice) + 78 for 8), 
    position(' ' in substring(raw_invoice from position('MODEL' in raw_invoice) + 78 for 8))) as model_code,
  substring(raw_invoice from position(E'\n' in raw_invoice) + 1 for 3) as color_code,
  trim(substring(raw_invoice from position(E'\n' in raw_invoice) + 6 for 31))::citext as color       
from gmgl.vehicle_invoices
where thru_date > current_date;
create unique index on invoices(vin);

select * 
from nc.stg_Availability a
left join invoices b on a.vin = b.vin
where a.color <> b.color

select a.vin, a.model_year, a.make, a.model, a.color_code, a.color, b.*
from nc.stg_availability a
left join nc.exterior_colors b on a.color_code = b.mfr_color_code
  and a.model_year = b.model_year
  and a.make = b.make
  and trim(replace(a.model, 'SEDAN',''))::citext = b.model -- honda models
where (a.color <> b.mfr_color_name or b.mfr_color_name is null)
order by color_code

-- the remaining 24 are all valid colors
update nc.stg_availability
set color = 'Nightfall Gray Metallic'
where vin in ('KL8CD6SA8KC721218');
update nc.stg_availability
set color = 'SATIN STEEL GRAY METALLIC'
where vin in ('KL4CJGSM6KB734811','KL4CJGSM5KB750966','LRBFX2SA9KD030521','SATIN STEEL GRAY METALLIC');

update nc.stg_availability
set color = 'Caspian Blue'
where vin in ('5N1AT2MV5JC838808');
update nc.stg_availability
set color = 'Caspian Blue Metallic'
where vin in ('JN8AT2MV2KW377818');
update nc.stg_availability
set color = 'Magnetic Black'
where vin in ('KNMAT2MV0JP576709','KNMAT2MV0JP561921','KNMAT2MV9JP551971');
update nc.stg_availability
set color = 'Magnetic Black Pearl'
where vin in ('1N6AD0EV5KN711757','5N1AT2MV1KC739534','JN8AT2MV1KW379690');
update nc.stg_availability
set color = 'Modern Steel Metallic'
where vin in ('2HKRW2H54JH685450','2HKRW2H89JH668556');
update nc.stg_availability
set color = 'Pacific Blue Metallic'
where vin in ('1G1ZD5ST3KF115149');
update nc.stg_availability
set color = 'Pearl White'
where vin in ('5N1AT2MV4JC771389');
update nc.stg_availability
set color = 'Polished Metal Metallic'
where vin = 'SHHFK7H64KU200162'
update nc.stg_availability
set color = 'PEARL WHITE'
where vin = 'JN1BJ1CRXJW254547'
update nc.stg_availability
set color = 'Scarlet Ember'
where vin = '1N4BL4DV8KC113845'

update nc.stg_availability
set color = 'Basque Red Pearl II'
where model = 'CR-V'
  and color_code = 'RB'
  and color <> 'Basque Red Pearl II';

update nc.stg_availability
set color = 'Brilliant Silver Metallic'
where make = 'NISSAN'
  and color_code = 'K23'
  and color <> 'Brilliant Silver Metallic';

update nc.stg_availability
set color = 'Cajun Red Tintcoat'
where make = 'Chevrolet'
  and color_code = 'GPJ'
  and color <> 'Cajun Red Tintcoat';  

update nc.stg_availability
set color = 'Caspian Blue Metallic'
where model = 'rogue'
  and color_code = 'RBY'
  and color <> 'Caspian Blue Metallic';    

update nc.stg_availability
set color = 'Chili Red Metallic'
where model = 'envision'
  and color_code = 'G8B'
  and color <> 'Chili Red Metallic';  

update nc.stg_availability
set color = 'Crystal White Tricoat'
where model = 'xt5'
  and color_code = 'G1W'
  and color <> 'Crystal White Tricoat';  
update nc.stg_availability
set color = 'Deep Blue Pearl Metallic'
where model = 'sentra'
  and color_code = 'RAY'
  and color <> 'Deep Blue Pearl Metallic';  
update nc.stg_availability
set color = 'Iridium Metallic'
where model = 'acadia'
  and color_code = 'gxg'
  and color <> 'Iridium Metallic';  
update nc.stg_availability
set color = 'Monarch Orange/Super Black'
where model = 'kicks'
  and color_code = 'XAH'
  and color <> 'Monarch Orange/Super Black';  
update nc.stg_availability
set color = 'Mosaic Black Metallic'
where model = 'sonic'
  and color_code = 'GB8'
  and color <> 'Mosaic Black Metallic';  
update nc.stg_availability
set color = 'Nightfall Gray Metallic'
where model = 'spark'
  and color_code = 'GK2'
  and color <> 'Nightfall Gray Metallic';  
update nc.stg_availability
set color = 'Northsky Blue Metallic'
where model = 'malibu'
  and color_code = 'GA0'
  and color <> 'Northsky Blue Metallic';  
update nc.stg_availability
set color = 'Obsidian Blue Pearl'
where model = 'cr-v'
  and color_code = 'BU'
  and color <> 'Obsidian Blue Pearl';                
update nc.stg_availability
set color = 'Pacific Blue Metallic'
where model = 'malibu'
  and color_code = 'g60'
  and color <> 'Pacific Blue Metallic';
update nc.stg_availability
set color = 'Pearl White Tricoat'
where make = 'nissan'
  and color_code = 'qab'
  and color <> 'Pearl White Tricoat';
update nc.stg_availability
set color = 'Pepperdust Metallic'
where model = 'acadia'
  and color_code = 'gmu'
  and color <> 'Pepperdust Metallic';
update nc.stg_availability
set color = 'Red Quartz Tintcoat'
where model = 'acadia'
  and color_code = 'gpj'
  and color <> 'Red Quartz Tintcoat';
update nc.stg_availability
set color = 'Sandstorm Metallic'
where model = 'cr-v'
  and color_code = 'bg'
  and color <> 'Sandstorm Metallic';
update nc.stg_availability
set color = 'Satin Steel Metallic'
where make = 'buick'
  and color_code = 'gir'
  and color <> 'Satin Steel Metallic';
update nc.stg_availability
set color = 'Satin Steel Metallic'
where make = 'gmc'
  and color_code = 'g9k'
  and color <> 'Satin Steel Metallic';
update nc.stg_availability
set color = 'Satin Steel Metallic'
where make = 'buick'
  and color_code = 'GYM'
  and color <> 'Satin Steel Metallic';
update nc.stg_availability
set color = 'Storm Blue'
where model = 'altima'
  and color_code = 'rbd'
  and color <> 'Storm Blue';
update nc.stg_availability
set color = 'Summit White'
where model = 'acadia'
  and color_code = 'gaz'
  and color <> 'Summit White';     
update nc.stg_availability
set color = 'White Diamond Pearl'
where model = 'cr-v'
  and color_code = 'wb'
  and color <> 'White Diamond Pearl'; 
update nc.stg_availability
set color = 'White Frost Tricoat'
where make = 'gmc'
  and color_code = 'g1w'
  and color <> 'White Frost Tricoat'; 
update nc.stg_availability
set color = 'Winterberry Red Metallic'
where model = 'encore'
  and color_code = 'gcs'
  and color <> 'Winterberry Red Metallic'; 
update nc.stg_availability
set color = ''
where model = 'rogue'
  and color_code = 'RBY'
  and color <> ''; 
update nc.stg_availability
set color = ''
where model = 'rogue'
  and color_code = 'RBY'
  and color <> ''; 
update nc.stg_availability
set color = ''
where model = 'rogue'
  and color_code = 'RBY'
  and color <> ''; 
update nc.stg_availability
set color = ''
where model = 'rogue'
  and color_code = 'RBY'
  and color <> ''; 
update nc.stg_availability
set color = ''
where model = 'rogue'
  and color_code = 'RBY'
  and color <> '';
update nc.stg_availability
set color = ''
where model = 'rogue'
  and color_code = 'RBY'
  and color <> ''; 
update nc.stg_availability
set color = ''
where model = 'rogue'
  and color_code = 'RBY'
  and color <> ''; 
update nc.stg_availability
set color = ''
where model = 'rogue'
  and color_code = 'RBY'
  and color <> '';       
)                                     
-------------------------------------------------------------------------------------
--/> colors
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--< chrome
-------------------------------------------------------------------------------------
do -- all vins exist in chr.describe_vehicle
$$
begin
assert (
  select count(*)
--   select * --a.vin
  from nc.stg_availability a
  left join (
    select vin, count(*)
    from (
      select vin, jsonb_array_elements(response->'style')
      from chr.describe_vehicle) x
    group by vin) b on a.vin = b.vin
  where b.vin is null) = 0, 'DANGER WILL ROBINSON, there are new vins missing from chr.describe_vehicle';
end
$$;  


do
$$
begin
assert ( -- vins with multiple chrome styles
      select count(*) = sum(b.style_count)
      from nc.stg_availability a
      left join chr.describe_vehicle b on a.vin = b.vin), 'vehicles with multiple chrome styles';
end
$$;


i anticipate multiple scenarios for vin with multiple chrome styles
this one is very straightforward
all hondas
all resolved by matching inpmast model_code to chrome model_code


select a.*
from nc.stg_availability a      
left join chr.describe_vehicle b on a.vin = b.vin
left join jsonb_array_elements(b.response->'style') as r(style) on true
where a.model_code = (r.style ->'attributes'->>'mfrModelCode')::citext


select *
from nc.stg_Availability a
join chr.describe_vehicle b on a.vin = b.vin
where b.style_count <> 1


  select a.*,
    r.style ->'attributes'->>'id' as chr_style_id,
    r.style ->'attributes'->>'mfrModelCode' as chr_model_code,
    r.style ->'attributes'->>'modelYear' as chr_model_year,
    r.style ->'attributes'->>'name' as style,
    r.style ->'attributes'->>'drivetrain' as drive_train,
    t.displacement->>'$value' as engine_displacement
  from nc.stg_availability a      
  left join chr.describe_vehicle b on a.vin = b.vin
  left join jsonb_array_elements(b.response->'style') as r(style) on true
  left join jsonb_array_elements(b.response->'engine') as s(engine) on true
  left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
    and t.displacement ->'attributes'->>'unit' = 'liters'
  where b.style_count <> 1
    and a.model_code <> (r.style ->'attributes'->>'mfrModelCode')::citext
-------------------------------------------------------------------------------------
--/> chrome
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--< new acquisitioins where vin already exists in nc.vehicles (ctp)
-------------------------------------------------------------------------------------
insert into nc.vehicle_acquisitions (vin,stock_number,ground_date,source,in_transit_sale)
select a.vin, a.stock_number, a.ground_date, a.source, false
from nc.stg_availability a
inner join nc.vehicles aa on a.vin = aa.vin
  and a.source <> 'unwind'; -- unwinds are processed separately
-------------------------------------------------------------------------------------
--/> new acquisitioins where vin already exists in nc.vehicles (ctp)
-------------------------------------------------------------------------------------  

--------------------------------------------------------------------------------------------------
--< nc.vehicles
--------------------------------------------------------------------------------------------------

(( -- nc.stg_vehicles
/*
  for now at least, start with stg_Vehicles, clean up the anomalies before 
  inserting into nc.vehicles
*/
CREATE TABLE nc.stg_vehicles
(
  vin citext NOT NULL,
  chrome_style_id integer DEFAULT '-1'::integer,
  model_year citext NOT NULL,
  make citext NOT NULL,
  model citext NOT NULL,
  model_code citext NOT NULL,
  drive citext NOT NULL,
  cab citext NOT NULL,
  trim_level citext,
  engine citext,
  color citext NOT NULL,
  sku_id integer,
  PRIMARY KEY (vin)
); 

truncate nc.stg_vehicles;
insert into nc.stg_vehicles (vin, chrome_Style_id, model_year, make, model,
  model_code, drive, cab, trim_level, engine, color)
-- the single chrome_style vins
select a.vin, (r.style ->'attributes'->>'id')::integer as chr_style_id,
  (r.style ->'attributes'->>'modelYear')::citext as chr_model_year,
  (r.style ->'division'->>'$value')::citext as chr_make,
  (r.style ->'model'->>'$value'::citext)::citext as chr_model,
  (r.style ->'attributes'->>'mfrModelCode')::citext as chr_model_code,
  case
    when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as chr_drive,
  case
    when u.cab->>'$value' like 'Crew%' then 'crew' 
    when u.cab->>'$value' like 'Extended%' then 'double'  
    when u.cab->>'$value' like 'Regular%' then 'reg'  
    else 'N/A'   
  end as chr_cab,
  r.style ->'attributes'->>'trim' as chr_trim,
  t.displacement->>'$value' as engine_displacement, a.color
from (
  select *
  from nc.stg_Availability) a
join chr.describe_vehicle c on a.vin = c.vin
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->'attributes'->>'unit' = 'liters'  
where c.style_count = 1
  and r.style ->'attributes'->>'modelYear' in ('2018','2019')
  and (r.style ->'model'->>'$value'::citext)::citext not like ('Silverado 1%')
  and (r.style ->'model'->>'$value'::citext)::citext not like ('Equin%')
union
-- the multiple chrome_style_vins
select a.vin, (r.style ->'attributes'->>'id')::integer as chr_style_id,
  (r.style ->'attributes'->>'modelYear')::citext as chr_model_year,
  (r.style ->'division'->>'$value')::citext as chr_make,
  (r.style ->'model'->>'$value'::citext)::citext as chr_model,
  (r.style ->'attributes'->>'mfrModelCode')::citext as chr_model_code,
  case
    when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'::citext
  end as chr_drive,
  case
    when u.cab->>'$value' like 'Crew%' then 'crew' 
    when u.cab->>'$value' like 'Extended%' then 'double'  
    when u.cab->>'$value' like 'Regular%' then 'reg'  
    else 'N/A'   
  end as chr_cab,
  r.style ->'attributes'->>'trim' as chr_trim,
  t.displacement->>'$value' as engine_displacement, a.color
from (
  select *
  from nc.stg_Availability) a
join chr.describe_vehicle c on a.vin = c.vin
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->'attributes'->>'unit' = 'liters'  
where c.style_count > 1
  and r.style ->'attributes'->>'modelYear' in ('2018','2019')
  and (r.style ->'model'->>'$value'::citext)::citext not like ('Silverado 1%')
  and (r.style ->'model'->>'$value'::citext)::citext not like ('Equin%')
  and a.model_code = (r.style ->'attributes'->>'mfrModelCode')::citext;



looks awfully good
select *
from nc.stg_vehicles
where drive = 'XXX'

select *
from nc.stg_vehicles a
join nc.vehicles b on a.vin = b.vin

-- only 4 without trim and they look legitimate
select *
from nc.stg_vehicles a
left join nc.stg_Availability b on a.vin = b.vin
where a.trim_level is null
order by a.make, a.model

select * from chr.describe_vehicle where vin = '2GT22NEG9K1136821'

)) 

(( -- bring equinox and silverado into the single script
-- equinox only dif is PREM vs Premier
-- engine and drive are good
select *
from (
  select a.*, 
    r.style ->'attributes'->>'trim' as chr_trim,
    (r.style ->'attributes'->>'modelYear')::citext as chr_model_year,
    (r.style ->'division'->>'$value')::citext as chr_make,
    (r.style ->'model'->>'$value'::citext)::citext as chr_model,
    (r.style ->'attributes'->>'mfrModelCode')::citext as chr_model_code,
    case
      when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
      when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
      when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
      when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
      else 'XXX'::citext
    end as chr_drive,
    t.displacement->>'$value' as chr_eng
  from nc.vehicles a
  join chr.describe_vehicle c on a.vin = c.vin
  join jsonb_array_elements(c.response->'style') as r(style) on true
  left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
  left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
    and t.displacement ->'attributes'->>'unit' = 'liters'    
  where a.model = 'equinox') x
-- where trim_level <> chr_trim
-- where drive <> chr_drive
where engine <> chr_eng



-- silverados lots of issues
biggest deal diff between 1LT and 2LT, and between 1LTZ and 2LTZ
WT vs work truck
2WD vs RWD
-- engine and drive are good
select *
from (
  select a.*, 
    r.style ->'attributes'->>'trim' as chr_trim,
    (r.style ->'attributes'->>'modelYear')::citext as chr_model_year,
    (r.style ->'division'->>'$value')::citext as chr_make,
    (r.style ->'model'->>'$value'::citext)::citext as chr_model,
    (r.style ->'attributes'->>'mfrModelCode')::citext as chr_model_code,
    case
      when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
      when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
      when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
      when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
      else 'XXX'::citext
    end as chr_drive,
    t.displacement->>'$value' as chr_eng
  from nc.vehicles a
  join chr.describe_vehicle c on a.vin = c.vin
  join jsonb_array_elements(c.response->'style') as r(style) on true
  left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
  left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
    and t.displacement ->'attributes'->>'unit' = 'liters'    
  where a.model like 'silverado 1%') x
-- where trim_level <> chr_trim
-- where drive <> chr_drive
where engine <> chr_eng
order by trim_level

))

insert into nc.vehicles(vin,chrome_style_id,model_year,make,model,
  model_code,drive,cab,trim_level,engine,color)
select vin,chrome_style_id,model_year,make,model,
  model_code,drive,cab,trim_level,engine,color
from nc.stg_vehicles a
where not exists (
  select 1
  from nc.vehicles 
  where vin = a.vin);

--------------------------------------------------------------------------------------------------
--/> nc.vehicles
--------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------
--< nc.vehicle_acquisitions
--------------------------------------------------------------------------------------------------
insert into nc.vehicle_acquisitions  (vin,stock_number,ground_date,source,in_transit_sale)
select a.vin, a.stock_number, a.ground_date, a.source, false
from nc.stg_availability a
left join nc.vehicle_acquisitions b on a.stock_number = b.stock_number
  and a.ground_date = b.ground_date
where b.vin is null  
  and a.source <> 'unwind';

-- and finally, the unwind acquisition
-- not sure how this will work for multiple unwinds, only want to update the 
-- must recent acquisition record 
update nc.vehicle_acquisitions x
set thru_date = y.the_date
from (
  select a.vin, a.stock_number, a.ground_date - 1 as the_date
  from nc.stg_availability a
  where source = 'unwind') y
where x.stock_number = y.stock_number;

insert into nc.vehicle_acquisitions  (vin,stock_number,ground_date,source,in_transit_sale)
select a.vin, a.stock_number, a.ground_date, a.source, false
from nc.stg_availability a
where source = 'unwind';   

--------------------------------------------------------------------------------------------------
--/> nc.vehicle_acquisitions
--------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------
--< SALES
--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
--< nc.stg_sales
--------------------------------------------------------------------------------------------------
-- accounting should include all sales
-- first step, accounting info and vin from inpmast
truncate nc.stg_sales;
insert into nc.stg_sales (stock_number, vin, trans_description, amount, acct_date,ground_date)
select a.control, f.inpmast_vin, e.description, a.amount, b.the_date, g.ground_date
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
--   and b.the_date between current_Date - 40 and current_date
join fin.dim_account c on a.account_key = c.account_key
  and c.account in ( -- all new vehicle inventory account
    select b.account
    from fin.dim_account b 
    where b.account_type = 'asset'
      and b.department_code = 'nc'
      and b.typical_balance = 'debit'
      and b.current_row
      and b.account <> '126104')    
join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code = 'VSN'
join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
left join arkona.xfm_inpmast f on a.control = f.inpmast_stock_number
  and f.current_row = true  
left join nc.vehicle_acquisitions g on a.control = g.stock_number
  and g.thru_date = ( -- need most recent acquisition
    select max(thru_date)
    from nc.vehicle_acquisitions
    where stock_number = g.stock_number)  
where a.post_status = 'Y'
  and abs(a.amount) > 10000
  and f.year in ('2018', '2019')
  and f.model not like 'equ%'
  and f.model not like 'silverado 1%'
  and not exists (
    select 1
    from nc.vehicle_sales
    where stock_number = a.control
      and ground_date = g.ground_date) ;

do 
$$
begin
assert ( -- ground date null
  select count(*)
--   select *
  from nc.stg_sales a
  where a.ground_date is null) = 0, 'NULL ground date';
end
$$;

( -- initial null ground dates 
holy shit 25 with null ground dates

-- case 1 missing acquisition for retail of ctp unit
select * from nc.vehicle_acquisitions where vin = '1G1ZD5ST2JF156080'
select * from nc.vehicles where vin = '1G1ZD5ST2JF156080'
select * from nc.vehicle_sales where vin = '1G1ZD5ST2JF156080'
select * from sls.ext_bopmast_partial where vin = '1G1ZD5ST2JF156080';
insert into nc.vehicle_acquisitions values ('1G1ZD5ST2JF156080', '32438R', '05/21/2018', '07/17/2018', 'ctp', false);
insert into nc.vehicle_sales values ('32438R', '05/21/2018','1G1ZD5ST2JF156080', 49310, '07/17/2018', 'retail', false) ; 

select * from sls.deals where vin = '1N4AL3APXJC477291'

-- factory acquisition dealer traded, no record in nc.vehicles
H11618  1N6AA1RP6JN507638

select * from nc.vehicles where vin = '1N6AA1RP6JN507638'

do
$$
declare
  _stock_number citext := 'H11618';
begin  
-- nc.vehicles 
  insert into nc.vehicles(vin,chrome_style_id,model_year,make,model,
    model_code,drive,cab,trim_level,engine,color)
  select a.vin, (r.style ->'attributes'->>'id')::integer as chr_style_id,
    (r.style ->'attributes'->>'modelYear')::citext as chr_model_year,
    (r.style ->'division'->>'$value')::citext as chr_make,
    (r.style ->'model'->>'$value'::citext)::citext as chr_model,
    (r.style ->'attributes'->>'mfrModelCode')::citext as chr_model_code,
    case
      when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
      when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
      when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
      when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
      else 'XXX'
    end as chr_drive,
    case
      when u.cab->>'$value' like 'Crew%' then 'crew' 
      when u.cab->>'$value' like 'Extended%' then 'double'  
      when u.cab->>'$value' like 'Regular%' then 'reg'  
      else 'N/A'   
    end as chr_cab,
    r.style ->'attributes'->>'trim' as chr_trim,
    t.displacement->>'$value' as engine_displacement, d.color
  from nc.stg_sales a
  join chr.describe_vehicle c on a.vin = c.vin
  join jsonb_array_elements(c.response->'style') as r(style) on true
  left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
    and u.ordinality =  2
  left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
  left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
    and t.displacement ->'attributes'->>'unit' = 'liters'  
  left join arkona.xfm_inpmast d on a.stock_number = d.inpmast_stock_number
    and d.current_row   
    where a.stock_number = _stock_number;

  -- now need the acquisition
  -- ground date = sale date = thru_Date
  insert into nc.vehicle_acquisitions(vin,stock_number,ground_date,thru_date,source,in_transit_sale)
  select vin, stock_number, '09/30/2016', acct_date, 'factory', false
  from nc.stg_sales
  where stock_number = _stock_number;

-- update the ground date in nc.stg_sales
-- select * from nc.stg_sales
  update nc.stg_sales
  set ground_date = acct_date
  where stock_number = _stock_number;
  
end  
$$;

)


-- bopmast_id
update nc.stg_sales x
set bopmast_id = y.bopmast_id,
    delivery_date = y.delivery_date
from (    
  select b.*
  from nc.stg_sales a
  left join (
    select stock_number, bopmast_id, max(seq) as seq, max(delivery_date) as delivery_date
    from sls.deals
    group by stock_number, bopmast_id) b on a.stock_number = b.stock_number) y
where x.stock_number = y.stock_number;

-- sale_types dealer trade & ctp
update nc.stg_sales x
set sale_type =
  case
    when bopmast_id is null then 'dealer trade'
    when trans_description like '%RYDELL%' then 'ctp'
  end;

   -- sale_type fleet
 update nc.stg_sales x
set sale_type = 'fleet'
where stock_number in (
  select a.stock_number
  from nc.stg_sales a
  inner join (
    select a.control, b.the_date
    from fin.fact_gl a
    inner join dds.dim_date b on a.date_key = b.date_key
    inner join fin.dim_account c on a.account_key = c.account_key
    inner join (
      select distinct d.gl_account
      from fin.fact_fs a 
      inner join fin.dim_fs b on a.fs_key = b.fs_key
        and b.year_month > 201601
        and b.page in (5,8,9,10)
        and b.line in (22,43)
        and b.col = 1 -- sales
      inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
--         and c.store = 'ry1'
      inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key) d on c.account = d.gl_account
    where a.post_status = 'Y') b on a.stock_number = b.control);

-- dealer trade delivery dates
update nc.stg_sales
set delivery_date = acct_date
where sale_type = 'dealer trade';

-- and finally, everything else is retail
update nc.stg_sales
set sale_type = 'retail'
where sale_type is null;   

select * 
from nc.stg_sales

-- one outlier
update nc.stg_sales set sale_type = 'retail' where stock_number = 'H11618';

-- test for ground date > delivery date
do
$$
begin
assert (
  select count(*)
  -- select *
    from nc.stg_sales
    where ground_date > delivery_date) = 0, 'ground date > delivery date';
end
$$;    

-- 2 anomalies, just change the ground date
-- first in the acquisition
update nc.vehicle_acquisitions
set ground_date = '12/11/2018'
where stock_number in ('G35903','G35895');

update nc.stg_sales
set ground_date = '12/11/2018'
where stock_number in ('G35903','G35895');

-- update nc.vehicle_acquisitions.thru_date for sales
update nc.vehicle_acquisitions x
set thru_date  = y.delivery_date
from (
  select a.*
  from nc.stg_sales a
  left join nc.vehicle_acquisitions b on a.stock_number = b.stock_number) y
where x.stock_number = y.stock_number
  and x.thru_date = '12/31/9999';

-- and install the sales
insert into nc.vehicle_sales
select stock_number, ground_date, vin, coalesce(bopmast_id, -1), delivery_date, sale_type
from nc.stg_sales;  

--------------------------------------------------------------------------------------------------
--/> SALES
--------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------
--/> nightly
--------------------------------------------------------------------------------------------