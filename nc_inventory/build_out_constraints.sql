﻿--------------------------------------------------------
--< 9/16/19
-------------------------------------------------------
FUNCTION nc.xfm_build_out_constraints() failed the assert, new model in the report: GMC SIERRA LD CREW CAB

insert into nc.build_out_constraint_make_models values
('GMC','SIERRA LD CREW CAB');

-------------------------------------------------------
--/> 9/16/19
-------------------------------------------------------


-- Table: nc.ext_build_out_constraints

-- DROP TABLE nc.ext_build_out_constraints;

CREATE TABLE nc.ext_build_out_constraints
(
  division citext,
  model citext,
  description citext,
  last_dosp citext,
  last_production_week citext
)
WITH (
  OIDS=FALSE
);
ALTER TABLE nc.ext_build_out_constraints
  OWNER TO rydell;
COMMENT ON TABLE nc.ext_build_out_constraints
  IS 'cleaned up version of nc.ext_build_out_constraints,
  left out division, only populate rows with data in description';


CREATE TABLE nc.xfm_build_out_constraints_1
(
  division citext,
  model citext,
  description citext,
  last_dosp citext,
  last_production_week citext,
  page integer,
  row_number integer
)

drop table if exists nc.build_out_constraints;
create table nc.build_out_constraints(
  division citext,
  model citext,
  description citext,
  last_dosp citext,
  last_production_week citext,
  primary key (division,model,description));
comment on table nc.build_out_constraints is 'cleaned data from gm report, National Constraints Report,
obtained from the Order workbench, option level build out dates';
  
create or replace function nc.xfm_build_out_constraints_1(_page integer)
returns void  
as 
$BODY$

insert into nc.xfm_build_out_constraints_1
select division, model, description, last_dosp, last_production_week,
  _page, row_number() over ()
from nc.ext_build_out_constraints;

$BODY$
language sql;

drop table if exists nc.build_out_constraint_make_models cascade;
create table nc.build_out_constraint_make_models (
  make citext not null,
  model citext not null,
  primary key (make,model));
  
insert into nc.build_out_constraint_make_models values
('GMC','SIERRA LD DOUBLE CAB'),
('GMC','SIERRA LD DOUBLE CAB - NEXT GEN'),
('Buick','ENCLAVE'),
('Buick','ENCORE'),
('Buick','REGAL'),
('Buick','REGAL WAGON'),
('Cadillac','CT6'),
('Cadillac','ESCALADE ESC'),
('Cadillac','ESCALADE ESV'),
('Cadillac','XT4'),
('Cadillac','XT5'),
('Cadillac','XTS'),
('Cadillac','XTS PROFESSIONAL'),
('Chevrolet','BLAZER'),
('Chevrolet','BOLTEV'),
('Chevrolet','CAMARO'),
('Chevrolet','CAMARO CONVERTIBLE'),
('Chevrolet','COLORADO'),
('Chevrolet','CORVETTE'),
('Chevrolet','CORVETTE Z06'),
('Chevrolet','EQUINOX'),
('Chevrolet','EXPRESS VAN'),
('Chevrolet','IMPALA'),
('Chevrolet','MALIBU'),
('Chevrolet','SILVERADO HD DOUBLE CAB'),
('Chevrolet','SILVERADO LD CREW CAB - NEXT GEN'),
('Chevrolet','SILVERADO LD DOUBLE CAB - CURR GEN'),
('Chevrolet','SILVERADO LD DOUBLE CAB - NEXT GEN'),
('Chevrolet','SILVERADO LD REG - NEXT GEN'),
('Chevrolet','SILVERADO MEDIUM DUTY'),
('Chevrolet','SONIC'),
('Chevrolet','SUBURBAN'),
('Chevrolet','TAHOE'),
('Chevrolet','TRAVERSE'),
('Chevrolet','TRAX'),
('GMC','ACADIA'),
('GMC','CANYON'),
('GMC','SAVANA VAN'),
('GMC','SIERRA HD DOUBLE CAB'),
('GMC','SIERRA LD CREW CAB - NEXT GEN'),
('GMC','SIERRA LD REG - NEXT GEN'),
('GMC','TERRAIN'),
('GMC','YUKON'),
('GMC','YUKON XL');



  
anomalies
   divisision            model              description
-------------------------------------------------------------------------
1. Changes ...           null               null                     P1 L1
2. Completed ...         null               null                     P1 L2
3. New Build ...         null               null                     P1 L3
4. Division              Brand / Model      Description              P1 L4
5. valid division        valid model        null                     P1 L9,10,11,15,16, etc
6. null                  valid model        *                        P3 L1-6, 8-29
7. null                  null               not null                 P2 L1
8. null                  valid model        null                     Ps L2, P3 L7, after the above changes, more of these are exposed
9. *                     Silverado LD
   *                     Sierra LD


-- start out be betting rid of the obvious noise 
-- 1 - 4
delete 
-- select *
from nc.xfm_build_out_constraints_1
where page = 1
  and (
    division like 'Changes%'
    or division like 'Completed%'
    or division like 'New Build%'
    or division = 'Division');

-- valid division/models with no constraints    
-- 5
-- select *     
-- from nc.xfm_build_out_constraints_1 a
-- join nc.build_out_constraint_make_models b on a.division = b.make
--   and a.model = b.model
-- where description = ''
-- order by a.page, a.row_number;

delete from nc.xfm_build_out_constraints_1 a
using nc.build_out_constraint_make_models b
where a.description = ''
  and a.division = b.make
  and a.model = b.model
returning a.*;

-- valid model with null division and non null description  
-- 6
update nc.xfm_build_out_constraints_1 x
set division = y.make
from (
  select a.model, b.make   
  from nc.xfm_build_out_constraints_1 a
  join nc.build_out_constraint_make_models b on a.model = b.model
  where a.division = ''
    and a.model <> ''
    and a.description <> '') y
where x.model = y.model
  and x.division = ''
  and x.model <> '';

-- 7
-- at least in this case, p1 l28 has the division and model, but the second constraint is
-- on p2 l1, but on that line, the division and model are empty
-- is if the description is not empty, and the previous like division and model are not empty, 
-- combine them

update nc.xfm_build_out_constraints_1 x
set division = y.new_division,
    model = y.new_model
from (    
  select * 
  from (
    select a.*, lag(a.division, 1) over (order by page, row_number) as new_division, 
      lag(a.model, 1) over (order by page, row_number) as new_model
    from nc.xfm_build_out_constraints_1 a) b  
  where division = ''
    and model = ''
    and description <> '') y
where x.division = ''
  and x.model = ''
  and x.description <> '';  

-- 8
-- valid models with no descriptions
delete from nc.xfm_build_out_constraints_1 a
using nc.build_out_constraint_make_models b
where a.description = ''
  and a.division = ''
  and a.model = b.model
returning a.* ;

-- 9
-- what's left are the multiline models that get separated across pages
-- in this initial version, it is just SIERRA LD / CREW CAB - NEXT GEN & SILVERADO LD / CREW CAB - NEXT GEN
-- for both of these, both lines need to have the complete model
-- the test for this condition could be:
-- this could be the model split across pages or it could be a new model
select a.*
from nc.xfm_build_out_constraints_1 a
left join nc.build_out_constraint_make_models b on a.model = b.model
where b.model is null
order by a.page, a.row_number;

do 
$$
begin
assert ( 
  select count(*)
--   select a.*
  from nc.xfm_build_out_constraints_1 a
  left join nc.build_out_constraint_make_models b on a.model = b.model
  where b.model is null) = 0, 'Invalid model, could be split or new';
end
$$;


4/25/19 new report, no models split across pages
so this is not necessary, so probably should do a test with an assert in the function
--silverado
update nc.xfm_build_out_constraints_1 x
set model = y.model || ' ' || y.model_b
from (
  select *
  from (
    select a.*, lead(a.model, 1) over (order by page, row_number) as model_b
    from nc.xfm_build_out_constraints_1 a) b
  where b.model = 'SILVERADO LD') y
where x.model = 'SILVERADO LD';

update nc.xfm_build_out_constraints_1 x
set model = y.model_b,
    division = y.division_b
from (
  select *
  from (
    select a.*, lag(a.model, 1) over (order by page, row_number) as model_b,
      lag(a.division, 1) over (order by page, row_number) as division_b
    from nc.xfm_build_out_constraints_1 a) b
  where b.model = 'CREW CAB - NEXT GEN'
  and b.division = '') y
where x.model = 'CREW CAB - NEXT GEN'
and x.division = '';  

--sierra
update nc.xfm_build_out_constraints_1 x
set model = y.model || ' ' || y.model_b,
    division = y.division_b
from (
  select *
  from (
    select a.*, lead(a.model, 1) over (order by page, row_number) as model_b,
      LEAD(a.division, 1) over (order by page, row_number) as division_b
    from nc.xfm_build_out_constraints_1 a) b
  where b.model = 'SIERRA LD') y
where x.model = 'SIERRA LD';

update nc.xfm_build_out_constraints_1 x
set model = y.model_b
from (
  select *
  from (
    select a.*, lag(a.model, 1) over (order by page, row_number) as model_b
    from nc.xfm_build_out_constraints_1 a) b
  where b.model = 'CREW CAB - NEXT GEN') y
where x.model = 'CREW CAB - NEXT GEN'; 


truncate nc.build_out_constraints;
insert into nc.build_out_constraints
select division, model, description, last_dosp, last_production_week
from nc.xfm_build_out_constraints_1;



