﻿-- 52 of these mfers
drop table if exists tem.bad_actors cascade;
create table tem.bad_actors as
select stock_number, vin, source
from nc.vehicle_acquisitions
where ground_date > current_Date;


-- this is the bulk of the problems, but not all of them
drop table if exists tem.bad_actors_1 cascade;
create table tem.bad_actors_1 as
select a.stock_number, a.vin, a.source, b.order_number
from nc.vehicle_acquisitions a
join jo.vehicle_orders b on a.vin = b.vin
where a.ground_date > current_Date;

-- 1 GM factory w/out an order number
select *
from tem.bad_Actors a
left join tem.bad_actors_1 b on a.vin = b.vin
left join gmgl.vehicle_orders c on b.order_number = c.order_number
where b.vin is null
order by source



-- lets start with availability and see where it takes us
select a.*, null::citext as order_number 
from tem.bad_actors a
where stock_number like 'H%'
union
select *
from tem.bad_actors_1;



truncate nc.stg_availability;
insert into nc.stg_availability (the_date,journal_date,stock_number,
  vin,model_year,make, model,
  model_code,color_code,color, source)
select current_date, f.the_date, f.control, 
  g.inpmast_vin, g.year, g.make, g.model, g.model_code, 
  g.color_code, g.color,
  case f.journal_code
    when 'PVI' then 'factory'
    when 'CDH' then 'dealer trade'
    when 'GJE' then 'ctp'
    when 'EFT' then 'dealer trade'
  end as source
from ( -- accounting journal
  select a.control, d.journal_code, max(b.the_date) as the_date, sum(a.amount) as amount
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_date between current_Date - 365 and current_date--'06/22/2018'
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account in ( -- all new vehicle inventory account
      select b.account
      from fin.dim_account b 
      where b.account_type = 'asset'
        and b.department_code = 'nc'
        and b.typical_balance = 'debit'
        and b.current_row
        and b.account <> '126104')   
  inner join fin.dim_journal d on a.journal_key = d.journal_key
    and 
      case
        when a.control = 'G37876' then d.journal_code = 'CDH'
--         when a.control in ('G38401','G38402','G38403','G38404','G38405','G38406','G38407','G38408','G38409') then d.journal_code = 'EFT'
        else d.journal_code in ('PVI','CDH','GJE','EFT')
      end
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where a.post_status = 'Y'
  group by a.control, d.journal_code
  having sum(a.amount) > 10000) f
inner join arkona.xfm_inpmast g on f.control = g.inpmast_stock_number
  and g.current_row = true
  and g.year::integer > 2017 -- in ('2018', '2019','2020')
  -- bogus broker
  and g.inpmast_vin not in ('2GNAXUEVXL6151670','2GNAXUEV0L6147806','2GNAXUEV9L6147996','2GNAXUEV4L6146626',
        '2GNAXUEV4L6154256','2GNAXUEV8L6146550','2GNAXUEV8L6150291','2GNAXUEV9L6150932','2GNAXUEV0L6104339')
  and not (f.control = 'G40872' and journal_code = 'CDH') -- *a*
join (
  select a.*, null::citext as order_number 
  from tem.bad_actors a
  where stock_number like 'H%'
  union
  select *
  from tem.bad_actors_1) h on g.inpmast_Stock_number = h.stock_number  
-- where not exists (
--   select 1
--   from nc.vehicle_acquisitions
--   where stock_number = f.control) ;

select * from nc.stg_availability


-- update the ground date
update nc.stg_availability x
set ground_date = y.ground_date
from (
  select aa.stock_number, 
    coalesce(cc.keyper_date, coalesce(bb.ro_date, '12/31/9999'::date)) as ground_date
  from nc.stg_availability aa
  left join (
    select d.vin, min(b.the_date) as ro_date
    from ads.ext_fact_repair_order a
    inner join dds.dim_date b on a.opendatekey = b.date_key
    inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey -- needed for vin from ro
    inner join nc.stg_availability e on d.vin = e.vin
    -- as a "reminder" of a stop sale, joel wrote an ro on H13312 & H13313 (2815811,2815812) on 02/07,
    -- thereby signaling that they are HERE, but they won't be for a couple of months
    -- so, to keep them out of the inventory & feed, this seemed like the best place
    -- to catch as nc.vehicle_acquisitions drives all kinds of tables
    where a.ro not in ('2815811','2815812')
    group by d.vin) bb on aa.vin = bb.vin
    left join (
      select a.stock_number, min(transaction_date::date) as keyper_date
      from keys.ext_keyper a
      join nc.stg_availability b on a.stock_number = b.stock_number
-- *** double check the affect of this ***
      where 
        case
          when a.stock_number = 'G38187' then a.transaction_date::date > '10/01/2019'
          else 1 =1
        end       
      group by a.stock_number) cc on aa.stock_number = cc.stock_number 
  where aa.source <> 'ctp') y
where x.stock_number = y.stock_number
  and x.source <> 'unwind';  

-- delete the acquistions, vehicles can stay
delete
select *
from nc.vehicle_Acquisitions
where vin in (
  select a.vin
  from tem.bad_Actors a
  join nc.stg_availability b on a.stock_number = b.stock_number  
    and b.ground_date > current_Date);


no go ahead and rerun all_nc_inventory_nightly using this version of nc.stg_availability

-- ok wtf, why  14 left
select *
from nc.vehicle_acquisitions
where ground_date > current_Date

-- this is the shit that makes me nervous, how the fuck did this happen
select *
-- delete 
from nc.vehicle_acquisitions
where stock_number in ('G41364','G41425','G41430','G41432','G41433')
  and ground_date = '12/31/9999'


  
-- ok wtf, why  9 left
drop table if exists tem.bad_actors_2;
create table tem.bad_actors_2 as
select *
from nc.vehicle_acquisitions
where ground_date > current_Date;

-- fuck me, 2 rows in nc.vehicle_acquistions AGAIN for the honda units
-- the 4 gm units already have a record in acquisitions, need to delete those and recreate from availability
select *
from tem.bad_Actors_2 a
left join nc.vehicle_acquisitions b on a.stock_number = b.stock_number
order by a.stock_number

select *
-- delete 
from nc.vehicle_acquisitions
where stock_number in ('H14052','H14107','H14108','H14109','H14111','G41428','G41477','G41478','G41488')
  and ground_date = '12/31/9999'




-- lets do availability again with bad_actors_2
truncate nc.stg_availability;
insert into nc.stg_availability (the_date,journal_date,stock_number,
  vin,model_year,make, model,
  model_code,color_code,color, source)
select current_date, f.the_date, f.control, 
  g.inpmast_vin, g.year, g.make, g.model, g.model_code, 
  g.color_code, g.color,
  case f.journal_code
    when 'PVI' then 'factory'
    when 'CDH' then 'dealer trade'
    when 'GJE' then 'ctp'
    when 'EFT' then 'dealer trade'
  end as source
from ( -- accounting journal
  select a.control, d.journal_code, max(b.the_date) as the_date, sum(a.amount) as amount
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_date between current_Date - 365 and current_date--'06/22/2018'
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account in ( -- all new vehicle inventory account
      select b.account
      from fin.dim_account b 
      where b.account_type = 'asset'
        and b.department_code = 'nc'
        and b.typical_balance = 'debit'
        and b.current_row
        and b.account <> '126104')   
  inner join fin.dim_journal d on a.journal_key = d.journal_key
    and 
      case
        when a.control = 'G37876' then d.journal_code = 'CDH'
--         when a.control in ('G38401','G38402','G38403','G38404','G38405','G38406','G38407','G38408','G38409') then d.journal_code = 'EFT'
        else d.journal_code in ('PVI','CDH','GJE','EFT')
      end
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where a.post_status = 'Y'
  group by a.control, d.journal_code
  having sum(a.amount) > 10000) f
inner join arkona.xfm_inpmast g on f.control = g.inpmast_stock_number
  and g.current_row = true
  and g.year::integer > 2017 -- in ('2018', '2019','2020')
  -- bogus broker
  and g.inpmast_vin not in ('2GNAXUEVXL6151670','2GNAXUEV0L6147806','2GNAXUEV9L6147996','2GNAXUEV4L6146626',
        '2GNAXUEV4L6154256','2GNAXUEV8L6146550','2GNAXUEV8L6150291','2GNAXUEV9L6150932','2GNAXUEV0L6104339')
  and not (f.control = 'G40872' and journal_code = 'CDH') -- *a*
join tem.bad_actors_2 h on g.inpmast_Stock_number = h.stock_number  



-- update the ground date
update nc.stg_availability x
set ground_date = y.ground_date
from (
  select aa.stock_number, 
    coalesce(cc.keyper_date, coalesce(bb.ro_date, '12/31/9999'::date)) as ground_date
  from nc.stg_availability aa
  left join (
    select d.vin, min(b.the_date) as ro_date
    from ads.ext_fact_repair_order a
    inner join dds.dim_date b on a.opendatekey = b.date_key
    inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey -- needed for vin from ro
    inner join nc.stg_availability e on d.vin = e.vin
    -- as a "reminder" of a stop sale, joel wrote an ro on H13312 & H13313 (2815811,2815812) on 02/07,
    -- thereby signaling that they are HERE, but they won't be for a couple of months
    -- so, to keep them out of the inventory & feed, this seemed like the best place
    -- to catch as nc.vehicle_acquisitions drives all kinds of tables
    where a.ro not in ('2815811','2815812')
    group by d.vin) bb on aa.vin = bb.vin
    left join (
      select a.stock_number, min(transaction_date::date) as keyper_date
      from keys.ext_keyper a
      join nc.stg_availability b on a.stock_number = b.stock_number
-- *** double check the affect of this ***
      where 
        case
          when a.stock_number = 'G38187' then a.transaction_date::date > '10/01/2019'
          else 1 =1
        end       
      group by a.stock_number) cc on aa.stock_number = cc.stock_number 
  where aa.source <> 'ctp') y
where x.stock_number = y.stock_number
  and x.source <> 'unwind';  

select * from nc.vehicle_acquisitions where ground_date > current_Date

-- go ahead and process the nc.stg_Availability

-- open orders gets fucked up because these are in nc.vehicles
-- so, fuck it get rid of them
-- vehicles with no record in acquisitions or sales and with an active order
delete 
-- select * 
from nc.vehicles
where vin in (
  select a.vin --, b.stock_number, bb.stock_number, c.order_number, d.event_code, e.*
  from nc.vehicles a
  left join nc.vehicle_acquisitions b on a.vin = b.vin
  left join nc.vehicle_Sales bb on a.vin = bb.vin
  join jo.vehicle_orders c on a.vin = c.vin
  left join (
    select order_number, max(event_code) as event_code
    from jo.vehicle_order_events 
    group by order_number) d on c.order_number = d.order_number
  left join nc.open_orders e on a.vin = e.vin  
  where b.vin is null);

