﻿select *
from nc.vehicle_acquisitions a
left join nc.vehicles b on a.vin = b.vin
where stock_number in ('H12347','H11623','H12466','H12828','H13026','H11734')   maybe H13026 & H13864 as well, based on inpmast


select * 
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
  and b.model = 'CR-V'
where stock_number like 'H%'
  and sale_type = 'dealer trade'
order by delivery_date desc   

-- these 3, H11734, H11623, H12347 sale was done against EFT rather than VSN
select a.control, f.inpmast_vin, e.description, a.amount, b.the_date, g.ground_date
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
--   and b.the_date between current_Date - 40 and current_date
join fin.dim_account c on a.account_key = c.account_key
  and c.account in ( -- all new vehicle inventory account
    select b.account
    from fin.dim_account b 
    where b.account_type = 'asset'
      and b.department_code = 'nc'
      and b.typical_balance = 'debit'
      and b.current_row
      and b.account <> '126104')    
join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code in ('EFT')  --VSN
join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
left join arkona.xfm_inpmast f on a.control = f.inpmast_stock_number
  and f.current_row = true  
left join nc.vehicle_acquisitions g on a.control = g.stock_number
  and g.thru_date = ( -- need most recent acquisition
    select max(thru_date)
    from nc.vehicle_acquisitions
    where stock_number = g.stock_number)  
where a.post_status = 'Y'
  and abs(a.amount) > 10000
  and f.year::integer > 2017
  and a.control not in( 'G38994','g39972')
  and not exists (
    select 1
    from nc.vehicle_sales
    where stock_number = a.control
      and ground_date = g.ground_date) ;


select a.*, b.inpmast_stock_number, status
from nc.vehicle_acquisitions a
left join arkona.ext_inpmast b on a.vin = b.inpmast_vin
where a.thru_date > current_Date
  and b.status = 'C'
order by a.stock_number  




        
drop table if exists tem.nc_inventory_balance;
create table tem.nc_inventory_balance as 
select a.control, sum(a.amount) as inv_balance
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
join fin.dim_account c on a.account_key = c.account_key
  and c.account in ( -- all new vehicle inventory accounts
    select b.account
    from fin.dim_account b 
    where b.account_type = 'asset'
      and b.department_code = 'nc'
      and b.typical_balance = 'debit'
      and b.current_row
      and b.account <> '126104')  
group by a.control;
create unique index on tem.nc_inventory_balance(control);


select aa.*, bb.*, cc.*
from nc.vehicle_acquisitions aa
left join tem.nc_inventory_balance bb on aa.stock_number = bb.control
left join arkona.ext_inpcmnt cc on aa.vin = cc.vin
where aa.thru_date > current_date  
  and bb.inv_balance = 0
order by aa.stock_number, cc.sequence_number  


SELECT * FROM nc.vehicle_acquisitions where stock_number = 'g39972'

select * from nc.vehicle_sales limit 10

drop table if exists acq;
create temp table acq as 
select * from nc.vehicle_acquisitions;

select * from acq limit 10

update nc.vehicle_acquisitions x
set thru_date = y.the_date
from (
  select a.*
  from tem.dt a
  join nc.vehicle_acquisitions b on a.stock_number = b.stock_number
    and b.thru_date > current_date
  where a.status = 'dt') y
where x.stock_number = y.stock_number

insert into nc.vehicle_sales
  select a.stock_number, b.ground_date, a.vin, -1, a.the_date, 'dealer trade', false
  from tem.dt a
  join nc.vehicle_acquisitions b on a.stock_number = b.stock_number
  where a.status = 'dt'