﻿select *
from dao.veh_trim_styles 
where year = 2019
  and mfr_model_num = 'CK10543'

select *
from dao.vin_reference
where year = 2019
  and mfr_model_num in ( 'CK10543', 'ck10743')
  

-- looks like mfr_package_code = PEG, yep, its null for honda &  nissan
select year, make, model, mfr_model_num, trim, body_subtype, drive_type, engine_size, bed_length, mfr_package_code, style
from dao.vin_reference
where year = 2019
  and make in ('chevrolet','gmc','buick','cadillac','honda','nissan')
group by year, make, model, mfr_model_num, trim, body_subtype, drive_type, engine_size, bed_length, mfr_package_code, style


select year, mfr_model_num, mfr_package_code
from dao.vin_reference
where year = 2019
  and make in ('chevrolet','gmc','buick','cadillac')
group by year, mfr_model_num, mfr_package_code


select * from nc.vehicle_configurations

-- ehh, spotty at best
-- blazers don't match because of how i generate trim in configs
-- clean this up, full outer join on subquieries
select *
from  (
select year, make, model, mfr_model_num, trim, body_subtype, drive_type, engine_size, bed_length, mfr_package_code, style
from dao.vin_reference
where year = 2019
  and make in ('chevrolet','gmc','buick','cadillac','honda','nissan')
group by year, make, model, mfr_model_num, trim, body_subtype, drive_type, engine_size, bed_length, mfr_package_code, style) a
full outer join nc.vehicle_configurations b on a.year = b.model_year
  and a.make = b.make
  and a.model = b.model
  and a.mfr_model_num = b.model_code
  and a.trim = b.trim_level
  and a.engine_size::citext = b.engine
where b.model_year = 2019


-- not bad,  but quite a few differences with how trim is designated
-- eg 2019 Cadillac CT5 6KJ69 dao:3.6L Premium Luxury chrome:Premium Luxury AWD
select *
from  (
select a.year, a.make, a.model, a.mfr_model_num, a.trim, a.body_subtype, a.drive_type, a.engine_size, a.bed_length, a.mfr_package_code, a.style
from dao.vin_reference a
join dao.veh_trim_styles b on a.vehicle_id = b.vehicle_id
  and b.fleet = 'N'
where a.year = 2019
  and a.make in ('chevrolet','gmc','buick','cadillac','honda','nissan')
group by a.year, a.make, a.model, a.mfr_model_num, a.trim, a.body_subtype, a.drive_type, a.engine_size, a.bed_length, a.mfr_package_code, a.style) a
full outer join (
  select *
  from nc.vehicle_configurations 
  where model_year = 2019) b on a.year = b.model_year
  and a.make = b.make
  and a.model = b.model
  and a.mfr_model_num = b.model_code
  and a.trim = b.trim_level
  and a.engine_size::citext = b.engine

select * from dao.vin_reference limit 10


