﻿select a.order_number, a.order_type, a.vin, a.model_year, a.alloc_group, 
  a.make, a.model_code, a.peg,
  c.*, b.from_date, b.thru_date
from gmgl.vehicle_orders a
left join gmgl.vehicle_order_events b on a.order_number = b.order_number
left join gmgl.vehicle_event_codes c on b.event_code = c.code
where a.peg is not null 
order by a.order_number, b.from_Date


select a.order_number, c.category, min(from_date) as from_date
from gmgl.vehicle_orders a
left join gmgl.vehicle_order_events b on a.order_number = b.order_number
left join gmgl.vehicle_event_codes c on b.event_code = c.code
where a.peg is not null 
group by a.order_number, c.category
order by a.order_number, b.from_Date


select order_number, vin,
  case when category = 'Placed' then from_date end as placed,
  case when category = 'In System' then from_date end as in_system,
  case when category = 'In Transit' then from_date end as in_transit,
  case when category = 'In Stock' then from_date end as in_stock
from (
  select a.order_number, a.vin, c.category, min(from_date) as from_date
  from gmgl.vehicle_orders a
  left join gmgl.vehicle_order_events b on a.order_number = b.order_number
  left join gmgl.vehicle_event_codes c on b.event_code = c.code
  where a.peg is not null 
  group by a.order_number, c.category) d
order by order_number


select a.order_number, a.vin, make, alloc_group,
  min(case when category = 'Placed' then from_date end) as placed,
  min(case when category = 'In System' then from_date end) as in_system,
  min(case when category = 'In Transit' then from_date end) as in_transit,
  min(case when category = 'In Stock' then from_date end) as in_stock
-- select *
from gmgl.vehicle_orders a
left join gmgl.vehicle_order_events b on a.order_number = b.order_number
left join gmgl.vehicle_event_codes c on b.event_code = c.code
group by a.order_number
order by a.order_number

the primary event_code for invoice is 4150, that is not being captured

select * from gmgl.vehicle_order_events where event_code = '4150'

drop table if exists order_lead_time;
create temp table order_lead_time as
select aa.*, bb.source, bb.ground_date, 
  bb.ground_date - coalesce(aa.placed, coalesce(aa.in_system, coalesce(aa.in_transit, aa.in_stock))) lead_time
from (
  select a.order_number, a.vin, make, alloc_group,
    min(case when category = 'Placed' then from_date end) as placed,
    min(case when category = 'In System' then from_date end) as in_system,
    min(case when category = 'In Transit' then from_date end) as in_transit,
    min(case when category = 'In Stock' then from_date end) as in_stock  
  from gmgl.vehicle_orders a
  left join gmgl.vehicle_order_events b on a.order_number = b.order_number
  left join gmgl.vehicle_event_codes c on b.event_code = c.code
  where a.peg is not null 
  group by a.order_number) aa
left join nc.vehicle_acquisitions bb on aa.vin = bb.vin
  and bb.source not in ('ctp', 'unwind')
where bb.source <> 'dealer trade'  
  and (aa.placed is not null and aa.in_system is not null);
-- order by aa.alloc_group, bb.ground_date - coalesce(aa.placed, coalesce(aa.in_system, coalesce(aa.in_transit, aa.in_stock)));

select a.alloc_group, b.year_month, round(avg(lead_time), 0), count(*)
from order_timeline a
join dds.dim_date b on a.ground_date = b.the_date
group by a.alloc_group, b.year_month
order by a.alloc_group, b.year_month

-- so whats ups with trax, 
-- acquisitions show jan:3, mar: 2, apr: 1
select a.stock_number, a.vin, a.ground_date, a.source
from nc.vehicle_acquisitions a
join nc.vehicles b on a.vin = b.vin
  and b.model = 'trax'
order by ground_date  

select * 
from gmgl.vehicle_orders
where vin = 'KL7CJPSB1KB811313'



-- alloc groups
select make, alloc_group, count(*) from gmgl.vehicle_orders where make is not null group by make, alloc_Group order by alloc_Group