﻿select *
from nc.year_make_models
order by make, model


select make, model, array_agg(model_year)
from nc.year_make_models
group by make, model


select make, model, color
from nc.model_colors
where model_year = 2018
union
select make, model, color
from nc.model_colors
where model_year = 2019
order by  make, model, color

select make, model, array_agg(level_header), array_agg(model_year)
from nc.model_levels
group by make, model



    select 
--       model_year,
      json_agg(row_to_json(C) order by seq) as levels
    from ( -- C level
      select a.make, a.model, 
--         a.model_year, 
        a.seq, a.level_header, sold_90, sold_365,
        inv_og, inv_transit, inv_system, b.colors					
      from nc.model_level_stats a
      left join ( -- B color 
        select model_year, make, model, level_seq, level_header, 
          json_agg(json_build_object('color',color,'seq', color_seq,'sold_90',sold_90,
          'sold_365', sold_365, 'inv_og',inv_og,'inv_transit',inv_transit,
          'inv_system',inv_system)order by color_seq) as colors
        from nc.model_level_color_stats						
--         where model_year in (select years::integer from json_array_elements_text(_model_year) as years)
--           and make = _make			
        where make = 'chevrolet'			
          and model = 'equinox'
          group by model_year, make, model, level_seq, level_header) b  on a.model_year = b.model_year
            and a.make = b.make
            and a.model = b.model
            and a.level_header = b.level_header
--       where a.model_year in (select years::integer from json_array_elements_text(_model_year) as years)			
--         and a.make = _make		
      where a.make = 'chevrolet'			
        and a.model ='equinox') C
--     group by model_year
  order by model_year desc