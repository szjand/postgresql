﻿select * from nc.vehicle_configurations
where model = 'sequoia'
  and model_year = 2024

-- this is where i fucked up, update with no where clause
update nc.vehicle_configurations
set config_type = 'te' , level_header = 'Platinum 3.4'


select max(configuration_id) from nc.vehicle_configurations
-- these 2 don't exist in the backup
2521
2522

-- populated from the restored backup on pg_standby (209), copied the query over with winscp
CREATE TABLE nc.vehicle_configurations_backup
(
  model_year integer NOT NULL,
  make citext NOT NULL,
  model citext NOT NULL,
  model_code citext NOT NULL,
  trim_level citext NOT NULL,
  cab citext NOT NULL DEFAULT 'N/A'::citext,
  drive citext NOT NULL,
  engine citext NOT NULL,
  chrome_style_id citext,
  alloc_group citext,
  configuration_id serial NOT NULL,
  box_size citext NOT NULL,
  name_wo_trim citext NOT NULL,
  config_type citext,
  level_header citext,
  CONSTRAINT vehicle_configurations_backup_pkey PRIMARY KEY (configuration_id)
)

-- looks good, only 2521 & 2522 do not exist in the backup
select a.configuration_id, a.config_type, a.level_header, b.configuration_id, b.config_type, b.level_header
from nc.vehicle_configurations a
left join nc.vehicle_configurations_backup b on a.configuration_id = b.configuration_id
order by a.configuration_id

-- worked
update nc.vehicle_configurations a
set config_type = b.config_type, 
    level_header = b.level_header
from (
  select configuration_id, config_type, level_header
  from nc.vehicle_configurations_backup) b
where a.configuration_id = b.configuration_id      


select * from nc.vehicle_configurations where configuration_id in (2521,2522)

select * from nc.vehicle_configurations
where model = 'sierra 1500'
  and model_year = 2024

update nc.vehicle_configurations
set config_type = 'tcbe', level_header = 'Denali crew Standard 3.0'
where configuration_id = 2521;

select * from nc.vehicle_configurations
where model = 'escalade'
  and model_year = 2023

update nc.vehicle_configurations
set config_type = 't', level_header = '4WD Sport Platinum'
where configuration_id = 2522; 