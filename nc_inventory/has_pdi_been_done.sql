﻿select b.stock_number, a.vin, a.model_year, a.make, a.model, b.ground_Date
from nc.vehicles a
join nc.vehicle_acquisitions b on a.vin = b.vin
  and b.ground_Date < current_date
  and b.thru_Date > current_date
left join ads.ext_dim_vehicle c on a.vin = c.vin
left join ads.ext_fact_repair_order d on c.vehiclekey = d.vehiclekey
left join ads.ext_dim_opcode e on  d.opcodekey = c.opcodekey

select c.*
from ads.ext_fact_repair_order a
join ads.ext_dim_vehicle b on a.vehiclekey = b.vehiclekey
  and b.vin = '1GYS4CKJ0LR164931'
join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey

drop table if exists wtf;
create temp table wtf as
select b.stock_number, a.vin, a.model_year, a.make, a.model, string_agg(e.opcode, ',') as opcodes, b.source
from nc.vehicles a
join nc.vehicle_acquisitions b on a.vin = b.vin
  and b.ground_Date < current_date
  and b.thru_Date > current_date
left join ads.ext_dim_vehicle c on a.vin = c.vin
left join ads.ext_fact_repair_order d on c.vehiclekey = d.vehiclekey
left join ads.ext_dim_opcode e on  d.opcodekey = e.opcodekey
  and e.opcode <> '*'
-- where a.vin = '1GYS4CKJ0LR164931'
group by b.stock_number, a.vin, a.model_year, a.make, a.model, b.source
order by a.make, a.model

select *
from wtf
where opcodes is null

select * from nc.vehicle_Acquisitions limit 100

select * from ads.ext_dim_opcode limit 10

select e.storecode, e.opcode, e.description, count(*)
from nc.vehicles a
join nc.vehicle_acquisitions b on a.vin = b.vin
  and b.ground_Date < current_date
  and b.thru_Date > current_date
left join ads.ext_dim_vehicle c on a.vin = c.vin
left join ads.ext_fact_repair_order d on c.vehiclekey = d.vehiclekey
left join ads.ext_dim_opcode e on  d.opcodekey = e.opcodekey
  and e.opcode <> '*'
group by e.storecode, e.opcode, e.description
order by storecode, count(*) desc

RY1 opcodes
PULSE
PDI
NITROPDI
PS1 (total care)
MFPDI (mud flaps)
RY2 opcodes
PULSE
PS2 (total care)
PDI (honda)
NPDI (nissan)
MUD4 (mud flaps)

select b.stock_number, a.vin, a.model_year, a.make, a.model, b.source,
  max(case when e.opcode = 'pulse' then 'X' end) as pulse,
  max(case when e.opcode in ('pdi','npdi') then 'X' end) as pdi,
  max(case when e.opcode in ('ps1','ps2') then 'X' end) as total_care,
  max(case when e.opcode in ('mfpdi','mud4') then 'X' end) as mud_flaps
from nc.vehicles a
join nc.vehicle_acquisitions b on a.vin = b.vin
  and b.ground_Date < current_date
  and b.thru_Date > current_date
left join ads.ext_dim_vehicle c on a.vin = c.vin
left join ads.ext_fact_repair_order d on c.vehiclekey = d.vehiclekey
left join ads.ext_dim_opcode e on  d.opcodekey = e.opcodekey
  and e.opcode <> '*'
  and e.opcode in ('pulse','pdi','ps1','mfpdi','ps2','npdi','mud4')
group by b.stock_number, a.vin, a.model_year, a.make, a.model, b.source
order by make, model