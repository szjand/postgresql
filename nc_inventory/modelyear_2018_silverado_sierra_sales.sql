﻿
drop table if exists nc.model_year_2018_1500_sales cascade;

CREATE TABLE nc.model_year_2018_1500_sales
(
  vin citext NOT NULL primary key,
  delivery_date date NOT NULL,
  model_year citext NOT NULL,
  make citext not null,
  model citext NOT NULL,
  color citext,
  body_style citext)
WITH (
  OIDS=FALSE
);
ALTER TABLE nc.silverado_sales
  OWNER TO postgres;
COMMENT ON TABLE nc.silverado_sales
  IS 'all sales of model year 2018 silverado 1500 and sierra 1500 sold retail from bopmast/inpmast';


insert into nc.model_year_2018_1500_sales
select a.vin, max(a.delivery_date) as delivery_date, b.year, 
  b.make, b.model, b.color, b.body_style-- select *
from sls.ext_bopmast_partial a
inner join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row = true
  and b.model like '%1500%'
  and b.make in ('chevrolet','gmc')
where b.year = 2016
  and a.sale_type in ('R','L')
group by a.vin, b.year, b.make, b.model, b.color, b.body_style;


drop table if exists nc.silverado_sierra_chrome_attributes cascade;
create table nc.silverado_sierra_chrome_attributes (
  vin citext primary key,
  best_style_name citext,
  engine_displacement citext,
  driving_wheels citext,
  style_name citext,
  alt_style_name citext,
  drive_train citext);
--   color_code citext,
--   color_name citext);

comment on column nc.silverado_sierra_chrome_attributes.best_style_name is 'attributes::bestStyleName';  
comment on column nc.silverado_sierra_chrome_attributes.engine_displacement is 'engine::displacement::value::$value';
comment on column nc.silverado_sierra_chrome_attributes.driving_wheels is 'vinDescription::attributes::drivingWheels';
comment on column nc.silverado_sierra_chrome_attributes.style_name is 'style::attributes::name';
comment on column nc.silverado_sierra_chrome_attributes.alt_style_name is 'style::attributes::altStyleName';
comment on column nc.silverado_sierra_chrome_attributes.drive_train is 'style::attributes::drivetrain';
comment on table nc.silverado_sierra_chrome_attributes is 'populated from chrome web service, describe-vehicle request, end point: http://beta.rydellvision.com:8888/chrome/describe-vehicle/"vin"';
-- comment on column nc.sierra_chrome_attributes.color_code is 'exteriorColor::attributes::colorCode';
-- comment on column nc.sierra_chrome_attributes.best_style_name is 'exteriorColor::attributes::colorName';

drop table if exists nc.inventory_chrome_attributes cascade;
create table nc.inventory_chrome_attributes (
  vin citext primary key,
  best_style_name citext,
  engine_displacement citext,
  driving_wheels citext,
  style_name citext,
  alt_style_name citext,
  drive_train citext);

  
drop table if exists base;
create temp table base as
select c.year_month, a.delivery_date, a.make, a.model, a.vin,
  case
    when b.alt_style_name like 'Reg%' then 'reg'
    when b.alt_style_name like 'doub%' then 'double'
    when b.alt_style_name like 'crew%' then 'crew'
  end as cab,
  case 
    when b.best_style_name like '%LS' then 'LS'
    when b.best_style_name like '%work%' then 'WT'
    when b.best_style_name like '%denali' then 'Denali'
    when b.best_style_name like '%Country' then 'High Country'
    when b.best_style_name like '%1LT' then '1LT'
    when b.best_style_name like '%2LT' then '2LT'
    when b.best_style_name like '%1LZ' then '1LZ'
    when b.best_style_name like '%2LZ' then '2LZ'
    when b.best_style_name like '%SLE' then 'SLE'
    when b.best_style_name like '%SLT' then 'SLT'
    when b.best_style_name like '%1LZ' then '1LZ'
    when b.best_style_name like '%2LZ' then '2LZ'
  end as trim,
  a.color, engine_displacement as engine
-- select *  
from nc.model_year_2018_1500_sales a
left join nc.silverado_sierra_chrome_attributes b on a.vin = b.vin
left join dds.dim_date c on a.delivery_date = c.the_date
where model_year = '2016';


select replace(model, ' 1500','') as model, cab, trim, coalesce(color, 'unknown') as color, engine as engine, count(*) as sales
from base
group by model, cab, trim, coalesce(color, 'unknown'), engine
order by count(*) desc

select replace(model, ' 1500','') as model, cab, trim, coalesce(color, 'unknown') as color, engine as engine, count(*) as sales
from base
group by model, cab, trim, coalesce(color, 'unknown'), engine
order by model, cab, trim, coalesce(color, 'unknown'), engine

select replace(model, ' 1500','') || ' ' || cab || ' ' || trim || ' ' || coalesce(color, 'unknown') || ' ' || engine, count(*)
from base
group by replace(model, ' 1500','') || ' ' || cab || ' ' || trim || ' ' || coalesce(color, 'unknown') || ' ' || engine
order by count(*) desc 

select * from nc.model_year_2018_1500_sales where color is null 

select *
from arkona.xfm_inpmast
where inpmast_vin in ('3GCUKREC7JG333375','3GCUKREC8JG287796','3GCUKSECXJG175296')



-- current inventory
select replace(model, ' 1500',''), cab, trim, coalesce(color, 'unknown'), engine, count(*)
from (
select b.inpmast_vin, b.year, 
  b.make, b.model, b.color, b.body_style
-- select inpmast_vin
from arkona.xfm_inpmast b 
  where b.current_row = true
  and b.model like '%1500%'
  and b.year = 2018
  and b.status = 'I') a
left join nc.inventory_chrome_attributes b on a.inpmast_vin = b.vin  
group by model, cab, trim, coalesce(color, 'unknown'), engine  


drop table if exists base_inventory;
create temp table base_inventory as
select a.make, a.model, a.inpmast_vin,
  case
    when b.alt_style_name like 'Reg%' then 'reg'
    when b.alt_style_name like 'doub%' then 'double'
    when b.alt_style_name like 'crew%' then 'crew'
  end as cab,
  case 
    when b.best_style_name like '%LS' then 'LS'
    when b.best_style_name like '%work%' then 'WT'
    when b.best_style_name like '%denali' then 'Denali'
    when b.best_style_name like '%Country' then 'High Country'
    when b.best_style_name like '%1LT' then '1LT'
    when b.best_style_name like '%2LT' then '2LT'
    when b.best_style_name like '%1LZ' then '1LZ'
    when b.best_style_name like '%2LZ' then '2LZ'
    when b.best_style_name like '%SLE' then 'SLE'
    when b.best_style_name like '%SLT' then 'SLT'
    when b.best_style_name like '%1LZ' then '1LZ'
    when b.best_style_name like '%2LZ' then '2LZ'
  end as trim,
  a.color, engine_displacement as engine
-- select *  
from (
select b.inpmast_vin, b.year, 
  b.make, b.model, b.color, b.body_style
-- select inpmast_vin
from arkona.xfm_inpmast b 
  where b.current_row = true
  and b.model like '%1500%'
  and b.year = 2017
  and b.status = 'I') a
left join nc.inventory_chrome_attributes b on a.inpmast_vin  = b.vin



select replace(model, ' 1500',''), cab, trim, coalesce(color, 'unknown'), engine, count(*)
from base_inventory
group by model, cab, trim, coalesce(color, 'unknown'), engine
order by count(*) desc 

-- sales with current inventory
select a.*, coalesce(units, 0) as units
from (
select replace(model, ' 1500','') as model, cab, trim, coalesce(color, 'unknown') as color, engine, count(*) as sales
from base
group by model, cab, trim, coalesce(color, 'unknown'), engine) a
left join (
select replace(model, ' 1500','') as model, cab, trim, coalesce(color, 'unknown') as color, engine, count(*) as units
from base_inventory
group by model, cab, trim, coalesce(color, 'unknown'), engine) b on a.model = b.model and a.cab = b.cab and a.trim = b.trim and a.color = b.color and a.engine = b.engine
order by sales desc 

select a.*, coalesce(b.sales, 0) as sales
from (
select replace(model, ' 1500','') as model, cab, trim, coalesce(color, 'unknown') as color, engine, count(*) as units
from base_inventory
group by model, cab, trim, coalesce(color, 'unknown'), engine) a 
left join (
select replace(model, ' 1500','') as model, cab, trim, coalesce(color, 'unknown') as color, engine, count(*) as sales
from base
group by model, cab, trim, coalesce(color, 'unknown'), engine) b on a.model = b.model and a.cab = b.cab and a.trim = b.trim and a.color = b.color and a.engine = b.engine
order by units desc 


on a.model = b.model and a.cab = b.cab and a.trim = b.trim and a.color = b.color and a.engine = b.engine
order by sales desc 





