﻿/*
10/3/19
taylor calls and says, G38104 & G38105 show as here on the inventory page, but i know they are not

for dealer trades, here (on ground) is determined by
  all_nc_inventory_nightly.sql: dealer trades: ground = coalesce(keyper, coaelsece dt ro, journal date)

well, the 2 vehicles she mentioned were purchased thru a locator (dealer trade brokered by a 3rd party)
the vehicles are in accounting, but are not here yet

due to inventory shorts and GM strike (consensus cancelled), taylor is getting lots of vehicles thru locators

so, 

this led me to discover that the keyper element is being generated from table ads.keyper_creation_dates
WHICH IS NO FUCKING LONGER BEING UPDATED FOR OVER A YEAR

so, need to fix that, and at taylor's request, remove the acct element from the on ground determination
for dealer trades
*/

select * from ads.keyper_creation_dates order by creation_date desc 


select *  
from keys.ext_keyper
where stock_number like 'G381%'

create index on keys.ext_keyper(stock_number)


-- dealer trade acquisitions without a keyper date (all based on accounting_
select *
from nc.vehicle_acquisitions a
where thru_date > current_date
  and source = 'dealer trade'
  and not exists (
    select 1
    from keys.ext_keyper
    where stock_number = a.stock_number)

-- ok, here are the dt acquisitions that should not be on ground yet
drop table if exists bad_dt;
create temp table bad_dt as
select a.*, b.created_date, c.the_date
from nc.vehicle_acquisitions a
left join (
  select stock_number, created_date
  from keys.ext_keyper
  where created_date > current_date - 180
  group by stock_number, created_date) b on a.stock_number = b.stock_number
left join (
  select d.vin, min(b.the_date) as the_Date
  from ads.ext_fact_repair_order a
  inner join dds.dim_date b on a.opendatekey = b.date_key
    and b.the_date > current_date - 365
--     inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
  inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
--   inner join nc.stg_availability e on d.vin = e.vin
  group by d.vin) c on a.vin = c.vin
where a.thru_date > current_date
  and a.source = 'dealer trade'
  and b.created_date is null 
  and c.the_date is null
  and a.stock_number not like 'H%'
order by a.stock_number

-- ok, need to whack em
delete
-- select *
from nc.vehicle_acquisitions
where stock_number in (
  select stock_number 
  from bad_dt)

inventory page should be ok with just whacking acquisitions

create table jon.bad_dt
as 
select * from bad_dt

select * from jon.bad_dt


need to fix
  all_nc_inventory_nightly.sql
  Function: nrv.get_vehicle_inventory_new()

-- this will expose any other uses
alter table ads.keyper_creation_dates
rename to z_unused_keyper_creation_dates;

10/4/19
fuck, they are still on the inventory page as here

first, fix all_nc_inventory_nightly
need to eliminate the journal_date & replace ads.keyper_creation_dates with keys.ext_keyper

now, no need to separate ground date determintation between factory and dealer trade

select * from nc.stg_availability


keyper: creation date at honda is goofy, use min(transaction_date)

  select aa.stock_number, 
    coalesce(cc.keyper_date, coalesce(bb.ro_date, '12/31/9999'::date)) as ground_date
  from nc.stg_availability aa
  left join (
    select d.vin, min(b.the_date) as ro_date
    from ads.ext_fact_repair_order a
    inner join dds.dim_date b on a.opendatekey = b.date_key
    inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey -- needed for vin from ro
    inner join nc.stg_availability e on d.vin = e.vin
    group by d.vin) bb on aa.vin = bb.vin
    left join (
      select a.stock_number, min(transaction_date::date) as keyper_date
      from keys.ext_keyper a
      join nc.stg_availability b on a.stock_number = b.stock_number
      group by a.stock_number) cc on aa.stock_number = cc.stock_number 
  where aa.source <> 'ctp'