﻿
-- new_inventory_V1: populate bens spreadsheet page



/*
FACTS
  chrome_style_id: does not distinguish between engines, neither does PEG
GM model codes:
  Example code: CK25903 is a Chevrolet, Four wheel drive, Regular cab Long Box.
  C/T: Identifies the brand/manufacturer. Chevrolet © or GMC (T)
  C/K: Identifies drivetrain. Two wheel drive (C) or four wheel drive (K)
  15/25/35/36: Identifies truck "Series". 1/2 ton trucks (15, 10 for 2019), 3/4 ton trucks (25), 1 ton trucks (35) 
    and 1 ton chassis cabs (36. These are the full on chassis cabs, rather than a "box delete" truck. Box deletes are 15/25/35 designation).
  5/7/9: Identifies box size. 5ft8in "Short" box (5), 6ft6in "Standard" box (7) 8ft "Long" box (9)
  03/53/43: Identifies cab configuration. Regular cab (03), Double/Ext. Cab (53), Crew cab (43)
  
Blazer trim, per ben show configs with trim of: L, 1LT, 2Lt, 3LT, RS, Premier
  Blazer configurations per ben: L, 1LT, 2Lt, 3LT, RS, Premier
  Trim Level:	L **	      Blazer	                                        RS	                Premier
  Trim Code:	1L0	        1LT       	2LT	              3LT	              1RS	                0HD
  Model Code:	1NH26	      1NK26	      1NK26 / 1NR26	    1NK26 / 1NR26	    1NL26 / 1NS26	      1NM26 / 1NT26
  Powertrain:	2.5L I4 FWD	2.5L I4 FWD	3.6L V6 FWD & AWD	3.6L V6 FWD & AWD	3.6L V6 FWD & AWD	  3.6L V6 FWD & AWD
  Seat Trim:	Cloth	      Cloth	      Cloth	   

converted nc.vehicle_configurations, nc.vehicles, nc.daily_inventory: changed trim_level WT to Work Truck, to be
consistent with how data is currently being generated
both chrome and dataone use "Work Truck" as the trim_level

the one non unique model code is CK15753, it applies to both 2018 Silverado 1500 and 2019 Silverado 1500 LD

*/

/*
spec questions
  should box size be a configuration parameter for pickups
    12/27: ben: yes
  should sales be retail only 
    12/27: ben: yes
backons: 
  vehicle sold multiple times in the past 365, which one counts?
  12/31: ben, only the most recent sale is of interest, back on negates the sale

1/18/19
inventory in_transit/in_system: only order type TRE  
*/  

/*
TODO
COLORS
  -- ok, there are obviously color discrepancies
  select color, count(*) from nc.vehicles group by color order by color

  select a.model_year, a.make, a.model, string_agg(distinct upper(b.color), ',' order by upper(b.color))
  from nc.vehicle_configurations a
  join nc.vehicles b on a.model_year = b.model_year
    and a.make = b.make
    and a.model = b.model
  where a.model_year > 2017  
  group by a.model_year, a.make, a.model

ORDERS WITHOUT VINS
PICK UP BED SIZE
  select vin, 
    u.cab->>'$value' as cab,
    t. bed->>'$value' as bed
  from chr.describe_vehicle 
  join jsonb_array_elements(response->'style') as r(style) on true
  left join jsonb_array_elements(r.style->'bodyType') with ordinality as t(bed) on true
    and t.ordinality =  1
  left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
    and u.ordinality =  2
  where vin in ('1N6AA1EJXKN513834','1N6AA1F47KN511189')

INACCURATE DATA
  1/7/18: 3 of these 5 have long been sold
  select *
  from nc.vehicle_acquisitions a
  join nc.vehicles b on a.vin = b.vin
  where b.model_year = 2018
    and b.model = 'equinox'
    and a.thru_date > current_date

DOUBLE CHECK  
  gm trucks: nc.vehicle_configuration values vs decoding the model_code

NEED TO CHANGE the schema of jon.describe_vehicle  
*/

-- How to read the GM model code:
 
Example code: CK25903 is a Chevrolet, Four wheel drive, Regular cab Long Box.
 
C/T: Identifies the brand/manufacturer. Chevrolet © or GMC (T)
 
C/K: Identifies drivetrain. Two wheel drive (C) or four wheel drive (K)
 
15/25/35/36: Identifies truck "Series". 1/2 ton trucks (15, 10 for 2019), 3/4 ton trucks (25), 1 ton trucks (35) and 1 ton chassis cabs (36. These are the full on chassis cabs, rather than a "box delete" truck. Box deletes are 15/25/35 designation).
 
5/7/9: Identifies box size. 5ft8in "Short" box (5), 6ft6in "Standard" box (7) 8ft "Long" box (9)
 
03/53/43: Identifies cab configuration. Regular cab (03), Double/Ext. Cab (53), Crew cab (43)

-- daily data
-- limit open orders  to order_type = TRE (retail stock)

/*

4/3/19
put this in a function
nc.update_daily_inventory
called from all_nc_inventory_nightly.sql
*/

insert into nc.daily_inventory  
select *
from (
  -- sale_90
  select current_date -1, c.stock_number, c.vin, b.color, a.model_year,
  a.make, a.model, a.model_code, a.trim_level, a.cab, a.drive, a.engine, a.chrome_style_id,
  a.configuration_id, 'sold_90' as status, 'none' as order_number
  from nc.vehicle_configurations a
  join nc.vehicles b on a.configuration_id = b.configuration_id
  join nc.vehicle_sales c on b.vin = c.vin
    and c.sale_type = 'retail'
    and c.delivery_date = (-- only the most recent sale
      select max(delivery_date)
      from nc.vehicle_sales
      where vin = c.vin)    
  where c.delivery_date between current_date - 91 and current_date - 1 and stock_number <> 'h14167'
  union all
  -- sale_365
  select current_date -1, c.stock_number, c.vin, b.color, a.model_year,
  a.make, a.model, a.model_code, a.trim_level, a.cab, a.drive, a.engine, a.chrome_style_id, 
    a.configuration_id, 'sold_365' as status, 'none' as order_number
  from nc.vehicle_configurations a
  join nc.vehicles b on a.configuration_id = b.configuration_id
  join nc.vehicle_sales c on b.vin = c.vin
    and c.sale_type = 'retail'
    and c.delivery_date = ( -- only the most recent sale
      select max(delivery_date)
      from nc.vehicle_sales
      where vin = c.vin)    
  where c.delivery_date between current_date - 366 and current_date -1
  union all
  -- inv_og
  select current_date - 1, c.stock_number, c.vin, b.color, a.model_year,
    a.make, a.model, a.model_code, a.trim_level, a.cab, a.drive, a.engine, a.chrome_style_id,
    a.configuration_id, 'inv_og' as status, 'none' as order_number
  from nc.vehicle_configurations a
  join nc.vehicles b on a.configuration_id = b.configuration_id
  join nc.vehicle_acquisitions c on b.vin = c.vin
    and thru_date > current_date
  -- inv_intransit/system
  union all  
  select current_date - 1, 'none', a.vin, a.color, a.model_year,
    b.make, b.model, b.model_code, b.trim_level, b.cab, b.drive, b.engine, b.chrome_style_id,
    a.configuration_id, 'inv_transit' as status, a.order_number
  from nc.open_orders a
  join nc.vehicle_configurations b on a.configuration_id = b.configuration_id
  where a.category = 'in transit'
    and a.order_type = 'TRE'
  union all  
  select current_date - 1, 'none', coalesce(a.vin, 'not yet'), a.color, a.model_year,
    b.make, b.model, b.model_code, b.trim_level, b.cab, b.drive, b.engine, b.chrome_style_id,
    a.configuration_id, 'inv_system' as status, a.order_number
  from nc.open_orders a
  join nc.vehicle_configurations b on a.configuration_id = b.configuration_id
  where a.category = 'in system'
    and a.order_type = 'TRE'
    and a.vin is not null
  union all -- order without vins with an existing configuration_id
  select current_date - 1, 'none', coalesce(a.vin, 'not yet'), a.color, a.model_year,
    b.make, b.model, b.model_code, b.trim_level, b.cab, b.drive, b.engine, b.chrome_style_id,
    a.configuration_id, 'inv_system' as status, a.order_number
  from nc.open_orders a
  join nc.vehicle_configurations b on a.configuration_id = b.configuration_id
  where a.category in( 'in system')
    and a.order_type = 'TRE'
    and a.vin is null
    and a.configuration_id is not null) a ;

-- select *
-- from nc.open_orders a
-- where order_type = 'tre'
--   and not exists (
--     select 1
--     from nc.daily_inventory
--     where the_date = current_date -1
--       and order_number = a.order_number)

do
$$
begin
assert (
  select count(*) 
  -- select *
  from (
    select stock_number
    from nc.daily_inventory
    where stock_number <> 'none'
    group by the_date, stock_number, status
    having count(*) > 1) a) = 0, 'dup stock_numbers';
end
$$;    

do
$$
begin
assert (
  select count(*) 
  -- select *
  from (
    select vin
    from nc.daily_inventory
    where stock_number = 'none'
      and vin <> 'not yet'
    group by the_date, vin, status
    having count(*) > 1) a) = 0, 'dup vins';
end
$$;    


select * from nc.daily_inventory limit 10

-- GM inventory status per day
select the_date, make, status, count(*)
from nc.daily_inventory
where status not in ('sold_365','sold_90') 
  and make not in ('Honda','Nissan')
group by the_date, make, status 
order by the_date desc, make, status

-- records per day  -- 3560
select the_Date, count(*) from nc.daily_inventory group by the_date order by the_date desc
-- records in status per day
select the_Date, status, count(*) from nc.daily_inventory group by the_date, status order by the_date, status

-- records in status today
select the_Date, status, count(*) from nc.daily_inventory where the_date = current_date - 1 group by the_date, status order by the_date, status

inv_og;416
inv_system;106
inv_transit;145




    select a.model, a.model_year, a.seq, a.color, 
      count(*) filter (where b.status = 'sold_365') as sold_365,
      count(*) filter (where b.status = 'sold_90') as sold_90,
      count(*) filter (where b.status = 'inv_og') as inv_og,
      count(*) filter (where b.status = 'inv_transit') as inv_transit,
      count(*) filter (where b.status = 'inv_system') as inv_system
    from nc.model_colors a
    left join nc.daily_inventory b on a.model_year = b.model_year
      and a.make = b.make
      and a.model = b.model
      and a.color = b.color
      and the_date = current_date - 1
    where a.model_year = 2019
      and a.make = 'chevrolet'
      and a.model = 'equinox'
    group by a.model, a.model_year,a.seq, a.color  
    order by a.seq

-- vertical totals, for each trim_level
select model, model_code, trim_level, cab, drive, engine, count(*) 
from nc.daily_inventory 
where the_Date = current_date - 1
  and model = 'equinox'
  and model_year = 2020
group by model, model_code, trim_level, cab, drive, engine
order by model_code, trim_level, cab, drive, engine











-- one of the abiding notions is that whatever inventory is displayed
-- it is limited by whats in daily_inventory for the day

-- nc.get_model_years()

select json_agg(row_to_json(a)) -- unnamed array
from (
  select model_year, seq
  from nc.model_years) a
    
select row_to_json(b) -- object composed of a named array
from (
  select json_agg(row_to_json(a)) as model_years
  from (
    select model_year, seq
    from nc.model_years) a) b

-- nc.get_makes(_model_year: integer)
do
$$
declare _model_year integer := 2018;
begin
  drop table if exists wtf;
  create temp table wtf as
  select json_agg(row_to_json(a)) as makes
  from (
    select make
    from nc.daily_inventory 
    where the_date = current_date - 1
      and model_year = _model_year
    group by make) a;  
  end
$$;
select * from wtf;

-- nc.get_models(_model_year integer, make citext)
do
$$
declare _model_year integer := 2018;
        _make citext := 'chevrolet';
begin
  drop table if exists wtf;
  create temp table wtf as
  select json_agg(row_to_json(a)) as models
  from (
    select model
    from nc.daily_inventory 
    where the_date = current_date - 1
      and model_year = _model_year
      and make = _make
    group by model) a;  
  end
$$;
select * from wtf;


1/10/19
here is some TODO maintenance
inaccurate current inventory       

-- sales with open acquisitions
update nc.vehicle_acquisitions z
set thru_date = x.delivery_date
from (
  select b.*
  from nc.vehicle_acquisitions a
  join nc.vehicle_sales b on a.stock_number = b.stock_number
    and a.ground_date = b.ground_date
  where a.thru_date > current_date) x
where z.stock_number = x.stock_number
  and z.ground_date = x.ground_date;  

/*
do i need to do a master list, maybe several
looking at above query, not all configs are in sold_90, so, should there be a row for each possible config regardless of activity
so, taking equinox, i only know the possible configs based on any activity
shit, so for 19s fewer  configs than for 18s

-- 9 2018 configs, 7 2019 configs
select model_year, configuration_id, count(*)
from nc.vehicles
where model = 'equinox'
group by model_year, configuration_id
order  by model_year, configuration_id

stop it, that is out of the immediate scope
purpose is not to be a fucking car book


-- so, open orders, instead of a separate table, add color and config_id to open_orders
-- while i am at it, get rid of the superfluous vehicle_Trim field, rename color to color_code



          
    
--order number WNVVFX, 1GCPYDEK8KZ202558 
order says model_code = CK10543
chrome says model_code = CK10743

select * from chr.describe_vehicle where vin = '1GCPYDEK8KZ202558'
select * from jon.describe_vehicle where vin = '1GCPYDEK8KZ202558'
select * from gmgl.vehicle_invoices where vin = '1GCPYDEK8KZ202558'
invoice says CK10543
select * from gmgl.vehicle_vis where vin = '1GCPYDEK8KZ202558'
select * from gmgl.vehicle_invoices where raw_invoice like '%CK10743%' limit 10
delete from chr.describe_Vehicle where vin = '1GCPYDEK8KZ202558'
delete from jon.describe_Vehicle where vin = '1GCPYDEK8KZ202558'


*/  

