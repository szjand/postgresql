﻿select *
  from gmgl.vehicle_order_events a
  inner join gmgl.vehicle_orders b on a.order_number = b.order_number
  where event_code = '5000'
   and thru_date > now()
    and make::citext = 'Chevrolet'


select a.*,b.source, b.thru_date,b.ground_date
    from nc.vehicles a
    inner join nc.vehicle_acquisitions b on a.vin = b.vin
    where thru_date > current_date
      and source in('factory','dealer trade')
      and make = 'Chevrolet'


select * from nc.vehicle_acquisitions where stock_number = 'G36852'

select * from nc.vehicle_acquisitions where vin = '3GNAXUEV9KS575163' 

select * from nc.vehicle_sales where vin = '3GNAXUEV9KS575163'

select *
from nc.vehicle_acquisitions a
join (
  select vin
  from nc.vehicle_acquisitions
  group by vin
  having count(*) > 1) b on a.vin = b.vin
order by a.vin    


select vin, source from nc.vehicle_acquisitions group by vin, source having count(*) > 1


select * from nc.vehicle_acquisitions where vin = '1GCVKREC9JZ137555' order by ground_date

G36442: "extra" acq record for 1GCVKREC9JZ137555, whack it
delete from nc.vehicle_acquisitions where stock_number = 'G36442';

-- on ground but not in global
select *
  from gmgl.vehicle_order_events a
  inner join gmgl.vehicle_orders b on a.order_number = b.order_number
    and b.vin in ('1GCRYDED8KZ305764','1GNEVGKW7KJ269358','1GCRYDED2KZ314086','1GCRYDED5KZ311179')
where thru_date > now()::date

------------------------------------------------
--< G35599 
------------------------------------------------
in acquisitions with a 12/31/9999 thru date but capped in dms and 6000 status in global

select * from nc.vehicle_acquisitions where stock_number = 'g35599'
select * from nc.vehicle_sales where stock_number = 'G35599' or vin = '3GNAXXEVXKL231367'
select * from arkona.xfm_bopmast where bopmast_stock_number = 'G35599'
looks like a dealer trade

select a.*, b.the_date, c.account, d.journal_code, dd.description
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
join fin.dim_account c on a.account_key = c.account_key
  and c.account in ( -- all new vehicle inventory account
    select b.account
    from fin.dim_account b 
    where b.account_type = 'asset'
      and b.department_code = 'nc'
      and b.typical_balance = 'debit'
      and b.current_row
      and b.account <> '126104')    
join fin.dim_journal d on a.journal_key = d.journal_key
join fin.dim_gl_Description dd on a.gl_Description_key = dd.gl_description_key
where a.post_status = 'Y'
  and a.control = 'G35599'

update nc.vehicle_acquisitions 
set thru_date = '12/11/2018' 
where stock_number = 'G35599' ;
insert into nc.vehicle_sales
select stock_number,ground_date,vin,-1,thru_date,'dealer trade', false
from nc.vehicle_acquisitions 
where stock_number = 'G35599';

------------------------------------------------
--/> G35599 
------------------------------------------------


------------------------------------------------
--< G36789, G34969
------------------------------------------------
maybe a cancelled dealer trade, emailed jeri & michelle
select a.*, b.the_date, c.account, d.journal_code, dd.description
-- select b.the_date, a.*
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
-- join fin.dim_account c on a.account_key = c.account_key
--   and c.account in ( -- all new vehicle inventory account
--     select b.account
--     from fin.dim_account b 
--     where b.account_type = 'asset'
--       and b.department_code = 'nc'
--       and b.typical_balance = 'debit'
--       and b.current_row
--       and b.account <> '126104')    
join fin.dim_journal d on a.journal_key = d.journal_key
join fin.dim_gl_Description dd on a.gl_Description_key = dd.gl_description_key
-- where a.post_status = 'Y'
where a.control = 'G36789'

from jeri: Yes, it was a dealer trade from Schweiters that we ended up not getting.

so, in dt inventory, they show with the other dealers name as a service customer,
and in inpimast transitioning from status I to C

select * from arkona.xfm_inpmast where inpmast_vin = '3GCUKSEC8JG511558'

i am just going to delete the acquisition records

delete
-- select * 
from nc.vehicle_acquisitions where stock_number in ('g34969','g36789','G36072');

add an assert for any open acquisition with no non-void gl transactions
and it found another one
do
$$
begin
assert (
  select count(*)
  from (
    select * 
    from nc.vehicle_acquisitions a
    where thru_date > current_date
      and not exists (
        select 1
        from fin.fact_gl
        where control = a.stock_number
          and post_status = 'Y'))x ), 'possible void dealer trade';
end
$$;

select * from fin.fact_gl where control = 'g36072'
------------------------------------------------
--/> G36789, G34969
------------------------------------------------

------------------------------------------------
--< G34969
------------------------------------------------
maybe a cancelled dealer trade
select a.*, b.the_date, c.account, d.journal_code, dd.description
-- select b.the_date, a.*
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
-- join fin.dim_account c on a.account_key = c.account_key
--   and c.account in ( -- all new vehicle inventory account
--     select b.account
--     from fin.dim_account b 
--     where b.account_type = 'asset'
--       and b.department_code = 'nc'
--       and b.typical_balance = 'debit'
--       and b.current_row
--       and b.account <> '126104')    
join fin.dim_journal d on a.journal_key = d.journal_key
join fin.dim_gl_Description dd on a.gl_Description_key = dd.gl_description_key
-- where a.post_status = 'Y'
where a.control = 'G34969'

------------------------------------------------
--/> G34969
------------------------------------------------


------------------------------------------------
--< G36165
------------------------------------------------
this one is just fucking weird, sale & acquisition with different vins

select * from nc.vehicle_sales a where not exists (select 1 from nc.vehicle_acquisitions where vin = a.vin)
3GCUKREC0JG533241: DT G36165 acq: 1/17/19  sold: 1/19/19  acq via dealer trade, ro: 1/18/19

select * from nc.vehicle_acquisitions where stock_number = 'G36165'
3GCUKREC3JG568937

select * from nc.vehicles where vin in ('3GCUKREC0JG533241','3GCUKREC3JG568937')

------------------------------------------------
--/> G36165
------------------------------------------------

select * from nc.vehicle_acquisitions where vin = '1GNEVGKW0JJ251119'

select a.order_number, from_date as global_date_5000, b.vin,b.make, 
  c.inpmast_stock_number, c.status, d.*
from gmgl.vehicle_order_events a
inner join gmgl.vehicle_orders b on a.order_number = b.order_number
left join arkona.xfm_inpmast c on b.vin = c.inpmast_vin
  and c.current_row
left join nc.vehicle_acquisitions d on c.inpmast_stock_number = d.stock_number  
where event_code = '5000'
  and a.thru_date > now()
  and not exists (
    select 1
    from nc.vehicles aa
    inner join nc.vehicle_acquisitions bb on aa.vin = bb.vin
    where thru_date > current_date
      and source in('factory','dealer trade')  
      and aa.vin = b.vin)
order by a.order_number



  
      
select a.*,b.source, b.thru_date,b.ground_date
    from nc.vehicles a
    inner join nc.vehicle_acquisitions b on a.vin = b.vin
    where thru_date > current_date
      and source in('factory','dealer trade')

select * from nc.vehicle_acquisitions where vin = '1GKS2HKJ3KR195909'



select * from nc.vehicle_acquisitions where vin = '1GKS2HKJ8KR200585'

sele


