﻿---------------------------------------------------------------------------------------------
--< now, the actual nightly, starting with jon.test_base, nope, that's just a reference
-- actually nightly will be scraping into a staging table that will be like jon.test_4
-- and from that testing and inserting into base tables: nc.vehicles, etc
---------------------------------------------------------------------------------------------
/*
drop table if exists nc.stg_availability cascade;
create table nc.stg_availability (
  the_date date not null,
  row_number integer,
  journal_date date,
  stock_number citext primary key,
  vin citext,
  chrome_style_id integer,
  model_year citext,
  make citext,
  model citext,
  model_code citext,
  drive citext,
  cab citext,
  trim_level citext,
  engine citext,
  color_code citext,
  color citext,
  ground_date date,
  ground_date date,
  delivery_date date,
  source citext,
  sale_type citext);
create index on nc.stg_availability(vin);
create index on nc.stg_availability(stock_number);

drop table if exists nc.exterior_colors cascade;`
drop table if exists nc.exterior_colors cascade;
create table nc.exterior_colors (
  model_year citext not null,
  make citext not null,
  model citext not null,
  mfr_color_code citext,
  mfr_color_name citext not null,
  constraint exterior_colors_pkey primary key (model_year, make, model, mfr_color_code, mfr_color_name)); 
comment on table nc.exterior_colors is 'subset of color_code and colors from dataone for rydell franchises only, 
  model year greater than 2016'; -- Table has no primary key, too many null mfr_color_code';  
create index on nc.exterior_colors(model_year);  
create index on nc.exterior_colors(make);
create index on nc.exterior_colors(model);
create index on nc.exterior_colors(mfr_color_code);
create index on nc.exterior_colors(mfr_color_name);
*/
-- -- 12/16/18 moved to .../all_nc_inventory_nightly.sql
-- -- update colors
-- truncate nc.exterior_colors;
-- insert into nc.exterior_colors
-- select a.year, a.make, a.model, c.mfr_color_code, c.mfr_color_name
-- from dao.veh_trim_styles a
-- inner join dao.lkp_veh_ext_color b on a.vehicle_id = b.vehicle_id
-- inner join dao.def_ext_color c on b.ext_color_id = c.ext_color_id
-- where year > 2017
--   and c.mfr_color_code <> ''
--   and a.make in ('chevrolet','gmc','buick','cadillac','honda','nissan')
-- group by a.year, a.make, a.model, c.mfr_color_code, c.mfr_color_name;
-- delete from nc.exterior_colors
-- where model_year = '2018'
--   and make = 'Chevrolet'
--   and model = 'Traverse'
--   and mfr_color_code = 'G2X'
--   and mfr_color_name = 'Havana Metallic';
-- delete from nc.exterior_colors
-- where model_year = '2018'
--   and make = 'Honda'
--   and model = 'Civic'
--   and mfr_color_code = 'GX'
--   and mfr_color_name = 'Modern Steel Metallic';  
-- delete from nc.exterior_colors
-- where model_year = '2018'
--   and make = 'Honda'
--   and model = 'Civic'
--   and mfr_color_code = 'GY'
--   and mfr_color_name = 'Polished Metal Metallic';   
-- delete from nc.exterior_colors
-- where mfr_color_name like '%mono%';  
-- delete from nc.exterior_colors
-- where model_year = '2019'
--   and make = 'Nissan'
--   and model like 'Titan%'
--   and mfr_color_code = 'QAB'
--   and mfr_color_name = 'Pearl White Tricoat';  
-- --------------------------------------------------------------
/*
 6/27 start over, use the stg_availabity table as the basis
1. insert acquisitions
2. insert sales
3. fill out data as necessary
*/
-- 1. new acquisitions
-- truncate nc.stg_availability

-- take as much from inpmast as possible, verify later against colors, chrome
-- 7/2 chrome style id will come from chrome not inpmast, not sure about model_code yet
truncate nc.stg_availability;
insert into nc.stg_availability (the_date,journal_date,stock_number,
  vin,model_year,make, model,
  model_code,color_code,color, source)
select current_date, f.the_date, f.control, 
  g.inpmast_vin, g.year, g.make, g.model, g.model_code, 
  g.color_code, g.color,
  case f.journal_code
    when 'PVI' then 'factory'
    when 'CDH' then 'dealer trade'
    when 'GJE' then 'ctp'
  end as source
from ( -- accounting journal
  select a.control, d.journal_code, max(b.the_date) as the_date, sum(a.amount) as amount
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_date between current_Date - 90 and current_date--'06/22/2018'
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account = '123700'
  inner join fin.dim_journal d on a.journal_key = d.journal_key
    and d.journal_code in ('PVI','CDH','GJE')
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where a.post_status = 'Y'
  group by a.control, d.journal_code
  having sum(a.amount) > 10000) f
inner join arkona.xfm_inpmast g on f.control = g.inpmast_stock_number
  and g.model like 'silverado 1500%' -- 9/23 fuck: SILVERADO 1500 LD 
  and g.current_row = true
where not exists (
  select 1
  from nc.vehicle_acquisitions
  where stock_number = f.control) ;


-- update source to dealer trade where journal = PVI and no transactions in acct 126102
-- 10/6 need to exclude fleet orders
-- actually just do a test for the fleet type

-- TRE is the retail stock order type
select b.control, c.order_type
  from nc.stg_availability a
  left join ( -- does the vehicle have dough iin 126102
    select a.control, a.doc, d.journal_code, a.amount
    from fin.fact_gl a
    inner join fin.dim_account c on a.account_key = c.account_key
      and c.account = '126102'
    inner join fin.dim_journal d on a.journal_key = d.journal_key
      and d.journal_code = 'PVI'
    where a.post_status = 'Y') b on a.stock_number = b.doc
  left join gmgl.vehicle_orders c on a.vin = c.vin
  where a.the_date = current_date
    and b.doc is null
    and a.source = 'factory'

-- another test for dealer trades: from the VIS, the delivery_store
select a.stock_number, a.vin, a.source, regexp_replace(b.delivery_store, '\n', '', 'g')
from nc.stg_availability a
left join gmgl.vehicle_vis b on a.vin = b.vin


/*
-- and query to clean up those that have been mis-categorized

select a.*, regexp_replace(delivery_store, '\n', '', 'g')
from nc.vehicle_acquisitions a
join gmgl.vehicle_vis b on a.vin = b.vin
where a.source = 'dealer trade' 
  or b.delivery_store not like '%RYDELL%'
order by a.vin

-- do some clean up
update nc.vehicle_acquisitions
set source = 'factory'
where stock_number in ('G35078','G35080','G35081','G35081','G35075');

update nc.vehicle_acquisitions
set source = 'dealer trade'
where stock_number in ('G33187','G33180','G33184','G33178');

update nc.vehicle_acquisitions
set source = 'unwind'
where stock_number = 'G33184'
  and thru_date > current_date;

update nc.stg_availability
set source = 'factory'
where stock_number in ('g35384','g35383')

  
*/   

-- if the above assert passes, go ahead and update the source to dealer trade
-- careful here ...
update nc.stg_availability
set source = 'dealer trade'
where the_date = current_date
  AND stock_number in (
    select stock_number
    from nc.stg_availability a
    left join (
      select a.control, a.doc, d.journal_code, a.amount
      from fin.fact_gl a
      inner join fin.dim_account c on a.account_key = c.account_key
        and c.account = '126102'
      inner join fin.dim_journal d on a.journal_key = d.journal_key
        and d.journal_code = 'PVI'
      where a.post_status = 'Y') b on a.stock_number = b.doc
    where a.the_date = current_date
      and b.doc is null
      and a.source = 'factory');

-- select * from nc.stg_availability

-- unwind acquisitions
-- test for unwind on same day as sale ?!?
insert into nc.stg_availability (the_date,stock_number,vin,model_year,
  model_code,color_code,color,ground_date,source)
select current_date, a.stock_number, a.vin, b.model_year,
  b.model_Code, c.color_code, b.color, a.run_date - 1, 'unwind'
from sls.deals a
inner join nc.vehicle_acquisitions aa on a.stock_number = aa.stock_number -- to be an unwind, previous acq record must exist
inner join nc.vehicles b on a.vin = b.vin
inner join arkona.xfm_inpmast c on b.vin = c.inpmast_vin
  and c.current_row = true
  and c.model like 'silverado 1500%'
where a.run_date > current_Date - 60
  and deal_status_code = 'deleted'
  and not exists (
    select 1
    from nc.vehicle_acquisitions
    where stock_number = a.stock_number
      and ground_date = a.run_date - 1);

update nc.stg_availability
set ground_date = journal_date
where source = 'ctp';

-- dealer trades: ground = coalesce(keyper, coaelsece dt ro, journal date)
update nc.stg_availability x
set ground_date = y.ground
from (
  select aa.stock_number, aa.vin, aa.journal_date, aa.source, 
    bb.tsp, bb.pdi, bb.dt, cc.creation_date as keyper,
    coalesce(cc.creation_date, coalesce(bb.dt, aa.journal_date)) as ground
  from nc.stg_availability aa
  left join (
    select d.vin, 
      max(case when c.opcode = 'tsp' then b.the_date end) as tsp,
      max(case when c.opcode = 'pdi' then b.the_date end) as pdi,
      max(case when c.opcode = 'dt' then b.the_date end) as dt
    from ads.ext_fact_repair_order a
    inner join dds.dim_date b on a.opendatekey = b.date_key
    inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
      and c.opcode in ('dt')
    inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
    inner join nc.stg_availability e on d.vin = e.vin
    group by d.vin) bb on aa.vin = bb.vin
  left join ads.keyper_creation_dates cc on aa.stock_number = cc.stock_number  
  where aa.source = 'dealer trade') y
where x.stock_number = y.stock_number; 

-- factory
-- if there is neither keyper nor ro, ground = 12/31/9999
-- what i have to decide is, what do i do with vehicles not here yet
update nc.stg_availability x
set ground_date = y.ground
from (
  select aa.stock_number, aa.vin, aa.journal_date, aa.source, 
    bb.tsp, bb.pdi, bb.dt, cc.creation_date as keyper, dd.event_code, dd.description, dd.from_date,
    coalesce(cc.creation_date, coalesce(bb.tsp, coalesce(bb.pdi, '12/31/9999'::date))) as ground
-- select *    
  from nc.stg_availability aa
  left join (
    select d.vin, 
      max(case when c.opcode = 'tsp' then b.the_date end) as tsp,
      max(case when c.opcode = 'pdi' then b.the_date end) as pdi,
      max(case when c.opcode = 'dt' then b.the_date end) as dt
    from ads.ext_fact_repair_order a
    inner join dds.dim_date b on a.opendatekey = b.date_key
    inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
      and c.opcode in ('tsp', 'dt', 'pdi')
    inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
    inner join nc.stg_availability e on d.vin = e.vin
    group by d.vin) bb on aa.vin = bb.vin
  left join ads.keyper_creation_dates cc on aa.stock_number = cc.stock_number  
  left join (
    SELECT a.order_number, a.order_type, a.vin, a.alloc_group, a.vehicle_trim,
      a.model_code, a.ship_to_bac, b.event_code, c.description, c.category,
      b.from_date
    FROM gmgl.vehicle_orders a
    JOIN gmgl.vehicle_order_events b ON a.order_number = b.order_number
    JOIN gmgl.vehicle_event_codes c ON b.event_code = c.code
    join nc.stg_availability d on a.vin = d.vin
      and d.source = 'factory'
    where b.thru_date = (
        select max(thru_date)
        from gmgl.vehicle_order_events
        where order_number = b.order_number)
    group by a.order_number, a.order_type, a.vin, a.alloc_group, a.vehicle_trim,
      a.model_code, a.ship_to_bac, b.event_code, c.description, c.category, b.from_date) dd on aa.vin = dd.vin  
  where source = 'factory') y  
where x.stock_number = y.stock_number;

-- ok, the i choose, delete if ground = 12/31/9999
delete 
-- select * 
from nc.stg_availability
where ground_date = '12/31/9999';

-----------------------------------------------------------------------------------
--< colors
-----------------------------------------------------------------------------------
-- 1. color does not match or ext_color is null 
-- i don't see how this can be automated yet, so, find them and break
-- keep a record of the anomalies and the fixes
-- 8/6/18: G34215 unwind, fails color assertion because no make, model at this point for unwind

-- 9/23: nc.exterior colors does not include model SILVERADO 1500 LD
-- invoice shows: 2019 SILVERADO LD 1500 4WD LT, chrome has 2019 SILVERADO 1500 LD

do
$$
begin
assert (
  select count(*)
  -- select *
  from nc.stg_availability a
  left join nc.exterior_colors b on a.color_code = b.mfr_color_code
    and a.model_year = b.model_year
    and a.make = b.make
--     and a.model = b.model 
    and case when a.model = 'SILVERADO 1500 LD' then b.model = 'Silverado 1500 Legacy' else a.model = b.model end
  where (a.color <> b.mfr_color_name or b.mfr_color_name is null)) = 0, 'DANGER WILL ROBINSON';
end
$$;

/*
color_code/color in stg_availability comes from inpmast and as such may not be correct/complete

update nc.stg_availability set color_code = 'G9K' where stock_number = 'g35145'

select *
from nc.exterior_colors
where model like 'silverado%'
order by mfr_color_name

select *
from nc.exterior_colors
where model_year in ('2018', '2019')
  and mfr_color_code in ('g1k','gan','gpj','ga0', 'g9k','gba','gpa')
  and model like 'silverado 1500%'

select *
from nc.vehicles
where vin = '3GCUKREC4JG428055'

-- from inpmast, had 2 spaces between words
update nc.stg_Availability
set color = 'Iridescent Pearl Tricoat'
where stock_number = 'G35989';

update nc.stg_Availability
set color = 'Shadow Gray Metallic'
where stock_number = 'G34962';

select *
from nc.exterior_colors
where model_year = '2019'

update nc.stg_availability
set color = 'Satin Steel Metallic'
where stock_number = 'G35131';

update nc.stg_availability
set color = 'Silver Ice Metallic'
where stock_number = 'G35081';

update nc.stg_availability
set color = 'Northsky Blue Metallic'
where stock_number = 'G35079';

update nc.stg_availability
set color = 'DEEP OCEAN BLUE METALLIC'
where stock_number = 'G35242';

update nc.stg_availability
  set color = 'CAJUN RED TINTCOAT'
where vin in ('2GCVKPEC7K1127606')


update nc.stg_availability
set color = 'Satin Steel Metallic'
where stock_number = 'G34983';

-- 9/21/18
select *
from nc.exterior_colors
where mfr_color_code in ('G9K')
  and model like 'Silverado 1500%'
  and model_year = '2019'

-- 8/21/18
select *
from nc.exterior_colors
where mfr_color_code in ('G1W')
  and model like 'Silverado 1500'
  and model_year = '2018'

GAN;Silver Ice Metallic
GPA;Graphite Metallic

update nc.stg_availability
set color = 'Silver Ice Metallic'
where color_code = 'GAN';

update nc.stg_availability
set color = 'Graphite Metallic'
where color_code = 'GPA';

-- 7/26
update nc.stg_availability
set color = 'Cajun Red Tintcoat'
where stock_number = 'g34874';

-- 7/24 dealer trade vehicle with color_code: G1W & color: IRIDESCENT PEARL
-- aha, color actually is Iridescent Pearl Tricoat
select *
from nc.exterior_colors
where mfr_color_code = 'G1W'
  and model = 'silverado 1500'

update nc.stg_availability
set color = 'Iridescent Pearl Tricoat'
where stock_number = 'G34596';

-- 1. color_code matches color does not
update nc.stg_availability
set color = 'Deep Ocean Blue Metallic'
where stock_number = 'G33179R';
-- rental, so, the original color must be wrong also
update nc.vehicles 
set color = 'Deep Ocean Blue Metallic'
where vin = '3GCUKSEC6JG225921';
-- 2. no color code and no such color in ext_colors (dealer trade)
select *
from nc.exterior_colors
where mfr_color_name like '%tan%'
  and model = 'silverado 1500'
  
update nc.stg_availability
set color = 'Doeskin Tan'
where vin = '3GCUKREC8JG376168';  
*/

-----------------------------------------------------------------------------------
--/> colors
-----------------------------------------------------------------------------------

-- run chr.describe vehicle: adds vins from nc.stg_availability
-- !!!!!!!!!!! NEED TO TEST NEW DESCRIBE_VEHICLE RECORDS FOR NUMBER OF STYLES !!!!!!!!!!!!!!!!!

do
$$
begin
assert (
  select count(*)
  -- select *
  from nc.stg_availability a
  where not exists (
    select 1
    from chr.describe_vehicle
    where vin = a.vin)) = 0, 'DANGER WILL ROBINSON, there are new vins missing from chr.describe_vehicle';
end
$$;  

do
$$
begin
assert (
  select count(*)
  -- select *
from chr.describe_vehicle a
where not exists (
  select 1
  from jon.describe_vehicle
  where vin = a.vin)) = 0, 'DANGER WILL ROBINSON, there are new vins missing from jon.describe_vehicle';
end
$$;  
--------

-- 7-2
-- new acquisitions where vin already exists in nc.vehicles
insert into nc.vehicle_acquisitions (vin,stock_number,ground_date,source,in_transit_sale)
select a.vin, a.stock_number, a.ground_date, a.source, false
from nc.stg_availability a
inner join nc.vehicles aa on a.vin = aa.vin
  and a.source <> 'unwind'; -- unwinds are processed separately

-- 7-10 need to implement checking for style_count, got one today with style_count = 4
-- per ben c, the "canonical" source is global connect
do
$$
begin
assert (
      select count(*) = sum(b.style_count)
      from nc.stg_availability a
      left join chr.describe_vehicle b on a.vin = b.vin), 'DANGER WILL ROBINSON, vehicles with multiple chrome styles';
end
$$;

/*
12/18/18
one ctp
that vin has 4 chrome styles
but it doesn't matter because, it already exists in nc.vehicles
*/
----------------------------------------------------------------------------------------------------
--< G35131: 2019 crew ltz ck10543
----------------------------------------------------------------------------------------------------
/*
-- -- 10/4, this is new, G35131, 1GCUYGED5KZ147475 acq thru dealer trade, a 2019 crew cab with 2 style ids, but the trim is the same on both
-- -- order type is FBC: Fleet Political Subdivision, which doesn't make any sense, but i don't know the meaning of the order types
-- --  in chrome, the only difference in the 2 styles is model code: CK10543 vs CK10743
-- -- both styles show LTZ as trim, order shows DAN: TRAIL, PEG = 1LZ, NO 1LZ in chrome
-- -- 10/5 not a dealer trade, thinking there is no holdback on a fleet order
-- -- 
select *
from nc.stg_availability a
inner join chr.describe_vehicle b on a.vin = b.vin

select * from arkona.xfm_inpmast
where inpmast_vin = '1GCUYGED5KZ147475'

-- looks like no holdback on fleet orders
select b.account, a.*
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
--   and b.account = '123700'
where control = 'g33896'
order by account

 
select a.order_type, b.vehicle_order_type, count(*)
from gmgl.vehicle_orders a
inner join gmgl.vehicle_order_types b on a.order_type = b.vehicle_order_type_key 
group by a.order_type, b.vehicle_order_type

select *
from gmgl.vehicle_orders
where order_type in ('SRE')

1GYS4JKJ6JR339362

select * 
from gmgl.vehicle_orders
where vin = '1GCUYGED5KZ147475'

-- so, first, it is not a dealer trade
update nc.stg_availability
set source = 'factory'
where stock_number = 'G35131';

but in dataone, 1LZ is exposed as mfr_package_code, but trim is LTZ
do
$$
declare
  vin citext := '1GCUYGED5KZ147475';
  vin_patt citext :=  left(vin, 8) || substring(vin, 10, 1) || substring(vin, 11, 1);
begin
drop table if exists wtf;
create temp table wtf as
select *
from dao.vin_reference
where vin_pattern = vin_patt;
end
$$;
select * from wtf;

need a test for this situation, which is
1. multiple styles from chrome
2. for this particular vehicle, the only distinguishing factor in chrome is the model_code
so, this works for G35131:

insert into nc.vehicles
select aa.vin, c.chrome_Style_id::integer, aa.model_year, aa.make, aa.model, aa.model_code,
  case
    when c.style like '2WD%' then '2WD'
    when c.style like '4WD%' then '4WD'
  end as drive,
  case 
    when c.style like '%Reg%' then 'reg'
    when c.style like '%Double%' then 'double'
    when c.style like '%Crew%' then 'crew'
    when c.style like '%Ext%' then 'extended'
  end as cab,
  case
    when model_year = '2019' then
      case
        when c.style like '%LT Trail Boss' then 'LT Trail Boss'
        when c.style like '%RST' then 'RST'
        when c.style like '%LTZ' then 'LTZ'
        when c.style like '%LT' then 'LT'
        when c.style like '%High Country' then 'High Country'
        else 'XXXXXXXXXXXXXXXXXXX'
      end
  end as trim,
  c.engine_displacement, aa.color, null as sku_id
from nc.stg_availability aa
inner join (
  select b.vin,
    jsonb_array_elements(b.response->'style')->'attributes'->>'id' as chrome_style_id,
    jsonb_array_elements(b.response->'style')->'attributes'->>'name' as style,
    jsonb_array_elements(b.response->'style')->'attributes'->>'mfrModelCode' as model_code ,
    jsonb_array_elements(jsonb_array_elements(response->'engine')->'displacement'->'value')->'attributes'->>'unit' as engine_displacement_units,
    jsonb_array_elements(jsonb_array_elements(response->'engine')->'displacement'->'value')->>'$value' as engine_displacement
  from nc.stg_availability a
  inner join chr.describe_vehicle b on a.vin = b.vin) c on aa.model_code = c.model_code

  

select * from nc.vehicles where vin = '3GCUKREC5JG496297'

select *
from nc.stg_availability

*/      
----------------------------------------------------------------------------------------------------
--/> G35131: 2019 crew ltz ck10543
----------------------------------------------------------------------------------------------------

/*
-- the multiple style counts, at least for silverados, is usually going to be based on trim
-- i currently know of no way to automate this process   
-- this tells me the trim is 2LT (dan = Z71, peg = 2LT, vehicle_trim = 2LT)  
select *
from gmgl.vehicle_orders
where vin in (  
  select a.vin   
  from nc.stg_availability a
  inner join chr.describe_vehicle b on a.vin = b.vin
    and b.style_count > 1)
    
-- so, this should give me the correct chrome_style_id
select cc.vin, cc.chrome_style_id
from gmgl.vehicle_orders aa
inner join (
  select b.vin, jsonb_array_elements(b.response->'style')->'attributes'->>'id' as chrome_style_id,
    jsonb_array_elements(b.response->'style')->'attributes'->>'name' as style,
    jsonb_array_elements(b.response->'style')->'attributes'->>'mfrModelCode' as model_code 
  from nc.stg_availability a
  inner join chr.describe_vehicle b on a.vin = b.vin
    and b.style_count > 1) cc on aa.vin = cc.vin and aa.model_code = cc.model_code and position(aa.peg in cc.style) <> 0
    
-- which disagrees with inpmast, oh well
select *
from arkona.xfm_inpmast
where inpmast_Vin = '3GCUKSEJ2JG455115'
  and current_row = true
*/
-- new acquisitions where vin does not yet exist in nc.vehicles
-- AND chr.describe_vehicles.style_count = 1
-- first, insert into nc.vehicles
-- 12/8/18  moved jsonb_array_elements to join
insert into nc.vehicles(vin,chrome_style_id,model_year,make,model,
  model_code,drive,cab,trim_level,engine,color)
select f.vin, f.chrome_style_id::citext, model_year::integer, f.make, f.model, f.model_code,
  case
    when f.style like '2WD%' then '2WD'
    when f.style like '4WD%' then '4WD'
  end as drive,
  case 
    when f.style like '%Reg%' then 'reg'
    when f.style like '%Double%' then 'double'
    when f.style like '%Crew%' then 'crew'
    when f.style like '%Ext%' then 'extended'
  end as cab,
  case
    when model_year = '2018' then
      case 
        when f.style in ('4WD Crew Cab 143.5" LTZ w/1LZ','4WD Crew Cab 153.0" LTZ w/1LZ',
          '4WD Double Cab 143.5" LTZ w/1LZ') then '1LZ'
        when f.style in ('4WD Crew Cab 143.5" LTZ w/2LZ','4WD Double Cab 143.5" LTZ w/2LZ') then '2LZ'
        when f.style in ('2WD Reg Cab 133.0" Work Truck','4WD Double Cab 143.5" Work Truck',
          '4WD Reg Cab 119.0" Work Truck','4WD Reg Cab 133.0" Work Truck') then 'WT'
        when f.style in ('4WD Crew Cab 143.5" High Country') then 'high country'
        when f.style in ('4WD Crew Cab 143.5" LT w/1LT','4WD Crew Cab 153.0" LT w/1LT',
          '4WD Double Cab 143.5" LT w/1LT') then '1LT'
        when f.style in ('4WD Crew Cab 143.5" LT w/2LT','4WD Crew Cab 153.0" LT w/2LT',
          '4WD Double Cab 143.5" LT w/2LT') then '2LT'
        when f.style in ('2WD Reg Cab 133.0" LS') then 'LS'
        when f.style in ('4WD Crew Cab 153.0" Custom') then 'custom'
        else 'XXXXXXXXXXXXXXXXXXX'
      end
    when model_year = '2019' and model = 'Silverado 1500' then
      case
        when f.style in ('4WD Crew Cab 147" LT Trail Boss') then 'LT Trail Boss'
        when f.style in ('4WD Crew Cab 147" RST','4WD Double Cab 147" RST') then 'RST'
        when f.style in ('4WD Crew Cab 147" LTZ','4WD Crew Cab 157" LTZ') then 'LTZ'
        when f.style in ('4WD Crew Cab 147" LT','4WD Crew Cab 157" LT','4WD Double Cab 147" LT') then 'LT'
        when f.style in ('4WD Crew Cab 147" High Country') then 'High Country'
        else 'XXXXXXXXXXXXXXXXXXX'
      end
    when model_year = '2019' and model = 'Silverado 1500 LD' then
      case
        when f.style in ('4WD Double Cab LT w/1LT','4WD Double Cab LT w/2LT') then 'LT'
        else 'XXXXXXXXXXXXXXXXXXX'
      end      
  end as trim,  
  f.engine, f.color 
from (    
  select a.vin, a.model_year, a.make, a.model,
    r.style ->'attributes'->>'id' as chrome_style_id,
    r.style ->'attributes'->>'mfrModelCode' as model_code,
    r.style ->'attributes'->>'name' as style,
    t.displacement->>'$value' as engine,
    a.color
  from nc.stg_availability a
  left join chr.describe_vehicle b on a.vin = b.vin
  left join jsonb_array_elements(b.response->'style') as r(style) on true
  left join jsonb_array_elements(b.response->'engine') as s(engine) on true
  left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
    and t.displacement ->'attributes'->>'unit' = 'liters'
  where style_count = 1
    and source <> 'unwind' -- unwinds are processed separately
    and not exists (
      select 1
      from nc.vehicles
      where vin = a.vin)) f;


do
$$
begin
assert ( -- unresolved trim level
  select count(*)
  -- select *
  from nc.vehicles
  where trim_level like 'X%') = 0, 'unresolved trim';
end
$$;


do 
$$
begin
assert (-- vehicles without configuration
  select count(*)
  -- select *
  from nc.vehicles a
  left join nc.vehicle_configurations b on a.model_year::integer = b.model_year
    and a.make = b.make
    and a.model = b.model
    and a.model_code = b.model_code
    and coalesce(a.trim_level, 'none') = coalesce(b.trim_level, 'none')
    and a.cab = b.cab
    and a.drive = b.drive
    and coalesce(a.engine, 'none') = coalesce(b.engine, 'none')
  where a.model_year in ('2018','2019')
    and b.make is null) = 0, 'no configuration';
end
$$;

update nc.vehicles z
set configuration_id = x.configuration_id
from (
  select b.*
  from nc.vehicles a
  left join nc.vehicle_configurations b on a.chrome_Style_id = b.chrome_style_id
    and a.trim_level = b.trim_level
    and a.engine = b.engine
  where a.configuration_id is null) x
where z.chrome_Style_id = x.chrome_style_id
  and z.trim_level = x.trim_level
  and z.engine = z.engine
  and z.configuration_id is null;
  
-- 7/10 for today this will work, 1 vehicle with multiple chrome style ids
-- 7/17 2 multi style vins, this correctly identifies both of them
insert into nc.vehicles(vin,chrome_style_id,model_year,make,model,
  model_code,drive,cab,trim_level,engine,color)
select f.vin, f.chrome_style_id::integer, model_year, f.make, f.model, f.model_code,
  case
    when f.style like '2WD%' then '2WD'
    when f.style like '4WD%' then '4WD'
  end as drive,
  case 
    when f.style like '%Reg%' then 'reg'
    when f.style like '%Double%' then 'double'
    when f.style like '%Crew%' then 'crew'
    when f.style like '%Ext%' then 'extended'
  end as cab,
  case
    when model_year = '2018' then
      case 
        when f.style like '%1LZ%' then '1LZ'
        when f.style like '%2LZ%' then '2LZ'
        when f.style like '%Work%' then 'WT'
        when f.style like '%High%' then 'high country'
        when f.style like '%1LT%' then '1LT'
        when f.style like '%2LT%' then '2LT'
        when f.style like '%LS%' then 'LS'
        when f.style like '%LT%' then 'LT'
        when f.style like '%Custom%' then 'custom'
        else 'XXXXXXXXXXXXXXXXXXX'
      end
    when model_year = '2019' then
      case when 
      case
        when f.style like '%LT Trail Boss' then 'LT Trail Boss'
        when f.style like '%RST' then 'RST'
        when f.style like '%LTZ' then 'LTZ'
        when f.style like '%LT' then 'LT'
        when f.style like '%High Country' then 'High Country'
        else 'XXXXXXXXXXXXXXXXXXX'
      end
  end as trim,
  f.engine, f.color 
from (  
  select a.vin, cc.chrome_style_id, a.model_year, a.make, a.model, a.model_code, cc.style, cc.engine, a.color
  from nc.stg_availability a
  left join gmgl.vehicle_orders aa on a.vin = aa.vin
  inner join (
    with 
      engine as ( -- necessary to do this in CTE to be able to filter on one of the array values (liters)
        select vin, jsonb_array_elements(jsonb_array_elements(response->'engine')->'displacement'->'value') as displacement
        from chr.describe_vehicle
        where vin in (select vin from nc.stg_availability))
    select b.vin, jsonb_array_elements(b.response->'style')->'attributes'->>'id' as chrome_style_id,
      jsonb_array_elements(b.response->'style')->'attributes'->>'name' as style,
      jsonb_array_elements(b.response->'style')->'attributes'->>'mfrModelCode' as model_code ,
      e.displacement->>'$value' as engine
    from nc.stg_availability a
    left join engine e on a.vin = e.vin and e.displacement ->'attributes'->>'unit' = 'liters'
    inner join chr.describe_vehicle b on a.vin = b.vin
      and b.style_count > 1) cc on aa.vin = cc.vin 
        and aa.model_code = cc.model_code 
        and position(aa.peg in cc.style) <> 0) f
where not exists (
  select 1
  from nc.vehicles
  where vin = f.vin);        

         
-- and update sku_id
update nc.vehicles x
set sku_id = y.sku_id
from (
  select aa.vin, bb.*
  from nc.vehicles aa
  left join gmgl.sku bb on aa.engine = bb.engine
    and
      case
        when bb.cab = 'regular' then aa.cab = 'reg'
        else aa.cab = bb.cab
      end
    and 
      case
        when bb.vehicle_trim = '1WT' then  aa.trim_level = 'WT'
        when bb.vehicle_trim = 'GAJ' then aa.trim_level = 'high country'
        when bb.vehicle_Trim = '1LS' then aa.trim_level = 'LS'
        when bb.vehicle_trim = 'ST9' then aa.trim_level = 'custom'
        else bb.vehicle_trim = aa.trim_level
      end
    and
      case
        when bb.color = 'unripened green' then aa.color = 'Unripened Green Metallic'
        else bb.color = aa.color
      end) y
where x.vin = y.vin
  and x.sku_id is null;

-- test for null sku_id
do
$$
declare
begin
  assert (
  select count(*) 
  -- select *
  from nc.vehicles
  where model_year = '2018'
    and model like 'silverado 1%'
    and sku_id is null) = 0, 'THIS VIN HAS NO SKU_ID';
end
$$;  

do
$$
declare
begin
  assert (
  select count(*) 
  from nc.vehicles
  where model_year = '2019'
    and sku_id is null) = 0, 'THIS VIN HAS NO SKU_ID: ' || (select string_agg(vin,',') from nc.vehicles where model_year = '2019' and sku_id is null group by vin);
end
$$; 


insert into gmgl.sku(make,cab,vehicle_trim,color,engine)
select make, cab, trim_level,color,engine 
from nc.vehicles 
where model_year = '2018'
  and model like 'silverado 1%'
  and sku_id is null;

-- now insert the new acquisitions
insert into nc.vehicle_acquisitions  (vin,stock_number,ground_date,source,in_transit_sale)
select a.vin, a.stock_number, a.ground_date, a.source, false
from nc.stg_availability a
left join nc.vehicle_acquisitions b on a.stock_number = b.stock_number
  and a.ground_date = b.ground_date
where b.vin is null  
  and a.source <> 'unwind';

-- and finally, the unwind acquisition
-- not sure how this will work for multiple unwinds, only want to update the 
-- must recent acquisition record 
update nc.vehicle_acquisitions x
set thru_date = y.the_date
from (
  select a.vin, a.stock_number, a.ground_date - 1 as the_date
  from nc.stg_availability a
  where source = 'unwind') y
where x.stock_number = y.stock_number;

insert into nc.vehicle_acquisitions  (vin,stock_number,ground_date,source,in_transit_sale)
select a.vin, a.stock_number, a.ground_date, a.source,false
from nc.stg_availability a
where source = 'unwind';

----------------------------------------------------------------------
--/> acquisitions
-----------------------------------------------------------------------

-- style_id & model_codes for 2018 silverados
select style_id, model_year, model_code, alt_Style_name, count(*)
from (
  select jsonb_array_elements(c.response->'style')->'attributes'->>'id' as style_id,
    c.response->'attributes'->>'modelYear' as model_year,
    jsonb_array_elements(c.response->'style')->'attributes'->>'mfrModelCode' as model_code,
    jsonb_array_elements(c.response->'style')->'attributes'->>'altStyleName' as alt_style_name
  from chr.describe_vehicle c) x
where model_year = '2018'  
group by style_id, model_year, model_code, alt_Style_name
order by model_code, alt_Style_name

-----------------------------------------------------------------------
--< sales
-----------------------------------------------------------------------
all sales should come from sls.deals except dealer trades
dealer trades will have to come from accounting

select * from nc.stg_availability

may want a second staging table for sales

select max(ground_date) from nc.vehicle_acquisitions

select * from nc.vehicle_sale_types
select * from nc.vehicle_sales where sale_type = 'ctp'
sale types:
  ctp
  fleet
  dealer trade
  retail (inc wholesale ?)
  are there any wholesale except ctp ?


drop table if exists nc.stg_sales;
create table nc.stg_sales (
  stock_number citext,
  vin citext,
  bopmast_id integer,
  delivery_date date,
  sale_type citext,
  trans_Description citext,
  amount numeric(8,2),
  acct_date date,
  ground_date date);

-- accounting should include all sales
-- first step, accounting info and vin from inpmast
truncate nc.stg_sales;
insert into nc.stg_sales (stock_number, vin, trans_description, amount, acct_date,ground_date)
select a.control, f.inpmast_vin, e.description, a.amount, b.the_date, g.ground_date
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_date between current_Date - 40 and current_date
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account = '123700'
inner join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code = 'VSN'
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
left join arkona.xfm_inpmast f on a.control = f.inpmast_stock_number
  and f.current_row = true  
left join nc.vehicle_acquisitions g on a.control = g.stock_number
  and g.thru_date = ( -- need most recent acquisition
    select max(thru_date)
    from nc.vehicle_acquisitions
    where stock_number = g.stock_number)  
where a.post_status = 'Y'
  and abs(a.amount) > 10000
  and f.model like 'silverado 1500%'
  and f.year in ('2018', '2019')
  and not exists (
    select 1
    from nc.vehicle_sales
    where stock_number = a.control
      and ground_date = g.ground_date) ;

select * from nc.stg_sales 

do
$$
begin
assert ( -- rows in nc.stg_sales with no ground date
  select count(*)
  -- select *
  from nc.stg_sales
  where ground_date is null) = 0, 'sales with null ground date';
end
$$;
-----------------------------------------------------------------------------
--< no ground date 12/8/18 updated to same format as equinoxen
-----------------------------------------------------------------------------
/*
-- sales with no ground date, the assumption is that there is no ground date because there is no acquisition: 
1. 
select * from nc.stg_sales where ground_date is null
process these one at a time
select * from gmgl.vehicle_orders where vin = '3GCUKSEJ8JG639023'
select * from gmgl.vehicle_order_events where order_number = 'WKZXC3'

2. in chr.describe_Vehicle?
select * from chr.describe_vehicle where vin = '3GCUKSEJ8JG639023'

3. create a row in nc.vehicles, nc.vehicle_acquisitions, update ground_date in nc.stg_sales

-----------------------------------------------------------------------------------------
-- don't know if i will ever use it, it comes to mind as try to figure out how to 
-- model vehicle orders in this schema
-- maybe another optional field, actual_ground_date
-- actual ground date for in_transit_sale vehicles:
  select aa.*,
    bb.the_date, cc.creation_date as keyper
  from nc.vehicle_acquisitions aa
  left join (
    select d.vin, min(b.the_date) as the_date
    from ads.ext_fact_repair_order a
    inner join dds.dim_date b on a.opendatekey = b.date_key
    inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
    inner join nc.vehicle_acquisitions e on d.vin = e.vin
      and e.in_transit_sale
    group by d.vin) bb on aa.vin = bb.vin
  left join ads.keyper_creation_dates cc on aa.stock_number = cc.stock_number  
  where in_transit_sale

------------------------------------------------------------------------------------------
do
$$
declare
  _stock_number citext := 'G35823';
begin  
-- nc.vehicles
  insert into nc.vehicles(vin,chrome_style_id,model_year,make,model,
    model_code,drive,cab,trim_level,engine,color)
  select f.vin, f.chrome_style_id::integer, chr_model_year, f.make, f.model, f.model_code,
    case
      when f.style like '2WD%' then '2WD'
      when f.style like '4WD%' then '4WD'
    end as drive,
    case 
      when f.style like '%Reg%' then 'reg'
      when f.style like '%Double%' then 'double'
      when f.style like '%Crew%' then 'crew'
      when f.style like '%Ext%' then 'extended'
    end as cab,
    case
      when chr_model_year = '2018' then
        case 
          when f.style in ('4WD Crew Cab 143.5" LTZ w/1LZ','4WD Crew Cab 153.0" LTZ w/1LZ',
            '4WD Double Cab 143.5" LTZ w/1LZ') then '1LZ'
          when f.style in ('4WD Crew Cab 143.5" LTZ w/2LZ','4WD Double Cab 143.5" LTZ w/2LZ') then '2LZ'
          when f.style in ('2WD Reg Cab 133.0" Work Truck','4WD Double Cab 143.5" Work Truck',
            '4WD Reg Cab 119.0" Work Truck','4WD Reg Cab 133.0" Work Truck') then 'WT'
          when f.style in ('4WD Crew Cab 143.5" High Country') then 'high country'
          when f.style in ('4WD Crew Cab 143.5" LT w/1LT','4WD Crew Cab 153.0" LT w/1LT',
            '4WD Double Cab 143.5" LT w/1LT') then '1LT'
          when f.style in ('4WD Crew Cab 143.5" LT w/2LT','4WD Crew Cab 153.0" LT w/2LT',
            '4WD Double Cab 143.5" LT w/2LT') then '2LT'
          when f.style in ('2WD Reg Cab 133.0" LS') then 'LS'
          when f.style in ('4WD Crew Cab 153.0" Custom') then 'custom'
          else 'XXXXXXXXXXXXXXXXXXX'
        end
      when chr_model_year = '2019' and model = 'Silverado 1500' then
        case
          when f.style in ('4WD Crew Cab 147" LT Trail Boss') then 'LT Trail Boss'
          when f.style in ('4WD Crew Cab 147" RST','4WD Double Cab 147" RST') then 'RST'
          when f.style in ('4WD Crew Cab 147" LTZ','4WD Crew Cab 157" LTZ') then 'LTZ'
          when f.style in ('4WD Crew Cab 147" LT','4WD Crew Cab 157" LT','4WD Double Cab 147" LT') then 'LT'
          when f.style in ('4WD Crew Cab 147" High Country') then 'High Country'
          else 'XXXXXXXXXXXXXXXXXXX'
        end
      when chr_model_year = '2019' and model = 'Silverado 1500 LD' then
        case
          when f.style in ('4WD Double Cab LT w/1LT','4WD Double Cab LT w/2LT') then 'LT'
          else 'XXXXXXXXXXXXXXXXXXX'
        end      
    end as trim,  
    f.engine, f.color 
  from (    
    select a.*,
      r.style ->'attributes'->>'id' as chrome_style_id,
      r.style ->'attributes'->>'mfrModelCode' as model_code,
      r.style ->'attributes'->>'name' as style,
      r.style ->'attributes'->>'modelYear' as chr_model_year,
      t.displacement->>'$value' as engine,
      b.response->'attributes'->>'bestMakeName' as make, b.response->'attributes'->>'bestModelName' as model,
      d.color
    from nc.stg_sales a
    left join chr.describe_vehicle b on a.vin = b.vin
    left join jsonb_array_elements(b.response->'style') as r(style) on true
    left join jsonb_array_elements(b.response->'engine') as s(engine) on true
    left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
      and t.displacement ->'attributes'->>'unit' = 'liters'
    left join arkona.xfm_inpmast d on a.stock_number = d.inpmast_stock_number
      and d.current_row 
    where a.stock_number = _stock_number) f;
    
  -- now need the acquisition
  -- ground date = sale date = thru_Date
  insert into nc.vehicle_acquisitions(vin,stock_number,ground_date,thru_date,source,in_transit_sale)
  select vin, stock_number, acct_date, acct_date, 'factory', true
  from nc.stg_sales
  where stock_number = _stock_number;

-- update the ground date in nc.stg_sales
-- select * from nc.stg_sales
  update nc.stg_sales
  set ground_date = acct_date
  where stock_number = _stock_number;
  
end  
$$;


*/
-----------------------------------------------------------------------------
--/> no ground date 
-----------------------------------------------------------------------------

-- ok the mind fuck is , no deal for dealer trades, but also unwinds (deal deleted)
-- which is why i should  use sls.deals instead of bopmast
-- bopmast_id & delivery_date from sls.deals
update nc.stg_sales x
set bopmast_id = y.bopmast_id,
    delivery_date = y.delivery_date
from (    
  select b.*
  from nc.stg_sales a
  left join (
    select stock_number, bopmast_id, max(seq) as seq, max(delivery_date) as delivery_date
    from sls.deals
    group by stock_number, bopmast_id) b on a.stock_number = b.stock_number) y
where x.stock_number = y.stock_number;

update nc.stg_sales x
set sale_type =
  case
    when bopmast_id is null then 'dealer trade'
    when trans_description like '%RYDELL%' then 'ctp'
  end;

update nc.stg_sales x
set sale_type = 'fleet'
where stock_number in (
  select a.stock_number
  from nc.stg_sales a
  inner join (
    select a.control, b.the_date
    from fin.fact_gl a
    inner join dds.dim_date b on a.date_key = b.date_key
    inner join fin.dim_account c on a.account_key = c.account_key
    inner join (
      select distinct d.gl_account
      from fin.fact_fs a 
      inner join fin.dim_fs b on a.fs_key = b.fs_key
        and b.year_month > 201601
        and b.page in (5,8,9,10)
        and b.line in (22,43)
        and b.col = 1 -- sales
      inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
        and c.store = 'ry1'
      inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key) d on c.account = d.gl_account
    where a.post_status = 'Y') b on a.stock_number = b.control);

update nc.stg_sales
set delivery_date = acct_date
where sale_type = 'dealer trade';

-- and finally, everything else is retail
update nc.stg_sales
set sale_type = 'retail'
where sale_type is null;

-- due to the incremental populating of stg_sales, can't do proper constraints
-- need to do them now
  
-- ground vs delivery
select *
from nc.stg_sales
where ground_date > delivery_date

-- -- g34248 is an anomaly, used journal date for ground, didn't work out, make it the same as delivery date
-- update nc.stg_sales set ground_date = '06/08/2018' where stock_number = 'g34248';
-- update nc.vehicle_acquisitions
-- set ground_date = '06/08/2018'
-- where stock_number = 'g34248';

-- 7/3 don't know what else to check at this time, go ahead and proceed with the processing
-- looking at the resulting data will reveal anomalies

-- so, 1st, update acquisitions.thru_date with info from stg_sales
-- issue G34248, ground and delivered on the same day
-- in that case thru_date = ground_date which fits the constraint
update nc.vehicle_acquisitions x
set thru_date  = y.delivery_date
from (
  select a.*
  from nc.stg_sales a
  left join nc.vehicle_acquisitions b on a.stock_number = b.stock_number) y
where x.stock_number = y.stock_number
  and x.thru_date = '12/31/9999';

insert into nc.vehicle_sales
select stock_number, ground_date, vin, coalesce(bopmast_id, -1), delivery_date, sale_type
from nc.stg_sales;


select * from nc.vehicle_acquisitions where stock_number = 'g35722'
select * from nc.stg_sales where stock_number = 'G35188'
update nc.stg_sales set ground_Date = '2018-10-19' where stock_number = 'G35188'
select * from nc.vehicle_acquisitions where stock_number = 'G35188'

--7/4/ ok that's it first take done

-- all tables
select b.stock_number, a.*, b.source, b.ground_date, b.thru_date, c.sale_type, c.bopmast_id, c.delivery_date
from nc.vehicles a
left join nc.vehicle_acquisitions b on a.vin = b.vin
left join nc.vehicle_sales c on b.stock_number = c.stock_number
  and b.ground_date = c.ground_date 
order by delivery_date desc, ground_date desc 

-- all tables, sales for a month
select b.stock_number, a.*, b.source, b.ground_date, b.thru_date, c.sale_type, c.bopmast_id, c.delivery_date, count(sale_type) over (partition by sale_type)
from nc.vehicles a
left join nc.vehicle_acquisitions b on a.vin = b.vin
left join nc.vehicle_sales c on b.stock_number = c.stock_number
  and b.ground_date = c.ground_date
where c.delivery_date between '09/01/2018' and current_date  
order by c.delivery_date

--------------------------------------------------------------------------------------------------------
some notion of inventory id, a combination of vin, stock_number, acq date
an unwind would be a different inventory id
sales would be of an inventory_id
the history of a vehicle could include many inventory ids
natural key would be stock_number and acquisition date

a true unwind implies a new bompast id (as opposed to accounting corrections, eg, uncapping a deal to make changes)

when a vehicle is sold, how dows one know which inventory_id it is, it will always be the 
instance where acq.thru_date > current_Date
7/4 so what i did was put ground_date in vehicle_sales, made stock_number/ground_date PK FK to acquisitions




-- ok, all 2018 silverados sold (non dealer trade) are in nc.vehicles
select a.run_date, a.bopmast_id, a.stock_number, a.vin, a.unit_count, a.delivery_date,
  c.*
-- select count(distinct vin) -- 313 2018 silverados have been sold
from sls.deals a
inner join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row = true
  and b.model = 'silverado 1500'
  and b.year = '2018'
left join nc.vehicles c on a.vin = c.vin

select a.run_date, a.bopmast_id, a.stock_number, a.vin, a.unit_count, a.delivery_date,
  c.*
-- select count(distinct vin) -- 313 2018 silverados have been sold
from sls.deals a
inner join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row = true
  and b.model = 'silverado 1500'
  and b.year = '2018'
left join nc.vehicle_sales c on a.stock_number = c.stock_number
  
-----------------------------------------------------------------------
--/> sales
-----------------------------------------------------------------------


-- shit need to do sales first, sold and unwound in the same month, 
-- will not allow insertion of uwnind acquition where sale does not exist
-- this will be one of the last transactions
-- for unwinds
-- if style count is 1 go ahead, if not, throw an error
select a.stock_number, a.vin, a.model_code, b.chrome_style_id, b.model_code as veh_model_code, 
  c.*
-- select *  
from nc.stg_availability a 
left join nc.vehicles b on a.vin = b.vin
left join chr.describe_vehicle c on a.vin = c.vin
where source = 'unwind'

------------------------------------------------------------------------
--< anomalies
------------------------------------------------------------------------

--< g33744/g34206 dealer trade, unwound, retail sale
select a.inpmast_Stock_number, a.status, a.row_from_date, a.row_thru_date, a.current_row
from arkona.xfm_inpmast a
where inpmast_c
order by inpmast_key

timelime:
G33744 ground 4/27 factory
G33744 delivery 5/23 dealer trade
G34206 unwind 6/6
G34206 delivery 6/20 fleet

select *
from nc.vehicle_acquisitions
where vin = '3GCUKREC2JG364680'

update nc.vehicle_acquisitions
set source = 'unwind',
    thru_date = '06/20/2018'
where stock_number = 'G34206';
-- and the sale record already exists, anomaly resolved
select a.*, b.stock_number, b.ground_date, b.thru_Date, b.source, c.delivery_Date, c.bopmast_id, c.sale_type
from nc.vehicles a
left join nc.vehicle_acquisitions b on a.vin = b.vin
left join nc.vehicle_sales c on b.stock_number = c.stock_number
  and c.ground_date = b.ground_date
where a.vin = '3GCUKREC2JG364680'
order by a.vin, b.ground_date  

--/> g33744/g34206 dealer trade, unwound, retail sale

-- comments:
--    at dealer trade: converted to customer
--    at dealer trade unwind: 1. converted to inventory (not always) 2.stock# change
-- 3GTU2NEC8JG187397: dealer trade, unwound, dealer trade !
-- instances of vins with multiple new car stock numbers
select inpmast_vin, inpmast_stock_number, status, type_n_u, inpmast_key, row_From_Date, row_thru_date
from arkona.xfm_inpmast
where inpmast_vin in (
  select inpmast_vin
  from (
    select inpmast_vin, inpmast_stock_number
    from arkona.xfm_inpmast
    where type_n_u = 'N'
      and inpmast_stock_number not like '%R'
    group by inpmast_vin, inpmast_stock_number) a
  group by inpmast_vin
  having count(*) > 1)
order by inpmast_vin, inpmast_key  

------------------------------------------------------------------
-- no nc.vehicle_sales records for 1GCVKREC9JZ137555
select a.*, b.stock_number, b.ground_date, b.thru_Date, b.source, c.delivery_Date, c.bopmast_id, c.sale_type
from nc.vehicles a
left join nc.vehicle_acquisitions b on a.vin = b.vin
left join nc.vehicle_sales c on b.stock_number = c.stock_number
  and c.ground_date = b.ground_date
where a.vin = '1GCVKREC9JZ137555'
order by a.vin, b.ground_date  

need a timeline for this dude
select inpmast_stock_number, status, date_delivered, row_From_date, row_thru_date, current_row
from arkona.xfm_inpmast
where inpmast_vin = '1GCVKREC9JZ137555'
order by inpmast_key

select b.bopname_search_name, a.*
from sls.deals a
left join arkona.xfm_bopname b on a.buyer_bopname_id = b.bopname_Record_key
where vin = '1GCVKREC9JZ137555'
order by run_date

2/24 sale retail
3/3 unwind 
5/28 sale retail
6/11 unwind

update nc.vehicle_sales
set delivery_date = '02/24/2018',
    sale_type = 'retail',
    bopmast_id = 46549
where stock_number = '32185'
  and ground_date = '10/09/2017';

insert into nc.vehicle_sales values
('32185','03/03/2018','1GCVKREC9JZ137555',48396,'05/28/2018','retail');
------------------------------------------------------------------
unwind acquisitions
1GCVKREC5HZ402921 / 31475 ground_date = 08/20
3GCUKREC1HG392187 / 31011 ground date = 07/21

select a.*, b.stock_number, b.ground_date, b.thru_Date, b.source, c.delivery_Date, c.bopmast_id, c.sale_type
from nc.vehicles a
left join nc.vehicle_acquisitions b on a.vin = b.vin
left join nc.vehicle_sales c on b.stock_number = c.stock_number
  and c.ground_date = b.ground_date
where a.vin = '1GCVKREC5HZ402921'
order by a.vin, b.ground_date  

select b.bopname_search_name, a.*
from sls.deals a
left join arkona.xfm_bopname b on a.buyer_bopname_id = b.bopname_Record_key
where vin = '1GCVKREC5HZ402921'
order by run_date

sale 8/17/17 retail
unwind 8/20
sale 8/31/17 retail

update nc.vehicle_acquisitions
set thru_date = '08/17/2017'
where stock_number = '31475'
  and ground_date = '07/13/2017';

update nc.vehicle_sales
set delivery_date = '08/17/2017',
    bopmast_id = 43608
where stock_number = '31475'; 

update nc.vehicle_acquisitions
set ground_date = '08/20/2017',
    thru_date = '08/31/2017'
where stock_number = '31475' 
  and source = 'unwind';

insert into nc.vehicle_sales values
('31475','08/20/2017','1GCVKREC5HZ402921',43826,'08/31/2017','retail');


select a.*, b.stock_number, b.ground_date, b.thru_Date, b.source, c.delivery_Date, c.bopmast_id, c.sale_type
from nc.vehicles a
left join nc.vehicle_acquisitions b on a.vin = b.vin
left join nc.vehicle_sales c on b.stock_number = c.stock_number
  and c.ground_date = b.ground_date
where a.vin = '3GCUKREC1HG392187'
order by a.vin, b.ground_date  

select b.bopname_search_name, a.*
from sls.deals a
left join arkona.xfm_bopname b on a.buyer_bopname_id = b.bopname_Record_key
where vin = '3GCUKREC1HG392187'
order by run_date
sale 6/30/17 retail 42937
unwind 7/21
sale 9/2/17 retail  43918

update nc.vehicle_acquisitions
set thru_date = '06/30/2017'
where stock_number = '31011'
  and source = 'factory';

update nc.vehicle_acquisitions
set ground_date = '07/21/2017',
    thru_date = '09/02/2017'
where stock_number = '31011'
  and source = 'unwind';

update nc.vehicle_sales
set delivery_date = '06/30/2017',
    bopmast_id = 42937
where stock_number = '31011';

insert into nc.vehicle_sales values
('31011','07/21/2017','3GCUKREC1HG392187',43918,'09/02/2017','retail');    

-- only one "missing" sls.deals record
select a.*, b.*, c.*
from sls.deals a
inner join nc.vehicles b on a.vin = b.vin
left join nc.vehicle_sales c on a.stock_number = c.stock_number
where c.stock_number is null
  and a.vehicle_type_code = 'N'
  and b.model_year = '2018'

select a.*, b.stock_number, b.ground_date, b.thru_Date, b.source, c.delivery_Date, c.bopmast_id, c.sale_type
from nc.vehicles a
left join nc.vehicle_acquisitions b on a.vin = b.vin
left join nc.vehicle_sales c on b.stock_number = c.stock_number
  and c.ground_date = b.ground_date
where a.vin = '3GCUKSEC7JG109451'
order by a.vin, b.ground_date  

select b.bopname_search_name, a.*
from sls.deals a
left join arkona.xfm_bopname b on a.buyer_bopname_id = b.bopname_Record_key
where vin = '3GCUKSEC7JG109451'
order by run_date  

sale 3/6 retail 46720
3/13 unwind

update nc.vehicle_acquisitions
set thru_date = '03/06/2018'
where stock_number = 'G33184';
insert into nc.vehicle_sales values 
('G33184','02/13/2018','3GCUKSEC7JG109451',46720,'03/06/2018','retail');
insert into nc.vehicle_acquisitions values 
('3GCUKSEC7JG109451','G33184','03/13/2018','12/31/9999','unwind');


-- 7/5 ctp deals on june statement show as retail in vehicle_sales
-- don't know how or why, just fix it
select *
from nc.vehicle_sales
where stock_number in ('g33372','g33907','g33928','g34019','g34110','g34125','g34175')

update nc.vehicle_sales
set sale_type = 'ctp'
where stock_number in ('g33372','g33907','g33928','g34019','g34110','g34125','g34175')


------------------------------------------------------------------------
--/> anomalies
------------------------------------------------------------------------

-- add sku to vehicles
-- only 1 without sku, fuck'em
alter table nc.vehicles
add column sku_id integer;
create index on nc.vehicles(sku_id);

update nc.vehicles x
set sku_id = y.sku_id
from (
  select aa.vin, bb.*
  from nc.vehicles aa
  left join gmgl.sku bb on aa.engine = bb.engine
    and
      case
        when bb.cab = 'regular' then aa.cab = 'reg'
        else aa.cab = bb.cab
      end
    and 
      case
        when bb.vehicle_trim = '1WT' then  aa.trim_level = 'WT'
        when bb.vehicle_trim = 'GAJ' then aa.trim_level = 'high country'
        when bb.vehicle_Trim = '1LS' then aa.trim_level = 'LS'
        when bb.vehicle_trim = 'ST9' then aa.trim_level = 'custom'
        else bb.vehicle_trim = aa.trim_level
      end
    and
      case
        when bb.color = 'unripened green' then aa.color = 'Unripened Green Metallic'
        else bb.color = aa.color
      end) y
where x.vin = y.vin


---------------------------------------------------------------------
-- queries
---------------------------------------------------------------------
-- vehicle - acquisitions - sales
select a.*, b.stock_number, b.ground_date, b.thru_Date, b.source, c.delivery_Date, c.bopmast_id, c.sale_type
from nc.vehicles a
left join nc.vehicle_acquisitions b on a.vin = b.vin
left join nc.vehicle_sales c on b.stock_number = c.stock_number
  and c.delivery_date = b.thru_date
-- where a.vin = '3GCUKREC2JG364680'
where c.delivery_date > '06/30/2018'
order by a.vin, b.ground_date  




-- 365 days sales by sku
select b.sku_id, count(a.stock_number) as sales_365
from nc.vehicle_sales a
inner join nc.vehicles b on a.vin = b.vin
  and b.model_year = '2018'
where a.delivery_date between current_date - 365 and current_date
  and a.sale_type <> 'fleet'
group by b.sku_id
order by count(*) desc

-- 90 days sales by sku
select b.sku_id, count(a.stock_number) as sales_365
from nc.vehicle_sales a
inner join nc.vehicles b on a.vin = b.vin
  and b.model_year = '2018'
where a.delivery_date between current_date - 90 and current_date
  and a.sale_type <> 'fleet'
group by b.sku_id
order by count(*) desc
-----------------------------------------------------------------------------------


-- for all "in stock" in inpmast, does it exist in acquistions, if not is it in transit
select a.inpmast_stock_number, b.*, d.*
from arkona.xfm_inpmast a
left join nc.vehicle_acquisitions b on a.inpmast_stock_number = b.stock_number
left join gmgl.vehicle_orders c on a.inpmast_vin = c.vin
  left join (
    SELECT a.order_number, a.order_type, b.event_code, c.description, c.category, b.from_date
    FROM gmgl.vehicle_orders a
    JOIN gmgl.vehicle_order_events b ON a.order_number = b.order_number
    JOIN gmgl.vehicle_event_codes c ON b.event_code = c.code
    where b.thru_date = (
        select max(thru_date)
        from gmgl.vehicle_order_events
        where order_number = b.order_number)
    group by a.order_number, a.order_type, b.event_code, c.description, c.category, b.from_date) d on c.order_number = d.order_number
where a.status = 'I'
  and a.model = 'silverado 1500'
  and a.type_n_u = 'N'  
  and a.current_row = true


-------------------------------------------------------------------------------------
--< gmgl.json_get_skus()
-------------------------------------------------------------------------------------
-- replacement for sold_data
select a.cab, a.engine, a.trim_level, a.color, a.sku_id,
  count(*) filter (where b.sale_type = 'retail' and b.delivery_date between current_date - 90 and current_date) as retail_sold90,
  count(*) filter (where b.sale_type = 'fleet' and b.delivery_date between current_date - 90 and current_date) as fleet_sold90,
  count(*) filter (where b.sale_type = 'ctp' and b.delivery_date between current_date - 90 and current_date) as ctp_sold90,
  count(*) filter (where b.sale_type = 'dealer trade' and b.delivery_date between current_date - 90 and current_date) as dealer_trade_sold90,
  count(*) filter (where b.sale_type = 'retail' and b.delivery_date between current_date - 365 and current_date) as retail_sold365,
  count(*) filter (where b.sale_type = 'fleet' and b.delivery_date between current_date - 365 and current_date) as fleet_sold365,
  count(*) filter (where b.sale_type = 'ctp' and b.delivery_date between current_date - 365 and current_date) as ctp_sold365,
  count(*) filter (where b.sale_type = 'dealer trade' and b.delivery_date between current_date - 365 and current_date) as dealer_trade_sold365
from nc.vehicles a
inner join nc.vehicle_sales b on a.vin = b.vin
where a.model_year = '2018'
group by a.cab, a.engine, a.trim_level, a.color, a.sku_id  
order by a.cab, a.engine, a.trim_level, a.color

-- current inventory
select a.cab, a.engine, a.trim_level, a.color, a.sku_id,
  count(*) filter (where b.vin is not null) as in_stock
from nc.vehicles a
left join nc.vehicle_acquisitions b on a.vin = b.vin
  and b.thru_date > current_date
where a.model_year = '2018'
group by a.cab, a.engine, a.trim_level, a.color, a.sku_id
order by a.cab, a.engine, a.trim_level, a.color


-- replacement for daily
drop table if exists wtf;
-- added sale_type, includes fleet
create temp table wtf as
with 
  sku_days as ( -- one row for each sku_ic in gmgl.sku for each day for the previous 180 days
    select a.the_date, b.sku_id
    from dds.dim_date a
    left join gmgl.sku b on 1 = 1
    where a.the_date between current_Date - 180 and current_date -1)
select c.the_date, c.sku_id, c.inventory, g.sold, g.sale_type
from (    
  select a.the_date, a.sku_id, count(b.sku_id) as inventory
  from sku_days a
  left join ( -- c: inventory
    select bb.sku_id, aa.ground_date, aa.thru_date
    from nc.vehicle_acquisitions aa
    inner join nc.vehicles bb on aa.vin = bb.vin
      and bb.model_year = '2018') b on a.sku_id = b.sku_id and a.the_date between b.ground_date and b.thru_date - 1
  group by a.the_date, a.sku_id) c
left join ( -- g
  select d.the_date, d.sku_id, e.sale_type, count(e.sku_id) as sold
  from sku_days d
  left join ( -- e sales
    select bb.sku_id, aa.delivery_date, aa.sale_type
    from nc.vehicle_sales aa
    inner join nc.vehicles bb on aa.vin = bb.vin
      and bb.model_year = '2018') e  on d.sku_id = e.sku_id and d.the_date = e.delivery_date
  group by d.the_date, d.sku_id, e.sale_type) g on c.the_date = g.the_Date
    and c.sku_id = g.sku_id
where sale_type is null    
    

select * 
from wtf
where sold <> 0

select *
from (
select sku_id, sale_type, count(*)
from wtf
where sold <> 0
group by sku_id, sale_type) a
left join gmgl.sku b on a.sku_id = b.sku_id
order by a.sku_id, sale_type desc 



-- was freaking out looking at dif in june sales between above and sls.deals,
-- the dif is fucking dealer trades   
select a.*, c.sale_type
from wtf a
inner join nc.vehicles b on a.sku_id = b.sku_id
inner join nc.vehicle_sales c on b.vin = c.vin
  and a.the_date = c.delivery_date
where the_date between '06/01/2018' and '06/30/2018'
  and sold <> 0
order by the_date, sku_id  

select year_month, sum(sold)
from wtf a
inner join dds.dim_date b on a.the_date = b.the_date
group by year_month


select *
from nc.vehicle_acquisitions a
-- inner join nc.vehicles b on a.vin = b.vin
--   and b.model_year = '2018'
left join gmgl.vehicle_orders b on a.vin = b.vin
where a.source = 'dealer trade'

-- how many non retail sales are in these numbers
select year_month, c.sale_type, count(*)
from wtf a
inner join dds.dim_date aa on a.the_date = aa.the_date
inner join nc.vehicles b on a.sku_id = b.sku_id
inner join nc.vehicle_sales c on b.vin = c.vin
  and a.the_date = c.delivery_date  
where a.the_date between '01/01/2018' and '06/30/2018'
  and sold <> 0
group by year_month, c.sale_type

alter table nc.silverado_sierra_chrome_attributes
rename to z_unused_silverado_sierra_chrome_attributes





 
 