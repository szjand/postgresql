﻿select * from nc.vehicle_invoices where vin = '1GC4YREYXLF146372'
compare this against nc.vehicle_invoices
then do the same for gmgl.vehicle_invoices
look at parsing that data in sql rather than python

from all_nc_inventory_nightly

truncate nc.vehicle_invoices cascade;  
insert into nc.vehicle_invoices  
select vin, trim(left(raw_invoice, position('GENERAL MOTORS LLC' in raw_invoice)-1)):: citext as description,
  case 
    when position('RYDELL' in raw_invoice) = 0 and position('CADILLAC OF GRAND FORKS' in raw_invoice) = 0 then 'dealer trade'
    else 'factory'
  end as source,
  left(substring(raw_invoice from position('MODEL' in raw_invoice) + 74 for 8), 
    position(' ' in substring(raw_invoice from position('MODEL' in raw_invoice) + 74 for 8))) as model_code,
  substring(raw_invoice from position(E'\n' in raw_invoice) + 1 for 3) as color_code,
  trim(substring(raw_invoice from position(E'\n' in raw_invoice) + 6 for 31))::citext as color,
--   substring(raw_invoice from position('INVOICE' in raw_invoice) + 232 for 8)::date as invoice_date,
  substring(substring(raw_invoice from 550 for 50) from position('INVOICE' in (substring(raw_invoice from 550 for 50))) + 8 for 8)::date as invoice_date,
  substring(raw_invoice from position('SHIPPED' in raw_invoice) + 8 for 8)::date as shipped_date,
  substring(raw_invoice from position('EXP I/T' in raw_invoice) + 8 for 8)::date as exp_in_transit_date       
from (
  select vin, replace(raw_invoice, '&amp;', '&') as raw_invoice
  from gmgl.vehicle_invoices
  where thru_date > current_date
    and position('CREDIT FOR INVOICE' in raw_invoice) = 0) a-- these are weird, i dont remember why, but should be excluded

    
-- create index on gmgl.vehicle_invoices(raw_invoice);
-- 
-- ERROR:  index row size 2736 exceeds maximum 2712 for index "vehicle_invoices_raw_invoice_idx"
-- HINT:  Values larger than 1/3 of a buffer page cannot be indexed.
-- Consider a function index of an MD5 hash of the value, or use full text indexing.
-- ********** Error **********
-- 
-- ERROR: index row size 2736 exceeds maximum 2712 for index "vehicle_invoices_raw_invoice_idx"
-- SQL state: 54000
-- Hint: Values larger than 1/3 of a buffer page cannot be indexed.
-- Consider a function index of an MD5 hash of the value, or use full text indexing.

drop table if exists inv_1;  --5398
create temp table inv_1 as
select vin,
  trim((left(split_part(raw_invoice, E'\n', 1), position('GENERAL' in raw_invoice) -1))::citext) as description,
  case 
    when position('RYDELL' in raw_invoice) = 0 and position('CADILLAC OF GRAND FORKS' in raw_invoice) = 0 then 'dealer trade'
    else 'factory'
  end as source,  
  trim((left(split_part(raw_invoice, E'\n', 8), 7))::citext) as model_code,
  trim((left(split_part(raw_invoice, E'\n', 2), 3))::citext) as color_code,
  trim((trim(substring(split_part(raw_invoice, E'\n', 2), 6, 31)))::citext) as color,
  left(trim(split_part(raw_invoice, 'INVOICE', 3)), 8)::date as invoice_date,
  left(trim(split_part(raw_invoice, 'SHIPPED', 2)), 8)::date as shipped_date,
  left(trim(split_part(raw_invoice, 'EXP I/T', 2)), 8)::date as exp_in_transit_date,
  md5(a::text)
from (
  select vin, replace(raw_invoice, '&amp;', '&') as raw_invoice
  from gmgl.vehicle_invoices
  where thru_date > current_date
    and position('CREDIT FOR INVOICE' in raw_invoice) = 0) a
    
select *
from gmgl.vehicle_invoices
where vin = '3GNAXUEV4LS576027'

select 
  trim(left(split_part(raw_invoice, 'EMPINC:', 2), 10))::numeric as empinc,
  trim(left(split_part(raw_invoice, 'EMPLOY:', 2), 10))::numeric as employ,
  trim(left(split_part(raw_invoice, 'SUPINC:', 2), 10))::numeric as supinc,
  trim(left(split_part(raw_invoice, 'SUPPLR:', 2), 10))::numeric as supplr
from gmgl.vehicle_invoices
where vin = '3GNAXUEV4LS576027'


