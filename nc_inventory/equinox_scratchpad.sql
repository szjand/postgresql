﻿select * 
from nc.stg_sales a
join nc.vehicles b on a.vin = b.vin

select * from nc.vehicles limit 10

select * from nc.stg_sales where ground_date is null

select * from nc.vehicles where vin = '2GNAXWEX1J6133925'

select  *
from nc.vehicle_Acquisitions a
join nc.vehicle_sales b on a.stock_number = b.stock_number
  and a.vin = b.vin
order by a.vin  

select *
from chr.describe_Vehicle
where vin = '2GNAXSEV2J6279788'



select a.*, b.color_code, b.color
from nc.stg_sales a
join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
where a.ground_date is null



select *
from nc.vehicle_Sales a
join nc.vehicle_acquisitions b on a.vin = b.vin
where a.sold_in_Transit

select distinct source from nc.vehicle_acquisitions

select *
from nc.vehicle_acquisitions
where source = 'intransit sale'



delete
from nc.stg_sales
where stock_number = '32021'
  and acct_date <> '09/30/2018'


select *  --339
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
where b.model = 'equinox'

select a.vin
from sls.ext_Bopmast_partial a
join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
  and b.year in (2018, 2019)
  and b.model = 'equinox'
where vehicle_Type = 'N'
  and not exists (
    select 1
    from nc.vehicles
    where vin = a.vin)



select * 
from sls.ext_bopmast_partial
where vin in ('2GNAXTEX9J6199833','2GNAXSEV6J6122166','2GNAXHEV6J6293597','2GNAXSEV1J6110779')

-- bens spreadsheet

select sale_type, delivery_Date, engine, color
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
where b.model = 'equinox'
  and b.trim_level = 'LT'
  and b.drive = 'FWD'
order by color

select sale_type, delivery_Date, engine, color
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
where b.model = 'equinox'
  and b.trim_level = 'LT'
  and b.drive = 'AWD'
order by color


select sale_type, delivery_Date, engine, color
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
where b.model = 'equinox'
  and b.trim_level = 'LS'
  and b.drive = 'FWD'
order by color


select sale_type, delivery_Date, engine, color
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
where b.model = 'equinox'
  and b.trim_level = 'LS'
  and b.drive = 'AWD'
order by color



select sale_type, delivery_Date, engine, color
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
where b.model = 'equinox'
  and b.trim_level = 'PREM'
  and b.drive = 'FWD'
order by color

select sale_type, delivery_Date, engine, color
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
where b.model = 'equinox'
  and b.trim_level = 'PREM'
  and b.drive = 'AWD'
order by color





drop table if exists colors;
create temp table colors as
select c.mfr_color_code, c.mfr_color_name
from dao.veh_trim_styles a
inner join dao.lkp_veh_ext_color b on a.vehicle_id = b.vehicle_id
inner join dao.def_ext_color c on b.ext_color_id = c.ext_color_id
where year between 2018 and 2019
  and a.model = 'equinox'
group by c.mfr_color_code, c.mfr_color_name
order by c.mfr_color_name;

select * from dao.def_ext_color where mfr_color_name like 'cent%'

drop table if exists sales;
create temp table sales as
select trim_level, drive, engine, sales_window, color, count(*) as sales
from (
select color, trim_level, drive, engine, 
  case 
    when delivery_Date between current_date - 90 and current_date then 90
    when delivery_Date between current_date - 365 and current_date then 365
  end as sales_window
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
where b.model = 'equinox'
  and delivery_Date between current_date - 365 and current_date
  and sale_type = 'retail') x
group by trim_level, drive, engine, sales_window, color
order by trim_level, drive, engine;


select *  
from colors a
left join sales b on a.mfr_color_name = b.color
order by trim_level, drive, engine, sales_window

-- spreadsheet: equinox_sales_1
select a.mfr_color_name,
  sum(sales) filter (where trim_level = 'LS' and sales_window = 90) as ls_fwd_90,
  sum(sales) filter (where trim_level = 'LS' and sales_window = 365) as ls_fwd_365,
  sum(sales) filter (where trim_level = 'LT' and drive = 'AWD' and engine = '1.5' and sales_window = 90) as lt_awd_15_90,
  sum(sales) filter (where trim_level = 'LT' and drive = 'AWD' and engine = '1.5' and sales_window = 365) as lt_awd_15_365,
  sum(sales) filter (where trim_level = 'LT' and drive = 'AWD' and engine = '1.6' and sales_window = 90) as lt_awd_16_90,
  sum(sales) filter (where trim_level = 'LT' and drive = 'AWD' and engine = '1.6' and sales_window = 365) as lt_awd_16_365,
  sum(sales) filter (where trim_level = 'LT' and drive = 'AWD' and engine = '2.0' and sales_window = 90) as lt_awd_20_90,
  sum(sales) filter (where trim_level = 'LT' and drive = 'AWD' and engine = '2.0' and sales_window = 365) as lt_awd_20_365,
  sum(sales) filter (where trim_level = 'LT' and drive = 'FWD' and engine = '1.5' and sales_window = 90) as lt_fwd_15_90,
  sum(sales) filter (where trim_level = 'LT' and drive = 'FWD' and engine = '1.5' and sales_window = 365) as lt_fwd_15_365,
  sum(sales) filter (where trim_level = 'PREM' and drive = 'AWD' and engine = '1.5' and sales_window = 90) as prem_awd_15_90,
  sum(sales) filter (where trim_level = 'PREM' and drive = 'AWD' and engine = '1.5' and sales_window = 365) as prem_awd_15_365,
  sum(sales) filter (where trim_level = 'PREM' and drive = 'AWD' and engine = '2.0' and sales_window = 90) as prem_awd_20_90,
  sum(sales) filter (where trim_level = 'PREM' and drive = 'AWD' and engine = '2.0' and sales_window = 365) as prem_awd_20_365
-- select *  
from colors a
left join sales b on a.mfr_color_name = b.color
group by a.mfr_color_name
order by a.mfr_color_name

Cajun Red Tintcoat
Iridescent Pearl Tricoat
Ivy Metallic
Kinetic Blue Metallic
Mosaic Black Metallic
Nightfall Gray Metallic
Orange Burst Metallic
Pacific Blue Metallic
Pepperdust Metallic
Sandy Ridge Metallic
Satin Steel Metallic
Silver Ice Metallic
Storm Blue
Summit White

-- 10/29 current inventory
select *
from nc.vehicle_acquisitions a
join nc.vehicles b on a.vin = b.vin
where b.model = 'equinox'
  and a.thru_Date > current_Date

drop table if exists inv;
create temp table inv as
select current_date, f.the_date, f.control, 
  g.inpmast_vin, g.year, g.make, g.model, g.model_code, 
  g.color_code, g.color,
  case f.journal_code
    when 'PVI' then 'factory'
    when 'CDH' then 'dealer trade'
    when 'GJE' then 'ctp'
  end as source
from ( -- accounting journal
  select a.control, d.journal_code, max(b.the_date) as the_date, sum(a.amount) as amount
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_date between current_Date - 90 and current_date--'06/22/2018'
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account = '123700'
  inner join fin.dim_journal d on a.journal_key = d.journal_key
    and d.journal_code in ('PVI','CDH','GJE')
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where a.post_status = 'Y'
  group by a.control, d.journal_code
  having sum(a.amount) > 10000) f
left join arkona.xfm_inpmast g on f.control = g.inpmast_stock_number
  and g.current_row = true
  and g.year in ('2018', '2019')
where g.model = 'equinox'





select a.*, 
  case
    when b.vin is null then 'in_transit'
    else 'on_ground'
  end as status, b.thru_date,
  c.*, d.*
from inv a
left join nc.vehicle_acquisitions b on a.control = b.stock_number
left join colors c on a.color = c.mfr_color_name
left join (
  select a.order_number, a.order_type, a.vin, a.vehicle_Trim, a.color as color_code, a.model_code, a.model_year,
    d.description as color
  -- select *  
  from gmgl.vehicle_orders a
  join gmgl.vehicle_order_events b on a.order_number = b.order_number 
--     and thru_date = '12/31/9999' 
  join gmgl.vehicle_event_codes c on b.event_code = c.code --and c.category = 'In Transit'
  join gmgl.x_vehicle_option_codes d on a.color = d.option_code 
    and d.category = 'Color'
  where a.vin is not null
    and a.alloc_group = 'EQUINX') d on a.inpmast_vin = d.vin

-- centennial blue metallic, think it should be storm blue

select a.order_number, a.order_type, a.vin, a.vehicle_Trim, a.color as color_code, a.model_code, a.model_year,
  d.description as color
-- select *  
from gmgl.vehicle_orders a
join gmgl.vehicle_order_events b on a.order_number = b.order_number 
  and thru_date = '12/31/9999' 
join gmgl.vehicle_event_codes c on b.event_code = c.code --and c.category = 'In Transit'
join gmgl.x_vehicle_option_codes d on a.color = d.option_code 
  and d.category = 'Color'
where a.vin is not null
  and a.alloc_group = 'EQUINX'

select * from dao.def_ext_color where mfr_color_code = 'G35'


select *
from dao.veh_trim_styles a
inner join dao.lkp_veh_ext_color b on a.vehicle_id = b.vehicle_id
inner join dao.def_ext_color c on b.ext_color_id = c.ext_color_id
where c.mfr_color_code = 'G35'
  and model = 'equinox'


  fuck it, use accounting for invoiced vehicles, acquistions for status


drop table if exists inv;
create temp table inv as
select a.control-- , sum(a.amount) as amount
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_date between current_Date - 900 and current_date--'06/22/2018'
inner join fin.dim_account c on a.account_key = c.account_key 
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and c.account in ( -- all new vehicle inventory account
    select b.account
    from fin.dim_account b 
    where b.account_type = 'asset'
      and b.department_code = 'nc'
      and b.typical_balance = 'debit'
      and b.current_row
      and b.account <> '126104')   
--   and b.the_date between current_Date - 600 and current_date -- this has no effect
group by a.control
having sum(a.amount) > 10000;


select a.control, b.inpmast_vin, b.body_style, c.mfr_color_name,
  f.*
from inv a
join arkona.xfm_inpmast b on a.control = b.inpmast_stock_number
  and b.current_row
  and b.model = 'equinox'
left join colors c on b.color_code = c.mfr_color_code
left join nc.vehicles d on b.inpmast_vin = d.vin
left join nc.vehicle_acquisitions e on a.control = e.stock_number
left join chr.describe_vehicle f on b.inpmast_vin = f.vin

-- vins not in chrome

select inpmast_vin
from arkona.xfm_inpmast a
where a.current_row
  and a.status = 'I'
  and a.model = 'equinox'
  and a.type_n_u = 'n'
  and not exists (
    select 1
    from chr.describe_vehicle
    where vin = a.inpmast_vin)


select a.control, b.inpmast_vin as vin
from inv a
join arkona.xfm_inpmast b on a.control = b.inpmast_stock_number
  and b.current_row
  and b.model = 'equinox'




drop table if exists inv_1;
create temp table inv_1 as
select a.control as stock_number, b.inpmast_vin as vin, b.color_code
from inv a
join arkona.xfm_inpmast b on a.control = b.inpmast_stock_number
  and b.current_row
  and b.model = 'equinox';

-- drop table if exists on_ground;
-- create temp table on_ground as

drop table if exists inv_2;
create temp table inv_2 as
select trim_level, drive, engine, color, status, count(*) as the_count
from (
  select *
  from  (
    select b.trim_level, b.drive, engine, color, 'on_ground' as status
    from inv_1 a
    join nc.vehicles b on a.vin = b.vin  
    union
    select 
      case 
        when c.style like '%4dr LT%' then 'LT'
        when c.style like '%4dr LS%' then 'LS'
        when c.style like '%4dr L%' then 'L'
        when c.style like '%Premier%' then 'PREM'
        else 'XXXXXX'
      end as trim_level,
      case drive_train
        when 'All Wheel Drive' then 'AWD'
        when 'Front Wheel Drive' then 'FWD'
        else 'XXX'
      end as drive_train,  
      c.engine_displacement, mfr_color_name, 'in_transit'
    from (
      select a.*, c.*,
          jsonb_array_elements(b.response->'style')->'attributes'->>'id' as chr_style_id,
          jsonb_array_elements(b.response->'style')->'attributes'->>'mfrModelCode' as chr_model_code,
          jsonb_array_elements(b.response->'style')->'attributes'->>'modelYear' as chr_model_year,
          jsonb_array_elements(b.response->'style')->'attributes'->>'name' as style,
          jsonb_array_elements(jsonb_array_elements(response->'engine')->'displacement'->'value')->>'$value' as engine_displacement,
          jsonb_array_elements(b.response->'style')->'attributes'->>'drivetrain' as drive_train
      from (
        select *
        from inv_1 a
        where not exists (
          select 1
          from nc.vehicles
          where vin = a.vin)) a  
      left join chr.describe_vehicle b on a.vin = b.vin
      left join colors c on a.color_code = c.mfr_color_code) c) x) z
group by trim_level, drive, engine, color, status


select a.mfr_color_name,
  sum(sales) filter (where b.trim_level = 'LS' and sales_window = 90) as ls_fwd_90,
  sum(sales) filter (where b.trim_level = 'LS' and sales_window = 365) as ls_fwd_365,
  sum(sales) filter (where b.trim_level = 'LT' and b.drive = 'AWD' and b.engine = '1.5' and sales_window = 90) as lt_awd_15_90,
  sum(sales) filter (where b.trim_level = 'LT' and b.drive = 'AWD' and b.engine = '1.5' and sales_window = 365) as lt_awd_15_365,
  sum(sales) filter (where b.trim_level = 'LT' and b.drive = 'AWD' and b.engine = '1.6' and sales_window = 90) as lt_awd_16_90,
  sum(sales) filter (where b.trim_level = 'LT' and b.drive = 'AWD' and b.engine = '1.6' and sales_window = 365) as lt_awd_16_365,
  sum(sales) filter (where b.trim_level = 'LT' and b.drive = 'AWD' and b.engine = '2.0' and sales_window = 90) as lt_awd_20_90,
  sum(sales) filter (where b.trim_level = 'LT' and b.drive = 'AWD' and b.engine = '2.0' and sales_window = 365) as lt_awd_20_365,
  sum(sales) filter (where b.trim_level = 'LT' and b.drive = 'FWD' and b.engine = '1.5' and sales_window = 90) as lt_fwd_15_90,
  sum(sales) filter (where b.trim_level = 'LT' and b.drive = 'FWD' and b.engine = '1.5' and sales_window = 365) as lt_fwd_15_365,
  sum(sales) filter (where b.trim_level = 'PREM' and b.drive = 'AWD' and b.engine = '1.5' and sales_window = 90) as prem_awd_15_90,
  sum(sales) filter (where b.trim_level = 'PREM' and b.drive = 'AWD' and b.engine = '1.5' and sales_window = 365) as prem_awd_15_365,
  sum(sales) filter (where b.trim_level = 'PREM' and b.drive = 'AWD' and b.engine = '2.0' and sales_window = 90) as prem_awd_20_90,
  sum(sales) filter (where b.trim_level = 'PREM' and b.drive = 'AWD' and b.engine = '2.0' and sales_window = 365) as prem_awd_20_365
-- select *  
from colors a
left join sales b on a.mfr_color_name = b.color
group by a.mfr_color_name
order by a.mfr_color_name

select * from inv_2 order by trim_level, drive, engine, color, status

select distinct trim_level, drive, engine from inv_2 order by trim_level, drive, engine

select a.mfr_color_name,
  coalesce(sum(the_count) filter (where b.trim_level = 'LS' and status = 'in_transit'), 0) as ls_fwd_it,
  coalesce(sum(the_count) filter (where b.trim_level = 'LS' and status = 'on_ground'), 0) as ls_fwd_og,
  coalesce(sum(the_count) filter (where b.trim_level = 'LT' and b.drive = 'AWD' and b.engine = '1.5' and status = 'in_transit'), 0) as lt_awd_15_it,
  coalesce(sum(the_count) filter (where b.trim_level = 'LT' and b.drive = 'AWD' and b.engine = '1.5' and status = 'on_ground'), 0) as lt_awd_15_og,
  coalesce(sum(the_count) filter (where b.trim_level = 'LT' and b.drive = 'AWD' and b.engine = '1.6' and status = 'in_transit'), 0) as lt_awd_16_it,
  coalesce(sum(the_count) filter (where b.trim_level = 'LT' and b.drive = 'AWD' and b.engine = '1.6' and status = 'on_ground'), 0) as lt_awd_16_og, 
  coalesce(sum(the_count) filter (where b.trim_level = 'LT' and b.drive = 'AWD' and b.engine = '2.0' and status = 'in_transit'), 0) as lt_awd_20_it,
  coalesce(sum(the_count) filter (where b.trim_level = 'LT' and b.drive = 'AWD' and b.engine = '2.0' and status = 'on_ground'), 0) as lt_awd_20_og,  
  coalesce(sum(the_count) filter (where b.trim_level = 'LT' and b.drive = 'FWD' and b.engine = '1.5' and status = 'in_transit'), 0) as lt_fwd_15_it,
  coalesce(sum(the_count) filter (where b.trim_level = 'LT' and b.drive = 'FWD' and b.engine = '1.5' and status = 'on_ground'), 0) as lt_fwd_15_og,
  coalesce(sum(the_count) filter (where b.trim_level = 'PREM' and b.drive = 'AWD' and b.engine = '1.5' and status = 'in_transit'), 0) as prem_awd_15_it,
  coalesce(sum(the_count) filter (where b.trim_level = 'PREM' and b.drive = 'AWD' and b.engine = '1.5' and status = 'on_ground'), 0) as prem_awd_15_og,
  coalesce(sum(the_count) filter (where b.trim_level = 'PREM' and b.drive = 'AWD' and b.engine = '2.0' and status = 'in_transit'), 0) as prem_awd_20_it,
  coalesce(sum(the_count) filter (where b.trim_level = 'PREM' and b.drive = 'AWD' and b.engine = '2.0' and status = 'on_ground'), 0) as prem_awd_20_og
-- select *  
from colors a
left join inv_2 b on a.mfr_color_name = b.color
group by a.mfr_color_name
order by a.mfr_color_name

-- this is the equinox_sales_inventory spreadsheet sent to ben on 10/31/18
-- needs to be modified to include orders
select aa.mfr_color_name, 
  ls_fwd_90, ls_fwd_365, ls_fwd_it, ls_fwd_og,
  lt_awd_15_90, lt_awd_15_365, lt_awd_15_it, lt_awd_15_og,
  lt_awd_16_90, lt_awd_16_365, lt_awd_16_it, lt_awd_16_og,
  lt_awd_20_90, lt_awd_20_365, lt_awd_20_it, lt_awd_20_og,
  lt_fwd_15_90, lt_fwd_15_365, lt_fwd_15_it, lt_fwd_15_og,
  prem_awd_15_90, prem_awd_15_365, prem_awd_15_it, prem_awd_15_og,
  prem_awd_20_90, prem_awd_20_365, prem_awd_20_it, prem_awd_20_og
from colors aa
left join (
  select a.mfr_color_name,
    sum(sales) filter (where b.trim_level = 'LS' and sales_window = 90) as ls_fwd_90,
    sum(sales) filter (where b.trim_level = 'LS' and sales_window = 365) as ls_fwd_365,
    sum(sales) filter (where b.trim_level = 'LT' and b.drive = 'AWD' and b.engine = '1.5' and sales_window = 90) as lt_awd_15_90,
    sum(sales) filter (where b.trim_level = 'LT' and b.drive = 'AWD' and b.engine = '1.5' and sales_window = 365) as lt_awd_15_365,
    sum(sales) filter (where b.trim_level = 'LT' and b.drive = 'AWD' and b.engine = '1.6' and sales_window = 90) as lt_awd_16_90,
    sum(sales) filter (where b.trim_level = 'LT' and b.drive = 'AWD' and b.engine = '1.6' and sales_window = 365) as lt_awd_16_365,
    sum(sales) filter (where b.trim_level = 'LT' and b.drive = 'AWD' and b.engine = '2.0' and sales_window = 90) as lt_awd_20_90,
    sum(sales) filter (where b.trim_level = 'LT' and b.drive = 'AWD' and b.engine = '2.0' and sales_window = 365) as lt_awd_20_365,
    sum(sales) filter (where b.trim_level = 'LT' and b.drive = 'FWD' and b.engine = '1.5' and sales_window = 90) as lt_fwd_15_90,
    sum(sales) filter (where b.trim_level = 'LT' and b.drive = 'FWD' and b.engine = '1.5' and sales_window = 365) as lt_fwd_15_365,
    sum(sales) filter (where b.trim_level = 'PREM' and b.drive = 'AWD' and b.engine = '1.5' and sales_window = 90) as prem_awd_15_90,
    sum(sales) filter (where b.trim_level = 'PREM' and b.drive = 'AWD' and b.engine = '1.5' and sales_window = 365) as prem_awd_15_365,
    sum(sales) filter (where b.trim_level = 'PREM' and b.drive = 'AWD' and b.engine = '2.0' and sales_window = 90) as prem_awd_20_90,
    sum(sales) filter (where b.trim_level = 'PREM' and b.drive = 'AWD' and b.engine = '2.0' and sales_window = 365) as prem_awd_20_365
  -- select *  
  from colors a
  left join sales b on a.mfr_color_name = b.color
  group by a.mfr_color_name) bb on aa.mfr_color_name = bb.mfr_color_name
left join (  
  select a.mfr_color_name,
    sum(the_count) filter (where b.trim_level = 'LS' and status = 'in_transit') as ls_fwd_it,
    sum(the_count) filter (where b.trim_level = 'LS' and status = 'on_ground') as ls_fwd_og,
    sum(the_count) filter (where b.trim_level = 'LT' and b.drive = 'AWD' and b.engine = '1.5' and status = 'in_transit') as lt_awd_15_it,
    sum(the_count) filter (where b.trim_level = 'LT' and b.drive = 'AWD' and b.engine = '1.5' and status = 'on_ground') as lt_awd_15_og,
    sum(the_count) filter (where b.trim_level = 'LT' and b.drive = 'AWD' and b.engine = '1.6' and status = 'in_transit') as lt_awd_16_it,
    sum(the_count) filter (where b.trim_level = 'LT' and b.drive = 'AWD' and b.engine = '1.6' and status = 'on_ground') as lt_awd_16_og, 
    sum(the_count) filter (where b.trim_level = 'LT' and b.drive = 'AWD' and b.engine = '2.0' and status = 'in_transit') as lt_awd_20_it,
    sum(the_count) filter (where b.trim_level = 'LT' and b.drive = 'AWD' and b.engine = '2.0' and status = 'on_ground') as lt_awd_20_og,  
    sum(the_count) filter (where b.trim_level = 'LT' and b.drive = 'FWD' and b.engine = '1.5' and status = 'in_transit') as lt_fwd_15_it,
    sum(the_count) filter (where b.trim_level = 'LT' and b.drive = 'FWD' and b.engine = '1.5' and status = 'on_ground') as lt_fwd_15_og,
    sum(the_count) filter (where b.trim_level = 'PREM' and b.drive = 'AWD' and b.engine = '1.5' and status = 'in_transit') as prem_awd_15_it,
    sum(the_count) filter (where b.trim_level = 'PREM' and b.drive = 'AWD' and b.engine = '1.5' and status = 'on_ground') as prem_awd_15_og,
    sum(the_count) filter (where b.trim_level = 'PREM' and b.drive = 'AWD' and b.engine = '2.0' and status = 'in_transit') as prem_awd_20_it,
    sum(the_count) filter (where b.trim_level = 'PREM' and b.drive = 'AWD' and b.engine = '2.0' and status = 'on_ground') as prem_awd_20_og
  -- select *  
  from colors a
  left join inv_2 b on a.mfr_color_name = b.color
  group by a.mfr_color_name) cc on aa.mfr_color_name = cc.mfr_color_name
order by aa.mfr_color_name



-- distribution of drive sales by month
select c.month_name,
  count(*) filter (where drive = 'AWD') as awd,
  count(*) filter (where drive = 'FWD') as fwd
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
  and b.model = 'equinox'
--   and b.drive = 'FWD'
join dds.dim_date c on a.delivery_date = c.the_date
group by month_name, month_of_year
order by month_of_year



select * 
from nc.vehicle_acquisitions a
join nc.vehicles b on a.vin = b.vin
  and b.model = 'equinox'
where a.thru_date > current_date  


select *
from gmgl.vehicle_orders a
join gmgl.vehicle_order_events b on a.order_number = b.order_number
  and b.thru_date > current_date
where alloc_group = 'equinx'
order by event_code


---------------------------------------------------------------------------------------------------
-- 11/30/18  inventory now becomes a combination of acquisitions/accounting and orders

-- all nc inventory stocknumbers from accounting
drop table if exists inv;
create temp table inv as
select a.control-- , sum(a.amount) as amount
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_date between current_Date - 900 and current_date--'06/22/2018'
inner join fin.dim_account c on a.account_key = c.account_key 
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and c.account in ( -- all new vehicle inventory account
    select b.account
    from fin.dim_account b 
    where b.account_type = 'asset'
      and b.department_code = 'nc'
      and b.typical_balance = 'debit'
      and b.current_row
      and b.account <> '126104')   
--   and b.the_date between current_Date - 600 and current_date -- this has no effect
group by a.control
having sum(a.amount) > 10000;

-- vin, color_code from inpmast
drop table if exists inv_1;
create temp table inv_1 as
select a.control as stock_number, b.inpmast_vin as vin, b.color_code
from inv a
join arkona.xfm_inpmast b on a.control = b.inpmast_stock_number
  and b.current_row
  and b.model = 'equinox';

-- on_ground = in nc.vehicles, in_transit: not in nc_vehicles
-- this goes to the way i have been processing silverados and euqinoxen, only
-- creating rows for vehicles on the ground, ie, keyper or ro
drop table if exists inv_2;
create temp table inv_2 as
select trim_level, drive, engine, color, status, count(*) as the_count
from (
  select *
  from  (
    select b.trim_level, b.drive, engine, color, 'on_ground' as status
    from inv_1 a
    join nc.vehicles b on a.vin = b.vin  
    union
    select 
      case 
        when c.style like '%4dr LT%' then 'LT'
        when c.style like '%4dr LS%' then 'LS'
        when c.style like '%4dr L%' then 'L'
        when c.style like '%Premier%' then 'PREM'
        else 'XXXXXX'
      end as trim_level,
      case drive_train
        when 'All Wheel Drive' then 'AWD'
        when 'Front Wheel Drive' then 'FWD'
        else 'XXX'
      end as drive_train,  
      c.engine_displacement, mfr_color_name, 'in_transit'
    from (
      select a.*, c.*,
          jsonb_array_elements(b.response->'style')->'attributes'->>'id' as chr_style_id,
          jsonb_array_elements(b.response->'style')->'attributes'->>'mfrModelCode' as chr_model_code,
          jsonb_array_elements(b.response->'style')->'attributes'->>'modelYear' as chr_model_year,
          jsonb_array_elements(b.response->'style')->'attributes'->>'name' as style,
          jsonb_array_elements(jsonb_array_elements(response->'engine')->'displacement'->'value')->>'$value' as engine_displacement,
          jsonb_array_elements(b.response->'style')->'attributes'->>'drivetrain' as drive_train
      from (
        select *
        from inv_1 a
        where not exists (
          select 1
          from nc.vehicles
          where vin = a.vin)) a  
      left join chr.describe_vehicle b on a.vin = b.vin
      left join colors c on a.color_code = c.mfr_color_code) c) x) z
group by trim_level, drive, engine, color, status  



maybe where i want to get to , for drilling in puposes is a daily table with one row
per vehicle with the sale/inventory status on that day.

select * from sales