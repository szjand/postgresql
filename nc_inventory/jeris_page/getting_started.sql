﻿------------------------------------------------------------------------------------------
--< Beg Inventory: all vehicles in accounting
------------------------------------------------------------------------------------------
-- query from all_nc_inventory_nightly
drop table if exists acct_inventory;
create temp table acct_inventory as
select f.the_date, f.control, 
  g.inpmast_vin, g.year, g.make, g.model, g.model_code, 
  g.color_code, g.color,
  amount, account,
  store_code
from ( -- accounting journal
  select a.control, max(b.the_date) as the_date, sum(a.amount) as amount, 
    min(c.account) as account, min(store_code) as store_code
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account in ( -- all new vehicle inventory account
      select b.account
      from fin.dim_account b 
      where b.account_type = 'asset'
        and b.department_code = 'nc'
        and b.typical_balance = 'debit'
        and b.current_row
        and b.account <> '126104')   
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where a.post_status = 'Y'
  group by a.control
  having sum(a.amount) > 10000) f
left join arkona.xfm_inpmast g on f.control = g.inpmast_stock_number
  and g.current_row = true;

select * from acct_inventory

-- count/amount
/************************************************************************************
H12619 in accounting, not in inpmast
************************************************************************************/

select store_code, count(*) , sum(amount)
from acct_inventory
group by store_code;

select store_code, count(*), sum(amount)::integer
from ( -- accounting journal
  select a.control, max(b.the_date) as the_date, sum(a.amount) as amount, min(c.account) as account, min(store_code) as store_code
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
--     and b.the_date < '08/01/2019'
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account in ( -- all new vehicle inventory account
      select b.account
      from fin.dim_account b 
      where b.account_type = 'asset'
        and b.department_code = 'nc'
        and b.typical_balance = 'debit'
        and b.current_row
        and b.account <> '126104')   
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where a.post_status = 'Y'
  group by a.control
  having sum(a.amount) > 10000) f
group by store_code;

------------------------------------------------------------------------------------------
--/> Beg Inventory: all vehicles in accounting
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--< Beg In System: ordered, not in accounting (committed)
------------------------------------------------------------------------------------------
-- from all_nc_inventory_nightly
select * 
from nc.open_orders a
left join acct_inventory b on a.vin = b.inpmast_vin
where b.inpmast_vin is null

dont know what $$ will be based on
------------------------------------------------------------------------------------------
--/> Beg In System: ordered, not in accounting (committed)
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--< Beg Grounded: including R units
------------------------------------------------------------------------------------------
select a.*, b.inpmast_vehicle_cost
from nc.vehicle_acquisitions a
left join arkona.xfm_inpmast b on a.stock_number = b.inpmast_stock_number
  and b.current_row
where a.thru_date > current_Date
  and a.stock_number not like 'H%'


select sum(b.inpmast_vehicle_cost)::integer
from nc.vehicle_acquisitions a
left join arkona.xfm_inpmast b on a.stock_number = b.inpmast_stock_number
  and b.current_row
where a.thru_date > current_Date
  and a.stock_number not like 'H%'

------------------------------------------------------------------------------------------
--/> Beg Grounded: including R units
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--< Invoicing / Arriving -- need a dynamic timeline
------------------------------------------------------------------------------------------

select order_number, event_code, vin, order_type, alloc_Group, 
  model_year::integer, model_code, 
  dan, peg, color as color_code, category 
from ( -- orders where current_event < 5000, type = TRE, category <> Placed
  select b.vin,
    b.order_number, b.order_type, b.alloc_group, b.model_year, 
    b.model_code, b.dan, b.peg, b.color, 
    c.event_code, c.from_Date, c.thru_date, d.*
  from gmgl.vehicle_orders b
  left join gmgl.vehicle_order_events c on b.order_number = c.order_number
    and c.thru_date = (
      select max(thru_date)
      from gmgl.vehicle_order_events
      where order_number = c.order_number)
  left join gmgl.vehicle_event_codes d on c.event_code = d.code
  where left(d.code, 1)::integer < 5
    and order_type = 'TRE'
--     and category <> 'Placed'
    and b.order_number not in ('VXXMJ1','VXXMJ2','VXXMJZ','VZHB4R','VZHBJ8','VZSRMK','VZSRMM','WBVX79','WCGN2T')) aa
order by event_code


-- 820 of 873 have order info
select a.stock_number, a.vin, a.ground_date, a.delivery_date, aa.source, b.*
from nc.vehicle_sales a
join nc.vehicle_acquisitions aa on a.stock_number = aa.stock_number
  and a.ground_date = aa.ground_date
join gmgl.vehicle_orders b on a.vin = b.vin
where extract(year from a.delivery_date) = 2019
  and a.sale_type = 'retail'
  and a.stock_number not like 'H%'

-- fuck, only one order event for this vehicle
select * 
from gmgl.vehicle_orders a
join gmgl.vehicle_order_events b on a.order_number = b.order_number
where a.vin = '3GNAXSEV5KS672607'


select order_number, count(*)
from gmgl.vehicle_order_events
group by order_number



select a.vin, count(*)
from nc.vehicle_sales a
join gmgl.vehicle_orders b on a.vin = b.vin
join gmgl.vehicle_order_events c on b.order_number = c.order_number
where extract(year from a.delivery_date) = 2019
  and a.sale_type = 'retail'
  and a.stock_number not like 'H%'
group by a.vin  



select * 
from gmgl.vehicle_orders a
join gmgl.vehicle_order_events b on a.order_number = b.order_number
join gmgl.vehicle_event_codes c on b.event_code = c.code
where a.vin = '1GYKNDRS7KZ190460'

drop table if exists order_timeline;
create temp table order_timeline as
select a.order_number, a.vin, a.model_year, a.alloc_Group, a.model_code,
--   min(b.from_date) filter (where left(b.event_code, 1) = '2') as placed,
  min(b.from_date) filter (where left(b.event_code, 1) = '3') as in_system,
  min(b.from_date) filter (where left(b.event_code, 1) = '4') as invoiced,
  min(b.from_date) filter (where c.category = 'In Transit') as in_transit
from gmgl.vehicle_orders a
join gmgl.vehicle_order_events b on a.order_number = b.order_number
join gmgl.vehicle_event_codes c on b.event_code = c.code
-- where a.vin = '1GYKNDRS7KZ190460'
group by a.order_number, a.vin, a.model_year, a.alloc_Group, a.model_code;

-- sold vehicles order timeline
select a.stock_number, a.vin, a.delivery_date, aa.source, b.*, a.ground_date
from nc.vehicle_sales a
join nc.vehicle_acquisitions aa on a.stock_number = aa.stock_number
  and a.ground_date = aa.ground_date
left join order_timeline b on a.vin = b.vin
where extract(year from a.delivery_date) = 2019
  and a.sale_type = 'retail'
  and a.stock_number not like 'H%'


select * 
from acct_inventory a
left join order_timeline b on a.inpmast_vin = b.vin
where a.store_code = 'RY1'


select a.*
from order_timeline a
where a.placed is not null
  and a.in_transit is not null

    
------------------------------------------------------------------------------------------
--/> Invoicing / Arriving -- need a dynamic timeline
------------------------------------------------------------------------------------------


------------------------------------------------------------------------------------------
--< Retailing -- by week within month
------------------------------------------------------------------------------------------

select *
from nc.vehicle_sales
where sale_type = 'retail'
order by delivery_date

select extract(year from delivery_date), count(*)
from nc.vehicle_sales
where sale_type = 'retail'
group by extract(year from delivery_date)


select 
  count(*) filter (where extract(day from delivery_date) between 1 and 7) as "1 - 7",
  count(*) filter (where extract(day from delivery_date) between 8 and 14) as "8 - 14",
  count(*) filter (where extract(day from delivery_date) between 15 and 21) as "15 - 21",
  count(*) filter (where extract(day from delivery_date) > 21) as "22 - END",
  count(*) as total
from nc.vehicle_sales
where sale_type = 'retail'
  and extract(year from delivery_date) > 2017

-- the factors
select 
  round(1.0 * count(*) filter (where extract(day from delivery_date) between 1 and 7)/count(*), 3)  as "1 - 7",
  round(1.0 * count(*) filter (where extract(day from delivery_date) between 8 and 14)/count(*), 3) as "8 - 14",
  round(1.0 * count(*) filter (where extract(day from delivery_date) between 15 and 21)/count(*), 3) as "15 - 21",
  round(1.0 * count(*) filter (where extract(day from delivery_date) > 21)/count(*), 3) as "22 - END",
  count(*) as total
from nc.vehicle_sales
where sale_type = 'retail'
  and stock_number not like 'H%'
  and extract(year from delivery_date) > 2017


do
$$
declare forecast integer := ( 
  select sum(forecast) 
  from nc.forecast
  where year_month = 201908
    and current_row);
begin    
drop table if exists wtf;
create temp table wtf as
 -- the factors
select 
  forecast * round(1.0 * count(*) filter (where extract(day from delivery_date) between 1 and 7)/count(*), 3)  as "1 - 7",
  forecast * round(1.0 * count(*) filter (where extract(day from delivery_date) between 8 and 14)/count(*), 3) as "8 - 14",
  forecast * round(1.0 * count(*) filter (where extract(day from delivery_date) between 15 and 21)/count(*), 3) as "15 - 21",
  forecast * round(1.0 * count(*) filter (where extract(day from delivery_date) > 21)/count(*), 3) as "22 - END",
  count(*) as total
from nc.vehicle_sales
where sale_type = 'retail'
  and stock_number not like 'H%'
  and extract(year from delivery_date) > 2017;
end
$$;
select * from wtf;
  
------------------------------------------------------------------------------------------
--/> Retailing -- by week within month
------------------------------------------------------------------------------------------