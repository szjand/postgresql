﻿select *
from nc.open_orders


select a.alloc_group, b.model_year, b.make, b.model, b.model_code, b.cab, b.drive, count(*)
from gmgl.vehicle_orders a
join nc.vehicles b on a.vin = b.vin
where b.model like 'silverado 1%'
group by a.alloc_group, b.model_year, b.make, b.model, b.model_code, b.cab, b.drive
order by a.alloc_group

select *
from gmgl.vehicle_orders
where alloc_group = 'CCRULD'


select * 
from nc.vehicles where vin = '3GCUKSEC9JG641039'

select a.stock_number, a.vin, a.model_year, a.make, a.model, b.*
from nc.stg_Availability a
left join nc.open_orders b on a.vin = b.vin

/*

-- 2018's are a mess
-- from taylor, possible trims for 2019s: wt, lt, lt trail boss, custom, custome trail boss, 
--  rst, ltz, high country
nc.vehicles generate trim based on chrome style:
2018: LTZ w/1LZ = 1LZ
      LTZ w/2LZ = 2LZ
      Work Truck = WT
      High Country = high country
      LT w/1LT = 1LT
      LT 2/2LT = 2LT
      LS = LS
      custom = custom
2019: Silverado 1500:
        LT Trail Boss = LT Trail Boss
        RST = RST
        LTZ = LTZ
        LT = LT
        High Country = High Country
2019: Silver 1500LD:
        LT = LT

*/
-- compares nc.vehicles info to order info
select a.model_year, a.model, a.model_code, a.drive, a.cab, a.trim_level, a.engine, 
  b.alloc_group, b.vehicle_trim, b.peg, count(*), min(a.vin), max(a.vin)
from nc.vehicles a
join gmgl.vehicle_orders b on a.vin = b.vin
where a.model like 'silverado 1%'
group by a.model_year, a.model, a.model_code, a.drive, a.cab, a.trim_level, a.engine, 
  b.alloc_group, b.vehicle_trim, b.peg


select * from gmgl.
