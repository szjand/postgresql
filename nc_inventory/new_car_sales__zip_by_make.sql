﻿-- 10/19/20 no longer need nc.sales_zip_by_make
alter table nc.sales_zip_by_make
rename to z_unused_sales_zip_by_make;

-- select b.make, d.zip_code, count(*)
-- -- select count(*)
-- from nc.vehicle_sales a
-- join nc.vehicles b on a.vin = b.vin
-- join arkona.ext_bopmast c on a.bopmast_id = c.record_key
--   and c.record_status = 'U'
--   and 
--     case
--       when left(a.stock_number, 1) = 'H' then c.bopmast_company_number = 'RY2'
--       else c.bopmast_company_number = 'RY1'
--     end
-- join arkona.ext_bopname d on c.buyer_number = d.bopname_record_key
--   and d.company_individ is not null
-- where a.delivery_date between current_date - interval '1 year' and current_date
--   and a.sale_type = 'retail'
-- group by b.make, d.zip_code
-- order by count(*) desc
-- 
-- drop table if exists nc.sales_zip_by_make cascade;
-- create table nc.sales_zip_by_make (
--   make citext not null,
--   zip_code citext not null,
--   the_count integer not null,
--   primary key(make,zip_code));
-- 
-- insert into nc.sales_zip_by_make
-- select b.make, d.zip_code, count(*)
-- from nc.vehicle_sales a
-- join nc.vehicles b on a.vin = b.vin
-- join arkona.ext_bopmast c on a.bopmast_id = c.record_key
--   and c.record_status = 'U'
--   and 
--     case
--       when left(a.stock_number, 1) = 'H' then c.bopmast_company_number = 'RY2'
--       else c.bopmast_company_number = 'RY1'
--     end
-- join arkona.ext_bopname d on c.buyer_number = d.bopname_record_key
--   and d.company_individ is not null
-- where a.delivery_date between current_date - interval '1 year' and current_date
--   and a.sale_type = 'retail'
-- group by b.make, d.zip_code; 


-- this needs to be updated nightly, ben would like some past year history as well
-- so, just because, i did a count comparison between nc.vehicle_sales and sls.deals
-- i am unhappy with the diff between nc.vehicle_sales and sls.deals, so lets give financial statement a shot
-- after going throught the fs stuff, vehicle sales is close enough
-- will include history from 01/01/2018 thru current

-- 10/20
keep it simple, generate a table of sales by day 
then be able to query any combination of months, years whatever

drop table if exists nc.sales_zip_by_make_model cascade;
create table nc.sales_zip_by_make_model (
  make citext not null,
  model citext not null,
  zip_code citext not null,
  the_count integer not null,
  primary key(make,model,zip_code));

insert into nc.sales_zip_by_make_model
select b.make, b.model, d.zip_code, count(*)
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
join arkona.ext_bopmast c on a.bopmast_id = c.record_key
  and c.record_status = 'U'
  and 
    case
      when left(a.stock_number, 1) = 'H' then c.bopmast_company_number = 'RY2'
      else c.bopmast_company_number = 'RY1'
    end
join arkona.ext_bopname d on c.buyer_number = d.bopname_record_key
  and d.company_individ is not null
where a.delivery_date between current_date - 365 and current_date
  and a.sale_type = 'retail'
group by b.make, b.model, d.zip_code; 
comment on table nc.sales_zip_by_make_model is 'count of sales by make, model, zipcode for the past 365 days';

drop table if exists nc.sales_zip_by_make_model_by_day cascade;
create table nc.sales_zip_by_make_model_by_day (
  the_date date not null,
  make citext not null,
  model citext not null,
  zip_code citext not null,
  the_count integer not null,
  primary key(the_date,make,model,zip_code));
comment on table nc.sales_zip_by_make_model_by_day is 'count of sales by date, make, model & zipcode since 01/01/2018';

insert into nc.sales_zip_by_make_model_by_day
select a.delivery_date, b.make, b.model, d.zip_code, count(*)
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
join arkona.ext_bopmast c on a.bopmast_id = c.record_key
  and c.record_status = 'U'
  and 
    case
      when left(a.stock_number, 1) = 'H' then c.bopmast_company_number = 'RY2'
      else c.bopmast_company_number = 'RY1'
    end
join arkona.ext_bopname d on c.buyer_number = d.bopname_record_key
  and d.company_individ is not null
where a.delivery_date between '01/01/2018' and current_date
  and a.sale_type = 'retail'
group by a.delivery_date, b.make, b.model, d.zip_code;  


create or replace function nc.update_sales_by_zip()
returns void as
$BODY$
/*
select nc.update_sales_by_zip();
*/

truncate nc.sales_zip_by_make_model;

insert into nc.sales_zip_by_make_model
select b.make, b.model, d.zip_code, count(*)
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
join arkona.ext_bopmast c on a.bopmast_id = c.record_key
  and c.record_status = 'U'
  and 
    case
      when left(a.stock_number, 1) = 'H' then c.bopmast_company_number = 'RY2'
      else c.bopmast_company_number = 'RY1'
    end
join arkona.ext_bopname d on c.buyer_number = d.bopname_record_key
  and d.company_individ is not null
where a.delivery_date between current_date - 365 and current_date
  and a.sale_type = 'retail'
group by b.make, b.model, d.zip_code; 

insert into nc.sales_zip_by_make_model_by_day
select a.delivery_date, b.make, b.model, d.zip_code, count(*)
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
join arkona.ext_bopmast c on a.bopmast_id = c.record_key
  and c.record_status = 'U'
  and 
    case
      when left(a.stock_number, 1) = 'H' then c.bopmast_company_number = 'RY2'
      else c.bopmast_company_number = 'RY1'
    end
join arkona.ext_bopname d on c.buyer_number = d.bopname_record_key
  and d.company_individ is not null
where a.delivery_date between current_date - 33 and current_date
  and a.sale_type = 'retail'
group by a.delivery_date, b.make, b.model, d.zip_code
on conflict (the_date,make,model,zip_code)
  do update
    set (the_count) = (excluded.the_count);

$BODY$
language sql;
comment on function nc.update_sales_by_zip() is 'called nightly by luigi.misc.py class UpdateNcSalesByZip()';