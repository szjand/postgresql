﻿/*
07/07/21
the new car crayon report shows 183 inbound chevys, global connect shows 107
wydat
the page is populated by nc.get_Crayon_report()
*/



select * from (
    select make, case when cab = 'n/a' then concat(model, ' ', drive) else concat(b.model, ' ',initcap(cab), ' ', 'Cab', ' ', drive) end as model,a.model_year, trim_level, color, a.vin, order_number, engine, drive
    from nc.open_orders a
    inner join nc.vehicle_configurations b on a.configuration_id = b.configuration_id
    where order_type = 'TRE'
  union 
    select b.make, case when cab = 'n/a' then concat(b.model, ' ', drive) else concat(b.model, ' ',initcap(cab), ' ', 'Cab', ' ', drive) end as model, b.model_year, b.trim_level, null, a.vin, order_number, null, drive
    from hn.nna_vehicle_details a
    left join jon.configurations b on a.model_code = b.model_code
    where not exists (
      select 1
      from nc.vehicles
      where vin = a.vin)
--   union 
--     select b.make, case when cab = 'n/a' then concat(model, ' ', drive) else concat(model, ' ',initcap(cab), ' ', 'Cab', ' ', drive) end as model,b.model_year, b.trim_level, null, a.vin, order_reference_type,  null, drive
--     from hn.hin_vehicle_statuses a
--     left join jon.configurations b on a.model_code = b.model_code
--     where not exists (
--       select 1
--       from nc.vehicles
--       where vin = a.vin)
--      and b.make is not null
) x where make = 'chevrolet'     


select aa.*,cc.* 
from (
	select make, case when cab = 'n/a' then concat(model, ' ', drive) else concat(b.model, ' ',initcap(cab), ' ', 'Cab', ' ', drive) end as model,a.model_year, trim_level, color, a.vin, order_number, engine, drive
	from nc.open_orders a
	inner join nc.vehicle_configurations b on a.configuration_id = b.configuration_id
	where order_type = 'TRE'
	and make = 'chevrolet') aa
left join gmgl.vehicle_orders bb on aa.order_number = bb.order_number  
left join (
	select *
	from (
	select order_number, event_code, from_date, row_number() over (partition by order_number order by from_date desc) seq
	from gmgl.vehicle_order_events) c
	where seq = 1) cc on aa.order_number = cc.order_number
order by aa.order_number

select *
from (
select order_number, event_code, from_date, row_number() over (partition by order_number order by from_date desc) seq
from gmgl.vehicle_order_events) c
where seq = 1


select *
from jo.vehicle_order_events
where order_number = 'zgwkqk'
order by event_code


select a.order_
from jo.vehicle_order_events a
where event_code = '0000'

select *
from jo.vehicle_order_events
where order_number like 'ZR%'
order by order_number, event_code

select *, row_number() over (partition by order_number order by event_timestamp) as seq
from jo.vehicle_order_events
-- where active = 'Y'
where order_number like 'Z%'
order by order_number, seq desc

--07/08/21 thinking the order_event of interest is the most current active event code

-- invoice when order has no vin?

select replace(trim(left(split_part(raw_invoice, 'VIN', 2), 21)), ' ','') as vin,
  left(split_part(raw_invoice, 'ORDER NO. ', 2), 6) as order_number
from gmgl.vehicle_invoices 

-- the global connect report
-- "insert into jon.gm_orders values('"&C2&"','"&L2&"','"&AB2&"');"
drop table if exists jon.gm_orders cascade;
create unlogged table jon.gm_orders(
  order_number citext primary key,
  vin citext,
  current_event citext);


1. event code 0000
2. ZRJDQ4 active order does not show in spreadsheet
-- 203 today, but global is 1 less at 106
select * from (
    select make, case when cab = 'n/a' then concat(model, ' ', drive) else concat(b.model, ' ',initcap(cab), ' ', 'Cab', ' ', drive) end as model,a.model_year, trim_level, color, a.vin, order_number, engine, drive
    from nc.open_orders a
    inner join nc.vehicle_configurations b on a.configuration_id = b.configuration_id
    where order_type = 'TRE'
  union 
    select b.make, case when cab = 'n/a' then concat(b.model, ' ', drive) else concat(b.model, ' ',initcap(cab), ' ', 'Cab', ' ', drive) end as model, b.model_year, b.trim_level, null, a.vin, order_number, null, drive
    from hn.nna_vehicle_details a
    left join jon.configurations b on a.model_code = b.model_code
    where not exists (
      select 1
      from nc.vehicles
      where vin = a.vin)
) x 
left join ( -- from invoice: order number & vin
	select replace(trim(left(split_part(raw_invoice, 'VIN', 2), 21)), ' ','') as vin,
		left(split_part(raw_invoice, 'ORDER NO. ', 2), 6) as order_number
	from gmgl.vehicle_invoices ) y on x.order_number = y.order_number
left join ( -- the most recent active event code
	select order_number, event_code, left(event_description, 25), event_timestamp::date
	from (
		select *, row_number() over (partition by order_number order by event_timestamp desc) as seq
		from jo.vehicle_order_events
		where active = 'Y') aa
		where seq = 1) w on x.order_number = w.order_number
full outer join jon.gm_orders z on x.order_number = z.order_number			
where x.make = 'chevrolet'     
order by w.event_code

-- this is the view
-- outer join gm report with nc.open_orders left join most recent active event code

select *
from (
	select * from jon.gm_orders wherre current_event <> '5000') a
full outer join (	
	select make, case when cab = 'n/a' then concat(model, ' ', drive) else concat(b.model, ' ',initcap(cab), ' ', 'Cab', ' ', drive) end as model,a.model_year, trim_level, color, a.vin, order_number, engine, drive
	from nc.open_orders a
	inner join nc.vehicle_configurations b on a.configuration_id = b.configuration_id
	where order_type = 'TRE'	
		 and make = 'chevrolet') b on a.order_number = b.order_number
left join (
	select order_number, event_code, left(event_description, 25), event_timestamp::date
	from (
		select *, row_number() over (partition by order_number order by event_timestamp desc) as seq
		from jo.vehicle_order_events
		where active = 'Y') aa
		where seq = 1) c on coalesce(a.order_number, b.order_number, 'xxx') = c.order_number
where c.event_code = '0000'		
order by c.event_code

select *
from (
	select * from jon.gm_orders) a
full outer join (	
	select make, case when cab = 'n/a' then concat(model, ' ', drive) else concat(b.model, ' ',initcap(cab), ' ', 'Cab', ' ', drive) end as model,a.model_year, trim_level, color, a.vin, order_number as order_number_1, engine, drive
	from nc.open_orders a
	inner join nc.vehicle_configurations b on a.configuration_id = b.configuration_id
	where order_type = 'TRE'	
		 and make = 'chevrolet') b on a.order_number = b.order_number_1
left join (
	select order_number as order_number_2, event_code, left(event_description, 25), event_timestamp::date
	from (
		select *, row_number() over (partition by order_number order by event_timestamp desc) as seq
		from jo.vehicle_order_events
		where active = 'Y') aa
		where seq = 1) c on coalesce(a.order_number, b.order_number_1, 'xxx') = c.order_number_2
order by c.event_code		
order by coalesce(a.order_number, b.order_number_1, c.order_number_2)

drop table if exists order_numbers;
create temp table order_numbers as
select *
from (
	select order_number gm from jon.gm_orders) a
full outer join (	
	select order_number oo
	from nc.open_orders a
	inner join nc.vehicle_configurations b on a.configuration_id = b.configuration_id
	where order_type = 'TRE'	
		 and make = 'chevrolet') b on a.gm = b.oo
left join (
	select order_number oon
	from (
		select *, row_number() over (partition by order_number order by event_timestamp desc) as seq
		from jo.vehicle_order_events
		where active = 'Y') aa
		where seq = 1) c on coalesce(a.gm, b.oo, 'xxx') = c.oon;

drop table if exists all_orders;
create temp table all_orders as
select gm from order_numbers where gm is not null
union
select oo from order_numbers where oo is not null
union 
select oon from order_numbers where oon is not null;


select * 
from all_orders x
left join (
	select *
	from (
			select order_number as gm, vin, current_event from jon.gm_orders) a
		full outer join (	
			select make, case when cab = 'n/a' then concat(model, ' ', drive) else concat(b.model, ' ',initcap(cab), ' ', 'Cab', ' ', drive) end as model,a.model_year, trim_level, color, a.vin, order_number as open_orders, engine, drive
			from nc.open_orders a
			inner join nc.vehicle_configurations b on a.configuration_id = b.configuration_id
			where order_type = 'TRE'	
				 and make = 'chevrolet') b on a.gm = b.open_orders
		left join (
			select order_number as order_events, event_code, left(event_description, 25), event_timestamp::date
			from (
				select *, row_number() over (partition by order_number order by event_timestamp desc) as seq
				from jo.vehicle_order_events
				where active = 'Y') aa
				where seq = 1) c on coalesce(a.gm, b.open_orders, 'xxx') = c.order_events
	where coalesce(c.event_code, 'xxx') <> '0000') y on x.gm = coalesce(y.gm, y.open_orders, y.order_events) 
order by x.gm				

select * from jo.vehicle_order_events where order_number = 'XFTN2Z'

select * from nc.open_orders where model_year < 2021 order by model_year

XFTN2Z
WWMD2F
ZGGC59
ZGWKQK
WWSVQF
ZFFHK2
WWSVQG
WWMD2G
ZGGC58
WWSVQD
ZFFHK0
WWMD2D

select * from jo.vehicle_order_events where order_number in ('XQWPSS','XWJT14','XZWVQH') order by order_number, event_code

wait for feedback from taylor ZTJH5N
select * from nc.open_orders where order_number in (
select order_number
from (
	select order_number, event_code, left(event_description, 25), event_timestamp::date
	from (
		select *, row_number() over (partition by order_number order by event_timestamp desc) as seq
		from jo.vehicle_order_events
		where active = 'Y') a
	where seq = 1) b
where event_code = '0000')	



select * 
from jo.vehicle_order_events
where left(event_code, 1)::integer > 4
order by event_timestamp desc