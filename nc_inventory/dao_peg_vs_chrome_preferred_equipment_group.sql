﻿select *
from nc.open_orders 

select *
from chr.describe_vehicle
where vin = '1GTU9FEL6KZ229231'

select *
from jon.describe_vehicle
where vin = '1GTU9FEL6KZ229231'

-- this query hilites the problem: dao.peg disagrees with trim
-- thinking it was issues with early decoding of 2019 models
-- or is it an issue with chr.describe_vehicle?
-- or am i just fucking up because of multiple chrome styles
-- anyway, this needs to be a test in nightly
-- thinking i fucked this up not accounting for multiple chrome styles
-- yep exactly
-- well, that's not all, for shure these 1500 ld's are fucked up
select *
from (
  select a.*, c.factory_option ->'attributes'->>'oemCode' as oem_code,
    c.factory_option ->'attributes'->>'chromeCode' as chrome_code
  from nc.open_orders a
--   from nc.vehicles a
  join jon.describe_vehicle b on a.vin = b.vin
  join jsonb_array_elements(b.response->'factoryOption') as c(factory_option) on true
  where a.model_year > 2017
    and c.factory_option->'header'->>'$value' = 'PREFERRED EQUIPMENT GROUP') d
left join gmgl.vehicle_invoices e on d.vin = e.vin  
where d.peg <> d.oem_code
  or d.oem_code <> d.chrome_code

select * from chr.describe_vehicle where vin = '1GNEVHKW0KJ237888'
select * from dao.peg_to_trim where model_year = 2019 and model = 'traverse'

select * from nc.vehicle_configurations where configuration_id = 252

select * from nc.vehicle_configurations where model = 'silverado 1500 ld'

2019 Silverado 1500 LD, traverse, ct6 where dao.peg <> chrome preferred equipment group

select * from gmgl.vehicle_order_options where order_number = 'WMPMN6'

select a.*, b.option_code, c.*
from nc.open_orders a
left join gmgl.vehicle_order_options b on a.order_number = b.order_number
left join gmgl.option_codes c on b.option_code = c.option_code
  and a.model_year = c.model_year::integer
  and a.model_code = c.model_code
where a.order_number = 'WMPMN6'
order by c.description


select mfr_model_num, trim, mfr_package_code, drive_type, style from dao.vin_reference where year = 2019 and model = 'traverse' group by mfr_model_num, trim, mfr_package_code, drive_type, style order by mfr_model_num, trim

select * from dao.veh_trim_styles where year = 2019 and model = 'traverse'

select * from gmgl.vehicle_orders where alloc_group = 'travrs' and model_year = 2019 order by model_code, peg

select * from dao.peg_to_trim where model_year = 2019 and model = 'traverse'

***************************************************************************************************************
looks like dao is fucked up on traverses, it shows mfr_package_code of 2FL
no such thing in chrome
or orders.peg

select * from nc.vehicles where model_year = 2019 and model = 'traverse' order by model_code, trim_level



  select a.*, c.factory_option ->'attributes'->>'oemCode' as oem_code,
    c.factory_option ->'attributes'->>'chromeCode' as chrome_code
  from nc.open_orders a
--   from nc.vehicles a
  join jon.describe_vehicle b on a.vin = b.vin
  join jsonb_array_elements(b.response->'factoryOption') as c(factory_option) on true
  where a.vin = '2GCVKPEC1K1158639'
    and c.factory_option->'header'->>'$value' = 'PREFERRED EQUIPMENT GROUP'

select * from chr.describe_vehicle where vin = '2GCVKPEC1K1158639'    

select * from jon.describe_vehicle where vin = '2GCVKPEC1K1158639'  

select *
from dao.peg_to_trim

select *
from nc.vehicle_configurations


select * from nc.open_orders where vin = '2GCVKPEC1K1158639'  


select *
from nc.vehicles a
join gmgl.vehicle_orders b on a.vin = b.vin


select a.configuration_id, a.vin, a.model_year, a.model_code, a.peg, b.make, b.model, b.trim_level, c.*
from nc.open_orders a
join nc.vehicle_configurations b on a.configuration_id = b.configuration_id
join dao.peg_to_trim c on b.model_year = c.model_year
  and b.make = c.make
  and left(b.model, 6) = left(c.model, 6) -- fucking 1500 LD
  and a.peg = c.peg
order by a.configuration_id


select * from nc.vehicle_configurations where model = 'silverado 1500 ld'


-- ok, 1500 LD's are fucked up, looks like nc.vehicles are ok, but open orders are not
select a.vin, a.trim_level, a.configuration_id, left(raw_invoice, position('GENERAL' in raw_invoice) - 1)
from nc.vehicles a
left join gmgl.vehicle_invoices b on a.vin = b.vin
where model = 'silverado 1500 ld'

select a.*, left(raw_invoice, position('GENERAL' in raw_invoice) - 1)
from nc.open_orders a
left join gmgl.vehicle_invoices b on a.vin = b.vin
where alloc_group = 'CDBLLD'

-- a 1500 LD, been in orders since 10/29/18
-- so, one way of looking at it, something has changed, why has it not updated
select *
from gmgl.vehicle_order_events
where order_number = 'WMPMN6'
order by from_date


this is the query that updates open_orders configuration_id
returns 2 rows for the 1500 LDs
Line 1880 in nightly
  select *
  from (
    select a.vin, a.order_number, (r.style ->'attributes'->>'id')::citext as chr_style_id,
        case 
          when (r.style ->'model'->>'$value'::citext)::citext = 'blazer' then 
            case 
              when a.peg like '%RS%' then 'RS'
              when a.peg like '%LT' then a.peg
              else 'XXX'
            end
          when (r.style ->'attributes'->>'modelYear')::integer = '2019' 
              and (r.style ->'model'->>'$value'::citext)::citext = 'Silverado 1500 LD' then
            case
              when r.style ->'attributes'->>'name'::citext like '%1LT' then '1LT'
              when r.style ->'attributes'->>'name'::citext like '%2LT' then '2LT'
              else 'XXXXXXXXXXXXXXXXXXX'
            end    
          else coalesce(r.style ->'attributes'->>'trim', 'none')    
        end::citext as chr_trim,       
        (coalesce(t.displacement->>'$value', 'n/a'))::citext as engine
    from nc.open_orders a
      left join chr.describe_vehicle c on a.vin = c.vin
      left join jsonb_array_elements(c.response->'style') as r(style) on true
      left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
      left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
        and t.displacement ->'attributes'->>'unit' = 'liters'
    where a.order_number = 'WRBCPP'
    and a.model_code = (r.style ->'attributes'->>'mfrModelCode')::citext) aa
  left join nc.vehicle_configurations bb on aa.chr_style_id = bb.chrome_style_id
    and aa.chr_trim = bb.trim_level

select * 
from chr.describe_Vehicle
where vin = '2GCVKPEC1K1158639'



select * from nc.open_orders where vin = '1GNEVHKW0KJ237888'

select * from nc.vehicle_configurations where chrome_style_id in ('399670','399669','399671')