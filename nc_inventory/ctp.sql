﻿-- this includes company cars, probably shouldn't
select * 
from nc.vehicle_sales a
where sale_type = 'ctp'
  and not exists (
    select 1
    from nc.vehicle_acquisitions
    where stock_number = a.stock_number || 'R')


-- looks like michelle moves the ctps into R inventory in inpmast before she does 
-- accounting, so, make this an inner join to inpmast
-- so, today, this gives me 79 vehicles still in rental
select x.control, y.inpmast_vin as vin, x.date_in_rental
from (
  select min(b.the_date) as date_in_rental, a.control, sum(a.amount)
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
  join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
  where a.post_status = 'Y'
    and c.account = '127703'
  group by a.control
  having sum(amount) > 10000) x
join arkona.xfm_inpmast y on x.control = y.inpmast_stock_number
  and y.current_row

some of these come up without corresponging record in inpmast
G36536 3GCUYDED3KG134139
select * from arkona.xfm_inpmast where inpmast_vin = '3GCUYDED3KG134139' order by inpmast_key

moved in inpmast, but not yet in accounting

select b.the_date, a.control, a.doc, a.ref, a.amount, 
  c.account, c.account_type, c.department, c.account_type, c.description,
  d.journal_code, e.description
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
join fin.dim_account c on a.account_key = c.account_key
  and c.current_row
join fin.dim_journal d on a.journal_key = d.journal_key
join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and c.account = '127703'
  and control = 'G36536' 


-- so lets see how these look compared to nc.inventory
create temp table ctps as
select x.control, y.inpmast_vin as vin, x.date_in_rental
from (
  select min(b.the_date) as date_in_rental, a.control, sum(a.amount)
  from fin.fact_gl a
  join dds.dim_date b on a.date_key = b.date_key
  join fin.dim_account c on a.account_key = c.account_key
    and c.current_row
  join fin.dim_journal d on a.journal_key = d.journal_key
  join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where a.post_status = 'Y'
    and c.account = '127703'
  group by a.control
  having sum(amount) > 10000) x
join arkona.xfm_inpmast y on x.control = y.inpmast_stock_number
  and y.current_row;

-- date_in_rental matches thru_Date
select *
from ctps a
left join nc.vehicle_acquisitions b on a.control = b.stock_number
-- where date_in_rental <> thru_date


-- some stats
select the_date, 
  count(vin) filter (where trans = 'acq') as to_inv,
  count(vin) filter (where trans = 'sale') as to_ctp
from (  
  select 'acq' as trans, vin, stock_number, ground_date as the_date
  from nc.vehicle_acquisitions
  where source = 'ctp'
  union
  select 'sale', vin, stock_number, delivery_date 
  from nc.vehicle_sales
  where sale_type = 'ctp') a
where the_date >= '01/01/2018'  
group by the_date
order by the_date


/*
2/6/90
afton volunteered to do a sku page for ctp's
here is the base query i sent her
excluded parts trucks & drivers trainings that got categorized as ctp
*/
select a.stock_number, a.vin, a.delivery_date as in_date, b.ground_date as out_date
from nc.vehicle_sales a
left join nc.vehicle_acquisitions b on a.vin = b.vin
  and b.source = 'ctp'
where a.sale_type = 'ctp'
  and left(a.stock_number, 1) <> 'H'
  and a.vin not in ('1GCNKNEC1GZ276712','1GCVKREC5HZ177321','1GKS2HKJ1JR100634',
    '1GYS4DKJ1JR158381','1GCVKREC1JZ181792','1GYS4BKJ1JR165464','1HA3GTCG6JN001883',
    '1G1BE5SM3J7103924','3GCUKSER2JG303860','1G1BE5SM4J7103298','1G1BE5SM9J7101949',
    '1G1ZD5STXJF180904','1G1BE5SM1J7125145','1G1ZD5ST3JF156167','3G1BE6SM1JS527366',
    'W04GV8SX0J1071312','1GTV2MEC3JZ147128','1HA3GTCG2KN000408','1GYS4DKJ7LR103338')
  and b.vin is null
order by a.delivery_date  


  