﻿
-- 4/8/19
/*
lets start building it
ref: smartdraw: nc_constraints_build_out_start_up.sdr
*/

drop table if exists nc.ext_dealer_constraints_1 cascade;
create table nc.ext_dealer_constraints_1 (
  f1 citext);
comment on table nc.ext_dealer_constraints_1 is 'raw data parsed from global connect report, dealer_constraints_distribution_report.pdf,
  each row into a single attribute';
  
drop table if exists nc.ext_dealer_constraints_2 cascade;
create table nc.ext_dealer_constraints_2 (
  row_number integer,
  f1 citext);
comment on table nc.ext_dealer_constraints_2 is 'add row number to data from nc.ext_dealer_constraints_1';

drop table if exists nc.ext_dealer_constraints_3 cascade;
CREATE TABLE nc.ext_dealer_constraints_3(
  row_number integer,
  f1 citext,
  f2 citext,
  f3 citext,
  f4 citext,
  f5 citext,
  f6 citext,
  f7 citext,
  f8 citext,
  f9 citext,
  f10 citext,
  f11 citext,
  f12 citext,
  f13 citext,
  f14 citext);
comment on table nc.ext_dealer_constraints_3 is 'data from nc.ext_dealer_constraints_2 parsed 
into row_number & 14 attributes';


-- dealer constraints distribution report
drop table if exists nc.dealer_constraints_distribution_reports;
create table nc.dealer_constraints_distribution_reports (
  bac citext not null,
  dealer_name citext not null,
  report_date date not null,
  current_row boolean not null,
  primary key(bac,report_date));
comment on table nc.dealer_constraints_distribution_reports is 'one row per dealer per report date';  
-- 4/15/19 history not required
alter table nc.dealer_constraints_distribution_reports
drop column current_row;

drop table if exists nc.dealer_constraints_allocation_groups cascade;  
create table nc.dealer_constraints_allocation_groups (
  bac citext not null ,
  report_date date not null,
  division citext not null,
  model_year integer not null,
  alloc_group citext not null,
  allocation_quantity integer not null,
  tpp_start date not null,
  tpp_end date not null,
  foreign key (bac,report_date) references nc.dealer_constraints_distribution_reports(bac,report_date),
  foreign key (model_year,alloc_group) references nc.tmp_allocation_groups(model_year,alloc_group),
  primary key(bac,report_date,model_year,alloc_group));

drop table if exists nc.tmp_allocation_groups;
create table nc.tmp_allocation_groups(
  division citext not null,
  model_year integer not null,
  alloc_group citext not null,
  division_row_number integer not null,
  primary key(model_year,alloc_group));

drop table if exists nc.dealer_constraints;
create table nc.dealer_constraints (
  bac citext not null ,
  report_date date not null,
  model_year integer not null,
  alloc_group citext not null,
  constraint_number citext not null,  
  description citext not null,  
  constraint_volume citext not null,  
  initially_distributed citext not null,  
  final_received citext not null,  
  dealer_placed citext not null,  
  balance_to_go citext not null,  
  dealer_constraint_availability citext not null,  
  number_of_days citext not null,  
  dealer_constraint_sales citext not null,  
  sales_rate citext not null,  
  dealer_ads citext not null,  
  ads_bar citext not null,  
  calculation_type citext not null,
  foreign key(bac,report_date,model_year,alloc_group) references nc.dealer_constraints_allocation_groups(bac,report_date,model_year,alloc_group),
  primary key(bac,report_date,model_year,alloc_group,constraint_number));

drop table if exists nc.xfm_dealer_constraints_1 cascade;
CREATE TABLE nc.xfm_dealer_constraints_1(
  row_number integer primary key,
  f1 citext,
  f2 citext,
  f3 citext,
  f4 citext,
  f5 citext,
  f6 citext,
  f7 citext,
  f8 citext,
  f9 citext,
  f10 citext,
  f11 citext,
  f12 citext,
  f13 citext,
  f14 citext);
comment on table nc.xfm_dealer_constraints_1 is 'subset of nc.ext_dealer_constraints_3 with only rows needed for alloc_groups & constraints'; 
  

  
