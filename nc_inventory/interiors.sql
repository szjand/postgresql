﻿-- gm only - join on gmgl.vehicle_option_codes
select a.*, c.interiors -> 'attributes' ->> 'oemCode', 
  c.interiors -> 'description' ->> 0,
  left(c.interiors -> 'description' ->> 0, position(',' in c.interiors -> 'description' ->> 0) -1),
  case 
    when c.interiors -> 'description' ->> 0 like '%LEATHER%' then 'Leather'
    else 'Cloth'
  end 
from nc.vehicle_acquisitions a
join chr.describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'factoryOption') as c(interiors) on true
  and c.interiors -> 'header' ->> '$value' in ('SEAT TRIM')
join gmgl.vehicle_option_codes d on a.vin = d.vin
  and c.interiors -> 'attributes' ->> 'oemCode' = d.option_code
where a.thru_date > current_date
order by a.vin

-- honda nissan
drop table if exists hn_interiors;
create temp table hn_interiors as
select a.*, aa.make, c.interiors -> 'attributes' ->> 'oemCode' as oem_code, 
  c.interiors -> 'description' ->> 0 as description,
  left(c.interiors -> 'description' ->> 0, position(',' in c.interiors -> 'description' ->> 0) -1) as color,
  case 
    when c.interiors -> 'description' ->> 0 like '%LEATHER%' then 'Leather'
    when c.interiors -> 'description' ->> 0 like '%LTHR%' then 'Leather'
    when c.interiors -> 'description' ->> 0 like '%ROCK CREEK%' then 'Leather'
    else 'Cloth'
  end as material
from nc.vehicle_acquisitions a
join nc.vehicles aa on a.vin = aa.vin
    and aa.make in ('honda','nissan')
join chr.describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'factoryOption') as c(interiors) on true
  and c.interiors -> 'header' ->> '$value' in ('SEAT TRIM')
where a.thru_date > current_date

select * from hn_interiors where vin = '5N1AZ2MS9KN126232'

select vin from (
select vin, material from hn_interiors group by vin, material ) x group by vin having count(*) > 1

select * from hn_interiors where vin = '5N1DR2MM8KC610886'

-- nissan

select * from hn.nissan_vehicle_details where vin = '5N1AZ2MS9KN126232'

select * from hn.nissan_vehicle_invoice_options where order_number = 'YF40919'

select interior_color, count(*)
from hn.nissan_Vehicle_Details
group by interior_color

