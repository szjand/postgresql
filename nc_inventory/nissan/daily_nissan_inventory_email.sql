﻿select *
from hn.daily_nissan_inventory_email a
where num <> 'SUM' -- excludes total inventory rows, these are also the only rows without a VIN
limit 100


-- this returns non vehicle specific, sparsely populated rows with
-- what looks like total inventory $$
select *
from hn.daily_nissan_inventory_email
where dealer_name <> 'RYDELL NISSAN/GRAND FORKS'
  or dealer_name_two <> 'RYDELL NISSAN/GRAND FORKS'
  or dealer_number <> '3071'

-- on 3/4/19, 56 days worth of data
select count(distinct lookup_date) from hn.daily_nissan_inventory_email

-- 217 vins
select vin, count(*)
from hn.daily_nissan_inventory_email
where num <> 'SUM' 
group by vin
order by count(*)

-- vin with 8 rows
select *
from hn.daily_nissan_inventory_email a
where vin = '1N4BL4BW1KC188351'

-- vin with 80 rows  ?!?!?!
-- 2 rows for each lookup_date with 
-- one row each for trim_level/model_line CVT/KICKS and one for PRO4X AUTO/TITAN
-- not sure what this means
select *
from hn.daily_nissan_inventory_email a
where vin = 'DBS15482716960302'

-- vins with multiple versions
select vin, trim_level, model_line, lookup_date
from hn.daily_nissan_inventory_email a
where vin in ( 'DBS15482716960302','DBS15482716959511','1N4BL4CW0KN307414','1N4BL4CWXKN303550','1N4BL4CVXKC119308')
order by vin, lookup_date

-- so, what does it mean when a vin stops showing in this table
select vin, max(lookup_date) as lookup_date
from hn.daily_nissan_inventory_email a
where num <> 'SUM'
group by vin
order by lookup_date


