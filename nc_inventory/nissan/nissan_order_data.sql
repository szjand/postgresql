﻿--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
3/4/19
last lookup_Date is 2/14/19
not sure what is going on
which of the tables are relevant?



select a.*, b.inpmast_stock_number, b.inpmast_vin, b.status
from hn.nissan_vehicle_details a
left join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
order by a.location_status  

select vin, count(*) from hn.nissan_vehicle_details group by vin having count(*) > 1


select * 
from hn.nissan_vehicle_details
where vin in ('JN1BJ1CR6KW315815','5N1AZ2MS2KN114777','KNMAT2MV9KP529387','KNMAT2MV8KP529395','1N6DD0EV4KN740356','1N6BA1F42JN515900')
order by vin


select order_number, lookup_date from hn.nissan_vehicle_details group by order_number, lookup_date having count(*) > 1

select count(distinct order_number) from hn.nissan_vehicle_details -- 120


-- lookup_date necessary


drop table if exists inventory;
create temp table inventory as
select * 
from hn.nissan_vehicle_details a
where lookup_date = (
  select max(lookup_date)
  from hn.nissan_vehicle_details
  where order_number = a.order_number);


-- model_description & mso_description are not the same
 select * from hn.nissan_vehicle_details where model_description <> mso_description


select a.invoice_number, a.vin, a.model_year, a.stock_number, a.model_code, 
 a.model_line, a.trim_description, a.model_description, a.mso_description, 
 a.exterior_color, a.order_number, a.production_month, a.ship_date, a.vehicle_status,
 a.location_status, a.wholesale_date, b.inpmast_stock_number, b.inpmast_vin, b.status, 
 c.ground_date, 
 d.*
-- select a.* 
from inventory a
left join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
left join (
  select aa.order_number,
    coalesce(cc.creation_date, coalesce(bb.the_date, null)) as ground_date
  from inventory aa
  left join (
    select d.vin, min(b.the_date) as the_Date
    from ads.ext_fact_repair_order a
    inner join dds.dim_date b on a.opendatekey = b.date_key
    inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
    inner join inventory e on d.vin = e.vin
    group by d.vin) bb on aa.vin = bb.vin
  left join ads.keyper_creation_dates cc on aa.stock_number = cc.stock_number) c on a.order_number = c.order_number  
left join nc.vehicles d on a.vin = d.vin  
-- order by a.location_status, a.vin
order by a.stock_number
order by a.vehicle_status


H12100 sold in transit