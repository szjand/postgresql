﻿
do
$$

/*
nc.ext_dealer_constraints_1 is populated by python
select * from nc.ext_dealer_constraints_1
4/15/19
originally, i modeled this to maintain history, eg nc.dealer_constraints_distribution_reports.true
looking at it today, i see no purpose in this
all we need is the current contraint data
*/

begin

truncate nc.ext_dealer_constraints_2;
insert into nc.ext_dealer_constraints_2
select row_number() over (), a.*
from nc.ext_dealer_constraints_1 a;


-- replace commas with semi-colons in fields surrounded by double quotes that
-- contain comma(s) within the beginning and ending double quotes
-- eg line in 150: 02066,"L,LS,LT,AND PREMIER WITH 1.5L ENGINE",2665,6,7,7,0,NA,NA,NA,NA,NA,NA,POA
-- L,LS,LT,AND PREMIER WITH 1.5L ENGINE is all one field
-- enables using split_part to breakout the fields
-- without this, those 'internal' commas are processed as delimiters
-- possible fragility: assumes one field per row with  double quotes surrounding comma(s)
update nc.ext_dealer_constraints_2 z
set f1 = x.new_f1
from (
  select row_number, -- f1, substring(f1, the_first, the_last-the_first+1), 
    replace(f1, substring(f1, the_first, the_last-the_first+1), 
    replace( substring(f1, the_first, the_last-the_first+1), ',', ';'))  as new_f1
  from (
    select row_number, f1 , position('"' in f1) as the_first, -- first double quote
      position('"' in f1) + position('"' in substring(f1, position('"' in f1) + 1, length(f1))) as the_last -- second double quote
    from nc.ext_dealer_constraints_2
    where f1 like '%"%') a
  where position(',' in substring(f1, the_first, the_last-the_first+1)) > 0) x -- only "" that contain a comma) 
where z.row_number = x.row_number;

truncate nc.ext_dealer_constraints_3;
insert into nc.ext_dealer_constraints_3(row_number, f1)
select row_number, split_part(f1, ',', 1)
from nc.ext_dealer_constraints_2;
update nc.ext_dealer_constraints_3 a
set f2 = (
  select split_part(f1, ',', 2)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);
update nc.ext_dealer_constraints_3 a
set f3 = (
  select split_part(f1, ',', 3)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);  
update nc.ext_dealer_constraints_3 a
set f4 = (
  select split_part(f1, ',', 4)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);  
update nc.ext_dealer_constraints_3 a
set f5 = (
  select split_part(f1, ',', 5)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);  
    update nc.ext_dealer_constraints_3 a
set f6 = (
  select split_part(f1, ',', 6)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);  
update nc.ext_dealer_constraints_3 a
set f7 = (
  select split_part(f1, ',', 7)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);  
update nc.ext_dealer_constraints_3 a
set f8 = (
  select split_part(f1, ',', 8)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);  
update nc.ext_dealer_constraints_3 a
set f9 = (
  select split_part(f1, ',', 9)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);  
update nc.ext_dealer_constraints_3 a
set f10 = (
  select split_part(f1, ',', 10)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);  
update nc.ext_dealer_constraints_3 a
set f11 = (
  select split_part(f1, ',', 11)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);  
update nc.ext_dealer_constraints_3 a
set f12 = (
  select split_part(f1, ',', 12)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);  
update nc.ext_dealer_constraints_3 a
set f13 = (
  select split_part(f1, ',', 13)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);  
update nc.ext_dealer_constraints_3 a
set f14 = (
  select split_part(f1, ',', 14)
  from nc.ext_dealer_constraints_2
  where row_number = a.row_number);  
     
-- return commas and delete double quotes
update nc.ext_dealer_constraints_3
set f2 = replace(f2, '"', ''),
    f4 = replace(f4, '"', '')
where f2 like '%"%'
  or f2 like '%;%'
  or f4 like '%;%';
update nc.ext_dealer_constraints_3
set f2 = replace(f2, ';', ','),
    f4 = replace(f4, ';', ',')
where f2 like '%"%'
  or f2 like '%;%'
  or f4 like '%;%';

truncate nc.dealer_constraints_distribution_reports cascade;
insert into nc.dealer_constraints_distribution_reports(bac,dealer_name,report_date)
select 
  (select f2 from nc.ext_dealer_constraints_3 where trim(f1) = 'BAC:'),
  (select f2 from nc.ext_dealer_constraints_3 where trim(f1) = 'Name:'),
  (select f4::date from nc.ext_dealer_constraints_3 where trim(f1) = 'Name:');

truncate nc.xfm_dealer_constraints_1;
insert into  nc.xfm_dealer_constraints_1
select * 
from nc.ext_dealer_constraints_3 
where row_number > 3 
  and f1 not in ('Constraint', 'Number')
order by row_number;

truncate nc.tmp_allocation_groups cascade;
insert into nc.tmp_allocation_groups
select trim(b.the_data[1])::citext as division, b.the_data[2]::integer as model_year,
  trim(b.the_data[3])::citext as alloc_group, division_row_number
from ( -- b: 1 row per alloc group
  select group_no, array_agg(f2 order by division_row_number) as the_data, min(division_row_number) as division_row_number
  from ( -- aa: assigns a group_no to each alloc_group and the row_number of the relevant rows for each alloc group
    select trim(replace(f1, ':', '')), f2, 
      (row_number()OVER(ORDER BY row_number) - 1 ) / 3 + 1 as group_no,
      row_number as division_row_number
    from nc.xfm_dealer_constraints_1 a
    where trim(a.f1) in('Division:','Model Year:','Allocation Group:')
    order by row_number) aa  
  group by group_no) b;  

truncate nc.dealer_constraints_allocation_groups cascade;
insert into nc.dealer_constraints_allocation_groups
select w.bac, w.report_date, v.division, v.model_year, v.alloc_group, 
  v.allocation_quantity::integer, v.tpp_start::date, v.tpp_end::date
from nc.dealer_constraints_distribution_reports w
cross join ( --v:
  select x.division, x.model_year, x.alloc_group, y.allocation_quantity, y.tpp_start, z.tpp_end
  -- select *
  from nc.tmp_allocation_groups x
  left join ( -- y: allocation_quantity & tpp_start
    select a.division, a.model_year, a.alloc_group, 
      c.f4 as allocation_quantity,
      c.f6 as tpp_start
    from nc.tmp_allocation_groups a
    left join ( -- division name with first & last row number for each
      select row_number, f2, coalesce(lead(row_number, 1) over () - 1, (select max(row_number) from nc.xfm_dealer_constraints_1)) as max_row_number
      from (-- updating nc.ext_dealer_contraints_2 changes the default order, but this disambiguates the subset and order
        select * 
        from nc.ext_dealer_constraints_3 
        where row_number > 3 
          and trim(f1) = 'Division:'
        order by row_number) x) b on a.division_row_number = b.row_number
    left join (
      select * 
      from nc.ext_dealer_constraints_3 
      where trim(f1) = 'Model Year:'
      order by row_number)  c on c.row_number between b.row_number and b.max_row_number  
    order by division, alloc_group) y on x.model_year = y.model_year and x.alloc_group = y.alloc_group
  left join ( -- z: division, model_year, alloc_group, tpp_end
    select a.division, a.model_year, trim(a.alloc_group) as alloc_group, 
      c.f6 as tpp_end
    -- select *
    from nc.tmp_allocation_groups a
    left join ( -- division name with first & last row number for each
      select g.row_number, g.f2, coalesce(lead(g.row_number, 1) over () - 1, 
        (select max(row_number) from nc.xfm_dealer_constraints_1)) as max_row_number
      from (-- updating nc.ext_dealer_contraints_2 changes the default order, but this disambiguates the subset and order
        select * 
        from nc.ext_dealer_constraints_3 
        where row_number > 3 
          and trim(f1) = 'Division:'
        order by row_number) g) b on a.division_row_number = b.row_number
    left join (
      select row_number, f6
      from nc.ext_dealer_constraints_3 
      where trim(f1) = 'Allocation Group:'
      order by row_number)  c on c.row_number between b.row_number and b.max_row_number) z on x.model_year = z.model_year 
        and trim(x.alloc_group) = trim(z.alloc_group)) v 
order by v.division, v.alloc_group, v.model_year;

truncate nc.dealer_constraints;
insert into nc.dealer_constraints
select s.bac, s.report_date, s.model_year, s.alloc_group, t.constraint_number, t.description,
  t.constraint_volume, t.initially_distributed, t.final_received, t.dealer_placed,  
  t.balance_to_go, t.dealer_constraint_availability, t.number_of_days,
  t.dealer_constraint_sales, t.sales_rate, t.dealer_ads, t.ads_bar, t.calculation_type
from nc.dealer_constraints_allocation_groups s
left join (
  select a.division, a.model_year, a.alloc_group, c.f1 as constraint_number, c.f2 as description, 
    c.f3 as constraint_volume, c.f4 as initially_distributed, c.f5 as final_received,
    c.f6 as dealer_placed, c.f7 as balance_to_go, c.f8 as dealer_constraint_availability,
    c.f9 as number_of_days, c.f10 as dealer_constraint_sales, c.f11 as sales_rate, 
    c.f12 as dealer_ads, c.f13 as ads_bar, c.f14 as calculation_type
  from nc.tmp_allocation_groups a
  left join (   -- division name with first & last row number for each alloc group
    select row_number, f2, coalesce(lead(row_number, 1) over () - 1, (select max(row_number) from nc.xfm_dealer_constraints_1)) as max_row_number
    from (-- updating nc.ext_dealer_contraints_2 changes the default order, but this disambiguates the subset and order
      select *
      from nc.xfm_dealer_constraints_1
      where trim(f1)  = 'Division:' 
      order by row_number) x) b on a.division_row_number = b.row_number
  left join nc.xfm_dealer_constraints_1 c on c.row_number between b.row_number and b.max_row_number  
    and trim(c.f1) not in('Division:','Model Year:','Allocation Group:', 'Constraint','Number')) t on s.model_year = t.model_year 
     and s.alloc_group = t.alloc_group
order by s.alloc_group, t.constraint_number;


  
end
$$;  

/*
select * from nc.dealer_constraints;

select *
from (
  select c.model_year, c.make, c.model, c.trim_level, c.cab, c.drive, c.box_size, c.engine, c.alloc_group, c.level_header
  from nc.vehicles a
  join nc.vehicle_acquisitions b on a.vin = b.vin 
    and b.thru_date > current_date
  join nc.vehicle_configurations c on a.configuration_id = c.configuration_id  
  group by c.model_year, c.make, c.model, c.trim_level, c.cab, c.drive, c.engine, c.box_size, c.alloc_group, c.level_header) d
join nc.dealer_constraints e on d.model_year = e.model_year
  and trim(d.alloc_group) = trim(e.alloc_group) 
*/
