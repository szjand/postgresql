﻿/*
select *
from nc.exterior_colors
order by make, model, mfr_color_name

select * from nc.vehicles

select chrome_Style_id, model_year, make, model, model_code, drive, cab, trim_level, engine, count(*), min(vin), max(vin)
from nc.vehicles
where model_year in ('2018','2019')
group by chrome_Style_id, model_year, make, model, model_code, drive, cab, trim_level, engine
order by make, model, model_code, model_year, drive, cab, trim_level, engine

select year, body_style, count(*), min(inpmast_vin), max(inpmast_vin) from arkona.xfm_inpmast where current_row and model = 'enclave' group by year, body_style

5GAERCKWXJJ145333

select * from chr.describe_vehicle where vin = '5GAEVBKW0KJ123297'


select model_year, make, model, count(*)
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
  and b.model_year in ('2018','2019')
where sale_type = 'retail'
group by model_year, make, model
order by count(*) desc 

select make, model, count(*), 
  count(*) filter (where model_year = '2018') as "2018",
  count(*) filter (where model_year = '2019') as "2019"
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
  and b.model_year in ('2018','2019')
where sale_type = 'retail'
group by make, model
order by count(*) desc 


select a.stock_number, a.vin, a.delivery_date, b.year, b.make, b.model
from sls.ext_bopmast_partial a
join arkona.xfm_inpmast b on a.stock_number = b.inpmast_stock_number
  and b.current_row
  and b.year > 2017
  and b.model like '%cr-v%'
where a.vehicle_Type = 'N'
order by b.year


-- keep going back to needing something like a config table
-- valid configurations
-- for grouping
-- for determining the arrival of new configs

-- decoding orders when there is no vin
model_code -> trim_level, drive

select * from nc.open_orders where model_year <> 2019 order by alloc_group
(( -- ddl
drop table if exists nc.vehicle_configurations;
create table nc.vehicle_configurations (
  model_year integer not null,
  make citext not null,
  model citext not null, 
  model_code citext not null,
  trim_level citext,
  cab citext not null default 'N/A',
  drive citext,
  engine citext,
  chrome_style_id citext,
  alloc_group citext);

create unique index config_idx on nc.vehicle_configurations(model_year,make,model,model_code,trim_level,cab,engine);

create index vehicle_config_idx on nc.vehicles(model_year,make,model,model_code,trim_level,cab,engine);

create index vehicle_chrome_idx on nc.vehicles(chrome_style_id,engine,trim_level);

alter table nc.vehicles
add foreign key (model_year,make,model,model_code,trim_level,cab,engine)
references nc.vehicle_configurations(model_year,make,model,model_code,trim_level,cab,engine);

alter table nc.vehicles
add foreign key (chrome_style_id,engine,trim_level)
references nc.vehicle_configurations(chrome_style_id,engine,trim_level);


update nc.vehicle_configurations set alloc_group = 'n/a' where alloc_group ='N/A'

select * from gmgl.vehicle_orders where model_code in ('4wt67','cg33903')

alter table nc.vehicle_configurations
add column configuration_id serial primary key;

alter table nc.vehicles
add column configuration_id integer;

update nc.vehicles z
set configuration_id = (
  select configuration_id
  from nc.vehicle_configurations
  where chrome_style_id = z.chrome_style_id
    and coalesce(trim_level, 'none') = coalesce(z.trim_level, 'none')
    and coalesce(engine, 'none') = coalesce(z.engine, 'none'))

create index on nc.vehicles(configuration_id) ;

alter table nc.vehicles
add foreign key (configuration_id)
references nc.vehicle_configurations(configuration_id)
))

-- got multiple rows for config because some vins of a config have an alloc_Group and some don't
insert into nc.vehicle_configurations
select chr_model_year::integer, chr_make, chr_model, chr_model_code,
  chr_trim, chr_cab, chr_drive, engine, chr_style_id, 
  case 
    when chr_make in ('honda', 'nissan') then 'N/A'
    else alloc_group
  end 
from (  
  select 
    (r.style ->'attributes'->>'modelYear')::citext as chr_model_year,
    (r.style ->'division'->>'$value')::citext as chr_make,
    (r.style ->'model'->>'$value'::citext)::citext as chr_model,
    (r.style ->'attributes'->>'mfrModelCode')::citext as chr_model_code,
    case
      when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
      when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
      when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
      when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
      else 'XXX'
    end as chr_drive,
    case
      when u.cab->>'$value' like 'Crew%' then 'crew' 
      when u.cab->>'$value' like 'Extended%' then 'double'  
      when u.cab->>'$value' like 'Regular%' then 'reg'  
      else 'N/A'   
    end as chr_cab,
    r.style ->'attributes'->>'trim' as chr_trim,
    t.displacement->>'$value' as engine,
    (r.style ->'attributes'->>'id')::integer as chr_style_id,
    coalesce(d.alloc_group, 'UNKNOWN') as alloc_group
  from chr.describe_vehicle c
  join jsonb_array_elements(c.response->'style') as r(style) on true
  left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
    and u.ordinality =  2
  left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
  left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
    and t.displacement ->'attributes'->>'unit' = 'liters'  
  left join gmgl.vehicle_orders d on c.vin = d.vin
  where (r.style ->'attributes'->>'modelYear')::citext in ('2018','2019')) x
-- where chr_model = 'equinox'  
group by chr_style_id, chr_model_year, chr_make, chr_model, chr_model_code,
  chr_trim, chr_cab, chr_drive, engine, alloc_group
-- order by chr_model_code
order by chr_make, chr_model, chr_model_code, chr_model_year
-- 


select *
from nc.vehicle_configurations
order by make, model, model_year



select make, model_year, model, model_code, alloc_group
from nc.vehicle_configurations
where make not in ('honda','nissan')
group by make, model_year, model, model_code, alloc_group
order by make, model, model_code, model_year, alloc_group



-- the only model code that applies to mulitple models
-- is CK15753, 2018 Silverado 1500 and 2019 Silverado 1500 LD
select model_code
from (
  select model, model_code
  from nc.vehicle_configurations
  group by model, model_code) a
group by model_code
having count(*) > 1

select * from nc.vehicle_configurations where model_code = 'CK15753'
(
update nc.vehicle_configurations
set alloc_group = 'CRUZE'
where model_code = '1BS69'
  and alloc_group is null;
  
update nc.vehicle_configurations
set alloc_group = 'CAM'
where model_code = '1AL37'
  and alloc_group is null;
  
update nc.vehicle_configurations
set alloc_group = 'ESCESV'
where model = 'Escalade ESV';

update nc.vehicle_configurations
set alloc_group = 'ENCLAV'
where model = 'enclave'
  and alloc_group is null;
  
update nc.vehicle_configurations
set alloc_group = 'ENCLAV'
where model = 'enclave'
  and alloc_group is null;
update nc.vehicle_configurations
set alloc_group = 'ENVISN'
where model = 'Envision'
  and alloc_group is null;
update nc.vehicle_configurations
set alloc_group = 'ENCORE'
where model = 'Encore'
  and alloc_group is null;
update nc.vehicle_configurations
set alloc_group = 'LACROS'
where model = 'LaCrosse'
  and alloc_group is null;      
update nc.vehicle_configurations
set alloc_group = 'REGAL'
where model = 'Regal Sportback'
  and alloc_group is null; 
update nc.vehicle_configurations
set alloc_group = 'RGLWGN'
where model = 'Regal TourX'
  and alloc_group is null; 
update nc.vehicle_configurations
set alloc_group = ''
where model = ''
  and alloc_group is null; 
update nc.vehicle_configurations
set alloc_group = 'ATS'
where model = 'ATS Sedan'
  and alloc_group is null; 
update nc.vehicle_configurations
set alloc_group = 'CT6'
where model = 'CT6 Sedan'
  and alloc_group is null; 
update nc.vehicle_configurations
set alloc_group = 'CTS'
where model = 'CTS Sedan'
  and alloc_group is null; 
update nc.vehicle_configurations
set alloc_group = 'ESCUTL'
where model = 'Escalade'
  and alloc_group is null; 
update nc.vehicle_configurations
set alloc_group = 'ESCESV'
where model = 'ESCUTL'
  and alloc_group is null; 
update nc.vehicle_configurations
set alloc_group = 'XT4'
where model = 'XT4'
  and alloc_group is null; 
update nc.vehicle_configurations
set alloc_group = 'XT5'
where model = 'XT5'
  and alloc_group is null; 
update nc.vehicle_configurations
set alloc_group = 'COLRDO'
where model = 'Colorado'
  and alloc_group is null;                     
update nc.vehicle_configurations
set alloc_group = 'VET'
where model = 'Corvette'
  and alloc_group is null;   
update nc.vehicle_configurations
set alloc_group = 'CRUZE'
where model_code in ('1br69', '1bt69')
  and alloc_group is null;   
update nc.vehicle_configurations
set alloc_group = 'CRZHBK'
where model_code in ('1bt68')
  and alloc_group is null;   
update nc.vehicle_configurations
set alloc_group = 'CAM'
where model_code = 'aAL37'
  and alloc_group is null;   
update nc.vehicle_configurations
set alloc_group = 'EQUINX'
where model = 'Equinox'
  and alloc_group is null;   
update nc.vehicle_configurations
set alloc_group = 'IMPALA'
where model = 'IMPALA'
  and alloc_group is null;   
update nc.vehicle_configurations
set alloc_group = ''
where model = ''
  and alloc_group is null;   
update nc.vehicle_configurations
set alloc_group = 'MALIBU'
where model = 'MALIBU'
  and alloc_group is null;   
update nc.vehicle_configurations
set alloc_group = 'CREGLD'
where model_code in( 'CC15903','CK15903')
  and alloc_group is null;   
update nc.vehicle_configurations
set alloc_group = 'CCRULD'
where model_code in( 'CK15543','CK15743','CK25743')
  and alloc_group is null;   
update nc.vehicle_configurations
set alloc_group = 'CDBLLD'
where model_code in( 'CK15753')
  and alloc_group is null;  
update nc.vehicle_configurations
set alloc_group = 'CCRUHD'
where model_code in( 'CK25743','CK25943','CK35743','CK36403','CK35943')
  and alloc_group is null;  
update nc.vehicle_configurations
set alloc_group = 'CREGHD'
where model_code in( 'CK25903')
  and alloc_group is null;  
update nc.vehicle_configurations
set alloc_group = 'CDBLHD'
where model_code in( 'CK25953','CK25753')
  and alloc_group is null;  
update nc.vehicle_configurations
set alloc_group = 'SUBURB'
where model_code in( 'CK15906')
  and alloc_group is null;          
update nc.vehicle_configurations
set alloc_group = 'TAHOE'
where model_code in( 'CK15706')  
  and alloc_group is null;     
update nc.vehicle_configurations
set alloc_group = 'TRAVRS'
where model = 'Traverse'
  and alloc_group is null;   
update nc.vehicle_configurations
set alloc_group = 'TRAX'
where model = 'TRAX'
  and alloc_group is null;   
update nc.vehicle_configurations
set alloc_group = 'ACADIA'
where model = 'ACADIA'
  and alloc_group is null;   
update nc.vehicle_configurations
set alloc_group = 'CANYON'
where model = 'CANYON'
  and alloc_group is null;   
update nc.vehicle_configurations
set alloc_group = 'GLDCRW'
where model_code = 'TK10543'
  and alloc_group is null;   
update nc.vehicle_configurations
set alloc_group = 'GLDCRW'
where model_code = 'TK10543'
  and alloc_group is null;   
update nc.vehicle_configurations
set alloc_group = 'GCRULD'
where model_code in ('TK15543', 'TK15743')
  and alloc_group is null;   
update nc.vehicle_configurations
set alloc_group = 'GDBLLD'
where model_code = 'TK15753'
  and alloc_group is null;   
update nc.vehicle_configurations
set alloc_group = 'GCRUHD'
where model_code in ('TK25743','TK35743','TK35943','TK25753')
  and alloc_group is null;   
update nc.vehicle_configurations
set alloc_group = 'TERAIN'
where model = 'terrain'
  and alloc_group is null;                                       
update nc.vehicle_configurations
set alloc_group = 'YKN'
where model = 'Yukon'
  and alloc_group is null; 
update nc.vehicle_configurations
set alloc_group = 'YKNXL'
where model = 'Yukon XL'
  and alloc_group is null; 
)


-- so, first thing is to get rid of the dups, at the very least each row should be unique across all fields
drop table if exists configs;
create temp table configs as
select model_year, make, model, model_code, trim_level, cab, drive, engine, chrome_style_id, alloc_group
from nc.vehicle_configurations
group by model_year, make, model, model_code, trim_level, cab, drive, engine, chrome_style_id, alloc_group;

truncate nc.vehicle_configurations;
insert into nc.vehicle_configurations
select * from configs;

(-- find a PK
-- going with chrome id first
--  multiple engines
select *
from nc.vehicle_configurations
where chrome_style_id in (
  select chrome_style_id
  from nc.vehicle_configurations
  group by chrome_Style_id
  having count(*) > 1)
order by chrome_Style_id

-- 3 dups 397456, 397457, 397454
select chrome_style_id, engine
from nc.vehicle_configurations
group by chrome_Style_id, engine
having count(*) > 1

-- the differences are trim_level for 18 accords
select *
from nc.vehicle_configurations
where chrome_style_id in ('397456','397457','397454')
order by chrome_style_id
)
-- valid candidate key
-- by virtue of being the smallest, here is the primary key
select chrome_style_id, engine, trim_level
from nc.vehicle_configurations
group by chrome_Style_id, engine, trim_level
having count(*) > 1

create unique index chrome_idx on nc.vehicle_configurations(chrome_style_id,engine,trim_level);


( -- failed attempt at a better key
select model_year, model_code
from nc.vehicle_configurations
group by model_year, model_code
having count(*) > 1

-- looks like it is engine and/or trim
select *
from nc.vehicle_configurations a
join (
  select model_year, model_code
  from nc.vehicle_configurations
  group by model_year, model_code
  having count(*) > 1) b on a.model_year = b.model_year and a.model_code = b.model_code
order by a.model_year, a.model_code

-- thats not all of them
select model_year, model_code, trim_level, engine
from nc.vehicle_configurations
group by model_year, model_code, trim_level, engine
having count(*) > 1

-- the difference is chrome_style_id
select *
from nc.vehicle_configurations a
join (
  select model_year, model_code, trim_level, engine
  from nc.vehicle_configurations
  group by model_year, model_code, trim_level, engine
  having count(*) > 1) b on a.model_year = b.model_year 
    and a.model_code = b.model_code
    and a.trim_level = b.trim_level
    and a.engine = b.engine
order by a.model_year, a.model_code

-- another candidate key
select model_year, model_code, trim_level, engine, chrome_style_id
from nc.vehicle_configurations
group by model_year, model_code, trim_level, engine, chrome_Style_id
having count(*) > 1
)

-- good, only one config per vehicle
select count(*) from nc.vehicles -- 4640
select *  -- 4640
from nc.vehicles a
left join nc.vehicle_configurations b on a.model_year::integer = b.model_year
  and a.make = b.make
  and a.model = b.model
  and a.model_code = b.model_code
  and a.cab = b.cab
  and a.drive = b.drive
  and a.engine = b.engine
  and a.trim_level = b.trim_level
  and a.chrome_Style_id = b.chrome_style_id::integer


  
select model_year, make, model, trim_level, string_agg(distinct cab || '-' ||  drive || '-' || engine, ',') as config
from nc.vehicle_configurations
where model_year = '2018'
  and model like 'silverado 1%'
group by model_year, make, model, trim_level  


select model_year, make, model, trim_level, array_agg(distinct cab || '_' ||  drive || '_' || engine)
from nc.vehicle_configurations
where model_year = '2018'
--   and model like 'silverado 1%'
group by model_year, make, model, trim_level  


select model_year, make, model, trim_level, cab, drive, engine
from nc.vehicle_configurations
group by model_year, make, model, trim_level, cab, drive, engine
order by model_year, make, model, trim_level, cab, drive, engine


-- ok, this is all looking pretty good
-- need a surrogate key in addition to the natural key
-- and need to update nc.vehicles with that surrogate

so, is it a config table with foreign key to vehicles, where all the attributes are repeated ?
is it a domain on vehicles

-- perfect
-- candidate key consisting of model_year, make, model, model_code, trim_level, cab, drive, engine
select model_year, make, model, model_code, trim_level, cab, drive, engine
from nc.vehicle_configurations
group by model_year, make, model, model_code, trim_level, cab, drive, engine
having count(*) > 1

select model_year, make, model, model_code, trim_level, cab, drive, engine
from nc.vehicle_configurations
group by model_year, make, model, model_code, trim_level, cab, drive, engine
having count(*) > 1



select model_year, count(*)
from nc.vehicles
group by model_year

select *
from nc.vehicle_Acquisitions a
join nc.vehicles b on a.vin = b.vin
where b.model_year < 2018

select *
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
where b.model_year < 2018

insert into nc.vehicle_configurations
select a.model_year, a.make, a.model, a.model_code, a.trim_level, a.cab, a.drive, a.engine, a.chrome_Style_id, 'unknown'
from nc.vehicles a
where a.model_year = 2017
group by  a.model_year, a.make, a.model, a.model_code, a.trim_level, a.cab, a.drive, a.engine, a.chrome_Style_id

insert into nc.vehicle_configurations
select a.model_year, a.make, a.model, a.model_code, a.trim_level, a.cab, a.drive, a.engine, a.chrome_Style_id, 'unknown'
from nc.vehicles a
where a.model_year = 2016
group by  a.model_year, a.make, a.model, a.model_code, a.trim_level, a.cab, a.drive, a.engine, a.chrome_Style_id

delete from nc.vehicle_sales
where vin in (select vin from nc.vehicles where model_year = 2015)

delete from nc.vehicle_acquisitions
where vin in (select vin from nc.vehicles where model_year = 2015)

delete from nc.vehicles where model_year = 2015

(-- why 2 chrome style ids ?
select *
from nc.vehicle_configurations
where model_year = '2019'
  and make = 'chevrolet'
  and model = 'silverado 1500 LD' 
  and model_code = 'ck15753'
  and trim_level = 'LT'
  and cab = 'double'
  and drive = '4wd'
  and engine = '5.3'

-- becausee 391721: LTZ w/1LZ  391722: LTZ w/2LZ
select c.vin, (r.style ->'attributes'->>'id')::integer, response
from chr.describe_Vehicle c
join jsonb_array_elements(c.response->'style') as r(style) on true
  and (r.style ->'attributes'->>'id')::integer in (391721, 391722)


-- same thing 1LT & 2LT
select c.vin, (r.style ->'attributes'->>'id')::integer, response
from chr.describe_Vehicle c
join jsonb_array_elements(c.response->'style') as r(style) on true
  and (r.style ->'attributes'->>'id')::integer in (398577, 398576)

select c.vin, (r.style ->'attributes'->>'id')::integer, response
from chr.describe_Vehicle c
join jsonb_array_elements(c.response->'style') as r(style) on true
  and (r.style ->'attributes'->>'id')::integer in (397488, 397487)
  
-- modify trim level configurations for 18 silverados to comply with diff chrome style ids
-- here's what needs to be changed
select a.model_code, a.chrome_style_id, a.trim_level, b.trim_level
from (
  select *
  from nc.vehicle_configurations
  where model_year = '2018'
    and make = 'chevrolet'
    and model = 'silverado 1500' ) a
left join nc.vehicles b on a.chrome_style_id::integer = b.chrome_style_id
where a.trim_level <> b.trim_level
group by a.model_code, a.chrome_style_id, a.trim_level, b.trim_level
order by b.trim_level, a.chrome_style_id

update nc.vehicle_configurations
set trim_level = 'WT'
where chrome_style_id::integer in (391690, 391708, 391712, 391716);

update nc.vehicle_configurations
set trim_level = '1LT'
where chrome_style_id::integer in (391719,391725,391732,398576);

update nc.vehicle_configurations
set trim_level = '1LZ'
where chrome_style_id::integer in (391721,391727,391734);

update nc.vehicle_configurations
set trim_level = '2LT'
where chrome_style_id::integer in (391720,391726,391733,398577);

update nc.vehicle_configurations
set trim_level = '2LZ'
where chrome_style_id::integer in (391722,391728,391735);
)


-- nc.vehicles with no record in configurations
select *
from nc.vehicles a
left join nc.vehicle_configurations b on a.model_year::integer = b.model_year
  and a.make = b.make
  and a.model = b.model
  and a.model_code = b.model_code
  and coalesce(a.trim_level, 'none') = coalesce(b.trim_level, 'none')
  and a.cab = b.cab
  and a.drive = b.drive
  and coalesce(a.engine, 'none') = coalesce(b.engine, 'none')
where a.model_year in ('2018','2019')
  and b.make is null  


select alloc_group, count(*)
from nc.vehicle_configurations
group by alloc_group

select *
from nc.vehicle_configurations
where alloc_group is null



select *
from nc.vehicles a
join nc.vehicle_configurations b on a.configuration_id = b.configuration_id
order by a.configuration_id

select *
from nc.vehicles
where model_year = 2018
  and model = 'colorado'
order by configuration_id


select a.stock_number, a.vin, a.delivery_date, a.sale_type, b.*
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
where a.delivery_date between current_date - 366 and current_date -1
  and b.model like 'sierra 1%'
order by b.configuration_id, b.color


select sales, model_year, make, model, trim_level, cab, drive, engine
from (
  select b.configuration_id, count(*) as sales
  from nc.vehicle_sales a
  join nc.vehicles b on a.vin = b.vin
    and b.model_year = 2018
  where a.delivery_date between current_date - 366 and current_date -1
  group by b.configuration_id) aa
join nc.vehicle_configurations bb on aa.configuration_id = bb.configuration_id
order by aa.sales desc  

select sales, model_year, make, model, trim_level, cab, drive, engine
from (
  select b.configuration_id, count(*) as sales
  from nc.vehicle_sales a
  join nc.vehicles b on a.vin = b.vin
    and b.model_year = 2019
  where a.delivery_date between current_date - 366 and current_date -1
  group by b.configuration_id) aa
join nc.vehicle_configurations bb on aa.configuration_id = bb.configuration_id
order by aa.sales desc  


-- 365 sales
select a.*
from nc.vehicle_configurations a
join nc.vehicles b on a.configuration_id = b.configuration_id
join nc.vehicle_sales c on b.vin = c.vin
  and c.sale_type = 'retail'
  and c.delivery_date between current_date - 366 and current_date -1
where a.model like 'sierra 1%'

-- 90 sales
select a.*
from nc.vehicle_configurations a
join nc.vehicles b on a.configuration_id = b.configuration_id
join nc.vehicle_sales c on b.vin = c.vin
  and c.sale_type = 'retail'
  and c.delivery_date between current_date - 91 and current_date -1
where a.model like 'sierra 1%'

-- current ground inventory
select a.*
from nc.vehicle_configurations a
join nc.vehicles b on a.configuration_id = b.configuration_id
join nc.vehicle_acquisitions c on b.vin = c.vin
  and thru_date > current_date
where a.model like 'sierra 1%'

-- each alloc_group can be many configurations
select *
from nc.open_orders a
where alloc_group in (
  select alloc_group
  from nc.vehicle_configurations
  where model like 'sierra 1%')

select * from nc.vehicles where vin in ('1GTU9DED4KZ201280','3GTU2NEC0JG581993')  

select * from gmgl.vehicle_orders where order_number = 'WNNFKK'
-- getting closer to order vs config
-- from orders i have model_year, alloc_group, make, model_code, peg
select *
from (
  select a.peg, b.configuration_id
  from gmgl.vehicle_orders a
  join nc.vehicles b on a.vin = b.vin
  where a.peg is not null
    and a.alloc_group in (
      select alloc_group
      from nc.vehicle_configurations
      where model like 'encore')
  group by a.peg, b.configuration_id) aa   
join nc.vehicle_configurations bb on aa.configuration_id = bb.configuration_id  
-- order by aa.peg
-- order by bb.alloc_group
order by bb.model_code



select model_year, make, model_code, model, alloc_group, count(*)
from nc.vehicle_configurations  
where model_year > 2017
  and alloc_group <> 'n/a'
group by model_year, make, model_code, model, alloc_group
order by model_year, make, model_code, alloc_group


can i get peg from chrome based on chrome_style_id

select model_year, vin from nc.vehicles where model like 'sierra 1%'

select *
from jon.describe_vehicle
where vin = '3GTU2NEC0JG581976'


-- this is where i lost work
-- ref chrome 2 end points for how i did colors out of jon.describe_Vehicle
-- trying to extract the PEG 

one of the most frequent and annoying results when i query json incorrectly is getting empty columns

select c.style_id, b.colors->'attributes'->>'colorCode', b.colors->'attributes'->>'colorName', --ordinality, 
--   a.response->'exteriorColor'->2, -- for each row returns the entire "2" object (styleId, attributes, genericColor)
--   b.colors -- returns the complete object for each row
  b.colors#>>'{attributes,colorCode}' -- returns the correct color code for each row, also reveals that ordinality is not required
from jon.describe_vehicle a
left join jsonb_array_elements(a.response->'exteriorColor') as b(colors) on true
left join jsonb_array_elements(b.colors->'styleId') as c(style_id)on true
where a.vin = '3GTU2NEC0JG581976'
-- limit 100   

-- this should be returning 172 rows, but is only returning one
-- !!!!!!!!! i had fat fingered factoryOption as factoryOptions, silently failed !!!!!!!!!!!!!!!!!!!
-- this all works exactly as expected, no magic
select ordinality, factory_option->'header'->>'$value' as header, factory_option->'attributes'->>'oemCode' as oem_code, 
  factory_option->'description'->>0
from jon.describe_vehicle a
left join jsonb_array_elements(a.response->'factoryOption') with ordinality as b(factory_option) on true
-- left join jsonb_array_elements(b.factory_option->'styleId') as c(style_id)on true
where a.vin = '3GTU2NEC0JG581976'

-- it now becomes a matter of trying to match up orders with configurations

i am worried that PEG does not account for engines

select count(*) from nc.vehicles where make not in ('honda','nissan')

-- get just the peg
-- works for GM only
select aa.*, factory_option->'attributes'->>'oemCode' as oem_code, 
  factory_option->'description'->>0 as peg
from nc.vehicles aa  
join jon.describe_vehicle a on aa.vin = a.vin
left join jsonb_array_elements(a.response->'factoryOption') with ordinality as b(factory_option) on true
where b.factory_option->'header'->>'$value' = 'PREFERRED EQUIPMENT GROUP'
  and aa.model_year > 2017
order by make, model, trim_level, model_year


--ok here is my concern:
the only difference between 3GCUKSEC9JG258668 and 3GCUKSEJ9JG459534 is the engine, but the peg is the same
which leads me to believe that from an order without a vin, there is no way to determine the configuration to engine level

select * from nc.vehicles where vin in ('3GCUKSEC9JG258668','3GCUKSEJ9JG459534')

-- 12/27/18 this probably isn't even the correct script to continue with this but, oh well

for orders without a vin, discern the configuration based on the options in gmgl.vehicle_order_options

oh, good, a PK on order & option code

-- ok, here are the identifiable open orders for sierras that do not have a vin yet
select *
from nc.open_orders a
where a.vin is null  
  and a.alloc_group in (
    select distinct alloc_group
    from nc.vehicle_configurations
    where model like 'sierra 1%')

so i guess what i am thinking is, find all records in jon.describe_vehicle that match what
i know about these orders, 

!!!!!!!!!!!!!!!!!!!!!!! ADD CONFIGURATION_ID TO OPEN_ORDERS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!1

-- no luck looking for alloc_group in this response
select * from jon.describe_vehicle where vin = '3GTU2NEC0JG581976'

select * from nc.vehicles where model_code = 'TK10543' and model_year = 2019
select * 
from nc.open_orders a
left join gmgl.vehicle_order_options b on a.order_number = b.order_number
where a.order_number = 'WPSSZC'

-- factory options for 2019 TK10543's
select a.vin, b.style->'attributes'->>'modelYear', b.style ->'attributes'->>'mfrModelCode',
  factory_option->'header'->>'$value' as header, (factory_option->'attributes'->>'oemCode')::citext as oem_code
from jon.describe_vehicle a
join jsonb_array_elements(a.response->'style') as b(style) on true
join jsonb_array_elements(a.response->'factoryOption') with ordinality as c(factory_option) on true
where (b.style->'attributes'->>'modelYear')::integer = 2019
  and (b.style ->'attributes'->>'mfrModelCode')::citext = 'TK10543'

-- engine options for 2019 TK10543's
select a.vin, b.style->'attributes'->>'modelYear', b.style ->'attributes'->>'mfrModelCode',
  factory_option->'header'->>'$value' as header, (factory_option->'attributes'->>'oemCode')::citext as oem_code,
  factory_option->'description'->>0
from jon.describe_vehicle a
join jsonb_array_elements(a.response->'style') as b(style) on true
join jsonb_array_elements(a.response->'factoryOption') with ordinality as c(factory_option) on true
where (b.style->'attributes'->>'modelYear')::integer = 2019
  and (b.style ->'attributes'->>'mfrModelCode')::citext = 'TK10543'
  and factory_option->'header'->>'$value' = 'ENGINE'

  
select *
from (
  select * 
  from nc.open_orders a
  left join gmgl.vehicle_order_options b on a.order_number = b.order_number
  where a.order_number = 'WPSSZC') aa
join (
  select a.vin, b.style->'attributes'->>'modelYear', b.style ->'attributes'->>'mfrModelCode',
    factory_option->'header'->>'$value' as header, (factory_option->'attributes'->>'oemCode')::citext as oem_code,
    factory_option->'description'->>0
  from jon.describe_vehicle a
  join jsonb_array_elements(a.response->'style') as b(style) on true
  join jsonb_array_elements(a.response->'factoryOption') with ordinality as c(factory_option) on true
  where (b.style->'attributes'->>'modelYear')::integer = 2019
    and (b.style ->'attributes'->>'mfrModelCode')::citext = 'TK10543') bb on aa.option_code = bb.oem_code  
where header = 'ENGINE'
    
--12/27/18
this definitely looks promising
today been asked to defer categorizing orders without vins and get the page up with everything except in system
shift to new_inventory_v1.sql

*/
-- 1/15/19 got v1 in vision, excluding orders without vins, time to get back to figuring out configuration of orders without vins




-- 1/19/19
compare peg_to_trim to dataone

drop table if exists peg_to_trim;
create temp table peg_to_trim as
select b.peg, c.model_year, c.make, c.model, c.trim_level, c.configuration_id, c.model_code
from nc.vehicles a
join gmgl.vehicle_orders b on a.vin = b.vin
join nc.vehicle_configurations c on a.configuration_id = c.configuration_id
where b.peg is not null
  and a.model_year > 2018
group by b.peg, c.model_year, c.make, c.model, c.trim_level, c.configuration_id, c.model_code
union
select b.peg, c.model_year, c.make, c.model, c.trim_level, c.configuration_id, c.model_code
from nc.open_orders a
join gmgl.vehicle_orders b on a.vin = b.vin
join nc.vehicle_configurations c on a.configuration_id = c.configuration_id
where b.peg is not null
  and a.model_year > 2018
group by b.peg, c.model_year, c.make, c.model, c.trim_level, c.configuration_id, c.model_code
order by make, model, trim_level;  

----------------------
-- ok what i am going for: peg_to_trim & order_engine all from dataone

-- cool, 109 rows, no dups on order number
drop table if exists order_engine;
create temp table order_engine as
select a.order_number, c.engine
from nc.open_orders a
join gmgl. vehicle_order_options b on a.order_number = b.order_number
  and b.thru_ts > now()
join option_codes_engines c on a.model_code = c.model_code
  and a.model_year = c.model_year::integer
  and b.option_code = c.oem_code
-- where a.order_number = 'WJMCRV'  
where a.vin is null;


drop table if exists option_codes_engines;
create temp table option_codes_engines as
select model_year, make, model, model_code, oem_code, engine
from (
  select a.vin, b.style->'attributes'->>'modelYear' as model_year, 
    a.response->'attributes'->>'bestMakeName' as make, 
    a.response->'attributes'->>'bestModelName' as model,
    b.style ->'attributes'->>'mfrModelCode' as model_code,
  --   factory_option->'header'->>'$value' as header, 
    factory_option->'attributes'->>'oemCode' as oem_code,
    left(factory_option->'description'->> 0, 40),
    position('L' in  left(factory_option->'description'->> 0, 40)),
    substring(left(factory_option->'description'->> 0, 40), position('L' in  left(factory_option->'description'->> 0, 40)) - 3, 3) as engine
  from jon.describe_vehicle a
  join jsonb_array_elements(a.response->'style') as b(style) on true
  join jsonb_array_elements(a.response->'factoryOption') with ordinality as c(factory_option) on true
  where (b.style->'attributes'->>'modelYear')::integer > 2017
    and  factory_option->'header'->>'$value' = 'ENGINE') x
group by  model_year, make, model, model_code, oem_code, engine; 

-- engine codes from chrome match dataone
select * 
from (
  select model_year, make, model, oem_code, engine
  from option_codes_engines a
  group by model_year, make, model, oem_code, engine) a
full outer join (
  select b.year, b.make, b.model, b.trim, a.engine_code, c.ice_displacement
  from dao.lkp_veh_eng a
  join dao.veh_trim_styles b on a.vehicle_id = b.vehicle_id
    and b.year > 2018
    and b.make in ('chevrolet','gmc','buick','cadillac')
  join dao.def_engine c on a.engine_id = c.engine_id  
  where b.fleet = 'N'
  group by b.year, b.make, b.model, b.trim, a.engine_code, c.ice_displacement) b on a.model_year::integer = b.year
    and a.make = b.make
    and a.model = b.model
    and a.oem_code = b.engine_code
where a.model_year = '2019'   
  and a.engine <> b.ice_displacement::citext

select *
from dao.vin_reference
limit 100

select *
from dao.def_oem_option
where oem_option_name like '%engine%'
limit 100


select *
from dao.lkp_veh_oem_option
limit 100

select b.year, b.make, b.model, b.trim, b.mfr_model_num, b.mfr_package_code,  
  a.engine_code, c.ice_displacement
from dao.lkp_veh_eng a
join dao.veh_trim_styles b on a.vehicle_id = b.vehicle_id
  and b.year > 2018
  and b.make in ('chevrolet','gmc','buick','cadillac')
join dao.def_engine c on a.engine_id = c.engine_id  
where b.fleet = 'N'

select * from order_engine

-- getting there
-- able to identify the open orders in dataone
-- now can i convert them to a configuration?
-- need drive
select a.*, b.engine, c.*
from nc.open_orders a
left join order_engine b on a.order_number = b.order_number
left join (
  select b.year, b.make, b.model, b.trim, b.mfr_model_num, b.drive_type, b.mfr_package_code,  
    a.engine_code, c.ice_displacement
  from dao.lkp_veh_eng a
  join dao.veh_trim_styles b on a.vehicle_id = b.vehicle_id
    and b.year > 2018
    and b.make in ('chevrolet','gmc','buick','cadillac')
  join dao.def_engine c on a.engine_id = c.engine_id  
  where b.fleet = 'N') c on a.model_year = c.year
    and a.model_code = c.mfr_model_num
    and a.peg = c.mfr_package_code
    and b.engine = c.ice_displacement::citext
where a.configuration_id is null
  and a.order_type = 'TRE'
order by a.order_number

-- my concern is that as orders change over time, the engine for an order
-- (without a vin) can change, so the configuration_id could also change,
-- how am i tracking that?
-- today 1/21/19, there do not seem to be any discrepancies
select a.*, b.*, c.engine
from nc.open_orders a
left join order_engine b on a.order_number = b.order_number
left join nc.vehicle_configurations c on a.configuration_id = c.configuration_id
where vin is null
  and order_type = 'tre'
  and b.engine <> c.engine


-- need to define the process for determining the configuration of orders without vins

select *
from nc.open_orders
where vin is null
  and order_type = 'tre'

select *
from nc.dao_peg_to_trim

WHAT I KEEP YEARNING FOR IS SOME SORT OF MASTER LIST
FOR EACH MODEL CODE, HOW MANY CONFIGS
FOR EACH MODEL/TRIM HOW MANY CONFIGS
AND THATS NOT REALLY IT
LOOKING FOR A dB TYPE RELATIONSHIP DEFINITION FOR THESE DIFFERENT CONFIGURATION COMPONENTS
WHAT DOES THE MODEL CODE DEFINITIVELY TELL ME ABOUT THE VEHICLE

SELECT model_code, array_agg(configuration_id)
FROM NC.VEHICLE_configurations
where model_year > 2017
group by model_code

select  model_code, model_year, make, model,array_agg(configuration_id), config_type
FROM NC.VEHICLE_configurations
where model_year > 2017
group by model_year, make, model, model_code, config_type
ORDER BY MODEL_CODE, model_year, make, model


so, for each model code, are there multiple model_year, make, model
this might be better in dataone, rather than the spotty sampling from current vehicle_configurations table


select year, make, model, mfr_model_num
from (
  select year, make, model, mfr_model_num, count(*)
  from dao.veh_trim_styles a
  where fleet = 'N'
    and year > 2017
    and make in ('chevrolet','gmc','buick','cadillac','honda','nissan')
  group by year, make, model, mfr_model_num) b
group by year, make, model, mfr_model_num
having count(*) > 1


select *
from (
  select year, make, model, mfr_model_num, count(*)
  from dao.veh_trim_styles a
  where fleet = 'N'
    and year > 2017
    and make in ('chevrolet','gmc','buick','cadillac','honda','nissan')
  group by year, make, model, mfr_model_num) b
where mfr_model_num = '6KL69'

select * from dao.veh_trim_styles where year > 2017 and mfr_model_num = ''

select * from arkona.xfm_inpmast where model = 'passport'

select *
from 

keep dataone data/tables in dao, eg, dao.peg_to_trim

drop table if exists dao.model_codes cascade;
create table dao.model_codes (
  model_year integer not null,
  make citext not null,
  model citext not null, 
  model_code citext not null,
  primary key (model_year,make,model,model_code));
comment on table dao.model_codes is 'subset of dao.veh_trim_styles comprised of rydell gf makes,
  used to identify year/make/model from model_code. Truncate and fill nightly';  
insert into dao.model_codes  
select year, make, model, mfr_model_num
from dao.veh_trim_styles a
where fleet = 'N'
  and year > 2017
  and make in ('chevrolet','gmc','buick','cadillac','honda','nissan')
group by year, make, model, mfr_model_num;

-- fuck i am having trouble focusing on identifying configuration for no vin orders

select * from dao.peg_to_trim



get the engine, then from all that has been derived see if i can get drive from chrome

select b.year, b.make, b.model, b.trim, b.mfr_package_code,  b.drive_type, b.mfr_model_num, 
  a.engine_code, c.ice_displacement
from dao.lkp_veh_eng a
join dao.veh_trim_styles b on a.vehicle_id = b.vehicle_id
  and b.year > 2018
  and b.make in ('chevrolet','gmc','buick','cadillac')
join dao.def_engine c on a.engine_id = c.engine_id  
where b.fleet = 'N'


select * from dao.veh_trim_styles limit 10

-- maybe a single dao table with engine codes, engine size, model codes, & peg to trim

drop table if exists dao_engine_model_code_peg_to_trim;
create temp table dao_engine_model_code_peg_to_trim as
select a.year as model_year, a.make, a.model, a.trim as trim_level, a.mfr_package_code as peg, 
  a.mfr_model_num as model_code, a.drive_type as drive,
  b.engine_code, c.ice_displacement as engine
-- select *
from dao.vin_reference a
join dao.lkp_veh_eng b on a.vehicle_id = b.vehicle_id
join dao.def_engine c on b.engine_id = c.engine_id
where make in ('chevrolet','gmc','buick','cadillac')
  and year = 2019 
group by a.year, a.make, a.model, a.trim, a.mfr_package_code, a.mfr_model_num, a.drive_type,
  b.engine_code, c.ice_displacement

drop table if exists dao_engine_option_codes;
create temp table  dao_engine_option_codes as
select a.year as model_year, a.make, a.model, 
  a.mfr_model_num as model_code,
  b.engine_code, c.ice_displacement as engine
-- select *
from dao.vin_reference a
join dao.lkp_veh_eng b on a.vehicle_id = b.vehicle_id
join dao.def_engine c on b.engine_id = c.engine_id
where make in ('chevrolet','gmc','buick','cadillac')
  and year = 2019 
group by a.year, a.make, a.model, a.mfr_model_num, 
  b.engine_code, c.ice_displacement;

drop table if exists option_codes_engines;
create temp table option_codes_engines as
select model_year, make, model, model_code, oem_code, engine
from (
  select a.vin, b.style->'attributes'->>'modelYear' as model_year, 
    a.response->'attributes'->>'bestMakeName' as make, 
    a.response->'attributes'->>'bestModelName' as model,
    b.style ->'attributes'->>'mfrModelCode' as model_code,
  --   factory_option->'header'->>'$value' as header, 
    factory_option->'attributes'->>'oemCode' as oem_code,
    left(factory_option->'description'->> 0, 40),
    position('L' in  left(factory_option->'description'->> 0, 40)),
    substring(left(factory_option->'description'->> 0, 40), position('L' in  left(factory_option->'description'->> 0, 40)) - 3, 3) as engine
  from jon.describe_vehicle a
  join jsonb_array_elements(a.response->'style') as b(style) on true
  join jsonb_array_elements(a.response->'factoryOption') with ordinality as c(factory_option) on true
  where (b.style->'attributes'->>'modelYear')::integer > 2017
    and  factory_option->'header'->>'$value' = 'ENGINE') x
group by  model_year, make, model, model_code, oem_code, engine; 

select *
from (
  select * 
  from dao_engine_option_codes) a
full outer join (
  select * 
  from option_codes_engines) b on  a.model_year = b.model_year::integer
    and a.model_code = b.model_code
    and a.engine_code = b.oem_code
  
-- still need something like this, integrating gmgl.vehicle_order_options to identify the engine code for the order
drop table if exists order_engine;
create temp table order_engine as
select a.order_number, c.oem_code as engine_code, c.engine
from nc.open_orders a
join gmgl.vehicle_order_options b on a.order_number = b.order_number
  and b.thru_ts > now()
join option_codes_engines c on a.model_code = c.model_code
  and a.model_year = c.model_year::integer
  and b.option_code = c.oem_code
-- where a.order_number = 'WJMCRV'  
where a.vin is null;

-- order engine
select * 
from nc.open_orders a
join gmgl.vehicle_order_options b on a.order_number = b.order_number
  and b.thru_ts > now()
join dao_engine_model_code_peg_to_trim c on a.model_year = c.model_year
  and b.option_code = c.engine_code  
where a.vin is null 


--ok, this "works" gives me the 106 rows
-- table structures and relationships are iffy, need more modular tables it think, at least for clarity sake
-- too much overlap
-- fucking dataone calls AWD equinoxes 4X4
-- i know the engine option code and displacement for an order: 
-- guess the question is sourcing the option_codes_engines from chrome or dao
-- !!! option_codes_engines is only required to generate order_engine
select a.order_number, a.alloc_group, a.peg, a.model_year, 
  b.make, b.model, a.model_code, c.trim_level,
--   left(a.model_code, 2), substring(a.model_code, 2, 1),
--   substring(a.model_code, 5,1), right(a.model_code, 2),
  case
    when left(a.model_code, 2) in ('CC','CK', 'TC','TK') and b.model not in ('suburban','tahoe','yukon','yukon xl') then
      case
        when right(a.model_code, 2) = '03' then 'reg'
        when right(a.model_code, 2) = '53' then 'double'
        when right(a.model_code, 2) = '43' then 'crew'
      end
    else 'n/a'
  end as cab,
  case
    when left(a.model_code, 2) in ('CC','CK', 'TC','TK') THEN -- and b.model not in ('suburban','tahoe') then
      case
        when substring(a.model_code, 2, 1) = 'C' then 'RWD'
        when substring(a.model_code, 2, 1) = 'K' then '4WD'
      end
  end as drive,  
  case
    when left(a.model_code, 2) in ('CC','CK', 'TC','TK') and b.model not in ('suburban','tahoe','yukon','yukon xl') then
      case
        when substring(a.model_code, 5,1) = '5' then 'Short'
        when substring(a.model_code, 5,1) = '7' then 'Standard'
        when substring(a.model_code, 5,1) = '9' then 'Long'
        else 'n/a'
      end
    else 'n/a'
  end as box_size , d.*--, e.*
from nc.open_orders a
join dao.model_codes b on a.model_year = b.model_year
  and a.model_code = b.model_code
join dao.peg_to_trim c on a.model_year = c.model_year
  and b.make = c.make  
  and b.model = c.model
  and a.peg = c.peg
join order_engine d on a.order_number = d.order_number  
-- join dao_engine_model_code_peg_to_trim e on a.model_year = e.model_year
--   and b.make = e.make
--   and b.model = e.model
--   and c.trim_level = e.trim_level
--   and a.model_code = e.model_code
--   and d.engine = e.engine::citext
--   and a.peg = e.peg
--   and d.engine_code = e.engine_code
where a.configuration_id is null
  and a.order_type = 'tre'
order by a.order_number


select * from order_engine

-- model/model_code/drive
-- dao vs vehicle_configurations
select *
from (
  select model, mfr_model_num, drive_type, count(*)
  from dao.veh_trim_styles
  where year > 2017
    and make = 'chevrolet'
  group by model, mfr_model_num, drive_type  
  order by model) a
full outer join (
  select model, model_code, drive, count(*) 
  from nc.vehicle_configurations
  where model_year > 2017
    and make = 'chevrolet'
  group by model, model_code, drive
  order by model, drive ) b on a.model = b.model and a.mfr_model_num = b.model_code 



select *
from dao.veh_trim_styles
where year = 2019
  and model = 'colorado'
order by mfr_model_num


select *
from dao.vin_reference
where year = 2019
  and model = 'colorado'
order by mfr_model_num  


/*
looking at doing all possible model_levels based on dao data
WAY too complex for now, amongst other issues, too many differences 
between chrome and dao

select year, make, model, count(distinct trim) as trim_level, count(distinct body_subtype) as cab,
  count(distinct bed_length) as box, count(distinct drive_type) as drive,
  count(distinct engine_size) as engine, count(distinct style) as name_wo_trim
from dao.vin_reference  
where year > 2017
  and make in ('chevrolet','gmc','buick','cadillac','honda','nissan')
group by year, make, model
order by make, model  


select year, make, model, trim as trim_level, body_subtype as cab,
  bed_length as box, drive_type as drive,
  engine_size as engine, style as name_wo_trim
from dao.vin_reference  
where year = 2018
  and model = 'enclave'
*/



-----------------------------------------------------------------------------------------------------------------
-- 1/27/19  i think, finally, have what is needed to assign a configuration_id to open_orders with no vin
-- except drive
-----------------------------------------------------------------------------------------------------------------

--ok, this "works" gives me the 106 rows
-- table structures and relationships are iffy, need more modular tables it think, at least for clarity sake
-- too much overlap
-- fucking dataone calls AWD equinoxes 4X4
-- i know the engine option code and displacement for an order: 
-- guess the question is sourcing the option_codes_engines from chrome or dao
-- !!! option_codes_engines is only required to generate order_engine


drop table if exists dao.engine_option_codes;
create table dao.engine_option_codes  (
  model_year integer not null,
  make citext not null,
  model citext not null,
  model_code citext not null,
  engine_code citext not null,
  engine citext not null,
  foreign key (model_year, make, model, model_code) references dao.model_codes(model_year, make, model, model_code),
  primary key (model_year,make,model,model_code,engine_code));
comment on table dao.engine_option_codes is 'for each GM model, the option codes for engines.
  truncate and fill nightly. used for determining configuration_id for orders without a vin';
insert into dao.engine_option_codes    
select a.year as model_year, a.make, a.model, 
  a.mfr_model_num as model_code,
  b.engine_code, c.ice_displacement as engine
-- select *
from dao.vin_reference a
join dao.veh_trim_styles aa on a.vehicle_id = aa.vehicle_id
  and aa.fleet = 'N'
join dao.lkp_veh_eng b on a.vehicle_id = b.vehicle_id
join dao.def_engine c on b.engine_id = c.engine_id
where a.make in ('chevrolet','gmc','buick','cadillac')
  and a.year > 2017
group by a.year, a.make, a.model, a.mfr_model_num, 
  b.engine_code, c.ice_displacement;


-- 105 retail/stock (tre) orders with no configuration_id
select * from nc.open_orders where configuration_id is null and order_type = 'tre'
-- year <- order, make<-model_codes, model<-model_codes, model_code<-order, trim_level<-peg_to_trim, 
-- cab<-, drive<-, engine<-, box_size<-, chr_Style_id<-, alloc_group<-

-- at this juncture, only have drive for model_codes 'CC','CK', 'TC','TK'
-- drop table if exists wtf;
-- create temp table wtf as
drop table if exists nc.xfm_open_orders cascade;
create table nc.xfm_open_orders (
  order_number citext primary key,
  alloc_group citext not null,
  peg citext not null,
  model_year integer not null,
  make citext not null,
  model citext not null,
  model_code citext not null,
  trim_level citext not null,
  cab citext not null,
  drive citext,
  box_size citext not null,
  engine citext not null,
  color citext not null);
comment on table nc.xfm_open_orders is 'flesh out rows from nc.open_orders without a vin';  

truncate nc.xfm_open_orders;
insert into nc.xfm_open_orders  
select a.order_number, a.alloc_group, a.peg, a.model_year, 
  b.make, b.model, a.model_code, c.trim_level,
  case
    when left(a.model_code, 2) in ('CC','CK', 'TC','TK') and b.model not in ('suburban','tahoe','yukon','yukon xl') then
      case
        when right(a.model_code, 2) = '03' then 'reg'
        when right(a.model_code, 2) = '53' then 'double'
        when right(a.model_code, 2) = '43' then 'crew'
      end
    else 'n/a'
  end as cab,
  case
    when left(a.model_code, 2) in ('CC','CK', 'TC','TK') THEN -- and b.model not in ('suburban','tahoe') then
      case
        when substring(a.model_code, 2, 1) = 'C' then 'RWD'
        when substring(a.model_code, 2, 1) = 'K' then '4WD'
      end
  end as drive,  
  case
    when left(a.model_code, 2) in ('CC','CK', 'TC','TK') and b.model not in ('suburban','tahoe','yukon','yukon xl') then
      case
        when substring(a.model_code, 5,1) = '5' then 'Short'
        when substring(a.model_code, 5,1) = '7' then 'Standard'
        when substring(a.model_code, 5,1) = '9' then 'Long'
        else 'n/a'
      end
    else 'n/a'
  end as box_size, e.engine, f.color
from nc.open_orders a
join dao.model_codes b on a.model_year = b.model_year
  and a.model_code = b.model_code
join dao.peg_to_trim c on a.model_year = c.model_year
  and b.make = c.make  
  and b.model = c.model
  and a.peg = c.peg
join gmgl.vehicle_order_options d on a.order_number = d.order_number
  and d.thru_ts > now()
join dao.engine_option_codes e on a.model_year = e.model_year
  and a.model_code = e.model_code
  and d.option_code = e.engine_code 
left join chr.exterior_colors f on a.model_year = f.model_year
  and e.make = f.make
  and e.model = f.model
  and a.color_code = f.color_code
where a.configuration_id is null
  and a.order_type = 'tre';

select a.order_number, a.color, b.configuration_id
from nc.xfm_open_orders a
join nc.vehicle_configurations b on a.model_year = b.model_year
  and a.make = b.make
  and a.model = b.model
  and a.model_code = b.model_code
  and a.trim_level = b.trim_level
  and a.engine = b.engine



-- 31 without an existing configuration
-- wow, got lucky, this decodes to no dups
-- need to simulate a configuration from this data to see if there already exists a vehicle_configuration row
-- -- sierra 2500HD dao trim:base chr trim:none
-- -- update nc.open_orders
-- -- set color = 'Cardinal Red',
-- --     configuration_id = 52
-- -- where order_number = 'WNVWDR'
-- -- suburbans dao trim:Premier 1500 chr trim:Premier  config_id = 263
-- update nc.open_orders
-- set color = 'Black', configuration_id = 263
-- where order_number = 'WPXDB0';
-- update nc.open_orders
-- set color = 'Summit White', configuration_id = 263
-- where order_number = 'WQGV2X';
-- update nc.open_orders
-- set color = 'Satin Steel Metallic', configuration_id = 263
-- where order_number = 'WRZBJS'; 

select a.order_number, a.model_year, a.make, a.model, a.model_code, a.trim_level, 
  a.cab, a.drive, c.drive_type, a.box_size, a.engine, 
  'none' as name_wo_trim, b.config_type, b.level_header, a.peg
from nc.xfm_open_orders a
left join nc.vehicle_configurations b on a.model_year = b.model_year
  and a.make = b.make
  and a.model = b.model
  and a.model_code = b.model_code
  and a.trim_level = b.trim_level
  and a.engine = b.engine
left join dao_config c on a.model_year = c.year
  and a.make = c.make
  and a.model = c.model
  and a.trim_level = c.trim
  and a.model_code = c.mfr_model_num
  and a.engine = c.engine_size::citext
  and a.peg = c.mfr_package_code
where b.model_year is null 
order by a.model

select *
from dao_config
where mfr_model_num = 'cg33705'

--------------------------------------------------------------------------------------
-- 1/28/19
-- may want to actually define what it is i am trying to accomplish
-- 1.  update those rows in nc.open_orders without a vin with a configuration_id and color
-- 2.  as requd, create new nc.vehicle_configuration rows
-- 3.  add those vehicles from nc.open_orders without a vin to daily_inventory

---------------------------------------------------------------------------------------


-- this may become of use
drop table if exists dao_config;
create temp table dao_config as
select a.year,a.make,a.model,a.trim,a.mfr_model_num,a.mfr_package_code,
  a.drive_type,a.body_type,a.body_subtype,a.bed_length,a.engine_size, trans_type, array_agg(distinct a.vin_pattern)
from dao.vin_reference a
join dao.veh_trim_styles aa on a.vehicle_id = aa.vehicle_id
  and aa.fleet = 'N'
where a.year > 2017
  and a.make in ('chevrolet','gmc','buick','cadillac','honda','nissan')
group by a.year,a.make,a.model,a.trim,a.mfr_model_num,a.mfr_package_code,
  a.drive_type,a.body_type,a.body_subtype,a.bed_length,a.engine_size,trans_type;

select * 
from dao_config


-- here is the unique combination of attributes
select year,make,model,mfr_package_code,engine_size,trans_type,mfr_model_num,trim,drive_type
from dao_config x
group by year,make,model,mfr_package_code,engine_size,trans_type,mfr_model_num,trim,drive_type
having count(*) > 1  


select *
from dao_config x
where mfr_model_num = '12p43'

select *
from nc.vehicle_configurations
where model_code = 'tnl26'
  and model_year = 2019



