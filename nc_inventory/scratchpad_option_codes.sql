﻿select * from nc.vehicles

select aa.*, b.* 
from chr.describe_vehicle a
join nc.vehicles aa on a.vin = aa.vin
left join gmgl.option_codes b on aa.model_year = b.model_year
  and aa.model_code = b.model_code
where a.vin = '2GCVKPEC0K1141508'


select * 
from gmgl.vehicle_invoices
where vin = '2GCVKPEC0K1141508'



select aa.vin, aa.model_year, aa.model, aa.trim_level, b.option_code, c.description
from chr.describe_vehicle a
join nc.vehicles aa on a.vin = aa.vin
left join gmgl.vehicle_option_codes b on aa.vin = b.vin
left join gmgl.option_codes c on aa.model_year = c.model_year
  and aa.model_code = c.model_code
  and b.option_code = c.option_code
where a.vin = '2GCVKPEC0K1141508'
order by b.option_code


select * 
from gmgl.option_codes



murano
5N1AZ2MH0JN108138

select * from nc.stg_Availability a
where not exists (
  select 1
  from chr.describe_vehicle
  where vin = a.vin)


3GNCJKSB1KL255340

select *
from chr.describe_Vehicle
where vin = '3GNCJKSB1KL255340'



                  select vin, count(*) as style_count
                  from (
                    select vin, jsonb_array_elements(response->'factoryOption')
                    from chr.describe_vehicle
                    where style_count is null) a
                  group by vin


select a.*, (r.style ->'attributes'->>'id')::integer as chr_style_id,
  (r.style ->'attributes'->>'modelYear')::citext as chr_model_year,
  (r.style ->'division'->>'$value')::citext as chr_make,
  (r.style ->'model'->>'$value'::citext)::citext as chr_model,
  (r.style ->'attributes'->>'mfrModelCode')::citext as chr_model_code,
  case
    when r.style ->'attributes'->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->'attributes'->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->'attributes'->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->'attributes'->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as chr_drive,
  case
    when u.cab->>'$value' like 'Crew%' then 'crew' 
    when u.cab->>'$value' like 'Extended%' then 'double'  
    when u.cab->>'$value' like 'Regular%' then 'reg'  
    else 'N/A'   
  end as chr_cab,
  r.style ->'attributes'->>'trim' as chr_trim,
  t.displacement->>'$value' as engine_displacement, c.style_count/*, b.color , b.color_code */
from (
  select *
  from nc.stg_Availability) a
join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
join chr.describe_vehicle c on a.vin = c.vin
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->'attributes'->>'unit' = 'liters'  
where a.vin = '3GNCJKSB1KL255340'  
-- where c.style_count = 1

select *
from chr.describe_vehicle a
where vin = '3GNCJKSB1KL255340'  



select z.options->>'attributes'
from chr.describe_vehicle a
left join jsonb_array_elements(a.response->'factoryOptions') with ordinality as z(options) on true
  and z.ordinality = 1
where vin = '3GNCJKSB1KL255340'  










  and r.style ->'attributes'->>'modelYear' in ('2018','2019')
  and (r.style ->'model'->>'$value'::citext)::citext not like ('Silverado 1%')
