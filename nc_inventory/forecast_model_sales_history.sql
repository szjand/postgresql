﻿drop table if exists fc_models_model_codes cascade;
create temp table fc_models_model_codes as
select category, make, fc_model, model_codes
from (  
  select a.category, a.make, a.model as fc_model, array_agg(distinct c.model_code) as model_codes
  from nc.forecast_models a
  left join nc.forecast_model_configurations b on a.make = b.make
    and a.model = b.model
  left join nc.vehicle_configurations c on b.configuration_id = c.configuration_id  
  group by a.category, a.make, a.model) e;
create unique index on fc_models_model_codes(make,fc_model);



select d.year_month, a.sale_type, c.make
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
--   and b.make not in ('honda','nissan')
join fc_models_model_codes c on b.model_code = any(c.model_codes)
join dds.dim_date d on a.delivery_date = d.the_date
where extract(year from a.delivery_date) = 2019


select c.make, year_month, sale_type, count(*)
from nc.vehicle_sales a
join nc.vehicles b on a.vin = b.vin
--   and b.make not in ('honda','nissan')
join fc_models_model_codes c on b.model_code = any(c.model_codes)
join dds.dim_date d on a.delivery_date = d.the_date
where extract(year from a.delivery_date) = 2019
group by c.make, year_month, sale_type
order by year_month, make, sale_type

-- from gl, exclude ry2 & used
drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select store, page, line, line_label, control, sum(unit_count) as unit_count
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201901--------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201901 -------------------------------------------------------------------
      and (
        (b.page between 5 and 15 and b.line between 1 and 45)) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
--         or
--         (b.page = 16 and b.line between 1 and 14)) -- used cars
-- --         or
-- --         (b.page = 17 and b.line between 1 and 20))-- f/i
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
      and f.store = 'RY1') d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by store, page, line, line_label, control
order by store, page, line;

select * 
from step_1 a
left join nc.vehicle_sales b on a.control = b.stock_number
  and extract(month from b.delivery_date) = 1
order by a.control

select * 
from step_1 a
join nc.vehicle_sales b on a.control = b.stock_number
  and extract(month from b.delivery_date) = 1
order by a.control

select * from step_1 where page = 5 and line = 31 order by control



-- count by line is accurate
select store, page, line, line_label, /*control,*/ sum(unit_count) as unit_count
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201901--------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201901 -------------------------------------------------------------------
      and (
        (b.page between 5 and 15 and b.line between 1 and 45)) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
--         or
--         (b.page = 16 and b.line between 1 and 14)) -- used cars
-- --         or
-- --         (b.page = 17 and b.line between 1 and 20))-- f/i
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
      and f.store = 'RY1') d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by store, page, line, line_label--, control
order by store, page, line;


select * from fin.dim_fs_org

drop table if exists step_2;
create temp table step_2 as
select store, page, line, line_label, control
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201901--------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201901 -------------------------------------------------------------------
      and (
        (b.page between 5 and 15 and b.line between 1 and 45)) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
--         or
--         (b.page = 16 and b.line between 1 and 14)) -- used cars
-- --         or
-- --         (b.page = 17 and b.line between 1 and 20))-- f/i
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
      and f.store = 'RY1') d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by store, page, line, line_label, control
  having sum(unit_count) = 1
order by store, page, line;


what i want from gl is the stocknumbers for sales for each month
-- count on chev silverado (5/31) is going to be 1 higher than statement 
-- because this is actual sales and does not include the backon
-- otherwise perfect
select page, 
  count(*) filter (where line < 20) as retail_car,
  count(*) filter (where line = 22) as fleet_car,
  count(*) filter (where line = 23) as internal_car,
  count(*) filter (where line between 25 and 40) as retail_truck,
  count(*) filter (where line = 43) as fleet_truck,
  count(*) filter (where line = 44) as internal_truck
from step_2
group by page
order by page

select *
from (
  select * 
  from nc.vehicle_sales
  where sale_type = 'dealer trade'
    and extract(month from delivery_date) = 1
    and extract(year from delivery_Date) = 2019) a
left join step_2 b on a.stock_number = b.control    


-- ok, down to 15 void dealer trades that were subsequently retailed
select a.*, b.bopmast_Stock_number, b.date_capped, b.record_key, c.*
from nc.vehicle_sales a
join arkona.xfm_bopmast b on a.vin = b.bopmast_vin
  and b.current_row
  and b.record_status = 'U'
left join nc.vehicle_sales c on b.record_key = c.bopmast_id  
where a.sale_type = 'dealer trade'
  and a.stock_number not like 'H%'

select * from nc.vehicle_Sales where stock_number = 'g35925'

select * from arkona.xfm_bopmast where bopmast_stock_number = 'g35925'

-- yep, shows up as a voided sales transaction for the dt
-- not all as it may seem 33744 stocknumber changed
-- G35167 retail unwind then dealer traded

select a.control, a.post_Status, b.account, c.journal_code, d.description
from fin.fact_gl a
join fin.dim_Account b on a.account_key = b.account_key
  and b.account = '123706'
join fin.dim_journal c on a.journal_key = c.journal_key
join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
where a.control = 'g35925'


need to come up with some clean up and maintenance for void dealer trades


select *
from nc.vehicle_acquisitions a
join (
  select vin
  from nc.vehicle_acquisitions
  group by vin
  having count(*) > 1) b on a.vin = b.vin
left join nc.vehicle_sales c on a.vin = c.vin and a.ground_date = c.ground_date  
order by a.vin, a.ground_date      


--- yeah yeah all well and good, midgets and giants come back to the anomalies
-- for now, using gl, get basic counts for the past year


drop table if exists step_2;
create temp table step_2 as
select year_month, store, page, line, line_label, control, sum(unit_count) as unit_count
from (
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201809 and 201908
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select b.year_month, f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month between 201809 and 201908
      and (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
      and f.store = 'RY1') d on c.account = d.gl_account and b.year_month = d.year_month
  where a.post_status = 'Y') h
group by year_month, store, page, line, line_label, control
order by year_month, store, page, line;

-- this looks (comparing to fin statments) close enough
select year_month, page, 
  count(*) filter (where line < 20) as retail_car,
  count(*) filter (where line = 22) as fleet_car,
  count(*) filter (where line = 23) as internal_car,
  count(*) filter (where line between 25 and 40) as retail_truck,
  count(*) filter (where line = 43) as fleet_truck,
  count(*) filter (where line = 44) as internal_truck
from step_2
group by year_month, page
order by year_month, page


select a.year_month, d.category, d.make, d.fc_model,
  case
    when line in (22,43) then 'fleet'
    when line in (23,44) then 'internal'
    else 'retail' 
  end as sale_type
from step_2 a
join (
  select stock_number, vin
  from nc.vehicle_acquisitions
  union
  select stock_number, vin
  from nc.vehicle_sales) b on a.control = b.stock_number
join nc.vehicles c on b.vin = c.vin  
join fc_models_model_codes d on c.model_code = any(d.model_codes)
where a.unit_count > 0
  
drop table if exists sales;
create temp table sales as
select year_month, category, make, fc_model, 
  count(*) filter (where sale_type = 'retail') as retail,
  count(*) filter (where sale_type = 'fleet') as fleet,
  count(*) filter (where sale_type = 'internal') as internal
from (
  select a.year_month, d.category, d.make, d.fc_model,
    case
      when line in (22,43) then 'fleet'
      when line in (23,44) then 'internal'
      else 'retail' 
    end as sale_type
  from step_2 a
  join (
    select stock_number, vin
    from nc.vehicle_acquisitions
    union
    select stock_number, vin
    from nc.vehicle_sales) b on a.control = b.stock_number
  join nc.vehicles c on b.vin = c.vin  
  join fc_models_model_codes d on c.model_code = any(d.model_codes)
  where a.unit_count > 0) aa
group by year_month, category, make, fc_model;

  
select * from sales


select a.category, a.make, a.model, a.year_month, a.forecast, b.retail, b.fleet, b.internal
from nc.forecast a
left join sales b on a.category = b.category
  and a.make = b.make
  and a.model = b.fc_model
  and a.year_month = b.year_month
where a.current_row
  and a.year_month < 201909
order by a.category, a.make, a.model, a.year_month