﻿drop table if exists nc.new_vehicle_sales cascade;
create table nc.new_vehicle_sales (
  vin citext primary key,
  delivery_date date not null,
  model_year citext not null,
  make citext not null, 
  model citext not null,
  color citext,
  body_style citext, 
  model_code citext);
comment on table nc.new_vehicle_sales is '18 months of new vehicles sold retail from bopmast/inpmast, all makes and models';

insert into nc.new_vehicle_sales
select a.vin, max(a.delivery_date) as delivery_date, b.year, 
  b.make, b.model, b.color, b.body_style, b.model_code
from sls.ext_bopmast_partial a
inner join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row = true
--   and b.model = 'SILVERADO 1500'
where a.delivery_date between current_date - interval '18 months' and current_date
  and a.vehicle_type = 'N'
  and a.sale_type in ('R','L')
group by a.vin, b.year, b.make, b.model, b.color, b.body_style, b.model_code;


select *
from nc.new_vehicle_sales