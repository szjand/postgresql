﻿/*
/*
select *
from chr.describe_vehicle
where vin in (
  select distinct vin
  from sls.deals_by_month
  where model like 'equi%'
    and vehicle_type = 'new')


select a.vin, a.chrome_Style_id, a.model_year, a.style, a.model_code, a.engine_displacement, a.drive_train
from (
  select b.vin,
    jsonb_array_elements(b.response->'style')->'attributes'->>'id' as chrome_style_id,
    jsonb_array_elements(b.response->'style')->'attributes'->>'modelYear' as model_year,
    jsonb_array_elements(b.response->'style')->'attributes'->>'name' as style,
    jsonb_array_elements(b.response->'style')->'attributes'->>'mfrModelCode' as model_code ,
    jsonb_array_elements(jsonb_array_elements(response->'engine')->'displacement'->'value')->'attributes'->>'unit' as engine_displacement_units,
    jsonb_array_elements(jsonb_array_elements(response->'engine')->'displacement'->'value')->>'$value' as engine_displacement,
    jsonb_array_elements(b.response->'style')->'attributes'->>'drivetrain' as drive_train
  from chr.describe_vehicle b
  where vin in (
    select distinct vin
    from sls.deals_by_month
    where model like 'equi%'
      and vehicle_type = 'new')) a
left join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row      
where a.engine_displacement_units = 'liters'
order by a.vin      


select *
from arkona.xfm_inpmast 
where inpmast_vin = '3GNAXUEV7KS517567'
  and current_row


  
select *
from gmgl.vehicle_orders
limit 10

*/
select * from nc. vehicles limit 10
select * from  nc.stg_availability

-- 10/22 from the silverado script
-- vehicles from accounting AND inpmast not yet in nc.vehicle_acquisitions
truncate nc.stg_availability;
insert into nc.stg_availability (the_date,journal_date,stock_number,
  vin,model_year,make, model,
  model_code,color_code,color, source)
select current_date, f.the_date, f.control, 
  g.inpmast_vin, g.year, g.make, g.model, g.model_code, 
  g.color_code, g.color,
  case f.journal_code
    when 'PVI' then 'factory'
    when 'CDH' then 'dealer trade'
    when 'GJE' then 'ctp'
  end as source
from ( -- accounting journal
  select a.control, d.journal_code, max(b.the_date) as the_date, sum(a.amount) as amount
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_date between current_Date - 90 and current_date--'06/22/2018'
  inner join fin.dim_account c on a.account_key = c.account_key
    and c.account = '123700'
  inner join fin.dim_journal d on a.journal_key = d.journal_key
    and d.journal_code in ('PVI','CDH','GJE')
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where a.post_status = 'Y'
  group by a.control, d.journal_code
  having sum(a.amount) > 10000) f
inner join arkona.xfm_inpmast g on f.control = g.inpmast_stock_number
  and g.model like 'eq%' 
  and g.current_row = true
  and g.year in ('2018', '2019')
where not exists (
  select 1
  from nc.vehicle_acquisitions
  where stock_number = f.control) ;

-- 10/23 it is official i give up on trying ascertain source as dealer trade from accounting with journal PVI
-- i don't currently have access to historical invoices, but ongoing should be ok
-- this shows me acquisitions that by default show source as factory but are dealer trades based on invoice
update nc.stg_availability
set source = 'dealer trade'
where stock_number in (
  select a.stock_number
  -- select a.*, regexp_replace(b.delivery_store, '\n', '', 'g')
  from nc.stg_availability a
  join gmgl.vehicle_vis b on a.vin = b.vin
  where regexp_replace(b.delivery_store, '\n', '', 'g') not like 'RYDELL%'
    and a.source = 'factory');


do 
$$
begin
assert ( -- Delivery store = Rydell, source <> factory
  select count(*)
  -- select a.*, regexp_replace(b.delivery_store, '\n', '', 'g')
  from nc.stg_availability a
  join gmgl.vehicle_vis b on a.vin = b.vin
  where regexp_replace(b.delivery_store, '\n', '', 'g') like 'RYDELL%'
    and a.stock_number not like '%R'
    and a.source <> 'factory') = 0, 'Delivery store = Rydell, source <> factory';
end
$$;

do 
$$
begin
assert ( -- Delivery store not Rydell, source = factory
  select count(*)
  -- select a.*, regexp_replace(b.delivery_store, '\n', '', 'g')
  from nc.stg_availability a
  join gmgl.vehicle_vis b on a.vin = b.vin
  where regexp_replace(b.delivery_store, '\n', '', 'g') not like 'RYDELL%'
    and a.stock_number not like '%R'
    and a.source = 'factory') = 0, 'Delivery store not Rydell, source = factory';
end
$$;


-- unwind acquisitions
-- test for unwind on same day as sale ?!?
insert into nc.stg_availability (the_date,stock_number,vin,model_year,
  model_code,color_code,color,ground_date,source)
select current_date, a.stock_number, a.vin, b.model_year,
  b.model_Code, c.color_code, b.color, a.run_date - 1, 'unwind'
from sls.deals a
inner join nc.vehicle_acquisitions aa on a.stock_number = aa.stock_number -- to be an unwind, previous acq record must exist
inner join nc.vehicles b on a.vin = b.vin
  and b.model = 'equinox'
inner join arkona.xfm_inpmast c on b.vin = c.inpmast_vin
  and c.current_row = true
where a.run_date > current_Date - 60
  and deal_status_code = 'deleted'
  and a.stock_number <> 'G34612'
  and not exists (
    select 1
    from nc.vehicle_acquisitions
    where stock_number = a.stock_number
      and ground_date = a.run_date - 1);


-------------------------------------------------------------------------------------
--< ground dates
-------------------------------------------------------------------------------------
-- ctp
update nc.stg_availability
set ground_date = journal_date
where source = 'ctp';

-- dealer trades: ground = coalesce(keyper, coaelsece dt ro, journal date)
-- 10/23 any ro
update nc.stg_availability x
set ground_date = y.ground
from (
  select aa.stock_number, aa.vin, aa.journal_date, aa.source, 
    bb.the_date, cc.creation_date as keyper,
    coalesce(cc.creation_date, coalesce(bb.the_date, aa.journal_date)) as ground
  from nc.stg_availability aa
  left join (
    select d.vin, min(b.the_date) as the_Date
    from ads.ext_fact_repair_order a
    inner join dds.dim_date b on a.opendatekey = b.date_key
--     inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
    inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
    inner join nc.stg_availability e on d.vin = e.vin
    group by d.vin) bb on aa.vin = bb.vin
  left join ads.keyper_creation_dates cc on aa.stock_number = cc.stock_number  
  where aa.source = 'dealer trade') y
where x.stock_number = y.stock_number; 

-- factory
-- 10/23 any ro, no need for order info
-- if there is neither keyper nor ro, ground = 12/31/9999
update nc.stg_availability x
set ground_date = y.ground
from (
  select aa.stock_number, aa.vin, aa.journal_date, aa.source, 
    bb.the_date, cc.creation_date as keyper, -- dd.event_code, dd.description, dd.from_date,
    coalesce(cc.creation_date, coalesce(bb.the_date, '12/31/9999'::date)) as ground
  from nc.stg_availability aa
  left join (
    select d.vin, min(b.the_date) as the_date
    from ads.ext_fact_repair_order a
    inner join dds.dim_date b on a.opendatekey = b.date_key
    inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
    inner join nc.stg_availability e on d.vin = e.vin
    group by d.vin) bb on aa.vin = bb.vin
  left join ads.keyper_creation_dates cc on aa.stock_number = cc.stock_number  
  where source = 'factory') y  
where x.stock_number = y.stock_number;


-------------------------------------------------------------------------------------
--/> ground dates
-------------------------------------------------------------------------------------

-- ok, delete if ground = 12/31/9999 
delete 
-- select * 
from nc.stg_availability
where ground_date = '12/31/9999';

do
$$
begin
assert ( -- null ground date
  select count(*)
  -- select *
  from nc.stg_availability a
  where ground_date is null) = 0, 'acquisitions with null ground date';
end
$$;

-------------------------------------------------------------------------------------
--< colors
-------------------------------------------------------------------------------------
do
$$
begin
assert (
  select count(*)
  -- select *
  from nc.stg_availability a
  left join nc.exterior_colors b on a.color_code = b.mfr_color_code
    and a.model_year = b.model_year
    and a.make = b.make
    and a.model = b.model 
  where (a.color <> b.mfr_color_name or b.mfr_color_name is null)) = 0, 'DANGER WILL ROBINSON';
end
$$;

/*
select *
  from nc.stg_availability a
  left join nc.exterior_colors b on a.color_code = b.mfr_color_code
    and a.model_year = b.model_year
    and a.make = b.make
    and a.model = b.model 
  where (a.color <> b.mfr_color_name or b.mfr_color_name is null)
order by a.color

update nc.stg_availability set color = 'Cajun Red Tintcoat' where stock_number = 'G35238'

update nc.stg_availability set color = 'Storm Blue' where stock_number = 'G35714'
 
select *
from nc.exterior_colors
where make = 'chevrolet'
  and model = 'equinox'
  and model_year in ( '2018','2019')
--   and mfr_color_name like 'silver%'
  and mfr_color_code = 'g35'

update nc.stg_availability
set color = 'Nightfall Gray Metallic'
where stock_number = 'G35864';

update nc.stg_availability
set color = 'Silver Ice Metallic',
    color_code = 'GAN'
where stock_number in ( 'G35258');

update nc.stg_availability
set color = 'Mosaic Black Metallic'
where stock_number in ( 'G35257');


update nc.stg_availability
set color = 'Pepperdust Metallic',
    color_code = 'GMU'
where stock_number in ( '31587R');

update nc.stg_availability
set color = 'Orange Burst Metallic',
    color_code = 'GGQ'
where stock_number in ( '30850');
  
update nc.stg_availability
set color = 'Satin Steel Metallic',
    color_code = 'G9K'
where stock_number in ( '31767');

update nc.stg_availability
set color = 'Pepperdust Metallic',
    color_code = 'GMU'
where stock_number in ( '31929');
  
update nc.stg_availability
set color = 'Pacific Blue Metallic'
where stock_number in ( 'G35259');

update nc.stg_availability
set color = 'Cajun Red Tintcoat'
where stock_number in ( 'G35309');

update nc.stg_availability
set color = 'Cajun Red Tintcoat'
where make = 'chevrolet'
  and model = 'equinox'
  and model_year in ('2018','2019')
  and color_code = 'GPJ'
  and color <> 'Cajun Red Tintcoat';

update nc.stg_availability
set color = 'Cajun Red Tintcoat',
    color_code = 'GPJ'
where color_code is null
  and color like 'cajun%';

update nc.stg_availability
set color = 'Storm Blue'
where make = 'chevrolet'
  and model = 'equinox'
  and model_year in ('2018','2019')
  and color_code = 'G35'
  and color <> 'Storm Blue';  

update nc.stg_availability
set color = 'Satin Steel Metallic'
where make = 'chevrolet'
  and model = 'equinox'
  and model_year in ('2018','2019')
  and color_code = 'G9K'
  and color <> 'Satin Steel Metallic';    

update nc.stg_availability
set color = 'Mosaic Black Metallic',
    color_code = 'GB8'
where model = 'equinox'
  and color like 'Mosaic%';

update nc.stg_availability
set color = 'Summit White',
    color_code = 'GAZ'
where stock_number in ('30776','31486','30948','31401','G35163')  

update nc.stg_availability
set color = 'Nightfall Gray Metallic',
    color_code = 'G7Q'
where model = 'equinox'
  and color like 'nightfall%';

update nc.stg_availability
set color = 'Iridescent Pearl Tricoat',
    color_code = 'G1W'
where model = 'equinox'
  and color like 'iridescent%';  
*/

-------------------------------------------------------------------------------------
--/> colors
-------------------------------------------------------------------------------------


-- all vins exist in chr.describe_vehicle
do
$$
begin
assert (
  select count(*)
--   select * --a.vin
  from nc.stg_availability a
  left join (
    select vin, count(*)
    from (
      select vin, jsonb_array_elements(response->'style')
      from chr.describe_vehicle) x
    group by vin) b on a.vin = b.vin
  where b.vin is null) = 0, 'DANGER WILL ROBINSON, there are new vins missing from chr.describe_vehicle';
end
$$;  

-- vins with multiple chrome styles (so far, none)
do
$$
begin
assert (
      select count(*) = sum(b.style_count)
      from nc.stg_availability a
      left join chr.describe_vehicle b on a.vin = b.vin), 'DANGER WILL ROBINSON, vehicles with multiple chrome styles';
end
$$;


-- new acquisitions where vin already exists in nc.vehicles
insert into nc.vehicle_acquisitions (vin,stock_number,ground_date,source,in_transit_sale)
select a.vin, a.stock_number, a.ground_date, a.source, false
from nc.stg_availability a
inner join nc.vehicles aa on a.vin = aa.vin
  and a.source <> 'unwind'; -- unwinds are processed separately



/*
-- configurations, accross 2018/2019
-- excludes w/2FL until i run into one - exclusive to 2019
-- based on this query:
-- -- select year, model, trim, style, drive_type, engine_size, mfr_model_num, mfr_package_code
-- -- from dao.vin_Reference
-- -- where model = 'equinox'
-- --   and year in (2018,2019)
-- -- group by year, model, trim, style, drive_type, engine_size, mfr_model_num, mfr_package_code
-- -- order by year, trim, drive_type, engine_size
-- distinct styles from chrome
-- -- select distinct style
-- -- from (
-- -- select  
-- --   jsonb_array_elements(response->'style')->'model'->>'$value' as model,
-- --   jsonb_array_elements(response->'style')->'attributes'->>'name' as style
-- -- from chr.describe_vehicle
-- -- where vin = '2GNAXJEV5J6164127') a
-- -- where model = 'Equinox'
-- distinct styles from invoices
-- -- select  left(raw_invoice, 35), count(*)
-- -- from gmgl.vehicle_invoices
-- -- where raw_invoice like '%EQUINOX%'
-- -- group by left(raw_invoice, 35)-- -- select  left(raw_invoice, 35), count(*)
-- -- from gmgl.vehicle_invoices
-- -- where raw_invoice like '%EQUINOX%'
-- -- group by left(raw_invoice, 35)

trim    drive   engine 
L       FWD     1.5
LS      FWD     1.5
LS      AWD     1.5
LT      FWD     1.5
LT      FWD     1.6
LT      FWD     2.0
LT      AWD     1.5
LT      AWD     1.6
LT      AWD     2.0
PREM    FWD     1.5
PREM    FWD     1.6
PREM    FWD     2.0
PREM    AWD     1.5
PREM    AWD     1.6
PREM    AWD     2.0
*/


--------------------------------------------------------------------------------------------------
--< nc.vehicles
--------------------------------------------------------------------------------------------------
-- new acquisitions where vin does not yet exist in nc.vehicles
-- AND chr.describe_vehicles.style_count = 1
-- first, insert into nc.vehicles
-- 11/19/18 refactored jsonb_array_elements out of select clause
-- 12/2/18 as i move toward all inventory, realized trim decoding needs to be specific
insert into nc.vehicles(vin,chrome_style_id,model_year,make,model,
  model_code,drive,cab,trim_level,engine,color)
select vin, chr_style_id::integer, model_year, make, model, chr_model_code,
  case drive_train
    when 'All Wheel Drive' then 'AWD'
    when 'Front Wheel Drive' then 'FWD'
    else 'XXX'
  end as drive_train,
  'N/A'::citext as cab,
  case 
    when c.style in ('AWD 4dr LT w/3LT','AWD 4dr LT w/1LT','AWD 4dr LT w/2LT',
      'FWD 4dr LT w/1LT','FWD 4dr LT w/2LT','') then 'LT'
    when c.style in ('AWD 4dr LS w/1LS','FWD 4dr LS w/1LS') then 'LS'
    when c.style in ('AWD 4dr Premier w/2LZ','AWD 4dr Premier w/1LZ','AWD 4dr Premier w/3LZ') then 'PREM'
    else 'XXXXXX'
  end as trim_level,
  c.engine_displacement, c.color
from(
  select a.*,
    r.style ->'attributes'->>'id' as chr_style_id,
    r.style ->'attributes'->>'mfrModelCode' as chr_model_code,
    r.style ->'attributes'->>'modelYear' as chr_model_year,
    r.style ->'attributes'->>'name' as style,
    r.style ->'attributes'->>'drivetrain' as drive_train,
    t.displacement->>'$value' as engine_displacement
  from nc.stg_availability a      
  left join chr.describe_vehicle b on a.vin = b.vin
  left join jsonb_array_elements(b.response->'style') as r(style) on true
  left join jsonb_array_elements(b.response->'engine') as s(engine) on true
  left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
    and t.displacement ->'attributes'->>'unit' = 'liters'
  where b.style_count = 1
    and a.source <> 'unwind' -- unwinds are processed separately
    and not exists (
      select 1
      from nc.vehicles
      where vin = a.vin)) c;

do
$$
begin
assert (
  select count(*)
--   select * --a.vin
  from nc.vehicles
  where trim_level like 'XX%') = 0, 'Undecoded trims';
end
$$;        
--------------------------------------------------------------------------------------------------
--/> nc.vehicles
--------------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------------
--< gmgl.sku
--------------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------------
--/> gmgl.sku
--------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------
--< nc.vehicle_acquisitions
--------------------------------------------------------------------------------------------------
insert into nc.vehicle_acquisitions  (vin,stock_number,ground_date,source,in_transit_sale)
select a.vin, a.stock_number, a.ground_date, a.source, false
from nc.stg_availability a
left join nc.vehicle_acquisitions b on a.stock_number = b.stock_number
  and a.ground_date = b.ground_date
where b.vin is null  
  and a.source <> 'unwind';

-- and finally, the unwind acquisition
-- not sure how this will work for multiple unwinds, only want to update the 
-- must recent acquisition record 
update nc.vehicle_acquisitions x
set thru_date = y.the_date
from (
  select a.vin, a.stock_number, a.ground_date - 1 as the_date
  from nc.stg_availability a
  where source = 'unwind') y
where x.stock_number = y.stock_number;

insert into nc.vehicle_acquisitions  (vin,stock_number,ground_date,source,in_transit_sale)
select a.vin, a.stock_number, a.ground_date, a.source, false
from nc.stg_availability a
where source = 'unwind';  
--------------------------------------------------------------------------------------------------
--/> nc.vehicle_acquisitions
--------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
--< SALES
--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
--< nc.stg_sales
--------------------------------------------------------------------------------------------------
-- accounting should include all sales
-- first step, accounting info and vin from inpmast
truncate nc.stg_sales;
insert into nc.stg_sales (stock_number, vin, trans_description, amount, acct_date,ground_date)
select a.control, f.inpmast_vin, e.description, a.amount, b.the_date, g.ground_date
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
--   and b.the_date between current_Date - 40 and current_date
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account = '123700'
inner join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code = 'VSN'
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
left join arkona.xfm_inpmast f on a.control = f.inpmast_stock_number
  and f.current_row = true  
left join nc.vehicle_acquisitions g on a.control = g.stock_number
  and g.thru_date = ( -- need most recent acquisition
    select max(thru_date)
    from nc.vehicle_acquisitions
    where stock_number = g.stock_number)  
where a.post_status = 'Y'
  and abs(a.amount) > 10000
  and f.model like 'equ%'
  and f.year in ('2018', '2019')
  and not exists (
    select 1
    from nc.vehicle_sales
    where stock_number = a.control
      and ground_date = g.ground_date) ;

do 
$$
begin
assert ( -- ground date null
  select count(*)
--   select stock_number
  from nc.stg_sales a
  where a.ground_date is null) = 0, 'NULL ground date';
end
$$;


---------------------------------------------------------------------------------------
-- select * from nc.stg_sales where ground_date is null
--< great start off with one with no ground date
---------------------------------------------------------------------------------------

-- 11/19 G35531 sold on 11/17 no ground date, therefor assume sold in transit
-- have been making the acquisition source = intransit sale, doesn't feel right, overwrites the
-- actual source
-- current intransit sources: G35311, G35313, G35023, G35185 all are actually source = factory
-- instead to acquisitions add a boolean attribute of in_transit_sale, make the ground date
-- same as the sale date

/* update the ddl and clean up

alter table nc.vehicle_Acquisitions
add column in_transit_sale boolean;

update nc.vehicle_acquisitions
set in_transit_sale = false;

-- clean up existing data
update nc.vehicle_acquisitions
set in_transit_sale = true,
    source = 'factory'
where stock_number in ('G35311','G35313','G35023','G35185');

-- and make the column not null
alter table nc.vehicle_acquisitions
alter column in_transit_sale set not null;

*/

/*

-- sales with no ground date, the assumption is that there is no ground date because there is no acquisition: 
1. 
select * from nc.stg_sales where ground_date is null
process these one at a time
select * from gmgl.vehicle_orders where vin = '2GNAXUEV8K6191387'
select * from gmgl.vehicle_order_events where order_number = 'WNVVT9'

2. in chr.describe_Vehicle?
select * from chr.describe_vehicle where vin = '2GNAXUEV8K6191387'

3. create a row in nc.vehicles, nc.vehicle_acquisitions, update ground_date in nc.stg_sales

-----------------------------------------------------------------------------------------
-- don't know if i will ever use it, it comes to mind as try to figure out how to 
-- model vehicle orders in this schema
-- maybe another optional field, actual_ground_date
-- actual ground date for in_transit_sale vehicles:
  select aa.*,
    bb.the_date, cc.creation_date as keyper
  from nc.vehicle_acquisitions aa
  left join (
    select d.vin, min(b.the_date) as the_date
    from ads.ext_fact_repair_order a
    inner join dds.dim_date b on a.opendatekey = b.date_key
    inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
    inner join nc.vehicle_acquisitions e on d.vin = e.vin
      and e.in_transit_sale
    group by d.vin) bb on aa.vin = bb.vin
  left join ads.keyper_creation_dates cc on aa.stock_number = cc.stock_number  
  where in_transit_sale

------------------------------------------------------------------------------------------
do
$$
declare
  _stock_number citext := 'G35747';
begin  
-- nc.vehicles 
  insert into nc.vehicles(vin,chrome_style_id,model_year,make,model,
    model_code,drive,cab,trim_level,engine,color)
  select vin, chr_style_id::integer, chr_model_year, make, model, chr_model_code,
    case drive_train
      when 'All Wheel Drive' then 'AWD'
      when 'Front Wheel Drive' then 'FWD'
      else 'XXX'
    end as drive_train,
    'N/A'::citext as cab,
    case 
      when c.style like '%4dr LT%' then 'LT'
      when c.style like '%4dr LS%' then 'LS'
      when c.style like '%4dr L%' then 'L'
      when c.style like '%Premier%' then 'PREM'
      else 'XXXXXX'
    end as trim_level,
    c.engine_displacement, c.color
  from(
    select a.*,
      r.style ->'attributes'->>'id' as chr_style_id, r.style ->'attributes'->>'mfrModelCode' as chr_model_code,
      r.style ->'attributes'->>'modelYear' as chr_model_year, r.style ->'attributes'->>'name' as style,
      r.style ->'attributes'->>'drivetrain' as drive_train, t.displacement->>'$value' as engine_displacement,
      b.response->'attributes'->>'bestMakeName' as make, b.response->'attributes'->>'bestModelName' as model,
      d.color   
    from nc.stg_sales a      
    left join chr.describe_vehicle b on a.vin = b.vin
    left join jsonb_array_elements(b.response->'style') as r(style) on true
    left join jsonb_array_elements(b.response->'engine') as s(engine) on true
    left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
      and t.displacement ->'attributes'->>'unit' = 'liters'
    left join arkona.xfm_inpmast d on a.stock_number = d.inpmast_stock_number
      and d.current_row 
    where a.stock_number = _stock_number) c;   

  -- now need the acquisition
  -- ground date = sale date = thru_Date
  insert into nc.vehicle_acquisitions(vin,stock_number,ground_date,thru_date,source,in_transit_sale)
  select vin, stock_number, acct_date, acct_date, 'factory', true
  from nc.stg_sales
  where stock_number = _stock_number;

-- update the ground date in nc.stg_sales
-- select * from nc.stg_sales
  update nc.stg_sales
  set ground_date = acct_date
  where stock_number = _stock_number;
  
end  
$$;

*/
---------------------------------------------------------------------------------------
--/> great start off with one with no ground date
---------------------------------------------------------------------------------------

-- bopmast_id & delivery_date from sls.deals
update nc.stg_sales x
set bopmast_id = y.bopmast_id,
    delivery_date = y.delivery_date
from (    
  select b.*
  from nc.stg_sales a
  left join (
    select stock_number, bopmast_id, max(seq) as seq, max(delivery_date) as delivery_date
    from sls.deals
    group by stock_number, bopmast_id) b on a.stock_number = b.stock_number) y
where x.stock_number = y.stock_number;

-- sale_types dealer trade & ctp
update nc.stg_sales x
set sale_type =
  case
    when bopmast_id is null then 'dealer trade'
    when trans_description like '%RYDELL%' then 'ctp'
  end;

 -- sale_type fleet
 update nc.stg_sales x
set sale_type = 'fleet'
where stock_number in (
  select a.stock_number
  from nc.stg_sales a
  inner join (
    select a.control, b.the_date
    from fin.fact_gl a
    inner join dds.dim_date b on a.date_key = b.date_key
    inner join fin.dim_account c on a.account_key = c.account_key
    inner join (
      select distinct d.gl_account
      from fin.fact_fs a 
      inner join fin.dim_fs b on a.fs_key = b.fs_key
        and b.year_month > 201601
        and b.page in (5,8,9,10)
        and b.line in (22,43)
        and b.col = 1 -- sales
      inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
        and c.store = 'ry1'
      inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key) d on c.account = d.gl_account
    where a.post_status = 'Y') b on a.stock_number = b.control);

-- dealer trade delivery dates
update nc.stg_sales
set delivery_date = acct_date
where sale_type = 'dealer trade';

-- and finally, everything else is retail
update nc.stg_sales
set sale_type = 'retail'
where sale_type is null;    

-- test for ground date > delivery date
do
$$
begin
assert (
  select count(*)
  -- select *
    from nc.stg_sales
    where ground_date > delivery_date) = 0, 'ground date > delivery date';
end
$$;


/*

G35630: ground_date: 11/19/18, delivery_date = 11/16/18
acquired as a dealer trade
board data says physically delivered on 11/16/18
select * from nc.stg_sales
select * from ads.keyper_creation_dates where stock_number = 'g35630'
select * from nc.vehicle_acquisitions where stock_number = 'g35630'
change ground date in nc.stage_sales & nc.vehicle_acquisitions

update nc.vehicle_acquisitions
set ground_Date = '11/16/2018'
where stock_number = 'G35630';
update nc.stg_sales
set ground_date = '11/16/2018'
where stock_number = 'G35630';


*/

-- update nc.vehicle_acquisitions.thru_date for sales
update nc.vehicle_acquisitions x
set thru_date  = y.delivery_date
from (
  select a.*
  from nc.stg_sales a
  left join nc.vehicle_acquisitions b on a.stock_number = b.stock_number) y
where x.stock_number = y.stock_number
  and x.thru_date = '12/31/9999';

-- and install the sales
insert into nc.vehicle_sales
select stock_number, ground_date, vin, coalesce(bopmast_id, -1), delivery_date, sale_type
from nc.stg_sales;  

--------------------------------------------------------------------------------------------------
--/> nc.stg_sales
--------------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
--/> SALES
--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------




--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
--< ORDERS
--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
-- 1. delete rows from nc.open_orders where the vin now exists in nc.vehicles (vehicle on ground or sold in transit)
delete 
-- select *
from nc.open_orders
where vin in (
  select a.vin
  from nc.open_orders a
  join nc.vehicles b on a.vin = b.vin);

-- 2. insert new rows, update changed rows
insert into nc.open_orders
select order_number, event_code, vin, order_type, alloc_Group, 
  model_year::integer, model_code, vehicle_trim,
  dan, peg, color, category 
from ( -- equinox orders where current_event < 5000
  select b.vin,
    b.order_number, b.order_type, b.alloc_group, b.model_year, 
    b.model_code, b.vehicle_trim, b.dan, b.peg, b.color, 
    c.event_code, c.from_Date, c.thru_date, d.*
  from gmgl.vehicle_orders b
  left join gmgl.vehicle_order_events c on b.order_number = c.order_number
    and c.thru_date = (
      select max(thru_date)
      from gmgl.vehicle_order_events
      where order_number = c.order_number)
  left join gmgl.vehicle_event_codes d on c.event_code = d.code
  where left(d.code, 1)::integer < 5) aa
where not exists (
  select 1
  from nc.vehicles
  where coalesce(vin, 'no vin') = aa.vin) 
on conflict (order_number)  
do update set (event_code,vin,order_type,alloc_group,model_year,model_code,vehicle_trim,
  dan,peg,color,category)
= (excluded.event_code, excluded.vin, excluded.order_type, excluded.alloc_group, 
     excluded.model_year, excluded.model_code, excluded.vehicle_trim, 
      excluded.dan, excluded.peg,excluded.color, excluded.category);

--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
--/> ORDERS
--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------

select * from nc.open_orders order by alloc_group
























-- doesnt seem to work for dealer trade determination like in silverados
select a.stock_number, a.vin, a.source, b.*
from nc.stg_availability a
join fin.fact_gl b on a.stock_number = b.control
  and b.post_Status = 'Y'
join fin.dim_account c on b.account_key = c.account_key
  and c.account = '126102'
join fin.dim_journal d on b.journal_key = d.journal_key
  and d.journal_code = 'PVI'
  
select * 
from gmgl.vehicle_vis
where vin = '2GNAXUEV1K6122346'

select b.account, c.journal_code, a.amount, d.description, b.description
from fin.fact_gl a
join fin.dim_account b on a. account_key = b.account_key
join fin.dim_journal c on a.journal_key = c.journal_key
join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
where a.post_status = 'Y'
  and a.control = 'G34274'
order by b.account, c.journal_code


 select a.*, b.source, c.ship_to_partid
 from nc.vehicle_Sales a
 join nc.vehicle_acquisitions b on a.stock_number = b.stock_number
 left join arkona.ext_inpvinv c on a.vin = c.vin
 order by delivery_Date desc
 limit 100

select *
from arkona.ext_inpvinv
limit 10



select *
from nc.vehicle_Acquisitions a
where a.thru_date > current_Date
order by a.ground_date desc


select b.account, c.journal_code, a.amount, d.description, b.description
from fin.fact_gl a
join fin.dim_account b on a. account_key = b.account_key
join fin.dim_journal c on a.journal_key = c.journal_key
join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key
where a.post_status = 'Y'
  and a.control = 'G34752'
order by b.account, c.journal_code

select e.the_date, b.control, b.amount
from fin.fact_gl b 
join fin.dim_account c on b.account_key = c.account_key
  and c.account = '126102'
join fin.dim_journal d on b.journal_key = d.journal_key
  and d.journal_code = 'PVI'
join dds.dim_Date e on b.date_key = e.date_key  
where b.post_Status = 'Y'
  and length(b.control) > 3
order by the_date desc
limit 300


select *
from nc.vehicles
limit 100




-- accounting should include all sales
-- first step, accounting info and vin from inpmast
truncate nc.stg_sales;
insert into nc.stg_sales (stock_number, vin, trans_description, amount, acct_date,ground_date)
select a.control, f.inpmast_vin, e.description, a.amount, b.the_date, g.ground_date
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_date between current_Date - 40 and current_date
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account = '123700'
inner join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code = 'VSN'
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
left join arkona.xfm_inpmast f on a.control = f.inpmast_stock_number
  and f.current_row = true  
left join nc.vehicle_acquisitions g on a.control = g.stock_number
  and g.thru_date = ( -- need most recent acquisition
    select max(thru_date)
    from nc.vehicle_acquisitions
    where stock_number = g.stock_number)  
where a.post_status = 'Y'
  and abs(a.amount) > 10000
  and f.model like 'silverado 1500%'
  and f.year in ('2018', '2019')
  and not exists (
    select 1
    from nc.vehicle_sales
    where stock_number = a.control) ;








 
-- truncate nc.stg_sales;
-- insert into nc.stg_sales (stock_number, vin, trans_description, amount, acct_date,ground_date)
-- 336 sold 2018/2019 equinoxes
drop table if exists sales_1;
create temp table sales_1 as
select a.control, f.inpmast_vin, e.description, a.amount, b.the_date, g.ground_date
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
--   and b.the_date between current_Date - 40 and current_date
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account = '123700'
inner join fin.dim_journal d on a.journal_key = d.journal_key
  and d.journal_code = 'VSN'
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
left join arkona.xfm_inpmast f on a.control = f.inpmast_stock_number
  and f.current_row = true  
-- left join nc.vehicle_acquisitions g on a.control = g.stock_number
left join acq_1 g on a.control = g.stock_number
  and g.thru_date = ( -- need most recent acquisition
    select max(thru_date)
    from nc.vehicle_acquisitions
    where stock_number = g.stock_number)  
where a.post_status = 'Y'
  and abs(a.amount) > 10000
  and f.model like 'equi%'
  and f.year in ('2018', '2019')
  and not exists (
    select 1
    from nc.vehicle_sales
    where stock_number = a.control) ;

select * from sales_1 order by the_date