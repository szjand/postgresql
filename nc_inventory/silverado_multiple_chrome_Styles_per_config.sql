﻿holy shit
reran the vins describe_vehicle
now they are identical but for the vin

it is actually the the chrome style id on the "old" record (130524) that changes from 413131 to 413032

select *
from nc.vehicle_configurations
where model_code = 'ck10903'


select chrome_style_id
from (
select configuration_id, chrome_style_id
from nc.vehicle_configurations
group by configuration_id, chrome_style_id) a
group by chrome_style_id
having count(*) > 1


select *
from nc.vehicle_configurations 
where chrome_Style_id in (
select chrome_style_id
from (
select configuration_id, chrome_style_id
from nc.vehicle_configurations
group by configuration_id, chrome_style_id) a
group by chrome_style_id
having count(*) > 1)
order by chrome_style_id


L82

select * from nc.vehicles where configuration_id = 1051

select *
from gmgl.vehicle_invoices
where vin = '3GCNYAEF4MG158747'

select * from chr.describe_vehicle where vin = '3GCNYAEF9MG130524'

select * from chr.describe_vehicle where vin = '3GCNYAEF4MG158747'

-- rerunning describe vehicle
delete from chr.describe_vehicle where vin = '3GCNYAEF9MG130524';
delete from chr.describe_vehicle where vin = '3GCNYAEF4MG158747';

select chrome_Style_id, engine, trim_level
from nc.vehicle_configurations
group by chrome_style_id, engine, trim_level
having count(*) > 1


select model_year, make, model, model_code, trim_level, cab, engine
from nc.vehicle_configurations
group by model_year, make, model, model_code, trim_level, cab, engine
having count(*) > 1


select *
from jon.configurations a
where chrome_Style_id in ('413032','413131')


select * 
from jon.get_styles a
limit 10

  select *
    from (
    select (attributes #>> '{id}') chrome_style_id, model_id, b."$value" as style_name
    from jon.get_styles,
      jsonb_to_recordset(jon.get_styles.styles) as b("$value" citext, attributes jsonb)) c
where model_id = 33344


select * 
from jon.models      
order by model_id


select * 
from jon.get_models_by_division      


  select * 
  from (
    select (attributes #>> '{id}')::integer as model_id, model_year, division_id, b."$value" as model
    from jon.get_models_by_division,
      jsonb_to_recordset(jon.get_models_by_division.models) as b("$value" citext, attributes jsonb)) c
where model_year = 2021
  and division_id = 8
order by  model_id

i dont know if this is going anywhere, but it appears that model id 33344 for vin 3GCNYAEF9MG130524
is not returned by getModels 
i dont even remenber how i got to this

except that it appears that chrome_style_id 413131 (3GCNYAEF9MG130524) and chrome_style_id 413032 (3GCNYAEF9MG130524)
are identical vehicles in terms of the unique indexes in nc.vehicle_configurations

ERROR: duplicate key value violates unique constraint "config_idx"
Detail: Key (model_year, make, model, model_code, trim_level, cab, engine)=(2021, Chevrolet, Silverado 1500, CK10903, Work Truck, reg, 5.3) already exists.

there are no other vehicles that have multiple chrome_style_ids for this array of attributes.

but this wont work
update nc.stg_vehicles
set configuration_id = 1051
where vin = '3GCNYAEF4MG158747'
because, attempting to install into nc.vehicle_configurations generates:
ERROR: null value in column "configuration_id" violates not-null constraint
SQL state: 23502
Detail: Failing row contains (3GCNYAEF4MG158747, 413032, 2021, Chevrolet, Silverado 1500, CK10903, 4WD, reg, Work Truck, 5.3, RED HOT, null, null).

because there is no vehicle_configuration record for 413032

so for today i have spent too much time on this with no resolution, delete the vehicle for now