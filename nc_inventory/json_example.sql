﻿-- https://stackoverflow.com/questions/42222968/create-nested-json-from-sql-query-postgres-9-4
/*
drop table if exists jon.wheel;
drop table if exists jon.car;
drop table if exists jon.person;

CREATE TABLE jon.car
(
  id integer NOT NULL,
  type character varying(30),
  personid integer,
  CONSTRAINT car_pk PRIMARY KEY (id));

CREATE TABLE jon.person
(
  id integer NOT NULL,
  name character varying(30),
  CONSTRAINT person_pk PRIMARY KEY (id));  

CREATE TABLE jon.wheel
(
  id integer NOT NULL,
  whichone character varying(30),
  serialnumber integer,
  carid integer,
  CONSTRAINT wheel_pk PRIMARY KEY (id));  

insert into jon.person(id, name) values 
    (1, 'Johny'), 
    (2, 'Freddy'),
    (3, 'Billy');

insert into jon.car(id, type, personid) values 
    (1, 'Toyota', 1),
    (2, 'Fiat', 2);

insert into jon.wheel(id, whichone, serialnumber, carid) values 
    (1, 'front', '11', 1),
    (2, 'back', '12', 1),
    (3, 'front', '21', 2),
    (4, 'back', '22', 2),
    (5, 'front', '3', 3);  
*/    

	select
		json_build_object(
			'persons', json_agg(
                case when cars is null then
                    json_build_object('person_name', p.name)
                else
                    json_build_object(
                        'person_name', p.name,
                        'cars', cars)
                end
			)
		) persons
	from jon.person p
	left join (
		select 
			personid,
--			json_agg(
				json_build_object(
					'carid', c.id,    
					'type', c.type,
					'comment', 'nice car', -- this is constant
					'wheels', wheels
					) cars --+
--				) cars
		from
			jon.car c
			left join (
				select 
					carid, 
					json_agg(
						json_build_object(
							'which', w.whichone,
							'serial number', w.serialnumber
						)
					) wheels
				from jon.wheel w
				group by 1
			) w on c.id = w.carid
--		group by personid
	) c on p.id = c.personid;    


select row_to_json(z)
from (
with wheels as (
    select 
        carid, 
        json_agg(
            json_build_object(
                'which', w.whichone,
                'serial number', w.serialnumber
            )
        ) wheels
    from jon.wheel w
    group by 1
),
cars as (
    select 
        personid,
        json_agg(
            json_build_object(
                'carid', c.id,    
                'type', c.type,
                'comment', 'nice car', -- this is constant
                'wheels', wheels
                )
            ) cars
    from jon.car c
    left join wheels w on c.id = w.carid
    group by c.personid
)
select
    json_build_object(
        'persons', json_agg(
            json_build_object(
                'person_name', p.name,
                'cars', cars
            )
        )
    ) persons
from jon.person p
left join cars c on p.id = c.personid) z