﻿drop table if exists invoices;
create temp table invoices as
select a.vin, trim(left(raw_invoice, position('GENERAL MOTORS LLC' in raw_invoice)-1)):: citext as description,
  case position('RYDELL' in raw_invoice)
    when 0 then 'dealer trade'
    else 'factory'
  end as source,
  left(substring(raw_invoice from position('MODEL' in raw_invoice) + 78 for 8), 
    position(' ' in substring(raw_invoice from position('MODEL' in raw_invoice) + 78 for 8))) as model_code,
  substring(raw_invoice from position(E'\n' in raw_invoice) + 1 for 3) as color_code,
  trim(substring(raw_invoice from position(E'\n' in raw_invoice) + 6 for 31))::citext as color,
--   substring(raw_invoice from position('INVOICE' in raw_invoice) + 236 for 8)::date as invoice_date,
--   substring(raw_invoice from position('SHIPPED' in raw_invoice) + 8 for 8)::date as shipped_date,
--   substring(raw_invoice from position('EXP I/T' in raw_invoice) + 8 for 8)::date as exp_in_transit_date,
--   substring(raw_invoice from position('INT COM' in raw_invoice) + 8 for 8)::date  as int_com_date,
--   substring(raw_invoice from position('PRC EFF' in raw_invoice) + 8 for 8) ::date as prc_eff_date,
  position('EXP I/T' in raw_invoice)
from (
  select vin, replace(raw_invoice, '&amp;', '&') as raw_invoice
  from gmgl.vehicle_invoices) a
-- -- where a.vin = '5GAEVCKW1KJ243213'
-- left join nc.open_orders b on a.vin = b.vin
where position('EXP I/T' in raw_invoice) = 726
  and a.vin not in ('1G1Y12D70J5102429','1HA3GTCG6JN001883','1HA3GTCG2KN000408','3GCUKREC0JG109879','1GKS2HKJ9JR115883','1GYS4CKJ3JR375697')
-- order by substring(raw_invoice from position('INT COM' in raw_invoice) + 8 for 8) desc
-- order by b.vin

select * from invoices


select *
from gmgl.vehicle_invoices
where vin in (
select a.vin
from nc.open_orders a
left join invoices b on a.vin = b.vin
where a.vin is not null
  and b.vin is null)


-- 2/27/2019  limit it to exp i/t date only

drop table if exists invoices;
create temp table invoices as
select a.vin, trim(left(raw_invoice, position('GENERAL MOTORS LLC' in raw_invoice)-1)):: citext as description,
  case position('RYDELL' in raw_invoice)
    when 0 then 'dealer trade'
    else 'factory'
  end as source,
  left(substring(raw_invoice from position('MODEL' in raw_invoice) + 78 for 8), 
    position(' ' in substring(raw_invoice from position('MODEL' in raw_invoice) + 78 for 8))) as model_code,
  substring(raw_invoice from position(E'\n' in raw_invoice) + 1 for 3) as color_code,
  trim(substring(raw_invoice from position(E'\n' in raw_invoice) + 6 for 31))::citext as color,
  substring(raw_invoice from position('EXP I/T' in raw_invoice) + 8 for 8)::date as exp_in_transit_date
from gmgl.vehicle_invoices a

select a.order_number, a.alloc_group, a.vin, a.dan, a.peg, c.model_year, c.make, c.model, c.model_code, c.trim_level, 
  c.cab, c.box_size, c.drive, c.engine, b.exp_in_transit_date
from nc.open_orders a
left join invoices b on a.vin = b.vin  
join nc.vehicle_configurations c on a.configuration_id = c.configuration_id
where a.category = 'in transit'
  and exp_in_transit_date < current_date
order by b.exp_in_transit_date desc

select category, count(*)
from nc.open_orders a
group by category

select *
from nc.open_orders
where category = 'in transit'

-- 6/13/19, need better info from invoices,
-- 1. clean up the raw invoice, get rid of the amp; before analyzing positions
-- 2. lets start out getting some positions
-- this works, add  the date fields to nc.vehicle_invoices in all_nc_inventory_nightly.sql
drop table if exists invoices;
create temp table invoices as
select a.vin, 
--   position('GENERAL MOTORS LLC' in raw_invoice), 
--   position('RYDELL' in raw_invoice), -- to determine factory vs dealer trade
--   position('MODEL' in raw_invoice),
--   position(E'\n' in raw_invoice),
-- --   trim(left(raw_invoice, position('GENERAL MOTORS LLC' in raw_invoice)-1)):: citext as description,
-- --   case position('RYDELL' in raw_invoice)
-- --     when 0 then 'dealer trade'
-- --     else 'factory'
-- --   end as source,
-- --   left(substring(raw_invoice from position('MODEL' in raw_invoice) + 78 for 8), 
-- --     position(' ' in substring(raw_invoice from position('MODEL' in raw_invoice) + 78 for 8))) as model_code,
-- --   substring(raw_invoice from position(E'\n' in raw_invoice) + 1 for 3) as color_code,
-- --   trim(substring(raw_invoice from position(E'\n' in raw_invoice) + 6 for 31))::citext as color,
-- --   substring(raw_invoice from position('INVOICE' in raw_invoice) + 236 for 8)::date as invoice_date,
-- --   substring(raw_invoice from position('SHIPPED' in raw_invoice) + 8 for 8)::date as shipped_date,
-- --   substring(raw_invoice from position('EXP I/T' in raw_invoice) + 8 for 8)::date as exp_in_transit_date,
-- --   substring(raw_invoice from position('INT COM' in raw_invoice) + 8 for 8)::date  as int_com_date,
-- --   substring(raw_invoice from position('PRC EFF' in raw_invoice) + 8 for 8) ::date as prc_eff_date,
--   position('EXP I/T' in raw_invoice),
--   position('CREDIT FOR INVOICE' in raw_invoice),
  substring(raw_invoice from position('INVOICE' in raw_invoice) + 232 for 8)::date as invoice_date,
  substring(raw_invoice from position('SHIPPED' in raw_invoice) + 8 for 8)::date as shipped_date,
  substring(raw_invoice from position('EXP I/T' in raw_invoice) + 8 for 8)::date as exp_in_transit_date
from (
  select vin, replace(raw_invoice, '&amp;', '&') as raw_invoice
  from gmgl.vehicle_invoices
  where thru_date > current_date) a
where position('CREDIT FOR INVOICE' in raw_invoice) = 0 -- these are weird, i dont remember why, but should be excluded

-- no variation except for rydell, which is fine
select gm,rydell,model,line_feed,expit,invoice,inv_amt,shipped
from (
select 
  position('GENERAL MOTORS LLC' in raw_invoice) as gm,
  position('RYDELL' in raw_invoice) as rydell, -- to determine factory vs dealer trade
  position('MODEL' in raw_invoice) as model,
  position(E'\n' in raw_invoice) as line_feed,
--   trim(left(raw_invoice, position('GENERAL MOTORS LLC' in raw_invoice)-1)):: citext as description,
--   case position('RYDELL' in raw_invoice)
--     when 0 then 'dealer trade'
--     else 'factory'
--   end as source,
--   left(substring(raw_invoice from position('MODEL' in raw_invoice) + 78 for 8), 
--     position(' ' in substring(raw_invoice from position('MODEL' in raw_invoice) + 78 for 8))) as model_code,
--   substring(raw_invoice from position(E'\n' in raw_invoice) + 1 for 3) as color_code,
--   trim(substring(raw_invoice from position(E'\n' in raw_invoice) + 6 for 31))::citext as color,
--   substring(raw_invoice from position('INVOICE' in raw_invoice) + 236 for 8)::date as invoice_date,
--   substring(raw_invoice from position('SHIPPED' in raw_invoice) + 8 for 8)::date as shipped_date,
--   substring(raw_invoice from position('EXP I/T' in raw_invoice) + 8 for 8)::date as exp_in_transit_date,
--   substring(raw_invoice from position('INT COM' in raw_invoice) + 8 for 8)::date  as int_com_date,
--   substring(raw_invoice from position('PRC EFF' in raw_invoice) + 8 for 8) ::date as prc_eff_date,
  position('EXP I/T' in raw_invoice) as expit,
  position('INVOICE' in raw_invoice) as invoice,
  position('INV AMT' in raw_invoice) as inv_amt,
  position('SHIPPED' in raw_invoice) as shipped
from (
  select vin, replace(raw_invoice, '&amp;', '&') as raw_invoice
  from gmgl.vehicle_invoices) a
where position('CREDIT FOR INVOICE' in raw_invoice) = 0 -- these are weird, i dont remember why, but should be excluded
) x group by gm,rydell,model,line_feed,expit,invoice,inv_amt,shipped


