﻿select * 
from sls.deals_by_month
where stock_number in (

select stock_number from nc.vehicle_sales where sale_type = 'ctp'  and delivery_date between '05/01/2019' and '05/31/2019')

select sale_type
from sls.deals_by_month
group by sale_type


select * 
from sls.deals_by_month
where stock_number in (

select stock_number from nc.vehicle_sales where sale_type = 'dealer trade'  and delivery_date between '05/01/2019' and '05/31/2019')

--1683
-- does not include 10 model_year 2017 (3 buick, 2 gmc, 5 cadillac)
select a.make, a.model,
  c.alloc_group

--   count(alloc_group) filter (where a.year_month = 201806) as Jun
-- select distinct c.alloc_group
select distinct a.make, a.model, c.cab, c.alloc_group
from sls.deals_by_month a
join nc.vehicles b on a.vin = b.vin
join nc.vehicle_configurations c on b.configuration_id = c.configuration_id
where a.year_month between 201806 and 209105
  and a.sale_type <> 'Wholesale'
  and a.vehicle_type = 'new'
  and a.make in ('buick','gmc','cadillac','chevrolet')
  and a.model ~* '1500|2500|3500'
order by alloc_group  
-- group by a.make, alloc_group
-- order by a.make, alloc_group

select make, model, cab, alloc_Group,
  case 
    when alloc_group = 'CCRUHD' and model = 'SILVERADO 2500HD' then '2500 Crew'
    when alloc_group = 'CCRUHD' and model = 'Silverado 3500HD' then '3500 Crew'
    when alloc_group = 'CCRULD' then '1500 Crew'
    when alloc_group = 'CDBLHD' and model = 'SILVERADO 2500HD' then '2500 Double'
    when alloc_group ~* 'CDBLLD|GDBLLD|GLDDBL' then '1500 Double'
    when alloc_group ~* 'CLDCRW|GLDCRW' then '1500 Crew'
    when alloc_group = 'CLDDBL' then '1500 Double'
    when alloc_group = 'CREGHD' and model = 'SILVERADO 2500HD' then '2500 Reg'
    when alloc_group ~* 'CREGLD|GREGLD' then '1500 Reg'
    when alloc_group = 'GCRUHD' and model = 'SIERRA 2500HD' then '2500 Crew'
    when alloc_group = 'GCRUHD' and model = 'SIERRA 3500HD' then '3500 Crew'
    when alloc_group = 'GCRULD' then '1500 Crew'
    when alloc_group = 'GDBLHD' and model = 'SIERRA 2500HD' then '2500 Double'
    else alloc_group
  end as forecast
from (
  select a.make, a.model, c.cab, c.alloc_group
  from sls.deals_by_month a
  join nc.vehicles b on a.vin = b.vin
  join nc.vehicle_configurations c on b.configuration_id = c.configuration_id
  where a.year_month between 201806 and 209105
    and a.sale_type <> 'Wholesale'
    and a.vehicle_type = 'new'
    and a.make in ('buick','gmc','cadillac','chevrolet')) x
order by make, model, cab, alloc_group    


select make, forecast,
  count(make) filter (where year_month = 201806) as Jun,
  count(make) filter (where year_month = 201807) as Jul,
  count(make) filter (where year_month = 201808) as Aug,
  count(make) filter (where year_month = 201809) as Sep,
  count(make) filter (where year_month = 201810) as Oct,
  count(make) filter (where year_month = 201811) as Nov,
  count(make) filter (where year_month = 201812) as "Dec",
  count(make) filter (where year_month = 201901) as Jan,
  count(make) filter (where year_month = 201902) as Feb,
  count(make) filter (where year_month = 201903) as Mar,
  count(make) filter (where year_month = 201904) as Apr,
  count(make) filter (where year_month = 201905) as May,
  count(make) as Total
from (
  select a.year_month, a.make, 
    case 
      when alloc_group = 'CCRUHD' and a.model = 'SILVERADO 2500HD' then '2500 Crew'
      when alloc_group = 'CCRUHD' and a.model = 'Silverado 3500HD' then '3500 Crew'
      when alloc_group = 'CCRULD' then '1500 Crew'
      when alloc_group = 'CDBLHD' and a.model = 'SILVERADO 2500HD' then '2500 Double'
      when alloc_group ~* 'CDBLLD|GDBLLD|GLDDBL' then '1500 Double'
      when alloc_group ~* 'CLDCRW|GLDCRW' then '1500 Crew'
      when alloc_group = 'CLDDBL' then '1500 Double'
      when alloc_group = 'CREGHD' and a.model = 'SILVERADO 2500HD' then '2500 Reg'
      when alloc_group ~* 'CREGLD|GREGLD' then '1500 Reg'
      when alloc_group = 'GCRUHD' and a.model = 'SIERRA 2500HD' then '2500 Crew'
      when alloc_group = 'GCRUHD' and a.model = 'SIERRA 3500HD' then '3500 Crew'
      when alloc_group = 'GCRULD' then '1500 Crew'
      when alloc_group = 'GDBLHD' and a.model = 'SIERRA 2500HD' then '2500 Double'
      else d.model
    end as forecast
  from sls.deals_by_month a
  join nc.vehicles b on a.vin = b.vin
  join nc.vehicle_configurations c on b.configuration_id = c.configuration_id
  left join nc.forecast_model_configurations  d on c.configuration_id = d.configuration_id
  where a.year_month between 201806 and 209105
    and a.sale_type <> 'Wholesale'
    and a.vehicle_type = 'new'
    and a.make in ('buick','gmc','cadillac','chevrolet')
    and a.model not in ('LACROSSE','ATS COUPE','ATS SEDAN','CRUZE','CTS','XTS','bolt ev','impala')) X
where forecast is not null    
group by make, forecast    
order by make, forecast  


select * from nc.forecast_model_configurations  
