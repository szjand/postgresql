﻿select *
from ads.ext_edw_employee_dim 
limit 100

select employeenumber, name, distcode, distribution, employeekeyfromdate, 
employeekeythrudate, termdate, left(rowchangereason, 15),
pydeptcode, pydept, currentrow
from ads.ext_edw_employee_dim 
where termdate > '12/31/2017' 
order by employeenumber, employeekey

select *
from arkona.ext_pyactgr
order by dist_code

select employeenumber, name, distcode, distribution, employeekeyfromdate, 
employeekeythrudate, termdate, left(rowchangereason, 30),
pydeptcode, pydept, currentrow
from ads.ext_edw_employee_dim 
where employeekeyfromdate > '02/28/2018'
order by employeenumber, employeekey

select employeenumber, name, distcode, distribution, employeekeyfromdate, 
  employeekeythrudate, left(rowchangereason, 15),
  pydeptcode, pydept, currentrow, termdate
from ads.ext_edw_employee_dim 
where employeenumber in (
  select employeenumber
  from ads.ext_edw_employee_dim 
  where employeekeyfromdate > '02/28/2018')
order by employeenumber, employeekey



select employeenumber, name, distcode, distribution, 
  min(employeekeyfromdate) as from_date, max(employeekeythrudate) as thru_date
from ads.ext_edw_employee_dim 
group by employeenumber, name, distcode, distribution
order by employeenumber, employeekey


-- this looks like my best shot at folks with multiple distcodes since 01/01/2017
select employeenumber, name, distcode, distribution, employeekeyfromdate, employeekeythrudate, termdate
from ads.ext_edw_employee_dim 
where employeenumber in (
  select employeenumber
  from (
    select employeenumber, distcode
    from ads.ext_edw_employee_dim 
    where distcode <> 'A-Z'
    and termdate > '12/31/2016'
      and employeekeythrudate > '12/31/2016'
    group by employeenumber, distcode) a
  group by employeenumber
  having count(*) > 1)
order by employeenumber, employeekey  

