﻿-- -- SELECT array_agg(attname)
-- -- FROM  pg_attribute
-- -- WHERE attrelid = 'arkona.ext_pymast'::regclass  -- table name, optionally schema-qualified
-- --   AND attnum > 0
-- --   AND NOT attisdropped 
-- 
-- 
-- select arkona.update_xfm_pymast()
-- 
-- select count(*) from arkona.xfm_pymast -- 1508
-- 
-- select count(*) from arkona.ext_pymast -- 1508
-- 
-- select *
-- from arkona.xfm_pymast
-- 
-- truncate arkona.xfm_pymast
-- 
-- select * from arkona.ext_pymast where employee_name like '%jon%'
-- 
-- update arkona.ext_pymast set employee_first_name = 'JOHN' where pymast_employee_number = '16425'
-- 
-- select * from arkona.xfm_pymast where pymast_employee_number = '16425'
-- 
-- create or replace function arkona.update_xfm_pymast()
--   returns void as
-- $BODY$
-- -- new rows
-- insert into arkona.xfm_pymast (
--   pymast_company_number,pymast_employee_number,active_code,department_code,
--   department_ext,security_group,state_code_unemployment,county_code_unemployment,
--   city_code_unemployment,employee_name,employee_first_name,employee_middle_name,
--   employee_last_name,record_key,address_1,address_2,city_name,state_code_address_,
--   zip_code,tel_area_code,telephone_number,cell_phone_number,
--   cell_phone_provider_code,email_address,soc_sec_number,drivers_license_,
--   birth_date,hire_date,org_hire_date,last_raise_date,termination_date,sex,
--   full_time_part_time,marital_status,exempt_from_fica,exempt_from_fed,
--   federal_exemptions,federal_add_on_amount,federal_add_on_percent,benfit_type,
--   payroll_class,eic_eligable,eic_class,pay_period,base_salary,base_hrly_rate,
--   alt_salary,alt_hrly_rate,distrib_code,elig_for_retire,retirement_,
--   retirement_fixed_amount,retirement_annual_max,wkman_comp_code,cum_wage_withhld,
--   state_tax_tab_code,county_tax_tab_code,city_tax_tab_code,last_update,last_check,
--   deceased_flag,exempt_from_state,state_exemptions,state_tax_add_on_amount,
--   state_add_on_percent,state_fixed_exemptions,exempt_from_county,
--   county_tax_add_on_amount,county_add_on_percent,exempt_from_local,
--   local_tax_add_on_amount,local_add_on_percent,bank_info,deferred_vacation,
--   deferred_holidays,deffered_sickleave,deferred_vac_year,deferred_hol_year,
--   deffered_sic_year,vac_hrly_rate,hol_hrly_rate,sck_hrly_rate,a_r_customer_,
--   driver_lic_state_code,driver_lic_expire_date,federal_filing_status,
--   state_filing_status,county_filing_status,city_filing_status,
--   state_filing_status_2,county_filing_status_2,city_filing_status_2,
--   state_tax_tab_code2,county_tax_tab_code2,city_tax_tab_code2,sdi_state,
--   wrkr_comp_state,reporting_code_1,reporting_code_2,eeoc,overtime_exempt,
--   high_compensate_flag,k_eligibility_date01e,k_matching_date01m,
--   temp_add_1,temp_add_2,temp_add_3,temp_add_4,temp_add_5,temp_add_6,
--   temp_add_7,temp_add_9,temp_add10,union_anniversary_date,
--   row_from_date, current_row, hash)
-- select pymast_company_number,pymast_employee_number,active_code,department_code,
--   department_ext,security_group,state_code_unemployment,county_code_unemployment,
--   city_code_unemployment,employee_name,employee_first_name,employee_middle_name,
--   employee_last_name,record_key,address_1,address_2,city_name,state_code_address_,
--   zip_code,tel_area_code,telephone_number,cell_phone_number,
--   cell_phone_provider_code,email_address,soc_sec_number,drivers_license_,
--   birth_date,hire_date,org_hire_date,last_raise_date,termination_date,sex,
--   full_time_part_time,marital_status,exempt_from_fica,exempt_from_fed,
--   federal_exemptions,federal_add_on_amount,federal_add_on_percent,benfit_type,
--   payroll_class,eic_eligable,eic_class,pay_period,base_salary,base_hrly_rate,
--   alt_salary,alt_hrly_rate,distrib_code,elig_for_retire,retirement_,
--   retirement_fixed_amount,retirement_annual_max,wkman_comp_code,cum_wage_withhld,
--   state_tax_tab_code,county_tax_tab_code,city_tax_tab_code,last_update,last_check,
--   deceased_flag,exempt_from_state,state_exemptions,state_tax_add_on_amount,
--   state_add_on_percent,state_fixed_exemptions,exempt_from_county,
--   county_tax_add_on_amount,county_add_on_percent,exempt_from_local,
--   local_tax_add_on_amount,local_add_on_percent,bank_info,deferred_vacation,
--   deferred_holidays,deffered_sickleave,deferred_vac_year,deferred_hol_year,
--   deffered_sic_year,vac_hrly_rate,hol_hrly_rate,sck_hrly_rate,a_r_customer_,
--   driver_lic_state_code,driver_lic_expire_date,federal_filing_status,
--   state_filing_status,county_filing_status,city_filing_status,
--   state_filing_status_2,county_filing_status_2,city_filing_status_2,
--   state_tax_tab_code2,county_tax_tab_code2,city_tax_tab_code2,sdi_state,
--   wrkr_comp_state,reporting_code_1,reporting_code_2,eeoc,overtime_exempt,
--   high_compensate_flag,k_eligibility_date01e,k_matching_date01m,
--   temp_add_1,temp_add_2,temp_add_3,temp_add_4,temp_add_5,temp_add_6,
--   temp_add_7,temp_add_9,temp_add10,union_anniversary_date,
--   -- skip pymast_key: serial
--   current_date as row_from_date,
--   -- skip row_thru_date: default value
--   true current_row,
--     (
--       select md5(z::text) as hash
--       from (
--         select pymast_company_number,pymast_employee_number,active_code,department_code,
--           department_ext,security_group,state_code_unemployment,county_code_unemployment,
--           city_code_unemployment,employee_name,employee_first_name,employee_middle_name,
--           employee_last_name,record_key,address_1,address_2,city_name,state_code_address_,
--           zip_code,tel_area_code,telephone_number,cell_phone_number,
--           cell_phone_provider_code,email_address,soc_sec_number,drivers_license_,
--           birth_date,hire_date,org_hire_date,last_raise_date,termination_date,sex,
--           full_time_part_time,marital_status,exempt_from_fica,exempt_from_fed,
--           federal_exemptions,federal_add_on_amount,federal_add_on_percent,benfit_type,
--           payroll_class,eic_eligable,eic_class,pay_period,base_salary,base_hrly_rate,
--           alt_salary,alt_hrly_rate,distrib_code,elig_for_retire,retirement_,
--           retirement_fixed_amount,retirement_annual_max,wkman_comp_code,cum_wage_withhld,
--           state_tax_tab_code,county_tax_tab_code,city_tax_tab_code,last_update,last_check,
--           deceased_flag,exempt_from_state,state_exemptions,state_tax_add_on_amount,
--           state_add_on_percent,state_fixed_exemptions,exempt_from_county,
--           county_tax_add_on_amount,county_add_on_percent,exempt_from_local,
--           local_tax_add_on_amount,local_add_on_percent,bank_info,deferred_vacation,
--           deferred_holidays,deffered_sickleave,deferred_vac_year,deferred_hol_year,
--           deffered_sic_year,vac_hrly_rate,hol_hrly_rate,sck_hrly_rate,a_r_customer_,
--           driver_lic_state_code,driver_lic_expire_date,federal_filing_status,
--           state_filing_status,county_filing_status,city_filing_status,
--           state_filing_status_2,county_filing_status_2,city_filing_status_2,
--           state_tax_tab_code2,county_tax_tab_code2,city_tax_tab_code2,sdi_state,
--           wrkr_comp_state,reporting_code_1,reporting_code_2,eeoc,overtime_exempt,
--           high_compensate_flag,k_eligibility_date01e,k_matching_date01m,
--           temp_add_1,temp_add_2,temp_add_3,temp_add_4,temp_add_5,temp_add_6,
--           temp_add_7,temp_add_9,temp_add10,union_anniversary_date      
--         from arkona.ext_pymast
--         where pymast_company_number = a.pymast_company_number
--           and pymast_employee_number = a.pymast_employee_number) z)
-- from arkona.ext_pymast a
-- where not exists (
--   select 1
--   from arkona.xfm_pymast
--   where pymast_company_number = a.pymast_company_number
--     and pymast_employee_number = a.pymast_employee_number);        
-- 
-- -- changed rows
-- truncate arkona.xfm_pymast_changed_rows;
-- insert into arkona.xfm_pymast_changed_rows
-- select a.pymast_company_number, a.pymast_employee_number
-- from (
--   select pymast_company_number, pymast_employee_number,
--     (
--       select md5(z::text) as hash
--       from (
--         select pymast_company_number,pymast_employee_number,active_code,department_code,
--           department_ext,security_group,state_code_unemployment,county_code_unemployment,
--           city_code_unemployment,employee_name,employee_first_name,employee_middle_name,
--           employee_last_name,record_key,address_1,address_2,city_name,state_code_address_,
--           zip_code,tel_area_code,telephone_number,cell_phone_number,
--           cell_phone_provider_code,email_address,soc_sec_number,drivers_license_,
--           birth_date,hire_date,org_hire_date,last_raise_date,termination_date,sex,
--           full_time_part_time,marital_status,exempt_from_fica,exempt_from_fed,
--           federal_exemptions,federal_add_on_amount,federal_add_on_percent,benfit_type,
--           payroll_class,eic_eligable,eic_class,pay_period,base_salary,base_hrly_rate,
--           alt_salary,alt_hrly_rate,distrib_code,elig_for_retire,retirement_,
--           retirement_fixed_amount,retirement_annual_max,wkman_comp_code,cum_wage_withhld,
--           state_tax_tab_code,county_tax_tab_code,city_tax_tab_code,last_update,last_check,
--           deceased_flag,exempt_from_state,state_exemptions,state_tax_add_on_amount,
--           state_add_on_percent,state_fixed_exemptions,exempt_from_county,
--           county_tax_add_on_amount,county_add_on_percent,exempt_from_local,
--           local_tax_add_on_amount,local_add_on_percent,bank_info,deferred_vacation,
--           deferred_holidays,deffered_sickleave,deferred_vac_year,deferred_hol_year,
--           deffered_sic_year,vac_hrly_rate,hol_hrly_rate,sck_hrly_rate,a_r_customer_,
--           driver_lic_state_code,driver_lic_expire_date,federal_filing_status,
--           state_filing_status,county_filing_status,city_filing_status,
--           state_filing_status_2,county_filing_status_2,city_filing_status_2,
--           state_tax_tab_code2,county_tax_tab_code2,city_tax_tab_code2,sdi_state,
--           wrkr_comp_state,reporting_code_1,reporting_code_2,eeoc,overtime_exempt,
--           high_compensate_flag,k_eligibility_date01e,k_matching_date01m,
--           temp_add_1,temp_add_2,temp_add_3,temp_add_4,temp_add_5,temp_add_6,
--           temp_add_7,temp_add_9,temp_add10,union_anniversary_date 
--         from arkona.ext_pymast
--         where pymast_company_number = aa.pymast_company_number
--           and pymast_employee_number = aa.pymast_employee_number) z)
--   from arkona.ext_pymast aa) a
-- inner join arkona.xfm_pymast b on a.pymast_company_number = b.pymast_company_number
--   and a.pymast_employee_number = b.pymast_employee_number
--   and a.hash <> b.hash
--   and b.current_row = true;     
-- 
-- -- update current version of changed row
-- update arkona.xfm_pymast x
-- set current_row = false,
--     row_thru_date = current_Date - 1
-- from arkona.xfm_pymast_changed_rows z
-- where x.pymast_company_number = z.pymast_company_number
--   and x.pymast_employee_number = z.pymast_employee_number
--   and current_row = true;  
-- 
-- -- add new row for each changed row  
-- insert into arkona.xfm_pymast (
--   pymast_company_number,pymast_employee_number,active_code,department_code,
--   department_ext,security_group,state_code_unemployment,county_code_unemployment,
--   city_code_unemployment,employee_name,employee_first_name,employee_middle_name,
--   employee_last_name,record_key,address_1,address_2,city_name,state_code_address_,
--   zip_code,tel_area_code,telephone_number,cell_phone_number,
--   cell_phone_provider_code,email_address,soc_sec_number,drivers_license_,
--   birth_date,hire_date,org_hire_date,last_raise_date,termination_date,sex,
--   full_time_part_time,marital_status,exempt_from_fica,exempt_from_fed,
--   federal_exemptions,federal_add_on_amount,federal_add_on_percent,benfit_type,
--   payroll_class,eic_eligable,eic_class,pay_period,base_salary,base_hrly_rate,
--   alt_salary,alt_hrly_rate,distrib_code,elig_for_retire,retirement_,
--   retirement_fixed_amount,retirement_annual_max,wkman_comp_code,cum_wage_withhld,
--   state_tax_tab_code,county_tax_tab_code,city_tax_tab_code,last_update,last_check,
--   deceased_flag,exempt_from_state,state_exemptions,state_tax_add_on_amount,
--   state_add_on_percent,state_fixed_exemptions,exempt_from_county,
--   county_tax_add_on_amount,county_add_on_percent,exempt_from_local,
--   local_tax_add_on_amount,local_add_on_percent,bank_info,deferred_vacation,
--   deferred_holidays,deffered_sickleave,deferred_vac_year,deferred_hol_year,
--   deffered_sic_year,vac_hrly_rate,hol_hrly_rate,sck_hrly_rate,a_r_customer_,
--   driver_lic_state_code,driver_lic_expire_date,federal_filing_status,
--   state_filing_status,county_filing_status,city_filing_status,
--   state_filing_status_2,county_filing_status_2,city_filing_status_2,
--   state_tax_tab_code2,county_tax_tab_code2,city_tax_tab_code2,sdi_state,
--   wrkr_comp_state,reporting_code_1,reporting_code_2,eeoc,overtime_exempt,
--   high_compensate_flag,k_eligibility_date01e,k_matching_date01m,
--   temp_add_1,temp_add_2,temp_add_3,temp_add_4,temp_add_5,temp_add_6,
--   temp_add_7,temp_add_9,temp_add10,union_anniversary_date,
--   row_from_date, current_row, hash)
-- select pymast_company_number,pymast_employee_number,active_code,department_code,
--   department_ext,security_group,state_code_unemployment,county_code_unemployment,
--   city_code_unemployment,employee_name,employee_first_name,employee_middle_name,
--   employee_last_name,record_key,address_1,address_2,city_name,state_code_address_,
--   zip_code,tel_area_code,telephone_number,cell_phone_number,
--   cell_phone_provider_code,email_address,soc_sec_number,drivers_license_,
--   birth_date,hire_date,org_hire_date,last_raise_date,termination_date,sex,
--   full_time_part_time,marital_status,exempt_from_fica,exempt_from_fed,
--   federal_exemptions,federal_add_on_amount,federal_add_on_percent,benfit_type,
--   payroll_class,eic_eligable,eic_class,pay_period,base_salary,base_hrly_rate,
--   alt_salary,alt_hrly_rate,distrib_code,elig_for_retire,retirement_,
--   retirement_fixed_amount,retirement_annual_max,wkman_comp_code,cum_wage_withhld,
--   state_tax_tab_code,county_tax_tab_code,city_tax_tab_code,last_update,last_check,
--   deceased_flag,exempt_from_state,state_exemptions,state_tax_add_on_amount,
--   state_add_on_percent,state_fixed_exemptions,exempt_from_county,
--   county_tax_add_on_amount,county_add_on_percent,exempt_from_local,
--   local_tax_add_on_amount,local_add_on_percent,bank_info,deferred_vacation,
--   deferred_holidays,deffered_sickleave,deferred_vac_year,deferred_hol_year,
--   deffered_sic_year,vac_hrly_rate,hol_hrly_rate,sck_hrly_rate,a_r_customer_,
--   driver_lic_state_code,driver_lic_expire_date,federal_filing_status,
--   state_filing_status,county_filing_status,city_filing_status,
--   state_filing_status_2,county_filing_status_2,city_filing_status_2,
--   state_tax_tab_code2,county_tax_tab_code2,city_tax_tab_code2,sdi_state,
--   wrkr_comp_state,reporting_code_1,reporting_code_2,eeoc,overtime_exempt,
--   high_compensate_flag,k_eligibility_date01e,k_matching_date01m,
--   temp_add_1,temp_add_2,temp_add_3,temp_add_4,temp_add_5,temp_add_6,
--   temp_add_7,temp_add_9,temp_add10,union_anniversary_date,
--   -- skip pymast_key: serial
--   current_date as row_from_date,
--   -- skip row_thru_date: default value
--   true current_row,
--     (
--       select md5(z::text) as hash
--       from (
--         select pymast_company_number,pymast_employee_number,active_code,department_code,
--           department_ext,security_group,state_code_unemployment,county_code_unemployment,
--           city_code_unemployment,employee_name,employee_first_name,employee_middle_name,
--           employee_last_name,record_key,address_1,address_2,city_name,state_code_address_,
--           zip_code,tel_area_code,telephone_number,cell_phone_number,
--           cell_phone_provider_code,email_address,soc_sec_number,drivers_license_,
--           birth_date,hire_date,org_hire_date,last_raise_date,termination_date,sex,
--           full_time_part_time,marital_status,exempt_from_fica,exempt_from_fed,
--           federal_exemptions,federal_add_on_amount,federal_add_on_percent,benfit_type,
--           payroll_class,eic_eligable,eic_class,pay_period,base_salary,base_hrly_rate,
--           alt_salary,alt_hrly_rate,distrib_code,elig_for_retire,retirement_,
--           retirement_fixed_amount,retirement_annual_max,wkman_comp_code,cum_wage_withhld,
--           state_tax_tab_code,county_tax_tab_code,city_tax_tab_code,last_update,last_check,
--           deceased_flag,exempt_from_state,state_exemptions,state_tax_add_on_amount,
--           state_add_on_percent,state_fixed_exemptions,exempt_from_county,
--           county_tax_add_on_amount,county_add_on_percent,exempt_from_local,
--           local_tax_add_on_amount,local_add_on_percent,bank_info,deferred_vacation,
--           deferred_holidays,deffered_sickleave,deferred_vac_year,deferred_hol_year,
--           deffered_sic_year,vac_hrly_rate,hol_hrly_rate,sck_hrly_rate,a_r_customer_,
--           driver_lic_state_code,driver_lic_expire_date,federal_filing_status,
--           state_filing_status,county_filing_status,city_filing_status,
--           state_filing_status_2,county_filing_status_2,city_filing_status_2,
--           state_tax_tab_code2,county_tax_tab_code2,city_tax_tab_code2,sdi_state,
--           wrkr_comp_state,reporting_code_1,reporting_code_2,eeoc,overtime_exempt,
--           high_compensate_flag,k_eligibility_date01e,k_matching_date01m,
--           temp_add_1,temp_add_2,temp_add_3,temp_add_4,temp_add_5,temp_add_6,
--           temp_add_7,temp_add_9,temp_add10,union_anniversary_date      
--         from arkona.ext_pymast
--         where pymast_company_number = x.pymast_company_number
--           and pymast_employee_number = x.pymast_employee_number) z)
-- from (
--   select a.*
--   from arkona.ext_pymast a
--   inner join arkona.xfm_pymast_changed_rows b on a.pymast_company_number = b.pymast_company_number
--     and a.pymast_employee_number = b.pymast_employee_number) x;
-- 
-- $BODY$
-- language sql;    


-- SELECT array_agg(attname)
-- FROM  pg_attribute
-- WHERE attrelid = 'arkona.ext_pyactgr'::regclass  -- table name, optionally schema-qualified
--   AND attnum > 0
--   AND NOT attisdropped 
-- 
-- select * from arkona.ext_pyactgr

select arkona.xfm_pyactgr()

delete from arkona.xfm_pyactgr

select * from arkona.xfm_pyactgr_changed_rows

create or replace function arkona.xfm_pyactgr()
  returns void as
$BODY$
-- new rows
insert into arkona.xfm_pyactgr (
  company_number,dist_code,seq_number,description,dist_co_,gross_dist,
  gross_expense_act_,overtime_act_,vacation_expense_act_,holiday_expense_act_,
  sick_leave_expense_act_,retire_expense_act_,emplr_fica_expense,
  emplr_med_expense,federal_un_emp_act_,state_unemp_act_,state_comp_act_,
  state_sdi_act_,emplr_contributions,
  row_from_date, current_row, hash)
select company_number,dist_code,seq_number,description,dist_co_,gross_dist,
  gross_expense_act_,overtime_act_,vacation_expense_act_,holiday_expense_act_,
  sick_leave_expense_act_,retire_expense_act_,emplr_fica_expense,
  emplr_med_expense,federal_un_emp_act_,state_unemp_act_,state_comp_act_,
  state_sdi_act_,emplr_contributions,
      -- skip pyactgr_key: serial
  current_date as row_from_date,
  -- skip row_thru_date: default value
  true as current_row,
    (
      select md5(z::text) as hash
      from (
        select company_number,dist_code,seq_number,description,dist_co_,gross_dist,
          gross_expense_act_,overtime_act_,vacation_expense_act_,holiday_expense_act_,
          sick_leave_expense_act_,retire_expense_act_,emplr_fica_expense,
          emplr_med_expense,federal_un_emp_act_,state_unemp_act_,state_comp_act_,
          state_sdi_act_,emplr_contributions
        from arkona.ext_pyactgr
        where company_number = a.company_number
          and dist_code = a.dist_code
          and seq_number = a.seq_number) z)
from arkona.ext_pyactgr a
where not exists (
  select 1
  from arkona.xfm_pyactgr
  where company_number = a.company_number
    and dist_code = a.dist_code
    and seq_number = seq_number);

-- changed rows
truncate arkona.xfm_pyactgr_changed_rows;
insert into arkona.xfm_pyactgr_changed_rows
select a.company_number, a.dist_code, a.seq_number
from (
  select company_number, dist_code, seq_number, 
    (
      select md5(z::text) as hash
      from (
        select company_number,dist_code,seq_number,description,dist_co_,gross_dist,
          gross_expense_act_,overtime_act_,vacation_expense_act_,holiday_expense_act_,
          sick_leave_expense_act_,retire_expense_act_,emplr_fica_expense,
          emplr_med_expense,federal_un_emp_act_,state_unemp_act_,state_comp_act_,
          state_sdi_act_,emplr_contributions
        from arkona.ext_pyactgr
        where company_number = aa.company_number
          and dist_code = aa.dist_code
          and seq_number = aa.seq_number) z)
  from arkona.ext_pyactgr aa) a
inner join arkona.xfm_pyactgr b on a.company_number = b.company_number
  and a.dist_code = b.dist_code
  and a.seq_number = b.seq_number
  and a.hash <> b.hash
  and b.current_row = true;

-- update current version of changed row
update arkona.xfm_pyactgr x
set current_row = false,
    row_thru_date = current_date - 1
from arkona.xfm_pyactgr_changed_rows z
where x.company_number = z.company_number
  and x.dist_code = x.dist_code
  and x.seq_number = z.seq_number
  and x.current_row = true;

-- add new row for each changed row     
insert into arkona.xfm_pyactgr (
  company_number,dist_code,seq_number,description,dist_co_,gross_dist,
  gross_expense_act_,overtime_act_,vacation_expense_act_,holiday_expense_act_,
  sick_leave_expense_act_,retire_expense_act_,emplr_fica_expense,
  emplr_med_expense,federal_un_emp_act_,state_unemp_act_,state_comp_act_,
  state_sdi_act_,emplr_contributions,
  row_from_date, current_row, hash)
select company_number,dist_code,seq_number,description,dist_co_,gross_dist,
  gross_expense_act_,overtime_act_,vacation_expense_act_,holiday_expense_act_,
  sick_leave_expense_act_,retire_expense_act_,emplr_fica_expense,
  emplr_med_expense,federal_un_emp_act_,state_unemp_act_,state_comp_act_,
  state_sdi_act_,emplr_contributions,
      -- skip pyactgr_key: serial
  current_date as row_from_date,
  -- skip row_thru_date: default value
  true as current_row,
    (
      select md5(z::text) as hash
      from (
        select company_number,dist_code,seq_number,description,dist_co_,gross_dist,
          gross_expense_act_,overtime_act_,vacation_expense_act_,holiday_expense_act_,
          sick_leave_expense_act_,retire_expense_act_,emplr_fica_expense,
          emplr_med_expense,federal_un_emp_act_,state_unemp_act_,state_comp_act_,
          state_sdi_act_,emplr_contributions
        from arkona.ext_pyactgr
        where company_number = x.company_number
          and dist_code = x.dist_code
          and seq_number = x.seq_number) z)
from (
  select a.*
  from arkona.ext_pyactgr a
  inner join arkona.xfm_pyactgr_changed_rows b on a.company_number = b.company_number
    and a.dist_code = b.dist_code
    and a.seq_number = b.seq_number) x;

$BODY$
language sql;              
























  























            


























    