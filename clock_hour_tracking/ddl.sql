﻿CREATE TABLE arkona.xfm_pymast
(
  pymast_company_number citext, -- YMCO# : COMPANY NUMBER
  pymast_employee_number citext, -- YMEMPN : EMPLOYEE NUMBER
  active_code citext, -- YMACTV : ACTIVE CODE
  department_code citext, -- YMDEPT : DEPARTMENT CODE
  department_ext integer, -- YMEXT : DEPARTMENT EXT
  security_group integer, -- YMSECG : SECURITY GROUP
  state_code_unemployment citext, -- YMSTCD : STATE CODE - UNEMPLOYMENT
  county_code_unemployment citext, -- YMCNCD : COUNTY CODE - UNEMPLOYMENT
  city_code_unemployment citext, -- YMMUCD : CITY CODE - UNEMPLOYMENT
  employee_name citext, -- YMNAME : EMPLOYEE NAME
  employee_first_name citext, -- YMFNAM : EMPLOYEE FIRST NAME
  employee_middle_name citext, -- YMMNAM : EMPLOYEE MIDDLE NAME
  employee_last_name citext, -- YMLNAM : EMPLOYEE LAST NAME
  record_key integer, -- YMNKEY : Record Key
  address_1 citext, -- YMADD1 : ADDRESS LINE 1
  address_2 citext, -- YMADD2 : ADDRESS LINE 2
  city_name citext, -- YMCITY : CITY NAME
  state_code_address_ citext, -- YMSTAT : STATE CODE (ADDRESS)
  zip_code integer, -- YMZIP : ZIP CODE
  tel_area_code integer, -- YMAREA : TEL AREA CODE
  telephone_number integer, -- YMPHON : TELEPHONE NUMBER
  cell_phone_number bigint, -- YMCELPHN : CELL PHONE NUMBER
  cell_phone_provider_code citext, -- YMCELPRV : CELL PHONE PROVIDER CODE
  email_address citext, -- YMEMAIL : EMAIL ADDRESS
  soc_sec_number integer, -- YMSS# : SOC SEC NUMBER
  drivers_license_ citext, -- YMDRIV : DRIVERS LICENSE#
  birth_date integer, -- YMBDTE : BIRTH DATE
  hire_date integer, -- YMHDTE : HIRE DATE
  org_hire_date integer, -- YMHDTO : ORG HIRE DATE
  last_raise_date integer, -- YMRDTE : LAST RAISE DATE
  termination_date integer, -- YMTDTE : TERMINATION DATE
  sex citext, -- YMSEX : SEX
  full_time_part_time citext, -- YMFP : Full Time/Part Time
  marital_status citext, -- YMMARI : MARITAL STATUS
  exempt_from_fica citext, -- YMEXSS : EXEMPT FROM FICA
  exempt_from_fed citext, -- YMEXFD : EXEMPT FROM FED
  federal_exemptions integer, -- YMFDEX : FEDERAL EXEMPTIONS
  federal_add_on_amount numeric(7,2), -- YMFDAD : FEDERAL ADD ON AMOUNT
  federal_add_on_percent numeric(3,3), -- YMFDAP : FEDERAL ADD ON PERCENT
  benfit_type citext, -- YMVTYP : BENFIT TYPE
  payroll_class citext, -- YMCLAS : PAYROLL CLASS
  eic_eligable citext, -- YMEEIC : EIC ELIGABLE
  eic_class citext, -- YMECLS : EIC CLASS
  pay_period citext, -- YMPPER : PAY PERIOD
  base_salary numeric(9,2), -- YMSALY : BASE SALARY
  base_hrly_rate numeric(9,2), -- YMRATE : BASE HRLY RATE
  alt_salary numeric(9,2), -- YMSALA : ALT  SALARY
  alt_hrly_rate numeric(9,2), -- YMRATA : ALT  HRLY RATE
  distrib_code citext, -- YMDIST : DISTRIB CODE
  elig_for_retire citext, -- YMRETE : ELIG FOR RETIRE
  retirement_ numeric(4,4), -- YMRETP : RETIREMENT %
  retirement_fixed_amount numeric(7,2), -- YMRFIX : RETIREMENT FIXED AMOUNT
  retirement_annual_max numeric(9,2), -- YMRAMX : RETIREMENT ANNUAL MAX
  wkman_comp_code integer, -- YMWKCD : WKMAN COMP CODE
  cum_wage_withhld citext, -- YMCWFL : CUM WAGE WITHHLD
  state_tax_tab_code citext, -- YMSTTX : STATE TAX TAB CODE
  county_tax_tab_code citext, -- YMCNTX : COUNTY TAX TAB CODE
  city_tax_tab_code citext, -- YMMUTX : CITY TAX TAB CODE
  last_update integer, -- YMLSUP : LAST UPDATE
  last_check integer, -- YMLSCK : LAST CHECK
  deceased_flag citext, -- YMDECE : DECEASED FLAG
  exempt_from_state citext, -- YMEXST : EXEMPT FROM STATE
  state_exemptions integer, -- YMSTEX : STATE EXEMPTIONS
  state_tax_add_on_amount numeric(7,2), -- YMSTAD : STATE TAX ADD ON AMOUNT
  state_add_on_percent numeric(3,3), -- YMSTAP : STATE ADD ON PERCENT
  state_fixed_exemptions numeric(7,2), -- YMSTFX : STATE FIXED EXEMPTIONS
  exempt_from_county citext, -- YMEXCN : EXEMPT FROM COUNTY
  county_tax_add_on_amount numeric(7,2), -- YMCNAD : COUNTY TAX ADD ON AMOUNT
  county_add_on_percent numeric(3,3), -- YMCNAP : COUNTY ADD ON PERCENT
  exempt_from_local citext, -- YMEXMU : EXEMPT FROM LOCAL
  local_tax_add_on_amount numeric(7,2), -- YMMUAD : LOCAL TAX ADD ON AMOUNT
  local_add_on_percent numeric(3,3), -- YMMUAP : LOCAL ADD ON PERCENT
  bank_info citext, -- YMSBKI : BANK INFO
  deferred_vacation numeric(5,2), -- YMDVAC : DEFERRED VACATION
  deferred_holidays numeric(5,2), -- YMDHOL : DEFERRED HOLIDAYS
  deffered_sickleave numeric(5,2), -- YMDSCK : DEFFERED SICKLEAVE
  deferred_vac_year integer, -- YMDVYR : DEFERRED VAC YEAR
  deferred_hol_year integer, -- YMDHYR : DEFERRED HOL YEAR
  deffered_sic_year integer, -- YMDSYR : DEFFERED SIC YEAR
  vac_hrly_rate numeric(9,2), -- YMVRAT : VAC  HRLY RATE
  hol_hrly_rate numeric(9,2), -- YMHRAT : HOL  HRLY RATE
  sck_hrly_rate numeric(9,2), -- YMSRAT : SCK  HRLY RATE
  a_r_customer_ citext, -- YMAR# : A/R CUSTOMER #
  driver_lic_state_code citext, -- YMDVST : DRIVER LIC. STATE CODE
  driver_lic_expire_date integer, -- YMDVEX : DRIVER LIC. EXPIRE DATE
  federal_filing_status citext, -- YMFDST : Federal Filing Status
  state_filing_status citext, -- YMSTST : State Filing Status
  county_filing_status citext, -- YMCNST : County Filing Status
  city_filing_status citext, -- YMMUST : City Filing Status
  state_filing_status_2 citext, -- YMSTS2 : State Filing Status 2
  county_filing_status_2 citext, -- YMCNS2 : County Filing Status 2
  city_filing_status_2 citext, -- YMMUS2 : City Filing Status 2
  state_tax_tab_code2 citext, -- YMSTT2 : STATE TAX TAB CODE2
  county_tax_tab_code2 citext, -- YMCNT2 : COUNTY TAX TAB CODE2
  city_tax_tab_code2 citext, -- YMMUT2 : CITY TAX TAB CODE2
  sdi_state citext, -- YMSDST : SDI STATE
  wrkr_comp_state citext, -- YMWKST : WRKR COMP STATE
  reporting_code_1 citext, -- YMRPT1 : REPORTING CODE 1
  reporting_code_2 citext, -- YMRPT2 : REPORTING CODE 2
  eeoc citext, -- YMEEOC : EEOC
  overtime_exempt citext, -- YMOTEX : OVERTIME EXEMPT
  high_compensate_flag citext, -- YMHGHC : HIGH COMPENSATE FLAG
  k_eligibility_date01e integer, -- YM401E : 401K ELIGIBILITY DATE
  k_matching_date01m integer, -- YM401M : 401K MATCHING DATE
  temp_add_1 citext, -- YMTMP01 : TEMP ADD 1
  temp_add_2 citext, -- YMTMP02 : TEMP ADD 2
  temp_add_3 citext, -- YMTMP03 : TEMP ADD 3
  temp_add_4 citext, -- YMTMP04 : TEMP ADD 4
  temp_add_5 citext, -- YMTMP05 : TEMP ADD 5
  temp_add_6 citext, -- YMTMP06 : TEMP ADD 6
  temp_add_7 citext, -- YMTMP07 : TEMP ADD 7
  temp_add_9 numeric(7,2), -- YMTMP09 : TEMP ADD 9
  temp_add10 numeric(7,2), -- YMTMP10 : TEMP ADD10
  union_anniversary_date date, -- YMUNION : Union Anniversary Date
  pymast_key serial not null primary key,
  row_from_date date not null,
  row_thru_date date not null default '9999-12-31'::date,
  current_row boolean not null,
  hash text not null
)
WITH (
  OIDS=FALSE
);
ALTER TABLE arkona.xfm_pymast
  OWNER TO postgres;
COMMENT ON COLUMN arkona.xfm_pymast.pymast_company_number IS 'YMCO# : COMPANY NUMBER';
COMMENT ON COLUMN arkona.xfm_pymast.pymast_employee_number IS 'YMEMPN : EMPLOYEE NUMBER';
COMMENT ON COLUMN arkona.xfm_pymast.active_code IS 'YMACTV : ACTIVE CODE';
COMMENT ON COLUMN arkona.xfm_pymast.department_code IS 'YMDEPT : DEPARTMENT CODE';
COMMENT ON COLUMN arkona.xfm_pymast.department_ext IS 'YMEXT : DEPARTMENT EXT';
COMMENT ON COLUMN arkona.xfm_pymast.security_group IS 'YMSECG : SECURITY GROUP';
COMMENT ON COLUMN arkona.xfm_pymast.state_code_unemployment IS 'YMSTCD : STATE CODE - UNEMPLOYMENT';
COMMENT ON COLUMN arkona.xfm_pymast.county_code_unemployment IS 'YMCNCD : COUNTY CODE - UNEMPLOYMENT';
COMMENT ON COLUMN arkona.xfm_pymast.city_code_unemployment IS 'YMMUCD : CITY CODE - UNEMPLOYMENT';
COMMENT ON COLUMN arkona.xfm_pymast.employee_name IS 'YMNAME : EMPLOYEE NAME';
COMMENT ON COLUMN arkona.xfm_pymast.employee_first_name IS 'YMFNAM : EMPLOYEE FIRST NAME';
COMMENT ON COLUMN arkona.xfm_pymast.employee_middle_name IS 'YMMNAM : EMPLOYEE MIDDLE NAME';
COMMENT ON COLUMN arkona.xfm_pymast.employee_last_name IS 'YMLNAM : EMPLOYEE LAST NAME';
COMMENT ON COLUMN arkona.xfm_pymast.record_key IS 'YMNKEY : Record Key';
COMMENT ON COLUMN arkona.xfm_pymast.address_1 IS 'YMADD1 : ADDRESS LINE 1';
COMMENT ON COLUMN arkona.xfm_pymast.address_2 IS 'YMADD2 : ADDRESS LINE 2';
COMMENT ON COLUMN arkona.xfm_pymast.city_name IS 'YMCITY : CITY NAME';
COMMENT ON COLUMN arkona.xfm_pymast.state_code_address_ IS 'YMSTAT : STATE CODE (ADDRESS)';
COMMENT ON COLUMN arkona.xfm_pymast.zip_code IS 'YMZIP : ZIP CODE';
COMMENT ON COLUMN arkona.xfm_pymast.tel_area_code IS 'YMAREA : TEL AREA CODE';
COMMENT ON COLUMN arkona.xfm_pymast.telephone_number IS 'YMPHON : TELEPHONE NUMBER';
COMMENT ON COLUMN arkona.xfm_pymast.cell_phone_number IS 'YMCELPHN : CELL PHONE NUMBER';
COMMENT ON COLUMN arkona.xfm_pymast.cell_phone_provider_code IS 'YMCELPRV : CELL PHONE PROVIDER CODE';
COMMENT ON COLUMN arkona.xfm_pymast.email_address IS 'YMEMAIL : EMAIL ADDRESS';
COMMENT ON COLUMN arkona.xfm_pymast.soc_sec_number IS 'YMSS# : SOC SEC NUMBER';
COMMENT ON COLUMN arkona.xfm_pymast.drivers_license_ IS 'YMDRIV : DRIVERS LICENSE#';
COMMENT ON COLUMN arkona.xfm_pymast.birth_date IS 'YMBDTE : BIRTH DATE';
COMMENT ON COLUMN arkona.xfm_pymast.hire_date IS 'YMHDTE : HIRE DATE';
COMMENT ON COLUMN arkona.xfm_pymast.org_hire_date IS 'YMHDTO : ORG HIRE DATE';
COMMENT ON COLUMN arkona.xfm_pymast.last_raise_date IS 'YMRDTE : LAST RAISE DATE';
COMMENT ON COLUMN arkona.xfm_pymast.termination_date IS 'YMTDTE : TERMINATION DATE';
COMMENT ON COLUMN arkona.xfm_pymast.sex IS 'YMSEX : SEX';
COMMENT ON COLUMN arkona.xfm_pymast.full_time_part_time IS 'YMFP : Full Time/Part Time';
COMMENT ON COLUMN arkona.xfm_pymast.marital_status IS 'YMMARI : MARITAL STATUS';
COMMENT ON COLUMN arkona.xfm_pymast.exempt_from_fica IS 'YMEXSS : EXEMPT FROM FICA';
COMMENT ON COLUMN arkona.xfm_pymast.exempt_from_fed IS 'YMEXFD : EXEMPT FROM FED';
COMMENT ON COLUMN arkona.xfm_pymast.federal_exemptions IS 'YMFDEX : FEDERAL EXEMPTIONS';
COMMENT ON COLUMN arkona.xfm_pymast.federal_add_on_amount IS 'YMFDAD : FEDERAL ADD ON AMOUNT';
COMMENT ON COLUMN arkona.xfm_pymast.federal_add_on_percent IS 'YMFDAP : FEDERAL ADD ON PERCENT';
COMMENT ON COLUMN arkona.xfm_pymast.benfit_type IS 'YMVTYP : BENFIT TYPE';
COMMENT ON COLUMN arkona.xfm_pymast.payroll_class IS 'YMCLAS : PAYROLL CLASS';
COMMENT ON COLUMN arkona.xfm_pymast.eic_eligable IS 'YMEEIC : EIC ELIGABLE';
COMMENT ON COLUMN arkona.xfm_pymast.eic_class IS 'YMECLS : EIC CLASS';
COMMENT ON COLUMN arkona.xfm_pymast.pay_period IS 'YMPPER : PAY PERIOD';
COMMENT ON COLUMN arkona.xfm_pymast.base_salary IS 'YMSALY : BASE SALARY';
COMMENT ON COLUMN arkona.xfm_pymast.base_hrly_rate IS 'YMRATE : BASE HRLY RATE';
COMMENT ON COLUMN arkona.xfm_pymast.alt_salary IS 'YMSALA : ALT  SALARY';
COMMENT ON COLUMN arkona.xfm_pymast.alt_hrly_rate IS 'YMRATA : ALT  HRLY RATE';
COMMENT ON COLUMN arkona.xfm_pymast.distrib_code IS 'YMDIST : DISTRIB CODE';
COMMENT ON COLUMN arkona.xfm_pymast.elig_for_retire IS 'YMRETE : ELIG FOR RETIRE';
COMMENT ON COLUMN arkona.xfm_pymast.retirement_ IS 'YMRETP : RETIREMENT %';
COMMENT ON COLUMN arkona.xfm_pymast.retirement_fixed_amount IS 'YMRFIX : RETIREMENT FIXED AMOUNT';
COMMENT ON COLUMN arkona.xfm_pymast.retirement_annual_max IS 'YMRAMX : RETIREMENT ANNUAL MAX';
COMMENT ON COLUMN arkona.xfm_pymast.wkman_comp_code IS 'YMWKCD : WKMAN COMP CODE';
COMMENT ON COLUMN arkona.xfm_pymast.cum_wage_withhld IS 'YMCWFL : CUM WAGE WITHHLD';
COMMENT ON COLUMN arkona.xfm_pymast.state_tax_tab_code IS 'YMSTTX : STATE TAX TAB CODE';
COMMENT ON COLUMN arkona.xfm_pymast.county_tax_tab_code IS 'YMCNTX : COUNTY TAX TAB CODE';
COMMENT ON COLUMN arkona.xfm_pymast.city_tax_tab_code IS 'YMMUTX : CITY TAX TAB CODE';
COMMENT ON COLUMN arkona.xfm_pymast.last_update IS 'YMLSUP : LAST UPDATE';
COMMENT ON COLUMN arkona.xfm_pymast.last_check IS 'YMLSCK : LAST CHECK';
COMMENT ON COLUMN arkona.xfm_pymast.deceased_flag IS 'YMDECE : DECEASED FLAG';
COMMENT ON COLUMN arkona.xfm_pymast.exempt_from_state IS 'YMEXST : EXEMPT FROM STATE';
COMMENT ON COLUMN arkona.xfm_pymast.state_exemptions IS 'YMSTEX : STATE EXEMPTIONS';
COMMENT ON COLUMN arkona.xfm_pymast.state_tax_add_on_amount IS 'YMSTAD : STATE TAX ADD ON AMOUNT';
COMMENT ON COLUMN arkona.xfm_pymast.state_add_on_percent IS 'YMSTAP : STATE ADD ON PERCENT';
COMMENT ON COLUMN arkona.xfm_pymast.state_fixed_exemptions IS 'YMSTFX : STATE FIXED EXEMPTIONS';
COMMENT ON COLUMN arkona.xfm_pymast.exempt_from_county IS 'YMEXCN : EXEMPT FROM COUNTY';
COMMENT ON COLUMN arkona.xfm_pymast.county_tax_add_on_amount IS 'YMCNAD : COUNTY TAX ADD ON AMOUNT';
COMMENT ON COLUMN arkona.xfm_pymast.county_add_on_percent IS 'YMCNAP : COUNTY ADD ON PERCENT';
COMMENT ON COLUMN arkona.xfm_pymast.exempt_from_local IS 'YMEXMU : EXEMPT FROM LOCAL';
COMMENT ON COLUMN arkona.xfm_pymast.local_tax_add_on_amount IS 'YMMUAD : LOCAL TAX ADD ON AMOUNT';
COMMENT ON COLUMN arkona.xfm_pymast.local_add_on_percent IS 'YMMUAP : LOCAL ADD ON PERCENT';
COMMENT ON COLUMN arkona.xfm_pymast.bank_info IS 'YMSBKI : BANK INFO';
COMMENT ON COLUMN arkona.xfm_pymast.deferred_vacation IS 'YMDVAC : DEFERRED VACATION';
COMMENT ON COLUMN arkona.xfm_pymast.deferred_holidays IS 'YMDHOL : DEFERRED HOLIDAYS';
COMMENT ON COLUMN arkona.xfm_pymast.deffered_sickleave IS 'YMDSCK : DEFFERED SICKLEAVE';
COMMENT ON COLUMN arkona.xfm_pymast.deferred_vac_year IS 'YMDVYR : DEFERRED VAC YEAR';
COMMENT ON COLUMN arkona.xfm_pymast.deferred_hol_year IS 'YMDHYR : DEFERRED HOL YEAR';
COMMENT ON COLUMN arkona.xfm_pymast.deffered_sic_year IS 'YMDSYR : DEFFERED SIC YEAR';
COMMENT ON COLUMN arkona.xfm_pymast.vac_hrly_rate IS 'YMVRAT : VAC  HRLY RATE';
COMMENT ON COLUMN arkona.xfm_pymast.hol_hrly_rate IS 'YMHRAT : HOL  HRLY RATE';
COMMENT ON COLUMN arkona.xfm_pymast.sck_hrly_rate IS 'YMSRAT : SCK  HRLY RATE';
COMMENT ON COLUMN arkona.xfm_pymast.a_r_customer_ IS 'YMAR# : A/R CUSTOMER #';
COMMENT ON COLUMN arkona.xfm_pymast.driver_lic_state_code IS 'YMDVST : DRIVER LIC. STATE CODE';
COMMENT ON COLUMN arkona.xfm_pymast.driver_lic_expire_date IS 'YMDVEX : DRIVER LIC. EXPIRE DATE';
COMMENT ON COLUMN arkona.xfm_pymast.federal_filing_status IS 'YMFDST : Federal Filing Status';
COMMENT ON COLUMN arkona.xfm_pymast.state_filing_status IS 'YMSTST : State Filing Status';
COMMENT ON COLUMN arkona.xfm_pymast.county_filing_status IS 'YMCNST : County Filing Status';
COMMENT ON COLUMN arkona.xfm_pymast.city_filing_status IS 'YMMUST : City Filing Status';
COMMENT ON COLUMN arkona.xfm_pymast.state_filing_status_2 IS 'YMSTS2 : State Filing Status 2';
COMMENT ON COLUMN arkona.xfm_pymast.county_filing_status_2 IS 'YMCNS2 : County Filing Status 2';
COMMENT ON COLUMN arkona.xfm_pymast.city_filing_status_2 IS 'YMMUS2 : City Filing Status 2';
COMMENT ON COLUMN arkona.xfm_pymast.state_tax_tab_code2 IS 'YMSTT2 : STATE TAX TAB CODE2';
COMMENT ON COLUMN arkona.xfm_pymast.county_tax_tab_code2 IS 'YMCNT2 : COUNTY TAX TAB CODE2';
COMMENT ON COLUMN arkona.xfm_pymast.city_tax_tab_code2 IS 'YMMUT2 : CITY TAX TAB CODE2';
COMMENT ON COLUMN arkona.xfm_pymast.sdi_state IS 'YMSDST : SDI STATE';
COMMENT ON COLUMN arkona.xfm_pymast.wrkr_comp_state IS 'YMWKST : WRKR COMP STATE';
COMMENT ON COLUMN arkona.xfm_pymast.reporting_code_1 IS 'YMRPT1 : REPORTING CODE 1';
COMMENT ON COLUMN arkona.xfm_pymast.reporting_code_2 IS 'YMRPT2 : REPORTING CODE 2';
COMMENT ON COLUMN arkona.xfm_pymast.eeoc IS 'YMEEOC : EEOC';
COMMENT ON COLUMN arkona.xfm_pymast.overtime_exempt IS 'YMOTEX : OVERTIME EXEMPT';
COMMENT ON COLUMN arkona.xfm_pymast.high_compensate_flag IS 'YMHGHC : HIGH COMPENSATE FLAG';
COMMENT ON COLUMN arkona.xfm_pymast.k_eligibility_date01e IS 'YM401E : 401K ELIGIBILITY DATE';
COMMENT ON COLUMN arkona.xfm_pymast.k_matching_date01m IS 'YM401M : 401K MATCHING DATE';
COMMENT ON COLUMN arkona.xfm_pymast.temp_add_1 IS 'YMTMP01 : TEMP ADD 1';
COMMENT ON COLUMN arkona.xfm_pymast.temp_add_2 IS 'YMTMP02 : TEMP ADD 2';
COMMENT ON COLUMN arkona.xfm_pymast.temp_add_3 IS 'YMTMP03 : TEMP ADD 3';
COMMENT ON COLUMN arkona.xfm_pymast.temp_add_4 IS 'YMTMP04 : TEMP ADD 4';
COMMENT ON COLUMN arkona.xfm_pymast.temp_add_5 IS 'YMTMP05 : TEMP ADD 5';
COMMENT ON COLUMN arkona.xfm_pymast.temp_add_6 IS 'YMTMP06 : TEMP ADD 6';
COMMENT ON COLUMN arkona.xfm_pymast.temp_add_7 IS 'YMTMP07 : TEMP ADD 7';
COMMENT ON COLUMN arkona.xfm_pymast.temp_add_9 IS 'YMTMP09 : TEMP ADD 9';
COMMENT ON COLUMN arkona.xfm_pymast.temp_add10 IS 'YMTMP10 : TEMP ADD10';
COMMENT ON COLUMN arkona.xfm_pymast.union_anniversary_date IS 'YMUNION : Union Anniversary Date';

create unique index on arkona.xfm_pymast(pymast_company_number,pymast_employee_number) where current_row;

---------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------

drop table if exists arkona.xfm_pymast_changed_rows cascade;
create table arkona.xfm_pymast_changed_rows (
  pymast_company_number citext not null,
  pymast_employee_number citext not null,
  primary key (pymast_company_number,pymast_employee_number));

---------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------  
drop table if exists arkona.xfm_pyactgr cascade;
CREATE TABLE arkona.xfm_pyactgr
(
  company_number citext, -- YTACO# : COMPANY NUMBER
  dist_code citext, -- YTADIC : DIST CODE
  seq_number integer, -- YTASEQ : SEQ NUMBER
  description citext, -- YTADSC : DESCRIPTION
  dist_co_ citext, -- YTAGCO : DIST CO#
  gross_dist numeric(5,2), -- YTAGPP : GROSS % DIST
  gross_expense_act_ citext, -- YTAGPA : GROSS EXPENSE ACT#
  overtime_act_ citext, -- YTAOPA : OVERTIME      ACT#
  vacation_expense_act_ citext, -- YTAVAC : VACATION EXPENSE ACT#
  holiday_expense_act_ citext, -- YTAHOL : HOLIDAY EXPENSE ACT#
  sick_leave_expense_act_ citext, -- YTASCK : SICK LEAVE EXPENSE ACT#
  retire_expense_act_ citext, -- YTAERA : RETIRE EXPENSE ACT#
  emplr_fica_expense citext, -- YTAETA : EMPLR FICA   EXPENSE
  emplr_med_expense citext, -- YTAMED : EMPLR MED    EXPENSE
  federal_un_emp_act_ citext, -- YTAFUC : FEDERAL UN-EMP ACT#
  state_unemp_act_ citext, -- YTASUC : STATE UNEMP    ACT#
  state_comp_act_ citext, -- YTACMP : STATE  COMP    ACT#
  state_sdi_act_ citext, -- YTASDI : STATE  SDI     ACT#
  emplr_contributions citext, -- YTACON : EMPLR CONTRIBUTIONS
  pyactgr_key serial primary key,
  row_from_date date not null,
  row_thru_date date not null default '9999-12-31'::date,
  current_row boolean not null,
  hash citext not null
)
WITH (
  OIDS=FALSE
);
ALTER TABLE arkona.xfm_pyactgr
  OWNER TO rydell;
COMMENT ON COLUMN arkona.xfm_pyactgr.company_number IS 'YTACO# : COMPANY NUMBER';
COMMENT ON COLUMN arkona.xfm_pyactgr.dist_code IS 'YTADIC : DIST CODE';
COMMENT ON COLUMN arkona.xfm_pyactgr.seq_number IS 'YTASEQ : SEQ NUMBER';
COMMENT ON COLUMN arkona.xfm_pyactgr.description IS 'YTADSC : DESCRIPTION';
COMMENT ON COLUMN arkona.xfm_pyactgr.dist_co_ IS 'YTAGCO : DIST CO#';
COMMENT ON COLUMN arkona.xfm_pyactgr.gross_dist IS 'YTAGPP : GROSS % DIST';
COMMENT ON COLUMN arkona.xfm_pyactgr.gross_expense_act_ IS 'YTAGPA : GROSS EXPENSE ACT#';
COMMENT ON COLUMN arkona.xfm_pyactgr.overtime_act_ IS 'YTAOPA : OVERTIME      ACT#';
COMMENT ON COLUMN arkona.xfm_pyactgr.vacation_expense_act_ IS 'YTAVAC : VACATION EXPENSE ACT#';
COMMENT ON COLUMN arkona.xfm_pyactgr.holiday_expense_act_ IS 'YTAHOL : HOLIDAY EXPENSE ACT#';
COMMENT ON COLUMN arkona.xfm_pyactgr.sick_leave_expense_act_ IS 'YTASCK : SICK LEAVE EXPENSE ACT#';
COMMENT ON COLUMN arkona.xfm_pyactgr.retire_expense_act_ IS 'YTAERA : RETIRE EXPENSE ACT#';
COMMENT ON COLUMN arkona.xfm_pyactgr.emplr_fica_expense IS 'YTAETA : EMPLR FICA   EXPENSE';
COMMENT ON COLUMN arkona.xfm_pyactgr.emplr_med_expense IS 'YTAMED : EMPLR MED    EXPENSE';
COMMENT ON COLUMN arkona.xfm_pyactgr.federal_un_emp_act_ IS 'YTAFUC : FEDERAL UN-EMP ACT#';
COMMENT ON COLUMN arkona.xfm_pyactgr.state_unemp_act_ IS 'YTASUC : STATE UNEMP    ACT#';
COMMENT ON COLUMN arkona.xfm_pyactgr.state_comp_act_ IS 'YTACMP : STATE  COMP    ACT#';
COMMENT ON COLUMN arkona.xfm_pyactgr.state_sdi_act_ IS 'YTASDI : STATE  SDI     ACT#';
COMMENT ON COLUMN arkona.xfm_pyactgr.emplr_contributions IS 'YTACON : EMPLR CONTRIBUTIONS';

create unique index on arkona.xfm_pyactgr(company_number,dist_code,seq_number);

---------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------- 

drop table if exists arkona.xfm_pyactgr_changed_rows cascade;
create table arkona.xfm_pyactgr_changed_rows (
  company_number citext not null,
  dist_code citext not null,
  seq_number integer not null,
  primary key (company_number,dist_code,seq_number));

---------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------  