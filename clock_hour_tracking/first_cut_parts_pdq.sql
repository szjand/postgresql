﻿
-- to start with, we are going to do both stores, pdq & parts & only non-split dist codes

-- of currently active employees, limiting gross_dist to 100 escludes:
-- ry1 kowalski 179790, aftermarket 3 way
-- ry2 larson 282713 serv/pdq writer 2 way
select a.pymast_company_number, a.active_code, a.pymast_employee_number, 
  a.employee_name, a.employee_first_name, a.employee_last_name,
  a.payroll_class, a.distrib_code, c.description, a.department_code, b.description, a.current_row, 
  arkona.db2_integer_to_date(a.hire_date) as hire_date, 
  arkona.db2_integer_to_date(a.termination_date) as term_date
from  arkona.xfm_pymast a
left join arkona.ext_pypclkctl b on a.department_code = b.department_code
  and a.pymast_company_number = b.company_number
left join arkona.xfm_pyactgr c on a.distrib_code = c.dist_code
  and a.pymast_company_number = c.company_number
where a.payroll_class = 'H'
  and a.department_code in ('06','03')
  and a.pymast_company_number in ('RY1','RY2')
  and c.gross_dist = 100 -- no split codes
  and arkona.db2_integer_to_date(a.termination_date) > '12/31/2017'
  and employee_last_name = 'elliot'
order by a.employee_name

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
-- 4/26/18 needd to add from/thr ? grouping by name resulted in mult rows for empl# 12725
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
drop table if exists employees;
create table employees as
select a.pymast_company_number as store_code, a.pymast_employee_number as employee_number, 
  max(a.employee_name) as employee_name,
  a.distrib_code, c.description as dist_code_description, 
  a.department_code, b.description as dept
from  arkona.xfm_pymast a
left join arkona.ext_pypclkctl b on a.department_code = b.department_code
  and a.pymast_company_number = b.company_number
left join arkona.xfm_pyactgr c on a.distrib_code = c.dist_code
  and a.pymast_company_number = c.company_number
where a.payroll_class = 'H'
  and a.department_code in ('06','03')
  and a.pymast_company_number in ('RY1','RY2')
  and c.gross_dist = 100 -- no split dist codes
  and arkona.db2_integer_to_date(a.termination_date) > '12/31/2017'
group by a.pymast_company_number, a.pymast_employee_number, 
--   a.employee_name, 
  a.distrib_code, c.description, 
  a.department_code, b.description;
create unique index on employees(store_code,employee_number,employee_name,distrib_code,
  dist_code_description,department_code,dept);


-- clock hours by month
drop table if exists clock_hours_by_month;
create temp table clock_hours_by_month as
select a.store_code, a.dept, a.distrib_code, a.dist_code_description, 
  c.the_year, c.year_month, a.employee_name, a.employee_number,
  sum(reg_hours) as reg_hours, sum(ot_hours) as ot_hours
from employees a
left join arkona.xfm_pypclockin b on a.employee_number = b.employee_number
  and b.the_date > '12/31/2016'
left join dds.dim_date c on b.the_date = c.the_date  
group by  a.store_code, a.dept, a.distrib_code, a.dist_code_description, 
  a.dept, c.the_year, c.year_month, a.employee_name, a.employee_number
having sum(reg_hours) > 0
order by the_year, year_month, store_code, dept, dist_code_description, dept, employee_name;

drop table if exists ot_comp_by_month;
create temp table ot_comp_by_month as
select aa.*, bb.year_month, bb.ot_comp
from (
  select a.employee_number, a.employee_name
  from employees a
  group by a.employee_number, employee_name) aa
left join (
  select a.employee_name, a.employee_, sum(b.amount) as ot_comp,
    100 * (2000 + (a.payroll_cen_year - 100)) + a.check_month as year_month,
    (2000 + (a.payroll_cen_year - 100)) as the_year
  from arkona.ext_pyhshdta a
  inner join arkona.ext_pyhscdta b on a.payroll_run_number = b.payroll_run_number
    and a.company_number = b.company_number
    and a.employee_ = b.employee_number
    and b.code_type in ('0','1') -- 0,1: income, 2: deduction
    and b.code_id = 'OVT'
  where a.payroll_cen_year > 116 
  group by a.employee_name, a.employee_, a.check_month, a.payroll_cen_year, b.description) bb on aa.employee_number = bb.employee_
where bb.ot_comp is not null ; 

select * from clock_hours_by_month

select * from ot_comp_by_month

select a.*, coalesce(b.ot_comp, 0)
from clock_hours_by_month a
left join ot_comp_by_month b on a.employee_number = b.employee_number
  and a.year_month = b.year_month


so, as expected, the mismatch between biweekly pay periods and calendar months surfaces, 
eg
caden Aker 10568, 201803 0 ot hours, but 14.76 ot comp

see pay_period_vs_calendar_month_impedance_mismatch.sql for an attempt at resolving

until i talk with jeri, what we have here is good enoughj

select * from dds.dim_date where year_month = 201804
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
working days
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

-- all these wd values need to exist in the calendar on non wd as well 

drop table if exists wd;
create temp table wd as
select x.the_date, 
  x.wd_in_year, 
  row_number() over (partition by the_year order by the_date) - 1 as wd_of_year_elapsed, 
  wd_in_year - row_number() over (partition by the_year order by the_date) + 1 as wd_of_year_remaining,
  row_number() over (partition by the_year order by the_date) as wd_of_year, 
  
  x.wd_in_month, 
  row_number() over (partition by year_month order by the_date) - 1 as wd_of_month_elapsed, 
  wd_in_month - row_number() over (partition by year_month order by the_date) + 1 as wd_of_month_remaining, 
  row_number() over (partition by year_month order by the_date) as wd_of_month,
  
  x.wd_in_biweekly_pay_period,
  row_number() over (partition by biweekly_pay_period_sequence order by the_date) - 1 as wd_of_biweekly_pay_period_elapsed, 
  wd_in_biweekly_pay_period - row_number() over (partition by biweekly_pay_period_sequence order by the_date) + 1 as wd_of_bi_weekly_pay_period_remaining,   
  row_number() over (partition by biweekly_pay_period_sequence order by the_date) as wd_of_biweekly_pay_period
  
from (
  select the_year, year_month, the_date, biweekly_pay_period_sequence, day_name, first_of_month, last_of_month,
    count(*) over (partition by the_year) as wd_in_year,
    count(*) over (partition by year_month) as wd_in_month,
    count(*) over (partition by biweekly_pay_period_sequence) as wd_in_biweekly_pay_period
  from dds.dim_date 
  where not holiday
    and day_of_week <> 1
  order by the_date) x;  


alter table dds.dim_date
add column wd_in_year integer, 
add column wd_of_year_elapsed integer, 
add column wd_of_year_remaining integer, 
add column wd_of_year integer, 
add column wd_in_month integer, 
add column wd_of_month_elapsed integer, 
add column wd_of_month_remaining integer,  
add column wd_of_month integer, 
add column wd_in_biweekly_pay_period integer, 
add column wd_of_biweekly_pay_period_elapsed integer, 
add column wd_of_bi_weekly_pay_period_remaining integer,   
add column wd_of_biweekly_pay_period integer;


-- this works, including 2 consecutive nulls: 09/02, 09/03
-- there are no instances of 3 nulls in a row, that would require 2 holidays on consecutive days
-- select * from wd limit 10
-- on the first of each month, need the following value, not the preceding value

update dds.dim_date y
set wd_in_year = z.wd_in_year,
    wd_of_year_elapsed = z.wd_of_year_elapsed,
    wd_of_year_remaining = z.wd_of_year_remaining ,
    wd_of_year = z.wd_of_year ,
    wd_in_month = z.wd_in_month,
    wd_of_month_elapsed = z.wd_of_month_elapsed ,
    wd_of_month_remaining = z.wd_of_month_remaining ,
    wd_of_month = z.wd_of_month ,
    wd_in_biweekly_pay_period = z.wd_in_biweekly_pay_period,
    wd_of_biweekly_pay_period_elapsed = z.wd_of_biweekly_pay_period_elapsed ,
    wd_of_bi_weekly_pay_period_remaining = z.wd_of_bi_weekly_pay_period_remaining  ,
    wd_of_biweekly_pay_period = z.wd_of_biweekly_pay_period
from (
  select cal_date,    
    case x.day_of_month
      when 1 then 
        coalesce(coalesce(wd_in_year, lead(x.wd_in_year, 1) over (order by cal_date)), lead(x.wd_in_year, 2) over (order by cal_date))
      else 
        coalesce(coalesce(wd_in_year, lag(x.wd_in_year, 1) over (order by cal_date)), lag(x.wd_in_year, 2) over (order by cal_date)) 
    end as wd_in_year,
    case x.day_of_month
      when 1 then
        coalesce(coalesce(wd_of_year_elapsed, lead(x.wd_of_year_elapsed, 1) over (order by cal_date)), lead(x.wd_of_year_elapsed, 2) over (order by cal_date))
      else
        coalesce(coalesce(wd_of_year_elapsed, lag(x.wd_of_year_elapsed, 1) over (order by cal_date)), lag(x.wd_of_year_elapsed, 2) over (order by cal_date)) 
    end as wd_of_year_elapsed,
    case x.day_of_month
      when 1 then
        coalesce(coalesce(wd_of_year_remaining, lead(x.wd_of_year_remaining, 1) over (order by cal_date)), lead(x.wd_of_year_remaining, 2) over (order by cal_date))
      else
        coalesce(coalesce(wd_of_year_remaining, lag(x.wd_of_year_remaining, 1) over (order by cal_date)), lag(x.wd_of_year_remaining, 2) over (order by cal_date))
    end as wd_of_year_remaining,
    coalesce(wd_of_year, 0) as wd_of_year,
--     case x.day_of_month
--       when 1 then
--         coalesce(coalesce(wd_of_year, lead(x.wd_of_year, 1) over (order by cal_date)), lead(x.wd_of_year, 2) over (order by cal_date))
--       else
--         coalesce(coalesce(wd_of_year, lag(x.wd_of_year, 1) over (order by cal_date)), lag(x.wd_of_year, 2) over (order by cal_date))
--     end as wd_of_year,
    case x.day_of_month
      when 1 then
        coalesce(coalesce(wd_in_month, lead(x.wd_in_month, 1) over (order by cal_date)), lead(x.wd_in_month, 2) over (order by cal_date))
      else
        coalesce(coalesce(wd_in_month, lag(x.wd_in_month, 1) over (order by cal_date)), lag(x.wd_in_month, 2) over (order by cal_date))
    end as wd_in_month,
    case x.day_of_month
      when 1 then
        coalesce(coalesce(wd_of_month_elapsed, lead(x.wd_of_month_elapsed, 1) over (order by cal_date)), lead(x.wd_of_month_elapsed, 2) over (order by cal_date)) 
      else
        coalesce(coalesce(wd_of_month_elapsed, lag(x.wd_of_month_elapsed, 1) over (order by cal_date)), lag(x.wd_of_month_elapsed, 2) over (order by cal_date)) 
    end as wd_of_month_elapsed,
    case x.day_of_month
      when 1 then
        coalesce(coalesce(wd_of_month_remaining, lead(x.wd_of_month_remaining, 1) over (order by cal_date)), lead(x.wd_of_month_remaining, 2) over (order by cal_date)) 
      else
        coalesce(coalesce(wd_of_month_remaining, lag(x.wd_of_month_remaining, 1) over (order by cal_date)), lag(x.wd_of_month_remaining, 2) over (order by cal_date)) 
    end as wd_of_month_remaining,
    coalesce(wd_of_month, 0) as wd_of_month,
--     coalesce(coalesce(wd_of_month, lag(x.wd_of_month, 1) over (order by cal_date)), lag(x.wd_of_month, 2) over (order by cal_date)) as wd_of_month,
    coalesce(coalesce(wd_in_biweekly_pay_period, lag(x.wd_in_biweekly_pay_period, 1) over (order by cal_date)), lag(x.wd_in_biweekly_pay_period, 2) over (order by cal_date)) as wd_in_biweekly_pay_period,
    coalesce(coalesce(wd_of_biweekly_pay_period_elapsed, lag(x.wd_of_biweekly_pay_period_elapsed, 1) over (order by cal_date)), lag(x.wd_of_biweekly_pay_period_elapsed, 2) over (order by cal_date)) as wd_of_biweekly_pay_period_elapsed,
    coalesce(coalesce(wd_of_bi_weekly_pay_period_remaining, lag(x.wd_of_bi_weekly_pay_period_remaining, 1) over (order by cal_date)), lag(x.wd_of_bi_weekly_pay_period_remaining, 2) over (order by cal_date)) as wd_of_bi_weekly_pay_period_remaining,
    coalesce(coalesce(wd_of_biweekly_pay_period, lag(x.wd_of_biweekly_pay_period, 1) over (order by cal_date)), lag(x.wd_of_biweekly_pay_period, 2) over (order by cal_date)) as wd_of_biweekly_pay_period
  from (  
    select a.the_date as cal_date, a.day_of_month, b.*
    from dds.dim_date a
    left join wd b on a.the_Date = b.the_date) x) z
where y.the_date = z.cal_date ;   

--------------------------------------------------------------------------------------------------------------------------------------------------------


drop table if exists cht;
create temp table cht as
select a.*, coalesce(b.ot_comp, 0) as ot_comp
from clock_hours_by_month a
left join ot_comp_by_month b on a.employee_number = b.employee_number
  and a.year_month = b.year_month;

select * from cht

select * from clock_hours

-- dept mtd
select store_code, dept, sum(reg_hours) as reg_hours, sum(ot_hours) as ot_hours, sum(ot_comp) as ot_comp
from cht  
where year_month = (
  select year_month
  from dds.dim_date
  where the_date = current_date)
group by store_code, dept  

-- dept ytd
select store_code, dept, sum(reg_hours) as reg_hours, sum(ot_hours) as ot_hours, sum(ot_comp) as ot_comp
from cht  
group by store_code, dept  

drop table if exists jeri.clock_hour_tracking;
create table jeri.clock_hour_tracking (
  store_code citext not null,
  department citext not null,
  dist_code citext not null,
  dist_code_description citext not null,
  the_year integer not null,
  year_month integer not null,
  employee_name citext not null,
  employee_number citext not null,
  regular_hours numeric (8,2) not null default 0,
  ot_hours numeric (8,2) not null default 0,
  ot_comp numeric (8,2) not null default 0,
  primary key (employee_number,year_month));

insert into jeri.clock_hour_tracking
select * from cht;


  
select * from jeri.clock_hour_tracking

select * from dds.dim_date where year_month = 201804

-- mtd days worked/fte
-- account for divide by 0, first of month/year
with 
  days_worked as (
    select wd_of_month_elapsed 
    from dds.dim_date 
    where the_date = current_date)
select store_code, dist_code,
  (select wd_of_month_elapsed from days_worked) as days_worked,
  round(sum(regular_hours + ot_hours) /
    (select 8 * (select wd_of_month_elapsed from days_worked)), 1)
from jeri.clock_hour_tracking 
where dist_code = 'PTEC'
  and year_month = (select year_month from dds.dim_date where the_date = current_date)
group by store_code, dist_code, year_month  


-- ytd days worked/fte
-- account for divide by 0, first of month/year
with 
  days_worked as (
    select wd_of_year_elapsed 
    from dds.dim_date 
    where the_date = current_date)
select store_code, dist_code,
  (select wd_of_year_elapsed from days_worked) as days_worked,
  round(sum(regular_hours + ot_hours) /
    (select 8 * (select wd_of_year_elapsed from days_worked)), 1)
from jeri.clock_hour_tracking 
where dist_code = 'PTEC'
  and the_year = (select the_year from dds.dim_date where the_date = current_date)
group by store_code, dist_code, the_year 


select a.store_code, a.department, a.dist_code, a.the_year, a.year_month, sum(a.regular_hours + a.ot_hours) as hours,
  b.mtd_days_worked, b.ytd_days_worked, 
  round(sum(a.regular_hours + a.ot_hours) / 8 * b.mtd_days_worked, 1) as mtd_fte,
  round(sum(a.regular_hours + a.ot_hours) / 8 * b.ytd_days_worked, 1) as ytd_fte 
from jeri.clock_hour_tracking a
left join (
  select the_year, year_month, max(wd_of_month_elapsed) as mtd_days_worked, max(wd_of_year_elapsed) as ytd_days_worked 
  from dds.dim_date 
  where the_date between '01/01/2017' and current_date
  group by the_year, year_month) b on a.year_month = b.year_month
group by a.store_code, a.department, a.dist_code, a.the_year, a.year_month, b.mtd_days_worked, b.ytd_days_worked



select the_year, year_month, max(wd_of_month_elapsed) as mtd_days_worked, max(wd_of_year_elapsed) as ytd_days_worked 
from dds.dim_date 
where the_date between '01/01/2017' and current_date
group by the_year, year_month
order by year_month

-- mtd days_worked_fte
select aa.store_code, aa.department, aa.dist_code, aa.year_month, bb.mtd_days_worked,
  round(aa.mtd_hours / (8 * bb.mtd_days_worked), 1) as mtd_fte
-- select *  
from (
  select a.store_code, a.department, a.dist_code, a.year_month, 
    sum(a.regular_hours + a.ot_hours) as mtd_hours
  from jeri.clock_hour_tracking a
  group by a.store_code, a.department, a.dist_code, a.year_month
  order by a.store_code, a.department, a.dist_code, a.year_month) aa
left join (
  select the_year, year_month, max(wd_of_month_elapsed) as mtd_days_worked, max(wd_of_year_elapsed) as ytd_days_worked 
  from dds.dim_date 
  where the_date between '01/01/2017' and current_date
  group by the_year, year_month) bb  on aa.year_month = bb.year_month
order by aa.store_code, aa.department, aa.dist_code, aa.year_month

-- ytd_days_worked_fte
select aa.store_code, aa.department, aa.the_year, aa.dist_code, bb.ytd_days_worked,
  round(aa.ytd_hours / (8 * bb.ytd_days_worked), 1) as ytd_fte
-- select *  
from (
  select store_code, department, dist_code, the_year, sum(aa.mtd_total_hours) as ytd_hours
  from (
    select a.store_code, a.department, a.dist_code, a.the_year, a.year_month, 
      sum(a.regular_hours + a.ot_hours) as mtd_total_hours
    from jeri.clock_hour_tracking a
    group by a.store_code, a.department, a.dist_code, a.the_year, a.year_month) aa
  group by store_code, department, dist_code, the_year) aa
left join (
  select the_year, max(wd_of_year_elapsed) as ytd_days_worked 
  from dds.dim_date 
  where the_date between '01/01/2017' and current_date
  group by the_year) bb  on aa.the_year = bb.the_year
order by aa.the_year, aa.store_code, aa.department, aa.dist_code



select store_code, department, dist_code, 
  b.mtd_days_worked, round(mtd_hours / (8 * b.mtd_days_worked), 1) as mtd_fte,
  b.ytd_days_worked,  round(a.ytd_hours / (8 * b.ytd_days_worked), 1) as ytd_fte
from ( -- dist_code hours
  select store_code, department, dist_code,
    sum(regular_hours + ot_hours) as ytd_hours,
    sum(regular_hours + ot_hours) filter (where year_month = 201804) as mtd_hours     
  from jeri.clock_hour_tracking 
  where the_year = 2018 ----------------------------------------------
  group by store_code, department, dist_code) a
cross join ( -- days worked
  select wd_of_month_elapsed as mtd_days_worked, wd_of_year_elapsed as ytd_days_worked 
  from dds.dim_date 
  where the_date = current_date) b


select e.*, mtd_days_worked, mtd_fte, ytd_days_worked, ytd_fte
from (
  select store_code, department, dist_code, dist_code_description,
    json_build_object( -- ytd
      'regular_hours',sum(regular_hours),
      'ot_hours', sum(ot_hours), 
      'ot_comp', sum(ot_comp)) as ytd_stats,
    json_build_object( -- mtd
      'regular_hours', sum(regular_hours) filter (where year_month = 201804),                      
      'ot_hours', sum(ot_hours) filter (where year_month = 201804),
      'ot_comp', sum(ot_comp) filter (where year_month = 201804)) as mtd_stats          
  from jeri.clock_hour_tracking 
  group by store_code, department, dist_code, dist_code_description) e 
left join (  
  select store_code, department, dist_code, 
    b.mtd_days_worked, round(mtd_hours / (8 * b.mtd_days_worked), 1) as mtd_fte,
    b.ytd_days_worked,  round(a.ytd_hours / (8 * b.ytd_days_worked), 1) as ytd_fte
  from ( -- dist_code hours
    select store_code, department, dist_code,
      sum(regular_hours + ot_hours) as ytd_hours,
      sum(regular_hours + ot_hours) filter (where year_month = 201804) as mtd_hours     
    from jeri.clock_hour_tracking 
    where the_year = 2018 ----------------------------------------------
    group by store_code, department, dist_code) a
  cross join ( -- days worked
    select wd_of_month_elapsed as mtd_days_worked, wd_of_year_elapsed as ytd_days_worked 
    from dds.dim_date 
    where the_date = current_date) b) f on e.store_code = f.store_code
      and e.department = f.department
      and e.dist_code = f.dist_code
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
/* 4/30

  talked to jeri
  the mismatch between month and pay period was too much, couple of ways to deal with it, 
  going with, calc ot_comp from hourly rate * clock hours


so, what i need is a table of employees with 1 row for each emp#, dept. dist_code, hourly rate combination
*/
---------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------        
select * from ads.ext_edw_employee_dim limit 100

drop table if exists emp_1;
create temp table emp_1 as
select max(name) as name, storecode, employeenumber, pydeptcode, pydept, distcode, distribution, termdate, hourlyrate, null::date as from_date, null::date as thru_date
from ads.ext_edw_employee_dim 
where termdate > '12/31/2016'
  and payrollclass = 'hourly'
  and pydeptcode in ('03','06')
group by storecode, employeenumber, pydeptcode, pydept, distcode, distribution, termdate, hourlyrate
order by employeenumber;

-- 1 with multiple stores
select name
from (
select name, storecode 
from emp_1 a
group by name, storecode) x
group by name having count(*) > 1

select a.storecode, a.name
from emp_1 a
left join ads.ext_edw_employee_dim b on a.employeenumber = b.employeenumber
where a.name = 'LINDQUIST, ADAM J'


-- 2 with multiple depts
select name
from (
select name, pydept 
from emp_1 a
group by name, pydept) x
group by name having count(*) > 1

select *
from emp_1
where name in ('LINDQUIST, ADAM J', 'HELGESON, JEFFREY S.')

-- 28 with mult distcodes
select name, employeenumber
from (
  select name, employeenumber, distcode, distribution 
  from emp_1 a
  group by name, employeenumber, distcode, distribution ) x
group by name, employeenumber having count(*) > 1

select a.*
from emp_1 a
inner join (
  select name, employeenumber
  from (
    select name, employeenumber, distcode, distribution 
    from emp_1 a
    group by name, employeenumber, distcode, distribution ) x
  group by name, employeenumber 
  having count(*) > 1) b on a.name = b.name and a.employeenumber = b.employeenumber
order by a.name



-- 80 with mult hourlyrates
select name, employeenumber
from (
  select name, employeenumber, hourlyrate 
  from emp_1 a
  group by name, employeenumber, hourlyrate) x
group by name, employeenumber having count(*) > 1

select *
from emp_1
where name in (
select name
from (
select name, hourlyrate 
from emp_1 a
group by name, hourlyrate) x
group by name having count(*) > 1) 
order by name


-- 5/3
wait a fucking minute i dont need to do all that, just fucking use ads.ext_edw_employee_dim

See V2
