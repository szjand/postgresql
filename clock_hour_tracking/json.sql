﻿/*
using aftons function as the basis: jeri.json_get_store_clock_hours(citext);
reformatted aftons function: jeri.zjon_test_json_get_store_clock_hours(citext) no other changes
  just, for me, easier to read, probably because i coded it.
refactor to use cte, filter
hoping to get some clarity on how this shit works

select * from jeri.clock_hour_tracking

*/

do
$$
declare 
  _year_month integer := (
    select year_month
    from dds.dim_date
    where the_date = current_date
    group by year_month);  
     
  _year integer := extract(year from current_date);
  
begin
drop table if exists wtf;
create temp table wtf as
  with 
    stores as (
      select store_code,
        json_build_object( -- ytd
          'regular_hours',sum(regular_hours),
          'ot_hours', sum(ot_hours), 
          'ot_comp', sum(ot_comp)) as ytd_stats,
        json_build_object( -- mtd
          'regular_hours', sum(regular_hours) filter (where year_month = _year_month),                      
          'ot_hours', sum(ot_hours) filter (where year_month = _year_month),
          'ot_comp', sum(ot_comp) filter (where year_month = _year_month)) as mtd_stats       
      from jeri.clock_hour_tracking 
      where the_year = _year
      group by store_code),
    departments as (
      select store_code, department,
        json_build_object( --ytd
          'regular_hours',sum(regular_hours),
          'ot_hours', sum(ot_hours), 
          'ot_comp', sum(ot_comp)) as ytd_stats,
        json_build_object( -- mtd
          'regular_hours', sum(regular_hours) filter (where year_month = _year_month),                      
          'ot_hours', sum(ot_hours) filter (where year_month = _year_month),
          'ot_comp', sum(ot_comp) filter (where year_month = _year_month)) as mtd_stats       
      from jeri.clock_hour_tracking 
      group by store_code, department),  
    dist_codes as (
      select e.*, mtd_days_worked, mtd_fte, ytd_days_worked, ytd_fte
      from (
        select store_code, department, dist_code, dist_code_description,
          json_build_object( -- ytd
            'regular_hours',sum(regular_hours),
            'ot_hours', sum(ot_hours), 
            'ot_comp', sum(ot_comp)) as ytd_stats,
          json_build_object( -- mtd
            'regular_hours', sum(regular_hours) filter (where year_month = _year_month),                      
            'ot_hours', sum(ot_hours) filter (where year_month = _year_month),
            'ot_comp', sum(ot_comp) filter (where year_month = _year_month)) as mtd_stats          
        from jeri.clock_hour_tracking 
        group by store_code, department, dist_code, dist_code_description) e 
      left join (  
        select store_code, department, dist_code, 
          b.mtd_days_worked, round(mtd_hours / (8 * b.mtd_days_worked), 1) as mtd_fte,
          b.ytd_days_worked,  round(a.ytd_hours / (8 * b.ytd_days_worked), 1) as ytd_fte
        from ( -- dist_code hours
          select store_code, department, dist_code,
            sum(regular_hours + ot_hours) as ytd_hours,
            sum(regular_hours + ot_hours) filter (where year_month = _year_month) as mtd_hours     
          from jeri.clock_hour_tracking 
          where the_year = _year 
          group by store_code, department, dist_code) a
        cross join ( -- days worked
          select wd_of_month_elapsed as mtd_days_worked, wd_of_year_elapsed as ytd_days_worked 
          from dds.dim_date 
          where the_date = current_date) b) f on e.store_code = f.store_code
            and e.department = f.department
            and e.dist_code = f.dist_code),
    employees as (
      select store_code, department, dist_code, dist_code_description,
        employee_name, employee_number,
        json_build_object( -- ytd
          'regular_hours',sum(regular_hours),
          'ot_hours', sum(ot_hours), 
          'ot_comp', sum(ot_comp)) as ytd_stats,
        json_build_object( --  mtd
          'regular_hours', sum(regular_hours) filter (where year_month = _year_month),                      
          'ot_hours', sum(ot_hours) filter (where year_month = _year_month),
          'ot_comp', sum(ot_comp) filter (where year_month = _year_month)) as mtd_stats           
      from jeri.clock_hour_tracking
      group by store_code, department, dist_code, dist_code_description,
        employee_name, employee_number)

select row_to_json(z)
from ( -- z
  select row_to_json(y) as time_clock_tracking
  from ( -- y
    select json_agg(row_to_json(x)) as stores
    from ( -- x: stores aa
      select *,
        ( -- w depts
          select json_agg(row_to_json(w))
          from ( -- bb departments
            select department, mtd_stats, ytd_stats,
              ( -- v dist codes
                select json_agg(row_to_json(v))
                from ( -- cc dist codes
                  select dist_code, dist_code_description, mtd_stats, ytd_stats,
                    mtd_days_worked, mtd_fte, ytd_days_worked, ytd_fte,
                    ( -- u employee
                      select json_agg(row_to_json(u))
                      from ( -- dd employees
                        select employee_name, employee_number,  mtd_stats, ytd_stats
                        from employees dd
                        where store_code = aa.store_code
                          and department = bb.department
                          and dist_code = cc.dist_code) u) as employee
                  from dist_codes cc
                  where store_code = bb.store_code
                    and department = bb.department) v) as dist_codes
            from departments bb
            where store_code = aa.store_code) w) as depts
      from stores aa) x) y) z;
      


end;
$$;        

select * from wtf;


