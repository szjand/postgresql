﻿5/2/18
ok, i expected this
this is what fucked me up in dds that i never fixed
for some unknown reason, at the new month, sales dist code distributions change
which fucks up arkona.xfm_pyactg which has a PK of company/dist_code/seq
so, conditional uniqique index where current_row = true ?

drop index arkona.xfm_pyactgr_company_number_dist_code_seq_number_idx cascade;
create unique index on arkona.xfm_pyactgr(company_number, dist_code, seq_number) where current_row = true;

ok, that worked, at least as far as updating for today, where ry2/Sale has different gross_dist

select *
from arkona.xfm_pyactgr
where company_number = 'RY2'
  and dist_code = 'sale'

but, wtf, why no current_row for ry2-cust-seq 1,2,3
makes no sense, let it run again tonight with new extract and we will see what it looks like
its not just CUST, but a shitload of RY2 dist codes

select *
from arkona.xfm_pyactgr
where company_number = 'RY2'
  and dist_code = 'cust'

    
select *
from arkona.xfm_pyactgr a
where exists (
select 1
from arkona.xfm_pyactgr
where current_row = false
  and company_number = a.company_number
  and dist_code = a.dist_code
  and seq_number = a.seq_number)
order by company_number, dist_code, seq_number  


  
insert into arkona.xfm_pyactgr (
  company_number,dist_code,seq_number,description,dist_co_,gross_dist,
  gross_expense_act_,overtime_act_,vacation_expense_act_,holiday_expense_act_,
  sick_leave_expense_act_,retire_expense_act_,emplr_fica_expense,
  emplr_med_expense,federal_un_emp_act_,state_unemp_act_,state_comp_act_,
  state_sdi_act_,emplr_contributions,
  row_from_date, current_row, hash)
select company_number,dist_code,seq_number,description,dist_co_,gross_dist,
  gross_expense_act_,overtime_act_,vacation_expense_act_,holiday_expense_act_,
  sick_leave_expense_act_,retire_expense_act_,emplr_fica_expense,
  emplr_med_expense,federal_un_emp_act_,state_unemp_act_,state_comp_act_,
  state_sdi_act_,emplr_contributions,
      -- skip pyactgr_key: serial
  current_date as row_from_date,
  -- skip row_thru_date: default value
  true as current_row,
    (
      select md5(z::text) as hash
      from (
        select company_number,dist_code,seq_number,description,dist_co_,gross_dist,
          gross_expense_act_,overtime_act_,vacation_expense_act_,holiday_expense_act_,
          sick_leave_expense_act_,retire_expense_act_,emplr_fica_expense,
          emplr_med_expense,federal_un_emp_act_,state_unemp_act_,state_comp_act_,
          state_sdi_act_,emplr_contributions
        from arkona.ext_pyactgr
        where company_number = a.company_number
          and dist_code = a.dist_code
          and seq_number = a.seq_number) z)
from arkona.ext_pyactgr a
where not exists (
  select 1
  from arkona.xfm_pyactgr
  where company_number = a.company_number
    and dist_code = a.dist_code
    and seq_number = seq_number);

-- changed rows
truncate arkona.xfm_pyactgr_changed_rows;
insert into arkona.xfm_pyactgr_changed_rows
select a.company_number, a.dist_code, a.seq_number
from (
  select company_number, dist_code, seq_number, 
    (
      select md5(z::text) as hash
      from (
        select company_number,dist_code,seq_number,description,dist_co_,gross_dist,
          gross_expense_act_,overtime_act_,vacation_expense_act_,holiday_expense_act_,
          sick_leave_expense_act_,retire_expense_act_,emplr_fica_expense,
          emplr_med_expense,federal_un_emp_act_,state_unemp_act_,state_comp_act_,
          state_sdi_act_,emplr_contributions
        from arkona.ext_pyactgr
        where company_number = aa.company_number
          and dist_code = aa.dist_code
          and seq_number = aa.seq_number) z)
  from arkona.ext_pyactgr aa) a
inner join arkona.xfm_pyactgr b on a.company_number = b.company_number
  and a.dist_code = b.dist_code
  and a.seq_number = b.seq_number
  and a.hash <> b.hash
  and b.current_row = true;
-- 
-- update current version of changed row
update arkona.xfm_pyactgr x
set current_row = false,
    row_thru_date = current_date - 1
from arkona.xfm_pyactgr_changed_rows z
where x.company_number = z.company_number
  and x.dist_code = x.dist_code
  and x.seq_number = z.seq_number
  and x.current_row = true;

-- -- add new row for each changed row     
-- insert into arkona.xfm_pyactgr (
--   company_number,dist_code,seq_number,description,dist_co_,gross_dist,
--   gross_expense_act_,overtime_act_,vacation_expense_act_,holiday_expense_act_,
--   sick_leave_expense_act_,retire_expense_act_,emplr_fica_expense,
--   emplr_med_expense,federal_un_emp_act_,state_unemp_act_,state_comp_act_,
--   state_sdi_act_,emplr_contributions,
--   row_from_date, current_row, hash)
-- select company_number,dist_code,seq_number,description,dist_co_,gross_dist,
--   gross_expense_act_,overtime_act_,vacation_expense_act_,holiday_expense_act_,
--   sick_leave_expense_act_,retire_expense_act_,emplr_fica_expense,
--   emplr_med_expense,federal_un_emp_act_,state_unemp_act_,state_comp_act_,
--   state_sdi_act_,emplr_contributions,
--       -- skip pyactgr_key: serial
--   current_date as row_from_date,
--   -- skip row_thru_date: default value
--   true as current_row,
--     (
--       select md5(z::text) as hash
--       from (
--         select company_number,dist_code,seq_number,description,dist_co_,gross_dist,
--           gross_expense_act_,overtime_act_,vacation_expense_act_,holiday_expense_act_,
--           sick_leave_expense_act_,retire_expense_act_,emplr_fica_expense,
--           emplr_med_expense,federal_un_emp_act_,state_unemp_act_,state_comp_act_,
--           state_sdi_act_,emplr_contributions
--         from arkona.ext_pyactgr
--         where company_number = x.company_number
--           and dist_code = x.dist_code
--           and seq_number = x.seq_number) z)
-- from (
--   select a.*
--   from arkona.ext_pyactgr a
--   inner join arkona.xfm_pyactgr_changed_rows b on a.company_number = b.company_number
--     and a.dist_code = b.dist_code
--     and a.seq_number = b.seq_number) x;

select * from  arkona.xfm_pyactgr_changed_rows

select * from  arkona.xfm_pyactgr where current_row = false

select * from arkona.ext_pyactgr where company_number = 'ry2' and dist_code = 'sale'



  select 'ext', company_number, dist_code, seq_number, 
    (
      select md5(z::text) as hash
      from (
        select company_number,dist_code,seq_number,description,dist_co_,gross_dist,
          gross_expense_act_,overtime_act_,vacation_expense_act_,holiday_expense_act_,
          sick_leave_expense_act_,retire_expense_act_,emplr_fica_expense,
          emplr_med_expense,federal_un_emp_act_,state_unemp_act_,state_comp_act_,
          state_sdi_act_,emplr_contributions
        from arkona.ext_pyactgr
        where company_number = aa.company_number
          and dist_code = aa.dist_code
          and seq_number = aa.seq_number) z)
  from arkona.ext_pyactgr aa
   where company_number = 'ry2' and dist_code = 'sale'
union
select 'xfm', company_number, dist_code, seq_number, hash
from arkona.xfm_pyactgr
where company_number = 'ry2' and dist_code = 'sale'


select 'ext', aa.company_number,dist_code,seq_number,description,dist_co_,gross_dist,
          gross_expense_act_,overtime_act_,vacation_expense_act_,holiday_expense_act_,
          sick_leave_expense_act_,retire_expense_act_,emplr_fica_expense,
          emplr_med_expense,federal_un_emp_act_,state_unemp_act_,state_comp_act_,
          state_sdi_act_,emplr_contributions, 
    (
      select md5(z::text) as hash
      from (
        select company_number,dist_code,seq_number,description,dist_co_,gross_dist,
          gross_expense_act_,overtime_act_,vacation_expense_act_,holiday_expense_act_,
          sick_leave_expense_act_,retire_expense_act_,emplr_fica_expense,
          emplr_med_expense,federal_un_emp_act_,state_unemp_act_,state_comp_act_,
          state_sdi_act_,emplr_contributions
        from arkona.ext_pyactgr
        where company_number = aa.company_number
          and dist_code = aa.dist_code
          and seq_number = aa.seq_number) z)                    
  from arkona.ext_pyactgr aa
   where company_number = 'ry2' and dist_code = '905'
union all

select 'xfm', aa.company_number,dist_code,seq_number,description,dist_co_,gross_dist,
          gross_expense_act_,overtime_act_,vacation_expense_act_,holiday_expense_act_,
          sick_leave_expense_act_,retire_expense_act_,emplr_fica_expense,
          emplr_med_expense,federal_un_emp_act_,state_unemp_act_,state_comp_act_,
          state_sdi_act_,emplr_contributions, hash
  from arkona.xfm_pyactgr aa
   where company_number = 'ry2' and dist_code = '905'   
order by seq_number

5/2/18
ok, it did not heal overnight
there is no reason that i can detect that ry2 CUST should have any non current rows, nothing has fucking changed
its like something changed on 4/30 or 5/1, but then immediately changed back i do not understand
so, i will try to fix them and wait until the end of may to figure whats going on i guess
double check everything before changing in arkona

also need to ask jeri about why sale is changed

-- ry2/cust
update arkona.xfm_pyactgr
set current_row = true,
    row_thru_date = '12/31/9999'
where company_number = 'ry2'
  and dist_code = 'CUST'  

update arkona.xfm_pyactgr
set current_row = true,
    row_thru_date = '12/31/9999'
where company_number = 'ry2'
  and dist_code = '905'  

update arkona.xfm_pyactgr
set current_row = true,
    row_thru_date = '12/31/9999'
where company_number = 'ry2'
  and dist_code in('DETA','DRI','MGR','OFFI','OWN','PART','PDQW','PTMG','SMGR','SRPQ','STA','STEA','SVR','TEAM','TECH','TPDQ')
  
-- SALE is the only one with a legitimate change
select *
from arkona.xfm_pyactgr a
where exists (
  select 1
  from arkona.xfm_pyactgr
  where current_row = false
    and company_number = a.company_number
    and dist_code = a.dist_code
    and seq_number = a.seq_number)
order by company_number, dist_code, seq_number  

6/2/18
same thing, i forgot, did not check this stuff on the first
jeri finished the fs yesterday (on the first), dont know if that matters
so, the dist code RY2 SALE actually changes
but all the others (ry2: 905, cust, deta, etc) get current row updated to false, but no new row is inserted

