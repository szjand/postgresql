﻿4/22/18
went down the allocation path here, the fesults are different but still not definitive
need to talk to jeri about what she wants, how she wants to deal with  calendar
month vs biweekly pay period




drop table if exists employees;
create table employees as
select a.pymast_company_number as store_code, a.pymast_employee_number as employee_number, 
  a.employee_name, 
  a.distrib_code, c.description as dist_code_description, 
  a.department_code, b.description as dept
from  arkona.xfm_pymast a
left join arkona.ext_pypclkctl b on a.department_code = b.department_code
  and a.pymast_company_number = b.company_number
left join arkona.xfm_pyactgr c on a.distrib_code = c.dist_code
  and a.pymast_company_number = c.company_number
where a.payroll_class = 'H'
  and a.department_code in ('06','03')
  and a.pymast_company_number in ('RY1','RY2')
  and c.gross_dist = 100
  and arkona.db2_integer_to_date(a.termination_date) > '12/31/2017'
group by a.pymast_company_number, a.pymast_employee_number, 
  a.employee_name, 
  a.distrib_code, c.description, 
  a.department_code, b.description;
create unique index on employees(store_code,employee_number,employee_name,distrib_code,
  dist_code_description,department_code,dept);


-- clock hours by month
drop table if exists clock_hours;
create temp table clock_hours as
select a.store_code, a.dept, a.distrib_code, a.dist_code_description, 
  c.year_month, a.employee_name, a.employee_number,
  sum(reg_hours) as reg_hours, sum(ot_hours) as ot_hours
from employees a
left join arkona.xfm_pypclockin b on a.employee_number = b.employee_number
  and b.the_date > '12/31/2017'
left join dds.dim_date c on b.the_date = c.the_date  
group by  a.store_code, a.dept, a.distrib_code, a.dist_code_description, 
  a.dept, c.year_month, a.employee_name, a.employee_number
having sum(reg_hours) > 0
order by year_month, store_code, dept, dist_code_description, dept, employee_name;



select biweekly_pay_period_start_date, biweekly_pay_period_end_date
from dds.dim_date
where the_date between '03/01/2018' and current_date
group by biweekly_pay_period_start_date, biweekly_pay_period_end_date
order by biweekly_pay_period_start_date, biweekly_pay_period_end_date


pay periods             check_date  batch   period ending   pyptbdta.payroll_start_date
2018-02-18;2018-03-03   03/09       309180    03/03
2018-03-04;2018-03-17   03/23       323180    03/17
2018-03-18;2018-03-31   04/06       406180    04/01
2018-04-01;2018-04-14   04/20       420180    04/15             04/01


select * from arkona.ext_pyptbdta where payroll_run_number = 331180

-- pyptbdta.payroll_start_date are all valid biweekly_pay_period_start_date-s
select distinct payroll_run_number, payroll_start_date, arkona.db2_integer_to_date_long(payroll_start_date), 
  (select bspp.valid_from_date(arkona.db2_integer_to_date_long(payroll_start_date))),
  payroll_start_date, arkona.db2_integer_to_date_long(payroll_start_date) + 13
from arkona.ext_pyptbdta
where payroll_cen_year = 118
  and selected_pay_per_ = 'B'

this is good, but i continually mind fuck on the mismatch between pay periods and year month

so, take a look at  accounting, i have the gross account in pyactgr

select * from arkona.xfm_pyactgr where dist_code = 'part' limit 100

select aa.*, b.gross_expense_act_
from (
  select a.store_code, a.employee_number, a.employee_name, distrib_code
  from employees a
  group by a.store_code, a.employee_number, employee_name, distrib_code) aa
left join arkona.xfm_pyactgr b on aa.store_code = b.company_number
  and aa.distrib_code = b.dist_code
left join fin.dim_account c on b.gross_expense_act_ = c.account
left join   


select b.the_date, c.account, a.amount, c.description, d.journal, e.doc_type, f.description, account_type
-- select distinct account 
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201803
inner join fin.dim_account c on a.account_key = c.account_key  
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_doc_type e on a.doc_type_key = e.doc_type_key
inner join fin.dim_gl_description f on a.gl_description_key = f.gl_description_key
where post_status = 'Y'
  and journal = 'payroll'
  and a.control = '16250'
order by account  


-- ok gross_Expense_acct is the total gross pay
-- and, of course, it turns out that the accounting date is the same as the check date

select b.the_date, c.account, sum(a.amount)-- , sum(a.amount) over (partition by the_date)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201803
inner join fin.dim_account c on a.account_key = c.account_key  
  and c.account in ('12306B','12406','12906', '12506')
where post_status = 'Y'
  and a.control = '16250'
group by b.the_date, c.account
order by b.the_date, c.account  


-- so some kind of of allocating ...
.dim_date
add column biweekly_pay_period_year_month_pct numeric(5,4);
comment on column dds.dim_date.biweekly_pay_period_year_month_pct is 'the percentage of the pay period in the year month, round((count(the_date) over (partition by year_month, biweekly_pay_period_start_date))/14.0, 4)';

create temp table new_value as
select the_date, biweekly_pay_period_start_date, biweekly_pay_period_end_date, year_month,
  biweekly_pay_period_sequence,
  round((count(the_date) over (partition by year_month, biweekly_pay_period_start_date))/14.0, 4) as biweekly_pay_period_year_month_pct
from dds.dim_date;

update dds.dim_date z
set biweekly_pay_period_year_month_pct = x.biweekly_pay_period_year_month_pct
from (
  select *
  from new_value) x
where z.the_date = x.the_date ;  

so, lets refigure ot_comp with this new info
  

drop table if exists ot_comp;
create temp table ot_comp as
select aa.*, bb.year_month, bb.ot_comp
from (
  select a.employee_number, a.employee_name
  from employees a
  group by a.employee_number, employee_name) aa
left join (
  select a.employee_name, a.employee_, sum(b.amount) as ot_comp,
    100 * (2000 + (a.payroll_cen_year - 100)) + a.check_month as year_month
  from arkona.ext_pyhshdta a
  inner join arkona.ext_pyhscdta b on a.payroll_run_number = b.payroll_run_number
    and a.company_number = b.company_number
    and a.employee_ = b.employee_number
    and b.code_type in ('0','1') -- 0,1: income, 2: deduction
    and b.code_id = 'OVT'
  where a.payroll_cen_year > 116 
  group by a.employee_name, a.employee_, a.check_month, a.payroll_cen_year, b.description) bb on aa.employee_number = bb.employee_
where bb.ot_comp is not null ; 


-- pyptbdta.payroll_start_date are all valid biweekly_pay_period_start_date-s
select distinct payroll_run_number, payroll_start_date, arkona.db2_integer_to_date_long(payroll_start_date), 
  (select bspp.valid_from_date(arkona.db2_integer_to_date_long(payroll_start_date))),
  payroll_start_date, arkona.db2_integer_to_date_long(payroll_start_date) + 13
from arkona.ext_pyptbdta
where payroll_cen_year = 118
  and selected_pay_per_ = 'B'

select biweekly_pay_period_start_date, biweekly_pay_period_end_date, year_month,
  biweekly_pay_period_sequence, biweekly_pay_period_year_month_pct
from dds.dim_date  
where the_year between 2017 and 2018
group by biweekly_pay_period_start_date, biweekly_pay_period_end_date, year_month,
  biweekly_pay_period_sequence, biweekly_pay_period_year_month_pct

-- i think this is it
select a.employee_name, a.employee_, bb.year_month,
 sum(round(b.amount * biweekly_pay_period_year_month_pct, 2)) as amount
from arkona.ext_pyhshdta a
inner join arkona.ext_pyptbdta aa on a.payroll_run_number = aa.payroll_run_number
  and a.company_number = aa.company_number
  and aa.selected_pay_per_ = 'B'
inner join (
  select biweekly_pay_period_start_date, biweekly_pay_period_end_date, year_month,
  biweekly_pay_period_sequence, biweekly_pay_period_year_month_pct
  from dds.dim_date  
  where the_year between 2017 and 2018
  group by biweekly_pay_period_start_date, biweekly_pay_period_end_date, year_month,
    biweekly_pay_period_sequence, biweekly_pay_period_year_month_pct) bb on arkona.db2_integer_to_date_long(aa.payroll_start_date) = bb.biweekly_pay_period_start_date
inner join arkona.ext_pyhscdta b on a.payroll_run_number = b.payroll_run_number
  and a.company_number = b.company_number
  and a.employee_ = b.employee_number
  and b.code_type in ('0','1') -- 0,1: income, 2: deduction
  and b.code_id = 'OVT'
where a.payroll_cen_year > 116 
--   and a.employee_ = '10568'
group by a.employee_name, a.employee_, bb.year_month



drop table if exists ot_comp;
create temp table ot_comp as
select aa.*, bb.year_month, bb.amount
from (
  select a.employee_number, a.employee_name
  from employees a
  group by a.employee_number, employee_name) aa    
left join (
  select a.employee_name, a.employee_, bb.year_month,
   sum(round(b.amount * biweekly_pay_period_year_month_pct, 2)) as amount
  from arkona.ext_pyhshdta a
  inner join arkona.ext_pyptbdta aa on a.payroll_run_number = aa.payroll_run_number
    and a.company_number = aa.company_number
    and aa.selected_pay_per_ = 'B'
  inner join (
    select biweekly_pay_period_start_date, biweekly_pay_period_end_date, year_month,
    biweekly_pay_period_sequence, biweekly_pay_period_year_month_pct
    from dds.dim_date  
    where the_year between 2017 and 2018
    group by biweekly_pay_period_start_date, biweekly_pay_period_end_date, year_month,
      biweekly_pay_period_sequence, biweekly_pay_period_year_month_pct) bb on arkona.db2_integer_to_date_long(aa.payroll_start_date) = bb.biweekly_pay_period_start_date
  inner join arkona.ext_pyhscdta b on a.payroll_run_number = b.payroll_run_number
    and a.company_number = b.company_number
    and a.employee_ = b.employee_number
    and b.code_type in ('0','1') -- 0,1: income, 2: deduction
    and b.code_id = 'OVT'
  where a.payroll_cen_year > 116 
  --   and a.employee_ = '10568'
  group by a.employee_name, a.employee_, bb.year_month) bb on aa.employee_number = bb.employee_ ;
  

select a.*, coalesce(b.amount, 0)
from clock_hours a
left join ot_comp b on a.employee_number = b.employee_number
  and a.year_month = b.year_month

still generating aker 10568 201803 0 ot_hours, but 3.16 ot comp

check paycheck clock hours vs 


select a.company_number, a.employee_, a.total_gross_pay, a.reg_hours, a.overtime_hours,
  b.payroll_run_number, arkona.db2_integer_to_date_long(b.payroll_start_date) as payroll_start_date, b.payroll_end_date, b.check_date
-- select *
from arkona.ext_pyhshdta a 
inner join arkona.ext_pyptbdta b on a.company_number = b.company_number
  and a.payroll_run_number = b.payroll_run_number
where a.payroll_Cen_year = 118 and a.employee_ = '10568'


select biweekly_pay_period_start_date, biweekly_pay_period_end_date, year_month,
  biweekly_pay_period_sequence, biweekly_pay_period_year_month_pct
from dds.dim_date
where the_date between '12/24/2017' and current_date
group by biweekly_pay_period_start_date, biweekly_pay_period_end_date, year_month,
  biweekly_pay_period_sequence, biweekly_pay_period_year_month_pct

select *
from clock_hours
where employee_number = '10568'



-- clock hours by month
drop table if exists clock_hours;
create temp table clock_hours as
select a.store_code, a.dept, a.distrib_code, a.dist_code_description, 
  c.year_month, a.employee_name, a.employee_number,
  sum(reg_hours) as reg_hours, sum(ot_hours) as ot_hours
from employees a
left join arkona.xfm_pypclockin b on a.employee_number = b.employee_number
  and b.the_date > '12/31/2017'
left join dds.dim_date c on b.the_date = c.the_date  
group by  a.store_code, a.dept, a.distrib_code, a.dist_code_description, 
  a.dept, c.year_month, a.employee_name, a.employee_number
having sum(reg_hours) > 0
order by year_month, store_code, dept, dist_code_description, dept, employee_name;


-- add ot comp from pyhscdta
select a.*, b.total_gross_pay, b.total_gross_pay, b.reg_hours, b.overtime_hours, b.payroll_Start_date, b.payroll_end_date, b.check_date,
  sum(a.reg_hours) over (partition by biweekly_pay_period_start_date) as reg_pp, sum(a.ot_hours) over (partition by biweekly_pay_period_start_date) as ot_pp,
  sum(a.reg_hours) over (partition by year_month) as reg_month
from (
  select employee_number, a.the_Date, reg_hours, ot_hours, biweekly_pay_period_start_date, biweekly_pay_period_end_date, b.year_month, b.biweekly_pay_period_year_month_pct
  from arkona.xfm_pypclockin a
  left join dds.dim_date b  on a.the_date = b.the_date
  where employee_number = '10568'
    and a.the_date between '12/24/2017' and current_date) a
left join (
  select a.company_number, a.employee_, a.total_gross_pay, a.reg_hours, a.overtime_hours,
    b.payroll_run_number, arkona.db2_integer_to_date_long(b.payroll_start_date) as payroll_start_date, b.payroll_end_date, b.check_date
  -- select *
  from arkona.ext_pyhshdta a 

    
  inner join arkona.ext_pyptbdta b on a.company_number = b.company_number
    and a.payroll_run_number = b.payroll_run_number
  where a.payroll_Cen_year = 118 and a.employee_ = '10568') b on a.biweekly_pay_period_start_date = b.payroll_start_date

  