﻿-- payroll dept
-- current census
drop table if exists payroll_depts;
create temp table payroll_depts as
select a.pymast_company_number, a.department_code, b.description as dept_desc, 
  a.distrib_code, c.description as dist_desc,
  count(active_code) filter (where active_code = 'A') as full_time,
  count(active_code) filter (where active_code = 'P') as part_time
from arkona.xfm_pymast a
left join arkona.ext_pypclkctl b on a.department_code = b.department_code
  and a.pymast_Company_number = b.company_number
left join arkona.xfm_pyactgr c on a.pymast_company_number = c.company_number
  and a.distrib_code = c.dist_code
  and c.seq_number = 1  
where a.pymast_company_number in ('RY1','RY2')
  and a.payroll_class = 'H'
  and a.active_code <> 'T'
  and a.current_row = true
group by a.pymast_company_number, a.department_code, b.description, a.distrib_code, c.description
order by a.pymast_company_number, dept_desc, a.distrib_code;

-- add info from pyactg
select a.*, b.description, b.gross_dist, b.gross_expense_act_, c.description, c.department_code, c.department
from payroll_depts a
left join arkona.xfm_pyactgr b on a.distrib_code = b.dist_code
  and a.pymast_company_number = b.company_number
--   and gross_dist = 100
left join fin.dim_account c on b.gross_expense_act_ = c.account
  and c.current_row = true  
order by a.pymast_company_number, a.dept_desc, a.distrib_code;


select *
from arkona.xfm_pyactgr
limit 10

select *
from arkona.xfm_pymast a
left join arkona.xfm_pyactgr c on a.pymast_company_number = c.company_number
  and a.distrib_code = c.dist_code
where a.pymast_company_number in ('RY1','RY2')
  and a.payroll_class = 'H'
  and a.active_code <> 'T'
  and a.current_row = true


select a.storecode, d.department, a.distcode, a.distribution, a.name, a.employeenumber, 
  a.hourlyrate, a.termdate, a.employeekeyfromdate, a.employeekeythrudate, d.account, c.gross_dist/100
--   c.gross_dist, gross_expense_act_, 
--   d.department_code, d.department
from ads.ext_edw_employee_dim a
inner join arkona.xfm_pyactgr c on a.distcode = c.dist_code
  and a.storecode = c.company_number
  and c.current_row = true
--   and c.gross_dist = 100
inner join fin.dim_account d on c.gross_expense_act_ = d.account
  and d.current_row = true
where a.payrollClassCode = 'H'
--   and a.pydeptcode in ('06','03')
  and a.storecode in ('RY1','RY2')
  and a.termdate > '01/01/2017'
  and a.employeekeythrudate > '12/31/2017'
order by employeenumber


-- add info from pyactg, just add a count, want 1 row/dist code, count > 1 indicates split distcode
-- unfortunately there are distrib codes that are used for multiple departments
select a.pymast_company_number, a.department_code, a.dept_desc, a.distrib_code, a.dist_desc, a.full_time, a.part_time, count(*)
from payroll_depts a
left join arkona.xfm_pyactgr b on a.distrib_code = b.dist_code
  and a.pymast_company_number = b.company_number
left join fin.dim_account c on b.gross_expense_act_ = c.account
  and c.current_row = true  
group by a.pymast_company_number, a.department_code, a.dept_desc, a.distrib_code, a.dist_desc, a.full_time, a.part_time  
-- ) x group by pymast_company_number, distrib_code having count(*) > 1
order by a.pymast_company_number, a.distrib_code;


-- what i am shooting for is, which dist codes have overtime, are they all single dept dist codes?

select a.store_code, a.employee_number, a.the_Date, a.ot_hours, b.department_code, b.distrib_code
drop table if exists dist_with_ot;
create temp table dist_with_ot as
select store_code, department_code, distrib_code, sum(ot_hours) as ot_hours
from arkona.xfm_pypclockin a
left join arkona.xfm_pymast b on a.store_code = b.pymast_company_number
  and a.employee_number = b.pymast_employee_number
where ot_hours <> 0
--   and the_date > '12/31/2017'
group by store_code, department_code, distrib_code;

-- dist codes with ot, goes back to beginning of 2017
select *
from ( 
  select a.pymast_company_number, a.department_code, a.dept_desc, a.distrib_code, a.dist_desc, a.full_time, a.part_time, count(*)
  from payroll_depts a
  left join arkona.xfm_pyactgr b on a.distrib_code = b.dist_code
    and a.pymast_company_number = b.company_number
  left join fin.dim_account c on b.gross_expense_act_ = c.account
    and c.current_row = true  
  group by a.pymast_company_number, a.department_code, a.dept_desc, a.distrib_code, a.dist_desc, a.full_time, a.part_time ) aa
left join dist_with_ot bb on aa.pymast_company_number = bb.store_code
  and aa.department_code =  bb.department_code
  and aa.distrib_code = bb.distrib_code 
order by ot_hours desc


-- 5/31 sent this to jeri as dist codes with ot
select c.company_number, c.dist_code, c.description, c.gross_dist, c.gross_expense_act_, d.department as acct_dept,
  f.description as payroll_dept, round(e.ot_hours * (c.gross_dist/100), 2) as ot_hours
from arkona.xfm_pyactgr c
left join (
  select b.store, b.department_code, b.department, a.gross_expense_act_
  from (
    select distinct gross_expense_act_
    from arkona.xfm_pyactgr) a
  left join fin.dim_account b on a.gross_expense_act_ = b.account) d on c.gross_expense_act_ = d.gross_expense_act_
left join (
  select store_code, department_code, distrib_code, sum(ot_hours) as ot_hours
  from arkona.xfm_pypclockin a
  left join arkona.xfm_pymast b on a.store_code = b.pymast_company_number
    and a.employee_number = b.pymast_employee_number
    and current_row = true
    and active_code <> 'T'
  group by store_code, department_code, distrib_code) e on c.company_number = e.store_code and c.dist_code = e.distrib_code  
left join arkona.ext_pypclkctl f on e.store_code = f.company_number and e.department_code = f.department_code
where c.current_row = true
  and coalesce(e.ot_hours, 0) > 0
order by e.ot_hours desc



