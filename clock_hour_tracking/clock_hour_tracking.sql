﻿-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
-- started out as first_cut_parts_pdq_V3.dql
-- 5/7/18, dept based on pypclkctl, payroll departments, no split dist codes
-- 6/29 dropped hourly_employee PK, included all dist codes
-- 7/7 removed join on pyactgr, reinstated PK on jeri.hourly_employees
-- going with payroll departments
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
drop table if exists jeri.hourly_employees cascade;
create table jeri.hourly_employees (
  store_code citext not null,
  department citext not null,
  dist_code citext not null,
  dist_code_description citext not null,
  employee_name citext not null,
  employee_number citext not null, 
  hourly_rate numeric(8,2) not null,
  term_date date not null,
  ek_from_date date not null,
  ek_thru_date date not null,
  primary key (employee_number, ek_from_date));

  
insert into jeri.hourly_employees  
select a.storecode, a.pydept, a.distcode, a.distribution, a.name, a.employeenumber, 
  a.hourlyrate, a.termdate, a.employeekeyfromdate, a.employeekeythrudate
from ads.ext_edw_employee_dim a
where a.payrollClassCode = 'H'
  and a.storecode in ('RY1','RY2')
  and a.termdate > '01/01/2017'
  and a.employeekeythrudate > '12/31/2016'
order by a.storecode, a.pydept, a.distcode, a.distribution, a.name;

select *
from jeri.hourly_employees
order by employee_name

select * from  jeri.hourly_employees where employee_name like '%axtman%'

select * from  jeri.clock_hour_tracking where employee_name like '%axtman%'


select department, count(*) from  jeri.clock_hour_tracking group by department

 drop table if exists jeri.clock_hour_tracking;
create table jeri.clock_hour_tracking (
  store_code citext not null,
  department citext not null,
  dist_code citext not null,
  dist_code_description citext not null,
  the_year integer not null,
  year_month integer not null,
  employee_name citext not null,
  employee_number citext not null,
  regular_hours numeric (8,2) not null default 0,
  ot_hours numeric (8,2) not null default 0,
  ot_comp numeric (8,2) not null default 0,
  primary key (year_month, department, dist_code, employee_number));


-- this populates jeri.clock_hour_tracking
truncate jeri.clock_hour_tracking;
insert into jeri.clock_hour_tracking
select b.store_Code, b.department, b.dist_code, b.dist_code_description, 
  aa.the_year, aa.year_month, max(b.employee_name), b.employee_number,
  sum(a.reg_hours) as reg_hours, sum(a.ot_hours) as ot_hours, 
  sum(ot_hours * 1.5 * hourly_rate) as ot_comp
from arkona.xfm_pypclockin a
inner join dds.dim_date aa on a.the_date = aa.the_date
inner join jeri.hourly_employees b on a.employee_number = b.employee_number
  and a.the_date between b.ek_from_date and b.ek_thru_date
  -- 7/7 add term date
  and a.the_date <= b.term_date
where a.the_date between '01/01/2017' and current_date
group by  b.store_Code, b.department, b.dist_code, b.dist_code_description, 
  aa.the_year, aa.year_month, /*b.employee_name,*/ b.employee_number

-- 7/8 working on refactoring fuction that populates the page

 FUNCTION jeri.json_get_store_clock_hours_backup(citext);

 select jeri.json_get_store_clock_hours('RY1')



select year_month, store_code, department, count(*) from  jeri.clock_hour_tracking group by year_month, store_code, department
order by store_code, department, year_month


select *
from  jeri.clock_hour_tracking
where store_code = 'ry1'
  and department = 'service recon'
  and year_month = 201807



select store_code, department, employee_name
from jeri.clock_hour_tracking
group by store_code, department, employee_name
order by employee_name  


select year_month, store_code, count(*)
from (
select year_month, store_code, department
from jeri.clock_hour_tracking
group by year_month, store_code, department
) x group by year_month, store_code
order by store_code, year_month

select year_month, store_code, department
from jeri.clock_hour_tracking
where year_month = 201707 and store_code = 'ry1'
group by year_month, store_code, department
union all
select year_month, store_code, department
from jeri.clock_hour_tracking
where year_month = 201807 and store_code = 'ry1'
group by year_month, store_code, department
order by  store_code, department


!!!!!!!!!!!!!!!!!!!!!
7/8/18
to sum it up, there is no fucking ry1 department service in 201807, those folks are now in dept service tech 

-- 7/14/18

put truncate/populate of jeri.hourly_employees & jeri.clock_hour_tracking & 
  populating of jeri.clock_hour_tracking_daily into luigi


-- uh oh  
updated data this morning
and fucking year to date ot hours and ot comp decreased, MTD increased  
this makes no sense

start doing a daily log, shit there is already a table called clock_hour_Tracking_daily, what is in it?

select max(the_date) from jeri.clock_hour_tracking_daily  -- 7/6

select *
from (
  select store_code, department, year_month, sum(reg_hours) as reg_hours, sum(ot_hours) as ot_hours,
    sum(ot_comp) as ot_comp
  from jeri.clock_hour_tracking_daily
  where year_month < 201807
  group by store_code, department, year_month) a
full outer join (
  select store_code, department, year_month, sum(regular_hours) as reg_hours, sum(ot_hours) as ot_hours,
    sum(ot_comp) as ot_comp
  from jeri.clock_hour_tracking
  where year_month < 201807
  group by store_code, department, year_month) b on a.store_code = b.store_code 
    and a.department = b.department
    and a.year_month = b.year_month
order by a.year_month, a.store_code, a.department, a.year_month


    


select b.store_Code, b.department, b.dist_code, b.dist_code_description, 
  aa.the_year, aa.year_month, max(b.employee_name), b.employee_number,
  sum(a.reg_hours) as reg_hours, sum(a.ot_hours) as ot_hours, 
  sum(ot_hours * 1.5 * hourly_rate) as ot_comp
from arkona.xfm_pypclockin a
inner join dds.dim_date aa on a.the_date = aa.the_date
inner join jeri.hourly_employees b on a.employee_number = b.employee_number
  and a.the_date between b.ek_from_date and b.ek_thru_date
  -- 7/7 add term date
  and a.the_date <= b.term_date
where a.the_date between '01/01/2017' and current_date
group by  b.store_Code, b.department, b.dist_code, b.dist_code_description, 
  aa.the_year, aa.year_month, /*b.employee_name,*/ b.employee_number



select min(the_date) -- 201701
from arkona.xfm_pypclockin
limit 100

select *
from arkona.xfm_pypclockin
limit 100

create index on arkona.xfm_pypclockin(the_Date);
create index on ads.ext_edw_employee_dim(storecode);
create index on ads.ext_edw_employee_dim(pydept);
create index on ads.ext_edw_employee_dim(distribution);
create index on ads.ext_edw_employee_dim(payrollclasscode);
select * from dds.dim_Date limit 10

drop table if exists wtf;
create temp table wtf as
select current_date as run_date, a.the_year, a.year_month, a.the_date, b.employee_number, 
  b.clock_hours, b.reg_hours, b.ot_hours, b.vac_hours, 
  b.pto_hours, b.hol_hours, c.name, c.storecode, c.pydept, c.distcode, c.distribution, 
  c.payrollclasscode, c.hourlyrate
from dds.dim_date a
inner join arkona.xfm_pypclockin b on a.the_date = b.the_date
inner join ads.ext_edw_employee_dim c on b.employee_number = c.employeenumber
  and a.the_date between c.employeekeyfromdate and c.employeekeythrudate
where a.the_date between '01/01/2018' and current_date - 1;

-- and of course, this agrees to what is in vision today
select storecode, pydept, sum(reg_hours) as reg_hours, sum(ot_hours) as ot_hours,
  round(sum(1.5 * ot_hours * hourlyrate), 2) as ot_comp
from wtf
where payrollclasscode = 'H'
  and the_year = 2018
group by storecode, pydept

-- an issue that has to be dealt with: over the course of a single month, an employee may have
-- multiple names, multiple hourly rates, depts or dist code
-- is that even an issue because on any given day there is only one row for an employee

DROP TABLE if exists jeri.clock_hour_tracking_daily;
CREATE TABLE jeri.clock_hour_tracking_daily (
  run_date date not null, 
  the_year integer NOT NULL,
  year_month integer NOT NULL,
  the_date date NOT NULL,
  employee_number citext NOT NULL,
  reg_hours numeric(8,2) NOT NULL DEFAULT 0,
  ot_hours numeric(8,2) NOT NULL DEFAULT 0,
  ot_comp numeric(8,2) NOT NULL DEFAULT 0,
  employee_name citext not null,
  store_code citext NOT NULL,
  department citext NOT NULL,
  dist_code citext NOT NULL,
  dist_code_description citext NOT NULL,
  payroll_class_code citext not null,
  hourly_rate numeric(8,2),
  PRIMARY KEY (employee_number, the_date));

create index on jeri.clock_hour_tracking_daily(the_date);
create index on jeri.clock_hour_tracking_daily(store_code);
create index on jeri.clock_hour_tracking_daily(department);
create index on jeri.clock_hour_tracking_daily(dist_code_description);
create index on jeri.clock_hour_tracking_daily(payroll_class_code);

-- this needs to be entered daily, and then track changes in history
insert into jeri.clock_hour_tracking_daily
select current_date as run_date, a.the_year, a.year_month, a.the_date, b.employee_number, 
  b.reg_hours, b.ot_hours,  round(1.5 * ot_hours * hourlyrate, 2) as ot_comp,
  c.name, c.storecode, c.pydept, c.distcode, c.distribution, 
  c.payrollclasscode, c.hourlyrate
from dds.dim_date a
inner join arkona.xfm_pypclockin b on a.the_date = b.the_date
inner join ads.ext_edw_employee_dim c on b.employee_number = c.employeenumber
  and a.the_date between c.employeekeyfromdate and c.employeekeythrudate
where a.the_date between '01/01/2018' and current_date - 1;


select * from jeri.clock_hour_tracking_daily ;
select * from jeri.clock_hour_tracking;
select * from jeri.hourly_employees

truncate jeri.clock_hour_tracking;
truncate jeri.hourly_employees

-- 7/15
-- would have liked to start seeing day to day change, but i fucked up and when
-- tracking_daily ran, it truncated first, fixed that, start daily comparison tomorrow

-- tracking vs daily: month to date diff in ot_comp
-- ok, need to qualify payroll_class_code in tracking_daily, now all diffs are < .03
select *
from (
  select store_code, department, sum(reg_hours) as reg_hours, sum(ot_hours) as ot_hours,
    sum(ot_comp) as ot_comp
  from jeri.clock_hour_tracking_daily
  where run_date = '07/15/2018'
    and year_month = 201807
    and payroll_class_code = 'H'
  group by store_code, department) a
-- order by store_code, department
inner join (
  select store_code, department, sum(regular_hours) as reg_hours, sum(ot_hours) as ot_hours,
    sum(ot_comp) as ot_comp
  from jeri.clock_hour_tracking
  where year_month = 201807
  group by store_code, department) b on a.store_code = b.store_code 
    and a.department = b.department
    and a.ot_comp <> b.ot_comp

-- tracking vs daily: diff in group of employees
-- hmmm laurie mathisen not in tracking
select *
from (
  select employee_name, sum(reg_hours) as reg_hours, sum(ot_hours) as ot_hours,
    sum(ot_comp) as ot_comp
  from jeri.clock_hour_tracking_daily
  where run_date = '07/15/2018'
    and year_month = 201807
    and payroll_class_code = 'H'
  group by employee_name) a
-- order by store_code, department
full outer join (
  select employee_name, sum(regular_hours) as reg_hours, sum(ot_hours) as ot_hours,
    sum(ot_comp) as ot_comp
  from jeri.clock_hour_tracking
  where year_month = 201807
  group by employee_name) b on a.employee_name = b.employee_name
where a.employee_name is null or b.employee_name is null


-- daily vs daily with diff dates
select *
from (
  select store_code, department, sum(reg_hours) as reg_hours, sum(ot_hours) as ot_hours,
    sum(ot_comp) as ot_comp
  from jeri.clock_hour_tracking_daily
  where run_date = '07/15/2018'
    and year_month = 201807
    and payroll_class_code = 'H'
  group by store_code, department) a
-- order by store_code, department
inner join (
  select store_code, department, sum(reg_hours) as reg_hours, sum(ot_hours) as ot_hours,
    sum(ot_comp) as ot_comp
  from jeri.clock_hour_tracking_daily
  where run_date = '07/16/2018'
    and year_month = 201807
    and payroll_class_code = 'H'
  group by store_code, department) b on a.store_code = b.store_code 
    and a.department = b.department
    and a.ot_comp <> b.ot_comp

 emp# 118746
7/15: 6/16 reg: 8.44  ot: .72
7/16: 6/16 reg: 9.16  ot: 0
this one makes no sense
select *
from (
  select the_date, sum(reg_hours) as reg_hours, sum(ot_hours) as ot_hours,
    sum(ot_comp) as ot_comp
  from jeri.clock_hour_tracking_daily
  where run_date = '07/15/2018'
    and year_month = 201806
    and employee_name = 'MONTGOMERY, CODY'
  group by the_date) a    
inner join (
  select the_date, sum(reg_hours) as reg_hours, sum(ot_hours) as ot_hours,
    sum(ot_comp) as ot_comp
  from jeri.clock_hour_tracking_daily
  where run_date = '07/16/2018'
    and year_month = 201806
    and employee_name = 'MONTGOMERY, CODY'
  group by the_date) b on a.the_date = b.the_date

select * from jeri.hourly_employees where employee_name = 'MONTGOMERY, CODY'

select * from arkona.xfm_pypclockin where employee_number = '118746' and the_Date = '06/16/2018'
-----------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------- 
