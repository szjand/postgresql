﻿select a.*, b.employee_name, b.distrib_code
from arkona.ext_pypclockin a
left join arkona.ext_pymast b on a.pymast_employee_number = b.pymast_employee_number
order by yiclkind desc
limit 100


select *
from arkona.ext_pymast
where active_code <> 'T'
  and distrib_code = 'DRI'

-- temp_add_1: this is the auto deduct: A: 1 hour, B: 1/2 hour
select employee_name, pymast_employee_number, distrib_code, temp_add_1
from arkona.ext_pymast
where active_code <> 'T'
  and temp_add_1 <> ''  


-- ray franks
select *
from arkona.ext_pypclockin
where pymast_employee_number = '148275'
order by yiclkind desc
limit 100  



select a.*, b.employee_name, b.distrib_code, yiclkoutt - yiclkint, cc.*
from arkona.ext_pypclockin a
left join arkona.ext_pymast b on a.pymast_employee_number = b.pymast_employee_number
left join (
select b.the_date, c.employeenumber, c.name, a.*
from ads.ext_edw_clock_hours_fact a
inner join dds.dim_date b on a.datekey = b.date_key
inner join ads.ext_edw_employee_dim c on a.employeekey = c.employeekey) cc on a.yiclkind = cc.the_Date and a.pymast_employee_number = cc.employeenumber
order by yiclkind desc
limit 1000


select b.the_date, c.employeenumber, c.name, a.*
from ads.ext_edw_clock_hours_fact a
inner join dds.dim_date b on a.datekey = b.date_key
inner join ads.ext_edw_employee_dim c on a.employeekey = c.employeekey
limit 100





select a.*, yiclkoutt - yiclkint, extract(epoch from (yiclkoutt - yiclkint)), 
  extract(epoch from (yiclkoutt - yiclkint))/3600.0, (extract(epoch from (yiclkoutt - yiclkint))/3600.0)::numeric(6,2)
from arkona.ext_pypclockin a
where yiclkind between '01/01/2018' and '01/31/2018'
order by yiclkind desc
limit 100  


select distinct yicode from arkona.ext_pypclockin: I, HOL, 666, V, O, BNK, 66, PTO, VAC

select yicode, count(*) from arkona.ext_pypclockin group by yicode

need to do from scratch in pg because dds excludes 666 (auto deduct)
alright, looking at proc dds.fctClockHours, what i need is

pypclockin -> tmppypclockin -> tmpclockhours  \
                            -> tmpOvertime    /  -> fact


select max(the_date) - 28 as the_date
from dds.dim_date
where day_of_week = 1
  and the_date <= current_date


select max(yiclkind) from arkona.ext_pypclockin  -- 4/16

-- wtf, corey wilde clocks in only ?!?!?
select *
from arkona.ext_pypclockin_tmp
where yicode = 'I'

select *
from arkona.ext_pypclockin_tmp
where pymast_employee_number = '2150210'
order by yiclkind


select *
from arkona.ext_pymast
where pymast_employee_number = '2150210'
-------------------------------------------------
-- no rows where yiclkind <> yiclkoutd
select * 
from arkona.ext_pypclockin_tmp
where yiclkind <> yiclkoutd


-- aside from the temp table this looks good, ie, the data looks correct
-- 


create table arkona.xfm_pypclockin_tmp (
  yico_ citext not null,
  pymast_employee_number citext not null,
  yiclkind date not null,
  clock_hours numeric(6,2) not null,
  hol_hours numeric(6,2) not null,
  pto_hours numeric(6,2) not null,
  vac_hours numeric(6,2) not null,
  constraint xfm_pypclockin_tmp_pkey primary key (yico_,pymast_employee_number,yiclkind));

  
truncate arkona.xfm_pypclockin_tmp;
insert into arkona.xfm_pypclockin_tmp
select yico_, pymast_employee_number, yiclkind, 
  coalesce(sum((extract(epoch from (yiclkoutt - yiclkint))/3600.0)::numeric(6,2)) filter (where yicode in ('666','O')), 0) as clock_hours,
  coalesce(sum((extract(epoch from (yiclkoutt - yiclkint))/3600.0)::numeric(6,2)) filter (where yicode = 'HOL'), 0) as hol_hours,
  coalesce(sum((extract(epoch from (yiclkoutt - yiclkint))/3600.0)::numeric(6,2)) filter (where yicode = 'PTO'), 0) as pto_hours,
  coalesce(sum((extract(epoch from (yiclkoutt - yiclkint))/3600.0)::numeric(6,2)) filter (where yicode = 'VAC'), 0) as vac_hours
from ( -- d: clock hours
  select a.yico_, a.pymast_employee_number, a.yiclkind, a.yiclkint, a.yiclkoutt, a.yicode
  -- select *
  from arkona.ext_pypclockin_tmp a
  left join ( -- exclude overlap
    select yico_, pymast_employee_number, yiclkind, min(yiclkint), max(yiclkint), min(yiclkoutt), max(yiclkoutt)
    from arkona.ext_pypclockin_tmp
    where yicode in ('HOL','666','O','PTO','VAC')
    group by yico_, pymast_employee_number, yiclkind
    having count(*) > 1
      and max(yiclkint) < min(yiclkoutt)) b on a.yico_ = b.yico_
        and a.pymast_employee_number = b.pymast_employee_number
        and a.yiclkind = b.yiclkind
  left join ( -- exclude dup
    select yico_, pymast_employee_number, yiclkind, yiclkint, yicode
    from arkona.ext_pypclockin_tmp
    group by yico_, pymast_employee_number, yiclkind, yiclkint, yicode
    having count(*) > 1) c on a.yico_ = c.yico_
        and a.pymast_employee_number = c.pymast_employee_number
        and a.yiclkind = c.yiclkind  
  where a.yicode in ('HOL','666','O','PTO','VAC')
    and b.yico_ is null
    and c.yico_ is null) d
group by yico_, pymast_employee_number, yiclkind;


insert into arkona.xfm_pypclockin
select aa.yico_, aa.pymast_employee_number, aa.yiclkind, coalesce(aa.clock_hours, 0), 
  coalesce(bb.reg_hours, 0), coalesce(bb.ot_hours, 0),
  coalesce(aa.vac_hours, 0), coalesce(aa.pto_hours, 0), 
  coalesce(aa.hol_hours, 0)
from arkona.xfm_pypclockin_tmp aa
left join ( -- bb: reg/ot hours
  select a.yico_, a.pymast_employee_number, w.sunday_to_saturday_week, a.yiclkind,
    round(
      case
        when coalesce(sum(b.clock_hours), 0) + a.clock_hours < 40 then a.clock_hours
        when coalesce(sum(b.clock_hours), 0) > 40 then 0
        else 40 - coalesce(sum(b.clock_hours), 0)
      end, 2) as reg_hours,
    round(
      case
        when coalesce(sum(b.clock_hours), 0) >= 40 then a.clock_hours
        when coalesce(sum(b.clock_hours), 0) + a.clock_hours < 40 then 0
        else a.clock_hours - (40 -coalesce(sum(b.clock_hours), 0))
      end, 2) as ot_hours    
  from (  -- w: weeks
    select min(the_date) as from_date, max(the_date) as thru_date, sunday_to_saturday_week
    from dds.dim_date 
    where the_date between (
      select max(the_date) - 28 as the_date 
      from dds.dim_date
      where day_of_week = 1
        and the_date <= current_date) and current_date
    group by sunday_to_saturday_week) w
  left join ( -- a
    select * 
    from arkona.xfm_pypclockin_tmp) a on a.yiclkind between w.from_date and w.thru_date
  left join ( -- b  
    select * 
    from arkona.xfm_pypclockin_tmp) b on b.yiclkind between w.from_date and w.thru_date
      and a.yico_ = b.yico_
      and a.pymast_employee_number = b.pymast_employee_number
      and b.yiclkind < a.yiclkind -- this is the magix
  where a.pymast_employee_number is not null
  group by w.sunday_to_saturday_week, a.yico_, a.pymast_employee_number, a.yiclkind, a.clock_hours) bb on aa.yico_ = bb.yico_
    and aa.pymast_employee_number = bb.pymast_employee_number
    and aa.yiclkind = bb.yiclkind;


drop table if exists arkona.xfm_pypclockin;
create table arkona.xfm_pypclockin (
  store_code citext not null,
  employee_number citext not null,
  the_date date not null,
  clock_hours numeric(6,2) not null,
  reg_hours numeric(6,2) not null,
  ot_hours numeric(6,2) not null,
  vac_hours numeric(6,2) not null,
  pto_hours numeric(6,2) not null,
  hol_hours numeric(6,2) not null,
  constraint xfm_pypclockin_pkey primary key (store_code, employee_number,the_date));


delete 
from arkona.xfm_pypclockin
where the_date >= (
  select max(the_date) - 28 as the_date
  from dds.dim_date
  where day_of_week = 1
    and the_date <= current_date)






select yiclkind, count(*)
from arkona.ext_pypclockin_tmp
group by yiclkind
order by yiclkind



select * from arkona.xfm_pypclockin()


select * from  arkona.xfm_pypclockin;


delete from arkona.xfm_pypclockin


-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------

select employee_name, telephone_number, b.*
-- select *
from arkona.ext_pymast a
left join arkona.xfm_pypclockin b on a.pymast_employee_number = b.employee_number
  and b.the_date between '03/01/2018' and current_date
where active_code <> 'T'
  and distrib_code = 'DRI'  




select *
from dds.dim_date limit 10
where the_date between (select min(the_date) from dds.dim_Date where

select nrv.json_get_users()



drop function nrv.json_get_sales_drivers_clock_hours()
create or replace function nrv.json_get_sales_drivers_clock_hours()
returns setof json as
$BODY$
/*
select * from nrv.json_get_sales_drivers_clock_hours();
*/
select row_to_json(zz)
from (
  select json_agg(row_to_json(z)) as drivers
  from (
    with 
      the_week as (
        select sunday_to_saturday_week
        from dds.dim_date 
        where the_date = current_date),
      the_dates as (
        select min(the_date) as from_date, max(the_date) as thru_date
        from dds.dim_date
        where sunday_to_saturday_week = (
          select sunday_to_saturday_week
          from the_week)),
      the_week_of as (
        select a.mmdd || ' thru ' || b.mmdd as week_of
        from (
          select distinct mmdd 
          from dds.dim_date
          where the_date = (select from_date from the_dates)) a
        left join (  
          select distinct mmdd 
          from dds.dim_date
          where the_date = (select thru_date from the_dates)) b on 1 = 1)
        
    select a.employee_name, a.telephone_number, a.cell_phone_number, 
      (select * from the_week_of), 
      coalesce(sum(b.reg_hours), 0) as reg_hours, coalesce(sum(b.ot_hours), 0) as ot_hours
    from arkona.ext_pymast a
    left join arkona.xfm_pypclockin b on a.pymast_employee_number = b.employee_number
      and b.the_date between (select from_date from the_dates) and (select thru_date from the_dates)
    where active_code <> 'T'
      and distrib_code = 'DRI' 
    group by a.employee_name, a.telephone_number, a.cell_phone_number, 
      (select * from the_week_of)) z) zz;
$BODY$
language sql;      


-- make sure the shit is running
select the_date, count(*) from arkona.xfm_pypclockin  group by the_date order by the_date

-- 3/19
2018-03-12;214
2018-03-13;212
2018-03-14;219
2018-03-15;221
2018-03-16;209
2018-03-17;61
2018-03-18;7
2018-03-19;1

-------------------------------------------------------------------------------------------------
-- 4/15, jeri's clock hour tracking project, includes clockhours, ot hours, comp for hourly employees
--    by department/distcode
-- need pymast, pypactgr, going the xfm_pymast route (like with inpmast), where record every change to the table
-- on a daily basis
-- pypclockin has been started with function arkon.xfm_pypclockin()
  