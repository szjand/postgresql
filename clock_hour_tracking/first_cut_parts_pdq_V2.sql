﻿---------------------------------------------------------------------------------------------
/* 4/30

  talked to jeri
  the mismatch between month and pay period was too much, couple of ways to deal with it, 
  going with, calc ot_comp from hourly rate * clock hours


*/
---------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------        
select * from ops.ads_extract order by the_date desc, task asc limit 100


-- 
-- drop table if exists employees;
-- create table employees as
-- select a.storecode, a.pydept, a.distcode, a.distribution, a.name, a.employeenumber, 
--   a.hourlyrate, a.termdate, a.employeekeyfromdate, a.employeekeythrudate
-- -- select a.*
-- from ads.ext_edw_employee_dim a
-- where a.payrollClassCode = 'H'
--   and a.pydeptcode in ('06','03')
--   and a.storecode in ('RY1','RY2')
--   and a.termdate > '01/01/2017';
-- create unique index on employees(employeenumber, employeekeyfromdate);
drop table if exists jeri.hourly_employees cascade;
create table jeri.hourly_employees (
  store_code citext not null,
  department citext not null,
  dist_code citext not null,
  dist_code_description citext not null,
  employee_name citext not null,
  employee_number citext not null, 
  hourly_rate numeric(8,2) not null,
  term_date date not null,
  ek_from_date date not null,
  ek_thru_date date not null,
  primary key (employee_number, ek_from_date));
insert into jeri.hourly_employees  
select a.storecode, a.pydept, a.distcode, a.distribution, a.name, a.employeenumber, 
  a.hourlyrate, a.termdate, a.employeekeyfromdate, a.employeekeythrudate
from ads.ext_edw_employee_dim a
where a.payrollClassCode = 'H'
  and a.pydeptcode in ('06','03')
  and a.storecode in ('RY1','RY2')
  and a.termdate > '01/01/2017';   
  

-- drop table if exists emp_days;
-- create temp table emp_days as
-- select aa.the_year, aa.year_month, aa.biweekly_pay_period_sequence, sunday_to_saturday_week, aa.the_date, 
--   bb.storecode, bb.pydept, bb.distcode, bb.distribution, bb.name, bb.employeenumber, 
--   bb.hourlyrate, bb.termdate, bb.employeekeyfromdate, bb.employeekeythrudate
-- from dds.dim_date aa
-- left join employees bb on aa.the_date between bb.employeekeyfromdate and bb.employeekeythrudate
-- where aa.the_date between '01/01/2017' and current_date;
-- create unique index on emp_days(the_date, employeenumber);
drop table if exists jeri.emp_days cascade;
create table jeri.emp_days (
  the_year integer not null,
  year_month integer not null,
  biweekly_pp_seq integer not null,
  sunday_to_saturday_week integer not null,
  the_date date not null,
  employee_number citext not null,
  ek_from_date date not null,
  foreign key (employee_number,ek_from_date) references jeri.hourly_employees(employee_number, ek_from_date),
  primary key (the_date,employee_number));
insert into jeri.emp_days  
select aa.the_year, aa.year_month, aa.biweekly_pay_period_sequence, 
  aa.sunday_to_saturday_week, aa.the_date, bb.employee_number, bb.ek_from_date
from dds.dim_date aa
inner join jeri.hourly_employees bb on aa.the_date between bb.ek_from_date and bb.ek_thru_date
where aa.the_date between '01/01/2017' and current_date;

-- clock hours by day
-- drop table if exists clock_hours_day;
-- create temp table clock_hours_by_day as
select a.*, coalesce(b.reg_hours, 0), coalesce(b.ot_hours, 0), coalesce(b.ot_hours, 0) * hourlyrate
from emp_days a
left join arkona.xfm_pypclockin b on a.employeenumber = b.employee_number
  and a.the_date = b.the_date
where a.year_month = 201804
  and a.storecode = 'RY1'
  and coalesce(b.reg_hours, 0) + coalesce(b.ot_hours, 0) <> 0
order by name, the_date


select name, count(*) 
from (select a.*, coalesce(b.reg_hours, 0), coalesce(b.ot_hours, 0)
from emp_days a
left join arkona.xfm_pypclockin b on a.employeenumber = b.employee_number
  and a.the_date = b.the_date
where a.year_month = 201804
  and a.storecode = 'RY1') x group by name

select *
from arkona.xfm_pypclockin
limit 100

select jeri.json_get_store_clock_hours('RY1')


select sum(reg_hours), sum(ot_hours), sum(1.5 * hourlyrate * ot_hours)
from emp_days a
left join arkona.xfm_pypclockin b on a.employeenumber = b.employee_number
  and a.the_date = b.the_date
where a.storecode = 'ry1'  
  and a.year_month = 201804
--   and a.the_year = 2018
  and a.pydept = 'PARTS'
--   and a.distcode = 'PDQW'
--   and a.name = 'green, harrison'


-- 5/4/
do i really need the emp_days?
NOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO

so,  modify jeri.hourly_employees to include all hourly employees with a non-split distcode

drop table if exists jeri.hourly_employees cascade;
create table jeri.hourly_employees (
  store_code citext not null,
  department citext not null,
  dist_code citext not null,
  dist_code_description citext not null,
  employee_name citext not null,
  employee_number citext not null, 
  hourly_rate numeric(8,2) not null,
  term_date date not null,
  ek_from_date date not null,
  ek_thru_date date not null,
  primary key (employee_number, ek_from_date));

  
insert into jeri.hourly_employees  
-- this enforces no split dist codes
select a.storecode, a.pydept, a.distcode, a.distribution, a.name, a.employeenumber, 
  a.hourlyrate, a.termdate, a.employeekeyfromdate, a.employeekeythrudate
from ads.ext_edw_employee_dim a
inner join arkona.xfm_pyactgr c on a.distcode = c.dist_code
  and a.storecode = c.company_number
  and c.current_row = true
  and c.gross_dist = 100
where a.payrollClassCode = 'H'
--   and a.pydeptcode in ('06','03')
  and a.storecode in ('RY1','RY2')
  and a.termdate > '01/01/2017'
  and a.employeekeythrudate > '12/31/2017'
order by a.storecode, a.pydept, a.distcode, a.distribution, a.name


select * from jeri.hourly_employees where employee_number = '113000'  

select jeri.json_get_store_clock_hours('RY1')



 drop table if exists jeri.clock_hour_tracking;
create table jeri.clock_hour_tracking (
  store_code citext not null,
  department citext not null,
  dist_code citext not null,
  dist_code_description citext not null,
  the_year integer not null,
  year_month integer not null,
  employee_name citext not null,
  employee_number citext not null,
  regular_hours numeric (8,2) not null default 0,
  ot_hours numeric (8,2) not null default 0,
  ot_comp numeric (8,2) not null default 0,
  primary key (employee_number,year_month));


-- this populates jeri.clock_hour_tracking
truncate jeri.clock_hour_tracking;
insert into jeri.clock_hour_tracking
select b.store_Code, b.department, b.dist_code, b.dist_code_description, 
  aa.the_year, aa.year_month, b.employee_name, b.employee_number,
  sum(a.reg_hours) as reg_hours, sum(a.ot_hours) as ot_hours, 
  sum(ot_hours * hourly_rate) as ot_comp
from arkona.xfm_pypclockin a
inner join dds.dim_date aa on a.the_date = aa.the_date
inner join jeri.hourly_employees b on a.employee_number = b.employee_number
  and a.the_date between b.ek_from_date and b.ek_thru_date
where a.the_date between '01/01/2018' and current_date
  and a.employee_number not in ('177950', '1109827','113000' ) -----------------------------------------------2 depts in same month
group by  b.store_Code, b.department, b.dist_code, b.dist_code_description, 
  aa.the_year, aa.year_month, b.employee_name, b.employee_number

------------------------------------------------------------------------------------------------
-- 5/5/18  
------------------------------------------------------------------------------------------------
lets see what it looks like using accounting departments, based on gross distribution account in pyactgr,
using the account department from fin.dim_account (chart of accounts)

select * from arkona.ext_pyhshdta limit 100
-- using accounting department requires adding department to PK due to split dist codes
-- dept is not enuf, multiple accounts per dept, eg 22201B/22210B
-- so, add account
-- add the distrib percent
drop table if exists jeri.hourly_employees cascade;
create table jeri.hourly_employees (
  store_code citext not null,
  department citext not null,
  dist_code citext not null,
  dist_code_description citext not null,
  employee_name citext not null,
  employee_number citext not null, 
  hourly_rate numeric(8,2) not null,
  term_date date not null,
  ek_from_date date not null,
  ek_thru_date date not null,
  gross_account citext not null,
  dist_percent numeric(3,2) not null,
  primary key (employee_number, department, gross_account, ek_from_date));

  
insert into jeri.hourly_employees  
-- include split dist codes
-- derive department from dim_account
-- add the
select a.storecode, d.department, a.distcode, a.distribution, a.name, a.employeenumber, 
  a.hourlyrate, a.termdate, a.employeekeyfromdate, a.employeekeythrudate, d.account, c.gross_dist/100
--   c.gross_dist, gross_expense_act_, 
--   d.department_code, d.department
from ads.ext_edw_employee_dim a
inner join arkona.xfm_pyactgr c on a.distcode = c.dist_code
  and a.storecode = c.company_number
  and c.current_row = true
--   and c.gross_dist = 100
inner join fin.dim_account d on c.gross_expense_act_ = d.account
  and d.current_row = true
where a.payrollClassCode = 'H'
--   and a.pydeptcode in ('06','03')
  and a.storecode in ('RY1','RY2')
  and a.termdate > '01/01/2017'
  and a.employeekeythrudate > '12/31/2017'
order by employeenumber


select *
from (
select *
from (  -- ok pyhsdta is using pymast department
select 'pyhshdta', department_code, distrib_code, count(*) from arkona.ext_pyhshdta where payroll_cen_year > 116 group by department_code, distrib_code order by department_code, distrib_code) a
full outer join (
select 'pymast', department_code, distrib_code, count(*) from arkona.ext_pymast group by department_code, distrib_code order by department_code, distrib_code) b on a.distrib_code = b.distrib_code
full outer join (
select 'pyactgr', b.department_code, b.department, a.dist_code from arkona.ext_pyactgr a inner join fin.dim_account b on a.gross_expense_act_ = b.account 
group by b.department_code, b.department, a.dist_code order by b.department_code, b.department, a.dist_code) c on a.distrib_code = c.dist_code-- 
select 'pyhshdta', department_code, distrib_code, count(*) from arkona.ext_pyhshdta where payroll_cen_year > 116 group by department_code, distrib_code order by department_code, distrib_code) a
full outer join (
select 'pymast', department_code, distrib_code, count(*) from arkona.ext_pymast group by department_code, distrib_code order by department_code, distrib_code) b on a.distrib_code = b.distrib_code
full outer join (
select 'pyactgr', b.department_code, b.department, a.dist_code from arkona.ext_pyactgr a inner join fin.dim_account b on a.gross_expense_act_ = b.account 
group by b.department_code, b.department, a.dist_code order by b.department_code, b.department, a.dist_code) c on a.distrib_code = c.dist_code

select * from jeri.hourly_employees where 

 drop table if exists jeri.clock_hour_tracking cascade;
 -- split dist codes means mult empl/year_month, add department to PK
create table jeri.clock_hour_tracking (
  store_code citext not null,
  department citext not null,
  dist_code citext not null,
  dist_code_description citext not null,
  the_year integer not null,
  year_month integer not null,
  employee_name citext not null,
  employee_number citext not null,
  regular_hours numeric (8,2) not null default 0,
  ot_hours numeric (8,2) not null default 0,
  ot_comp numeric (8,2) not null default 0,
  primary key (employee_number,department, year_month));

  
-- this populates jeri.clock_hour_tracking
-- multiple names per year_month, take name out of groupking
truncate jeri.clock_hour_tracking;
insert into jeri.clock_hour_tracking
select b.store_Code, b.department, b.dist_code, b.dist_code_description, 
  aa.the_year, aa.year_month, max(b.employee_name), b.employee_number,
  sum(a.reg_hours * b.dist_percent) as reg_hours, sum(a.ot_hours * b.dist_percent) as ot_hours, 
  sum(ot_hours * b.dist_percent * hourly_rate) as ot_comp
from arkona.xfm_pypclockin a
inner join dds.dim_date aa on a.the_date = aa.the_date
inner join jeri.hourly_employees b on a.employee_number = b.employee_number
  and a.the_date between b.ek_from_date and b.ek_thru_date
where a.the_date between '01/01/2018' and current_date
group by  b.store_Code, b.department, b.dist_code, b.dist_code_description, 
  aa.the_year, aa.year_month, b.employee_number
order by store_code, dist_code

